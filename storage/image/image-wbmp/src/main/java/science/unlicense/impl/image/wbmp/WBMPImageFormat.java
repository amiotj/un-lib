
package science.unlicense.impl.image.wbmp;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * Wireless Application Protocol Bitmap Format.
 *
 * resources :
 * http://en.wikipedia.org/wiki/Wireless_Application_Protocol_Bitmap_Format
 * http://www.wapforum.org/what/technical/SPEC-WAESpec-19990524.pdf
 *
 * @author Johann Sorel
 */
public class WBMPImageFormat extends AbstractImageFormat{

    public WBMPImageFormat() {
        super(new Chars("wbmp"),
              new Chars("WBMP"),
              new Chars("Wireless Application Protocol Bitmap Format"),
              new Chars[]{
                  new Chars("image/x-wap.wbmp"),
                  new Chars("image/vnd-wap.wbmp")
              },
              new Chars[]{
                  new Chars("wbmp")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new WBMPImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
