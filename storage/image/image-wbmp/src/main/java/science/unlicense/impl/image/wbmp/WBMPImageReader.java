package science.unlicense.impl.image.wbmp;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.ImageSetMetadata.*;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.PlanarRawModel2D;

/**
 *
 * @author Johann Sorel
 */
public class WBMPImageReader extends AbstractImageReader{

    private byte type;
    private int width;
    private int height;
    private DefaultTypedNode mdImage;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws science.unlicense.api.io.IOException {
        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        type = ds.readByte();
        if(type != 0){
            throw new IOException("Unvalid wbmp type : "+type);
        }

        final byte fixedHeader = ds.readByte();
        if(fixedHeader != 0){
            throw new IOException("Unvalid fix header value : "+fixedHeader);
        }

        width = readInt(ds);
        height = readInt(ds);

        mdImage =
        new DefaultTypedNode(MD_IMAGE.getType(),new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    /**
     * WBMP define variable size integer.
     * the first bit of each byte indicate the value continues on the next byte.
     *
     * @param ds
     * @return int
     * @throws IOException
     */
    private static int readInt(final DataInputStream ds) throws IOException{
        int b = ds.readByte();
        int val = 0;
        for(;;){
            val = (val << 7) | (b & 0x7F);
            if((b&0x80) != 0){
                //there is a next byte
                b = ds.readByte();
            }else{
                break;
            }
        }
        return val;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws science.unlicense.api.io.IOException {
        readMetadatas(stream);
        final DataInputStream ds = new DataInputStream(stream);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out);
        for(int y=0;y<height;y++){
            for(int x=0;x<width;x++){
                final int b = ds.readBits(1);
                ods.writeBit(b);
            }
            ds.skipToByteEnd();
        }
        ods.flush();

        final Buffer bank = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());
        final RawModel sm = new PlanarRawModel2D(RawModel.TYPE_1_BIT, 1);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

}
