package science.unlicense.impl.image.ani;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.image.DefaultImageReadParameters;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.model.Metadata;
import science.unlicense.impl.binding.riff.RIFFReader;
import science.unlicense.impl.image.ani.model.AnihChunk;
import science.unlicense.impl.image.ani.model.IconChunk;
import science.unlicense.impl.image.ani.model.RateChunk;
import science.unlicense.impl.image.ani.model.SeqChunk;

/**
 *
 * @author Johann Sorel
 */
public class ANIImageReader extends AbstractReader implements ImageReader{

    @Override
    public ImageReadParameters createParameters() {
        return new DefaultImageReadParameters();
    }

    @Override
    public Chars[] getMetadataNames() throws IOException {
        return new Chars[0];
    }

    @Override
    public Metadata getMetadata(Chars name) throws IOException {
        return null;
    }

    @Override
    public Image read(ImageReadParameters params) throws IOException {
        
        final RIFFReader reader = createRiffReader();
        while(reader.hasNext()){
            System.out.println(reader.next());
        }
        
        throw new IOException("TODO");
        
    }

    public RIFFReader createRiffReader() throws IOException{
        final Dictionary types = new HashDictionary();
        types.add(ANIConstants.TYPE_ANIHEADER, AnihChunk.class);
        types.add(ANIConstants.TYPE_SEQUENCE, SeqChunk.class);
        types.add(ANIConstants.TYPE_RATE, RateChunk.class);
        types.add(ANIConstants.TYPE_ICON, IconChunk.class);
        final RIFFReader reader = new RIFFReader(types);
        reader.setInput(input);
        return reader;
    }
    
}
