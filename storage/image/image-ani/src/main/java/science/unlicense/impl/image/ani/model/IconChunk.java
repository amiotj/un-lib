
package science.unlicense.impl.image.ani.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.image.ani.ANIConstants;

/**
 *
 * @author Johann Sorel
 */
public class IconChunk extends DefaultChunk {

    public byte[] imgData;

    public IconChunk() {
        super(ANIConstants.TYPE_SEQUENCE);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        imgData = ds.readFully(new byte[(int)size]);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUInt(size);
        ds.write(imgData);
    }
    
}
