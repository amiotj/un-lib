
package science.unlicense.impl.image.ani;

import science.unlicense.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public final class ANIConstants {

    private ANIConstants(){}
    
    public static final Chars TYPE_ACON = new Chars("ACON");

    public static final Chars TYPE_ANIHEADER = new Chars(new byte[]{'a','n','i','h'});
    public static final Chars TYPE_SEQUENCE = new Chars(new byte[]{'s','e','q',' '});
    public static final Chars TYPE_RATE = new Chars(new byte[]{'r','a','t','e'});
    public static final Chars TYPE_ICON = new Chars(new byte[]{'i','c','o','n'});
    
    public static final Chars FRAME_LIST = new Chars("fram");
    
}
