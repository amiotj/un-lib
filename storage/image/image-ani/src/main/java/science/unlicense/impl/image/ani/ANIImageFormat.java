
package science.unlicense.impl.image.ani;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;
import science.unlicense.impl.binding.riff.RIFFConstants;

/**
 * Microsoft animated cursor format.
 * 
 * Resources :
 * https://en.wikipedia.org/wiki/ANI_(file_format)
 * http://www.gdgsoft.com/anituner/help/aniformat.htm
 * https://www.daubnet.com/en/file-format-ani
 * 
 * 
 * @author Johann Sorel
 */
public class ANIImageFormat extends AbstractImageFormat{

    public ANIImageFormat() {
        super(new Chars("ani"),
              new Chars("ANI"),
              new Chars("Animated mouse cursor"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ani")
              },
              new byte[][]{
                    RIFFConstants.CHUNK_RIFF.toBytes(),
                    RIFFConstants.CHUNK_RIFX.toBytes(),
                });
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new ANIImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
