
package science.unlicense.impl.image.apng;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.media.AbstractMediaCapabilities;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import static science.unlicense.impl.image.png.PNGConstants.CHUNK_IEND;
import static science.unlicense.impl.image.png.PNGConstants.SIGNATURE;

/**
 * PNG extension for animated images.
 *
 * @author Johann Sorel
 */
public class APNGMediaFormat extends AbstractMediaFormat{

    public static final APNGMediaFormat INSTANCE = new APNGMediaFormat();
    
    public APNGMediaFormat() {
        super(new Chars("apng"),
              new Chars("Animated-PNG"),
              new Chars("Animated-PNG"),
              new Chars[]{
                  new Chars("image/png"),
              },
              new Chars[]{
                  new Chars("png"),
                  new Chars("apng")
              },
              new byte[][]{SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public MediaStore createStore(Object input) throws IOException {
        return new APNGMediaStore((Path)input);
    }

    public boolean canDecode(Object input) throws IOException {
        if(super.canDecode(input)){
            //check if this png contains an acTL chunk
            boolean isApng = false;
            
            final boolean[] closeStream = new boolean[1];
            final ByteInputStream is = IOUtilities.toInputStream(input, closeStream);
            final DataInputStream ds = new DataInputStream(is, NumberEncoding.BIG_ENDIAN);
            //skip signature
            ds.skipFully(8);
            //quick loop on chunks
            while(true){
                //read chunk header
                final int length = ds.readInt();
                final byte[] btype = ds.readFully(new byte[4]);
                final Chars type = new Chars(btype);
                //skip chunk data + crc
                ds.skipFully(length + 4);

                if(APNGMetaModel.CHUNK_acTL.equals(type)){
                    isApng = true;
                    break;
                }else if(CHUNK_IEND.equals(type)){
                    //we have finish reading file
                    break;
                }
            }
            if(closeStream[0]) is.close();
            
            return isApng;
        }
        return false;
    }
    
    

}
