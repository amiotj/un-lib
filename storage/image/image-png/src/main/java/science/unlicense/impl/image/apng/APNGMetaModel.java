package science.unlicense.impl.image.apng;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultNodeCardinality;
import science.unlicense.api.model.tree.NodeCardinality;
import static science.unlicense.impl.image.png.PNGMetadata.*;

/**
 * source :
 * http://en.wikipedia.org/wiki/APNG
 * https://wiki.mozilla.org/APNG_Specification
 *
 * @author Johann Sorel
 */
public final class APNGMetaModel {

    /**
     * APNG_DISPOSE_OP_NONE: no disposal is done on this frame before
     * rendering the next; the contents of the output buffer are left as is.
     */
    public static final byte APNG_DISPOSE_OP_NONE = 0;
    /**
     * APNG_DISPOSE_OP_BACKGROUND: the frame's region of the output
     * buffer is to be cleared to fully transparent black before rendering the next frame.
     */
    public static final byte APNG_DISPOSE_OP_BACKGROUND = 1;
    /**
     * APNG_DISPOSE_OP_PREVIOUS: the frame's region of the output buffer
     * is to be reverted to the previous contents before rendering the next frame.
     */
    public static final byte APNG_DISPOSE_OP_PREVIOUS = 2;

    /**
     * If `blend_op` is APNG_BLEND_OP_SOURCE all color components of the frame,
     * including alpha, overwrite the current contents of the frame's output buffer region
     */
    public static final byte APNG_BLEND_OP_SOURCE = 0;
    /**
     * If `blend_op` is APNG_BLEND_OP_OVER the frame should be composited
     * onto the output buffer based on its alpha, using a simple OVER
     * operation as described in the "Alpha Channel Processing" section of
     * the PNG specification [PNG-1.2].
     * Note that the second variation of the sample code is applicable.
     */
    public static final byte APNG_BLEND_OP_OVER = 1;

    public static final Chars CHUNK_acTL = new Chars("acTL");
    public static final Chars CHUNK_fcTL = new Chars("fcTL");
    public static final Chars CHUNK_fdAT = new Chars("fdAT");

    public static final NodeCardinality MD_acTL;
    public static final NodeCardinality MD_acTL_NUMFRAMES = new DefaultNodeCardinality(new Chars("num_frames"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_acTL_NUMPLAYS  = new DefaultNodeCardinality(new Chars("num_plays"),null, null,Integer.class, false, null);

    public static final NodeCardinality MD_fcTL;
    public static final NodeCardinality MD_fcTL_SEQUENCENUMBER = new DefaultNodeCardinality(new Chars("sequence_number"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_WIDTH  = new DefaultNodeCardinality(new Chars("width"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_HEIGHT  = new DefaultNodeCardinality(new Chars("height"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_XOFFSET  = new DefaultNodeCardinality(new Chars("x_offset"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_YOFFSET  = new DefaultNodeCardinality(new Chars("y_offset"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_DELAYNUM  = new DefaultNodeCardinality(new Chars("delay_num"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_DELAYDEN  = new DefaultNodeCardinality(new Chars("delay_den"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_fcTL_DISPOSEOP  = new DefaultNodeCardinality(new Chars("dispose_op"),null, null,Byte.class, false, null);
    public static final NodeCardinality MD_fcTL_BLENDOP  = new DefaultNodeCardinality(new Chars("blend_op"),null, null,Byte.class, false, null);

    public static final NodeCardinality MD_fdAT;
    public static final NodeCardinality MD_fdAT_SEQUENCENUMBER = new DefaultNodeCardinality(new Chars("sequence_number"),null, null,Integer.class, false, null);

    static {

        MD_acTL = new DefaultNodeCardinality(CHUNK_acTL,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_acTL_NUMFRAMES,
            MD_acTL_NUMPLAYS,
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_fcTL = new DefaultNodeCardinality(CHUNK_fcTL,null, null,Sequence.class, null, false, 0,Integer.MAX_VALUE,new NodeCardinality[]{
            MD_fcTL_SEQUENCENUMBER,
            MD_fcTL_WIDTH,
            MD_fcTL_HEIGHT,
            MD_fcTL_XOFFSET,
            MD_fcTL_YOFFSET,
            MD_fcTL_DELAYNUM,
            MD_fcTL_DELAYDEN,
            MD_fcTL_DISPOSEOP,
            MD_fcTL_BLENDOP,
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_fdAT = new DefaultNodeCardinality(CHUNK_fdAT,null, null,Sequence.class, null, false, 0,Integer.MAX_VALUE,new NodeCardinality[]{
            MD_fdAT_SEQUENCENUMBER,
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });

    }

    private APNGMetaModel(){}

}
