
package science.unlicense.impl.image.png;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.cryptography.hash.CRC32;
import science.unlicense.api.color.Color;
import science.unlicense.impl.io.zlib.ZlibOutputStream;
import science.unlicense.api.image.AbstractImageWriter;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageWriteParameters;
import science.unlicense.api.image.color.PixelBuffer;

/**
 * PNG image writer, images are written as RGBA.
 * 
 * @author Johann Sorel
 */
public class PNGImageWriter extends AbstractImageWriter{

    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {
        
        final DataOutputStream ds = new DataOutputStream(stream);
        
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        
        //compression
        final ArrayOutputStream stemp = new ArrayOutputStream();
        final DataOutputStream dstemp = new DataOutputStream(stemp);
        final CRC32 crc = new CRC32();
        
        
        //write signature ------------------------------------------------------
        ds.write(PNGConstants.SIGNATURE);
        
        //write IHDR -----------------------------------------------------------
        stemp.getBuffer().removeAll();
        
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final byte bitdepth = 8;
        final byte colortype = PNGConstants.COLOR_TRUECOLOUR_ALPHA;
        final byte compression = PNGConstants.COMPRESSION_DEFAULT;
        final byte filter = PNGConstants.FILTER_DEFAULT;
        final byte interlace = PNGConstants.INTERLACE_NONE;
        
        dstemp.write(PNGConstants.CHUNK_IHDR.toBytes(CharEncodings.US_ASCII));        
        dstemp.writeInt(width);
        dstemp.writeInt(height);
        dstemp.writeByte(bitdepth);
        dstemp.writeByte(colortype);
        dstemp.writeByte(compression);
        dstemp.writeByte(filter);
        dstemp.writeByte(interlace);
        dstemp.flush();
        byte[] array = stemp.getBuffer().toArrayByte();
        crc.reset();
        crc.update(array);
        ds.writeInt(array.length-4);
        ds.write(array);
        ds.writeInt((int)crc.getResultLong());
        
        
        //write IDAT -----------------------------------------------------------
        stemp.getBuffer().removeAll();
        stemp.write(PNGConstants.CHUNK_IDAT.toBytes(CharEncodings.US_ASCII));
        final ByteOutputStream zlib = new ZlibOutputStream(stemp);
        
        //write each color
        final int[] coord = new int[2];
        for (int y = 0; y <height; y++) {
            //normaly we should calculate the best scanline,but for now lets just use NONE
            zlib.write((byte)PNGConstants.FILTER_TYPE_NONE);
            
            coord[1] = y;
            for (int x = 0; x < width; x++) {
                coord[0] = x;
                final Color color = cm.getColor(coord);
                zlib.write((byte) (color.getRed() * 255));
                zlib.write((byte) (color.getGreen() * 255));
                zlib.write((byte) (color.getBlue() * 255));
                zlib.write((byte) (color.getAlpha() * 255));
            }
        }
        
        
        zlib.flush();
        zlib.close();
        stemp.flush();
        array = stemp.getBuffer().toArrayByte();
        crc.reset();
        crc.update(array);
        ds.writeInt(array.length-4);
        ds.write(array);
        ds.writeInt((int)crc.getResultLong());
                
        
        //write IEND -----------------------------------------------------------
        ds.write(PNGConstants.IENDCHUNK);
        
    }
    
}
