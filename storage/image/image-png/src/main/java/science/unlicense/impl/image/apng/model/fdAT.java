
package science.unlicense.impl.image.apng.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.apng.APNGMetaModel;
import static science.unlicense.impl.image.apng.APNGMetaModel.*;
import science.unlicense.impl.image.png.model.IDAT;

/**
 *
 * @author Johann Sorel
 */
public class fdAT extends IDAT {

    public fdAT() {
        super(APNGMetaModel.MD_fdAT);
    }

    public int getHeaderSize(){
        return 4;
    }

    public void read(DataInputStream ds, int length) throws IOException {
        addChild(MD_fcTL_SEQUENCENUMBER, ds.readInt());
        ds.skipFully(length-4);
    }

}
