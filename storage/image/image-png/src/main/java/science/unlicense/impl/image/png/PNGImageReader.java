
package science.unlicense.impl.image.png;

import science.unlicense.api.character.Chars;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.io.zlib.ZlibInputStream;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.ImageSetMetadata.*;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.ImageSetMetadata;
import science.unlicense.api.image.color.ColorIndex;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.IndexedColorModel;
import static science.unlicense.impl.image.png.PNGConstants.*;
import static science.unlicense.impl.image.png.PNGMetadata.*;
import science.unlicense.impl.image.png.io.Adam7UnScanInputStream;
import science.unlicense.impl.image.png.io.IDatInputStream;
import science.unlicense.impl.image.png.io.UnScanInputStream;
import science.unlicense.impl.image.png.model.Chunk;
import science.unlicense.impl.image.png.model.IDAT;
import science.unlicense.impl.image.png.model.IHDR;
import science.unlicense.impl.image.png.model.PLTE;
import science.unlicense.impl.image.png.model.tRNS;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;

/**
 *
 * @author Johann Sorel
 */
public class PNGImageReader extends AbstractImageReader{

    private PNGMetadata mdPng = null;
    private ImageSetMetadata mdImageSet = null;
    private PLTE plte = null;
    private tRNS alphas = null;

    private final Dictionary chunkTable = new HashDictionary();

    public PNGImageReader() {
        chunkTable.add(CHUNK_IHDR,IHDR.class);
        chunkTable.add(CHUNK_PLTE,PLTE.class);
        chunkTable.add(CHUNK_IDAT,IDAT.class);
        chunkTable.add(CHUNK_IEND,Chunk.class);
        chunkTable.add(CHUNK_cHRM,Chunk.class);
        chunkTable.add(CHUNK_gAMA,Chunk.class);
        chunkTable.add(CHUNK_iCCP,Chunk.class);
        chunkTable.add(CHUNK_sBIT,Chunk.class);
        chunkTable.add(CHUNK_sRGB,Chunk.class);
        chunkTable.add(CHUNK_bKGD,Chunk.class);
        chunkTable.add(CHUNK_hIST,Chunk.class);
        chunkTable.add(CHUNK_tRNS,tRNS.class);
        chunkTable.add(CHUNK_pHYs,Chunk.class);
        chunkTable.add(CHUNK_sPLT,Chunk.class);
        chunkTable.add(CHUNK_tIME,Chunk.class);
        chunkTable.add(CHUNK_iTXt,Chunk.class);
        chunkTable.add(CHUNK_tEXt,Chunk.class);
        chunkTable.add(CHUNK_zTXt,Chunk.class);
    }

    public Dictionary getChunkTable() {
        return chunkTable;
    }

    private Chunk newInstance(Chars chunkName){
        final Class clazz = (Class) chunkTable.getValue(chunkName);
        if(clazz == null) return null;
        try {
            return (Chunk) clazz.newInstance();
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    public BacktrackInputStream getInputBSStream() throws IOException{
        return getInputAsBacktrackStream();
    }

    protected Dictionary readMetadatas(BacktrackInputStream bs) throws IOException {

        final DataInputStream ds = new DataInputStream(bs, NumberEncoding.BIG_ENDIAN);
        final byte[] signature = new byte[8];
        ds.readFully(signature);
        if(!Arrays.equals(signature, SIGNATURE)){
            throw new IOException("Not a PNG file");
        }

        mdPng = new PNGMetadata();
        IHDR ihdr = null;

        while(true){
            //read chunk header
            final int length = ds.readInt();
            final byte[] btype = ds.readFully(new byte[4]);
            final Chars type = new Chars(btype);

            // CRITICAL CHUNKS /////////////////////////////////////////////////
            Chunk chunk = newInstance(type);
            if(chunk != null){
                mdPng.getChildren().add(chunk);
                chunk.addChild(MD_CHUNK_DATA_LENGTH, length);
                chunk.addChild(MD_CHUNK_DATA_OFFSET, bs.position());
                chunk.read(ds,length);

                if(chunk instanceof IHDR){
                    ihdr = (IHDR) chunk;
                }else if(chunk instanceof PLTE){
                    plte = (PLTE) chunk;
                    if(alphas!=null){
                        plte.fix(alphas,ihdr.getColorType());
                    }
                }else if(chunk instanceof tRNS){
                    alphas = (tRNS) chunk;
                    if(plte!=null){
                        plte.fix(alphas,ihdr.getColorType());
                    }
                }

            }else{
                //skip unknowned chunk
                ds.skip(length);
            }
            //skip the crc
            ds.readInt();

            if(CHUNK_IEND.equals(type)){
                //we have finish reading file
                break;
            }
        }

        mdImageSet = new ImageSetMetadata();
        mdImageSet.getChildren().add(
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,ihdr.getWidth())}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,ihdr.getHeight())})
        }));

        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        metas.add(mdPng.getType().getId(), mdPng);
        return metas;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws IOException {

        if(mdPng == null){
            //read metas
            readMetadatas(stream);
            stream.rewind();
        }

        final IHDR ihdr = (IHDR) mdPng.getChild(MD_IHDR);
        final int width  = ihdr.getWidth();
        final int height = ihdr.getHeight();

        // concatenate idat chunks.
        final TypedNode[] idatas = mdPng.getChildren(MD_IDAT);
        return readSingleImage(stream, idatas, width, height, ihdr);
    }

    public Image readSingleImage(BacktrackInputStream stream, TypedNode[] idatas,
            int width, int height, IHDR ihdr) throws IOException{

        final int bitdepth      = ihdr.getBitDepth();
        final int colortype     = ihdr.getColorType();
        final int compression   = ihdr.getCompression();
        final int filter        = ihdr.getFilter();
        final int interlace     = ihdr.getInterlace();

        if(compression != 0) throw new IOException("Compression method not supported. Only 0 : Deflate/inflate is supported.");
        if(filter != 0) throw new IOException("Filter method not supported. Only 0 : adaptive filtering with five basic filter types is supported.");
        if(interlace != 0 && interlace != 1) throw new IOException("Interlace method not supported. Only 0 or 1 : none or Adam7 is supported.");

        final int bitsPerPixel = getBitsPerPixel(colortype, bitdepth);

        // concatenate idat chunks.
        ByteInputStream dataStream = new IDatInputStream(stream,idatas);
        // decompress
        dataStream = new ZlibInputStream(dataStream);

        //unfilter and unscan
        if(interlace == 0){
            //no interlace
            dataStream = new UnScanInputStream(dataStream, width, height, bitsPerPixel);
        }else{
            //adam7 interlace
            dataStream = new Adam7UnScanInputStream(dataStream, width, height, bitsPerPixel);
        }

        final ArrayOutputStream datas = new ArrayOutputStream();
        int b;
        while( (b = dataStream.read()) != -1){
            datas.write((byte)b);
        }

        final int sampleType = toSampleType(bitdepth);
        final int nbSample = toNbSample(colortype);

        final Buffer bank = DefaultBufferFactory.wrap(datas.getBuffer().getBackArray());
        final RawModel sm = new InterleavedRawModel(sampleType, nbSample);

        final ColorModel cm;
        if(colortype == COLOR_GREYSCALE){
            cm = new DirectColorModel(sm, new int[]{0,0,0,-1},false);
        }else if(colortype == COLOR_TRUECOLOUR){
            cm = new DirectColorModel(sm, new int[]{0,1,2,-1},false);
        }else if(colortype == COLOR_INDEXED){
            cm = new IndexedColorModel(sampleType, new ColorIndex(plte.palette));
        }else if(colortype == COLOR_GREYSCALE_ALPHA){
            cm = new DirectColorModel(sm, new int[]{0,0,0,1},false);
        }else if(colortype == COLOR_TRUECOLOUR_ALPHA){
            cm = new DirectColorModel(sm, new int[]{0,1,2,3},false);
        }else{
            throw new RuntimeException("Unknowned color model : " + colortype);
        }

        return new DefaultImage(bank, new Extent.Long(width, height), sm,cm);
    }

    private int getBitsPerPixel(int colortype, int bitdepth) throws IOException{
        final int bitsPerPixel;

        //check valid values
        if(colortype == COLOR_GREYSCALE){
            if( !(bitdepth==1 || bitdepth==2 || bitdepth==4 || bitdepth==8 || bitdepth==16)){
                throw new IOException("Unvalid bitdepth "+bitdepth+" for color type "+colortype);
            }
            bitsPerPixel = bitdepth;

        }else if(colortype == COLOR_TRUECOLOUR){
            if( !(bitdepth==8 || bitdepth==16)){
                throw new IOException("Unvalid bitdepth "+bitdepth+" for color type "+colortype);
            }
            bitsPerPixel = bitdepth*3;

        }else if(colortype == COLOR_INDEXED){
            if( !(bitdepth==1 || bitdepth==2 || bitdepth==4 || bitdepth==8 )){
                throw new IOException("Unvalid bitdepth "+bitdepth+" for color type "+colortype);
            }
            bitsPerPixel = bitdepth;

        }else if(colortype == COLOR_GREYSCALE_ALPHA){
            if( !(bitdepth==8 || bitdepth==16 )){
                throw new IOException("Unvalid bitdepth "+bitdepth+" for color type "+colortype);
            }
            bitsPerPixel = bitdepth*2;

        }else if(colortype == COLOR_TRUECOLOUR_ALPHA){
            if( !(bitdepth==8 || bitdepth==16 )){
                throw new IOException("Unvalid bitdepth "+bitdepth+" for color type "+colortype);
            }
            bitsPerPixel = bitdepth*4;

        }else{
            throw new IOException("Unknowed color type "+colortype);
        }

        return bitsPerPixel;
    }

    private static int toSampleType(int bitDepth){
        if(bitDepth == 1){
            return RawModel.TYPE_1_BIT;
        }else if(bitDepth == 2){
            return RawModel.TYPE_2_BIT;
        }else if(bitDepth == 4){
            return RawModel.TYPE_4_BIT;
        }else if(bitDepth == 8){
            return RawModel.TYPE_UBYTE;
        }else if(bitDepth == 16){
            return RawModel.TYPE_USHORT;
        }else{
            throw new RuntimeException("unsupported bitDepth.");
        }
    }

    private static int toNbSample(int colorType){
        if(colorType == PNGConstants.COLOR_INDEXED){
            return 1;
        }else if(colorType == PNGConstants.COLOR_GREYSCALE){
            return 1;
        }else if(colorType == PNGConstants.COLOR_GREYSCALE_ALPHA){
            return 2;
        }else if(colorType == PNGConstants.COLOR_TRUECOLOUR){
            return 3;
        }else if(colorType == PNGConstants.COLOR_TRUECOLOUR_ALPHA){
            return 4;
        }else{
            throw new RuntimeException("unsupported colorType.");
        }
    }

}
