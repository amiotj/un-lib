
package science.unlicense.impl.image.png.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.png.PNGMetadata;

/**
 *
 * @author Johann Sorel
 */
public class tRNS extends Chunk {

    public byte[] datas;

    public tRNS() {
        super(PNGMetadata.MD_tRNS);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        datas = ds.readFully(new byte[length]);
    }

}
