package science.unlicense.impl.image.png;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.model.Metadata;
import science.unlicense.api.model.tree.DefaultNodeCardinality;
import science.unlicense.api.model.tree.DefaultNodeType;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeCardinality;

/**
 * source : http://www.w3.org/TR/PNG
 *
 * @author Johann Sorel
 */
public final class PNGMetadata extends DefaultTypedNode implements Metadata{

    public static final DefaultNodeType MD_PNG;
    public static final NodeCardinality MD_CHUNK_DATA_LENGTH  = new DefaultNodeCardinality(new Chars("chuckLength"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_CHUNK_DATA_OFFSET  = new DefaultNodeCardinality(new Chars("chuckOffset"),null, null,Long.class, false, null);
    public static final NodeCardinality MD_IHDR;
    public static final NodeCardinality MD_IHDR_WIDTH         = new DefaultNodeCardinality(new Chars("width"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_IHDR_HEIGHT        = new DefaultNodeCardinality(new Chars("height"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_IHDR_BITDEPTH      = new DefaultNodeCardinality(new Chars("bitDepth"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_IHDR_COLORTYPE     = new DefaultNodeCardinality(new Chars("colorType"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_IHDR_COMPRESSION   = new DefaultNodeCardinality(new Chars("compression"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_IHDR_FILTER        = new DefaultNodeCardinality(new Chars("filter"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_IHDR_INTERLACE     = new DefaultNodeCardinality(new Chars("interlace"),null, null,Integer.class, false, null);

    public static final NodeCardinality MD_PLTE;
    public static final NodeCardinality MD_PLTE_TABLE         = new DefaultNodeCardinality(new Chars("table"),null, null,Color[].class, false, null);
    public static final NodeCardinality MD_IDAT;
    public static final NodeCardinality MD_IEND;
    public static final NodeCardinality MD_cHRM;
    public static final NodeCardinality MD_gAMA;
    public static final NodeCardinality MD_iCCP;
    public static final NodeCardinality MD_sBIT;
    public static final NodeCardinality MD_sRGB;
    public static final NodeCardinality MD_bKGB;
    public static final NodeCardinality MD_hIST;
    public static final NodeCardinality MD_tRNS;
    public static final NodeCardinality MD_pHYs;
    public static final NodeCardinality MD_sPLT;
    public static final NodeCardinality MD_tIME;
    public static final NodeCardinality MD_iTXt;
    public static final NodeCardinality MD_tEXt;
    public static final NodeCardinality MD_zTXt;
    //for unknowned chunks
    public static final NodeCardinality MD_uwkd;


    static {

        MD_IHDR = new DefaultNodeCardinality(PNGConstants.CHUNK_IHDR,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_IHDR_WIDTH,
            MD_IHDR_HEIGHT,
            MD_IHDR_BITDEPTH,
            MD_IHDR_COLORTYPE,
            MD_IHDR_COMPRESSION,
            MD_IHDR_FILTER,
            MD_IHDR_INTERLACE,
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_PLTE = new DefaultNodeCardinality(PNGConstants.CHUNK_PLTE,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_PLTE_TABLE,
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_IDAT = new DefaultNodeCardinality(PNGConstants.CHUNK_IDAT,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_IEND = new DefaultNodeCardinality(PNGConstants.CHUNK_IEND,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_cHRM = new DefaultNodeCardinality(PNGConstants.CHUNK_cHRM,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_gAMA = new DefaultNodeCardinality(PNGConstants.CHUNK_gAMA,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_iCCP = new DefaultNodeCardinality(PNGConstants.CHUNK_iCCP,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_sBIT = new DefaultNodeCardinality(PNGConstants.CHUNK_sBIT,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_sRGB = new DefaultNodeCardinality(PNGConstants.CHUNK_sRGB,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_bKGB = new DefaultNodeCardinality(PNGConstants.CHUNK_bKGD,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_hIST = new DefaultNodeCardinality(PNGConstants.CHUNK_hIST,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_tRNS = new DefaultNodeCardinality(PNGConstants.CHUNK_tRNS,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_pHYs = new DefaultNodeCardinality(PNGConstants.CHUNK_pHYs,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_sPLT = new DefaultNodeCardinality(PNGConstants.CHUNK_sPLT,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_tIME = new DefaultNodeCardinality(PNGConstants.CHUNK_tIME,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_iTXt = new DefaultNodeCardinality(PNGConstants.CHUNK_iTXt,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_tEXt = new DefaultNodeCardinality(PNGConstants.CHUNK_tEXt,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_zTXt = new DefaultNodeCardinality(PNGConstants.CHUNK_zTXt,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });
        MD_uwkd = new DefaultNodeCardinality(PNGConstants.CHUNK_UNKNOWNED,null, null,Sequence.class, null, false, 0,1,new NodeCardinality[]{
            MD_CHUNK_DATA_LENGTH,
            MD_CHUNK_DATA_OFFSET
        });

        MD_PNG = new DefaultNodeType(new Chars("png"),null, null,Sequence.class, false, new NodeCardinality[]{
            MD_IHDR,
            MD_PLTE,
            MD_IDAT,
            MD_IEND,
            MD_cHRM,
            MD_gAMA,
            MD_iCCP,
            MD_sBIT,
            MD_sRGB,
            MD_bKGB,
            MD_hIST,
            MD_tRNS,
            MD_pHYs,
            MD_sPLT,
            MD_tIME,
            MD_iTXt,
            MD_tEXt,
            MD_zTXt,
            MD_uwkd
        });

    }

    public  PNGMetadata(){
        super(MD_PNG);
    }

    @Override
    public Node toNode() {
        return this;
    }

}
