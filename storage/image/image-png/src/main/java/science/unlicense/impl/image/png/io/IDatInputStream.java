
package science.unlicense.impl.image.png.io;

import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.impl.io.ClipInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.png.model.IDAT;

/**
 * Concatenate and deflate idat chunks.
 */
public class IDatInputStream extends AbstractInputStream {

    private final BacktrackInputStream fileStream;
    private final TypedNode[] idatas;
    private int index = 0;
    private ByteInputStream idatStream = null;

    public IDatInputStream(final BacktrackInputStream stream, TypedNode[] idatas) {
        this.fileStream = stream;
        this.idatas = idatas;
    }

    @Override
    public int read() throws IOException {
        while (idatStream != null || index < idatas.length) {
            if (idatStream != null) {
                int b = idatStream.read();
                if (b == -1) {
                    //end of this idat block
                    idatStream = null;
                } else {
                    return b;
                }
            }
            if (index < idatas.length) {
                //open an idatStream
                fileStream.rewind();
                final IDAT idat = (IDAT)idatas[index];
                final int offset = idat.getChunkOffset();
                final int length = idat.getChunkLength();
                final int headerSize = idat.getHeaderSize();
                fileStream.skip(offset+headerSize);
                //clip region to read
                idatStream = new ClipInputStream(fileStream, length-headerSize);
                index++;
            }
        }
        //nothing left in idat blocks.
        return -1;
    }

    @Override
    public void close() throws IOException {
    }

}
