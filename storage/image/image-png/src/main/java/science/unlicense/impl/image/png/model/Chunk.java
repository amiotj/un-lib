
package science.unlicense.impl.image.png.model;

import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.png.PNGMetadata;
import static science.unlicense.impl.image.png.PNGMetadata.*;

/**
 *
 * @author Johann Sorel
 */
public class Chunk extends DefaultTypedNode{

    public Chunk() {
        super(PNGMetadata.MD_uwkd);
    }

    public Chunk(NodeCardinality card) {
        super(card);
    }

    public void read(DataInputStream ds, int length) throws IOException{
        ds.skipFully(length);
    }

    public int getChunkOffset(){
        return (Integer)getChild(MD_CHUNK_DATA_OFFSET).getValue();
    }

    public int getChunkLength(){
        return (Integer)getChild(MD_CHUNK_DATA_LENGTH).getValue();
    }

}
