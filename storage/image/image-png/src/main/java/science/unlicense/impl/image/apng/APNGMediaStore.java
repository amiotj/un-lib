
package science.unlicense.impl.image.apng;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.media.ImageStreamMeta;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.image.ImageSetMetadata;
import science.unlicense.impl.image.apng.model.acTL;
import science.unlicense.impl.image.apng.model.fcTL;
import science.unlicense.impl.image.apng.model.fdAT;
import science.unlicense.impl.image.png.PNGImageReader;
import science.unlicense.impl.image.png.model.IDAT;
import science.unlicense.impl.image.png.model.IHDR;

/**
 *
 * @author Johann Sorel
 */
public class APNGMediaStore extends AbstractMediaStore{

    private final Path path;

    //caches
    private ImageStreamMeta videoMeta = null;
    private IHDR imageInfo = null;
    private acTL videoInfo = null;
    private Sequence frames = null;

    public APNGMediaStore(Path input) {
        super(APNGMediaFormat.INSTANCE,input);
        this.path = input;
    }

    /**
     * Create a PNG reader with extended chunks support.
     *
     * @return PNGImageReader
     * @throws IOException
     */
    private PNGImageReader createReader() throws IOException{
        final PNGImageReader reader = new PNGImageReader();
        reader.getChunkTable().add(APNGMetaModel.CHUNK_acTL, acTL.class);
        reader.getChunkTable().add(APNGMetaModel.CHUNK_fcTL, fcTL.class);
        reader.getChunkTable().add(APNGMetaModel.CHUNK_fdAT, fdAT.class);
        reader.setInput(path);
        return reader;
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        if(videoMeta == null){
            final PNGImageReader reader = createReader();
            final ImageSetMetadata metas = (ImageSetMetadata) reader.getMetadata(new Chars("imageset"));
            final TypedNode imageMeta = metas.getChild(ImageSetMetadata.MD_IMAGE.getId());
            final TypedNode pngMeta = (TypedNode) reader.getMetadata(new Chars("png"));
            buildFrames(pngMeta);
            
            final int nbFrame;
            if(videoInfo==null){
                nbFrame = 1;
            }else{
                nbFrame = videoInfo.getNumFrames();
            }

            videoMeta = new ImageStreamMeta() {

                public TypedNode getImageMetadata() {
                    return imageMeta;
                }

                public int getNbFrames() {
                    return nbFrame;
                }

                public int getFrameRate() {
                    return 0;
                }

                public Chars getName() {
                    return new Chars("video");
                }
            };

        }
        return new MediaStreamMeta[]{videoMeta};
    }

    private void buildFrames(TypedNode mdPNG){
        frames = new ArraySequence();

        final Iterator chunks = mdPNG.getChildren().createIterator();
        APNGFrame frame = null;
        while (chunks.hasNext()) {
            final Object chunk = chunks.next();
            if(chunk instanceof IHDR){
                imageInfo = (IHDR) chunk;
            }else if(chunk instanceof acTL){
                videoInfo = (acTL) chunk;
            }else if(chunk instanceof fcTL){
                frame = new APNGFrame();
                frame.fcTL = (fcTL) chunk;
                frames.add(frame);
            }else if(chunk instanceof IDAT && frame != null){
                frame.datas.add(chunk);
            }
        }

    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        //ensure metas are loaded
        getStreamsMeta();
        //create reader
        final PNGImageReader reader = createReader();
        //ensure metas are loaded
        reader.getMetadataNames();
        
        return new APNGVideoReader(reader, frames, imageInfo);
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
