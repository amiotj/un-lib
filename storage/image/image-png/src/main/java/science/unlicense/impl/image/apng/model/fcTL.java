
package science.unlicense.impl.image.apng.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.png.model.Chunk;
import static science.unlicense.impl.image.apng.APNGMetaModel.*;

/**
 *
 * @author Johann Sorel
 */
public class fcTL extends Chunk {

    public fcTL() {
        super(MD_fcTL);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        addChild(MD_fcTL_SEQUENCENUMBER,    ds.readInt());
        addChild(MD_fcTL_WIDTH,             ds.readInt());
        addChild(MD_fcTL_HEIGHT,            ds.readInt());
        addChild(MD_fcTL_XOFFSET,           ds.readInt());
        addChild(MD_fcTL_YOFFSET,           ds.readInt());
        addChild(MD_fcTL_DELAYNUM,          ds.readUShort());
        addChild(MD_fcTL_DELAYDEN,          ds.readUShort());
        addChild(MD_fcTL_DISPOSEOP,         ds.readByte());
        addChild(MD_fcTL_BLENDOP,           ds.readByte());
    }

    public int getSequenceNumber(){
        return (Integer)getChild(MD_fcTL_SEQUENCENUMBER).getValue();
    }

    public int getWidth(){
        return (Integer)getChild(MD_fcTL_WIDTH).getValue();
    }

    public int getHeight(){
        return (Integer)getChild(MD_fcTL_HEIGHT).getValue();
    }

    public int getXOffset(){
        return (Integer)getChild(MD_fcTL_XOFFSET).getValue();
    }

    public int getYOffset(){
        return (Integer)getChild(MD_fcTL_YOFFSET).getValue();
    }

    public int getDelayNum(){
        return (Integer)getChild(MD_fcTL_DELAYNUM).getValue();
    }

    public int getDelayDen(){
        return (Integer)getChild(MD_fcTL_DELAYDEN).getValue();
    }

    public int getDisposeOp(){
        return (Byte)getChild(MD_fcTL_DISPOSEOP).getValue();
    }

    public int getBlendOp(){
        return (Byte)getChild(MD_fcTL_BLENDOP).getValue();
    }

}
