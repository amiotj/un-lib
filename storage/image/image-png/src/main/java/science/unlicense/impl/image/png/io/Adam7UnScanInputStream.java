
package science.unlicense.impl.image.png.io;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;

/**
 * Adam7 stream.
 */
public class Adam7UnScanInputStream extends AbstractInputStream {

    private static final int[][] ADAM7 = {
        {1,6,4,6,2,6,4,6},
        {7,7,7,7,7,7,7,7},
        {5,6,5,6,5,6,5,6},
        {7,7,7,7,7,7,7,7},
        {3,6,4,6,3,6,4,6},
        {7,7,7,7,7,7,7,7},
        {5,6,5,6,5,6,5,6},
        {7,7,7,7,7,7,7,7}
    };

    private final byte[] fullimage;
    private int index = 0;

    public Adam7UnScanInputStream(final ByteInputStream stream,
            final int imagewidth, final int imageheight, final int bitsPerPixel) throws IOException {

        final int bytePerPixel = (bitsPerPixel < 8) ? 1 : bitsPerPixel / 8;
        fullimage = new byte[imagewidth*imageheight*bytePerPixel];

        int subwidth;
        int subheight;

        //pass 1
        subwidth = imagewidth / 8;
        subheight = imageheight / 8;
        final byte[] image1 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image1, subwidth, subheight, bytePerPixel, imagewidth, 8, 0, 8, 0);

        //pass 2
        subwidth =  (int)Math.ceil((imagewidth-4) / 8.0);
        subheight = imageheight / 8;
        final byte[] image2 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image2, subwidth, subheight, bytePerPixel, imagewidth, 8, 4, 8, 0);

        //pass 3
        subwidth = imagewidth / 4;
        subheight = (int)Math.ceil((imageheight-4) / 8.0);
        final byte[] image3 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image3, subwidth, subheight, bytePerPixel, imagewidth, 4, 0, 8, 4);

        //pass 4
        subwidth = (int)Math.ceil((imagewidth-2) / 4.0);
        subheight = imageheight / 4;
        final byte[] image4 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image4, subwidth, subheight, bytePerPixel, imagewidth, 4, 2, 4, 0);

        //pass 5
        subwidth = imagewidth / 2;
        subheight = (int)Math.ceil((imageheight-2) / 4.0);
        final byte[] image5 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image5, subwidth, subheight, bytePerPixel, imagewidth, 2, 0, 4, 2);

        //pass 6
        subwidth = (int)Math.ceil((imagewidth-1) / 2.0);
        subheight = imageheight / 2;
        final byte[] image6 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image6, subwidth, subheight, bytePerPixel, imagewidth, 2, 1, 2, 0);

        //pass 7
        subwidth = imagewidth;
        subheight = (int)Math.ceil((imageheight-1) / 2.0);
        final byte[] image7 = extractPass(stream, subwidth, subheight, bitsPerPixel);
        fillImage(image7, subwidth, subheight, bytePerPixel, imagewidth, 1, 0, 2, 1);

    }

    private byte[] extractPass(ByteInputStream stream, int width, int height, int bitsPerPixel) throws IOException{
        final UnScanInputStream is = new UnScanInputStream(stream, width, height, bitsPerPixel);
        final ByteSequence datas = new ByteSequence();
        int b;
        while( (b = is.read()) != -1){
            datas.put((byte)b);
        }
        return datas.getBackArray();
    }

    private void fillImage(byte[] subimage, int subwidth, int subheight, int bytePerPixel, int imagewidth,
            int spacex, int offsetx, int spacey, int offsety){
        int offset = 0;
        for(int y=0;y<subheight;y++){
            for(int x=0;x<subwidth;x++){
                Arrays.copy(subimage, offset, bytePerPixel, fullimage,
                  toImageCoord(x, spacex, offsetx, y, spacey, offsety, imagewidth, bytePerPixel));
                offset +=bytePerPixel;
            }
        }
    }

    private static int toImageCoord(int x, int spacex, int offsetx, int y, int spacey, int offsety,
            int imgwidth, int bytePerPixel){
        x = x*spacex + offsetx;
        y = y*spacey + offsety;
        return y*imgwidth*bytePerPixel + x*bytePerPixel;
    }

    public int read() throws IOException {
        if(index>=fullimage.length){
            return -1;
        }
        final int b = fullimage[index] & 0xff; //unsign it
        index++;
        return b;
    }

    public void close() throws IOException {
    }

}
