
package science.unlicense.impl.image.png.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.impl.image.png.PNGMetadata;
import static science.unlicense.impl.image.png.PNGMetadata.MD_PLTE_TABLE;

/**
 *
 * @author Johann Sorel
 */
public class PLTE extends Chunk {

    public Color[] palette = null;

    public PLTE() {
        super(PNGMetadata.MD_PLTE);
    }

    public void read(DataInputStream ds, int length) throws IOException {
        //rebuild palette
        palette = new Color[length/3];
        for(int i=0;i<palette.length;i++){
            palette[i] = new Color(
                    ds.readUByte(),
                    ds.readUByte(),
                    ds.readUByte());
        }
        addChild(MD_PLTE_TABLE, palette);
    }

    /**
     * Adjust pallete colors with given alphas.
     * @param alphas
     */
    public void fix(tRNS alphas, int colorType) throws IOException{
        if (alphas == null) return;

        if(colorType == 0){
            throw new IOException("Not supported yet.");
        }else if(colorType == 2){
            throw new IOException("Not supported yet.");
        }else if(colorType == 3){
            for(int i=0;i<alphas.datas.length;i++){
                final int alpha = alphas.datas[i] &0xFF;
                final float[] rgba = palette[i].toRGBA();
                rgba[3] = (float)alpha/255f;
                palette[i] = new Color(rgba);
            }
        }else{
            throw new IOException("Not supported yet.");
        }

    }

}
