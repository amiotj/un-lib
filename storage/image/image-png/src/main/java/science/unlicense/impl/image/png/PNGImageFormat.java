
package science.unlicense.impl.image.png;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;
import static science.unlicense.impl.image.png.PNGConstants.SIGNATURE;

/**
 *
 * @author Johann Sorel
 */
public class PNGImageFormat extends AbstractImageFormat{

    public PNGImageFormat() {
        super(new Chars("png"),
              new Chars("PNG"),
              new Chars("Portable Network Graphics"),
              new Chars[]{
                  new Chars("image/png")
              },
              new Chars[]{
                new Chars("png")
              },
              new byte[][]{SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return true;
    }

    public ImageReader createReader() {
        return new PNGImageReader();
    }

    public ImageWriter createWriter() {
        return new PNGImageWriter();
    }

}
