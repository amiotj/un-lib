
package science.unlicense.impl.image.apng;

import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.impl.image.png.PNGImageReader;
import science.unlicense.impl.image.png.model.IDAT;
import science.unlicense.impl.image.png.model.IHDR;
import science.unlicense.impl.image.process.paint.BlendOperator;
import science.unlicense.api.media.DefaultImagePacket;
import science.unlicense.api.media.ImagePacket;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class APNGVideoReader implements MediaReadStream {

    private final PNGImageReader reader;
    private final Sequence frames;
    private final IHDR ihdr;
    private final int frameRate;

    //iteration state
    private Image workspace;
    private Image image;
    private int currentImageIndex = -1;

    public APNGVideoReader(PNGImageReader reader, Sequence frames, IHDR ihdr) throws IOException {
        this.reader = reader;
        this.frames = frames;
        this.ihdr = ihdr;
        this.frameRate = 10;
        this.workspace = Images.create(new Extent.Long(ihdr.getWidth(), ihdr.getHeight()), Images.IMAGE_TYPE_RGBA);
    }

    public ImagePacket next() throws IOException {
        if(currentImageIndex < frames.getSize()-1){
            currentImageIndex++;

            final APNGFrame frame = (APNGFrame) frames.get(currentImageIndex);
            final int disposeOp = frame.fcTL.getDisposeOp();
            final int blendOp = frame.fcTL.getBlendOp();
            final int xoffset = frame.fcTL.getXOffset();
            final int yoffset = frame.fcTL.getYOffset();
            final int width = frame.fcTL.getWidth();
            final int height = frame.fcTL.getHeight();

            //build frame image
            final IDAT[] idatas = new IDAT[frame.datas.getSize()];
            Collections.copy(frame.datas, idatas, 0);
            final BacktrackInputStream bs = reader.getInputBSStream();
            final Image frameImage = reader.readSingleImage(bs, idatas,
                    frame.fcTL.getWidth(), frame.fcTL.getHeight(), ihdr);

            //blend image
            final Affine2 trs = new Affine2();
            trs.set(0, 2, xoffset);
            trs.set(1, 2, yoffset);

            //start by making a copy of the workspace
            image = Images.copy(workspace);
            if(APNGMetaModel.APNG_BLEND_OP_OVER == blendOp){
                new BlendOperator().execute(image, frameImage, AlphaBlending.create(AlphaBlending.SRC_OVER, 1f), trs);
            }else if(APNGMetaModel.APNG_BLEND_OP_SOURCE == blendOp){
                new BlendOperator().execute(image, frameImage, AlphaBlending.create(AlphaBlending.SRC, 1f), trs);
            }else{
                throw new IOException("Unknowned blend operation : "+blendOp);
            }

            //clean up for next image
            if(APNGMetaModel.APNG_DISPOSE_OP_NONE == disposeOp){
                //don't change anything
                workspace = image;
            }else if(APNGMetaModel.APNG_DISPOSE_OP_BACKGROUND == disposeOp){
                //erase frame area in the worksapce
                final TupleBuffer sm = workspace.getRawModel().asTupleBuffer(workspace);
                final Object sample = workspace.getColorModel().toSample(Color.TRS_WHITE);
                final BBox bbox = new BBox(2);
                bbox.setRange(0, xoffset, xoffset+width);
                bbox.setRange(1, yoffset, yoffset+height);
                sm.setTuple(bbox, sample);

            }else if(APNGMetaModel.APNG_DISPOSE_OP_PREVIOUS == disposeOp){
                //use previous state, keep same worksapce
            }else{
                throw new IOException("Unknowned dispose operation : "+disposeOp);
            }
            
            final long time = currentImageIndex * frameRate;
            
            return new DefaultImagePacket(null, time, frameRate, image);
        }
        return null;
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
