
package science.unlicense.impl.image.apng;

import science.unlicense.impl.image.apng.APNGMediaFormat;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.media.MediaFormat;
import science.unlicense.api.media.Medias;

/**
 *
 * @author Johann Sorel
 */
public class APNGMediaFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final MediaFormat[] formats = Medias.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof APNGMediaFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("Media format not found.");
    }

}
