package science.unlicense.impl.image.png;

import science.unlicense.impl.image.png.PNGImageReader;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageSetMetadata;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.IndexedColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class PNGReaderTest {

    @Test
    public void testTrueColorUncompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/TrueColorUncompressed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,0,0}, b00); //RED
        Assert.assertArrayEquals(new int[]{255,255,0}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0,255,255}, b01); //CYAN
        Assert.assertArrayEquals(new int[]{0,0,255}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

    @Test
    public void testTrueColorAlphaUncompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/TrueColorAlphaUncompressed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,0,0,255}, b00); //RED
        Assert.assertArrayEquals(new int[]{255,255,0,206}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0,255,255,0}, b01); //TRANSLUCENT
        Assert.assertArrayEquals(new int[]{0,0,255,255}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 206), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 0), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

    @Test
    public void testTrueColorAlphaCompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/TrueColorAlphaCompressed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,0,0,255}, b00); //RED
        Assert.assertArrayEquals(new int[]{255,255,0,206}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0,255,255,0}, b01); //TRANSLUCENT
        Assert.assertArrayEquals(new int[]{0,0,255,255}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 206), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 0), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

    @Test
    public void testTrueColorCompressedLarge() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/TrueColorCompressedLarge.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in RGB

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //WHITE
        sm.getTuple(new int[]{2,0},storage);
        Assert.assertArrayEquals(new int[]{255,255,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 255, 255), cm.toColor(storage));
        //YELLOW
        sm.getTuple(new int[]{3,0},storage);
        Assert.assertArrayEquals(new int[]{255,255,0}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));
        //CYAN
        sm.getTuple(new int[]{2,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,255}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(storage));
        //GRAY
        sm.getTuple(new int[]{3,1},storage);
        Assert.assertArrayEquals(new int[]{100,100,100}, (int[])storage);
        Assert.assertEquals(new Color(100, 100, 100, 255), cm.toColor(storage));

    }

    /**
     * There was a bug when encountering a 128 byte value in the unscanline.
     * Because of byte values not properly unsigned.
     * this test ensure it won't happen anymore.
     */
    @Test
    public void testTrueColorUncompressed128() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/LongScanLine.png")).createInputStream();
        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);
        final Image image = reader.read(reader.createParameters());

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object samples = sm.createTupleStore();
        sm.getTuple(new int[]{365,1}, samples);
        final Color c = cm.toColor(samples);

    }

    //still bugs, unscan line issue
    @Ignore
    @Test
    public void testIndexed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/Indexed.png")).createInputStream();

        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        Assert.assertTrue(cm instanceof IndexedColorModel);

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are indexed
        Assert.assertArrayEquals(new int[]{2}, b00); //RED
        Assert.assertArrayEquals(new int[]{3}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0}, b01); //GRAY-LIKE
        Assert.assertArrayEquals(new int[]{1}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(b10));
        Assert.assertEquals(new Color(101, 45, 103, 255), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }
}
