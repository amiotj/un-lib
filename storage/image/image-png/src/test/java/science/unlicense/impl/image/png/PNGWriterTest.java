package science.unlicense.impl.image.png;

import science.unlicense.impl.image.png.PNGImageWriter;
import science.unlicense.impl.image.png.PNGImageReader;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class PNGWriterTest {

    @Ignore
    @Test
    public void debug() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/LongScanLine.png")).createInputStream();
        final ImageReader reader = new PNGImageReader();
        reader.setInput(bs);
        final Image image = reader.read(reader.createParameters());

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object samples = sm.createTupleStore();
        sm.getTuple(new int[]{365,1}, samples);
        final Color c = cm.toColor(samples);

        final Chars f = new Chars("/home/eclesia/image.png");
        final ImageWriter writer = new PNGImageWriter();
        writer.setOutput(Paths.resolve(f).createOutputStream());
        writer.write(image, null);

        final ImageReader reader2 = new PNGImageReader();
        reader2.setInput(Paths.resolve(f).createInputStream());
        final Image img2 = reader2.read(reader2.createParameters());


    }

}
