
package science.unlicense.impl.image.png;

import science.unlicense.impl.image.png.PNGImageFormat;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.image.ImageFormat;
import science.unlicense.api.image.Images;

/**
 *
 * @author Johann Sorel
 */
public class PNGImageFormatTest {
    
    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof PNGImageFormat){
                //ok, found it
                return;
            }
        }
        
        Assert.fail("Image format not found.");
    }
    
}
