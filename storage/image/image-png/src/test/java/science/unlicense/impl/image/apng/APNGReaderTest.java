package science.unlicense.impl.image.apng;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.Medias;
import science.unlicense.api.media.ImageStreamMeta;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.path.Path;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageSetMetadata;
import science.unlicense.api.media.ImagePacket;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class APNGReaderTest {

    @Test
    public void mediaReaderTest() throws Exception{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/imagery/apng/clock.png"));

        //check store creation
        final MediaStore store = Medias.open(path);
        Assert.assertNotNull(store);

        //check metadatas
        final MediaStreamMeta[] metas = store.getStreamsMeta();
        Assert.assertNotNull(metas);
        Assert.assertEquals(1, metas.length);

        final ImageStreamMeta meta = (ImageStreamMeta) metas[0];
        Assert.assertEquals(40, meta.getNbFrames());

        final TypedNode n =  meta.getImageMetadata();
        final TypedNode[] dimensions = n.getChildren(ImageSetMetadata.MD_IMAGE_DIMENSION.getId());
        Assert.assertEquals(2,dimensions.length);
        Assert.assertEquals(new Integer(150), dimensions[0].getChild(ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND.getId()).getValue());
        Assert.assertEquals(new Integer(150), dimensions[1].getChild(ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND.getId()).getValue());

        //check reading
        final MediaReadStream reader = store.createReader(null);
        Assert.assertNotNull(reader);

        //check images are generated
        for(int i=0;i<40;i++){
            final MediaPacket pack = reader.next();
            Assert.assertNotNull(pack);
            Assert.assertTrue(pack instanceof ImagePacket);
            final Image image = ((ImagePacket)pack).getImage();

        }

        //ensure nothing left
        Assert.assertNull(reader.next());

    }

}
