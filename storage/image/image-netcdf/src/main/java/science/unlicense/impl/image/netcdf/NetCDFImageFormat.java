
package science.unlicense.impl.image.netcdf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class NetCDFImageFormat extends AbstractImageFormat{

    public NetCDFImageFormat() {
        super(new Chars("netcdf"),
              new Chars("NetCDF"),
              new Chars("Network Common Data Form"),
              new Chars[]{
                  new Chars("application/netcdf"),
                  new Chars("application/x-netcdf")
              },
              new Chars[]{
                  new Chars("nc"),
                  new Chars("cdf")
              },
              new byte[][]{NetCDFMetaModel.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new NetCDFImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
