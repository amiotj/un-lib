
package science.unlicense.impl.image.netcdf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultNodeCardinality;
import science.unlicense.api.model.tree.DefaultNodeType;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.NodeType;

/**
 *
 * @author Johann Sorel
 */
public final class NetCDFMetaModel {

    public static final byte[] SIGNATURE = new byte[]{'C','D','F'};
    
    /** 8-bit signed integers */
    public static final int NC_BYTE = 1;
    /** text characters */
    public static final int NC_CHAR = 2;
    /** 16-bit signed integers */
    public static final int NC_SHORT = 3;
    /** 32-bit signed integers */
    public static final int NC_INT = 4;
    /** IEEE single precision floats */
    public static final int NC_FLOAT = 5;
    /** IEEE double precision floats */
    public static final int NC_DOUBLE = 6;

    public static final int NC_DIMENSION = 10;
    public static final int NC_VARIABLE = 11;
    public static final int NC_ATTRIBUTE = 12;

    private NetCDFMetaModel(){}

    public static final NodeType MD_NETCDF;
    public static final NodeCardinality MD_NETCDF_VERSION = new DefaultNodeCardinality(new Chars("version"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_NETCDF_NBREC = new DefaultNodeCardinality(new Chars("nbrec"),null,null,Integer.class, false, null);

    public static final NodeCardinality MD_DIMENSION;
    public static final NodeCardinality MD_DIMENSION_NAME = new DefaultNodeCardinality(new Chars("name"),null, null,String.class, false, null);
    public static final NodeCardinality MD_DIMENSION_SIZE = new DefaultNodeCardinality(new Chars("size"),null, null,Integer.class, false, null);

    public static final NodeCardinality MD_ATTRIBUTE;
    public static final NodeCardinality MD_ATTRIBUTE_NAME = new DefaultNodeCardinality(new Chars("name"),null, null,String.class, false, null);
    public static final NodeCardinality MD_ATTRIBUTE_TYPE = new DefaultNodeCardinality(new Chars("type"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_ATTRIBUTE_VALUE = new DefaultNodeCardinality(new Chars("value"),null, null,Object.class, false, null);

    public static final NodeCardinality MD_VARIABLE;
    public static final NodeCardinality MD_VARIABLE_NAME = new DefaultNodeCardinality(new Chars("name"),null, null,String.class, false, null);
    public static final NodeCardinality MD_VARIABLE_TYPE = new DefaultNodeCardinality(new Chars("type"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_VARIABLE_SIZE = new DefaultNodeCardinality(new Chars("size"),null, null,Long.class, false, null);
    public static final NodeCardinality MD_VARIABLE_BEGIN = new DefaultNodeCardinality(new Chars("begin"),null, null,Long.class, false, null);


    static {

        MD_DIMENSION = new DefaultNodeCardinality(new Chars("dimension"),null, null,Sequence.class, null, false, 0,Integer.MAX_VALUE,new NodeCardinality[]{
            MD_DIMENSION_NAME,
            MD_DIMENSION_SIZE
        });

        MD_ATTRIBUTE = new DefaultNodeCardinality(new Chars("attribute"),null, null,Sequence.class, null, false, 0,Integer.MAX_VALUE,new NodeCardinality[]{
            MD_ATTRIBUTE_NAME,
            MD_ATTRIBUTE_TYPE,
            MD_ATTRIBUTE_VALUE
        });

        MD_VARIABLE = new DefaultNodeCardinality(new Chars("variable"),null, null,Sequence.class, null, false, 0,Integer.MAX_VALUE,new NodeCardinality[]{
            MD_VARIABLE_NAME,
            MD_VARIABLE_TYPE,
            MD_VARIABLE_SIZE,
            MD_VARIABLE_BEGIN,
            MD_DIMENSION,
            MD_ATTRIBUTE
        });

        MD_NETCDF = new DefaultNodeType(new Chars("netcdf"),null, null,Sequence.class, false, new NodeCardinality[]{
            MD_NETCDF_VERSION,
            MD_DIMENSION,
            MD_ATTRIBUTE,
            MD_VARIABLE
        });

    }


}
