package science.unlicense.impl.image.netcdf;

import science.unlicense.impl.image.netcdf.NetCDFImageReader;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import org.junit.Test;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.ImageReader;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class NetCDFReaderTest {

    private static final float DELTA = 0.000000001f;
    @Test
    public void testSample() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/netcdf/testrh.nc")).createInputStream();

        final ImageReader reader = new NetCDFImageReader();
        reader.setInput(bs);

        //check metadatas
        final Chars[] names = reader.getMetadataNames();
        Assert.assertEquals(2, names.length);
        Assert.assertEquals(new Chars("netcdf"),names[0]);
        Assert.assertEquals(new Chars("imageset"),names[1]);

        final ImageReadParameters params = reader.createParameters();
        final Image image = reader.read(params);
        Assert.assertEquals(1, image.getExtent().getDimension());
        Assert.assertEquals(10000, image.getExtent().getL(0));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);

        final float[] b0 = sm.getTupleFloat(new int[]{0},null);
        final float[] b1 = sm.getTupleFloat(new int[]{1},null);
        final float[] b9999 = sm.getTupleFloat(new int[]{9999},null);

        //values are floats
        Assert.assertArrayEquals(new float[]{420f}, b0, DELTA);
        Assert.assertArrayEquals(new float[]{197}, b1, DELTA);
        Assert.assertArrayEquals(new float[]{444}, b9999, DELTA);

    }

}
