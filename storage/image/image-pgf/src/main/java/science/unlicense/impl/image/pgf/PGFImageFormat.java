
package science.unlicense.impl.image.pgf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * http://en.wikipedia.org/wiki/Progressive_Graphics_File
 * 
 * @author Johann Sorel
 */
public class PGFImageFormat extends AbstractImageFormat{

    public PGFImageFormat() {
        super(new Chars("pgf"),
              new Chars("PGF"),
              new Chars("Progressive Graphics File"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("pgf")
              },
              new byte[0][0]);
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
