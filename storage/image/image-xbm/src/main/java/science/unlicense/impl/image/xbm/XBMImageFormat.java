
package science.unlicense.impl.image.xbm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * Resources :
 * http://en.wikipedia.org/wiki/X_BitMap
 *
 * @author Johann Sorel
 */
public class XBMImageFormat extends AbstractImageFormat{

    public XBMImageFormat() {
        super(new Chars("xbm"),
              new Chars("XBM"),
              new Chars("X BitMap"),
              new Chars[]{
                  new Chars("image/x-xbitmap"),
                  new Chars("image/x-xbm")
              },
              new Chars[]{
                new Chars("xbm")
              },
              new byte[][]{XBMConstants.SIGNATURE.toBytes()});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return true;
    }

    public ImageReader createReader() {
        return new XBMImageReader();
    }

    public ImageWriter createWriter() {
        return new XBMImageWriter();
    }

}
