

package science.unlicense.impl.image.xbm;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class XBMConstants {

    /**
     * Not a real signature but will do.
     */
    public static final Chars SIGNATURE = new Chars("#define");
    public static final Chars DECLARATION = new Chars("static unsigned char sample_bits[] = {");

    public XBMConstants() {
    }

}
