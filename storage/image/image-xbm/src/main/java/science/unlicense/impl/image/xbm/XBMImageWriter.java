
package science.unlicense.impl.image.xbm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageWriter;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageWriteParameters;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.impl.code.c.CWriter;

/**
 *
 * @author Johann Sorel
 */
public class XBMImageWriter extends AbstractImageWriter {

    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {

        final RawModel sm = image.getRawModel();
        if(sm.getPrimitiveType()!= RawModel.TYPE_1_BIT){
            throw new IOException("Only 1Bit sample image supported.");
        }else if(sm.getSampleCount() != 1){
            throw new IOException("Only one sample supported.");
        }

        final CWriter writer = new CWriter();
        writer.setOutput(getOutput());

        writer.writeDefine(new Chars("width"), Int32.encode((int) image.getExtent().getL(0)));
        writer.writeDefine(new Chars("height"), Int32.encode((int) image.getExtent().getL(1)));
        //TODO
    }

}
