package science.unlicense.impl.image.xbm;

import science.unlicense.impl.image.xbm.XBMImageReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class XBMReaderTest {

    @Test
    public void testReader() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/xbm/sample.xbm")).createInputStream();

        final ImageReader reader = new XBMImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(11,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final boolean[][] expected = new boolean[][]{
            {true, false,true, false,true, true, true, true, false,true,false},
            {false,true, false,true, false,false,false,false,false,true,false},
        };

        for(int y=0;y<2;y++){
            for(int x=0;x<11;x++){
                final boolean[] b00 = sm.getTupleBoolean(new int[]{x,y},null);
                Assert.assertEquals("at "+y+" "+x,expected[y][x], b00[0]);
            }
        }

    }

}
