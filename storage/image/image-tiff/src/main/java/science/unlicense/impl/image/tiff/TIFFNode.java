
package science.unlicense.impl.image.tiff;

import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.NodeType;

/**
 *
 * @author Johann Sorel
 */
public class TIFFNode extends DefaultTypedNode{

    public int tag;
    public int type;
    public long count;

    public TIFFNode(NodeType type) {
        super(type);
    }
    
    public TIFFNode(NodeCardinality type) {
        super(type);
    }

}
