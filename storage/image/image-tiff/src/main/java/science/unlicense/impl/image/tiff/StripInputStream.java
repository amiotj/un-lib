

package science.unlicense.impl.image.tiff;

import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.IOException;

/**
 * Stream concatenating tiff strips.
 *
 * @author Johann Sorel
 */
public class StripInputStream extends AbstractInputStream {

    private final BacktrackInputStream bs;
    private final TypedNode[] stripOffsets;
    private int stripOffset = -1;
    private final int stripSize;
    private int bufferOffset = -1;

    public StripInputStream(BacktrackInputStream bs, TypedNode[] stripOffsets, int stripSize) {
        this.bs = bs;
        this.stripOffsets = stripOffsets;
        this.stripSize = stripSize;
    }

    public int read() throws IOException {
        if(bufferOffset>=stripSize){
            bufferOffset=-1;
        }

        if(bufferOffset==-1){
            stripOffset++;
            if(stripOffset>=stripOffsets.length){
                //no more strips
                return -1;
            }
            bs.rewind();
            bs.skip( ((Number)stripOffsets[stripOffset].getValue()).longValue() );
        }
        bufferOffset++;
        return bs.read();
    }

    public void close() throws IOException {

    }

}
