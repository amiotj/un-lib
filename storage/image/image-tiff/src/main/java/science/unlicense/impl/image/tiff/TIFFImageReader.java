package science.unlicense.impl.image.tiff;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import static science.unlicense.api.image.ImageSetMetadata.*;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.DirectColorModel;
import static science.unlicense.impl.image.tiff.TIFFMetaModel.*;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.image.sample.PlanarRawModel2D;

/**
 * TIFF image format defined here :
 * http://partners.adobe.com/public/developer/tiff/index.html
 *
 * @author Johann Sorel
 */
public class TIFFImageReader extends AbstractImageReader{

    private TypedNode mdTiff = null;
    private TypedNode mdImage = null;

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final DataInputStream ds;

        //Tiff file header ---------------------------------------------------
        final int s1 = stream.read();
        final int s2 = stream.read();
        if(s1=='I' && s2=='I'){
            ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);
        }else if(s1=='M' && s2=='M'){
            ds = new DataInputStream(stream, NumberEncoding.BIG_ENDIAN);
        }else {
            throw new IOException("Stream is not a TIFF image.");
        }

        final int signature = ds.readUShort();
        if(signature != TIFFMetaModel.SIGNATURE) {
            throw new IOException("Stream is not a TIFF image.");
        }

        final int ifdOffset = ds.readInt();
        stream.rewind();
        stream.skip(ifdOffset);

        final TypedNode tiffNode = new DefaultTypedNode(TIFFMetaModel.MD_TIFF);
        final int nbDirectory = ds.readUShort();
        final int offset = stream.position();
        for(int i=0; i<nbDirectory; i++){
            stream.rewind();
            stream.skip(offset+12*i);
            final TIFFNode tf = readDirectory(stream,ds);
            tiffNode.getChildren().add(tf);
        }

        mdTiff = tiffNode;

        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,mdTiff.getChild(MD_IMAGE_WIDTH).getValue())}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,mdTiff.getChild(MD_IMAGE_LENGTH).getValue())})
        });

        TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGE,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    @Override
    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws IOException {

        if(mdTiff == null){
            //read metas
            readMetadatas(stream);
            stream.rewind();
        }

        final int photoInterp = (Integer)mdTiff.getChild(TIFFMetaModel.MD_PHOTOMETRIC_INTERPRETATION).getValue();
        final int width = (Integer)mdTiff.getChild(TIFFMetaModel.MD_IMAGE_WIDTH).getValue();
        final int height = (Integer)mdTiff.getChild(TIFFMetaModel.MD_IMAGE_LENGTH).getValue();
        final int compression = (Integer)mdTiff.getChild(TIFFMetaModel.MD_COMPRESSION).getValue();
        final Object bps = mdTiff.getChild(TIFFMetaModel.MD_BITS_PER_SAMPLE).getValue();
        final int rowsPerStrip = (Integer)mdTiff.getChild(TIFFMetaModel.MD_ROWS_PER_STRIP).getValue();
        final TypedNode[] stripOffsets = mdTiff.getChildren(TIFFMetaModel.MD_STRIP_OFFSETS);
        final int stripByteCount = ((Number)mdTiff.getChild(TIFFMetaModel.MD_STRIP_BYTE_COUNTS).getValue()).intValue();

        final int[] sampleType = toSampletype(bps);

        //create a stream on all strips
        final ByteInputStream stripStream = new StripInputStream(stream, stripOffsets, stripByteCount);
        final DataInputStream ds = new DataInputStream(stripStream);

        final Buffer bank;
        final RawModel sm;
        final ColorModel cm;

        if(compression != TIFFMetaModel.COMPRESSED_UNCOMPRESSED){
            throw new IOException("Only not compressed tif supported.");
        }

        if(TIFFMetaModel.PI_BLACK_IS_ZERO == photoInterp || TIFFMetaModel.PI_WHITE_IS_ZERO == photoInterp){
            //Grayscale or black and white
            final int bitsPerSample = Images.getBitsPerSample(sampleType[0]);

            final long nbBits = width*height*bitsPerSample;
            bank = params.getBufferFactory().createByte((int)(nbBits/8));
            ds.readFully(bank);

            sm = new PlanarRawModel2D(sampleType[0], 1);
            cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        }else if(TIFFMetaModel.PI_RGB == photoInterp){
            //RGB image
            final int nbSamples = (Integer)mdTiff.getChild(TIFFMetaModel.MD_SAMPLES_PER_PIXEL).getValue();
            final int bitsPerSample = Images.getBitsPerSample(sampleType[0]);

            final long nbBits = width*height*bitsPerSample*nbSamples;
            bank = params.getBufferFactory().createByte((int)(nbBits/8));
            ds.readFully(bank);

            sm = new InterleavedRawModel(sampleType[0], nbSamples);
            if(nbSamples==3){
                cm = new DirectColorModel(sm, new int[]{0,1,2,-1}, false);
            }else if(nbSamples==4){
                //todo is there something which indicate if alpha is premultiplied ?
                cm = new DirectColorModel(sm, new int[]{0,1,2,3}, false);
            }else{
                throw new IOException("unexpected number of sample : "+nbSamples);
            }
            
        }else if(TIFFMetaModel.PI_RGB_PALETTE == photoInterp){
            //Palette image
            throw new IOException("photo interpretation not supported yet : "+photoInterp);
        }else{
            //TODO others
            throw new IOException("photo interpretation not supported yet : "+photoInterp);
        }

        return new DefaultImage(bank, new Extent.Long(width, height), sm,cm);
    }

    private static int[] toSampletype(Object candidate) throws IOException{
        if(candidate instanceof Number){
            return new int[]{toSampletype( ((Number)candidate).intValue()) };
        }else if(candidate instanceof Sequence){
            final Sequence seq = (Sequence) candidate;
            final int[] sampleTypes = new int[seq.getSize()];
            for(int i=0;i<sampleTypes.length;i++){
                sampleTypes[i] = toSampletype( ((Number)seq.get(i)).intValue() );
            }
            return sampleTypes;
        }else{
            throw new IOException("Unexpected tag value : "+candidate);
        }
    }

    private static int toSampletype(int bitsPerSample) throws IOException{
        final int sampleType;
        if(bitsPerSample == 1){
            sampleType = RawModel.TYPE_1_BIT;
        }else if(bitsPerSample == 2){
            sampleType = RawModel.TYPE_2_BIT;
        }else if(bitsPerSample == 4){
            sampleType = RawModel.TYPE_4_BIT;
        }else if(bitsPerSample == 8){
            sampleType = RawModel.TYPE_UBYTE;
        }else if(bitsPerSample == 16){
            sampleType = RawModel.TYPE_USHORT;
        }else{
            throw new IOException("Unsupported bits per sample : "+bitsPerSample);
        }
        return sampleType;
    }

    private TIFFNode readDirectory(BacktrackInputStream backtrack, DataInputStream ds) throws IOException{

        final int tag = ds.readUShort();
        NodeCardinality type = TIFFMetaModel.findType(tag);
        if(type == null){
            type = TIFFMetaModel.MD_UNKNOWNED;
        }

        final TIFFNode node = new TIFFNode(type);
        node.tag = tag;
        node.type = ds.readUShort();
        node.count = ds.readUInt();

        if(node.count == 1 && (
                TIFFMetaModel.TYPE_BYTE == node.type
             || TIFFMetaModel.TYPE_FLOAT == node.type
             || TIFFMetaModel.TYPE_LONG == node.type
             || TIFFMetaModel.TYPE_SBYTE == node.type
             || TIFFMetaModel.TYPE_SHORT == node.type
             || TIFFMetaModel.TYPE_SLONG == node.type
             || TIFFMetaModel.TYPE_SSHORT == node.type
          )){
            //value is stored in the next 4 bytes.
            if(TIFFMetaModel.TYPE_BYTE == node.type){
                node.setValue(ds.readUByte());
            }else if(TIFFMetaModel.TYPE_FLOAT == node.type){
                node.setValue(ds.readFloat());
            }else if(TIFFMetaModel.TYPE_LONG == node.type){
                node.setValue(ds.readUInt());
            }else if(TIFFMetaModel.TYPE_SBYTE == node.type){
                node.setValue(ds.readByte());
            }else if(TIFFMetaModel.TYPE_SHORT == node.type){
                node.setValue(ds.readUShort());
            }else if(TIFFMetaModel.TYPE_SLONG == node.type){
                node.setValue(ds.readInt());
            }else if(TIFFMetaModel.TYPE_SSHORT == node.type){
                node.setValue(ds.readShort());
            }
        }else{
            final long offset = ds.readUInt();
            backtrack.rewind();
            backtrack.skip(offset);
            final Sequence seq = new ArraySequence();
            for(int i=0;i<node.count;i++){
                if(TIFFMetaModel.TYPE_ASCII == node.type){
                    final ByteSequence buffer = new ByteSequence();
                    for(int k=0;k<node.count;k++){
                        int b = ds.read();
                        if(b == 0){
                            seq.add(new String(buffer.toArrayByte()));
                            buffer.removeAll();
                        }else{
                            buffer.put((byte)b);
                        }
                    }
                    break;
                }else if(TIFFMetaModel.TYPE_BYTE == node.type){
                    seq.add(ds.readUByte());
                }else if(TIFFMetaModel.TYPE_DOUBLE == node.type){
                    seq.add(ds.readDouble());
                }else if(TIFFMetaModel.TYPE_FLOAT == node.type){
                    seq.add(ds.readFloat());
                }else if(TIFFMetaModel.TYPE_LONG == node.type){
                    seq.add(ds.readUInt());
                }else if(TIFFMetaModel.TYPE_RATIONAL == node.type){
                    final Rational r = new Rational();
                    r.fraction = ds.readUInt();
                    r.denominator = ds.readUInt();
                    seq.add(r);
                }else if(TIFFMetaModel.TYPE_SBYTE == node.type){
                    seq.add(ds.readByte());
                }else if(TIFFMetaModel.TYPE_SHORT == node.type){
                    seq.add(ds.readUShort());
                }else if(TIFFMetaModel.TYPE_SLONG == node.type){
                    seq.add(ds.readInt());
                }else if(TIFFMetaModel.TYPE_SRATIONAL == node.type){
                    final Rational r = new Rational();
                    r.fraction = ds.readUInt();
                    r.denominator = ds.readInt();
                    seq.add(r);
                }else if(TIFFMetaModel.TYPE_SSHORT == node.type){
                    seq.add(ds.readShort());
                }else if(TIFFMetaModel.TYPE_UNDEFINED == node.type){
                    seq.add(ds.readByte());
                }
            }
            node.setValue(seq);
        }
        return node;
    }

}
