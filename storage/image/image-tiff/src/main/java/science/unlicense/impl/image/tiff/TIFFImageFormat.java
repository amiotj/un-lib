
package science.unlicense.impl.image.tiff;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class TIFFImageFormat extends AbstractImageFormat{

    public TIFFImageFormat() {
        super(new Chars("tiff"),
              new Chars("TIFF"),
              new Chars("Tagged Image File Format"),
              new Chars[]{
                  new Chars("image/tiff")
              },
              new Chars[]{
                  new Chars("tif"),
                  new Chars("tiff")
              },
              new byte[][]{
                  {0x49,0x49,0x2A,0x00},
                  {0x4D,0x4D,0x00,0x2A},
                  {0x4D,0x4D,0x00,0x2B}
              });
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new TIFFImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
