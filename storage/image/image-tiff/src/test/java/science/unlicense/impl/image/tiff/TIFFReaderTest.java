package science.unlicense.impl.image.tiff;

import science.unlicense.impl.image.tiff.TIFFImageReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class TIFFReaderTest {

    @Test
    public void testBWNoCompression() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tiff/bw.tiff")).createInputStream();

        final ImageReader reader = new TIFFImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        Assert.assertArrayEquals(new int[]{0}, b00); //BLACK
        Assert.assertArrayEquals(new int[]{255}, b10); //WHITE
        Assert.assertArrayEquals(new int[]{255}, b01); //WHITE
        Assert.assertArrayEquals(new int[]{0}, b11); //BLACK

    }

    @Test
    public void testGrayscaleNoCompression() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tiff/grayscale.tiff")).createInputStream();

        final ImageReader reader = new TIFFImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        Assert.assertArrayEquals(new int[]{ 54}, b00);
        Assert.assertArrayEquals(new int[]{237}, b10);
        Assert.assertArrayEquals(new int[]{201}, b01);
        Assert.assertArrayEquals(new int[]{ 18}, b11);

        Assert.assertEquals(new Color( 54,  54,  54, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(237, 237, 237, 255), cm.toColor(b10));
        Assert.assertEquals(new Color(201, 201, 201, 255), cm.toColor(b01));
        Assert.assertEquals(new Color( 18,  18,  18, 255), cm.toColor(b11));

    }

    @Test
    public void testRGB() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tiff/rgb.tiff")).createInputStream();

        final ImageReader reader = new TIFFImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in BGR
        Assert.assertArrayEquals(new int[]{255,0,0}, b00); //RED
        Assert.assertArrayEquals(new int[]{255,255,0}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0,255,255}, b01); //CYAN
        Assert.assertArrayEquals(new int[]{0,0,255}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

}
