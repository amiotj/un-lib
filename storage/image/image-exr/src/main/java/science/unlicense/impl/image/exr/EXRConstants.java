
package science.unlicense.impl.image.exr;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class EXRConstants {


    public static final byte[] SIGNATURE = new byte[]{0x76, 0x2f, 0x31, 0x01};

    /**
     * for file types : all
     * type : chlist
     */
    public static final Chars ATT_CHANNELS = new Chars("channels");
    /**
     * for file types : all
     * type : compression
     */
    public static final Chars ATT_COMPRESSION = new Chars("compression");
    /**
     * for file types : all
     * type : box2i
     */
    public static final Chars ATT_DATA_WINDOW = new Chars("dataWindow");
    /**
     * for file types : all
     * type : box2i
     */
    public static final Chars ATT_DISPLAY_WINDOW = new Chars("displayWindow");
    /**
     * for file types : all
     * type : lineOrder
     */
    public static final Chars ATT_LINE_ORDER = new Chars("lineOrder");
    /**
     * for file types : all
     * type : float
     */
    public static final Chars ATT_PIXEL_ASPECT_RATIO = new Chars("pixelAspectRatio");
    /**
     * for file types : all
     * type : v2f
     */
    public static final Chars ATT_SCREEN_WINDOW_CENTER = new Chars("screenWindowCenter");
    /**
     * for file types : all
     * type : float
     */
    public static final Chars ATT_SCREEN_WINDOW_WIDTH = new Chars("screenWindowWidth");
    /**
     * for file types : tile, multi-part,deep
     * type : tiledesc
     */
    public static final Chars ATT_TILES = new Chars("tiles");
    /**
     * for file types : multi-view
     * type : text
     */
    public static final Chars ATT_VIEW = new Chars("view");
    /**
     * for file types :  multi-part,deep
     * type : string
     */
    public static final Chars ATT_NAME = new Chars("name");
    /**
     * for file types :  multi-part,deep
     * type : string
     */
    public static final Chars ATT_TYPE = new Chars("type");
    /**
     * for file types :  multi-part,deep
     * type : chlist
     */
    public static final Chars ATT_VERSION = new Chars("version");
    /**
     * for file types :  multi-part,deep
     * type : int
     */
    public static final Chars ATT_CHUNK_COUNT = new Chars("chunkCount");
    /**
     * for file types :  deep
     * type : int
     */
    public static final Chars ATT_MAX_SAMPLES_PER_PIXEL = new Chars("maxSamplesPerPixel");


    public static final Chars TYPE_SCANLINE_IMAGE = new Chars("scanlineimage");
    public static final Chars TYPE_TILED_IMAGE = new Chars("tiledimage");
    public static final Chars TYPE_DEEP_SCANLINE = new Chars("deepscanline");
    public static final Chars TYPE_DEEP_TILE = new Chars("deeptile");

//    public static final Chars NO_COMPRESSION;
//    public static final Chars RLE_COMPRESSION;
//    public static final Chars ZIPS_COMPRESSION;
//    public static final Chars ZIP_COMPRESSION;
//    public static final Chars PIZ_COMPRESSION;
//    public static final Chars PXR24_COMPRESSION;
//    public static final Chars B44_COMPRESSION;
//    public static final Chars B44A_COMPRESSION;


}
