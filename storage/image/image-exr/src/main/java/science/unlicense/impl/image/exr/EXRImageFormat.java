
package science.unlicense.impl.image.exr;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * OpenEXR, high quality multiband images :
 * http://www.openexr.com/documentation.html
 *
 * @author Johann Sorel
 */
public class EXRImageFormat extends AbstractImageFormat{

    public EXRImageFormat() {
        super(new Chars("exr"),
              new Chars("EXR"),
              new Chars("OpenEXR"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("exr")
              },
              new byte[0][0]);
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
