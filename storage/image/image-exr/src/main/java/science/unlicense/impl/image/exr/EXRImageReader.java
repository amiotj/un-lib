
package science.unlicense.impl.image.exr;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.io.BacktrackInputStream;
import static science.unlicense.impl.image.exr.EXRConstants.*;
import science.unlicense.impl.image.exr.model.Attribute;

/**
 *
 * @author Johann Sorel
 */
public class EXRImageReader extends AbstractImageReader {

    public EXRImageReader() {
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {

        DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        if(!Arrays.equals(ds.readFully(new byte[4]),SIGNATURE)){
            throw new IOException("Stream is not an EXR file.");
        }

        final int version = ds.readBits(8);
        final int flags = ds.readBits(24);
        final boolean singleTile = (flags & 0x200) ==1;
        final boolean longNames = (flags & 0x400) ==1;
        final boolean nonImage = (flags & 0x800) ==1;
        final boolean multipart = (flags & 0x1000) ==1;

        //reade header
        final Dictionary attributes = new HashDictionary();
        for(;;){
            final Attribute att = new Attribute();
            att.read(ds, longNames);
            if(att.name.isEmpty()) break;
            attributes.add(att.name, att);
        }

        //read table
        final int nbEntry;
        if(!multipart && attributes.getValue(ATT_CHUNK_COUNT)==null){
            //calculated number of entries
            throw new IOException("TODO");
        }else{
            nbEntry = (Integer)((Attribute)attributes.getValue(ATT_CHUNK_COUNT)).value;
        }
        final long[] offsets = ds.readLong(nbEntry);

        throw new UnimplementedException("Not supported yet.");
    }

}
