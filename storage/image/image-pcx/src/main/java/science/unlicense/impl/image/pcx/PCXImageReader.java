package science.unlicense.impl.image.pcx;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.ImageSetMetadata.*;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.ColorIndex;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.IndexedColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;

/**
 *
 * @author Johann Sorel
 */
public class PCXImageReader extends AbstractImageReader{

    private PCXHeader header;
    private int width;
    private int height;
    private DefaultTypedNode mdImage;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws science.unlicense.api.io.IOException {
        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        final int signature = ds.readByte();
        if(signature != PCXImageFormat.SIGNATURE[0]){
            throw new IOException("Source is not a PCX image.");
        }

        //read header
        header = new PCXHeader();
        header.version = ds.readByte();
        header.encoding = ds.readByte();
        header.bitsPerPixel = ds.readByte();
        header.minX = ds.readUShort();
        header.minY = ds.readUShort();
        header.maxX = ds.readUShort();
        header.maxY = ds.readUShort();
        header.resX = ds.readUShort();
        header.resY = ds.readUShort();
        header.palette = ds.readFully(new byte[48]);
        header.reserved = ds.readByte();
        header.nbColorPlanes = ds.readByte();
        header.bitsPerLine = ds.readUShort();
        header.paletteType = ds.readUShort();
        header.fill = ds.readFully(new byte[58]);

        width = header.maxX - header.minX +1;
        height = header.maxY - header.minY +1;

        mdImage =
        new DefaultTypedNode(MD_IMAGE.getType(),new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws science.unlicense.api.io.IOException {
        readMetadatas(stream);

        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        final Buffer bank;
        final RawModel sm;
        final ColorModel cm;

        if(header.bitsPerPixel == 8){
            if(header.nbColorPlanes == 1){
                final byte[] data = new byte[width*height];
                uncompress(ds, data);
                bank = DefaultBufferFactory.wrap(data);
                sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 1);
                //go read the palette at the end of the file
                //we don't know the file size, so just continue reading until
                //we find the palette flag
                while(true){
                    final byte b = ds.readByte();
                    if(b==PCXImageFormat.PALETTE_FLAG){
                        break;
                    }
                }
                final byte[] paletteData = ds.readFully(new byte[256]);
                cm = readPalette(data, RawModel.TYPE_UBYTE);
            }else if(header.nbColorPlanes == 3){
                final byte[] data = new byte[3*width*height];
                uncompress(ds, data);
                //we must reorder datas since each the pcx order is RRR,VVV,BBB for each line
                final byte[] ordered = new byte[data.length];
                final int lineSize = width*3;
                for(int y=0;y<height;y++){
                    final int lineOffset = y*lineSize;
                    for(int x=0;x<width;x++){
                        final int colOffset = x*3;
                        ordered[lineOffset +colOffset +0] = data[lineOffset + 0*width +x];
                        ordered[lineOffset +colOffset +1] = data[lineOffset + 1*width +x];
                        ordered[lineOffset +colOffset +2] = data[lineOffset + 2*width +x];
                    }
                }

                bank = DefaultBufferFactory.wrap(ordered);
                sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
                cm = new DirectColorModel(sm, new int[]{0,1,2,-1}, false);
            }else{
                throw new IOException("Number of color planes not supported yet "+header.nbColorPlanes);
            }
        }else{
            throw new IOException("Number of bits per pixel not supported yet "+header.bitsPerPixel);
        }

        return new DefaultImage(bank, new Extent.Long(width,height), sm, cm);
    }

    /**
     * Decode PCX RLE.
     * @param ds
     * @param data exprected result size
     */
    private static void uncompress(DataInputStream ds, byte[] data) throws IOException{
        int offset = 0;
        while(offset<data.length){
            final int b = ds.readUByte();
            if(b>=192){ //0xCO PCX last 2 bits are use to indicate palette or rle
                final int repetition = b & 0x3F; //last bits indicate the repetition of next byte
                final byte val = ds.readByte();
                Arrays.fill(data, offset,repetition, val);
                offset+=repetition;
            }else{
                //use value directly
                data[offset++] = (byte)b;
            }
        }
    }

    /**
     * Convert 768 bytes palette in an indexed color model.
     * @param data palette data
     * @return IndexedImageColorModel
     */
    private static IndexedColorModel readPalette(byte[] data, int sampleType){
        final Color[] palette = new Color[256];
        int k=0;
        for(int i=0;i<256;i++){
            palette[i] = new Color(data[k++]&0xFF,data[k++]&0xFF,data[k++]&0xFF);
        }
        return new IndexedColorModel(sampleType, new ColorIndex(palette));
    }

}
