
package science.unlicense.impl.image.pcx;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * PCX image format.
 *
 * resources :
 * http://en.wikipedia.org/wiki/PCX
 * http://www.commentcamarche.net/contents/1203-le-format-pcx
 *
 * @author Johann Sorel
 */
public class PCXImageFormat extends AbstractImageFormat{

    public PCXImageFormat() {
        super(new Chars("pcx"),
              new Chars("PCX"),
              new Chars("Personal Computer Exchange"),
              new Chars[]{
                  new Chars("image/x-pcx")
              },
              new Chars[]{
                new Chars("pcx")
              },
              new byte[][]{SIGNATURE});
    }

    /**
     * File signature.
     */
    public static final byte[] SIGNATURE = new byte[]{0x0A};
    /**
     * Flag indication a 256 color palette.
     * Placed at the end of the file at : Size - 769.
     */
    public static final byte PALETTE_FLAG = 0x0C;

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new PCXImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
