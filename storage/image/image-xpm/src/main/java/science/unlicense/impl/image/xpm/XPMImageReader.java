

package science.unlicense.impl.image.xpm;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.number.Int32;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;

/**
 *
 * @author Johann Sorel
 */
public class XPMImageReader extends AbstractImageReader {

    private TypedNode mdImage = null;
    private int width;
    private int height;
    private int nbcolor;
    private int charPerSample;
    private int hotspotx;
    private int hotspoty;

    private byte[][] paletteValues;
    private int[] paletteColors;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final CharInputStream cs = new CharInputStream(stream, CharEncodings.US_ASCII, new Char('\n'));

        final Chars signature = cs.readLine();
        final Chars cdefinition = cs.readLine();
        final Chars header = trimCStyle(cs.readLine());
        final Chars[] parts = header.split(' ');
        width           = Int32.decode(parts[0]);
        height          = Int32.decode(parts[1]);
        nbcolor         = Int32.decode(parts[2]);
        charPerSample   = Int32.decode(parts[3]);

        //check if there is a cursor hotspot coordinate
        if(parts.length>4){
            hotspotx = Int32.decode(parts[4]);
            hotspoty = Int32.decode(parts[5]);
        }

        //read the palette
        paletteValues = new byte[nbcolor][charPerSample];
        paletteColors = new int[nbcolor];
        for(int i=0;i<nbcolor;i++){
            final Chars line = trimCStyle(cs.readLine());
            paletteValues[i] = line.truncate(0, charPerSample).toBytes();
            final Char type = line.getCharacter(charPerSample+1);
            final Chars colorValue = line.truncate(charPerSample+3,line.getCharLength());
            if(XPMConstants.TYPE_COlOR.equals(type)){
                if(XPMConstants.COLOR_NONE.equals(colorValue, true, true)){
                    paletteColors[i] = Color.TRS_WHITE.toARGB();
                }else{
                    paletteColors[i] = Colors.fromHexa(colorValue).toARGB();
                }
            }else if(XPMConstants.TYPE_MONOCHROME.equals(type)){
                throw new IOException("monochrome colors not supported yet.");
            }else if(XPMConstants.TYPE_GRAYSCALE.equals(type)){
                throw new IOException("grayscale colors not supported yet.");
            }else if(XPMConstants.TYPE_SYMBOLIC.equals(type)){
                throw new IOException("Symbolic colors not supported yet.");
            }else{
                throw new IOException("Unkowned color type : "+type);
            }
        }

        //rebuild metas
        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);
        stream.rewind();

        final CharInputStream cs = new CharInputStream(stream, CharEncodings.US_ASCII,new Char('\n'));
        //skip header and palette
        for(int i=0,n=3+nbcolor;i<n;i++){
            cs.readLine();
        }

        final Image image = Images.create(new Extent.Long(width, height),Images.IMAGE_TYPE_RGBA,parameters.getBufferFactory());
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        final int[] coord = new int[2];
        for(;coord[1]<height;coord[1]++){
            coord[0]=0;
            final Chars line = trimCStyle(cs.readLine());
            final byte[] bytes = line.toBytes();
            for(int offset=0;coord[0]<width;coord[0]++,offset+=charPerSample){
                final int color = paletteColors[findColor(bytes, offset)];
                cm.setARGB(coord, color);
            }
        }

        return image;
    }

    private int findColor(byte[] line, int offset) throws IOException{
        for(int i=0;i<nbcolor;i++){
            if(Arrays.equals(paletteValues[i],0,charPerSample,line,offset)){
                return i;
            }
        }
        throw new IOException("No color found for value : "+new Chars(Arrays.copy(line, offset, charPerSample)));
    }

    private static Chars trimCStyle(Chars candidate){
        final int start = candidate.getFirstOccurence('"');
        final int end = candidate.getLastOccurence('"');
        return candidate.truncate(start+1, end);
    }

}
