
package science.unlicense.impl.image.xpm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * Resources :
 * http://fr.wikipedia.org/wiki/X_PixMap
 * http://en.wikipedia.org/wiki/X_PixMap
 *
 * @author Johann Sorel
 */
public class XPMImageFormat extends AbstractImageFormat{

    public XPMImageFormat() {
        super(new Chars("xpm"),
              new Chars("XPM"),
              new Chars("X PixMap"),
              new Chars[]{
                  new Chars("image/x-xpixmap"),
                  new Chars("image/x-xpm")
              },
              new Chars[]{
                  new Chars("xpm")
              },
              new byte[][]{XPMConstants.SIGNATURE.toBytes()});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new XPMImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
