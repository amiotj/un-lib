

package science.unlicense.impl.image.xpm;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class XPMConstants {

    public static final Chars SIGNATURE = new Chars("/* XPM */");
    public static final Char TYPE_COlOR = new Char('c');
    public static final Char TYPE_MONOCHROME = new Char('m');
    public static final Char TYPE_GRAYSCALE = new Char('g');
    public static final Char TYPE_SYMBOLIC = new Char('s');
    public static final Chars COLOR_NONE = new Chars("None");

    public XPMConstants() {
    }

}
