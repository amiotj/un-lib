
package science.unlicense.impl.image.ktx;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * Specification :
 * https://www.khronos.org/opengles/sdk/tools/KTX/file_format_spec/
 * 
 * @author Johann Sorel
 */
public class KTXImageFormat extends AbstractImageFormat{

    public KTXImageFormat() {
        super(new Chars("ktx"),
              new Chars("KTX"),
              new Chars("KTX"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ktx")
              },
              new byte[][]{KTXConstants.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new KTXImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
