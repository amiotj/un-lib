
package science.unlicense.impl.image.ktx;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 *
 * @author Johann Sorel
 */
public class KTXImageReader extends AbstractImageReader{

    private final KTXHeader header = new KTXHeader();
    private final Dictionary metas = new HashDictionary();
    
    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        return new HashDictionary();
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.BIG_ENDIAN);
        metas.removeAll();
        
        //read header
        header.read(ds);
        
        //read metadatas
        //for each keyValuePair that fits in bytesOfKeyValueData
        //    UInt32   keyAndValueByteSize
        //    Byte     keyAndValue[keyAndValueByteSize]
        //    Byte     valuePadding[3 - ((keyAndValueByteSize + 3) % 4)]
        //end
        int metaSize = header.bytesOfKeyValueData;
        while(metaSize>3){
            final int keyAndValueByteSize = ds.readInt();
            final Chars key = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
            final byte[] value = ds.readFully(new byte[keyAndValueByteSize-(key.getByteLength()+1)]);
            metas.add(key, value);
        }
        //4byte alignment
        ds.realign(4);
        
        //read images, mipmaps, faces ...
        //for each mipmap_level in numberOfMipmapLevels*
        //    UInt32 imageSize; 
        //    for each array_element in numberOfArrayElements*
        //       for each face in numberOfFaces
        //           for each z_slice in pixelDepth*
        //               for each row or row_of_blocks in pixelHeight*
        //                   for each pixel or block_of_pixels in pixelWidth
        //                       Byte data[format-specific-number-of-bytes]**
        //                   end
        //               end
        //           end
        //           Byte cubePadding[0-3]
        //       end
        //    end
        //    Byte mipPadding[3 - ((imageSize + 3) % 4)]
        //end
        final int nbMipmaplLevel = (header.numberOfMipmapLevels==0) ? 1 : header.numberOfMipmapLevels;
        final int nbArrayElement = (header.numberOfArrayElements==0) ? 1 : header.numberOfArrayElements;
        final int pixelDepth     = (header.pixelDepth==0) ? 1 : header.pixelDepth;
        final int pixelHeight    = (header.pixelHeight==0) ? 1 : header.pixelHeight;
        final int pixelWidth     = header.pixelWidth;
        
        //image structure
        final Buffer[][][][] struct = new Buffer[nbMipmaplLevel][nbArrayElement][header.numberOfFaces][pixelDepth];
        
        //mipmaps
        for(int m=0;m<nbMipmaplLevel;m++){
            final int imageSize = ds.readInt();
            //array elements
            for(int a=0;a<nbArrayElement;a++){
                //faces
                for(int f=0;f<header.numberOfFaces;f++){
                    //z
                    for(int z=0;z<pixelDepth;z++){
                        final Buffer buffer = parameters.getBufferFactory().createByte(imageSize);
                        ds.readFully(buffer);
                        struct[m][a][f][z] = buffer;
                    }
                    ds.realign(4);
                }
            }
        }
        
        if(header.glFormat == GLC.Texture.Format.RGB){
            final RawModel rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
            final ColorModel cm = new DirectColorModel(rm, new int[]{0,1,2,-1}, false);
            final Image img = new DefaultImage(struct[0][0][0][0], new Extent.Long(header.pixelWidth, header.pixelHeight), rm, cm);
            return img;
        }
        //TODO all other OpenGL formats
        //TODO think about a correct API to support mipmaps : use ImageStore ?
        else{
            throw new IOException("Unsupported image type : "+header.glFormat);
        }
    }
    
}
