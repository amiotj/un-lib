
package science.unlicense.impl.image.ktx;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class KTXHeader {
 
    public int endianness;
    /**
     * One of GLC.Texture.Type
     */
    public int glType;
    /**
     * 
     */
    public int glTypeSize;
    /**
     * One of GLC.Texture.Format
     */
    public int glFormat;
    /**
     * One of GLC.Texture.InternalFormat
     */
    public int glInternalFormat;
    public int glBaseInternalFormat;
    public int pixelWidth;
    public int pixelHeight;
    public int pixelDepth;
    public int numberOfArrayElements;
    /**
     * For cubemap : 6 , in order +X, -X, +Y, -Y, +Z, -Z
     */
    public int numberOfFaces;
    public int numberOfMipmapLevels;
    public int bytesOfKeyValueData;
    
    public void read(DataInputStream ds) throws IOException {
        
        //check signature
        final byte[] signature = ds.readFully(new byte[12]);
        if(!Arrays.equals(signature, KTXConstants.SIGNATURE)){
            throw new IOException("Input is not a KTX image, wrong signature");
        }
        
        //all values are UInt32
        endianness              = ds.readInt();
        if(endianness==KTXConstants.BIG_ENDIAN){
            ds.setEncoding(NumberEncoding.BIG_ENDIAN);
        }else if(endianness==KTXConstants.LITTLE_ENDIAN){
            ds.setEncoding(NumberEncoding.LITTLE_ENDIAN);
        }else{
            throw new IOException("Unvalid endianness value : "+endianness);
        }
        
        glType                  = ds.readInt();
        glTypeSize              = ds.readInt();
        glFormat                = ds.readInt();
        glInternalFormat        = ds.readInt();
        glBaseInternalFormat    = ds.readInt();
        pixelWidth              = ds.readInt();
        pixelHeight             = ds.readInt();
        pixelDepth              = ds.readInt();
        numberOfArrayElements   = ds.readInt();
        numberOfFaces           = ds.readInt();
        numberOfMipmapLevels    = ds.readInt();
        bytesOfKeyValueData     = ds.readInt();
    }
            
}
