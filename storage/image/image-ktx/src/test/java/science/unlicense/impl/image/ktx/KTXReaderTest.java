
package science.unlicense.impl.image.ktx;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;

/**
 * TODO
 * 
 * @author Johann Sorel
 */
public class KTXReaderTest {
    
    @Ignore
    @Test
    public void readTest() throws IOException{
        //TODO find some free files to add in tests.
        final Path path = Paths.resolve(new Chars("mod:..."));
        final Image image = Images.read(path);
        System.out.println(image);
        
    }
    
}
