package science.unlicense.impl.image.pnm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.ImageWriteParameters;

/**
 *
 * @author Remi Bonnaud
 */
public final class PNMWriteParameters implements ImageWriteParameters {
    // The signature for pnm file (P1, P2, P3, P4, P5, P6)
    private final Chars signature;
    
    private PNMWriteParameters( Chars pnmSignature ) {
        signature = pnmSignature;
    }
    
    /**
     * Accessor for the signature.
     * @return the signature in Chars
     */
    public Chars getSignature(){
        return signature;
    }
    
    /**
     * method that creates an object whith signature = SIGNATURE_BW_ASCII
     * @return PNMWriteParameters with SIGNATURE_BW_ASCII
     */
    public static PNMWriteParameters getPNMMetaModelBwAscii() {
        return new PNMWriteParameters(PNMMetaModel.SIGNATURE_BW_ASCII);
    }
    
    /**
     * method that creates an object whith signature = SIGNATURE_BW_BINARY
     * @return PNMWriteParameters with SIGNATURE_BW_BINARY
     */
    public static PNMWriteParameters getPNMMetaModelBwBinary() {
        return new PNMWriteParameters(PNMMetaModel.SIGNATURE_BW_BINARY);
    }
    
    /**
     * method that creates an object whith signature = SIGNATURE_GRAYSCALE_ASCII
     * @return PNMWriteParameters with SIGNATURE_GRAYSCALE_ASCII
     */
    public static PNMWriteParameters getPNMMetaModelGrayScaleAscii() {
        return new PNMWriteParameters(PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII);
    }

    /**
     * method that creates an object whith signature = SIGNATURE_GRAYSCALE_BINARY
     * @return PNMWriteParameters with SIGNATURE_GRAYSCALE_BINARY
     */
    public static PNMWriteParameters getPNMMetaModelGrayScaleBinary() {
        return new PNMWriteParameters(PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY);
    }

    /**
     * method that creates an object whith signature = SIGNATURE_RGB_ASCII
     * @return PNMWriteParameters with SIGNATURE_RGB_ASCII
     */
    public static PNMWriteParameters getPNMMetaModelRgbAscii() {
        return new PNMWriteParameters(PNMMetaModel.SIGNATURE_RGB_ASCII);
    }
    
    /**
     * method that creates an object whith signature = SIGNATURE_RGB_BINARY
     * @return PNMWriteParameters with SIGNATURE_RGB_BINARY
     */
    public static PNMWriteParameters getPNMMetaModelRgbBinary() {
        return new PNMWriteParameters(PNMMetaModel.SIGNATURE_RGB_BINARY);
    }    

}
