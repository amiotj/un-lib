package science.unlicense.impl.image.pnm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.image.AbstractImageWriter;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageWriteParameters;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.geometry.AbstractTupleBuffer;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Remi Bonnaud
 */
public class PNMImageWriter  extends AbstractImageWriter {

    /**
     * Write an image.
     * @param image
     * @param params
     * @param stream
     * @throws IOException 
     */
    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {                               
        PNMWriteParameters pnmParams = (PNMWriteParameters)params;
        Chars signature = pnmParams.getSignature();
        
        if( PNMMetaModel.SIGNATURE_BW_ASCII.equals(signature) ) {
            writeBWASCII(stream, image );
        }else if(PNMMetaModel.SIGNATURE_BW_BINARY.equals(signature)) {
            writeBWBinary(stream, image);
        }else if(PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.equals(signature)) {
            writeGrayscaleASCII(stream, image);
        }else if(PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.equals(signature)) {
            writeGrayscaleBinary(stream, image);
        }else if(PNMMetaModel.SIGNATURE_RGB_ASCII.equals(signature)) {
            writeRGBASCII( stream, image);
        }else if(PNMMetaModel.SIGNATURE_RGB_BINARY.equals(signature)) {
            writeRGBBinary(stream, image);
        }else {
            throw new IOException("Unknowed PNM image type : "+signature);
        }      
        stream.close();
    }

    /**
     * Store an image in BW_ASCII
     * @param stream
     * @param image
     * @throws IOException 
     * CHECKED OK
     */
    private void writeBWASCII(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);        
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_BW_ASCII.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');      
        ds.write(Int32.encode(height) );
        ds.write('\n');
        //write each pixel
        final int[] coord = new int[2];
        boolean[] pixel;
        for (int y=0; y<height; y++) {            
            coord[1] = y;
            for (int x=0; x<width; x++) {
                coord[0] = x;
                pixel = sm.getTupleBoolean(coord, null);
                if( pixel[0] )
                    ds.write( Int32.encode(1) );
                else
                    ds.write( Int32.encode(0) );
                ds.write(' ');                
            }
            ds.write('\n');
        }  
    }

    /**
     * Store an image in BW_BINARY
     * @param stream
     * @param image
     * @throws IOException 
     * CHECKED OK
     */    
    private void writeBWBinary(ByteOutputStream stream, Image image) throws IOException{
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_BW_BINARY.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');      
        ds.write(Int32.encode(height));
        ds.write('\n');
        
        final ArrayOutputStream aos = new ArrayOutputStream();
        final DataOutputStream dos = new DataOutputStream( aos );                
        //write each pixel
        final int[] coord = new int[2];
        boolean[] pixel;
        final Boolean needPadding = width % 8 != 0;
        final int padding = 8 - (width % 8) ;
        for (int y = 0; y<height; y++) {            
            coord[1] = y;
            for (int x = 0; x<width; x++) {
                coord[0] = x;
                pixel = sm.getTupleBoolean(coord, null);
                if( pixel[0] )
                    dos.writeBit( 1 );
                else
                    dos.writeBit( 0 ); 
            }
            if( needPadding ) {
                dos.writeBits(0, padding);
            }            
        }   
        dos.flush();
        ds.write( aos.getBuffer().toArrayByte() );
        dos.close();
        aos.close();
    }
    
    /**
     * Store an image in GRAYSCALE_ASCII
     * @param stream
     * @param image
     * @throws IOException 
     * CHECKED OK
     */
    private void writeGrayscaleASCII(ByteOutputStream stream, Image image) throws IOException{        
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final RawModel rm = image.getRawModel();
        final TupleBuffer sm = rm.asTupleBuffer(image);
                
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');      
        ds.write(Int32.encode(height));
        ds.write('\n');
        // 
        ds.write( Int32.encode( (int) Math.pow(2, Primitive.getSizeInBits(rm.getPrimitiveType()))-1) );
        ds.write('\n');        
        //write each grayscale
        final int[] coord = new int[2];
        int[] pixel;
        for (int y = 0; y <height; y++) {            
            coord[1] = y;
            for (int x = 0; x < width; x++) {
                coord[0] = x;
                pixel = sm.getTupleUShort(coord, null);
                ds.write( Int32.encode( pixel[0] ) );      
                ds.write(' ');                
            }
            ds.write('\n');
        }   
    }

    /**
     * 
     * @param stream
     * @param image
     * @throws IOException 
     */    
    private void writeGrayscaleBinary(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final RawModel rm = image.getRawModel();
        final TupleBuffer sm = rm.asTupleBuffer(image);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');      
        ds.write(Int32.encode(height));
        ds.write('\n');
        final int maxValue = (int) Math.pow(2,  Primitive.getSizeInBits(rm.getPrimitiveType()))-1;
        ds.write( Int32.encode(maxValue) );
        ds.write('\n');
        //write each pixel
        final ArrayOutputStream aos = new ArrayOutputStream();
        final DataOutputStream dos = new DataOutputStream( aos );    
        final int[] coord = new int[2];
        if( maxValue<256 ) {                        
            byte[] pixel;
            for (int y = 0; y<height; y++) {            
                coord[1] = y;
                for (int x = 0; x<width; x++) {
                    coord[0] = x;
                    pixel = sm.getTupleByte(coord, null);
                    dos.write( pixel[0] );
                }           
            } 
        }
        else {
            int[] pixel;
            for (int y = 0; y<height; y++) {            
                coord[1] = y;
                for (int x = 0; x<width; x++) {
                    coord[0] = x;
                    pixel = sm.getTupleUByte(coord, null);
                    dos.writeUShort( pixel[0] );
                }           
            } 
        }
        dos.flush();
        ds.write( aos.getBuffer().toArrayByte() );
        dos.close();
        aos.close();  
    }

    /**
     * Store an image in RGB_ASCII 
     * @param stream ByteOutputStream for save.
     * @param image image object to save.
     * @throws IOException 
     * CHECKED OK 
     */
    private void writeRGBASCII(ByteOutputStream stream, Image image) throws IOException {
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final RawModel rm = image.getRawModel();
        final TupleBuffer sm = rm.asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();                
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_RGB_ASCII.toBytes());
        ds.write('\n');
        // width and height
        ds.write( Int32.encode(width) );
        ds.write(' ');      
        ds.write(Int32.encode(height));
        ds.write('\n');
        // 
        ds.write( Int32.encode( (int) Math.pow(2,  Primitive.getSizeInBits(rm.getPrimitiveType()))-1) );
        ds.write('\n');
        //write each color
        final int[] coord = new int[2];
        Color color = null;
        final Object samples = sm.createTupleStore();
        for (int y = 0; y <height; y++) {            
            coord[1] = y;
            for (int x = 0; x < width; x++) {
                coord[0] = x;
                sm.getTuple(coord, samples);
                color = cm.toColor(samples);
                ds.write( Int32.encode( (int)(color.getRed() * 255)) );
                ds.write(' ');  
                ds.write( Int32.encode( (int)(color.getGreen() * 255)) );
                ds.write(' ');  
                ds.write( Int32.encode( (int)(color.getBlue() * 255)) );
                ds.write(' ');                
            }
            ds.write('\n');
        }
    }
    
    /**
     * Store an image in RGB_BINARY
     * @param stream
     * @param image
     * @throws IOException 
     * CHECKER OK
     */
    private void writeRGBBinary(ByteOutputStream stream, Image image) throws IOException {        
        final CharOutputStream ds = new CharOutputStream(stream);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final RawModel rm = image.getRawModel();
        final TupleBuffer sm = rm.asTupleBuffer(image);
        // Signature
        ds.write(PNMMetaModel.SIGNATURE_RGB_BINARY.toBytes());
        ds.write('\n');
        // Size
        ds.write( Int32.encode(width) );
        ds.write(' ');      
        ds.write(Int32.encode(height));
        ds.write('\n');
        final int maxValue = (int) Math.pow(2,  Primitive.getSizeInBits(rm.getPrimitiveType()))-1;
        ds.write( Int32.encode(maxValue) );
        ds.write('\n');     
        //write each pixel
        final int[] coord = new int[2];          
        final ArrayOutputStream aos = new ArrayOutputStream();
        final DataOutputStream dos = new DataOutputStream( aos );          
        if( maxValue<256 ) {

            byte[] pixel;
            for (int y = 0; y<height; y++) {            
                coord[1] = y;
                for (int x = 0; x<width; x++) {
                    coord[0] = x;
                    pixel = sm.getTupleByte(coord, null);
                    dos.write(pixel[0] );
                    dos.write(pixel[1] );
                    dos.write(pixel[2] );
                }           
            } 
        }
        else {
            int[] pixel;
            for (int y = 0; y<height; y++) {            
                coord[1] = y;
                for (int x = 0; x<width; x++) {
                    coord[0] = x;
                    pixel = sm.getTupleUByte(coord, null);
                    dos.writeUShort(pixel[0] );
                    dos.writeUShort(pixel[1] );
                    dos.writeUShort(pixel[2] );
                }           
            } 
        }
        dos.flush();
        ds.write( aos.getBuffer().toArrayByte() );
        dos.close();
        aos.close();    
    }
    
    /**
     * return a default ImageWriteParameters for PNM image
     * @return PNMWriteParameters.getPNMMetaModelGrayScaleAscii()
     */
    public ImageWriteParameters createParameters() {
        return PNMWriteParameters.getPNMMetaModelGrayScaleAscii() ;
    }    

}
