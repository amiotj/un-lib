package science.unlicense.impl.image.pnm;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class PNMMetaModel {

    public static final Chars SIGNATURE_BW_ASCII        = new Chars("P1");
    public static final Chars SIGNATURE_BW_BINARY       = new Chars("P4");
    public static final Chars SIGNATURE_GRAYSCALE_ASCII = new Chars("P2");
    public static final Chars SIGNATURE_GRAYSCALE_BINARY= new Chars("P5");
    public static final Chars SIGNATURE_RGB_ASCII       = new Chars("P3");
    public static final Chars SIGNATURE_RGB_BINARY      = new Chars("P6");

    private PNMMetaModel(){}

}
