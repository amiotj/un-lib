
package science.unlicense.impl.image.pnm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * Format explication :
 * http://fr.wikipedia.org/wiki/Portable_pixmap
 *
 * @author Johann Sorel
 */
public class PNMImageFormat extends AbstractImageFormat{

    public PNMImageFormat() {
        super(new Chars("pnm"),
              new Chars("Netpbm"),
              new Chars("Netpbm"),
              new Chars[]{
                  new Chars("image/x-portable-pixmap"),
                  new Chars("image/x-portable-graymap"),
                  new Chars("image/x-portable-bitmap"),
              },
              new Chars[]{
                  new Chars("ppm"),
                  new Chars("pgm"),
                  new Chars("pbm"),
                  new Chars("pnm")
              },
              new byte[][]{
                  PNMMetaModel.SIGNATURE_BW_ASCII.toBytes(),
                  PNMMetaModel.SIGNATURE_BW_BINARY.toBytes(),
                  PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.toBytes(),
                  PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.toBytes(),
                  PNMMetaModel.SIGNATURE_RGB_ASCII.toBytes(),
                  PNMMetaModel.SIGNATURE_RGB_BINARY.toBytes()
              });
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return true;
    }

    public ImageReader createReader() {
        return new PNMImageReader();
    }

    public ImageWriter createWriter() {
        return new PNMImageWriter();
    }

}
