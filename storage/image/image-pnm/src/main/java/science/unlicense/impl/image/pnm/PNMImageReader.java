package science.unlicense.impl.image.pnm;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageSetMetadata;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.image.sample.PlanarRawModel2D;

/**
 *
 * @author Johann Sorel
 */
public class PNMImageReader extends AbstractImageReader{

    private Chars type;
    private int width;
    private int height;
    //used in grascale and rgb images only
    private int maxValue;
    private ImageSetMetadata.Image mdImage;

    //for various internal use
    private final ByteSequence buffer = new ByteSequence();

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        stream.mark();
        final DataInputStream ds = new DataInputStream(stream);
        type = new Chars(ds.readFully(new byte[2]));
        expectSpace(stream,ds,true);
        width = Int32.decode(new Chars(readWord(stream,ds)));
        expectSpace(stream,ds,true);
        height = Int32.decode(new Chars(readWord(stream,ds)));
        expectSpace(stream,ds,true);
        if(   PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.equals(type)
           || PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.equals(type)
           || PNMMetaModel.SIGNATURE_RGB_ASCII.equals(type)
           || PNMMetaModel.SIGNATURE_RGB_BINARY.equals(type)){
            maxValue = Int32.decode(new Chars(readWord(stream,ds)));
            expectSpace(stream,ds,true);
        }

        mdImage = new ImageSetMetadata.Image(width, height);
        final ImageSetMetadata mdImageSet = new ImageSetMetadata(mdImage);
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);

        if(PNMMetaModel.SIGNATURE_BW_ASCII.equals(type)){
            return readBWASCII(stream);
        }else if(PNMMetaModel.SIGNATURE_BW_BINARY.equals(type)){
            return readBWBinary(stream);
        }else if(PNMMetaModel.SIGNATURE_GRAYSCALE_ASCII.equals(type)){
            return readGrayscaleASCII(stream);
        }else if(PNMMetaModel.SIGNATURE_GRAYSCALE_BINARY.equals(type)){
            return readGrayscaleBinary(stream);
        }else if(PNMMetaModel.SIGNATURE_RGB_ASCII.equals(type)){
            return readRGBASCII(stream);
        }else if(PNMMetaModel.SIGNATURE_RGB_BINARY.equals(type)){
            return readRGBBinary(stream);
        }else{
            throw new IOException("Unknowed PNM image type : "+type);
        }

    }

    private Image readBWASCII(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out);
        for(int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for(int x=0;x<width;x++){
                expectSpace(stream, ds, false);
                final byte b = ds.readByte();
                if(b=='0'){
                    ods.writeBit(0);
                }else if(b=='1'){
                    ods.writeBit(1);
                }else{
                    throw new IOException("Unpected value "+b);
                }
            }
        }
        ods.flush();

        final Buffer bank = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());
        final RawModel sm = new PlanarRawModel2D(RawModel.TYPE_1_BIT, 1);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readBWBinary(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out);
        for(int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for(int x=0;x<width;x++){
                final int b = ds.readBits(1);
                ods.writeBit(b);
            }
            ds.skipToByteEnd();
        }
        ods.flush();

        final Buffer bank = DefaultBufferFactory.wrap(out.getBuffer().getBackArray());
        final RawModel sm = new PlanarRawModel2D(RawModel.TYPE_1_BIT, 1);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readGrayscaleASCII(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);

        final int sampleType = (maxValue<256) ? RawModel.TYPE_UBYTE : RawModel.TYPE_USHORT;

        final int bytePerSample = (Images.getBitsPerSample(sampleType)/8);
        final int scanline = bytePerSample*width;
        final byte[] datas = new byte[scanline*height];
        int offset = 0;

        for(int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for(int x=0;x<width;x++){
                expectSpace(stream, ds, false);
                final int val = Int32.decode(new Chars(readWord(stream,ds)));
                if(sampleType==RawModel.TYPE_UBYTE){
                    NumberEncoding.BIG_ENDIAN.writeUByte(val, datas, offset);
                    offset+=1;
                }else{
                    NumberEncoding.BIG_ENDIAN.writeUShort(val, datas, offset);
                    offset+=2;
                }
            }
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final RawModel sm = new PlanarRawModel2D(sampleType, 1);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readGrayscaleBinary(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);

        final int sampleType = (maxValue<256) ? RawModel.TYPE_UBYTE : RawModel.TYPE_USHORT;

        final int scanline = (Images.getBitsPerSample(sampleType)/8)*width;
        final byte[] datas = new byte[scanline*height];

        for(int y=0,offset=0;y<height;y++,offset+=scanline){
            expectSpace(stream, ds, false);
            ds.readFully(datas,offset,scanline);
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final RawModel sm = new PlanarRawModel2D(sampleType, 1);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readRGBASCII(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);
        final int sampleType = (maxValue<256) ? RawModel.TYPE_UBYTE : RawModel.TYPE_USHORT;

        final int scanline = (Images.getBitsPerSample(sampleType)/8)*width*3;
        final byte[] datas = new byte[scanline*height];
        int offset = 0;

        for(int y=0;y<height;y++){
            expectSpace(stream, ds, false);
            for(int x=0,n=width*3;x<n;x++){
                expectSpace(stream, ds, false);
                final int val = Int32.decode(new Chars(readWord(stream,ds)));
                if(sampleType==RawModel.TYPE_UBYTE){
                    NumberEncoding.BIG_ENDIAN.writeUByte(val, datas, offset);
                    offset+=1;
                }else{
                    NumberEncoding.BIG_ENDIAN.writeUShort(val, datas, offset);
                    offset+=2;
                }
            }
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final RawModel sm = new InterleavedRawModel(sampleType, 3);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,1,2,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    private Image readRGBBinary(BacktrackInputStream stream) throws IOException{
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);

        final int sampleType = (maxValue<256) ? RawModel.TYPE_UBYTE : RawModel.TYPE_USHORT;

        final int scanline = (Images.getBitsPerSample(sampleType)/8)*width*3;
        final byte[] datas = new byte[scanline*height];

        for(int y=0,offset=0;y<height;y++,offset+=scanline){
            expectSpace(stream, ds, false);
            ds.readFully(datas,offset,scanline);
        }

        final Buffer bank = DefaultBufferFactory.wrap(datas);
        final RawModel sm = new InterleavedRawModel(sampleType, 3);
        final ColorModel cm = new DirectColorModel(sm, new int[]{0,1,2,-1}, false);

        return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
    }

    /**
     * Check escape character and skip comments.
     * @param ds
     * @throws IOException
     */
    private void expectSpace(BacktrackInputStream bs, DataInputStream ds, boolean expected) throws IOException{
        final byte c = ds.readByte();

        if(c==' ' || c=='\t'){
            //do nothing
        }else if(c=='\n'){
            //skip comment lines
            for(byte b=ds.readByte(); b=='#'; b=ds.readByte()){
                //find end of line
                for(b=ds.readByte(); b!='\n'; b=ds.readByte());
            }
            int position = bs.position()-1;
            bs.rewind();
            bs.skip(position);
        }else{
            if(expected){
                //escape char is imposed
                throw new IOException("Was expecting a spacing character.");
            }else{
                //escape char is optional, move back
                int position = bs.position()-1;
                bs.rewind();
                bs.skip(position);
            }
        }

    }

    private byte[] readWord(BacktrackInputStream bs, final DataInputStream ds) throws IOException{
        buffer.removeAll();
        for(byte b=ds.readByte(); !isEscape(b); b=ds.readByte()){
            buffer.put(b);
        }
        int position = bs.position()-1;
        bs.rewind();
        bs.skip(position);
        return buffer.toArrayByte();
    }

    private static boolean isEscape(final byte b){
        return b==' '||b=='\t'||b=='\n';
    }

}
