package science.unlicense.impl.image.pnm;

import science.unlicense.impl.image.pnm.PNMImageWriter;
import science.unlicense.impl.image.pnm.PNMImageReader;
import science.unlicense.impl.image.pnm.PNMWriteParameters;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;
import science.unlicense.system.path.Paths;

/**
 * 
 * @author Remi Bonnaud
 * @author Johann Sorel
 */
public class PNMWriterTest {
  

    @Test
    public void testBWASCII() throws Exception {
        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/pnm/ascii.pbm")).createInputStream();
        final ImageReader reader = new PNMImageReader();
        reader.setInput(bs);        
        final Image image = reader.read(reader.createParameters());

        final ImageWriter writer = new PNMImageWriter();
        final ArrayOutputStream bos = new ArrayOutputStream();
        writer.setOutput( bos );
                    
        writer.write(image, PNMWriteParameters.getPNMMetaModelBwAscii() );     
        
        final Chars result = new Chars(bos.getBuffer().toArrayByte());
        Assert.assertEquals(new Chars(
                "P1\n" +
                "2 2\n" +
                "1 0 \n" +
                "0 1 \n"), 
                result);
        
    }

    @Test
    public void testBWBinary() throws Exception {
        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/pnm/binary.pbm")).createInputStream();
        final ImageReader reader = new PNMImageReader();
        reader.setInput(bs);        
        final Image image = reader.read(reader.createParameters());

        final ImageWriter writer = new PNMImageWriter();
        final ArrayOutputStream bos = new ArrayOutputStream();
        writer.setOutput( bos );
               
        writer.write(image, PNMWriteParameters.getPNMMetaModelBwBinary());    
        
    }

    @Test
    public void testGrayscaleASCII() throws Exception {
        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/pnm/ascii.pgm")).createInputStream();
        final ImageReader reader = new PNMImageReader();
        reader.setInput(bs);        
        final Image image = reader.read(reader.createParameters());
                
        final ImageWriter writer = new PNMImageWriter();
        final ArrayOutputStream bos = new ArrayOutputStream();
        writer.setOutput( bos );
              
        writer.write(image, PNMWriteParameters.getPNMMetaModelGrayScaleAscii());      
        
        final Chars result = new Chars(bos.getBuffer().toArrayByte());
        Assert.assertEquals(new Chars(
                "P2\n" +
                "2 2\n" +
                "255\n" +
                "54 237 \n" +
                "201 18 \n"), 
                result);
    }

    @Test
    public void testGrayscaleBinary() throws Exception {       
        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/pnm/binary.pgm")).createInputStream();
        final ImageReader reader = new PNMImageReader();
        reader.setInput(bs);        
        final Image image = reader.read(reader.createParameters());

        final ImageWriter writer = new PNMImageWriter();
        final ArrayOutputStream bos = new ArrayOutputStream();
        writer.setOutput( bos );
                     
        writer.write(image, PNMWriteParameters.getPNMMetaModelGrayScaleBinary());            
    }

    @Test
    public void testRGBASCII() throws Exception {
        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/pnm/ascii.ppm")).createInputStream();
        final ImageReader reader = new PNMImageReader();
        reader.setInput(bs);        
        final Image image = reader.read(reader.createParameters());        
        
        final ImageWriter writer = new PNMImageWriter();
        final ArrayOutputStream bos = new ArrayOutputStream();
        writer.setOutput( bos );
        writer.write(image, PNMWriteParameters.getPNMMetaModelRgbAscii());
        
        final Chars result = new Chars(bos.getBuffer().toArrayByte());
        Assert.assertEquals(new Chars(
                "P3\n" +
                "2 2\n" +
                "255\n" +
                "255 0 0 255 255 0 \n" +
                "0 255 255 0 0 255 \n"), 
                result);
    }

    @Test
    public void testRGBBinary() throws Exception {
        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/pnm/binary.ppm")).createInputStream();
        final ImageReader reader = new PNMImageReader();
        reader.setInput(bs);        
        final Image image = reader.read(reader.createParameters());        
        
        final ImageWriter writer = new PNMImageWriter();
        final ArrayOutputStream bos = new ArrayOutputStream();
        writer.setOutput( bos );
                       
        writer.write(image, PNMWriteParameters.getPNMMetaModelRgbBinary());
    }    
    
}
