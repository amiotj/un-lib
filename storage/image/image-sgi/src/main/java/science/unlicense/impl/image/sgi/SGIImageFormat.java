
package science.unlicense.impl.image.sgi;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * Format specification :
 * ftp://ftp.sgi.com/graphics/grafica/sgiimage.html
 *
 * @author Johann Sorel
 */
public class SGIImageFormat extends AbstractImageFormat{

    public SGIImageFormat() {
        super(new Chars("sgi"),
              new Chars("SGI"),
              new Chars("Silicon Graphics Image"),
              new Chars[]{
                  new Chars("image/sgi")
              },
              new Chars[]{
                new Chars("sgi")
              },
              new byte[][]{SGIConstants.BSIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new SGIImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
