
package science.unlicense.impl.image.sgi;

/**
 *
 * @author Johann Sorel
 */
public final class SGIConstants {

    /**
     * 2byte file header magic.
     */
    public static final short SIGNATURE = 474;
    public static final byte[] BSIGNATURE = new byte[]{0x01,(byte)0xDA};

    /**
     * No compression.
     */
    public static final byte STORAGE_VERBATIM = 0;
    /**
     * RLE compression.
     */
    public static final byte STORAGE_RLE = 1;

    /** 0: NORMAL
     * The data in the channels represent B/W values for images
     * with 1 channel, RGB values for images with 3 channels, and RGBA
     * values for images with 4 channels. Almost all the SGI image files are of this type.
     */
    public static final int COLORMAP_NORMAL = 0;

    /** 1: DITHERED
     * The image will have only 1 channel of data. For each pixel, RGB data
     * is packed into one 8 bit value. 3 bits are used for red and green, while
     * blue uses 2 bits. Red data is found in bits[2..0], green data in bits[5..3],
     * and blue data in bits[7..6]. This format is obsolete.
     */
    public static final int COLORMAP_DITHERED = 1;

    /** 2: SCREEN
     * The image will have only 1 channel of data. This format was used to
     * store color-indexed pixels. To convert the pixel values into RGB values a
     * colormap must be used. The appropriate color map varies from image to image.
     * This format is obsolete.
     */
    public static final int COLORMAP_SCREEN = 2;

    /** 3: COLORMAP
     * The image is used to store a color map from an SGI machine. In this
     * case the image is not displayable in the conventional sense.
     */
    public static final int COLORMAP_COLORMAP = 3;

    private SGIConstants() {}

}
