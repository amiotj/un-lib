
package science.unlicense.impl.image.sgi;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.impl.io.ClipInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.PlanarRawModel2D;

/**
 * SGI Image reader.
 *
 * @author Johann Sorel
 */
public class SGIImageReader extends AbstractImageReader {

    private SGIHeader header;
    private TypedNode mdImage;

    protected Dictionary readMetadatas(final BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.BIG_ENDIAN);
        if(SGIConstants.SIGNATURE != ds.readUShort()){
            throw new IOException("Stream is not a valid SGI Image.");
        }

        header = new SGIHeader();
        header.read(stream);

        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,header.xsize)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,header.ysize)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);

        stream.rewind();
        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.BIG_ENDIAN);
        ds.skipFully(header.dataOffset);

        //read pixel datas
        final int scanlineSize = header.bpc*header.xsize;
        final Buffer bank = parameters.getBufferFactory().createByte(scanlineSize*header.ysize*header.zsize);

        //the image data start from the lower left corner so we need to
        //flip the image while reading
        if(SGIConstants.STORAGE_VERBATIM == header.storage){
            for(int z=0;z<header.zsize;z++){
                int offset = header.xsize*header.ysize*z;
                for(int y=header.ysize-1;y>=0;y--){
                    ds.readFully(bank, offset+y*scanlineSize , scanlineSize);
                }
            }

        }else if(SGIConstants.STORAGE_RLE == header.storage){
            final int[] info = new int[2];

            for(int z=0;z<header.zsize;z++){
                int offset = header.xsize*header.ysize*z;
                for(int y=0;y<header.ysize;y++){
                    //get the row offset
                    header.getRowRLEInfo(y, z, info);
                    //move the stream at correct location
                    stream.rewind();
                    ds.skipFully(info[0]);
                    //decompress row
                    final ClipInputStream ci = new ClipInputStream(stream, info[1]);
                    final SGIRLEInputStream rleis = new SGIRLEInputStream(ci, header.bpc);
                    final DataInputStream rds = new DataInputStream(rleis, NumberEncoding.BIG_ENDIAN);
                    rds.readFully(bank, offset+(header.ysize-1-y)*scanlineSize , scanlineSize);
                }
            }
        }

        //rebuild sample model
        final int sampleType = (header.bpc == 1) ? RawModel.TYPE_UBYTE : RawModel.TYPE_USHORT;
        final RawModel sm = new PlanarRawModel2D(sampleType,header.zsize);

        //rebuild color model
        final ColorModel cm;
        if(SGIConstants.COLORMAP_NORMAL == header.colormap){
            if(header.zsize == 1){
                //bleack and white or grayscale
                cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);
            }else if(header.zsize == 3){
                //RGB image
                cm = new DirectColorModel(sm, new int[]{0,1,2,-1}, false);
            }else if(header.zsize == 4){
                //RGBA image
                cm = new DirectColorModel(sm, new int[]{0,1,2,3}, false);
            }else{
                //anything else, just use the first channel as grayscale
                cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);
            }
        }else if(SGIConstants.COLORMAP_DITHERED == header.colormap){
            throw new IOException("Dithered colormap not supported yet.");
        }else if(SGIConstants.COLORMAP_SCREEN == header.colormap){
            throw new IOException("Screen colormap not supported yet.");
        }else if(SGIConstants.COLORMAP_COLORMAP == header.colormap){
            throw new IOException("Colormap type colormap not supported yet.");
        }else{
            throw new IOException("Unknowned colormap type : "+ header.colormap);
        }

        return new DefaultImage(bank, new Extent.Long(header.xsize, header.ysize), sm,cm);
    }

}
