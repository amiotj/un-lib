
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Quantization component.
 *
 * Describes the quantization used for compressing a particular component.
 *
 * @author Johann Sorel
 */
public class QCC extends Marker {

    public int nbComponent;
    public int codingStyle;
    public byte[] parameters;

    public QCC() {
        super(JPEG2KConstants.MK_QCC);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();
        if(siz.nbComponent<257){
            nbComponent = ds.readUByte();
        }else{
            nbComponent = ds.readUShort();
        }
        codingStyle = ds.readUByte();
        parameters = ds.readFully(new byte[parametersLength-2-1- ((siz.nbComponent<257)?1:2)]);
    }

}
