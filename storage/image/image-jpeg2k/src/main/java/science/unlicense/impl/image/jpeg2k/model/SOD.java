
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Start of data.
 *
 * Indicates the beginning of bit stream data for the current tile-part.
 * The SOD also indicates the end of a tile-part header.
 *
 * @author Johann Sorel
 */
public class SOD extends Marker {

    public SOD() {
        super(JPEG2KConstants.MK_SOD);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException {
        parametersLength = 0;
    }

}
