
package science.unlicense.impl.image.jpeg2k;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * JPEG2000 specification :
 * only drafts are public, not final spec,
 * must pay ISO for the full spec, I'm not that rich :D
 *
 * http://www.jpeg.org/public/fcd15444-1.pdf
 * http://www.jpeg.org/public/fcd15444-2.pdf
 * http://www.jpeg.org/public/fcd15444-3.pdf
 * http://www.jpeg.org/public/fcd15444-4.pdf
 * http://www.jpeg.org/public/fcd15444-6.pdf
 * http://www.jpeg.org/public/fcd15444-8.pdf
 * http://www.jpeg.org/public/fcd15444-10.pdf
 * http://www.jpeg.org/public/fcd15444-11.pdf
 * http://www.jpeg.org/public/15444-1annexi.pdf
 *
 *
 * @author Johann Sorel
 */
public class JPEG2KImageFormat extends AbstractImageFormat{

    public JPEG2KImageFormat() {
        super(new Chars("jpeg2k"),
              new Chars("JPEG-2000"),
              new Chars("Joint Photographic Experts Group - 2000"),
              new Chars[]{
                  new Chars("image/jp2"),
                  new Chars("image/jpx"),
                  new Chars("image/jpm"),
                  new Chars("video/mj2")
              },
              new Chars[]{
                  new Chars("dcm"),
                  new Chars("jp2"),
                  new Chars("j2k"),
                  new Chars("jpf"),
                  new Chars("jpx"),
                  new Chars("jpm"),
                  new Chars("mj2")
              },new byte[][]{JPEG2KConstants.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new JPEG2KImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
