

package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KUtilities;

/**
 *
 * @author Johann Sorel
 */
public class Marker {

    /** All markers have an identifier, 2 bytes */
    public final int identifier;
    /** Attributes lenghts, 2 bytes unsigned */
    public int parametersLength;

    public Marker(int identifier) {
        this.identifier = identifier;
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        if(JPEG2KUtilities.isZeroLengthMarker(identifier)){
            parametersLength = 0;
            return;
        }
        parametersLength = ds.readUShort();
        ds.skipFully(parametersLength-2);

    }

}
