
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class MainHeader {

    public final Sequence markers = new ArraySequence();

}
