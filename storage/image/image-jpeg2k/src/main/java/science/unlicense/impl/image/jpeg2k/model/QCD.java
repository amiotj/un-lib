
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Quantization default.
 *
 * Describes the quantization default used for compressing all components not
 * defined by a QCC marker segment. The parameter values can be overridden for
 * an individual component by a QCC marker segment in either the main or tile-part header.
 *
 * @author Johann Sorel
 */
public class QCD extends Marker {

    public int codingStyle;
    public byte[] parameters;

    public QCD() {
        super(JPEG2KConstants.MK_QCD);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();
        codingStyle = ds.readUByte();
        parameters = ds.readFully(new byte[parametersLength-2-1]);
    }

}
