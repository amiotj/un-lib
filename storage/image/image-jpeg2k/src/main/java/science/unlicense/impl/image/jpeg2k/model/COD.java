
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Coding style default.
 *
 * Describes the coding style, decomposition, and layering that is the default
 * used for compressing all components of an image (if in the main header) or a
 * tile (if in the tile-part header) that are not described by COC marker segment.
 * The parameter values can be overridden for an individual component by a COC
 * marker segment in either the main or tile-part header.
 *
 * @author Johann Sorel
 */
public class COD extends Marker {

    public int codingStyle;
    public byte[] parameters;

    public COD() {
        super(JPEG2KConstants.MK_COD);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();
        codingStyle = ds.readUByte();
        parameters = ds.readFully(new byte[parametersLength-2-1]);
    }

}
