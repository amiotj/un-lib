
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Packed packet headers, tile-part header.
 *
 * A collection of the packet headers so multiple reads are not required to decode headers.
 *
 * @author Johann Sorel
 */
public class PPT extends Marker {

    public PPT() {
        super(JPEG2KConstants.MK_PPT);
    }

}
