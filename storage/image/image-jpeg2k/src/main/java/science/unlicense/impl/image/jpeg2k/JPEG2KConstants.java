
package science.unlicense.impl.image.jpeg2k;

/**
 *
 * @author Johann Sorel
 */
public final class JPEG2KConstants {

    public static final byte[] SIGNATURE = new byte[]{
        0x00,0x00,0x00,0x0C,
        0x6A,0x50,0x20,0x20,
        0x0D,0x0A,(byte)0x87,0x0A
    };
    
    //Delimiting marker segments ///////////
    /** Start of codestream */
    public static final int MK_SOC = 0xFF4F;
    /** Start of tile-part */
    public static final int MK_SOT = 0xFF90;
    /** Start of data */
    public static final int MK_SOD = 0xFF93;
    /** End of codestreamb */
    public static final int MK_EOC = 0xFFD9;
    //Fixed information marker segments ////
    /** Image and tile size */
    public static final int MK_SIZ = 0xFF51;
    //Functional marker segments ///////////
    /** Coding style default */
    public static final int MK_COD = 0xFF52;
    /** Coding style component */
    public static final int MK_COC = 0xFF53;
    /** Region-of-interest */
    public static final int MK_RGN = 0xFF5E;
    /** Quantization default */
    public static final int MK_QCD = 0xFF5C;
    /** Quantization component */
    public static final int MK_QCC = 0xFF5D;
    /** Progression order default */
    public static final int MK_POD = 0xFF5F;
    //Pointer marker segments///////////////
    /** Tile-part lengths, main header */
    public static final int MK_TLM = 0xFF55;
    /** Packet length, main header */
    public static final int MK_PLM = 0xFF57;
    /** Packet length, tile-part header */
    public static final int MK_PLT = 0xFF58;
    /** Packed packet headers, main header */
    public static final int MK_PPM = 0xFF60;
    /** Packed packet headers, tile-part header */
    public static final int MK_PPT = 0xFF61;
    //In bit stream marker segments/////////
    /** Start of packet */
    public static final int MK_SOP = 0xFF91;
    /** End of packet header */
    public static final int MK_EPH = 0xFF92;
    //Informational marker segments/////////
    /** Comment and extension */
    public static final int MK_CME = 0xFF64;

    private JPEG2KConstants(){}

}
