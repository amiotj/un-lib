
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Start of tile-part.
 *
 * Marks the beginning of a tile-part and the index of its tile within a
 * codestream. The tile-parts of a tile shall appear in order (see TPsot) in the
 * codestream, but not necessarily consecutively.
 *
 * @author Johann Sorel
 */
public class SOT extends Marker {

    public int tileNumber;
    /**
     * Length, in bytes, from the beginning of the first byte of this SOT
     * marker segment of the tile-part to the end of the data of that tile-part.
     * Last SOT may have a size of 0.
     */
    public int totalLength;
    public int tilePartInstance;
    public int numberOfTilePart;


    public SOT() {
        super(JPEG2KConstants.MK_SOT);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();
        tileNumber = ds.readUShort();
        totalLength = ds.readInt();
        tilePartInstance = ds.readUByte();
        numberOfTilePart = ds.readUByte();
    }

}
