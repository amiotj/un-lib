
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Region-of-interest.
 *
 * Signals the location, shift, and type of RGN in the codestream.
 *
 * @author Johann Sorel
 */
public class RGN extends Marker {

    public int nbComponent;
    public int roiStyle;
    public byte[] parameters;

    public RGN() {
        super(JPEG2KConstants.MK_RGN);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException{
        parametersLength = ds.readUShort();
        if(siz.nbComponent<257){
            nbComponent = ds.readUByte();
        }else{
            nbComponent = ds.readUShort();
        }
        roiStyle = ds.readUByte();
        parameters = ds.readFully(new byte[parametersLength-2-1- ((siz.nbComponent<257)?1:2)]);
    }

}
