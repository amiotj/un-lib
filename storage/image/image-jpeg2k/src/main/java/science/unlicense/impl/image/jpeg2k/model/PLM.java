
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Packet length, main header.
 *
 * A list of packet lengths in the tile-parts. This exists for every tile-part in order.
 *
 * @author Johann Sorel
 */
public class PLM extends Marker {

    public int markerIndex;

    public PLM() {
        super(JPEG2KConstants.MK_PLM);
    }

//    public void read(DataInputStream ds, SIZ siz) throws IOException{
//        parametersLength = ds.readUnsignedShort();
//        markerIndex = ds.readUnsignedByte();
//        //TODO
//
//    }

}
