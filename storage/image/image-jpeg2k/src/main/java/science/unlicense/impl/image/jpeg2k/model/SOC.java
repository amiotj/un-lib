
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * Start of codestream.
 *
 * Marks the beginning of a codestream specified in this Recommendation | International Standard.
 *
 * @author Johann Sorel
 */
public class SOC extends Marker {

    public SOC() {
        super(JPEG2KConstants.MK_SOC);
    }

    public void read(DataInputStream ds, SIZ siz) throws IOException {
        parametersLength = 0;
    }

}
