

package science.unlicense.impl.image.jpeg2k;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.impl.image.jpeg2k.model.Marker;
import science.unlicense.impl.image.jpeg2k.model.SIZ;

/**
 *
 * @author Johann Sorel
 */
public class JPEG2KImageReader extends AbstractImageReader {


    private final Sequence markers = new ArraySequence();
    private SIZ imageSize;

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);
        
        if(!Arrays.equals(JPEG2KConstants.SIGNATURE,ds.readFully(new byte[12]))){
            throw new IOException("Wrong signature, stream is not a valid jpeg 2000 image.");
        }
        
        stream.mark();

        readMarkers(stream);


        return null;
    }

    private void readMarkers(final BacktrackInputStream stream) throws IOException{

        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);

        for(;;){
            final int b1 = ds.read();
            if(b1==-1) break;
            final int b2 = ds.read();
            final int markerId = b1 << 8 | b2;

            final Marker marker = JPEG2KUtilities.createMarker(markerId);
            if(marker instanceof SIZ){
                imageSize = (SIZ) marker;
            }
            marker.read(ds,imageSize);
            System.out.println(marker.getClass());

        }

    }

}
