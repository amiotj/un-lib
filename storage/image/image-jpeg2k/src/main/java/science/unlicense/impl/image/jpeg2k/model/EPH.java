
package science.unlicense.impl.image.jpeg2k.model;

import science.unlicense.impl.image.jpeg2k.JPEG2KConstants;

/**
 * End of packet header.
 *
 * Indicates the end of the packet header for a given packet. This delimits the
 * packet headers in stream or in the PPM or PPT marker segments. This marker does
 * not denote the beginning of packet data. If there is no packet header in stream,
 * this marker shall not be used.
 *
 * @author Johann Sorel
 */
public class EPH extends Marker {

    public EPH() {
        super(JPEG2KConstants.MK_EPH);
    }

}
