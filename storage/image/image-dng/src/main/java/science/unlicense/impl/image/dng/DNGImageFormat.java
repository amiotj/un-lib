
package science.unlicense.impl.image.dng;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * http://en.wikipedia.org/wiki/Digital_Negative
 * 
 * @author Johann Sorel
 */
public class DNGImageFormat extends AbstractImageFormat{

    public DNGImageFormat() {
        super(new Chars("dng"),
              new Chars("DNG"),
              new Chars("Digital Negative"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("dng")
              },
              new byte[0][0]);
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
