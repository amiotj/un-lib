
package science.unlicense.impl.image.hdr;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * Source :
 * http://en.wikipedia.org/wiki/RGBE_image_format
 * http://en.wikipedia.org/wiki/Radiance_(software)#HDR_image_format
 *
 *
 * @author Johann Sorel
 */
public class HDRImageFormat extends AbstractImageFormat{

    public HDRImageFormat() {
        super(new Chars("hdr"),
              new Chars("HDR"),
              new Chars("RGBE - Radiance"),
              new Chars[]{
                  new Chars("image/vnd.radiance")
              },
              new Chars[]{
                new Chars("hdr")
              },
              new byte[0][0]);
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
