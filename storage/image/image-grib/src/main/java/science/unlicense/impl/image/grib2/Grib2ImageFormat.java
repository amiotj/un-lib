
package science.unlicense.impl.image.grib2;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class Grib2ImageFormat extends AbstractImageFormat{

    public Grib2ImageFormat() {
        super(new Chars("grib2"),
              new Chars("GRIdded Binary 2"),
              new Chars("General Regularly-distributed Information in Binary form 2"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("grd")
              },
              new byte[0][0]);
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
