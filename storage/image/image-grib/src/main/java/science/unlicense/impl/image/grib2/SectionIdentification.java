
package science.unlicense.impl.image.grib2;

/**
 *
 * 1-4	Length of the section in octets (21 or N)
 * 5	Number of the section (1)
 * 6-7	Identification of originating/generating center (See Table 0) (See note 4)
 * 8-9	Identification of originating/generating subcenter (See Table C)
 * 10	GRIB master tables version number (currently 2) (See Table 1.0) (See note 1)
 * 11	Version number of GRIB local tables used to augment Master Tables (see Table 1.1)
 * 12	Significance of reference time (See Table 1.2)
 * 13-14	Year (4 digits)
 * 15	Month
 * 16	Day
 * 17	Hour
 * 18	Minute
 * 19	Second
 * 20	Production Status of Processed data in the GRIB message (See Table 1.3)
 * 21	Type of processed data in this GRIB message (See Table 1.4)
 * 22-N	Reserved
 *
 * @author Johann Sorel
 */
public class SectionIdentification implements Section {

    public short center;
    public short subcenter;
    public int masterTableVersion;
    public int localTableVersion;
    public int timeSignificance;
    public short year;
    public int month;
    public int day;
    public int hour;
    public int minute;
    public int second;
    public int status;
    public int type;
    public byte[] reserved;

    @Override
    public String toString() {
        return "SectionIdentification{" + "center=" + center + ", subcenter=" + subcenter + ", masterTableVersion=" + masterTableVersion + ", localTableVersion=" + localTableVersion + ", timeSignificance=" + timeSignificance + ", year=" + year + ", month=" + month + ", day=" + day + ", hour=" + hour + ", minute=" + minute + ", second=" + second + ", status=" + status + ", type=" + type + '}';
    }

}
