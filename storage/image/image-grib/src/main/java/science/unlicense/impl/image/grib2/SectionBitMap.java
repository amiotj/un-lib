
package science.unlicense.impl.image.grib2;

/**
 *
 * 1-4	Length of the section in octets (nn)
 * 5	Number of the section (6)
 * 6	Bit-map indicator (See Table 6.0) (See note 1 below)
 * 7-nn	Bit-map
 *
 * @author Johann Sorel
 */
public class SectionBitMap implements Section {

    public int bitmapIndicator;
    public byte[] bitmap;

    @Override
    public String toString() {
        return "SectionBitMap{" + "bitmapIndicator=" + bitmapIndicator + '}';
    }

}
