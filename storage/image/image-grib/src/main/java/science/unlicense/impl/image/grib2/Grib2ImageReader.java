
package science.unlicense.impl.image.grib2;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;

/**
 * GRIB 2 Image Reader
 *
 * @author Johann Sorel
 */
public class Grib2ImageReader extends AbstractImageReader{

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
