
package science.unlicense.impl.image.grib2;

/**
 *
 * 1-4	Length of the section in octets (N)
 * 5	Number of the section (2)
 * 6-N	Local Use
 *
 * @author Johann Sorel
 */
public class SectionLocalUse implements Section {

    public byte[] localUse;

    @Override
    public String toString() {
        return "SectionLocalUse{" + '}';
    }

}
