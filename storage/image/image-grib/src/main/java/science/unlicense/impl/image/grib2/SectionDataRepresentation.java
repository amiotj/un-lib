
package science.unlicense.impl.image.grib2;

/**
 *
 * 1-4	Length of the section in octets (nn)
 * 5	Number of the section (5)
 * 6-9	Number of data points where one or more values are specified in Section 7 when a bit map is present,
 *      total number of data points when a bit map is absent.
 * 10-11	Data representation template number (See Table 5.0)
 * 12-nn	Data representation template (See Template 5.X, where X is the number given in octets 10-11)
 *
 * @author Johann Sorel
 */
public class SectionDataRepresentation implements Section{

    public int nbDataPoint;
    public short templateNumber;
    public byte[] template;

    @Override
    public String toString() {
        return "SectionDataRepresentation{" + "nbDataPoint=" + nbDataPoint + ", templateNumber=" + templateNumber + '}';
    }

}
