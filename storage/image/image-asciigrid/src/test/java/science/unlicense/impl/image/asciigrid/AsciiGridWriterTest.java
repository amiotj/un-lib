
package science.unlicense.impl.image.asciigrid;

import science.unlicense.impl.image.asciigrid.AsciiGridImageWriter;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridWriterTest {

    @Test
    public void writeTest() throws IOException{
        
        final Image image = Images.createCustomBand(new Extent.Long(2, 2), 1, Primitive.TYPE_INT);
        image.getDataBuffer().cursorInt().write(new int[]{0,1,2,3});
        
        final AsciiGridImageWriter writer = new AsciiGridImageWriter();
        final ArrayOutputStream out = new ArrayOutputStream();
        writer.setOutput(out);        
        writer.write(image, null);
        writer.dispose();
        
        final Chars ascii = new Chars(out.getBuffer().toArrayByte());
        
        Assert.assertEquals(new Chars(
                "NCOLS 2\n"
              + "NROWS 2\n"
              + "XLLCORNER 0\n"
              + "YLLCORNER 0\n"
              + "CELLSIZE 1\n"
              + "NODATA_VALUE -9999\n"
              + "0 1 \n"
              + "2 3 \n",CharEncodings.US_ASCII), ascii);
    }
    
}
