
package science.unlicense.impl.image.asciigrid;

import science.unlicense.impl.image.asciigrid.AsciiGridImageReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridImageReaderTest {

    @Test
    public void readTest() throws IOException{
        
        final AsciiGridImageReader reader = new AsciiGridImageReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/imagery/asciigrid/image.txt")));
        
        final Image image = reader.read(null);
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));
        
        final int[] data = image.getDataBuffer().toIntArray();
        Assert.assertArrayEquals(new int[]{0,1,2,3},data);
        
    }
    
}
