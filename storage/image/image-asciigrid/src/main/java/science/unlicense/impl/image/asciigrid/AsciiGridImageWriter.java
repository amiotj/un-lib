
package science.unlicense.impl.image.asciigrid;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.AbstractImageWriter;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageWriteParameters;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class AsciiGridImageWriter extends AbstractImageWriter{

    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {
        
        final CharOutputStream cs = new CharOutputStream(stream,CharEncodings.US_ASCII);        
        final int ncols = (int) image.getExtent().getL(0);
        final int nrows = (int) image.getExtent().getL(1);
        
        //write header
        cs.write(AsciiGridConstants.NCOLS).write(' ').write(Int32.encode(ncols)).endLine();
        cs.write(AsciiGridConstants.NROWS).write(' ').write(Int32.encode(nrows)).endLine();
        cs.write(AsciiGridConstants.XLLCORNER).write(' ').write('0').endLine();
        cs.write(AsciiGridConstants.YLLCORNER).write(' ').write('0').endLine();
        cs.write(AsciiGridConstants.CELLSIZE).write(' ').write('1').endLine();
        cs.write(AsciiGridConstants.NODATA_VALUE).write(new Chars(" -9999")).endLine();
        
        //write datas
        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final int[] sample = new int[sm.getSampleCount()];
        final int[] coord = new int[2];
        for(coord[1]=0;coord[1]<nrows;coord[1]++){
            for(coord[0]=0;coord[0]<ncols;coord[0]++){
                sm.getTupleInt(coord, sample);
                cs.write(Int32.encode(sample[0])).write(' ');
            }
            cs.endLine();
        }
        
    }

}
