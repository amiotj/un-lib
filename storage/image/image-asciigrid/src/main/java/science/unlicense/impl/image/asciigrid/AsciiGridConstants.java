
package science.unlicense.impl.image.asciigrid;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;

/**
 * AsciiGrid constants.
 * 
 * @author Johann Sorel
 */
public final class AsciiGridConstants {

    public static final Chars NCOLS     = new Chars("NCOLS",CharEncodings.US_ASCII);
    public static final Chars NROWS     = new Chars("NROWS",CharEncodings.US_ASCII);
    public static final Chars XLLCENTER = new Chars("XLLCENTER",CharEncodings.US_ASCII);
    public static final Chars YLLCENTER = new Chars("YLLCENTER",CharEncodings.US_ASCII);
    public static final Chars XLLCORNER = new Chars("XLLCORNER",CharEncodings.US_ASCII);
    public static final Chars YLLCORNER = new Chars("YLLCORNER",CharEncodings.US_ASCII);
    public static final Chars CELLSIZE  = new Chars("CELLSIZE",CharEncodings.US_ASCII);
    public static final Chars NODATA_VALUE = new Chars("NODATA_VALUE",CharEncodings.US_ASCII);
    
    private AsciiGridConstants(){}
    
}
