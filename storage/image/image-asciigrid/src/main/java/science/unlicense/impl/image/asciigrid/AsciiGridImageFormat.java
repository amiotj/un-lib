
package science.unlicense.impl.image.asciigrid;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * Ascii grid image format.
 * 
 * References : 
 * http://daac.ornl.gov/MODIS/ASCII_Grid_Format_Description.html
 * http://resources.arcgis.com/en/help/main/10.1/index.html#//009t0000000z000000
 * 
 * @author Johann Sorel
 */
public class AsciiGridImageFormat extends AbstractImageFormat{

    public AsciiGridImageFormat() {
        super(new Chars("asciigrid"),
              new Chars("ASCIIGRID"),
              new Chars("AsciiGrid"),
              new Chars[]{
              },
              new Chars[]{
              },
              new byte[][]{
                  AsciiGridConstants.NCOLS.toBytes(),
                  AsciiGridConstants.NCOLS.toLowerCase().toBytes(),
                  AsciiGridConstants.NROWS.toBytes(),
                  AsciiGridConstants.NROWS.toLowerCase().toBytes(),
                });
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return true;
    }

    public ImageReader createReader() {
        return new AsciiGridImageReader();
    }

    public ImageWriter createWriter() {
        return new AsciiGridImageWriter();
    }
}
