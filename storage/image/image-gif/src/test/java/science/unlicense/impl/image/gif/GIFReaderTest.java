
package science.unlicense.impl.image.gif;

import science.unlicense.impl.image.gif.GIFImageReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GIFReaderTest {

    @Test
    public void testRead2x2() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/gif/sample.gif")).createInputStream();

        final ImageReader reader = new GIFImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

    @Test
    public void testReader4x2() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/gif/sample4x2.gif")).createInputStream();

        final ImageReader reader = new GIFImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in RGB

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //WHITE
        sm.getTuple(new int[]{2,0},storage);
        Assert.assertEquals(new Color(255, 255, 255, 255), cm.toColor(storage));
        //YELLOW
        sm.getTuple(new int[]{3,0},storage);
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));
        //CYAN
        sm.getTuple(new int[]{2,1},storage);
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(storage));
        //GRAY
        sm.getTuple(new int[]{3,1},storage);
        Assert.assertEquals(new Color(100, 100, 100, 255), cm.toColor(storage));

    }

}
