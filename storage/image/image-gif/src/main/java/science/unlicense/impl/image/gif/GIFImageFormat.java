
package science.unlicense.impl.image.gif;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class GIFImageFormat extends AbstractImageFormat{

    public GIFImageFormat() {
        super(new Chars("gif"),
              new Chars("GIF"),
              new Chars("Graphics Interchange Format"),
              new Chars[]{
                  new Chars("image/gif")
              },
              new Chars[]{
                new Chars("gif")
              },
              new byte[][]{GIFMetaData.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new GIFImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}