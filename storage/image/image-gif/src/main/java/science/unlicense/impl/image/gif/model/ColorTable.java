
package science.unlicense.impl.image.gif.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import science.unlicense.api.image.color.ColorIndex;
import science.unlicense.api.image.color.IndexedColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.io.DataOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class ColorTable {

    public int[] colors;

    public IndexedColorModel toColorModel(){
        final Color[] palette = new Color[colors.length];
        for(int i=0;i<colors.length;i++){
            palette[i] = new Color(colors[i]);
        }
        return new IndexedColorModel(RawModel.TYPE_UBYTE, new ColorIndex(palette));
    }
    
    public void read(DataInputStream ds, int globalColorTableSize) throws IOException {
        colors = new int[(int)Math.pow(2, globalColorTableSize+1)];
        for(int i=0;i<colors.length;i++){
            final int red = ds.readUByte();
            final int green = ds.readUByte();
            final int blue = ds.readUByte();
            colors[i] = Colors.toARGB(red, green, blue);
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        int[] rgba = new int[4];
        for(int i=0;i<colors.length;i++){
            Colors.toRGBA(colors[i], rgba);
            ds.writeUByte(rgba[0]);
            ds.writeUByte(rgba[1]);
            ds.writeUByte(rgba[2]);
        }
    }

}