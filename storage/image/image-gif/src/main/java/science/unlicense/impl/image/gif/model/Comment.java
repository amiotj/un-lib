
package science.unlicense.impl.image.gif.model;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.TypedNode;
import static science.unlicense.impl.image.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class Comment extends Extension {

    public Chars comment;
    
    public void read(DataInputStream ds) throws IOException{
        //read sub blocks
        final ByteSequence buffer = new ByteSequence();
        for(int bs = ds.readUByte(); bs!=0; bs=ds.readUByte()){
            buffer.put(ds.readFully(new byte[bs]));
        }
        comment = new Chars(buffer.toArrayByte());
    }

    public void write(DataOutputStream ds) throws IOException {
        final byte[] dataBytes = comment.toBytes();
        int index = 0;
        while(index<dataBytes.length){
            final int nb = Maths.min(dataBytes.length-index,256);
            ds.write(dataBytes, index, nb);
            index+=nb;
        }
        ds.writeUByte(0);
    }
    
    public TypedNode toNode(){
        final TypedNode node = new DefaultTypedNode(MD_GIF_COMMENT);
        node.getOrCreateChild(MD_GIF_COMMENT_TEXT).setValue(comment);
        return node;
    }
    
}