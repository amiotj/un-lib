package science.unlicense.impl.image.gif;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultNodeCardinality;
import science.unlicense.api.model.tree.DefaultNodeType;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.NodeType;

/**
 *
 * @author Johann Sorel
 */
public final class GIFMetaData {

    /** 3 first bytes of the file */
    public static final byte[] SIGNATURE = new byte[]{'G','I','F'};
    public static final byte[] V_87a = new byte[]{'8','7','a'};
    public static final byte[] V_89a = new byte[]{'8','9','a'};

    public static final int BLOCK_IMAGE_DESC = 0x2C;
    public static final int BLOCK_EXT = 0x21;
    public static final int BLOCK_EXT_GRAPHIC_CONTROL = 0xF9;
    public static final int BLOCK_EXT_COMMENT = 0xFE;
    public static final int BLOCK_EXT_PLAIN_TEXT = 0x01;
    public static final int BLOCK_EXT_APPLICATION = 0xFF;
    public static final int BLOCK_TRAILER = 0x3B;

    public static final NodeType MD_GIF;
    public static final NodeCardinality MD_GIF_HEADER;
    public static final NodeCardinality MD_GIF_HEADER_VERSION           = new DefaultNodeCardinality(new Chars("version"),null, null,String.class, false, null);

    public static final NodeCardinality MD_GIF_SCREEN;
    public static final NodeCardinality MD_GIF_SCREEN_WIDTH                    = new DefaultNodeCardinality(new Chars("width"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_HEIGHT                   = new DefaultNodeCardinality(new Chars("height"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_GLOBALCOLORTABLE         = new DefaultNodeCardinality(new Chars("gloabalColorTable"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_COLORRESOLUTION          = new DefaultNodeCardinality(new Chars("colorResolution"),null, null,Byte.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_SORT                     = new DefaultNodeCardinality(new Chars("sort"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_GLOBALCOLORTABLESIZE     = new DefaultNodeCardinality(new Chars("gloabalColorTableSize"),null, null,Byte.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_BGCOlORINDEX             = new DefaultNodeCardinality(new Chars("bgColorIndex"),null, null,Short.class, false, null);
    public static final NodeCardinality MD_GIF_SCREEN_PIXELASPECTRATIO         = new DefaultNodeCardinality(new Chars("pixelAspectRatio"),null, null,Short.class, false, null);

    public static final NodeCardinality MD_GIF_IMAGE;
    public static final NodeCardinality MD_GIF_IMAGE_LEFTPOSITION       = new DefaultNodeCardinality(new Chars("leftPosition"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_TOPPOSITION        = new DefaultNodeCardinality(new Chars("topPosition"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_WIDTH              = new DefaultNodeCardinality(new Chars("width"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_HEIGHT             = new DefaultNodeCardinality(new Chars("height"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_LOCALCOLORTABLE    = new DefaultNodeCardinality(new Chars("localColorTable"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_INTERLACED         = new DefaultNodeCardinality(new Chars("interlaced"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_SORT               = new DefaultNodeCardinality(new Chars("sort"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_RESERVED           = new DefaultNodeCardinality(new Chars("reserved"),null, null,Byte.class, false, null);
    public static final NodeCardinality MD_GIF_IMAGE_LOCALCOLORTABLESIZE= new DefaultNodeCardinality(new Chars("localColorTableSize"),null, null,Byte.class, false, null);

    public static final NodeCardinality MD_GIF_OFFSET                   = new DefaultNodeCardinality(new Chars("offset"),null, null,Integer.class, false, null);

    public static final NodeCardinality MD_GIF_EXT;
    public static final NodeCardinality MD_GIF_EXT_SIZE                 = new DefaultNodeCardinality(new Chars("size"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_EXT_LABEL                = new DefaultNodeCardinality(new Chars("label"),null, null,Chars.class, false, null);

    public static final NodeCardinality MD_GIF_COMMENT;
    public static final NodeCardinality MD_GIF_COMMENT_TEXT             = new DefaultNodeCardinality(new Chars("comment"),null, null,Chars.class, false, null);

    public static final NodeCardinality MD_GIF_APP;
    public static final NodeCardinality MD_GIF_APP_IDENTIFIER           = new DefaultNodeCardinality(new Chars("appIdentifier"),null, null,Chars.class, false, null);
    public static final NodeCardinality MD_GIF_APP_AUTHCODE             = new DefaultNodeCardinality(new Chars("appAuthenticationCode"),null, null,Chars.class, false, null);
    public static final NodeCardinality MD_GIF_APP_DATA                 = new DefaultNodeCardinality(new Chars("data"),null, null,Chars.class, false, null);

    public static final NodeCardinality MD_GIF_PLAINTEXT;
    public static final NodeCardinality MD_GIF_PLAINTEXT_GLP            = new DefaultNodeCardinality(new Chars("textGridLeftPosition"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_GTP            = new DefaultNodeCardinality(new Chars("textGridTopPosition"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_GW             = new DefaultNodeCardinality(new Chars("textGridWidth"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_GH             = new DefaultNodeCardinality(new Chars("textGridHeight"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_CW             = new DefaultNodeCardinality(new Chars("charCellWidth"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_CH             = new DefaultNodeCardinality(new Chars("charCellHeight"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_FC             = new DefaultNodeCardinality(new Chars("textForegroundColor"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_BC             = new DefaultNodeCardinality(new Chars("textBackgroundColor"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_PLAINTEXT_TEXT           = new DefaultNodeCardinality(new Chars("text"),null, null,Chars.class, false, null);

    public static final NodeCardinality MD_GIF_GRAPHICCONTROL;
    public static final NodeCardinality MD_GIF_GRAPHICCONTROL_RESERVED   = new DefaultNodeCardinality(new Chars("reversed"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_GRAPHICCONTROL_DISPMETHOD = new DefaultNodeCardinality(new Chars("disposalMethod"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_GRAPHICCONTROL_USERINPUTF = new DefaultNodeCardinality(new Chars("userInputFlag"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_GRAPHICCONTROL_TRSCOLORF  = new DefaultNodeCardinality(new Chars("transparentColorFlag"),null, null,Boolean.class, false, null);
    public static final NodeCardinality MD_GIF_GRAPHICCONTROL_DELAYTIME  = new DefaultNodeCardinality(new Chars("delayTime"),null, null,Integer.class, false, null);
    public static final NodeCardinality MD_GIF_GRAPHICCONTROL_TRSCOLOR   = new DefaultNodeCardinality(new Chars("transparentColorIndex"),null, null,Integer.class, false, null);


    static {
        MD_GIF_EXT = new DefaultNodeCardinality(new Chars("extension"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_OFFSET,
            MD_GIF_EXT_SIZE,
            MD_GIF_EXT_LABEL
        });

        MD_GIF_COMMENT = new DefaultNodeCardinality(new Chars("comment"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_OFFSET,
            MD_GIF_EXT_SIZE,
            MD_GIF_EXT_LABEL,
            MD_GIF_COMMENT_TEXT
        });

        MD_GIF_APP = new DefaultNodeCardinality(new Chars("application"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_OFFSET,
            MD_GIF_EXT_SIZE,
            MD_GIF_EXT_LABEL,
            MD_GIF_APP_IDENTIFIER,
            MD_GIF_APP_AUTHCODE,
            MD_GIF_APP_DATA
        });

        MD_GIF_PLAINTEXT = new DefaultNodeCardinality(new Chars("plaintext"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_OFFSET,
            MD_GIF_EXT_SIZE,
            MD_GIF_EXT_LABEL,
            MD_GIF_PLAINTEXT_GLP,
            MD_GIF_PLAINTEXT_GTP,
            MD_GIF_PLAINTEXT_GW,
            MD_GIF_PLAINTEXT_GH,
            MD_GIF_PLAINTEXT_CW,
            MD_GIF_PLAINTEXT_CH,
            MD_GIF_PLAINTEXT_FC,
            MD_GIF_PLAINTEXT_BC
        });

        MD_GIF_GRAPHICCONTROL = new DefaultNodeCardinality(new Chars("graphiccontrol"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_OFFSET,
            MD_GIF_EXT_SIZE,
            MD_GIF_EXT_LABEL,
            MD_GIF_GRAPHICCONTROL_RESERVED,
            MD_GIF_GRAPHICCONTROL_DISPMETHOD,
            MD_GIF_GRAPHICCONTROL_USERINPUTF,
            MD_GIF_GRAPHICCONTROL_TRSCOLORF,
            MD_GIF_GRAPHICCONTROL_DELAYTIME,
            MD_GIF_GRAPHICCONTROL_TRSCOLOR
        });

        MD_GIF_IMAGE = new DefaultNodeCardinality(new Chars("image"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_IMAGE_LEFTPOSITION,
            MD_GIF_IMAGE_TOPPOSITION,
            MD_GIF_IMAGE_WIDTH,
            MD_GIF_IMAGE_HEIGHT,
            MD_GIF_IMAGE_LOCALCOLORTABLE,
            MD_GIF_IMAGE_INTERLACED,
            MD_GIF_IMAGE_SORT,
            MD_GIF_IMAGE_RESERVED,
            MD_GIF_IMAGE_LOCALCOLORTABLESIZE,
            //extensions
            MD_GIF_COMMENT,
            MD_GIF_APP,
            MD_GIF_PLAINTEXT,
            MD_GIF_GRAPHICCONTROL
        });

        MD_GIF_SCREEN = new DefaultNodeCardinality(new Chars("screen"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_SCREEN_WIDTH,
            MD_GIF_SCREEN_HEIGHT,
            MD_GIF_SCREEN_GLOBALCOLORTABLE,
            MD_GIF_SCREEN_COLORRESOLUTION,
            MD_GIF_SCREEN_SORT,
            MD_GIF_SCREEN_GLOBALCOLORTABLESIZE,
            MD_GIF_SCREEN_BGCOlORINDEX,
            MD_GIF_SCREEN_PIXELASPECTRATIO,
            MD_GIF_IMAGE
        });

        MD_GIF_HEADER = new DefaultNodeCardinality(new Chars("header"),null, null,Sequence.class, null, false, 0, Integer.MAX_VALUE, new NodeCardinality[]{
            MD_GIF_HEADER_VERSION
        });

        MD_GIF = new DefaultNodeType(new Chars("gif"),null, null,Sequence.class, false, new NodeCardinality[]{
            MD_GIF_HEADER,
            MD_GIF_SCREEN,
            MD_GIF_IMAGE
        });

    }

    private GIFMetaData(){}

}