
package science.unlicense.impl.image.gif;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaCapabilities;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class GIFMediaFormat extends AbstractMediaFormat{

    public static final GIFMediaFormat INSTANCE = new GIFMediaFormat();
    
    public GIFMediaFormat() {
        super(new Chars("gif"),
              new Chars("GIF"),
              new Chars("Graphics Interchange Format"),
              new Chars[]{
                  new Chars("image/gif")
              },
              new Chars[]{
                new Chars("gif")
              },
              new byte[][]{GIFMetaData.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public MediaStore createStore(Object input) throws IOException {
        return new GIFMediaStore((Path)input);
    }

}