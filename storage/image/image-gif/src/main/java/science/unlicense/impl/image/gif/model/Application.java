
package science.unlicense.impl.image.gif.model;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.TypedNode;
import static science.unlicense.impl.image.gif.GIFMetaData.*;

/**
 *
 * @author Johann Sorel
 */
public class Application extends Extension {

    public Chars identifier;
    public Chars authCode;
    public Chars data;
        
    public void read(DataInputStream ds) throws IOException{
        size = ds.readUByte();
        identifier = new Chars(ds.readFully(new byte[8]));
        authCode = new Chars(ds.readFully(new byte[3]));

        //read sub blocks
        final ByteSequence buffer = new ByteSequence();
        for(int bs = ds.readUByte(); bs!=0; bs=ds.readUByte()){
            buffer.put(ds.readFully(new byte[bs]));
        }
        data = new Chars(buffer.toArrayByte());
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUByte(size);
        final byte[] idBytes = identifier.toBytes();
        if(idBytes.length!=8) throw new IOException("Identifier byte size must be 8");
        ds.write(idBytes);
        final byte[] authBytes = identifier.toBytes();
        if(authBytes.length!=3) throw new IOException("AuthCode byte size must be 3");
        ds.write(authBytes);
        
        final byte[] dataBytes = data.toBytes();
        int index = 0;
        while(index<dataBytes.length){
            final int nb = Maths.min(dataBytes.length-index,256);
            ds.write(dataBytes, index, nb);
            index+=nb;
        }
        ds.writeUByte(0);
    }

    public TypedNode toNode(){
        final TypedNode node = new DefaultTypedNode(MD_GIF_APP);
        node.getOrCreateChild(MD_GIF_APP_IDENTIFIER).setValue(identifier);
        node.getOrCreateChild(MD_GIF_APP_AUTHCODE).setValue(authCode);
        node.getOrCreateChild(MD_GIF_APP_DATA).setValue(data);
        return node;
    }
    
}