
package science.unlicense.impl.image.gif;

import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * Loops on data block until terminator is reached.
 *
 * @author Johann Sorel
 */
public class BlockStream extends AbstractInputStream{

    private final DataInputStream stream;
    private int counter = 0;

    public BlockStream(DataInputStream stream) {
        this.stream = stream;
    }

    public int read() throws IOException {
        if(counter==0){
            //begining of a block
            int b = stream.readUByte();
            if(b==0){
                //we have finish
                return -1;
            }else{
                counter = b;
            }
        }

        counter--;
        return stream.readUByte();
    }

    public void close() throws IOException {}

}
