
package science.unlicense.impl.image.gif;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.media.DefaultImagePacket;
import science.unlicense.api.media.ImagePacket;
import science.unlicense.impl.image.gif.model.GraphicControl;

/**
 *
 * @author Johann Sorel
 */
public class GIFVideoReader implements MediaReadStream {

    private final GIFImageReader reader;
    private final int nbFrame;
    private final int frameStep;
    private Image workspace;
    private int currentImageIndex = -1;

    private long currentTime = 0;

    public GIFVideoReader(GIFImageReader reader, int nbFrame) throws IOException {
        this.reader = reader;
        this.nbFrame = nbFrame;
        //frame rate by default 10 FPS
        this.frameStep = 1000/10;

        //force loading metas.
        reader.getMetadataNames();

    }

    public ImagePacket next() throws IOException {
        if(currentImageIndex < nbFrame-1){
            currentImageIndex++;

            final GIFImage gifi = (GIFImage) reader.getImageDescs().get(currentImageIndex);

            long frameStep = this.frameStep;
            //search for a graphiccontrol which define the delay
            for(int i=0,n=gifi.extensions.getSize();i<n;i++){
                Object ext = gifi.extensions.get(i);
                if(ext instanceof GraphicControl){
                    //in 1/100 of second
                    if(((GraphicControl)ext).delayTime>0){
                        frameStep = ((GraphicControl)ext).delayTime*10;
                    }
                }
            }

            if(workspace != null){
                workspace = gifi.fillImage(reader.getSubStream(), workspace,null);
            }else{
                workspace = gifi.toImage(reader.getSubStream(),null);
            }
            final Image image = Images.copy(workspace);
            final double time = currentTime;

            currentTime += frameStep;
            return new DefaultImagePacket(null, (long) time, frameStep, image);
            
        }
        return null;
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
