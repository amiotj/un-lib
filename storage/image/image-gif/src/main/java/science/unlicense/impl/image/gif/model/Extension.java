
package science.unlicense.impl.image.gif.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.TypedNode;

/**
 * An extension block.
 *
 * @author Johann Sorel
 */
public class Extension extends Chunk {

    protected Chars label;

    public Chars getLabel(){
        return label;
    }

    public void setLabel(Chars label){
        this.label = label;
    }

    public void read(DataInputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public TypedNode toNode() {
        throw new UnimplementedException("Not supported yet.");
    }

}
