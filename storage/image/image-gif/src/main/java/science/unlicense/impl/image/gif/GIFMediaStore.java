
package science.unlicense.impl.image.gif;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.media.ImageStreamMeta;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.image.ImageSetMetadata;

/**
 * View a GIF as a video media.
 *
 * @author Johann Sorel
 */
public class GIFMediaStore extends AbstractMediaStore{

    private final Path path;

    //caches
    private ImageStreamMeta videoMeta = null;

    public GIFMediaStore(Path path) {
        super(GIFMediaFormat.INSTANCE,path);
        this.path = path;
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        if(videoMeta == null){
            final GIFImageReader reader = new GIFImageReader();
            reader.setInput(path);
            final ImageSetMetadata metas = (ImageSetMetadata) reader.getMetadata(new Chars("imageset"));
            final ImageSetMetadata.Image imageMeta = (ImageSetMetadata.Image) metas.getChild(ImageSetMetadata.MD_IMAGE.getId());
            final int nbFrame = metas.getChildren().getSize();
            final int sizeX = ((ImageSetMetadata.Dimension)imageMeta.getDimensions().get(0)).geExtent();
            final int sizeY = ((ImageSetMetadata.Dimension)imageMeta.getDimensions().get(1)).geExtent();

            videoMeta = new ImageStreamMeta() {

                public TypedNode getImageMetadata() {
                    return imageMeta;
                }

                public int getNbFrames() {
                    return nbFrame;
                }

                public int getFrameRate() {
                    return 0;
                }

                public Chars getName() {
                    return new Chars("video");
                }
            };

        }
        return new MediaStreamMeta[]{videoMeta};
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        getStreamsMeta();
        final GIFImageReader reader = new GIFImageReader();
        reader.setInput(path);
        return new GIFVideoReader(reader, videoMeta.getNbFrames());
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
