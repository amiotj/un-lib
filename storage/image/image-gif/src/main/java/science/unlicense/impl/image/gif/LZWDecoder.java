
package science.unlicense.impl.image.gif;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * Annexe F
 *
 * Useful explications :
 * http://www.daubnet.com/en/file-format-gif
 * http://www.starsdev.com/developer/lzw.php#intro
 *
 * @author Johann Sorel
 */
public class LZWDecoder {

    public static final int NO_REPLACE = 256;

    private final DataInputStream stream;
    private final byte[][] dico = new byte[4096][];
    private final ByteSequence result;
    private final int baseSize;
    private final int baseCodeSize;
    private final int end;
    private final int clear;
    private final int skipValue;
    //variable values while decoding
    private int dicoLength, codeSize;

    /**
     *
     * @param stream
     * @param lzwCodeSize
     * @param bank output byte bank
     * @param skipValue value to ignore while filling buffer. use NO_REPLACE if not needed.
     *         Used in GIF image to avoid overriding buffer values.
     * @throws IOException
     */
    public LZWDecoder(DataInputStream stream, int lzwCodeSize,
            ByteSequence bank, int skipValue) throws IOException {
        this.stream         = stream;
        this.result         = bank;  bank.moveTo(0);//get back to beginning of buffer
        this.skipValue      = skipValue;
        this.baseSize       = 1 << lzwCodeSize;
        this.baseCodeSize   = lzwCodeSize +1;
        this.codeSize       = baseCodeSize;
        this.clear          = baseSize;
        this.end            = baseSize + 1;
        this.dicoLength     = baseSize + 2;

        //rebuild dictionnary
        for(int i=0;i<baseSize;i++){
            dico[i] = new byte[]{(byte)i};
        }
    }

    /**
     * Put values in buffer. checking for skipValue.
     * @param values
     */
    private void putValue(byte[] values){
        if(skipValue == NO_REPLACE){
            result.put(values);
        }else{
            for(int i=0;i<values.length;i++){
                if(values[i]==skipValue){
                    result.skip();
                }else{
                    result.put(values[i]);
                }
            }
        }
    }

    private int nextCode() throws IOException{
        return stream.readBits(codeSize, DataInputStream.LSB);
    }

    public ByteSequence decode() throws IOException{
        int old = -1; // -1 for no previous code
        int code;
        while(true){
            code = nextCode();

            if(code == clear){
                //reset dictionnary
                dicoLength = baseSize+2;
                codeSize = baseCodeSize;
                old = -1;
            }else if (code == end){
                break;
            }else if (old == -1) {
                putValue(dico[code]);
                old = code;
            }else{
                if(code < dicoLength){
                    final byte k = dico[code][0];
                    final byte[] nc = Arrays.insert(dico[old], dico[old].length, k);
                    dico[dicoLength++] = nc;
                    putValue(dico[code]);
                }else{
                    final byte k = dico[old][0];
                    final byte[] nc = Arrays.insert(dico[old], dico[old].length, k);
                    dico[dicoLength++] = nc;
                    putValue(nc);
                }
                old = code;
            }

            if(dicoLength >= (1 << codeSize) && codeSize < 12){
                codeSize++;
            }
        }

        return result;
    }

}
