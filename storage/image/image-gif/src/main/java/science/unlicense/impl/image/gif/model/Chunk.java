
package science.unlicense.impl.image.gif.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.Metadata;

/**
 *
 * @author Johann Sorel
 */
public abstract class Chunk implements Metadata{

    public int offset;
    public int size;

    public Chunk() {
    }

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;
        
}