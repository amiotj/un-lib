

package science.unlicense.impl.image.bmp;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class BMPFileHeader {

    public Chars signature;
    public int fileSize;
    public short reserved1;
    public short reserved2;
    public int offset;

    public void read(DataInputStream ds) throws IOException{
        signature = new Chars(ds.readFully(new byte[2]));
        fileSize = ds.readInt();
        reserved1 = ds.readShort();
        reserved2 = ds.readShort();
        offset = ds.readInt();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(signature.toBytes(CharEncodings.US_ASCII));
        ds.writeInt(fileSize);
        ds.writeShort(reserved1);
        ds.writeShort(reserved2);
        ds.writeInt(offset);
    }

}
