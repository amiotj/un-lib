
package science.unlicense.impl.image.bmp;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class BMPImageFormat extends AbstractImageFormat{

    public BMPImageFormat() {
        super(new Chars("bmp"),
              new Chars("BMP"),
              new Chars("Bitmap"),
              new Chars[]{
                  new Chars("image/bmp"),
                  new Chars("image/x-bmp")
              },
              new Chars[]{
                new Chars("bmp"),
                new Chars("dib")
              },
              new byte[][]{
                  BMPConstants.SIGNATURE_BM.toBytes(),
                  BMPConstants.SIGNATURE_BA.toBytes(),
                  BMPConstants.SIGNATURE_CI.toBytes(),
                  BMPConstants.SIGNATURE_CP.toBytes(),
                  BMPConstants.SIGNATURE_IC.toBytes(),
                  BMPConstants.SIGNATURE_PT.toBytes(),
                });
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return true;
    }

    public ImageReader createReader() {
        return new BMPImageReader();
    }

    public ImageWriter createWriter() {
        return new BMPImageWriter();
    }

}
