

package science.unlicense.impl.image.bmp;

import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.color.Colors;
import science.unlicense.api.image.AbstractImageWriter;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageWriteParameters;
import science.unlicense.api.image.color.PixelBuffer;

/**
 *
 * @author Johann Sorel
 */
public class BMPImageWriter extends AbstractImageWriter{


    protected void write(Image image, ImageWriteParameters params, ByteOutputStream stream) throws IOException {

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);

        final int datasize = width*height*4;

        //TODO handle various image cases
        final BMPInfoHeader infoHeader = new BMPInfoHeader();
        infoHeader.width = width;
        infoHeader.height = height;
        infoHeader.planes = 1;
        infoHeader.bitPerPixel = 32;
        infoHeader.dibSize = 40;
        // 40bytes DIB
        infoHeader.compression = BMPConstants.COMPRESSION_NONE;
        infoHeader.imageSize = datasize;
        infoHeader.xPixelPerMeter = 3780;
        infoHeader.yPixelPerMeter = 3780;
        infoHeader.colorsInColorTable = 0;
        infoHeader.importantColorCount = 0;

        final BMPFileHeader fileHeader = new BMPFileHeader();
        fileHeader.signature = BMPConstants.SIGNATURE_BM;
        fileHeader.reserved1 = 0;
        fileHeader.reserved2 = 0;
        fileHeader.offset = 14+infoHeader.dibSize;
        fileHeader.fileSize = 14+infoHeader.dibSize+datasize;

        //images are in BVRA starting from last line
        final byte[] array = new byte[datasize];
        int k = 0;
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        final int[] coord = new int[2];
        final int[] argb = new int[4];
        for(coord[1]=height-1;coord[1]>=0;coord[1]--){
            for(coord[0]=0;coord[0]<width;coord[0]++){
                final int color = cm.getARGB(coord);
                Colors.toARGB(color, argb);
                array[k++] = (byte)argb[3];
                array[k++] = (byte)argb[2];
                array[k++] = (byte)argb[1];
                array[k++] = (byte)argb[0];
            }
        }

        //now write everything
        final DataOutputStream ds = getOutputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        fileHeader.write(ds);
        infoHeader.write(ds);

        ds.write(array);

    }

}
