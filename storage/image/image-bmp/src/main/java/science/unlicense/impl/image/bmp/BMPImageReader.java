package science.unlicense.impl.image.bmp;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageSetMetadata;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;

/**
 * BMP image format defined here :
 * http://en.wikipedia.org/wiki/BMP_file_format
 *
 * @author Johann Sorel
 */
public class BMPImageReader extends AbstractImageReader{

    private BMPFileHeader fileHeader;
    private BMPInfoHeader header;

    /**
     * Used by ICO,CUR formats.
     * @param header
     */
    public void setHeader(BMPInfoHeader header) {
        this.header = header;
    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        //Bitmap file header ---------------------------------------------------
        fileHeader = new BMPFileHeader();
        fileHeader.read(ds);

        //DIB header -----------------------------------------------------------
        header = new BMPInfoHeader();
        header.read(ds);

        if(header.planes != 1) throw new IOException("Only handle 1 plane, found "+ header.planes);
        if (header.bitPerPixel == 1) throw new IOException("1Bit per pixel BMP not supported yet");

        final ImageSetMetadata meta = new ImageSetMetadata();
        meta.getChildren().add(new ImageSetMetadata.Image(header.width, header.height));

        final Dictionary metadatas = new HashDictionary();
        metadatas.add(meta.getType().getId(), meta);
        return metadatas;
    }

    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws IOException {
        stream.mark();
        readMetadatas(stream);
        stream.rewind();
        stream.skip(14+header.dibSize);
        return readNoSkip(params, stream);
    }

    /**
     * Used by BMP,ICO,CUR formats.
     * ICO and CUR do not contains a fileHeader.
     *
     * @param params
     * @param stream
     * @return
     * @throws science.unlicense.api.io.IOException
     */
    public Image readNoSkip(ImageReadParameters params, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        //start reading image
        if(header.bitPerPixel != 24 && header.bitPerPixel != 32){
            throw new IOException("BMP different from 24bit or 32bit RGB.A not supported yet.");
        }

        final int lineLength = (header.bitPerPixel/8)*header.width;
        final Buffer datas = params.getBufferFactory().createByte(lineLength*header.height);

        //lines must be a multiple of 4 bytes, skip any other bytes.
        int padding = lineLength % 4 ;
        if(padding != 0) padding = 4-padding;

        //line stored in reverse order
        for(int y=header.height-1; y>=0 ;y--){
            //from left to right
            ds.readFully(datas, y*lineLength, lineLength);
            if(padding != 0) ds.skip(padding);
        }

        //images are in BVR starting from last line
        final RawModel sm;
        final ColorModel cm;

        if(header.bitPerPixel == 24){
            sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
            cm = new DirectColorModel(sm, new int[]{2,1,0,-1},false);
        }else{
            sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
            cm = new DirectColorModel(sm,new int[]{2,1,0,3},false);
        }

        return new DefaultImage(datas, new Extent.Long(header.width, header.height), sm, cm);
    }

}
