package science.unlicense.impl.image.bmp;

import science.unlicense.impl.image.bmp.BMPImageReader;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class BMPReaderTest {

    @Test
    public void testSample() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/Sample.bmp")).createInputStream();

        final ImageReader reader = new BMPImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in BGR
        Assert.assertArrayEquals(new int[]{0,0,255}, b00); //RED
        Assert.assertArrayEquals(new int[]{0,255,255}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{255,255,0}, b01); //CYAN
        Assert.assertArrayEquals(new int[]{255,0,0}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

    @Test
    public void testSampleRGBA() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/SampleRGBA.bmp")).createInputStream();

        final ImageReader reader = new BMPImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));;

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in BGRA
        Assert.assertArrayEquals(new int[]{167,174, 26,175}, b00);
        Assert.assertArrayEquals(new int[]{156,163, 50,137}, b10);
        Assert.assertArrayEquals(new int[]{157,163, 48,143}, b01);
        Assert.assertArrayEquals(new int[]{142,148, 80,111}, b11);

        Assert.assertEquals(new Color( 26, 174, 167, 175), cm.toColor(b00));
        Assert.assertEquals(new Color( 50, 163, 156, 137), cm.toColor(b10));
        Assert.assertEquals(new Color( 48, 163, 157, 143), cm.toColor(b01));
        Assert.assertEquals(new Color( 80, 148, 142, 111), cm.toColor(b11));

    }

    //TODO
    @Ignore
    @Test
    public void testSampleRGBX() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/SampleRGBX.bmp")).createInputStream();

        final ImageReader reader = new BMPImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in BGRA
        Assert.assertArrayEquals(new int[]{167,174, 26,175}, b00);
        Assert.assertArrayEquals(new int[]{156,163, 50,137}, b10);
        Assert.assertArrayEquals(new int[]{157,163, 48,143}, b01);
        Assert.assertArrayEquals(new int[]{142,148, 80,111}, b11);

        Assert.assertEquals(new Color( 26, 174, 167, 175), cm.toColor(b00));
        Assert.assertEquals(new Color( 50, 163, 156, 137), cm.toColor(b10));
        Assert.assertEquals(new Color( 48, 163, 157, 143), cm.toColor(b01));
        Assert.assertEquals(new Color( 80, 148, 142, 111), cm.toColor(b11));

    }
}
