
package science.unlicense.impl.image.bmp;

import science.unlicense.impl.image.bmp.BMPImageFormat;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.image.ImageFormat;
import science.unlicense.api.image.Images;

/**
 *
 * @author Johann Sorel
 */
public class BMPImageFormatTest {
    
    @Test
    public void testRegistered() throws Exception{
        final ImageFormat[] formats = Images.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof BMPImageFormat){
                //ok, found it
                return;
            }
        }
        
        Assert.fail("Image format not found.");
    }
    
}
