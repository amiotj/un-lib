

package science.unlicense.impl.image.bmp;

import science.unlicense.impl.image.bmp.BMPImageWriter;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageWriter;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class BMPWriterTest {

    @Test
    public void writeRGBATest() throws IOException{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/bmp/SampleRGBA.bmp")).createInputStream();
        final byte[] expected = IOUtilities.readAll(bs);

        final ArrayOutputStream out = new ArrayOutputStream();
        final ImageWriter writer = new BMPImageWriter();
        writer.setOutput(out);

        final Image image = Images.create(new Extent.Long(2, 2),Images.IMAGE_TYPE_RGBA);
        final PixelBuffer sm = image.getColorModel().asTupleBuffer(image);
        sm.setColor(new int[]{0,0}, new Color(26, 174, 167, 175));
        sm.setColor(new int[]{1,0}, new Color(50, 163, 156, 137));
        sm.setColor(new int[]{0,1}, new Color(48, 163, 157, 143));
        sm.setColor(new int[]{1,1}, new Color(80, 148, 142, 111));

        writer.write(image, null);
        final byte[] result = out.getBuffer().toArrayByte();

        Assert.assertArrayEquals(expected, result);

    }

}
