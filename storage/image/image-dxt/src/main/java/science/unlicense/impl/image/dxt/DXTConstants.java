
package science.unlicense.impl.image.dxt;

import science.unlicense.api.character.Chars;


/**
 *
 * Full documentation at :
 * http://msdn.microsoft.com/en-us/library/bb943982.aspx
 * http://msdn.microsoft.com/en-us/library/bb173059.aspx
 *
 * compressions :
 * DXT1 http://msdn.microsoft.com/en-us/library/bb147243(v=vs.85).aspx
 * DXT3 http://msdn.microsoft.com/en-us/library/bb206238(v=vs.85).aspx
 *
 * more interesting docs :
 * http://www.opengl.org/registry/specs/EXT/texture_compression_s3tc.txt
 *
 * DXT10/11 :
 * https://msdn.microsoft.com/en-us/library/windows/desktop/bb943983(v=vs.85).aspx
 * https://msdn.microsoft.com/en-us/library/windows/desktop/bb943991(v=vs.85).aspx
 * http://blogs.msdn.com/b/chuckw/archive/2010/02/05/the-dds-file-format-lives.aspx
 * https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx?
 *
 * D3DFMT codes :
 * https://msdn.microsoft.com/en-us/library/windows/desktop/bb172558%28v=vs.85%29.aspx
 * DXGI codes :
 * https://msdn.microsoft.com/en-us/library/windows/desktop/bb173059(v=vs.85).aspx
 *
 * @author Johann Sorel
 */
public final class DXTConstants {

    public static final byte[] SIGNATURE = new byte[]{'D','D','S',' '};

    ////////////////////////////////////////////////////////////////////////////

    /** Required */
    public static final int DDSD_CAPS = 0x1;
    /** Required */
    public static final int DDSD_HEIGHT = 0x2;
    /** Required */
    public static final int DDSD_WIDTH = 0x4;
    /** Required when pitch is provided for an uncompressed texture. */
    public static final int DDSD_PITCH = 0x8;
    /** Required */
    public static final int DDSD_PIXELFORMAT = 0x1000;
    /** Required in a mipmapped texture. */
    public static final int DDSD_MIPMAPCOUNT = 0x20000;
    /** Required when pitch is provided for a compressed texture. */
    public static final int DDSD_LINEARSIZE = 0x80000;
    /** Required in a depth texture. */
    public static final int DDSD_DEPTH = 0x800000;

    ////////////////////////////////////////////////////////////////////////////
    public static final int DDPF_ALPHAPIXELS = 0x00000001;
    public static final int DDPF_FOURCC = 0x00000004;
    public static final int DDPF_INDEXED = 0x00000020;
    public static final int DDPF_RGB = 0x00000040;

    ////////////////////////////////////////////////////////////////////////////

    /** Optional; must be used on any file that contains more than one surface
     * (a mipmap, a cubic environment map, or mipmapped volume texture). */
    public static final int DDSCAPS_COMPLEX = 0x8;
    /** Optional; should be used for a mipmap. */
    public static final int DDSCAPS_MIPMAP = 0x400000;
    /** Required */
    public static final int DDSCAPS_TEXTURE = 0x1000;

    ////////////////////////////////////////////////////////////////////////////

    /** Required for a cube map. */
    public static final int DDSCAPS2_CUBEMAP = 0x200;
    /** Required when these surfaces are stored in a cube map. */
    public static final int DDSCAPS2_CUBEMAP_POSITIVEX = 0x400;
    /** Required when these surfaces are stored in a cube map. */
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEX = 0x800;
    /** Required when these surfaces are stored in a cube map. */
    public static final int DDSCAPS2_CUBEMAP_POSITIVEY = 0x1000;
    /** Required when these surfaces are stored in a cube map. */
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEY = 0x2000;
    /** Required when these surfaces are stored in a cube map. */
    public static final int DDSCAPS2_CUBEMAP_POSITIVEZ = 0x4000;
    /** Required when these surfaces are stored in a cube map. */
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x8000;
    /** Required for a volume texture. */
    public static final int DDSCAPS2_VOLUME = 0x200000;

    ////////////////////////////////////////////////////////////////////////////

    public static final Chars D3DFMT_DXT1 = new Chars(new byte[]{'D','X','T','1'});
    public static final Chars D3DFMT_DXT2 = new Chars(new byte[]{'D','X','T','2'});
    public static final Chars D3DFMT_DXT3 = new Chars(new byte[]{'D','X','T','3'});
    public static final Chars D3DFMT_DXT4 = new Chars(new byte[]{'D','X','T','4'});
    public static final Chars D3DFMT_DXT5 = new Chars(new byte[]{'D','X','T','5'});
    public static final Chars D3DFMT_ATI1 = new Chars(new byte[]{'A','T','I','1'});
    public static final Chars D3DFMT_ATI2 = new Chars(new byte[]{'A','T','I','2'});
    public static final Chars D3DFMT_DX10 = new Chars(new byte[]{'D','X','1','0'});

    ////////////////////////////////////////////////////////////////////////////

    public static final Chars COMPRESSION_UNKOWN = new Chars("UNKOWN");
    public static final Chars COMPRESSION_BC1 = new Chars("BC1");
    public static final Chars COMPRESSION_BC2 = new Chars("BC2");
    public static final Chars COMPRESSION_BC3 = new Chars("BC3");
    public static final Chars COMPRESSION_BC4U = new Chars("BC4U");
    public static final Chars COMPRESSION_BC4S = new Chars("BC4S");
    public static final Chars COMPRESSION_BC5U = new Chars("BC5U");
    public static final Chars COMPRESSION_BC5S = new Chars("BC5S");
    public static final Chars COMPRESSION_BC6H = new Chars("BC6H");
    public static final Chars COMPRESSION_BC7 = new Chars("BC7");

    ////////////////////////////////////////////////////////////////////////////

    public static final int DXGI_FORMAT_UNKNOWN                     = 0;
    public static final int DXGI_FORMAT_R32G32B32A32_TYPELESS       = 1;
    public static final int DXGI_FORMAT_R32G32B32A32_FLOAT          = 2;
    public static final int DXGI_FORMAT_R32G32B32A32_UINT           = 3;
    public static final int DXGI_FORMAT_R32G32B32A32_SINT           = 4;
    public static final int DXGI_FORMAT_R32G32B32_TYPELESS          = 5;
    public static final int DXGI_FORMAT_R32G32B32_FLOAT             = 6;
    public static final int DXGI_FORMAT_R32G32B32_UINT              = 7;
    public static final int DXGI_FORMAT_R32G32B32_SINT              = 8;
    public static final int DXGI_FORMAT_R16G16B16A16_TYPELESS       = 9;
    public static final int DXGI_FORMAT_R16G16B16A16_FLOAT          = 10;
    public static final int DXGI_FORMAT_R16G16B16A16_UNORM          = 11;
    public static final int DXGI_FORMAT_R16G16B16A16_UINT           = 12;
    public static final int DXGI_FORMAT_R16G16B16A16_SNORM          = 13;
    public static final int DXGI_FORMAT_R16G16B16A16_SINT           = 14;
    public static final int DXGI_FORMAT_R32G32_TYPELESS             = 15;
    public static final int DXGI_FORMAT_R32G32_FLOAT                = 16;
    public static final int DXGI_FORMAT_R32G32_UINT                 = 17;
    public static final int DXGI_FORMAT_R32G32_SINT                 = 18;
    public static final int DXGI_FORMAT_R32G8X24_TYPELESS           = 19;
    public static final int DXGI_FORMAT_D32_FLOAT_S8X24_UINT        = 20, D3DFMT_R8G8B8         = 20;
    public static final int DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS    = 21, D3DFMT_A8R8G8B8       = 21;
    public static final int DXGI_FORMAT_X32_TYPELESS_G8X24_UINT     = 22, D3DFMT_X8R8G8B8       = 22;
    public static final int DXGI_FORMAT_R10G10B10A2_TYPELESS        = 23, D3DFMT_R5G6B5               = 23;
    public static final int DXGI_FORMAT_R10G10B10A2_UNORM           = 24, D3DFMT_X1R5G5B5             = 24;
    public static final int DXGI_FORMAT_R10G10B10A2_UINT            = 25, D3DFMT_A1R5G5B5             = 25;
    public static final int DXGI_FORMAT_R11G11B10_FLOAT             = 26, D3DFMT_A4R4G4B4             = 26;
    public static final int DXGI_FORMAT_R8G8B8A8_TYPELESS           = 27, D3DFMT_R3G3B2               = 27;
    public static final int DXGI_FORMAT_R8G8B8A8_UNORM              = 28, D3DFMT_A8                   = 28;
    public static final int DXGI_FORMAT_R8G8B8A8_UNORM_SRGB         = 29, D3DFMT_A8R3G3B2             = 29;
    public static final int DXGI_FORMAT_R8G8B8A8_UINT               = 30, D3DFMT_X4R4G4B4             = 30;
    public static final int DXGI_FORMAT_R8G8B8A8_SNORM              = 31, D3DFMT_A2B10G10R10          = 31;
    public static final int DXGI_FORMAT_R8G8B8A8_SINT               = 32, D3DFMT_A8B8G8R8             = 32;
    public static final int DXGI_FORMAT_R16G16_TYPELESS             = 33, D3DFMT_X8B8G8R8             = 33;
    public static final int DXGI_FORMAT_R16G16_FLOAT                = 34, D3DFMT_G16R16               = 34;
    public static final int DXGI_FORMAT_R16G16_UNORM                = 35, D3DFMT_A2R10G10B10          = 35;
    public static final int DXGI_FORMAT_R16G16_UINT                 = 36, D3DFMT_A16B16G16R16         = 36;
    public static final int DXGI_FORMAT_R16G16_SNORM                = 37;
    public static final int DXGI_FORMAT_R16G16_SINT                 = 38;
    public static final int DXGI_FORMAT_R32_TYPELESS                = 39;
    public static final int DXGI_FORMAT_D32_FLOAT                   = 40, D3DFMT_A8P8                 = 40;
    public static final int DXGI_FORMAT_R32_FLOAT                   = 41, D3DFMT_P8                   = 41;
    public static final int DXGI_FORMAT_R32_UINT                    = 42;
    public static final int DXGI_FORMAT_R32_SINT                    = 43;
    public static final int DXGI_FORMAT_R24G8_TYPELESS              = 44;
    public static final int DXGI_FORMAT_D24_UNORM_S8_UINT           = 45;
    public static final int DXGI_FORMAT_R24_UNORM_X8_TYPELESS       = 46;
    public static final int DXGI_FORMAT_X24_TYPELESS_G8_UINT        = 47;
    public static final int DXGI_FORMAT_R8G8_TYPELESS               = 48;
    public static final int DXGI_FORMAT_R8G8_UNORM                  = 49;
    public static final int DXGI_FORMAT_R8G8_UINT                   = 50, D3DFMT_L8                   = 50;
    public static final int DXGI_FORMAT_R8G8_SNORM                  = 51, D3DFMT_A8L8                 = 51;
    public static final int DXGI_FORMAT_R8G8_SINT                   = 52, D3DFMT_A4L4                 = 52;
    public static final int DXGI_FORMAT_R16_TYPELESS                = 53;
    public static final int DXGI_FORMAT_R16_FLOAT                   = 54;
    public static final int DXGI_FORMAT_D16_UNORM                   = 55;
    public static final int DXGI_FORMAT_R16_UNORM                   = 56;
    public static final int DXGI_FORMAT_R16_UINT                    = 57;
    public static final int DXGI_FORMAT_R16_SNORM                   = 58;
    public static final int DXGI_FORMAT_R16_SINT                    = 59;
    public static final int DXGI_FORMAT_R8_TYPELESS                 = 60, D3DFMT_V8U8                 = 60;
    public static final int DXGI_FORMAT_R8_UNORM                    = 61, D3DFMT_L6V5U5               = 61;
    public static final int DXGI_FORMAT_R8_UINT                     = 62, D3DFMT_X8L8V8U8             = 62;
    public static final int DXGI_FORMAT_R8_SNORM                    = 63, D3DFMT_Q8W8V8U8             = 63;
    public static final int DXGI_FORMAT_R8_SINT                     = 64, D3DFMT_V16U16               = 64;
    public static final int DXGI_FORMAT_A8_UNORM                    = 65;
    public static final int DXGI_FORMAT_R1_UNORM                    = 66;
    public static final int DXGI_FORMAT_R9G9B9E5_SHAREDEXP          = 67, D3DFMT_A2W10V10U10          = 67;
    public static final int DXGI_FORMAT_R8G8_B8G8_UNORM             = 68;
    public static final int DXGI_FORMAT_G8R8_G8B8_UNORM             = 69;
    public static final int DXGI_FORMAT_BC1_TYPELESS                = 70, D3DFMT_D16_LOCKABLE         = 70;
    public static final int DXGI_FORMAT_BC1_UNORM                   = 71, D3DFMT_D32                  = 71;
    public static final int DXGI_FORMAT_BC1_UNORM_SRGB              = 72;
    public static final int DXGI_FORMAT_BC2_TYPELESS                = 73, D3DFMT_D15S1                = 73;
    public static final int DXGI_FORMAT_BC2_UNORM                   = 74;
    public static final int DXGI_FORMAT_BC2_UNORM_SRGB              = 75, D3DFMT_D24S8                = 75;
    public static final int DXGI_FORMAT_BC3_TYPELESS                = 76;
    public static final int DXGI_FORMAT_BC3_UNORM                   = 77, D3DFMT_D24X8                = 77;
    public static final int DXGI_FORMAT_BC3_UNORM_SRGB              = 78;
    public static final int DXGI_FORMAT_BC4_TYPELESS                = 79, D3DFMT_D24X4S4              = 79;
    public static final int DXGI_FORMAT_BC4_UNORM                   = 80, D3DFMT_D16                  = 80;
    public static final int DXGI_FORMAT_BC4_SNORM                   = 81, D3DFMT_L16                  = 81;
    public static final int DXGI_FORMAT_BC5_TYPELESS                = 82, D3DFMT_D32F_LOCKABLE        = 82;
    public static final int DXGI_FORMAT_BC5_UNORM                   = 83, D3DFMT_D24FS8               = 83;
    public static final int DXGI_FORMAT_BC5_SNORM                   = 84, D3DFMT_D32_LOCKABLE         = 84;
    public static final int DXGI_FORMAT_B5G6R5_UNORM                = 85, D3DFMT_S8_LOCKABLE          = 85;
    public static final int DXGI_FORMAT_B5G5R5A1_UNORM              = 86;
    public static final int DXGI_FORMAT_B8G8R8A8_UNORM              = 87;
    public static final int DXGI_FORMAT_B8G8R8X8_UNORM              = 88;
    public static final int DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM  = 89;
    public static final int DXGI_FORMAT_B8G8R8A8_TYPELESS           = 90;
    public static final int DXGI_FORMAT_B8G8R8A8_UNORM_SRGB         = 91;
    public static final int DXGI_FORMAT_B8G8R8X8_TYPELESS           = 92;
    public static final int DXGI_FORMAT_B8G8R8X8_UNORM_SRGB         = 93;
    public static final int DXGI_FORMAT_BC6H_TYPELESS               = 94;
    public static final int DXGI_FORMAT_BC6H_UF16                   = 95;
    public static final int DXGI_FORMAT_BC6H_SF16                   = 96;
    public static final int DXGI_FORMAT_BC7_TYPELESS                = 97;
    public static final int DXGI_FORMAT_BC7_UNORM                   = 98;
    public static final int DXGI_FORMAT_BC7_UNORM_SRGB              = 99;
    public static final int DXGI_FORMAT_AYUV                        = 100, D3DFMT_VERTEXDATA           =100;
    public static final int DXGI_FORMAT_Y410                        = 101, D3DFMT_INDEX16              =101;
    public static final int DXGI_FORMAT_Y416                        = 102, D3DFMT_INDEX32              =102;
    public static final int DXGI_FORMAT_NV12                        = 103;
    public static final int DXGI_FORMAT_P010                        = 104;
    public static final int DXGI_FORMAT_P016                        = 105;
    public static final int DXGI_FORMAT_420_OPAQUE                  = 106;
    public static final int DXGI_FORMAT_YUY2                        = 107;
    public static final int DXGI_FORMAT_Y210                        = 108;
    public static final int DXGI_FORMAT_Y216                        = 109;
    public static final int DXGI_FORMAT_NV11                        = 110, D3DFMT_Q16W16V16U16         = 110;
    public static final int DXGI_FORMAT_AI44                        = 111, D3DFMT_R16F                 = 111;
    public static final int DXGI_FORMAT_IA44                        = 112, D3DFMT_G16R16F              = 112;
    public static final int DXGI_FORMAT_P8                          = 113, D3DFMT_A16B16G16R16F        = 113;
    public static final int DXGI_FORMAT_A8P8                        = 114;
    public static final int DXGI_FORMAT_B4G4R4A4_UNORM              = 115;
    public static final int DXGI_FORMAT_P208                        = 130;
    public static final int DXGI_FORMAT_V208                        = 131;
    public static final int DXGI_FORMAT_V408                        = 132;
    public static final int DXGI_FORMAT_ASTC_4X4_UNORM              = 134;
    public static final int DXGI_FORMAT_ASTC_4X4_UNORM_SRGB         = 135;
    public static final int DXGI_FORMAT_ASTC_5X4_TYPELESS           = 137;
    public static final int DXGI_FORMAT_ASTC_5X4_UNORM              = 138;
    public static final int DXGI_FORMAT_ASTC_5X4_UNORM_SRGB         = 139;
    public static final int DXGI_FORMAT_ASTC_5X5_TYPELESS           = 141;
    public static final int DXGI_FORMAT_ASTC_5X5_UNORM              = 142;
    public static final int DXGI_FORMAT_ASTC_5X5_UNORM_SRGB         = 143;
    public static final int DXGI_FORMAT_ASTC_6X5_TYPELESS           = 145;
    public static final int DXGI_FORMAT_ASTC_6X5_UNORM              = 146;
    public static final int DXGI_FORMAT_ASTC_6X5_UNORM_SRGB         = 147;
    public static final int DXGI_FORMAT_ASTC_6X6_TYPELESS           = 149;
    public static final int DXGI_FORMAT_ASTC_6X6_UNORM              = 150;
    public static final int DXGI_FORMAT_ASTC_6X6_UNORM_SRGB         = 151;
    public static final int DXGI_FORMAT_ASTC_8X5_TYPELESS           = 153;
    public static final int DXGI_FORMAT_ASTC_8X5_UNORM              = 154;
    public static final int DXGI_FORMAT_ASTC_8X5_UNORM_SRGB         = 155;
    public static final int DXGI_FORMAT_ASTC_8X6_TYPELESS           = 157;
    public static final int DXGI_FORMAT_ASTC_8X6_UNORM              = 158;
    public static final int DXGI_FORMAT_ASTC_8X6_UNORM_SRGB         = 159;
    public static final int DXGI_FORMAT_ASTC_8X8_TYPELESS           = 161;
    public static final int DXGI_FORMAT_ASTC_8X8_UNORM              = 162;
    public static final int DXGI_FORMAT_ASTC_8X8_UNORM_SRGB         = 163;
    public static final int DXGI_FORMAT_ASTC_10X5_TYPELESS          = 165;
    public static final int DXGI_FORMAT_ASTC_10X5_UNORM             = 166;
    public static final int DXGI_FORMAT_ASTC_10X5_UNORM_SRGB        = 167;
    public static final int DXGI_FORMAT_ASTC_10X6_TYPELESS          = 169;
    public static final int DXGI_FORMAT_ASTC_10X6_UNORM             = 170;
    public static final int DXGI_FORMAT_ASTC_10X6_UNORM_SRGB        = 171;
    public static final int DXGI_FORMAT_ASTC_10X8_TYPELESS          = 173;
    public static final int DXGI_FORMAT_ASTC_10X8_UNORM             = 174;
    public static final int DXGI_FORMAT_ASTC_10X8_UNORM_SRGB        = 175;
    public static final int DXGI_FORMAT_ASTC_10X10_TYPELESS         = 177;
    public static final int DXGI_FORMAT_ASTC_10X10_UNORM            = 178;
    public static final int DXGI_FORMAT_ASTC_10X10_UNORM_SRGB       = 179;
    public static final int DXGI_FORMAT_ASTC_12X10_TYPELESS         = 181;
    public static final int DXGI_FORMAT_ASTC_12X10_UNORM            = 182;
    public static final int DXGI_FORMAT_ASTC_12X10_UNORM_SRGB       = 183;
    public static final int DXGI_FORMAT_ASTC_12X12_TYPELESS         = 185;
    public static final int DXGI_FORMAT_ASTC_12X12_UNORM            = 186;
    public static final int DXGI_FORMAT_ASTC_12X12_UNORM_SRGB       = 187;
    public static final int DXGI_FORMAT_FORCE_UINT                  = 0xffffffff;

    ////////////////////////////////////////////////////////////////////////////
    // possible values in dxt10 header
    public static final int D3D10_RESOURCE_DIMENSION_UNKNOWN = 0;
    public static final int D3D10_RESOURCE_DIMENSION_BUFFER = 1;
    public static final int D3D10_RESOURCE_DIMENSION_TEXTURE1D = 2;
    public static final int D3D10_RESOURCE_DIMENSION_TEXTURE2D = 3;
    public static final int D3D10_RESOURCE_DIMENSION_TEXTURE3D = 4;

    ////////////////////////////////////////////////////////////////////////////
    public static final int DDS_RESOURCE_MISC_TEXTURECUBE = 0x04;

    ////////////////////////////////////////////////////////////////////////////
    public static final int DDS_ALPHA_MODE_UNKNOWN	 = 0x00;
    public static final int DDS_ALPHA_MODE_STRAIGHT	 = 0x01;
    public static final int DDS_ALPHA_MODE_PREMULTIPLIED = 0x02;
    public static final int DDS_ALPHA_MODE_OPAQUE	 = 0x03;
    public static final int DDS_ALPHA_MODE_CUSTOM	 = 0x04;
    
    
    private DXTConstants() {}

}
