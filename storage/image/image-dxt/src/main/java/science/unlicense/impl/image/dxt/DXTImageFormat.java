
package science.unlicense.impl.image.dxt;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * DXT/DDS image format.
 *
 * @author Johann Sorel
 */
public class DXTImageFormat extends AbstractImageFormat{

    public DXTImageFormat() {
        super(new Chars("dxt"),
              new Chars("S3TC/DXT"),
              new Chars("S3 Texture Compression"),
              new Chars[]{
                  new Chars("image/dxt")
              },
              new Chars[]{
                  new Chars("dxt"),
                  new Chars("dds")
              },
              new byte[][]{DXTConstants.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new DXTImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}