
package science.unlicense.impl.image.dxt;

import science.unlicense.api.image.DefaultImageReadParameters;

/**
 *
 * @author Johann Sorel
 */
public class DXTImageReadParameters extends DefaultImageReadParameters{
    
    private DXTHeader header;

    public DXTImageReadParameters() {
    }

    public void setHeader(DXTHeader header) {
        this.header = header;
    }

    public DXTHeader getHeader() {
        return header;
    }
    
}
