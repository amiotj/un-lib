
package science.unlicense.impl.image.dxt;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.DirectColorModel;
import static science.unlicense.impl.image.dxt.DXTConstants.*;
import science.unlicense.impl.image.craw.dxt.DXTRawModel;
import science.unlicense.impl.image.craw.bc.BCRawModel;

/**
 * Reader for DDS image format.
 *
 * @author Johann Sorel
 */
public class DXTImageReader extends AbstractImageReader{

    private DXTHeader header;

    private void readHeader(final DataInputStream ds) throws IOException{

        final byte[] signature = ds.readFully(new byte[4]);
        if(!Arrays.equals(DXTConstants.SIGNATURE, signature)){
            throw new IOException("Stream is not a DXT image");
        }

        header = new DXTHeader();
        header.read(ds);

    }

    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        if(header == null){
            final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);
            readHeader(ds);
            //TODO rebuild meta
        }
        return new HashDictionary();
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        final BufferFactory bufferFactory = parameters.getBufferFactory();
                
        stream.mark();
        
        if(parameters instanceof DXTImageReadParameters){
            final DXTHeader header = ((DXTImageReadParameters)parameters).getHeader();
            if(header!=null){
                this.header = header;
            }
        }
        readMetadatas(stream);

        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        final boolean decompress = parameters.isDecompress();
        
        if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DXT1)){
            final Extent.Long dim = new Extent.Long(header.width,header.height);
            final DXTRawModel rm = new DXTRawModel(dim,DXTRawModel.TYPE_DXT1);
            if(decompress){
                return rm.readDXT1(ds, bufferFactory);
            }else{
                final Buffer buffer = rm.createBuffer(dim, bufferFactory);
                ds.readFully(buffer);
                final ColorModel cm = new DirectColorModel(rm, new int[]{1,2,3,0}, true);
                return new DefaultImage(buffer, dim, rm, cm);
            }
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DXT2)){
            final Extent.Long dim = new Extent.Long(header.width,header.height);
            final DXTRawModel rm = new DXTRawModel(dim,DXTRawModel.TYPE_DXT2);
            if(decompress){
                return rm.readDXT2or3(ds, true, bufferFactory);
            }else{
                final Buffer buffer = rm.createBuffer(dim, bufferFactory);
                ds.readFully(buffer);
                final ColorModel cm = new DirectColorModel(rm, new int[]{1,2,3,0}, true);
                return new DefaultImage(buffer, dim, rm, cm);
            }
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DXT3)){
            final Extent.Long dim = new Extent.Long(header.width,header.height);
            final DXTRawModel rm = new DXTRawModel(dim,DXTRawModel.TYPE_DXT3);
            if(decompress){
                return rm.readDXT2or3(ds, false, bufferFactory);
            }else{
                final Buffer buffer = rm.createBuffer(dim, bufferFactory);
                ds.readFully(buffer);
                final ColorModel cm = new DirectColorModel(rm, new int[]{1,2,3,0}, false);
                return new DefaultImage(buffer, dim, rm, cm);
            }
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DXT4)){
            final Extent.Long dim = new Extent.Long(header.width,header.height);
            final DXTRawModel rm = new DXTRawModel(dim,DXTRawModel.TYPE_DXT4);
            if(decompress){
                return rm.readDXT4or5(ds, true, bufferFactory);
            }else{
                final Buffer buffer = rm.createBuffer(dim, bufferFactory);
                ds.readFully(buffer);
                final ColorModel cm = new DirectColorModel(rm, new int[]{1,2,3,0}, true);
                return new DefaultImage(buffer, dim, rm, cm);
            }
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DXT5)){
            final Extent.Long dim = new Extent.Long(header.width,header.height);
            final DXTRawModel rm = new DXTRawModel(dim,DXTRawModel.TYPE_DXT5);
            if(decompress){
                return rm.readDXT4or5(ds, false, bufferFactory);
            }else{
                final Buffer buffer = rm.createBuffer(dim, bufferFactory);
                ds.readFully(buffer);
                final ColorModel cm = new DirectColorModel(rm, new int[]{1,2,3,0}, false);
                return new DefaultImage(buffer, dim, rm, cm);
            }
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_ATI1)){
            final BCRawModel rm = new BCRawModel(header.width,header.height);
            return rm.readATI1(ds);
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_ATI2)){
            final BCRawModel rm = new BCRawModel(header.width,header.height);
            return rm.readATI2(ds);
        }else if(header.pixelFormat.fourCC.equals(DXTConstants.D3DFMT_DX10)){
            return readDX10(ds,bufferFactory);
        }else{
            throw new IOException("DXT pixel format "+header.pixelFormat.fourCC+" not supported yet.");
        }
    }

    private Image readDX10(final DataInputStream ds,BufferFactory bufferFactory) throws IOException{

        switch(header.header10.dxgiFormat){
            case DXGI_FORMAT_UNKNOWN :
            case DXGI_FORMAT_R32G32B32A32_TYPELESS :
            case DXGI_FORMAT_R32G32B32A32_FLOAT :
            case DXGI_FORMAT_R32G32B32A32_UINT :
            case DXGI_FORMAT_R32G32B32A32_SINT :
            case DXGI_FORMAT_R32G32B32_TYPELESS :
            case DXGI_FORMAT_R32G32B32_FLOAT :
            case DXGI_FORMAT_R32G32B32_UINT :
            case DXGI_FORMAT_R32G32B32_SINT :
            case DXGI_FORMAT_R16G16B16A16_TYPELESS :
            case DXGI_FORMAT_R16G16B16A16_FLOAT :
            case DXGI_FORMAT_R16G16B16A16_UNORM :
            case DXGI_FORMAT_R16G16B16A16_UINT :
            case DXGI_FORMAT_R16G16B16A16_SNORM :
            case DXGI_FORMAT_R16G16B16A16_SINT :
            case DXGI_FORMAT_R32G32_TYPELESS :
            case DXGI_FORMAT_R32G32_FLOAT :
            case DXGI_FORMAT_R32G32_UINT :
            case DXGI_FORMAT_R32G32_SINT :
            case DXGI_FORMAT_R32G8X24_TYPELESS :
            case DXGI_FORMAT_D32_FLOAT_S8X24_UINT :
            case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS :
            case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT :
            case DXGI_FORMAT_R10G10B10A2_TYPELESS :
            case DXGI_FORMAT_R10G10B10A2_UNORM :
            case DXGI_FORMAT_R10G10B10A2_UINT :
            case DXGI_FORMAT_R11G11B10_FLOAT :
            case DXGI_FORMAT_R8G8B8A8_TYPELESS :
            case DXGI_FORMAT_R8G8B8A8_UNORM :
                throw new IOException("Unsupported DX10 dxgi format : "+ header.header10.dxgiFormat);
            case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB :
                return readR8G8B8A8_UNORM_SRGB(ds,bufferFactory);
            case DXGI_FORMAT_R8G8B8A8_UINT :
            case DXGI_FORMAT_R8G8B8A8_SNORM :
            case DXGI_FORMAT_R8G8B8A8_SINT :
            case DXGI_FORMAT_R16G16_TYPELESS :
            case DXGI_FORMAT_R16G16_FLOAT :
            case DXGI_FORMAT_R16G16_UNORM :
            case DXGI_FORMAT_R16G16_UINT :
            case DXGI_FORMAT_R16G16_SNORM :
            case DXGI_FORMAT_R16G16_SINT :
            case DXGI_FORMAT_R32_TYPELESS :
            case DXGI_FORMAT_D32_FLOAT :
            case DXGI_FORMAT_R32_FLOAT :
            case DXGI_FORMAT_R32_UINT :
            case DXGI_FORMAT_R32_SINT :
            case DXGI_FORMAT_R24G8_TYPELESS :
            case DXGI_FORMAT_D24_UNORM_S8_UINT :
            case DXGI_FORMAT_R24_UNORM_X8_TYPELESS :
            case DXGI_FORMAT_X24_TYPELESS_G8_UINT :
            case DXGI_FORMAT_R8G8_TYPELESS :
            case DXGI_FORMAT_R8G8_UNORM :
            case DXGI_FORMAT_R8G8_UINT :
            case DXGI_FORMAT_R8G8_SNORM :
            case DXGI_FORMAT_R8G8_SINT :
            case DXGI_FORMAT_R16_TYPELESS :
            case DXGI_FORMAT_R16_FLOAT :
            case DXGI_FORMAT_D16_UNORM :
            case DXGI_FORMAT_R16_UNORM :
            case DXGI_FORMAT_R16_UINT :
            case DXGI_FORMAT_R16_SNORM :
            case DXGI_FORMAT_R16_SINT :
            case DXGI_FORMAT_R8_TYPELESS :
            case DXGI_FORMAT_R8_UNORM :
            case DXGI_FORMAT_R8_UINT :
            case DXGI_FORMAT_R8_SNORM :
            case DXGI_FORMAT_R8_SINT :
            case DXGI_FORMAT_A8_UNORM :
            case DXGI_FORMAT_R1_UNORM :
            case DXGI_FORMAT_R9G9B9E5_SHAREDEXP :
            case DXGI_FORMAT_R8G8_B8G8_UNORM :
            case DXGI_FORMAT_G8R8_G8B8_UNORM :
            case DXGI_FORMAT_BC1_TYPELESS :
            case DXGI_FORMAT_BC1_UNORM :
            case DXGI_FORMAT_BC1_UNORM_SRGB :
            case DXGI_FORMAT_BC2_TYPELESS :
            case DXGI_FORMAT_BC2_UNORM :
            case DXGI_FORMAT_BC2_UNORM_SRGB :
            case DXGI_FORMAT_BC3_TYPELESS :
            case DXGI_FORMAT_BC3_UNORM :
            case DXGI_FORMAT_BC3_UNORM_SRGB :
            case DXGI_FORMAT_BC4_TYPELESS :
            case DXGI_FORMAT_BC4_UNORM :
            case DXGI_FORMAT_BC4_SNORM :
            case DXGI_FORMAT_BC5_TYPELESS :
            case DXGI_FORMAT_BC5_UNORM :
            case DXGI_FORMAT_BC5_SNORM :
            case DXGI_FORMAT_B5G6R5_UNORM :
            case DXGI_FORMAT_B5G5R5A1_UNORM :
            case DXGI_FORMAT_B8G8R8A8_UNORM :
            case DXGI_FORMAT_B8G8R8X8_UNORM :
            case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM :
            case DXGI_FORMAT_B8G8R8A8_TYPELESS :
            case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB :
            case DXGI_FORMAT_B8G8R8X8_TYPELESS :
            case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB :
            case DXGI_FORMAT_BC6H_TYPELESS :
            case DXGI_FORMAT_BC6H_UF16 :
            case DXGI_FORMAT_BC6H_SF16 :
            case DXGI_FORMAT_BC7_TYPELESS :
            case DXGI_FORMAT_BC7_UNORM :
            case DXGI_FORMAT_BC7_UNORM_SRGB :
            case DXGI_FORMAT_AYUV :
            case DXGI_FORMAT_Y410 :
            case DXGI_FORMAT_Y416 :
            case DXGI_FORMAT_NV12 :
            case DXGI_FORMAT_P010 :
            case DXGI_FORMAT_P016 :
            case DXGI_FORMAT_420_OPAQUE :
            case DXGI_FORMAT_YUY2 :
            case DXGI_FORMAT_Y210 :
            case DXGI_FORMAT_Y216 :
            case DXGI_FORMAT_NV11 :
            case DXGI_FORMAT_AI44 :
            case DXGI_FORMAT_IA44 :
            case DXGI_FORMAT_P8 :
            case DXGI_FORMAT_A8P8 :
            case DXGI_FORMAT_B4G4R4A4_UNORM :
            case DXGI_FORMAT_P208 :
            case DXGI_FORMAT_V208 :
            case DXGI_FORMAT_V408 :
            case DXGI_FORMAT_ASTC_4X4_UNORM :
            case DXGI_FORMAT_ASTC_4X4_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_5X4_TYPELESS :
            case DXGI_FORMAT_ASTC_5X4_UNORM :
            case DXGI_FORMAT_ASTC_5X4_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_5X5_TYPELESS :
            case DXGI_FORMAT_ASTC_5X5_UNORM :
            case DXGI_FORMAT_ASTC_5X5_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_6X5_TYPELESS :
            case DXGI_FORMAT_ASTC_6X5_UNORM :
            case DXGI_FORMAT_ASTC_6X5_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_6X6_TYPELESS :
            case DXGI_FORMAT_ASTC_6X6_UNORM :
            case DXGI_FORMAT_ASTC_6X6_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_8X5_TYPELESS :
            case DXGI_FORMAT_ASTC_8X5_UNORM :
            case DXGI_FORMAT_ASTC_8X5_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_8X6_TYPELESS :
            case DXGI_FORMAT_ASTC_8X6_UNORM :
            case DXGI_FORMAT_ASTC_8X6_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_8X8_TYPELESS :
            case DXGI_FORMAT_ASTC_8X8_UNORM :
            case DXGI_FORMAT_ASTC_8X8_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_10X5_TYPELESS :
            case DXGI_FORMAT_ASTC_10X5_UNORM :
            case DXGI_FORMAT_ASTC_10X5_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_10X6_TYPELESS :
            case DXGI_FORMAT_ASTC_10X6_UNORM :
            case DXGI_FORMAT_ASTC_10X6_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_10X8_TYPELESS :
            case DXGI_FORMAT_ASTC_10X8_UNORM :
            case DXGI_FORMAT_ASTC_10X8_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_10X10_TYPELESS :
            case DXGI_FORMAT_ASTC_10X10_UNORM :
            case DXGI_FORMAT_ASTC_10X10_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_12X10_TYPELESS :
            case DXGI_FORMAT_ASTC_12X10_UNORM :
            case DXGI_FORMAT_ASTC_12X10_UNORM_SRGB :
            case DXGI_FORMAT_ASTC_12X12_TYPELESS :
            case DXGI_FORMAT_ASTC_12X12_UNORM :
            case DXGI_FORMAT_ASTC_12X12_UNORM_SRGB :
            default :
                throw new IOException("Unsupported DX10 dxgi format : "+ header.header10.dxgiFormat);
        }

    }
    
    private Image readR8G8B8A8_UNORM_SRGB(final DataInputStream ds,BufferFactory bufferFactory) throws IOException{
        //TODO we can optimize this, using the byte array directly for the image
        final byte[] buffer = new byte[4*header.width*header.height];
        ds.readFully(buffer);
        final Image image = Images.create(new Extent.Long(header.width, header.height),Images.IMAGE_TYPE_RGBA,bufferFactory);
        image.getDataBuffer().writeByte(buffer, 0);
        return image;
    }
    
    
}