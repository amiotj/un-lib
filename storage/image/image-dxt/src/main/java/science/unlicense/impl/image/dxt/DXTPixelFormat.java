
package science.unlicense.impl.image.dxt;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DXTPixelFormat {

    public int size;
    public int flags;
    public Chars fourCC;
    public int RGBBitCount;
    public int RBitMask;
    public int GBitMask;
    public int BBitMask;
    public int ABitMask;
}
