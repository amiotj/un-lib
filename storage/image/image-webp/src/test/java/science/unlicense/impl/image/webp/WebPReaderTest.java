

package science.unlicense.impl.image.webp;

import science.unlicense.impl.image.webp.WebPReader;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class WebPReaderTest {

    @Ignore
    @Test
    public void testAnimated() throws IOException{
        Path p = Paths.resolve(new Chars("mod:/un/storage/imagery/webp/dancing_banana.webp"));
        
        WebPReader reader = new WebPReader();
        reader.setInput(p);
        
        Image image = reader.read(null);
        
    }

    @Ignore
    @Test
    public void testLosslessAlpha() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/webp/LosslessAlpha.webp")).createInputStream();

        final ImageReader reader = new WebPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,0,0,255}, b00); //RED
        Assert.assertArrayEquals(new int[]{255,255,0,206}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0,255,255,0}, b01); //TRANSLUCENT
        Assert.assertArrayEquals(new int[]{0,0,255,255}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 206), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 0), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }

    @Ignore
    @Test
    public void testCompressedAlpha() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/png/CompressedAlpha.webp")).createInputStream();

        final ImageReader reader = new WebPReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();

        final int[] b00 = sm.getTupleUByte(new int[]{0,0},null);
        final int[] b10 = sm.getTupleUByte(new int[]{1,0},null);
        final int[] b01 = sm.getTupleUByte(new int[]{0,1},null);
        final int[] b11 = sm.getTupleUByte(new int[]{1,1},null);

        //values are in RGB
        Assert.assertArrayEquals(new int[]{255,0,0,255}, b00); //RED
        Assert.assertArrayEquals(new int[]{255,255,0,206}, b10); //YELLOW
        Assert.assertArrayEquals(new int[]{0,255,255,0}, b01); //TRANSLUCENT
        Assert.assertArrayEquals(new int[]{0,0,255,255}, b11); //BLUE

        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(b00));
        Assert.assertEquals(new Color(255, 255, 0, 206), cm.toColor(b10));
        Assert.assertEquals(new Color(0, 255, 255, 0), cm.toColor(b01));
        Assert.assertEquals(new Color(0,   0, 255, 255), cm.toColor(b11));

    }
    
}
