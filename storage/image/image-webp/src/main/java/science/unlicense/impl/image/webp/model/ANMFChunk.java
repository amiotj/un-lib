

package science.unlicense.impl.image.webp.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.image.webp.WebPConstants;
import static science.unlicense.impl.image.webp.WebPConstants.CHUNK_VP8L;

/**
 * Animation frame chunk.
 * 
 * @author Johann Sorel
 */
public class ANMFChunk extends DefaultChunk{
    
    /** 24bits : upper left corner X coordinate of the frame * 2 */
    public int frameX;
    /** 24bits : upper left corner Y coordinate of the frame * 2 */
    public int frameY;
    /** 24bits : frame width - 1 */
    public int frameWidth;
    /** 24bits : frame height - 1 */
    public int frameHeight;
    /** 24bits : frame duration */
    public int frameDuration;
    /** 6bits : reserved */
    public int reserved;
    /** 1bit : blend mode , see spec : https://developers.google.com/speed/webp/docs/riff_container#animation
     * 0 = use alpha blending
     * 1 = do not blend
     */
    public boolean blendMode;
    /** 1bit : dispose mode , see spec : https://developers.google.com/speed/webp/docs/riff_container#animation
     * 0 = do not dispose
     * 1 = reset to background color
     */
    public boolean disposeMode;

    public VP8LChunk frame;

    public ANMFChunk() {
        super(WebPConstants.CHUNK_ANMF);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        frameX = ds.readUInt24();
        frameY = ds.readUInt24();
        frameWidth = ds.readUInt24();
        frameHeight = ds.readUInt24();
        frameDuration = ds.readUInt24();
        reserved = ds.readBits(6);
        blendMode = ds.readBits(1)==1;
        disposeMode = ds.readBits(1)==1;

        //Read frame data
        final Chars frameFourCC = ds.readBlockZeroTerminatedChars(4, CharEncodings.US_ASCII);
        final long frameSizeByte = ds.readUInt();

        if(CHUNK_VP8L.equals(frameFourCC)){
            frame = new VP8LChunk();
            frame.setFourCC(frameFourCC);
            frame.setSize(frameSizeByte);
            frame.read(ds);
        }else{
            throw new IOException("Unexpected frame data chunk : "+frameFourCC);
        }

    }
    
    
    
}
