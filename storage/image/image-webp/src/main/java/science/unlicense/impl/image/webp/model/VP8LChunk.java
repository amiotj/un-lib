

package science.unlicense.impl.image.webp.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.image.webp.WebPConstants;
import science.unlicense.impl.media.vp8l.VP8LReader;

/**
 * Contains a lossless image.
 * 
 * specification :
 * https://gerrit.chromium.org/gerrit/gitweb?p=webm/libwebp.git;a=blob;f=doc/webp-lossless-bitstream-spec.txt;hb=master
 * 
 * @author Johann Sorel
 */
public class VP8LChunk extends DefaultChunk{
    
    public VP8LChunk() {
        super(WebPConstants.CHUNK_VP8L);
    }

    @Override
    protected void readInternal(DataInputStream ds) throws IOException {
        VP8LReader reader = new VP8LReader();
        reader.setInput(ds);
        reader.read();
    }

}
