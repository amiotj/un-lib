

package science.unlicense.impl.image.webp.model;

import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.image.webp.WebPConstants;

/**
 * ICCP color profile chunk.
 * 
 * @author Johann Sorel
 */
public class ICCPChunk extends DefaultChunk{
    
    public ICCPChunk() {
        super(WebPConstants.CHUNK_ICCP);
    }
    
}
