

package science.unlicense.impl.image.webp;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class WebPConstants {
    
    public static final Chars CHUNK_VP8 = new Chars(new byte[]{'V','P','8',' '});
    public static final Chars CHUNK_VP8L = new Chars(new byte[]{'V','P','8','L'});
    public static final Chars CHUNK_VP8X = new Chars(new byte[]{'V','P','8','X'});
    public static final Chars CHUNK_ICCP = new Chars(new byte[]{'I','C','C','P'});
    public static final Chars CHUNK_ALPH = new Chars(new byte[]{'A','L','P','H'});
    public static final Chars CHUNK_ANIM = new Chars(new byte[]{'A','N','I','M'});
    public static final Chars CHUNK_ANMF = new Chars(new byte[]{'A','N','M','F'});
    public static final Chars CHUNK_EXIF = new Chars(new byte[]{'E','X','I','F'});
    public static final Chars CHUNK_XMP = new Chars(new byte[]{'X','M','P',' '});
    
    private WebPConstants(){}
    
}
