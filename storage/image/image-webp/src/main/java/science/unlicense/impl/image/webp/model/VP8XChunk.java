

package science.unlicense.impl.image.webp.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.binding.riff.model.LISTChunk;
import science.unlicense.impl.image.webp.WebPConstants;

/**
 * Contains meta informations on image.
 * 
 * @author Johann Sorel
 */
public class VP8XChunk extends DefaultChunk{
    
    /** 2bits : reserverd */
    public int reserved1;
    /** 1bit : has ICCProfile chunk */
    public boolean hasIcc;
    /** 1bit : has Alpha */
    public boolean hasAlpha;
    /** 1bit : has exif chunk */
    public boolean hasExif;
    /** 1bit : has xmp chunk */
    public boolean hasXmp;
    /** 1bit : has Anim chunks */
    public boolean hasAnimA;
    /** 1bit : has anmf chunks */
    public boolean hasAnmf;
    /** 24bits : reserverd */
    public int reserved2;
    /** 24bits : canvas width - 1 */
    public int canvasWidth;
    /** 24bits : canvas height - 1 */
    public int canvasHeight;
    
    public VP8XChunk() {
        super(WebPConstants.CHUNK_VP8X);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        reserved1 = ds.readBits(2);
        hasIcc = ds.readBits(1) == 1;
        hasAlpha = ds.readBits(1) == 1;
        hasExif = ds.readBits(1) == 1;
        hasXmp = ds.readBits(1) == 1;
        hasAnimA = ds.readBits(1) == 1;
        hasAnmf = ds.readBits(1) == 1;
        reserved2 = ds.readBits(24);
        canvasWidth = ds.readUInt24();
        canvasHeight = ds.readUInt24();
    }
    
    
    
}
