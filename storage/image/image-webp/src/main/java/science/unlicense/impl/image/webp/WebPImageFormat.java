
package science.unlicense.impl.image.webp;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * 
 * docs, info and specification : 
 * http://en.wikipedia.org/wiki/Progressive_Graphics_File
 * https://developers.google.com/speed/webp/docs/riff_container
 * http://tools.ietf.org/html/rfc6386
 *
 * @author Johann Sorel
 */
public class WebPImageFormat extends AbstractImageFormat{

    public WebPImageFormat() {
        super(new Chars("webp"),
              new Chars("WEBP"),
              new Chars("WebP"),
              new Chars[]{
                  new Chars("image/webp")
              },
              new Chars[]{
                  new Chars("webp")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new WebPReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
