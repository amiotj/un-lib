

package science.unlicense.impl.image.webp.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.image.webp.WebPConstants;

/**
 * XMP metadata chunk.
 * 
 * @author Johann Sorel
 */
public class XMPChunk extends DefaultChunk{
    
    //TODO XMP data decoder
    public byte[] xmpData;
    
    public XMPChunk() {
        super(WebPConstants.CHUNK_XMP);
    }
    
    protected void readInternal(DataInputStream ds) throws IOException {
        xmpData = ds.readFully(new byte[(int)size]);
    }
    
}
