

package science.unlicense.impl.image.webp.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.image.webp.WebPConstants;

/**
 * Animation chunk.
 * 
 * @author Johann Sorel
 */
public class ANIMChunk extends DefaultChunk{
    
    /** 32bits : animation background color in RGBA */
    public int backgroundColorRGBA;
    /** 16bits : number of loops, 0 for infinite */
    public int nbLoop;
    
    public ANIMChunk() {
        super(WebPConstants.CHUNK_ANIM);
    }

    protected void readInternal(DataInputStream ds) throws IOException {
        backgroundColorRGBA = ds.readInt();
        nbLoop = ds.readUShort();
    }
    
}
