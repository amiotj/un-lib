
package science.unlicense.impl.image.dicom;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class DICOMImageFormat extends AbstractImageFormat{

    public DICOMImageFormat() {
        super(new Chars("dicom"),
              new Chars("DICOM"),
              new Chars("Digital Imaging and Communications in Medicine"),
              new Chars[]{
                  new Chars("application/dicom")
              },
              new Chars[]{
                new Chars("dcm")
              },
              new byte[0][0]);
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
