
package science.unlicense.impl.image.etc;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * resources :
 * http://www.khronos.org/opengles/sdk/tools/KTX/file_format_spec/
 * http://en.wikipedia.org/wiki/Ericsson_Texture_Compression
 *
 * @author Johann Sorel
 */
public class ETCImageFormat extends AbstractImageFormat{

    public ETCImageFormat() {
        super(new Chars("etc"),
              new Chars("ETC"),
              new Chars("Ericsson Texture Compression"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("etc"),
                new Chars("ftx")
              },
              new byte[0][0]);
    }

    public boolean canDecode(ByteInputStream stream) throws IOException {
        return false;
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
