
package science.unlicense.impl.image.pic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class PICImageFormat extends AbstractImageFormat{

    public PICImageFormat() {
        super(new Chars("pic"),
              new Chars("PIC"),
              new Chars("PICtor"),
              new Chars[]{
                  new Chars("image/pic")
              },
              new Chars[]{
                  new Chars("pic")
              },
              new byte[0][0]);
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return null;
    }

    public ImageWriter createWriter() {
        return null;
    }

}
