
package science.unlicense.impl.image.tga;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * @author Johann Sorel
 */
public class TGAImageFormat extends AbstractImageFormat{

    public TGAImageFormat() {
        super(new Chars("tga"),
              new Chars("TGA"),
              new Chars("Truevision Targa"),
              new Chars[]{
                  new Chars("image/tga"),
                  new Chars("image/targa"),
                  new Chars("application/targa")
              },
              new Chars[]{
                new Chars("tga"),
                new Chars("tpic")
              },
              new byte[][]{});
    }

//    public boolean canDecode(Object input) throws IOException {
//        if(super.canDecode(input)){
//            return true;
//        }else{
//            final boolean[] closeStream = new boolean[1];
//            final ByteInputStream stream = IOUtilities.toInputStream(input, closeStream);
//            try{
//                return canDecode(stream);
//            }finally{
//                if(closeStream[0]){
//                    stream.close();
//                }
//            }
//        }
//    }

    private boolean canDecode(ByteInputStream stream) throws IOException {
        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.BIG_ENDIAN);
        final byte[] buffer = new byte[3];
        ds.readFully(buffer);
        //not exact but we don't want to read the file footer to know if it's a tga
        if(Arrays.contains(new int[]{
            TGAMetaModel.IMAGE_TYPE_NODATA,
            TGAMetaModel.IMAGE_TYPE_RLE_BW,
            TGAMetaModel.IMAGE_TYPE_RLE_COLORMAP,
            TGAMetaModel.IMAGE_TYPE_RLE_TRUECOLOR,
            TGAMetaModel.IMAGE_TYPE_UNCOMPRESSED_BW,
            TGAMetaModel.IMAGE_TYPE_UNCOMPRESSED_COLORMAP,
            TGAMetaModel.IMAGE_TYPE_UNCOMPRESSED_TRUECOLOR
            }, buffer[2])){
            //TODO we should do more checks
            return true;
        }
        return false;
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new TGAImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
