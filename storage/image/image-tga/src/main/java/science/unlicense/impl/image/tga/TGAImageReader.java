package science.unlicense.impl.image.tga;

import science.unlicense.api.character.Chars;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.io.rle.RLEInputStream;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.ImageSetMetadata.*;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.ColorIndex;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.IndexedColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import static science.unlicense.impl.image.tga.TGAMetaModel.*;

/**
 *
 * @author Johann Sorel
 */
public class TGAImageReader extends AbstractImageReader{

    //file metadatas
    private TypedNode mdTga = null;
    private TypedNode mdImage = null;

    //informations -------------------------------------------------------------
    private int idLength;
    private int colorMapType;
    private int imageType;
    //colormap spec : 5 bytes
    private int colorMapSpec_firstEntryIndex;
    private int colorMapSpec_colorMapLength;
    private int colorMapSpec_colorMapEntrySize;
    //image spec : 10 bytes
    private int imageSpec_XOrigin;
    private int imageSpec_YOrigin;
    private int imageSpec_width;
    private int imageSpec_height;
    private int imageSpec_pixeldepth;
    private byte imageSpec_descriptor;
    private Chars id;
    //--------------------------------------------------------------------------


    public TGAImageReader() {
    }

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        idLength = ds.read();
        colorMapType = ds.read();
        imageType = ds.read();

        //colormap spec : 5 bytes
        colorMapSpec_firstEntryIndex = ds.readUShort();
        colorMapSpec_colorMapLength = ds.readUShort();
        colorMapSpec_colorMapEntrySize = ds.readUByte();

        //image spec : 10 bytes
        imageSpec_XOrigin = ds.readUShort();
        imageSpec_YOrigin = ds.readUShort();
        imageSpec_width = ds.readUShort();
        imageSpec_height = ds.readUShort();
        imageSpec_pixeldepth = ds.readUByte();
        imageSpec_descriptor = ds.readByte();
        //read ID
        id = new Chars(ds.readFully(new byte[idLength]));

        //read the footer to determine if it's a old or new TGA file.
        //contain developer and extension offset.
        //TODO

        //rebuild metamodel tree
        mdTga =
        new DefaultTypedNode(MD_TGA, new TypedNode[]{
            new DefaultTypedNode(MD_TGA_ID,id),
            new DefaultTypedNode(MD_TGA_COLORMAP, new TypedNode[]{
                new DefaultTypedNode(MD_TGA_COLORMAP_TYPE,colorMapType),
                new DefaultTypedNode(MD_TGA_COLORMAP_FIRSTENTRYINDEX,colorMapSpec_firstEntryIndex),
                new DefaultTypedNode(MD_TGA_COLORMAP_LENGTH,colorMapSpec_colorMapLength),
                new DefaultTypedNode(MD_TGA_COLORMAP_SIZE,colorMapSpec_colorMapEntrySize) }),
            new DefaultTypedNode(MD_TGA_IMAGE, new TypedNode[]{
                new DefaultTypedNode(MD_TGA_IMAGE_TYPE,imageType),
                new DefaultTypedNode(MD_TGA_IMAGE_XORIGIN,imageSpec_XOrigin),
                new DefaultTypedNode(MD_TGA_IMAGE_YORIGIN,imageSpec_YOrigin),
                new DefaultTypedNode(MD_TGA_IMAGE_WIDTH,imageSpec_width),
                new DefaultTypedNode(MD_TGA_IMAGE_HEIGHT,imageSpec_height),
                new DefaultTypedNode(MD_TGA_IMAGE_DEPTH,imageSpec_pixeldepth),
                new DefaultTypedNode(MD_TGA_IMAGE_DESCRIPTOR,imageSpec_descriptor) })
        });

        mdImage =
        new DefaultTypedNode(MD_IMAGE,new Node[]{
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"x"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,imageSpec_width)}),
            new DefaultTypedNode(MD_IMAGE_DIMENSION,new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID,"y"),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND,imageSpec_height)})
        });

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGE,new Node[]{mdImage});

        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        metas.add(mdTga.getType().getId(), mdTga);
        return metas;
    }

    @Override
    protected Image read(ImageReadParameters params, BacktrackInputStream stream) throws IOException {

        final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

        if(mdTga != null){
            //we already read the meta
            //skip base header + colormap spec + image spec
            ds.skip(3 + 5 + 10 + idLength);

        }else{
            //read the metas
            readMetadatas(stream);
        }

        final boolean flipVertical = (imageSpec_descriptor & 0x20) == 0;
        final boolean flipHorizontal = (imageSpec_descriptor & 0x10) != 0;

        int imageSpec_pixeldepth = this.imageSpec_pixeldepth;
        if(imageSpec_pixeldepth % 8 != 0){
            throw new IOException("pixel depth unsupported, must be a module of 8.");
        }
        imageSpec_pixeldepth = imageSpec_pixeldepth/8;


        //read colormap
        ColorIndex colorIndex = null;
        if(colorMapType != 0){
            if(colorMapSpec_colorMapEntrySize % 8 != 0){
                throw new IOException("colormap of size not a module of 8 not supported.");
            }
            final byte[][] colorMap = new byte[colorMapSpec_colorMapLength][colorMapSpec_colorMapEntrySize/8];
            for(int i=0;i<colorMapSpec_colorMapLength;i++){
                ds.readFully(colorMap[i]);
            }
            //colors are in BVR order
            final Color[] palette = new Color[colorMapSpec_colorMapLength];
            for(int i=0;i<palette.length;i++){
                if(colorMap[0].length==3){
                    palette[i] = new Color(colorMap[i][2]&0xFF, colorMap[i][1]&0xFF, colorMap[i][0]&0xFF);
                }else if(colorMap[0].length==4){
                    palette[i] = new Color(colorMap[i][2]&0xFF, colorMap[i][1]&0xFF, colorMap[i][0]&0xFF, colorMap[i][3]&0xFF);
                }else{
                    throw new IOException("unexpected colormap tuple size"+colorMap[0].length);
                }
            }
            colorIndex = new ColorIndex(palette);
        }

        //read image data
        final int lineSize = imageSpec_width*imageSpec_pixeldepth;
        final int n = imageSpec_height*lineSize;
        final byte[] data = new byte[n];
        final DataInputStream uncompressed;
        if(    imageType == IMAGE_TYPE_RLE_TRUECOLOR
            || imageType == IMAGE_TYPE_RLE_COLORMAP
            || imageType == IMAGE_TYPE_RLE_BW){
            //decompress rle stream
            final RLEInputStream rle = new RLEInputStream(stream,imageSpec_pixeldepth);
            uncompressed = new DataInputStream(rle);
        }else{
            uncompressed = ds;
        }

        //read and reorder bits
        for(int y=0;y<imageSpec_height;y++){
            final int offset;
            if(flipVertical){
                offset = (imageSpec_height-y-1)*lineSize;
            }else{
                offset = lineSize*y;
            }
            uncompressed.readFully(data,offset,lineSize);

            if(flipHorizontal){
                Arrays.reverse(data, offset, lineSize, imageSpec_pixeldepth);
            }
        }

        final Buffer bank = DefaultBufferFactory.wrap(data);

        if(imageType == IMAGE_TYPE_RLE_TRUECOLOR || imageType == IMAGE_TYPE_UNCOMPRESSED_TRUECOLOR){
            final int[] mapping;
            if(imageSpec_pixeldepth == 3){
                mapping = new int[]{2,1,0,-1};
            }else if(imageSpec_pixeldepth == 4){
                mapping = new int[]{2,1,0,3};
            }else{
                throw new IOException("Only handle 3 and 4 byte tga so far.");
            }

            final RawModel sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, imageSpec_pixeldepth);

            //colors are in BVR
            final ColorModel cm = new DirectColorModel(sm, mapping,false);

            return new DefaultImage(bank, new Extent.Long(imageSpec_width, imageSpec_height), sm,cm);
        }else if(imageType == IMAGE_TYPE_RLE_COLORMAP || imageType == IMAGE_TYPE_UNCOMPRESSED_COLORMAP){
            final RawModel sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 1);
            final ColorModel cm = new IndexedColorModel(RawModel.TYPE_UBYTE, colorIndex);
            return new DefaultImage(bank, new Extent.Long(imageSpec_width, imageSpec_height), sm,cm);
        
        }else if(imageType == IMAGE_TYPE_RLE_BW || imageType == IMAGE_TYPE_UNCOMPRESSED_BW){
            final int[] mapping = new int[]{0,0,0,-1};
            final RawModel sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 1);
            final ColorModel cm = new DirectColorModel(sm, mapping,false);
            return new DefaultImage(bank, new Extent.Long(imageSpec_width, imageSpec_height), sm,cm);
        }else{
            throw new IOException("Unsupported TGA image type "+imageType);
        }
    }

}
