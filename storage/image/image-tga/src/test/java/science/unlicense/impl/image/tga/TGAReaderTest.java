package science.unlicense.impl.image.tga;

import science.unlicense.impl.image.tga.TGAImageReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class TGAReaderTest {

    /**
     * This image pixels starts at the up left.
     */
    @Test
    public void testReadUncompressedUL() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleUncompressedTL.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in BVR

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));

    }

    /**
     * This image pixels starts at the bottom left.
     */
    @Test
    public void testReadUncompressedBL() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleUncompressedBL.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in BVR

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));

    }

    /**
     * This image pixels starts at the bottom left. 4*2 dimension
     */
    @Test
    public void testReadUncompressedBLLarge() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleUncompressedBLLarge.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(4,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in BVR

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //WHITE
        sm.getTuple(new int[]{2,0},storage);
        Assert.assertArrayEquals(new int[]{255,255,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 255, 255), cm.toColor(storage));
        //YELLOW
        sm.getTuple(new int[]{3,0},storage);
        Assert.assertArrayEquals(new int[]{0,255,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));
        //CYAN
        sm.getTuple(new int[]{2,1},storage);
        Assert.assertArrayEquals(new int[]{255,255,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(storage));
        //GRAY
        sm.getTuple(new int[]{3,1},storage);
        Assert.assertArrayEquals(new int[]{100,100,100}, (int[])storage);
        Assert.assertEquals(new Color(100, 100, 100, 255), cm.toColor(storage));

    }

    @Test
    public void testReadCompressed() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleCompressed.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in BVR

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{255,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0,0,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));

    }

    /**
     * Grayscale rle compressed image.
     */
    @Test
    public void testReadBW() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleBW.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //WHITE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{255}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 255, 255), cm.toColor(storage));
        //DARK GRAY
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{100}, (int[])storage);
        Assert.assertEquals(new Color(100, 100, 100, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //LIGHT GRAY
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{200}, (int[])storage);
        Assert.assertEquals(new Color(200, 200, 200, 255), cm.toColor(storage));

    }
    
    /**
     * Grayscale uncompressed image.
     */
    @Test
    public void testReadUncompressedBW() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleUncompressedBW.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //WHITE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{255}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 255, 255), cm.toColor(storage));
        //DARK GRAY
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{100}, (int[])storage);
        Assert.assertEquals(new Color(100, 100, 100, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //LIGHT GRAY
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{200}, (int[])storage);
        Assert.assertEquals(new Color(200, 200, 200, 255), cm.toColor(storage));

    }
    
    /**
     * Colormap uncompressed image.
     */
    @Test
    public void testReadUncompressedColorMap() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleUncompressedColorMap.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //RED
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{2}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{3}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //BLUE
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{1}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));

    }
    
    /**
     * Colormap rle compressed image.
     */
    @Test
    public void testReadColorMap() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/tga/sampleColorMap.tga")).createInputStream();

        final ImageReader reader = new TGAImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2,image.getExtent().getDimension());
        Assert.assertEquals(2,image.getExtent().getL(0));
        Assert.assertEquals(2,image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //RED
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{2}, (int[])storage);
        Assert.assertEquals(new Color(255, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{3}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 0, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //BLUE
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{1}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 255, 255), cm.toColor(storage));

    }
    
}
