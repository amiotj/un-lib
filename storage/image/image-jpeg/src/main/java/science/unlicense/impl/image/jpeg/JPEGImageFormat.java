
package science.unlicense.impl.image.jpeg;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;
import static science.unlicense.impl.image.jpeg.JPEGConstants.SIGNATURE;

/**
 * JPEG image format.
 *
 * @author Johann Sorel
 */
public class JPEGImageFormat extends AbstractImageFormat{

    public JPEGImageFormat() {
        super(new Chars("jpeg"),
              new Chars("JPEG"),
              new Chars("Joint Photographic Experts Group"),
              new Chars[]{
                  new Chars("image/jpeg"),
                  new Chars("image/jpg"),
              },
              new Chars[]{
                  new Chars("jpg"),
                  new Chars("jpeg"),
                  new Chars("jpe")
              },
              new byte[][]{SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new JPEGImageReader();
    }

    public ImageWriter createWriter() {
        return null;
    }

}
