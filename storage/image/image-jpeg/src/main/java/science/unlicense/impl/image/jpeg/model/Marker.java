

package science.unlicense.impl.image.jpeg.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Global interface for all jpeg markers.
 * 
 * @author Johann Sorel
 */
public interface Marker {
    
    void read(DataInputStream ds) throws IOException;
    
    void write(DataOutputStream ds) throws IOException;
    
}
