
package science.unlicense.impl.image.jpeg.model;

/**
 *
 * @author Johann Sorel
 */
public final class SOFComponent {
    
    public int componentId;
    /**
     * The relative number of horizontal data units of a particular component with respect
     * to the number of horizontal data units in the other components.
     */
    public int horizontalSamplingFactor;
    /**
     * The relative number of vertical data units of a particular component with respect to
     * the number of vertical data units in the other components in the frame.
     */
    public int verticalSamplingFactor;
    
    public int quantizationTable;
    
}
