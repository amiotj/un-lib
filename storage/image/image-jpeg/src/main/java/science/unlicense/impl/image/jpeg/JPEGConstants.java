
package science.unlicense.impl.image.jpeg;

/**
 *
 * @author Johann Sorel
 */
public final class JPEGConstants {
    
    public static final byte[] SIGNATURE = new byte[]{(byte)0xFF,(byte)0xD8};
    
    //Start Of Frame markers, non-differential, Huffman coding
    public static final int MARKER_SOF_0 = 0xFFC0;  //Baseline DCT
    public static final int MARKER_SOF_1 = 0xFFC1;  //Extended sequential DCT
    public static final int MARKER_SOF_2 = 0xFFC2;  //Progressive DCT
    public static final int MARKER_SOF_3 = 0xFFC3;  //Lossless (sequential)
    //Start Of Frame markers, differential, Huffman coding
    public static final int MARKER_SOF_5 = 0xFFC5;  //Differential sequential DCT
    public static final int MARKER_SOF_6 = 0xFFC6;  //Differential progressive DCT
    public static final int MARKER_SOF_7 = 0xFFC7;  //Differential lossless (sequential)
    //Start Of Frame markers, non-differential, arithmetic coding
    public static final int MARKER_JPG    = 0xFFC8; //Reserved for JPEG extensions
    public static final int MARKER_SOF_9  = 0xFFC9; //Extended sequential DCT
    public static final int MARKER_SOF_10 = 0xFFCA; //Progressive DCT
    public static final int MARKER_SOF_11 = 0xFFCB; //Lossless (sequential)
    //Start Of Frame markers, differential, arithmetic coding
    public static final int MARKER_SOF_13 = 0xFFCD; //Differential sequential DCT
    public static final int MARKER_SOF_14 = 0xFFCE; //Differential progressive DCT
    public static final int MARKER_SOF_15 = 0xFFCF; //Differential lossless (sequential)
    //Huffman ZIGZAG specification
    public static final int MARKER_DHT    = 0xFFC4; //Define Huffman ZIGZAG(s)
    //Arithmetic coding conditioning specification
    public static final int MARKER_DAC    = 0xFFCC; //Define arithmetic coding conditioning(s)
    //Restart interval termination
    public static final int MARKER_RST_ST = 0xFFD0; //Restart with modulo 8 count “m” : range start
    public static final int MARKER_RST_ED = 0xFFD7; //Restart with modulo 8 count “m” : range end
    //Other markers
    public static final int MARKER_SOI    = 0xFFD8; //Start of image
    public static final int MARKER_EOI    = 0xFFD9; //End of image
    public static final int MARKER_SOS    = 0xFFDA; //Start of scan
    public static final int MARKER_DQT    = 0xFFDB; //Define quantization ZIGZAG(s)
    public static final int MARKER_DNL    = 0xFFDC; //Define number of lines
    public static final int MARKER_DRI    = 0xFFDD; //Define restart interval
    public static final int MARKER_DHP    = 0xFFDE; //Define hierarchical progression
    public static final int MARKER_EXP    = 0xFFDF; //Expand reference component(s)
    public static final int MARKER_APP_ST = 0xFFE0; //Reserved for application segments : range start
    public static final int MARKER_APP_ED = 0xFFEF; //Reserved for application segments : range end
    public static final int MARKER_JPG_ST = 0xFFF0; //Reserved for JPEG extensions : range start
    public static final int MARKER_JPG_ED = 0xFFFD; //Reserved for JPEG extensions : range end
    public static final int MARKER_COM    = 0xFFFE; //Comment
    //Reserved markers
    public static final int MARKER_TEM    = 0xFF01; //For temporary private use in arithmetic coding
    public static final int MARKER_RES_ST = 0xFF02; //Reserved : range start
    public static final int MARKER_RES_ED = 0xFFBF; //Reserved : range end

    
    public static final int[] ZIGZAG= {
        0,   1,  5,  6, 14, 15, 27, 28,
        2,   4,  7, 13, 16, 26, 29, 42,
        3,   8, 12, 17, 25, 30, 41, 43,
        9,  11, 18, 24, 31, 40, 44, 53,
        10, 19, 23, 32, 39, 45, 52, 54,
        20, 22, 33, 38, 46, 51, 55, 60,
        21, 34, 37, 47, 50, 56, 59, 61,
        35, 36, 48, 49, 57, 58, 62, 63
    };
    
    // Table K.1 – Luminance quantization table
    public static final int[] LUMINANCE= {
        16, 11, 10, 16, 24, 40, 51, 61,
        12, 12, 14, 19, 26, 58, 60, 55,
        14, 13, 16, 24, 40, 57, 69, 56,
        14, 17, 22, 29, 51, 87, 80, 62,
        18, 22, 37, 56, 68,109,103, 77,
        24, 35, 55, 64, 81,104,113, 92,
        49, 64, 78, 87,103,121,120,101,
        72, 92, 95, 98,112,100,103, 99
    };
    
    // Table K.2 – Chrominance quantization table
    public static final int[] CHROMINANCE= {
        17, 18, 24, 47, 99, 99, 99, 99,
        18, 21, 26, 66, 99, 99, 99, 99,
        24, 26, 56, 99, 99, 99, 99, 99,
        47, 66, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99
    };
    
    
    private JPEGConstants(){}
    
}
