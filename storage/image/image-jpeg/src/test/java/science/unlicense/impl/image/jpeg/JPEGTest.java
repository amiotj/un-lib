package science.unlicense.impl.image.jpeg;

import science.unlicense.impl.image.jpeg.JPEGImageReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JPEGTest {

    @Test
    public void testRead() throws Exception{

        final ByteInputStream bs = Paths.resolve(new Chars("mod:/un/storage/imagery/jpeg/Sample.jpg")).createInputStream();

        final ImageReader reader = new JPEGImageReader();
        reader.setInput(bs);

        final Image image = reader.read(reader.createParameters());
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(4, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final ColorModel cm = image.getColorModel();
        final Object storage = sm.createTupleStore();

        //values are in RGB
        //colors are not exact because of compression and YUV storage mode

        //BLUE
        sm.getTuple(new int[]{0,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,254}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 254, 255), cm.toColor(storage));
        //BLACK
        sm.getTuple(new int[]{1,0},storage);
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])storage);
        Assert.assertEquals(new Color(0, 0, 0, 255), cm.toColor(storage));
        //WHITE
        sm.getTuple(new int[]{2,0},storage);
        Assert.assertArrayEquals(new int[]{255,255,255}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 255, 255), cm.toColor(storage));
        //YELLOW
        sm.getTuple(new int[]{3,0},storage);
        Assert.assertArrayEquals(new int[]{255,255,0}, (int[])storage);
        Assert.assertEquals(new Color(255, 255, 0, 255), cm.toColor(storage));
        //RED
        sm.getTuple(new int[]{0,1},storage);
        Assert.assertArrayEquals(new int[]{254,0,0}, (int[])storage);
        Assert.assertEquals(new Color(254, 0, 0, 255), cm.toColor(storage));
        //GREEN
        sm.getTuple(new int[]{1,1},storage);
        Assert.assertArrayEquals(new int[]{0,254,2}, (int[])storage);
        Assert.assertEquals(new Color(0, 254, 2, 255), cm.toColor(storage));
        //CYAN
        sm.getTuple(new int[]{2,1},storage);
        Assert.assertArrayEquals(new int[]{0,255,255}, (int[])storage);
        Assert.assertEquals(new Color(0, 255, 255, 255), cm.toColor(storage));
        //GRAY
        sm.getTuple(new int[]{3,1},storage);
        Assert.assertArrayEquals(new int[]{100,100,100}, (int[])storage);
        Assert.assertEquals(new Color(100, 100, 100, 255), cm.toColor(storage));

    }
    
}
