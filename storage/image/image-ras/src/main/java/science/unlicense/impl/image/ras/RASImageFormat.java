
package science.unlicense.impl.image.ras;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/Sun_Raster
 * http://www.fileformat.info/format/sunraster/egff.htm
 * 
 * @author Johann Sorel
 */
public class RASImageFormat extends AbstractImageFormat{

    public RASImageFormat() {
        super(new Chars("ras"),
              new Chars("RAS"),
              new Chars("Sun Raster"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ras"),
                new Chars("sun")
              },
              new byte[][]{
                  RASConstants.SIGNATURE
                });
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        throw new UnimplementedException("Not supported yet.");
    }

    public ImageWriter createWriter() {
        throw new UnimplementedException("Not supported yet.");
    }

}
