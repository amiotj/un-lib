
package science.unlicense.impl.image.ras;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.io.BacktrackInputStream;

/**
 *
 * @author Johann Sorel
 */
public class RASImageReader extends AbstractImageReader{

    @Override
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);
        
        //read header
        if(!Arrays.equals(RASConstants.SIGNATURE,ds.readFully(new byte[4]))){
            throw new IOException("Wrong signature, stream is not a valid sun raster image.");
        }
        final RASHeader header = new RASHeader();
        header.read(ds);
        
        //read colormap
        if(header.colormaptype==RASConstants.COLORMAP_RGB){
            
        }else if(header.colormaptype==RASConstants.COLORMAP_RAW){
            
        }
        
        //read image datas
        throw new UnimplementedException("Not supported yet.");
    }
    
}
