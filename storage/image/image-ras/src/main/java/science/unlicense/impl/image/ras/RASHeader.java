
package science.unlicense.impl.image.ras;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;


/**
 * Sun raster image header.
 * 
 * @author Johann Sorel
 */
public class RASHeader {
    
    /** 
     * Image width.
     */
    public int width;
    /** 
     * Image height.
     */
    public int height;
    /** 
     * Image pixel depth.
     * possible values : 1,8,24,32
     */
    public int depth;
    /**
     * Image data size in bytes.
     */
    public int length;
    /**
     * Type of image.
     * One of RASConstants.TYPE_X
     */
    public int type;
    /**
     * Color map type.
     * One of RASConstants.COLORMAP_X
     */
    public int colormaptype;
    /**
     * Color map size in bytes.
     */
    public int colormaplength;
    
    public void read(DataInputStream ds) throws IOException{
        width           = ds.readInt();
        height          = ds.readInt();
        depth           = ds.readInt();
        length          = ds.readInt();
        type            = ds.readInt();
        colormaptype    = ds.readInt();
        colormaplength  = ds.readInt();
    }
    
    public void write(DataOutputStream ds) throws IOException{
        ds.writeInt(width);
        ds.writeInt(height);
        ds.writeInt(depth);
        ds.writeInt(length);
        ds.writeInt(type);
        ds.writeInt(colormaptype);
        ds.writeInt(colormaplength);
    }
    
}
