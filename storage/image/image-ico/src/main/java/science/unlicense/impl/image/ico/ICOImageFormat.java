
package science.unlicense.impl.image.ico;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;

/**
 * Resources :
 * http://en.wikipedia.org/wiki/ICO_(file_format)
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/18/10077133.aspx
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/19/10077610.aspx
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/21/10078690.aspx
 * http://blogs.msdn.com/b/oldnewthing/archive/2010/10/22/10079192.aspx
 *
 * @author Johann Sorel
 */
public class ICOImageFormat extends AbstractImageFormat{

    public ICOImageFormat() {
        super(new Chars("ico"),
              new Chars("ICO"),
              new Chars("ICO file format"),
              new Chars[]{
                  new Chars("image/x-icon"),
                  new Chars("image/ico"),
                  new Chars("image/icon"),
                  new Chars("image/vnd.microsoft.icon"),
                  new Chars("text/ico"),
                  new Chars("application/ico")
              },
              new Chars[]{
                  new Chars("ico")
              },
              new byte[][]{ICOConstants.SIGNATURE_ICO});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new ICOImageReader(ICOConstants.SIGNATURE_ICO);
    }

    public ImageWriter createWriter() {
        return null;
    }

}
