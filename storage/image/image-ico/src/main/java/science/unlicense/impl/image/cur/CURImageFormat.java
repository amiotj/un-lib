
package science.unlicense.impl.image.cur;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.AbstractImageFormat;
import science.unlicense.api.image.ImageReader;
import science.unlicense.api.image.ImageWriter;
import science.unlicense.impl.image.ico.ICOConstants;
import science.unlicense.impl.image.ico.ICOImageReader;

/**
 *
 * @author Johann Sorel
 */
public class CURImageFormat extends AbstractImageFormat{

    public CURImageFormat() {
        super(new Chars("cur"),
              new Chars("CUR"),
              new Chars("CUR file format"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("cur")
              },
              new byte[][]{ICOConstants.SIGNATURE_CUR});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public ImageReader createReader() {
        return new ICOImageReader(ICOConstants.SIGNATURE_CUR);
    }

    public ImageWriter createWriter() {
        return null;
    }

}
