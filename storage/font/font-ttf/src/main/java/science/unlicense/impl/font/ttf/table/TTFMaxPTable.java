
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'maxp' table establishes the memory requirements for a font.
 * 
 * @author Johann Sorel
 */
public class TTFMaxPTable extends TTFTable{
    
    /** Fixed (1.0) */
    public int version = 0x00010000;
    /** uint16 : the number of glyphs in the font */
    public int numGlyphs;
    /** uint16 : points in non-compound glyph */
    public int maxPoints;
    /** uint16 : contours in non-compound glyph */
    public int maxContours;
    /** unit16 : points in compound glyph */
    public int maxComponentPoints;
    /** uint16 : contours in compound glyph */
    public int maxComponentContours;
    /** uint16 : set to 2 */
    public int maxZones;
    /** uint16 : points used in Twilight Zone (Z0) */
    public int maxTwilightPoints;
    /** uint16 : number of Storage Area locations */
    public int maxStorage;
    /** uint16 : number of FDEFs */
    public int maxFunctionDefs;
    /** uint16 : number of IDEFs */
    public int maxInstructionDefs;
    /** uint16 : maximum stack depth */
    public int maxStackElements;
    /** uint16 : byte count for glyph instructions */
    public int maxSizeOfInstructions;
    /** uint16 : number of glyphs referenced at top level */
    public int maxComponentElements;
    /** uint16 : levels of recursion, set to 0 if font has only simple glyphs */
    public int maxComponentDepth;

    public TTFMaxPTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_MAXP);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        version                 = ds.readInt();
        numGlyphs               = ds.readUShort();
        maxPoints               = ds.readUShort();
        maxContours             = ds.readUShort();
        maxComponentPoints      = ds.readUShort();
        maxComponentContours    = ds.readUShort();
        maxZones                = ds.readUShort();
        maxTwilightPoints       = ds.readUShort();
        maxStorage              = ds.readUShort();
        maxFunctionDefs         = ds.readUShort();
        maxInstructionDefs      = ds.readUShort();
        maxStackElements        = ds.readUShort();
        maxSizeOfInstructions   = ds.readUShort();
        maxComponentElements    = ds.readUShort();
        maxComponentDepth       = ds.readUShort();
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        sb.append("-numGlyphs=")             .append(numGlyphs).append('\n');
        sb.append("-maxPoints=")             .append(maxPoints).append('\n');
        sb.append("-maxContours=")           .append(maxContours).append('\n');
        sb.append("-maxComponentPoints=")    .append(maxComponentPoints).append('\n');
        sb.append("-maxComponentContours=")  .append(maxComponentContours).append('\n');
        sb.append("-maxZones=")              .append(maxZones).append('\n');
        sb.append("-maxTwilightPoints=")     .append(maxTwilightPoints).append('\n');
        sb.append("-maxStorage=")            .append(maxStorage).append('\n');
        sb.append("-maxFunctionDefs=")       .append(maxFunctionDefs).append('\n');
        sb.append("-maxInstructionDefs=")    .append(maxInstructionDefs).append('\n');
        sb.append("-maxStackElements=")      .append(maxStackElements).append('\n');
        sb.append("-maxSizeOfInstructions=") .append(maxSizeOfInstructions).append('\n');
        sb.append("-maxComponentElements=")  .append(maxComponentElements).append('\n');
        sb.append("-maxComponentDepth=")     .append(maxComponentDepth).append('\n');
        return sb.toChars();
    }
    
}
