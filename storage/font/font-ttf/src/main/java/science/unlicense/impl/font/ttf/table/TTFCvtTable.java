
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'cvt ' table is optional. It can be used by fonts that contain instructions. 
 * It contains an array of FWords that can be accessed by instructions. 
 * The 'cvt ' is used to tie together certain font features when their 
 * values are sufficiently close to the table value.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFCvtTable extends TTFTable{
    
    public TTFCvtTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_CVT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
