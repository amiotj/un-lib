
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * This table contains information which describes the preferred rasterization 
 * techniques for the typeface when it is rendered on grayscale-capable devices. 
 * This table also has some use for monochrome devices, which may use the table to turn 
 * off hinting at very large or small sizes, to improve performance.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFGaspTable extends TTFTable{
    
    public TTFGaspTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_GASP);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
