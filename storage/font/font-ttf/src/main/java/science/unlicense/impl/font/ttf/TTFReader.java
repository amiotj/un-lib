
package science.unlicense.impl.font.ttf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import static science.unlicense.impl.font.ttf.TTFConstants.*;
import static science.unlicense.impl.font.otf.OTFConstants.*;
import science.unlicense.impl.font.otf.table.*;
import science.unlicense.impl.font.ttf.table.*;

/**
 * TTF Reader.
 *
 * @author Johann Sorel
 */
public class TTFReader extends AbstractReader{

    private TrueTypeFont font;

    public TrueTypeFont read() throws IOException {

        DataInputStream ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);

        font = new TrueTypeFont();
        font.path = input;
        font.scalerType     = ds.readInt();
        final int numTables = ds.readUShort();
        font.searchRange    = ds.readUShort();
        font.entrySelector  = ds.readUShort();
        font.rangeShift     = ds.readUShort();

        for (int i=0; i<numTables; i++) {
            TTFTable table = readTable(ds);
            font.tables.add(table);
        }

        return font;
    }

    private TTFTable readTable(final DataInputStream ds) throws IOException{

        final byte[] tagbuffer = new byte[4];
        ds.readFully(tagbuffer);
        Chars tag = new Chars(tagbuffer);

        final TTFTable table;

        //TTF tables
        if (TAG_ACNT.equals(tag)) {            table = new TTFAcntTable(font);
        } else if (TAG_AVAR.equals(tag)) {     table = new TTFAvarTable(font);
        } else if (TAG_BDAT.equals(tag)) {     table = new TTFBdatTable(font);
        } else if (TAG_BHED.equals(tag)) {     table = new TTFBhedTable(font);
        } else if (TAG_BLOC.equals(tag)) {     table = new TTFBlocTable(font);
        } else if (TAG_BSLN.equals(tag)) {     table = new TTFBslnTable(font);
        } else if (TAG_CMAP.equals(tag)) {     table = new TTFCmapTable(font);
        } else if (TAG_CVAR.equals(tag)) {     table = new TTFCvarTable(font);
        } else if (TAG_CVT .equals(tag)) {     table = new TTFCvtTable(font);
        } else if (TAG_FDSC.equals(tag)) {     table = new TTFFdscTable(font);
        } else if (TAG_FEAT.equals(tag)) {     table = new TTFFeatTable(font);
        } else if (TAG_FMTX.equals(tag)) {     table = new TTFFmtxTable(font);
        } else if (TAG_FPGM.equals(tag)) {     table = new TTFFpgmTable(font);
        } else if (TAG_FVAR.equals(tag)) {     table = new TTFFvarTable(font);
        } else if (TAG_GASP.equals(tag)) {     table = new TTFGaspTable(font);
        } else if (TAG_GLYF.equals(tag)) {     table = new TTFGlyfTable(font);
        } else if (TAG_GVAR.equals(tag)) {     table = new TTFGvarTable(font);
        } else if (TAG_HDMX.equals(tag)) {     table = new TTFHdmxTable(font);
        } else if (TAG_HEAD.equals(tag)) {     table = new TTFHeadTable(font);
        } else if (TAG_HHEA.equals(tag)) {     table = new TTFHheaTable(font);
        } else if (TAG_HMTX.equals(tag)) {     table = new TTFHmtxTable(font);
        } else if (TAG_HSTY.equals(tag)) {     table = new TTFHstyTable(font);
        } else if (TAG_JUST.equals(tag)) {     table = new TTFJustTable(font);
        } else if (TAG_KERN.equals(tag)) {     table = new TTFKernTable(font);
        } else if (TAG_LCAR.equals(tag)) {     table = new TTFLcarTable(font);
        } else if (TAG_LOCA.equals(tag)) {     table = new TTFLocaTable(font);
        } else if (TAG_MAXP.equals(tag)) {     table = new TTFMaxPTable(font);
        } else if (TAG_MORT.equals(tag)) {     table = new TTFMortTable(font);
        } else if (TAG_MORX.equals(tag)) {     table = new TTFMorxTable(font);
        } else if (TAG_NAME.equals(tag)) {     table = new TTFNameTable(font);
        } else if (TAG_OPBD.equals(tag)) {     table = new TTFOpbdTable(font);
        } else if (TAG_OS2 .equals(tag)) {     table = new TTFOs2Table(font);
        } else if (TAG_POST.equals(tag)) {     table = new TTFPostTable(font);
        } else if (TAG_PREP.equals(tag)) {     table = new TTFPrepTable(font);
        } else if (TAG_PROP.equals(tag)) {     table = new TTFPropTable(font);
        } else if (TAG_TRAK.equals(tag)) {     table = new TTFTrakTable(font);
        } else if (TAG_VHEA.equals(tag)) {     table = new TTFVheaTable(font);
        } else if (TAG_VMTX.equals(tag)) {     table = new TTFVmtxTable(font);
        } else if (TAG_ZAPF.equals(tag)) {     table = new TTFZapfTable(font);

        //OTF tables
        } else if (TAG_BASE.equals(tag)) {     table = new OTFBASETable(font);
        } else if (TAG_CFF.equals(tag))  {     table = new OTFCFFTable(font);
        } else if (TAG_DSIG.equals(tag)) {     table = new OTFDSIGTable(font);
        } else if (TAG_EBDT.equals(tag)) {     table = new OTFEBDTTable(font);
        } else if (TAG_EBLC.equals(tag)) {     table = new OTFEBLCTable(font);
        } else if (TAG_EBSC.equals(tag)) {     table = new OTFEBSCTable(font);
        } else if (TAG_GDEF.equals(tag)) {     table = new OTFGDEFTable(font);
        } else if (TAG_GPOS.equals(tag)) {     table = new OTFGPOSTable(font);
        } else if (TAG_GSUB.equals(tag)) {     table = new OTFGSUBTable(font);
        } else if (TAG_JSTF.equals(tag)) {     table = new OTFJSTFTable(font);
        } else if (TAG_LTSH.equals(tag)) {     table = new OTFLTSHTable(font);
        } else if (TAG_PCLT.equals(tag)) {     table = new OTFPCLTTable(font);
        } else if (TAG_VDMX.equals(tag)) {     table = new OTFVDMXTable(font);
        } else if (TAG_VORG.equals(tag)) {     table = new OTFVORGTable(font);

        //unknowned table
        } else {                               table = new TTFTable(font,tag);
        }
        table.readHeader(ds);

        return table;
    }

}
