
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'bhed' table contains global information about a bitmap font. 
 * It records such facts as the font version number, the creation and modification dates, 
 * revision number and basic typographic data that applies to the font as a whole.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFBhedTable extends TTFTable{
    
    public TTFBhedTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_BHED);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
