
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The name table(tag name: 'name') allows you to include human-readable 
 * names for features and settings, copyright notices, font names, 
 * style names, and other information related to your font.
 * 
 * @author Johann Sorel
 */
public class TTFNameTable extends TTFTable{

    public static class NamedRecord{
        /** UInt16 : Platform identifier code.*/
        public int platformID;
        /** UInt16 : Platform-specific encoding identifier.*/
        public int platformSpecificID;
        /** UInt16 : Language identifier.*/
        public int languageID;
        /** UInt16 : Name identifiers.*/
        public int nameID;
        /** UInt16 : Name string length in bytes.*/
        public int length;
        /** UInt16 : Name string offset in bytes from stringOffset.*/
        public int offset;
        /** added for convinience : decoded name */
        public Chars value;

        public Chars toChars() {
            return new Chars("NamedRecord["+platformID+","+platformSpecificID+","+languageID+","+nameID+","+length+","+offset+","+value+"]");
        }
    }
    
    /** UInt16 : Format selector. Set to 0. */
    public int format;
    /** UInt16 : The number of nameRecords in this name table.*/
    public int count;
    /** UInt16 : Offset in bytes to the beginning of the name character strings.*/
    public int stringOffset;
    /** NameRecord Array : The name records array.*/
    public NamedRecord[] nameRecord;
    /** variable : character strings The character strings of the names. 
     * Note that these are not necessarily ASCII!*/
    public byte[] namesbuffer;	
    
    public TTFNameTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_NAME);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        format = ds.readUShort();
        count = ds.readUShort();
        stringOffset = ds.readUShort();
        nameRecord = new NamedRecord[count];
        int currentOffset = 6;
        int bufferEnd = 0;
        for(int i=0;i<nameRecord.length;i++){
            final NamedRecord rec = new NamedRecord();
            rec.platformID = ds.readUShort();
            rec.platformSpecificID = ds.readUShort();
            rec.languageID = ds.readUShort();
            rec.nameID = ds.readUShort();
            rec.length = ds.readUShort();
            rec.offset = ds.readUShort();
            nameRecord[i] = rec;
            currentOffset += 12;
            bufferEnd = Math.max(bufferEnd, rec.offset+rec.length);
        }
        if(currentOffset<stringOffset){ //skip any remaining bytes to names buffer
            ds.readFully(new byte[stringOffset-currentOffset]);
        }
        namesbuffer = ds.readFully(new byte[bufferEnd]);
        
        //rebuild names, TODO handle encoding based on platform and language
        for(int i=0;i<nameRecord.length;i++){
            final NamedRecord rec = nameRecord[i];
            rec.value = new Chars(Arrays.copy(namesbuffer, rec.offset, rec.length, new byte[rec.length], 0));
        }
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        sb.append("-format=").append(format).append('\n');
        sb.append("-count=").append(count).append('\n');
        sb.append("-stringOffset=").append(stringOffset).append('\n');
        sb.append("-nameRecord=[...]:").append(nameRecord.length).append('\n');
        for(int i=0;i<nameRecord.length;i++){
            sb.append("  ["+i+"]");
            sb.append(nameRecord[i].toChars());
            sb.append('\n');
        }
        return sb.toChars();
    }
    
}
