
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'post' table contains information needed to use a TrueType font 
 * on a PostScript printer. It contains the data needed for the FontInfo dictionary 
 * entry as well as the PostScript names for all of the glyphs in the font.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFPostTable extends TTFTable{
    
    public TTFPostTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_POST);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
