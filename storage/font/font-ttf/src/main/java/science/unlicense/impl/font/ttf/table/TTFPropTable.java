
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The glyph properties table(tag name: 'prop') allows you to create Apple 
 * Advanced Typography (AAT) fonts with different properties associated with each glyph.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFPropTable extends TTFTable{
    
    public TTFPropTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_PROP);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
