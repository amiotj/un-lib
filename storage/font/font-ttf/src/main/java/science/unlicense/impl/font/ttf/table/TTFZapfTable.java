
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * By collecting all this information into one placeÑthe 'Zapf' table—named 
 * with permission after legendary type designer Hermann Zapf—can make 
 * it much easier for the user interface parts of applications to get what 
 * they need to present sensible choices to their users. 
 * This document proposes a format for this table which represents all this information in a relatively compact form.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFZapfTable extends TTFTable{
    
    public TTFZapfTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_ZAPF);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
