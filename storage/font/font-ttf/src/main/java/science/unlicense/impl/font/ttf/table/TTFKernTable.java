
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'kern' table contains the values that adjust the intercharacter 
 * spacing for glyphs in a font. It can have multiple subtables in varied 
 * formats that can contain information for vertical or horizontal text. 
 * Kerning values are used to adjust intercharacter spacing.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFKernTable extends TTFTable{
    
    public TTFKernTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_KERN);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
