
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The CVT variations table (tag name: 'cvar') allows you to include all 
 * of the data required for stylizing the CVT to match the styling done to outlines. 
 * This table mirrors the glyph variation table in that it contains the changes needed 
 * to create a style for any location in the font's style space.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFCvarTable extends TTFTable{
    
    public TTFCvarTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_CVAR);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
