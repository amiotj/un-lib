
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The Font Metrics Table (tag: 'fmtx') identifies a glyph whose points 
 * represent various font-wide metrics: ascent, descent, caret angle, caret offset.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFFmtxTable extends TTFTable{
    
    public TTFFmtxTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FMTX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
