
package science.unlicense.impl.font.ttf;

import science.unlicense.api.painter2d.AbstractFontMetadata;
import science.unlicense.api.character.Char;
import science.unlicense.api.io.IOException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.impl.font.ttf.table.TTFHeadTable;
import science.unlicense.impl.font.ttf.table.TTFHheaTable;
import science.unlicense.impl.font.ttf.table.TTFHmtxTable;

/**
 * TTF font metadata.
 * 
 * @author Johann Sorel
 */
public class TTFFontMetadata extends AbstractFontMetadata{

    private final TrueTypeFont font;
    private final double spaceWidth;
    
    public TTFFontMetadata(TrueTypeFont font) throws IOException {
        this.font = font;
        final TTFHeadTable head = (TTFHeadTable) font.getTable(TTFConstants.TAG_HEAD);
        final TTFHheaTable hhea = (TTFHheaTable) font.getTable(TTFConstants.TAG_HHEA);

        final BBox bbox = new BBox(2);
        bbox.setRange(0, head.xMin, head.xMax);
        bbox.setRange(1, head.yMin, head.yMax);
        setGlyphBox(bbox);
        setAdvanceWidthMax(hhea.advanceWidthMax);
        setAscent(hhea.ascent);
        setDescent(hhea.descent);
        setLineGap(hhea.lineGap);
        setMinLeftSideBearing(hhea.minLeftSideBearing);
        setMinRightSideBearing(hhea.minRightSideBearing);
        setxMaxExtent(hhea.xMaxExtent);
        setAdvanceWidthMax(hhea.advanceWidthMax);
        
        spaceWidth = bbox.getSpan(0);
    }

    public double getAdvanceWidth(int cp) {
        if(cp==0){
            return spaceWidth;
        }
        
        try {
            int glyphIndex = font.findGlyphIndex(cp);
            if(glyphIndex<0){
                //no glyph
                return 0;
            }else if(glyphIndex==0){
                //TODO : the empty glyph, the advance is often a strange value, bug, misinterpretation ?
                //return the glyphbox span
                return glyphBox.getSpan(0);
            }
        
            final TTFHmtxTable hmtx = (TTFHmtxTable) font.getTable(TTFConstants.TAG_HMTX);
            return hmtx.hMetrics[glyphIndex].advanceWidth;
        } catch (IOException ex) {
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
            return 0;
        }
    }
    
}
