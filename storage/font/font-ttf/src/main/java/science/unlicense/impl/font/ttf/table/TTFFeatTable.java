
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Thefeature name table(tag name: 'feat') allows you to include the 
 * font's text features, the settings for each text feature, and the 
 * name table indices for common (human-readable) names for the features and settings.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFFeatTable extends TTFTable{
    
    public TTFFeatTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FEAT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
