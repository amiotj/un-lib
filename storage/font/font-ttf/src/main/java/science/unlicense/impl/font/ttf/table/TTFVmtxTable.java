
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The vertical metrics table (tag name: 'vmtx') allows you to specify 
 * the vertical spacing for each glyph in a Quickdraw GX vertical font.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFVmtxTable extends TTFTable{
    
    public TTFVmtxTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_VMTX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
