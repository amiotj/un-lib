
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The optical bounds table (tag name: 'opbd') allows you to design Quickdraw 
 * GX fonts that contains information identifying the optical edges of glyphs. 
 * This information is used to make the edges of lines of text line up in a more visually pleasing way.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFOpbdTable extends TTFTable{
    
    public TTFOpbdTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_OPBD);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
