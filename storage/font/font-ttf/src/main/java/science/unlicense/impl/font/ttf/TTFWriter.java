
package science.unlicense.impl.font.ttf;

import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * TTF Writer.
 * 
 * @author Johann Sorel
 */
public class TTFWriter extends AbstractWriter {

    public void write(TrueTypeFont font) throws IOException{

        final DataOutputStream ds = getOutputAsDataStream(NumberEncoding.BIG_ENDIAN);

        final int numTables = font.tables.getSize();
        ds.writeInt(font.scalerType);
        ds.writeUShort(numTables);
        ds.writeUShort(font.searchRange);
        ds.writeUShort(font.entrySelector);
        ds.writeUShort(font.rangeShift);

        int offset = 12;

        for (int i=0; i<numTables; i++) {
            TTFTable table = (TTFTable) font.tables.get(i);
            offset += table.write(ds,offset);
        }
    }

}
