
package science.unlicense.impl.font.ttf;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.cryptography.hash.CRC32;
import science.unlicense.impl.cryptography.hash.HashOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class TTFTable extends CObject {

    public final TrueTypeFont font;
    public final Chars name;
    public int checksum;
    public int offset;
    public int length;

    private boolean loaded = false;

    public TTFTable(TrueTypeFont font,Chars name) {
        this.font = font;
        this.name = name;
    }

    public final TTFTable load() throws IOException{
        if(loaded) return this;
        final DataInputStream ds = font.openNewStream();
        ds.skip(offset);
        readData(ds);
        ds.close();
        return this;
    }

    public final boolean isLoaded() {
        return loaded;
    }

    public final void readHeader(DataInputStream ds) throws IOException{
        checksum = ds.readInt();
        offset = ds.readInt();
        length = ds.readInt();
    }

    public void readData(DataInputStream ds) throws IOException{
        loaded = true;
    }

    public int write(DataOutputStream ds, int offset) throws IOException{
        final ArrayOutputStream bds = new ArrayOutputStream();
        final HashOutputStream cos = new HashOutputStream(new CRC32(), bds);
        final DataOutputStream ads = new DataOutputStream(cos);
        writeData(ads);
        ads.flush();

        final byte[] buffer = bds.getBuffer().getBackArray();
        final int size = bds.getBuffer().getSize();

        ds.writeInt((int)cos.hashValue());
        ds.writeInt(offset+12);
        ds.writeInt(size);
        ds.write(buffer,0,size);

        return size+12;
    }

    public void writeData(DataOutputStream ds) throws IOException{
        throw new IOException("Writing table "+name+" not supported yet.");
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append(name);
        sb.append('[');
        sb.append("cs:").append(checksum).append(',');
        sb.append("offset:").append(offset).append(',');
        sb.append("length:").append(length);
        sb.append(']');
        return sb.toChars();
    }

}
