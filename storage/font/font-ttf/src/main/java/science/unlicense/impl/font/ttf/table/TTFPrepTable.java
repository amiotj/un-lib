
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'prep' table stores the instructions that make up the control value program, 
 * a set of TrueType instructions that will be executed once when the font 
 * is first accessed and again whenever the font, point size or transformation matrix change
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFPrepTable extends TTFTable{
    
    public TTFPrepTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_PREP);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
