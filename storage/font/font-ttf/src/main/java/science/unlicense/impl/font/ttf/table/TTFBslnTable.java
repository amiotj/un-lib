
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The baseline table (tag name: 'bsln') allows you to design your AAT 
 * fonts to accommodate the automatic alignment of text to different baselines. 
 * All fonts have a natural baseline defined by the position of each glyph in the font's em-square.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFBslnTable extends TTFTable{
    
    public TTFBslnTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_BSLN);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
