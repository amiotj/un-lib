
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'OS/2' table consists of a set of metrics that are required 
 * by OS/2 and Windows. It is not used by the Mac OS.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFOs2Table extends TTFTable{
    
    public TTFOs2Table(TrueTypeFont font) {
        super(font,TTFConstants.TAG_OS2);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
