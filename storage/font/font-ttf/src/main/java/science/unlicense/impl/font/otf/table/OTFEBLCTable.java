
package science.unlicense.impl.font.otf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.otf.OTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * OTF Specification :
 * Embedded bitmap location data
 * 
 * http://www.microsoft.com/typography/otspec/eblc.htm
 * 
 * @author Johann Sorel
 */
public class OTFEBLCTable extends TTFTable{
    
    public OTFEBLCTable(TrueTypeFont font) {
        super(font,OTFConstants.TAG_EBLC);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
