
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The tracking table (tag: 'trak') allows you to design Quickdraw GX 
 * fonts which allow adjustment to normal interglyph spacing.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFTrakTable extends TTFTable{
    
    public TTFTrakTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_TRAK);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
