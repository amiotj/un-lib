
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The ligature caret table (tag name: 'lcar') allows you to design your 
 * Quickdraw GX fonts with division points inside of some or all of the ligatures. 
 * This allows you to specify how a ligature is to be divided by an application. 
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFLcarTable extends TTFTable{
    
    public TTFLcarTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_LCAR);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
