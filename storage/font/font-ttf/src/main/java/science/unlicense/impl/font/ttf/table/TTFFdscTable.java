
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The font descriptors table (tag name: 'fdsc') allows applications to 
 * take an existing run of text and allow the user to specify a new font 
 * family for that run of text. A new style run that best preserves the font 
 * style information using the new font family will be created by using 
 * the style information that you provide.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFFdscTable extends TTFTable{
    
    public TTFFdscTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_FDSC);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
