
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Theglyph metamorphosis table(tag name: 'mort') allows you to specify 
 * a set of transformations that can apply to the glyphs of your font.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFMortTable extends TTFTable{
    
    public TTFMortTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_MORT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
