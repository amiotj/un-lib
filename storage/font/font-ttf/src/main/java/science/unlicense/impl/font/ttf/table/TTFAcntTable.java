
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The accent attachment table (tag name: 'acnt') provides a space-efficient 
 * method of combining component glyphs into compound glyphs to form accents.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFAcntTable extends TTFTable{
    
    public TTFAcntTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_ACNT);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
