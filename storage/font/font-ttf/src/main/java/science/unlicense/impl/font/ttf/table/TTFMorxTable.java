
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * he 'morx' table structure is related to the 'mort' table structure; 
 * developers should be familiar with the latter.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFMorxTable extends TTFTable{
    
    public TTFMorxTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_MORX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
