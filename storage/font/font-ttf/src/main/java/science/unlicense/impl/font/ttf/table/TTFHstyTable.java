
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'hsty' table provides information about how to synthesize styles 
 * such as boldfacing and italics on the Newton OS. 
 * It is used to mimic the way in which PostScript will modify fonts when the requested style is not directly available.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFHstyTable extends TTFTable{
    
    public TTFHstyTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_HSTY);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
