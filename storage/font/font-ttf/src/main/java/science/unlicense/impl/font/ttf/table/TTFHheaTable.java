
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The 'hhea' table contains information needed to layout fonts whose 
 * characters are written horizontally, that is, either left 
 * to right or right to left.
 * 
 * @author Johann Sorel
 */
public class TTFHheaTable extends TTFTable{
    
    /** Fixed (1.0) */
    public int version = 0x00010000;
    /** FWord : Distance from baseline of highest ascender */
    public short ascent;
    /** FWord : Distance from baseline of lowest descender */
    public short descent;
    /** FWord : typographic line gap */
    public short lineGap;
    /** uFWord : must be consistent with horizontal metrics */
    public int advanceWidthMax;
    /** FWord : must be consistent with horizontal metrics */
    public short minLeftSideBearing;
    /** FWord : must be consistent with horizontal metrics */
    public short minRightSideBearing;
    /** FWord : max(lsb + (xMax-xMin)) */
    public short xMaxExtent;
    /** int16 : used to calculate the slope of the caret (rise/run) set to 1 for vertical caret */
    public short caretSlopeRise;
    /** int 16 : 0 for vertical */
    public short caretSlopeRun;
    /** FWord : set value to 0 for non-slanted fonts */
    public short caretOffset;
    /** int16 : set value to 0 */
    public short reserved1;
    /** int16 : set value to 0 */
    public short reserved2;
    /** int16 : set value to 0 */
    public short reserved3;
    /** int16 : set value to 0 */
    public short reserved4;
    /** int16 : 0 for current format */
    public short metricDataFormat;
    /** uint16 : number of advance widths in metrics table */
    public int numOfLongHorMetrics;

    public TTFHheaTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_HHEA);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
        version                 = ds.readInt();
        ascent                  = ds.readShort();
        descent                 = ds.readShort();
        lineGap                 = ds.readShort();
        advanceWidthMax         = ds.readUShort();
        minLeftSideBearing      = ds.readShort();
        minRightSideBearing     = ds.readShort();
        xMaxExtent              = ds.readShort();
        caretSlopeRise          = ds.readShort();
        caretSlopeRun           = ds.readShort();
        caretOffset             = ds.readShort();
        reserved1               = ds.readShort();
        reserved2               = ds.readShort();
        reserved3               = ds.readShort();
        reserved4               = ds.readShort();
        metricDataFormat        = ds.readShort();
        numOfLongHorMetrics     = ds.readUShort();
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        sb.append("-version=").append(version).append('\n');
        sb.append("-ascent=").append(ascent).append('\n');
        sb.append("-descent=").append(descent).append('\n');
        sb.append("-lineGap=").append(lineGap).append('\n');
        sb.append("-advanceWidthMax=").append(advanceWidthMax).append('\n');
        sb.append("-minLeftSideBearing=").append(minLeftSideBearing).append('\n');
        sb.append("-minRightSideBearing=").append(minRightSideBearing).append('\n');
        sb.append("-xMaxExtent=").append(xMaxExtent).append('\n');
        sb.append("-caretSlopeRise=").append(caretSlopeRise).append('\n');
        sb.append("-caretSlopeRun=").append(caretSlopeRun).append('\n');
        sb.append("-caretOffset=").append(caretOffset).append('\n');
        sb.append("-reserved1=").append(reserved1).append('\n');
        sb.append("-reserved2=").append(reserved2).append('\n');
        sb.append("-reserved3=").append(reserved3).append('\n');
        sb.append("-reserved4=").append(reserved4).append('\n');
        sb.append("-metricDataFormat=").append(metricDataFormat).append('\n');
        sb.append("-numOfLongHorMetrics=").append(numOfLongHorMetrics).append('\n');
        return sb.toChars();
    }
    
}
