
package science.unlicense.impl.font.otf;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class OTFConstants {
    
    /** PostScript font program (compact font format) */
    public static final Chars TAG_CFF = new Chars(new byte[]{'C','F','F',' '});
    /** Vertical Origin */
    public static final Chars TAG_VORG = new Chars(new byte[]{'V','O','R','G'});
    /** Embedded bitmap data */
    public static final Chars TAG_EBDT = new Chars(new byte[]{'E','B','D','T'});
    /** Embedded bitmap location data */
    public static final Chars TAG_EBLC = new Chars(new byte[]{'E','B','L','C'});
    /** Embedded bitmap scaling data */
    public static final Chars TAG_EBSC = new Chars(new byte[]{'E','B','S','C'});
    /** Baseline data */
    public static final Chars TAG_BASE = new Chars(new byte[]{'B','A','S','E'});
    /** Glyph definition data */
    public static final Chars TAG_GDEF = new Chars(new byte[]{'G','D','E','F'});
    /** Glyph positioning data */
    public static final Chars TAG_GPOS = new Chars(new byte[]{'G','P','O','S'});
    /** Glyph substitution data */
    public static final Chars TAG_GSUB = new Chars(new byte[]{'G','S','U','B'});
    /** Justification data */
    public static final Chars TAG_JSTF = new Chars(new byte[]{'J','S','T','F'});
    /** Digital signature */
    public static final Chars TAG_DSIG = new Chars(new byte[]{'D','S','I','G'});
    /** Linear threshold data */
    public static final Chars TAG_LTSH = new Chars(new byte[]{'L','T','S','H'});
    /** PCL 5 data */
    public static final Chars TAG_PCLT = new Chars(new byte[]{'P','C','L','T'});
    /** Vertical device metrics */
    public static final Chars TAG_VDMX = new Chars(new byte[]{'V','D','M','X'});
    
    private OTFConstants(){}
    
}
