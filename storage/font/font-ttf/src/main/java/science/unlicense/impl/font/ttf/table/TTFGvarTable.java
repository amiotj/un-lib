
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * Apple Advanced Typography style variations allow the font designer 
 * to build high quality styles into the typeface itself. 
 * This reduces the dependence on algorithmic styling in the graphics system. 
 * To include font variations in your font, you must also include the font variations table.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFGvarTable extends TTFTable{
    
    public TTFGvarTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_GVAR);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
