
package science.unlicense.impl.font.otf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.otf.OTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * OTF Specification :
 * Vertical device metrics
 * 
 * http://www.microsoft.com/typography/otspec/vdmx.htm
 * 
 * @author Johann Sorel
 */
public class OTFVDMXTable extends TTFTable{
    
    public OTFVDMXTable(TrueTypeFont font) {
        super(font,OTFConstants.TAG_VDMX);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
