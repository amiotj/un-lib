
package science.unlicense.impl.font.ttf;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class TTFConstants {
    
    /** accent attachment table */
    public static final Chars TAG_ACNT = new Chars(new byte[]{'a','c','n','t'});
    /** axis variation table */
    public static final Chars TAG_AVAR = new Chars(new byte[]{'a','v','a','r'});
    /** bitmap data table */
    public static final Chars TAG_BDAT = new Chars(new byte[]{'b','d','a','t'});
    /** bitmap font header table */
    public static final Chars TAG_BHED = new Chars(new byte[]{'b','h','e','d'});
    /** bitmap location table */
    public static final Chars TAG_BLOC = new Chars(new byte[]{'b','l','o','c'});
    /** baseline table */
    public static final Chars TAG_BSLN = new Chars(new byte[]{'b','s','l','n'});
    /** character code mapping table */
    public static final Chars TAG_CMAP = new Chars(new byte[]{'c','m','a','p'});
    /** CVT variation table */
    public static final Chars TAG_CVAR = new Chars(new byte[]{'c','v','a','r'});
    /** control value table */
    public static final Chars TAG_CVT = new Chars(new byte[]{'c','v','t',' '});
    /** font descriptor table  */
    public static final Chars TAG_FDSC = new Chars(new byte[]{'f','d','s','c'});
    /** layout feature table */
    public static final Chars TAG_FEAT = new Chars(new byte[]{'f','e','a','t'});
    /** font metrics table  */
    public static final Chars TAG_FMTX = new Chars(new byte[]{'f','m','t','x'});
    /** font program table */
    public static final Chars TAG_FPGM = new Chars(new byte[]{'f','p','g','m'});
    /** font variation table */
    public static final Chars TAG_FVAR = new Chars(new byte[]{'f','v','a','r'});
    /** grid-fitting and scan-conversion procedure table */
    public static final Chars TAG_GASP = new Chars(new byte[]{'g','a','s','p'});
    /** glyph outline table */
    public static final Chars TAG_GLYF = new Chars(new byte[]{'g','l','y','f'});
    /** glyph variation table */
    public static final Chars TAG_GVAR = new Chars(new byte[]{'g','v','a','r'});
    /** horizontal device metrics table */
    public static final Chars TAG_HDMX = new Chars(new byte[]{'h','d','m','x'});
    /** font header table */
    public static final Chars TAG_HEAD = new Chars(new byte[]{'h','e','a','d'});
    /** horizontal header table */
    public static final Chars TAG_HHEA = new Chars(new byte[]{'h','h','e','a'});
    /** horizontal metrics table */
    public static final Chars TAG_HMTX = new Chars(new byte[]{'h','m','t','x'});
    /** horizontal style table */
    public static final Chars TAG_HSTY = new Chars(new byte[]{'h','s','t','y'});
    /** justification table */
    public static final Chars TAG_JUST = new Chars(new byte[]{'j','u','s','t'});
    /** kerning table */
    public static final Chars TAG_KERN = new Chars(new byte[]{'k','e','r','n'});
    /** ligature caret table */
    public static final Chars TAG_LCAR = new Chars(new byte[]{'l','c','a','r'});
    /** glyph location table */
    public static final Chars TAG_LOCA = new Chars(new byte[]{'l','o','c','a'});
    /** maximum profile table */
    public static final Chars TAG_MAXP = new Chars(new byte[]{'m','a','x','p'});
    /** metamorphosis table */
    public static final Chars TAG_MORT = new Chars(new byte[]{'m','o','r','t'});
    /** extended metamorphosis table */
    public static final Chars TAG_MORX = new Chars(new byte[]{'m','o','r','x'});
    /** name table */
    public static final Chars TAG_NAME = new Chars(new byte[]{'n','a','m','e'});
    /** optical bounds table */
    public static final Chars TAG_OPBD = new Chars(new byte[]{'o','p','b','d'});
    /** compatibility table */
    public static final Chars TAG_OS2 = new Chars(new byte[]{'O','S','/','2'});
    /** glyph name and PostScript compatibility table */
    public static final Chars TAG_POST = new Chars(new byte[]{'p','o','s','t'});
    /** control value program table */
    public static final Chars TAG_PREP = new Chars(new byte[]{'p','r','e','p'});
    /** properties table */
    public static final Chars TAG_PROP = new Chars(new byte[]{'p','r','o','p'});
    /** tracking table */
    public static final Chars TAG_TRAK = new Chars(new byte[]{'t','r','a','k'});
    /** vertical header table */
    public static final Chars TAG_VHEA = new Chars(new byte[]{'v','h','e','a'});
    /** vertical metrics table */
    public static final Chars TAG_VMTX = new Chars(new byte[]{'v','m','t','x'});
    /** glyph reference table*/
    public static final Chars TAG_ZAPF = new Chars(new byte[]{'Z','a','p','f'});
        
    
    ////////////////////////////////////////////////////////////////////////////
    //// MASKS FOR SYMPLE GLYPH FLAGS //////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * On Curve
     * If set, the point is on the curve;
     * Otherwise, it is off the curve.
     */
    public static final byte SG_ONCURVE = 0x01;
    /**
     * x-Short Vector
     * If set, the corresponding x-coordinate is 1 byte long;
     * Otherwise, the corresponding x-coordinate is 2 bytes long
     */
    public static final byte SG_XSV = 0x02;
    /**
     * y-Short Vector
     * If set, the corresponding y-coordinate is 1 byte long;
     * Otherwise, the corresponding y-coordinate is 2 bytes long
     */
    public static final byte SG_YSV = 0x04;
    /**
     * Repeat
     * If set, the next byte specifies the number of additional times 
     * this set of flags is to be repeated. In this way, the number of flags 
     * listed can be smaller than the number of points in a character.
     */
    public static final byte SG_REPEAT = 0x08;
    /**
     * This x is same (Positive x-Short vector)
     * This flag has one of two meanings, depending on how the x-Short Vector flag is set.
     * If the x-Short Vector bit is set, this bit describes the sign of the value, with a value of 1 equalling positive and a zero value negative.
     * If the x-short Vector bit is not set, and this bit is set, then the current x-coordinate is the same as the previous x-coordinate.
     * If the x-short Vector bit is not set, and this bit is not set, the current x-coordinate is a signed 16-bit delta vector. In this case, the delta vector is the change in x
     */
    public static final byte SG_XMSK = 0x10;
    /**
     * This y is same (Positive y-Short vector)
     * This flag has one of two meanings, depending on how the y-Short Vector flag is set.
     * If the y-Short Vector bit is set, this bit describes the sign of the value, with a value of 1 equalling positive and a zero value negative.
     * If the y-short Vector bit is not set, and this bit is set, then the current y-coordinate is the same as the previous y-coordinate.
     * If the y-short Vector bit is not set, and this bit is not set, the current y-coordinate is a signed 16-bit delta vector. In this case, the delta vector is the change in y
     */
    public static final byte SG_YMSK = 0x20;

    private TTFConstants(){}
    
}
