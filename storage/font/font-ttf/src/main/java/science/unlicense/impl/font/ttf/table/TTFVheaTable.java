
package science.unlicense.impl.font.ttf.table;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.font.ttf.TTFConstants;
import science.unlicense.impl.font.ttf.TTFTable;
import science.unlicense.impl.font.ttf.TrueTypeFont;

/**
 * TTF Specification :
 * The vertical headertable (tag name: 'vhea') contains information needed 
 * for vertical fonts. The glyphs of vertical fonts are written either top to bottom or bottom to top.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class TTFVheaTable extends TTFTable{
    
    public TTFVheaTable(TrueTypeFont font) {
        super(font,TTFConstants.TAG_VHEA);
    }

    public void readData(DataInputStream ds) throws IOException {
        super.readData(ds);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer(super.toChars());
        sb.append('\n');
        return sb.toChars();
    }
    
}
