
package science.unlicense.impl.font.ttf.glyph;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.s2d.Curve;
import science.unlicense.impl.geometry.s2d.CurvePolygon;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.font.ttf.TTFConstants;

/**
 * TTF simple glyph.
 *
 * @author Johann Sorel
 */
public class SimpleGlyph extends Glyph {
    /** uint16 : Array of last points of each contour;
     * n is the number of contours; array entries are point indices*/
    public int[] endPtsOfContours;
    /** uint16 : Total number of bytes needed for instructions*/
    public int instructionLength;
    /** uint8 : Array of instructions for this glyph*/
    public byte[] instructions;
    /** uint8 : Array of flags*/
    public byte[] flags;
    /** uint8 or int16 : Array of x-coordinates;
     * the first is relative to (0,0), others are relative to previous point*/
    public short[] xCoordinates;
    /** uint8 or int16 : Array of y-coordinates;
     * the first is relative to (0,0), others are relative to previous point*/
    public short[] yCoordinates;

    public Geometry2D getGeometry() {

        if(endPtsOfContours.length==1){
            return toPath(0, endPtsOfContours[0]);
        }else{
            Path out = null;
            Sequence inners = new ArraySequence();
            for(int i=0;i<endPtsOfContours.length;i++){
                Path candidate;
                if(i==0){
                    candidate = toPath(0, endPtsOfContours[0]);
                }else{
                    candidate = toPath(endPtsOfContours[i-1]+1, endPtsOfContours[i]);
                }

                //TTF expect the outter path to be in clockwise direction
                final boolean cw = Geometries.isClockWise(candidate.createPathIterator());
                if(cw){
                    if(out != null){
                        // TODO what do we do in this case ?
                        //outer is often the last,let's consider thre previous out as a wrong inner
                        inners.add(out);
                        out = candidate;
                    }else{
                        out = candidate;
                    }
                }else{
                    inners.add(candidate);
                }
            }
            return new CurvePolygon(out, inners);
        }
    }

    private Path toPath(int start, int end){
        end = end+1;
        final Path path = new Path();

        int firstMoveTo = -1;
        double firstMoveToX = 0;
        double firstMoveToY = 0;
        boolean firstOnCurve = false;
        boolean previousOnCurve = true;
        for(int k=start;k<end;k++){
            if(k==start){
                firstOnCurve = (flags[k]&TTFConstants.SG_ONCURVE)==1;
            }

            if( (flags[k]&TTFConstants.SG_ONCURVE)==1 ){
                if(previousOnCurve){
                    //straight line
                    if(firstMoveTo==-1){
                        path.appendMoveTo(xCoordinates[k], yCoordinates[k]);
                        firstMoveTo = k;
                        firstMoveToX = xCoordinates[k];
                        firstMoveToY = yCoordinates[k];
                    }else{
                        path.appendLineTo(xCoordinates[k], yCoordinates[k]);
                    }
                }else{
                    //quadratic
                    if(firstMoveTo==-1){
                        path.appendMoveTo(xCoordinates[k], yCoordinates[k]);
                        firstMoveTo = k;
                        firstMoveToX = xCoordinates[k];
                        firstMoveToY = yCoordinates[k];
                    }else{
                        path.appendQuadTo(xCoordinates[k-1], yCoordinates[k-1], xCoordinates[k], yCoordinates[k]);
                    }
                }
                previousOnCurve = true;
            }else{
                if(previousOnCurve){
                    //do nothing, way for next point
                }else{
                    //two points one after another off the curve, interpolate a middle point
                    final double midx = (xCoordinates[k-1]+xCoordinates[k])/2;
                    final double midy = (yCoordinates[k-1]+yCoordinates[k])/2;
                    if(firstMoveTo==-1){
                        firstMoveTo = 1;
                        firstMoveToX = midx;
                        firstMoveToY = midy;
                        path.appendMoveTo(firstMoveToX, firstMoveToY);
                    }
                    path.appendQuadTo(xCoordinates[k-1], yCoordinates[k-1], midx, midy);

                }
                previousOnCurve = false;
            }

        }

        //close the path properly
        if(firstOnCurve){
            if(previousOnCurve){
                //straight line
                path.appendClose();
            }else{
                //we add a last quadratic back to first point
                int k = end-1;
                path.appendQuadTo(xCoordinates[k], yCoordinates[k], xCoordinates[start], yCoordinates[start]);
                path.appendClose();
            }
        }else{
            if(!previousOnCurve){
                //add quadratic over first coord
                final int k = end-1;
                final double midx = (xCoordinates[k]+xCoordinates[start])/2;
                final double midy = (yCoordinates[k]+yCoordinates[start])/2;
                path.appendQuadTo(xCoordinates[k], yCoordinates[k], midx, midy);
            }
            path.appendQuadTo(xCoordinates[start], yCoordinates[start], firstMoveToX, firstMoveToY);
            path.appendClose();
        }
        return path;
    }

}
