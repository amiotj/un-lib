
package science.unlicense.impl.font.ttf.glyph;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Matrix3x3;

/**
 * TTF compound glyph.
 * 
 * @author Johann Sorel
 */
public class CompoundGlyph extends Glyph {
    /** uint16 : Component flag*/
    public int flags;
    /** uint16 : Glyph index of component*/
    public int glyphIndex;
    /** int16, uint16, int8 or uint8 : X-offset for component or point number;
     * type depends on bits 0 and 1 in component flags*/
    public int argument1;
    /** int16, uint16, int8 or uint8 : Y-offset for component or point number
     * type depends on bits 0 and 1 in component flags*/
    public int argument2;
    /** transformation option One of the transformation options from Table 19*/
    public Matrix3x3 transformation;

    @Override
    public Geometry2D getGeometry() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
