
package science.unlicense.impl.font.ttf;

import science.unlicense.api.painter2d.AbstractFontMetadata;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;
import science.unlicense.api.collection.primitive.IntSet;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.path.Path;
import science.unlicense.api.painter2d.CachedFontMetadata;
import science.unlicense.system.path.Paths;
import science.unlicense.api.painter2d.FontStore;

/**
 * TTF font store.
 *
 * @author Johann Sorel
 */
public class TTFFontStore implements FontStore{

    public static final TTFFontStore INSTANCE = new TTFFontStore();
    
    private final Dictionary dico = new HashDictionary();
        
    public TTFFontStore() {
        try {
            registerFont(Paths.resolve(new Chars("mod:/font/Tuffy.ttf")));
            registerFont(Paths.resolve(new Chars("mod:/font/Mona.ttf")));
            registerFont(Paths.resolve(new Chars("mod:/font/Unicon.ttf")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Set getFamilies() {
        final Set s = new HashSet();
        s.addAll(dico.getKeys());
        return s;
    }

    public boolean containsFamily(Chars name) {
        return dico.getValue(name) != null;
    }

    public FontMetadata getMetadata(Chars family) {
        final FontFileInfo ffi = (FontFileInfo) dico.getValue(family);
        if(ffi==null)throw new InvalidArgumentException("No font for family name : "+family);
        return ffi.getMetadata();
    }

    public Geometry2D getGlyph(Char character, Chars family) {
        return getGlyph(character.toUnicode(), family);
    }
    
    public Geometry2D getGlyph(int cp, Chars family) {
        final FontFileInfo ffi = (FontFileInfo) dico.getValue(family);
        if(ffi==null) return null;
        Geometry2D g = (Geometry2D) ffi.glyphs.getValue(cp);
        if(g==null){
            g = ffi.getFont().getGlyph(cp);
            ffi.glyphs.add(cp, g);
        }
        return g;
    }

    public IntSet listCharacters(Chars family) {
        final FontFileInfo ffi = (FontFileInfo) dico.getValue(family);
        try {
            return ffi.font.listCharacters();
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Override
    public void registerFont(Path path) throws IOException {
        final Chars name = Paths.stripExtension(path.getName());
        dico.add(name, new FontFileInfo(path));
        
    }

    private static final class FontFileInfo{
        
        private final Path path;
        private TrueTypeFont font;
        private AbstractFontMetadata metadata;

        /**
         * Cache glyphs.
         */
        public final Dictionary glyphs = new HashDictionary();
    
        public FontFileInfo(Path path) {
            this.path = path;
        }

        public TrueTypeFont getFont() {
            if(font==null){
                TTFReader reader = new TTFReader();
                try {
                    reader.setInput(path);
                    font = reader.read();
                    reader.dispose();
                    metadata = new CachedFontMetadata(new TTFFontMetadata(font));
                } catch (IOException ex) {
                    Loggers.get().log(ex,Logger.LEVEL_WARNING);
                }
            }
            return font;
        }

        public AbstractFontMetadata getMetadata() {
            getFont();//ensure it is loaded
            return metadata;
        }
        
        
        
    }
    
}
