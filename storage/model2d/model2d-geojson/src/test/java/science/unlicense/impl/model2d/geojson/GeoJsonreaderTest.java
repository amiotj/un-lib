
package science.unlicense.impl.model2d.geojson;

import science.unlicense.impl.model2d.geojson.GeoJsonReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.impl.geometry.Point;

/**
 *
 * @author Johann Sorel
 */
public class GeoJsonreaderTest {

    /**
     * Read a collection test.
     */
    @Test
    public void featureCollectionTest() throws IOException {

        final Chars text = new Chars(
                "{\n" +
"       \"type\": \"FeatureCollection\",\n" +
"       \"features\": [{\n" +
"           \"type\": \"Feature\",\n" +
"           \"geometry\": {\n" +
"               \"type\": \"Point\",\n" +
"               \"coordinates\": [102.0, 0.5]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\"\n" +
"           }\n" +
"       }, {\n" +
"           \"type\": \"Feature\",\n" +
"           \"geometry\": {\n" +
"               \"type\": \"LineString\",\n" +
"               \"coordinates\": [\n" +
"                   [102.0, 0.0],\n" +
"                   [103.0, 1.0],\n" +
"                   [104.0, 0.0],\n" +
"                   [105.0, 1.0]\n" +
"               ]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\",\n" +
"               \"prop1\": 0.0\n" +
"           }\n" +
"       }, {\n" +
"           \"type\": \"Feature\",\n" +
"           \"geometry\": {\n" +
"               \"type\": \"Polygon\",\n" +
"               \"coordinates\": [\n" +
"                   [\n" +
"                       [100.0, 0.0],\n" +
"                       [101.0, 0.0],\n" +
"                       [101.0, 1.0],\n" +
"                       [100.0, 1.0],\n" +
"                       [100.0, 0.0]\n" +
"                   ]\n" +
"               ]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\",\n" +
"               \"prop1\": {\n" +
"                   \"this\": \"that\"\n" +
"               }\n" +
"           }\n" +
"       }]\n" +
"   }"
        );

        GeoJsonReader reader = new GeoJsonReader();
        reader.setInput(text.toBytes());

        Assert.assertTrue(reader.hasNext());
        Document doc1 = reader.next();
        Assert.assertTrue(reader.hasNext());
        Document doc2 = reader.next();
        Assert.assertTrue(reader.hasNext());
        Document doc3 = reader.next();
        Assert.assertFalse(reader.hasNext());


    }

    /**
     * Read a single feature test.
     */
    @Test
    public void featureTest() throws IOException {

        final Chars text = new Chars(
            "{\n" +
"           \"type\": \"Feature\",\n" +
"           \"id\": \"FID-123\",\n" +
"           \"bbox\": [-10.0, -20.0, 10.0, 20.0],\n" +
"           \"geometry\": {\n" +
"               \"type\": \"Point\",\n" +
"               \"coordinates\": [102.0, 0.5]\n" +
"           },\n" +
"           \"properties\": {\n" +
"               \"prop0\": \"value0\"\n" +
"           },\n" +
"           \"extra\": \"something\"\n" +
"       }"
        );

        GeoJsonReader reader = new GeoJsonReader();
        reader.setInput(text.toBytes());

        Assert.assertTrue(reader.hasNext());
        Document doc = reader.next();
        Assert.assertEquals(new Chars("FID-123"), doc.getFieldValue(new Chars("id")));
        Assert.assertEquals(new BBox(new double[]{-10,-20}, new double[]{10,20}), doc.getFieldValue(new Chars("bbox")));
        Assert.assertEquals(new Point(102, 0.5), doc.getFieldValue(new Chars("geometry")));
        Assert.assertEquals(new Chars("value0"), doc.getFieldValue(new Chars("prop0")));
        Assert.assertEquals(new Chars("something"), doc.getFieldValue(new Chars("extra")));

        Assert.assertFalse(reader.hasNext());

    }

}
