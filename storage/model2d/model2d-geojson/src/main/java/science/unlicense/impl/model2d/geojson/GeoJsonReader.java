
package science.unlicense.impl.model2d.geojson;

import java.lang.reflect.Array;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.Reader;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.Documents;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.MultiGeometry;
import science.unlicense.impl.geometry.s2d.MultiPoint;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.MultiPolyline;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.binding.json.JSONReader;
import science.unlicense.impl.binding.json.JSONUtilities;
import static science.unlicense.impl.model2d.geojson.GeoJsonConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class GeoJsonReader implements Reader {

    private final JSONReader reader;
    private Iterator features;

    public GeoJsonReader() {
        this.reader = new JSONReader();
    }

    @Override
    public void setInput(Object input) throws IOException {
        reader.setInput(input);
    }

    @Override
    public void setConfiguration(Document configuration) {
        reader.setConfiguration(configuration);
    }

    @Override
    public Document getConfiguration() {
        return reader.getConfiguration();
    }

    @Override
    public Object getInput() {
        return reader.getInput();
    }

    public boolean hasNext() throws IOException {
        init();
        return features.hasNext();
    }

    public Document next() throws IOException {
        init();
        return (Document) features.next();
    }

    private void init() throws IOException {
        if(features!=null) return;

        final Sequence all = new ArraySequence();
        //TODO make a progressive reader
        Document fullDoc = JSONUtilities.readAsDocument(reader);
        Object type = fullDoc.getFieldValue(PROP_TYPE);
        if (TYPE_FEATURECOLLECTION.equals(type)) {
            Object[] values = (Object[]) fullDoc.getFieldValue(PROP_FEATURES);
            for(Object o : values) {
                all.add(toFeature((Document) o));
            }
        } else if(TYPE_FEATURE.equals(type)) {
            all.add(toFeature(fullDoc));
        }

        this.features = all.createIterator();
    }

    private Document toFeature(Document jsonDoc) throws IOException {

        jsonDoc.setFieldValue(PROP_TYPE, null);

        //convert bbox
        Object jsonBbox = jsonDoc.getFieldValue(PROP_BBOX);
        if (jsonBbox != null) {
            final Object[] values = (Object[]) jsonBbox;
            if (values.length==4) {
                BBox bbox = new BBox(2);
                bbox.setRange(0, ((Number)values[0]).doubleValue(), ((Number)values[2]).doubleValue());
                bbox.setRange(1, ((Number)values[1]).doubleValue(), ((Number)values[3]).doubleValue());
                jsonDoc.setFieldValue(PROP_BBOX, bbox);
            } else if(values.length==6) {
                BBox bbox = new BBox(3);
                bbox.setRange(0, ((Number)values[0]).doubleValue(), ((Number)values[3]).doubleValue());
                bbox.setRange(1, ((Number)values[1]).doubleValue(), ((Number)values[4]).doubleValue());
                bbox.setRange(2, ((Number)values[2]).doubleValue(), ((Number)values[5]).doubleValue());
                jsonDoc.setFieldValue(PROP_BBOX, bbox);
            } else {
                throw new IOException("Unvalid bbox size "+Arrays.toChars(values));
            }
        }

        //convert geometry
        Document geometry = (Document) jsonDoc.getFieldValue(PROP_GEOMETRY);
        if (geometry!=null) {
            jsonDoc.setFieldValue(PROP_GEOMETRY, readGeometry(geometry));
        }

        //flatten properties
        Document properties = (Document) jsonDoc.getFieldValue(PROP_PROPERTIES);
        if (properties != null) {
            jsonDoc.setFieldValue(PROP_PROPERTIES, null);
            Documents.overrideFields(jsonDoc, properties);
        }

        return jsonDoc;
    }

    private Geometry readGeometry(Document geometry) throws IOException {
        Object geomType = geometry.getFieldValue(PROP_TYPE);
        Object coordinates = geometry.getFieldValue(PROP_COORDINATES);

        final Geometry2D geom;
        if (GEOM_POINT.equals(geomType)) {
            geom = readPoint(coordinates);
        } else if (GEOM_MULTIPOINT.equals(geomType)) {
            geom = readMultiPoint(coordinates);
        } else if (GEOM_LINESTRING.equals(geomType)) {
            geom = readLineString(coordinates);
        } else if (GEOM_MULTILINESTRING.equals(geomType)) {
            geom = readMultiLineString(coordinates);
        } else if (GEOM_POLYGON.equals(geomType)) {
            geom = readPolygon(coordinates);
        } else if (GEOM_MULTIPOLYGON.equals(geomType)) {
            geom = readMultiPolygon(coordinates);
        } else if (GEOM_GEOMETRYCOLLECTION.equals(geomType)) {
            Object subs = geometry.getFieldValue(PROP_GEOMETRIES);
            final int nbGeom = Array.getLength(subs);
            final Sequence geometries = new ArraySequence(nbGeom);
            for (int i=0;i<nbGeom;i++) {
                geometries.add(readGeometry((Document) Array.get(subs, i)));
            }
            geom = new MultiGeometry(geometries);

        } else {
            throw new IOException("Unexpected geometry type "+geomType);
        }

        return geom;
    }

    private Point readPoint(Object coordinates) {
        return new Point(readTuple(coordinates));
    }

    private MultiPoint readMultiPoint(Object coordinates) {
        final Sequence points = readTuples(coordinates);
        for (int i=0,n=points.getSize();i<n;i++) {
            points.replace(i, new Point((TupleRW) points.get(i)));
        }
        return new MultiPoint(points);
    }

    private Polyline readLineString(Object coordinates) {
        final double[] coords = readTuplesArray(coordinates);
        return new Polyline(new DefaultTupleBuffer1D(coords,tupleSize));
    }

    private MultiPolyline readMultiLineString(Object coordinates) {
        final int nbGeom = Array.getLength(coordinates);
        final Sequence geometries = new ArraySequence(nbGeom);
        for (int i=0;i<nbGeom;i++) {
            final double[] coords = readTuplesArray(Array.get(coordinates, i));
            geometries.add(new Polyline(new DefaultTupleBuffer1D(coords,tupleSize)));
        }
        return new MultiPolyline(geometries);
    }

    private Polygon readPolygon(Object coordinates) {
        final int nbGeom = Array.getLength(coordinates);
        double[] coords = readTuplesArray(Array.get(coordinates, 0));
        final Polyline outer = new Polyline(new DefaultTupleBuffer1D(coords, tupleSize));
        final Sequence geometries = new ArraySequence(nbGeom-1);
        for (int i=1;i<nbGeom;i++) {
            coords = readTuplesArray(Array.get(coordinates, i));
            geometries.add(new Polyline(new DefaultTupleBuffer1D(coords,tupleSize)));
        }
        return new Polygon(outer, geometries);
    }

    private MultiPolygon readMultiPolygon(Object coordinates) {
        final int nbGeom = Array.getLength(coordinates);
        final Sequence geometries = new ArraySequence(nbGeom);
        for (int i=0;i<nbGeom;i++) {
            geometries.add(readPolygon(Array.get(coordinates, i)));
        }
        return new MultiPolygon(geometries);
    }


    private static Sequence readTuples(Object array) {
        final int length = Array.getLength(array);
        final Sequence tuples = new ArraySequence(length);
        for (int i=0;i<length;i++) {
            tuples.add(readTuple(Array.get(array, i)));
        }
        return tuples;
    }

    private int tupleSize;
    private double[] readTuplesArray(Object array) {
        final int length = Array.getLength(array);
        double[] tuples = null;
        for (int i=0;i<length;i++) {
            Object tuple = Array.get(array, i);
            if(tuples==null) {
                tupleSize = Array.getLength(tuple);
                tuples = new double[length*tupleSize];
            }
            for (int k=0;k<tupleSize;k++) {
                tuples[i*tupleSize+k] = ((Number)Array.get(tuple, k)).doubleValue();
            }
        }

        return tuples;
    }

    private static DefaultTuple readTuple(Object array) {
        final int length = Array.getLength(array);
        final DefaultTuple t = new DefaultTuple(length);
        t.setXY(((Number)Array.get(array, 0)).doubleValue(), ((Number)Array.get(array, 1)).doubleValue());
        if(length>=3) t.setZ(((Number)Array.get(array, 2)).doubleValue());
        return t;
    }

    @Override
    public void dispose() throws IOException {
        reader.dispose();
    }

}
