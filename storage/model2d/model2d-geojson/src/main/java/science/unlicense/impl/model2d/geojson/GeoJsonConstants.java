

package science.unlicense.impl.model2d.geojson;

import science.unlicense.api.character.Chars;

/**
 * GeoJSON constants.
 * 
 * @author Johann Sorel
 */
public final class GeoJsonConstants {

    public static final Chars PROP_TYPE         = new Chars("type");
    public static final Chars PROP_ID           = new Chars("id");
    public static final Chars PROP_BBOX         = new Chars("bbox");
    public static final Chars PROP_GEOMETRY     = new Chars("geometry");
    public static final Chars PROP_PROPERTIES   = new Chars("properties");
    public static final Chars PROP_FEATURES     = new Chars("features");
    public static final Chars PROP_COORDINATES  = new Chars("coordinates");
    public static final Chars PROP_GEOMETRIES   = new Chars("geometries");

    public static final Chars TYPE_FEATURECOLLECTION    = new Chars("FeatureCollection");
    public static final Chars TYPE_FEATURE              = new Chars("Feature");

    public static final Chars GEOM_POINT                = new Chars("Point");
    public static final Chars GEOM_MULTIPOINT           = new Chars("MultiPoint");
    public static final Chars GEOM_LINESTRING           = new Chars("LineString");
    public static final Chars GEOM_MULTILINESTRING      = new Chars("MultiLineString");
    public static final Chars GEOM_POLYGON              = new Chars("Polygon");
    public static final Chars GEOM_MULTIPOLYGON         = new Chars("MultiPolygon");
    public static final Chars GEOM_GEOMETRYCOLLECTION   = new Chars("GeometryCollection");

    private GeoJsonConstants(){}
    
}
