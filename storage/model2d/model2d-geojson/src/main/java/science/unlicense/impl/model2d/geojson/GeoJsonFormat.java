

package science.unlicense.impl.model2d.geojson;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Graphtec Vector Graphic Data format.
 * 
 * Resource :
 * https://tools.ietf.org/html/rfc7946
 * http://geojson.org/geojson-spec.html
 * https://en.wikipedia.org/wiki/GeoJSON
 * 
 * @author Johann Sorel
 */
public class GeoJsonFormat extends DefaultFormat {

    public GeoJsonFormat() {
        super(new Chars("geojson"),
              new Chars("GeoJSON"),
              new Chars("Geographic data structures in JSON"),
              new Chars[]{
                  new Chars("application/vnd.geo+json")
              },
              new Chars[]{
                  new Chars("geojson")
              },
              new byte[][]{});
    }
        
}
