

package science.unlicense.impl.model2d.pdf;

/**
 * PDF constants.
 * 
 * @author Johann Sorel
 */
public final class PDFConstants {
    
    /**
     * PDF signature.
     */
    public static byte[] SIGNATURE = new byte[]{'%','P','D','F'};
    
    private PDFConstants(){}
    
}
