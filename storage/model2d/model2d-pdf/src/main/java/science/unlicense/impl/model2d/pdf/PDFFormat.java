

package science.unlicense.impl.model2d.pdf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * PDF format.
 * 
 * Resource : 
 * http://en.wikipedia.org/wiki/Portable_Document_Format
 * 
 * @author Johann Sorel
 */
public class PDFFormat extends DefaultFormat {

    public PDFFormat() {
        super(new Chars("PDF"),
              new Chars("PDF"),
              new Chars("Portable Document Format"),
              new Chars[]{
                  new Chars("application/pdf"),
                  new Chars("application/x-pdf"),
                  new Chars("application/x-bzpdf"),
                  new Chars("application/x-gzpdf"),
              },
              new Chars[]{
                  new Chars("pdf")
              },
              new byte[][]{PDFConstants.SIGNATURE});
    }
        
}
