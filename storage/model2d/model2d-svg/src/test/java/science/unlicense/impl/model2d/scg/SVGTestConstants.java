

package science.unlicense.impl.model2d.scg;

import science.unlicense.api.character.Chars;

/**
 * Constants used for read and write tests.
 * 
 * @author Johann Sorel
 */
public final class SVGTestConstants {
   
    public static final Chars TEST_DOCUMENT = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" width=\"800\" height=\"600\"></svg>");
    
    public static final Chars TEST_RECTANGLE = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<rect width=\"100\" height=\"200\" x=\"10\" y=\"20\" rx=\"12\" ry=\"24\"/>"
          + "</svg>");
    
    public static final Chars TEST_CIRCLE = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<circle cx=\"3\" cy=\"2\" r=\"10\" />"
          + "</svg>");
    
    public static final Chars TEST_ELLIPSE = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<ellipse cx=\"3\" cy=\"2\" rx=\"10\" ry=\"12\" />"
          + "</svg>");
    
    public static final Chars TEST_LINE = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<line x1=\"1\" y1=\"2\" x2=\"3\" y2=\"4\" />"
          + "</svg>");
    
    public static final Chars TEST_POLYGON = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<polygon points=\"1,2 3,4 5,6\" />"
          + "</svg>");
    
    public static final Chars TEST_POLYLINE = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<polyline points=\"   1\n,2\n3\t , 4\t\t\n  5\n,\t6  \" />"
          + "</svg>");
    
    public static final Chars TEST_PATH1 = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<path d=\"M1 2 L3 4 L5 6 Z\" />"
          + "</svg>");
    public static final Chars TEST_PATH2 = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<path d=\"M1 2 L3 4 5 6 Z\" />"
          + "</svg>");
    public static final Chars TEST_PATH3 = new Chars(
            "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
          + "<path d=\"m 10e-4,0.859 M -1.427,-0.059 z\" />"
          + "</svg>");
    
    
}
