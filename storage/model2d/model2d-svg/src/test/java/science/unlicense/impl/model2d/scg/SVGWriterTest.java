

package science.unlicense.impl.model2d.scg;

import science.unlicense.impl.model2d.svg.SVGWriter;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model2d.svg.model.SVGDocument;
import static science.unlicense.impl.model2d.scg.SVGTestConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class SVGWriterTest {
    
    //@Test
    public void writeDocument() throws IOException{
        final SVGDocument doc = new SVGDocument();
        doc.setWidth(800.0);
        doc.setHeight(600.0);
        write(doc, TEST_DOCUMENT);
    }
    
    public void write(SVGDocument doc, Chars expected) throws IOException{
        final SVGWriter writer = new SVGWriter();
        final ArrayOutputStream out = new ArrayOutputStream();
        writer.setOutput(out);
        writer.write(doc);
        writer.dispose();
        
        final Chars res = new Chars(out.getBuffer().toArrayByte(),CharEncodings.UTF_8);
        Assert.assertEquals(expected, res);
    }
    
}
