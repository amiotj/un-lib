

package science.unlicense.impl.model2d.scg;

import science.unlicense.impl.model2d.svg.SVGReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.model2d.svg.model.SVGDocument;
import static science.unlicense.impl.model2d.scg.SVGTestConstants.*;
import science.unlicense.impl.model2d.svg.model.SVGCircle;
import science.unlicense.impl.model2d.svg.model.SVGEllipse;
import science.unlicense.impl.model2d.svg.model.SVGLine;
import science.unlicense.impl.model2d.svg.model.SVGPath;
import science.unlicense.impl.model2d.svg.model.SVGPolygon;
import science.unlicense.impl.model2d.svg.model.SVGPolyline;
import science.unlicense.impl.model2d.svg.model.SVGRect;

/**
 *
 * @author Johann Sorel
 */
public class SVGReaderTest {
    
    private static final double DELTA = 0.000001;
    
    @Test
    public void readDocumentTest() throws IOException{
        final SVGDocument doc = read(TEST_DOCUMENT);
        Assert.assertEquals(800.0,doc.getWidth(),      DELTA);
        Assert.assertEquals(600.0,doc.getHeight(),     DELTA);
    }
    
    @Test
    public void readRectTest() throws IOException{
        final SVGDocument doc = read(TEST_RECTANGLE);
        final SVGRect element = (SVGRect) doc.getChildren().get(0);
        Assert.assertEquals(100.0,element.getWidth(),  DELTA);
        Assert.assertEquals(200.0,element.getHeight(), DELTA);
        Assert.assertEquals( 10.0,element.getX(),      DELTA);
        Assert.assertEquals( 20.0,element.getY(),      DELTA);
        Assert.assertEquals( 12.0,element.getRadiusX(),DELTA);
        Assert.assertEquals( 24.0,element.getRadiusY(),DELTA);
    }
    
    @Test
    public void readCercleTest() throws IOException{
        final SVGDocument doc = read(TEST_CIRCLE);
        final SVGCircle element = (SVGCircle) doc.getChildren().get(0);
        Assert.assertEquals( 3.0,element.getCenterX(),  DELTA);
        Assert.assertEquals( 2.0,element.getCenterY(),  DELTA);
        Assert.assertEquals(10.0,element.getRadius(),   DELTA);
    }
    
    @Test
    public void readEllipseTest() throws IOException{
        final SVGDocument doc = read(TEST_ELLIPSE);
        final SVGEllipse element = (SVGEllipse) doc.getChildren().get(0);
        Assert.assertEquals( 3.0,element.getCenterX(),  DELTA);
        Assert.assertEquals( 2.0,element.getCenterY(),  DELTA);
        Assert.assertEquals(10.0,element.getRadiusX(),  DELTA);
        Assert.assertEquals(12.0,element.getRadiusY(),  DELTA);
    }
    
    @Test
    public void readLineTest() throws IOException{
        final SVGDocument doc = read(TEST_LINE);
        final SVGLine element = (SVGLine) doc.getChildren().get(0);
        Assert.assertEquals(1.0,element.getX1(),  DELTA);
        Assert.assertEquals(2.0,element.getY1(),  DELTA);
        Assert.assertEquals(3.0,element.getX2(),  DELTA);
        Assert.assertEquals(4.0,element.getY2(),  DELTA);
    }
    
    @Test
    public void readPolygonTest() throws IOException{
        final SVGDocument doc = read(TEST_POLYGON);
        final SVGPolygon element = (SVGPolygon) doc.getChildren().get(0);
        final TupleBuffer1D points = element.getPointsAsCoords();
        Assert.assertEquals(3, points.getDimension());
        Assert.assertArrayEquals(new double[]{1, 2}, points.getTupleDouble(0,null),DELTA);
        Assert.assertArrayEquals(new double[]{3, 4}, points.getTupleDouble(1,null),DELTA);
        Assert.assertArrayEquals(new double[]{5, 6}, points.getTupleDouble(2,null),DELTA);
    }
    
    @Test
    public void readPolylineTest() throws IOException{
        final SVGDocument doc = read(TEST_POLYLINE);
        final SVGPolyline element = (SVGPolyline) doc.getChildren().get(0);
        final TupleBuffer1D points = element.getPointsAsCoords();
        Assert.assertEquals(3, points.getDimension());
        Assert.assertArrayEquals(new double[]{1, 2}, points.getTupleDouble(0,null),DELTA);
        Assert.assertArrayEquals(new double[]{3, 4}, points.getTupleDouble(1,null),DELTA);
        Assert.assertArrayEquals(new double[]{5, 6}, points.getTupleDouble(2,null),DELTA);
    }
    
    @Test
    public void readPath1Test() throws IOException{
        final TupleRW tuple = new DefaultTuple(2);
        
        final SVGDocument doc = read(TEST_PATH1);
        final SVGPath element = (SVGPath) doc.getChildren().get(0);
        final PathIterator ite = element.asGeometry().createPathIterator();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(1, 2), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(3, 4), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(5, 6), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_CLOSE, ite.getType());
        Assert.assertFalse(ite.next());
    }
    
    @Test
    public void readPath2Test() throws IOException{
        final TupleRW tuple = new DefaultTuple(2);
        
        final SVGDocument doc = read(TEST_PATH2);
        final SVGPath element = (SVGPath) doc.getChildren().get(0);
        final PathIterator ite = element.asGeometry().createPathIterator();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(1, 2), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(3, 4), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(5, 6), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_CLOSE, ite.getType());
        Assert.assertFalse(ite.next());
    }
    
    @Test
    public void readPath3Test() throws IOException{
        final TupleRW tuple = new DefaultTuple(2);
        
        final SVGDocument doc = read(TEST_PATH3);
        final SVGPath element = (SVGPath) doc.getChildren().get(0);
        final PathIterator ite = element.asGeometry().createPathIterator();
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(10e-4, 0.859), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO, ite.getType());
        Assert.assertEquals(new DefaultTuple(-1.427,-0.059), ite.getPosition(tuple));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_CLOSE, ite.getType());
        Assert.assertFalse(ite.next());
    }
    
    private static SVGDocument read(Chars text) throws IOException{
        final ArrayInputStream in = new ArrayInputStream(text.toBytes());
        final SVGReader reader = new SVGReader();
        reader.setInput(in);
        return reader.read();
    }
    
}
