

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TSpanElement
 * 
 * @author Johann Sorel
 */
public class SVGTSpan extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TSPAN;
    
    public SVGTSpan() {
        super(NAME);
    }
    
    public SVGTSpan(DomElement node) {
        super(node);
    }
    
}
