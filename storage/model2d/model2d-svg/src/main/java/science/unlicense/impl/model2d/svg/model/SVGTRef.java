

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TRefElement
 * 
 * @author Johann Sorel
 */
public class SVGTRef extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TREF;
    
    public SVGTRef() {
        super(NAME);
    }
    
    public SVGTRef(DomElement node) {
        super(node);
    }
    
}
