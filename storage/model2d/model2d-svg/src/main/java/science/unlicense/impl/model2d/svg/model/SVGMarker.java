

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/painting.html#MarkerElement
 * 
 * @author Johann Sorel
 */
public class SVGMarker extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_MARKER;
    
    public SVGMarker() {
        super(NAME);
    }
    
    public SVGMarker(DomElement node) {
        super(node);
    }
    
}
