

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 *
 * @author Johann Sorel
 */
public class SVGGroup extends SVGGraphic {
    
    public static final FName FName = SVGConstants.NAME_G;

    public SVGGroup() {
        super(FName);
    }
    
    public SVGGroup(DomElement base){
        super(base);
    }

    public Geometry2D asGeometry() {
        return null;
    }
        
}
