

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Ellipse;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/shapes.html#EllipseElement
 * 
 * @author Johann Sorel
 */
public class SVGEllipse extends SVGGraphic {
    
    public static final FName FName = SVGConstants.NAME_ELLIPSE;
    public static final Chars PROP_CX = SVGConstants.CX;
    public static final Chars PROP_CY = SVGConstants.CY;
    public static final Chars PROP_RX = SVGConstants.RX;
    public static final Chars PROP_RY = SVGConstants.RY;

    public SVGEllipse() {
        super(FName);
    }
    
    public SVGEllipse(DomElement base){
        super(base);
    }
    
    public Double getCenterX() {
        return getPropertyDouble(PROP_CX);
    }

    public void setCenterX(Double value) {
        getProperties().add(PROP_CX, value);
    }

    public Double getCenterY() {
        return getPropertyDouble(PROP_CY);
    }

    public void setCenterY(Double value) {
        getProperties().add(PROP_CY, value);
    }
    
    public Double getRadiusX() {
        return getPropertyDouble(PROP_RX);
    }

    public void setRadiusX(Double value) {
        getProperties().add(PROP_RX, value);
    }

    public Double getRadiusY() {
        return getPropertyDouble(PROP_RY);
    }

    public void setRadiusY(Double value) {
        getProperties().add(PROP_RY, value);
    }
    
    public Geometry2D asGeometry(){
        final Ellipse geom = new Ellipse();
        geom.setCenterX(getCenterX());
        geom.setCenterY(getCenterY());
        geom.setRadiusX(getRadiusX());
        geom.setRadiusY(getRadiusY());
        return geom;
    }
    
}
