

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#RadialGradients
 * 
 * @author Johann Sorel
 */
public class SVGRadialGradient extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_RADIALGRADIENT;
    
    public SVGRadialGradient() {
        super(NAME);
    }
    
    public SVGRadialGradient(DomElement node) {
        super(node);
    }
    
}
