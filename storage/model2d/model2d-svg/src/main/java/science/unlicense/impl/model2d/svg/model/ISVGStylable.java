

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.code.css.CSSProperties;
import science.unlicense.api.color.Color;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/types.html#InterfaceSVGStylable
 * 
 * @author Johann Sorel
 */
public interface ISVGStylable {
    
    public static final Chars PROPERTY_STYLE = SVGConstants.STYLE;
    public static final Chars PROPERTY_CLASS = SVGConstants.CLASS;
    
    public static final Chars PROPERTY_ALIGNMENT_BASELINE           = new Chars("alignment-baseline");
    public static final Chars PROPERTY_BASELINE_SHIFT               = new Chars("baseline-shift");
    public static final Chars PROPERTY_CLIP                         = new Chars("clip");
    public static final Chars PROPERTY_CLIP_PATH                    = new Chars("clip-path");
    public static final Chars PROPERTY_CLIP_RULE                    = new Chars("clip-rule");
    public static final Chars PROPERTY_COLOR                        = new Chars("color");
    public static final Chars PROPERTY_COLOR_INTERPOLATION          = new Chars("color-interpolation");
    public static final Chars PROPERTY_COLOR_INTERPOLATION_FILTERS  = new Chars("color-interpolation-filters");
    public static final Chars PROPERTY_COLOR_PROFILE                = new Chars("color-profile");
    public static final Chars PROPERTY_COLOR_RENDERING              = new Chars("color-rendering");
    public static final Chars PROPERTY_CURSOR                       = new Chars("cursor");
    public static final Chars PROPERTY_DIRECTION                    = new Chars("direction");
    public static final Chars PROPERTY_DISPLAY                      = new Chars("display");
    public static final Chars PROPERTY_DOMINANT_BASELINE            = new Chars("dominant-baseline");
    public static final Chars PROPERTY_ENABLE_BACKGROUND            = new Chars("enable-background");
    public static final Chars PROPERTY_FILL                         = new Chars("fill");
    public static final Chars PROPERTY_FILL_OPACITY                 = new Chars("fill-opacity");
    public static final Chars PROPERTY_FILL_RULE                    = new Chars("fill-rule");
    public static final Chars PROPERTY_FILTER                       = new Chars("filter");
    public static final Chars PROPERTY_FLOOD_COLOR                  = new Chars("flood-color");
    public static final Chars PROPERTY_FLOOD_OPACITY                = new Chars("flood-opacity");
    public static final Chars PROPERTY_FONT_FAMILY                  = new Chars("font-family");
    public static final Chars PROPERTY_FONT_SIZE                    = new Chars("font-size");
    public static final Chars PROPERTY_FONT_SIZE_ADJUST             = new Chars("font-size-adjust");
    public static final Chars PROPERTY_FONT_STRETCH                 = new Chars("font-stretch");
    public static final Chars PROPERTY_FONT_STYLE                   = new Chars("font-style");
    public static final Chars PROPERTY_FONT_VARIANT                 = new Chars("font-variant");
    public static final Chars PROPERTY_FONT_WEIGHT                  = new Chars("font-weight");
    public static final Chars PROPERTY_GLYPH_ORIENTATION_HORIZONTAL = new Chars("glyph-orientation-horizontal");
    public static final Chars PROPERTY_GLYPH_ORIENTATION_VERTICAL   = new Chars("glyph-orientation-vertical");
    public static final Chars PROPERTY_IMAGE_RENDERING              = new Chars("image-rendering");
    public static final Chars PROPERTY_KERNING                      = new Chars("kerning");
    public static final Chars PROPERTY_LETTER_SPACING               = new Chars("letter-spacing");
    public static final Chars PROPERTY_LIGHTNING_COLOR              = new Chars("lighting-color");
    public static final Chars PROPERTY_MARKER_END                   = new Chars("marker-end");
    public static final Chars PROPERTY_MARKER_MID                   = new Chars("marker-mid");
    public static final Chars PROPERTY_MARKER_START                 = new Chars("marker-start");
    public static final Chars PROPERTY_MASK                         = new Chars("mask");
    public static final Chars PROPERTY_OPACITY                      = new Chars("opacity");
    public static final Chars PROPERTY_OVERFLOW                     = new Chars("overflow");
    public static final Chars PROPERTY_POINTER_EVENTS               = new Chars("pointer-events");
    public static final Chars PROPERTY_SHAPE_RENDERING              = new Chars("shape-rendering");
    public static final Chars PROPERTY_STOP_COLOR                   = new Chars("stop-color");
    public static final Chars PROPERTY_STOP_OPACITY                 = new Chars("stop-opacity");
    public static final Chars PROPERTY_STROKE                       = new Chars("stroke");
    public static final Chars PROPERTY_STROKE_DASHARRAY             = new Chars("stroke-dasharray");
    public static final Chars PROPERTY_STROKE_DASHOFFSET            = new Chars("stroke-dashoffset");
    public static final Chars PROPERTY_STROKE_LINECAP               = new Chars("stroke-linecap");
    public static final Chars PROPERTY_STROKE_LINEJOIN              = new Chars("stroke-linejoin");
    public static final Chars PROPERTY_STROKE_MITERLIMIT            = new Chars("stroke-miterlimit");
    public static final Chars PROPERTY_STROKE_OPACITY               = new Chars("stroke-opacity");
    public static final Chars PROPERTY_STROKE_WIDTH                 = new Chars("stroke-width");
    public static final Chars PROPERTY_TEXT_ANCHOR                  = new Chars("text-anchor");
    public static final Chars PROPERTY_TEXT_DECORATION              = new Chars("text-decoration");
    public static final Chars PROPERTY_TEXT_RENDERING               = new Chars("text-rendering");
    public static final Chars PROPERTY_UNICODE_BIDI                 = new Chars("unicode-bidi");
    public static final Chars PROPERTY_VISIBILITY                   = new Chars("visibility");
    public static final Chars PROPERTY_WORD_SPACING                 = new Chars("word-spacing");
    public static final Chars PROPERTY_WRITING_MODE                 = new Chars("writing-mode");
    
    
    
    // CSS style ///////////////////////////////////////////////////////////////
    
    Chars getStyleClass();
    
    CSSProperties getStyle();
    
    // attributes //////////////////////////////////////////////////////////////
    
    Chars getAlignmentBaseline();

    void setAlignmentBaseline(Chars value);

    Chars getBaselineShift();

    void setBaselineShift(Chars value);

    Chars getClip();

    void setClip(Chars value);

    Chars getClipPath();

    void setClipPath(Chars value);

    Chars getClipRule();

    void setClipRule(Chars value);

    Color getColor();

    void setColor(Color value);

    Chars getColorInterpolation();

    void setColorInterpolation(Chars value);

    Chars getColorInterpolationFilters();

    void setColorInterpolationFilters(Chars value);

    Chars getColorProfile();

    void setColorProfile(Chars value);

    Chars getColorRendering();

    void setColorRendering(Chars value);

    Chars getCursor();

    void setCursor(Chars value);

    Chars getDirection();

    void setDirection(Chars value);

    Chars getDisplay();

    void setDisplay(Chars value);

    Chars getDominantBaseline();

    void setDominantBaseline(Chars value);

    Chars getEnableBackground();

    void setEnableBackground(Chars value);

    Color getFill();

    void setFill(Color value);

    Chars getFillOpacity();

    void setFillOpacity(Chars value);

    Chars getfillRule();

    void setfillRule(Chars value);

    Chars getFilter();

    void setFilter(Chars value);

    Color getFloodColor();

    void setFloodColor(Color value);

    Chars getFloodOpacity();

    void setFloodOpacity(Chars value);

    Chars getFontFamily();

    void setFontFamily(Chars value);

    Chars getFontSize();

    void setFontSize(Chars value);

    Chars getFontSizeAdjust();

    void setFontSizeAdjust(Chars value);

    Chars getFontStretch();

    void setFontStretch(Chars value);

    Chars getFontStyle();

    void setFontStyle(Chars value);

    Chars getFontVariant();

    void setFontVariant(Chars value);

    Chars getFontWeight();

    void setFontWeight(Chars value);

    Chars getGlyphOrientationHorizontal();

    void setGlyphOrientationHorizontal(Chars value);

    Chars getGlyphOrientationVertical();

    void setGlyphOrientationVertical(Chars value);

    Chars getImageRendering();

    void setImageRendering(Chars value);

    Chars getKerning();

    void setKerning(Chars value);

    Chars getLetterSpacing();

    void setLetterSpacing(Chars value);

    Color getLightingColor();

    void setLightingColor(Color value);

    Chars getMarkerEnd();

    void setMarkerEnd(Chars value);

    Chars getMarkerMid();

    void setMarkerMid(Chars value);

    Chars getMarkerStart();

    void setMarkerStart(Chars value);

    Chars getMask();

    void setMask(Chars value);

    Chars getOpacity();

    void setOpacity(Chars value);

    Chars getOverflow();

    void setOverflow(Chars value);

    Chars getPointerEvents();

    void setPointerEvents(Chars value);

    Chars getShapeRendering();

    void setShapeRendering(Chars value);

    Color getStopColor();

    void setStopColor(Color value);

    Chars getStopOpacity();

    void setStopOpacity(Chars value);

    Color getStroke();

    void setStroke(Color value);

    Chars getStrokeDasharray();

    void setStrokeDasharray(Chars value);

    Chars getStrokeDashoffset();

    void setStrokeDashoffset(Chars value);

    Chars getStrokeLinecap();

    void setStrokeLinecap(Chars value);

    Chars getStrokeLinejoin();

    void setStrokeLinejoin(Chars value);

    Chars getStrokeMiterlimit();

    void setStrokeMiterlimit(Chars value);

    Chars getStrokeOpacity();

    void setStrokeOpacity(Chars value);

    Double getStrokeWidth();

    void setStrokeWidth(Double value);

    Chars getTextAnchor();

    void setTextAnchor(Chars value);

    Chars getTextDecoration();

    void setTextDecoration(Chars value);

    Chars getTextRendering();

    void setTextRendering(Chars value);

    Chars getUnicodeBidi();

    void setUnicodeBidi(Chars value);

    Chars getVisibility();

    void setVisibility(Chars value);

    Chars getWordSpacing();

    void setWordSpacing(Chars value);

    Chars getWritingMode();

    void setWritingMode(Chars value);

}
