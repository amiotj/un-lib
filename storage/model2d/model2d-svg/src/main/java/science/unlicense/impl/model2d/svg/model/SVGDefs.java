

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#DefsElement
 * 
 * @author Johann Sorel
 */
public class SVGDefs extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_DEFS;
    
    public SVGDefs() {
        super(NAME);
    }
    
    public SVGDefs(DomElement base){
        super(base);
    }
    
}
