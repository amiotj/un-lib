

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Matrix3x3;

/**
 * http://www.w3.org/TR/SVG/coords.html
 * 
 * @author Johann Sorel
 */
public interface ISVGTransform {
    
    public static final Chars PROPERTY_TRANSFORM          = new Chars("transforme");
    public static final Chars PROPERTY_VIEWBOX            = new Chars("viewBox");
    public static final Chars PROPERTY_PRESERVEASPECRATIO = new Chars("preserveAspectRatio");
    
    public static final Chars TRANSFORM_MATRIX      = new Chars("matrix");
    public static final Chars TRANSFORM_TRANSLATE   = new Chars("translate");
    public static final Chars TRANSFORM_SCALE       = new Chars("scale");
    public static final Chars TRANSFORM_ROTATE      = new Chars("rotate");
    public static final Chars TRANSFORM_SKEWX       = new Chars("skewX");
    public static final Chars TRANSFORM_SKEWY       = new Chars("skewY");
    
    Matrix3x3 getTransform();
    
    void setTransform(Matrix3x3 trs);
    
    BBox getViewBox();
    
    void setViewBox(BBox bbox);
    
    //TODO preserve ratio, akward definition
    
}
