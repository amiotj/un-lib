

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * Map an SVG DOM as a more friendly manipulation model.
 * 
 * @author Johann Sorel
 */
public class SVGDocument extends SVGGraphic implements ISVGStylable {
    
    public static final FName FName = SVGConstants.NAME_SVG;
    public static final Chars PROP_WIDTH = SVGConstants.WIDTH;
    public static final Chars PROP_HEIGHT = SVGConstants.HEIGHT;
    
    public SVGDocument(){
        super(FName);
    }
    
    public SVGDocument(DomElement root){
        super(root);
    }
    
    public Double getWidth() {
        return getPropertyDouble(PROP_WIDTH);
    }

    public void setWidth(Double width) {
        getProperties().add(PROP_WIDTH, width);
    }

    public Double getHeight() {
        return getPropertyDouble(PROP_HEIGHT);
    }

    public void setHeight(Double height) {
        getProperties().add(PROP_HEIGHT, height);
    }

    public Geometry2D asGeometry() {
        return null;
    }
  
}
