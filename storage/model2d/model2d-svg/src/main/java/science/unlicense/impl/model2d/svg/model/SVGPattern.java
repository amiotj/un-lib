

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#Patterns
 * 
 * @author Johann Sorel
 */
public class SVGPattern extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_PATTERN;
    
    public SVGPattern() {
        super(NAME);
    }
    
    public SVGPattern(DomElement node) {
        super(node);
    }
    
}
