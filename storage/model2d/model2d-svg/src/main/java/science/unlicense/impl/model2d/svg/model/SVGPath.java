

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.RenderState;
import science.unlicense.impl.model2d.svg.SVGConstants;
import static science.unlicense.impl.model2d.svg.SVGConstants.P_SEP1;
import static science.unlicense.impl.model2d.svg.SVGConstants.P_SEP2;
import static science.unlicense.impl.model2d.svg.SVGConstants.P_SEP3;
import static science.unlicense.impl.model2d.svg.SVGConstants.P_SEP4;
import static science.unlicense.impl.model2d.svg.SVGConstants.d;

/**
 *
 * @author Johann Sorel
 */
public class SVGPath extends SVGGraphic {
    
    public static final FName FName = SVGConstants.NAME_PATH;
    public static final Chars PROP_D = SVGConstants.d;
   
    public SVGPath() {
        super(FName);
    }
    
    public SVGPath(DomElement base){
        super(base);
    }
    
    public Chars getD() {
        return getPropertyChars(PROP_D);
    }

    public Geometry2D asGeometry(){
        final Chars coords = getPropertyChars(d).trim();            
        final Path geom = new Path();
        readPath(coords, geom);
        return geom;
    }

    @Override
    public RenderState buildRenderState() {
        return super.buildRenderState(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    private static void readPath(Chars coords, Path path){
        final CharIterator ite = coords.createIterator();
        double cx1 = 0;
        double cy1 = 0;
        double cx2 = 0;
        double cy2 = 0;
        double px = 0;
        double py = 0;
        double axis = 0;
        boolean large = false;
        boolean sweep = false;
                
        while(ite.hasNext()){
            moveToNext(ite);
            
            if(ite.nextEquals(P_MOVETO_ABS)){
                ite.skip();
                do{
                    px = readNumber(ite);
                    py = readNumber(ite);
                    path.appendMoveTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_MOVETO_REL)){
                ite.skip();
                do{
                    px += readNumber(ite);
                    py += readNumber(ite);
                    path.appendMoveTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_LINETO_ABS)){
                ite.skip();
                do{
                    px = readNumber(ite);
                    py = readNumber(ite);
                    path.appendLineTo(px, py);
                }while(moveToNext(ite));
                                
            }else if(ite.nextEquals(P_LINETO_REL)){
                ite.skip();
                do{
                    px += readNumber(ite);
                    py += readNumber(ite);
                    path.appendLineTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_HORIZONTAL_ABS)){
                ite.skip();
                do{
                    px = readNumber(ite);
                    path.appendLineTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_HORIZONTAL_REL)){
                ite.skip();
                do{
                    px += readNumber(ite);
                    path.appendLineTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_VERTICAL_ABS)){
                ite.skip();
                do{
                    py = readNumber(ite);
                    path.appendLineTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_VERTICAL_REL)){
                ite.skip();
                do{
                    py += readNumber(ite);
                    path.appendLineTo(px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_CURVETO_ABS)){
                ite.skip();
                do{
                    cx1 = readNumber(ite);
                    cy1 = readNumber(ite);
                    cx2 = readNumber(ite);
                    cy2 = readNumber(ite);
                    px = readNumber(ite);
                    py = readNumber(ite);
                    path.appendCubicTo(cx1, cy1, cx2, cy2, px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_CURVETO_REL)){
                ite.skip();
                do{
                    cx1 = px+readNumber(ite);
                    cy1 = py+readNumber(ite);
                    cx2 = px+readNumber(ite);
                    cy2 = py+readNumber(ite);
                    px += readNumber(ite);
                    py += readNumber(ite);
                    path.appendCubicTo(cx1, cy1, cx2, cy2, px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_SCURVETO_ABS)){
                ite.skip();
                do{
                    cx1 = px+(px-cx2);
                    cy1 = py+(py-cy2);
                    cx2 = readNumber(ite);
                    cy2 = readNumber(ite);
                    px = readNumber(ite);
                    py = readNumber(ite);
                    path.appendCubicTo(cx1, cy1, cx2, cy2, px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_SCURVETO_REL)){
                ite.skip();
                do{
                    cx1 = px+(px-cx2);
                    cy1 = py+(py-cy2);
                    cx2 = px+readNumber(ite);
                    cy2 = py+readNumber(ite);
                    px += readNumber(ite);
                    py += readNumber(ite);
                    path.appendCubicTo(cx1, cy1, cx2, cy2, px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_ARC_ABS)){
                ite.skip();
                do{
                    cx1 = readNumber(ite);
                    cy1 = readNumber(ite);
                    axis = readNumber(ite);
                    large = readNumber(ite) != 0.0;
                    sweep = readNumber(ite) != 0.0;
                    px = readNumber(ite);
                    py = readNumber(ite);
                    path.appendArcTo(cx1, cy1, axis, large, sweep, px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_ARC_REL)){
                ite.skip();
                do{
                    cx1 = readNumber(ite);
                    cy1 = readNumber(ite);
                    axis = readNumber(ite);
                    large = readNumber(ite) != 0.0;
                    sweep = readNumber(ite) != 0.0;
                    px += readNumber(ite);
                    py += readNumber(ite);
                    path.appendArcTo(cx1, cy1, axis, large, sweep, px, py);
                }while(moveToNext(ite));
                
            }else if(ite.nextEquals(P_CLOSE_ABS)){
                ite.skip();
                path.appendClose();
                
            }else if(ite.nextEquals(P_CLOSE_REL)){
                ite.skip();
                path.appendClose();
                
            }else if(ite.nextEquals(P_SEP1)||ite.nextEquals(P_SEP2)
                   ||ite.nextEquals(P_SEP3)||ite.nextEquals(P_SEP4)){
                ite.skip();
                //do nothing
            }else{
                throw new RuntimeException("Unexpected char : "+ite.nextToUnicode());
            }
        }
    }
    
    private static boolean moveToNext(CharIterator ite){
        
        while(ite.hasNext()){
            if(ite.nextEquals(P_SEP1)||ite.nextEquals(P_SEP2)
             ||ite.nextEquals(P_SEP3)||ite.nextEquals(P_SEP4)){
                ite.skip();
            }else{
                final int unicode = ite.peekToUnicode();
                return unicode=='-' || (unicode>='0' && unicode<='9');
            }
        }
        return false;
    }
    
}
