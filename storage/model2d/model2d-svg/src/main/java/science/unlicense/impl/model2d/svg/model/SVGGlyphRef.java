

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#AlternateGlyphDefinitions
 * 
 * @author Johann Sorel
 */
public class SVGGlyphRef extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_GLYPHREF;
    
    public SVGGlyphRef() {
        super(NAME);
    }
    
    public SVGGlyphRef(DomElement node) {
        super(node);
    }
    
}
