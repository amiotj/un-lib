

package science.unlicense.impl.model2d.svg;

import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 * Rebuild all style informations from an SVG Graphic.
 * CSS and local style informations could be mixed together which makes it troublesome
 * to choose the right parameters.
 * Animations and expression can also be part of the style definition in such case
 * the RenderState is for a fix time.
 * 
 * @author Johann Sorel
 */
public class RenderState {
    
    public Geometry2D fillGeometry;
    public Paint fillPaint;
    
    public Geometry2D strokeGeometry;
    public Paint strokePaint;
    public Brush strokeBrush;
    
}
