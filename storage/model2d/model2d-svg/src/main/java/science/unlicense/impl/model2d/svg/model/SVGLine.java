

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/shapes.html#LineElement
 * 
 * @author Johann Sorel
 */
public class SVGLine extends SVGGraphic {
    
    public static final FName FName = SVGConstants.NAME_LINE;
    public static final Chars PROP_X1 = SVGConstants.X1;
    public static final Chars PROP_Y1 = SVGConstants.Y1;
    public static final Chars PROP_X2 = SVGConstants.X2;
    public static final Chars PROP_Y2 = SVGConstants.Y2;

    public SVGLine() {
        super(FName);
    }
    
    public SVGLine(DomElement base){
        super(base);
    }
    
    public Double getX1() {
        return getPropertyDouble(PROP_X1);
    }

    public void setX1(Double value) {
        getProperties().add(PROP_X1, value);
    }

    public Double getY1() {
        return getPropertyDouble(PROP_Y1);
    }

    public void setY1(Double value) {
        getProperties().add(PROP_Y1, value);
    }
    
    public Double getX2() {
        return getPropertyDouble(PROP_X2);
    }

    public void setX2(Double value) {
        getProperties().add(PROP_X2, value);
    }

    public Double getY2() {
        return getPropertyDouble(PROP_Y2);
    }

    public void setY2(Double value) {
        getProperties().add(PROP_Y2, value);
    }
    
    public Geometry2D asGeometry(){
        final Segment geom = new Segment();
        geom.setStartX(getX1());
        geom.setStartY(getY1());
        geom.setEndX(getX2());
        geom.setEndY(getY2());
        return geom;
    }
    
}
