

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/masking.html#Masking
 * 
 * @author Johann Sorel
 */
public class SVGMask extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_MASK;
    
    public SVGMask() {
        super(NAME);
    }
    
    public SVGMask(DomElement node) {
        super(node);
    }
    
}
