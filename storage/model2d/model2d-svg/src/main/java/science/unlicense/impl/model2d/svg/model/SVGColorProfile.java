

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/color.html#ColorProfileElement
 * 
 * @author Johann Sorel
 */
public class SVGColorProfile extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_COLORPROFILE;
    
    public SVGColorProfile() {
        super(NAME);
    }
    
    public SVGColorProfile(DomElement node) {
        super(node);
    }
    
}
