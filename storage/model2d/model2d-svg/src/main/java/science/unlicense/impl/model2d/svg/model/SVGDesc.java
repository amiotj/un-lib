
package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#DescriptionAndTitleElements
 * 
 * @author Johann Sorel
 */
public class SVGDesc extends SVGElement {
    
    public static final FName NAME = SVGConstants.NAME_DESC;

    public SVGDesc() {
        super(NAME);
    }
    
    public SVGDesc(DomElement base) {
        super(base);
        setText(base.getText());
    }
    
}
