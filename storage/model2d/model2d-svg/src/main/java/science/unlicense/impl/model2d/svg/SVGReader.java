
package science.unlicense.impl.model2d.svg;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.binding.xml.dom.DomNode;
import science.unlicense.impl.binding.xml.dom.DomReader;

import static science.unlicense.impl.model2d.svg.SVGConstants.*;
import science.unlicense.impl.model2d.svg.model.SVGAltGlyph;
import science.unlicense.impl.model2d.svg.model.SVGAltGlyphDef;
import science.unlicense.impl.model2d.svg.model.SVGAltGlyphItem;
import science.unlicense.impl.model2d.svg.model.SVGCircle;
import science.unlicense.impl.model2d.svg.model.SVGClipPath;
import science.unlicense.impl.model2d.svg.model.SVGColorProfile;
import science.unlicense.impl.model2d.svg.model.SVGDefs;
import science.unlicense.impl.model2d.svg.model.SVGDesc;
import science.unlicense.impl.model2d.svg.model.SVGDocument;
import science.unlicense.impl.model2d.svg.model.SVGElement;
import science.unlicense.impl.model2d.svg.model.SVGEllipse;
import science.unlicense.impl.model2d.svg.model.SVGGlyphRef;
import science.unlicense.impl.model2d.svg.model.SVGGroup;
import science.unlicense.impl.model2d.svg.model.SVGImage;
import science.unlicense.impl.model2d.svg.model.SVGLine;
import science.unlicense.impl.model2d.svg.model.SVGLinearGradient;
import science.unlicense.impl.model2d.svg.model.SVGMarker;
import science.unlicense.impl.model2d.svg.model.SVGMask;
import science.unlicense.impl.model2d.svg.model.SVGPath;
import science.unlicense.impl.model2d.svg.model.SVGPattern;
import science.unlicense.impl.model2d.svg.model.SVGPolygon;
import science.unlicense.impl.model2d.svg.model.SVGPolyline;
import science.unlicense.impl.model2d.svg.model.SVGRadialGradient;
import science.unlicense.impl.model2d.svg.model.SVGRect;
import science.unlicense.impl.model2d.svg.model.SVGStop;
import science.unlicense.impl.model2d.svg.model.SVGStyle;
import science.unlicense.impl.model2d.svg.model.SVGSwitch;
import science.unlicense.impl.model2d.svg.model.SVGSymbol;
import science.unlicense.impl.model2d.svg.model.SVGTRef;
import science.unlicense.impl.model2d.svg.model.SVGTSpan;
import science.unlicense.impl.model2d.svg.model.SVGText;
import science.unlicense.impl.model2d.svg.model.SVGTextPath;
import science.unlicense.impl.model2d.svg.model.SVGTitle;
import science.unlicense.impl.model2d.svg.model.SVGUse;

/**
 *
 * @author Johann Sorel
 */
public class SVGReader extends AbstractReader{
        
    public SVGDocument read() throws IOException{    
        Object root = getInput();
        if(!(root instanceof DomNode)){
            //parse the stream
             final DomReader domReader = new DomReader();
            domReader.setInput(root);
            root = domReader.read();
            domReader.dispose();
        }
        
        final SVGDocument doc = (SVGDocument) read(null,(DomNode)root);
        return doc;
    }
    
    private SVGElement read(SVGDocument doc, DomNode candidate){
        
        final SVGElement current;
        final DomElement node = (DomElement) candidate;
        //namespace is often missing on svg docs, TODO ? leave this ?
        //might bring problems when mixing different namespaces
        final FName name = new FName(NAMESPACE, node.getName().getLocalPart());
        
        
        if(NAME_SVG.equals(name)){
            current = new SVGDocument(node);
            doc = (SVGDocument) current;
        }
        //meta ypes
        else if(NAME_DEFS.equals(name)){
            current = new SVGDefs(node);
        }else if(NAME_DESC.equals(name)){
            current = new SVGDesc(node);
        }else if(NAME_TITLE.equals(name)){
            current = new SVGTitle(node);
        }
        //geometry types
        else if(NAME_RECT.equals(name)){
            current = new SVGRect(node);
        }else if(NAME_CERCLE.equals(name)){
            current = new SVGCircle(node);
        }else if(NAME_ELLIPSE.equals(name)){
            current = new SVGEllipse(node);
        }else if(NAME_LINE.equals(name)){
            current = new SVGLine(node);
        }else if(NAME_POLYLINE.equals(name)){
            current = new SVGPolyline(node);
        }else if(NAME_POLYGON.equals(name)){
            current = new SVGPolygon(node);
        }else if(NAME_PATH.equals(name)){
            current = new SVGPath(node);
        }else if(NAME_G.equals(name)){
            current = new SVGGroup(node);
        }
        //defs
        else if(NAME_DEFS.equals(name)){
            current = new SVGDefs(node);
        }else if(NAME_SYMBOL.equals(name)){
            current = new SVGSymbol(node);
        }else if(NAME_USE.equals(name)){
            current = new SVGUse(node);
        }else if(NAME_IMAGE.equals(name)){
            current = new SVGImage(node);
        }else if(NAME_SWITCH.equals(name)){
            current = new SVGSwitch(node);
        }else if(NAME_STYLE.equals(name)){
            current = new SVGStyle(node);
        }
        //texts
        else if(NAME_TEXT.equals(name)){
            current = new SVGText(node);
        }else if(NAME_TSPAN.equals(name)){
            current = new SVGTSpan(node);
        }else if(NAME_TEXTPATH.equals(name)){
            current = new SVGTextPath(node);
        }else if(NAME_TREF.equals(name)){
            current = new SVGTRef(node);
        }else if(NAME_ALTGLYPH.equals(name)){
            current = new SVGAltGlyph(node);
        }else if(NAME_ALTGLYPHDEF.equals(name)){
            current = new SVGAltGlyphDef(node);
        }else if(NAME_ALTGLYPHITEM.equals(name)){
            current = new SVGAltGlyphItem(node);
        }else if(NAME_GLYPHREF.equals(name)){
            current = new SVGGlyphRef(node);
        }
        //painting
        else if(NAME_MARKER.equals(name)){
            current = new SVGMarker(node);
        }else if(NAME_COLORPROFILE.equals(name)){
            current = new SVGColorProfile(node);
        }else if(NAME_LINEARGRADIENT.equals(name)){
            current = new SVGLinearGradient(node);
        }else if(NAME_RADIALGRADIENT.equals(name)){
            current = new SVGRadialGradient(node);
        }else if(NAME_STOP.equals(name)){
            current = new SVGStop(node);
        }else if(NAME_PATTERN.equals(name)){
            current = new SVGPattern(node);
        }else if(NAME_CLIPPATH.equals(name)){
            current = new SVGClipPath(node);
        }else if(NAME_MASK.equals(name)){
            current = new SVGMask(node);
        }
        //fallback, just encapsulate it   
        else{
            current = new SVGElement(node);
        }
        
        final Iterator nodes = candidate.getChildren().createIterator();
        while (nodes.hasNext()) {
            current.getChildren().add(read(doc,(DomNode)nodes.next()));
        }
        
        return current;
    }
                 
}
