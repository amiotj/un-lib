

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/styling.html#StyleElement
 * 
 * @author Johann Sorel
 */
public class SVGStyle extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_USE;
    
    public SVGStyle() {
        super(NAME);
    }
    
    public SVGStyle(DomElement node) {
        super(node);
    }
    
}
