

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#ImageElement
 * 
 * @author Johann Sorel
 */
public class SVGImage extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_IMAGE;
    
    public SVGImage() {
        super(NAME);
    }
    
    public SVGImage(DomElement node) {
        super(node);
    }
    
}
