

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#AltGlyphElement
 * 
 * @author Johann Sorel
 */
public class SVGAltGlyph extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_ALTGLYPH;
    
    public SVGAltGlyph() {
        super(NAME);
    }
    
    public SVGAltGlyph(DomElement node) {
        super(node);
    }
    
}
