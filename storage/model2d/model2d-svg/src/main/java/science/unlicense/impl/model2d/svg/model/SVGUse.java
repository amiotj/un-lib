

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#UseElement
 * 
 * @author Johann Sorel
 */
public class SVGUse extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_USE;
    
    public SVGUse() {
        super(NAME);
    }
    
    public SVGUse(DomElement node) {
        super(node);
    }
    
}
