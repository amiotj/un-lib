

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#GradientStops
 * 
 * @author Johann Sorel
 */
public class SVGStop extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_STOP;
    
    public SVGStop() {
        super(NAME);
    }
    
    public SVGStop(DomElement node) {
        super(node);
    }
    
}
