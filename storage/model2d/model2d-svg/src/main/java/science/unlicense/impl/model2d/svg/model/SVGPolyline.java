

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/shapes.html#PolylineElement
 * 
 * @author Johann Sorel
 */
public class SVGPolyline extends SVGGraphic {
    
    public static final FName FName = SVGConstants.NAME_POLYLINE;
    public static final Chars PROP_POINTS = SVGConstants.POINTS;

    public SVGPolyline() {
        super(FName);
    }
    
    public SVGPolyline(DomElement base){
        super(base);
    }
    
    public Chars getPoints() {
        return getPropertyChars(PROP_POINTS);
    }

    public void setPoints(Chars value) {
        getProperties().add(PROP_POINTS, value);
    }
    
    public TupleBuffer1D getPointsAsCoords(){
        return readCoordinates(getPoints());
    }
    
    public Geometry2D asGeometry(){
        final TupleBuffer1D coords = getPointsAsCoords();
        final Polyline geom = new Polyline(coords);
        return geom;
    }
    
}
