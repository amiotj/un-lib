

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TextElement
 * 
 * @author Johann Sorel
 */
public class SVGText extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TEXT;
    
    public SVGText() {
        super(NAME);
    }
    
    public SVGText(DomElement node) {
        super(node);
    }
    
}
