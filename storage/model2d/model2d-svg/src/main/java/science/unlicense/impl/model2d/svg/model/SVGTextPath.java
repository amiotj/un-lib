

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/text.html#TextPathElement
 * 
 * @author Johann Sorel
 */
public class SVGTextPath extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_TEXTPATH;
    
    public SVGTextPath() {
        super(NAME);
    }
    
    public SVGTextPath(DomElement node) {
        super(node);
    }
    
}
