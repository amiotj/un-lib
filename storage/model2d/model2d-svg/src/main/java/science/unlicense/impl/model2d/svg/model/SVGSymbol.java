

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#SymbolElement
 * 
 * @author Johann Sorel
 */
public class SVGSymbol extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_SYMBOL;
    
    public SVGSymbol() {
        super(NAME);
    }
    
    public SVGSymbol(DomElement node) {
        super(node);
    }

}
