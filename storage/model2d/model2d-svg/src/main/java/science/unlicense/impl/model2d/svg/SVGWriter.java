

package science.unlicense.impl.model2d.svg;

import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.xml.XMLOutputStream;
import science.unlicense.impl.binding.xml.dom.DomWriter;
import science.unlicense.impl.model2d.svg.model.SVGDocument;

/**
 *
 * @author Johann Sorel
 */
public class SVGWriter extends AbstractWriter {
    
    public void write(SVGDocument doc) throws IOException{
        Object out = getOutput();
        XMLOutputStream xmlOut = null;
        if(out instanceof XMLOutputStream){
            xmlOut = (XMLOutputStream) out;
        }else{
            xmlOut = new XMLOutputStream();
            xmlOut.setOutput(out);
        }
        
        final DomWriter writer = new DomWriter();
        writer.setOutput(xmlOut);
        writer.write(doc);
    }
    
}
