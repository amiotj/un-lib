
package science.unlicense.impl.model2d.svg;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.xml.FName;

/**
 *
 * @author Johann Sorel
 */
public class SVGConstants {
    
    public static final Chars NAMESPACE = new Chars("http://www.w3.org/2000/svg");
    
    public static final FName NAME_SVG      = new FName(NAMESPACE, new Chars("svg"));
    public static final FName NAME_DESC     = new FName(NAMESPACE, new Chars("desc"));
    public static final FName NAME_TITLE    = new FName(NAMESPACE, new Chars("title"));
    //shapes
    public static final FName NAME_RECT     = new FName(NAMESPACE, new Chars("rect"));
    public static final FName NAME_CERCLE   = new FName(NAMESPACE, new Chars("circle"));
    public static final FName NAME_ELLIPSE  = new FName(NAMESPACE, new Chars("ellipse"));
    public static final FName NAME_LINE     = new FName(NAMESPACE, new Chars("line"));
    public static final FName NAME_POLYLINE = new FName(NAMESPACE, new Chars("polyline"));
    public static final FName NAME_POLYGON  = new FName(NAMESPACE, new Chars("polygon"));
    public static final FName NAME_PATH     = new FName(NAMESPACE, new Chars("path"));
    public static final FName NAME_G        = new FName(NAMESPACE, new Chars("g"));
    //defs
    public static final FName NAME_DEFS     = new FName(NAMESPACE, new Chars("defs"));
    public static final FName NAME_SYMBOL   = new FName(NAMESPACE, new Chars("symbol"));
    public static final FName NAME_USE      = new FName(NAMESPACE, new Chars("use"));
    public static final FName NAME_IMAGE    = new FName(NAMESPACE, new Chars("image"));
    public static final FName NAME_SWITCH   = new FName(NAMESPACE, new Chars("switch"));
    public static final FName NAME_STYLE    = new FName(NAMESPACE, new Chars("style"));
    //texts
    public static final FName NAME_TEXT     = new FName(NAMESPACE, new Chars("text"));
    public static final FName NAME_TSPAN    = new FName(NAMESPACE, new Chars("tspan"));
    public static final FName NAME_TEXTPATH = new FName(NAMESPACE, new Chars("textPath"));
    public static final FName NAME_TREF     = new FName(NAMESPACE, new Chars("tref"));
    public static final FName NAME_ALTGLYPH = new FName(NAMESPACE, new Chars("altGlyph"));
    public static final FName NAME_ALTGLYPHDEF = new FName(NAMESPACE, new Chars("altGlyphDef"));
    public static final FName NAME_ALTGLYPHITEM= new FName(NAMESPACE, new Chars("altGlyphItem"));
    public static final FName NAME_GLYPHREF    = new FName(NAMESPACE, new Chars("glyphRef"));
    //painting
    public static final FName NAME_MARKER         = new FName(NAMESPACE, new Chars("marker"));
    public static final FName NAME_COLORPROFILE   = new FName(NAMESPACE, new Chars("color-profile"));
    public static final FName NAME_LINEARGRADIENT = new FName(NAMESPACE, new Chars("linearGradient"));
    public static final FName NAME_RADIALGRADIENT = new FName(NAMESPACE, new Chars("radialGradient"));
    public static final FName NAME_STOP           = new FName(NAMESPACE, new Chars("stop"));
    public static final FName NAME_PATTERN        = new FName(NAMESPACE, new Chars("pattern"));
    public static final FName NAME_CLIPPATH       = new FName(NAMESPACE, new Chars("clipPath"));
    public static final FName NAME_MASK           = new FName(NAMESPACE, new Chars("mask"));
    
        
    public static final Chars ID = new Chars("id");
    public static final Chars STYLE = new Chars("style");
    public static final Chars CLASS = new Chars("class");
    
    public static final Chars FILL = new Chars("fill");
    public static final Chars STROKEWIDTH = new Chars("stroke-width");
    public static final Chars STROKELINECAP = new Chars("stroke-linecap");
    public static final Chars WIDTH = new Chars("width");
    public static final Chars HEIGHT = new Chars("height");
    public static final Chars INHERIT = new Chars("inherit");
    public static final Chars d = new Chars("d");
    public static final Chars X = new Chars("x");
    public static final Chars Y = new Chars("y");
    public static final Chars RX = new Chars("rx");
    public static final Chars RY = new Chars("ry");
    public static final Chars CX = new Chars("cx");
    public static final Chars CY = new Chars("cy");
    public static final Chars R = new Chars("r");
    public static final Chars X1 = new Chars("x1");
    public static final Chars Y1 = new Chars("y1");
    public static final Chars X2 = new Chars("x2");
    public static final Chars Y2 = new Chars("y2");
    public static final Chars POINTS = new Chars("points");
    public static final Chars CAP_BUTT = new Chars("butt");
    public static final Chars CAP_ROUND = new Chars("round");
    public static final Chars CAP_SQUARE = new Chars("square");
    
    public static final Char P_SEP1 = new Char(' ');
    public static final Char P_SEP2 = new Char(',');
    public static final Char P_SEP3 = new Char('\n');
    public static final Char P_SEP4 = new Char('\t');
    
    public static final Char P_ADD = new Char('+');
    public static final Char P_SUB = new Char('-');
    
    public static final Chars NONE = new Chars("none");
    
    private SVGConstants(){}
    
}
