

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/masking.html#EstablishingANewClippingPath
 * 
 * @author Johann Sorel
 */
public class SVGClipPath extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_CLIPPATH;
    
    public SVGClipPath() {
        super(NAME);
    }
    
    public SVGClipPath(DomElement node) {
        super(node);
    }
    
}
