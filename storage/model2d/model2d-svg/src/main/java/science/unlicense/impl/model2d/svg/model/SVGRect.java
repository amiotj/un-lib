

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.RoundedRectangle;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/shapes.html#RectElement
 * 
 * @author Johann Sorel
 */
public class SVGRect extends SVGGraphic {
    
    public static final FName FName = SVGConstants.NAME_RECT;
    public static final Chars PROP_X = SVGConstants.X;
    public static final Chars PROP_Y = SVGConstants.Y;
    public static final Chars PROP_WIDTH = SVGConstants.WIDTH;
    public static final Chars PROP_HEIGHT = SVGConstants.HEIGHT;
    public static final Chars PROP_RX = SVGConstants.RX;
    public static final Chars PROP_RY = SVGConstants.RY;

    public SVGRect() {
        super(FName);
    }
    
    public SVGRect(DomElement base){
        super(base);
    }
    
    public Double getX() {
        return getPropertyDouble(PROP_X);
    }

    public void setX(Double value) {
        getProperties().add(PROP_X, value);
    }

    public Double getY() {
        return getPropertyDouble(PROP_Y);
    }

    public void setY(Double value) {
        getProperties().add(PROP_Y, value);
    }
    
    public Double getWidth() {
        return getPropertyDouble(PROP_WIDTH);
    }

    public void setWidth(Double value) {
        getProperties().add(PROP_WIDTH, value);
    }

    public Double getHeight() {
        return getPropertyDouble(PROP_HEIGHT);
    }

    public void setHeight(Double value) {
        getProperties().add(PROP_HEIGHT, value);
    }
    
    public Double getRadiusX() {
        return getPropertyDouble(PROP_RX);
    }

    public void setRX(Double value) {
        getProperties().add(PROP_RX, value);
    }

    public Double getRadiusY() {
        return getPropertyDouble(PROP_RY);
    }

    public void setRY(Double value) {
        getProperties().add(PROP_RY, value);
    }
    
    public Geometry2D asGeometry(){
        final RoundedRectangle geom = new RoundedRectangle();
        geom.setX(getX());
        geom.setY(getY());
        geom.setWidth(getWidth());
        geom.setHeight(getHeight());
        if(getRadiusX() != null && getRadiusY() != null){
            geom.setRx(getRadiusX());
            geom.setRy(getRadiusY());
        }
        return geom;
    }
    
}
