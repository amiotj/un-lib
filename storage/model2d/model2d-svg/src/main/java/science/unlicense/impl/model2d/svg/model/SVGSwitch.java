

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/struct.html#SwitchElement
 * 
 * @author Johann Sorel
 */
public class SVGSwitch extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_SWITCH;
    
    public SVGSwitch() {
        super(NAME);
    }
    
    public SVGSwitch(DomElement node) {
        super(node);
    }
    
}
