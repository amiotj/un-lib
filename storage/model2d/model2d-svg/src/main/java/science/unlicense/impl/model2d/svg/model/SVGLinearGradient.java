

package science.unlicense.impl.model2d.svg.model;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.model2d.svg.SVGConstants;

/**
 * http://www.w3.org/TR/SVG/pservers.html#LinearGradients
 * 
 * @author Johann Sorel
 */
public class SVGLinearGradient extends SVGElement {

    public static final FName NAME = SVGConstants.NAME_LINEARGRADIENT;
    
    public SVGLinearGradient() {
        super(NAME);
    }
    
    public SVGLinearGradient(DomElement node) {
        super(node);
    }
    
}
