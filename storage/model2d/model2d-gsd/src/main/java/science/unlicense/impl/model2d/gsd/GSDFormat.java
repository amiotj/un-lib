

package science.unlicense.impl.model2d.gsd;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Graphtec Vector Graphic Data format.
 * 
 * Resource : 
 * http://en.wikipedia.org/wiki/Graphtec_Corporation
 * 
 * @author Johann Sorel
 */
public class GSDFormat extends DefaultFormat {

    public GSDFormat() {
        super(new Chars("gsd"),
              new Chars("GSD"),
              new Chars("Graphtec Vector Graphic Data"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("gsd")
              },
              new byte[][]{GSDConstants.SIGNATURE});
    }
        
}
