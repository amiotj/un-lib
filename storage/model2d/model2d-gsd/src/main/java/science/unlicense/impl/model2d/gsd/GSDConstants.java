

package science.unlicense.impl.model2d.gsd;

/**
 * GSD constants.
 * 
 * @author Johann Sorel
 */
public final class GSDConstants {
    
    /**
     * GSD file signature.
     */
    public static byte[] SIGNATURE = new byte[]{'G','R','A','P','H','T','E','C'};
    
    private GSDConstants(){}
    
}
