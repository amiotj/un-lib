
package science.unlicense.impl.model2d.wkt;

import science.unlicense.api.character.Chars;

/**
 * WKT constants
 * 
 * @author Johann Sorel
 */
public final class WKTConstants {

    private WKTConstants() {}
    
    static final byte[] B_POINT                 = new byte[]{'P','O','I','N','T'};
    static final byte[] B_LINESTRING            = new byte[]{'L','I','N','E','S','T','R','I','N','G'};
    static final byte[] B_CIRCULARSTRING        = new byte[]{'C','I','R','C','U','L','A','R','S','T','R','I','N','G'};
    static final byte[] B_COMPOUNDCURVE         = new byte[]{'C','O','M','P','O','U','N','D','C','U','R','V','E'};
    static final byte[] B_CURVEPOLYGON          = new byte[]{'C','U','R','V','E','P','O','L','Y','G','O','N'};
    static final byte[] B_POLYGON               = new byte[]{'P','O','L','Y','G','O','N'};
    static final byte[] B_TRIANGLE              = new byte[]{'T','R','I','A','N','G','L','E'};
    static final byte[] B_POLYHEDRALSURFACE     = new byte[]{'P','O','L','Y','H','E','D','R','A','L','S','U','R','F','A','C','E'};
    static final byte[] B_TIN                   = new byte[]{'T','I','N'};
    static final byte[] B_MULTIPOINT            = new byte[]{'M','U','L','T','I','P','O','I','N','T'};
    static final byte[] B_MULTICURVE            = new byte[]{'M','U','L','T','I','C','U','R','V','E'};
    static final byte[] B_MULTILINESTRING       = new byte[]{'M','U','L','T','I','L','I','N','E','S','T','R','I','N','G'};
    static final byte[] B_MULTISURFACE          = new byte[]{'M','U','L','T','I','S','U','R','F','A','C','E'};
    static final byte[] B_MULTIPOLYGON          = new byte[]{'M','U','L','T','I','P','O','L','Y','G','O','N'};
    static final byte[] B_GEOMETRYCOLLECTION    = new byte[]{'G','E','O','M','E','T','R','Y','C','O','L','L','E','C','T','I','O','N'};
    
    static final byte[] B_PATCHES               = new byte[]{'P','A','T','C','H','E','S'};
    static final byte[] B_ELEMENTS              = new byte[]{'E','L','E','M','E','N','T','S'};
    static final byte[] B_POINTS                = new byte[]{'P','O','I','N','T','S'};
    static final byte[] B_GROUPSPOT             = new byte[]{'G','R','O','U','P','S','P','O','T'};
    static final byte[] B_BOUNDARY              = new byte[]{'B','O','U','N','D','A','R','Y'};
    static final byte[] B_BREAKLINE             = new byte[]{'B','R','E','A','K','L','I','N','E'};
    static final byte[] B_SOFTBREAK             = new byte[]{'S','O','F','T','B','R','E','A','K'};
    static final byte[] B_CONTROLCONTOUR        = new byte[]{'C','O','N','T','R','O','L','C','O','N','T','O','U','R'};
    static final byte[] B_BREAKVOID             = new byte[]{'B','R','E','A','K','V','O','I','D'};
    static final byte[] B_DRAPEVOID             = new byte[]{'D','R','A','P','E','V','O','I','D'};
    static final byte[] B_VOID                  = new byte[]{'V','O','I','D'};
    static final byte[] B_HOLE                  = new byte[]{'H','O','L','E'};
    static final byte[] B_STOPLINE              = new byte[]{'S','T','O','P','L','I','N','E'};
    static final byte[] B_ID                    = new byte[]{'I','D'};
    static final byte[] B_TAG                   = new byte[]{'T','A','G'};
    static final byte[] B_EMPTY                 = new byte[]{'E','M','P','T','Y'};
    static final byte[] B_ZM                    = new byte[]{'Z','M'};
    static final byte[] B_Z                     = new byte[]{'Z'};
    static final byte[] B_M                     = new byte[]{'M'};
    static final byte[] B_MAXSIDELENGTH         = new byte[]{'M','A','X','S','I','D','E','L','E','N','G','T','H'};
    
    public static final Chars KW_POINT                 = new Chars(B_POINT);
    public static final Chars KW_LINESTRING            = new Chars(B_LINESTRING);
    public static final Chars KW_CIRCULARSTRING        = new Chars(B_CIRCULARSTRING);
    public static final Chars KW_COMPOUNDCURVE         = new Chars(B_COMPOUNDCURVE);
    public static final Chars KW_CURVEPOLYGON          = new Chars(B_CURVEPOLYGON);
    public static final Chars KW_POLYGON               = new Chars(B_POLYGON);
    public static final Chars KW_TRIANGLE              = new Chars(B_TRIANGLE);
    public static final Chars KW_POLYHEDRALSURFACE     = new Chars(B_POLYHEDRALSURFACE);
    public static final Chars KW_TIN                   = new Chars(B_TIN);
    public static final Chars KW_MULTIPOINT            = new Chars(B_MULTIPOINT);
    public static final Chars KW_MULTICURVE            = new Chars(B_MULTICURVE);
    public static final Chars KW_MULTILINESTRING       = new Chars(B_MULTILINESTRING);
    public static final Chars KW_MULTISURFACE          = new Chars(B_MULTISURFACE);
    public static final Chars KW_MULTIPOLYGON          = new Chars(B_MULTIPOLYGON);
    public static final Chars KW_GEOMETRYCOLLECTION    = new Chars(B_GEOMETRYCOLLECTION);
    
    public static final Chars KW_PATCHES               = new Chars(B_PATCHES);
    public static final Chars KW_ELEMENTS              = new Chars(B_ELEMENTS);
    public static final Chars KW_POINTS                = new Chars(B_POINTS);
    public static final Chars KW_GROUPSPOT             = new Chars(B_GROUPSPOT);
    public static final Chars KW_BOUNDARY              = new Chars(B_BOUNDARY);
    public static final Chars KW_BREAKLINE             = new Chars(B_BREAKLINE);
    public static final Chars KW_SOFTBREAK             = new Chars(B_SOFTBREAK);
    public static final Chars KW_CONTROLCONTOUR        = new Chars(B_CONTROLCONTOUR);
    public static final Chars KW_BREAKVOID             = new Chars(B_BREAKVOID);
    public static final Chars KW_DRAPEVOID             = new Chars(B_DRAPEVOID);
    public static final Chars KW_VOID                  = new Chars(B_VOID);
    public static final Chars KW_HOLE                  = new Chars(B_HOLE);
    public static final Chars KW_STOPLINE              = new Chars(B_STOPLINE);
    public static final Chars KW_ID                    = new Chars(B_ID);
    public static final Chars KW_TAG                   = new Chars(B_TAG);
    public static final Chars KW_EMPTY                 = new Chars(B_EMPTY);
    public static final Chars KW_ZM                    = new Chars(B_ZM);
    public static final Chars KW_Z                     = new Chars(B_Z);
    public static final Chars KW_M                     = new Chars(B_M);
    public static final Chars KW_MAXSIDELENGTH         = new Chars(B_MAXSIDELENGTH);
    
    public static final int COORD_TYPE_2D = 0 ;
    public static final int COORD_TYPE_2DM = 1 ;
    public static final int COORD_TYPE_3D = 2 ;
    public static final int COORD_TYPE_3DM = 3 ;
    
    /** Extended WKT, geometry property index key to store the srid */
    public static final Chars SRID = new Chars(
            new byte[]{'S','R','I','D'});
    /** Geometry coordinate type key */
    public static final Chars COORD_TYPE = new Chars(
            new byte[]{'C','O','O','R','D','_','T','Y','P','E'});
    
}
