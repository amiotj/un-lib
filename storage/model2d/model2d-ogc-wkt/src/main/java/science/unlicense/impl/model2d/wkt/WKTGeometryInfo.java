
package science.unlicense.impl.model2d.wkt;

import science.unlicense.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
class WKTGeometryInfo {
    //current geometry informations
    protected int flags;
    protected Chars geomType;
    protected int coordType;
    protected int dimension;
    
}
