
package science.unlicense.impl.model2d.wkt;

import science.unlicense.impl.model2d.wkt.WKTReader;
import science.unlicense.impl.model2d.wkt.WKTConstants;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.s2d.MultiPoint;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.MultiPolyline;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.geometry.s2d.CircularString;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class WKTReaderTest {
          
    private static final double DELTA = 0.000001;
    
    private Geometry read(Chars data) throws IOException{
        final WKTReader reader = new WKTReader();
        return reader.read(data);
    }
    
    public void readGeometry() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    @Test
    public void readPoint() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT (14.3 9.5)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(2, geom.getCoordinate().getSize());
        Assert.assertEquals(14.3d, geom.getX(),DELTA);
        Assert.assertEquals(9.5d, geom.getY(),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_2D, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }
    
    @Test
    public void readPointM() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT M (14.3 9.5 4.8)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(3, geom.getCoordinate().getSize());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(4.8d, geom.getCoordinate().get(2),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_2DM, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }
    
    @Test
    public void readPointZ() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT Z (14.3 9.5 6.1)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(3, geom.getCoordinate().getSize());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(6.1d, geom.getCoordinate().get(2),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_3D, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }
        
    @Test
    public void readPointZM() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT ZM (14.3 9.5 6.1 4.8)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(4, geom.getCoordinate().getSize());
        Assert.assertEquals(14.3d, geom.getCoordinate().get(0),DELTA);
        Assert.assertEquals(9.5d, geom.getCoordinate().get(1),DELTA);
        Assert.assertEquals(6.1d, geom.getCoordinate().get(2),DELTA);
        Assert.assertEquals(4.8d, geom.getCoordinate().get(3),DELTA);
        Assert.assertEquals(WKTConstants.COORD_TYPE_3DM, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }
    
    @Test
    public void readPointEmpty() throws IOException{
        final Geometry geometry = read(
                new Chars("POINT ZM EMPTY", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Point);
        final Point geom = (Point) geometry;
        Assert.assertEquals(null, geom.getCoordinate());
        Assert.assertEquals(WKTConstants.COORD_TYPE_3DM, geom.getUserProperties().getValue(WKTConstants.COORD_TYPE));
    }
    
    @Test
    public void readLineString() throws IOException{
        final Geometry geometry = read(
                new Chars("LINESTRING (47.5 21.54, 56.3 84, -67 -12)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Polyline);
        final Polyline ls = (Polyline) geometry;
        Assert.assertEquals(3, ls.getCoordinates().getDimension());
        Assert.assertArrayEquals(new double[]{47.5,21.54}, ls.getCoordinates().getTupleDouble(0,null),DELTA);
        Assert.assertArrayEquals(new double[]{56.3,84   }, ls.getCoordinates().getTupleDouble(1,null),DELTA);
        Assert.assertArrayEquals(new double[]{-67 ,-12  }, ls.getCoordinates().getTupleDouble(2,null),DELTA);
    }
    
    @Test
    public void readPolygon() throws IOException{
        final Geometry geometry = read(
                new Chars("POLYGON ((14 7, -56.3 39.1, 80 50.2, 14 7))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof Polygon);
        final Polygon poly = (Polygon) geometry;
        Assert.assertEquals(0, poly.getHoles().getSize());
        final Polyline exterior = poly.getExterior();
        Assert.assertEquals(4, exterior.getCoordinates().getDimension());
        Assert.assertArrayEquals(new double[]{14   ,7   }, exterior.getCoordinates().getTupleDouble(0,null),DELTA);
        Assert.assertArrayEquals(new double[]{-56.3,39.1}, exterior.getCoordinates().getTupleDouble(1,null),DELTA);
        Assert.assertArrayEquals(new double[]{80   ,50.2}, exterior.getCoordinates().getTupleDouble(2,null),DELTA);
        Assert.assertArrayEquals(new double[]{14   ,7   }, exterior.getCoordinates().getTupleDouble(3,null),DELTA);
    }
    
    @Test
    public void readMultiPoint() throws IOException{
        final Geometry geometry = read(
                new Chars("MULTIPOINT ((59.1 92), (-5.2 3.56), (36 -89))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof MultiPoint);
        final MultiPoint mul = (MultiPoint) geometry;
        Assert.assertEquals(3, mul.getGeometries().getSize());
        Assert.assertEquals(new Point(new DefaultTuple(59.1,92  )), mul.getGeometries().get(0));
        Assert.assertEquals(new Point(new DefaultTuple(-5.2,3.56)), mul.getGeometries().get(1));
        Assert.assertEquals(new Point(new DefaultTuple(36  ,-89 )), mul.getGeometries().get(2));
    }
    
    @Test
    public void readMultiLineString() throws IOException{
        final Geometry geometry = read(
                new Chars("MULTILINESTRING ((-14.8 -15, 54 31), (56.9 -1.1, -10 21))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof MultiPolyline);
        final MultiPolyline mul = (MultiPolyline) geometry;
        Assert.assertEquals(2, mul.getGeometries().getSize());
        Assert.assertEquals(2,((Polyline)mul.getGeometries().get(0)).getCoordinates().getDimension());
        Assert.assertEquals(2,((Polyline)mul.getGeometries().get(1)).getCoordinates().getDimension());
        Assert.assertArrayEquals(new double[]{-14.8,-15 },((Polyline)mul.getGeometries().get(0)).getCoordinates().getTupleDouble(0,null),DELTA );
        Assert.assertArrayEquals(new double[]{54   ,31  },((Polyline)mul.getGeometries().get(0)).getCoordinates().getTupleDouble(1,null),DELTA );
        Assert.assertArrayEquals(new double[]{56.9 ,-1.1},((Polyline)mul.getGeometries().get(1)).getCoordinates().getTupleDouble(0,null),DELTA );
        Assert.assertArrayEquals(new double[]{-10  ,21  },((Polyline)mul.getGeometries().get(1)).getCoordinates().getTupleDouble(1,null),DELTA );
    }
    
    @Test
    public void readMultiPolygon() throws IOException{
        final Geometry geometry = read(
                new Chars("MULTIPOLYGON (((14 7, -56.3 39.1, 80 50.2, 14 7)))", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof MultiPolygon);
        final MultiPolygon mul = (MultiPolygon) geometry;
        Assert.assertEquals(1, mul.getGeometries().getSize());
        final Polygon poly = (Polygon) mul.getGeometries().get(0);
        Assert.assertEquals(0, poly.getHoles().getSize());
        final Polyline exterior = poly.getExterior();
        Assert.assertEquals(4, exterior.getCoordinates().getDimension());
        Assert.assertArrayEquals(new double[]{14   ,7   }, exterior.getCoordinates().getTupleDouble(0,null),DELTA);
        Assert.assertArrayEquals(new double[]{-56.3,39.1}, exterior.getCoordinates().getTupleDouble(1,null),DELTA);
        Assert.assertArrayEquals(new double[]{80   ,50.2}, exterior.getCoordinates().getTupleDouble(2,null),DELTA);
        Assert.assertArrayEquals(new double[]{14   ,7   }, exterior.getCoordinates().getTupleDouble(3,null),DELTA);
    }
    
    public void readGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    @Test
    public void readCircularString() throws IOException{
        final Geometry geometry = read(
                new Chars("CIRCULARSTRING (47.5 21.54, 56.3 84, -67 -12)", CharEncodings.US_ASCII));
        Assert.assertTrue(geometry instanceof CircularString);
        final CircularString ls = (CircularString) geometry;
        Assert.assertEquals(3, ls.getCoordinates().getDimension());
        Assert.assertArrayEquals(new double[]{47.5,21.54}, ls.getCoordinates().getTupleDouble(0,null),DELTA);
        Assert.assertArrayEquals(new double[]{56.3,84   }, ls.getCoordinates().getTupleDouble(1,null),DELTA);
        Assert.assertArrayEquals(new double[]{-67 ,-12  }, ls.getCoordinates().getTupleDouble(2,null),DELTA);
    }
        
    public void readCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void readCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void readMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void readMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
        
    public void readPolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void readTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void readTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
}
