
package science.unlicense.impl.model2d.wkt;

import science.unlicense.impl.model2d.wkt.WKTWriter;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class WKTWriterTest {
    
    private Chars write(Geometry geom) throws IOException{
        return WKTWriter.toWKT(geom);
    }
        
    @Ignore
    @Test
    public void writePoint() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_POINT2D, write(WKTTestConstants.G_POINT2D));
    }
    
    @Ignore
    @Test
    public void writeLineString() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_LINESTRING2D, write(WKTTestConstants.G_LINESTRING2D));
    }
    
    @Test
    public void writePolygon() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_POLYGON2D, write(WKTTestConstants.G_POLYGON2D));
    }
    
    @Ignore
    @Test
    public void writeMultiPoint() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_MPOINT2D, write(WKTTestConstants.G_MPOINT2D));
    }
    
    @Ignore
    @Test
    public void writeMultiLineString() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_MLINESTRING2D, write(WKTTestConstants.G_MLINESTRING2D));
    }
    
    @Ignore
    @Test
    public void writeMultiPolygon() throws IOException{
        Assert.assertEquals(WKTTestConstants.T_MPOLYGON2D, write(WKTTestConstants.G_MPOLYGON2D));
    }
    
    public void writeGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeCircularString() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writePolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }
}
