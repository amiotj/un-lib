
package science.unlicense.impl.model2d.wkb;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.s2d.MultiPoint;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.MultiPolyline;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;

/**
 * WKB Geometry reader as defined by : 
 * - OGC Simple Feature Access
 * - ISO/IEC 13249-3:2011 "Information technology, 
 *   Database languages,SQL multimedia and application packages,
 *   Part 3: Spatial" (also called SQL/MM)
 * 
 * @author Johann Sorel
 */
public class WKBReader extends AbstractReader{
    
    protected DataInputStream ds = null;
    protected WKBGeometryInfo currentGeom = null;
    
    public void reset() throws IOException{
        dispose();
        setInput(null);
        ds = null;
    }
    
    public Geometry read() throws IOException{
        ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
        
        currentGeom = readFlags();
        
        final Geometry geometry;
        final int geomType = currentGeom.geomType;
        if      (geomType == WKBConstants.GEOM_TYPE_GEOMETRY){
            geometry = readGeometry();
        }else if(geomType == WKBConstants.GEOM_TYPE_POINT){
            geometry = readPoint();
        }else if(geomType == WKBConstants.GEOM_TYPE_LINESTRING){
            geometry = readLineString();
        }else if(geomType == WKBConstants.GEOM_TYPE_POLYGON){
            geometry = readPolygon();
        }else if(geomType == WKBConstants.GEOM_TYPE_MULTIPOINT){
            geometry = readMultiPoint();
        }else if(geomType == WKBConstants.GEOM_TYPE_MULTILINESTRING){
            geometry = readMultiLineString();
        }else if(geomType == WKBConstants.GEOM_TYPE_MULTIPOLYGON){
            geometry = readMultiPolygon();
        }else if(geomType == WKBConstants.GEOM_TYPE_GEOMETRYCOLLECTION){
            geometry = readGeometryCollection();
        }else if(geomType == WKBConstants.GEOM_TYPE_CIRCULARSTRING){
            geometry = readCircularString();
        }else if(geomType == WKBConstants.GEOM_TYPE_COMPOUNDCURVE){
            geometry = readCompoundCurve();
        }else if(geomType == WKBConstants.GEOM_TYPE_CURVEPOLYGON){
            geometry = readCurvePolygon();
        }else if(geomType == WKBConstants.GEOM_TYPE_MULTICURVE){
            geometry = readMultiCurve();
        }else if(geomType == WKBConstants.GEOM_TYPE_MULTISURFACE){
            geometry = readMultiSurface();
        }else if(geomType == WKBConstants.GEOM_TYPE_CURVE){
            geometry = readCurve();
        }else if(geomType == WKBConstants.GEOM_TYPE_SURFACE){
            geometry = readSurface();
        }else if(geomType == WKBConstants.GEOM_TYPE_POLYHEDRALSURFACE){
            geometry = readPolyhedralSurface();
        }else if(geomType == WKBConstants.GEOM_TYPE_TIN){
            geometry = readTIN();
        }else if(geomType == WKBConstants.GEOM_TYPE_TRIANGLE){
            geometry = readTriangle();
        }else{
            throw new IOException("Unknowned geometry type : " + geomType);
        }
        
        geometry.getUserProperties().add(WKBConstants.COORD_TYPE, currentGeom.coordType);
        return geometry;
    }
    
    protected WKBGeometryInfo readFlags() throws IOException{
        final WKBGeometryInfo current = new WKBGeometryInfo();
        //read the encoding
        final int endian = ds.read();
        if(endian == WKBConstants.ENCODING_BIG_ENDIAN){
            current.encoding = NumberEncoding.BIG_ENDIAN;
        }else if(endian == WKBConstants.ENCODING_LITTLE_ENDIAN){
            current.encoding = NumberEncoding.LITTLE_ENDIAN;
        }else{
            throw new IOException("Unknowned encoding : " +endian);
        }
        
        //read type
        current.flags = ds.readInt(current.encoding);
        current.geomType = current.flags & 0xff;
        if(current.flags > 4000){
            current.coordType = WKBConstants.COORD_TYPE_2DM;
            current.dimension = 3;
        }else if(current.flags > 3000){
            current.coordType = WKBConstants.COORD_TYPE_3D;
            current.dimension = 3;
        }else if(current.flags > 2000){
            current.coordType = WKBConstants.COORD_TYPE_3DM;
            current.dimension = 4;
        }else{
            current.coordType = WKBConstants.COORD_TYPE_2D;
            current.dimension = 2;
        }
        
        return current;
    }
    
    private double[] readCoordinate() throws IOException{
        final double[] array = new double[currentGeom.dimension];
        ds.readDouble(array,currentGeom.encoding);
        return array;
    }
    
    private Geometry readGeometry() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Point readPoint() throws IOException{
        return new Point(readCoordinate());
    }
    
    private Polyline readLineString() throws IOException{
        final int nb = ds.readInt(currentGeom.encoding);
        final double[] array = ds.readDouble(new double[nb*currentGeom.dimension], currentGeom.encoding);
        return new Polyline(new DefaultTupleBuffer1D(array, currentGeom.dimension));
    }
    
    private Polygon readPolygon() throws IOException{
        final int nb = ds.readInt(currentGeom.encoding);
        final Polyline out = readLineString();
        final Sequence holes = new ArraySequence();
        for(int i=0;i<nb-1;i++){
            holes.add(readLineString());
        }
        return new Polygon(out,holes);
    }
    
    private MultiPoint readMultiPoint() throws IOException{
        final WKBGeometryInfo state = currentGeom;
        final int nb = ds.readInt(state.encoding);
        final Sequence points = new ArraySequence();
        for(int i=0;i<nb;i++){
            currentGeom = readFlags();
            points.add(readPoint());
        }
        currentGeom = state;
        return new MultiPoint(points);
    }
    
    private MultiPolyline readMultiLineString() throws IOException{
        final WKBGeometryInfo state = currentGeom;
        final int nb = ds.readInt(state.encoding);
        final Sequence lines = new ArraySequence();
        for(int i=0;i<nb;i++){
            currentGeom = readFlags();
            lines.add(readLineString());
        }
        currentGeom = state;
        return new MultiPolyline(lines);
    }
    
    private MultiPolygon readMultiPolygon() throws IOException{
        final WKBGeometryInfo state = currentGeom;
        final int nb = ds.readInt(state.encoding);
        final Sequence polys = new ArraySequence();
        for(int i=0;i<nb;i++){
            currentGeom = readFlags();
            polys.add(readPolygon());
        }
        currentGeom = state;
        return new MultiPolygon(polys);
    }
    
    private Geometry readGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readCircularString() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readPolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    private Geometry readTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }
        
}
