
package science.unlicense.impl.model2d.wkb;

import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.impl.geometry.s2d.MultiPoint;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.MultiPolyline;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;

/**
 *
 * @author Johann Sorel
 */
public class WKBWriter extends AbstractWriter{

    private NumberEncoding globalEncoding = NumberEncoding.BIG_ENDIAN;
    private DataOutputStream ds;
    protected WKBGeometryInfo currentGeom = null;
    
    void write(Geometry candidate) throws IOException {
        ds = getOutputAsDataStream(globalEncoding);
        
        if(candidate instanceof Point){
            final Point geom = (Point)candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.encoding = globalEncoding;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_POINT;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            writeCoordinate(geom.getCoordinate().getValues());
            
        }else if(candidate instanceof Polyline){
            final Polyline geom = (Polyline)candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.encoding = globalEncoding;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_LINESTRING;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            writeCoordinates(geom.getCoordinates());
            
        }else if(candidate instanceof Polygon){
            final Polygon geom = (Polygon)candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.encoding = globalEncoding;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_POLYGON;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getHoles().getSize()+1,currentGeom.encoding);
            writeCoordinates(geom.getExterior().getCoordinates());
            for(int i=0,n=geom.getHoles().getSize();i<n;i++){
                writeCoordinates(((Polyline)geom.getHoles().get(i)).getCoordinates());
            }
            
        }else if(candidate instanceof MultiPoint){
            final MultiPoint geom = (MultiPoint)candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.encoding = globalEncoding;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_MULTIPOINT;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getGeometries().getSize(),currentGeom.encoding);
            final WKBGeometryInfo stack = currentGeom;
            for(int i=0,n=geom.getGeometries().getSize();i<n;i++){
                write((Geometry)geom.getGeometries().get(i));
            }
            currentGeom = stack;
        }else if(candidate instanceof MultiPolyline){
            final MultiPolyline geom = (MultiPolyline)candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.encoding = globalEncoding;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_MULTILINESTRING;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getGeometries().getSize(),currentGeom.encoding);
            final WKBGeometryInfo stack = currentGeom;
            for(int i=0,n=geom.getGeometries().getSize();i<n;i++){
                write((Geometry)geom.getGeometries().get(i));
            }
            currentGeom = stack;
        }else if(candidate instanceof MultiPolygon){
            final MultiPolygon geom = (MultiPolygon)candidate;
            currentGeom = new WKBGeometryInfo();
            currentGeom.encoding = globalEncoding;
            currentGeom.geomType = WKBConstants.GEOM_TYPE_MULTIPOLYGON;
            currentGeom.coordType = WKBConstants.COORD_TYPE_2D;
            currentGeom.dimension = 2;
            writeFlag();
            ds.writeInt(geom.getGeometries().getSize(),currentGeom.encoding);
            final WKBGeometryInfo stack = currentGeom;
            for(int i=0,n=geom.getGeometries().getSize();i<n;i++){
                write((Geometry)geom.getGeometries().get(i));
            }
            currentGeom = stack;
        }else{
            throw new IOException("Unsupported geometry : "+candidate);
        }
    }
    
    private void writeFlag() throws IOException{
        //endian
        ds.write(WKBConstants.ENCODING_BIG_ENDIAN);
        
        //type and dimensions
        int val = currentGeom.geomType;
        if(currentGeom.coordType == WKBConstants.COORD_TYPE_2D){
            //2d no change
        }else if(currentGeom.coordType == WKBConstants.COORD_TYPE_2DM){
            val += 2000;
        }else if(currentGeom.coordType == WKBConstants.COORD_TYPE_3D){
            val += 1000;
        }else if(currentGeom.coordType == WKBConstants.COORD_TYPE_3DM){
            val += 3000;
        }
        
        ds.writeInt(val, currentGeom.encoding);
    }
    
    private void writeCoordinate(double[] coord) throws IOException{
        ds.writeDouble(coord, currentGeom.encoding);
    }
    
    private void writeCoordinates(TupleBuffer1D coords) throws IOException{
        ds.writeInt(coords.getDimension(),currentGeom.encoding);
        final double[] t = new double[coords.getSampleCount()];
        for(int i=0,n=coords.getDimension();i<n;i++){
            writeCoordinate(coords.getTupleDouble(i,t));
        }
    }
    
}
