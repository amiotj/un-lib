package science.unlicense.impl.model2d.wkb;

import science.unlicense.api.io.IOException;
import science.unlicense.api.geometry.Geometry;

/**
 * Extend WKB Reader as defined in PostGIS EWKB.
 * This allows to store an additional SRID information.
 * The SRID value will be stored in the user property index of the geometry
 * with the key : WKBConstants.SRID
 * 
 * @author Johann Sorel
 */
public class EWKBReader extends WKBReader{

    protected int srid = -1;

    public Geometry read() throws IOException {
        final Geometry geom = super.read();
        if(srid>=0){
            geom.getUserProperties().add(WKBConstants.SRID, srid);
        }
        return geom;
    }
    
    protected WKBGeometryInfo readFlags() throws IOException {
        WKBGeometryInfo c = super.readFlags();
        
        //srid flag is on the third bit
        if((c.flags & 0x20000000) != 0){
            srid = ds.readInt(c.encoding);
        }
        
        return c;
    }
    
}
