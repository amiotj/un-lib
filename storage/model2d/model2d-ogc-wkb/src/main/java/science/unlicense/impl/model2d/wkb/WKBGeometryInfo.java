
package science.unlicense.impl.model2d.wkb;

import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
class WKBGeometryInfo {
    //current geometry informations
    protected NumberEncoding encoding = null;
    protected int flags;
    protected int geomType;
    protected int coordType;
    protected int dimension;
    
}
