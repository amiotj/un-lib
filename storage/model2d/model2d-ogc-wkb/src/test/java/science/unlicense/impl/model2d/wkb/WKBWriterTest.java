
package science.unlicense.impl.model2d.wkb;

import science.unlicense.impl.model2d.wkb.WKBWriter;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class WKBWriterTest {
        
    private byte[] write(Geometry geom) throws IOException{
        final ArrayOutputStream stream = new ArrayOutputStream();
        final WKBWriter writer = new WKBWriter();
        writer.setOutput(stream);  
        writer.write(geom);
        return stream.getBuffer().toArrayByte();
    }
        
    @Test
    public void writePoint() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_POINT2D, write(WKBTestConstants.G_POINT2D));
    }
    
    @Test
    public void writeLineString() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_LINESTRING2D, write(WKBTestConstants.G_LINESTRING2D));
    }
    
    @Test
    public void writePolygon() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_POLYGON2D, write(WKBTestConstants.G_POLYGON2D));
    }
    
    @Test
    public void writeMultiPoint() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_MPOINT2D, write(WKBTestConstants.G_MPOINT2D));
    }
    
    @Test
    public void writeMultiLineString() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_MLINESTRING2D, write(WKBTestConstants.G_MLINESTRING2D));
    }
    
    @Test
    public void writeMultiPolygon() throws IOException{
        Assert.assertArrayEquals(WKBTestConstants.B_MPOLYGON2D, write(WKBTestConstants.G_MPOLYGON2D));
    }
    
    public void writeGeometryCollection() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeCircularString() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeCompoundCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeCurvePolygon() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeMultiCurve() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeMultiSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writePolyhedralSurface() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeTIN() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
    public void writeTriangle() throws IOException{
        throw new IOException("Not supported yet.");
    }
    
}
