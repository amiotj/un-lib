

package science.unlicense.impl.model2d.hpgl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Hewlett-Packard Graphics Language.
 * 
 * Resource : 
 * http://en.wikipedia.org/wiki/HPGL
 * http://www.hpmuseum.net/document.php?catfile=213
 * 
 * @author Johann Sorel
 */
public class HPGLFormat extends DefaultFormat {

    public HPGLFormat() {
        super(new Chars("hpgl"),
              new Chars("HPGL"),
              new Chars("Hewlett-Packard Graphics Language"),
              new Chars[]{
              },
              new Chars[]{
              },
              new byte[][]{});
    }
        
}
