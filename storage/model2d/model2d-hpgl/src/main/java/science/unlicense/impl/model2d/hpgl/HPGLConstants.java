

package science.unlicense.impl.model2d.hpgl;

/**
 * HPGL constants.
 * 
 * @author Johann Sorel
 */
public final class HPGLConstants {
        
    private HPGLConstants(){}
    
}
