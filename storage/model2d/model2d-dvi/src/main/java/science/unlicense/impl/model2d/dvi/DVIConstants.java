

package science.unlicense.impl.model2d.dvi;

/**
 * DVI constants.
 * 
 * @author Johann Sorel
 */
public final class DVIConstants {
        
    private DVIConstants(){}
    
}
