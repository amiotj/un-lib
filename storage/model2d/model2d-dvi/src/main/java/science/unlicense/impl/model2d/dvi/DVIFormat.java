

package science.unlicense.impl.model2d.dvi;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Device independent file format.
 * 
 * Resource : 
 * http://en.wikipedia.org/wiki/Device_independent_file_format
 * 
 * @author Johann Sorel
 */
public class DVIFormat extends DefaultFormat {

    public DVIFormat() {
        super(new Chars("dvi"),
              new Chars("DVI"),
              new Chars("Device independent file format"),
              new Chars[]{
                  new Chars("application/x-dvi")
              },
              new Chars[]{
                  new Chars("dvi")
              },
              new byte[][]{});
    }
        
}
