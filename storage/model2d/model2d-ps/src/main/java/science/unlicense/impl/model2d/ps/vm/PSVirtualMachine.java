package science.unlicense.impl.model2d.ps.vm;

import science.unlicense.api.collection.ArrayStack;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.color.Color;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.array.ALoad;
import science.unlicense.impl.model2d.ps.vm.array.AStore;
import science.unlicense.impl.model2d.ps.vm.array.Array;
import science.unlicense.impl.model2d.ps.vm.array.Copy;
import science.unlicense.impl.model2d.ps.vm.array.EndArray;
import science.unlicense.impl.model2d.ps.vm.array.ForAll;
import science.unlicense.impl.model2d.ps.vm.array.Get;
import science.unlicense.impl.model2d.ps.vm.array.GetInterval;
import science.unlicense.impl.model2d.ps.vm.array.Length;
import science.unlicense.impl.model2d.ps.vm.array.Put;
import science.unlicense.impl.model2d.ps.vm.array.PutInterval;
import science.unlicense.impl.model2d.ps.vm.array.StartArray;
import science.unlicense.impl.model2d.ps.vm.bitop.And;
import science.unlicense.impl.model2d.ps.vm.bitop.BitShift;
import science.unlicense.impl.model2d.ps.vm.bitop.Eq;
import science.unlicense.impl.model2d.ps.vm.bitop.False;
import science.unlicense.impl.model2d.ps.vm.bitop.Ge;
import science.unlicense.impl.model2d.ps.vm.bitop.Gt;
import science.unlicense.impl.model2d.ps.vm.bitop.Le;
import science.unlicense.impl.model2d.ps.vm.bitop.Lt;
import science.unlicense.impl.model2d.ps.vm.bitop.Ne;
import science.unlicense.impl.model2d.ps.vm.bitop.Not;
import science.unlicense.impl.model2d.ps.vm.bitop.Or;
import science.unlicense.impl.model2d.ps.vm.bitop.True;
import science.unlicense.impl.model2d.ps.vm.bitop.Xor;
import science.unlicense.impl.model2d.ps.vm.control.CountExecStack;
import science.unlicense.impl.model2d.ps.vm.control.Exec;
import science.unlicense.impl.model2d.ps.vm.control.ExecStack;
import science.unlicense.impl.model2d.ps.vm.control.Exit;
import science.unlicense.impl.model2d.ps.vm.control.For;
import science.unlicense.impl.model2d.ps.vm.control.If;
import science.unlicense.impl.model2d.ps.vm.control.IfElse;
import science.unlicense.impl.model2d.ps.vm.control.Loop;
import science.unlicense.impl.model2d.ps.vm.control.Quit;
import science.unlicense.impl.model2d.ps.vm.control.Repeat;
import science.unlicense.impl.model2d.ps.vm.control.Start;
import science.unlicense.impl.model2d.ps.vm.control.Stop;
import science.unlicense.impl.model2d.ps.vm.control.Stopped;
import science.unlicense.impl.model2d.ps.vm.dict.Begin;
import science.unlicense.impl.model2d.ps.vm.dict.ClearDictStack;
import science.unlicense.impl.model2d.ps.vm.dict.CountDictStack;
import science.unlicense.impl.model2d.ps.vm.dict.CurrentDict;
import science.unlicense.impl.model2d.ps.vm.dict.Def;
import science.unlicense.impl.model2d.ps.vm.dict.Dict;
import science.unlicense.impl.model2d.ps.vm.dict.DictStack;
import science.unlicense.impl.model2d.ps.vm.dict.End;
import science.unlicense.impl.model2d.ps.vm.dict.ErrorDict;
import science.unlicense.impl.model2d.ps.vm.dict.GlobalDict;
import science.unlicense.impl.model2d.ps.vm.dict.Known;
import science.unlicense.impl.model2d.ps.vm.dict.Load;
import science.unlicense.impl.model2d.ps.vm.dict.MaxLength;
import science.unlicense.impl.model2d.ps.vm.dict.MaxMax;
import science.unlicense.impl.model2d.ps.vm.dict.MinMin;
import science.unlicense.impl.model2d.ps.vm.dict.StatusDict;
import science.unlicense.impl.model2d.ps.vm.dict.Store;
import science.unlicense.impl.model2d.ps.vm.dict.SystemDict;
import science.unlicense.impl.model2d.ps.vm.dict.Undef;
import science.unlicense.impl.model2d.ps.vm.dict.UserDict;
import science.unlicense.impl.model2d.ps.vm.dict.Where;
import science.unlicense.impl.model2d.ps.vm.file.BytesAvailable;
import science.unlicense.impl.model2d.ps.vm.file.CloseFile;
import science.unlicense.impl.model2d.ps.vm.file.CurrentFile;
import science.unlicense.impl.model2d.ps.vm.file.CurrentObjectFormat;
import science.unlicense.impl.model2d.ps.vm.file.DeleteFile;
import science.unlicense.impl.model2d.ps.vm.file.Equal;
import science.unlicense.impl.model2d.ps.vm.file.EqualEqual;
import science.unlicense.impl.model2d.ps.vm.file.File;
import science.unlicense.impl.model2d.ps.vm.file.FileNameForAll;
import science.unlicense.impl.model2d.ps.vm.file.FilePosition;
import science.unlicense.impl.model2d.ps.vm.file.Filter;
import science.unlicense.impl.model2d.ps.vm.file.Flush;
import science.unlicense.impl.model2d.ps.vm.file.FlushFile;
import science.unlicense.impl.model2d.ps.vm.file.PStack;
import science.unlicense.impl.model2d.ps.vm.file.Print;
import science.unlicense.impl.model2d.ps.vm.file.PrintObject;
import science.unlicense.impl.model2d.ps.vm.file.Read;
import science.unlicense.impl.model2d.ps.vm.file.ReadHexString;
import science.unlicense.impl.model2d.ps.vm.file.ReadLine;
import science.unlicense.impl.model2d.ps.vm.file.ReadString;
import science.unlicense.impl.model2d.ps.vm.file.RenameFile;
import science.unlicense.impl.model2d.ps.vm.file.ResetFile;
import science.unlicense.impl.model2d.ps.vm.file.Run;
import science.unlicense.impl.model2d.ps.vm.file.SetFilePosition;
import science.unlicense.impl.model2d.ps.vm.file.SetObjectFormat;
import science.unlicense.impl.model2d.ps.vm.file.Stack;
import science.unlicense.impl.model2d.ps.vm.file.Status;
import science.unlicense.impl.model2d.ps.vm.file.Write;
import science.unlicense.impl.model2d.ps.vm.file.WriteHexString;
import science.unlicense.impl.model2d.ps.vm.file.WriteObject;
import science.unlicense.impl.model2d.ps.vm.file.WriteString;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentBlackGeneration;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentColorRendering;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentColorScreen;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentColorTransfer;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentFlat;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentHalfTone;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentOverPrint;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentScreen;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentSmoothness;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentTransfer;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.CurrentUnderColorRemoval;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetBlackGeneration;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetColorRendering;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetColorScreen;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetColorTransfer;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetFlat;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetHalfTone;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetOverPrint;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetScreen;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetSmoothness;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetTransfer;
import science.unlicense.impl.model2d.ps.vm.graphicstatedep.SetUnderColorRemoval;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.ClipRestore;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.ClipSave;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentCMYKColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentColorspace;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentDash;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentGState;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentGray;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentHSBColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentLineCap;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentLineJoin;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentLineWidth;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentMiterLimit;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentRGBColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.CurrentStrokeAdjust;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.GRestore;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.GRestoreAll;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.GSave;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.GState;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.InitGraphics;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetCMYKColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetColorspace;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetDash;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetGState;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetGray;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetHSBColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetLineCap;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetLineJoin;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetLineWidth;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetMiterLimit;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetRGBColor;
import science.unlicense.impl.model2d.ps.vm.graphicstateindep.SetStrokeAdjust;
import science.unlicense.impl.model2d.ps.vm.insideness.InEOFill;
import science.unlicense.impl.model2d.ps.vm.insideness.InFill;
import science.unlicense.impl.model2d.ps.vm.insideness.InStroke;
import science.unlicense.impl.model2d.ps.vm.insideness.InUEOFill;
import science.unlicense.impl.model2d.ps.vm.insideness.InUFill;
import science.unlicense.impl.model2d.ps.vm.insideness.InUStroke;
import science.unlicense.impl.model2d.ps.vm.math.Abs;
import science.unlicense.impl.model2d.ps.vm.math.Add;
import science.unlicense.impl.model2d.ps.vm.math.Atan;
import science.unlicense.impl.model2d.ps.vm.math.Ceiling;
import science.unlicense.impl.model2d.ps.vm.math.Cos;
import science.unlicense.impl.model2d.ps.vm.math.Div;
import science.unlicense.impl.model2d.ps.vm.math.Exp;
import science.unlicense.impl.model2d.ps.vm.math.Floor;
import science.unlicense.impl.model2d.ps.vm.math.IDiv;
import science.unlicense.impl.model2d.ps.vm.math.Ln;
import science.unlicense.impl.model2d.ps.vm.math.Log;
import science.unlicense.impl.model2d.ps.vm.math.Mod;
import science.unlicense.impl.model2d.ps.vm.math.Mul;
import science.unlicense.impl.model2d.ps.vm.math.Neg;
import science.unlicense.impl.model2d.ps.vm.math.RRand;
import science.unlicense.impl.model2d.ps.vm.math.Rand;
import science.unlicense.impl.model2d.ps.vm.math.Round;
import science.unlicense.impl.model2d.ps.vm.math.SRand;
import science.unlicense.impl.model2d.ps.vm.math.Sin;
import science.unlicense.impl.model2d.ps.vm.math.Sqrt;
import science.unlicense.impl.model2d.ps.vm.math.Sub;
import science.unlicense.impl.model2d.ps.vm.math.Truncate;
import science.unlicense.impl.model2d.ps.vm.matrix.Concat;
import science.unlicense.impl.model2d.ps.vm.matrix.ConcatMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.CurrentMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.DTransform;
import science.unlicense.impl.model2d.ps.vm.matrix.DefaultMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.IDTransform;
import science.unlicense.impl.model2d.ps.vm.matrix.ITransform;
import science.unlicense.impl.model2d.ps.vm.matrix.IdentMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.InitMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.InvertMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.Matrix;
import science.unlicense.impl.model2d.ps.vm.matrix.Rotate;
import science.unlicense.impl.model2d.ps.vm.matrix.Scale;
import science.unlicense.impl.model2d.ps.vm.matrix.SetMatrix;
import science.unlicense.impl.model2d.ps.vm.matrix.Transform;
import science.unlicense.impl.model2d.ps.vm.matrix.Translate;
import science.unlicense.impl.model2d.ps.vm.memory.CurrentGlobal;
import science.unlicense.impl.model2d.ps.vm.memory.DefineUserObject;
import science.unlicense.impl.model2d.ps.vm.memory.ExecUserObject;
import science.unlicense.impl.model2d.ps.vm.memory.GCheck;
import science.unlicense.impl.model2d.ps.vm.memory.Restore;
import science.unlicense.impl.model2d.ps.vm.memory.Save;
import science.unlicense.impl.model2d.ps.vm.memory.SetGlobal;
import science.unlicense.impl.model2d.ps.vm.memory.StartJob;
import science.unlicense.impl.model2d.ps.vm.memory.UndefineUserObject;
import science.unlicense.impl.model2d.ps.vm.memory.UserObjects;
import science.unlicense.impl.model2d.ps.vm.misc.Bind;
import science.unlicense.impl.model2d.ps.vm.misc.Echo;
import science.unlicense.impl.model2d.ps.vm.misc.Executive;
import science.unlicense.impl.model2d.ps.vm.misc.LanguageLevel;
import science.unlicense.impl.model2d.ps.vm.misc.Null;
import science.unlicense.impl.model2d.ps.vm.misc.Product;
import science.unlicense.impl.model2d.ps.vm.misc.Prompt;
import science.unlicense.impl.model2d.ps.vm.misc.RealTime;
import science.unlicense.impl.model2d.ps.vm.misc.Revision;
import science.unlicense.impl.model2d.ps.vm.misc.SerialNumber;
import science.unlicense.impl.model2d.ps.vm.misc.UserTime;
import science.unlicense.impl.model2d.ps.vm.misc.Version;
import science.unlicense.impl.model2d.ps.vm.packedarray.CurrentPacking;
import science.unlicense.impl.model2d.ps.vm.packedarray.PackedArray;
import science.unlicense.impl.model2d.ps.vm.packedarray.SetPacking;
import science.unlicense.impl.model2d.ps.vm.painting.ColorImage;
import science.unlicense.impl.model2d.ps.vm.painting.EOFill;
import science.unlicense.impl.model2d.ps.vm.painting.ErasePage;
import science.unlicense.impl.model2d.ps.vm.painting.Fill;
import science.unlicense.impl.model2d.ps.vm.painting.Image;
import science.unlicense.impl.model2d.ps.vm.painting.ImageMask;
import science.unlicense.impl.model2d.ps.vm.painting.RectFill;
import science.unlicense.impl.model2d.ps.vm.painting.RectStroke;
import science.unlicense.impl.model2d.ps.vm.painting.SHFill;
import science.unlicense.impl.model2d.ps.vm.painting.Stroke;
import science.unlicense.impl.model2d.ps.vm.painting.UEOFill;
import science.unlicense.impl.model2d.ps.vm.painting.UFill;
import science.unlicense.impl.model2d.ps.vm.painting.UStroke;
import science.unlicense.impl.model2d.ps.vm.path.Arc;
import science.unlicense.impl.model2d.ps.vm.path.ArcTo;
import science.unlicense.impl.model2d.ps.vm.path.Arcn;
import science.unlicense.impl.model2d.ps.vm.path.Arct;
import science.unlicense.impl.model2d.ps.vm.path.CharPath;
import science.unlicense.impl.model2d.ps.vm.path.Clip;
import science.unlicense.impl.model2d.ps.vm.path.ClipPath;
import science.unlicense.impl.model2d.ps.vm.path.ClosePath;
import science.unlicense.impl.model2d.ps.vm.path.CurrentPoint;
import science.unlicense.impl.model2d.ps.vm.path.CurveTo;
import science.unlicense.impl.model2d.ps.vm.path.EOClip;
import science.unlicense.impl.model2d.ps.vm.path.FlattenPath;
import science.unlicense.impl.model2d.ps.vm.path.InitClip;
import science.unlicense.impl.model2d.ps.vm.path.LineTo;
import science.unlicense.impl.model2d.ps.vm.path.MoveTo;
import science.unlicense.impl.model2d.ps.vm.path.NewPath;
import science.unlicense.impl.model2d.ps.vm.path.PathBBox;
import science.unlicense.impl.model2d.ps.vm.path.PathForAll;
import science.unlicense.impl.model2d.ps.vm.path.RCurveTo;
import science.unlicense.impl.model2d.ps.vm.path.RLineTo;
import science.unlicense.impl.model2d.ps.vm.path.RMoveTo;
import science.unlicense.impl.model2d.ps.vm.path.RectClip;
import science.unlicense.impl.model2d.ps.vm.path.ReversePath;
import science.unlicense.impl.model2d.ps.vm.path.SetBBox;
import science.unlicense.impl.model2d.ps.vm.path.StrokePath;
import science.unlicense.impl.model2d.ps.vm.path.UAppend;
import science.unlicense.impl.model2d.ps.vm.path.UCache;
import science.unlicense.impl.model2d.ps.vm.path.UPath;
import science.unlicense.impl.model2d.ps.vm.path.UStrokePath;
import science.unlicense.impl.model2d.ps.vm.device.CopyPage;
import science.unlicense.impl.model2d.ps.vm.device.CurrentPageDevice;
import science.unlicense.impl.model2d.ps.vm.pattern.ExecForm;
import science.unlicense.impl.model2d.ps.vm.pattern.MakePattern;
import science.unlicense.impl.model2d.ps.vm.device.NullDevice;
import science.unlicense.impl.model2d.ps.vm.device.SetPageDevice;
import science.unlicense.impl.model2d.ps.vm.pattern.SetPattern;
import science.unlicense.impl.model2d.ps.vm.device.ShowPage;
import science.unlicense.impl.model2d.ps.vm.font.AShow;
import science.unlicense.impl.model2d.ps.vm.font.AWidthShow;
import science.unlicense.impl.model2d.ps.vm.font.CShow;
import science.unlicense.impl.model2d.ps.vm.font.ComposeFont;
import science.unlicense.impl.model2d.ps.vm.font.CurrentFont;
import science.unlicense.impl.model2d.ps.vm.font.DefineFont;
import science.unlicense.impl.model2d.ps.vm.font.FindEncoding;
import science.unlicense.impl.model2d.ps.vm.font.FindFont;
import science.unlicense.impl.model2d.ps.vm.font.FontDirectory;
import science.unlicense.impl.model2d.ps.vm.font.GlobalFontDirectory;
import science.unlicense.impl.model2d.ps.vm.font.GlyphShow;
import science.unlicense.impl.model2d.ps.vm.font.ISOLatin1Encoding;
import science.unlicense.impl.model2d.ps.vm.font.KShow;
import science.unlicense.impl.model2d.ps.vm.font.MakeFont;
import science.unlicense.impl.model2d.ps.vm.font.RootFont;
import science.unlicense.impl.model2d.ps.vm.font.ScaleFont;
import science.unlicense.impl.model2d.ps.vm.font.SelectFont;
import science.unlicense.impl.model2d.ps.vm.font.SetCacheDevice;
import science.unlicense.impl.model2d.ps.vm.font.SetCacheDevice2;
import science.unlicense.impl.model2d.ps.vm.font.SetCharWidth;
import science.unlicense.impl.model2d.ps.vm.font.SetFont;
import science.unlicense.impl.model2d.ps.vm.font.Show;
import science.unlicense.impl.model2d.ps.vm.font.StandardEncoding;
import science.unlicense.impl.model2d.ps.vm.font.StringWidth;
import science.unlicense.impl.model2d.ps.vm.font.UndefineFont;
import science.unlicense.impl.model2d.ps.vm.font.WidthShow;
import science.unlicense.impl.model2d.ps.vm.font.XShow;
import science.unlicense.impl.model2d.ps.vm.font.XYShow;
import science.unlicense.impl.model2d.ps.vm.font.YShow;
import science.unlicense.impl.model2d.ps.vm.interpreter.CacheStatus;
import science.unlicense.impl.model2d.ps.vm.interpreter.CurrentCacheParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.CurrentDevParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.CurrentSystemParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.CurrentUserParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetCacheLimit;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetCacheParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetDevParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetSystemParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetUCacheParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetUserParams;
import science.unlicense.impl.model2d.ps.vm.interpreter.SetVMThreshold;
import science.unlicense.impl.model2d.ps.vm.interpreter.UCacheStatus;
import science.unlicense.impl.model2d.ps.vm.interpreter.VMReclaim;
import science.unlicense.impl.model2d.ps.vm.interpreter.VMStatus;
import science.unlicense.impl.model2d.ps.vm.presentation.A4;
import science.unlicense.impl.model2d.ps.vm.presentation.A4Small;
import science.unlicense.impl.model2d.ps.vm.presentation.B5;
import science.unlicense.impl.model2d.ps.vm.presentation.Ledger;
import science.unlicense.impl.model2d.ps.vm.presentation.Legal;
import science.unlicense.impl.model2d.ps.vm.presentation.Letter;
import science.unlicense.impl.model2d.ps.vm.presentation.LetterSmall;
import science.unlicense.impl.model2d.ps.vm.resource.DefineResource;
import science.unlicense.impl.model2d.ps.vm.resource.FindColorRendering;
import science.unlicense.impl.model2d.ps.vm.resource.FindResource;
import science.unlicense.impl.model2d.ps.vm.resource.ResourceForAll;
import science.unlicense.impl.model2d.ps.vm.resource.ResourceStatus;
import science.unlicense.impl.model2d.ps.vm.resource.UndefineResource;
import science.unlicense.impl.model2d.ps.vm.stack.Clear;
import science.unlicense.impl.model2d.ps.vm.stack.ClearToMark;
import science.unlicense.impl.model2d.ps.vm.stack.Count;
import science.unlicense.impl.model2d.ps.vm.stack.CountToMark;
import science.unlicense.impl.model2d.ps.vm.stack.Dup;
import science.unlicense.impl.model2d.ps.vm.stack.Exch;
import science.unlicense.impl.model2d.ps.vm.stack.Index;
import science.unlicense.impl.model2d.ps.vm.stack.Mark;
import science.unlicense.impl.model2d.ps.vm.stack.Pop;
import science.unlicense.impl.model2d.ps.vm.stack.Roll;
import science.unlicense.impl.model2d.ps.vm.string.AnchorSearch;
import science.unlicense.impl.model2d.ps.vm.string.Search;
import science.unlicense.impl.model2d.ps.vm.string.Token;
import science.unlicense.impl.model2d.ps.vm.type.Cvi;
import science.unlicense.impl.model2d.ps.vm.type.Cvlit;
import science.unlicense.impl.model2d.ps.vm.type.Cvn;
import science.unlicense.impl.model2d.ps.vm.type.Cvr;
import science.unlicense.impl.model2d.ps.vm.type.Cvrs;
import science.unlicense.impl.model2d.ps.vm.type.Cvs;
import science.unlicense.impl.model2d.ps.vm.type.Cvx;
import science.unlicense.impl.model2d.ps.vm.type.ExecuteOnly;
import science.unlicense.impl.model2d.ps.vm.type.NoAccess;
import science.unlicense.impl.model2d.ps.vm.type.RCheck;
import science.unlicense.impl.model2d.ps.vm.type.ReadOnly;
import science.unlicense.impl.model2d.ps.vm.type.Type;
import science.unlicense.impl.model2d.ps.vm.type.WCheck;
import science.unlicense.impl.model2d.ps.vm.type.XCheck;

/**
 * A Postscript virtual machine interpret the postscript code.
 * While this VM undetstand postscript it does not do the related rendering
 * or queries to the user, For this you must provide the VM a Painter2D and a Prompter.
 * 
 * @author Johann Sorel
 */
public class PSVirtualMachine {

    //objects reuquiered to implement all postcript fonctionnalities.
    static final Dictionary systemDict = new HashDictionary();

    private final VMState vmstate = new VMState();


    private Painter2D painter;
    private GraphicState graphicState;
    
    static {
        //reference : PLRM.pdf page 521
        final PSFunction[] functions = new PSFunction[]{
            //stack
            new Pop(), new Exch(), new Dup(), new science.unlicense.impl.model2d.ps.vm.stack.Copy(),
            new Index(), new Roll(), new Clear(), new Count(), new Mark(), new ClearToMark(),
            new CountToMark(),
            //arithmetic
            new Add(), new Div(), new IDiv(), new Mod(), new Mul(), new Sub(), 
            new Abs(), new Neg(), new Ceiling(), new Floor(), new Round(), new Truncate(), 
            new Sqrt(), new Atan(), new Cos(), new Sin(), new Exp(), new Ln(),
            new Log(), new Rand(), new SRand(), new RRand(),
            //array
            new Array(), new StartArray(), new EndArray(), new Length(), new Get(),
            new Put(), new GetInterval(), new PutInterval(), new AStore(), new ALoad(),
            new Copy(), new ForAll(),
            //packed array
            new PackedArray(), new SetPacking(), new CurrentPacking(), new science.unlicense.impl.model2d.ps.vm.packedarray.Length(),
            new science.unlicense.impl.model2d.ps.vm.packedarray.Get(), new science.unlicense.impl.model2d.ps.vm.packedarray.GetInterval(),
            new science.unlicense.impl.model2d.ps.vm.packedarray.ALoad(), new science.unlicense.impl.model2d.ps.vm.packedarray.Copy(),
            new science.unlicense.impl.model2d.ps.vm.packedarray.ForAll(),
            //dictionnary
            new Dict(), new MinMin(), new MaxMax(), new science.unlicense.impl.model2d.ps.vm.dict.Length(),
            new MaxLength(), new Begin(), new End(), new Def(), new Load(), new Store(),
            new science.unlicense.impl.model2d.ps.vm.dict.Get(), new science.unlicense.impl.model2d.ps.vm.dict.Put(),
            new Undef(), new Known(), new Where(), new science.unlicense.impl.model2d.ps.vm.dict.Copy(),
            new science.unlicense.impl.model2d.ps.vm.dict.ForAll(), new CurrentDict(), new ErrorDict(),
            new science.unlicense.impl.model2d.ps.vm.dict.Error(), new SystemDict(), new UserDict(),
            new GlobalDict(), new StatusDict(), new CountDictStack(), new DictStack(),
            new ClearDictStack(),
            //string
            new science.unlicense.impl.model2d.ps.vm.string.String(), new science.unlicense.impl.model2d.ps.vm.string.Length(),
            new science.unlicense.impl.model2d.ps.vm.string.Get(), new science.unlicense.impl.model2d.ps.vm.string.Put(),
            new science.unlicense.impl.model2d.ps.vm.string.GetInterval(), new science.unlicense.impl.model2d.ps.vm.string.PutInterval(),
            new science.unlicense.impl.model2d.ps.vm.string.Copy(), new science.unlicense.impl.model2d.ps.vm.string.ForAll(),
            new AnchorSearch(), new Search(), new Token(),
            //relation,boolean,bitwise
            new Eq(), new Ne(), new Ge(), new Gt(), new Le(), new Lt(), new And(),
            new Not(), new Or(), new Xor(), new True(), new False(), new BitShift(),
            //control
            new Exec(), new If(), new IfElse(), new For(), new Repeat(), new Loop(),
            new Exit(), new Stop(), new Stopped(), new CountExecStack(), new ExecStack(),
            new Quit(), new Start(),
            //type,attribute,conversion
            new Type(), new Cvlit(), new Cvx(), new XCheck(), new ExecuteOnly(),
            new NoAccess(), new ReadOnly(), new RCheck(), new WCheck(), new Cvi(),
            new Cvn(), new Cvr(), new Cvrs(), new Cvs(),
            //file
            new File(), new Filter(), new CloseFile(), new Read(), new Write(),
            new ReadHexString(), new WriteHexString(), new ReadString(), new WriteString(),
            new ReadLine(), new science.unlicense.impl.model2d.ps.vm.file.Token(), new BytesAvailable(),
            new Flush(), new FlushFile(), new ResetFile(), new Status(), new Run(),
            new CurrentFile(), new DeleteFile(), new RenameFile(), new FileNameForAll(),
            new SetFilePosition(), new FilePosition(), new Print(), new Equal(),
            new EqualEqual(), new Stack(), new PStack(), new PrintObject(), new WriteObject(),
            new SetObjectFormat(), new CurrentObjectFormat(),
            //resource
            new DefineResource(), new UndefineResource(), new FindResource(),
            new FindColorRendering(), new ResourceStatus(), new ResourceForAll(),
            //virtual memory
            new Save(), new Restore(), new SetGlobal(), new CurrentGlobal(),
            new GCheck(), new StartJob(), new DefineUserObject(), new ExecUserObject(),
            new UndefineUserObject(), new UserObjects(),
            //miscellaneous
            new Bind(), new Null(), new Version(), new RealTime(), new UserTime(),
            new LanguageLevel(), new Product(), new Revision(), new SerialNumber(),
            new Executive(), new Echo(), new Prompt(),
            //graphic state independante
            new GSave(), new GRestore(), new ClipSave(), new ClipRestore(),
            new GRestoreAll(), new InitGraphics(), new GState(), new SetGState(),
            new CurrentGState(), new SetLineWidth(), new CurrentLineWidth(),
            new SetLineCap(), new CurrentLineCap(), new SetLineJoin(), new CurrentLineJoin(),
            new SetMiterLimit(), new CurrentMiterLimit(), new SetStrokeAdjust(),
            new CurrentStrokeAdjust(), new SetDash(), new CurrentDash(),
            new SetColorspace(), new CurrentColorspace(), new SetColor(),
            new CurrentColor(), new SetGray(), new CurrentGray(), new SetHSBColor(),
            new CurrentHSBColor(), new SetRGBColor(), new CurrentRGBColor(),
            new SetCMYKColor(), new CurrentCMYKColor(),
            //graphic state dependante
            new SetHalfTone(), new CurrentHalfTone(), new SetScreen(), new CurrentScreen(),
            new SetColorScreen(), new CurrentColorScreen(), new SetTransfer(),
            new CurrentTransfer(), new SetColorTransfer(), new CurrentColorTransfer(),
            new SetBlackGeneration(), new CurrentBlackGeneration(), new SetUnderColorRemoval(),
            new CurrentUnderColorRemoval(), new SetColorRendering(), new CurrentColorRendering(),
            new SetFlat(), new CurrentFlat(), new SetOverPrint(), new CurrentOverPrint(),
            new SetSmoothness(), new CurrentSmoothness(),
            //coordinate system, matrix
            new Matrix(), new InitMatrix(), new IdentMatrix(), new DefaultMatrix(),
            new CurrentMatrix(), new SetMatrix(), new Translate(), new Scale(),
            new Rotate(), new Concat(), new ConcatMatrix(), new Transform(),
            new DTransform(), new ITransform(), new IDTransform(), new InvertMatrix(),
            //path construction
            new NewPath(), new CurrentPoint(), new MoveTo(), new RMoveTo(), new LineTo(),
            new RLineTo(), new Arc(), new Arcn(), new Arct(), new ArcTo(),
            new CurveTo(), new RCurveTo(), new ClosePath(), new FlattenPath(),
            new ReversePath(), new StrokePath(), new UStrokePath(), new CharPath(),
            new UAppend(), new ClipPath(), new SetBBox(), new PathBBox(), new PathForAll(),
            new UPath(), new InitClip(), new Clip(), new EOClip(), new RectClip(),
            new UCache(),
            //painting
            new ErasePage(), new Stroke(), new Fill(), new EOFill(), new RectStroke(),
            new RectFill(), new UStroke(), new UFill(), new UEOFill(), new SHFill(),
            new Image(), new ColorImage(), new ImageMask(),
            //insideness testing
            new InFill(), new InEOFill(), new InUFill(), new InUEOFill(),
            new InStroke(), new InUStroke(),
            //form and pattern
            new MakePattern(), new SetPattern(), new ExecForm(), 
            //device setup and output
            new ShowPage(), new CopyPage(), new SetPageDevice(),
            new CurrentPageDevice(), new NullDevice(),
            //glyph and font
            new DefineFont(), new ComposeFont(), new UndefineFont(), new FindFont(),
            new ScaleFont(), new MakeFont(), new SetFont(), new RootFont(), new CurrentFont(),
            new SelectFont(), new Show(), new AShow(), new WidthShow(), new AWidthShow(),
            new XShow(), new XYShow(), new YShow(), new GlyphShow(), new StringWidth(),
            new CShow(), new KShow(), new FontDirectory(), new GlobalFontDirectory(),
            new StandardEncoding(), new ISOLatin1Encoding(), new FindEncoding(),
            new SetCacheDevice(), new SetCacheDevice2(), new SetCharWidth(),
            //interpreter
            new SetSystemParams(), new CurrentSystemParams(), new SetUserParams(),
            new CurrentUserParams(), new SetDevParams(), new CurrentDevParams(),
            new VMReclaim(), new SetVMThreshold(), new VMStatus(), new CacheStatus(),
            new SetCacheLimit(), new SetCacheParams(), new CurrentCacheParams(),
            new SetUCacheParams(), new UCacheStatus(),
            //presentation, page 824
            new A4(), new A4Small(), new B5(), new Ledger(), new Legal(),
            new Letter(), new LetterSmall()

        };
        for(PSFunction f : functions){
            systemDict.add(f.getName(), f);
        }
    }
        

    public PSVirtualMachine() {
        //dictionnary stack starts with : system,global and user dictionaries
        vmstate.dictStack.add(systemDict);
        vmstate.dictStack.add(vmstate.globalDict);
        vmstate.dictStack.add(vmstate.userDict);
        //default graphic stack
        graphicState = new GraphicState(false);
    }

    public Painter2D getPainter() {
        return painter;
    }

    public void setPainter(Painter2D painter) {
        this.painter = painter;
        graphicState.restore(painter);
    }

    public science.unlicense.api.collection.Stack getOperandStack() {
        return vmstate.operandStack;
    }

    public ArrayStack getDictStack() {
        return vmstate.dictStack;
    }

    public science.unlicense.api.collection.Stack getExecStack() {
        return vmstate.execStack;
    }

    public science.unlicense.api.collection.Stack getGraphicStack() {
        return vmstate.graphicStack;
    }

    public Dictionary getSystemDictionary() {
        return systemDict;
    }

    public Dictionary getGlobalDictionary() {
        return vmstate.globalDict;
    }

    public Dictionary getUserDictionary() {
        return vmstate.userDict;
    }

    public Dictionary getStatusDictionary() {
        return vmstate.statusDict;
    }

    public Dictionary getCurrentDictionary() {
        return (Dictionary) vmstate.dictStack.lookEnd();
    }

    public GraphicState getCurrentGraphicState(){
        return graphicState;
    }

    public Object resolve(PSPointer pointer){
        return vmstate.heap.getValue(pointer.name);
    }

    public void push(Object candidate) throws VMException{
        if(candidate instanceof PSCall){
            final PSCall call = (PSCall) candidate;
            
            Object fct = null;
            for(int i=vmstate.dictStack.getSize()-1;fct==null && i>=0;i--){
                fct = ((Dictionary)vmstate.dictStack.get(i)).getValue(call.functionName);
            }
            
            if(fct instanceof PSFunction){
                ((PSFunction)fct).execute(this);                
            }else if(fct instanceof PSProcedure){
                ((PSProcedure)fct).execute(this);
            }else {
                throw new VMException("Unknown function "+call.functionName);
            }
        }else{
            vmstate.operandStack.add(candidate);
        }
    }
    
    public Object pull(){
        return vmstate.operandStack.pickEnd();
    }

}
