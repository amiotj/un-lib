package science.unlicense.impl.model2d.ps.vm.array;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Create a new array of given size.
 *
 * Stack in|out :
 * size | -
 * 
 * @author Johann Sorel
 */
public class Array extends PSFunction {

    public static final Chars NAME = new Chars("array");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final int size = ((Number) pullValue(vm)).intValue();
        final Object[] array = new Object[size];
        vm.push(array);
    }

}
