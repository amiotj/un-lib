package science.unlicense.impl.model2d.ps.vm.math;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class Mul extends PSFunction {

    public static final Chars NAME = new Chars("mul");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Double v1 = (Double) vm.pull();
        Double v2 = (Double) vm.pull();
        vm.push(v1 * v2);
    }

}
