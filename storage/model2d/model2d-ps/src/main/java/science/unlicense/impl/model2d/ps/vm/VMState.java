
package science.unlicense.impl.model2d.ps.vm;

import science.unlicense.api.collection.ArrayStack;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 * Stores the VM state.
 *
 * @author Johann Sorel
 */
public class VMState {

    final science.unlicense.api.collection.Stack operandStack = new ArrayStack();
    final ArrayStack dictStack = new ArrayStack();
    final science.unlicense.api.collection.Stack execStack = new ArrayStack();
    final science.unlicense.api.collection.Stack graphicStack = new ArrayStack();
    final Dictionary heap = new HashDictionary();

    final Dictionary userDict = new HashDictionary();
    final Dictionary globalDict = new HashDictionary();
    final Dictionary statusDict = new HashDictionary();

    public void restore(VMState state){
        
    }

    public VMState copy(){
        final VMState state = new VMState();
        return state;
    }
    
}
