package science.unlicense.impl.model2d.ps.vm.array;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Store all elements of the stack in an array.
 *
 * Stack in|out :
 * o1 ... o2 array | array
 *
 * @author Johann Sorel
 */
public class AStore extends PSFunction {

    public static final Chars NAME = new Chars("astore");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Object[] array = (Object[]) pullValue(vm);
        for(int i=array.length-1;i>=0;i--){
            array[i] = vm.pull();
        }
        vm.push(array);
    }

}
