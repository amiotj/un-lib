package science.unlicense.impl.model2d.ps.vm.dict;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSPointer;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Put in current dictionary the given key and associated value.
 * Page 583
 * 
 * Stack in|out :
 * key value | - 
 * 
 * @author Johann Sorel
 */
public class Def extends PSFunction {

    public static final Chars NAME = new Chars("def");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Object value = vm.pull();
        final Object key = vm.pull();
        vm.getCurrentDictionary().add(((PSPointer)key).name, value);
    }

}
