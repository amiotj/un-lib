package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Reset graphics state.
 * 
 * Stack in|out :
 * - | -
 * 
 * @author Johann Sorel
 */
public class InitGraphics extends PSFunction {

    public static final Chars NAME = new Chars("initgraphics");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final GraphicState state = vm.getCurrentGraphicState();
        state.reset();
    }

}
