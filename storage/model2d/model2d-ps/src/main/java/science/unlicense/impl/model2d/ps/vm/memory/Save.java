package science.unlicense.impl.model2d.ps.vm.memory;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Save a copy of the current vm state on the stack.
 *
 * Stack in|out :
 * - | save
 *
 * @author Johann Sorel
 */
public class Save extends PSFunction {

    public static final Chars NAME = new Chars("save");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO
    }

}
