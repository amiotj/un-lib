package science.unlicense.impl.model2d.ps.vm.font;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.painter2d.Font;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Set font.
 *
 * Stack in|out :
 * font | -
 * 
 * @author Johann Sorel
 */
public class SetFont extends PSFunction {

    public static final Chars NAME = new Chars("setfont");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Font font = (Font) pullValue(vm);
        final GraphicState state = vm.getCurrentGraphicState();
        state.font = font;
        vm.getPainter().setFont(font);
    }

}
