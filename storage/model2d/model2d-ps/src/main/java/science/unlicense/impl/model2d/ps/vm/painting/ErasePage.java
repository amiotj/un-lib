package science.unlicense.impl.model2d.ps.vm.painting;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Erase current page.
 * 
 * Stack in|out :
 * - | -
 * 
 * @author Johann Sorel
 */
public class ErasePage extends PSFunction {

    public static final Chars NAME = new Chars("erasepage");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO : fire an event ? clear the page using a white transparent blackground ?
        
    }

}
