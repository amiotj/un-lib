package science.unlicense.impl.model2d.ps.vm.font;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.path.PathStep2D;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Paint string with current font.
 *
 * Stack in|out :
 * string | -
 * 
 * @author Johann Sorel
 */
public class Show extends PSFunction {

    public static final Chars NAME = new Chars("show");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Chars text = (Chars) pullValue(vm);
        final GraphicState state = vm.getCurrentGraphicState();
        final Sequence steps = state.currentGeometry.getSteps();
        final PathStep2D step = (PathStep2D) steps.get(steps.getSize()-1);
        vm.getPainter().fill(text, (float)step.values[0], (float)step.values[1]);
    }

}
