package science.unlicense.impl.model2d.ps.vm.dict;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Put global dictionary on the stack.
 * 
 * Stack in|out :
 * - | dict 
 * 
 * @author Johann Sorel
 */
public class GlobalDict extends PSFunction {

    public static final Chars NAME = new Chars("globaldict");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        vm.push(vm.getGlobalDictionary());
    }

}
