package science.unlicense.impl.model2d.ps.vm.painting;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSCall;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;
import science.unlicense.impl.model2d.ps.vm.path.NewPath;

/**
 * Stroke current path.
 * spec page 714
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class Stroke extends PSFunction {

    public static final Chars NAME = new Chars("stroke");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final GraphicState gs = vm.getCurrentGraphicState();
        vm.getPainter().stroke(gs.currentGeometry);
        
        //clear the path, strange it's the bahavior defined in the spec
        vm.push(new PSCall(NewPath.NAME));
    }

}
