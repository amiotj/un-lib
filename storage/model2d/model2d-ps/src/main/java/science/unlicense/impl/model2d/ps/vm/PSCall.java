package science.unlicense.impl.model2d.ps.vm;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PSCall extends CObject{

    public final Chars functionName;

    public PSCall(Chars functionName) {
        this.functionName = functionName;
    }

    public Chars toChars() {
        return new Chars("PSCall:").concat(functionName);
    }

}
