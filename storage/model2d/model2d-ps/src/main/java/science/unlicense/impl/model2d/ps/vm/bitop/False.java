package science.unlicense.impl.model2d.ps.vm.bitop;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Push a boolean false value on the vm stack.
 * 
 * Stack in|out :
 * - | false
 * 
 * @author Johann Sorel
 */
public class False extends PSFunction {

    public static final Chars NAME = new Chars("false");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        vm.push(Boolean.FALSE);
    }

}
