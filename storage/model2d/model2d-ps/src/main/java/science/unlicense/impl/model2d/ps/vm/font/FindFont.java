package science.unlicense.impl.model2d.ps.vm.font;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Set;
import science.unlicense.api.painter2d.Font;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSPointer;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Find font for given name.
 *
 * Stack in|out :
 * key | font
 * 
 * @author Johann Sorel
 */
public class FindFont extends PSFunction {

    public static final Chars NAME = new Chars("findfont");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final PSPointer pointer = (PSPointer) vm.pull();
        Chars family = pointer.name;
        final Set fonts = vm.getPainter().getFontStore().getFamilies();
        if(!fonts.contains(family)){
            //fallback on first font
            family = (Chars) fonts.createIterator().next();
        }
        final Font font = new Font(new Chars[]{family}, 1, Font.WEIGHT_NONE);
        vm.push(font);
    }

}
