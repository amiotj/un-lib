package science.unlicense.impl.model2d.ps.vm.presentation;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Configures the page to letter format.
 * spec page 824
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class Letter extends PSFunction {

    public static final Chars NAME = new Chars("letter");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO : event ? draw a back border of format size ?
        Painter2D painter = vm.getPainter();
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
        final Rectangle rect = new Rectangle(0, 0, 8.5*72, 11*72);
        painter.setPaint(new ColorPaint(Color.WHITE));
        painter.fill(rect);
        painter.setPaint(new ColorPaint(Color.BLACK));
        painter.stroke(rect);
        vm.getCurrentGraphicState().restore(painter);
    }

}
