package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Set stroke line width.
 *
 * Stack in|out :
 * width | -
 *
 * @author Johann Sorel
 */
public class SetLineWidth extends PSFunction {

    public static final Chars NAME = new Chars("setlinewidth");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        float width = ((Number) pullValue(vm)).floatValue();

        final GraphicState state = vm.getCurrentGraphicState();
        state.lineWidth = width;
        state.restoreBrush(vm.getPainter());

    }

}
