package science.unlicense.impl.model2d.ps.vm.path;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.path.PathStep2D;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Add a new point in current path.
 * Coordinates are relative to the last point.
 *
 * Stack in|out :
 * x y | -
 *
 * @author Johann Sorel
 */
public class RLineTo extends PSFunction {

    public static final Chars NAME = new Chars("rlineto");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Number y = (Number) pullValue(vm);
        final Number x = (Number) pullValue(vm);
        final GraphicState state = vm.getCurrentGraphicState();
        final Sequence steps = state.currentGeometry.getSteps();
        final PathStep2D lastStep = (PathStep2D) steps.get(steps.getSize()-1);
        state.currentGeometry.appendLineTo(lastStep.values[0] + x.doubleValue(), lastStep.values[1] + y.doubleValue());
    }

}
