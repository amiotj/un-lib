package science.unlicense.impl.model2d.ps.vm.matrix;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Scale ctm or given matrix.
 * 
 * Stack in|out :
 * sx sy | -
 * sx sy matrix | matrix
 * 
 * @author Johann Sorel
 */
public class Scale extends PSFunction {

    public static final Chars NAME = new Chars("scale");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object obj3 = pullValue(vm);
        Object obj2 = pullValue(vm);
        
        if(obj3 instanceof Matrix){
            Object obj1 = pullValue(vm);
            final double sy = ((Number)obj2).doubleValue();
            final double sx = ((Number)obj1).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 0, sx);
            t.set(1, 1, sy);
            science.unlicense.impl.math.Affine2 m = new Affine2((science.unlicense.impl.math.DefaultMatrix) obj3);
            t.localMultiply(m);
            vm.push(t);
            
        }else{
            final double sy = ((Number)obj3).doubleValue();
            final double sx = ((Number)obj2).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 0, sx);
            t.set(1, 1, sy);
            
            final GraphicState state = vm.getCurrentGraphicState();
            t.localMultiply(state.ctm);
            state.ctm = t;
            vm.getPainter().setTransform(state.ctm);
        }
    }

}
