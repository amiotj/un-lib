package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Set line cap.
 *
 * Stack in|out :
 * int | -
 *
 * @author Johann Sorel
 */
public class SetLineCap extends PSFunction {

    public static final Chars NAME = new Chars("setlinecap");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final int val = ((Number) pullValue(vm)).intValue();

        final GraphicState state = vm.getCurrentGraphicState();
        state.lineCap = (val==0) ? PlainBrush.LINECAP_BUTT : (val==1) ? PlainBrush.LINECAP_ROUND : PlainBrush.LINECAP_SQUARE;
        state.restoreBrush(vm.getPainter());
    }

}
