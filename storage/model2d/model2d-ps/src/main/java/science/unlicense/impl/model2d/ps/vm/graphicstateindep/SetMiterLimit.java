package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Set line join miter limit.
 *
 * Stack in|out :
 * num | -
 *
 * @author Johann Sorel
 */
public class SetMiterLimit extends PSFunction {

    public static final Chars NAME = new Chars("setmiterlimit");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        float limit = ((Number) pullValue(vm)).floatValue();

        final GraphicState state = vm.getCurrentGraphicState();
        state.miterLimit = limit;
        state.restoreBrush(vm.getPainter());
    }

}
