package science.unlicense.impl.model2d.ps.vm.path;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class CurrentPoint extends PSFunction {

    public static final Chars NAME = new Chars("currentpoint");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        throw new UnimplementedException("Not supported yet");
    }

}
