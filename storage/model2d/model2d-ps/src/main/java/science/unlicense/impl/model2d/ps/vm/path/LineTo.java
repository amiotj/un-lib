package science.unlicense.impl.model2d.ps.vm.path;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Add a new point in current path.
 *
 * Stack in|out :
 * x y | -
 *
 * @author Johann Sorel
 */
public class LineTo extends PSFunction {

    public static final Chars NAME = new Chars("lineto");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Number y = (Number) pullValue(vm);
        final Number x = (Number) pullValue(vm);
        vm.getCurrentGraphicState().currentGeometry.appendLineTo(x.doubleValue(), y.doubleValue());
    }

}
