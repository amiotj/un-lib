package science.unlicense.impl.model2d.ps.vm.dict;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Hasher;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Create a dictionary and push it on the operand stack.
 * 
 * Stack in|out :
 * int | dict
 *
 * @author Johann Sorel
 */
public class Dict extends PSFunction {

    public static final Chars NAME = new Chars("dict");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Number num = (Number) pullValue(vm);
        final Dictionary dict = new HashDictionary(Hasher.DEFAULT, num.intValue());
        vm.push(dict);
    }

}
