package science.unlicense.impl.model2d.ps.vm.path;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Start a new path.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class NewPath extends PSFunction {

    public static final Chars NAME = new Chars("newpath");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        vm.getCurrentGraphicState().currentGeometry = new Path();
    }

}
