package science.unlicense.impl.model2d.ps.vm;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PSPointer {

    public final Chars name;

    public PSPointer(Chars name) {
        this.name = name;
    }

}
