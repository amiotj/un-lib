package science.unlicense.impl.model2d.ps.vm.matrix;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Concatenate given matrix with ctm.
 *
 * Stack in|out :
 * matrix | -
 *
 * @author Johann Sorel
 */
public class Concat extends PSFunction {

    public static final Chars NAME = new Chars("concat");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO
//        final Matrix4x4 mat = (Matrix4x4) pullValue(vm);
//        final GraphicState state = vm.getCurrentGraphicState();
//        final Matrix4x4 res = (Matrix4x4) mat.multiply(state.ctm);
//        state.ctm = res;
//        vm.getPainter().setTransform(res.copy());
    }

}
