package science.unlicense.impl.model2d.ps.vm.misc;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Replace operators in a procedure.
 *
 * Stack in|out :
 * proc | proc
 *
 * @author Johann Sorel
 */
public class Bind extends PSFunction {

    public static final Chars NAME = new Chars("bind");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO this is some optimisation function
    }

}
