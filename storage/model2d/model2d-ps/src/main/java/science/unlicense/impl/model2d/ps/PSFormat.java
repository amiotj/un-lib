package science.unlicense.impl.model2d.ps;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * PostScript format.
 * 
 * Resource :
 * http://en.wikipedia.org/wiki/PostScript
 * http://www.adobe.com/products/postscript/pdfs/PLRM.pdf
 * http://partners.adobe.com/public/developer/en/ps/PLRM.pdf
 * http://www-cdf.fnal.gov/offline/PostScript/BLUEBOOK.PDF
 * 
 * @author Johann Sorel
 */
public class PSFormat extends DefaultFormat {

    public PSFormat() {
        super(new Chars("ps"),
              new Chars("PostScript"),
              new Chars("PostScript"),
              new Chars[]{
                  new Chars("application/postscript")
              },
              new Chars[]{
                  new Chars("ps"),
                  new Chars("eps")
              },
              new byte[][]{PSConstants.SIGNATURE});
    }
        
}
