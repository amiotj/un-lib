package science.unlicense.impl.model2d.ps.vm.type;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class ExecuteOnly extends PSFunction {

    public static final Chars NAME = new Chars("executeonly");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        throw new UnimplementedException("Not supported yet");
    }

}
