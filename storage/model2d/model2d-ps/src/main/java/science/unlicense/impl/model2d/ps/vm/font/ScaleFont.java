package science.unlicense.impl.model2d.ps.vm.font;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.painter2d.Font;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Scale font.
 *
 * Stack in|out :
 * font scale | font
 * 
 * @author Johann Sorel
 */
public class ScaleFont extends PSFunction {

    public static final Chars NAME = new Chars("scalefont");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final float scale = ((Number)pullValue(vm)).floatValue();
        final Font font = (Font) pullValue(vm);
        vm.push(new Font(font.getFamilies(), font.getSize()*scale, font.getWeight()));
    }

}
