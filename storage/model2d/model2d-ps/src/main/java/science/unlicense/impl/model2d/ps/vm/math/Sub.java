package science.unlicense.impl.model2d.ps.vm.math;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 *
 * @author Johann Sorel
 */
public class Sub extends PSFunction {

    public static final Chars NAME = new Chars("sub");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Number v2 = (Number) pullValue(vm);
        final Number v1 = (Number) pullValue(vm);
        vm.push(v1.doubleValue() - v2.doubleValue());
    }

}
