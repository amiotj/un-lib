package science.unlicense.impl.model2d.ps.vm.dict;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArrayStack;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Find the dictionary that contains a given key.
 *
 * Stack in|out :
 * key | dict true or false
 *
 * @author Johann Sorel
 */
public class Where extends PSFunction {

    public static final Chars NAME = new Chars("where");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final Object key = vm.pull();

        final ArrayStack dictStack = vm.getDictStack();
        Dictionary dico = null;
        for(int i=dictStack.getSize()-1;dico==null && i>=0;i--){
            Object obj = ((Dictionary)dictStack.get(i)).getValue(key);
            if(obj!=null){
                dico = (Dictionary)dictStack.get(i);
            }
        }

        if(dico==null){
            vm.push(Boolean.FALSE);
        }else{
            vm.push(dico);
            vm.push(Boolean.TRUE);
        }

    }

}
