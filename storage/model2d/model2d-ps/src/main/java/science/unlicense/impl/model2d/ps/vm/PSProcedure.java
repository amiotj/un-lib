package science.unlicense.impl.model2d.ps.vm;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.impl.vm.VMException;

/**
 *
 * @author Johann Sorel
 */
public class PSProcedure extends ArraySequence {

    public void execute(PSVirtualMachine vm) throws VMException{
        for(int i=0,n=getSize();i<n;i++){
            vm.push(get(i));
        }
    }
    
}
