package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * restore a copy of the current graphic state from the graphic stack.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class GRestore extends PSFunction {

    public static final Chars NAME = new Chars("grestore");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        
        final Painter2D painter = vm.getPainter();
        //restore last saved graphic state
        final GraphicState state = (GraphicState) vm.getGraphicStack().lookEnd();
        //no effec if empty
        if(state!=null){
            if(!state.isSave){
                //no a save operation, we pop the state
                vm.getGraphicStack().pickEnd();
            }
            state.restore(painter);
        }

    }

}
