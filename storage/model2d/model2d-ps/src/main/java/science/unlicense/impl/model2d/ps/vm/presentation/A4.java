package science.unlicense.impl.model2d.ps.vm.presentation;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Configures the page to A4 format.
 * spec page 824
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class A4 extends PSFunction {

    public static final Chars NAME = new Chars("a4");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO : event ? draw a back border of format size ?
        
    }

}
