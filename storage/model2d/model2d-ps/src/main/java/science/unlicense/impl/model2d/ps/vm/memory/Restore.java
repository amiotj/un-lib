package science.unlicense.impl.model2d.ps.vm.memory;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Restore last saved state.
 * 
 * @author Johann Sorel
 */
public class Restore extends PSFunction {

    public static final Chars NAME = new Chars("restore");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        //TODO
    }

}
