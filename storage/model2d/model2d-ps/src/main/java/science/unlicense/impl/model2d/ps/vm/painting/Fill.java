package science.unlicense.impl.model2d.ps.vm.painting;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Fill current path.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class Fill extends PSFunction {

    public static final Chars NAME = new Chars("fill");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final GraphicState gs = vm.getCurrentGraphicState();
        vm.getPainter().fill(gs.currentGeometry);
    }

}
