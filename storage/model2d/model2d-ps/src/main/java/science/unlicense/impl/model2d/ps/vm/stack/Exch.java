package science.unlicense.impl.model2d.ps.vm.stack;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Exchange the last two element in the stack.
 *
 * Stack in|out :
 * o1 o2 | o2 o1
 *
 * @author Johann Sorel
 */
public class Exch extends PSFunction {

    public static final Chars NAME = new Chars("exch");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object o2 = vm.pull();
        Object o1 = vm.pull();
        vm.push(o2);
        vm.push(o1);
    }

}
