package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.math.Maths;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Set color to a gray variation.
 * 0 = black
 * 1 = white
 *
 * Stack in|out :
 * num | -
 *
 * @author Johann Sorel
 */
public class SetGray extends PSFunction {

    public static final Chars NAME = new Chars("setgray");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        float g = ((Number) pullValue(vm)).floatValue();
        g = Maths.clamp(g, 0f, 1f);

        final GraphicState state = vm.getCurrentGraphicState();
        state.paint = new ColorPaint(new Color(g, g, g));
        vm.getPainter().setPaint(state.paint);
    }

}
