package science.unlicense.impl.model2d.ps.vm.dict;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Push dictionary on operand stack on dictionay stack.
 *
 * Stack in|out :
 * dict | -
 *
 * @author Johann Sorel
 */
public class Begin extends PSFunction {

    public static final Chars NAME = new Chars("begin");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final Dictionary dict = (Dictionary) pullValue(vm);
        vm.getDictStack().add(dict);
    }

}
