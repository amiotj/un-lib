package science.unlicense.impl.model2d.ps.vm.stack;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Duplicate the last element in the stack.
 *
 * Stack in|out :
 * o1 | o1 o1
 *
 * @author Johann Sorel
 */
public class Dup extends PSFunction {

    public static final Chars NAME = new Chars("dup");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object o1 = vm.pull();
        vm.push(o1);
        vm.push(o1);
    }

}
