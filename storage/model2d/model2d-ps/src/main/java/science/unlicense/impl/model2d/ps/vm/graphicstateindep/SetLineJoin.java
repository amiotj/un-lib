package science.unlicense.impl.model2d.ps.vm.graphicstateindep;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Set line join.
 *
 * Stack in|out :
 * int | -
 *
 * @author Johann Sorel
 */
public class SetLineJoin extends PSFunction {

    public static final Chars NAME = new Chars("setlinejoin");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) {
        final int val = ((Number) pullValue(vm)).intValue();

        //TODO support line join
        final GraphicState state = vm.getCurrentGraphicState();
        state.lineJoin = (val==0) ? PlainBrush.LINEJOIN_MITER : (val==1) ? PlainBrush.LINEJOIN_ROUND : PlainBrush.LINEJOIN_BEVEL;
        state.restoreBrush(vm.getPainter());
    }

}
