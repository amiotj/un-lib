package science.unlicense.impl.model2d.ps.vm.string;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Create a new string of given length.
 * 
 * Stack in|out :
 * size | string
 * 
 * @author Johann Sorel
 */
public class String extends PSFunction {

    public static final Chars NAME = new Chars("string");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final int size = ((Number)pullValue(vm)).intValue();
        
        vm.push(new Chars(new byte[size],CharEncodings.UTF_8));
        
    }

}
