package science.unlicense.impl.model2d.ps.vm.dict;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Remove dictionary from dictionary stack.
 *
 * Stack in|out :
 * - | -
 *
 * @author Johann Sorel
 */
public class End extends PSFunction {

    public static final Chars NAME = new Chars("end");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object dict = vm.getDictStack().pickEnd();
        if(dict==vm.getUserDictionary()){
            throw new VMException("dictstackunderflow : Unallowed to pull user dict.");
        }
    }

}
