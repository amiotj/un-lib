package science.unlicense.impl.model2d.eps;

import science.unlicense.impl.model2d.ps.PSConstants;
import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Encapsulated PostScript format.
 * 
 * Resource :
 * https://en.wikipedia.org/wiki/Encapsulated_PostScript
 * 
 * @author Johann Sorel
 */
public class EPSFormat extends DefaultFormat {

    public EPSFormat() {
        super(new Chars("eps"),
              new Chars("Encapsulated PostScript"),
              new Chars("Encapsulated PostScript"),
              new Chars[]{
                  new Chars("application/postscript"),
                  new Chars("application/eps"),
                  new Chars("application/x-eps"),
                  new Chars("image/eps"),
                  new Chars("image/x-eps")
              },
              new Chars[]{
                  new Chars("ps"),
                  new Chars("eps")
              },
              new byte[][]{
                  EPSConstants.WIN_SIGNATURE,
                  PSConstants.SIGNATURE
              });
    }
        
}
