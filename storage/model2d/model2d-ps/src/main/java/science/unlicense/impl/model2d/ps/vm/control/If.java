package science.unlicense.impl.model2d.ps.vm.control;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSProcedure;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Execute one  procedure if boolean value is true.
 *
 * Stack in|out :
 * bool proc | -
 *
 * @author Johann Sorel
 */
public class If extends PSFunction {

    public static final Chars NAME = new Chars("if");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        final PSProcedure proc1 = (PSProcedure) pullValue(vm);
        final Boolean bool = (Boolean) pullValue(vm);
        if(bool){
            proc1.execute(vm);
        }
    }

}
