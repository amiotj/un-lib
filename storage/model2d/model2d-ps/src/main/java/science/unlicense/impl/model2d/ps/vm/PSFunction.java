package science.unlicense.impl.model2d.ps.vm;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.vm.VMException;

/**
 *
 * @author Johann Sorel
 */
public abstract class PSFunction {

    public abstract Chars getName();

    public abstract void execute(PSVirtualMachine state) throws VMException;

    protected static Object pullValue(PSVirtualMachine vm) {
        Object candidate = vm.pull();
        if (candidate instanceof PSPointer) {
            return vm.resolve((PSPointer) candidate);
        }
        return candidate;
    }

}
