package science.unlicense.impl.model2d.ps.vm.matrix;

import science.unlicense.impl.model2d.ps.vm.PSFunction;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.GraphicState;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;

/**
 * Translate ctm or given matrix.
 * 
 * Stack in|out :
 * tx ty | -
 * tx ty matrix | matrix
 * 
 * @author Johann Sorel
 */
public class Translate extends PSFunction {

    public static final Chars NAME = new Chars("translate");

    public Chars getName() {
        return NAME;
    }

    public void execute(PSVirtualMachine vm) throws VMException {
        Object obj3 = pullValue(vm);
        Object obj2 = pullValue(vm);
        
        if(obj3 instanceof Matrix){
            Object obj1 = pullValue(vm);
            final double ty = ((Number)obj2).doubleValue();
            final double tx = ((Number)obj1).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 3, tx);
            t.set(1, 3, ty);
            Affine2 m = (Affine2) obj3;
            t.localMultiply(m);
            vm.push(t);
            
        }else{
            final double ty = ((Number)obj3).doubleValue();
            final double tx = ((Number)obj2).doubleValue();
            final Affine2 t = new Affine2();
            t.set(0, 3, tx);
            t.set(1, 3, ty);
            
            final GraphicState state = vm.getCurrentGraphicState();
            t.localMultiply(state.ctm);
            state.ctm = t;
            vm.getPainter().setTransform(state.ctm);
        }
        
    }

}
