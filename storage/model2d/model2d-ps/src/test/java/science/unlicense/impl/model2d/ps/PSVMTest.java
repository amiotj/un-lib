package science.unlicense.impl.model2d.ps;

import science.unlicense.impl.model2d.ps.PSReader;
import junit.framework.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.impl.vm.VMException;
import science.unlicense.impl.model2d.ps.vm.PSVirtualMachine;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class PSVMTest {
    
    private static final double DELTA = 0.00000001;
    
    @Test
    public void mathTest() throws IOException, VMException{
        Object result = execute("mod:/un/storage/model2d/ps/math.ps");
        Assert.assertEquals(9.0, (Double)result, DELTA);
        
    }
    
    @Test
    public void functionTest() throws IOException, VMException{
        Object result = execute("mod:/un/storage/model2d/ps/function.ps");
        Assert.assertEquals(16, (Double)result, DELTA);
        
    }
    
    private Object execute(String filePath) throws IOException, VMException{
        final Path path = Paths.resolve(new Chars(filePath));
        final PSReader reader = new PSReader();
        reader.setInput(path);

        final PSVirtualMachine vm = new PSVirtualMachine();

        for(Object o = reader.next();o!=null;o = reader.next()){
            vm.push(o);
        }

        return vm.pull();
    }
    
}
