
package science.unlicense.impl.model2d.ps;

import science.unlicense.impl.model2d.ps.PSReader;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class EPSReadTest {

    @Test
    public void readTest() throws IOException {

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model2d/eps/square.eps"));
        final PSReader reader = new PSReader();
        reader.setInput(path);

        for(Object o = reader.next();o!=null;o = reader.next()){
        }
    }

}
