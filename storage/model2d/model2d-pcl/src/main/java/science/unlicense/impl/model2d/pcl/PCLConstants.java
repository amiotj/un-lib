

package science.unlicense.impl.model2d.pcl;

/**
 * PCL constants.
 * 
 * @author Johann Sorel
 */
public final class PCLConstants {
        
    private PCLConstants(){}
    
}
