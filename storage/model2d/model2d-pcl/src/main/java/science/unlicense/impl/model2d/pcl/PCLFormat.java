

package science.unlicense.impl.model2d.pcl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Printer Command Language.
 * 
 * Resource : 
 * http://en.wikipedia.org/wiki/Printer_Command_Language
 * 
 * @author Johann Sorel
 */
public class PCLFormat extends DefaultFormat {

    public PCLFormat() {
        super(new Chars("pcl"),
              new Chars("PCL"),
              new Chars("Printer Command Language"),
              new Chars[]{
              },
              new Chars[]{
              },
              new byte[][]{});
    }
        
}
