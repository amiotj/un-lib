

package science.unlicense.impl.media.webm;

import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mkv.MKVMediaStore;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class WebmReaderTest {
    
    @Test
    public void readTest() throws IOException{
        MKVMediaStore store = new MKVMediaStore(Paths.resolve(new Chars("mod:/un/storage/media/webm/sample.webm")));
        store.createReader(null);
    }
    
}
