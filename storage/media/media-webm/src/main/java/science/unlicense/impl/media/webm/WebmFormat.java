

package science.unlicense.impl.media.webm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaCapabilities;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class WebmFormat extends AbstractMediaFormat{

    public static final WebmFormat INSTANCE = new WebmFormat();
    
    public WebmFormat() {
        super(new Chars("webm"),
              new Chars("Webm"),
              new Chars("Webm"),
              new Chars[]{
                  new Chars("video/webm"),
                  new Chars("audio/webm")
              },
              new Chars[]{
                new Chars("webm")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public MediaStore createStore(Object input) throws IOException {
        throw new RuntimeException("not yet");
    }
    
}
