

package science.unlicense.impl.media.mp1.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Mpeg-1/2 stream block.
 * 
 * @author Johann Sorel
 */
public class Block extends CObject {
        
    public void read(DataInputStream ds) throws IOException {
        throw new IOException("Not supported yet.");
    }
    
    public void write(DataOutputStream ds) throws IOException{
        throw new IOException("Not supported yet.");
    }
    
}
