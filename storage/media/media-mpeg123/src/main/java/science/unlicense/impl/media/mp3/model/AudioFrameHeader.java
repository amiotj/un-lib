

package science.unlicense.impl.media.mp3.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp3.MP3Constants;

/**
 *
 * Some informations are from :
 * https://github.com/wilkie/djehuty
 * Thanks to Dave Wilkinson
 * 
 * @author Johann Sorel
 */
public class AudioFrameHeader {
    
    public int version;
    public int layer;
    public boolean crcFlag;
    public int bitRate;
    public int frequency;
    public boolean framePadded;
    public int privBit;
    public int mode;
    public int modeExt;
    public boolean copyright;
    public boolean original;
    public int emphasis;
        
    //decoding calculated informations
    public int frameNbSample;
    public int frameLength;
    
    public int samplesPerSecond;
    public int compressionType;
    public int numChannels;
    public int averageBytesPerSecond;
    public int blockAlign;
    public int bitsPerSample;
    
    public void updateDecodingInfo() throws IOException{

        //frame informations
        if(layer == MP3Constants.LAYER_I){
            frameNbSample = 384;
            throw new IOException("how to calculate layer 1 length");
        }else{
            frameNbSample = 1152;
            // Calculate the length of the Audio Data
            frameLength = (int)(144 * MP3Constants.MPEG1_LAYER_III_BITRATES[bitRate] / MP3Constants.MEPG1_SAMPLING_FREQUENCIES[frequency]);
            if (framePadded) {
                frameLength++;
            }
        }
        
        // subtract the size of the header
        frameLength -= 4;

        samplesPerSecond = (int)(MP3Constants.MEPG1_SAMPLING_FREQUENCIES[0]*1000.0);
        compressionType = 1;

        if(MP3Constants.MODE_SINGLE_CHANNEL == mode){
            numChannels = 1;
        }else if(MP3Constants.MODE_STEREO == mode 
              || MP3Constants.MODE_DUAL_CHANNEL == mode
              || MP3Constants.MODE_JOINT_STEREO == mode){
            numChannels = 2;
        }else{
            throw new IOException("Unknow mode type");
        }
        
        averageBytesPerSecond = samplesPerSecond * 2 * numChannels;
        blockAlign = 2 * numChannels;
        bitsPerSample = 16;
        
    }
    
    public void read(DataInputStream ds) throws IOException{
        version         = ds.readBits(1);
        layer           = ds.readBits(2);
        crcFlag         = ds.readBits(1) ==0 ;
        bitRate         = ds.readBits(4);
        frequency       = ds.readBits(2);
        framePadded     = ds.readBits(1) != 0;
        privBit         = ds.readBits(1);
        mode            = ds.readBits(2);
        modeExt         = ds.readBits(2);
        copyright       = ds.readBits(1) != 0;
        original        = ds.readBits(1) != 0;
        emphasis        = ds.readBits(2);
        updateDecodingInfo();
    }
        
}
