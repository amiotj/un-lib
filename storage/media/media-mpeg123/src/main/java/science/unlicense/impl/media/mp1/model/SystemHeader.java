
package science.unlicense.impl.media.mp1.model;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SystemHeader extends Block {

    /**
     * Header size without 4 id bytes and 2bytes length.
     */
    public int headerSize;
    public int maxRate;
    public int maxAudio;
    public int maxVideo;
    public boolean fixBitRate;
    public boolean csps;
    public boolean systemAudioLock;
    public boolean systemVideoLock;
    
    public Sequence streamSpecs = new ArraySequence();
    
    public void read(DataInputStream ds) throws IOException {
        headerSize = ds.readUShort();
        if(ds.readBits(1)!=1) throw new IOException("Invalid system header");
        maxRate = ds.readBits(22);
        if(ds.readBits(1)!=1) throw new IOException("Invalid system header");
        maxAudio = ds.readBits(6);
        fixBitRate = ds.readBits(1)==1;
        csps = ds.readBits(1)==1;
        systemAudioLock = ds.readBits(1)==1;
        systemVideoLock = ds.readBits(1)==1;
        if(ds.readBits(1)!=1) throw new IOException("Invalid system header");
        maxVideo = ds.readBits(5);
        //reserved unused byte
        ds.readByte();
                
    }
    
    public static class StreamSpec{
        public int streamId;
        public boolean bufferBoundScale;
        public int stdBufferSize;
    }
    
}