

package science.unlicense.impl.media.mp3.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AudioDecoder {
    
    public abstract byte[] decode(DataInputStream ds, AudioFrameHeader header) throws IOException;
    
}
