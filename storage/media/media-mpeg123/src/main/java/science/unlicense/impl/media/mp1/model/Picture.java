
package science.unlicense.impl.media.mp1.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp1.MP1Constants;

/**
 *
 * @author Johann Sorel
 */
public class Picture extends Block {

    public int temporalReference;
    public int codingType;
    public int ybyDelay;
    
    //only for P or B type pictures
    public boolean fullForwardVector;
    public int forwardCode;
    public boolean fullBackwardVector;
    public int backwardCode;
    
    public byte[] extraInformations;
    
    public final Sequence slices = new ArraySequence();
    
    @Override
    public void read(DataInputStream ds) throws IOException {
        temporalReference = ds.readBits(10);
        codingType = ds.readBits(3);
        ybyDelay = ds.readBits(16);
        
        if(codingType == MP1Constants.PICTURE_TYPE_P || codingType == MP1Constants.PICTURE_TYPE_B){
            fullForwardVector = ds.readBits(1)==1;
            forwardCode = ds.readBits(3);
        }
        if(codingType == MP1Constants.PICTURE_TYPE_B){
            fullBackwardVector = ds.readBits(1)==1;
            backwardCode = ds.readBits(3);
        }
        
        extraInformations = new byte[0];
        if(ds.readBits(1)==1){
            Arrays.resize(extraInformations, extraInformations.length+1);
            extraInformations[extraInformations.length-1] = ds.readByte();
        }
        
        //skip remaining bits
        ds.skipToByteEnd();
        
    }
 
    
    public static class Slice extends Block {

        public Picture picture;
        
        public int quantizerScale;
        public byte[] extraInformations;
        public final Sequence macroblocks = new ArraySequence();

        public void read(DataInputStream ds) throws IOException {
            quantizerScale = ds.readBits(5);

            extraInformations = new byte[0];
            if(ds.readBits(1)==1){
                Arrays.resize(extraInformations, extraInformations.length+1);
                extraInformations[extraInformations.length-1] = ds.readByte();
            }
            
            //read macroblocks
            
            //skip stuffing
            int mark = ds.readBits(11);
            while(mark==0xF){
                mark = ds.readBits(11);
            }
            
            //TODO don't have found enough info on how to parse this yet.
            //need to find a proper explaining document
            
        }

        public class MacroBlock{

            public byte[][] y;
            public byte[] cb;
            public byte[] cr;

        }

    }
    
    
}