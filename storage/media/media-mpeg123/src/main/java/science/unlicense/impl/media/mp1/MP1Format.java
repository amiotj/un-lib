
package science.unlicense.impl.media.mp1;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * Mpeg-1/ Mepg-2 format.
 *
 * Specification :
 * https://www.itu.int/rec/dologin_pub.asp?lang=e&id=T-REC-H.261-199303-I!!PDF-E&type=items
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MPEG-1
 * http://en.wikipedia.org/wiki/MPEG_program_stream
 * http://msdn.microsoft.com/en-us/library/windows/desktop/dd390712(v=vs.85).aspx
 * http://dvd.sourceforge.net/dvdinfo/mpeghdrs.html
 * http://stnsoft.com/DVD/mpeg-1_pes-hdr.html
 * http://www.andrewduncan.ws/MPEG/MPEG-1_Picts.html
 * 
 * @author Johann Sorel
 */
public class MP1Format extends AbstractMediaFormat{

    public static final MP1Format INSTANCE = new MP1Format();
    
    public MP1Format() {
        super(new Chars("mp1"),
              new Chars("MPEG"),
              new Chars("MPEG-1"),
              new Chars[]{
                  new Chars("audio/mpeg"),
                  new Chars("video/mpeg")
              },
              new Chars[]{
                  new Chars("mpg"),
                  new Chars("mpeg"),
                  new Chars("mp1"),
                  new Chars("mp2"),
                  new Chars("mp3"),
                  new Chars("m1v"),
                  new Chars("m1a"),
                  new Chars("m2a"),
                  new Chars("mpa"),
                  new Chars("mpv")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public MediaStore createStore(Object input) throws IOException {
        return new MP1Store((Path)input);
    }

}