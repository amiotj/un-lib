

package science.unlicense.impl.media.mp3.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SideInformation {
 
    public static class GranuleInfo{
        public int part23Length;
        public int bigValues;
        public int globalGain;
        public int scaleFactorCompress;
        public int windowSwitchFlag;
        public int blockType;
        public int mixedBlockFlag;
        public int tableSelect;
        public int subblockGain;
        public int region0Count;
        public int region1Count;
        public int preFlag;
        public int scaleFactorScale;
        public int count1TableSelect;
    }
    
    /**
     * Negative offset in bytes from the sync mark of the frame.
     * Dat can be placed before the frame start.
     * If value is zero, the data is directly after the side informations.
     */
    public int mainDataBegin;
    /** unused bits, could be used for anything */
    public int reserved;
    /** */
    public int[] scaleFactors;
    public GranuleInfo[] granules = new GranuleInfo[2];
    
    
    public void read(DataInputStream ds , boolean singleChanel) throws IOException{
        mainDataBegin   = ds.readBits(9);
        reserved        = ds.readBits( singleChanel ? 5 : 3);
        granules = new GranuleInfo[2];
        for(int i=0;i<(singleChanel?1:2);i++){
            granules[i] = new GranuleInfo();
            granules[i].part23Length        = ds.readBits(12);
            granules[i].bigValues           = ds.readBits(9);
            granules[i].globalGain          = ds.readBits(8);
            granules[i].scaleFactorCompress = ds.readBits(4);
            granules[i].windowSwitchFlag    = ds.readBits(1);
            granules[i].blockType           = ds.readBits(2);
            granules[i].mixedBlockFlag      = ds.readBits(1);
            granules[i].tableSelect         = ds.readBits(12); // TODO dynamic size
            granules[i].subblockGain        = ds.readBits(9);
            granules[i].region0Count        = ds.readBits(12); // TODO dynamic size
            granules[i].region1Count        = ds.readBits(12); // TODO dynamic size
            granules[i].preFlag             = ds.readBits(1);
            granules[i].scaleFactorScale    = ds.readBits(1);
            granules[i].count1TableSelect   = ds.readBits(1);
        }
    }
    
}
