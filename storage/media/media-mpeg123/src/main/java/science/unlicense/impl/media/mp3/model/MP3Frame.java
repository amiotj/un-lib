
package science.unlicense.impl.media.mp3.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp3.MP3Constants;

/**
 *
 * @author Johann Sorel (adapted to java/unlicense-lib and multiple changes)
 */
public class MP3Frame {
    
    public AudioFrameHeader header;
    public int crc;
    public SideInformation sideInfo;
    public byte[] audioData;
    
    public void read(DataInputStream ds, boolean skipSync) throws IOException{
        if(!skipSync){
            //read the sync bits
            byte b = ds.readByte();
            if(b!=(byte)0xFF){
                throw new IOException("Was expecting SYNC bits. source is not an MP3");
            }
            ds.readBits(4);
        }
        
        //read header
        header = new AudioFrameHeader();
        header.read(ds);
        
        //read crc if any
        if (header.crcFlag) {
            crc = ds.readUShort();
        }
         
        //read side infos
        sideInfo = new SideInformation();
        sideInfo.read(ds, header.mode == MP3Constants.MODE_SINGLE_CHANNEL);
        
        
        //read audio data
        final AudioDecoder decoder;
        if(header.layer==MP3Constants.LAYER_I){
            decoder = new AudioLayerIDecoder();
        }else if(header.layer==MP3Constants.LAYER_II){
            decoder = new AudioLayerIIDecoder();
        }else if(header.layer==MP3Constants.LAYER_III){
            decoder = new AudioLayerIIIDecoder();
        }else{
            throw new IOException("Unknowned audio layer "+ header.layer);
        }
        
        audioData = decoder.decode(ds, header);
        
    }
        
}
