

package science.unlicense.impl.media.mp3.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ID3Tag {
    
    public static final byte[] SIGNATURE = new byte[]{'I','D','3'};
    
    // Header
    /** 3bytes */
    public byte[] signature;
    /** 1 byte */
    public int versionMajor;
    /** 1 byte */
    public int versionMinor;
    /** 1 byte */
    public int flags;
    /** 4 bytes */
    public int length;
    
    //Tag content
    public Chars text;
    
    public void read(DataInputStream ds, boolean noSignature) throws IOException{
        if(!noSignature){
            signature = ds.readFully(new byte[3]);
            if(!Arrays.equals(signature,SIGNATURE)){
                throw new IOException("Invalid ID3 header signature.");
            }
        }
        
        versionMajor = ds.readUByte();
        versionMinor = ds.readUByte();
        flags = ds.readByte();
        length = ds.readInt();
        
        text = new Chars(ds.readFully(new byte[length]));
    }
    
}
