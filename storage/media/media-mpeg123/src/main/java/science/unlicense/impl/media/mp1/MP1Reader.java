

package science.unlicense.impl.media.mp1;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import static science.unlicense.impl.media.mp1.MP1Constants.*;
import science.unlicense.impl.media.mp1.model.Block;
import science.unlicense.impl.media.mp1.model.SystemHeader;

/**
 *
 * @author Johann Sorel
 */
public class MP1Reader implements MediaReadStream{
    
    private final Path path;

    public MP1Reader(Path path) {
        this.path = path;
    }
    
    public void read() throws IOException{
        
        final ByteInputStream is = path.createInputStream();
        final DataInputStream ds = new DataInputStream(is, NumberEncoding.BIG_ENDIAN);
        
        
        final byte[] blockId = new byte[4];
        int offset=0;
        for(;;){
            ds.readFully(blockId, offset, 4-offset);
            offset=0;
            
            if(!Arrays.equals(blockId,0,3,SIGNATURE,0)){
                throw new IOException("Invalid block id");
            }
            
            if(blockId[3]==STREAM_PROG_END){
                //end marker
                break;
            }else{
                final Block block = createBlock(blockId[3]);
                block.read(ds);
                if(block instanceof SystemHeader){
                    //check for stream specs elements
                    while(ds.readBits(1)==1){
                        final SystemHeader.StreamSpec spec = new SystemHeader.StreamSpec();
                        spec.streamId = ds.readBits(7) | 0x80;
                        if(ds.readBits(2)!=3) throw new IOException("Invalid stream spec");
                        spec.bufferBoundScale = ds.readBits(1)==1;
                        spec.stdBufferSize = ds.readBits(13);
                        ((SystemHeader)block).streamSpecs.add(spec);
                    }
                    offset=1;
                    ds.readBits(7);
                }
                
                System.out.println(block);
            }
        }
        
        
    }

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaPacket next() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
}