
package science.unlicense.impl.media.mp3;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * MP3 format.
 * 
 * Useful docs :
 * http://en.wikipedia.org/wiki/MP3
 * http://www.datavoyage.com/mpgscript/mpeghdr.htm
 * 
 * @author Johann Sorel
 */
public class MP3Format extends AbstractMediaFormat{

    public static final MP3Format INSTANCE = new MP3Format();
    
    public MP3Format() {
        super(new Chars("mp3"),
              new Chars("MP3"),
              new Chars("MPEG-2 Audio Layer III"),
              new Chars[]{
                  new Chars("audio/mpeg"),
                  new Chars("audio/MPA"),
                  new Chars("audio/mpa-robust")
              },
              new Chars[]{
                  new Chars("mp3")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public MediaStore createStore(Object input) throws IOException {
        return new MP3Store((Path)input);
    }

}