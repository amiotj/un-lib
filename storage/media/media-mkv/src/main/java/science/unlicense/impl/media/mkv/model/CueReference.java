
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 * CueReference := db container [ card:*; ] {
 *  CueRefTime := 96 uint;
 *  CueRefCluster := 97 uint;
 *  CueRefNumber := 535f uint [ range:1..; def:1; ]
 *  CueRefCodecState := eb uint [ def:0; ]
 * }
 *
 * @author Johann Sorel
 */
public class CueReference extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x96,  TYPE_UINT, new Chars("CueRefTime")),
        new PropertyType(0x97,  TYPE_UINT, new Chars("CueRefCluster")),
        new PropertyType(0x535f,TYPE_UINT, new Chars("CueRefNumber")),
        new PropertyType(0xeb,  TYPE_UINT, new Chars("CueRefCodecState"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getCueRefTime() {
        return getPropertyUInt(0x96, null);
    }

    public Integer getCueRefCluster() {
        return getPropertyUInt(0x97, null);
    }

    public Integer getCueRefNumber() {
        return getPropertyUInt(0x535f, 1);
    }

    public Integer getCueRefCodecState() {
        return getPropertyUInt(0xeb, 0);
    }
}
