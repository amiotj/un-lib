
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * Targets := 63c0 container {
 *    TrackUID := 63c5 uint [ card:*; def:0; ]
 *    ChapterUID := 63c4 uint [ card:*; def:0; ]
 *    AttachmentUID := 63c6 uint [ card:*; def:0; ]
 *  }
 *
 * @author Johann Sorel
 */
public class Targets extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x63c5,  TYPE_UINT,    new Chars("TrackUID")),
        new PropertyType(0x63c4,  TYPE_UINT,    new Chars("ChapterUID")),
        new PropertyType(0x63c6,  TYPE_UINT,    new Chars("AttachmentUID"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getTrackUID() {
        return getPropertyUInt(0x63c5, 0);
    }

    public Integer getChapterUID() {
        return getPropertyUInt(0x63c4, 0);
    }

    public Integer getAttachmentUID() {
        return getPropertyUInt(0x63c6, 0);
    }
}
