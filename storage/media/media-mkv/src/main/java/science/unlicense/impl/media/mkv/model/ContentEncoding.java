
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * ContentEncoding := 6240 container [ card:*; ] {
 *  ContentEncodingOrder := 5031 uint [ def:0; ]
 *  ContentEncodingScope := 5032 uint [ range:1..; def:1; ]
 *  ContentEncodingType := 5033 uint;
 *  ContentCompression := 5034 container ...
 *  ContentEncryption := 5035 container ...
 * }
 * 
 * @author Johann Sorel
 */
public class ContentEncoding extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x5031, TYPE_UINT, new Chars("ContentEncodingOrder")),
        new PropertyType(0x5032, TYPE_UINT, new Chars("ContentEncodingScope")),
        new PropertyType(0x5033, TYPE_UINT, new Chars("ContentEncodingType")),
        new PropertyType(0x5034, TYPE_SUB,  new Chars("ContentCompression")),
        new PropertyType(0x5035, TYPE_SUB,  new Chars("ContentEncryption"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getContentEncodingOrder() {
        return getPropertyUInt(0x5031, 0);
    }

    public Integer getContentEncodingScope() {
        return getPropertyUInt(0x5032, 1);
    }

    public Integer getContentEncodingType() {
        return getPropertyUInt(0x5033, null);
    }

    public ContentCompression getContentCompression() {
        return (ContentCompression) getSub(0x5034);
    }

    public ContentEncryption getContentEncryption() {
        return (ContentEncryption) getSub(0x5035);
    }
}
