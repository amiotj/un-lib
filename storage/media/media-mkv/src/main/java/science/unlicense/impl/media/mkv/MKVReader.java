
package science.unlicense.impl.media.mkv;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.MediaPacket;

/**
 *
 * @author Johann Sorel
 */
public class MKVReader implements MediaReadStream {

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaPacket next() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }


}
