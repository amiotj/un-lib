
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * Info := 1549a966 container [ card:*; ] {
 *    SegmentUID := 73a4 binary;
 *    SegmentFilename := 7384 string;
 *    PrevUID := 3cb923 binary;
 *    PrevFilename := 3c83ab string;
 *    NextUID := 3eb923 binary;
 *    NextFilename := 3e83bb string;
 *    TimecodeScale := 2ad7b1 uint [ def:1000000; ]
 *    Duration := 4489 float [ range:>0.0; ]
 *    DateUTC := 4461 date;
 *    Title := 7ba9 string;
 *    MuxingApp := 4d80 string;
 *    WritingApp := 5741 string;
 *  }
 * 
 * @author Johann Sorel
 */
public class Info extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x73a4,   TYPE_BINARY, new Chars("SegmentUID")),
        new PropertyType(0x7384,   TYPE_STRING, new Chars("SegmentFilename")),
        new PropertyType(0x3cb923, TYPE_BINARY, new Chars("PrevUID")),
        new PropertyType(0x3c83ab, TYPE_STRING, new Chars("PrevFilename")),
        new PropertyType(0x3eb923, TYPE_BINARY, new Chars("NextUID")),
        new PropertyType(0x3e83bb, TYPE_STRING, new Chars("NextFilename")),
        new PropertyType(0x2ad7b1, TYPE_UINT,   new Chars("TimecodeScale")),
        new PropertyType(0x4489,   TYPE_FLOAT,  new Chars("Duration")),
        new PropertyType(0x4461,   TYPE_DATE,   new Chars("DateUTC")),
        new PropertyType(0x7ba9,   TYPE_STRING, new Chars("Title")),
        new PropertyType(0x4d80,   TYPE_STRING, new Chars("MuxingApp")),
        new PropertyType(0x5741,   TYPE_STRING, new Chars("WritingApp"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public byte[] getSegmentUID() {
        return getPropertyBinary(0x73a4, null);
    }

    public Chars getSegmentFilename() {
        return getPropertyString(0x7384, null);
    }

    public byte[] getPrevUID() {
        return getPropertyBinary(0x3cb923, null);
    }

    public Chars getPrevFilename() {
        return getPropertyString(0x3c83ab, null);
    }

    public byte[] getNextUID() {
        return getPropertyBinary(0x3eb923, null);
    }

    public Chars getNextFilename() {
        return getPropertyString(0x3e83bb, null);
    }

    public Integer getTimecodeScale() {
        return getPropertyUInt(0x2ad7b1, 1000000);
    }

    public Float getDuration() {
        return getPropertyFloat(0x4489, null);
    }

    public Chars getDateUTC() {
        return getPropertyDate(0x4461, null);
    }

    public Chars getTitle() {
        return getPropertyString(0x7ba9, null);
    }

    public Chars getMuxingApp() {
        return getPropertyString(0x4d80, null);
    }

    public Chars getWritingApp() {
        return getPropertyString(0x5741, null);
    }
}
