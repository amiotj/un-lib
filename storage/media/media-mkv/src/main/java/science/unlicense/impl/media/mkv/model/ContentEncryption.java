
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * ContentEncryption := 5035 container {
 *    ContentEncAlgo := 47e1 uint [ def:0; ]
 *    ContentEncKeyID := 47e2 binary;
 *    ContentSignature := 47e3 binary;
 *    ContentSigKeyID := 47e4 binary;
 *    ContentSigAlgo := 47e5 uint;
 *    ContentSigHashAlgo := 47e6 uint;
 *  }
 * 
 * @author Johann Sorel
 */
public class ContentEncryption extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x47e1,  TYPE_UINT,   new Chars("ContentEncAlgo")),
        new PropertyType(0x47e2,  TYPE_BINARY, new Chars("ContentEncKeyID")),
        new PropertyType(0x47e3,  TYPE_BINARY, new Chars("ContentSignature")),
        new PropertyType(0x47e4,  TYPE_BINARY, new Chars("ContentSigKeyID")),
        new PropertyType(0x47e5,  TYPE_UINT,   new Chars("ContentSigAlgo")),
        new PropertyType(0x47e6,  TYPE_UINT,   new Chars("ContentSigHashAlgo"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getContentEncAlgo() {
        return getPropertyUInt(0x47e1, 0);
    }

    public byte[] getContentEncKeyID() {
        return getPropertyBinary(0x47e2, null);
    }

    public byte[] getContentSignature() {
        return getPropertyBinary(0x47e3, null);
    }

    public byte[] getContentSigKeyID() {
        return getPropertyBinary(0x47e4, null);
    }

    public Integer getContentSigAlgo() {
        return getPropertyUInt(0x47e5, null);
    }

    public Integer getContentSigHashAlgo() {
        return getPropertyUInt(0x47e6, null);
    }
}
