
package science.unlicense.impl.media.mkv;

/**
 *
 * @author Johann Sorel
 */
public final class MKVConstants {

    public static final int ID_Segment = 0x18538067;

    public static final int ID_SEEK_SeekHead = 0x114d9b74;
    public static final int ID_SEEK_Seek = 0x4dbb;
    public static final int ID_SEEK_SeekID = 0x53ab;
    public static final int ID_SEEK_SeekPosition = 0x53ac;


    public static final int ID_INFO_Info = 0x1549a966;
    public static final int ID_INFO_SegmentUID = 0x73a4;
    public static final int ID_INFO_SegmentFilename = 0x7384;
    public static final int ID_INFO_PrevUID = 0x3cb923;
    public static final int ID_INFO_PrevFilename = 0x3c83ab;
    public static final int ID_INFO_NextUID = 0x3eb923;
    public static final int ID_INFO_NextFilename = 0x3e83bb;
    public static final int ID_INFO_TimecodeScale = 0x2ad7b1;
    public static final int ID_INFO_Duration = 0x4489;
    public static final int ID_INFO_DateUTC = 0x4461;
    public static final int ID_INFO_Title = 0x7ba9;
    public static final int ID_INFO_MuxingApp = 0x4d80;
    public static final int ID_INFO_WritingApp = 0x5741;


    public static final int ID_CLUSTER_Cluster = 0x1f43b675;
    public static final int ID_CLUSTER_Timecode = 0xe7;
    public static final int ID_CLUSTER_Position = 0xa7;
    public static final int ID_CLUSTER_PrevSize = 0xab;
    public static final int ID_CLUSTER_BlockGroup = 0xa0;
    public static final int ID_CLUSTER_Block = 0xa1;
    public static final int ID_CLUSTER_BlockVirtual = 0xa2;
    public static final int ID_CLUSTER_BlockAdditions = 0x75a1;
    public static final int ID_CLUSTER_BlockMore = 0xa6;
    public static final int ID_CLUSTER_BlockAddID = 0xee;
    public static final int ID_CLUSTER_BlockAdditional = 0xa5;
    public static final int ID_CLUSTER_BlockDuration = 0x9b;
    public static final int ID_CLUSTER_ReferencePriority = 0xfa;
    public static final int ID_CLUSTER_ReferenceBlock = 0xfb;
    public static final int ID_CLUSTER_ReferenceVirtual = 0xfd;
    public static final int ID_CLUSTER_CodecState = 0xa4;
    public static final int ID_CLUSTER_Slices = 0x8e;
    public static final int ID_CLUSTER_TimeSlice = 0xe8;
    public static final int ID_CLUSTER_LaceNumber = 0xcc;
    public static final int ID_CLUSTER_FrameNumber = 0xcd;
    public static final int ID_CLUSTER_BlockAdditionID = 0xcb;
    public static final int ID_CLUSTER_Delay = 0xce;
    public static final int ID_CLUSTER_Duration = 0xcf;


    public static final int ID_TRACK_Tracks = 0x1654ae6b;
    public static final int ID_TRACK_TrackEntry = 0xae;
    public static final int ID_TRACK_TrackNumber = 0xd7;
    public static final int ID_TRACK_TrackUID = 0x73c5;
    public static final int ID_TRACK_TrackType = 0x83;
    public static final int ID_TRACK_FlagEnabled = 0xb9;
    public static final int ID_TRACK_FlagDefault = 0x88;
    public static final int ID_TRACK_FlagLacing  = 0x9c;
    public static final int ID_TRACK_MinCache = 0x6de7;
    public static final int ID_TRACK_MaxCache = 0x6df8;
    public static final int ID_TRACK_DefaultDuration = 0x23e383;
    public static final int ID_TRACK_TrackTimecodeScale = 0x23314f;
    public static final int ID_TRACK_Name = 0x536e;
    public static final int ID_TRACK_Language = 0x22b59c;
    public static final int ID_TRACK_CodecID = 0x86;
    public static final int ID_TRACK_CodecPrivate = 0x63a2;
    public static final int ID_TRACK_CodecName = 0x258688;
    public static final int ID_TRACK_CodecSettings = 0x3a9697;
    public static final int ID_TRACK_CodecInfoURL = 0x3b4040;
    public static final int ID_TRACK_CodecDownloadURL = 0x26b240;
    public static final int ID_TRACK_CodecDecodeAll = 0xaa;
    public static final int ID_TRACK_TrackOverlay = 0x6fab;


    public static final int ID_VIDEO_Video = 0xe0;
    public static final int ID_VIDEO_FlagInterlaced = 0x9a;
    public static final int ID_VIDEO_StereoMode = 0x53b8;
    public static final int ID_VIDEO_PixelWidth = 0xb0;
    public static final int ID_VIDEO_PixelHeight = 0xba;
    public static final int ID_VIDEO_DisplayWidth = 0x54b0;
    public static final int ID_VIDEO_DisplayHeight = 0x54ba;
    public static final int ID_VIDEO_DisplayUnit = 0x54b2;
    public static final int ID_VIDEO_AspectRatioType = 0x54b3;
    public static final int ID_VIDEO_ColourSpace = 0x2eb524;
    public static final int ID_VIDEO_GammaValue = 0x2fb523;


    public static final int ID_AUDIO_Audio = 0xe1;
    public static final int ID_AUDIO_SamplingFrequency = 0xb5;
    public static final int ID_AUDIO_OutputSamplingFrequency = 0x78b5;
    public static final int ID_AUDIO_Channels = 0x94;
    public static final int ID_AUDIO_ChannelPositions = 0x7d7b;
    public static final int ID_AUDIO_BitDepth = 0x6264;


    public static final int ID_CONTENT_ContentEncodings = 0x6d80;
    public static final int ID_CONTENT_ContentEncoding = 0x6240;
    public static final int ID_CONTENT_ContentEncodingOrder = 0x5031;
    public static final int ID_CONTENT_ContentEncodingScope = 0x5032;
    public static final int ID_CONTENT_ContentEncodingType = 0x5033;
    public static final int ID_CONTENT_ContentCompression = 0x5034;
    public static final int ID_CONTENT_ContentCompAlgo = 0x4254;
    public static final int ID_CONTENT_ContentCompSettings = 0x4255;
    public static final int ID_CONTENT_ContentEncryption = 0x5035;
    public static final int ID_CONTENT_ContentEncAlgo = 0x47e1;
    public static final int ID_CONTENT_ContentEncKeyID = 0x47e2;
    public static final int ID_CONTENT_ContentSignature = 0x47e3;
    public static final int ID_CONTENT_ContentSigKeyID = 0x47e4;
    public static final int ID_CONTENT_ContentSigAlgo = 0x47e5;
    public static final int ID_CONTENT_ContentSigHashAlgo = 0x47e6;


    public static final int ID_CUE_Cues = 0x1c53bb6b;
    public static final int ID_CUE_CuePoint = 0xbb;
    public static final int ID_CUE_CueTime = 0xb3;
    public static final int ID_CUE_CueTrackPositions = 0xb7;
    public static final int ID_CUE_CueTrack = 0xf7;
    public static final int ID_CUE_CueClusterPosition = 0xf1;
    public static final int ID_CUE_CueBlockNumber = 0x5378;
    public static final int ID_CUE_CueCodecState = 0xea;
    public static final int ID_CUE_CueReference = 0xdb;
    public static final int ID_CUE_CueRefTime = 0x96;
    public static final int ID_CUE_CueRefCluster = 0x97;
    public static final int ID_CUE_CueRefNumber = 0x535f;
    public static final int ID_CUE_CueRefCodecState = 0xeb;


    public static final int ID_ATTACH_Attachments = 0x1941a469;
    public static final int ID_ATTACH_AttachedFile = 0x61a7;
    public static final int ID_ATTACH_FileDescription = 0x467e;
    public static final int ID_ATTACH_FileName = 0x466e;
    public static final int ID_ATTACH_FileMimeType = 0x4660;
    public static final int ID_ATTACH_FileData = 0x465c;
    public static final int ID_ATTACH_FileUID = 0x46ae;

    public static final int ID_CHAPTER_Chapters = 0x1043a770;
    public static final int ID_CHAPTER_EditionEntry = 0x45b9;
    public static final int ID_CHAPTER_ChapterAtom = 0xb6;
    public static final int ID_CHAPTER_ChapterUID = 0x73c4;
    public static final int ID_CHAPTER_ChapterTimeStart = 0x91;
    public static final int ID_CHAPTER_ChapterTimeEnd = 0x92;
    public static final int ID_CHAPTER_ChapterFlagHidden = 0x98;
    public static final int ID_CHAPTER_ChapterFlagEnabled = 0x4598;
    public static final int ID_CHAPTER_ChapterTrack = 0x8f;
    public static final int ID_CHAPTER_ChapterTrackNumber = 0x89;
    public static final int ID_CHAPTER_ChapterDisplay = 0x80;
    public static final int ID_CHAPTER_ChapString = 0x85;
    public static final int ID_CHAPTER_ChapLanguage = 0x437c;
    public static final int ID_CHAPTER_ChapCountry = 0x437e;

    public static final int ID_TAG_Tags = 0x1254c367;
    public static final int ID_TAG_Tag = 0x7373;
    public static final int ID_TAG_Targets = 0x63c0;
    public static final int ID_TAG_TrackUID = 0x63c5;
    public static final int ID_TAG_ChapterUID = 0x63c4;
    public static final int ID_TAG_AttachmentUID = 0x63c6;
    public static final int ID_TAG_SimpleTag = 0x67c8;
    public static final int ID_TAG_TagName = 0x45a3;
    public static final int ID_TAG_TagString = 0x4487;
    public static final int ID_TAG_TagBinary = 0x4485;

    private MKVConstants(){}

}