
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * SeekHead := 114d9b74 container [ card:*; ] {
 *   Seek := 4dbb container [ card:*; ] ...
 * }
 * @author Johann Sorel
 */
public class SeekHead extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x4dbb, TYPE_SUB, new Chars("Seek"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Sequence getSeek() {
        return getSubs(0x4dbb);
    }
}
