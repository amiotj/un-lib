
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * Seek := 4dbb container [ card:*; ] {
 *  SeekID := 53ab binary;
 *  SeekPosition := 53ac uint;
 * }
 *
 * @author Johann Sorel
 */
public class Seek extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x53ab,  TYPE_BINARY,  new Chars("SeekID")),
        new PropertyType(0x53ac,  TYPE_UINT,    new Chars("SeekPosition"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public byte[] getSeekID() {
        return getPropertyBinary(0x53ab, null);
    }

    public Integer getSeekPosition() {
        return getPropertyUInt(0x53ac, null);
    }
}
