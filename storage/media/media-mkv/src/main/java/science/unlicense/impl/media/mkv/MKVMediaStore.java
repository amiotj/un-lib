
package science.unlicense.impl.media.mkv;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.ebml.EBMLChunk;
import science.unlicense.api.path.Path;
import science.unlicense.impl.binding.ebml.EBMLReader;
import science.unlicense.impl.media.mkv.model.AttachedFile;
import science.unlicense.impl.media.mkv.model.Attachements;
import science.unlicense.impl.media.mkv.model.Audio;
import science.unlicense.impl.media.mkv.model.BlockAdditions;
import science.unlicense.impl.media.mkv.model.BlockGroup;
import science.unlicense.impl.media.mkv.model.BlockMore;
import science.unlicense.impl.media.mkv.model.ChapterAtom;
import science.unlicense.impl.media.mkv.model.ChapterDisplay;
import science.unlicense.impl.media.mkv.model.ChapterTrack;
import science.unlicense.impl.media.mkv.model.Chapters;
import science.unlicense.impl.media.mkv.model.Cluster;
import science.unlicense.impl.media.mkv.model.ContentCompression;
import science.unlicense.impl.media.mkv.model.ContentEncoding;
import science.unlicense.impl.media.mkv.model.ContentEncodings;
import science.unlicense.impl.media.mkv.model.ContentEncryption;
import science.unlicense.impl.media.mkv.model.CuePoint;
import science.unlicense.impl.media.mkv.model.CueReference;
import science.unlicense.impl.media.mkv.model.CueTrackPositions;
import science.unlicense.impl.media.mkv.model.Cues;
import science.unlicense.impl.media.mkv.model.EditionEntry;
import science.unlicense.impl.media.mkv.model.Info;
import science.unlicense.impl.media.mkv.model.Seek;
import science.unlicense.impl.media.mkv.model.SeekHead;
import science.unlicense.impl.media.mkv.model.Segment;
import science.unlicense.impl.media.mkv.model.SimpleTag;
import science.unlicense.impl.media.mkv.model.Slices;
import science.unlicense.impl.media.mkv.model.Tag;
import science.unlicense.impl.media.mkv.model.Tags;
import science.unlicense.impl.media.mkv.model.Targets;
import science.unlicense.impl.media.mkv.model.TimeSlice;
import science.unlicense.impl.media.mkv.model.TrackEntry;
import science.unlicense.impl.media.mkv.model.Tracks;
import science.unlicense.impl.media.mkv.model.Video;

/**
 * View a GIF as a video media.
 *
 * @author Johann Sorel
 */
public class MKVMediaStore extends AbstractMediaStore {

    private static final Dictionary CHUNKS = new HashDictionary();
    static {
        CHUNKS.add(0x61a7,      AttachedFile.class);
        CHUNKS.add(0x1941a469,  Attachements.class);
        CHUNKS.add(0xe1,        Audio.class);
        CHUNKS.add(0x75a1,      BlockAdditions.class);
        CHUNKS.add(0xa0,        BlockGroup.class);
        CHUNKS.add(0xa6,        BlockMore.class);
        CHUNKS.add(0xb6,        ChapterAtom.class);
        CHUNKS.add(0x80,        ChapterDisplay.class);
        CHUNKS.add(0x8f,        ChapterTrack.class);
        CHUNKS.add(0x1043a770,  Chapters.class);
        CHUNKS.add(0x1f43b675,  Cluster.class);
        CHUNKS.add(0x5034,      ContentCompression.class);
        CHUNKS.add(0x6240,      ContentEncoding.class);
        CHUNKS.add(0x6d80,      ContentEncodings.class);
        CHUNKS.add(0x5035,      ContentEncryption.class);
        CHUNKS.add(0xbb,        CuePoint.class);
        CHUNKS.add(0xdb,        CueReference.class);
        CHUNKS.add(0xb7,        CueTrackPositions.class);
        CHUNKS.add(0x1c53bb6b,  Cues.class);
        CHUNKS.add(0x45b9,      EditionEntry.class);
        CHUNKS.add(0x1549a966,  Info.class);
        CHUNKS.add(0x4dbb,      Seek.class);
        CHUNKS.add(0x114d9b74,  SeekHead.class);
        CHUNKS.add(0x18538067,  Segment.class);
        CHUNKS.add(0x67c8,      SimpleTag.class);
        CHUNKS.add(0x8e,        Slices.class);
        CHUNKS.add(0x7373,      Tag.class);
        CHUNKS.add(0x1254c367,  Tags.class);
        CHUNKS.add(0x63c0,      Targets.class);
        CHUNKS.add(0xe8,        TimeSlice.class);
        CHUNKS.add(0xae,        TrackEntry.class);
        CHUNKS.add(0x1654ae6b,  Tracks.class);
        CHUNKS.add(0xe0,        Video.class);
    }
    
    private final Path path;

    public MKVMediaStore(Path path) {
        super(MKVMediaFormat.INSTANCE,path);
        this.path = path;
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        final EBMLReader reader = new EBMLReader(CHUNKS);
        reader.setInput(path);

        while(reader.hasNext()){
            final EBMLChunk chunk = reader.next();
            System.out.println(chunk.toCharsTree(10));
        }
        
        return null;
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        getStreamsMeta();
        return null;
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
