
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * SimpleTag := 67c8 container [ card:*; ] {
 *    TagName := 45a3 string;
 *    TagString := 4487 string;
 *    TagBinary := 4485 binary;
 *  }
 *
 * @author Johann Sorel
 */
public class SimpleTag extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x45a3,  TYPE_STRING,    new Chars("TagName")),
        new PropertyType(0x4487,  TYPE_STRING,    new Chars("TagString")),
        new PropertyType(0x4485,  TYPE_BINARY,    new Chars("TagBinary"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Chars getTagName() {
        return getPropertyString(0x45a3, null);
    }

    public Chars getTagString() {
        return getPropertyString(0x4487, null);
    }

    public byte[] getTagBinary() {
        return getPropertyBinary(0x4485, null);
    }
}
