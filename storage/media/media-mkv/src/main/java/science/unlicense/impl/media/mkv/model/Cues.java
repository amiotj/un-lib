
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * Cues := 1c53bb6b container {
 *    CuePoint := bb container [ card:*; ] ...
 *  }
 * @author Johann Sorel
 */
public class Cues extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xbb,  TYPE_SUB, new Chars("CuePoint"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getCuePoint() {
        return getSubs(0xbb);
    }
}
