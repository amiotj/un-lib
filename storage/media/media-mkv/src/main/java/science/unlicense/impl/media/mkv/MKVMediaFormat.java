
package science.unlicense.impl.media.mkv;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaCapabilities;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.impl.binding.ebml.EBMLConstants;

/**
 *
 * @author Johann Sorel
 */
public class MKVMediaFormat extends AbstractMediaFormat{

    public static final MKVMediaFormat INSTANCE = new MKVMediaFormat();
    
    public MKVMediaFormat() {
        super(new Chars("mkv"),
              new Chars("Matroska"),
              new Chars("Matroska"),
              new Chars[]{
                  new Chars("video/x-matroska"),
                  new Chars("audio/x-matroska")
              },
              new Chars[]{
                new Chars("mkv")
              },
              new byte[][]{EBMLConstants.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public MediaStore createStore(Object input) throws IOException {
        return new MKVMediaStore((Path)input);
    }

}