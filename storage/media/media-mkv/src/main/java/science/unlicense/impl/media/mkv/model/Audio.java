
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * Audio := e1 container {
 *    SamplingFrequency := b5 float [ range:>0.0; def:8000.0; ]
 *    OutputSamplingFrequency := 78b5 float [ range:>0.0; def:8000.0; ]
 *    Channels := 94 uint [ range:1..; def:1; ]
 *    ChannelPositions := 7d7b binary;
 *    BitDepth := 6264 uint [ range:1..; ]
 *  }
 *
 * @author Johann Sorel
 */
public class Audio extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xb5,   TYPE_FLOAT,    new Chars("SamplingFrequency")),
        new PropertyType(0x78b5, TYPE_FLOAT,    new Chars("OutputSamplingFrequency")),
        new PropertyType(0x94,   TYPE_UINT,     new Chars("Channels")),
        new PropertyType(0x7d7b, TYPE_BINARY,   new Chars("ChannelPositions")),
        new PropertyType(0x6264, TYPE_UINT,     new Chars("BitDepth"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Float getSamplingFrequency(){
        return getPropertyFloat(0xb5, 8000.0f);
    }
    
    public Float getOutputSamplingFrequency(){
        return getPropertyFloat(0x78b5, 8000.0f);
    }
    
    public Integer getChannels(){
        return getPropertyUInt(0x94, 1);
    }
    
    public byte[] getChannelPositions(){
        return getPropertyBinary(0x7d7b, null);
    }
    
    public Integer getBitDepth(){
        return getPropertyUInt(0x6264, null);
    }
        
}