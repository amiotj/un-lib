
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * EditionEntry := 45b9 container [ card:*; ] {
 *    ChapterAtom := b6 container [ card:*; ] ...
 *  }
 * @author Johann Sorel
 */
public class EditionEntry extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xb6, TYPE_SUB, new Chars("ChapterAtom"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getChapterAtom() {
        return getSubs(0xb6);
    }
    
}
