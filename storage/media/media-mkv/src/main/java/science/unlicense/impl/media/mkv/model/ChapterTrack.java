
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * ChapterTrack := 8f container {
 *      ChapterTrackNumber := 89 uint [ card:*; range:0..1; ]
 *      ChapterDisplay := 80 container [ card:*; ] ...
 *    }
 * @author Johann Sorel
 */
public class ChapterTrack extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x89, TYPE_UINT, new Chars("ChapterTrackNumber")),
        new PropertyType(0x80, TYPE_SUB,  new Chars("ChapterDisplay"))
    };
    
    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getChapterTrackNumber() {
        return getPropertyUInt(0x89, null);
    }

    public Sequence getChapterDisplay() {
        return getSubs(0x80);
    }
}
