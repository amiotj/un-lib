
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * BlockGroup := a0 container [ card:*; ] {
 *  Block := a1 binary;
 *  BlockVirtual := a2 binary;
 *  BlockAdditions := 75a1 container ...
 *  BlockDuration := 9b uint [ def:TrackDuration; ];
 *  ReferencePriority := fa uint;
 *  ReferenceBlock := fb int [ card:*; ]
 *  ReferenceVirtual := fd int;
 *  CodecState := a4 binary;
 *  Slices := 8e container [ card:*; ] ...
 * }
 *
 * @author Johann Sorel
 */
public class BlockGroup extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xa1,  TYPE_BINARY,    new Chars("Block")),
        new PropertyType(0xa2,  TYPE_BINARY,    new Chars("BlockVirtual")),
        new PropertyType(0x75a1,TYPE_SUB,       new Chars("BlockAdditions")),
        new PropertyType(0x9b,  TYPE_UINT,      new Chars("BlockDuration")),
        new PropertyType(0xfa,  TYPE_UINT,      new Chars("ReferencePriority")),
        new PropertyType(0xfb,  TYPE_INT,       new Chars("ReferenceBlock")),
        new PropertyType(0xfd,  TYPE_INT,       new Chars("ReferenceVirtual")),
        new PropertyType(0xa4,  TYPE_BINARY,    new Chars("CodecState")),
        new PropertyType(0x8e,  TYPE_SUB,       new Chars("Slices"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public byte[] getBlock() {
        return getPropertyBinary(0xa1, null);
    }

    public byte[] getBlockVirtual() {
        return getPropertyBinary(0xa2, null);
    }

    public BlockAdditions getBlockAdditions() {
        return (BlockAdditions) getSub(0x75a1);
    }

    public Integer getBlockDuration() {
        return getPropertyUInt(0x9b, null);
    }

    public Integer getReferencePriority() {
        return getPropertyUInt(0xfa, null);
    }

    public Integer getReferenceBlock() {
        return getPropertyInt(0xfb, null);
    }

    public Integer getReferenceVirtual() {
        return getPropertyUInt(0xfd, null);
    }

    public byte[] getCodecState() {
        return getPropertyBinary(0xa4, null);
    }

    public Sequence getSlices() {
        return getSubs(0x8e);
    }
}
