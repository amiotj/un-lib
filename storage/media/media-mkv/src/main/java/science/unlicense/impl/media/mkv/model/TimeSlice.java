
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * TimeSlice := e8 container [ card:*; ] {
 *  LaceNumber := cc uint [ def:0; ]
 *  FrameNumber := cd uint [ def:0; ]
 *  BlockAdditionID := cb uint [ def:0; ]
 *  Delay := ce uint [ def:0; ]
 *  Duration := cf uint [ def:TrackDuration; ];
 * }
 *
 * @author Johann Sorel
 */
public class TimeSlice extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0xcc,  TYPE_UINT,    new Chars("LaceNumber")),
        new PropertyType(0xcd,  TYPE_UINT,    new Chars("FrameNumber")),
        new PropertyType(0xcb,  TYPE_UINT,    new Chars("BlockAdditionID")),
        new PropertyType(0xce,  TYPE_UINT,    new Chars("Delay")),
        new PropertyType(0xcf,  TYPE_UINT,    new Chars("Duration"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getLaceNumber() {
        return getPropertyUInt(0xcc, 0);
    }

    public Integer getFrameNumber() {
        return getPropertyUInt(0xcd, 0);
    }

    public Integer getBlockAdditionID() {
        return getPropertyUInt(0xcb, 0);
    }

    public Integer getDelay() {
        return getPropertyUInt(0xce, 0);
    }

    public Integer getDuration() {
        return getPropertyUInt(0xcf, null);
    }
}
