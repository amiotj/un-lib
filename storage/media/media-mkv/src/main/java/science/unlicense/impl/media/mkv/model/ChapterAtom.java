
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * ChapterAtom := b6 container [ card:*; ] {
 *    ChapterUID := 73c4 uint [ range:1..; ]
 *    ChapterTimeStart := 91 uint;
 *    ChapterTimeEnd := 92 uint;
 *    ChapterFlagHidden := 98 uint [ range:0..1; def:0; ]
 *    ChapterFlagEnabled := 4598 uint [ range:0..1; def:0; ]
 *    ChapterTrack := 8f container ...
 *  }
 *
 * @author Johann Sorel
 */
public class ChapterAtom extends EBMLChunk{

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x73c4, TYPE_UINT, new Chars("ChapterUID")),
        new PropertyType(0x91,   TYPE_UINT, new Chars("ChapterTimeStart")),
        new PropertyType(0x92,   TYPE_UINT, new Chars("ChapterTimeEnd")),
        new PropertyType(0x98,   TYPE_UINT, new Chars("ChapterFlagHidden")),
        new PropertyType(0x4598, TYPE_UINT, new Chars("ChapterFlagEnabled")),
        new PropertyType(0x8f,   TYPE_SUB,  new Chars("ChapterTrack"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getChapterUID() {
        return getPropertyUInt(0x73c4, null);
    }

    public Integer getChapterTimeStart() {
        return getPropertyUInt(0x91, null);
    }

    public Integer getChapterTimeEnd() {
        return getPropertyUInt(0x92, null);
    }

    public Integer getChapterFlagHidden() {
        return getPropertyUInt(0x98, 0);
    }

    public Integer getChapterFlagEnabled() {
        return getPropertyUInt(0x4598, 0);
    }

    public ChapterTrack getChapterTrack() {
        return (ChapterTrack) getSub(0x8f);
    }
}
