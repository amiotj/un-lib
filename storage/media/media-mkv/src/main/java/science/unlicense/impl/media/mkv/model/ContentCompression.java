
package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * ContentCompression := 5034 container {
 *   ContentCompAlgo := 4254 uint [ def:0; ]
 *   ContentCompSettings := 4255 binary;
 * }
 * 
 * @author Johann Sorel
 */
public class ContentCompression extends EBMLChunk{

    
    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x4254, TYPE_UINT,   new Chars("ContentCompAlgo")),
        new PropertyType(0x4255, TYPE_BINARY, new Chars("ContentCompSettings"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }
    
    public Integer getContentCompAlgo() {
        return getPropertyUInt(0x4254, 0);
    }

    public byte[] getContentCompSettings() {
        return getPropertyBinary(0x4255, null);
    }
}
