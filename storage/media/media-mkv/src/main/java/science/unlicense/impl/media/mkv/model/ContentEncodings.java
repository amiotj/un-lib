package science.unlicense.impl.media.mkv.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.ebml.EBMLChunk;

/**
 *
 * ContentEncodings := 6d80 container { ContentEncoding := 6240 container [
 * card:*; ] ... }
 *
 * @author Johann Sorel
 */
public class ContentEncodings extends EBMLChunk {

    private static final PropertyType[] STRUCT = new PropertyType[]{
        new PropertyType(0x6240, TYPE_SUB, new Chars("ContentEncoding"))
    };

    protected PropertyType[] getStructure() {
        return STRUCT;
    }

    public Sequence getContentEncoding() {
        return getSubs(0x6240);
    }
}
