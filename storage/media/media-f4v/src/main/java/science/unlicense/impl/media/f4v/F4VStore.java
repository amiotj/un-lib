

package science.unlicense.impl.media.f4v;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class F4VStore extends AbstractMediaStore{

    private final Path path;


    public F4VStore(Path path) {
        super(F4VFormat.INSTANCE,path);
        this.path = path;
        try {
            read();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void read() throws IOException{
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
