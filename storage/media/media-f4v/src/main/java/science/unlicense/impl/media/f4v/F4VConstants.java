
package science.unlicense.impl.media.f4v;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class F4VConstants {

    /**
     * File signature ?
     */
    public static final Chars SIGNATURE = new Chars(new byte[]{'F','4','V'});

    private F4VConstants(){}

}
