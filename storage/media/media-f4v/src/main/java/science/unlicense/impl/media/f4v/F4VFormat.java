

package science.unlicense.impl.media.f4v;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * Specification :
 * http://www.adobe.com/devnet/f4v.html
 * http://download.macromedia.com/f4v/video_file_format_spec_v10_1.pdf
 *
 * @author Johann Sorel
 */
public class F4VFormat extends AbstractMediaFormat{

    public static final F4VFormat INSTANCE = new F4VFormat();
    
    public F4VFormat() {
        super(new Chars("f4v"),
              new Chars("F4V"),
              new Chars("F4V"),
              new Chars[]{
                  new Chars("video/x-flv"),
              },
              new Chars[]{
                  new Chars("f4v")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public MediaStore createStore(Object input) throws IOException {
        return new F4VStore((Path)input);
    }

}
