

package science.unlicense.impl.media.f4v;

import science.unlicense.impl.media.f4v.F4VFormat;
import org.junit.Test;
import science.unlicense.api.media.MediaFormat;
import science.unlicense.api.media.Medias;
import org.junit.Assert;

/**
 * Test FLV Format declaration.
 *
 * @author Johann Sorel
 */
public class F4VFormatTest {

    @Test
    public void formatTest(){
        final MediaFormat[] formats = Medias.getFormats();
        for(MediaFormat format : formats){
            if(format instanceof F4VFormat){
                return;
            }
        }

        Assert.fail("F4V Format not found.");
    }

}
