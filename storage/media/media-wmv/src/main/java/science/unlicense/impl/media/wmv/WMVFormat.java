

package science.unlicense.impl.media.wmv;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaCapabilities;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class WMVFormat extends AbstractMediaFormat{

    public static final WMVFormat INSTANCE = new WMVFormat();
    
    public WMVFormat() {
        super(new Chars("wmv"),
              new Chars("WMV"),
              new Chars("Windows Media Video"),
              new Chars[]{
                  new Chars("video/x-ms-wmv")
              },
              new Chars[]{
                new Chars("wmv")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public MediaStore createStore(Object input) throws IOException {
        throw new RuntimeException("not yet");
    }
    
}
