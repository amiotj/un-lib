
package science.unlicense.impl.media.avi.chunk;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * http://msdn.microsoft.com/en-us/library/windows/desktop/dd318229(v=vs.85).aspx
 *
 * @author Johann Sorel
 */
public class BitmapInfoHeader {

    //audio type
    public int biSize;
    public int biWidth;
    public int biHeight;
    public short biPlanes;
    public short biBitCount;
    public Chars biCompression;
    public int biSizeImage;
    public int biXPelsPerMeter;
    public int biYPelsPerMeter;
    public int biColorUsed;
    public int biColorImportant;
    public Color[] palette;

    //decoding informations
    private boolean verticalFlip = false;
    private int nbPixel = 0;
    private int scanlineSize = 0;

    public void read(DataInputStream ds) throws IOException {

        biSize = ds.readInt();
        biWidth = ds.readInt();
        biHeight = ds.readInt();
        biPlanes = ds.readShort();
        biBitCount = ds.readShort();
        biCompression = new Chars(ds.readFully(new byte[4]));
        biSizeImage = ds.readInt();
        biXPelsPerMeter = ds.readInt();
        biYPelsPerMeter = ds.readInt();
        biColorUsed = ds.readInt();
        biColorImportant = ds.readInt();

        if(biColorUsed!=0){
            palette = new Color[biColorUsed];
            for(int i=0;i<biColorUsed;i++){
                palette[i] = new Color(ds.readUByte(), ds.readUByte(), ds.readUByte());
                ds.readUByte();
            }
        }
        
        //extract decoding information
        verticalFlip = biHeight < 0;
        nbPixel = biWidth * biHeight;
        //4 byte padding
        scanlineSize = biWidth * biBitCount;
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(biSize);
        ds.writeInt(biWidth);
        ds.writeInt(biHeight);
        ds.writeShort(biPlanes);
        ds.writeShort(biBitCount);
        ds.write(biCompression.toBytes(CharEncodings.US_ASCII));
        ds.writeInt(biSizeImage);
        ds.writeInt(biXPelsPerMeter);
        ds.writeInt(biYPelsPerMeter);
        ds.writeInt(biColorUsed);
        ds.writeInt(biColorImportant);
    }

    public long computeSize() {
        long size = 40;
        if(palette!=null){
            size += palette.length*4;
        }
        return size;
    }

}
