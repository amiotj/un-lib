
package science.unlicense.impl.media.avi;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.path.Path;
import science.unlicense.impl.binding.riff.RIFFConstants;

/**
 *
 * Resources :
 * 
 * 
 * @author Johann Sorel
 */
public class AVIFormat extends AbstractMediaFormat{

    public static final AVIFormat INSTANCE = new AVIFormat();

    public AVIFormat() {
        super(new Chars("avi"),
              new Chars("AVI"),
              new Chars("Graphics Interchange Format"),
              new Chars[]{
                  new Chars("video/avi"),
                  new Chars("video/msvideo"),
                  new Chars("video/x-msvideo"),
                  new Chars("image/avi"),
                  new Chars("video/xmpg2"),
                  new Chars("application/x-troff-msvideo"),
                  new Chars("audio/aiff"),
                  new Chars("audio/avi")
              },
              new Chars[]{
                new Chars("avi")
              },
              new byte[][]{
                    RIFFConstants.CHUNK_RIFF.toBytes(),
                    RIFFConstants.CHUNK_RIFX.toBytes(),
                });
    }

    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaStore createStore(Object input) {
        return new AVIMediaStore((Path) input);
    }

}
