

package science.unlicense.impl.media.avi.chunk;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.media.avi.AVIConstants;
import static science.unlicense.impl.media.avi.AVIConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class StrfChunk extends DefaultChunk {

    public BitmapInfoHeader bitmapHeader;
    public WaveFormatex waveHeader;

    public StrhChunk type;

    public StrfChunk() {
        super(AVIConstants.TYPE_FORMATHEADER);
    }

    public void computeSize() {
        size = 0;
        if(bitmapHeader!=null){
            size = bitmapHeader.computeSize();
        }
        if(waveHeader!=null){
            size = waveHeader.computeSize();
        }
    }

    public void readInternal(DataInputStream ds) throws IOException {

        if(STREAM_VIDEO.equals(type.fccType)){
            bitmapHeader = new BitmapInfoHeader();
            bitmapHeader.read(ds);
        }else if(STREAM_AUDIO.equals(type.fccType)){
            waveHeader = new WaveFormatex();
            waveHeader.read(ds);
        }else if(STREAM_MIDI.equals(type.fccType)){
            throw new IOException("TODO");
        }else if(STREAM_TEXT.equals(type.fccType)){
            throw new IOException("TODO");
        }else{
            throw new IOException("unknwoned type : "+ type.fccType);
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        if(STREAM_VIDEO.equals(type.fccType)){
            bitmapHeader.write(ds);
        }else if(STREAM_AUDIO.equals(type.fccType)){
            waveHeader.write(ds);
        }else if(STREAM_MIDI.equals(type.fccType)){
            throw new IOException("TODO");
        }else if(STREAM_TEXT.equals(type.fccType)){
            throw new IOException("TODO");
        }else{
            throw new IOException("unknwoned type : "+ type.fccType);
        }
    }
    
}
