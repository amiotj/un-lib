
package science.unlicense.impl.media.avi;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.RIFFWriter;
import science.unlicense.impl.binding.riff.model.LISTChunk;
import science.unlicense.impl.binding.riff.model.RIFFChunk;
import science.unlicense.impl.media.avi.chunk.AvihChunk;
import science.unlicense.impl.media.avi.chunk.BitMapChunk;
import science.unlicense.impl.media.avi.chunk.BitmapInfoHeader;
import science.unlicense.impl.media.avi.chunk.StrfChunk;
import science.unlicense.impl.media.avi.chunk.StrhChunk;

/**
 *
 * @author Johann Sorel
 */
public class AVIMediaWriter extends AbstractWriter{
    
    private int frameRate = 30;
    private Sequence images = new ArraySequence();

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
    }

    public int getFrameRate() {
        return frameRate;
    }
    
    public void pushImage(Image image){
        images.add(image);
    }
    
    public void flush() throws IOException{
        
        final RIFFWriter writer = new RIFFWriter();
        
        /*
        RIFF AVI
        +-hdrl        
        | +-avih
        | +-LIST strl
        | | +-strh
        | | +-strf
        +-LIST movi
        | +-... frames...
        +-idx1
        */
        final RIFFChunk avi = new RIFFChunk();
        avi.setFourCC(AVIConstants.TYPE_AVI);
        final LISTChunk hdrl = new LISTChunk();
        hdrl.setType(AVIConstants.TYPE_HEADER);
            final AvihChunk avih = new AvihChunk();
            final LISTChunk strl = new LISTChunk();
            strl.setType(AVIConstants.TYPE_STREAMLIST);
                final StrhChunk strh = new StrhChunk();
                final StrfChunk strf = new StrfChunk();
        final LISTChunk movi = new LISTChunk();
        movi.setType(AVIConstants.TYPE_MOVIE);
        //final IndxChunk indx = new IndxChunk(); TODO
        
        avi.getSubchunks().add(hdrl);
            hdrl.getSubchunks().add(avih);
            hdrl.getSubchunks().add(strl);
                strl.getSubchunks().add(strh);
                strl.getSubchunks().add(strf);
        avi.getSubchunks().add(movi);
        //avi.getSubchunks().add(indx);
        
        //encode image chunks
        Extent.Long size = null;
        for(int i=0,n=images.getSize();i<n;i++){
            final Image img = (Image) images.get(i);
            size = img.getExtent();
            final BitMapChunk bitmap = new BitMapChunk();
            //TODO make other encodings
            final ArrayOutputStream out = new ArrayOutputStream();
            Images.write(img, new Chars("png"), out);
            bitmap.type = AVIConstants.CHUNK_TYPE_VIDEO;
            bitmap.data = out.getBuffer().toArrayByte();
            movi.getSubchunks().add(bitmap);
        }
        
        //fill metadatas
        avih.dwMicroSecPerFrame = 0;
        avih.dwFlags = 0; //TODO 0x10 when index will work
        avih.dwTotalFrames = movi.getSubchunks().getSize();
        avih.dwStreams = 1; //we only have a video stream
        avih.dwWidth = (int) size.getL(0);
        avih.dwHeight = (int) size.getL(1);
        
        strh.fccType = new Chars("vids");
        strh.fccHandler = AVIConstants.COMPRESSION_PNG;
        strh.dwScale = 1;
        strh.dwRate = frameRate;
        strh.dwLength = movi.getSubchunks().getSize();
        strh.rcFrameright = (int) size.getL(0);
        strh.rcFramebottom = (int) size.getL(1);
        
        strf.bitmapHeader = new BitmapInfoHeader();
        strf.bitmapHeader.biWidth = (int) size.getL(0);
        strf.bitmapHeader.biHeight = (int) size.getL(1);
        strf.bitmapHeader.biPlanes = 1;
        strf.bitmapHeader.biBitCount = 32;
        strf.bitmapHeader.biCompression = AVIConstants.COMPRESSION_PNG;
        strf.bitmapHeader.biSizeImage = (int) (size.getL(0) * size.getL(1) * 4);
        
        
        //compute final size and write chunks
        avi.computeSize();
        writer.write(avi);
        
    }
    
}
