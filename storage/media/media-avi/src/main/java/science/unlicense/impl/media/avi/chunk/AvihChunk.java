
package science.unlicense.impl.media.avi.chunk;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.media.avi.AVIConstants;

/**
 * http://msdn.microsoft.com/en-us/library/windows/desktop/dd318180(v=vs.85).aspx
 *
 * @author Johann Sorel
 */
public class AvihChunk extends DefaultChunk {

    public int dwMicroSecPerFrame;
    public int dwMaxBytesPerSec;
    public int dwPaddingGranularity;
    public int dwFlags;
    public int dwTotalFrames;
    public int dwInitialFrames;
    public int dwStreams;
    public int dwSuggestedBufferSize;
    public int dwWidth;
    public int dwHeight;
    public int[] dwReserved; //size 4

    public AvihChunk() {
        super(AVIConstants.TYPE_AVIHEADER,56);
    }

    public void readInternal(DataInputStream ds) throws IOException {
        dwMicroSecPerFrame      = ds.readInt();
        dwMaxBytesPerSec        = ds.readInt();
        dwPaddingGranularity    = ds.readInt();
        dwFlags                 = ds.readInt();
        dwTotalFrames           = ds.readInt();
        dwInitialFrames         = ds.readInt();
        dwStreams               = ds.readInt();
        dwSuggestedBufferSize   = ds.readInt();
        dwWidth                 = ds.readInt();
        dwHeight                = ds.readInt();
        dwReserved              = ds.readInt(4);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(dwMicroSecPerFrame);
        ds.writeInt(dwMaxBytesPerSec);
        ds.writeInt(dwPaddingGranularity);
        ds.writeInt(dwFlags);
        ds.writeInt(dwTotalFrames);
        ds.writeInt(dwInitialFrames);
        ds.writeInt(dwStreams);
        ds.writeInt(dwSuggestedBufferSize);
        ds.writeInt(dwWidth);
        ds.writeInt(dwHeight);
        ds.writeInt(dwReserved);
    }
    
}
