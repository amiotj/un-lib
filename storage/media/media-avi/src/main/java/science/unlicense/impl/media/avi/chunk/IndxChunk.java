

package science.unlicense.impl.media.avi.chunk;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.media.avi.AVIConstants;

/**
 *
 * @author Johann Sorel
 */
public class IndxChunk extends DefaultChunk {

    public int ckid;
    public int dwFlags;
    public int dwChunkOffset;
    public int dwChunkLength;

    public IndxChunk() {
        super(AVIConstants.TYPE_INDEX);
    }
    
    public void readInternal(DataInputStream ds) throws IOException {
        ckid = ds.readInt();
        dwFlags = ds.readInt();
        dwChunkOffset = ds.readInt();
        dwChunkLength = ds.readInt();
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeInt(ckid);
        ds.writeInt(dwFlags);
        ds.writeInt(dwChunkOffset);
        ds.writeInt(dwChunkLength);
    }
    
}
