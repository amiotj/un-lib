

package science.unlicense.impl.media.avi.chunk;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.DefaultChunk;

/**
 *
 * @author Johann Sorel
 */
public class BitMapChunk extends DefaultChunk {

    public byte[] type;
    public byte[] data;
    
    public BitMapChunk() {
    }

    public void computeSize() {
        size = type.length + data.length;
    }

    public void readInternal(DataInputStream ds) throws IOException {
        data = ds.readFully(new byte[(int)size]);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(type);
        ds.write(data);
    }
    
}
