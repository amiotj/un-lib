

package science.unlicense.impl.media.avi;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.RIFFElement;
import science.unlicense.impl.binding.riff.RIFFReader;
import science.unlicense.impl.binding.riff.model.Chunk;
import science.unlicense.impl.media.avi.chunk.AvihChunk;
import science.unlicense.impl.media.avi.chunk.BitMapChunk;
import science.unlicense.impl.media.avi.chunk.StrfChunk;
import science.unlicense.impl.media.avi.chunk.StrhChunk;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class AVIMediaStore extends AbstractMediaStore{

    private final Path input;

    public AVIMediaStore(Path input) {
        super(AVIFormat.INSTANCE,input);
        this.input = input;
    }

    private void analyze(){
        try{
            final RIFFReader reader = createRiffReader();
            while(reader.hasNext()){
                final RIFFElement element = reader.next();
                System.out.println(element.getChunk());
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        analyze();
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        return new AVIMediaReader(createRiffReader());
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public RIFFReader createRiffReader() throws IOException{
        final Dictionary types = new HashDictionary();
        types.add(AVIConstants.TYPE_AVIHEADER, AvihChunk.class);
        types.add(AVIConstants.TYPE_STREAMHEADER, StrhChunk.class);
        types.add(AVIConstants.TYPE_FORMATHEADER, StrfChunk.class);
        types.add(AVIConstants.TYPE_BITMAP, BitMapChunk.class);
        final AVIRiffReader reader = new AVIRiffReader(types);
        reader.setInput(input);
        return reader;
    }

    private static class AVIRiffReader extends RIFFReader{

        private StrhChunk streamHeader;

        private AVIRiffReader(Dictionary dico){
            super(dico);
        }

        protected Chunk createChunk(Chars fourCC) throws IOException {
            Chunk chunk = super.createChunk(fourCC);
            if(chunk instanceof StrhChunk){
                streamHeader = (StrhChunk) chunk;
            }else if(chunk instanceof StrfChunk){
                ((StrfChunk)chunk).type = streamHeader;
            }
            return chunk;
        }

    }

}
