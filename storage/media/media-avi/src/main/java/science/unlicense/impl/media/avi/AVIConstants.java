
package science.unlicense.impl.media.avi;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class AVIConstants {

    //stream type
    public static final byte[] CHUNK_TYPE_VIDEO = new byte[]{'0','0','d','c'};
    public static final byte[] CHUNK_TYPE_AUDIO = new byte[]{'0','0','w','b'};
    public static final byte[] CHUNK_TYPE_TEXT = new byte[]{'0','0','t','x'};

    // values for list type
    public static final Chars LIST_TYPE_JUNK = new Chars(new byte[]{'J','U','N','K'});

    public static final Chars TYPE_AVI = new Chars(new byte[]{'A','V','I',' '});

    public static final Chars TYPE_HEADER = new Chars(new byte[]{'h','d','r','l'});
    public static final Chars TYPE_MOVIE = new Chars(new byte[]{'m','o','v','i'});
    public static final Chars TYPE_INDEX = new Chars(new byte[]{'i','d','x','1'});
    public static final Chars TYPE_AVIHEADER = new Chars(new byte[]{'a','v','i','h'});
    public static final Chars TYPE_STREAMLIST = new Chars(new byte[]{'s','t','r','l'});
    public static final Chars TYPE_STREAMHEADER = new Chars(new byte[]{'s','t','r','h'});
    public static final Chars TYPE_FORMATHEADER = new Chars(new byte[]{'s','t','r','f'});
    public static final Chars TYPE_BITMAP = new Chars(new byte[]{'0','0','d','b'});

    public static final Chars STREAM_AUDIO = new Chars(new byte[]{'a','u','d','s'});
    public static final Chars STREAM_MIDI = new Chars(new byte[]{'m','i','d','s'});
    public static final Chars STREAM_TEXT = new Chars(new byte[]{'t','x','t','s'});
    public static final Chars STREAM_VIDEO = new Chars(new byte[]{'v','i','d','s'});

    public static final short WAVE_FORMAT_PCM           = (short)0x0001;
    public static final short WAVE_FORMAT_IEEE_FLOAT    = (short)0x0003;
    public static final short WAVE_FORMAT_ALAW          = (short)0x0006;
    public static final short WAVE_FORMAT_MULAW         = (short)0x0007;
    public static final short WAVE_FORMAT_MPEG          = (short)0x0050;
    public static final short WAVE_FORMAT_MPEGLAYER3    = (short)0x0055;
    public static final short WAVE_FORMAT_EXTENSIBLE    = (short)0xFFFE;

    //uncompressed types
    public static final Chars COMPRESSION_NONE = new Chars(new byte[]{0,0,0,0});
    public static final Chars COMPRESSION_RGB = new Chars(new byte[]{'R','G','B',' '});
    public static final Chars COMPRESSION_RAW = new Chars(new byte[]{'R','A','W',' '});
    public static final Chars COMPRESSION_IV41 = new Chars(new byte[]{'I','V','4','1'});
    
    public static final Chars COMPRESSION_RLE = new Chars(new byte[]{'R','L','E',' '});
    public static final Chars COMPRESSION_DIB = new Chars(new byte[]{'D','I','B',' '});
    public static final Chars COMPRESSION_MJPG = new Chars(new byte[]{'M','J','P','G'});
    public static final Chars COMPRESSION_PNG = new Chars(new byte[]{'p','n','g',' '});
    
    
    private AVIConstants(){}

}