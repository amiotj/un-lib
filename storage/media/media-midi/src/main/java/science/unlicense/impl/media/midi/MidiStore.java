

package science.unlicense.impl.media.midi;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class MidiStore extends AbstractMediaStore{

    public MidiStore(Path input) {
        super(MidiFormat.INSTANCE,input);
    }
    
    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
