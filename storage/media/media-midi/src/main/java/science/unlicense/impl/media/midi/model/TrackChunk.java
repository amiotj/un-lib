
package science.unlicense.impl.media.midi.model;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TrackChunk {
    
    public int length;
    public final Sequence events = new  ArraySequence();
    
    public void read(DataInputStream ds) throws IOException {
        length   = ds.readInt();
        
        
        
    }
    
}
