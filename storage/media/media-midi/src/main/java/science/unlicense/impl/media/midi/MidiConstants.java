

package science.unlicense.impl.media.midi;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class MidiConstants {
    
    public static final Chars CHUNK_HEADER = new Chars(new byte[]{'M','T','h','d'});
    public static final Chars CHUNK_TRACK = new Chars(new byte[]{'M','T','r','k'});
    
    public static final int CHUNK_META_EVENT = 0xFF;
    public static final int CHUNK_SYSTEM_EVENT = 0xF0;
    
    public static final int FORMAT_SINGLE_TRACK = 0;
    public static final int FORMAT_MULTIPLE_TRACK = 1;
    public static final int FORMAT_MULTIPLE_SONG = 2;
    
    public static final int META_SEQUENCE_NUMBER                = 0x00;	 
    public static final int META_TEXT_EVENT                     = 0x01;
    public static final int META_COPYRIGHT_NOTICE               = 0x02;
    public static final int META_SEQUENCE_OF_TRACK_NAME         = 0x03;
    public static final int META_INSTRUMENT_NAME                = 0x04;	 
    public static final int META_LYRIC_TEXT                     = 0x05;	 
    public static final int META_MARKER_TEXT                    = 0x06;	 
    public static final int META_CUE_POINT                      = 0x07;
    public static final int META_MIDI_CHANNEL_PREFIX_ASSIGNMENT = 0x20;
    public static final int META_END_OF_TRACK                   = 0x2F;
    public static final int META_TEMPO_SETTING                  = 0x51;
    public static final int META_SMPTE_OFFSET                   = 0x54;
    public static final int META_TIME_SIGNATURE                 = 0x58;
    public static final int META_KEY_SIGNATURE                  = 0x59;
    public static final int META_SEQUENCER_SPECIFIC_EVENT       = 0x7F;
    
    private MidiConstants(){}
    
}
