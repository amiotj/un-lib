

package science.unlicense.impl.media.midi;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MIDI
 * http://faydoc.tripod.com/formats/mid.htm
 * http://www.ccarh.org/courses/253/handout/smf/
 * http://www.sonicspot.com/guide/midifiles.html
 * 
 * @author Johann Sorel
 */
public class MidiFormat extends AbstractMediaFormat{
    
    public static final MidiFormat INSTANCE = new MidiFormat();
    
    public MidiFormat() {
        super(new Chars("midi"),
              new Chars("MIDI"),
              new Chars("Musical Instrument Digital Interface"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("mid")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public MediaStore createStore(Object input) throws IOException {
        return new MidiStore((Path)input);
    }
}
