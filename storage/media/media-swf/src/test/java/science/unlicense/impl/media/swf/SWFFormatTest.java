

package science.unlicense.impl.media.swf;

import science.unlicense.impl.media.swf.SWFFormat;
import org.junit.Test;
import science.unlicense.api.media.MediaFormat;
import science.unlicense.api.media.Medias;
import org.junit.Assert;
import org.junit.Ignore;

/**
 * Test SWF Format declaration.
 *
 * @author Johann Sorel
 */
public class SWFFormatTest {

    @Ignore
    @Test
    public void formatTest(){
        final MediaFormat[] formats = Medias.getFormats();
        for(MediaFormat format : formats){
            if(format instanceof SWFFormat){
                return;
            }
        }

        Assert.fail("SWF Format not found.");
    }

}
