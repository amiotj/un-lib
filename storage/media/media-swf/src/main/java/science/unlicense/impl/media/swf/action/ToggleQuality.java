

package science.unlicense.impl.media.swf.action;

/**
 *
 * @author Johann Sorel
 */
public class ToggleQuality extends Action {
    
    public ToggleQuality() {
        super(0x08);
    }
    
}
