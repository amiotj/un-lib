

package science.unlicense.impl.media.swf.action;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SetTarget extends Action {

    /** STRING */
    public Chars TargetName;
    
    public SetTarget() {
        super(0x8B);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        TargetName = readChars(ds);
    }
    
}
