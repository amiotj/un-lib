

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.geometry.s2d.Rectangle;
import static science.unlicense.impl.media.swf.SWFUtilities.*;
import science.unlicense.impl.media.swf.shape.SWFShapeWithStyle;

/**
 *
 * @author Johann Sorel
 */
public class SWFDefineShape extends SWFTag {

    /** UInt16 */
    public int shapeId;
    /** RECT */
    public Rectangle bounds;
    /** SHAPEWITHSTYLE */
    public SWFShapeWithStyle shape;
    
    public void read(DataInputStream ds) throws IOException {
        shapeId = ds.readUShort();
        bounds = readRectangle(ds);
        shape = new SWFShapeWithStyle();
        int version = 0;
        //if(this instanceof SWFDefineShape4) version = 4;
        if(this instanceof SWFDefineShape3) version = 3;
        else if(this instanceof SWFDefineShape2) version = 2;
        else if(this instanceof SWFDefineShape) version = 1;
        
        shape.read(ds, version);
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeUShort(shapeId);
        writeRectangle(ds, bounds);
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append('\n');
        cb.append(CObjects.toChars(shape));
        return cb.toChars();
    }
    
}
