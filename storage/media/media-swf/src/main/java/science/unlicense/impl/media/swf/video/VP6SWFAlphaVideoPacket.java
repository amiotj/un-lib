
package science.unlicense.impl.media.swf.video;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 * Spec : p.218
 * 
 * @author Johann Sorel
 */
public class VP6SWFAlphaVideoPacket implements VideoPacket{

    public int OffsetToAlpha;
    public byte[] Data;
    public byte[] AlphaData;

    public void read(DataInputStream in, int length) throws IOException{
        OffsetToAlpha = readUB(in, 24);
        Data = in.readFully(new byte[OffsetToAlpha]);
        AlphaData = in.readFully(new byte[length-OffsetToAlpha-3]);
    }

    public void write(DataOutputStream out) throws IOException {
        writeUB(out, 24, OffsetToAlpha);
        out.write(Data);
        out.write(AlphaData);
    }

}
