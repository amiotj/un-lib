

package science.unlicense.impl.media.swf.color;

import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class ColorTransform {

    private final int addR;
    private final int addG;
    private final int addB;
    private final int addA;
    private final int mulR;
    private final int mulG;
    private final int mulB;
    private final int mulA;

    public ColorTransform(
            int addR, int addG, int addB, int addA,
            int mulR, int mulG, int mulB, int mulA) {
        this.addR = addR;
        this.addG = addG;
        this.addB = addB;
        this.addA = addA;
        this.mulR = mulR;
        this.mulG = mulG;
        this.mulB = mulB;
        this.mulA = mulA;
    }

    public void transform(int[] rgb){
        rgb[0] = Maths.clamp( ((rgb[0]*mulR)/256)+addR ,0,255);
        rgb[1] = Maths.clamp( ((rgb[1]*mulG)/256)+addG ,0,255);
        rgb[2] = Maths.clamp( ((rgb[2]*mulB)/256)+addB ,0,255);
        if(rgb.length>3){
            rgb[3] = Maths.clamp( ((rgb[3]*mulA)/256)+addA ,0,255);
        }
    }

}
