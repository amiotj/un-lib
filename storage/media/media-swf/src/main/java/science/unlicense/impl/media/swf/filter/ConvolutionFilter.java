

package science.unlicense.impl.media.swf.filter;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultMatrix;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class ConvolutionFilter extends Filter {

    /** UI8 */
    public int MatrixX;
    /** UI8 */
    public int MatrixY;
    /** FLOAT */
    public float Divisor;
    /** FLOAT */
    public float Bias;
    /** FLOAT[MatrixX * MatrixY] */
    public MatrixRW matrix;
    /** RGBA */
    public Color DefaultColor;
    /** UB[1] */
    public int Clamp;
    /** UB[1] */
    public int PreserveAlpha;

    
    public void read(DataInputStream ds) throws IOException {
        MatrixX = ds.readUByte();
        MatrixY = ds.readUByte();
        Divisor = ds.readFloat();
        Bias = ds.readFloat();
        matrix = DefaultMatrix.create(MatrixX, MatrixY);
        for(int y=0;y<MatrixY;y++){
            for(int x=0;x<MatrixX;x++){
                matrix.set(y,x,ds.readFloat());
            }
        }
        DefaultColor = readRGBA(ds);
        ds.readBits(6);
        Clamp = ds.readBits(1);
        PreserveAlpha = ds.readBits(1);
    }
    
    
}
