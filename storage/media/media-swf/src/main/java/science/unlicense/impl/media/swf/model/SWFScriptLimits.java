

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SWFScriptLimits extends SWFTag {

    /** UI16 */
    public int MaxRecursionDepth;
    /** UI16 */
    public int ScriptTimeoutSeconds;

    public void read(DataInputStream ds) throws IOException {
        MaxRecursionDepth = ds.readUShort();
        ScriptTimeoutSeconds = ds.readUShort();
    }
    
}
