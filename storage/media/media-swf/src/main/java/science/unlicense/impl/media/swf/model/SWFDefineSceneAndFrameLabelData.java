

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFDefineSceneAndFrameLabelData extends SWFTag {

    /** EncodedU32 */
    public int SceneCount;
    /** EncodedU32 size SceneCount */
    public int[] Offsets;
    /** String size SceneCount */
    public Chars[] Names;
    /** EncodedU32 */
    public int FrameLabelCount;
    /** EncodedU32 size FrameLabelCount */
    public int[] FrameNums;
    /** String size FrameLabelCount */
    public Chars[] FrameLabels;



    public void read(DataInputStream ds) throws IOException {
        SceneCount = (int) ds.readVarLengthUInt();

        Offsets = new int[SceneCount];
        Names = new Chars[SceneCount];
        for(int i=0;i<SceneCount;i++){
            Offsets[i] = (int)ds.readVarLengthUInt();
            Names[i] = readChars(ds);
        }

        FrameLabelCount = (int)ds.readVarLengthUInt();
        FrameNums = new int[FrameLabelCount];
        FrameLabels = new Chars[FrameLabelCount];
        for(int i=0;i<FrameLabelCount;i++){
            FrameNums[i] = (int)ds.readVarLengthUInt();
            FrameLabels[i] = readChars(ds);
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        writeRUI32(ds, SceneCount);
        for(int i=0;i<SceneCount;i++){
            writeRUI32(ds, Offsets[i]);
            writeChars(ds, Names[i]);
        }
        writeRUI32(ds, FrameLabelCount);
        for(int i=0;i<FrameLabelCount;i++){
            writeRUI32(ds, FrameNums[i]);
            writeChars(ds, FrameLabels[i]);
        }
    }



}
