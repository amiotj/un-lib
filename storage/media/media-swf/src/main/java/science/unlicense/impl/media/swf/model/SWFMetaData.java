

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFMetaData extends SWFTag {

    /** STRING */
    public Chars metadata;

    public void read(DataInputStream ds) throws IOException {
        metadata = readChars(ds);
    }

    public Chars toChars() {
        return new Chars(getClass().getSimpleName()).concat(metadata);
    }
    
}
