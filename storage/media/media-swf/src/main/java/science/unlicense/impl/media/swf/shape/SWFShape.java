
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.swf.SWFObject;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class SWFShape extends SWFObject {

    /** UB[4] */
    public int NumFillBits;
    /** UB[4] */
    public int NumLineBits;
    /** SHAPERECORD[N] */
    public Sequence shapeRecords = new ArraySequence();
    
    public void read(DataInputStream ds) throws IOException {
        NumFillBits = SWFUtilities.readUB(ds, 4);
        NumLineBits = SWFUtilities.readUB(ds, 4);
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
