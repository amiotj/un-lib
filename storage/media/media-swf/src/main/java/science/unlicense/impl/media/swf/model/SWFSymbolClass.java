

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFSymbolClass extends SWFTag {

    public static final class Symbol extends CObject {
        /** UI16 */
        public int Tag;
        /** STRING */
        public Chars Name;

        public Chars toChars() {
            return new Chars(Tag+"="+Name);
        }
        
    }

    /** UI16 */
    public int NumSymbols;

    public Symbol[] symbols;

    public void read(DataInputStream ds) throws IOException {
        NumSymbols = ds.readUShort();

        symbols = new Symbol[NumSymbols];
        for(int i=0;i<NumSymbols;i++){
            symbols[i] = new Symbol();
            symbols[i].Tag = ds.readUShort();
            symbols[i].Name = readChars(ds);
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
        ds.writeUShort(NumSymbols);
        for(int i=0;i<NumSymbols;i++){
            ds.writeUShort(symbols[i].Tag);
            writeChars(ds,symbols[i].Name);
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(Nodes.toChars(new Chars(this.getClass().getSimpleName()), symbols));
        return cb.toChars();
    }
    
}
