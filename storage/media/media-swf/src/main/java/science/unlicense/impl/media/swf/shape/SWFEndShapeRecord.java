
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.swf.SWFObject;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFEndShapeRecord extends SWFObject {
    
    /** UB[1] */
    public int typeFlag;
    /** UB[5] */
    public int endOfShape;
    
    public void read(DataInputStream ds) throws IOException {
        typeFlag = readUB(ds, 1);
        endOfShape = readUB(ds, 5);
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
