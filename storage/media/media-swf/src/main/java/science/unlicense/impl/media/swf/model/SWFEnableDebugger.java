

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFEnableDebugger extends SWFTag {

    /** STRING */
    public Chars Password;

    public void read(DataInputStream ds) throws IOException {
        Password = readChars(ds);
    }
    
}
