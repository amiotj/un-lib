

package science.unlicense.impl.media.swf.filter;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Filter {
    
    public void read(DataInputStream ds) throws IOException {
        throw new IOException("Not implemented yet");
    }
    
    public void write(DataOutputStream ds) throws IOException {
        throw new IOException("Not implemented yet");
    }
    
}
