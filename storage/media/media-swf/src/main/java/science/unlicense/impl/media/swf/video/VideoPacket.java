
package science.unlicense.impl.media.swf.video;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Video packets are contained in the SWFVideoFrame tag.
 * The type of video packet is defined in the SWFDefineVideoStream.
 * 
 * @author Johann Sorel
 */
public interface VideoPacket {

    /**
     * Read video packet.
     *
     * @param in input
     * @param frameLength total frame length
     * @throws science.unlicense.api.io.IOException
     */
    void read(DataInputStream in, int frameLength) throws IOException;

    /**
     * Write video packet.
     *
     * @param out output
     * @throws science.unlicense.api.io.IOException
     */
    void write(DataOutputStream out) throws IOException;

}
