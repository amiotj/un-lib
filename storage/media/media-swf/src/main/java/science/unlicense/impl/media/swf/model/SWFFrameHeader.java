
package science.unlicense.impl.media.swf.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.media.swf.SWFObject;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 * This should be part of the header, but unlike the header it might be compressed.
 * So we isolate it from the other header informations
 * 
 * @author Johann Sorel
 */
public class SWFFrameHeader extends SWFObject {

    public Rectangle frame;
    public int frameRate;
    public int frameCount;
    
    public void read(final DataInputStream ds) throws IOException{
        frame = SWFUtilities.readRectangle(ds);
        frameRate = ds.readUShort();
        frameCount = ds.readUShort();
    }
    
    public void write(final DataOutputStream ds) throws IOException{
        SWFUtilities.writeRectangle(ds, frame);
        ds.writeUShort(frameRate);
        ds.writeUShort(frameCount);
    }
    
}
