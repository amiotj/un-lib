

package science.unlicense.impl.media.swf.filter;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class BlurFilter extends Filter {

    /** FIXED */
    public float BlurX;
    /** FIXED */
    public float BlurY;
    /** UB[5] */
    public int Passes;

    
    public void read(DataInputStream ds) throws IOException {
        
        BlurX = readFixed32(ds);
        BlurY = readFixed32(ds);
        Passes = ds.readBits(5);
        ds.readBits(3);
    }
    
    
}
