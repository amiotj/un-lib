
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class SWFLineStyle {

    public static final int SHAPE_VERSION_1 = 1;
    public static final int SHAPE_VERSION_2 = 2;
    public static final int SHAPE_VERSION_3 = 3;
    public static final int SHAPE_VERSION_4 = 4;
    
    /** UI16 */
    public int width;
    public Color color;
    
    public void read(DataInputStream ds, int shapeVersion) throws IOException {
        width = ds.readUShort();
        if(shapeVersion>=SHAPE_VERSION_3){
            color = SWFUtilities.readRGBA(ds);
        }else{
            color = SWFUtilities.readRGB(ds);
        }
    }

    public void write(DataOutputStream ds, int shapeVersion) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
