
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFStyleChangeRecord {

    private final int shapeVersion;
    
    /** UB[1] */
    public int StateNewStyles;
    /** UB[1] */
    public int StateLineStyle;
    /** UB[1] */
    public int StateFillStyle1;
    /** UB[1] */
    public int StateFillStyle0;
    /** UB[1] */
    public int StateMoveTo;
    /** 0 or UB[5] */
    public int MoveBits;
    /** 0 or SB[MoveBits] */
    public int MoveDeltaX;
    /** 0 or SB[MoveBits] */
    public int MoveDeltaY;
    /** 0 or UB[FillBits] */
    public int FillStyle0;
    /** 0 or UB[FillBits] */
    public int FillStyle1;
    /** 0 or UB[LineBits] */
    public int LineStyle;
    /** null or FILLSTYLEARRAY */
    public SWFFillStyleArray FillStyles;
    /** null or LINESTYLEARRAY */
    public SWFLineStyleArray LineStyles;
    /** 0 or UB[4] */
    public int NumFillBits;
    /** 0 or UB[4] */
    public int NumLineBits;

    public SWFStyleChangeRecord(int shapeVersion) {
        this.shapeVersion = shapeVersion;
    }
    
    public void read(DataInputStream ds, int flags, int numFillBits, int numLineBits) throws IOException {
        StateNewStyles      = (flags >> 4) & 0x01;
        StateLineStyle      = (flags >> 3) & 0x01;
        StateFillStyle1     = (flags >> 2) & 0x01;
        StateFillStyle0     = (flags >> 1) & 0x01;
        StateMoveTo         = (flags >> 0) & 0x01;
        if(StateMoveTo==1) MoveBits = readUB(ds, 5);
        if(StateMoveTo==1) MoveDeltaX = ds.readSignedBits(MoveBits);
        if(StateMoveTo==1) MoveDeltaY = ds.readSignedBits(MoveBits);
        if(StateFillStyle0==1) FillStyle0 = readUB(ds, numFillBits);
        if(StateFillStyle1==1) FillStyle1 = readUB(ds, numFillBits);
        if(StateLineStyle==1) LineStyle = readUB(ds, numLineBits);
        if(StateNewStyles==1){
            FillStyles = new SWFFillStyleArray();
            FillStyles.read(ds,shapeVersion);
            LineStyles = new SWFLineStyleArray();
            LineStyles.read(ds,shapeVersion);
            NumFillBits = readUB(ds, 4);
            NumLineBits = readUB(ds, 4);
        }
        
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public String toString() {
        return "SWFStyleChangeRecord "+StateMoveTo+"  "+MoveDeltaX+"  "+MoveDeltaY;
    }
    
}
