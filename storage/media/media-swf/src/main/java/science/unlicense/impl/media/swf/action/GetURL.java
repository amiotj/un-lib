

package science.unlicense.impl.media.swf.action;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class GetURL extends Action {

    /** STRING */
    public Chars UrlString;
    /** STRING */
    public Chars TargetString;
    
    public GetURL() {
        super(0x83);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        UrlString = readChars(ds);
        TargetString = readChars(ds);
    }
    
}
