

package science.unlicense.impl.media.swf.filter;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class GlowFilter extends Filter {

    /** RGBA */
    public Color GlowColor;
    /** FIXED */
    public float BlurX ;
    /** FIXED */
    public float BlurY;
    /** FIXED8 */
    public float Strength;
    /** UB[1] */
    public int InnerGlow;
    /** UB[1] */
    public int Knockout;
    /** UB[1] */
    public int CompositeSource;
    /** UB[5] */
    public int Passes;

    
    public void read(DataInputStream ds) throws IOException {
        GlowColor = readRGBA(ds);
        BlurX = readFixed32(ds);
        BlurY = readFixed32(ds);
        Strength = readFB(ds, 8);
        InnerGlow = ds.readBits(1);
        Knockout = ds.readBits(1);
        CompositeSource = ds.readBits(1);
        Passes = ds.readBits(5);
    }
    
}
