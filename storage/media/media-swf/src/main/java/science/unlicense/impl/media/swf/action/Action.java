

package science.unlicense.impl.media.swf.action;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Action {
    
    public final int code;
    
    public int Length = 0;

    protected Action(int code) {
        this.code = code;
    }
    
    public void read(DataInputStream ds) throws IOException {
        if(code>=0x80){
            Length = ds.readUShort();
        }
    }
    
    public void write(DataOutputStream ds) throws IOException {
        throw new IOException("Not implemented yet");
    }
    
}
