

package science.unlicense.impl.media.swf.filter;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class DropShadowFilter extends Filter {

    /** RGBA */
    public Color DropShadowColor;
    /** FIXED */
    public float BlurX;
    /** FIXED */
    public float BlurY;
    /** FIXED */
    public float Angle;
    /** FIXED */
    public float Distance;
    /** FIXED8 */
    public float Strength;
    /** UB[1] */
    public int InnerShadow;
    /** UB[1] */
    public int Knockout;
    /** UB[1] */
    public int CompositeSource;
    /** UB[5] */
    public int Passes;

    public void read(DataInputStream ds) throws IOException {
        DropShadowColor = readRGBA(ds);
        BlurX = readFixed32(ds);
        BlurY = readFixed32(ds);
        Angle = readFixed32(ds);
        Distance = readFixed32(ds);
        Strength = readFB(ds, 8);
        InnerShadow = ds.readBits(1);
        Knockout = ds.readBits(1);
        CompositeSource = ds.readBits(1);
        Passes = ds.readBits(5);
    }
    
}
