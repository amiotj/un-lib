
package science.unlicense.impl.media.swf;

import science.unlicense.api.CObject;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * @author Johann Sorel
 */
public abstract class SWFObject extends CObject{

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;
}
