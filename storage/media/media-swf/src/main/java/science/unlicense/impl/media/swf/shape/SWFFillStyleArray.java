
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SWFFillStyleArray {

    /** UI8 */
    public int fillStyleCount;
    public SWFFillStyle[] styles;
    
    public void read(DataInputStream ds, int shapeVersion) throws IOException {
        fillStyleCount = ds.readUByte();
        if(fillStyleCount==0xFF){
            fillStyleCount = ds.readUShort();
        }
        styles = new SWFFillStyle[fillStyleCount];
        for(int i=0;i<fillStyleCount;i++){
            styles[i] = new SWFFillStyle();
            styles[i].read(ds,shapeVersion);
        }
    }

    public void write(DataOutputStream ds, int shapeVersion) throws IOException {
        if(fillStyleCount<0xFF){
            ds.writeUByte(fillStyleCount);
        }else{
            ds.writeUByte(0xFF);
            ds.writeUShort(fillStyleCount);
        }
        for(int i=0;i<fillStyleCount;i++){
            styles[i].write(ds,shapeVersion);
        }
    }

}
