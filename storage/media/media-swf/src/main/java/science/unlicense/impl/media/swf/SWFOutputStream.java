
package science.unlicense.impl.media.swf;

import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Extends DataOutputStream to automaticaly realign stream to byte end when
 * writing some primitive types.
 * 
 * 
 * @author Johann Sorel
 */
public class SWFOutputStream extends DataOutputStream {
    
    public SWFOutputStream(ByteOutputStream out, NumberEncoding encoding) {
        super(out, encoding);
    }

    @Override
    public void writeByte(byte value) throws IOException {
        skipToByteEnd();
        super.writeByte(value);
    }

    @Override
    public void writeUByte(int value) throws IOException {
        skipToByteEnd();
        super.writeUByte(value);
    }

    @Override
    public void writeUByte(int[] values, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        super.writeUByte(values, offset, length, encoding);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        skipToByteEnd();
        super.write(buffer, offset, length);
    }

    @Override
    public void writeShort(short value) throws IOException {
        skipToByteEnd();
        super.writeShort(value);
    }

    @Override
    public void writeShort(short[] values, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        super.writeShort(values, offset, length, encoding);
    }

    @Override
    public void writeUShort(int value) throws IOException {
        skipToByteEnd();
        super.writeUShort(value);
    }

    @Override
    public void writeUShort(int[] values, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        super.writeUShort(values, offset, length, encoding);
    }

    @Override
    public void writeInt(int value) throws IOException {
        skipToByteEnd();
        super.writeInt(value);
    }

    @Override
    public void writeInt(int[] values, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        super.writeInt(values, offset, length, encoding);
    }

    @Override
    public void writeFloat(float value) throws IOException {
        skipToByteEnd();
        super.writeFloat(value);
    }

    @Override
    public void writeFloat(float[] values, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        super.writeFloat(values, offset, length, encoding);
    }
        
}
