
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class SWFShapeWithStyle extends CObject{

    public SWFFillStyleArray fillStyles;
    public SWFLineStyleArray lineStyles;
    /** UB[4] */
    public int numFillBits;
    /** UB[4] */
    public int numLineBits;
    /** SHAPERECORD[N] */
    public Sequence shapeRecords = new ArraySequence();
    
    /**
     * Draft rebuild geometry from shape records.
     * 
     * @return 
     */
    public Geometry2D buildGeometry(){
                
        final Path path = new Path();
        
        for(int i=0,n=shapeRecords.getSize();i<n;i++){
            final Object cdt = shapeRecords.get(i);
            if(cdt instanceof SWFStyleChangeRecord){
                final SWFStyleChangeRecord c = (SWFStyleChangeRecord) cdt;
                if(c.StateMoveTo==1){
                    path.appendMoveTo(c.MoveDeltaX, c.MoveDeltaY);
                }
                
            }else if(cdt instanceof SWFCurvedEdgeRecord){
                final SWFCurvedEdgeRecord c = (SWFCurvedEdgeRecord) cdt;
                path.appendQuadToRelative(
                        c.controlDeltaX, c.controlDeltaY,
                        c.anchorDeltaX, c.anchorDeltaY);          
                
            }else if(cdt instanceof SWFStraightEdgeRecord){
                final SWFStraightEdgeRecord c = (SWFStraightEdgeRecord) cdt;
                path.appendLineToRelative(c.deltaX, c.deltaY);
                
            }else{
                throw new RuntimeException("Unexpected type "+cdt);
            }
        }
        
        return path;
    }
    
    public void read(DataInputStream ds, int shapeversion) throws IOException {
        fillStyles = new SWFFillStyleArray();
        fillStyles.read(ds, shapeversion);
        lineStyles = new SWFLineStyleArray();
        lineStyles.read(ds, shapeversion);
        numFillBits = SWFUtilities.readUB(ds, 4);
        numLineBits = SWFUtilities.readUB(ds, 4);
        
        while(true){
            final int flag0 = SWFUtilities.readUB(ds, 1);
            
            if(flag0==0){
                final int flags = SWFUtilities.readUB(ds, 5);
                if(flags==0){
                    //end
                    break;
                }else{
                    //style change
                    final SWFStyleChangeRecord rec = new SWFStyleChangeRecord(shapeversion);
                    rec.read(ds, flags, numFillBits, numLineBits);
                    shapeRecords.add(rec);
                    if(rec.StateNewStyles==1){
                        //this block changes the number of the next blocks bits read
                        numFillBits = rec.NumFillBits;
                        numLineBits = rec.NumLineBits;
                    }
                }
            }else{
                final int flag1 = SWFUtilities.readUB(ds, 1);
                if(flag1==1){
                    final SWFStraightEdgeRecord rec = new SWFStraightEdgeRecord();
                    rec.read(ds);
                    shapeRecords.add(rec);
                }else{
                    final SWFCurvedEdgeRecord rec = new SWFCurvedEdgeRecord();
                    rec.read(ds);
                    shapeRecords.add(rec);
                }
            }
        }
        
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName());
        cb.append(Nodes.toChars(new Chars("shapes"), shapeRecords));
        return cb.toChars();
    }

}
