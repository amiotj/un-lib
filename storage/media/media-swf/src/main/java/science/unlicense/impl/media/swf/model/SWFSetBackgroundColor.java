

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFSetBackgroundColor extends SWFTag {

    /** RGB */
    public Color backgroundColor;

    public void read(DataInputStream ds) throws IOException {
        backgroundColor = readRGB(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeRGB(ds, backgroundColor);
    }

    public Chars toChars() {
        return new Chars(getClass().getSimpleName()).concat(backgroundColor.toChars());
    }

}
