

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFSoundStreamHead extends SWFTag {

    /** UB[4] */
    public int Reserved;
    /**
     * UB[2]
     * Playback sampling rate:
     * 0 = 5.5 kHz
     * 1 = 11 kHz
     * 2 = 22 kHz
     * 3 = 44 kHz
     */
    public int PlaybackSoundRate;
    /**
     * UB[1]
     * Always 1 (16 bit).
     */
    public int PlaybackSoundSize;
    /**
     * UB[1]
     * Number of playback channels:
     * 0 = sndMono
     * 1 = sndStereo
     */
    public int PlaybackSoundType;
    /**
     * UB[4]
     * Format of streaming sound data :
     * 1 = ADPCM
     * 2 = MP3
     */
    public int StreamSoundCompression;
    /**
     * UB[2]
     * The sampling rate of the streaming sound data:
     * 0 = 5.5 kHz
     * 1 = 11 kHz
     * 2 = 22 kHz
     * 3 = 44 kHz
     */
    public int StreamSoundRate;
    /**
     * UB[1] Always 1 (16 bit).
     */
    public int StreamSoundSize;
    /**
     * UB[1]
     * Number of channels in the streaming sound data.
     * 0 = sndMono
     * 1 = sndStereo
     */
    public int StreamSoundType;
    /** UI16 */
    public int StreamSoundSampleCount;
    /**
     * SI16
     * If sound compression = 2;
     * Otherwise field missing
     */
    public int LatencySeek;


    public void read(DataInputStream ds) throws IOException {
        Reserved = readUB(ds, 4);
        PlaybackSoundRate = readUB(ds, 2);
        PlaybackSoundSize = readUB(ds, 1);
        PlaybackSoundType = readUB(ds, 1);
        StreamSoundCompression = readUB(ds, 4);
        StreamSoundRate = readUB(ds, 2);
        StreamSoundSize = readUB(ds, 1);
        StreamSoundType = readUB(ds, 1);
        StreamSoundSampleCount = ds.readUShort();
        if(StreamSoundCompression==2){
            LatencySeek = ds.readUShort();
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
        writeUB(ds, 4, Reserved);
        writeUB(ds, 2, PlaybackSoundRate);
        writeUB(ds, 1, PlaybackSoundSize);
        writeUB(ds, 1, PlaybackSoundType);
        writeUB(ds, 4, StreamSoundCompression);
        writeUB(ds, 2, StreamSoundRate);
        writeUB(ds, 1, StreamSoundSize);
        writeUB(ds, 1, StreamSoundType);
        ds.writeUShort(StreamSoundSampleCount);
        if(StreamSoundCompression==2){
            ds.writeShort((short) LatencySeek);
        }
    }



}
