

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SWFSetTabIndex extends SWFTag {
    
    /** UI16 */
    public int Depth;
    /** UI16 */
    public int TabIndex;

    public void read(DataInputStream ds) throws IOException {
        Depth = ds.readUShort();
        TabIndex = ds.readUShort();
    }
    
}
