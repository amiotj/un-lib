
package science.unlicense.impl.media.swf.vm;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;

/**
 * Flash virtual machine.
 * TODO : uncomplete 
 * 
 * @author Johann Sorel
 */
public class SWFVM {

    private final Dictionary dictionnary = new HashDictionary();
    private final Sequence displayList = new ArraySequence();

    public SWFVM() {
    }

    public Dictionary getDictionnary() {
        return dictionnary;
    }

    public Sequence getDisplayList() {
        return displayList;
    }
    
}
