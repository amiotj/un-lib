

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFImportAssets extends SWFTag {

    /** URL */
    public Chars url;
    /** UI16 */
    public int Count;
    /** Map of <Tag,Name> UI16,String. */
    public final Dictionary tags = new HashDictionary();

    public void read(DataInputStream ds) throws IOException {
        url = readChars(ds);
        Count = ds.readUShort();
        for(int i=0;i<Count;i++){
            int tag = ds.readUShort();
            Chars name = readChars(ds);
            tags.add(tag, name);
        }
    }
    
}
