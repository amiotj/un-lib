

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.geometry.s2d.Rectangle;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFDefineScalingGrid extends SWFTag {

    /** UI16 */
    public int CharacterId;
    /** RECT */
    public Rectangle Splitter;

    public void read(DataInputStream ds) throws IOException {
        CharacterId = ds.readUShort();
        Splitter = readRectangle(ds);
    }
    
    
}
