
package science.unlicense.impl.media.swf;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class SWFConstants {

    public static final Chars SIGNATURE_CWS = new Chars(new byte[]{'C','W','S'});
    public static final Chars SIGNATURE_FWS = new Chars(new byte[]{'F','W','S'});
    public static final Chars SIGNATURE_ZWS = new Chars(new byte[]{'Z','W','S'});

    public static final int LANGUAGE_LATIN = 1;
    public static final int LANGUAGE_JAPANESE = 2;
    public static final int LANGUAGE_KOREAN = 3;
    public static final int LANGUAGE_SIMPLIFIED_CHINESE = 4;
    public static final int LANGUAGE_TRADITIONAL_CHINESE = 5;

    //DISPLAY LIST TAGS
    public static final int TAG_PLACEOBJECT = 4;
    public static final int TAG_PLACEOBJECT2 = 26;
    public static final int TAG_PLACEOBJECT3 = 70;
    public static final int TAG_REMOVEOBJECT = 5;
    public static final int TAG_REMOVEOBJECT2 = 28;
    public static final int TAG_SHOWFRAME = 1;
    //CONTROL TAGS
    public static final int TAG_SETBACKGROUNDCOLOR = 9;
    public static final int TAG_FRAMELABEL = 43;
    public static final int TAG_PROTECT = 24;
    public static final int TAG_END = 0;
    public static final int TAG_EXPORTASSETS = 56;
    public static final int TAG_IMPORTASSETS = 57;
    public static final int TAG_ENABLEDEBUGGER = 58;
    public static final int TAG_ENABLEDEBUGGER2 = 64;
    public static final int TAG_SCRIPTLIMITS = 65;
    public static final int TAG_SETTABINDEX = 66;
    public static final int TAG_FILEATTRIBUTES = 69;
    public static final int TAG_IMPORTASSETS2 = 71;
    public static final int TAG_SYMBOLCLASS = 76;
    public static final int TAG_METADATA = 77;
    public static final int TAG_DEFINESCALINGGRID = 78;
    public static final int TAG_DEFINESCENEANDFRAMELABELDATA = 86;
    //ACTION TAGS
    public static final int TAG_DOACTION = 12;
    public static final int TAG_DOINITACTION = 59;
    public static final int TAG_DOABC = 82;
    //SHAPES TAGS
    public static final int TAG_DEFINESHAPE = 2;
    public static final int TAG_DEFINESHAPE2 = 22;
    public static final int TAG_DEFINESHAPE3 = 32;
    public static final int TAG_DEFINESHAPE4 = 83;
    //BITMAP TAGS
    public static final int TAG_DEFINEBITS = 6;
    public static final int TAG_JPEGTABLES = 8;
    public static final int TAG_DEFINEBITSJPEG2 = 21;
    public static final int TAG_DEFINEBITSJPEG3 = 35;
    public static final int TAG_DEFINEBITSLOSSLESS = 20;
    public static final int TAG_DEFINEBITSLOSSLESS2 = 36;
    public static final int TAG_DEFINEBITSJPEG4 = 90;
    //SHAPE MORPHING TAGS
    public static final int TAG_DEFINEMORPHSHAPE = 46;
    public static final int TAG_DEFINEMORPHSHAPE2 = 84;
    //FONT/TEXT TAGS
    public static final int TAG_DEFINEFONT = 10;
    public static final int TAG_DEFINEFONTINFO = 13;
    public static final int TAG_DEFINEFONTINFO2 = 62;
    public static final int TAG_DEFINEFONT2 = 48;
    public static final int TAG_DEFINEFONT3 = 75;
    public static final int TAG_DEFINEFONTALIGNZONES = 73;
    public static final int TAG_DEFINEFONTNAME = 88;
    public static final int TAG_DEFINETEXT = 11;
    public static final int TAG_DEFINETEXT2 = 33;
    public static final int TAG_DEFINEEDITTEXT = 37;
    public static final int TAG_CSMTEXTSETTINGS = 74;
    public static final int TAG_DEFINEFONT4 = 91;
    //SOUND TAGS
    public static final int TAG_DEFINESOUND = 14;
    public static final int TAG_STARTSOUND = 15;
    public static final int TAG_STARTSOUND2 = 89;
    public static final int TAG_SOUNDSTREAMHEAD = 18;
    public static final int TAG_SOUNDSTREAMHEAD2 = 45;
    public static final int TAG_SOUNDSTREAMBLOCK = 19;
    //BUTTON TAGS
    public static final int TAG_DEFINEBUTTON = 7;
    public static final int TAG_DEFINEBUTTON2 = 34;
    public static final int TAG_DEFINEBUTTONCXFORM = 23;
    public static final int TAG_DEFINEBUTTONSOUND = 17;
    //SPRITE/MOVIE TAGS
    public static final int TAG_DEFINESPRITE = 39;
    public static final int TAG_DEFINEVIDEOSTREAM = 60;
    public static final int TAG_VIDEOFRAME = 61;
    //OTHER TAGS
    public static final int TAG_ENABLETELEMETRY = 93;
    public static final int TAG_DEFINEBINARYDATA = 87;


    private SWFConstants(){}

}
