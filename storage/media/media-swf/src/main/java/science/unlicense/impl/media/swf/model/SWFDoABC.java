

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFDoABC extends SWFTag {

    /** UI32 */
    public int Flags;
    /** STRING */
    public Chars Name;
    /** BYTE[] */
    public byte[] ABCData;


    public void read(DataInputStream ds) throws IOException {
        Flags = ds.readInt();
        Name = readChars(ds);
        ABCData = ds.readFully(new byte[remainingBytes(ds)]);
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append(':');
        cb.append(Name);
        return cb.toChars();
    }
    
}
