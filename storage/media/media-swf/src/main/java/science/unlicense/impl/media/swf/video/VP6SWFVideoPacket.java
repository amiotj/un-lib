
package science.unlicense.impl.media.swf.video;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Spec : p.218
 * 
 * @author Johann Sorel
 */
public class VP6SWFVideoPacket implements VideoPacket{

    public byte[] Data;

    public void read(DataInputStream in, int length) throws IOException{
        Data = in.readFully(new byte[length]);
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(Data);
    }

}
