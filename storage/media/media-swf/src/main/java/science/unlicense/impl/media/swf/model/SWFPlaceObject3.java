

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.math.MatrixRW;
import static science.unlicense.impl.media.swf.SWFUtilities.*;
import science.unlicense.impl.media.swf.color.ColorTransform;
import science.unlicense.impl.media.swf.filter.BevelFilter;
import science.unlicense.impl.media.swf.filter.BlurFilter;
import science.unlicense.impl.media.swf.filter.ColorMatrixFilter;
import science.unlicense.impl.media.swf.filter.ConvolutionFilter;
import science.unlicense.impl.media.swf.filter.DropShadowFilter;
import science.unlicense.impl.media.swf.filter.Filter;
import science.unlicense.impl.media.swf.filter.GlowFilter;
import science.unlicense.impl.media.swf.filter.GradientBevelFilter;
import science.unlicense.impl.media.swf.filter.GradientGlowFilter;

/**
 *
 * @author Johann Sorel
 */
public class SWFPlaceObject3 extends SWFTag {

    public static final int BLEND_NORMAL0       = 0;
    public static final int BLEND_NORMAL1       = 1;
    public static final int BLEND_LAYER         = 2;
    public static final int BLEND_MULTIPLY      = 3;
    public static final int BLEND_SCREEN        = 4;
    public static final int BLEND_LIGHTEN       = 5;
    public static final int BLEND_DRAKEN        = 6;
    public static final int BLEND_DIFFERENCE    = 7;
    public static final int BLEND_ADD           = 8;
    public static final int BLEND_SUBSTRACT     = 9;
    public static final int BLEND_INVERT        = 10;
    public static final int BLEND_ALPHA         = 11;
    public static final int BLEND_ERASE         = 12;
    public static final int BLEND_OVERLAY       = 13;
    public static final int BLEND_HARDLIGHT     = 14;
    
    public static final int FILTER_DROPSHADOW   = 0;
    public static final int FILTER_BLUR         = 1;
    public static final int FILTER_GLOW         = 2;
    public static final int FILTER_BEVEL        = 3;
    public static final int FILTER_GRADIENTGLOW = 4;
    public static final int FILTER_CONVOLUTION  = 5;
    public static final int FILTER_COLORMATRIX  = 6;
    public static final int FILTER_GRADIENTBEVEL= 7;
    
    
    /** UB[1] */
    public boolean PlaceFlagHashClipActions;
    /** UB[1] */
    public boolean PlaceFlagHasClipDepth;
    /** UB[1] */
    public boolean PlaceFlagHasName;
    /** UB[1] */
    public boolean PlaceFlagHasRatio;
    /** UB[1] */
    public boolean PlaceFlagHasColorTransform;
    /** UB[1] */
    public boolean PlaceFlagHasMatrix;
    /** UB[1] */
    public boolean PlaceFlagHasCharacter;
    /** UB[1] */
    public boolean PlaceFlagMove;
    /** UB[1] */
    public boolean Reserved;
    /** UB[1] */
    public boolean PlaceFlagOpaqueBackground;
    /** UB[1] */
    public boolean PlaceFlagHasVisible;
    /** UB[1] */
    public boolean PlaceFlagHasImage;
    /** UB[1] */
    public boolean PlaceFlagHasClassName;
    /** UB[1] */
    public boolean PlaceFlagHasCacheAsBitmap;
    /** UB[1] */
    public boolean PlaceFlagHasBlendMode;
    /** UB[1] */
    public boolean PlaceFlagHasFilterList;
    /** UI16 */
    public int Depth;
    /** Chars */
    public Chars ClassName;
    /** UI16 */
    public Integer CharacterId;
    /** Transform matrix data */
    public MatrixRW matrix;
    /** color transform data */
    public ColorTransform colorTransform;
    /** UI16 */
    public Integer ratio;
    /** Chars , character name */
    public Chars Name;
    /** UI16 */
    public Integer ClipDepth;
    /** Surface filter list */
    public Sequence SurafeceFitlerList;
    /** UI8 */
    public Integer BlendMode;
    /** UI8 */
    public Integer BitmapCache;
    /** UI8 */
    public Integer Visible;
    /** UI8 */
    public Color BackgroundColor;
    /** Clip actions */
    public Sequence ClipActions;
    
    
    public void read(DataInputStream ds) throws IOException {
         
        PlaceFlagHashClipActions    = readUB(ds, 1) != 0;
        PlaceFlagHasClipDepth       = readUB(ds, 1) != 0;
        PlaceFlagHasName            = readUB(ds, 1) != 0;
        PlaceFlagHasRatio           = readUB(ds, 1) != 0;
        PlaceFlagHasColorTransform  = readUB(ds, 1) != 0;
        PlaceFlagHasMatrix          = readUB(ds, 1) != 0;
        PlaceFlagHasCharacter       = readUB(ds, 1) != 0;
        PlaceFlagMove               = readUB(ds, 1) != 0;
        Reserved                    = readUB(ds, 1) != 0;
        PlaceFlagOpaqueBackground   = readUB(ds, 1) != 0;
        PlaceFlagHasVisible         = readUB(ds, 1) != 0;
        PlaceFlagHasImage           = readUB(ds, 1) != 0;
        PlaceFlagHasClassName       = readUB(ds, 1) != 0;
        PlaceFlagHasCacheAsBitmap   = readUB(ds, 1) != 0;
        PlaceFlagHasBlendMode       = readUB(ds, 1) != 0;
        PlaceFlagHasFilterList      = readUB(ds, 1) != 0;
        
        Depth = ds.readUShort();
        
        if(PlaceFlagHasClassName || (PlaceFlagHasImage && PlaceFlagHasCharacter)){
            ClassName = readChars(ds);
        }
        if(PlaceFlagHasCharacter){
            CharacterId = ds.readUShort();
        }
        if(PlaceFlagHasMatrix){
            matrix = readMatrix(ds);
        }
        if(PlaceFlagHasColorTransform){
            colorTransform = readRGBATransform(ds);
        }
        if(PlaceFlagHasRatio){
            ratio = ds.readUShort();
        }
        if(PlaceFlagHasName){
            Name = readChars(ds);
        }
        if(PlaceFlagHasClipDepth){
            ClipDepth = ds.readUShort();
        }
        if(PlaceFlagHasFilterList){
            SurafeceFitlerList = new ArraySequence();
            final int nb = ds.readUByte();
            for(int i=0;i<nb;i++){
                final int type = ds.readUByte();
                final Filter filter;
                if(type==FILTER_DROPSHADOW)        filter = new DropShadowFilter();
                else if(type==FILTER_BLUR)         filter = new BlurFilter();
                else if(type==FILTER_GLOW)         filter = new GlowFilter();
                else if(type==FILTER_BEVEL)        filter = new BevelFilter();
                else if(type==FILTER_GRADIENTGLOW) filter = new GradientGlowFilter();
                else if(type==FILTER_CONVOLUTION)  filter = new ConvolutionFilter();
                else if(type==FILTER_COLORMATRIX)  filter = new ColorMatrixFilter();
                else if(type==FILTER_GRADIENTBEVEL)filter = new GradientBevelFilter();
                else throw new IOException("Unknowed filter "+ type);
                filter.read(ds);
                SurafeceFitlerList.add(filter);
            }
        }
        if(PlaceFlagHasBlendMode){
            BlendMode = ds.readUByte();
        }
        if(PlaceFlagHasCacheAsBitmap){
            BitmapCache = ds.readUByte();
        }
        if(PlaceFlagHasVisible){
            Visible = ds.readUByte();
            BackgroundColor = readRGBA(ds);
        }
        if(PlaceFlagHashClipActions){
            ClipActions = new ArraySequence();
        }
        
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
    }
        
}
