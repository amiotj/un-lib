
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SWFLineStyleArray  {

    /** UI8 */
    public int count;
    public SWFLineStyle[] styles;
    
    public void read(DataInputStream ds, int shapeVersion) throws IOException {
        count = ds.readUByte();
        if(count==0xFF){
            count = ds.readUShort();
        }
        styles = new SWFLineStyle[count];
        for(int i=0;i<count;i++){
            if(shapeVersion==1 || shapeVersion==2 || shapeVersion==3){
                styles[i] = new SWFLineStyle();
            }else{
                styles[i] = new SWFLineStyle2();
            }
            styles[i].read(ds,shapeVersion);
        }
    }

    public void write(DataOutputStream ds, int shapeVersion) throws IOException {
        if(count<0xFF){
            ds.writeUByte(count);
        }else{
            ds.writeUByte(0xFF);
            ds.writeUShort(count);
        }
        for(int i=0;i<count;i++){
            styles[i].write(ds,shapeVersion);
        }
    }

}
