
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class SWFFillStyle {

    public static final int SHAPE_VERSION_1 = 1;
    public static final int SHAPE_VERSION_2 = 2;
    public static final int SHAPE_VERSION_3 = 3;
    
    public static final int TYPE_SOLID = 0x00;
    public static final int TYPE_LINEAR_GRADIENT = 0x10;
    public static final int TYPE_RADIAL_GRADIENT = 0x12;
    public static final int TYPE_FOCAL_GRADIENT = 0x13;
    public static final int TYPE_REPEATED_BITMAP = 0x40;
    public static final int TYPE_CLIPPED_BITMAP = 0x41;
    public static final int TYPE_NONSMOOTH_REPEATED_BITMAP = 0x42;
    public static final int TYPE_NONSMOOTH_CLIPPED_BITMAP = 0x43;
    
    /** UI8 */
    public int fillStyleType;
    public Color rgba;
    public MatrixRW gradientMatrix;
    public Object gradient;
    public int bitmapId;
    public MatrixRW bitmapMatrix;
    
    public void read(DataInputStream ds, int shapeVersion) throws IOException {
        fillStyleType = ds.readUByte();
        
        if(fillStyleType!=TYPE_SOLID 
           && fillStyleType!=TYPE_LINEAR_GRADIENT
           && fillStyleType!=TYPE_RADIAL_GRADIENT
           && fillStyleType!=TYPE_FOCAL_GRADIENT
           && fillStyleType!=TYPE_REPEATED_BITMAP
           && fillStyleType!=TYPE_CLIPPED_BITMAP
           && fillStyleType!=TYPE_NONSMOOTH_REPEATED_BITMAP
           && fillStyleType!=TYPE_NONSMOOTH_CLIPPED_BITMAP){
            throw new IOException("Unexpected fill type : "+fillStyleType);
        }
        
        if(fillStyleType==TYPE_SOLID){
            if(shapeVersion>=SHAPE_VERSION_3){
                rgba = SWFUtilities.readRGBA(ds);
            }else{
                rgba = SWFUtilities.readRGB(ds);
            }
        }        
        if(fillStyleType==TYPE_LINEAR_GRADIENT || fillStyleType==TYPE_RADIAL_GRADIENT || fillStyleType==TYPE_FOCAL_GRADIENT){
            gradientMatrix = SWFUtilities.readMatrix(ds);
        }
        if(fillStyleType==TYPE_LINEAR_GRADIENT || fillStyleType==TYPE_RADIAL_GRADIENT){
            gradient = new SWFGradient();
            ((SWFGradient)gradient).read(ds, shapeVersion);
        }else if(fillStyleType==TYPE_FOCAL_GRADIENT){
            gradient = new SWFFocalGradient();
            ((SWFFocalGradient)gradient).read(ds, shapeVersion);
        }else if(fillStyleType==TYPE_REPEATED_BITMAP || fillStyleType==TYPE_CLIPPED_BITMAP ||
                 fillStyleType==TYPE_NONSMOOTH_REPEATED_BITMAP || fillStyleType==TYPE_NONSMOOTH_CLIPPED_BITMAP){
            bitmapId = ds.readUShort();
            bitmapMatrix = SWFUtilities.readMatrix(ds);
        }
        
    }

    public void write(DataOutputStream ds, int shapeVersion) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
