

package science.unlicense.impl.media.swf;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.AbstractStore;
import science.unlicense.impl.io.zlib.ZlibInputStream;
import science.unlicense.impl.io.zlib.ZlibOutputStream;
import science.unlicense.impl.media.swf.model.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFStore extends AbstractStore{

    private static final Dictionary TAG_CLASSES = new HashDictionary();
    static {
        //TODO list all tags
        TAG_CLASSES.add( 4, SWFPlaceObject.class);
        TAG_CLASSES.add(26, SWFPlaceObject2.class);
        TAG_CLASSES.add(70, SWFPlaceObject3.class);
        TAG_CLASSES.add( 5, SWFRemoveObject.class);
        TAG_CLASSES.add(28, SWFRemoveObject2.class);
        TAG_CLASSES.add( 1, SWFShowFrame.class);

        TAG_CLASSES.add( 9, SWFSetBackgroundColor.class);
        TAG_CLASSES.add(43, SWFFrameLabel.class);
        TAG_CLASSES.add(24, SWFProtect.class);
        TAG_CLASSES.add( 0, SWFEnd.class);
        TAG_CLASSES.add(56, SWFExportAssets.class);
        TAG_CLASSES.add(57, SWFImportAssets.class);
        TAG_CLASSES.add(58, SWFEnableDebugger.class);
        TAG_CLASSES.add(64, SWFEnableDebugger2.class);
        TAG_CLASSES.add(65, SWFScriptLimits.class);
        TAG_CLASSES.add(66, SWFSetTabIndex.class);
        TAG_CLASSES.add(69, SWFFileAttributes.class);
        TAG_CLASSES.add(71, SWFImportAssets2.class);
        TAG_CLASSES.add(76, SWFSymbolClass.class);
        TAG_CLASSES.add(77, SWFMetaData.class);
        TAG_CLASSES.add(78, SWFDefineScalingGrid.class);
        TAG_CLASSES.add(86, SWFDefineSceneAndFrameLabelData.class);

        TAG_CLASSES.add(12, SWFDoAction.class);
        TAG_CLASSES.add(59, SWFDoInitAction.class);
        TAG_CLASSES.add(82, SWFDoABC.class);

        TAG_CLASSES.add( 2, SWFDefineShape.class);
        TAG_CLASSES.add(22, SWFDefineShape2.class);
        TAG_CLASSES.add(32, SWFDefineShape3.class);
        TAG_CLASSES.add(83, SWFDefineShape4.class);

        TAG_CLASSES.add( 6, SWFDefineBits.class);
        TAG_CLASSES.add( 8, SWFJpegTables.class);
        TAG_CLASSES.add(21, SWFDefineBitsJpeg2.class);
        TAG_CLASSES.add(35, SWFDefineBitsJpeg3.class);
        TAG_CLASSES.add(20, SWFDefineBitsLossless.class);
        TAG_CLASSES.add(36, SWFDefineBitsLossless2.class);
        TAG_CLASSES.add(90, SWFDefineBitsJpeg4.class);

        TAG_CLASSES.add(46, SWFDefineMorphShape.class);
        TAG_CLASSES.add(84, SWFDefineMorphShape2.class);

        TAG_CLASSES.add(10, SWFDefineFont.class);
        TAG_CLASSES.add(13, SWFDefineFontInfo.class);
        TAG_CLASSES.add(62, SWFDefineFontInfo2.class);
        TAG_CLASSES.add(48, SWFDefineFont2.class);
        TAG_CLASSES.add(75, SWFDefineFont3.class);
        TAG_CLASSES.add(73, SWFDefineFontAlignZones.class);
        TAG_CLASSES.add(88, SWFDefineFontName.class);
        TAG_CLASSES.add(11, SWFDefineText.class);
        TAG_CLASSES.add(33, SWFDefineText2.class);
        TAG_CLASSES.add(37, SWFDefineEditText.class);
        TAG_CLASSES.add(74, SWFCSMTextSettings.class);
        TAG_CLASSES.add(91, SWFDefineFont4.class);

        TAG_CLASSES.add(14, SWFDefineSound.class);
        TAG_CLASSES.add(15, SWFStartSound.class);
        TAG_CLASSES.add(89, SWFStartSound2.class);
        TAG_CLASSES.add(18, SWFSoundStreamHead.class);
        TAG_CLASSES.add(45, SWFSoundStreamHead2.class);
        TAG_CLASSES.add(19, SWFSoundStreamBlock.class);

        TAG_CLASSES.add( 7, SWFDefineButton.class);
        TAG_CLASSES.add(34, SWFDefineButton2.class);
        TAG_CLASSES.add(23, SWFDefineButtonCXForm.class);
        TAG_CLASSES.add(17, SWFDefineButtonSound.class);

        TAG_CLASSES.add(39, SWFDefineSprite.class);
        TAG_CLASSES.add(60, SWFDefineVideoStream.class);
        TAG_CLASSES.add(61, SWFVideoFrame.class);

        TAG_CLASSES.add(93, SWFEnableTelemetry.class);
        TAG_CLASSES.add(87, SWFDefineBinaryData.class);

    }

    private final Path path;

    private final Sequence tags = new ArraySequence();
    private SWFHeader header;
    private SWFFrameHeader frameHeader;

    public SWFStore(Path path) throws IOException {
        super(SWFFormat.INSTANCE,path);
        this.path = path;
    }

    public SWFHeader getHeader() {
        return header;
    }

    public void setHeader(SWFHeader header) {
        this.header = header;
    }

    public SWFFrameHeader getFrameHeader() {
        return frameHeader;
    }

    public void setFrameHeader(SWFFrameHeader frameHeader) {
        this.frameHeader = frameHeader;
    }

    public Sequence getTags() {
        return tags;
    }
    
    public void read() throws IOException{
        tags.removeAll();
        final ByteInputStream bs = path.createInputStream();
        SWFInputStream ds = new SWFInputStream(bs,NumberEncoding.LITTLE_ENDIAN);

        header = new SWFHeader();
        header.read(ds);
        
        final ByteInputStream in;
        if(header.signature.equals(SWFConstants.SIGNATURE_CWS)){
            //zlib compression
            in = new ZlibInputStream(bs);
        }else if(header.signature.equals(SWFConstants.SIGNATURE_ZWS)){
            //LZMA compression
            throw new IOException("LZMA compression not linked yet");
        }else{
            //no compression
            in = bs;
        }
        ds = new SWFInputStream(in,NumberEncoding.LITTLE_ENDIAN);
        
        frameHeader = new SWFFrameHeader();
        frameHeader.read(ds);

        readTags(ds);
    }

    private void readTags(SWFInputStream ds) throws IOException{
        while(true){
            final int codeAndLength = ds.readUShort();
            final int type = codeAndLength >> 6;
            long length = codeAndLength & 0x3F;
            final boolean longTag = length == 63;
            if(longTag){
                length = ds.readUInt();
            }
            final long offset = ds.getByteOffset();

            //find the tag of this type
            final SWFTag tag = createTag(type);
            tag.offset = offset;
            tag.type = type;
            tag.length = length;
            tag.longTag = longTag;
            tag.read(ds);

            System.out.println(tag);
            tags.add(tag);

            
            if(ds.getByteOffset()!=offset+length){
                throw new IOException("Decoded tag incorrect, expected offset after tag "+(offset+length)+" but was "+ds.getByteOffset());
            }
            
            if(type==0){
                //end tag
                break;
            }
        }
    }

    public void write() throws IOException{
        final ByteOutputStream out = path.createOutputStream();
        DataOutputStream ds = new DataOutputStream(out);
        
        header.write(ds);
        final ByteOutputStream o;
        if(header.signature.equals(SWFConstants.SIGNATURE_CWS)){
            //zlib compression
            o = new ZlibOutputStream(out);
        }else if(header.signature.equals(SWFConstants.SIGNATURE_ZWS)){
            //LZMA compression
            throw new IOException("LZMA compression not linked yet");
        }else{
            //no compression
            o = out;
        }
        ds = new SWFOutputStream(o, NumberEncoding.LITTLE_ENDIAN);

        frameHeader.write(ds);

        for(int i=0,n=tags.getSize();i<n;i++){
            final SWFTag tag = (SWFTag) tags.get(i);

            //write tag header
            if(tag.longTag){
                final int codeAndLength = (tag.type << 6) | 0x3F;
                ds.writeUShort(codeAndLength);
                ds.writeUInt(tag.length);
            }else{
                final int codeAndLength = (tag.type << 6) | (int)tag.length;
                ds.writeUShort(codeAndLength);
            }

            tag.write(ds);
        }
        

    }
    
    private SWFTag createTag(int id) throws IOException{
        Class clazz = (Class) TAG_CLASSES.getValue(id);
        if(clazz==null) clazz = SWFTag.class;
        try {
            return (SWFTag) clazz.newInstance();
        } catch (InstantiationException ex) {
            throw new IOException("Failed to create tag for id "+id,ex);
        } catch (IllegalAccessException ex) {
            throw new IOException("Failed to create tag for id "+id,ex);
        }
    }

}
