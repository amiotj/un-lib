
package science.unlicense.impl.media.swf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.DefaultFormat;

/**
 * Specification :
 * http://www.adobe.com/devnet/swf.html
 *
 * @author Johann Sorel
 */
public class SWFFormat extends DefaultFormat{

    public static final SWFFormat INSTANCE = new SWFFormat();

    public SWFFormat() {
        super(new Chars("swf"),
              new Chars("SWF"),
              new Chars("SWF"),
              new Chars[]{
                  new Chars("application/vnd.adobe.flash-movie"),
              },
              new Chars[]{
                  new Chars("swf")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public SWFStore createStore(Object input) throws IOException {
        return new SWFStore((Path)input);
    }

}
