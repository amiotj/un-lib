
package science.unlicense.impl.media.swf.model;

/**
 *
 * @author Johann Sorel
 */
public class SWFClipEventFlags {

    /** UB[1] */
    public int ClipEventKeyUp;
    /** UB[1] */
    public int ClipEventKeyDown;
    /** UB[1] */
    public int ClipEventMouseUp;
    /** UB[1] */
    public int ClipEventMouseDown;
    /** UB[1] */
    public int ClipEventMouseMove;
    /** UB[1] */
    public int ClipEventUnload;
    /** UB[1] */
    public int ClipEventEnterFrame;
    /** UB[1] */
    public int ClipEventLoad;
    /** UB[1] SWF 6 and later */
    public int ClipEventDragOver;
    /** UB[1] SWF 6 and later */
    public int ClipEventRollOut;
    /** UB[1] SWF 6 and later */
    public int ClipEventRollOver;
    /** UB[1] SWF 6 and later */
    public int ClipEventReleaseOutside;
    /** UB[1] SWF 6 and later */
    public int ClipEventRelease;
    /** UB[1] SWF 6 and later */
    public int ClipEventPress;
    /** UB[1] SWF 6 and later */
    public int ClipEventInitialize;
    /** UB[1] SWF 6 and later */
    public int ClipEventData;
    /** UB[5] SWF 6 and later */
    public int Reserved0;
    /** UB[1] SWF 6 and later */
    public int ClipEventConstruct;
    /** UB[1] SWF 6 and later */
    public int ClipEventKeyPress;
    /** UB[1] SWF 6 and later */
    public int ClipEventDragOut;
    /** UB[8] SWF 6 and later */
    public int Reserved1;


}
