

package science.unlicense.impl.media.swf.filter;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultMatrix;

/**
 *
 * @author Johann Sorel
 */
public class ColorMatrixFilter extends Filter {

    /** FLOAT[20] */
    public MatrixRW matrix;
    
    public void read(DataInputStream ds) throws IOException {
        matrix = DefaultMatrix.create(5, 5);
        matrix.set(0, 0, ds.readFloat());
        matrix.set(0, 1, ds.readFloat());
        matrix.set(0, 2, ds.readFloat());
        matrix.set(0, 3, ds.readFloat());
        matrix.set(0, 4, ds.readFloat());
        
        matrix.set(1, 0, ds.readFloat());
        matrix.set(1, 1, ds.readFloat());
        matrix.set(1, 2, ds.readFloat());
        matrix.set(1, 3, ds.readFloat());
        matrix.set(1, 4, ds.readFloat());
        
        matrix.set(2, 0, ds.readFloat());
        matrix.set(2, 1, ds.readFloat());
        matrix.set(2, 2, ds.readFloat());
        matrix.set(2, 3, ds.readFloat());
        matrix.set(2, 4, ds.readFloat());
        
        matrix.set(3, 0, ds.readFloat());
        matrix.set(3, 1, ds.readFloat());
        matrix.set(3, 2, ds.readFloat());
        matrix.set(3, 3, ds.readFloat());
        matrix.set(3, 4, ds.readFloat());
        
        matrix.set(4, 0, 0);
        matrix.set(4, 1, 0);
        matrix.set(4, 2, 0);
        matrix.set(4, 3, 0);
        matrix.set(4, 4, 1);
    }
    
}
