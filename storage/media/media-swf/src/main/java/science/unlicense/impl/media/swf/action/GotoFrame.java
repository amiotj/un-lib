

package science.unlicense.impl.media.swf.action;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class GotoFrame extends Action {

    /** UI16 */
    public int Frame;
    
    public GotoFrame() {
        super(0x81);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        Frame = ds.readUShort();
    }
    
}
