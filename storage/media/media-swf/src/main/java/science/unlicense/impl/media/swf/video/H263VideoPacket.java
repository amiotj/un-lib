
package science.unlicense.impl.media.swf.video;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Spec : p.207
 * 
 * @author Johann Sorel
 */
public class H263VideoPacket implements VideoPacket{

    public void read(DataInputStream in, int length) throws IOException{
        throw new UnimplementedException("TODO");
    }

    public void write(DataOutputStream out) throws IOException {
        throw new UnimplementedException("TODO");
    }

}
