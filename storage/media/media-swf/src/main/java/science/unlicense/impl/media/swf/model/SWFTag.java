
package science.unlicense.impl.media.swf.model;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.swf.SWFObject;

/**
 *
 * @author Johann Sorel
 */
public class SWFTag extends SWFObject{

    public int type;
    /**
     * byte lenght of the tag not including recordheader
     */
    public long length;
    public boolean longTag;
    /**
     * Offset of the tag in the file after the recordheader
     */
    public long offset;

    /**
     * Read tag content, this do not read the tag header.
     *
     * @param ds
     * @throws IOException
     */
    public void read(DataInputStream ds) throws IOException {
        ds.skipFully(length);
    }

    /**
     * Write tag content, this do not write the tag header.
     * 
     * @param ds
     * @throws IOException
     */
    public void write(DataOutputStream ds) throws IOException {
        throw new IOException("Not implemented yet.");
    }

    protected int remainingBytes(DataInputStream ds){
        if(longTag){
            return (int) (length - (ds.getByteOffset() - offset));
        }else{
            return (int) (length - (ds.getByteOffset() - offset));
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append(':');
        cb.append(CObjects.toChars(length));
        return cb.toChars();
    }

}
