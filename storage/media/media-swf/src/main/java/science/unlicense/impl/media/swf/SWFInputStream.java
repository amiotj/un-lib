
package science.unlicense.impl.media.swf;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Extends DataInputStream to automaticaly realign stream to byte end when
 * reading some primitive types.
 * 
 * 
 * 
 * @author Johann Sorel
 */
public class SWFInputStream extends DataInputStream {
    
    public SWFInputStream(ByteInputStream in, NumberEncoding encoding) {
        super(in, encoding);
    }

    @Override
    public byte readByte() throws IOException {
        skipToByteEnd();
        return super.readByte();
    }

    @Override
    public int readUByte() throws IOException {
        skipToByteEnd();
        return super.readUByte();
    }

    @Override
    public int[] readUByte(int[] buffer, int offset, int length) throws IOException {
        skipToByteEnd();
        return super.readUByte(buffer, offset, length);
    }

    @Override
    public byte[] readFully(byte[] buffer, int offset, int length) throws IOException {
        skipToByteEnd();
        return super.readFully(buffer, offset, length);
    }

    @Override
    public short readShort(NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readShort(encoding);
    }

    @Override
    public short[] readShort(short[] buffer, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readShort(buffer, offset, length, encoding);
    }

    @Override
    public int readUShort(NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readUShort(encoding);
    }

    @Override
    public int[] readUShort(int[] buffer, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readUShort(buffer, offset, length, encoding);
    }
    
    @Override
    public int readInt(NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readInt(encoding);
    }

    @Override
    public int[] readInt(int[] buffer, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readInt(buffer, offset, length, encoding);
    }

    @Override
    public float readFloat(NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readFloat(encoding);
    }

    @Override
    public float[] readFloat(float[] buffer, int offset, int length, NumberEncoding encoding) throws IOException {
        skipToByteEnd();
        return super.readFloat(buffer, offset, length, encoding);
    }
    
}
