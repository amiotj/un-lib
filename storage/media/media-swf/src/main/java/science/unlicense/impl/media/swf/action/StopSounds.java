

package science.unlicense.impl.media.swf.action;

/**
 *
 * @author Johann Sorel
 */
public class StopSounds extends Action {
    
    public StopSounds() {
        super(0x09);
    }
    
}
