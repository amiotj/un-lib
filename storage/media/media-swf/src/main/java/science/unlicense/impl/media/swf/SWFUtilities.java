
package science.unlicense.impl.media.swf;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Float16;
import science.unlicense.api.number.IEEE754;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.math.Maths;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.media.swf.color.ColorTransform;
import science.unlicense.impl.media.swf.model.SWFClipEventFlags;

/**
 *
 * @author Johann Sorel
 */
public final class SWFUtilities {

    public static float readFixed32(DataInputStream ds) throws IOException{
        return ds.readInt() / (float)0x10000;
    }

    public static float readFixed16(DataInputStream ds) throws IOException{
        return ds.readShort() / (float)0x100;
    }

    public static float readHalf(DataInputStream ds) throws IOException{
        final IEEE754 ieee = Float16.decompose(ds.readUShort());
        return Float16.compose(ieee);
    }

    public static int readUB(DataInputStream ds, int nbBits) throws IOException{
        return ds.readBits(nbBits);
    }

    public static float readFB(DataInputStream ds, int nbBits) throws IOException{
        return ds.readSignedBits(nbBits) / (float)0x100;
    }

    public static Chars readChars(DataInputStream ds) throws IOException{
        final ByteSequence buffer = new ByteSequence();
        for(byte b=ds.readByte();b!=0;b=ds.readByte()){
            buffer.put(b);
        }
        return new Chars(buffer.toArrayByte());
    }

    public static Color readRGB(DataInputStream ds) throws IOException{
        return new Color(ds.readUByte(), ds.readUByte(), ds.readUByte());
    }

    public static Color readRGBA(DataInputStream ds) throws IOException{
        return new Color(ds.readUByte(), ds.readUByte(), ds.readUByte(), ds.readUByte());
    }

    public static Color readARGB(DataInputStream ds) throws IOException{
        final int alpha = ds.readUByte();
        return new Color(ds.readUByte(), ds.readUByte(), ds.readUByte(), alpha);
    }

    public static Rectangle readRectangle(DataInputStream ds) throws IOException{
        ds.skipToByteEnd();
        final int nbBits = ds.readBits(5);
        final int xmin = ds.readSignedBits(nbBits);
        final int xmax = ds.readSignedBits(nbBits);
        final int ymin = ds.readSignedBits(nbBits);
        final int ymax = ds.readSignedBits(nbBits);
        ds.skipToByteEnd();
        return new Rectangle(xmin, ymin, xmax-xmin, ymax-ymin);
    }

    public static Matrix3x3 readMatrix(DataInputStream ds) throws IOException{
        ds.skipToByteEnd();

        float scalex = 1;
        float scaley = 1;
        float rotatex = 0;
        float rotatey = 0;
        if(ds.readBits(1)==1){
            final int nbBits = ds.readBits(5);
            scalex = readFB(ds, nbBits);
            scaley = readFB(ds, nbBits);
        }
        if(ds.readBits(1)==1){
            final int nbBits = ds.readBits(5);
            rotatex = readFB(ds, nbBits);
            rotatey = readFB(ds, nbBits);
        }
        final int nbBits = ds.readBits(5);
        float trsx = ds.readSignedBits(nbBits);
        float trsy = ds.readSignedBits(nbBits);

        ds.skipToByteEnd();
        return new Matrix3x3(
                scalex, rotatex, trsx,
               rotatey,  scaley, trsy,
                     0,       0,    1);
    }

    public static ColorTransform readRGBTransform(DataInputStream ds) throws IOException{
        ds.skipToByteEnd();
        int addR = 0;
        int addG = 0;
        int addB = 0;
        int addA = 0;
        int mulR = 1;
        int mulG = 1;
        int mulB = 1;
        int mulA = 1;

        final boolean hasAdd = ds.readBits(1) == 1;
        final boolean hasMul = ds.readBits(1) == 1;
        final int nbBits = ds.readBits(4);
        if(hasMul){
            mulR = ds.readSignedBits(nbBits);
            mulG = ds.readSignedBits(nbBits);
            mulB = ds.readSignedBits(nbBits);
        }
        if(hasAdd){
            addR = ds.readSignedBits(nbBits);
            addG = ds.readSignedBits(nbBits);
            addB = ds.readSignedBits(nbBits);
        }
        ds.skipToByteEnd();
        return new ColorTransform(addR, addG, addB, addA, mulR, mulG, mulB, mulA);
    }

    public static ColorTransform readRGBATransform(DataInputStream ds) throws IOException{
        ds.skipToByteEnd();
        int addR = 0;
        int addG = 0;
        int addB = 0;
        int addA = 0;
        int mulR = 1;
        int mulG = 1;
        int mulB = 1;
        int mulA = 1;

        final boolean hasAdd = ds.readBits(1) == 1;
        final boolean hasMul = ds.readBits(1) == 1;
        final int nbBits = ds.readBits(4);
        if(hasMul){
            mulR = ds.readSignedBits(nbBits);
            mulG = ds.readSignedBits(nbBits);
            mulB = ds.readSignedBits(nbBits);
            mulA = ds.readSignedBits(nbBits);
        }
        if(hasAdd){
            addR = ds.readSignedBits(nbBits);
            addG = ds.readSignedBits(nbBits);
            addB = ds.readSignedBits(nbBits);
            addA = ds.readSignedBits(nbBits);
        }
        ds.skipToByteEnd();
        return new ColorTransform(addR, addG, addB, addA, mulR, mulG, mulB, mulA);
    }

    public static SWFClipEventFlags readClipEvents(DataInputStream ds) throws IOException{
        throw new IOException("TODO");
    }

    public static void writeFixed32(DataOutputStream ds, float val) throws IOException{
        ds.writeInt( (int)(val * 0x10000));
    }

    public static void writeFixed16(DataOutputStream ds, float val) throws IOException{
        ds.writeShort((short)(val * 0x100));
    }

    public static void writeHalf(DataOutputStream ds, float val) throws IOException{
        throw new IOException("TODO");
    }

    public static void writeUI24(DataOutputStream ds, int val) throws IOException{
        throw new IOException("TODO");
    }

    /**
     * EncodedU32
     *
     * @param ds
     * @param val
     * @throws IOException
     */
    public static void writeRUI32(DataOutputStream ds, int val) throws IOException{
        ds.writeVarLengthUInt(val);
    }

    public static void writeSB(DataOutputStream ds, int nbBits, int val) throws IOException{
        throw new IOException("TODO");
    }

    public static void writeUB(DataOutputStream ds, int nbBits, int val) throws IOException{
        ds.writeBits(nbBits, val);
    }

    public static void writeFB(DataOutputStream ds, int nbBits, float val) throws IOException{
        throw new IOException("TODO");
    }

    public static void writeChars(DataOutputStream ds, Chars str) throws IOException{
        ds.write(str.toBytes());
        ds.writeByte((byte)0);
    }

    public static void writeRGB(DataOutputStream ds, Color color) throws IOException{
        ds.writeUByte((int) (color.getRed()*255));
        ds.writeUByte((int) (color.getGreen()*255));
        ds.writeUByte((int) (color.getBlue()*255));
    }

    public static void writeRGBA(DataOutputStream ds, Color color) throws IOException{
        ds.writeUByte((int) (color.getRed()*255));
        ds.writeUByte((int) (color.getGreen()*255));
        ds.writeUByte((int) (color.getBlue()*255));
        ds.writeUByte((int) (color.getAlpha()*255));
    }

    public static void writeARGB(DataOutputStream ds, Color color) throws IOException{
        ds.writeUByte((int) (color.getAlpha()*255));
        ds.writeUByte((int) (color.getRed()*255));
        ds.writeUByte((int) (color.getGreen()*255));
        ds.writeUByte((int) (color.getBlue()*255));
    }

    public static void writeRectangle(DataOutputStream ds, Rectangle rect) throws IOException{
        int xmin = (int) rect.getX();
        int ymin = (int) rect.getY();
        int xmax = (int) rect.getWidth()+xmin;
        int ymax = (int) rect.getHeight()+ymin;

        int nbBit = Int32.getSignificantNumberOfNBits(Math.abs(xmin));
        nbBit = Maths.max(nbBit,Int32.getSignificantNumberOfNBits(Math.abs(ymin)));
        nbBit = Maths.max(nbBit,Int32.getSignificantNumberOfNBits(Math.abs(xmax)));
        nbBit = Maths.max(nbBit,Int32.getSignificantNumberOfNBits(Math.abs(ymax)));
        nbBit++;//for the sign

        ds.skipToByteEnd();
        ds.writeSignedBits(xmin, nbBit);
        ds.writeSignedBits(ymin, nbBit);
        ds.writeSignedBits(xmax, nbBit);
        ds.writeSignedBits(ymax, nbBit);
        ds.skipToByteEnd();
    }

    public static void writeMatrix(DataOutputStream ds, Matrix3x3 matrix) throws IOException{
        throw new IOException("TODO");
    }

    public static void writeRGBTransform(DataOutputStream ds, ColorTransform trs) throws IOException{
        throw new IOException("TODO");
    }

    public static void writeRGBATransform(DataOutputStream ds, ColorTransform trs) throws IOException{
        throw new IOException("TODO");
    }

}
