

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class SWFEnd extends SWFTag {

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
    }
    
}
