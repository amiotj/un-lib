

package science.unlicense.impl.media.swf.action;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.media.swf.SWFUtilities.*;

/**
 *
 * @author Johann Sorel
 */
public class GoToLabel extends Action {

    /** STRING */
    public Chars Label;
    
    public GoToLabel() {
        super(0x8C);
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        Label = readChars(ds);
    }
    
}
