
package science.unlicense.impl.media.swf.shape;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.swf.SWFObject;

/**
 *
 * @author Johann Sorel
 */
public class SWFShapeRecord extends SWFObject {

    public void read(DataInputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
