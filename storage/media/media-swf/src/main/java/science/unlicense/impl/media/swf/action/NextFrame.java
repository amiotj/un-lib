

package science.unlicense.impl.media.swf.action;

/**
 *
 * @author Johann Sorel
 */
public class NextFrame extends Action {
    
    public NextFrame() {
        super(0x04);
    }
    
}
