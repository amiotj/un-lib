

package science.unlicense.impl.media.swf.model;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Matrix3x3;
import static science.unlicense.impl.media.swf.SWFUtilities.*;
import science.unlicense.impl.media.swf.color.ColorTransform;

/**
 *
 * @author Johann Sorel
 */
public class SWFPlaceObject extends SWFTag {

    /** UI16 */
    public int CharacterId;
    /** UI16 */
    public int Depth;
    /** MATRIX */
    public Matrix3x3 matrix;
    /**  (optional) CXFORM */
    public ColorTransform ColorTransform;


    public void read(DataInputStream ds) throws IOException {
        CharacterId = ds.readUShort();
        Depth = ds.readUShort();
        matrix = readMatrix(ds);
        if(remainingBytes(ds)>0){
            ColorTransform = readRGBTransform(ds);
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        super.write(ds);
        ds.writeUShort(CharacterId);
        ds.writeUShort(Depth);
        writeMatrix(ds, matrix);
        if(ColorTransform!=null){
            writeRGBTransform(ds, ColorTransform);
        }
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(this.getClass().getSimpleName()).append('\n');
        cb.append(CObjects.toChars(matrix));
        return cb.toChars();
    }
    
}
