package science.unlicense.impl.media.aac;

import science.unlicense.api.io.IOException;


/**
 * Standard exception, thrown when decoding of an AAC frame fails.
 * The message gives more detailed information about the error.
 * 
 * @author Alexander Simm
 */
public class AACException extends IOException {

    private final boolean eos;

    public AACException(String message) {
        this(message, false);
    }

    public AACException(String message, boolean eos) {
        super(message);
        this.eos = eos;
    }

    public AACException(Throwable cause) {
        super(cause);
        eos = false;
    }

    boolean isEndOfStream() {
        return eos;
    }
}
