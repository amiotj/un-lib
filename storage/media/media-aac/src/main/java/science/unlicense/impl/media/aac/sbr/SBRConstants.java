package science.unlicense.impl.media.aac.sbr;

/**
 * 
 * @author Alexander Simm
 */
public final class SBRConstants {

    public static final int[] startMinTable = {7, 7, 10, 11, 12, 16, 16,
        17, 24, 32, 35, 48};
    public static final int[] offsetIndexTable = {5, 5, 4, 4, 4, 3, 2, 1, 0,
        6, 6, 6};
    public static final int[][] OFFSET = {
        {-8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7}, //16000
        {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13}, //22050
        {-5, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 16}, //24000
        {-6, -4, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 16}, //32000
        {-4, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 16, 20}, //44100-64000
        {-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 16, 20, 24}, //>64000
        {0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 16, 20, 24, 28, 33}
    };
    public static final int EXTENSION_ID_PS = 2;
    public static final int MAX_NTSRHFG = 40; //maximum of number_time_slots * rate + HFGen. 16*2+8
    public static final int MAX_NTSR = 32; //max number_time_slots * rate, ok for DRM and not DRM mode
    public static final int MAX_M = 49; //maximum value for M
    public static final int MAX_L_E = 5; //maximum value for L_E
    public static final int EXT_SBR_DATA = 13;
    public static final int EXT_SBR_DATA_CRC = 14;
    public static final int FIXFIX = 0;
    public static final int FIXVAR = 1;
    public static final int VARFIX = 2;
    public static final int VARVAR = 3;
    public static final int LO_RES = 0;
    public static final int HI_RES = 1;
    public static final int NO_TIME_SLOTS_960 = 15;
    public static final int NO_TIME_SLOTS = 16;
    public static final int RATE = 2;
    public static final int NOISE_FLOOR_OFFSET = 6;
    public static final int T_HFGEN = 8;
    public static final int T_HFADJ = 2;
    
    private SBRConstants(){}
    
}
