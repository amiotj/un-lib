package science.unlicense.impl.media.aac.syntax;

import science.unlicense.impl.media.aac.AACException;
import science.unlicense.impl.media.aac.DecoderConfig;

/**
 * 
 * @author Alexander Simm
 */
class SCE_LFE extends Element {

    private final ICStream ics;

    SCE_LFE(int frameLength) {
        super();
        ics = new ICStream(frameLength);
    }

    void decode(BitStream in, DecoderConfig conf) throws AACException {
        readElementInstanceTag(in);
        ics.decode(in, false, conf);
    }

    public ICStream getICStream() {
        return ics;
    }
}
