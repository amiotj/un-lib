package science.unlicense.impl.media.aac.syntax;

import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;

/**
 * 
 * @author Alexander Simm
 */
public final class Constants {

    public static final Logger LOGGER = Loggers.get();
    
    public static final int MAX_ELEMENTS = 16;
    public static final int BYTE_MASK = 0xFF;
    public static final int MIN_INPUT_SIZE = 768; //6144 bits/channel
    //frame length
    public static final int WINDOW_LEN_LONG = 1024;
    public static final int WINDOW_LEN_SHORT = WINDOW_LEN_LONG/8;
    public static final int WINDOW_SMALL_LEN_LONG = 960;
    public static final int WINDOW_SMALL_LEN_SHORT = WINDOW_SMALL_LEN_LONG/8;
    //element types
    public static final int ELEMENT_SCE = 0;
    public static final int ELEMENT_CPE = 1;
    public static final int ELEMENT_CCE = 2;
    public static final int ELEMENT_LFE = 3;
    public static final int ELEMENT_DSE = 4;
    public static final int ELEMENT_PCE = 5;
    public static final int ELEMENT_FIL = 6;
    public static final int ELEMENT_END = 7;
    //maximum numbers
    public static final int MAX_WINDOW_COUNT = 8;
    public static final int MAX_WINDOW_GROUP_COUNT = MAX_WINDOW_COUNT;
    public static final int MAX_LTP_SFB = 40;
    public static final int MAX_SECTIONS = 120;
    public static final int MAX_MS_MASK = 128;
    public static final float SQRT2 = 1.414213562f;
}
