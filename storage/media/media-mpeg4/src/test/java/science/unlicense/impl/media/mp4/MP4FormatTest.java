

package science.unlicense.impl.media.mp4;

import science.unlicense.impl.media.mp4.MP4Format;
import org.junit.Test;
import science.unlicense.api.media.MediaFormat;
import science.unlicense.api.media.Medias;
import org.junit.Assert;

/**
 * Test MP4 Format declaration.
 *
 * @author Johann Sorel
 */
public class MP4FormatTest {

    @Test
    public void formatTest(){
        final MediaFormat[] formats = Medias.getFormats();
        for(MediaFormat format : formats){
            if(format instanceof MP4Format){
                return;
            }
        }

        Assert.fail("MP4 Format not found.");
    }

}
