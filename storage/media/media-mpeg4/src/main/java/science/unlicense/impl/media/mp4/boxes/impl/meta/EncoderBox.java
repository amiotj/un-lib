package science.unlicense.impl.media.mp4.boxes.impl.meta;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;

/**
 * 
 * @author Alexander Simm
 */
public class EncoderBox extends FullBox {

    private String data;

    public EncoderBox() {
        super("Encoder Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        if(parent.getType()==MP4Constants.BOX_ITUNES_META_LIST) readChildren(in);
        else {
            super.decode(in);
            data = in.readString((int) getLeft(in));
        }
    }

    public String getData() {
        return data;
    }
}
