package science.unlicense.impl.media.mp4.boxes.impl;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;

/**
 * This box contains the degradation priority of each sample. The values are
 * stored in the table, one for each sample. Specifications derived from this
 * define the exact meaning and acceptable range of the priority field.
 * 
 * @author Alexander Simm
 */
public class DegradationPriorityBox extends FullBox {

    private int[] priorities;

    public DegradationPriorityBox() {
        super("Degradation Priority Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        //get number of samples from SampleSizeBox
        final int sampleCount = ((SampleSizeBox) parent.getChild(MP4Constants.BOX_SAMPLE_SIZE)).getSampleCount();

        priorities = new int[sampleCount];
        for(int i = 0; i<sampleCount; i++) {
            priorities[i] = (int) in.readBytes(2);
        }
    }

    /**
     * The priority is integer specifying the degradation priority for each
     * sample.
     * @return the list of priorities
     */
    public int[] getPriorities() {
        return priorities;
    }
}
