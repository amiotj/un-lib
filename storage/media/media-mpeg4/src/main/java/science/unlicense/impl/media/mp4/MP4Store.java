package science.unlicense.impl.media.mp4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.DefaultAudioStreamMeta;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.api.AudioTrack;
import science.unlicense.impl.media.mp4.api.Brand;
import science.unlicense.impl.media.mp4.api.Movie;
import science.unlicense.impl.media.mp4.api.Track;
import science.unlicense.impl.media.mp4.api.VideoTrack;
import science.unlicense.impl.media.mp4.boxes.impl.FileTypeBox;
import science.unlicense.impl.media.mp4.boxes.impl.ProgressiveDownloadInformationBox;
import science.unlicense.impl.media.mp4.boxes.Box;
import science.unlicense.api.path.Path;
import static science.unlicense.api.media.AudioStreamMeta.*;

/**
 * The MP4Store is the central class for the MP4 demultiplexer. It reads the
 * container and gives access to the containing data.
 *
 * The data source can be either an <code>InputStream</code> or a
 * <code>RandomAccessFile</code>. Since the specification does not decree a
 * specific order of the content, the data needed for parsing (the sample
 * tables) may be at the end of the stream. In this case, random access is
 * needed and reading from an <code>InputSteam</code> will cause an exception.
 * Thus, whenever possible, a <code>RandomAccessFile</code> should be used for 
 * local files. Parsing from an <code>InputStream</code> is useful when reading 
 * from a network stream.
 *
 * Each <code>MP4Container</code> can return the used file brand (file format
 * version). Optionally, the following data may be present:
 * <ul>
 * <li>progressive download informations: pairs of download rate and playback
 * delay, see {@link #getDownloadInformationPairs() getDownloadInformationPairs()}</li>
 * <li>a <code>Movie</code></li>
 * </ul>
 *
 * Additionally it gives access to the underlying MP4 boxes, that can be 
 * retrieved by <code>getBoxes()</code>. However, it is not recommended to 
 * access the boxes directly.
 * 
 * @author Alexander Simm
 * @author Johann Sorel
 */
public class MP4Store extends AbstractMediaStore{

    private final MP4InputStream in;
    private final List<Box> boxes = new ArrayList<Box>();
    private Brand major, minor;
    private Brand[] compatible;
    private FileTypeBox ftyp;
    private ProgressiveDownloadInformationBox pdin;
    private Box moov;
    private Movie movie;

    public MP4Store(Path input) throws IOException {
        super(MP4Format.INSTANCE,input);
        this.in = new MP4InputStream(input.createSeekableBuffer(true, false, false));
        readContent();
    }
    
    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        final List<Track> tracks = getMovie().getTracks(AudioTrack.AudioCodec.AAC);
        final MediaStreamMeta[] metas = new MediaStreamMeta[tracks.size()];
        for(int i=0;i<metas.length;i++){
            final Track track = tracks.get(i);
            if(track instanceof AudioTrack){
                final AudioTrack at = (AudioTrack) track;
                final Chars name = new Chars("AAC");
                final int nbChan = at.getChannelCount();
                final int[] channels;
                final int[] bitsPerSample;
                if(nbChan==1){
                    channels = new int[]{CHANNEL_MONO};
                    bitsPerSample = new int[]{at.getSampleSize()};
                }else{
                    //TODO improve this
                    channels = new int[]{CHANNEL_LEFT,CHANNEL_RIGHT};
                    bitsPerSample = new int[]{at.getSampleSize(),at.getSampleSize()};
                }
                final double sampleRate = at.getSampleRate();
                metas[i] = new DefaultAudioStreamMeta(name, channels, bitsPerSample, sampleRate);
                
            }else if(track instanceof VideoTrack){
                metas[i] = null;
            }else{
                throw new IOException("Unexpected track type : "+track);
            }
        }
        
        return metas;
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        final Track[] tracks = getMovie().getTracks(AudioTrack.AudioCodec.AAC).toArray(new Track[0]);
        final MediaStreamMeta[] metas = getStreamsMeta();
        final MP4MediaReader reader = new MP4MediaReader(tracks, metas);
        return reader;
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
    private void readContent() throws IOException {
        //read all boxes
        Box box = null;
        long type;
        boolean moovFound = false;
        while(in.hasLeft()) {
            box = MP4Constants.parseBox(null, in);
            if(boxes.isEmpty()&&box.getType()!=MP4Constants.BOX_FILE_TYPE) throw new MP4Exception("no MP4 signature found");
            boxes.add(box);

            type = box.getType();
            if(type==MP4Constants.BOX_FILE_TYPE) {
                if(ftyp==null) ftyp = (FileTypeBox) box;
            }
            else if(type==MP4Constants.BOX_MOVIE) {
                if(movie==null) moov = box;
                moovFound = true;
            }
            else if(type==MP4Constants.BOX_PROGRESSIVE_DOWNLOAD_INFORMATION) {
                if(pdin==null) pdin = (ProgressiveDownloadInformationBox) box;
            }
            else if(type==MP4Constants.BOX_MEDIA_DATA) {
                if(moovFound) break;
                else if(!in.hasRandomAccess()) throw new MP4Exception("movie box at end of file, need random access");
            }
        }
    }

    public Brand getMajorBrand() {
        if(major==null) major = Brand.forID(ftyp.getMajorBrand());
        return major;
    }

    public Brand getMinorBrand() {
        if(minor==null) minor = Brand.forID(ftyp.getMajorBrand());
        return minor;
    }

    public Brand[] getCompatibleBrands() {
        if(compatible==null) {
            final String[] s = ftyp.getCompatibleBrands();
            compatible = new Brand[s.length];
            for(int i = 0; i<s.length; i++) {
                compatible[i] = Brand.forID(s[i]);
            }
        }
        return compatible;
    }

    //TODO: pdin, movie fragments??
    public Movie getMovie() {
        if(moov==null) return null;
        else if(movie==null) movie = new Movie(moov, in);
        return movie;
    }

    public List<Box> getBoxes() {
        return Collections.unmodifiableList(boxes);
    }
}
