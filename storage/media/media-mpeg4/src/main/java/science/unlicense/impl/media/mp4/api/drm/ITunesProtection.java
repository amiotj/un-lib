package science.unlicense.impl.media.mp4.api.drm;

import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.api.Protection;
import science.unlicense.impl.media.mp4.boxes.impl.drm.FairPlayDataBox;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * 
 * @author Alexander Simm
 */
public class ITunesProtection extends Protection {

    private final String userID; 
    private final String userName; 
    private final String userKey;
    private final byte[] privateKey; 
    private final byte[] initializationVector;

    public ITunesProtection(Box sinf) {
        super(sinf);

        final Box schi = sinf.getChild(MP4Constants.BOX_SCHEME_INFORMATION);
        userID = new String(((FairPlayDataBox) schi.getChild(MP4Constants.BOX_FAIRPLAY_USER_ID)).getData());
        
        //user name box is filled with 0
        final byte[] b = ((FairPlayDataBox) schi.getChild(MP4Constants.BOX_FAIRPLAY_USER_NAME)).getData();
        int i = 0;
        while(b[i]!=0) {
            i++;
        }
        userName = new String(b, 0, i-1);
        
        userKey = new String(((FairPlayDataBox) schi.getChild(MP4Constants.BOX_FAIRPLAY_USER_KEY)).getData());
        privateKey = ((FairPlayDataBox) schi.getChild(MP4Constants.BOX_FAIRPLAY_PRIVATE_KEY)).getData();
        initializationVector = ((FairPlayDataBox) schi.getChild(MP4Constants.BOX_FAIRPLAY_IV)).getData();
    }

    @Override
    public Scheme getScheme() {
        return Scheme.ITUNES_FAIR_PLAY;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserKey() {
        return userKey;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public byte[] getInitializationVector() {
        return initializationVector;
    }
}
