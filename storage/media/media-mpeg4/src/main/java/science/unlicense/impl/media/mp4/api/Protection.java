package science.unlicense.impl.media.mp4.api;

import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.api.Track.Codec;
import science.unlicense.impl.media.mp4.api.drm.ITunesProtection;
import science.unlicense.impl.media.mp4.boxes.impl.OriginalFormatBox;
import science.unlicense.impl.media.mp4.boxes.impl.SchemeTypeBox;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * This class contains information about a DRM system.
 * 
 * @author Alexander Simm
 */
public abstract class Protection {

    public static enum Scheme {

        ITUNES_FAIR_PLAY(1769239918),
        UNKNOWN(-1);
        private long type;

        private Scheme(long type) {
            this.type = type;
        }
    }

    static Protection parse(Box sinf) {
        Protection p = null;
        if(sinf.hasChild(MP4Constants.BOX_SCHEME_TYPE)) {
            final SchemeTypeBox schm = (SchemeTypeBox) sinf.getChild(MP4Constants.BOX_SCHEME_TYPE);
            final long l = schm.getSchemeType();
            if(l==Scheme.ITUNES_FAIR_PLAY.type) p = new ITunesProtection(sinf);
        }

        if(p==null) p = new UnknownProtection(sinf);
        return p;
    }
    private final Codec originalFormat;

    protected Protection(Box sinf) {
        //original format
        final long type = ((OriginalFormatBox) sinf.getChild(MP4Constants.BOX_ORIGINAL_FORMAT)).getOriginalFormat();
        Codec c;
        //TODO: currently it tests for audio and video codec, can do this any other way?
        if(!(c = AudioTrack.AudioCodec.forType(type)).equals(AudioTrack.AudioCodec.UNKNOWN_AUDIO_CODEC)) originalFormat = c;
        else if(!(c = VideoTrack.VideoCodec.forType(type)).equals(VideoTrack.VideoCodec.UNKNOWN_VIDEO_CODEC)) originalFormat = c;
        else originalFormat = null;
    }

    Codec getOriginalFormat() {
        return originalFormat;
    }

    public abstract Scheme getScheme();

    //default implementation for unknown protection schemes
    private static class UnknownProtection extends Protection {

        UnknownProtection(Box sinf) {
            super(sinf);
        }

        @Override
        public Scheme getScheme() {
            return Scheme.UNKNOWN;
        }
    }
}
