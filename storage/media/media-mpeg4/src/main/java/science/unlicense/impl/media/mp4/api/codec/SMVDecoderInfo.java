package science.unlicense.impl.media.mp4.api.codec;

import science.unlicense.impl.media.mp4.api.DecoderInfo;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.SMVSpecificBox;

/**
 * 
 * @author Alexander Simm
 */
public class SMVDecoderInfo extends DecoderInfo {

    private final SMVSpecificBox box;

    public SMVDecoderInfo(CodecSpecificBox box) {
        this.box = (SMVSpecificBox) box;
    }

    public int getDecoderVersion() {
        return box.getDecoderVersion();
    }

    public long getVendor() {
        return box.getVendor();
    }

    public int getFramesPerSample() {
        return box.getFramesPerSample();
    }
}
