package science.unlicense.impl.media.adts;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.aac.ChannelConfiguration;
import science.unlicense.impl.media.aac.SampleFrequency;

/**
 * 
 * @author Alexander Simm
 */
public class ADTSFrame {

    //fixed
    private boolean id; 
    private boolean protectionAbsent; 
    private boolean privateBit; 
    private boolean copy; 
    private boolean home;
    private int layer; 
    private int profile; 
    private int sampleFrequency; 
    private int channelConfiguration;
    //variable
    private boolean copyrightIDBit; 
    private boolean copyrightIDStart;
    private int frameLength; 
    private int adtsBufferFullness;
    private int rawDataBlockCount;
    //error check
    private int[] rawDataBlockPosition;
    private int crcCheck;
    //decoder specific info
    private byte[] info;

    public ADTSFrame(DataInputStream in) throws IOException {
        readHeader(in);

        if(!protectionAbsent) crcCheck = in.readUShort();
        if(rawDataBlockCount==0) {
            //raw_data_block();
        } else {
            int i;
            //header error check
            if(!protectionAbsent) {
                rawDataBlockPosition = new int[rawDataBlockCount];
                for(i = 0; i<rawDataBlockCount; i++) {
                    rawDataBlockPosition[i] = in.readUShort();
                }
                crcCheck = in.readUShort();
            }
            //raw data blocks
            for(i = 0; i<rawDataBlockCount; i++) {
                //raw_data_block();
                if(!protectionAbsent) crcCheck = in.readUShort();
            }
        }
    }

    private void readHeader(DataInputStream in) throws IOException {
        //fixed header:
        //1 bit ID, 2 bits layer, 1 bit protection absent
        int i = in.read();
        id = ((i>>3)&0x1)==1;
        layer = (i>>1)&0x3;
        protectionAbsent = (i&0x1)==1;

        //2 bits profile, 4 bits sample frequency, 1 bit private bit
        i = in.read();
        profile = ((i>>6)&0x3)+1;
        sampleFrequency = (i>>2)&0xF;
        privateBit = ((i>>1)&0x1)==1;

        //3 bits channel configuration, 1 bit copy, 1 bit home
        i = (i<<8)|in.read();
        channelConfiguration = ((i>>6)&0x7);
        copy = ((i>>5)&0x1)==1;
        home = ((i>>4)&0x1)==1;
        //int emphasis = in.readBits(2);

        //variable header:
        //1 bit copyrightIDBit, 1 bit copyrightIDStart, 13 bits frame length,
        //11 bits adtsBufferFullness, 2 bits rawDataBlockCount
        copyrightIDBit = ((i>>3)&0x1)==1;
        copyrightIDStart = ((i>>2)&0x1)==1;
        i = (i<<16)|in.readUShort();
        frameLength = (i>>5)&0x1FFF;
        i = (i<<8)|in.read();
        adtsBufferFullness = (i>>2)&0x7FF;
        rawDataBlockCount = i&0x3;
    }

    public int getFrameLength() {
        return frameLength-(protectionAbsent ? 7 : 9);
    }

    public byte[] createDecoderSpecificInfo() {
        if(info==null) {
            //5 bits profile, 4 bits sample frequency, 4 bits channel configuration
            info = new byte[2];
            info[0] = (byte) (profile<<3);
            info[0] |= (sampleFrequency>>1)&0x7;
            info[1] = (byte) ((sampleFrequency&0x1)<<7);
            info[1] |= (channelConfiguration<<3);
            /*1 bit frame length flag, 1 bit depends on core coder,
            1 bit extension flag (all three currently 0)*/
        }

        return info;
    }

    public int getSampleFrequency() {
        return SampleFrequency.forInt(sampleFrequency).getFrequency();
    }

    public int getChannelCount() {
        return ChannelConfiguration.forInt(channelConfiguration).getChannelCount();
    }
    
}
