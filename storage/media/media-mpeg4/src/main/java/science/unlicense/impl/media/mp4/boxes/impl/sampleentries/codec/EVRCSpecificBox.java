package science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;

/**
 * 
 * @author Alexander Simm
 */
public class EVRCSpecificBox extends CodecSpecificBox {

    private int framesPerSample;

    public EVRCSpecificBox() {
        super("EVCR Specific Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        decodeCommon(in);

        framesPerSample = in.read();
    }

    public int getFramesPerSample() {
        return framesPerSample;
    }
}
