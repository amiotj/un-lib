package science.unlicense.impl.media.mp4.boxes.impl.samplegroupentries;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * 
 * @author Alexander Simm
 */
public abstract class SampleGroupDescriptionEntry extends Box {

    protected SampleGroupDescriptionEntry(String name) {
        super(name);
    }

    @Override
    public abstract void decode(MP4InputStream in) throws IOException;
}
