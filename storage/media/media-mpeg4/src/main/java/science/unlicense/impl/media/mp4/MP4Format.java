
package science.unlicense.impl.media.mp4;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * Mpeg-4 format.
 * This format is defined by ISO 14496-1 to 14496-22.
 * I'm not rich enough to pay more than 3000€ for just this spec !
 * So this reader/writer will likely remain a draft/in-progress state.
 * 
 * Other descriptive docs and tutorials :
 * http://en.wikipedia.org/wiki/ISO_base_media_file_format
 * http://www.ftyps.com
 * http://atomicparsley.sourceforge.net/mpeg-4files.html
 * http://perso.telecom-paristech.fr/~concolat/MPEGFileFormats.pdf
 * 
 * @author Johann Sorel
 */
public class MP4Format extends AbstractMediaFormat{

    public static final MP4Format INSTANCE = new MP4Format();
    
    public MP4Format() {
        super(new Chars("mp4"),
              new Chars("MP4"),
              new Chars("MPEG-4"),
              new Chars[]{
                  new Chars("video/mp4"),
              },
              new Chars[]{
                  new Chars("mp4"),
                  new Chars("m4a"),
                  new Chars("m4p"),
                  new Chars("m4b"),
                  new Chars("m4r"),
                  new Chars("m4v")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public MediaStore createStore(Object input) throws IOException {
        return new MP4Store((Path)input);
    }

}
