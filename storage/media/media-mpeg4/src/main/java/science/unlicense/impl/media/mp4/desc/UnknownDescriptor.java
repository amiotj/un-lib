package science.unlicense.impl.media.mp4.desc;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;

/**
 * This class is used if any unknown Descriptor is found in a stream. All
 * contents of the Descriptor will be skipped.
 *
 * @author Alexander Simm
 */
public class UnknownDescriptor extends Descriptor {

    @Override
    void decode(MP4InputStream in) throws IOException {
        //content will be skipped
    }
}
