package science.unlicense.impl.media.mp4.boxes;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * 
 * @author Alexander Simm
 */
public class FullBox extends Box {

    protected int version;
    protected int flags;

    public FullBox(String name) {
        super(name);
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        version = in.read();
        flags = (int) in.readBytes(3);
    }
}
