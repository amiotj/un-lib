package science.unlicense.impl.media.mp4.api.codec;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.media.mp4.api.DecoderInfo;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.EAC3SpecificBox;

/**
 * 
 * @author Alexander Simm
 */
public class EAC3DecoderInfo extends DecoderInfo {

    private final EAC3SpecificBox box;
    private final IndependentSubstream[] is;

    public EAC3DecoderInfo(CodecSpecificBox box) {
        this.box = (EAC3SpecificBox) box;
        is = new IndependentSubstream[this.box.getIndependentSubstreamCount()];
        for(int i = 0; i<is.length; i++) {
            is[i] = new IndependentSubstream(i);
        }
    }

    public int getDataRate() {
        return box.getDataRate();
    }

    public IndependentSubstream[] getIndependentSubstreams() {
        return is;
    }

    public class IndependentSubstream {

        private final int index;
        private final DependentSubstream[] dependentSubstreams;

        private IndependentSubstream(int index) {
            this.index = index;

            final int loc = box.getDependentSubstreamLocation()[index];
            final Sequence list = new ArraySequence();
            for(int i = 0; i<9; i++) {
                if(((loc>>(8-i))&1)==1) list.add(DependentSubstream.values()[i]);
            }
            dependentSubstreams = new DependentSubstream[list.getSize()];
            Collections.copy(list, dependentSubstreams, 0);
        }

        public int getFscod() {
            return box.getFscods()[index];
        }

        public int getBsid() {
            return box.getBsids()[index];
        }

        public int getBsmod() {
            return box.getBsmods()[index];
        }

        public int getAcmod() {
            return box.getAcmods()[index];
        }

        public boolean isLfeon() {
            return box.getLfeons()[index];
        }

        public DependentSubstream[] getDependentSubstreams() {
            return dependentSubstreams;
        }
    }

    public enum DependentSubstream {

        LC_RC_PAIR,
        LRS_RRS_PAIR,
        CS,
        TS,
        LSD_RSD_PAIR,
        LW_RW_PAIR,
        LVH_RVH_PAIR,
        CVH,
        LFE2
    }
}
