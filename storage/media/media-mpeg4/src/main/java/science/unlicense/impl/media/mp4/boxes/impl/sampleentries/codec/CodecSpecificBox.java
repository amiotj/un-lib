package science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * The <code>CodecSpecificBox</code> can be used instead of an <code>ESDBox</code>
 * in a sample entry. It contains <code>DecoderSpecificInfo</code>s.
 *
 * @author Alexander Simm
 */
public abstract class CodecSpecificBox extends Box {

    private long vendor;
    private int decoderVersion;

    public CodecSpecificBox(String name) {
        super(name);
    }

    protected void decodeCommon(MP4InputStream in) throws IOException {
        vendor = in.readBytes(4);
        decoderVersion = in.read();
    }

    public long getVendor() {
        return vendor;
    }

    public int getDecoderVersion() {
        return decoderVersion;
    }
}
