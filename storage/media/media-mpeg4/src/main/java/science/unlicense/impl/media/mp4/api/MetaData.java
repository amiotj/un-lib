package science.unlicense.impl.media.mp4.api;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.boxes.impl.CopyrightBox;
import science.unlicense.impl.media.mp4.boxes.impl.meta.ID3TagBox;
import science.unlicense.impl.media.mp4.boxes.impl.meta.ITunesMetadataBox;
import science.unlicense.impl.media.mp4.boxes.impl.meta.NeroMetadataTagsBox;
import science.unlicense.impl.media.mp4.boxes.impl.meta.ThreeGPPAlbumBox;
import science.unlicense.impl.media.mp4.boxes.impl.meta.ThreeGPPLocationBox;
import science.unlicense.impl.media.mp4.boxes.impl.meta.ThreeGPPMetadataBox;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * This class contains the metadata for a movie. It parses different metadata
 * types (iTunes tags, ID3).
 * The fields can be read via the <code>get(Field)</code> method using one of
 * the predefined <code>Field</code>s.
 *
 * @author Alexander Simm
 */
public class MetaData {

    public static class Field<T> {

        public static final Field<String> ARTIST = new Field<String>("Artist");
        public static final Field<String> TITLE = new Field<String>("Title");
        public static final Field<String> ALBUM_ARTIST = new Field<String>("Album Artist");
        public static final Field<String> ALBUM = new Field<String>("Album");
        public static final Field<Integer> TRACK_NUMBER = new Field<Integer>("Track Number");
        public static final Field<Integer> TOTAL_TRACKS = new Field<Integer>("Total Tracks");
        public static final Field<Integer> DISK_NUMBER = new Field<Integer>("Disk Number");
        public static final Field<Integer> TOTAL_DISKS = new Field<Integer>("Total disks");
        public static final Field<String> COMPOSER = new Field<String>("Composer");
        public static final Field<String> COMMENTS = new Field<String>("Comments");
        public static final Field<Integer> TEMPO = new Field<Integer>("Tempo");
        public static final Field<Integer> LENGTH_IN_MILLISECONDS = new Field<Integer>("Length in milliseconds");
        public static final Field<Date> RELEASE_DATE = new Field<Date>("Release Date");
        public static final Field<String> GENRE = new Field<String>("Genre");
        public static final Field<String> ENCODER_NAME = new Field<String>("Encoder Name");
        public static final Field<String> ENCODER_TOOL = new Field<String>("Encoder Tool");
        public static final Field<Date> ENCODING_DATE = new Field<Date>("Encoding Date");
        public static final Field<String> COPYRIGHT = new Field<String>("Copyright");
        public static final Field<String> PUBLISHER = new Field<String>("Publisher");
        public static final Field<Boolean> COMPILATION = new Field<Boolean>("Part of compilation");
        public static final Field<List<Artwork>> COVER_ARTWORKS = new Field<List<Artwork>>("Cover Artworks");
        public static final Field<String> GROUPING = new Field<String>("Grouping");
        public static final Field<String> LOCATION = new Field<String>("Location");
        public static final Field<String> LYRICS = new Field<String>("Lyrics");
        public static final Field<Integer> RATING = new Field<Integer>("Rating");
        public static final Field<Integer> PODCAST = new Field<Integer>("Podcast");
        public static final Field<String> PODCAST_URL = new Field<String>("Podcast URL");
        public static final Field<String> CATEGORY = new Field<String>("Category");
        public static final Field<String> KEYWORDS = new Field<String>("Keywords");
        public static final Field<Integer> EPISODE_GLOBAL_UNIQUE_ID = new Field<Integer>("Episode Global Unique ID");
        public static final Field<String> DESCRIPTION = new Field<String>("Description");
        public static final Field<String> TV_SHOW = new Field<String>("TV Show");
        public static final Field<String> TV_NETWORK = new Field<String>("TV Network");
        public static final Field<String> TV_EPISODE = new Field<String>("TV Episode");
        public static final Field<Integer> TV_EPISODE_NUMBER = new Field<Integer>("TV Episode Number");
        public static final Field<Integer> TV_SEASON = new Field<Integer>("TV Season");
        public static final Field<String> INTERNET_RADIO_STATION = new Field<String>("Internet Radio Station");
        public static final Field<String> PURCHASE_DATE = new Field<String>("Purchase Date");
        public static final Field<String> GAPLESS_PLAYBACK = new Field<String>("Gapless Playback");
        public static final Field<Boolean> HD_VIDEO = new Field<Boolean>("HD Video");
        public static final Field<Locale> LANGUAGE = new Field<Locale>("Language");
        //sorting
        public static final Field<String> ARTIST_SORT_TEXT = new Field<String>("Artist Sort Text");
        public static final Field<String> TITLE_SORT_TEXT = new Field<String>("Title Sort Text");
        public static final Field<String> ALBUM_SORT_TEXT = new Field<String>("Album Sort Text");
        private String name;

        private Field(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    private static final String[] STANDARD_GENRES = {
        "undefined",
        //IDv1 standard
        "blues",
        "classic rock",
        "country",
        "dance",
        "disco",
        "funk",
        "grunge",
        "hip hop",
        "jazz",
        "metal",
        "new age",
        "oldies",
        "other",
        "pop",
        "r and b",
        "rap",
        "reggae",
        "rock",
        "techno",
        "industrial",
        "alternative",
        "ska",
        "death metal",
        "pranks",
        "soundtrack",
        "euro techno",
        "ambient",
        "trip hop",
        "vocal",
        "jazz funk",
        "fusion",
        "trance",
        "classical",
        "instrumental",
        "acid",
        "house",
        "game",
        "sound clip",
        "gospel",
        "noise",
        "alternrock",
        "bass",
        "soul",
        "punk",
        "space",
        "meditative",
        "instrumental pop",
        "instrumental rock",
        "ethnic",
        "gothic",
        "darkwave",
        "techno industrial",
        "electronic",
        "pop folk",
        "eurodance",
        "dream",
        "southern rock",
        "comedy",
        "cult",
        "gangsta",
        "top ",
        "christian rap",
        "pop funk",
        "jungle",
        "native american",
        "cabaret",
        "new wave",
        "psychedelic",
        "rave",
        "showtunes",
        "trailer",
        "lo fi",
        "tribal",
        "acid punk",
        "acid jazz",
        "polka",
        "retro",
        "musical",
        "rock and roll",
        //winamp extension
        "hard rock",
        "folk",
        "folk rock",
        "national folk",
        "swing",
        "fast fusion",
        "bebob",
        "latin",
        "revival",
        "celtic",
        "bluegrass",
        "avantgarde",
        "gothic rock",
        "progressive rock",
        "psychedelic rock",
        "symphonic rock",
        "slow rock",
        "big band",
        "chorus",
        "easy listening",
        "acoustic",
        "humour",
        "speech",
        "chanson",
        "opera",
        "chamber music",
        "sonata",
        "symphony",
        "booty bass",
        "primus",
        "porn groove",
        "satire",
        "slow jam",
        "club",
        "tango",
        "samba",
        "folklore",
        "ballad",
        "power ballad",
        "rhythmic soul",
        "freestyle",
        "duet",
        "punk rock",
        "drum solo",
        "a capella",
        "euro house",
        "dance hall"
    };
    private static final String[] NERO_TAGS = {
        "artist", "title", "album", "track", "totaltracks", "year", "genre",
        "disc", "totaldiscs", "url", "copyright", "comment", "lyrics",
        "credits", "rating", "label", "composer", "isrc", "mood", "tempo"
    };
    private Dictionary contents;

    MetaData() {
        contents = new HashDictionary();
    }

    /*moov.udta:
     * -3gpp boxes
     * -meta
     * --ilst
     * --tags
     * --meta (no container!)
     * --tseg
     * ---tshd
     */
    void parse(Box udta, Box meta) {
        //standard boxes
        if(meta.hasChild(MP4Constants.BOX_COPYRIGHT)) {
            CopyrightBox cprt = (CopyrightBox) meta.getChild(MP4Constants.BOX_COPYRIGHT);
            put(Field.LANGUAGE, new Locale(cprt.getLanguageCode()));
            put(Field.COPYRIGHT, cprt.getNotice());
        }
        //3gpp user data
        if(udta!=null) parse3GPPData(udta);
        //id3, TODO: can be present in different languages
        if(meta.hasChild(MP4Constants.BOX_ID3_TAG)) parseID3((ID3TagBox) meta.getChild(MP4Constants.BOX_ID3_TAG));
        //itunes
        if(meta.hasChild(MP4Constants.BOX_ITUNES_META_LIST)) parseITunesMetaData(meta.getChild(MP4Constants.BOX_ITUNES_META_LIST));
        //nero tags
        if(meta.hasChild(MP4Constants.BOX_NERO_METADATA_TAGS)) parseNeroTags((NeroMetadataTagsBox) meta.getChild(MP4Constants.BOX_NERO_METADATA_TAGS));
    }

    //parses specific children of 'udta': 3GPP
    //TODO: handle language codes
    private void parse3GPPData(Box udta) {
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_ALBUM)) {
            ThreeGPPAlbumBox albm = (ThreeGPPAlbumBox) udta.getChild(MP4Constants.BOX_THREE_GPP_ALBUM);
            put(Field.ALBUM, albm.getData());
            put(Field.TRACK_NUMBER, albm.getTrackNumber());
        }
        //if(udta.hasChild(MP4Constants.THREE_GPP_AUTHOR));
        //if(udta.hasChild(MP4Constants.THREE_GPP_CLASSIFICATION));
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_DESCRIPTION)) put(Field.DESCRIPTION, ((ThreeGPPMetadataBox) udta.getChild(MP4Constants.BOX_THREE_GPP_DESCRIPTION)).getData());
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_KEYWORDS)) put(Field.KEYWORDS, ((ThreeGPPMetadataBox) udta.getChild(MP4Constants.BOX_THREE_GPP_KEYWORDS)).getData());
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_LOCATION_INFORMATION)) put(Field.LOCATION, ((ThreeGPPLocationBox) udta.getChild(MP4Constants.BOX_THREE_GPP_LOCATION_INFORMATION)).getPlaceName());
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_PERFORMER)) put(Field.ARTIST, ((ThreeGPPMetadataBox) udta.getChild(MP4Constants.BOX_THREE_GPP_PERFORMER)).getData());
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_RECORDING_YEAR)) {
            final String value = ((ThreeGPPMetadataBox) udta.getChild(MP4Constants.BOX_THREE_GPP_RECORDING_YEAR)).getData();
            try {
                put(Field.RELEASE_DATE, new Date(Integer.parseInt(value)));
            }
            catch(NumberFormatException e) {
                MP4Constants.LOGGER.info(new Chars("unable to parse 3GPP metadata: recording year value: "+ value));
            }
        }
        if(udta.hasChild(MP4Constants.BOX_THREE_GPP_TITLE)) put(Field.TITLE, ((ThreeGPPMetadataBox) udta.getChild(MP4Constants.BOX_THREE_GPP_TITLE)).getData());
    }

    //parses children of 'ilst': iTunes
    private void parseITunesMetaData(Box ilst) {
        final Sequence boxes = ilst.getChildren();
        long l;
        ITunesMetadataBox data;
        for(int k=0,kn=boxes.getSize();k<kn;k++) {
            final Box box = (Box) boxes.get(k);
            l = box.getType();
            data = (ITunesMetadataBox) box.getChild(MP4Constants.BOX_ITUNES_METADATA);

            if(l==MP4Constants.BOX_ARTIST_NAME) put(Field.ARTIST, data.getText());
            else if(l==MP4Constants.BOX_TRACK_NAME) put(Field.TITLE, data.getText());
            else if(l==MP4Constants.BOX_ALBUM_ARTIST_NAME) put(Field.ALBUM_ARTIST, data.getText());
            else if(l==MP4Constants.BOX_ALBUM_NAME) put(Field.ALBUM, data.getText());
            else if(l==MP4Constants.BOX_TRACK_NUMBER) {
                byte[] b = data.getData();
                put(Field.TRACK_NUMBER, new Integer(b[3]));
                put(Field.TOTAL_TRACKS, new Integer(b[5]));
            }
            else if(l==MP4Constants.BOX_DISK_NUMBER) put(Field.DISK_NUMBER, data.getInteger());
            else if(l==MP4Constants.BOX_COMPOSER_NAME) put(Field.COMPOSER, data.getText());
            else if(l==MP4Constants.BOX_COMMENTS) put(Field.COMMENTS, data.getText());
            else if(l==MP4Constants.BOX_TEMPO) put(Field.TEMPO, data.getInteger());
            else if(l==MP4Constants.BOX_RELEASE_DATE) put(Field.RELEASE_DATE, data.getDate());
            else if(l==MP4Constants.BOX_GENRE||l==MP4Constants.BOX_CUSTOM_GENRE) {
                String s = null;
                if(data.getDataType()==ITunesMetadataBox.DataType.UTF8) s = data.getText();
                else {
                    final int i = data.getInteger();
                    if(i>0&&i<STANDARD_GENRES.length) s = STANDARD_GENRES[data.getInteger()];
                }
                if(s!=null) put(Field.GENRE, s);
            }
            else if(l==MP4Constants.BOX_ENCODER_NAME) put(Field.ENCODER_NAME, data.getText());
            else if(l==MP4Constants.BOX_ENCODER_TOOL) put(Field.ENCODER_TOOL, data.getText());
            else if(l==MP4Constants.BOX_COPYRIGHT) put(Field.COPYRIGHT, data.getText());
            else if(l==MP4Constants.BOX_COMPILATION_PART) put(Field.COMPILATION, data.getBoolean());
            else if(l==MP4Constants.BOX_COVER) {
                final Artwork aw = new Artwork(Artwork.Type.forDataType(data.getDataType()), data.getData());
                if(contents.getKeys().contains(Field.COVER_ARTWORKS)) get(Field.COVER_ARTWORKS).add(aw);
                else {
                    final List<Artwork> list = new ArrayList<Artwork>();
                    list.add(aw);
                    put(Field.COVER_ARTWORKS, list);
                }
            }
            else if(l==MP4Constants.BOX_GROUPING) put(Field.GROUPING, data.getText());
            else if(l==MP4Constants.BOX_LYRICS) put(Field.LYRICS, data.getText());
            else if(l==MP4Constants.BOX_RATING) put(Field.RATING, data.getInteger());
            else if(l==MP4Constants.BOX_PODCAST) put(Field.PODCAST, data.getInteger());
            else if(l==MP4Constants.BOX_PODCAST_URL) put(Field.PODCAST_URL, data.getText());
            else if(l==MP4Constants.BOX_CATEGORY) put(Field.CATEGORY, data.getText());
            else if(l==MP4Constants.BOX_KEYWORD) put(Field.KEYWORDS, data.getText());
            else if(l==MP4Constants.BOX_DESCRIPTION) put(Field.DESCRIPTION, data.getText());
            else if(l==MP4Constants.BOX_LONG_DESCRIPTION) put(Field.DESCRIPTION, data.getText());
            else if(l==MP4Constants.BOX_TV_SHOW) put(Field.TV_SHOW, data.getText());
            else if(l==MP4Constants.BOX_TV_NETWORK_NAME) put(Field.TV_NETWORK, data.getText());
            else if(l==MP4Constants.BOX_TV_EPISODE) put(Field.TV_EPISODE, data.getText());
            else if(l==MP4Constants.BOX_TV_EPISODE_NUMBER) put(Field.TV_EPISODE_NUMBER, data.getInteger());
            else if(l==MP4Constants.BOX_TV_SEASON) put(Field.TV_SEASON, data.getInteger());
            else if(l==MP4Constants.BOX_PURCHASE_DATE) put(Field.PURCHASE_DATE, data.getText());
            else if(l==MP4Constants.BOX_GAPLESS_PLAYBACK) put(Field.GAPLESS_PLAYBACK, data.getText());
            else if(l==MP4Constants.BOX_HD_VIDEO) put(Field.HD_VIDEO, data.getBoolean());
            else if(l==MP4Constants.BOX_ARTIST_SORT) put(Field.ARTIST_SORT_TEXT, data.getText());
            else if(l==MP4Constants.BOX_TRACK_SORT) put(Field.TITLE_SORT_TEXT, data.getText());
            else if(l==MP4Constants.BOX_ALBUM_SORT) put(Field.ALBUM_SORT_TEXT, data.getText());
        }
    }

    //parses children of ID3
    private void parseID3(ID3TagBox box) {
        try {
            final DataInputStream in = new DataInputStream(new ArrayInputStream(box.getID3Data()));
            ID3Tag tag = new ID3Tag(in);
            int[] num;
            for(ID3Frame frame : tag.getFrames()) {
                switch(frame.getID()) {
                    case ID3Frame.TITLE:
                        put(Field.TITLE, frame.getEncodedText());
                        break;
                    case ID3Frame.ALBUM_TITLE:
                        put(Field.ALBUM, frame.getEncodedText());
                        break;
                    case ID3Frame.TRACK_NUMBER:
                        num = frame.getNumbers();
                        put(Field.TRACK_NUMBER, num[0]);
                        if(num.length>1) put(Field.TOTAL_TRACKS, num[1]);
                        break;
                    case ID3Frame.ARTIST:
                        put(Field.ARTIST, frame.getEncodedText());
                        break;
                    case ID3Frame.COMPOSER:
                        put(Field.COMPOSER, frame.getEncodedText());
                        break;
                    case ID3Frame.BEATS_PER_MINUTE:
                        put(Field.TEMPO, frame.getNumber());
                        break;
                    case ID3Frame.LENGTH:
                        put(Field.LENGTH_IN_MILLISECONDS, frame.getNumber());
                        break;
                    case ID3Frame.LANGUAGES:
                        put(Field.LANGUAGE, frame.getLocale());
                        break;
                    case ID3Frame.COPYRIGHT_MESSAGE:
                        put(Field.COPYRIGHT, frame.getEncodedText());
                        break;
                    case ID3Frame.PUBLISHER:
                        put(Field.PUBLISHER, frame.getEncodedText());
                        break;
                    case ID3Frame.INTERNET_RADIO_STATION_NAME:
                        put(Field.INTERNET_RADIO_STATION, frame.getEncodedText());
                        break;
                    case ID3Frame.ENCODING_TIME:
                        put(Field.ENCODING_DATE, frame.getDate());
                        break;
                    case ID3Frame.RELEASE_TIME:
                        put(Field.RELEASE_DATE, frame.getDate());
                        break;
                    case ID3Frame.ENCODING_TOOLS_AND_SETTINGS:
                        put(Field.ENCODER_TOOL, frame.getEncodedText());
                        break;
                    case ID3Frame.PERFORMER_SORT_ORDER:
                        put(Field.ARTIST_SORT_TEXT, frame.getEncodedText());
                        break;
                    case ID3Frame.TITLE_SORT_ORDER:
                        put(Field.TITLE_SORT_TEXT, frame.getEncodedText());
                        break;
                    case ID3Frame.ALBUM_SORT_ORDER:
                        put(Field.ALBUM_SORT_TEXT, frame.getEncodedText());
                        break;
                }
            }
        }catch(IOException e) {
            MP4Constants.LOGGER.critical(new Chars("Exception in MetaData.parseID3: "+e.toString()));
        }
    }

    //parses children of 'tags': Nero
    private void parseNeroTags(NeroMetadataTagsBox tags) {
        final Dictionary pairs = tags.getPairs();
        String val;
        final Iterator ite = pairs.getPairs().createIterator();
        while (ite.hasNext()) {
            Pair pair = (Pair) ite.next();
            String key = (String) pair.getValue1();
            val = (String) pair.getValue2();
            try {
                if(key.equals(NERO_TAGS[0])) put(Field.ARTIST, val);
                if(key.equals(NERO_TAGS[1])) put(Field.TITLE, val);
                if(key.equals(NERO_TAGS[2])) put(Field.ALBUM, val);
                if(key.equals(NERO_TAGS[3])) put(Field.TRACK_NUMBER, Integer.parseInt(val));
                if(key.equals(NERO_TAGS[4])) put(Field.TOTAL_TRACKS, Integer.parseInt(val));
                if(key.equals(NERO_TAGS[5])) {
                    Calendar c = Calendar.getInstance();
                    c.set(Calendar.YEAR, Integer.parseInt(val));
                    put(Field.RELEASE_DATE, c.getTime());
                }
                if(key.equals(NERO_TAGS[6])) put(Field.GENRE, val);
                if(key.equals(NERO_TAGS[7])) put(Field.DISK_NUMBER, Integer.parseInt(val));
                if(key.equals(NERO_TAGS[8])) put(Field.TOTAL_DISKS, Integer.parseInt(val));
                if(key.equals(NERO_TAGS[9])); //url
                if(key.equals(NERO_TAGS[10])) put(Field.COPYRIGHT, val);
                if(key.equals(NERO_TAGS[11])) put(Field.COMMENTS, val);
                if(key.equals(NERO_TAGS[12])) put(Field.LYRICS, val);
                if(key.equals(NERO_TAGS[13])); //credits
                if(key.equals(NERO_TAGS[14])) put(Field.RATING, Integer.parseInt(val));
                if(key.equals(NERO_TAGS[15])) put(Field.PUBLISHER, val);
                if(key.equals(NERO_TAGS[16])) put(Field.COMPOSER, val);
                if(key.equals(NERO_TAGS[17])); //isrc
                if(key.equals(NERO_TAGS[18])); //mood
                if(key.equals(NERO_TAGS[19])) put(Field.TEMPO, Integer.parseInt(val));
            }
            catch(NumberFormatException e) {
                MP4Constants.LOGGER.critical(new Chars("Exception in MetaData.parseNeroTags: "+e.toString()));
            }
        }
    }

    private <T> void put(Field<T> field, T value) {
        contents.add(field, value);
    }

    boolean containsMetaData() {
        return !contents.getKeys().isEmpty();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Field<T> field) {
        return (T) contents.getValue(field);
    }

    public Dictionary getAll() {
        return Collections.readOnlyDictionary(contents);
    }
}
