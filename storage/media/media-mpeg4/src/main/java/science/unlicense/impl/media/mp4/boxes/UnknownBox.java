package science.unlicense.impl.media.mp4.boxes;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;

/**
 * Box implementation that is used for unknown types.
 * 
 * @author Alexander Simm
 */
public class UnknownBox extends Box {

    public UnknownBox() {
        super("unknown");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        //no need to read, box will be skipped
    }
}
