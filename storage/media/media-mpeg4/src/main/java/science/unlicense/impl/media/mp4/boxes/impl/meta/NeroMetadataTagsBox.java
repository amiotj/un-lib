package science.unlicense.impl.media.mp4.boxes.impl.meta;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * 
 * @author Alexander Simm
 */
public class NeroMetadataTagsBox extends Box {

    private final Dictionary pairs;

    public NeroMetadataTagsBox() {
        super("Nero Metadata Tags Box");
        pairs = new HashDictionary();
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        in.skipBytes(12); //meta box

        String key, val;
        int len;
        //TODO: what are the other skipped fields for?
        while(getLeft(in)>0&&in.read()==0x80) {
            in.skipBytes(2); //x80 x00 x06/x05
            key = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
            in.skipBytes(4); //0x00 0x01 0x00 0x00 0x00
            len = in.read();
            val = in.readString(len);
            pairs.add(key, val);
        }
    }

    public Dictionary getPairs() {
        return pairs;
    }
}
