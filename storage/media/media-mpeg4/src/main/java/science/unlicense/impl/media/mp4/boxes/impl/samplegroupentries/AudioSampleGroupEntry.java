package science.unlicense.impl.media.mp4.boxes.impl.samplegroupentries;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;

/**
 * 
 * @author Alexander Simm
 */
public class AudioSampleGroupEntry extends SampleGroupDescriptionEntry {

    public AudioSampleGroupEntry() {
        super("Audio Sample Group Entry");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
    }
}
