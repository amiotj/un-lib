package science.unlicense.impl.media.mp4;

import science.unlicense.api.io.IOException;


/**
 * 
 * @author Alexander Simm
 */
public class MP4Exception extends IOException {

    public MP4Exception(String message) {
        super(message);
    }
}
