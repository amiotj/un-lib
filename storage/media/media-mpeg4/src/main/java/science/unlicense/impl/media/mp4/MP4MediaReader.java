

package science.unlicense.impl.media.mp4;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AudioSamples;
import science.unlicense.api.media.AudioStreamMeta;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.impl.media.aac.AACException;
import science.unlicense.impl.media.aac.Decoder;
import science.unlicense.impl.media.aac.SampleBuffer;
import science.unlicense.impl.media.mp4.api.AudioTrack;
import science.unlicense.impl.media.mp4.api.Frame;
import science.unlicense.impl.media.mp4.api.Track;

/**
 *
 * @author Johann Sorel
 */
public class MP4MediaReader implements MediaReadStream{

    private final Track[] tracks;
    private final MediaStreamMeta[] metas;

    private final Object[] buffer;        
    
    //audio track data
    private AudioStreamMeta audioMeta;
    private int bitsPerSample;
    private int[] isamples;
    private AudioSamples samples;
    private byte[] allDatas;
    private DataInputStream audioDs;
    
    //track time
    private double timeStep;
    private long currentStep;
    private long nbSteps;
    private int[] next;
    
    public MP4MediaReader(Track[] tracks, MediaStreamMeta[] metas) throws IOException {
        this.tracks = tracks;
        this.metas = metas;
        this.buffer = new Object[tracks.length];
        
        for(int i=0;i<buffer.length;i++){
            final Track track = tracks[i];
            
            if(track instanceof AudioTrack){
                audioMeta = (AudioStreamMeta) metas[i];
                isamples = new int[audioMeta.getChannels().length];                
                bitsPerSample = audioMeta.getBitsPerSample()[0];
                samples = new AudioSamples(
                        audioMeta.getChannels(), AudioSamples.ENCODING_PCM, bitsPerSample, isamples);
                buffer[i] = samples;
                timeStep = 1d / audioMeta.getSampleRate();
                
                //create AAC decoder
                final Decoder dec = new Decoder(track.getDecoderSpecificInfo());

                //decode
                final ArrayOutputStream out = new ArrayOutputStream();
                final SampleBuffer buf = new SampleBuffer();
                while(track.hasMoreFrames()) {
                    final Frame frame = track.readNextFrame();
                    try {
                        dec.decodeFrame(frame.getData(), buf);
                        out.write(buf.getData());
                    }catch(AACException e) {
                        e.printStackTrace();
                        //since the frames are separate, decoding can continue if one fails
                    }
                }
                allDatas = out.getBuffer().toArrayByte();
                audioDs = new DataInputStream(new ArrayInputStream(allDatas));
            }
        }
        
        nbSteps = allDatas.length*8 / (audioMeta.getBitsPerSample()[0]*isamples.length);
        currentStep = -1;
    }
    
    public MediaPacket next() throws IOException {
        findNext();
        if(next!=null){
            final long time = (long) (currentStep * timeStep);
            next = null;
        }
        return null;
    }
    
    private void findNext() throws IOException{
        if(next!=null) return;
        currentStep++;

        if(currentStep>=nbSteps){
            //nothing left
            return;
        }

        next = isamples;        
        for(int i=0;i<isamples.length;i++){
            isamples[i] = audioDs.readBits(bitsPerSample);
        }
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
