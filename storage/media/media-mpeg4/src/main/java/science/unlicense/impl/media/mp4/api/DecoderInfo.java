package science.unlicense.impl.media.mp4.api;

import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.api.codec.*;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.*;

/**
 * The <code>DecoderInfo</code> object contains the neccessary data to 
 * initialize a decoder. A track either contains a <code>DecoderInfo</code> or a
 * byte-Array called the 'DecoderSpecificInfo', which is e.g. used for AAC.
 * 
 * The <code>DecoderInfo</code> object received from a track is a subclass of 
 * this class depending on the <code>Codec</code>.
 * 
 * <code>
 * AudioTrack track = (AudioTrack) movie.getTrack(AudioCodec.AC3);
 * AC3DecoderInfo info = (AC3DecoderInfo) track.getDecoderInfo();
 * </code>
 * 
 * @author Alexander Simm
 */
public abstract class DecoderInfo {

    static DecoderInfo parse(CodecSpecificBox css) {
        final long l = css.getType();

        final DecoderInfo info;
        if(l==MP4Constants.BOX_H263_SPECIFIC) info = new H263DecoderInfo(css);
        else if(l==MP4Constants.BOX_AMR_SPECIFIC) info = new AMRDecoderInfo(css);
        else if(l==MP4Constants.BOX_EVRC_SPECIFIC) info = new EVRCDecoderInfo(css);
        else if(l==MP4Constants.BOX_QCELP_SPECIFIC) info = new QCELPDecoderInfo(css);
        else if(l==MP4Constants.BOX_SMV_SPECIFIC) info = new SMVDecoderInfo(css);
        else if(l==MP4Constants.BOX_AVC_SPECIFIC) info = new AVCDecoderInfo(css);
        else if(l==MP4Constants.BOX_AC3_SPECIFIC) info = new AC3DecoderInfo(css);
        else if(l==MP4Constants.BOX_EAC3_SPECIFIC) info = new EAC3DecoderInfo(css);
        else info = new UnknownDecoderInfo();
        return info;
    }

    private static class UnknownDecoderInfo extends DecoderInfo {
    }
}
