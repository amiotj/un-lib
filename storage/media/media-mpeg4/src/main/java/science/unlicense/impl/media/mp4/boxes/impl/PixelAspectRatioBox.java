package science.unlicense.impl.media.mp4.boxes.impl;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * 
 * @author Alexander Simm
 */
public class PixelAspectRatioBox extends Box {

    private long hSpacing;
    private long vSpacing;

    public PixelAspectRatioBox() {
        super("Pixel Aspect Ratio Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        hSpacing = in.readBytes(4);
        vSpacing = in.readBytes(4);
    }

    public long getHorizontalSpacing() {
        return hSpacing;
    }

    public long getVerticalSpacing() {
        return vSpacing;
    }
}
