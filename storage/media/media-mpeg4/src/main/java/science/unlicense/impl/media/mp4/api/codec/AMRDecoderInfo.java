package science.unlicense.impl.media.mp4.api.codec;

import science.unlicense.impl.media.mp4.api.DecoderInfo;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.AMRSpecificBox;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.CodecSpecificBox;

/**
 * 
 * @author Alexander Simm
 */
public class AMRDecoderInfo extends DecoderInfo {

    private final AMRSpecificBox box;

    public AMRDecoderInfo(CodecSpecificBox box) {
        this.box = (AMRSpecificBox) box;
    }

    public int getDecoderVersion() {
        return box.getDecoderVersion();
    }

    public long getVendor() {
        return box.getVendor();
    }

    public int getModeSet() {
        return box.getModeSet();
    }

    public int getModeChangePeriod() {
        return box.getModeChangePeriod();
    }

    public int getFramesPerSample() {
        return box.getFramesPerSample();
    }
}
