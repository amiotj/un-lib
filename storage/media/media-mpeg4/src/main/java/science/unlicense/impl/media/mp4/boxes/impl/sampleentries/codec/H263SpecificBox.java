package science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;

/**
 * 
 * @author Alexander Simm
 */
public class H263SpecificBox extends CodecSpecificBox {

    private int level, profile;

    public H263SpecificBox() {
        super("H.263 Specific Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        decodeCommon(in);

        level = in.read();
        profile = in.read();
    }

    public int getLevel() {
        return level;
    }

    public int getProfile() {
        return profile;
    }
}
