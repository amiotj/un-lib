
package science.unlicense.impl.media.mp4.boxes.impl.sampleentries;

import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.boxes.Box;

/**
 * 
 * @author Alexander Simm
 */
public abstract class SampleEntry extends Box {

    private long dataReferenceIndex;

    protected SampleEntry(String name) {
        super(name);
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        in.skipBytes(6); //reserved
        dataReferenceIndex = in.readBytes(2);
    }

    /**
     * The data reference index is an integer that contains the index of the
     * data reference to use to retrieve data associated with samples that use
     * this sample description. Data references are stored in Data Reference
     * Boxes. The index ranges from 1 to the number of data references.
     */
    public long getDataReferenceIndex() {
        return dataReferenceIndex;
    }
}
