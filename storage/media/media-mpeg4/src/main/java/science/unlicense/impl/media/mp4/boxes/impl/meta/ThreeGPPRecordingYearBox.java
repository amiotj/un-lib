package science.unlicense.impl.media.mp4.boxes.impl.meta;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;

/**
 * 
 * @author Alexander Simm
 */
public class ThreeGPPRecordingYearBox extends FullBox {

    private int year;

    public ThreeGPPRecordingYearBox() {
        super("3GPP Recording Year Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        year = (int) in.readBytes(2);
    }

    public int getYear() {
        return year;
    }
}
