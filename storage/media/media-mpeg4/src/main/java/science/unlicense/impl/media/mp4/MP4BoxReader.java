

package science.unlicense.impl.media.mp4;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.media.mp4.boxes.Box;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class MP4BoxReader implements MediaReadStream{
    
    private final Path path;

    public MP4BoxReader(Path path) {
        this.path = path;
    }
    
    public void read() throws IOException{
        
        final ByteInputStream is = path.createInputStream();
        final DataInputStream ds = new DataInputStream(is, NumberEncoding.BIG_ENDIAN);
        
        final byte[] blength = new byte[4];
        
        while(true){
            Box box = read(ds);
            if(box==null) break;
            System.out.println(box);
        }
    }
    
    private Box read(DataInputStream ds) throws IOException{
        final int b = ds.read();
        if(b==-1) return null;
        
        final byte[] blength = new byte[4];
        blength[0] = (byte) b;
        ds.readFully(blength, 1, 3);
        final long length = NumberEncoding.BIG_ENDIAN.readUInt(blength, 0);
        final long type = ds.readUInt();
        final Chars name = new Chars(ds.readFully(new byte[4]));
        
        final Box box = MP4Constants.createBox(type);
        box.setParams(null, length, type, ds.getByteOffset()-8);

        if(MP4Constants.containsSubBoxes(name)){
            long end = length - 8 + ds.getByteOffset();
            while(end != ds.getByteOffset()){
                final Box child = read(ds);
                box.getChildren().add(child);
            }
        }else{
            box.read(ds);
        }

        return box;
    }

    @Override
    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MediaPacket next() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
        
}
