package science.unlicense.impl.media.mp4.api.codec;

import science.unlicense.impl.media.mp4.api.DecoderInfo;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.QCELPSpecificBox;

/**
 * 
 * @author Alexander Simm
 */
public class QCELPDecoderInfo extends DecoderInfo {

    private final QCELPSpecificBox box;

    public QCELPDecoderInfo(CodecSpecificBox box) {
        this.box = (QCELPSpecificBox) box;
    }

    public int getDecoderVersion() {
        return box.getDecoderVersion();
    }

    public long getVendor() {
        return box.getVendor();
    }

    public int getFramesPerSample() {
        return box.getFramesPerSample();
    }
}
