package science.unlicense.impl.media.mp4.boxes.impl.oma;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;

/**
 * 
 * @author Alexander Simm
 */
public class OMAAccessUnitFormatBox extends FullBox {

    private boolean selectiveEncrypted;
    private int keyIndicatorLength, initialVectorLength;

    public OMAAccessUnitFormatBox() {
        super("OMA DRM Access Unit Format Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        //1 bit selective encryption, 7 bits reserved
        selectiveEncrypted = ((in.read()>>7)&1)==1;
        keyIndicatorLength = in.read(); //always zero?
        initialVectorLength = in.read();
    }

    public boolean isSelectiveEncrypted() {
        return selectiveEncrypted;
    }

    public int getKeyIndicatorLength() {
        return keyIndicatorLength;
    }

    public int getInitialVectorLength() {
        return initialVectorLength;
    }
}
