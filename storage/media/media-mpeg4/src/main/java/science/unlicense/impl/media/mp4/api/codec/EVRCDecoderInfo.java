package science.unlicense.impl.media.mp4.api.codec;

import science.unlicense.impl.media.mp4.api.DecoderInfo;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.CodecSpecificBox;
import science.unlicense.impl.media.mp4.boxes.impl.sampleentries.codec.EVRCSpecificBox;

/**
 * 
 * @author Alexander Simm
 */
public class EVRCDecoderInfo extends DecoderInfo {

    private final EVRCSpecificBox box;

    public EVRCDecoderInfo(CodecSpecificBox box) {
        this.box = (EVRCSpecificBox) box;
    }

    public int getDecoderVersion() {
        return box.getDecoderVersion();
    }

    public long getVendor() {
        return box.getVendor();
    }

    public int getFramesPerSample() {
        return box.getFramesPerSample();
    }
}
