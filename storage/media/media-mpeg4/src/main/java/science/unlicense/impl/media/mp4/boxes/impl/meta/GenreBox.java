package science.unlicense.impl.media.mp4.boxes.impl.meta;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;

/**
 * 
 * @author Alexander Simm
 */
public class GenreBox extends FullBox {

    private String languageCode, genre;

    public GenreBox() {
        super("Genre Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        //3gpp or iTunes
        if(parent.getType()==MP4Constants.BOX_USER_DATA) {
            super.decode(in);
            languageCode = MP4Constants.getLanguageCode(in.readBytes(2));
            final byte[] b = in.readTerminated((int) getLeft(in), 0);
            genre = new String(b, MP4Constants.UTF8);
        }
        else readChildren(in);
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public String getGenre() {
        return genre;
    }
}
