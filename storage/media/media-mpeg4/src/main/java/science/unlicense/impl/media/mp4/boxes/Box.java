

package science.unlicense.impl.media.mp4.boxes;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.number.Int64;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.MP4InputStream;

/**
 * MP4 Box (also called Atom).
 * 
 * @author Johann Sorel
 * @author Alexander Simm
 */
public class Box extends CObject {
    
    protected Box parent;
    protected long type;
    
    /**
     * Box size in bytes, including name (4bytes) and size (4bytes)
     */
    protected long size;
    /**
     * Box offset from file start in bytes.
     */
    protected long offset;
        
    protected final Sequence children = new ArraySequence();

    public Box() {
    }
    
    public Box(String name){
        //TODO remove this constructor
    }

    public Box(long type, long size, long offset) {
        this.type = type;
        this.size = size;
        this.offset = offset;
    }
        
    public void setParams(Box parent, long size, long type, long offset) {
        this.size = size;
        this.type = type;
        this.parent = parent;
        this.offset = offset;
    }

    public Box getParent() {
        return parent;
    }

    public void setParent(Box parent) {
        this.parent = parent;
    }
    
    /**
     * Returns the size of this box including its header.
     *
     * @return this box's size
     */
    public long getSize(){
        return size;
    }

    /**
     * Returns the type of this box as a 4CC converted to a long.
     * 
     * @return this box's type
     */
    public long getType(){
        return type;
    }

    /**
     * Returns the offset of this box in the stream/file. This is needed as a
     * seek point for random access.
     *
     * @return this box's offset
     */
    public long getOffset(){
        return offset;
    }
    
    /**
     * Returns the FourCC typeof this box as a human-readable string 
     * (e.g. "moov").
     *
     * @return this box's fourCC
     */
    public Chars getFourCC(){
        return MP4Constants.getFourCC(type);
    }
    
    /**
     * Returns the name of this box as a human-readable string 
     * (e.g. "Track Header Box").
     *
     * @return this box's name
     */
    public Chars getName(){
        return MP4Constants.getFullName(type);
    }

    /**
     * Indicates if this box has children.
     *
     * @return true if this box contains at least one child
     */
    public boolean hasChildren(){
        return !children.isEmpty();
    }

    /**
     * Indicated if the box has a child with the given type.
     *
     * @param type the type of child box to look for
     * @return true if this box contains at least one child with the given type
     */
    public boolean hasChild(long type){
        for(int i=0,n=children.getSize();i<n;i++){
            final Box b = (Box) children.get(i);
            if(b.getType()==type) return true;
        }
        return false;
    }

    /**
     * Returns an ordered and unmodifiable list of all direct children of this
     * box. The list does not contain the children's children.
     *
     * @return this box's children
     */
    public Sequence getChildren(){
        return children;
    }

    /**
     * Returns an ordered and unmodifiable list of all direct children of this
     * box with the specified type. The list does not contain the children's
     * children. If there is no child with the given type, the list will be
     * empty.
     *
     * @param type the type of child boxes to look for
     * @return this box's children with the given type
     */
    public Sequence getChildren(long type){
        final Sequence res = new ArraySequence();
        for(int i=0,n=children.getSize();i<n;i++){
            final Box b = (Box) children.get(i);
            if(b.getType()==type) res.add(b);
        }
        return res;
    }

    /**
     * Returns the child box with the specified type. If this box has no child
     * with the given type, null is returned. To check if there is such a child
     * <code>hasChild(type)</code> can be used.
     * If more than one child exists with the same type, the first one will
     * always be returned. A list of all children with that type can be received
     * via <code>getChildren(type)</code>.
     *
     * @param type the type of child box to look for
     * @return the first child box with the given type, or null if none is found
     */
    public Box getChild(long type){
        for(int i=0,n=children.getSize();i<n;i++){
            final Box b = (Box) children.get(i);
            if(b.getType()==type) return b;
        }
        return null;
    }
    
    
    public void read(DataInputStream ds) throws IOException {
        ds.skipFully(size-8);
    }
    
    public void write(DataOutputStream ds) throws IOException{
        throw new IOException("Not supported yet.");
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(getFourCC());
        cb.append(' ');
        cb.append(Int64.encode(size));
        cb.append(' ');
        cb.append(Int64.encode(offset));
        return Nodes.toChars(cb.toChars(), children);
    }
    
    
    
    protected long getLeft(MP4InputStream in) throws IOException {
        return (offset+size)-in.getOffset();
    }

    /**
     * Decodes the given input stream by reading this box and all of its
     * children (if any).
     * 
     * @param in an input stream
     * @throws IOException if an error occurs while reading
     */
    public void decode(MP4InputStream in) throws IOException {
    }

    public void readChildren(MP4InputStream in) throws IOException {
        while(in.getOffset()<(offset+size)) {
            Box box = MP4Constants.parseBox(this, in);
            children.add(box);
        }
    }

    protected void readChildren(MP4InputStream in, int number) throws IOException {
        for(int i = 0; i<number; i++) {
            Box box = MP4Constants.parseBox(this, in);
            children.add(box);
        }
    }
    
}
