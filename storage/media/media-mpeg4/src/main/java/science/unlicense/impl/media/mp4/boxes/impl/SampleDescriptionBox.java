package science.unlicense.impl.media.mp4.boxes.impl;

import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;
import science.unlicense.api.io.IOException;

/**
 * The sample description table gives detailed information about the coding type
 * used, and any initialization information needed for that coding.
 * 
 * @author Alexander Simm
 */
public class SampleDescriptionBox extends FullBox {

    public SampleDescriptionBox() {
        super("Sample Description Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        final int entryCount = (int) in.readBytes(4);

        readChildren(in, entryCount);
    }
}
