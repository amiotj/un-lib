package science.unlicense.impl.media.mp4.boxes.impl;

import science.unlicense.api.array.Arrays;
import science.unlicense.impl.media.mp4.boxes.FullBox;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;

/**
 * 
 * @author Alexander Simm
 */
public class SampleSizeBox extends FullBox {

    private long sampleCount;
    private long[] sampleSizes;

    public SampleSizeBox() {
        super("Sample Size Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        final boolean compact = type==MP4Constants.BOX_COMPACT_SAMPLE_SIZE;

        final int sampleSize;
        if(compact) {
            in.skipBytes(3);
            sampleSize = in.read();
        }
        else sampleSize = (int) in.readBytes(4);

        sampleCount = in.readBytes(4);
        sampleSizes = new long[(int) sampleCount];

        if(compact) {
            //compact: sampleSize can be 4, 8 or 16 bits
            if(sampleSize==4) {
                int x;
                for(int i = 0; i<sampleCount; i += 2) {
                    x = in.read();
                    sampleSizes[i] = (x>>4)&0xF;
                    sampleSizes[i+1] = x&0xF;
                }
            }
            else readSizes(in, sampleSize/8);
        }
        else if(sampleSize==0) readSizes(in, 4);
        else Arrays.fill(sampleSizes, sampleSize);
    }

    private void readSizes(MP4InputStream in, int len) throws IOException {
        for(int i = 0; i<sampleCount; i++) {
            sampleSizes[i] = in.readBytes(len);
        }
    }

    public int getSampleCount() {
        return (int) sampleCount;
    }

    public long[] getSampleSizes() {
        return sampleSizes;
    }
}
