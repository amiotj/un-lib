package science.unlicense.impl.media.mp4.api;

/**
 * Represents the data type of a <code>Frame</code> or <code>Track</code>.
 * 
 * @author Alexander Simm
 */
public enum Type {

    VIDEO,
    AUDIO;
}
