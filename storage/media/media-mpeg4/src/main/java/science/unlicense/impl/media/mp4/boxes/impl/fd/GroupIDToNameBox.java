package science.unlicense.impl.media.mp4.boxes.impl.fd;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;

/**
 * 
 * @author Alexander Simm
 */
public class GroupIDToNameBox extends FullBox {

    private final Dictionary map;

    public GroupIDToNameBox() {
        super("Group ID To Name Box");
        map = new HashDictionary();
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        final int entryCount = (int) in.readBytes(2);
        long id;
        String name;
        for(int i = 0; i<entryCount; i++) {
            id = in.readBytes(4);
            name = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
            map.add(id, name);
        }
    }

    /**
     * Returns the map that contains the ID-name-pairs for all groups.
     *
     * @return the ID to name map
     */
    public Dictionary getMap() {
        return map;
    }
}
