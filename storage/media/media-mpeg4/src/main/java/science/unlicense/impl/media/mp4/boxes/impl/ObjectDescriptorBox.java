package science.unlicense.impl.media.mp4.boxes.impl;

import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.impl.media.mp4.boxes.FullBox;
import science.unlicense.impl.media.mp4.desc.Descriptor;

/**
 * 
 * @author Alexander Simm
 */
public class ObjectDescriptorBox extends FullBox {

    private Descriptor objectDescriptor;

    public ObjectDescriptorBox() {
        super("Object Descriptor Box");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);
        objectDescriptor = Descriptor.createDescriptor(in);
    }

    public Descriptor getObjectDescriptor() {
        return objectDescriptor;
    }
}
