
package science.unlicense.impl.media.mp4.boxes.impl.sampleentries;

import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;

/**
 * 
 * @author Alexander Simm
 */
abstract class MetadataSampleEntry extends SampleEntry {

    private String contentEncoding;

    MetadataSampleEntry(String name) {
        super(name);
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        contentEncoding = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
    }

    /**
     * A string providing a MIME type which identifies the content encoding of
     * the timed metadata. If not present (an empty string is supplied) the
     * timed metadata is not encoded.
     * An example for this field is 'application/zip'.
     * @return the encoding's MIME-type
     */
    public String getContentEncoding() {
        return contentEncoding;
    }
}
