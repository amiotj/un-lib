
package science.unlicense.impl.media.mp4.boxes.impl.sampleentries;

import science.unlicense.impl.media.mp4.MP4InputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.mp4.MP4Constants;

/**
 * 
 * @author Alexander Simm
 */
public class TextMetadataSampleEntry extends MetadataSampleEntry {

    private String mimeType;

    public TextMetadataSampleEntry() {
        super("Text Metadata Sample Entry");
    }

    @Override
    public void decode(MP4InputStream in) throws IOException {
        super.decode(in);

        mimeType = in.readUTFString((int) getLeft(in), MP4Constants.UTF8);
    }

    /**
     * Provides a MIME type which identifies the content format of the timed
     * metadata. Examples for this field are 'text/html' and 'text/plain'.
     * 
     * @return the content's MIME type
     */
    public String getMimeType() {
        return mimeType;
    }
}
