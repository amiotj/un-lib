

package science.unlicense.impl.media.flac;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public final class FLACUtils {

    private FLACUtils() {
    }
    
    public static int readUnary(DataInputStream ds, int endBitValue) throws IOException{
        int nb=0;
        while(ds.readBits(1)!=endBitValue){
            nb++;
        }
        return nb;
    }
    
    
}
