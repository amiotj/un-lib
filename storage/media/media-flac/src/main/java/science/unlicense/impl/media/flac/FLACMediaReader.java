

package science.unlicense.impl.media.flac;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AudioSamples;
import science.unlicense.api.media.AudioStreamMeta;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.AudioPacket;
import science.unlicense.api.media.DefaultAudioPacket;
import science.unlicense.impl.media.flac.model.Frame;
import science.unlicense.impl.media.flac.model.MetadataStreamInfo;

/**
 *
 * @author Johann Sorel
 */
public class FLACMediaReader implements MediaReadStream {

    private final MetadataStreamInfo info;
    private final Sequence frames;
    private final int nbChannel;
    private final int[] channels;
    
    //decoding state
    private int currentFrameIndex = -1;
    private Frame currentFrame = null;
    private int currentSampleIndex = -1;
        
    private final int[] samples;
    private final AudioSamples as;
    //private final long nbSteps;
    private final double timeStep;
    
    private long currentStep;
    private int[] next;
    

    public FLACMediaReader(MetadataStreamInfo info, Sequence frames, int[] channels) {
        this.info = info;
        this.frames = frames;
        this.nbChannel = channels.length;
        this.channels = channels;
        
        samples = new int[info.nbChannels+1];
        as = new AudioSamples(channels, AudioSamples.ENCODING_PCM, info.bitsPerSample+1, samples);
        timeStep = 1d / info.sampleRate;
        currentStep = -1;
    }
    
    public AudioPacket next() throws IOException {
        findNext();
        
        if(next!=null){
            final long time = (long) (currentStep * timeStep);
            next = null;
            return new DefaultAudioPacket(null, time, (long)timeStep, as);
        }
        
        return null;
    }

    private void findNext() throws IOException{
        if(next!=null) return;
        currentStep++;

        while(true){
            if(currentFrame==null){
                currentFrameIndex++;
                if(currentFrameIndex>=frames.getSize()){
                    //nothing left to read
                    return;
                }
                currentFrame = (Frame) frames.get(currentFrameIndex);
                currentSampleIndex=-1;
            }
            
            currentSampleIndex++;
            if(currentSampleIndex>=currentFrame.subs[0].samples.length){
                //no more data in this frame
                currentFrame=null;
                continue;
            }
            
            for(int i=0;i<nbChannel;i++){
                samples[i] = currentFrame.subs[i].samples[currentSampleIndex];
            }
            
            next = samples;
            
            //adjust values if there is a diff channel
            if(nbChannel==2){
                if(currentFrame.channels[0] == AudioStreamMeta.CHANNEL_DIFF){
                    samples[0] = samples[0] + samples[1];
                }else if(currentFrame.channels[1] == AudioStreamMeta.CHANNEL_DIFF){
                    samples[1] = samples[0] - samples[1];
                }
            }
            
            break;
        }
        
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
