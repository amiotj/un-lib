package science.unlicense.impl.media.flac;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.EOSException;
import science.unlicense.impl.media.flac.model.Frame;
import science.unlicense.impl.media.flac.model.MetadataApplication;
import science.unlicense.impl.media.flac.model.MetadataBlock;
import science.unlicense.impl.media.flac.model.MetadataCueSheet;
import science.unlicense.impl.media.flac.model.MetadataPadding;
import science.unlicense.impl.media.flac.model.MetadataPicture;
import science.unlicense.impl.media.flac.model.MetadataSeekTable;
import science.unlicense.impl.media.flac.model.MetadataStreamInfo;
import science.unlicense.impl.media.flac.model.MetadataVorbisComment;

/**
 *
 * @author Johann Sorel
 */
public class FLACReader extends AbstractReader{

    private static final Dictionary METABLOCKS = new HashDictionary();
    static {
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_STREAMINFO      , MetadataStreamInfo.class);
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_PADDING         , MetadataPadding.class);
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_APPLICATION     , MetadataApplication.class);
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_SEEKTABLE       , MetadataSeekTable.class);
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_VORBIS_COMMENT  , MetadataVorbisComment.class);
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_CUESHEET        , MetadataCueSheet.class);
        METABLOCKS.add(FLACConstants.BLOCK_TYPE_PICTURE         , MetadataPicture.class);
    }
    
    private final Sequence metadataBlocks = new ArraySequence();
    private final Sequence frames = new ArraySequence();

    public Sequence getMetadataBlocks() {
        return metadataBlocks;
    }

    public Sequence getFrames() {
        return frames;
    }
    
    public void read() throws IOException{
        
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
        
        //check signature
        final byte[] signature = ds.readFully(new byte[4]);
        if(!Arrays.equals(FLACConstants.SIGNATURE, signature)){
            throw new IOException("Input is not a FLAC.");
        }
        
        //read metadata blocks
        MetadataStreamInfo info = null;
        boolean lastBlock;
        int blockType;
        int blockLength;
        do{
            lastBlock = ds.readBits(1) == 1;
            blockType = ds.readBits(7);
            blockLength = ds.readUInt24();
            final MetadataBlock block = createMetaBlock(blockType);
            block.isLast = lastBlock;
            block.type = blockType;
            block.length = blockLength;
            block.read(ds);
            metadataBlocks.add(block);
            
            if(info == null){
                //first block is the metadata stream info
                info = (MetadataStreamInfo) block;
            }
        }while(!lastBlock);
        
        //read frames
        int framesync;
        try{
            framesync = ds.readBits(14);
        }catch(EOSException ex){
            //no frames ? is that ok ?
            return;
        }
        while(true){
            if(framesync != FLACConstants.FRAME_SYNC){
                throw new IOException("Was expecting a frame sync code");
            }
            
            final Frame frame = new Frame();
            frame.sync = framesync;
            frame.read(info, ds);
            frames.add(frame);
            
            try{
                framesync = ds.readBits(14);
            }catch(EOSException ex){
                break;
            }
        }
        
    }
    
    private MetadataBlock createMetaBlock(int type) throws IOException{
        final Class c = (Class) METABLOCKS.getValue(type);
        if(c==null){
            throw new IOException("Unknowned Metadata block : "+ type);
        }
        try {
            return (MetadataBlock) c.newInstance();
        } catch (InstantiationException ex) {
            throw new IOException("Failed to instanciate metadata block : "+ c);
        } catch (IllegalAccessException ex) {
            throw new IOException("Failed to instanciate metadata block : "+ c);
        }
    }
    
    private TypedNode readMetadata() throws IOException{
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
        return null;
    }
    
}
