
package science.unlicense.impl.media.flac;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.path.Path;

/**
 * http://xiph.org/flac/format.html
 *
 * @author Johann Sorel
 */
public class FLACFormat extends AbstractMediaFormat{

    public static final FLACFormat INSTANCE = new FLACFormat();
    
    public FLACFormat() {
        super(new Chars("flac"),
              new Chars("FLAC"),
              new Chars("Free Lossless Audio Codec"),
              new Chars[]{
                  new Chars("audio/x-flac")
              },
              new Chars[]{
                new Chars("flac")
              },
              new byte[][]{});
    }

    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaStore createStore(Object input) {
        return new FLACMediaStore((Path) input);
    }

}
