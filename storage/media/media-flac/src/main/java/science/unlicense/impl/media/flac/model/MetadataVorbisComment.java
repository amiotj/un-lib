
package science.unlicense.impl.media.flac.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * Metadata vorbis commment.
 * 
 * @author Johann Sorel
 */
public class MetadataVorbisComment extends MetadataBlock{
    
    public byte[] comment;

    public void read(DataInputStream ds) throws IOException {
        //TODO parse vorbis comment properly
        // http://www.xiph.org/vorbis/doc/v-comment.html
        comment = ds.readFully(new byte[length]);
    }
    
}
