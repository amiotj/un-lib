
package science.unlicense.impl.media.flac.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * Provides informations to move to a target position more quickly.
 * 
 * @author Johann Sorel
 */
public class MetadataSeekTable extends MetadataBlock{

    public SeekPoint[] seekPoints;
    
    public void read(DataInputStream ds) throws IOException {
        seekPoints = new SeekPoint[length/18];
        for(int i=0;i<seekPoints.length;i++){
            seekPoints[i] = new SeekPoint();
            seekPoints[i].read(ds);
        }
    }
    
    public static final class SeekPoint{
        /** 64bits : first sample number in target frame */
        public long sampleNum;
        /** 64bits : offset from first frame to target frame */
        public long offset;
        /** 16bits : number of samples in target frame */
        public int nbSamples;
        
        public void read(DataInputStream ds) throws IOException{
            sampleNum   = ds.readLong();
            offset      = ds.readLong();
            nbSamples   = ds.readUShort();
        }
    }
    
}
