
package science.unlicense.impl.media.flac.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MetadataStreamInfo extends MetadataBlock{
    
    /** 16bits : minimum block size */
    public int minBlockSize;
    /** 16bits : maximum block size */
    public int maxBlockSize;
    /** 24bits : maximum frame sizen in bytes */
    public int minFrameSize;
    /** 24bits : maximum frame size, in bytes */
    public int maxFrameSize;
    /** 20bits : sample rate, in hertz */
    public int sampleRate;
    /** 3bits : number of channels -1 , 1 to 8*/
    public int nbChannels;
    /** 5bits : number of bits per sample -1 */
    public int bitsPerSample;
    /** 36bits : number of samples in stream */
    public int totalNbSample;
    /** 128bits : MD5 checksum for unencoded data */
    public byte[] md5;

    public void read(DataInputStream ds) throws IOException {
        minBlockSize = ds.readUShort();
        maxBlockSize = ds.readUShort();
        minFrameSize = ds.readUInt24();
        maxFrameSize = ds.readUInt24();
        sampleRate   = ds.readBits(20);
        nbChannels   = ds.readBits(3);
        bitsPerSample= ds.readBits(5);
        totalNbSample= ds.readBits(36);
        md5          = ds.readFully(new byte[16]);
    }
    
}
