

package science.unlicense.impl.media.flac;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.AudioStreamMeta;
import science.unlicense.api.media.DefaultAudioStreamMeta;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.flac.model.MetadataBlock;
import science.unlicense.impl.media.flac.model.MetadataStreamInfo;
import science.unlicense.api.path.Path;

/**
 * Flac file store.
 * 
 * @author Johann Sorel
 */
public class FLACMediaStore extends AbstractMediaStore {

    private Sequence frames;
    private Sequence blocks;
    private MetadataStreamInfo info;
    private int[] channels;
    
    
    public FLACMediaStore(Path input) {
        super(FLACFormat.INSTANCE,input);
    }
    
    private void analyze() throws IOException{
        if(frames!=null) return;
        
        //todo, we should split in 2 calls
        //only read the meta blocks and the frames in the media reader only.
        final FLACReader reader = new FLACReader();
        reader.setInput(input);
        reader.read();
        blocks = reader.getMetadataBlocks();
        frames = reader.getFrames();
    }
    
    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        analyze();
        
        MediaStreamMeta[] metas = new MediaStreamMeta[0];
        for(int i=0;i<blocks.getSize();i++){
            final MetadataBlock block = (MetadataBlock) blocks.get(i);
            if(!(block instanceof MetadataStreamInfo)) continue;
            info = (MetadataStreamInfo) block;
            channels = new int[info.nbChannels+1];
            final int[] bitsPerSample = new int[info.nbChannels+1];
            Arrays.fill(bitsPerSample, info.bitsPerSample+1);
            
            //todo improve this
            if(channels.length==1){
                channels[0] = AudioStreamMeta.CHANNEL_CENTER;
            }else if(channels.length==2){
                channels[0] = AudioStreamMeta.CHANNEL_LEFT;
                channels[1] = AudioStreamMeta.CHANNEL_RIGHT;
            }
            
            MediaStreamMeta meta = new DefaultAudioStreamMeta(Chars.EMPTY, channels, bitsPerSample, info.sampleRate);
            metas = new MediaStreamMeta[]{meta};
        }
        
        return metas;
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        getStreamsMeta();
        return new FLACMediaReader(info, frames, channels);
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
