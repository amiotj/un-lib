

package science.unlicense.impl.media.flv;

import science.unlicense.impl.media.flv.FLVFormat;
import org.junit.Test;
import science.unlicense.api.media.MediaFormat;
import science.unlicense.api.media.Medias;
import org.junit.Assert;

/**
 * Test FLV Format declaration.
 *
 * @author Johann Sorel
 */
public class FLVFormatTest {

    @Test
    public void formatTest(){
        final MediaFormat[] formats = Medias.getFormats();
        for(MediaFormat format : formats){
            if(format instanceof FLVFormat){
                return;
            }
        }

        Assert.fail("FLV Format not found.");
    }

}
