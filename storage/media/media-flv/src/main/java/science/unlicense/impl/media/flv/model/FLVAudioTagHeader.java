

package science.unlicense.impl.media.flv.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.flv.FLVConstants;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class FLVAudioTagHeader {

    public int soundFormat;
    public int soundRate;
    public int soundSize;
    public int soundType;
    public int aacPacketType;

    public void read(DataInputStream ds) throws IOException {
        soundFormat = SWFUtilities.readUB(ds, 4);
        soundRate = SWFUtilities.readUB(ds, 2);
        soundSize = SWFUtilities.readUB(ds, 1);
        soundType = SWFUtilities.readUB(ds, 1);

        if(soundFormat == FLVConstants.SOUNDFORMAT_AAC){
            aacPacketType = ds.readUByte();
        }

    }

}
