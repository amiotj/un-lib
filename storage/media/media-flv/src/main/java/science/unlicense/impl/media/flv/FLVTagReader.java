
package science.unlicense.impl.media.flv;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.media.flv.model.FLVTag;

/**
 *
 * @author Johann Sorel
 */
public class FLVTagReader extends AbstractReader {

    private boolean start = true;

    public FLVTag next() throws IOException {
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);

        if(start){
            //skip the first 'previous tag size'
            start = false;
            ds.readInt();
        }
        
        final FLVTag tag = new FLVTag();
        tag.read(ds);

        return tag;
    }

}
