

package science.unlicense.impl.media.flv;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.media.flv.model.FLVHeader;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class FLVStore extends AbstractMediaStore {

    private final Path path;

    private FLVHeader header;

    public FLVStore(Path path) {
        super(FLVFormat.INSTANCE,path);
        this.path = path;
        try {
            read();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void read() throws IOException{
        final ByteInputStream bs = path.createInputStream();
        final DataInputStream ds = new DataInputStream(bs,NumberEncoding.BIG_ENDIAN);

        header = new FLVHeader();
        header.read(ds);
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
