

package science.unlicense.impl.media.flv;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * Specification :
 * http://www.adobe.com/devnet/f4v.html
 * http://download.macromedia.com/f4v/video_file_format_spec_v10_1.pdf
 *
 * @author Johann Sorel
 */
public class FLVFormat extends AbstractMediaFormat{

    public static final FLVFormat INSTANCE = new FLVFormat();
    
    public FLVFormat() {
        super(new Chars("flv"),
              new Chars("FLV"),
              new Chars("FLV"),
              new Chars[]{
                  new Chars("video/x-flv"),
              },
              new Chars[]{
                  new Chars("flv")
              },
              new byte[][]{});
    }

    public boolean supportReading() {
        return true;
    }

    public MediaStore createStore(Object input) throws IOException {
        return new FLVStore((Path)input);
    }

}
