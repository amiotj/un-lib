
package science.unlicense.impl.media.flv;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class FLVConstants {

    /**
     * File signature.
     */
    public static final Chars SIGNATURE = new Chars(new byte[]{'F','L','V'});

    /**
     * Used in FLVHeader to indicate if file contains audio.
     */
    public static final int HEADER_AUDIO_MASK = 0x04;
    /**
     * Used in FLVHeader to indicate if file contains video.
     */
    public static final int HEADER_VIDEO_MASK = 0x01;

    public static final int TAG_TYPE_AUDIO = 8;
    public static final int TAG_TYPE_VIDEO = 9;
    public static final int TAG_TYPE_SCRIPT = 18;

    // AUDIO HEADER CONSTANTS
    public static final int SOUNDFORMAT_LINEAR_PCM_PLATFORM_SPECIFIC = 0;
    public static final int SOUNDFORMAT_ADPCM = 1;
    public static final int SoUNDFORMAT_MP3 = 2;
    public static final int SOUNDFORMAT_LINEAR_PCM_LITTLE_ENDIAN = 3;
    public static final int SOUNDFORMAT_NELLYMOSER_16K_MONO = 4;
    public static final int SOUNDFORMAT_NELLYMOSER_8K_MONO = 5;
    public static final int SOUNDFORMAT_NELLYMOSER = 6;
    public static final int SOUNDFORMAT_G_711_ALAW_LOG_PCM = 7;
    public static final int SOUNDFORMAT_G_711_MULAW_LOG_PCM = 8;
    public static final int SOUNDFORMAT_RESERVED = 9;
    public static final int SOUNDFORMAT_AAC = 10;
    public static final int SOUNDFORMAT_SPEEX = 11;
    public static final int SOUNDFORMAT_MP3_8K = 14;
    public static final int SOUNDFORMAT_DEVICE_SPECIFIC = 15;

    public static final int SOUNDRATE_5_5 = 0;
    public static final int SOUNDRATE_11 = 1;
    public static final int SOUNDRATE_22 = 2;
    public static final int SOUNDRATE_44 = 3;

    public static final int SOUNDSIZE_8BIT = 0;
    public static final int SOUNDSIZE_16BIT = 1;

    public static final int SOUNDTYPE_MONO = 0;
    public static final int SOUNDTYPE_STEREO = 1;

    public static final int SOUNDAAC_SEQHEADER = 0;
    public static final int SOUNDAAC_RAW = 1;

    //VIDEO HEADER CONSTANTS
    public static final int FRAMETYPE_KEYFRAME = 1;
    public static final int FRAMETYPE_INTERFRAME = 2;
    public static final int FRAMETYPE_DISPOSABLE_INTERFRAME = 3;
    public static final int FRAMETYPE_GENERATED_KEYFRAME = 4;
    public static final int FRAMETYPE_VIDEOINFO = 5;

    public static final int CODEC_H263 = 2;
    public static final int CODEC_SCREENVIDEO = 3;
    public static final int CODEC_VP6 = 4;
    public static final int CODEC_VP6_WITHALPHA = 5;
    public static final int CODEC_SCREENVIDEO_V2 = 6;
    public static final int CODEC_AVC = 7;

    public static final int AVCPACKET_SEQHEADER = 0;
    public static final int AVCPACKET_NALU = 1;
    public static final int AVCPACKET_ENDOFSEQ = 2;



    private FLVConstants(){}

}
