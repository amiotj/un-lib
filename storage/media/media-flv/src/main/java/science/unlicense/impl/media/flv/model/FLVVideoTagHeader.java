
package science.unlicense.impl.media.flv.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.media.flv.FLVConstants;
import science.unlicense.impl.media.swf.SWFUtilities;

/**
 *
 * @author Johann Sorel
 */
public class FLVVideoTagHeader {

    public int frameType;
    public int codecId;
    public int avcPacketType;
    public int compositionTime;

    public void read(DataInputStream ds) throws IOException {
        frameType = SWFUtilities.readUB(ds, 4);
        codecId = SWFUtilities.readUB(ds, 4);
        if(codecId == FLVConstants.CODEC_AVC){
            avcPacketType = ds.readUByte();
            compositionTime = ds.readUInt24();
        }
    }

}
