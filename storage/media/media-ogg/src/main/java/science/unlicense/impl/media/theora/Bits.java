
package science.unlicense.impl.media.theora;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class Bits {
    
    private final ArrayOutputStream out = new ArrayOutputStream();
    private final DataOutputStream ds = new DataOutputStream(out,NumberEncoding.LITTLE_ENDIAN);
    private DataInputStream in;
    private int nbBit = 0;
    
    public void append(int bit, int nb) throws IOException {
        for (int i=0;i<nb;i++) {
            ds.writeBit(bit);
        }
        nbBit += nb;
    }
    
    /**
     * Switch the Bits to read mode.
     */
    public Bits readMode() throws IOException {
        ds.flush();
        byte[] data = out.getBuffer().toArrayByte();
//        Arrays.reverse(data, 0, data.length);
        in = new DataInputStream(new ArrayInputStream(data));
//        in.setBitsDirection(DataInputStream.LSB);
//        
//        if((nbBit%8) !=0) {
//            in.readBits(8-(nbBit%8));
//        }
        
        return this;
    }
    
    public int pollHeadBit() throws IOException {
        return in.readBits(1);
    }
    
}
