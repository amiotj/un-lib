
package science.unlicense.impl.media.theora;

/**
 *
 * @author Johann Sorel
 */
public class TheoraMacroBlockLocation {

    public int index;
    public int codedIndex;
    public int superBlock;
    public int x;
    public int y;

    public TheoraBlockLocation[] luma;
    public TheoraBlockLocation[] chromaB;
    public TheoraBlockLocation[] chromaR;

    public TheoraMacroBlockLocation(int PF) {
        luma = new TheoraBlockLocation[4];
        switch (PF) {
            case 0 : // 4:2:0
                chromaB = new TheoraBlockLocation[1];
                chromaR = new TheoraBlockLocation[1];
                break;
            case 2 : // 4:2:2
                chromaB = new TheoraBlockLocation[2];
                chromaR = new TheoraBlockLocation[2];
                break;
            case 3 : // 4:4:4
                chromaB = new TheoraBlockLocation[4];
                chromaR = new TheoraBlockLocation[4];
                break;
        }
    }



}
