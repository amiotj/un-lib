
package science.unlicense.impl.media.theora;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class TheoraCommentHeader {

    public Chars vendor;
    public Chars[] comments;
    
    public void read(DataInputStream ds) throws IOException {
        
        //1. Decode the common header fields according to the procedure described in
        //   Section 6.1. If HEADERTYPE returned by this procedure is not 0x80,
        //   then stop. This packet is not the identification header.
        if (!Arrays.equals(ds.readBytes(6),TheoraConstants.HEADER_SIGNATURE)) {
            throw new IOException("Unvalid signature");
        }
        
        vendor = ds.readBlockZeroTerminatedChars(ds.readInt(NumberEncoding.LITTLE_ENDIAN), CharEncodings.US_ASCII);
        comments = new Chars[ds.readInt(NumberEncoding.LITTLE_ENDIAN)];
        for (int i=0;i<comments.length;i++) {
            comments[i] = ds.readBlockZeroTerminatedChars(ds.readInt(NumberEncoding.LITTLE_ENDIAN), CharEncodings.US_ASCII);
        }
    }
    
}
