
package science.unlicense.impl.media.ogg;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class OGGPage extends CObject {
    
    public OGGPageHeader header;
    public byte[] datas;

    @Override
    public Chars toChars() {
        return new Chars("Page stream:"+header.streamSerialNumber+" size:"+datas.length);
    }
    
}
