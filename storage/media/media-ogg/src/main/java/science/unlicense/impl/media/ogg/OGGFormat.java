
package science.unlicense.impl.media.ogg;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;

/**
 *
 * @author Johann Sorel
 */
public class OGGFormat extends AbstractMediaFormat{

    public OGGFormat() {
        super(new Chars("ogg"),
              new Chars("OGG"),
              new Chars("OGG Vorbis"),
              new Chars[]{
                  new Chars("video/ogg"),
                  new Chars("audio/ogg"),
                  new Chars("application/ogg")
              },
              new Chars[]{
                new Chars("ogg"),
                new Chars("ogv"),
                new Chars("oga"),
                new Chars("ogx"),
                new Chars("spx"),
                new Chars("opus")
              },
              new byte[][]{});
    }

    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaStore createStore(Object input) {
        throw new UnimplementedException("Not supported yet.");
    }

}