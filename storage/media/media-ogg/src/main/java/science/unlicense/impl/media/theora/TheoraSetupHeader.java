
package science.unlicense.impl.media.theora;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Numbers;
import science.unlicense.impl.io.huffman.HuffmanNode;
import science.unlicense.impl.io.huffman.HuffmanTree;

/**
 * Decoding informations.
 * 
 * @author Johann Sorel
 */
public class TheoraSetupHeader {

    public int[] LFLIMS;
    public int[] ACSCALE;
    public int[] DCSCALE;
    public int NBMS;
    public int[][] BMS;
    public int[][] NQRS = new int[2][3];
    public int[][][] QRSIZES = new int[2][3][63];
    public int[][][] QRBMIS = new int[2][3][64];
        
    final HuffmanTree[] HTS = new HuffmanTree[80];
    
    public void read(DataInputStream ds) throws IOException {
        //1. Decode the common header fields according to the procedure described in
        //   Section 6.1. If HEADERTYPE returned by this procedure is not 0x80,
        //   then stop. This packet is not the identification header.
        if (!Arrays.equals(ds.readBytes(6),TheoraConstants.HEADER_SIGNATURE)) {
            throw new IOException("Unvalid signature");
        }
        
        readLoopFilter(ds);
        readQTables(ds);
        readHuffmanTables(ds);
    }
    
    /**
     * Spec : page 52
     */
    private void readLoopFilter(DataInputStream ds) throws IOException {
        final int NBITS = ds.readBits(3);
        LFLIMS = ds.readBits(new int[64], NBITS);
    }
    
    /**
     * Spec : page 54,55
     */
    private void readQTables(DataInputStream ds) throws IOException {
        
        //1. Read a 4-bit unsigned integer. Assign NBITS the value read, plus one.
        int NBITS = ds.readBits(4) + 1;
        
        //2. For each consecutive value of qi from 0 to 63, inclusive:        
        //   (a) Read an NBITS-bit unsigned integer as ACSCALE[qi ].
        ACSCALE = ds.readBits(new int[64], NBITS);
                
        //3. Read a 4-bit unsigned integer. Assign NBITS the value read, plus one.
        NBITS = ds.readBits(4) + 1;
        
        //4. For each consecutive value of qi from 0 to 63, inclusive:
        //   (a) Read an NBITS-bit unsigned integer as DCSCALE[qi ].
        DCSCALE = ds.readBits(new int[64], NBITS);
        
        //5. Read a 9-bit unsigned integer. Assign NBMS the value decoded, plus
        //   one. NBMS MUST be no greater than 384.
        NBMS = ds.readBits(9) + 1;
        if (NBMS>384) throw new IOException("Unvalid NBMS value : "+NBMS);
        
        //6. For each consecutive value of bmi from 0 to (NBMS − 1), inclusive:
        //   (a) For each consecutive value of ci from 0 to 63, inclusive:
        //      i. Read an 8-bit unsigned integer as BMS[bmi ][ci ].
        BMS = new int[NBMS][64];
        for (int bmi=0;bmi<NBMS;bmi++) {
            BMS[bmi] = ds.readUByte(64);
        }
                
        //7. For each consecutive value of qti from 0 to 1, inclusive:
        for (int qti=0; qti<2; qti++) {
            // (a) For each consecutive value of pli from 0 to 2, inclusive:
            for (int pli=0; pli<3; pli++) {
                // i. If qti > 0 or pli > 0, read a 1-bit unsigned integer as NEWQR.
                // ii. Else, assign NEWQR the value one.
                final int NEWQR = (qti>0 || pli>0) ? ds.readBits(1) : 1;
                
                if (NEWQR==0) {
                    // iii. If NEWQR is zero, then we are copying a previously defined set
                    //      of quant ranges. In that case:
                    // A. If qti > 0, read a 1-bit unsigned integer as RPQR.
                    // B. Else, assign RPQR the value zero.
                    final int RPQR = (qti>0) ? ds.readBits(1) : 0;
                    
                    int qtj;
                    int plj;
                    if (RPQR == 1) {
                        // C. If RPQR is one, assign qtj the value (qti − 1) and assign plj
                        //    the value pli . This selects the set of quant ranges defined
                        //    for the same color plane as this one, but for the previous
                        //    quantization type.
                        qtj = qti - 1;
                        plj = pli;
                    } else {
                        // D. Else assign qtj the value (3 ∗ qti + pli − 1)//3 and assign plj
                        //    the value (pli + 2)%3. This selects the most recent set of
                        //    quant ranges defined.
                        qtj = (3 * qti + pli -1) / 3;
                        plj = (pli + 2) % 3;
                    }
                    // E. Assign NQRS[qti ][pli ] the value NQRS[qtj ][plj ].
                    NQRS[qti][pli] = NQRS[qtj][plj];
                    // F. Assign QRSIZES[qti ][pli ] the values in QRSIZES[qtj ][plj ].
                    QRSIZES[qti][pli] = QRSIZES[qtj][plj];
                    // G. Assign QRBMIS[qti ][pli ] the values in QRBMIS[qtj ][plj ].
                    QRBMIS[qti][pli] = QRBMIS[qtj][plj];
                } else {
                    // iv. Else, NEWQR is one, which indicates that we are defining a new
                    //     set of quant ranges. In that case:
                    // A. Assign qri the value zero.
                    int qri = 0;
                    // B. Assign qi the value zero.
                    int qi = 0;
                    // C. Read an ilog(NBMS − 1)-bit unsigned integer as
                    //    QRBMIS[qti ][pli ][qri ]. If this is greater than or equal to
                    //    NBMS, stop. The stream is undecodable.
                    QRBMIS[qti][pli][qri] = ds.readBits(ilog(NBMS-1));
                    if (QRBMIS[qti][pli][qri] >= NBMS) {
                        throw new IOException("Stream is undecodable");
                    }
                    
                    do {
                        // D. Read an ilog(62 − qi )-bit unsigned integer. Assign
                        //    QRSIZES[qti ][pli ][qri ] the value read, plus one.
                        QRSIZES[qti][pli][qri] = ds.readBits(ilog(62-qi)) + 1;                    
                        // E. Assign qi the value qi + QRSIZES[qti ][pli ][qri ].
                        qi = qi + QRSIZES[qti][pli][qri];
                        // F. Assign qri the value qri + 1.
                        qri = qri + 1;
                        // G. Read an ilog(NBMS − 1)-bit unsigned integer as
                        //    QRBMIS[qti ][pli ][qri ].
                        QRBMIS[qti][pli][qri] = ds.readBits(ilog(NBMS-1));
                        // H. If qi is less than 63, go back to step 7(a)ivD.
                    } while (qi < 63);
                    
                    // I. If qi is greater than 63, stop. The stream is undecodable.
                    if (qi > 63) throw new IOException("Stream is undecodable");
                    // J. Assign NQRS[qti ][pli ] the value qri .
                    NQRS[qti][pli] = qri;
                }
            }
        }
        
    }
    
    /**
     * Read huffman tables.
     * 
     * Spec : page 59,60
     */
    private void readHuffmanTables(DataInputStream ds) throws IOException {
        // 1. For each consecutive value of hti from 0 to 79, inclusive:
        for (int hti=0;hti<80;hti++) {
            final Sequence nodes = new ArraySequence();
            readHuffmanNode(ds,nodes,0,0);
            HTS[hti] = new HuffmanTree((HuffmanNode[]) nodes.toArray(HuffmanNode.class));
        }
    }
    
    private void readHuffmanNode(DataInputStream ds, Sequence nodes, int length, int code) throws IOException {

        // (b) If HBITS is longer than 32 bits in length, stop. The stream is unde-codable.            
        // (c) Read a 1-bit unsigned integer as ISLEAF.
        boolean ISLEAF = ds.readBits(1) == 1;
        if (ISLEAF) {
            // (d) If ISLEAF is one:
            // i. If the number of entries in table HTS[hti ] is already 32, stop.
            //    The stream is undecodable.
            // ii. Read a 5-bit unsigned integer as TOKEN.
            // iii. Add the pair (HBITS, TOKEN) to Huffman table HTS[hti ].
            final HuffmanNode node = new HuffmanNode(length,code);
            node.value = ds.readBits(5);
            nodes.add(node);
        } else {
            // (e) Otherwise:
            // i. Add a ‘0’ to the end of HBITS.
            // ii. Decode the ‘0’ sub-tree using this procedure, starting from step 1b.
            readHuffmanNode(ds,nodes,length+1, code);
            // iii. Remove the ‘0’ from the end of HBITS and add a ‘1’ to the end of HBITS.
            // iv. Decode the ‘1’ sub-tree using this procedure, starting from step 1b.
            readHuffmanNode(ds,nodes,length+1, code | (1<<length));
            // v. Remove the ‘1’ from the end of HBITS.
        }
    }
    
    /**
     * The minimum number of bits required to store a positive integer a in
     * two’s complement notation, or 0 for a non-positive integer a.
     * 
     * Spec: page 12
     */
    static int ilog(int a) {
        if (a<=0) return 0;
        int nbBits = 0;
        for (;a!=0; a=(a>>1), nbBits++) {}
        return nbBits;
    }
    
}
