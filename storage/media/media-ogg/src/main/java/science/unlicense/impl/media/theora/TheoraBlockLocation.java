
package science.unlicense.impl.media.theora;

/**
 *
 * @author Johann Sorel
 */
public class TheoraBlockLocation {
    
    public static enum Type{
        Y(0),
        CB(1),
        CR(2);

        public final int planeIndex;
        
        private Type(int planeIndex) {
            this.planeIndex = planeIndex;
        }
    }
    
    public int rasterIndex;
    public int codedIndex;
    public Type type;
    public int superBlock;
    public TheoraMacroBlockLocation macroBlock;
    //in frame position X/Y, in block units
    public int x;
    public int y;
    
    //left, right, top, down blocks
    public TheoraBlockLocation left;
    public TheoraBlockLocation right;
    public TheoraBlockLocation top;
    public TheoraBlockLocation down;
    
}
