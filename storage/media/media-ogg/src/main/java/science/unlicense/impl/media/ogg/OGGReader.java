package science.unlicense.impl.media.ogg;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.number.NumberEncoding;

/**
 * http://xiph.org/ogg/doc/framing.html
 *
 * @author Johann Sorel
 */
public class OGGReader extends AbstractReader{

    /**
     * Read next OGG page.
     * 
     * @return next page, or null if none remaining
     * @throws IOException 
     */
    public OGGPage read() throws IOException{

        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);

        
        final byte[] signature;
        try {
            signature = ds.readFully(new byte[4]);
        } catch(EOSException ex) {
            return null;
        }
        if(!Arrays.equals(OGGConstants.SIGNATURE,signature)){
            throw new IOException("Not an OGG stream");
        }
        
        final OGGPageHeader header = new OGGPageHeader();
        header.read(ds);
        
        final int dataSize = Maths.sum(header.segment_table);
        
        final OGGPage page = new OGGPage();
        page.header = header;
        page.datas = ds.readBytes(dataSize);
        
        return page;
    }

}