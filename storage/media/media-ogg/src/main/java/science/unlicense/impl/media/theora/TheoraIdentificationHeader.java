
package science.unlicense.impl.media.theora;

import science.unlicense.api.Sorter;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;

/**
 * Video informations.
 *
 * @author Johann Sorel
 */
public class TheoraIdentificationHeader {

    private static final int[][][][] HILBER2_XY = {
        {//1 row
            {{0,0}                  },
            {{0,0},            {1,0}}
        },
        {//2 row
            {{0,0},{0,1}},
            {{0,0},{0,1},{1,1},{1,0}}
        }
    };

    private static final int[][][][] HILBER4_XY = {
        {//1 row
            {{0,0},                                                                                           },
            {{0,0},{1,0},                                                                                     },
            {{0,0},{1,0},                                                                          {2,0}      },
            {{0,0},{1,0},                                                                          {2,0},{3,0}}
        },
        {//2 row
            {{0,0},            {0,1},                                                                          },
            {{0,0},{1,0},{1,1},{0,1},                                                                          },
            {{0,0},{1,0},{1,1},{0,1},                                                         {2,1},{2,0}      },
            {{0,0},{1,0},{1,1},{0,1},                                                   {3,1},{2,1},{2,0},{3,0}}
        },
        {//3 row
            {{0,0},            {0,1}, {0,2},                                                                   },
            {{0,0},{1,0},{1,1},{0,1}, {0,2},            {1,2}                                                  },
            {{0,0},{1,0},{1,1},{0,1}, {0,2},            {1,2}, {2,2},                         {2,1},{2,0}      },
            {{0,0},{1,0},{1,1},{0,1}, {0,2},            {1,2}, {2,2},            {3,2}, {3,1},{2,1},{2,0},{3,0}}
        },
        {//4 row
            {{0,0},            {0,1}, {0,2},{0,3}                                                              },
            {{0,0},{1,0},{1,1},{0,1}, {0,2},{0,3},{1,3},{1,2}                                                  },
            {{0,0},{1,0},{1,1},{0,1}, {0,2},{0,3},{1,3},{1,2}, {2,2},{2,3},                   {2,1},{2,0}      },
            {{0,0},{1,0},{1,1},{0,1}, {0,2},{0,3},{1,3},{1,2}, {2,2},{2,3},{3,3},{3,2}, {3,1},{2,1},{2,0},{3,0}}
        }
    };

    private static final Sorter RASTERORDER_SORTER = new Sorter() {
        @Override
        public int sort(Object first, Object second) {
            final TheoraBlockLocation b1 = (TheoraBlockLocation) first;
            final TheoraBlockLocation b2 = (TheoraBlockLocation) second;
            
            int d = b1.type.compareTo(b2.type);
            if (d!=0) return d;
            d = Integer.compare(b1.y,b2.y);
            if (d!=0) return d;
            return Integer.compare(b1.x,b2.x);
        }
    };
    
    /**
     * Major version number.
     */
    public int VMAJ;
    /**
     * Minor version number.
     */
    public int VMIN;
    /**
     * Revision version number.
     */
    public int VREV;
    /**
     * Frame width in macro blocks.
     */
    public int FMBW;
    /**
     * Frame height in macro blocks.
     */
    public int FMBH;
    /**
     * Number of super blocks in a frame.
     */
    public int NSBS;
    /**
     * Number of blocks in a frame.
     */
    public int NBS;
    /**
     * Number of macro blocks in a frame.
     */
    public int NMBS;
    /**
     * Image width in pixels.
     */
    public int PICW;
    /**
     * Image height in pixels.
     */
    public int PICH;
    /**
     * Image start offset X in pixels.
     */
    public int PICX;
    /**
     * Image start offset Y in pixels.
     */
    public int PICY;
    /**
     * Video frame rate numerator.
     */
    public int FRN;
    /**
     * Video frame rate denominator.
     */
    public int FRD;
    /**
     * Video aspect ratio numerator.
     */
    public int PARN;
    /**
     * Video aspect ratio denominator.
     */
    public int PARD;
    /**
     * Video color space.
     */
    public int CS;
    /**
     * Video pixel format.
     */
    public int PF;
    /**
     * Video bits per seconds (average value).
     */
    public int NOMBR;
    /**
     * Video quality hint.
     */
    public int QUAL;
    /**
     * The amount to shift the key frame number by in the granule position.
     */
    public int KFGSHIFT;

    //computed values
    
    /**
     * Block locations in coded-order index.
     */
    public TheoraBlockLocation[] blockCodedOrder;
    /**
     * Block locations in raster-order index.
     */
    public TheoraBlockLocation[] blockRasterOrder;

    /**
     * Macroblock locations in coded-order index.
     */
    public TheoraMacroBlockLocation[] macroblockCodedOrder;

    public void read(DataInputStream ds) throws IOException {

        //1. Decode the common header fields according to the procedure described in
        //   Section 6.1. If HEADERTYPE returned by this procedure is not 0x80,
        //   then stop. This packet is not the identification header.
        if (!Arrays.equals(ds.readBytes(6),TheoraConstants.HEADER_SIGNATURE)) {
            throw new IOException("Unvalid signature");
        }

        //2. Read an 8-bit unsigned integer as VMAJ. If VMAJ is not 3, then stop.
        //   This stream is not decodable according to this specification.
        //3. Read an 8-bit unsigned integer as VMIN. If VMIN is not 2, then stop.
        //   This stream is not decodable according to this specification.
        //4. Read an 8-bit unsigned integer as VREV. If VREV is greater than 1,
        //   then this stream may contain optional features or interpretational changes
        //   documented in a future version of this specification. Regardless of the
        //   value of VREV, the stream is decodable according to this specification.
        VMAJ = ds.readUByte();
        VMIN = ds.readUByte();
        VREV = ds.readUByte();
        //5. Read a 16-bit unsigned integer as FMBW. This MUST be greater than
        //   zero. This specifies the width of the coded frame in macro blocks. The
        //   actual width of the frame in pixels is FMBW ∗ 16.
        FMBW = ds.readUShort();
        //6. Read a 16-bit unsigned integer as FMBH. This MUST be greater than
        //   zero. This specifies the height of the coded frame in macro blocks. The
        //   actual height of the frame in pixels is FMBH ∗ 16.
        FMBH = ds.readUShort();
        //7. Read a 24-bit unsigned integer as PICW. This MUST be no greater
        //   than (FMBW ∗ 16). Note that 24 bits are read, even though only 20 bits
        //   are sufficient to specify any value of the picture width. This is done to
        //   preserve octet alignment in this header, to allow for a simplified parser
        //   implementation.
        PICW = ds.readUInt24();
        //8. Read a 24-bit unsigned integer as PICH. This MUST be no greater
        //   than (FMBH ∗ 16). Together with PICW, this specifies the size of the
        //   displayable picture region within the coded frame. See Figure 2.1. Again,
        //   24 bits are read instead of 20.
        PICH = ds.readUInt24();
        //9. Read an 8-bit unsigned integer as PICX. This MUST be no greater than
        //   (FMBW ∗ 16 − PICX).
        PICX = ds.readUByte();
        //10. Read an 8-bit unsigned integer as PICY. This MUST be no greater than
        //   (FMBH ∗ 16 − PICY). Together with PICX, this specifies the location
        //   of the lower-left corner of the displayable picture region. See Figure 2.1.
        PICY = ds.readUByte();
        //11. Read a 32-bit unsigned integer as FRN. This MUST be greater than
        //   zero.
        FRN = (int) ds.readUInt();
        //12. Read a 32-bit unsigned integer as FRD. This MUST be greater than
        //   zero. Theora is a fixed-frame rate video codec. Frames are sampled at
        //   the constant rate of FRN
        //   FRD frames per second. The presentation time of
        //   the first frame is at zero seconds. No mechanism is provided to specify a
        //   non-zero offset for the initial frame.
        FRD = (int) ds.readUInt();
        //13. Read a 24-bit unsigned integer as PARN.
        PARN = ds.readUInt24();
        //14. Read a 24-bit unsigned integer as PARD. Together with PARN, these
        //   specify the aspect ratio of the pixels within a frame, defined as the ratio
        //   of the physical width of a pixel to its physical height. This is given by
        //   the ratio PARN : PARD. If either of these fields are zero, this indicates
        //   that pixel aspect ratio information was not available to the encoder. In
        //   this case it MAY be specified by the application via an external means,
        //   or a default value of 1 : 1 MAY be used.
        PARD = ds.readUInt24();
        //15. Read an 8-bit unsigned integer as CS. This is a value from an enumerated
        //   list of the available color spaces, given in Table 6.3. The ‘Undefined’ value
        //   indicates that color space information was not available to the encoder. It
        //   MAY be specified by the application via an external means. If a reserved
        //   value is given, a decoder MAY refuse to decode the stream.
        CS = ds.readUByte();
        //16. Read a 24-bit unsigned integer as NOMBR signifying a rate in bits per
        //   second. Rates equal to or greater than 2 24 − 1 bits per second are repre-
        //   sented as 2 24 − 1. The NOMBR field is used only as a hint. For pure
        //   VBR streams, this value may be considerably off. The field MAY be set
        //   to zero to indicate that the encoder did not care to speculate.
        NOMBR = ds.readUInt24();
        //17. Read a 6-bit unsigned integer as QUAL. This value is used to provide
        //   a hint as to the relative quality of the stream when compared to others
        //   produced by the same encoder. Larger values indicate higher quality. This
        //   can be used, for example, to select among several streams containing the
        //   same material encoded with different settings.
        QUAL = ds.readBits(6);
        //18. Read a 5-bit unsigned integer as KFGSHIFT. The KFGSHIFT is
        //   used to partition the granule position associated with each packet into two
        //   different parts. The frame number of the last key frame, starting from zero,
        //   is stored in the upper 64−KFGSHIFT bits, while the lower KFGSHIFT
        //   bits contain the number of frames since the last keyframe. Complete
        //   details on the granule position mapping are specified in Section REF.
        KFGSHIFT = ds.readBits(5);
        //19. Read a 2-bit unsigned integer as PF. The PF field contains a value from
        //   an enumerated list of the available pixel formats, given in Table 6.4. If the
        //   reserved value 1 is given, stop. This stream is not decodable according to
        //   this specification.
        PF = ds.readBits(2);
        //20. Read a 3-bit unsigned integer. These bits are reserved. If this value
        //   is not zero, then stop. This stream is not decodable according to this
        //   specification.
        ds.readBits(3);
        //21. Assign NSBS a value according to PF, as given by Table 6.5.
        //22. Assign NBS a value according to PF, as given by Table 6.6.
        switch(PF) {
            case 0 :
                NSBS = ((FMBW + 1)/2) * ((FMBH + 1)/2) + 2 * ((FMBW + 3)/4) * ((FMBH + 3)/4);
                NBS = 6 * FMBW * FMBH;
                break;
            case 2 :
                NSBS = ((FMBW + 1)/2) * ((FMBH + 1)/2) + 2 * ((FMBW + 3)/4) * ((FMBH + 1)/2);
                NBS = 8 * FMBW * FMBH;
                break;
            case 3 :
                NSBS = 3 * ((FMBW + 1)/2) * ((FMBH + 1)/2);
                NBS = 12 * FMBW * FMBH;
                break;
        }
        //23. Assign NMBS the value (FMBW ∗ FMBH).
        NMBS = FMBW * FMBH;

        buildMacroBlockIndex();
        buildBlockIndex();
    }

    private void buildBlockIndex() throws IOException {

        blockCodedOrder = new TheoraBlockLocation[NBS];
        for (int i=0;i<blockCodedOrder.length;i++) {
            blockCodedOrder[i] = new TheoraBlockLocation();
        }

        int bIdx = 0;
        int sbIdx = 0;

        //macroblocks are always complete and composed of 2x2 luma blocks
        int frameBlockWidth = FMBW * 2;
        int frameBlockHeight = FMBH * 2;

        //superblocks are composed of 4x4 blocks
        //superblocks are in raster-order
        //blocks are in coded-order (hilbert)

        //luma plane
        int[] res = fillBlockIndex(bIdx, sbIdx, frameBlockWidth, frameBlockHeight, TheoraBlockLocation.Type.Y);
        bIdx = res[0];
        sbIdx = res[1];

        //chroma planes
        int subx = 1;
        int suby = 1;
        switch(PF) {
            case 0 : // 4:2:0
                break;
            case 2 : // 4:2:2
                suby = 2;
                break;
            case 3 : // 4:4:4
                subx = 2;
                suby = 2;
                break;
        }
        frameBlockWidth = FMBW * subx;
        frameBlockHeight = FMBH * suby;

        res = fillBlockIndex(bIdx, sbIdx, frameBlockWidth, frameBlockHeight, TheoraBlockLocation.Type.CB);
        bIdx = res[0];
        sbIdx = res[1];

        res = fillBlockIndex(bIdx, sbIdx, frameBlockWidth, frameBlockHeight, TheoraBlockLocation.Type.CR);
        bIdx = res[0];
        sbIdx = res[1];

        if (bIdx!=NBS || sbIdx!=NSBS) {
            throw new IOException("Index computation algo failed");
        }

        //complete relations between blocks
        for (int i=0;i<blockCodedOrder.length;i++) {
            for (int j=0;j<blockCodedOrder.length;j++) {
                if (blockCodedOrder[j].type != blockCodedOrder[i].type) continue;
                if ((blockCodedOrder[j].x == blockCodedOrder[i].x  ) && (blockCodedOrder[j].y == blockCodedOrder[i].y+1)) blockCodedOrder[i].top = blockCodedOrder[j];
                if ((blockCodedOrder[j].x == blockCodedOrder[i].x  ) && (blockCodedOrder[j].y == blockCodedOrder[i].y-1)) blockCodedOrder[i].down = blockCodedOrder[j];
                if ((blockCodedOrder[j].x == blockCodedOrder[i].x+1) && (blockCodedOrder[j].y == blockCodedOrder[i].y  )) blockCodedOrder[i].right = blockCodedOrder[j];
                if ((blockCodedOrder[j].x == blockCodedOrder[i].x-1) && (blockCodedOrder[j].y == blockCodedOrder[i].y  )) blockCodedOrder[i].left = blockCodedOrder[j];
            }
        }
        
        //compute block raster-order index.
        blockRasterOrder = blockCodedOrder.clone();
        Arrays.sort(blockRasterOrder, RASTERORDER_SORTER);
        for (int ri=0;ri<blockRasterOrder.length;ri++) {
            blockRasterOrder[ri].rasterIndex = ri;
        }
        
    }

    private void buildMacroBlockIndex() throws IOException {

        macroblockCodedOrder = new TheoraMacroBlockLocation[FMBW*FMBH];
        for (int i=0;i<macroblockCodedOrder.length;i++) {
            macroblockCodedOrder[i] = new TheoraMacroBlockLocation(PF);
            macroblockCodedOrder[i].index = i;
        }

        int mbIdx = 0;
        int sbIdx = 0;

        int frameBlockWidth = FMBW ;
        int frameBlockHeight = FMBH;


        int nbSuperBlockX = (frameBlockWidth+1) / 2;
        int nbSuperBlockY = (frameBlockHeight+1) / 2;
        for (int sby=0;sby<nbSuperBlockY;sby++) {
            final int sbh = Maths.clamp(frameBlockHeight-sby*2, 0, 2);
            for (int sbx=0;sbx<nbSuperBlockX;sbx++) {
                final int sbw = Maths.clamp(frameBlockWidth-sby*2, 0, 2);

                for (int t=0; t<HILBER2_XY[sbh-1][sbw-1].length; t++,mbIdx++) {
                    macroblockCodedOrder[mbIdx].codedIndex = mbIdx;
                    macroblockCodedOrder[mbIdx].superBlock = sbIdx;
                    //compute position in global frame
                    macroblockCodedOrder[mbIdx].x = sbx*2 + HILBER2_XY[sbh-1][sbw-1][t][0];
                    macroblockCodedOrder[mbIdx].y = sby*2 + HILBER2_XY[sbh-1][sbw-1][t][1];
                }
                sbIdx++;
            }
        }

        if (mbIdx!=macroblockCodedOrder.length) {
            throw new IOException("Index computation algo failed");
        }

    }


    private int[] fillBlockIndex(int bIdx, int sbIdx, int frameBlockWidth, int frameBlockHeight, TheoraBlockLocation.Type type) throws IOException{

        int nbSuperBlockX = (frameBlockWidth+3) / 4;
        int nbSuperBlockY = (frameBlockHeight+3) / 4;
        for (int sby=0;sby<nbSuperBlockY;sby++) {
            final int sbh = Maths.clamp(frameBlockHeight-sby*4, 0, 4);
            for (int sbx=0;sbx<nbSuperBlockX;sbx++) {
                final int sbw = Maths.clamp(frameBlockWidth-sby*4, 0, 4);

                for (int t=0; t<HILBER4_XY[sbh-1][sbw-1].length; t++,bIdx++) {
                    blockCodedOrder[bIdx].codedIndex = bIdx;
                    blockCodedOrder[bIdx].type = type;
                    blockCodedOrder[bIdx].superBlock = sbIdx;
                    //compute position in global frame
                    blockCodedOrder[bIdx].x = sbx*4 + HILBER4_XY[sbh-1][sbw-1][t][0];
                    blockCodedOrder[bIdx].y = sby*4 + HILBER4_XY[sbh-1][sbw-1][t][1];
                    //find containing macroblock index
                    blockCodedOrder[bIdx].macroBlock = findMacroBlock(blockCodedOrder[bIdx]);
                    
                    TheoraBlockLocation[] group;
                    switch (blockCodedOrder[bIdx].type) {
                        case Y : group = blockCodedOrder[bIdx].macroBlock.luma; break;
                        case CB : group = blockCodedOrder[bIdx].macroBlock.chromaB; break;
                        case CR : group = blockCodedOrder[bIdx].macroBlock.chromaR; break;
                        default: throw new IOException("Unexpected type");
                    }
                    for(int i=0;;i++) {
                        if(group[i]==null) {
                            group[i] = blockCodedOrder[bIdx];
                            break;
                        }
                    }
                }
                sbIdx++;
            }
        }

        return new int[]{bIdx,sbIdx};
    }

    private TheoraMacroBlockLocation findMacroBlock(TheoraBlockLocation block) throws IOException {
        int bx = block.x;
        int by = block.y;
        if(block.type == TheoraBlockLocation.Type.CB || block.type == TheoraBlockLocation.Type.CR){
            switch(PF) {
                case 0 : // 4:2:0
                    bx *= 2;
                    by *= 2;
                    break;
                case 2 : // 4:2:2
                    bx *= 2;
                    break;
            }
        }
        
        //compute macroblock x,y
        int mbx = (bx/2);
        int mby = (by/2);
        
        for (TheoraMacroBlockLocation l : macroblockCodedOrder) {
            if (l.x==mbx && l.y==mby) {
                return l;
            }
        }
        throw new IOException("Index computation algo failed");
    }

}
