
package science.unlicense.impl.media.vorbis;

/**
 *
 * @author Johann Sorel
 */
public abstract class VorbisPacket {

    private final byte type;

    public VorbisPacket(byte type) {
        this.type = type;
    }

    public byte getType() {
        return type;
    }

}