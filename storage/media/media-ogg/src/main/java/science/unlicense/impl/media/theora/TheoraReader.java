
package science.unlicense.impl.media.theora;

import java.io.DataInput;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.MishandleException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.color.colorspace.YCbCr;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class TheoraReader extends AbstractReader {

    private final TheoraIdentificationHeader ident = new TheoraIdentificationHeader();
    private final TheoraCommentHeader comment = new TheoraCommentHeader();
    private final TheoraSetupHeader setup = new TheoraSetupHeader();

    private DataInputStream ds;
    private TheoraFrame ws;

    /**
     * Precaldulated quantization matrices.
     */
    private int[][][][] QMATS = new int[2][3][64][0];
    
    public void read() throws IOException {

        ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
        ds.setBitsDirection(DataInputStream.MSB);

        //read headers
        if (ds.read()!=0x80) throw new IOException("Missing identification header");
        ident.read(ds);

        if (ds.read()!=0x81) throw new IOException("Missing comment header");
        comment.read(ds);

        if (ds.read()!=0x82) throw new IOException("Missing setup header");
        setup.read(ds);

        
        //read quantization matrices, expensive operation, cache those.
        for (int qti=0;qti<2;qti++) {
            for (int pli=0;pli<3;pli++) {
                for (int qi=0;qi<64;qi++) {
                    QMATS[qti][pli][qi] = computeQuantizationMatrix_6_4_3(setup.ACSCALE, setup.DCSCALE, setup.BMS, setup.NQRS, setup.QRSIZES, setup.QRBMIS, qti, pli, qi);
                }
            }
        }
        
        //read frames
        int i=0;
        while (true) {
            System.out.println("frams start at "+ds.getByteOffset());
            
            ws = new TheoraFrame();
            ws.COEFFS = new short[ident.NBS][64];
            ws.RECY = new int[ident.FMBH*16][ident.FMBW*16];
            ws.RECCB = new int[ident.FMBH*16][ident.FMBW*16];
            ws.RECCR = new int[ident.FMBH*16][ident.FMBW*16];
            try {
                completeFrameDecode();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            
            final Image image = Images.create(new Extent.Long(ws.RPYW, ws.RPYH), Images.IMAGE_TYPE_RGB);
            final PixelBuffer pixels = image.getColorModel().asTupleBuffer(image);
            for (int x=0;x<ws.RPYW;x++) {
                for (int y=0;y<ws.RPYH;y++) {
                    int ty = ws.RECY[y][x];
                    int tcb = ws.RECCB[y/2][x/2];
                    int tcr = ws.RECCR[y/2][x/2];
                    
                    float[] ycbcr = new float[]{ty/255f,tcb/255f,tcr/255f};
                    float[] rgba = new float[4];
                    YCbCr.INSTANCE.toRGBA(ycbcr,rgba);
                    //Color c = new Color(ws.RECY[y][x],ws.RECCB[y/2][x/2],ws.RECCR[y/2][x/2]);
                    Color c = new Color(rgba);
                    c = new Color(ty,tcb,tcr);
                    pixels.setARGB(new int[]{x,y}, c.toARGB());
                }
            }
            
            Images.write(image, new Chars("png"), Paths.resolve(new Chars("file:/home/husky/frame"+i+".png")));
            i++;
            System.exit(0);
        }

    }

    /**
     * 7.11 Complete Frame Decode
     * Spec : page 137
     *
     */
    private void completeFrameDecode() throws IOException {

        if (true) { //TODO
            //1. If the size of the data packet is non-zero:
            // (a) Decode the frame header values FTYPE, NQIS, and QIS using the
            //   procedure given in Section 7.1.
            frameHeaderDecode_7_1();
            // (b) Using FTYPE, NSBS, and NBS, decode the list of coded block
            //   flags into BCODED using the procedure given in Section 7.3.
            ws.BCODED = codedBlockFlagsDecode_7_3(ds, ident, ws.FTYPE, ident.NSBS, ident.NBS);
            // (c) Using FTYPE, NMBS, NBS, and BCODED, decode the macro
            //   block coding modes into MBMODES using the procedure given in
            //   Section 7.4.
            ws.MBMODES = macroBlockCodingModes_7_4(ident, ds, ws.FTYPE, ident.NMBS, ident.NBS, ws.BCODED);
            
            if (ws.FTYPE!=0) {
                // (d) If FTYPE is non-zero (inter frame), using PF, NMBS, MBMODES,
                //   NBS, and BCODED, decode the motion vectors into MVECTS using
                //   the procedure given in Section 7.5.1.
                int[] MV = motionVectorDecode_7_5_1(ds, 0); //TODO MVMODE
                ws.MVX = MV[0];
                ws.MVY = MV[1];
            }
            // (e) Using NBS, BCODED, and NQIS, decode the block-level qi values
            //   into QIIS using the procedure given in Section 7.6.
            ws.QIIS = blockLevelQiDecode_7_6(ds,ident.NBS,ws.BCODED,ws.NQIS);
            // (f) Using NBS, NMBS, BCODED, and HTS, decode the DCT coeffi-
            //   cients into NCOEFFS and NCOEFFS using the procedure given in
            //   Section 7.7.3.
            DCTCoefficientDecode_7_7_3();
            // (g) Using BCODED and MBMODES, undo the DC prediction on the
            //   DC coefficients stored in COEFFS using the procedure given in Sec-
            //   tion 7.8.2.
            invertingDCPredictionProcess_7_8_2();
        } else {
            //2. Otherwise:
            // (a) Assign FTYPE the value 1 (inter frame).
            ws.FTYPE = 1;
            // (b) Assign NQIS the value 1.
            ws.NQIS = 1;
            // (c) Assign QIS[0] the value 63.
            ws.QIS = new int[]{63};
            // (d) For each value of bi from 0 to (NBS − 1), assign BCODED[bi ] the
            // value zero.
            ws.BCODED = new int[ident.NBS];
        }

        //3. Assign RPYW and RPYH the values (16 ∗ FMBW) and (16 ∗ FMBH),
        //   respectively.
        ws.RPYW = 16 * ident.FMBW;
        ws.RPYH = 16 * ident.FMBH;
        //4. Assign RPCW and RPCH the values from the row of Table 7.89 corre-
        //   sponding to PF.
        switch(ident.PF) {
            case 0 :
                ws.RPCW = 8*ident.FMBW;
                ws.RPCH = 8*ident.FMBH;
                break;
            case 2 :
                ws.RPCW = 8*ident.FMBW;
                ws.RPCH = 16*ident.FMBH;
                break;
            case 3 :
                ws.RPCW = 16*ident.FMBW;
                ws.RPCH = 16*ident.FMBH;
                break;
            default:
                throw new IOException("Unsupported PF : "+ident.PF);
        }

        //5. Using ACSCALE, DCSCALE, BMS, NQRS, QRSIZES, QRBMIS,
        //   NBS, BCODED, MBMODES, MVECTS, COEFFS, NCOEFFS, QIS,
        //   QIIS, RPYW, RPYH, RPCW, RPCH, GOLDREFY, GOLDREFCB,
        //   GOLDREFCR, PREVREFY, PREVREFCB, and PREVREFCR,
        //   reconstruct the complete frame into RECY, RECCB, and RECCR us-
        //   ing the procedure given in Section 7.9.4.
        completeReconstructionAlgorithm_7_9_4();
        //6. Using LFLIMS, RPYW, RPYH, RPCW, RPCH, NBS, BCODED, and
        //   QIS, apply the loop filter to the reconstructed frame in RECY, RECCB,
        //   and RECCR using the procedure given in Section 7.10.3.
        completeLoopFilter();
        //7. If FTYPE is zero (intra frame), assign GOLDREFY, GOLDREFCB,
        //   and GOLDREFCR the values RECY, RECCB, and RECCR, re-
        //   spectively.
        if (ws.FTYPE==0) {
            ws.GOLDREFY = ws.RECY;
            ws.GOLDREFCB = ws.RECCB;
            ws.GOLDREFCB = ws.RECCR;
        }
        //8. Assign PREVREFY, PREVREFCB, and PREVREFCR the values
        //   RECY, RECCB, and RECCR, respectively.
        ws.PREVREFY = ws.RECY;
        ws.PREVREFCB = ws.RECCB;
        ws.PREVREFCR = ws.RECCR;

    }

    /**
     * 6.4.3 Compute a quantization matrix.
     * Spec : page 56
     *
     * Note that the product of the scale value and the base matrix value is in units
     * of 100ths of a pixel value, and thus is divided by 100 to return it to units of a
     * single pixel value. This value is then scaled by four, to match the scaling of the
     * DCT output, which is also a factor of four larger than the orthonormal version
     * of the transform.
     *
     * @return matrix size 64
     */
    public static int[] computeQuantizationMatrix_6_4_3( int[] ACSCALE, int[] DCSCALE,
            int[][] BMS, int[][] NQRS, int[][][] QRSIZES, int[][][] QRBMIS,
            int qti, int pli, int qi
            ) {


        // 1. Assign qri the index of a quant range such that
        //         qri-1
        //    qi ≥   ∑  QRSIZES[qti][pli][qrj]
        //         qrj=0
        //    and
        //          qri
        //    qi ≤   ∑  QRSIZES[qti][pli][qrj]
        //         qrj=0
        // 2. Assign QISTART the value
        //     qri-1
        //      ∑    QRSIZES[qti][pli][qrj]
        //     qrj=0
        // 3. Assign QIEND the value
        //     qri
        //      ∑    QRSIZES[qti][pli][qrj]
        //     qrj=0
        int qri;
        int QISTART = 0;
        int QIEND = 0;
        for (qri=0; qri<63; qri++) {
            QISTART = QIEND;
            QIEND += QRSIZES[qti][pli][qri];
            if (qi>=QISTART && qi<=QIEND) break;
        }

        // 4. Assign bmi the value QRBMIS[qti ][pli ][qri ].
        final int bmi = QRBMIS[qti][pli][qri];

        // 5. Assign bmj the value QRBMIS[qti ][pli ][qri + 1].
        final int bmj = QRBMIS[qti][pli][qri+1];

        // 6. For each consecutive value of ci from 0 to 63, inclusive:
        final int[] QMAT = new int[64];
        for (int ci=0;ci<64;ci++) {
            // (a) Assign BM[ci ] the value
            //   (2 ∗ (QIEND − qi ) ∗ BMS[bmi ][ci ]
            //   + 2 ∗ (qi − QISTART) ∗ BMS[bmj ][ci ]
            //   + QRSIZES[qti ][pli ][qri ]) / (2 ∗ QRSIZES[qti ][pli ][qri ])
            final int BM = (2 * (QIEND - qi) * BMS[bmi][ci]
                         + 2 * (qi - QISTART) * BMS[bmj][ci]
                         + QRSIZES[qti][pli][qri]) / (2 * QRSIZES[qti][pli][qri]);
            // (b) Assign QMIN the value given by Table 6.18 according to qti and ci .
            final int QMIN = (ci==0) ?
                                ((qti==0) ? 16 : 32) :
                                ((qti==0) ?  8 : 16) ;

            // (c) If ci equals zero, assign QSCALE the value DCSCALE[qi ].
            // (d) Else, assign QSCALE the value ACSCALE[qi ].
            final int QSCALE = (ci==0) ? DCSCALE[qi] : ACSCALE[qi];

            // (e) Assign QMAT[ci ] the value
            // max(QMIN, min((QSCALE ∗ BM[ci ]//100) ∗ 4, 4096)).
            QMAT[ci] = Maths.max(QMIN, Maths.min((QSCALE * BM /100) * 4, 4096));
        }

        return QMAT;
    }

    /**
     * 7.1 Frame Header Decode
     * Spec : page 63
     *
     */
    private void frameHeaderDecode_7_1() throws IOException {

        //1. Read a 1-bit unsigned integer. If the value read is not zero, stop. This is
        //   not a data packet.
        if (ds.readBits(1)!=0) throw new IOException("Not a data packet");

        //2. Read a 1-bit unsigned integer as FTYPE. This is the type of frame being
        //   decoded, as given in Table 7.3. If this is the first frame being decoded,
        //   this MUST be zero.
        ws.FTYPE = ds.readBits(1);

        //3. Read in a 6-bit unsigned integer as QIS[0].
        ws.QIS = new int[1];
        ws.QIS[0] = ds.readBits(6);
        //4. Read a 1-bit unsigned integer as MOREQIS.
        //5. If MOREQIS is zero, set NQIS to 1.
        //6. Otherwise:
        //   (a) Read in a 6-bit unsigned integer as QIS[1].
        //   (b) Read a 1-bit unsigned integer as MOREQIS.
        //   (c) If MOREQIS is zero, set NQIS to 2.
        //   (d) Otherwise:
        //      i. Read in a 6-bit unsigned integer as QIS[2].
        //      ii. Set NQIS to 3.
        if (ds.readBits(1)!=0) {
            ws.QIS = Arrays.insert(ws.QIS, 1, ds.readBits(6));
            if (ds.readBits(1)!=0) {
                ws.QIS = Arrays.insert(ws.QIS, 2, ds.readBits(6));
            }
        }
        ws.NQIS = ws.QIS.length;

        //7. If FTYPE is 0, read a 3-bit unsigned integer. These bits are reserved. If
        //   this value is not zero, stop. This frame is not decodable according to this
        //   specification.
        if (ws.FTYPE==0) {
            if (ds.readBits(3) !=0) {
                throw new IOException("Stream is not decodable");
            }
        }
    }

    /**
     * 7.2.1 Long-Run Bit String Decode
     * Spec : page 65
     */
    private static Bits longRunBitStringDecode_7_2_1(DataInputStream ds, final int NBITS) throws IOException {
        //1. Assign LEN the value 0.
        int LEN = 0;
        //2. Assign BITS the empty string.
        Bits BITS = new Bits();
        //3. If LEN equals NBITS, return the completely decoded string BITS.
        if (LEN==NBITS) return BITS.readMode();
        //4. Read a 1-bit unsigned integer as BIT.
        int BIT = ds.readBits(1);
        int RSTART;
        int RBITS;
        for(;;) {
            //5. Read a bit at a time until one of the Huffman codes given in Table 7.7 is
            //   recognized.
            //6. Assign RSTART and RBITS the values given in Table 7.7 according to
            //   the Huffman code read.
            int[] res = (int[]) TheoraConstants.T7_7_LONGRUN_TREE.decode(ds);
            RSTART = res[0];
            RBITS = res[1];
            //7. Read an RBITS-bit unsigned integer as ROFFS.
            int ROFFS = ds.readBits(RBITS);
            //8. Assign RLEN the value (RSTART + ROFFS).
            int RLEN = RSTART + ROFFS;
            //9. Append RLEN copies of BIT to BITS.
            BITS.append(BIT, RLEN);
            //10. Add RLEN to the value LEN. LEN MUST be less than or equal to
            //    NBITS.
            LEN += RLEN;
            if (LEN > NBITS) throw new IOException("Unvalid LEN value :"+LEN);
            //11. If LEN equals NBITS, return the completely decoded string BITS.
            if (LEN == NBITS) return BITS.readMode();

            if (RLEN == 4129) {
                //12. If RLEN equals 4129, read a 1-bit unsigned integer as BIT.
                BIT = ds.readBits(1);
            } else {
                //13. Otherwise, assign BIT the value (1 − BIT).
                BIT = 1 - BIT;
            }
            //14. Continue decoding runs from step 5.
        }
    }

    /**
     * 7.2.2 Short-Run Bit String Decode
     * Spec : page 67
     */
    private static Bits shortRunBitStringDecode_7_2_2(DataInputStream ds, final int NBITS) throws IOException {

        //1. Assign LEN the value 0.
        int LEN = 0;
        //2. Assign BITS the empty string.
        Bits BITS = new Bits();
        //3. If LEN equals NBITS, return the completely decoded string BITS.
        if (LEN==NBITS) return BITS.readMode();
        //4. Read a 1-bit unsigned integer as BIT.
        int BIT = ds.readBits(1);
        int RSTART;
        int RBITS;
        for (;;) {
            //5. Read a bit at a time until one of the Huffman codes given in Table 7.11
            //   is recognized.
            //6. Assign RSTART and RBITS the values given in Table 7.11 according to
            //   the Huffman code read.
            int[] res = (int[]) TheoraConstants.T7_11_SHORTRUN_RTEE.decode(ds);
            RSTART = res[0];
            RBITS = res[1];

            //7. Read an RBITS-bit unsigned integer as ROFFS.
            int ROFFS = ds.readBits(RBITS);
            //8. Assign RLEN the value (RSTART + ROFFS).
            int RLEN = RSTART + ROFFS;
            //9. Append RLEN copies of BIT to BITS.
            BITS.append(BIT, RLEN);
            //10. Add RLEN to the value LEN. LEN MUST be less than or equal to
            //   NBITS.
            LEN += RLEN;
            if (LEN > NBITS) throw new IOException("Unvalid LEN value :"+LEN);
            //11. If LEN equals NBITS, return the completely decoded string BITS.
            if (LEN == NBITS) return BITS.readMode();
            //12. Assign BIT the value (1 − BIT).
            BIT = 1 - BIT;
            //13. Continue decoding runs from step 5.
        }

    }

    /**
     * 7.3 Coded Block Flags Decode
     * Spec : page 69
     * 
     * @return BCODED
     */
    private static int[] codedBlockFlagsDecode_7_3(DataInputStream ds, TheoraIdentificationHeader ident, 
            final int FTYPE, final int NSBS, final int NBS) throws IOException {
        final int[] BCODED = new int[NBS];

        if (FTYPE==0) {
            //1. If FTYPE is zero (intra frame):
            // (a) For each consecutive value of bi from 0 to (NBS−1), assign BCODED[bi ]
            //     the value one.
            Arrays.fill(BCODED, 1);
        } else {
            //2. Otherwise (inter frame):
            // (a) Assign NBITS the value NSBS.
            int NBITS = NSBS;

            // (b) Read an NBITS-bit bit string into BITS, using the procedure de-
            //    scribed in Section 7.2.1. This represents the list of partially coded
            //    super blocks.
            Bits BITS = longRunBitStringDecode_7_2_1(ds,NBITS);

            // (c) For each consecutive value of sbi from 0 to (NSBS − 1), remove the
            //    bit at the head of the string BITS and assign it to SBPCODED[sbi ].
            // (d) Assign NBITS the total number of super blocks such that
            //    SBPCODED[sbi ] equals zero.
            NBITS = 0;
            int[] SBPCODED = new int[NSBS];
            for (int sbi=0;sbi<NSBS;sbi++) {
                SBPCODED[sbi] = BITS.pollHeadBit();
                if (SBPCODED[sbi]==0) NBITS++;
            }

            // (e) Read an NBITS-bit bit string into BITS, using the procedure de-
            //    scribed in Section 7.2.1. This represents the list of fully coded super
            //    blocks.
            BITS = longRunBitStringDecode_7_2_1(ds,NBITS);

            // (f) For each consecutive value of sbi from 0 to (NSBS − 1) such that
            //    SBPCODED[sbi ] equals zero, remove the bit at the head of the string
            //    BITS and assign it to SBFCODED[sbi ].
            int[] SBFCODED = new int[NSBS];
            for (int sbi=0;sbi<NSBS;sbi++) {
                if (SBPCODED[sbi]==0) {
                    SBFCODED[sbi] = BITS.pollHeadBit();
                }
            }

            // (g) Assign NBITS the number of blocks contained in super blocks where
            //    SBPCODED[sbi ] equals one. Note that this might not be equal to 16
            //    times the number of partially coded super blocks, since super blocks
            //    which overlap the edge of the frame will have fewer than 16 blocks
            //    in them.
            NBITS = 0;
            for (int sbi=0;sbi<NSBS;sbi++) {
                if (SBPCODED[sbi]==1) NBITS++;
            }

            // (h) Read an NBITS-bit bit string into BITS, using the procedure de-
            //    scribed in Section 7.2.2.
            BITS = shortRunBitStringDecode_7_2_2(ds,NBITS);

            // (i) For each block in coded order—indexed by bi :
            for (int bi=0;bi<NBS;bi++) {
                //    i. Assign sbi the index of the super block containing block bi .
                int sbi = ident.blockCodedOrder[bi].superBlock;
                //    ii. If SBPCODED[sbi ] is zero, assign BCODED[bi ] the value SBFCODED[sbi ].
                //    iii. Otherwise, remove the bit at the head of the string BITS and
                //         assign it to BCODED[bi ].
                BCODED[bi] = (SBPCODED[sbi]==0) ? SBFCODED[sbi] : BITS.pollHeadBit();
            }
        }
        
        return BCODED;
    }

    /**
     * 7.4 Macro Block Coding Modes
     * Spec : page 87
     * 
     * @return MBMODES
     */
    private static int[] macroBlockCodingModes_7_4(TheoraIdentificationHeader ident, DataInputStream ds, 
            final int FTYPE, final int NMBS, final int NBS, final int[] BCODED) throws IOException {
        final int[] MBMODES = new int[NMBS];

        if (FTYPE==0) {
            //1. If FTYPE is 0 (intra frame):
            //(a) For each consecutive value of mbi from 0 to (NMBS − 1), inclusive,
            //   assign MBMODES[mbi ] the value 1 (INTRA).
            Arrays.fill(MBMODES, 1);
        } else {
            //2. Otherwise (inter frame):
            //(a) Read a 3-bit unsigned integer as MSCHEME.
            int MSCHEME = ds.readBits(3);
            int[] MALPHABET = new int[6];
            if (MSCHEME==0) {
                //(b) If MSCHEME is 0:
                //    i. For each consecutive value of MODE from 0 to 7, inclusive:
                for (int MODE=0;MODE<8;MODE++) {
                    //      A. Read a 3-bit unsigned integer as mi .
                    int mi = ds.readBits(3);
                    //      B. Assign MALPHABET[mi ] the value MODE.
                    MALPHABET[mi] = MODE;
                }
            } else if(MSCHEME!=7) {
                //(c) Otherwise, if MSCHEME is not 7, assign the entries of MALPHABET
                //    the values in the corresponding column of Table 7.19.
                switch(MSCHEME) {
                    case 1 : MALPHABET = new int[]{3,4,2,0,1,5,6,7};
                    case 2 : MALPHABET = new int[]{3,4,0,2,1,5,6,7};
                    case 3 : MALPHABET = new int[]{3,2,4,0,1,5,6,7};
                    case 4 : MALPHABET = new int[]{3,2,0,4,1,5,6,7};
                    case 5 : MALPHABET = new int[]{0,3,4,2,1,5,6,7};
                    case 6 : MALPHABET = new int[]{0,5,3,4,2,1,6,7};
                    default : throw new IOException("Unexpected MSCHEME value "+MSCHEME);
                }
            }

            //(d) For each consecutive macro block in coded order (cf. Section 2.4)—
            //    indexed by mbi :
            for (int mbi=0;mbi<ident.macroblockCodedOrder.length;mbi++) {
                //    i. If a block bi in the luma plane of macro block mbi exists such
                //        that BCODED[bi ] is 1:
                TheoraMacroBlockLocation mbLoc = ident.macroblockCodedOrder[mbi];
                boolean luma1 = false;
                for (int li=0;li<4;li++) {
                    if (BCODED[mbLoc.luma[li].codedIndex] == 1) {
                        luma1=true;
                        break;
                    }
                }
                if (luma1) {
                    if (MSCHEME != 7) {
                        //        A. If MSCHEME is not 7, read one bit at a time until one of
                        //            the Huffman codes in Table 7.19 is recognized, and assign
                        //            MBMODES[mbi ] the value MALPHABET[mi ], where mi
                        //            is the index of the Huffman code decoded.
                        final int mi = (Integer) TheoraConstants.T7_19.decode(ds);
                        MBMODES[mbi] = MALPHABET[mi];
                    } else {
                        //        B. Otherwise, read a 3-bit unsigned integer as MBMODES[mbi ].
                        MBMODES[mbi] = ds.readBits(3);
                    }
                } else {
                    //    ii. Otherwise, if no luma-plane blocks in the macro block are coded,
                    //        assign MBMODES[mbi ] the value 0 (INTER NOMV).
                    MBMODES[mbi] = 0;
                }
            }
        }
        
        return MBMODES;
    }

    /**
     * 7.5.1 Motion Vector Decode
     * Spec : page 90
     * 
     * @return MVX, MVY
     */
    private static int[] motionVectorDecode_7_5_1(DataInputStream ds, final int MVMODE) throws IOException {
        int MVX;
        int MVY;
        if (MVMODE==0) {
            //1. If MVMODE is 0:
            //  (a) Read 1 bit at a time until one of the Huffman codes in Table 7.23 is
            //     recognized, and assign the value to MVX.
            MVX = (Integer)TheoraConstants.T7_23_MOTIONVECTOR_TREE.decode(ds);
            //  (b) Read 1 bit at a time until one of the Huffman codes in Table 7.23 is
            //     recognized, and assign the value to MVY.
            MVY = (Integer)TheoraConstants.T7_23_MOTIONVECTOR_TREE.decode(ds);
        } else {
            //2. Otherwise:
            //  (a) Read a 5-bit unsigned integer as MVX.
            MVX = ds.readBits(5);
            //  (b) Read a 1-bit unsigned integer as MVSIGN.
            int MVSIGN = ds.readBits(1);
            //  (c) If MVSIGN is 1, assign MVX the value −MVX.
            if (MVSIGN==1) MVX = -MVX;
            //  (d) Read a 5-bit unsigned integer as MVY.
            MVY = ds.readBits(5);
            //  (e) Read a 1-bit unsigned integer as MVSIGN.
            MVSIGN = ds.readBits(1);
            //  (f) If MVSIGN is 1, assign MVY the value −MVY.
            if (MVSIGN==1) MVY = -MVY;
        }
        
        return new int[]{MVX,MVY};
    }

    /**
     * 7.6 Block-Level qi Decode
     * Spec : page 97
     * 
     * @return QIIS
     */
    private static int[] blockLevelQiDecode_7_6(DataInputStream ds, 
            final int NBS, final int[] BCODED, final int NQIS) throws IOException {
        
        //1. For each value of bi from 0 to (NBS − 1), assign QIIS[bi ] the value zero.
        final int[] QIIS = new int[NBS];

        //2. For each consecutive value of qii from 0 to (NQIS − 2):
        for (int qii=0; qii<NQIS-1; qii++) {
            //  (a) Assign NBITS be the number of blocks bi such that BCODED[bi ]
            //     is non-zero and QIIS[bi ] equals qii .
            int NBITS = 0;
            for (int bi=0; bi<NBS; bi++) {
                if (BCODED[bi]!=0 && QIIS[bi] == qii) {
                    NBITS++;
                }
            }

            //  (b) Read an NBITS-bit bit string into BITS, using the procedure de-
            //     scribed in Section 7.2.1. This represents the list of blocks that use qi
            //     value qii or higher.
            Bits BITS = longRunBitStringDecode_7_2_1(ds,NBITS);

            //  (c) For each consecutive value of bi from 0 to (NBS − 1) such that
            //     BCODED[bi ] is non-zero and QIIS[bi ] equals qii :
            //    i. Remove the bit at the head of the string BITS and add its value
            //      to QIIS[bi ].
            for (int bi=0; bi<NBS; bi++) {
                if (BCODED[bi]!=0 && QIIS[bi] == qii) {
                    QIIS[bi] = BITS.pollHeadBit();
                }
            }
        }

        return QIIS;
    }

    /**
     * 7.7.1 EOB Token Decode
     * Spec : page 101
     * 
     * Modifies TIS, COEFFS
     * @return EOBS
     */
    private static int EOBTokenDecode_7_7_1(DataInputStream ds, final int TOKEN, final int NBS, 
            final int[] TIS, final int[] NCOEFFS, final short[][] COEFFS, final int bi, final int ti) throws IOException {
        int EOBS = 1;
        switch (TOKEN) {
            //1. If TOKEN is 0, assign EOBS the value 1.
            case 0 : EOBS = 1; break;
            //2. Otherwise, if TOKEN is 1, assign EOBS the value 2.
            case 1 : EOBS = 2; break;
            //3. Otherwise, if TOKEN is 2, assign EOBS the value 3.
            case 2 : EOBS = 3; break;
            //4. Otherwise, if TOKEN is 3:
            //  (a) Read a 2-bit unsigned integer as EOBS.
            //  (b) Assign EOBS the value (EOBS + 4).
            case 3 : EOBS = ds.readBits(2) + 4; break;
            //5. Otherwise, if TOKEN is 4:
            //  (a) Read a 3-bit unsigned integer as EOBS.
            //  (b) Assign EOBS the value (EOBS + 8).
            case 4 : EOBS = ds.readBits(3) + 8; break;
            //6. Otherwise, if TOKEN is 5:
            //  (a) Read a 4-bit unsigned integer as EOBS.
            //  (b) Assign EOBS the value (EOBS + 16).
            case 5 : EOBS = ds.readBits(4) + 16; break;
            //7. Otherwise, TOKEN is 6:
            //  (a) Read a 12-bit unsigned integer as EOBS.
            //  (b) If EOBS is zero, assign EOBS to be the number of coded blocks bj
            //     such that TIS[bj ] is less than 64.
            case 6 :
                EOBS = ds.readBits(12);
                if (EOBS==0) {
                    for (int bj=0;bj<NBS;bj++) {
                        if (TIS[bj] < 64) EOBS++;
                    }
                }
                break;
            default: throw new IOException("Unvalid token value : "+TOKEN);
        }
        //8. For each value of tj from ti to 63, assign COEFFS[bi ][tj ] the value zero.
        for (int tj=ti;tj<64;tj++) {
            COEFFS[bi][tj] = 0;
        }

        //9. Assign NCOEFFS[bi ] the value TIS[bi ].
        NCOEFFS[bi] = TIS[bi];
        //10. Assign TIS[bi ] the value 64.
        TIS[bi] = 64;
        //11. Assign EOBS the value (EOBS − 1).
        EOBS = EOBS - 1;
        
        return EOBS;
    }

    /**
     * 7.7.2 Coefficient Token Decode
     * Spec : page 104
     * 
     * Modifies TIS, COEFFS, NCOEFFS
     */
    private static void coefficientTokenDecode_7_7_2(DataInputStream ds, final int TOKEN, final int NBS, 
            final int[] TIS, final int[] NCOEFFS, final short[][] COEFFS, final int bi, final int ti) throws IOException {
        
        switch (TOKEN) {
            case 7 : {
                //1. If TOKEN is 7:
                //  (a) Read in a 3-bit unsigned integer as RLEN.
                //  (b) Assign RLEN the value (RLEN + 1).
                int RLEN = ds.readBits(3) + 1;
                //  (c) For each value of tj from ti to (ti +RLEN−1), assign COEFFS[bi ][tj ]
                //      the value zero.
                Arrays.fill(COEFFS[bi], ti, RLEN, (short)0);
                //  (d) Assign TIS[bi ] the value TIS[bi ] + RLEN.
                TIS[bi] = TIS[bi] + RLEN;
                } break;
            case 8 : {
                //2. Otherwise, if TOKEN is 8:
                //  (a) Read in a 6-bit unsigned integer as RLEN.
                //  (b) Assign RLEN the value (RLEN + 1).
                int RLEN = ds.readBits(6) + 1;
                //  (c) For each value of tj from ti to (ti +RLEN−1), assign COEFFS[bi ][tj ]
                //      the value zero.
                Arrays.fill(COEFFS[bi], ti, RLEN, (short)0);
                //  (d) Assign TIS[bi ] the value TIS[bi ] + RLEN.
                TIS[bi] = TIS[bi] + RLEN;
                } break;
            case 9 : {
                //3. Otherwise, if TOKEN is 9:
                //  (a) Assign COEFFS[bi ][ti ] the value 1.
                COEFFS[bi][ti] = 1;
                //  (b) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (c) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 10 : {
                //4. Otherwise, if TOKEN is 10:
                //  (a) Assign COEFFS[bi ][ti ] the value −1.
                COEFFS[bi][ti] = -1;
                //  (b) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (c) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 11 : {
                //5. Otherwise, if TOKEN is 11:
                //  (a) Assign COEFFS[bi ][ti ] the value 2.
                COEFFS[bi][ti] = 2;
                //  (b) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (c) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 12 : {
                //6. Otherwise, if TOKEN is 12:
                //  (a) Assign COEFFS[bi ][ti ] the value −2.
                COEFFS[bi][ti] = -2;
                //  (b) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (c) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 13 : {
                //7. Otherwise, if TOKEN is 13:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) If SIGN is zero, assign COEFFS[bi ][ti ] the value 3.
                //  (c) Otherwise, assign COEFFS[bi ][ti ] the value −3.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? 3 : -3);
                //  (d) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (e) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 14 : {
                //8. Otherwise, if TOKEN is 14:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) If SIGN is zero, assign COEFFS[bi ][ti ] the value 4.
                //  (c) Otherwise, assign COEFFS[bi ][ti ] the value −4.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? 4 : -4);
                //  (d) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (e) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 15 : {
                //9. Otherwise, if TOKEN is 15:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) If SIGN is zero, assign COEFFS[bi ][ti ] the value 5.
                //  (c) Otherwise, assign COEFFS[bi ][ti ] the value −5.
                COEFFS[bi][ti] = (short)((SIGN==0) ? 5 : -5);
                //  (d) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (e) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 16 : {
                //10. Otherwise, if TOKEN is 16:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) If SIGN is zero, assign COEFFS[bi ][ti ] the value 6.
                //  (c) Otherwise, assign COEFFS[bi ][ti ] the value −6.
                COEFFS[bi][ti] = (short)((SIGN==0) ? 6 : -6);
                //  (d) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (e) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 17 : {
                //11. Otherwise, if TOKEN is 17:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 1-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 7).
                int MAG = ds.readBits(1) + 7;
                //  (d) If SIGN is zero, assign COEFFS[bi ][ti ] the value MAG.
                //  (e) Otherwise, assign COEFFS[bi ][ti ] the value −MAG.
                COEFFS[bi][ti] = (short)((SIGN==0) ? MAG : -MAG);
                //  (f) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (g) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 18 : {
                //12. Otherwise, if TOKEN is 18:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 2-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 9).
                int MAG = ds.readBits(2) + 9;
                //  (d) If SIGN is zero, assign COEFFS[bi ][ti ] the value MAG.
                //  (e) Otherwise, assign COEFFS[bi ][ti ] the value −MAG.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (f) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (g) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 19 : {
                //13. Otherwise, if TOKEN is 19:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 3-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 13).
                int MAG = ds.readBits(3) + 13;
                //  (d) If SIGN is zero, assign COEFFS[bi ][ti ] the value MAG.
                //  (e) Otherwise, assign COEFFS[bi ][ti ] the value −MAG.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (f) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (g) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 20 : {
                //14. Otherwise, if TOKEN is 20:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 4-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 21).
                int MAG = ds.readBits(4) + 21;
                //  (d) If SIGN is zero, assign COEFFS[bi ][ti ] the value MAG.
                //  (e) Otherwise, assign COEFFS[bi ][ti ] the value −MAG.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (f) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (g) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 21 : {
                //15. Otherwise, if TOKEN is 21:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 5-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 37).
                int MAG = ds.readBits(5) + 37;
                //  (d) If SIGN is zero, assign COEFFS[bi ][ti ] the value MAG.
                //  (e) Otherwise, assign COEFFS[bi ][ti ] the value −MAG.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (f) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (g) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 22 : {
                //16. Otherwise, if TOKEN is 22:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 9-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 69).
                int MAG = ds.readBits(9) + 69;
                //  (d) If SIGN is zero, assign COEFFS[bi ][ti ] the value MAG.
                //  (e) Otherwise, assign COEFFS[bi ][ti ] the value −MAG.
                COEFFS[bi][ti] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (f) Assign TIS[bi ] the value TIS[bi ] + 1.
                TIS[bi] = TIS[bi] + 1;
                //  (g) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 23 : {
                //17. Otherwise, if TOKEN is 23:
                //  (a) Assign COEFFS[bi ][ti ] the value zero.
                COEFFS[bi][ti] = 0;
                //  (b) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (c) If SIGN is zero, assign COEFFS[bi ][ti + 1] the value 1.
                //  (d) Otherwise, assign COEFFS[bi ][ti + 1] the value −1.
                COEFFS[bi][ti+1] = (short) ((SIGN==0) ? 1 : -1);
                //  (e) Assign TIS[bi ] the value TIS[bi ] + 2.
                TIS[bi] = TIS[bi] + 2;
                //  (f) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 24 : {
                //18. Otherwise, if TOKEN is 24:
                //  (a) For each value of tj from ti to (ti + 1), assign COEFFS[bi ][tj ] the
                //      value zero.
                COEFFS[bi][ti+0] = 0;
                COEFFS[bi][ti+1] = 0;
                //  (b) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (c) If SIGN is zero, assign COEFFS[bi ][ti + 2] the value 1.
                //  (d) Otherwise, assign COEFFS[bi ][ti + 2] the value −1.
                COEFFS[bi][ti+2] = (short) ((SIGN==0) ? 1 : -1);
                //  (e) Assign TIS[bi ] the value TIS[bi ] + 3.
                TIS[bi] = TIS[bi] + 3;
                //  (f) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 25 : {
                //19. Otherwise, if TOKEN is 25:
                //  (a) For each value of tj from ti to (ti + 2), assign COEFFS[bi ][tj ] the
                //      value zero.
                COEFFS[bi][ti+0] = 0;
                COEFFS[bi][ti+1] = 0;
                COEFFS[bi][ti+2] = 0;
                //  (b) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (c) If SIGN is zero, assign COEFFS[bi ][ti + 3] the value 1.
                //  (d) Otherwise, assign COEFFS[bi ][ti + 3] the value −1.
                COEFFS[bi][ti+3] = (short) ((SIGN==0) ? 1 : -1);
                //  (e) Assign TIS[bi ] the value TIS[bi ] + 4.
                TIS[bi] = TIS[bi] + 4;
                //  (f) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 26 : {
                //20. Otherwise, if TOKEN is 26:
                //  (a) For each value of tj from ti to (ti + 3), assign COEFFS[bi ][tj ] the
                //      value zero.
                COEFFS[bi][ti+0] = 0;
                COEFFS[bi][ti+1] = 0;
                COEFFS[bi][ti+2] = 0;
                COEFFS[bi][ti+3] = 0;
                //  (b) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (c) If SIGN is zero, assign COEFFS[bi ][ti + 4] the value 1.
                //  (d) Otherwise, assign COEFFS[bi ][ti + 4] the value −1.
                COEFFS[bi][ti+4] = (short) ((SIGN==0) ? 1 : -1);
                //  (e) Assign TIS[bi ] the value TIS[bi ] + 5.
                TIS[bi] = TIS[bi] + 5;
                //  (f) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 27 : {
                //21. Otherwise, if TOKEN is 27:
                //  (a) For each value of tj from ti to (ti + 4), assign COEFFS[bi ][tj ] the
                //      value zero.
                COEFFS[bi][ti+0] = 0;
                COEFFS[bi][ti+1] = 0;
                COEFFS[bi][ti+2] = 0;
                COEFFS[bi][ti+3] = 0;
                COEFFS[bi][ti+4] = 0;
                //  (b) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (c) If SIGN is zero, assign COEFFS[bi ][ti + 5] the value 1.
                //  (d) Otherwise, assign COEFFS[bi ][ti + 5] the value −1.
                COEFFS[bi][ti+5] = (short) ((SIGN==0) ? 1 : -1);
                //  (e) Assign TIS[bi ] the value TIS[bi ] + 6.
                TIS[bi] = TIS[bi] + 6;
                //  (f) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 28 : {
                //22. Otherwise, if TOKEN is 28:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 2-bit unsigned integer as RLEN.
                //  (c) Assign RLEN the value (RLEN + 6).
                int RLEN = ds.readBits(2) + 6;
                //  (d) For each value of tj from ti to (ti +RLEN−1), assign COEFFS[bi ][tj ]
                //      the value zero.
                Arrays.fill(COEFFS[bi], ti, RLEN, (short)0);
                //  (e) If SIGN is zero, assign COEFFS[bi ][ti + RLEN] the value 1.
                //  (f) Otherwise, assign COEFFS[bi ][ti + RLEN] the value −1.
                COEFFS[bi][ti+RLEN] = (short) ((SIGN==0) ? 1 : -1);
                //  (g) Assign TIS[bi ] the value TIS[bi ] + RLEN + 1.
                TIS[bi] = TIS[bi] + RLEN + 1;
                //  (h) Assign NCOEFFS[bi ] the value TIS[bi ].
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 29 : {
                //23. Otherwise, if TOKEN is 29:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 3-bit unsigned integer as RLEN.
                //  (c) Assign RLEN the value (RLEN + 10).
                int RLEN = ds.readBits(3) + 10;
                //  (d) For each value of tj from ti to (ti +RLEN−1), assign COEFFS[bi ][tj ]
                //      the value zero.
                Arrays.fill(COEFFS[bi], ti, RLEN, (short)0);
                //  (e) If SIGN is zero, assign COEFFS[bi ][ti + RLEN] the value 1.
                //  (f) Otherwise, assign COEFFS[bi ][ti + RLEN] the value −1.
                COEFFS[bi][ti+RLEN] = (short) ((SIGN==0) ? 1 : -1);
                //  (g) Assign TIS[bi ] the value TIS[bi ]+RLEN+1. Assign NCOEFFS[bi ]
                //      the value TIS[bi ].
                TIS[bi] = TIS[bi] + RLEN + 1;
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 30 : {
                //24. Otherwise, if TOKEN is 30:
                //  (a) Assign COEFFS[bi ][ti ] the value zero.
                COEFFS[bi][ti] = 0;
                //  (b) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (c) Read a 1-bit unsigned integer as MAG.
                //  (d) Assign MAG the value (MAG + 2).
                int MAG = ds.readBits(1) + 2;
                //  (e) If SIGN is zero, assign COEFFS[bi ][ti + 1] the value MAG.
                //  (f) Otherwise, assign COEFFS[bi ][ti + 1] the value −MAG.
                COEFFS[bi][ti+1] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (g) Assign TIS[bi ] the value TIS[bi ] + 2. Assign NCOEFFS[bi ] the
                //      value TIS[bi ].
                TIS[bi] = TIS[bi] + 2;
                NCOEFFS[bi] = TIS[bi];
                } break;
            case 31 : {
                //25. Otherwise, if TOKEN is 31:
                //  (a) Read a 1-bit unsigned integer as SIGN.
                int SIGN = ds.readBits(1);
                //  (b) Read a 1-bit unsigned integer as MAG.
                //  (c) Assign MAG the value (MAG + 2).
                int MAG = ds.readBits(1) + 2;
                //  (d) Read a 1-bit unsigned integer as RLEN.
                //  (e) Assign RLEN the value (RLEN + 2).
                int RLEN = ds.readBits(1) + 2;
                //  (f) For each value of tj from ti to (ti +RLEN−1), assign COEFFS[bi ][tj ]
                //      the value zero.
                Arrays.fill(COEFFS[bi], ti, RLEN, (short)0);
                //  (g) If SIGN is zero, assign COEFFS[bi ][ti + RLEN] the value MAG.
                //  (h) Otherwise, assign COEFFS[bi ][ti + RLEN] the value −MAG.
                COEFFS[bi][ti+RLEN] = (short) ((SIGN==0) ? MAG : -MAG);
                //  (i) Assign TIS[bi ] the value TIS[bi ]+RLEN+1. Assign NCOEFFS[bi ]
                //      the value TIS[bi ].
                TIS[bi] = TIS[bi] + RLEN + 1;
                NCOEFFS[bi] = TIS[bi];
                } break;
            default : throw new IOException("Unexpected TOKEN : "+TOKEN);
        }
        
    }

    /**
     * 7.7.3 DCT Coefficient Decode
     * Spec : page 112
     */
    private void DCTCoefficientDecode_7_7_3() throws IOException {
        ws.NCOEFFS = new int[ident.NBS];
        
        //1. Assign NLBS the value (NMBS ∗ 4).
        final int NLBS = ident.NMBS * 4;

        //2. For each consecutive value of bi from 0 to (NBS − 1), assign TIS[bi ] the
        //   value zero
        final int[] TIS = new int[ident.NBS];

        //3. Assign EOBS the value 0.
        int EOBS = 0;

        //4. For each consecutive value of ti from 0 to 63:
        int htiL = 0;
        int htiC = 0;
        for (int ti=0; ti<64; ti++) {

            if (ti==0 || ti==1) {
                //(a) If ti is 0 or 1:
                //  i. Read a 4-bit unsigned integer as hti L .
                htiL = ds.readBits(4);
                //  ii. Read a 4-bit unsigned integer as hti C .
                htiC = ds.readBits(4);
            }
            //(b) For each consecutive value of bi from 0 to (NBS − 1) for which
            //    BCODED[bi ] is non-zero and TIS[bi ] equals ti :
            for (int bi=0; bi<ident.NBS; bi++) {
                if (ws.BCODED[bi]!=0 && TIS[bi] == ti) {
                    //  i. Assign NCOEFFS[bi ] the value ti .
                    ws.NCOEFFS[bi] = ti;

                    if (EOBS>0) {
                        //  ii. If EOBS is greater than zero:
                        //     A. For each value of tj from ti to 63, assign COEFFS[bi ][tj ]
                        //       the value zero.
                        for (int tj=ti;tj<64;tj++) ws.COEFFS[bi][tj] = 0;
                        //     B. Assign TIS[bi ] the value 64.
                        TIS[bi] = 64;
                        //     C. Assign EOBS the value (EOBS − 1).
                        EOBS = EOBS - 1;
                    } else {
                        //  iii. Otherwise:
                        //     A. Assign HG a value based on ti from Table 7.42.
                        int HG;
                        if (ti==0) HG = 0;
                        else if (ti<6) HG = 1;
                        else if (ti<15) HG = 2;
                        else if (ti<28) HG = 3;
                        else if (ti<64) HG = 4;
                        else throw new IOException("Unvalid ti value");
                        //     B. If bi is less than NLBS, assign hti the value (16 ∗ HG + hti L ).
                        //     C. Otherwise, assign hti the value (16 ∗ HG + hti C ).
                        int hti = (bi<NLBS) ? (16*HG+htiL) : (16*HG+htiC);
                        //     D. Read one bit at a time until one of the codes in HTS[hti ] is
                        //       recognized, and assign the value to TOKEN.
                        int TOKEN = (Integer)setup.HTS[hti].decode(ds);

                        if (TOKEN < 7) {
                            //     E. If TOKEN is less than 7, expand an EOB token using the pro-
                            //       cedure given in Section 7.7.1 to update TIS[bi ], COEFFS[bi ],
                            //       and EOBS.
                            EOBS = EOBTokenDecode_7_7_1(ds,TOKEN,ident.NBS,TIS,ws.NCOEFFS,ws.COEFFS,bi,ti);
                        } else {
                            //     F. Otherwise, expand a coefficient token using the procedure
                            //       given in Section 7.7.2 to update TIS[bi ], COEFFS[bi ], and
                            //       NCOEFFS[bi ].
                            coefficientTokenDecode_7_7_2(ds,TOKEN,ident.NBS,TIS,ws.NCOEFFS,ws.COEFFS, bi, ti);
                        }
                    }
                }
            }
        }
        
        //verifications
        if (EOBS!=0) {
           throw new IOException("EOBS is not zero at procedure ends");
        }
        for(int i=0;i<TIS.length;i++) {
            if(TIS[i]!=64) {
                throw new IOException("TIS value is not 64 at procedure ends");
            }
        }
    }

    /**
     * 7.8.1 Computing the DC Predictor
     * Spec : page 115
     *
     * @return DCPRED
     */
    private int computingDCPredictor_7_8_1(int bi, int[] LASTDC) throws IOException {
        final TheoraBlockLocation biloc = ident.blockCodedOrder[bi];
        final int[] P = new int[4];
        final int[] PBI = new int[4];
        int DCPRED;

        //1. Assign mbi the index of the macro block containing block bi .
        final int mbi = ident.blockCodedOrder[bi].macroBlock.codedIndex;
        //2. Assign rfi the value of the Reference Frame Index column of Table 7.46
        //   corresponding to MBMODES[mbi ].
        final int rfi = TheoraConstants.T7_46[ws.MBMODES[mbi]];

        //3. If block bi is not along the left edge of the coded frame:
        if (biloc.x!=0) {
            //  (a) Assign bj the coded-order index of block bi ’s left neighbor, i.e., in
            //     the same row but one column to the left.
            final TheoraBlockLocation bjLoc = biloc.left;
            final int bj = bjLoc.codedIndex;

            if (ws.BCODED[bj] != 0) {
                //  (b) If BCODED[bj ] is not zero:
                //     i. Assign mbj the index of the macro block containing block bj .
                final int mbj = bjLoc.macroBlock.codedIndex;

                if (rfi == TheoraConstants.T7_46[ws.MBMODES[mbj]]) {
                    //     ii. If the value of the Reference Frame Index column of Table 7.46
                    //         corresonding to MBMODES[mbj ] equals rfi :
                    //        A. Assign P[0] the value 1.
                    P[0] = 1;
                    //        B. Assign PBI[0] the value bj .
                    PBI[0] = bj;
                } else {
                    //     iii. Otherwise, assign P[0] the value zero.
                    P[0] = 0;
                }

            } else {
                //  (c) Otherwise, assign P[0] the value zero.
                P[0] = 0;
            }
        } else {
            //4. Otherwise, assign P[0] the value zero.
            P[0] = 0;
        }

        //5. If block bi is not along the left edge nor the bottom edge of the coded
        //   frame:
        if (biloc.x!=0 && biloc.y!=0) {
            //  (a) Assign bj the coded-order index of block bi ’s lower-left neighbor, i.e.,
            //     one row down and one column to the left.
            final TheoraBlockLocation bjLoc = biloc.down.left;
            final int bj = bjLoc.codedIndex;

            if (ws.BCODED[bj] != 0) {
                //  (b) If BCODED[bj ] is not zero:
                //     i. Assign mbj the index of the macro block containing block bj .
                final int mbj = bjLoc.macroBlock.codedIndex;

                if (rfi == TheoraConstants.T7_46[ws.MBMODES[mbj]]) {
                    //     ii. If the value of the Reference Frame Index column of Table 7.46
                    //         corresonding to MBMODES[mbj ] equals rfi :
                    //        A. Assign P[1] the value 1.
                    P[1] = 1;
                    //        B. Assign PBI[1] the value bj .
                    PBI[1] = bj;
                } else {
                    //     iii. Otherwise, assign P[1] the value zero.
                    P[1] = 0;
                }
            } else {
                //  (c) Otherwise, assign P[1] the value zero.
                P[1] = 0;
            }
        } else {
            //6. Otherwise, assign P[1] the value zero.
            P[1] = 0;
        }

        //7. If block bi is not along the the bottom edge of the coded frame:
        if (biloc.y!=0) {
            //  (a) Assign bj the coded-order index of block bi ’s lower neighbor, i.e., in
            //     the same column but one row down.
            final TheoraBlockLocation bjLoc = biloc.down;
            final int bj = bjLoc.codedIndex;

            if (ws.BCODED[bj] != 0) {
                //  (b) If BCODED[bj ] is not zero:
                //     i. Assign mbj the index of the macro block containing block bj .
                final int mbj = bjLoc.macroBlock.codedIndex;

                if (rfi == TheoraConstants.T7_46[ws.MBMODES[mbj]]) {
                    //     ii. If the value of the Reference Frame Index column of Table 7.46
                    //         corresonding to MBMODES[mbj ] equals rfi :
                    //        A. Assign P[2] the value 1.
                    P[2] = 1;
                    //        B. Assign PBI[2] the value bj .
                    PBI[2] = bj;
                } else {
                    //     iii. Otherwise, assign P[2] the value zero.
                    P[2] = 0;
                }
            } else {
                //  (c) Otherwise, assign P[2] the value zero.
                P[2] = 0;
            }
        } else {
            //8. Otherwise, assign P[2] the value zero.
            P[2] = 0;
        }

        //9. If block bi is not along the right edge nor the bottom edge of the coded
        //   frame:
        if (biloc.right!=null && biloc.y!=0) {
            //  (a) Assign bj the coded-order index of block bi ’s lower-right neighbor,
            //      i.e., one row down and one column to the right.
            final TheoraBlockLocation bjLoc = biloc.down.right;
            final int bj = bjLoc.codedIndex;

            if (ws.BCODED[bj] != 0) {
                //  (b) If BCODED[bj ] is not zero:
                //      i. Assign mbj the index of the macro block containing block bj .
                final int mbj = bjLoc.macroBlock.codedIndex;

                if (rfi == TheoraConstants.T7_46[ws.MBMODES[mbj]]) {
                    //      ii. If the value of the Reference Frame Index column of Table 7.46
                    //         corresonding to MBMODES[mbj ] equals rfi :
                    //        A. Assign P[3] the value 1.
                    P[3] = 1;
                    //        B. Assign PBI[3] the value bj .
                    PBI[3] = bj;
                } else {
                    //      iii. Otherwise, assign P[3] the value zero.
                    P[3] = 0;
                }
            } else {
                //  (c) Otherwise, assign P[3] the value zero.
                P[3] = 0;
            }
        } else {
            //10. Otherwise, assign P[3] the value zero.
            P[3] = 0;
        }


        if (P[0]==0 && P[1]==0 && P[2]==0 && P[3]==0) {
            //11. If none of the values P[0], P[1], P[2], nor P[3] are non-zero, then assign
            //   DCPRED the value LASTDC[rfi ].
            DCPRED = LASTDC[rfi];
        } else {
            //12. Otherwise:
            final int[] W;
            final int PDIV;

            //  (a) Assign the array W and the variable PDIV the values from the row
            //     of Table 7.47 corresonding to the values of each P[i ].
                 if (P[0]==1 && P[1]==0 && P[2]==0 && P[3]==0) {W = new int[]{ 1,  0,  0, 0}; PDIV =   1;}
            else if (P[0]==0 && P[1]==1 && P[2]==0 && P[3]==0) {W = new int[]{ 0,  1,  0, 0}; PDIV =   1;}
            else if (P[0]==1 && P[1]==1 && P[2]==0 && P[3]==0) {W = new int[]{ 1,  0,  0, 0}; PDIV =   1;}
            else if (P[0]==0 && P[1]==0 && P[2]==1 && P[3]==0) {W = new int[]{ 0,  0,  1, 0}; PDIV =   1;}
            else if (P[0]==1 && P[1]==0 && P[2]==1 && P[3]==0) {W = new int[]{ 1,  0,  1, 0}; PDIV =   2;}
            else if (P[0]==0 && P[1]==1 && P[2]==1 && P[3]==0) {W = new int[]{ 0,  0,  1, 0}; PDIV =   1;}
            else if (P[0]==1 && P[1]==1 && P[2]==1 && P[3]==0) {W = new int[]{29,-26, 29, 0}; PDIV =  32;}
            else if (P[0]==0 && P[1]==0 && P[2]==0 && P[3]==1) {W = new int[]{ 0,  0,  0, 1}; PDIV =   1;}
            else if (P[0]==1 && P[1]==0 && P[2]==0 && P[3]==1) {W = new int[]{75,  0,  0,53}; PDIV = 128;}
            else if (P[0]==0 && P[1]==1 && P[2]==0 && P[3]==1) {W = new int[]{ 0,  1,  0, 1}; PDIV =   2;}
            else if (P[0]==1 && P[1]==1 && P[2]==0 && P[3]==1) {W = new int[]{75,  0,  0,53}; PDIV = 128;}
            else if (P[0]==0 && P[1]==0 && P[2]==1 && P[3]==1) {W = new int[]{ 0,  0,  1, 0}; PDIV =   1;}
            else if (P[0]==1 && P[1]==0 && P[2]==1 && P[3]==1) {W = new int[]{75,  0,  0,53}; PDIV = 128;}
            else if (P[0]==0 && P[1]==1 && P[2]==1 && P[3]==1) {W = new int[]{ 0,  3, 10, 3}; PDIV =  16;}
            else if (P[0]==1 && P[1]==1 && P[2]==1 && P[3]==1) {W = new int[]{29,-26, 29, 0}; PDIV =  32;}
            else throw new IOException("Unexpected");
            
            //  (b) Assign DCPRED the value zero.
            DCPRED = 0;

            //  (c) If P[0] is non-zero, assign DCPRED the value (DCPRED + W[0] ∗
            //     COEFFS[PBI[0]][0]).
            if (P[0] != 0) DCPRED += W[0] * ws.COEFFS[PBI[0]][0];
            //  (d) If P[1] is non-zero, assign DCPRED the value (DCPRED + W[1] ∗
            //     COEFFS[PBI[1]][0]).
            if (P[1] != 0) DCPRED += W[1] * ws.COEFFS[PBI[1]][0];
            //  (e) If P[2] is non-zero, assign DCPRED the value (DCPRED + W[2] ∗
            //     COEFFS[PBI[2]][0]).
            if (P[2] != 0) DCPRED += W[2] * ws.COEFFS[PBI[2]][0];
            //  (f) If P[3] is non-zero, assign DCPRED the value (DCPRED + W[3] ∗
            //     COEFFS[PBI[3]][0]).
            if (P[3] != 0) DCPRED += W[3] * ws.COEFFS[PBI[3]][0];

            //  (g) Assign DCPRED the value (DCPRED//PDIV).
            DCPRED = DCPRED / PDIV;
            //  (h) If P[0], P[1], and P[2] are all non-zero:
            if (P[0]!=0 && P[1]!=0 && P[2]!=0) {
                if ( Math.abs(DCPRED-ws.COEFFS[PBI[2]][0]) > 128 ) {
                    //     i. If |DCPRED−COEFFS[PBI[2]][0]| is greater than 128, assign
                    //        DCPRED the value COEFFS[PBI[2]][0].
                    DCPRED = ws.COEFFS[PBI[2]][0];
                } else if ( Math.abs(DCPRED - ws.COEFFS[PBI[0]][0]) > 128) {
                    //     ii. Otherwise, if |DCPRED − COEFFS[PBI[0]][0]| is greater than
                    //        128, assign DCPRED the value COEFFS[PBI[0]][0].
                    DCPRED = ws.COEFFS[PBI[0]][0];
                } else if ( Math.abs(DCPRED - ws.COEFFS[PBI[1]][0]) > 128) {
                    //     iii. Otherwise, if |DCPRED − COEFFS[PBI[1]][0]| is greater than
                    //        128, assign DCPRED the value COEFFS[PBI[1]][0].
                    DCPRED = ws.COEFFS[PBI[1]][0];
                }
            }
        }

        return DCPRED;
    }

    /**
     * 7.8.2 Inverting the DC Prediction Process
     * Spec : page 119
     */
    private void invertingDCPredictionProcess_7_8_2() throws IOException {
        
        //1. For each consecutive value of pli from 0 to 2:
        for (int pli=0;pli<3;pli++) {
            //  (a) Assign LASTDC[0] the value zero.
            //  (b) Assign LASTDC[1] the value zero.
            //  (c) Assign LASTDC[2] the value zero.
            final int[] LASTDC = new int[3];

            //  (d) For each block of color plane pli in raster order, with coded-order
            //       index bi :
            for (int ri=0;ri<ident.blockRasterOrder.length;ri++) {
                if (ident.blockRasterOrder[ri].type.planeIndex != pli) continue;
                final int bi = ident.blockRasterOrder[ri].codedIndex;
                //     i. If BCODED[bi ] is non-zero:
                if (ws.BCODED[bi] != 0) {
                    //       A. Compute the value DCPRED using the procedure outlined
                    //          in Section 7.8.1.
                    final int DCPRED = computingDCPredictor_7_8_1(bi, LASTDC);
                    //       B. Assign DC the value (COEFFS[bi ][0] + DCPRED).
                    int DC = ws.COEFFS[bi][0] + DCPRED;
                    //       C. Truncate DC to a 16-bit representation by dropping any
                    //          higher-order bits.
                    //       D. Assign COEFFS[bi ][0] the value DC.
                    ws.COEFFS[bi][0] = (short) DC;
                    //       E. Assign mbi the index of the macro block containing block bi .
                    final int mbi = ident.blockRasterOrder[ri].macroBlock.codedIndex;
                    //       F. Assign rfi the value of the Reference Frame Index column of
                    //          Table 7.46 corresponding to MBMODES[mbi ].
                    final int rfi = TheoraConstants.T7_46[ws.MBMODES[mbi]];
                    //       G. Assign LASTDC[rfi ] the value DC.
                    LASTDC[rfi] = DC;
                }
            }
        }
    }

    /**
     * 7.9.1.1 The Intra Predictor
     * Spec : page 122
     */
    private static int[][] intraPredicator_7_9_1_1() throws IOException {
        //1. For each value of by from 0 to 7, inclusive:
        //  (a) For each value of bx from 0 to 7, inclusive:
        //     i. Assign PRED[by][bx ] the value 128.
        final int[][] PRED = new int[8][8];
        for(int i=0;i<8;i++) Arrays.fill(PRED[i], 128);
        return PRED;
    }

    /**
     * 7.9.1.2 The Whole-Pixel Predictor
     * Spec : page 122
     */
    private static int[][] wholePixelPredicator_7_9_1_2(int RPW, int RPH, int[][] REFP, int BX, int BY, int MVX, int MVY) throws IOException {
        final int[][] PRED = new int[8][8];
        //1. For each value of by from 0 to 7, inclusive:
        for (int by=0; by<8; by++) {
            //  (a) Assign ry the value (BY + MVY + by).
            int ry = (BY + MVY + by);
            //  (b) If ry is greater than (RPH − 1), assign ry the value (RPH − 1).
            if (ry>(RPH-1)) ry = RPH - 1;
            //  (c) If ry is less than zero, assign ry the value zero.
            if (ry<0) ry = 0;
            //  (d) For each value of bx from 0 to 7, inclusive:
            for (int bx=0; bx<8; bx++) {
                //     i. Assign rx the value (BX + MVX + bx ).
                int rx = (BX + MVX + bx );
                //     ii. If rx is greater than (RPW−1), assign rx the value (RPW−1).
                if (rx>(RPW-1)) rx = RPW - 1;
                //     iii. If rx is less than zero, assign rx the value zero.
                if (rx<0) rx = 0;
                //     iv. Assign PRED[by][bx ] the value REFP[ry][rx ].
                PRED[by][bx] = REFP[ry][rx];
            }
        }
        return PRED;
    }

    /**
     * 7.9.1.3 The Half-Pixel Predictor
     * Spec : page 124
     *
     * @return PRED
     */
    private static int[][] halfPixelPredicator_7_9_1_3(int RPW, int RPH, int[][] REFP, int BX, int BY, int MVX1, int MVY1, int MVX2, int MVY2) throws IOException {
        final int[][] PRED = new int[8][8];

        //1. For each value of by from 0 to 7, inclusive:
        for (int by=0;by<8;by++) {
            //  (a) Assign ry1 the value (BY + MVY1 + by).
            int ry1 = (BY + MVY1 + by);
            //  (b) If ry1 is greater than (RPH − 1), assign ry1 the value (RPH − 1).
            if (ry1 > (RPH-1)) ry1 = RPH - 1;
            //  (c) If ry1 is less than zero, assign ry1 the value zero.
            if (ry1 < 0 ) ry1 = 0;

            //  (d) Assign ry2 the value (BY + MVY2 + by).
            int ry2 = (BY + MVY2 + by);
            //  (e) If ry2 is greater than (RPH − 1), assign ry2 the value (RPH − 1).
            if (ry2 > (RPH-1)) ry2 = RPH - 1;
            //  (f) If ry2 is less than zero, assign ry2 the value zero.
            if (ry2 < 0 ) ry2 = 0;

            //  (g) For each value of bx from 0 to 7, inclusive:
            for (int bx=0;bx<8;bx++) {
                //    i. Assign rx1 the value (BX + MVX1 + bx ).
                int rx1 = (BX + MVX1 + bx);
                //    ii. If rx1 is greater than (RPW −1), assign rx1 the value (RPW − 1).
                if (rx1 > (RPW-1)) rx1 = RPW - 1;
                //    iii. If rx1 is less than zero, assign rx1 the value zero.
                 if (rx1 < 0 ) rx1 = 0;

                //    iv. Assign rx2 the value (BX + MVX2 + bx ).
                int rx2 = (BX + MVX2 + bx);
                //    v. If rx2 is greater than (RPW −1), assign rx2 the value (RPW − 1).
                if (rx2 > (RPW-1)) rx2 = RPW - 1;
                //    vi. If rx2 is less than zero, assign rx2 the value zero.
                 if (rx2 < 0 ) rx2 = 0;
                //    vii. Assign PRED[by][bx ] the value (REFP[ry1 ][rx1 ] + REFP[ry2 ][rx2 ]) >> 1.
                PRED[by][bx] = (REFP[ry1][rx1] + REFP[ry2][rx2]) >> 1;
            }
        }
        return PRED;
    }

    /**
     * 7.9.2 Dequantization
     * Spec : page 126
     *
     * @return DQC
     */
    private short[] dequantization_7_9_2(short[][] COEFFS, int qti, int pli, int qi0, int qi, int bi) throws IOException {
        short[] DQC = new short[64];
        //1. Using ACSCALE, DCSCALE, BMS, NQRS, QRSIZES, QRBMIS,
        //   qti , pli , and qi0 , use the procedure given in Section 6.4.3 to compute the
        //   DC quantization matrix QMAT.
        int[] QMAT = QMATS[qti][pli][qi0];

        //2. Assign C the value COEFFS[bi ][0] ∗ QMAT[0].
        int C = COEFFS[bi][0] * QMAT[0];
        //3. Truncate C to a 16-bit representation by dropping any higher-order bits.
        //4. Assign DQC[0] the value C.
        DQC[0] = (short) C;
        //5. Using ACSCALE, DCSCALE, BMS, NQRS, QRSIZES, QRBMIS,
        //   qti , pli , and qi , use the procedure given in Section 6.4.3 to compute the
        //   AC quantization matrix QMAT.
        QMAT = QMATS[qti][pli][qi];
        //6. For each value of ci from 1 to 63, inclusive:
        for (int ci=1;ci<64;ci++) {
            //  (a) Assign zzi the index in zig-zag order corresponding to ci . E.g., the
            //     value at row (ci/8) and column (ci % 8) in Figure 2.8
            final int zzi = TheoraConstants.T2_8[ci/8][ci%8];
            //  (b) Assign C the value COEFFS[bi ][zzi ] ∗ QMAT[ci ].
            C = COEFFS[bi][zzi] * QMAT[ci];
            //  (c) Truncate C to a 16-bit representation by dropping any higher-order
            //     bits.
            //  (d) Assign DQC[ci ] the value C.
            DQC[ci] = (short) C;
        }
        return DQC;
    }

    /**
     * 7.9.3.1 1D Inverse DCT
     * Spec : page 129
     *
     * @return X
     */
    private static int[] inverseDCT1D_7_9_3_1(int[] Y) throws IOException {
        final int[] C = {0,64277,60547,54491,46341,36410,25080,12785};
        final int[] S = {0,12785,25080,36410,46341,54491,60547,64277};

        int[] X = new int[8];
        int[] T = new int[8];
        int R;

        //1. Assign T[0] the value Y[0] + Y[4].
        //2. Truncate T[0] to a 16-bit signed representation by dropping any higher-
        //   order bits.
        T[0] =  (Y[0] + Y[4]) & 0xFFFF;
        //3. Assign T[0] the value C4 ∗ T[0] >> 16.
        T[0] = (C[4] * T[0]) >> 16;
        //4. Assign T[1] the value Y[0] − Y[4].
        //5. Truncate T[1] to a 16-bit signed representation by dropping any higher-
        //   order bits.
        T[1] = (Y[0] - Y[4]) & 0xFFFF;
        //6. Assign T[1] the value C4 ∗ T[1] >> 16.
        T[1] = (C[4] * T[1]) >> 16;
        //7. Assign T[2] the value (C6 ∗ Y[2] >> 16) − (S6 ∗ Y[6] >> 16).
        T[2] = (C[6] * Y[2] >> 16) - (S[6] * Y[6] >> 16);
        //8. Assign T[3] the value (S6 ∗ Y[2] >> 16) + (C6 ∗ Y[6] >> 16).
        T[3] = (S[6] * Y[2] >> 16) + (C[6] * Y[6] >> 16);
        //9. Assign T[4] the value (C7 ∗ Y[1] >> 16) − (S7 ∗ Y[7] >> 16).
        T[4] = (C[7] * Y[1] >> 16) - (S[7] * Y[7] >> 16);
        //10. Assign T[5] the value (C3 ∗ Y[5] >> 16) − (S3 ∗ Y[3] >> 16).
        T[5] = (C[3] * Y[5] >> 16) - (S[3] * Y[3] >> 16);
        //11. Assign T[6] the value (S3 ∗ Y[5] >> 16) + (C3 ∗ Y[3] >> 16).
        T[6] = (S[3] * Y[5] >> 16) + (C[3] * Y[3] >> 16);
        //12. Assign T[7] the value (S7 ∗ Y[1] >> 16) + (C7 ∗ Y[7] >> 16).
        T[7] = (S[7] * Y[1] >> 16) + (C[7] * Y[7] >> 16);
        //13. Assign R the value T[4] + T[5].
        R =  T[4] + T[5];
        //14. Assign T[5] the value T[4] − T[5].
        //15. Truncate T[5] to a 16-bit signed representation by dropping any higher-
        //    order bits.
        T[5] = (T[4] - T[5]) & 0xFFFF;
        //16. Assign T[5] the value C4 ∗ T[5] >> 16.
        T[5] = (C[4] * T[5]) >> 16;
        //17. Assign T[4] the value R.
        T[4] = R;
        //18. Assign R the value T[7] + T[6].
        R =  T[7] + T[6];
        //19. Assign T[6] the value T[7] − T[6].
        //20. Truncate T[6] to a 16-bit signed representation by dropping any higher-
        //    order bits.
        T[6] = (T[7] - T[6]) & 0xFFFF;
        //21. Assign T[6] the value C4 ∗ T[6] >> 16.
        T[6] = (C[4] * T[6]) >> 16;
        //22. Assign T[7] the value R.
        T[7] = R;
        //23. Assign R the value T[0] + T[3].
        R = T[0] + T[3];
        //24. Assign T[3] the value T[0] − T[3].
        T[3] = T[0] - T[3];
        //25. Assign T[0] the value R.
        T[0] = R;
        //26. Assign R the value T[1] + T[2]
        R = T[1] + T[2];
        //27. Assign T[2] the value T[1] − T[2]
        T[2] = T[1] - T[2];
        //28. Assign T[1] the value R.
        T[1] = R;
        //29. Assign R the value T[6] + T[5].
        R = T[6] + T[5];
        //30. Assign T[5] the value T[6] − T[5].
        T[5] = T[6] - T[5];
        //31. Assign T[6] the value R.
        T[6] = R;
        //32. Assign R the value T[0] + T[7].
        R = T[0] + T[7];
        //33. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //34. Assign X[0] the value R.
        X[0] = R;
        //35. Assign R the value T[1] + T[6].
        R = T[1] + T[6];
        //36. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //37. Assign X[1] the value R.
        X[1] = R;
        //38. Assign R the value T[2] + T[5].
        R = T[2] + T[5];
        //39. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //40. Assign X[2] the value R.
        X[2] = R;
        //41. Assign R the value T[3] + T[4].
        R = T[3] + T[4];
        //42. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //43. Assign X[3] the value R.
        X[3] = R;
        //44. Assign R the value T[3] − T[4].
        R = T[3] - T[4];
        //45. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //46. Assign X[4] the value R.
        X[4] = R;
        //47. Assign R the value T[2] − T[5].
        R = T[2] - T[5];
        //48. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //49. Assign X[5] the value R.
        X[5] = R;
        //50. Assign R the value T[1] − T[6].
        R = T[1] - T[6];
        //51. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //52. Assign X[6] the value R.
        X[6] = R;
        //53. Assign R the value T[0] − T[7].
        R = T[0] - T[7];
        //54. Truncate R to a 16-bit signed representation by dropping any higher-order
        //    bits.
        R = R & 0xFFFF;
        //55. Assign X[7] the value R.
        X[7] = R;

        return X;
    }

    /**
     * 7.9.3.2 2D Inverse DCT
     * Spec : page 134
     *
     * @return RES
     */
    private static int[][] inverseDCT2D_7_9_3_2(short[] DQC) throws IOException {
        final int[][] RES = new int[8][8];
        final int[] Y = new int[8];

        //1. For each value of ri from 0 to 7:
        for (int ri=0;ri<8;ri++) {
            //  (a) For each value of ci from 0 to 7:
            for (int ci=0;ci<8;ci++) {
                //     i. Assign Y[ci ] the value DQC[ri ∗ 8 + ci ].
                Y[ci] = DQC[ri * 8 + ci];
            }
            //  (b) Compute X, the 1D inverse DCT of Y using the procedure described
            //     in Section 7.9.3.1.
            int[] X = inverseDCT1D_7_9_3_1(Y);
            //  (c) For each value of ci from 0 to 7:
            for (int ci=0;ci<8;ci++) {
                //    i. Assign RES[ri ][ci ] the value X[ci ].
                RES[ri][ci] = X[ci];
            }
        }
        //2. For each value of ci from 0 to 7:
        for (int ci=0;ci<8;ci++) {
            //  (a) For each value of ri from 0 to 7:
            for (int ri=0;ri<8;ri++) {
                //     i. Assign Y[ri ] the value RES[ri ][ci ].
                Y[ri] = RES[ri][ci];
            }
            //  (b) Compute X, the 1D inverse DCT of Y using the procedure described
            //     in Section 7.9.3.1.
            int[] X = inverseDCT1D_7_9_3_1(Y);
            //  (c) For each value of ri from 0 to 7:
            for (int ri=0;ri<8;ri++) {
                //    i. Assign RES[ri ][ci ] the value (X[ri ] + 8) >> 4.
                RES[ri][ci] = (X[ri] + 8) >> 4;
            }
        }

        return RES;
    }

    /**
     * 7.9.4 Complete Reconstruction Algorithm
     * Spec : page 119
     */
    private void completeReconstructionAlgorithm_7_9_4() throws IOException {
        int RPW;
        int RPH;
        int[][] REFP;
        int BX;
        int BY;
        int MVX1;
        int MVY1;
        int MVX2;
        int MVY2;
        int[] QMAT;
        int DC;
        int P;
        int qi;
        
        //1. Assign qi0 the value QIS[0].
        int qi0 = ws.QIS[0];
        //2. For each value of bi from 0 to (NBS − 1):
        for( int bi=0;bi<ident.NBS;bi++) {
            final int[][] PRED;
            final int[][] RES;
        
            final TheoraBlockLocation biLoc = ident.blockCodedOrder[bi];
            //  (a) Assign pli the index of the color plane block bi belongs to.
            int pli = ident.blockCodedOrder[bi].type.planeIndex;
            //  (b) Assign BX the horizontal pixel index of the lower-left corner of block
            //      bi .
            BX = biLoc.x*8;
            //  (c) Assign BY the vertical pixel index of the lower-left corner of block
            //      bi .
            BY = biLoc.y*8;
            //  (d) If BCODED[bi ] is non-zero:
            if (ws.BCODED[bi] != 0) {
                //    i. Assign mbi the index of the macro block containing block bi .
                int mbi = biLoc.macroBlock.codedIndex;
                //    ii. If MBMODES[mbi ] is 1 (INTRA), assign qti the value 0.
                //    iii. Otherwise, assign qti the value 1.
                int qti = (ws.MBMODES[mbi]==1) ? 0 : 1;
                //    iv. Assign rfi the value of the Reference Frame Index column of
                //        Table 7.46 corresponding to MBMODES[mbi ].
                int rfi = TheoraConstants.T7_46[ws.MBMODES[mbi]];

                //    v. If rfi is zero, compute PRED using the procedure given in Sec-
                //       tion 7.9.1.1.
                if (rfi==0) {
                    PRED = intraPredicator_7_9_1_1();
                } else {
                    //    vi. Otherwise:
                    //       A. Assign REFP, RPW, and RPH the values given in Table 7.75
                    //          corresponding to current value of rfi and pli .
                    if (rfi==1) {
                        switch (pli) {
                            case 0 :
                                REFP = ws.PREVREFY;
                                RPW = ws.RPYW;
                                RPH = ws.RPYH;
                                break;
                            case 1 :
                                REFP = ws.PREVREFCB;
                                RPW = ws.RPCW;
                                RPH = ws.RPCH;
                                break;
                            case 2 :
                                REFP = ws.PREVREFCR;
                                RPW = ws.RPCW;
                                RPH = ws.RPCH;
                                break;
                            default: throw new IOException("Unvalid pli value");
                        }
                    } else if (rfi==2) {
                        switch (pli) {
                            case 0 :
                                REFP = ws.GOLDREFY;
                                RPW = ws.RPYW;
                                RPH = ws.RPYH;
                                break;
                            case 1 :
                                REFP = ws.GOLDREFCB;
                                RPW = ws.RPCW;
                                RPH = ws.RPCH;
                                break;
                            case 2 :
                                REFP = ws.GOLDREFCR;
                                RPW = ws.RPCW;
                                RPH = ws.RPCH;
                                break;
                            default: throw new IOException("Unvalid pli value");
                        }
                    } else {
                        throw new IOException("Unvalid rfi value");
                    }
                    
                    //       B. Assign MVX the value
                    //          b|MVECTS[bi ] x |c ∗ sign(MVECTS[bi ] x ).
                    MVX1 = floor(abs(ws.MVECTS[bi][0])) * sign(ws.MVECTS[bi][0]);
                    //       C. Assign MVY the value
                    //          b|MVECTS[bi ] y |c ∗ sign(MVECTS[bi ] y ).
                    MVY1 = floor(abs(ws.MVECTS[bi][1])) * sign(ws.MVECTS[bi][1]);
                    //       D. Assign MVX2 the value
                    //          d|MVECTS[bi ] x |e ∗ sign(MVECTS[bi ] x ).
                    MVX2 = ceil(abs(ws.MVECTS[bi][0])) * sign(ws.MVECTS[bi][0]);
                    //       E. Assign MVY2 the value
                    //          d|MVECTS[bi ] y |e ∗ sign(MVECTS[bi ] y ).
                    MVY2 = ceil(abs(ws.MVECTS[bi][1])) * sign(ws.MVECTS[bi][1]);
                    
                    if (MVX1 == MVX2 && MVY2==MVY2) {
                        //       F. If MVX equals MVX2 and MVY equals MVY2, use the val-
                        //          ues REFP, RPW, RPH, BX, BY, MVX, and MVY, compute
                        //          PRED using the procedure given in Section 7.9.1.2.
                        PRED = wholePixelPredicator_7_9_1_2(RPW, RPH, REFP, BX, BY, MVX1, MVY2);
                    } else {
                        //       G. Otherwise, use the values REFP, RPW, RPH, BX, BY,
                        //          MVX, MVY, MVX2, and MVY2 to compute PRED using
                        //          the procedure given in Section 7.9.1.3.
                        PRED = halfPixelPredicator_7_9_1_3(RPW, RPH, REFP, BX, BY, MVX1, MVY1, MVX2, MVY2);
                    }
                }
                
                if (ws.NCOEFFS[bi] < 2) {
                    //    vii. If NCOEFFS[bi ] is less than 2:
                    //       A. Using ACSCALE, DCSCALE, BMS, NQRS,
                    //          QRSIZES, QRBMIS, qti , pli , and qi0 , use the procedure
                    //          given in Section 6.4.3 to compute the DC quantization matrix
                    //          QMAT.
                    QMAT = QMATS[qti][pli][qi0];
                    //       B. Assign DC the value
                    //          (COEFFS[bi ][0] ∗ QMAT[0] + 15) >> 5.
                    DC = (ws.COEFFS[bi][0] * QMAT[0] + 15) >> 5;
                    //       C. Truncate DC to a 16-bit signed representation by dropping
                    //          any higher-order bits.
                    DC = (short)DC; //& 0xFFFF;
                    //       D. For each value of by from 0 to 7, and each value of bx from
                    //          0 to 7, assign RES[by][bx ] the value DC.
                    RES = new int[8][8];
                    for (int by=0;by<8;by++) {
                        for (int bx=0;bx<8;bx++) {
                            RES[by][bx] = DC;
                        }
                    }
                } else {
                    //    viii. Otherwise:
                    //       A. Assign qi the value QIS[QIIS[bi ]].
                    qi = ws.QIS[ws.QIIS[bi]];
                    //       B. Using ACSCALE, DCSCALE, BMS, NQRS,
                    //          QRSIZES, QRBMIS, qti , pli , qi0 , and qi , compute DQC
                    //          using the procedure given in Section 7.9.2.
                    short[] DQC = dequantization_7_9_2(ws.COEFFS, qti, pli, qi0, qi, bi);
                    //       C. Using DQC, compute RES using the procedure given in Sec-
                    //          tion 7.9.3.2.
                    RES = inverseDCT2D_7_9_3_2(DQC);
                }
            } else {
                //  (e) Otherwise:
                //    i. Assign rfi the value 1.
                int rfi = 1;
                //    ii. Assign REFP, RPW, and RPH the values given in Table 7.75
                //        corresponding to current value of rfi and pli .
                if (rfi==1) {
                    switch (pli) {
                        case 0 :
                            REFP = ws.PREVREFY;
                            RPW = ws.RPYW;
                            RPH = ws.RPYH;
                            break;
                        case 1 :
                            REFP = ws.PREVREFCB;
                            RPW = ws.RPCW;
                            RPH = ws.RPCH;
                            break;
                        case 2 :
                            REFP = ws.PREVREFCR;
                            RPW = ws.RPCW;
                            RPH = ws.RPCH;
                            break;
                        default: throw new IOException("Unvalid pli value");
                    }
                } else if (rfi==2) {
                    switch (pli) {
                        case 0 :
                            REFP = ws.GOLDREFY;
                            RPW = ws.RPYW;
                            RPH = ws.RPYH;
                            break;
                        case 1 :
                            REFP = ws.GOLDREFCB;
                            RPW = ws.RPCW;
                            RPH = ws.RPCH;
                            break;
                        case 2 :
                            REFP = ws.GOLDREFCR;
                            RPW = ws.RPCW;
                            RPH = ws.RPCH;
                            break;
                        default: throw new IOException("Unvalid pli value");
                    }
                } else {
                    throw new IOException("Unvalid rfi value");
                }
                
                //    iii. Assign MVX the value 0.
                MVX1 = 0;
                //    iv. Assign MVY the value 0.
                MVY1 = 0;
                //    v. Using the values REFP, RPW, RPH, BX, BY, MVX, and MVY,
                //       compute PRED using the procedure given in Section 7.9.1.2.
                //       This is simply a copy of the co-located block in the previous
                //       reference frame.
                PRED = wholePixelPredicator_7_9_1_2(RPW, RPH, REFP, BX, BY, MVX1, MVY1);
                //    vi. For each value of by from 0 to 7, and each value of bx from 0 to
                //        7, assign RES[by][bx ] the value 0.
                RES = new int[8][8];
                for (int by=0;by<8;by++) {
                    for (int bx=0;bx<8;bx++) {
                        RES[by][bx] = 0;
                    }
                }
            }
            //  (f) For each value of by from 0 to 7, and each value of bx from 0 to 7:
            for (int by=0;by<8;by++) {
                for (int bx=0;bx<8;bx++) {
                    //    i. Assign P the value (PRED[by][bx ] + RES[by][bx ]).
                    P = (PRED[by][bx] + RES[by][bx]);
                    //    ii. If P is greater than 255, assign P the value 255.
                    if (P > 255) P = 255;
                    //    iii. If P is less than 0, assign P the value 0.
                    if (P < 0) P = 0;

                    if (pli == 0) {
                        //    iv. If pli equals 0, assign RECY[BY + by][BX + bx ] the value P.
                        ws.RECY[BY + by][BX + bx] = P;
                    } else if (pli == 1) {
                        //    v. Otherwise, if pli equals 1, assign RECB[BY + by][BX + bx ] the
                        //       value P.
                        ws.RECCB[BY + by][BX + bx] = P;
                    } else {
                        //    vi. Otherwise, pli equals 2, so assign RECR[BY + by][BX + bx ] the
                        //        value P.
                        ws.RECCR[BY + by][BX + bx] = P;
                    }
                }
            }
        }
    }

    /**
     * 7.10.1 Horizontal Filter
     * Spec : page 146
     */
    private static void horizontalFilter(int[][] RECP, int FX, int FY, int L) throws IOException {
        //1. For each value of by from 0 to 7:
        for (int by=0;by<8;by++) {
            //  (a) Assign R the value
            //     (RECP[FY + by][FX] − 3 ∗ RECP[FY + by][FX + 1]+
            //     3 ∗ RECP[FY + by][FX + 2] − RECP[FY + by][FX + 3] + 4) >> 3
            int R = (RECP[FY + by][FX] - 3 * RECP[FY + by][FX + 1]+ 3 * RECP[FY + by][FX + 2] - RECP[FY + by][FX + 3] + 4) >> 3;
            //  (b) Assign P the value (RECP[FY + by][FX + 1] + lflim(R, L)).
            int P = (RECP[FY + by][FX + 1] + lflim(R, L));
            //  (c) If P is less than zero, assign RECP[FY + by][FX + 1] the value zero.
            if (P < 0) {
                RECP[FY + by][FX + 1] = 0;
            } else if (P > 255) {
                //  (d) Otherwise, if P is greater than 255, assign RECP[FY + by][FX + 1]
                //      the value 255.
                RECP[FY + by][FX + 1] = 255;
            } else {
                //  (e) Otherwise, assign RECP[FY + by][FX + 1] the value P.
                RECP[FY + by][FX + 1] = P;
            }
            //  (f) Assign P the value (RECP[FY + by][FX + 2] − lflim(R, L)).
            P = (RECP[FY + by][FX + 2] - lflim(R, L));

            if (P < 0) {
                //  (g) If P is less than zero, assign RECP[FY + by][FX + 2] the value zero.
                RECP[FY + by][FX + 2] = 0;
            } else if (P > 255) {
                //  (h) Otherwise, if P is greater than 255, assign RECP[FY + by][FX + 2]
                //      the value 255.
                RECP[FY + by][FX + 2] = 255;
            } else {
                //  (i) Otherwise, assign RECP[FY + by][FX + 2] the value P.
                RECP[FY + by][FX + 2] = P;
            }
        }
    }

    /**
     * 7.10.2 Vertical Filter
     * Spec : page 147
     */
    private static void verticalFilter(int[][] RECP, int FX, int FY, int L) throws IOException {
        //1. For each value of bx from 0 to 7:
        for (int bx=0;bx<8;bx++) {
            //  (a) Assign R the value
            //     (RECP[FY][FX + bx ] − 3 ∗ RECP[FY + 1][FX + bx ]+
            //     3 ∗ RECP[FY + 2][FX + bx ] − RECP[FY + 3][FX + bx ] + 4) >> 3
            int R = (RECP[FY][FX + bx ] - 3 * RECP[FY + 1][FX + bx ]+ 3 * RECP[FY + 2][FX + bx ] - RECP[FY + 3][FX + bx ] + 4) >> 3;
            //  (b) Assign P the value (RECP[FY + 1][FX + bx ] + lflim(R, L)).
            int P = (RECP[FY + 1][FX + bx ] + lflim(R, L));

            if (P < 0) {
                //  (c) If P is less than zero, assign RECP[FY + 1][FX + bx ] the value zero.
                RECP[FY + 1][FX + bx] = 0;
            } else if (P > 255) {
                //  (d) Otherwise, if P is greater than 255, assign RECP[FY + 1][FX + bx ]
                //      the value 255.
                RECP[FY + 1][FX + bx] = 255;
            } else {
                //  (e) Otherwise, assign RECP[FY + 1][FX + bx ] the value P.
                RECP[FY + 1][FX + bx] = P;
            }

            //  (f) Assign P the value (RECP[FY + 2][FX + bx ] − lflim(R, L)).
            if (P < 0) {
                //  (g) If P is less than zero, assign RECP[FY + 2][FX + bx ] the value zero.
                RECP[FY + 2][FX + bx] = 0;
            } else if (P > 255) {
                //  (h) Otherwise, if P is greater than 255, assign RECP[FY + 2][FX + bx ]
                //      the value 255.
                RECP[FY + 2][FX + bx] = 255;
            } else {
                //  (i) Otherwise, assign RECP[FY + 2][FX + bx ] the value P.
                RECP[FY + 2][FX + bx] = P;
            }
        }
    }

    /**
     * 7.10.3 Complete Loop Filter
     * Spec : page 149
     */
    private void completeLoopFilter() throws IOException {
        int RPW;
        int RPH;
        int BX;
        int BY;
        int FX;
        int FY;
        
        //1. Assign L the value LFLIMS[QIS[0]].
        int L = setup.LFLIMS[ws.QIS[0]];
        
        //2. For each block in raster order, with coded-order index bi :
        for (int ri=0; ri<ident.blockRasterOrder.length;ri++) {
            final TheoraBlockLocation loc = ident.blockRasterOrder[ri];
            int bi = loc.codedIndex;
            // (a) If BCODED[bi ] is non-zero:
            if (ws.BCODED[bi] != 0) {
                //i. Assign pli the index of the color plane block bi belongs to.
                int pli = loc.type.planeIndex;
                //ii. Assign RECP, RPW, and RPH the values given in Table 7.85
                //   corresponding to the value of pli .
                int[][] RECP = (pli==0) ? ws.RECY : ((pli==1) ? ws.RECCB : ws.RECCR);
                RPW = (pli==0) ? ws.RPYW : ws.RPCW;
                RPH = (pli==0) ? ws.RPYH : ws.RPCH;
                //iii. Assign BX the horizontal pixel index of the lower-left corner of
                //   the block bi .
                BX = loc.x*8; 
                //iv. Assign BY the vertical pixel index of the lower-left corner of the
                //   block bi .
                BY = loc.y*8; 
                if (BX>0) {
                    //v. If BX is greater than zero:
                    //  A. Assign FX the value (BX − 2).
                    FX = (BX - 2);
                    //  B. Assign FY the value BY.
                    FY = BY;
                    //  C. Using RECP, FX, FY, and L, apply the horizontal block
                    //     filter to the left edge of block bi with the procedure described
                    //     in Section 7.10.1.
                    horizontalFilter(RECP, FX, FY, L);
                }
                if (BY>0) {
                    //vi. If BY is greater than zero:
                    //  A. Assign FX the value BX.
                    FX = BX;
                    //  B. Assign FY the value (BY − 2)
                    FY = (BY - 2);
                    //  C. Using RECP, FX, FY, and L, apply the vertical block filter
                    //     to the bottom edge of block bi with the procedure described
                    //     in Section 7.10.2.
                    verticalFilter(RECP, FX, FY, L);
                }
                //vii. If (BX + 8) is less than RPW and BCODED[bj ] is zero, where
                //   bj is the coded-order index of the block adjacent to bi on the
                //   right:
                if ( (BX + 8) < RPW && ws.BCODED[loc.right.codedIndex] == 0) {
                    //  A. Assign FX the value (BX + 6).
                    FX = BX + 6;
                    //  B. Assign FY the value BY.
                    FY = BY;
                    //  C. Using RECP, FX, FY, and L, apply the horizontal block fil-
                    //     ter to the right edge of block bi with the procedure described
                    //     in Section 7.10.1.
                    horizontalFilter(RECP, FX, FY, L);
                }
                //viii. If (BY + 8) is less than RPH and BCODED[bj ] is zero, where
                //   bj is the coded-order index of the block adjacent to bi above:
                if ( (BY+8) < RPH && ws.BCODED[loc.top.codedIndex] == 0) {
                    //  A. Assign FX the value BX.
                    FX = BX;
                    //  B. Assign FY the value (BY + 6)
                    FY = BY + 6;
                    //  C. Using RECP, FX, FY, and L, apply the vertical block filter
                    //     to the top edge of block bi with the procedure described in
                    //     Section 7.10.2.
                    verticalFilter(RECP, FX, FY, L);
                }
            }
        }
    }

    /**
     * Spec: page 146
     */
    private static int lflim(int R, int L) {
        if (R <= (-2*L)) return 0;
        else if ((-2*L) < R && R <= -L ) return -R - 2*L;
        else if (-L < R && R < L) return R;
        else if (L <= R && R < (2*L)) return -R + 2*L;
        else if ((2*L)<= R) return 0;
        else throw new MishandleException("Unexpected R,L values");
    }
    
    private static int sign(int value) {
        if (value<0) {
            return -1;
        } else if(value==0) {
            return 0;
        } else {
            return +1;
        }
    }

    private static int abs(int value) {
        return Math.abs(value);
    }
    
    private static int floor(int value) {
        return value;
    }
    
    private static int ceil(int value) {
        return value;
    }
    
}
