/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package science.unlicense.impl.media.theora;

import science.unlicense.impl.media.theora.Bits;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.io.IOException;

/**
 *
 * @author husky
 */
public class BitsTest {
    
    @Test
    public void rwTest() throws IOException {
        
        Bits bits = new Bits();
        bits.append(1, 3);
        bits.append(0, 1);
        bits.append(1, 1);
        bits.append(0, 8);
        bits.readMode();
        
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(1,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        Assert.assertEquals(0,bits.pollHeadBit());
        
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
//        Assert.assertEquals(0,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
//        Assert.assertEquals(1,bits.pollHeadBit());
    }
    
}
