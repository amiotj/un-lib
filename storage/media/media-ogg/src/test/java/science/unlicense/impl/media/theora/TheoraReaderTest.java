
package science.unlicense.impl.media.theora;

import science.unlicense.impl.media.theora.TheoraSetupHeader;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Johann Sorel
 */
public class TheoraReaderTest {
    
    @Test
    public void ilogTest() {
        assertEquals(0, TheoraSetupHeader.ilog(-1));
        assertEquals(0, TheoraSetupHeader.ilog(0));
        assertEquals(1, TheoraSetupHeader.ilog(1));
        assertEquals(2, TheoraSetupHeader.ilog(2));
        assertEquals(2, TheoraSetupHeader.ilog(3));
        assertEquals(3, TheoraSetupHeader.ilog(4));
        assertEquals(3, TheoraSetupHeader.ilog(7));
    }
    
}
