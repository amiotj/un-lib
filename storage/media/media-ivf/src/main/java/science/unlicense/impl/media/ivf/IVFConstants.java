

package science.unlicense.impl.media.ivf;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class IVFConstants {
    
    public static final Chars SIGNATURE = new Chars(new byte[]{'D','K','I','F'});
    
    private IVFConstants(){}
    
}
