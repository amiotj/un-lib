

package science.unlicense.impl.media.ivf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class IVFHeader implements IVFElement {

    /** 2 bytes : version, should be 0 */
    public int version;
    /** 2 bytes : length of header in bytes */
    public int length;
    /** 4 bytes : FourCC , 'VP80' */
    public Chars fourCC;
    /** 2 bytes : width in pixels */
    public int width;
    /** 2 bytes : height in pixels */
    public int height;
    /** 4 bytes : frame rate */
    public int frameRate;
    /** 4 bytes : time scale */
    public int timeScale;
    /** 4 bytes : number of frame in file */
    public int nbFrames;   
    /** 4 bytes : unused */                  
    public byte[] unused;
    
    public void read(DataInputStream ds) throws IOException {
        version = ds.readUShort();
        length = ds.readUShort();
        fourCC = new Chars(ds.readFully(new byte[4]));
        width = ds.readUShort();
        height = ds.readUShort();
        frameRate = ds.readInt();
        timeScale = ds.readInt();
        nbFrames = ds.readInt();
        unused = ds.readFully(new byte[4]);
    }
    
}
