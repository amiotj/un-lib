

package science.unlicense.impl.media.ivf;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * IVF is a simple container for VP8 raw data.
 * 
 * Specification : 
 * http://wiki.multimedia.cx/index.php?title=IVF
 *
 * @author Johann Sorel
 */
public class IVFMediaStore extends AbstractMediaStore{

    private final Path input;

    public IVFMediaStore(Path input) {
        super(IVFFormat.INSTANCE,input);
        this.input = input;
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
