

package science.unlicense.impl.media.ivf;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * IVF is a simple container for VP8 raw data.
 * 
 * Specification : 
 * http://wiki.multimedia.cx/index.php?title=IVF
 * 
 * @author Johann Sorel 
 */
public class IVFReader extends AbstractReader{
    
    private boolean headerRead = false;
    private IVFElement next = null;
    
    public boolean hasNext() throws IOException{
        findNext();
        return next != null;
    }
    
    public IVFElement next() throws IOException{
        findNext();
        if(next==null){
            throw new IOException("No more elements");
        }
        final IVFElement ele = next;
        next = null;
        return ele;
    }
    
    private void findNext() throws IOException {
        if(next!=null) return;
        
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        
        //check if we have something next
        final int b = ds.read();
        if(b==-1){
            //nothing left
            return;
        }
        final byte[] head = new byte[4];
        head[0] = (byte) b;
        ds.readFully(head, 1, 3);
        
        if(!headerRead){
            if(!Arrays.equals(head, IVFConstants.SIGNATURE.toBytes(CharEncodings.US_ASCII))){
                throw new IOException("Stream is not a valid IVF.");
            }
            //first iteration, the IVF header
            headerRead = true;
            next = new IVFHeader();
            next.read(ds);
        }else{
            next = new IVFFrame();
            ((IVFFrame)next).length = NumberEncoding.LITTLE_ENDIAN.readInt(head, 0);
            next.read(ds);
        }
    }
    
    
}
