
package science.unlicense.impl.media.ivf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;

/**
 *
 * @author Johann Sorel
 */
public class IVFFormat extends AbstractMediaFormat{

    public static final IVFFormat INSTANCE = new IVFFormat();

    public IVFFormat() {
        super(new Chars("ivf"),
              new Chars("IVF"),
              new Chars("IVF"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ivf")
              },
              new byte[][]{{'D','K','I','F'}});
    }
    
}
