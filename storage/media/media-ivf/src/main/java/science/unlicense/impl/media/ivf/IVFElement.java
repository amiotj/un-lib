
package science.unlicense.impl.media.ivf;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface IVFElement {
    
    void read(DataInputStream ds) throws IOException;
    
}
