

package science.unlicense.impl.media.wav;

import science.unlicense.impl.media.wav.WAVFormat;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.media.MediaFormat;
import science.unlicense.api.media.Medias;

/**
 *
 * @author Johann Sorel
 */
public class WAVFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final MediaFormat[] formats = Medias.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof WAVFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("WAV format not found.");
    }
}
