

package science.unlicense.impl.media.wav.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.model.Chunk;
import static science.unlicense.impl.media.wav.WAVMetaModel.*;

/**
 *
 * @author Johann Sorel
 */
public class FmtChunk extends DefaultTypedNode implements Chunk{

    private Chars fourCC;
    private long size;
    private long fileOffset;

    public FmtChunk() {
        super(MD_AUDIO);
    }

    public Chars getFourCC() {
        return fourCC;
    }

    public void setFourCC(Chars fourCC) {
        this.fourCC = fourCC;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getFileOffset() {
        return fileOffset;
    }

    public void setFileOffset(long fileOffset) {
        this.fileOffset = fileOffset;
    }

    public boolean isFixedSize() {
        return false;
    }

    public void computeSize() {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public void read(DataInputStream ds) throws IOException {
        final long offset = ds.getByteOffset();
        setAudioFormat(ds.readUShort());
        setAudioNbChannel(ds.readUShort());
        setAudioFrequency(ds.readUInt());
        setAudioBytePerSec(ds.readUInt());
        setAudioBytePerBlock(ds.readUShort());
        setAudioBitPerSample(ds.readUShort());
        ds.skipFully( (offset+size)-ds.getByteOffset() );
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public Integer getAudioFormat(){
        return (Integer) getOrCreateChild(MD_AUDIO_FORMAT).getValue();
    }

    public void setAudioFormat(Integer value){
        getOrCreateChild(MD_AUDIO_FORMAT).setValue(value);
    }

    public Integer getAudioNbChannel(){
        return (Integer) getOrCreateChild(MD_AUDIO_NBCHANNEL).getValue();
    }

    public void setAudioNbChannel(Integer value){
        getOrCreateChild(MD_AUDIO_NBCHANNEL).setValue(value);
    }

    public Long getAudioFrequency(){
        return (Long) getOrCreateChild(MD_AUDIO_FREQUENCY).getValue();
    }

    public void setAudioFrequency(Long value){
        getOrCreateChild(MD_AUDIO_FREQUENCY).setValue(value);
    }

    public Long getAudioBytePerSec(){
        return (Long) getOrCreateChild(MD_AUDIO_BYTEPERSEC).getValue();
    }

    public void setAudioBytePerSec(Long value){
        getOrCreateChild(MD_AUDIO_BYTEPERSEC).setValue(value);
    }

    public Integer getAudioBytePerBlock(){
        return (Integer) getOrCreateChild(MD_AUDIO_BYTEPERBLOCK).getValue();
    }

    public void setAudioBytePerBlock(Integer value){
        getOrCreateChild(MD_AUDIO_BYTEPERBLOCK).setValue(value);
    }

    public Integer getAudioBitPerSample(){
        return (Integer) getOrCreateChild(MD_AUDIO_BITPERSAMPLE).getValue();
    }

    public void setAudioBitPerSample(Integer value){
        getOrCreateChild(MD_AUDIO_BITPERSAMPLE).setValue(value);
    }


}
