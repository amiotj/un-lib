

package science.unlicense.impl.media.wav;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.AudioStreamMeta;
import science.unlicense.api.media.DefaultAudioStreamMeta;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.riff.RIFFReader;
import science.unlicense.impl.binding.riff.model.Chunk;
import science.unlicense.impl.binding.riff.model.RIFFChunk;
import science.unlicense.impl.media.wav.model.DataChunk;
import science.unlicense.impl.media.wav.model.FmtChunk;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class WAVMediaStore extends AbstractMediaStore {

    private final Path input;
    private RIFFChunk wavriff;
    private FmtChunk wavmeta;
    private DataChunk wavdata;

    public WAVMediaStore(Path input) {
        super(WAVFormat.INSTANCE,input);
        this.input = input;
    }

    /**
     * Parse file header to build metadatas
     */
    private void analyze() throws IOException{
        if(wavriff!=null) return;
        final RIFFReader reader = createReader();
        while(reader.hasNext()){
            final Chunk chunk = reader.next().getChunk();
            if(chunk instanceof RIFFChunk){
                wavriff = (RIFFChunk) chunk;
            }else if(chunk instanceof FmtChunk){
                wavmeta = (FmtChunk) chunk;
            }else if(chunk instanceof DataChunk){
                wavdata = (DataChunk) chunk;
            }
        }
    }

    public MediaStreamMeta[] getStreamsMeta() throws IOException {
        analyze();

        final int[] channels = WAVMetaModel.nbChannelToMapping(wavmeta.getAudioNbChannel());
        final int[] bitsPerSample = new int[channels.length];
        Arrays.fill(bitsPerSample, wavmeta.getAudioBitPerSample());

        final AudioStreamMeta audioMeta = new DefaultAudioStreamMeta(
                new Chars("stream"), channels ,bitsPerSample, wavmeta.getAudioFrequency());
        return new MediaStreamMeta[]{audioMeta};
    }

    public MediaReadStream createReader(MediaReadParameters params) throws IOException {
        analyze();
        final WAVReader reader = new WAVReader(wavriff,wavmeta,wavdata);
        reader.setInput(input);
        return reader;
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    private RIFFReader createReader() throws IOException{
        final Dictionary dico = new HashDictionary();
        dico.add(WAVMetaModel.FMT, FmtChunk.class);
        dico.add(WAVMetaModel.DATA, DataChunk.class);
        final RIFFReader reader = new RIFFReader(dico);
        reader.setInput(input);
        return reader;
    }

}
