

package science.unlicense.impl.media.wav.model;

import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.media.wav.WAVMetaModel;

/**
 *
 * @author Johann Sorel
 */
public class DataChunk extends DefaultChunk{

    public DataChunk() {
        super(WAVMetaModel.DATA);
    }

}
