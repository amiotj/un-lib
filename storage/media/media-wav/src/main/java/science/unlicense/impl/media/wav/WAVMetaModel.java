
package science.unlicense.impl.media.wav;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import static science.unlicense.api.media.AudioStreamMeta.*;
import science.unlicense.api.model.tree.DefaultNodeCardinality;
import science.unlicense.api.model.tree.NodeCardinality;

/**
 *
 * @author Johann Sorel
 */
public final class WAVMetaModel {

    /** FileFormatID */
    public static final Chars WAVE = new Chars(new byte[]{'W','A','V','E'});
    /** FormatBlocID */
    public static final Chars FMT = new Chars(new byte[]{'f','m','t',' '});
    /** DataBlocID */
    public static final Chars DATA = new Chars(new byte[]{'d','a','t','a'});


    /** MD_AUDIO_NBCHANEL value : Mono */
    public static final int CHANNEL_1 = 1;
    public static final int[] CHANNEL_1_MAPPING = {CHANNEL_MONO};
    /** MD_AUDIO_NBCHANEL value : Stereo */
    public static final int CHANNEL_2 = 2;
    public static final int[] CHANNEL_2_MAPPING = {CHANNEL_LEFT,CHANNEL_RIGHT};
    /** MD_AUDIO_NBCHANEL value : Left, Right, Center */
    public static final int CHANNEL_3 = 3;
    public static final int[] CHANNEL_3_MAPPING = {CHANNEL_LEFT,CHANNEL_CENTER,CHANNEL_RIGHT};
    /** MD_AUDIO_NBCHANEL value : Front left, Front right, Rear left, Rear right  */
    public static final int CHANNEL_4 = 4;
    public static final int[] CHANNEL_4_MAPPING = {CHANNEL_FRONT_LEFT,CHANNEL_FRONT_RIGHT,CHANNEL_REAR_LEFT,CHANNEL_REAR_RIGHT};
    /** MD_AUDIO_NBCHANEL value : Left, Center, Right, Surround */
    public static final int CHANNEL_5 = 5;
    public static final int[] CHANNEL_5_MAPPING = {CHANNEL_LEFT,CHANNEL_CENTER,CHANNEL_RIGHT,CHANNEL_SURROUND};
    /** MD_AUDIO_NBCHANEL value : Center left, Left, Center, Center right, right, Surround */
    public static final int CHANNEL_6 = 6;
    public static final int[] CHANNEL_6_MAPPING = {CHANNEL_CENTER_LEFT,CHANNEL_LEFT,CHANNEL_CENTER,CHANNEL_CENTER_RIGHT,CHANNEL_RIGHT,CHANNEL_SURROUND};


    public static final NodeCardinality MD_AUDIO;
    public static final NodeCardinality MD_AUDIO_BLOCKID;
    public static final NodeCardinality MD_AUDIO_SIZE;
    public static final NodeCardinality MD_AUDIO_FORMAT;
    public static final NodeCardinality MD_AUDIO_NBCHANNEL;
    public static final NodeCardinality MD_AUDIO_FREQUENCY;
    public static final NodeCardinality MD_AUDIO_BYTEPERSEC;
    public static final NodeCardinality MD_AUDIO_BYTEPERBLOCK;
    public static final NodeCardinality MD_AUDIO_BITPERSAMPLE;


    static {
        MD_AUDIO = new DefaultNodeCardinality(new Chars("Audio"), null, null,Sequence.class, null, false, new NodeCardinality[]{
            MD_AUDIO_BLOCKID = new DefaultNodeCardinality(new Chars("ChunkID"), null, null,Chars.class, false, null),
            MD_AUDIO_SIZE           = new DefaultNodeCardinality(new Chars("ChunkSize"), null, null,Long.class, false, null),
            MD_AUDIO_FORMAT         = new DefaultNodeCardinality(new Chars("AudioFormat"), null, null,Integer.class, false, null),
            MD_AUDIO_NBCHANNEL      = new DefaultNodeCardinality(new Chars("NbrCanaux"), null, null,Integer.class, false, null),
            MD_AUDIO_FREQUENCY      = new DefaultNodeCardinality(new Chars("Frequence"), null, null,Long.class, false, null),
            MD_AUDIO_BYTEPERSEC     = new DefaultNodeCardinality(new Chars("BytePerSec"), null, null,Long.class, false, null),
            MD_AUDIO_BYTEPERBLOCK   = new DefaultNodeCardinality(new Chars("BytePerBlock"), null, null,Integer.class, false, null),
            MD_AUDIO_BITPERSAMPLE   = new DefaultNodeCardinality(new Chars("BitsPerSample"), null, null,Integer.class, false, null)
        });
    }

    private WAVMetaModel() {
    }

    public static int[] nbChannelToMapping(final int nbChannel){
        if(nbChannel == 1){
            return CHANNEL_1_MAPPING;
        }else if(nbChannel == 2){
            return CHANNEL_2_MAPPING;
        }else if(nbChannel == 3){
            return CHANNEL_3_MAPPING;
        }else if(nbChannel == 4){
            return CHANNEL_4_MAPPING;
        }else if(nbChannel == 5){
            return CHANNEL_5_MAPPING;
        }else if(nbChannel == 6){
            return CHANNEL_6_MAPPING;
        }else{
            throw new InvalidArgumentException("Invalid number of channels : "+nbChannel);
        }
    }

}
