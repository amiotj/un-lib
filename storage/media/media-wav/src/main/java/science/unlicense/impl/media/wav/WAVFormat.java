
package science.unlicense.impl.media.wav;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaFormat;
import science.unlicense.api.media.MediaCapabilities;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class WAVFormat extends AbstractMediaFormat{

    public static final WAVFormat INSTANCE = new WAVFormat();

    public WAVFormat() {
        super(new Chars("wav"),
              new Chars("WAVE"),
              new Chars("Waveform Audio File Format"),
              new Chars[]{
                  new Chars("audio/vnd.wave"),
                  new Chars("audio/wav"),
                  new Chars("audio/wave"),
                  new Chars("audio/x-wav")
              },
              new Chars[]{
                new Chars("wav"),
                new Chars("wave")
              },
              new byte[][]{});
    }

    public MediaCapabilities getCapabilities() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaStore createStore(Object input) {
        return new WAVMediaStore((Path)input);
    }

}
