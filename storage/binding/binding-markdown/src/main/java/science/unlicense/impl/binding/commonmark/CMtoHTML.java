
package science.unlicense.impl.binding.commonmark;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.impl.binding.html.HTMLOutputStream;

import static science.unlicense.impl.binding.commonmark.CMConstants.*;

/**
 * Convert MarkDown to HTML.
 * 
 * @author Johann Sorel
 */
public class CMtoHTML extends AbstractWriter{

    public Chars writeToChars(CharArray md) throws IOException{
        final ArrayOutputStream bo = new ArrayOutputStream();
        setOutput(bo);
        write(md);
        return new Chars(bo.getBuffer().toArrayByte(),CharEncodings.UTF_8);
    }
    
    public void write(CharArray md) throws IOException{
        final CMReader reader = new CMReader();
        reader.setInput(new ArrayInputStream(md.toBytes()));
        final SyntaxNode ast = reader.read();
        write(ast);
    }


    public void write(SyntaxNode node) throws IOException{
        final HTMLOutputStream out = new HTMLOutputStream();
        out.setOutput(getOutputAsByteStream());

        final Sequence children = node.getChildrenByRule(new Chars("block"));
        for(int i=0,n=children.getSize();i<n;i++){
            final SyntaxNode block = (SyntaxNode) children.get(i);
            final SyntaxNode sn = (SyntaxNode) block.getChildren().get(0);
            final Chars ruleName = sn.getRule().getName();
            
            if(RULE_ATXHEADER.equals(ruleName)){
                final SyntaxNode dia = sn.getChildByRule(RULE_DIA16);
                final Chars text = sn.getChildByRule(RULE_INLINE).getTokensChars();
                final int nb = dia.getChildrenByToken(TOKEN_DIA).getSize();
                out.writeH(nb, text);

            }else if(RULE_CODEBLOCK.equals(ruleName)){
                throw new IOException("TODO");
            }else if(RULE_HRUL.equals(ruleName)){
                out.writeHr();
            }else if(RULE_LINE.equals(ruleName)){
                Chars text = sn.getTokensChars();
                writeLine(out, text);
            }else if(RULE_LINK.equals(ruleName)){
                throw new IOException("TODO");
            }else if(RULE_LISTITEM.equals(ruleName)){

                out.writeElementStart(null, new Chars("ul"));
                for(int k=i;k<n;k++){
                    if( ((SyntaxNode)(((SyntaxNode) children.get(k)).getChildren().get(0))).getRule().getName().equals(RULE_LISTITEM)){
                        final SyntaxNode li = (SyntaxNode) (((SyntaxNode) children.get(k)).getChildren().get(0));
                        final Chars text = li.getChildByRule(RULE_INLINE).getTokensChars();
                        out.writeElementStart(null, new Chars("li"));
                        out.writeText(text);
                        out.writeElementEnd(null, new Chars("li"));
                        i++;
                    }else{
                        break;
                    }
                }

                out.writeElementEnd(null, new Chars("ul"));

            }else if(RULE_SETEXHEADER.equals(ruleName)){
                throw new IOException("TODO");
            }else{
                throw new IOException("Unexpected rule "+ruleName);
            }

        }

    }

    protected void writeLine(HTMLOutputStream out, Chars text) throws IOException{
        out.writeText(text);
        out.writeBr();
    }

}
