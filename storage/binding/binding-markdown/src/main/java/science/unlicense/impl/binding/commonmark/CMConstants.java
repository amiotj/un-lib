
package science.unlicense.impl.binding.commonmark;

import science.unlicense.api.character.Chars;

/**
 * Common Mark constants
 * http://commonmark.org/
 * 
 * @author Johann Sorel
 */
public class CMConstants {

    public static final Chars RULE_SETEXHEADER = new Chars("setextheader");
    public static final Chars RULE_HRUL = new Chars("hrul");
    public static final Chars RULE_ATXHEADER = new Chars("atxheader");
    public static final Chars RULE_CODEBLOCK = new Chars("codeblock");
    public static final Chars RULE_LINK = new Chars("link");
    public static final Chars RULE_LISTITEM = new Chars("listitem");
    public static final Chars RULE_LINE = new Chars("line");

    public static final Chars RULE_DIA16 = new Chars("dia16");
    public static final Chars RULE_INLINE = new Chars("inline");

    public static final Chars TOKEN_DIA = new Chars("DIA");

    private CMConstants(){}

}
