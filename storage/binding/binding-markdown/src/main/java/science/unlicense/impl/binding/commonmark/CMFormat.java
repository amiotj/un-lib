
package science.unlicense.impl.binding.commonmark;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * Resources :
 * http://spec.commonmark.org/0.22/    Version 0.18 (2015-03-03)
 *
 * 
 * @author Johann Sorel
 */
public class CMFormat extends DefaultFormat{

    public static final CMFormat INSTANCE = new CMFormat();

    private CMFormat() {
        super(new Chars("MARKDOWN"),
              new Chars("MarkDwon"),
              new Chars("Common MarkDown"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("md"),
                  new Chars("markdown")
              },
              new byte[][]{});
    }
    
}
