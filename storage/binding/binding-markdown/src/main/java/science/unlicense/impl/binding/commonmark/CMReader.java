
package science.unlicense.impl.binding.commonmark;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CMReader extends AbstractReader {

    private static final OrderedHashDictionary TOKENS = new OrderedHashDictionary();
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    private static final Rule RULE_FILE;
    static {
        try{
            //parse grammar
            final UNGrammarReader reader = new UNGrammarReader();
            reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/cm.gr")));
            reader.read(TOKENS, RULES);
            RULE_FILE = (Rule) RULES.getValue(new Chars("file"));
            
            
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public SyntaxNode read() throws IOException{
        final ByteInputStream in = getInputAsByteStream(); 
        
        final Parser parser = new Parser(TOKENS,RULE_FILE,null);
        parser.setInput(in);
        return parser.parse();
    }
}
