
package science.unlicense.impl.binding.commonmark;

import science.unlicense.impl.binding.commonmark.CMtoHTML;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class CMtoHTMLTest {

    @Test
    public void test() throws IOException{

        final Chars md = new Chars("- one\n- two\n- three");

        Chars html = new CMtoHTML().writeToChars(md);
        html = html.replaceAll(new Chars(" "), Chars.EMPTY);
        html = html.replaceAll(new Chars("\n"), Chars.EMPTY);
        html = html.replaceAll(new Chars("\t"), Chars.EMPTY);

        Assert.assertEquals(new Chars("<ul><li>one</li><li>two</li><li>three</li></ul>"), html);

    }

}
