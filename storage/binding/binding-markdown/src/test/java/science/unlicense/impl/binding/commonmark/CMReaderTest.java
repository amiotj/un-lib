
package science.unlicense.impl.binding.commonmark;

import science.unlicense.impl.binding.commonmark.CMReader;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CMReaderTest {

    @Test
    public void plainTextTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/PlainText.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn);

    }

    @Test
    public void horizontalRuleTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/HorizontalRule.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn.toCharsTree(20));

    }

    @Test
    public void ATXHeaderTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/ATXHeader.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn.toCharsTree(20));

    }
    
    @Test
    public void CodeBlockTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/CodeBlock.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn.toCharsTree(20));

    }
    
    @Ignore
    @Test
    public void FencedCodeBlockTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/FencedCodeBlock.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn.toCharsTree(20));

    }

    @Test
    public void LinkTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/Link.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn.toCharsTree(20));

    }

    @Test
    public void ListTest() throws IOException{

        final CMReader reader = new CMReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/commonmark/List.md")));

        final SyntaxNode sn = reader.read();
        System.out.println(sn.toCharsTree(20));

    }

}
