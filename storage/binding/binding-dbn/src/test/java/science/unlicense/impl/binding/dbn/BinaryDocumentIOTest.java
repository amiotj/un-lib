
package science.unlicense.impl.binding.dbn;

import science.unlicense.impl.binding.dbn.DocumentReader;
import science.unlicense.impl.binding.dbn.BinaryFieldValueType;
import science.unlicense.impl.binding.dbn.DocumentWriter;
import science.unlicense.impl.binding.dbn.BinaryFieldType;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import org.junit.Assert;

/**
 *
 * @author Johann Sorel
 */
public class BinaryDocumentIOTest {
    
    @Test
    public void testReadWriteDocType() throws IOException{

        final BinaryFieldType fieldBool = BinaryFieldType.bool(
                new Chars("fieldBool"), new Chars("fieldBoolTitle"), new Chars("fieldBoolDesc"),
                1, 1, null,null);

        final FieldType[] fields = new FieldType[]{
            fieldBool
        };

        final DocumentType docType = new DefaultDocumentType(
                new Chars("typeId"),
                new Chars("typeTitle"),
                new Chars("typeDesc"),
                true, fields, null);
        final Document doc = new DefaultDocument(true, docType);
        doc.setFieldValue(new Chars("fieldBool"), true);

        
        final ArrayOutputStream out = new ArrayOutputStream();
        final DocumentWriter writer = new DocumentWriter();
        writer.setOutput(out);
        writer.write(doc);

        final DocumentReader reader = new DocumentReader(docType);
        reader.setInput(out.getBuffer().toArrayByte());
        final Document res = reader.read();

        Assert.assertEquals(doc, res);
    }

    @Test
    public void testHeritageReadWriteDocType() throws IOException{

        final FieldType[] fieldsupers = new FieldType[]{
            BinaryFieldType.text(new Chars("fieldSuper"), new Chars("fieldSuperTitle"), new Chars("fieldSuperDesc"), 1, 1, null, CharEncodings.UTF_8, null)
        };
        final DocumentType superType = new DefaultDocumentType(
                new Chars("superId"),
                new Chars("superTitle"),
                new Chars("superDesc"),
                true, fieldsupers, null);

        final FieldType[] fields = new FieldType[]{
            BinaryFieldType.bool(new Chars("fieldBool"), new Chars("fieldBoolTitle"), new Chars("fieldBoolDesc"), 1, 1, null,null)
        };
        final DocumentType docType = new DefaultDocumentType(
                new Chars("typeId"),
                new Chars("typeTitle"),
                new Chars("typeDesc"),
                true, fields, null, new DocumentType[]{superType});
        final Document doc = new DefaultDocument(true, docType);
        doc.setFieldValue(new Chars("fieldSuper"), new Chars("Super!"));
        doc.setFieldValue(new Chars("fieldBool"), true);


        final ArrayOutputStream out = new ArrayOutputStream();
        final DocumentWriter writer = new DocumentWriter();
        writer.setOutput(out);
        writer.write(doc);

        final DocumentReader reader = new DocumentReader(docType);
        reader.setInput(out.getBuffer().toArrayByte());
        final Document res = reader.read();

        Assert.assertEquals(doc, res);
    }

    @Test
    public void testReadWriteInline() throws IOException{

        //create types
        final FieldType dateField = BinaryFieldType.numeric(new Chars("date"), 1, 1, BinaryFieldValueType.Mapping.INT32LE.code, null, null);

        final DocumentType subType = new DefaultDocumentType(
                new Chars("superId"),
                new Chars("superTitle"),
                new Chars("superDesc"),
                true, new FieldType[]{dateField}, null);

        final BinaryFieldType fieldBool = BinaryFieldType.bool(
                new Chars("fieldBool"), new Chars("fieldBoolTitle"), new Chars("fieldBoolDesc"),
                1, 1, null,null);
        final BinaryFieldType fieldNotInline = BinaryFieldType.doc(new Chars("NotInline"), 1, 1, null, subType, false, null);
        final BinaryFieldType fieldInline = BinaryFieldType.doc(new Chars("inline"), 1, 1, null, subType, true, null);

        final DocumentType docType = new DefaultDocumentType(
                new Chars("typeId"),
                new Chars("typeTitle"),
                new Chars("typeDesc"),
                true, new FieldType[]{fieldBool,fieldNotInline,fieldInline}, null);

        //create document
        final Document subNotInline = new DefaultDocument(true, subType);
        subNotInline.setFieldValue(new Chars("date"), 42);
        final Document subInline = new DefaultDocument(true, subType);
        subInline.setFieldValue(new Chars("date"), 13);

        final Document doc = new DefaultDocument(true, docType);
        doc.setFieldValue(new Chars("fieldBool"), true);
        doc.setFieldValue(new Chars("NotInline"), subNotInline);
        doc.setFieldValue(new Chars("inline"), subInline);

        //write document
        final ArrayOutputStream out = new ArrayOutputStream();
        final DocumentWriter writer = new DocumentWriter();
        writer.setOutput(out);
        writer.write(doc);

        //check buffer
        final byte[] buffer = out.getBuffer().toArrayByte();
        Assert.assertEquals( 0,buffer[0]); //mode = 0 : streamed
        Assert.assertEquals( 1,buffer[1]); //fieldBool = true
        Assert.assertEquals( 0,buffer[2]); //mode = 0 : streamed , not inlined
        Assert.assertEquals(42,buffer[3]); //dateField = 42
        Assert.assertEquals( 0,buffer[4]);
        Assert.assertEquals( 0,buffer[5]);
        Assert.assertEquals( 0,buffer[6]);
        Assert.assertEquals(13,buffer[7]); //dateField = 13, inlined
        Assert.assertEquals( 0,buffer[8]);
        Assert.assertEquals( 0,buffer[9]);
        Assert.assertEquals( 0,buffer[10]);



        //read document
        final DocumentReader reader = new DocumentReader(docType);
        reader.setInput(out.getBuffer().toArrayByte());
        final Document res = reader.read();

        Assert.assertEquals(doc, res);
    }

}
