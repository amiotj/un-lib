
package science.unlicense.impl.binding.dbn;

import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class DBNWriter extends AbstractWriter {

    public void write(Document doc) throws IOException {
        write(doc,true,0);
    }

    public void write(Document doc, boolean writeType, int mode) throws IOException {

        final DataOutputStream ds = getOutputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        ds.write(DBNFormat.SIGNATURE);
        ds.writeUInt(1);
        if (writeType) {
            DocumentWriter.writeDoc(ds, BinaryDocuments.toDocument(doc.getType()),mode,false);
        }
        DocumentWriter.writeDoc(ds,doc,mode,false);
    }

}
