
package science.unlicense.impl.binding.dbn;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 *
 * @author Johann Sorel
 */
public class BinaryFieldTypeBuilder extends FieldTypeBuilder{

    protected BinaryFieldValueType valueType = null;

    public BinaryFieldTypeBuilder(){

    }

    public BinaryFieldTypeBuilder id(Chars id) {
        super.id(id);
        return this;
    }

    public BinaryFieldTypeBuilder title(CharArray title) {
        super.title(title);
        return this;
    }

    public BinaryFieldTypeBuilder description(CharArray description) {
        super.description(description);
        return this;
    }

    @Override
    public BinaryFieldTypeBuilder valueClass(Class valueClass) {
        valueType = BinaryFieldValueType.forValueClass(valueClass);
        super.valueClass(valueClass);
        return this;
    }

    public BinaryFieldTypeBuilder defaultValue(Object defaultValue) {
        super.defaultValue(defaultValue);
        return this;
    }

    public BinaryFieldTypeBuilder minOcc(int minOcc) {
        super.minOcc(minOcc);
        return this;
    }

    public BinaryFieldTypeBuilder maxOcc(int maxOcc) {
        super.maxOcc(maxOcc);
        return this;
    }

    public BinaryFieldTypeBuilder ref(DocumentType ref) {
        valueType = BinaryFieldValueType.doc(ref, false);
        super.ref(ref);
        return this;
    }

    public BinaryFieldTypeBuilder attributes(Dictionary attributes) {
        super.attributes(attributes);
        return this;
    }

    public BinaryFieldTypeBuilder constraints(Sequence constraints) {
        super.constraints(constraints);
        return this;
    }

    public BinaryFieldTypeBuilder type(BinaryFieldValueType valueType){
        this.valueType = valueType;
        this.valueClass = valueType.getValueClass();
        return this;
    }


    public BinaryFieldType build() {
        return new BinaryFieldType(id, title, description, minOcc, maxOcc, defaultValue, valueType, attributes);
    }

}
