
package science.unlicense.impl.binding.dbn;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class DBNReader extends AbstractReader {

    private DocumentType docType;

    public DBNReader(){
        this(null);
    }

    /**
     *
     * @param docType
     */
    public DBNReader(DocumentType docType){
        this.docType = docType;
    }

    public DocumentType getDocumentType() throws IOException {
        readHeader();
        return docType;
    }

    public Document read() throws IOException {
        readHeader();
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        return DocumentReader.readDoc(ds, docType,false);
    }

    public BinaryStreamDocumentReader AsStream() throws IOException {
        readHeader();
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        final BinaryStreamDocumentReader reader = new BinaryStreamDocumentReader(docType) {
            @Override
            public void dispose() throws IOException {
                super.dispose();
                DBNReader.this.dispose();
            }
        };
        reader.setInput(ds);
        return reader;
    }

    private void readHeader() throws IOException {
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        if (!Arrays.equals(DBNFormat.SIGNATURE,ds.readBytes(4))) {
            throw new IOException("Unvalid file signature.");
        }
        final long version = ds.readUInt();
        if (version != 1) {
            throw new IOException("Unsupported version "+version);
        }
        final Document ddType = DocumentReader.readDoc(ds, BinaryDocuments.DOCTYPE,false);
        docType = BinaryDocuments.toDocumentType(ddType);
        //skip any remaining bits
        ds.skipToByteEnd();
    }

}
