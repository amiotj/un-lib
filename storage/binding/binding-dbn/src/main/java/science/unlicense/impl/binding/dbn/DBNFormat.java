
package science.unlicense.impl.binding.dbn;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * DBN Format.
 *
 * Specification :
 * http://unlicense.developpez.com/specifications/index.html
 *
 *
 * @author Johann Sorel
 */
public class DBNFormat extends DefaultFormat {

    public static final byte[] SIGNATURE = new byte[]{'D','B','N',' '};

    public DBNFormat() {
        super(new Chars("dbn"),
              new Chars("DBN"),
              new Chars("Document as binary"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("dbn")
              },
              new byte[][]{
                  SIGNATURE
                });
    }

}
