
package science.unlicense.impl.binding.dbn;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.impl.binding.dbn.BinaryFieldValueType.Mapping;
import static science.unlicense.impl.binding.dbn.BinaryFieldValueType.chars;
import static science.unlicense.impl.binding.dbn.BinaryFieldValueType.doc;
import static science.unlicense.impl.binding.dbn.BinaryFieldValueType.numeric;

/**
 *
 * @author Johann Sorel
 */
public final class BinaryDocuments {
    
    private static final Chars ATT_ID = new Chars("id");
    private static final Chars ATTX_TITLE = new Chars("title");
    private static final Chars ATTX_DESCRIPTION = new Chars("description");
    private static final Chars ATT_MINOCC = new Chars("minocc");
    private static final Chars ATT_MAXOCC = new Chars("maxocc");
    private static final Chars ATT_NBOCC = new Chars("nbocc");
    private static final Chars ATT_SIZE = new Chars("size");
    private static final Chars ATT_PTYPE = new Chars("ptype");
    private static final Chars ATT_DOCTYPE = new Chars("doctype");
    private static final Chars ATT_INLINE = new Chars("inline");
    private static final Chars ATT_CHARENC = new Chars("charenc");
    private static final Chars ATT_PARENTS = new Chars("parents");
    private static final Chars ATT_FIELDS = new Chars("fields");
    private static final Chars ATT_ATTRIBUTES = new Chars("attributes");
    private static final Chars ATT_VALUETYPE = new Chars("valuetype");
    private static final Chars ATT_VALUEDEFAULT = new Chars("default");
    private static final Chars ATT_ATTVALUE = new Chars("value");
    private static final Chars ATT_DOCS = new Chars("docs");

    final BinaryFieldType fieldTitle = BinaryFieldType.text(ATTX_TITLE, 0, 1, null, CharEncodings.UTF_8, null);
    final BinaryFieldType fieldDescription = BinaryFieldType.text(ATTX_DESCRIPTION, 0, 1, null, CharEncodings.UTF_8, null);

    
    public static final DocumentType FIELDTYPE;
    public static final DocumentType FIELDVALUETYPE;
    public static final DocumentType ATTRIBUTETYPE;
    public static final DocumentType DOCTYPE;
    public static final DocumentType SUBDOCTYPE;
    static {
        BinaryDocumentTypeBuilder dtb;

        final BinaryFieldTypeBuilder fieldId = new BinaryFieldTypeBuilder().id(ATT_ID).type(chars(CharEncodings.UTF_8));
        
        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("FieldValueType"));
        dtb.addField(ATT_PTYPE).type(numeric(Mapping.INT8));
        dtb.addField(ATT_SIZE).type(numeric(Mapping.ULEB128,new long[]{0})).minOcc(0);
        dtb.addField(ATT_DOCTYPE).type(numeric(Mapping.INT32LE)).minOcc(0);
        dtb.addField(ATT_INLINE).type(numeric(Mapping.BOOL_BYTE)).minOcc(0);
        dtb.addField(ATT_CHARENC).type(chars(CharEncodings.UTF_8)).minOcc(0);
        FIELDVALUETYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("AttributeType"));
        dtb.addField(fieldId);
        dtb.addField(ATT_NBOCC).type(numeric(Mapping.ULEB128));
        dtb.addField(ATT_VALUETYPE).type(doc(FIELDVALUETYPE,true));
        dtb.addField(ATT_ATTVALUE).type(numeric(Mapping.INT8,new long[]{0})).minOcc(0);
        ATTRIBUTETYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("FieldType"));
        dtb.addField(fieldId);
        dtb.addField(ATT_MINOCC).type(numeric(Mapping.INT32LE));
        dtb.addField(ATT_MAXOCC).type(numeric(Mapping.INT32LE));
        dtb.addField(ATT_ATTRIBUTES).type(doc(ATTRIBUTETYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        dtb.addField(ATT_VALUETYPE).type(doc(FIELDVALUETYPE,true));
        dtb.addField(ATT_VALUEDEFAULT).type(numeric(Mapping.INT8,new long[]{0})).minOcc(0);
        FIELDTYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("DocumentType"));
        dtb.addField(fieldId);
        dtb.addField(ATT_PARENTS).type(numeric(Mapping.INT32LE)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        dtb.addField(ATT_FIELDS).type(doc(FIELDTYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        dtb.addField(ATT_ATTRIBUTES).type(doc(ATTRIBUTETYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        SUBDOCTYPE = dtb.build();

        dtb = new BinaryDocumentTypeBuilder();
        dtb.id(new Chars("RegisterType"));
        dtb.addField(ATT_DOCS).type(doc(SUBDOCTYPE,false)).minOcc(0).maxOcc(Integer.MAX_VALUE);
        DOCTYPE = dtb.build();
        
    }
    
    private BinaryDocuments(){}
    
    
    public static DocumentType toDocumentType(Document doc){
        
        final Sequence subdocs = (Sequence) doc.getFieldValue(ATT_DOCS);

        final Sequence types = new ArraySequence();
        for (int i=0,n=subdocs.getSize();i<n;i++) {
            final Document sdoc = (Document) subdocs.get(i);
            final Chars id = (Chars) sdoc.getFieldValue(ATT_ID);
            final Sequence supers = (Sequence) sdoc.getFieldValue(ATT_PARENTS);
            final Sequence fields = (Sequence) sdoc.getFieldValue(ATT_FIELDS);
            final BinaryFieldType[] fieldTypes = new BinaryFieldType[fields.getSize()];
            final DocumentType[] parents = new DocumentType[supers.getSize()];

            Chars title = null;
            Chars desc = null;
            final Dictionary attMap = new HashDictionary();
            final Sequence attributes = (Sequence) sdoc.getFieldValue(ATT_ATTRIBUTES);
            if (!attributes.isEmpty()) {
                try {
                    for (int k=0,kn=attributes.getSize();k<kn;k++) {
                        final Pair att = toAttribute((Document)attributes.get(k));
                        if(ATTX_TITLE.equals(att.getValue1())){
                            title = (Chars) att.getValue2();
                        }else if(ATTX_DESCRIPTION.equals(att.getValue1())){
                            desc = (Chars) att.getValue2();
                        }else{
                            attMap.add(att.getValue1(), att.getValue2());
                        }
                    }
                } catch(IOException ex) {
                    throw new RuntimeException(ex.getMessage(),ex);
                }
            }

            final DocumentType type = new BinaryDocumentType(id, title, desc, true, fieldTypes, attMap, parents);
            types.add(type);
        }

        //second pass to build attributes
        for (int i=0,n=subdocs.getSize();i<n;i++) {
            final Document sdoc = (Document) subdocs.get(i);
            final Sequence fields = (Sequence) sdoc.getFieldValue(ATT_FIELDS);
            final FieldType[] fieldTypes = ((BinaryDocumentType)types.get(i)).getFieldsInternal();
            for(int k=0;k<fieldTypes.length;k++){
                fieldTypes[k] = toFieldType((Document) fields.get(k),types);
            }
        }

        //third pass to build heritage
        for (int i=0,n=subdocs.getSize();i<n;i++) {
            final Document sdoc = (Document) subdocs.get(i);
            final Sequence supers = (Sequence) sdoc.getFieldValue(ATT_PARENTS);
            final BinaryDocumentType type = (BinaryDocumentType) types.get(i);
            for (int k=0,kn=supers.getSize(); k<kn; k++) {
                type.getSuperInternal()[k] = (DocumentType) types.get((Integer)supers.get(k));
            }
            //rebuild index
            type.reindexInternal();
        }

        
        return (DocumentType) types.get(0);
    }

    public static Pair toAttribute(Document doc) throws IOException{

        final Chars id = (Chars) doc.getFieldValue(ATT_ID);
        final Long nbOcc = (Long) doc.getFieldValue(ATT_NBOCC);
        final BinaryFieldValueType valueType = toFieldValueType((Document) doc.getFieldValue(ATT_VALUETYPE));
        final byte[] array = (byte[])doc.getFieldValue(ATT_ATTVALUE);
        final DataInputStream ds = new DataInputStream(new ArrayInputStream(array));
        final Object value;
        if (nbOcc==1) {
            value = DocumentReader.readFieldValue(ds, valueType);
        } else {
            final Sequence seq = new ArraySequence(nbOcc.intValue());
            for (int i=0;i<nbOcc;i++) {
                seq.add(DocumentReader.readFieldValue(ds, valueType));
            }
            value = seq;
        }
        return new Pair(id, value);
    }

    private static BinaryFieldType toFieldType(Document doc, Sequence stack){
        
        final Chars id = (Chars) doc.getFieldValue(ATT_ID);
        final int minOcc = (Integer) doc.getFieldValue(ATT_MINOCC);
        final int maxOcc = (Integer) doc.getFieldValue(ATT_MAXOCC);
        final Document valueTypeDoc = (Document) doc.getFieldValue(ATT_VALUETYPE);

        Chars title = null;
        Chars desc = null;
        final Dictionary attMap = new HashDictionary();
        final Sequence attributes = (Sequence) doc.getFieldValue(ATT_ATTRIBUTES);
        if (attributes!=null && !attributes.isEmpty()) {
            try {
                for (int k=0,kn=attributes.getSize();k<kn;k++) {
                    final Pair att = toAttribute((Document)attributes.get(k));
                    if(ATTX_TITLE.equals(att.getValue1())){
                        title = (Chars) att.getValue2();
                    }else if(ATTX_DESCRIPTION.equals(att.getValue1())){
                        desc = (Chars) att.getValue2();
                    }else{
                        attMap.add(att.getValue1(), att.getValue2());
                    }
                }
            } catch(IOException ex) {
                throw new RuntimeException(ex.getMessage(),ex);
            }
        }

        final BinaryFieldValueType valueType = toFieldValueType(valueTypeDoc, stack);
        return new BinaryFieldType(id,title,desc, minOcc, maxOcc, null, valueType, attMap);
    }

    public static BinaryFieldValueType toFieldValueType(Document doc){
        return toFieldValueType(doc, null);
    }

    private static BinaryFieldValueType toFieldValueType(Document doc, Sequence stack){

        final int ptype = ((Byte) doc.getFieldValue(ATT_PTYPE)) & 0xFF;
        final long[] size = (long[]) doc.getFieldValue(ATT_SIZE);
        final Integer reftype = (Integer) doc.getFieldValue(ATT_DOCTYPE);
        final Boolean inline = (Boolean) doc.getFieldValue(ATT_INLINE);
        final Chars charenc = (Chars) doc.getFieldValue(ATT_CHARENC);

        if(reftype!=null){
            final DocumentType docType = (DocumentType) stack.get(reftype);
            return new BinaryFieldValueType(ptype, null,  docType, size,inline);
        }else if(ptype == Mapping.CHARS.code){
            return new BinaryFieldValueType(ptype, CharEncodings.find(charenc),  null, size,true);
        }else if(ptype == Mapping.BOOL_BYTE.code){
            return new BinaryFieldValueType(ptype, null, null, size,true);
        }else{
            return new BinaryFieldValueType(ptype, null, null, size,true);
        }
    }
    
    public static Document toDocument(DocumentType docType){
        final Document doc = new DefaultDocument(true, DOCTYPE);
        final Sequence allTypes = new ArraySequence();
        final Sequence alldocs = new ArraySequence();
        toDocument(docType,allTypes,alldocs);
        doc.setFieldValue(ATT_DOCS,alldocs);
        return doc;
    }
    
    private static Document toDocument(DocumentType docType, Sequence alltypes, Sequence alldocs) {
        
        final Document doc = new DefaultDocument(true, SUBDOCTYPE);
        doc.setFieldValue(ATT_ID, docType.getId());
        
        alltypes.add(docType);
        alldocs.add(doc);

        //convert parent types
        final DocumentType[] parents = docType.getSuper();
        final Sequence supers = new ArraySequence();
        for (int i=0; i<parents.length; i++) {
            final DocumentType refType = parents[i];
            int idx = alltypes.search(refType);
            if (idx<0) {
               toDocument(refType,alltypes,alldocs);
               idx = alltypes.search(refType);
            }
            supers.add(idx);
        }
        doc.setFieldValue(ATT_PARENTS, supers);

        //convert fields
        final Sequence fields = new ArraySequence();
        for (FieldType fieldType : docType.getLocalFields()) {
            fields.add(toDocument(fieldType,alltypes,alldocs));
        }
        doc.setFieldValue(ATT_FIELDS, fields);

        //convert attributes
        final Sequence attributes = new ArraySequence();
        try {
            if(docType.getTitle()!=null) attributes.add(attributeToDocument(ATTX_TITLE, docType.getTitle()));
            if(docType.getDescription()!=null) attributes.add(attributeToDocument(ATTX_DESCRIPTION, docType.getDescription()));
            final Dictionary atts = docType.getAttributes();
            final Iterator ite = atts.getPairs().createIterator();
            while(ite.hasNext()){
                final Pair pair = (Pair) ite.next();
                attributes.add(attributeToDocument((Chars) pair.getValue1(), pair.getValue2()));
            }
        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        doc.setFieldValue(ATT_ATTRIBUTES, attributes);


        return doc;
    }
    
    private static Document toDocument(FieldType fieldType, Sequence alltypes, Sequence alldocs) {
        final BinaryFieldType bft = (BinaryFieldType) fieldType;
        final Document doc = new DefaultDocument(true, FIELDTYPE);
        doc.setFieldValue(ATT_ID, bft.getId());
        doc.setFieldValue(ATT_MINOCC, bft.getMinOccurences());
        doc.setFieldValue(ATT_MAXOCC, bft.getMaxOccurences());
        doc.setFieldValue(ATT_VALUETYPE, toDocument(bft.getValueType(), alltypes, alldocs));

        final Object def = bft.getDefaultValue();
        if (def!=null) {
            final ArrayOutputStream out = new ArrayOutputStream();
            try {
                DocumentWriter.writeSingleValue(new DataOutputStream(out), bft.getValueType(), def);
                doc.setFieldValue(ATT_VALUEDEFAULT, out.getBuffer().toArrayByte());
            } catch (IOException ex) {
                //won't happen
                throw new RuntimeException("Should not happen");
            }
        }


        //convert attributes
        final Sequence attributes = new ArraySequence();
        try {
            if(fieldType.getTitle()!=null) attributes.add(attributeToDocument(ATTX_TITLE, fieldType.getTitle()));
            if(fieldType.getDescription()!=null) attributes.add(attributeToDocument(ATTX_DESCRIPTION, fieldType.getDescription()));
            final Dictionary atts = fieldType.getAttributes();
            final Iterator ite = atts.getPairs().createIterator();
            while(ite.hasNext()){
                final Pair pair = (Pair) ite.next();
                attributes.add(attributeToDocument((Chars) pair.getValue1(), pair.getValue2()));
            }
        } catch(IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        doc.setFieldValue(ATT_ATTRIBUTES, attributes);

        return doc;
    }

    private static Document attributeToDocument(Chars key, Object value) throws IOException {
        final Document doc = new DefaultDocument(false, ATTRIBUTETYPE);


        final BinaryFieldValueType valueType;
        final long nbOcc;
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out);
        if (value instanceof Sequence) {
            final Sequence seq = (Sequence) value;
            nbOcc = seq.getSize();
            valueType = BinaryFieldValueType.forValue(seq.get(0));
            for(int i=0,n=seq.getSize();i<n;i++){
                DocumentWriter.writeFieldValue(ds, valueType, seq.get(i));
            }
        } else {
            nbOcc = 1;
            valueType = BinaryFieldValueType.forValue(value);
            DocumentWriter.writeFieldValue(ds, valueType, value);
        }
        doc.setFieldValue(ATT_ID, key);
        doc.setFieldValue(ATT_NBOCC, nbOcc);
        doc.setFieldValue(ATT_VALUETYPE, toDocument(valueType,null,null));
        doc.setFieldValue(ATT_ATTVALUE, out.getBuffer().toArrayByte());

        return doc;
    }

    private static Document toDocument(BinaryFieldValueType bft, Sequence alltypes, Sequence alldocs) {

        final Document doc = new DefaultDocument(true, FIELDVALUETYPE);
        doc.setFieldValue(ATT_SIZE, bft.getArraySize());
        doc.setFieldValue(ATT_PTYPE, (byte)bft.getPrimitiveType());
        if (bft.getCharEncoding()!=null) {
            doc.setFieldValue(ATT_CHARENC, bft.getCharEncoding().getName());
        }

        final DocumentType refType = bft.getDocumentType();
        if (refType != null) {
            doc.setFieldValue(ATT_INLINE, bft.isInline());
            int idx = alltypes.search(refType);
            if (idx<0) {
               toDocument(refType,alltypes,alldocs);
               idx = alltypes.search(refType);
            }
            doc.setFieldValue(ATT_DOCTYPE, idx);
        }

        return doc;
    }


    public static final class BinaryDocumentType extends DefaultDocumentType{

        public BinaryDocumentType(Chars id, CharArray title, CharArray description, boolean strict, FieldType[] fields, Dictionary attributes) {
            super(id, title, description, strict, fields, attributes, null);
        }

        public BinaryDocumentType(Chars id, CharArray title, CharArray description, boolean strict, FieldType[] fields, Dictionary attributes, DocumentType[] parents) {
            super(id, title, description, strict, fields, attributes, parents);
        }

        private FieldType[] getFieldsInternal() {
            return localFields;
        }

        DocumentType[] getSuperInternal() {
            return parentTypes;
        }

        void reindexInternal() {
            reindex();
        }

    }
    
}
