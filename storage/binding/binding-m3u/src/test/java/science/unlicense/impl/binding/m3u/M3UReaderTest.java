
package science.unlicense.impl.binding.m3u;

import science.unlicense.impl.binding.m3u.M3UReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.m3u.model.M3UEntry;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class M3UReaderTest {
    
    @Test
    public void readTest() throws IOException{
        
        final M3UReader reader = new M3UReader(M3UReader.FORMAT_M3U8);
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/binding/m3u/sample.m3u")));
        
        final M3UEntry entry1 = reader.next();
        Assert.assertNotNull(entry1);
        Assert.assertEquals(Paths.resolve(new Chars("file:/file1.avi")), entry1.getPath());
        Assert.assertEquals(new Chars("title1"), entry1.getTitle());
        Assert.assertEquals(new Chars("comment1"), entry1.getComment());
        Assert.assertEquals(100, entry1.getLength());
        
        final M3UEntry entry2 = reader.next();
        Assert.assertNotNull(entry2);
        Assert.assertEquals(Paths.resolve(new Chars("file:/file2.avi")), entry2.getPath());
        Assert.assertEquals(new Chars("title2"), entry2.getTitle());
        Assert.assertEquals(new Chars("comment2"), entry2.getComment());
        Assert.assertEquals(200, entry2.getLength());
        
        final M3UEntry entry3 = reader.next();
        Assert.assertNull(entry3);
        
    }
    
}
