
package science.unlicense.impl.binding.m3u.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class M3UEntry {
    
    private Path path;
    private int length;
    private Chars title;
    private Chars comment;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    /**
     * Get file length in seconds.
     * 
     * @return 
     */
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Chars getTitle() {
        return title;
    }

    public void setTitle(Chars title) {
        this.title = title;
    }

    public Chars getComment() {
        return comment;
    }

    public void setComment(Chars comment) {
        this.comment = comment;
    }
    
}
