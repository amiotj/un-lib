
package science.unlicense.impl.binding.m3u;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * Resources :
 * http://forums.winamp.com/showthread.php?threadid=65772
 * https://en.wikipedia.org/wiki/M3U
 * 
 * @author Johann Sorel
 */
public class M3UFormat extends DefaultFormat{

    public static final M3UFormat INSTANCE = new M3UFormat();

    private M3UFormat() {
        super(new Chars("M3U"),
              new Chars("M3U"),
              new Chars("Multimedia playlist"),
              new Chars[]{
                  new Chars("application/x-mpegurl")
              },
              new Chars[]{
                  new Chars("m3u"),
                  new Chars("m3u8")
              },
              new byte[][]{});
    }
    
}
