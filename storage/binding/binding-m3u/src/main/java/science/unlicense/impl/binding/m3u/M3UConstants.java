
package science.unlicense.impl.binding.m3u;

import science.unlicense.api.character.Chars;

/**
 * https://fr.wikipedia.org/wiki/M3U
 * 
 * @author Johann Sorel
 */
public class M3UConstants {

    public static final Chars SIGNATURE = new Chars("#EXTM3U");
    
    public static final Chars EXTINF = new Chars("#EXTINF:");
    public static final Chars EXTREM = new Chars("#EXTREM:");
    
    
    
}
