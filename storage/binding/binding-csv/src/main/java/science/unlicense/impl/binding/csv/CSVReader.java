
package science.unlicense.impl.binding.csv;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.DocumentTypeBuilder;
import science.unlicense.api.number.Int32;

/**
 * 
 * @author Johann Sorel
 */
public class CSVReader extends AbstractReader {
        
    private final CharEncoding encoding;
    private final Char separator;
    private DocumentType type;
    private boolean firstLineHeader;

    public CSVReader() {
        this(CharEncodings.UTF_8,new Char(';'),true);
    }

    public CSVReader(CharEncoding encoding, Char separator, DocumentType docType) {
        this.encoding = encoding;
        this.separator = separator;
        this.type = docType;
    }
    
    public CSVReader(CharEncoding encoding, Char separator, boolean firstLineHeader) {
        this.encoding = encoding;
        this.separator = separator;
        this.firstLineHeader = firstLineHeader;
    }
    
    public CSVDocument next() throws IOException{
        final CharInputStream cs = getInputAsCharStream(encoding);
        Chars line = cs.readLine();
        if(line==null) return null;
        Sequence rowValues = readRow(cs, line);
        
        if (type==null) {
            //build header from first line
            final DocumentTypeBuilder dtb = new DocumentTypeBuilder(new Chars("Unnamed"));
            if (firstLineHeader) {
                for (int i=0,n=rowValues.getSize(); i<n; i++) {
                    dtb.addField((Chars) rowValues.get(i)).valueClass(Chars.class);
                }
                //read next line
                line = cs.readLine();
                if (line!=null) rowValues = readRow(cs, line.trim());
            } else {
                for (int i=0,n=rowValues.getSize(); i<n; i++) {
                    dtb.addField(Int32.encode(i)).valueClass(Chars.class);
                }
            }
            type = dtb.build();
        }
        
        if(line==null) return null;
        return new CSVDocument(type,rowValues);
    }

    /**
     * Read row values.
     *
     * @param cs
     * @param line
     * @return
     * @throws IOException
     */
    private Sequence readRow(CharInputStream cs, Chars line) throws IOException {
        int cursor = 0;
        final Sequence rowValues = new ArraySequence();

        loop:
        for (;;) {
            final int sepIndex = line.getFirstOccurence(separator, cursor);
            final boolean escaped = line.getUnicode(cursor) == '\"';

            if (escaped) {
                cursor++;
                int escStart = cursor;

                //find end of escaped value
                CharBuffer buffer = null;

                for(;;) {
                    int end = line.getFirstOccurence('\"',cursor);
                    while (end>=0 && line.getUnicode(end+1)=='\"') {
                        cursor = end+2;
                        end = line.getFirstOccurence('\"',cursor);
                    }
                    if (end>0) {
                        //found escape end
                        if (buffer!=null) {
                            buffer.append(line.truncate(escStart, end));
                            rowValues.add(buffer.toChars().replaceAll(new Chars("\"\""), new Chars("\"")));
                        } else {
                            rowValues.add(line.truncate(escStart, end).replaceAll(new Chars("\"\""), new Chars("\"")));
                        }
                        
                        int sepIdx = line.getFirstOccurence(separator,end+1);
                        if (sepIdx>0) {
                            cursor = sepIdx+1;
                            break;
                        } else {
                            //no fields left
                            break loop;
                        }
                    } else {
                        //continue search on next line
                        if (buffer==null) buffer = new CharBuffer();
                        buffer.append(line.truncate(escStart, -1));
                        buffer.append('\n');
                        line = cs.readLine().trim();
                        if (line==null) throw new IOException("Unfinished escaped string");
                        cursor=0;
                        escStart=0;
                    }
                }

            } else {
                if (sepIndex>=0) {
                    //truncate value at separator
                    rowValues.add(line.truncate(cursor, sepIndex).trim());
                    cursor = sepIndex+1;
                } else {
                    //truncate to line end, end of row
                    rowValues.add(line.truncate(cursor,-1).trim());
                    break;
                }
            }

        }
        return rowValues;
    }
    
    
}
