
package science.unlicense.impl.binding.csv;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArrayOrderedSet;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.model.doc.AbstractDocument;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldNotFoundException;
import science.unlicense.api.model.doc.FieldType;

/**
 *
 * @author Johann Sorel
 */
public class CSVDocument extends AbstractDocument {

    private final DocumentType type;
    private final Sequence values;

    public CSVDocument(DocumentType type, Sequence values) {
        this.type = type;
        this.values = values;
    }

    @Override
    public Set getFieldNames() {
        final Set fieldNames = new ArrayOrderedSet();
        FieldType[] fields = type.getFields();
        for(int i=0;i<fields.length;i++) fieldNames.add(fields[i].getId());
        return fieldNames;
    }

    @Override
    public Object getFieldValue(Chars name) {
        final int idx = Arrays.getFirstOccurenceIdentity(type.getFields(),type.getField(name));
        if(idx<0) throw new FieldNotFoundException(name);
        return getFieldValue(idx);
    }

    public Object getFieldValue(int index) {
        return values.get(index);
    }
    
    @Override
    public void setFieldValue(Chars name, Object value) throws FieldNotFoundException {
        final int idx = Arrays.getFirstOccurenceIdentity(type.getFields(),type.getField(name));
        if(idx<0) throw new FieldNotFoundException(name);
        setFieldValue(idx, value);
    }
    
    public void setFieldValue(int index, Object value) throws FieldNotFoundException {
        Object old = values.replace(index, value);
        if (old!=value && hasListeners()) {
            sendPropertyEvent(this, type.getFields()[index].getId(), old, value);
        }
    }
    
}
