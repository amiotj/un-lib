
package science.unlicense.impl.binding.csv;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/Comma-separated_values
 * https://www.ietf.org/rfc/rfc4180.txt
 *
 * @author Johann Sorel
 */
public class CSVFormat extends DefaultFormat {

    public CSVFormat() {
        super(new Chars("csv"),
              new Chars("CSV"),
              new Chars("Comma-separated values"),
              new Chars[]{
                new Chars("text/csv")
              },
              new Chars[]{
                new Chars("csv")
              },
              new byte[][]{
                });
    }

}
