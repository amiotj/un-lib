
package science.unlicense.impl.binding.xml;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.meta.DefaultMeta;

/**
 *
 * @author Johann Sorel
 */
public class XMLMeta extends DefaultMeta {

    private static final Chars ID = new Chars("XML");

    public XMLMeta() {
        super(ID);
    }

}
