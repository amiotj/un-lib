
package science.unlicense.impl.binding.xml;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.Parameter;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.impl.code.java.model.JavaClass;
import science.unlicense.impl.code.java.model.JavaException;
import science.unlicense.impl.code.java.model.JavaFunction;

/**
 * Create an XML  reader class for given classes.
 *
 * @author Johann Sorel
 */
public class XMLWriterGenerator {



    public JavaClass create(Chars name, Collection classes){

        //extends abstract reader
        final JavaClass clazz = new JavaClass();
        clazz.id = name;
        clazz.parents.add(GeneratorConstants.CLASS_ABSTRACTWRITER);

        //add base read method
        final JavaFunction fct = new JavaFunction();
        final Parameter inParam = new Parameter();
        inParam.id = new Chars("candidate");
        inParam.type = GeneratorConstants.CLASS_OBJECT;
        fct.id = new Chars("write");
        fct.inParameters.add(inParam);
        fct.metas.add(new JavaException(GeneratorConstants.CLASS_IOEXCEPTION));

        clazz.functions.add(fct);

        final Iterator ite = classes.createIterator();
        
        return clazz;
    }

}
