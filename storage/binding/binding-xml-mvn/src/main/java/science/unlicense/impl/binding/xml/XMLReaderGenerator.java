
package science.unlicense.impl.binding.xml;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.Parameter;
import science.unlicense.api.code.inst.Reference;
import science.unlicense.api.code.inst.Return;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.impl.code.java.JavaConstants;
import science.unlicense.impl.code.java.model.JavaClass;
import science.unlicense.impl.code.java.model.JavaException;
import science.unlicense.impl.code.java.model.JavaFunction;

/**
 * Create an XML  reader class for given classes.
 *
 * @author Johann Sorel
 */
public class XMLReaderGenerator {

    public JavaClass create(Chars name, Collection classes){

        //extends abstract reader
        final JavaClass clazz = new JavaClass();
        clazz.id = name;
        clazz.parents.add(GeneratorConstants.CLASS_ABSTRACTREADER);

        //add base read method
        final JavaFunction fct = new JavaFunction();
        final Parameter outParam = new Parameter();
        outParam.id = new Chars("read");
        outParam.type = GeneratorConstants.CLASS_OBJECT;
        fct.id = new Chars("read");
        fct.outParameters.add(outParam);
        fct.metas.add(new JavaException(GeneratorConstants.CLASS_IOEXCEPTION));
        clazz.functions.add(fct);
        
        final Return ret = new Return(new Reference(JavaConstants.NULL));
        fct.startInstruction = ret;
        

        final Iterator ite = classes.createIterator();
        
        return clazz;
    }

}
