
package science.unlicense.impl.binding.ebml;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * EBML decoder, utility class
 *
 * Specification :
 * http://www.matroska.org/technical/specs/rfc/index.html
 *
 * @author Johann Sorel
 */
public class EBMLDecoder {

    /**
     * Id of the current element.
     */
    private long currentId = -1;

    Dictionary types = null;
    public DataInputStream ds = null;

    public EBMLDecoder() {
    }

    
    /**
     * Get the next id, but does not discard it until a consume is called.
     * This allow readers to check the id, read it if they can or pass it back to the parent
     * reader.
     * Since EBML does not have element end flags this allows us to avoid rewind the stream.
     *
     * @return
     * @throws IOException
     */
    public long peekId() throws IOException{
        if(currentId !=-1) return currentId;
        currentId = readVarID();
        return currentId;
    }

    /**
     * Consume this id.
     */
    public void consumeId(){
        currentId = -1;
    }

    /**
     * Read next as a variable size id.
     * Same as varInt but with the zeros and separator aggregated
     * @return long
     */
    private long readVarID() throws IOException{
        int nbzero = 0;
        while(ds.readBits(1) == 0){
            nbzero++;
        }

        final int nbbit = 8*(nbzero+1) - (1+nbzero);
        int id = ds.readBits(nbbit);
        id |= 1<<nbbit;
        return id;
    }

    /**
     * Read next as a variable size integer.
     * @return long
     */
    public long readInt() throws IOException{
        final int size = readVarInt();

        if(size==0){
            return 0;
        }else if(size==1){
            return ds.readByte();
        }else if(size==2){
            return ds.readShort();
        }else if(size==4){
            return ds.readInt();
        }else if(size==8){
            return ds.readLong();
        }

        throw new IOException("Unexpected size : "+size);
    }

    /**
     * Read next as a variable size unsigned integer.
     * @return long
     */
    public long readUInt() throws IOException{
        final int size = readVarInt();

        if(size==0){
            return 0;
        }else if(size==1){
            return ds.readUByte();
        }else if(size==2){
            return ds.readUShort();
        }else if(size==3){
            return ds.readUInt24();
        }else if(size==4){
            return ds.readUInt();
        }else if(size==8){
            return ds.readLong();
        }

        throw new IOException("Unexpected size : "+size);
    }

    public Chars readString() throws IOException{
        final int size = readVarInt();
        final byte[] buffer = new byte[size];
        ds.readFully(buffer);
        return new Chars(buffer, CharEncodings.US_ASCII);
    }

    /**
     * Read next as a variable size integer.
     * @return long
     */
    public int readVarInt() throws IOException{
        int nbzero = 0;
        while(ds.readBits(1) == 0){
            nbzero++;
        }

        final int nbbit = 8*(nbzero+1) - (1+nbzero);
        return ds.readBits(nbbit);
    }

    public byte[] readBinary() throws IOException{
        final int size = readVarInt();
        return ds.readFully(new byte[size]);
    }

    public boolean isChunk(int id){
        return types.getValue(id)!=null;
    }
    
    public EBMLChunk createChunk(int id) throws IOException{
        Class type = (Class) types.getValue(id);
        if(type == null){
            type = EBMLChunk.class;
        }

        final EBMLChunk chunk;
        try {
            chunk = (EBMLChunk) type.newInstance();
        } catch (InstantiationException ex) {
            throw new IOException(ex);
        } catch (IllegalAccessException ex) {
            throw new IOException(ex);
        }

        return chunk;
    }

}
