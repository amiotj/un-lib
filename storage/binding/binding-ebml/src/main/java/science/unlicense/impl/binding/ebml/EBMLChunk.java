

package science.unlicense.impl.binding.ebml;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.number.Int32;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class EBMLChunk extends DefaultNode{
    
    private static final PropertyType[] EMPTY = new PropertyType[0];
    
    protected static final int TYPE_INT = 1;
    protected static final int TYPE_UINT = 2;
    protected static final int TYPE_FLOAT = 3;
    protected static final int TYPE_STRING = 4;
    protected static final int TYPE_UTF8 = 5;
    protected static final int TYPE_DATE = 6;
    protected static final int TYPE_BINARY = 7;
    protected static final int TYPE_SUB = 8;
    
    protected final Dictionary properties = new HashDictionary();
    private long id;
    private long size;

    public EBMLChunk(){
        super(true);
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Dictionary getProperties() {
        return properties;
    }
    
    /**
     * Define the structure of the chunk.
     * [dataId0 , type, dataId1, type, ....]
     * Type is one of TYPE_X constants
     * @return int[]
     */
    protected PropertyType[] getStructure(){
        return EMPTY;
    }
    
    public Integer getPropertyUInt(int code, Integer def){
        final Integer value = (Integer) getProperties().getValue(code);
        return value!=null ? value : def;
    }
    
    public Integer getPropertyInt(int code, Integer def){
        final Integer value = (Integer) getProperties().getValue(code);
        return value!=null ? value : def;
    }
    
    public Float getPropertyFloat(int code, Float def){
        final Float value = (Float) getProperties().getValue(code);
        return value!=null ? value : def;
    }
    
    public Chars getPropertyString(int code, Chars def){
        final Chars value = (Chars) getProperties().getValue(code);
        return value!=null ? value : def;
    }
    
    public Sequence getPropertiesString(int code){
        final Object candidate = getProperties().getValue(code);
        final Sequence seq = new ArraySequence();
        if(candidate instanceof Chars){
            seq.add(candidate);
        }else if(candidate instanceof Sequence){
            seq.addAll((Sequence)candidate);
        }
        return seq;
    }
    
    public Chars getPropertyDate(int code, Chars def){
        final Chars value = (Chars) getProperties().getValue(code);
        return value!=null ? value : def;
    }
    
    public byte[] getPropertyBinary(int code, byte[] def){
        final byte[] value = (byte[]) getProperties().getValue(code);
        return value!=null ? value : def;
    }
    
    public EBMLChunk getSub(int code){
        final Iterator ite = this.children.createIterator();
        while(ite.hasNext()) {
            final Object cdt = ite.next();
            if(cdt instanceof EBMLChunk && (int)((EBMLChunk)cdt).id == code){
                return (EBMLChunk)cdt;
            }
        }
        return null;
    }
    
    public Sequence getSubs(int code){
        final Sequence seq = new ArraySequence();
        final Iterator ite = this.children.createIterator();
        while(ite.hasNext()) {
            final Object cdt = ite.next();
            if(cdt instanceof EBMLChunk && (int)((EBMLChunk)cdt).id == code){
                seq.add(cdt);
            }
        }
        return seq;
    }
        
    public final void read(EBMLDecoder decoder) throws IOException{
        final long offset = decoder.ds.getByteOffset();
        readInternal(decoder);
        //skip any remaining bytes, size to not always match the size of all properties
        decoder.ds.skipFully( (offset+size)-decoder.ds.getByteOffset() );
    }
    
    protected final void readInternal(EBMLDecoder decoder) throws IOException{
        final long offset = decoder.ds.getByteOffset();
        while(decoder.ds.getByteOffset()< offset+size){
            final long id = decoder.peekId();
            decoder.consumeId();
            readProperty(decoder, (int) id);
        }
    }
    
    protected void readProperty(EBMLDecoder decoder, int id) throws IOException{
        //check if we know this type
        int type = -1;
        final PropertyType[] struct = getStructure();
        for(int i=0;i<struct.length;i++){
            if(struct[i].id==id){
                type = struct[i].type;
                break;
            }
        }
        
        if(type==TYPE_INT){
            properties.add(id, decoder.readInt());
        }else if(type==TYPE_UINT){
            properties.add(id, decoder.readUInt());
        }else if(type==TYPE_FLOAT){
            properties.add(id, decoder.readBinary());
        }else if(type==TYPE_STRING){
            properties.add(id, decoder.readString());
        }else if(type==TYPE_UTF8){
            properties.add(id, decoder.readString());
        }else if(type==TYPE_DATE){
            properties.add(id, decoder.readBinary());
        }else if(type==TYPE_BINARY){
            properties.add(id, decoder.readBinary());
        }else if(type==TYPE_SUB || decoder.isChunk(id)){
            //it's a sub element
            final int length = decoder.readVarInt();
            final EBMLChunk chunk = decoder.createChunk(id);
            chunk.setId(id);
            chunk.setSize(length);
            chunk.read(decoder);
            children.add(chunk);
        }else{
            //unknowned, read as binary
            properties.add(id, decoder.readBinary());
        }
        
    }

    public Chars getPropertyName(int id){
        final PropertyType[] struct = getStructure();
        for(int i=0;i<struct.length;i++){
            if(struct[i].id==id){
                return struct[i].name;
            }
        }
        return null;
    }
    
    public Chars thisToChars() {
        final CharBuffer buffer = new CharBuffer();
        if(getClass().equals(EBMLChunk.class)){
            buffer.append(new Chars(getClass().getSimpleName()+" id : "+id));
        }else{
            buffer.append(new Chars(getClass().getSimpleName()));
        }
        
        final Dictionary dico = getProperties();
        final Iterator ite = dico.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final int id = (Integer)pair.getValue1();
            final Object value = pair.getValue2();
            buffer.append("\n ◇ ");
            final Chars propName = getPropertyName(id);
            if(propName!=null){
                buffer.append(propName);
                buffer.append("(0x");
                buffer.append(Int32.encodeHexa(id));
                buffer.append(')');
            }else{
                buffer.append("0x");
                buffer.append(Int32.encodeHexa(id));
            }
            buffer.append(" = ");
            buffer.append(CObjects.toChars(value));
        }
        
        return buffer.toChars();
    }
    
    public static final class PropertyType {
        public final int id;
        public final int type;
        public final Chars name;
        public PropertyType(int id, int type, Chars name) {
            this.id = id;
            this.type = type;
            this.name = name;
        }
    }
    
}
