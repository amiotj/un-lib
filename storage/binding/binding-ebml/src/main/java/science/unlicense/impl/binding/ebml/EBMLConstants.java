
package science.unlicense.impl.binding.ebml;

/**
 *
 * @author Johann Sorel
 */
public final class EBMLConstants {

    public static final byte[] SIGNATURE = new byte[]{(byte)0x1a,(byte)0x45,(byte)0xdf,(byte)0xa3};

    /** container [ card:+; ] */
    public static final int ID_HEADER = 0x1a45dfa3;
    /** uint [ def:1; ] */
    public static final int ID_HEADER_EBMLVersion = 0x4286;
    /** uint [ def:1; ] */
    public static final int ID_HEADER_EBMLReadVersion = 0x42f7;
    /** uint [ def:4; ] */
    public static final int ID_HEADER_EBMLMaxIDLength = 0x42f2;
    /** uint [ def:8; ] */
    public static final int ID_HEADER_EBMLMaxSizeLength = 0x42f3;
    /** string [ range:32..126; ] */
    public static final int ID_HEADER_DocType = 0x4282;
    /** uint [ def:1; ] */
    public static final int ID_HEADER_DocTypeVersion = 0x4287;
    /** uint [ def:1; ] */
    public static final int ID_HEADER_DocTypeReadVersion = 0x4285;

}
