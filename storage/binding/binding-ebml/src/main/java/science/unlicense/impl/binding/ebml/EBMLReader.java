
package science.unlicense.impl.binding.ebml;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * EBML reader.
 *
 * Specification :
 * http://www.matroska.org/technical/specs/rfc/index.html
 *
 * @author Johann Sorel
 */
public class EBMLReader extends AbstractReader {

    private final Dictionary types = new HashDictionary();

    private EBMLDecoder decoder = null;
    private EBMLChunk next = null;

    /**
     *
     * @param types Dictionary of knowned chunks : int -> class
     *          EBMLHeader is already present
     */
    public EBMLReader(Dictionary types) {
        if(types!=null) this.types.addAll(types);
        this.types.add(0x1a45dfa3, EBMLHeader.class);
    }

    public boolean hasNext() throws IOException {
        findNext();
        return next != null;
    }

    public EBMLChunk next() throws IOException{
        findNext();
        final EBMLChunk temp = next;
        next = null;
        return temp;
    }

    private void findNext() throws IOException{
        if(next!=null) return;

        if(decoder==null){
            decoder = new EBMLDecoder();
            decoder.ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
            decoder.types = types;
        }

        final int chunkid;
        try{
            chunkid = (int) decoder.peekId();
        }catch(EOSException ex){
            //file finished
            return;
        }
        
        //consume id
        decoder.consumeId();
        //read size of the container.
        final long size = decoder.readVarInt();
        
        final EBMLChunk chunk = decoder.createChunk(chunkid);
        chunk.setId(chunkid);
        chunk.setSize(size);
        chunk.read(decoder);
        next = chunk;
    }

}
