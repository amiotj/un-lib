

package science.unlicense.impl.binding.riff;

import science.unlicense.impl.binding.riff.model.Chunk;
import science.unlicense.impl.binding.riff.model.LISTChunk;

/**
 * RIFF model is a tree structure.
 * We use a similar approach as XML to indicate in the reader if the current
 * chunk is a simple chunk, start or end of a RIFF/LIST chunk.
 *
 * @author Johann Sorel
 */
public class RIFFElement {

    public static final int TYPE_CHUNK = 0;
    public static final int TYPE_LIST_START = 1;
    public static final int TYPE_LIST_END = 2;

    private final int type;
    private final Chunk chunk;

    public RIFFElement(int type, Chunk chunk) {
        this.type = type;
        this.chunk = chunk;
    }

    public int getType() {
        return type;
    }

    public Chunk getChunk() {
        return chunk;
    }

    @Override
    public String toString() {
        if(type==TYPE_CHUNK){
            return "RIFFElement "+chunk.getClass().getName();
        }else if(type==TYPE_LIST_START){
            return "RIFFElement LIST START "+((LISTChunk)chunk).getType();
        }else if(type==TYPE_LIST_END){
            return "RIFFElement LIST END "+((LISTChunk)chunk).getType();
        }
        throw new IllegalStateException("Unvalid type : "+type);
    }
    
}
