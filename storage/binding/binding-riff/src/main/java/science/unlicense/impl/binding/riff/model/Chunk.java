

package science.unlicense.impl.binding.riff.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface Chunk {

    /**
     * Chunk id.
     * @return 4 characters chunk id.
     */
    Chars getFourCC();

    void setFourCC(Chars fourCC);

    /**
     * Chunk size.
     * The size does not includ chunk fourcc and size.
     * 
     * @return negative if unknowned yet.
     */
    long getSize();

    void setSize(long size);

    /**
     * Offset from file start.
     * @return
     */
    long getFileOffset();

    void setFileOffset(long fileOffset);

    /**
     * Indicate if this chun has a fixed size.
     * 
     * @return 
     */
    boolean isFixedSize();
    
    /**
     * Calculate chunk size from it's current configuration and possible sub chunks.
     * 
     */
    void computeSize();
        
    /**
     * Read chunk data excluding FourCC and Size.
     * 
     * @param ds
     * @throws IOException 
     */
    void read(DataInputStream ds) throws IOException;

    /**
     * Write chunk data excluding FourCC and Size.
     * 
     * @param ds
     * @throws IOException 
     */
    void write(DataOutputStream ds) throws IOException;

}
