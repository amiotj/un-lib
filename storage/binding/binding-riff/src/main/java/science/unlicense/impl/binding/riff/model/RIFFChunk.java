
package science.unlicense.impl.binding.riff.model;

import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.binding.riff.RIFFConstants;

/**
 *
 * @author Johann Sorel
 */
public class RIFFChunk extends LISTChunk {


    public RIFFChunk() {
        super(RIFFConstants.CHUNK_RIFF);
    }

    public RIFFChunk(NumberEncoding encoding) {
        super(encoding == NumberEncoding.LITTLE_ENDIAN ? RIFFConstants.CHUNK_RIFF : RIFFConstants.CHUNK_RIFX);
    }

    public NumberEncoding getEncoding(){
        return RIFFConstants.CHUNK_RIFF.equals(fourCC) ? NumberEncoding.LITTLE_ENDIAN : NumberEncoding.BIG_ENDIAN;
    }

}
