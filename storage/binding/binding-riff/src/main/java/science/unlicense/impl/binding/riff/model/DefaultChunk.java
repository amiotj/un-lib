

package science.unlicense.impl.binding.riff.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.Int64;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Base RIFF chunk.
 *
 * @author Johann Sorel
 */
public class DefaultChunk extends CObject implements Chunk{

    protected Chars fourCC;
    protected long size = -1;
    protected final boolean fixedSize;
    protected long fileOffset = -1;

    public DefaultChunk() {
        this(null);
    }

    public DefaultChunk(Chars fourCC) {
        this.fourCC = fourCC;
        fixedSize = false;
    }
    
    public DefaultChunk(Chars fourCC, long fixedSize) {
        this.fourCC = fourCC;
        this.size = fixedSize;
        this.fixedSize = true;
    }

    /**
     * Chunk id.
     * @return 4 characters chunk id.
     */
    public Chars getFourCC() {
        return fourCC;
    }

    public void setFourCC(Chars fourCC) {
        this.fourCC = fourCC;
    }

    /**
     * Chunk size.
     * @return negative if unknowned yet.
     */
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        if(isFixedSize()){
            throw new InvalidArgumentException("Chunk has a fixed size.");
        }
        this.size = size;
    }

    public final boolean isFixedSize() {
        return fixedSize;
    }

    public void computeSize() {
        if(isFixedSize()) return;
        throw new UnimplementedException("Chunk "+fourCC+" has not defined a compute size method.");
    }

    /**
     * Offset from file start.
     * @return
     */
    public long getFileOffset() {
        return fileOffset;
    }

    public void setFileOffset(long fileOffset) {
        this.fileOffset = fileOffset;
    }

    public final void read(DataInputStream ds) throws IOException{
        final long offset = ds.getByteOffset();
        readInternal(ds);
        //skip any remaining bytes, size to not always match the size of all properties
        ds.skipFully( (offset+size)-ds.getByteOffset() );
    }
    
    protected void readInternal(DataInputStream ds) throws IOException{
    }
        
    public void write(DataOutputStream ds) throws IOException{
        throw new IOException("Chunk "+fourCC+" has not defined a writer.");
    }

    public Chars toChars() {
        final CharBuffer buffer = new CharBuffer();
        buffer.append(fourCC);
        buffer.append(' ');
        buffer.append(Int64.encode(size));
        return buffer.toChars();
    }

}
