

package science.unlicense.impl.binding.riff;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.binding.riff.model.Chunk;
import science.unlicense.impl.binding.riff.model.DefaultChunk;
import science.unlicense.impl.binding.riff.model.LISTChunk;
import science.unlicense.impl.binding.riff.model.RIFFChunk;

/**
 *
 * @author Johann Sorel
 */
public class RIFFReader extends AbstractReader {


    private final Dictionary types = new HashDictionary();

    //stack of read chunks, we keep this sequence
    //to send again the chunk end element if it's a RIFF/List chunk.
    private final Sequence stack = new ArraySequence();
    private RIFFElement next = null;
    private DataInputStream ds = null;

    /**
     *
     * @param types Dictionary of knowned chunks : Chars(FourCC -> Chunkclass)
     *               Default RIFF chunks are already defined.
     */
    public RIFFReader(Dictionary types) {
        if(types!=null) this.types.addAll(types);
        this.types.add(RIFFConstants.CHUNK_RIFF, RIFFChunk.class);
        this.types.add(RIFFConstants.CHUNK_LIST, LISTChunk.class);
    }

    public boolean hasNext() throws IOException {
        findNext();
        return next != null;
    }

    public RIFFElement next() throws IOException{
        findNext();
        final RIFFElement temp = next;
        next = null;
        return temp;
    }

    private void findNext() throws IOException{
        if(next!=null) return;

        if(ds==null){
            //first time we open the stream
            ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
            //we expect to find a RIFF or RIFX
            final Chars riff = new Chars(ds.readFully(new byte[4]));
            final RIFFChunk chunk;
            if(RIFFConstants.CHUNK_RIFF.equals(riff)){
                //keep little endian encoding
                chunk = new RIFFChunk(NumberEncoding.LITTLE_ENDIAN);
            }else if(RIFFConstants.CHUNK_RIFX.equals(riff)){
                //switch to big endian encoding
                ds.setEncoding(NumberEncoding.BIG_ENDIAN);
                chunk = new RIFFChunk(NumberEncoding.BIG_ENDIAN);
            }else{
                throw new IOException("Stream is not a RIFF.");
            }
            chunk.setSize(ds.readUInt());
            chunk.setFileOffset(ds.getByteOffset()-8);
            final Chars subType = new Chars(ds.readFully(new byte[4]));
            chunk.setType(subType);
            next = new RIFFElement(RIFFElement.TYPE_LIST_START, chunk);
            stack.add(chunk);
            return;
        }

        if(stack.isEmpty()){
            //nothing left to read
            return;
        }

        //check padding
        long currentOffset = ds.getByteOffset();
        if(ds.getByteOffset()%2 !=0){
            ds.skipFully(1);
            currentOffset++;
        }

        //check if we reached the end of parent chunk
        final Chunk parentChunk = (Chunk) stack.get(stack.getSize()-1);
        if(currentOffset-parentChunk.getFileOffset()-(parentChunk.getSize()+8) == 0){
            //end of chunk
            stack.remove(stack.getSize()-1);
            next = new RIFFElement(RIFFElement.TYPE_LIST_END, parentChunk);
            return;
        }

        //read a normal chunk
        final Chars fourCC;
        try{
            fourCC = new Chars(ds.readFully(new byte[4]));
        }catch(EOSException ex){
            //no chunk left
            return;
        }
        final long length = ds.readUInt();

        final Chunk chunk = createChunk(fourCC);

        chunk.setFourCC(fourCC);
        chunk.setSize(length);
        chunk.setFileOffset(ds.getByteOffset()-8);

        if(chunk instanceof LISTChunk){
            //list type chunk
            final Chars subType = new Chars(ds.readFully(new byte[4]));
            ((LISTChunk)chunk).setType(subType);
            next = new RIFFElement(RIFFElement.TYPE_LIST_START, chunk);
            stack.add(chunk);
        }else{
            //leaf chunk
            chunk.read(ds);
            next = new RIFFElement(RIFFElement.TYPE_CHUNK, chunk);
        }

    }

    protected Chunk createChunk(Chars fourCC) throws IOException{
        Class type = (Class) types.getValue(fourCC);
        if(type == null){
            type = DefaultChunk.class;
        }

        final Chunk chunk;
        try {
            chunk = (Chunk) type.newInstance();
        } catch (InstantiationException ex) {
            throw new IOException(ex);
        } catch (IllegalAccessException ex) {
            throw new IOException(ex);
        }

        return chunk;
    }

}
