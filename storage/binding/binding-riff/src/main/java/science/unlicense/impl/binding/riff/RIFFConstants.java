

package science.unlicense.impl.binding.riff;

import science.unlicense.api.character.Chars;

/**
 * RIFF Constants
 * @author Johann Sorel
 */
public class RIFFConstants {

    /** FileTypeBlocID - Little Endian file */
    public static final Chars CHUNK_RIFF = new Chars(new byte[]{'R','I','F','F'});
    /** FileTypeBlocID - Big Endian file */
    public static final Chars CHUNK_RIFX = new Chars(new byte[]{'R','I','F','X'});
    public static final Chars CHUNK_LIST = new Chars(new byte[]{'L','I','S','T'});

}
