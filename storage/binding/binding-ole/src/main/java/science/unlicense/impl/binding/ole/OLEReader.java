

package science.unlicense.impl.binding.ole;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.binding.ole.model.Header;

/**
 * Resource :
 * http://hdl.loc.gov/loc.gdc/digformat.000006.1
 * http://msdn.microsoft.com/en-us/library/dd942138.aspx
 * http://www.openoffice.org/sc/compdocfileformat.pdf
 * https://googledrive.com/host/0B3fBvzttpiiSS0hEb0pjU2h6a2c/OLE%20Compound%20File%20format.pdf
 *
 * @author Johann Sorel
 */
public class OLEReader extends AbstractReader{

    public void read() throws IOException{

        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, NumberEncoding.LITTLE_ENDIAN);

        final byte[] sign = ds.readFully(new byte[8]);
        if(!Arrays.equals(OLEConstants.SIGNATURE_NEW, sign) && !Arrays.equals(OLEConstants.SIGNATURE_OLD, sign)){
            throw new IOException("File is not a valid OLE stream.");
        }

        final Header header = new Header();
        header.read(ds);

        //loop on all sectors
        final int[] sectors = header.readAllSectorId(bs);
        for(int i=0;i<sectors.length;i++){
            System.out.println(sectors[i]);
        }

    }

}
