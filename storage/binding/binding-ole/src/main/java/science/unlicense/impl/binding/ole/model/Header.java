

package science.unlicense.impl.binding.ole.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class Header extends Sector{

    public byte[] guid;
    public int minorVersion;
    public int majorVersion;
    /** 0xFFFE Big-Endian, 0xFEFF LittleEndian */
    public int byteOrder;
    /** in power of 2 */
    public int sectorSize;
    /** in power of 2 */
    public int miniSectorSize;
    public int reserved1;
    public int reserved2;
    public int reserved3;
    public int nbSector;

    public int firstSector;
    public int transaction;
    public int miniSectorCutoff;
    public int secidSSAT;
    public int nbSectorSSAT;
    public int secidMSAT;
    public int nbSectorMSAT;
    public int[] sidTable;

    public void read(DataInputStream ds) throws IOException{
        guid = ds.readFully(new byte[16]);
        minorVersion = ds.readUShort();
        majorVersion = ds.readUShort();
        byteOrder = ds.readUShort();
        sectorSize = ds.readUShort();
        miniSectorSize = ds.readUShort();
        reserved1 = ds.readUShort();
        reserved2 = ds.readInt();
        reserved3 = ds.readInt();
        nbSector = ds.readInt();
        firstSector = ds.readInt();
        transaction = ds.readInt();
        miniSectorCutoff = ds.readInt();
        secidSSAT = ds.readInt();
        nbSectorSSAT = ds.readInt();
        secidMSAT = ds.readInt();
        nbSectorMSAT = ds.readInt();
        sidTable = ds.readInt(109);

    }

    public long getSectorOffset(int sectorId){
        return 512 + sectorId*(2^sectorSize);
    }

    public long getShortSectorOffset(int shortSectorId){
        return shortSectorId*(2^miniSectorSize);
    }

    public int[] readAllSectorId(BacktrackInputStream bs) throws IOException {
        bs.rewind();
        final int[] sectors = new int[nbSector];
        int nb = nbSector;

        //copy the sector id in the header
        Arrays.copy(sidTable,0,Maths.min(sidTable.length, nb),sectors,0);
        nb -= 109;

        //search additional sectors
        int nextSid = secidMSAT;
        while(nb>0){
            bs.rewind();
            final DataInputStream ds = new DataInputStream(bs, NumberEncoding.LITTLE_ENDIAN);
            ds.skipFully(getSectorOffset(nextSid));
            final int nbId = (sectorSize-4)/4;
            ds.readInt(sectors, nb, Maths.min(nb, nbId));
            nb-=nbId;
            nextSid = ds.readInt();
        }

        return sectors;
    }

}
