

package science.unlicense.impl.binding.ber;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * TODO
 * 
 * Specification :
 * https://en.wikipedia.org/wiki/X.690#DER_encoding
 * 
 * @author Johann Sorel
 */
public class BERReader extends AbstractReader {
    
    public void read() throws IOException {
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
        
        // 8.1.2 Identifier octets 
        byte b1 = ds.readByte(); 
        int clazz = b1 & 0b11000000; 
        int pc = b1 & 0b00100000; 
        int num = b1 & 0b00011111; 

        if (num==0x1F) {
            num = (int) ds.readVarLengthUInt();
        } 

        // 8.1.3 Length octets 
        int multibit = ds.readBits(1); 
        int length = ds.readBits(7); 
        if (multibit==1) { 
            length = ds.readBits(length*8);
        }
        
        boolean inIndefinite = length==0; 

        //read real 
        int bt1 = ds.readBits(1); 

        if (bt1==1) { 
            // 8.5.6 
            int S = ds.readBits(1)==1 ? -1 : 0; 
            int base = ds.readBits(2); 
            if(base==0) base = 2; 
            else if(base==1) base = 8; 
            else if(base==2) base = 16; 
            int F = ds.readBits(2); 
            int exp = ds.readBits(2); 
            int E; 
            if (base==0) { 
                E = ds.readByte(); 
            } else if(base==1) { 
                E = ds.readShort(); 
            } else if(base==2) { 
                //TODO
            } 

        } else { 
            int bt2 = ds.readBits(1); 
            if (bt2==1) { 
                // 8.5.8 
            } else { 
                // 8.5.7 
            } 
        } 
        
    }
    
}
