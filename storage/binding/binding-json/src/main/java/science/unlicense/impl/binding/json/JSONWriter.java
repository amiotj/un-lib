
package science.unlicense.impl.binding.json;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.number.Float64;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Int64;

/**
 * JSON writer.
 * 
 * @author Johann Sorel
 */
public class JSONWriter extends AbstractWriter{
    
    private CharEncoding encoding = CharEncodings.UTF_8;
    private boolean formatted = false;
    private Chars indentation = new Chars("\t");
    
    private final Sequence stack = new ArraySequence();
    private boolean currentEmpty = true;

    public JSONWriter() {
    }

    public CharEncoding getEncoding() {
        return encoding;
    }

    public void setEncoding(CharEncoding encoding) {
        this.encoding = encoding;
    }

    public void setFormatted(boolean formatted) {
        this.formatted = formatted;
    }

    public boolean isFormatted() {
        return formatted;
    }

    public Chars getIndentation() {
        return indentation;
    }

    public void setIndentation(Chars indentation) {
        this.indentation = indentation;
    }
    
    /**
     * Write object begin '{' .
     * 
     * @throws IOException 
     */
    public void writeObjectBegin() throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        final boolean inArray = currentIsArray();
        if(currentEmpty && !stack.isEmpty()){
            //this case can only be if it's first object in an array
            jumpLine();
        }else if(inArray){
            cs.write(JSONConstants.VALUE_SEPARATOR);
            jumpLine();
        }
        if(formatted && inArray) indent();
        
        cs.write(JSONConstants.OBJECT_BEGIN);
        stack.add(0);
        
        currentEmpty = true;
    }
    
    /**
     * Write object end '}' .
     * 
     * @throws IOException 
     */
    public void writeObjectEnd() throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        stack.remove(stack.getSize()-1);
        if(!currentEmpty){
            jumpLine();
            indent();
        }
        cs.write(JSONConstants.OBJECT_END);
        currentEmpty = false;
    }
    
    /**
     * Write array begin '[' .
     * 
     * @throws IOException 
     */
    public void writeArrayBegin() throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        cs.write(JSONConstants.ARRAY_BEGIN);
        stack.add(1);
        currentEmpty = true;
    }
    
    /**
     * Write array end ']' .
     * 
     * @throws IOException 
     */
    public void writeArrayEnd() throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        stack.remove(stack.getSize()-1);
        if(!currentEmpty){
            jumpLine();
            indent();
        }
        cs.write(JSONConstants.ARRAY_END);
        currentEmpty = false;
    }
    
    /**
     * Write JSON name and the name separator ':' .
     * Value separators are automatically written if needed.
     * 
     * @param name
     * @throws IOException 
     */
    public void writeName(Chars name) throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        if(!currentEmpty){
            cs.write(JSONConstants.VALUE_SEPARATOR);
        }
        jumpLine();
        indent();
        cs.write('\"').write(name).write('\"');
        cs.write(JSONConstants.NAME_SEPARATOR);
        currentEmpty = false;
    }
        
    /**
     * Write value.
     * - true
     * - false
     * - null
     * - Number
     * - Chars
     * 
     * @param value
     * @throws IOException 
     */
    public void writeValue(Object value) throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        
        final boolean inArray = currentIsArray();
        if(!currentEmpty && inArray) cs.write(JSONConstants.VALUE_SEPARATOR);
        if(currentEmpty || inArray) jumpLine();
        if(inArray) indent();
        
        
        if(value == null){
            cs.write(JSONConstants.NULL);
        }else if(Boolean.TRUE.equals(value)){
            cs.write(JSONConstants.TRUE);
        }else if(Boolean.FALSE.equals(value)){
            cs.write(JSONConstants.FALSE);
        }else if(value instanceof Integer || value instanceof Long || value instanceof Byte || value instanceof Short){
            cs.write( Int64.encode(((Number)value).longValue()));
        }else if(value instanceof Number){
            cs.write( Float64.encode(((Number)value).doubleValue()));
        }else if(value instanceof Chars){
            cs.write('\"').write((Chars)value).write('\"');
        }else{
            throw new IOException("Unexpected value type : "+value);
        }
        currentEmpty = false;
    }
        
    /**
     * Shortcut for writing : name + name separator + value
     * Array type values are support but only primitive values.
     * 
     * @param name property name
     * @param value property value
     * @throws science.unlicense.api.io.IOException
     */
    public void writeProperty(Chars name, Object value) throws IOException{
        writeName(name);
        
        if(value!=null && value.getClass().isArray()){
            writeArrayBegin();
            final int length = Arrays.getSize(value);
            for(int i=0;i<length;i++){
                writeValue(Arrays.getValue(value, i));
            }
            writeArrayEnd();
        }else{
            writeValue(value);
        }
    }
    
    private void indent() throws IOException{
        if(!formatted) return;        
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        for(int i=0,n=stack.getSize();i<n;i++)cs.write(indentation);
        
    }
    
    private void jumpLine() throws IOException{
        if(!formatted) return;        
        final CharOutputStream cs = getOutputAsCharStream(encoding);
        cs.write('\n');        
    }
    
    private boolean currentIsArray(){
        if(stack.isEmpty()) return false;
        final int type = (Integer)stack.get(stack.getSize()-1);
        return type==1;
    }
    
}
