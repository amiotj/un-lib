
package science.unlicense.impl.binding.hjson;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.RegexTokenType;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenGroup;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.binding.json.AbstractJSONReader;
import science.unlicense.impl.binding.json.JSONElement;

/**
 *
 * @author Johann Sorel
 */
public class HJSONReader extends AbstractJSONReader {

    private static final TokenType TT_BEGIN_ARRAY       = RegexTokenType.keyword(new Chars("["), new Chars("\\["));
    private static final TokenType TT_END_ARRAY         = RegexTokenType.keyword(new Chars("]"), new Chars("\\]"));
    private static final TokenType TT_BEGIN_OBJECT      = RegexTokenType.keyword(new Chars("{"), new Chars("{"));
    private static final TokenType TT_END_OBJECT        = RegexTokenType.keyword(new Chars("}"), new Chars("}"));
    private static final TokenType TT_NAME_SEPARATOR    = RegexTokenType.keyword(new Chars(":"), new Chars(":"));
    private static final TokenType TT_VALUE_SEPARATOR   = RegexTokenType.keyword(new Chars(","), new Chars(","));
    private static final TokenType TT_SPACE             = RegexTokenType.keyword(new Chars("sp"), new Chars("( |\\t|\\r)+"));
    private static final TokenType TT_NEWLINE           = RegexTokenType.keyword(new Chars("nl"), new Chars("\\n"));
    private static final TokenType TT_TRUE              = RegexTokenType.keyword(new Chars("true"), new Chars("true"));
    private static final TokenType TT_FALSE             = RegexTokenType.keyword(new Chars("false"), new Chars("false"));
    private static final TokenType TT_NULL              = RegexTokenType.keyword(new Chars("null"), new Chars("null"));
    private static final TokenType TT_NUMBER            = RegexTokenType.decimal();
    private static final TokenType TT_STRING            = RegexTokenType.keyword(new Chars("quoteString"), new Chars("\\\"[^\\\"]*\\\""));
    private static final TokenType TT_QUOTELESSS        = RegexTokenType.keyword(new Chars("quotelessString"), new Chars("[^ \\r\\n\\,\\:\\[\\]\\{\\}][^\\r\\n\\,\\:\\[\\]\\{\\}]*"));
    private static final TokenType TT_MULTILINE         = RegexTokenType.keyword(new Chars("multiString"), new Chars("\\'\\'\\'([^\\']|(\\'[^\\'])|(\\'\\'[^\\']))*\\'\\'\\'"));
    private static final TokenType TT_COMMENT1          = RegexTokenType.keyword(new Chars("comment1"), new Chars("#[^\\n]*"));
    private static final TokenType TT_COMMENT2          = RegexTokenType.keyword(new Chars("comment2"), new Chars("//[^\\n]*"));
    private static final TokenType TT_COMMENT3          = RegexTokenType.keyword(new Chars("comment3"), new Chars("/\\*([^\\*]|(\\*[^/]))*\\*/"));

    private Lexer lexer;
    private JSONElement next;
    //contain where the cursor is : in array=1, in object=0
    private final Sequence stateStack = new ArraySequence();
    private boolean nextIsName = false;

    public boolean hashNext() throws IOException{
        findNext();
        return next != null;
    }

    public JSONElement next() throws IOException{
        findNext();
        if(next==null) throw new IOException("No more elements");
        final JSONElement temp = next;
        next = null;
        return temp;
    }

    private void findNext() throws IOException{
        if(next!=null) return;

        if(lexer==null){
            lexer = new Lexer();
            final TokenGroup tts = lexer.getTokenGroup();
            tts.add(TT_COMMENT1);
            tts.add(TT_COMMENT2);
            tts.add(TT_COMMENT3);
            tts.add(TT_BEGIN_ARRAY);
            tts.add(TT_END_ARRAY);
            tts.add(TT_BEGIN_OBJECT);
            tts.add(TT_END_OBJECT);
            tts.add(TT_NAME_SEPARATOR);
            tts.add(TT_VALUE_SEPARATOR);
            tts.add(TT_SPACE);
            tts.add(TT_NEWLINE);
            tts.add(TT_TRUE);
            tts.add(TT_FALSE);
            tts.add(TT_NULL);
            tts.add(TT_NUMBER);
            tts.add(TT_STRING);
            tts.add(TT_QUOTELESSS);
            tts.add(TT_MULTILINE);
            lexer.setInput(getInput());
            lexer.init();
        }

        while(next==null){
            final Token token = lexer.next();
            if(token==null) return;

            if(token.type==TT_BEGIN_ARRAY){
                next = new JSONElement(JSONElement.TYPE_ARRAY_BEGIN, null);
                stateStack.add(1);
            }else if(token.type==TT_END_ARRAY){
                next = new JSONElement(JSONElement.TYPE_ARRAY_END, null);
                stateStack.remove(stateStack.getSize()-1);
            }else if(token.type==TT_BEGIN_OBJECT){
                nextIsName=true;
                next = new JSONElement(JSONElement.TYPE_OBJECT_BEGIN, null);
                stateStack.add(0);
            }else if(token.type==TT_END_OBJECT){
                nextIsName=true;
                next = new JSONElement(JSONElement.TYPE_OBJECT_END, null);
                stateStack.remove(stateStack.getSize()-1);
            }else if(token.type==TT_NAME_SEPARATOR){
                nextIsName=false;
                //do nothing skip it
                //TODO ensure we are after a name
            }else if(token.type==TT_VALUE_SEPARATOR){
                nextIsName=true;
                //do nothing skip it
                //TODO ensure we are in array or object
            }else if(token.type==TT_SPACE){
                //do nothing skip it
            }else if(token.type==TT_NEWLINE){
                nextIsName=true;
                //do nothing skip it
            }else if(token.type==TT_TRUE){
                next = new JSONElement(JSONElement.TYPE_VALUE, true);
            }else if(token.type==TT_FALSE){
                next = new JSONElement(JSONElement.TYPE_VALUE, false);
            }else if(token.type==TT_NULL){
                next = new JSONElement(JSONElement.TYPE_VALUE, null);
            }else if(token.type==TT_NUMBER){
                Object value;
                try{
                    value = Int32.decode(token.value);
                }catch(RuntimeException ex){ //TODO change this exception type
                    value = Float64.decode(token.value);
                }
                next = new JSONElement(JSONElement.TYPE_VALUE, value);
            }else if(token.type==TT_STRING){
                if(nextIsName && (Integer)stateStack.get(stateStack.getSize()-1) == 0){
                    next = new JSONElement(JSONElement.TYPE_NAME, trimQuotes(token.value));
                }else{
                    next = new JSONElement(JSONElement.TYPE_VALUE, trimQuotes(token.value));
                }
            }else if(token.type==TT_MULTILINE){
                if(nextIsName && (Integer)stateStack.get(stateStack.getSize()-1) == 0){
                    throw new IOException("Multiline string can only be used as value : "+token);
                }else{
                    next = new JSONElement(JSONElement.TYPE_VALUE, token.value.truncate(3, token.value.getCharLength()-3));
                }
            }else if(token.type==TT_QUOTELESSS){
                if(nextIsName && (Integer)stateStack.get(stateStack.getSize()-1) == 0){
                    next = new JSONElement(JSONElement.TYPE_NAME, token.value.trim());
                }else{
                    next = new JSONElement(JSONElement.TYPE_VALUE, token.value);
                }
            }else if(token.type==TT_COMMENT1){
                next = new JSONElement(JSONElement.TYPE_COMMENT, token.value.truncate(1, -1));
            }else if(token.type==TT_COMMENT2){
                next = new JSONElement(JSONElement.TYPE_COMMENT, token.value.truncate(2, -1));
            }else if(token.type==TT_COMMENT3){
                next = new JSONElement(JSONElement.TYPE_COMMENT, token.value.truncate(2, token.value.getCharLength()-2));
            }else{
                throw new IOException("Unexpected token : "+token);
            }
        }

    }

    private static Chars trimQuotes(Chars candidate){
        return candidate.truncate(1, candidate.getCharLength()-1);
    }

}
