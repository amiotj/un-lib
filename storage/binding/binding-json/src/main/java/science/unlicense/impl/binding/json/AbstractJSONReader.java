
package science.unlicense.impl.binding.json;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.IOException;

/**
 * Common parent for JSon, HJson and other json variants.
 *
 * @author Johann Sorel
 */
public abstract class AbstractJSONReader extends AbstractReader {

    public abstract boolean hashNext() throws IOException;

    public abstract JSONElement next() throws IOException;

}
