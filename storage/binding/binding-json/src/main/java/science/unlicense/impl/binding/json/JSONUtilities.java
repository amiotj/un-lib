

package science.unlicense.impl.binding.json;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultNamedNode;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;

/**
 *
 * @author Johann Sorel
 */
public final class JSONUtilities {
    
    private JSONUtilities(){}
    
    /**
     * Read a json input and return it as a dictionnary.
     * 
     * @param input, standard reader input or an AbstractJSONReader
     * @return NamedNode
     * @throws IOException 
     */
    public static Dictionary readAsDictionary(Object input) throws IOException{
        
        final AbstractJSONReader reader;
        if (input instanceof AbstractJSONReader) {
            reader = (AbstractJSONReader) input;
        } else {
            reader = new JSONReader();
            reader.setInput(input);
        }

        while(reader.hashNext()){
            final JSONElement element = reader.next();            
            final int type = element.getType();            
            if(type==JSONElement.TYPE_OBJECT_BEGIN){
                return readObjectAsDictionnary(reader);
            }else{
                throw new IOException("Was expecting an object begin element");
            }            
        }
        
        return null;
    }
    
    /**
     * Write a Dictionary in json.
     * 
     * @param dico
     * @param writer
     * @throws IOException 
     */
    public static void writeDictionary(Dictionary dico, JSONWriter writer) throws IOException{
        
        writer.writeObjectBegin();
        
        final Iterator ite = dico.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Chars name = (Chars) pair.getValue1();
            writer.writeName(name);
            
            Object value = pair.getValue2();
            if(value!=null && value.getClass().isArray()){
                writer.writeArrayBegin();
                final int size = Arrays.getSize(value);
                for(int i=0;i<size;i++){
                    Object c = Arrays.getValue(value, i);
                    if(c instanceof Dictionary){
                        writeDictionary((Dictionary) c, writer);
                    }else{
                        writer.writeValue(c);
                    }
                }
                writer.writeArrayEnd();
            }else if(value instanceof Dictionary){
                writeDictionary((Dictionary) value, writer);
            }else{
                writer.writeValue(value);
            }
        }
        
        writer.writeObjectEnd();
    }
    
    /**
     * Read a json input and return it as a tree.
     * 
     * @param input, standard reader input or an AbstractJSONReader
     * @return NamedNode
     * @throws IOException 
     */
    public static NamedNode readAsTree(Object input) throws IOException{

        final AbstractJSONReader reader;
        if (input instanceof AbstractJSONReader) {
            reader = (AbstractJSONReader) input;
        } else {
            reader = new JSONReader();
            reader.setInput(input);
        }
        
        while(reader.hashNext()){
            final JSONElement element = reader.next();            
            final int type = element.getType();            
            if(type==JSONElement.TYPE_OBJECT_BEGIN){
                return readObject(reader);
            }else{
                throw new IOException("Was expecting an object begin element");
            }            
        }
        
        return null;
    }

    /**
     * Read a json input and return it as a Document.
     *
     * @param input, standard reader input or an AbstractJSONReader
     * @return NamedNode
     * @throws IOException
     */
    public static Document readAsDocument(Object input) throws IOException{

        final AbstractJSONReader reader;
        if (input instanceof AbstractJSONReader) {
            reader = (AbstractJSONReader) input;
        } else {
            reader = new JSONReader();
            reader.setInput(input);
        }

        while(reader.hashNext()){
            final JSONElement element = reader.next();
            final int type = element.getType();
            if(type==JSONElement.TYPE_OBJECT_BEGIN){
                return readObjectDoc(reader);
            }else{
                throw new IOException("Was expecting an object begin element");
            }
        }

        return null;
    }

    /**
     * Write a Dictionary in json.
     *
     * @param dico
     * @param out
     * @throws IOException
     */
    public static void writeDocument(Document dico, Object out) throws IOException{

        JSONWriter writer = null;
        if(out instanceof JSONWriter) {
            writer = (JSONWriter) out;
        } else {
            writer = new JSONWriter();
            writer.setOutput(out);
        }

        writer.writeObjectBegin();

        final Iterator ite = dico.getFieldNames().createIterator();
        while(ite.hasNext()){
            final Chars name = (Chars) ite.next();
            writer.writeName(name);

            Object value = dico.getFieldValue(name);
            if(value!=null && value.getClass().isArray()){
                writer.writeArrayBegin();
                final int size = Arrays.getSize(value);
                for(int i=0;i<size;i++){
                    Object c = Arrays.getValue(value, i);
                    if(c instanceof Document){
                        writeDocument((Document) c, writer);
                    }else{
                        writer.writeValue(c);
                    }
                }
                writer.writeArrayEnd();
            }else if(value instanceof Dictionary){
                writeDictionary((Dictionary) value, writer);
            }else{
                writer.writeValue(value);
            }
        }

        writer.writeObjectEnd();
    }

    /**
     * Write a tree node in json.
     * 
     * @param node
     * @param writer
     * @throws IOException 
     */
    public static void writeTree(NamedNode node, JSONWriter writer) throws IOException{
        
        final Chars name = node.getName();
        
        if(name!=null){
            writer.writeName(name);
        }
        
        final Collection children = node.getChildren();
        if(!children.isEmpty()){
            //object type
            writer.writeObjectBegin();
            final Iterator ite = children.createIterator();
            while (ite.hasNext()) {
                writeTree((NamedNode)ite.next(), writer);
            }
            writer.writeObjectEnd();
        }else{
            //single value
            Object value = node.getValue();
            if(value!=null && value.getClass().isArray()){
                writer.writeArrayBegin();
                final int size = Arrays.getSize(value);
                for(int i=0;i<size;i++){
                    Object c = Arrays.getValue(value, i);
                    if(c instanceof NamedNode){
                        writeTree((NamedNode) c, writer);
                    }else{
                        writer.writeValue(c);
                    }
                }
                writer.writeArrayEnd();
            }else{
                writer.writeValue(value);
            }
        }
    }
    
    private static Dictionary readObjectAsDictionnary(AbstractJSONReader reader) throws IOException{
        Dictionary node = new HashDictionary();
        
        Chars propName = null;
        Object propValue = null;
        
        while(reader.hashNext()){          
            final JSONElement element = reader.next();    
            final int type = element.getType();          
            
            if(propName==null){
                //name or end expected
                if(type==JSONElement.TYPE_NAME){
                    propName = (Chars) element.getValue();
                }else if(type==JSONElement.TYPE_OBJECT_END){
                    break;
                }else{
                    throw new IOException("Unexpected json element : "+element);
                }                
            }else{
                //value expected
                if(type==JSONElement.TYPE_VALUE){
                    propValue = element.getValue();
                }else if(type==JSONElement.TYPE_ARRAY_BEGIN){
                    propValue = readArrayAsDictionnary(reader);
                }else if(type==JSONElement.TYPE_OBJECT_BEGIN){
                    propValue = readObjectAsDictionnary(reader);
                }else{
                    throw new IOException("Unexpected json element : "+element);
                }
                
                node.add(propName, propValue);
                propValue = null;
                propName = null;
            }
        }
        return node;
    }
    
    private static NamedNode readObject(AbstractJSONReader reader) throws IOException{
        final NamedNode node = new DefaultNamedNode(true);
        
        Chars propName = null;
        Object propValue = null;
        
        while(reader.hashNext()){          
            final JSONElement element = reader.next();    
            final int type = element.getType();          
            
            if(propName==null){
                //name or end expected
                if(type==JSONElement.TYPE_NAME){
                    propName = (Chars) element.getValue();
                }else if(type==JSONElement.TYPE_OBJECT_END){
                    break;
                }else{
                    throw new IOException("Unexpected json element : "+element);
                }                
            }else{
                //value expected
                if(type==JSONElement.TYPE_VALUE){
                    propValue = element.getValue();
                }else if(type==JSONElement.TYPE_ARRAY_BEGIN){
                    propValue = readArray(reader);
                }else if(type==JSONElement.TYPE_OBJECT_BEGIN){
                    propValue = readObject(reader);
                }else{
                    throw new IOException("Unexpected json element : "+element);
                }
                
                if(propValue instanceof NamedNode){
                    ((NamedNode)propValue).setName(propName);
                    node.getChildren().add((NamedNode)propValue);
                }else{
                    node.getChildren().add(new DefaultNamedNode(propName, propValue, true));
                }
                
                propValue = null;
                propName = null;
            }
        }
        return node;
    }
    
    private static Object[] readArray(AbstractJSONReader reader) throws IOException{
        final Sequence objects = new ArraySequence();
        
        while(reader.hashNext()){          
            final JSONElement element = reader.next();    
            final int type = element.getType();          
            
            if(type==JSONElement.TYPE_VALUE){
                objects.add(element.getValue());
            }else if(type==JSONElement.TYPE_ARRAY_BEGIN){
                objects.add(readArray(reader));
            }else if(type==JSONElement.TYPE_OBJECT_BEGIN){
                objects.add(readObject(reader));
            }else if(type==JSONElement.TYPE_ARRAY_END){
                break;
            }else{
                throw new IOException("Unexpected json element : "+element);
            }
        }
        return objects.toArray();
    }

    private static Object[] readArrayAsDictionnary(AbstractJSONReader reader) throws IOException{
        final Sequence objects = new ArraySequence();

        while(reader.hashNext()){
            final JSONElement element = reader.next();
            final int type = element.getType();

            if(type==JSONElement.TYPE_VALUE){
                objects.add(element.getValue());
            }else if(type==JSONElement.TYPE_ARRAY_BEGIN){
                objects.add(readArrayAsDictionnary(reader));
            }else if(type==JSONElement.TYPE_OBJECT_BEGIN){
                objects.add(readObjectAsDictionnary(reader));
            }else if(type==JSONElement.TYPE_ARRAY_END){
                break;
            }else{
                throw new IOException("Unexpected json element : "+element);
            }
        }
        return objects.toArray();
    }


    private static Document readObjectDoc(AbstractJSONReader reader) throws IOException{
        final Document node = new DefaultDocument(true);

        Chars propName = null;
        Object propValue = null;

        while(reader.hashNext()){
            final JSONElement element = reader.next();
            final int type = element.getType();

            if(propName==null){
                //name or end expected
                if(type==JSONElement.TYPE_NAME){
                    propName = (Chars) element.getValue();
                }else if(type==JSONElement.TYPE_OBJECT_END){
                    break;
                }else if(type==JSONElement.TYPE_COMMENT){
                    continue;
                }else{
                    throw new IOException("Unexpected json element : "+element);
                }
            }else{
                //value expected
                if(type==JSONElement.TYPE_VALUE){
                    propValue = element.getValue();
                }else if(type==JSONElement.TYPE_ARRAY_BEGIN){
                    propValue = readArrayDoc(reader);
                }else if(type==JSONElement.TYPE_OBJECT_BEGIN){
                    propValue = readObjectDoc(reader);
                }else if(type==JSONElement.TYPE_COMMENT){
                    continue;
                }else{
                    throw new IOException("Unexpected json element : "+element);
                }

                node.setFieldValue(propName, propValue);
                propValue = null;
                propName = null;
            }
        }
        return node;
    }

    private static Object[] readArrayDoc(AbstractJSONReader reader) throws IOException{
        final Sequence objects = new ArraySequence();

        while(reader.hashNext()){
            final JSONElement element = reader.next();
            final int type = element.getType();

            if(type==JSONElement.TYPE_VALUE){
                objects.add(element.getValue());
            }else if(type==JSONElement.TYPE_ARRAY_BEGIN){
                objects.add(readArrayDoc(reader));
            }else if(type==JSONElement.TYPE_OBJECT_BEGIN){
                objects.add(readObjectDoc(reader));
            }else if(type==JSONElement.TYPE_ARRAY_END){
                break;
            }else{
                throw new IOException("Unexpected json element : "+element);
            }
        }
        return objects.toArray();
    }

}
