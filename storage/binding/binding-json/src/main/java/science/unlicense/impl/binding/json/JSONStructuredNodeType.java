
package science.unlicense.impl.binding.json;

import science.unlicense.api.model.tree.NodeType;

/**
 * Extend the common tree type model to store json additional informations.
 * JSON schema description can be found here :
 * http://tools.ietf.org/html/draft-zyp-json-schema-03
 *
 * @author Johann Sorel
 */
public interface JSONStructuredNodeType extends NodeType {

}
