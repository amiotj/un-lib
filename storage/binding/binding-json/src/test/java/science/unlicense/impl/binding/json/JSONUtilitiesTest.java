
package science.unlicense.impl.binding.json;

import science.unlicense.impl.binding.json.JSONUtilities;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class JSONUtilitiesTest {
    
    @Test
    public void readTreeTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "    \"person\": {\n" +
                "        \"id\": 25.4,\n" +
                "        \"name\": \"Paul\",\n" +
                "        \"data\": [\n" +
                "            true,\n" +
                "            false,\n" +
                "            null,\n" +
                "            -15,\n" +
                "            \"test\"\n" +
                "            ]\n" +
                "    }\n" +
                "}"
        );
        
        final NamedNode node = JSONUtilities.readAsTree(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));
        Assert.assertNotNull(node);        
        Assert.assertNull(node.getName());
        Assert.assertEquals(1, node.getChildren().getSize());
        
        final NamedNode person = (NamedNode)((Sequence)node.getChildren()).get(0);
        Assert.assertEquals(new Chars("person"), person.getName());
        Assert.assertEquals(3, person.getChildren().getSize());
        
        final NamedNode id = (NamedNode)((Sequence)person.getChildren()).get(0);
        final NamedNode name = (NamedNode)((Sequence)person.getChildren()).get(1);
        final NamedNode data = (NamedNode)((Sequence)person.getChildren()).get(2);
        Assert.assertEquals(new Chars("id"), id.getName());
        Assert.assertEquals(new Chars("name"), name.getName());
        Assert.assertEquals(new Chars("data"), data.getName());
        Assert.assertEquals(25.4, id.getValue());
        Assert.assertEquals(new Chars("Paul"), name.getValue());
        Assert.assertTrue(Arrays.equals(new Object[]{true,false,null,-15,new Chars("test")}, (Object[])data.getValue()));
        
    }
    
    @Test
    public void readDictionaryTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "    \"person\": {\n" +
                "        \"id\": 25.4,\n" +
                "        \"name\": \"Paul\",\n" +
                "        \"data\": [\n" +
                "            true,\n" +
                "            false,\n" +
                "            null,\n" +
                "            -15,\n" +
                "            \"test\"\n" +
                "            ]\n" +
                "    }\n" +
                "}"
        );
        
        final Dictionary node = JSONUtilities.readAsDictionary(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));
        Assert.assertNotNull(node);
        Assert.assertEquals(1, node.getKeys().getSize());
        
        final Dictionary person = (Dictionary) node.getValue(new Chars("person"));
        Assert.assertEquals(3, person.getKeys().getSize());
        
        Assert.assertEquals(25.4, person.getValue(new Chars("id")));
        Assert.assertEquals(new Chars("Paul"), person.getValue(new Chars("name")));
        Assert.assertTrue(Arrays.equals(new Object[]{true,false,null,-15,new Chars("test")}, 
                (Object[])person.getValue(new Chars("data"))));
        
    }
    
}
