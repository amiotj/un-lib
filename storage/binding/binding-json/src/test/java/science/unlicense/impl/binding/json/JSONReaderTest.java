

package science.unlicense.impl.binding.json;

import science.unlicense.impl.binding.json.JSONElement;
import science.unlicense.impl.binding.json.JSONReader;
import junit.framework.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class JSONReaderTest {
    
    @Test
    public void readTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "    \"person\": {\n" +
                "        \"id\": 25.4,\n" +
                "        \"name\": \"Paul\",\n" +
                "        \"data\": [\n" +
                "            true,\n" +
                "            false,\n" +
                "            null,\n" +
                "            -15,\n" +
                "            \"test\"\n" +
                "            ]\n" +
                "    }\n" +
                "}"
        );
        
        final JSONReader reader = new JSONReader();
        reader.setInput(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));
        
        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
        assertJson(JSONElement.TYPE_NAME, new Chars("person"), reader.next());
        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("id"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, 25.4d, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("name"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, new Chars("Paul"), reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("data"), reader.next());
            assertJson(JSONElement.TYPE_ARRAY_BEGIN, null, reader.next());
                assertJson(JSONElement.TYPE_VALUE, true, reader.next());
                assertJson(JSONElement.TYPE_VALUE, false, reader.next());
                assertJson(JSONElement.TYPE_VALUE, null, reader.next());
                assertJson(JSONElement.TYPE_VALUE, -15, reader.next());
                assertJson(JSONElement.TYPE_VALUE, new Chars("test"), reader.next());
            assertJson(JSONElement.TYPE_ARRAY_END, null, reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
    }
    
    private void assertJson(int type, Object value, JSONElement element){
        Assert.assertEquals(type,element.getType());
        Assert.assertEquals(value,element.getValue());
    }
    
}
