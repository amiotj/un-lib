

package science.unlicense.impl.binding.hjson;

import science.unlicense.impl.binding.json.JSONElement;
import science.unlicense.impl.binding.hjson.HJSONReader;
import junit.framework.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class HJSONReaderTest {
    
    @Test
    public void readJsonConformTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "    \"person\": {\n" +
                "        \"id\": 25.4,\n" +
                "        \"name\": \"Paul\",\n" +
                "        \"data\": [\n" +
                "            true,\n" +
                "            false,\n" +
                "            null,\n" +
                "            -15,\n" +
                "            \"test\"\n" +
                "            ]\n" +
                "    }\n" +
                "}"
        );
        
        final HJSONReader reader = new HJSONReader();
        reader.setInput(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));
        
        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
        assertJson(JSONElement.TYPE_NAME, new Chars("person"), reader.next());
        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("id"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, 25.4d, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("name"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, new Chars("Paul"), reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("data"), reader.next());
            assertJson(JSONElement.TYPE_ARRAY_BEGIN, null, reader.next());
                assertJson(JSONElement.TYPE_VALUE, true, reader.next());
                assertJson(JSONElement.TYPE_VALUE, false, reader.next());
                assertJson(JSONElement.TYPE_VALUE, null, reader.next());
                assertJson(JSONElement.TYPE_VALUE, -15, reader.next());
                assertJson(JSONElement.TYPE_VALUE, new Chars("test"), reader.next());
            assertJson(JSONElement.TYPE_ARRAY_END, null, reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
    }

    @Test
    public void readNoCommaTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "   \"id\": 25.4\n" +
                "   \"name\": \"Paul\"\n" +
                "}"
        );

        final HJSONReader reader = new HJSONReader();
        reader.setInput(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));

        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("id"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, 25.4d, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("name"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, new Chars("Paul"), reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
    }

    @Test
    public void readNoQuoteTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "   id : 25.4\n" +
                "   name: Paul\n" +
                "}"
        );

        final HJSONReader reader = new HJSONReader();
        reader.setInput(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));

        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("id"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, 25.4d, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("name"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, new Chars("Paul"), reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
    }

    @Test
    public void readMultilineTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "   name: "+
                "''' Paul is\n" +
                "a very long name\n" +
                "for sure'''\n" +
                "}"
        );

        final HJSONReader reader = new HJSONReader();
        reader.setInput(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));

        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("name"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, new Chars(" Paul is\na very long name\nfor sure"), reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
    }

    @Test
    public void readCommentTest() throws IOException{
        final Chars text = new Chars(
                "{\n" +
                "   # comment 1\n"+
                "   id : 25.4\n" +
                "   // comment 2\n"+
                "   name: Paul\n" +
                "   /* comment 3 */\n"+
                "}"
        );

        final HJSONReader reader = new HJSONReader();
        reader.setInput(new ArrayInputStream(text.toBytes(CharEncodings.UTF_8)));

        assertJson(JSONElement.TYPE_OBJECT_BEGIN, null, reader.next());
            assertJson(JSONElement.TYPE_COMMENT, new Chars(" comment 1"), reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("id"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, 25.4d, reader.next());
            assertJson(JSONElement.TYPE_COMMENT, new Chars(" comment 2"), reader.next());
            assertJson(JSONElement.TYPE_NAME, new Chars("name"), reader.next());
            assertJson(JSONElement.TYPE_VALUE, new Chars("Paul"), reader.next());
            assertJson(JSONElement.TYPE_COMMENT, new Chars(" comment 3 "), reader.next());
        assertJson(JSONElement.TYPE_OBJECT_END, null, reader.next());
    }
    
    private void assertJson(int type, Object value, JSONElement element){
        Assert.assertEquals(type,element.getType());
        Assert.assertEquals(value,element.getValue());
    }
    
}
