
package science.unlicense.impl.binding.generator;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.codehaus.plexus.compiler.Compiler;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.compiler.CompilerConfiguration;
import org.codehaus.plexus.compiler.CompilerMessage;
import org.codehaus.plexus.compiler.CompilerResult;
import org.codehaus.plexus.compiler.manager.CompilerManager;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 * Compile and execute a class located in the module.
 * This approach allows to create source code files before compilation
 * without writing specific maven mojo for each case.
 * 
 * @author Johann Sorel
 */
@Mojo(name = "generator", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresProject = true, 
        requiresDependencyResolution = ResolutionScope.COMPILE)
public class GeneratorCallerMojo extends AbstractMojo {

    /**
     * Plexus compiler manager.
     */
    @Component
    private CompilerManager compilerManager;

    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;
        
    /**
     * Path to generator sources.
     */
    @Parameter(property = "generatorSources", required = true, readonly = true)
    protected List<String> generatorSources;

    /**
     * Fulle generator class name.
     */
    @Parameter(property = "generatorClassName", required = true, readonly = true)
    protected String generatorClassName;
    
    /**
     * Folder where to store generated code.
     */
    @Parameter(property = "sourcePath", required = true, readonly = true)
    protected String sourcePath;

    /**
     * The compiler id of the compiler to use.
     * See maven-compiler plugin.
     */
    @Parameter( property = "maven.compiler.compilerId", defaultValue = "javac" )
    private String compilerId;
    

    @Override
    public void execute() throws MojoExecutionException {
        if(File.separator.equals("\\")){
            //windows hack
            if(generatorSources==null) generatorSources = new ArrayList();
            for(int i=0,n=generatorSources.size();i<n;i++){
                generatorSources.set(i, generatorSources.get(i).replace('/', '\\'));
            }
            sourcePath = sourcePath.replace('/', '\\');
        }
        project.addCompileSourceRoot(sourcePath);

        try {
            //compile the generator class
            final List<String> elements = new ArrayList<String>(project.getCompileClasspathElements());
            final CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
            compilerConfiguration.setOutputLocation(sourcePath);
            compilerConfiguration.setClasspathEntries(elements);
            compilerConfiguration.setOptimize(true);
            compilerConfiguration.setVerbose(false);
            compilerConfiguration.setShowWarnings(true);
            compilerConfiguration.setShowDeprecation(true);
            compilerConfiguration.setSourceVersion("1.6");
            compilerConfiguration.setTargetVersion("1.6");
            final Set<File> generatorFiles = new HashSet<File>();
            for(int i=0,n=generatorSources.size();i<n;i++){
                listSourceFiles(new File(generatorSources.get(i)),generatorFiles);
            }
            compilerConfiguration.setSourceFiles(generatorFiles);

            final Compiler compiler = compilerManager.getCompiler(compilerId);
            final CompilerResult compilerResult = compiler.performCompile( compilerConfiguration );
            if(!compilerResult.isSuccess()){
                final CharBuffer sb = new CharBuffer();
                for(CompilerMessage cm : compilerResult.getCompilerMessages()){
                    sb.append(cm.getMessage());
                    sb.append('\n');
                }
                throw new MojoExecutionException("Generator compilation failed\n"+sb.toString());
            }else{
                System.out.println("Generator compilation successful.");
            }


            //execute the generator
            final Set<URL> urls = new HashSet<URL>();
            urls.add(new File(sourcePath).toURI().toURL());
            for (String element : elements) {
                urls.add(new File(element).toURI().toURL());
            }
            final ClassLoader generatorCL = URLClassLoader.newInstance(
                    urls.toArray(new URL[0]),
                    Thread.currentThread().getContextClassLoader());

            final Class generatorClazz = generatorCL.loadClass(generatorClassName);
            final Object generator = generatorClazz.newInstance();
            final Method method = generatorClazz.getMethod("execute",Dictionary.class);
            
            final Dictionary params = new HashDictionary();
            params.add("modulePath", project.getBasedir().getPath());
            method.invoke(generator,params);


        } catch (Exception e) {
            if(e instanceof MojoExecutionException){
                throw (MojoExecutionException)e;
            }else{
                throw new MojoExecutionException(e.getMessage(), e);
            }
        }

    }
    
    private static Set<File> listSourceFiles(File f, Set<File> sources){
        if(sources == null){
            sources = new HashSet<File>();
        }
        
        if(f.isDirectory()){
            for(File c : f.listFiles()){
                listSourceFiles(c, sources);
            }
        }else if(f.getName().endsWith(".java")){
            sources.add(f);
        }
        
        return sources;
    }
    
}
