
package science.unlicense.impl.binding.ini;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/INI_file
 *
 * @author Johann Sorel
 */
public class INIFormat extends DefaultFormat {

    public INIFormat() {
        super(new Chars("ini"),
              new Chars("INI"),
              new Chars("Initialization file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ini")
              },
              new byte[][]{
                });
    }

}
