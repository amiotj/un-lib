
package science.unlicense.impl.binding.ini;

import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;

/**
 * 
 * @author Johann Sorel
 */
public class INIReader extends AbstractReader {
        
    private final CharEncoding encoding;

    public INIReader() {
        this(CharEncodings.UTF_8,null);
    }

    public INIReader(CharEncoding encoding, DocumentType docType) {
        this.encoding = encoding;
    }
    
    public Document read() throws IOException{
        final CharInputStream cs = getInputAsCharStream(encoding);

        final Document root = new DefaultDocument(true);

        Document current = root;
        for (Chars line=cs.readLine();line!=null;line=cs.readLine()) {
            line = line.trim();

            if (line.isEmpty()) {
                continue;
            } else if (line.startsWith(';')) {
                //skip comments
                //TODO : add them on next property/section as attribute ?
                continue;
            } else if (line.startsWith('[')) {
                //new section
                Chars name = line.truncate(1, line.getFirstOccurence(']'));
                current = new DefaultDocument(true);
                root.setFieldValue(name, current);
            } else {
                //new property
                final int sep = line.getFirstOccurence('=');
                if (sep>0) {
                    final Chars propName = line.truncate(0, sep).trim();
                    final Chars propValue = line.truncate(sep+1, -1).trim();
                    current.setFieldValue(propName, propValue);
                } else {
                    throw new IOException("Unvalid line "+line);
                }
            }
        }

        return root;
    }
    
}
