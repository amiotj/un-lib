
package science.unlicense.impl.binding.ini;

import science.unlicense.impl.binding.ini.INIReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;

/**
 * INI reader test.
 * 
 * @author Johann Sorel
 */
public class INIReaderTest {

    /**
     * Read ini file test
     *
     * @throws IOException
     */
    @Test
    public void readDefaultTest() throws IOException {

        Chars csv = new Chars(
                "; config file\n"
              + "id=1234\n"
              + " \n "
              + "[data]\n"
              + "isbn = ABCD \n"
              + "name = The Big Book \n"
              + "; a nice book\n"
              + "[user]\n"
              + "\t name = tom\n"
              + "job=none");

        INIReader reader = new INIReader();
        reader.setInput(csv.toBytes());

        Document doc = reader.read();
        Assert.assertEquals(new Chars("1234"), doc.getFieldValue(new Chars("id")));
        Document data = (Document) doc.getFieldValue(new Chars("data"));
        Assert.assertEquals(new Chars("ABCD"), data.getFieldValue(new Chars("isbn")));
        Assert.assertEquals(new Chars("The Big Book"), data.getFieldValue(new Chars("name")));
        Document user = (Document) doc.getFieldValue(new Chars("user"));
        Assert.assertEquals(new Chars("tom"), user.getFieldValue(new Chars("name")));
        Assert.assertEquals(new Chars("none"), user.getFieldValue(new Chars("job")));
    }

}
