
package science.unlicense.impl.binding.html;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.xml.XMLOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class HTMLOutputStream extends XMLOutputStream{

    private static final Chars H1 = new Chars("h1");
    private static final Chars H2 = new Chars("h2");
    private static final Chars H3 = new Chars("h3");
    private static final Chars H4 = new Chars("h4");
    private static final Chars H5 = new Chars("h5");

    private static final Chars BR = new Chars("br");
    private static final Chars HR = new Chars("hr");
    private static final Chars SPAN = new Chars("span");
    private static final Chars CLASS = new Chars("class");
    private static final Chars A = new Chars("a");
    private static final Chars NAME = new Chars("name");
    private static final Chars HREF = new Chars("href");
    private static final Chars TARGET = new Chars("target");

    public HTMLOutputStream(){        
    }

    public void writeH(int value, CharArray text) throws IOException{
        switch(value){
            case 1 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            case 2 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            case 3 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            case 4 : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
            default : writeElementStart(null, H1);writeText(text);writeElementEnd(null, H1); break;
        }
    }

    public void writeBr() throws IOException{
        writeElementStart(null, BR);
        writeElementEnd(null, BR);
    }

    public void writeHr() throws IOException{
        writeElementStart(null, HR);
        writeElementEnd(null, HR);
    }

    public void writeSpanText(CharArray text, Chars styleClass) throws IOException{
        writeElementStart(null, SPAN);
        if(styleClass!=null){
            writeProperty(CLASS, styleClass);
        }
        writeText(text);
        writeElementEnd(null, SPAN);
    }

    public void writeAnchor(Chars text, Chars name) throws IOException{
        writeElementStart(null, A);
        writeProperty(NAME, name);
        writeText(text);
        writeElementEnd(null, A);
    }

    public void writeAText(Chars text, Chars href, Chars target) throws IOException{
        writeElementStart(null, A);
        writeProperty(HREF, href);
        if(target!=null){
            writeProperty(TARGET,target);
        }
        writeText(text);
        writeElementEnd(null, A);
    }

}
