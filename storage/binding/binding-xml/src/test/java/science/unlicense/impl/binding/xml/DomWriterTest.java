
package science.unlicense.impl.binding.xml;

import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLOutputStream;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.impl.binding.xml.dom.DomComment;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.binding.xml.dom.DomNode;
import science.unlicense.impl.binding.xml.dom.DomWriter;

/**
 *
 * @author Johann Sorel
 */
public class DomWriterTest {

     private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void writeTest() throws Exception {

        //<human gender = "male" >
        //    <!--someone very important-->
        //    <firstName>hubert</firstName>
        //    <lastName>dupont</lastName>
        //    <single/>
        //</human>
        final DomElement human = new DomElement(new FName(null, new Chars("human", ENC)));
        human.getProperties().add(new Chars("gender", ENC), new Chars("male", ENC));
        final DomNode comment = new DomComment(new Chars("someone very important", ENC));
        final DomNode firstName = new DomElement(
                new FName(null, new Chars("firstName", ENC)),
                new Chars("hubert", ENC));
        final DomNode lastName = new DomElement(
                new FName(null, new Chars("lastName", ENC)),
                new Chars("dupont", ENC));
        final DomNode single = new DomElement(new FName(null, new Chars("single", ENC)));
        
        human.getChildren().add(comment);
        human.getChildren().add(firstName);
        human.getChildren().add(lastName);
        human.getChildren().add(single);
        
        final ArrayOutputStream back = new ArrayOutputStream();
        final XMLOutputStream stream = new XMLOutputStream();
        stream.setEncoding(ENC);
        stream.setIndent(new Chars("    ", ENC));
        stream.setOutput(back);

        final DomWriter writer = new DomWriter();
        writer.setOutput(stream);
        writer.write(human);
        writer.dispose();
        
        Chars result = new Chars(back.getBuffer().toArrayByte());
        
        final Chars expected = new Chars(
                  "<human gender=\"male\">\n"
                + "    <!--someone very important-->\n"
                + "    <firstName>hubert</firstName>\n"
                + "    <lastName>dupont</lastName>\n"
                + "    <single/>\n"
                + "</human>", ENC);
        Assert.assertEquals(expected, result);

    }
}
