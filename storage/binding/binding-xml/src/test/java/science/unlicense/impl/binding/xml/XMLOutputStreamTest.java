package science.unlicense.impl.binding.xml;

import science.unlicense.impl.binding.xml.XMLOutputStream;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class XMLOutputStreamTest {

    private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void writeNoIndentTest() throws Exception {

        final ArrayOutputStream back = new ArrayOutputStream();
        final XMLOutputStream stream = new XMLOutputStream();
        stream.setOutput(back);
        stream.setEncoding(ENC);
        
        
        //<?xml version="1.0" encoding="UTF-8"?>--------------------------------
        stream.writeMetaStart(new Chars("xml",ENC));
        stream.writeProperty(new Chars("version",ENC), new Chars("1.0",ENC));
        stream.writeProperty(new Chars("encoding",ENC), new Chars("UTF-8",ENC));
        stream.writeMetaEnd();
        
        //<human gender = "male" >----------------------------------------------
        stream.writeElementStart(null,new Chars("human",ENC));
        stream.writeProperty(new Chars("gender",ENC), new Chars("male",ENC));
        
        //    <!--someone very important-->-------------------------------------
        stream.writeComment(new Chars("someone very important",ENC));
        
        //    <firstName>hubert</firstName>-------------------------------------
        stream.writeElementStart(null,new Chars("firstName",ENC));
        stream.writeText(new Chars("hubert",ENC));       
        stream.writeElementEnd(null,new Chars("firstName",ENC));
        
        //    <lastName>dupont</lastName>---------------------------------------
        stream.writeElementStart(null,new Chars("lastName",ENC));
        stream.writeText(new Chars("dupont",ENC));       
        stream.writeElementEnd(null,new Chars("lastName",ENC));
        
        //    <single/>---------------------------------------------------------
        stream.writeElementStart(null,new Chars("single",ENC));
        stream.writeElementEnd(null,new Chars("single",ENC));
        
        //</human>--------------------------------------------------------------
        stream.writeElementEnd(null,new Chars("human",ENC));
        
        stream.dispose();
        
        Chars result = new Chars(back.getBuffer().toArrayByte());
        
        final Chars expected = new Chars(
                  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<human gender=\"male\">"
                + "<!--someone very important-->"
                + "<firstName>hubert</firstName>"
                + "<lastName>dupont</lastName>"
                + "<single/>"
                + "</human>", ENC);
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void writeIndentTest() throws Exception {

        final ArrayOutputStream back = new ArrayOutputStream();
        final XMLOutputStream stream = new XMLOutputStream();
        stream.setOutput(back);
        stream.setEncoding(ENC);
        stream.setIndent(new Chars("    ", ENC));
        
        
        //<?xml version="1.0" encoding="UTF-8"?>--------------------------------
        stream.writeMetaStart(new Chars("xml",ENC));
        stream.writeProperty(new Chars("version",ENC), new Chars("1.0",ENC));
        stream.writeProperty(new Chars("encoding",ENC), new Chars("UTF-8",ENC));
        stream.writeMetaEnd();
        
        //<human gender = "male" >----------------------------------------------
        stream.writeElementStart(null,new Chars("human",ENC));
        stream.writeProperty(new Chars("gender",ENC), new Chars("male",ENC));
        
        //<!--someone very important-->-----------------------------------------
        stream.writeComment(new Chars("someone very important",ENC));
        
        //<firstName>hubert</firstName>-----------------------------------------
        stream.writeElementStart(null,new Chars("firstName",ENC));
        stream.writeText(new Chars("hubert",ENC));       
        stream.writeElementEnd(null,new Chars("firstName",ENC));
        
        //<lastName>dupont</lastName>-------------------------------------------
        stream.writeElementStart(null,new Chars("lastName",ENC));
        stream.writeText(new Chars("dupont",ENC));       
        stream.writeElementEnd(null,new Chars("lastName",ENC));
        
        //<single/>-------------------------------------------------------------
        stream.writeElementStart(null,new Chars("single",ENC));
        stream.writeElementEnd(null,new Chars("single",ENC));
        
        //</human>--------------------------------------------------------------
        stream.writeElementEnd(null,new Chars("human",ENC));
        
        stream.dispose();
        
        Chars result = new Chars(back.getBuffer().toArrayByte());
        
        final Chars expected = new Chars(
                  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<human gender=\"male\">\n"
                + "    <!--someone very important-->\n"
                + "    <firstName>hubert</firstName>\n"
                + "    <lastName>dupont</lastName>\n"
                + "    <single/>\n"
                + "</human>", ENC);
        Assert.assertEquals(expected, result);
    }
    
}
