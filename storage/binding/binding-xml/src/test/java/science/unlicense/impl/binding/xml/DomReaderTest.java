
package science.unlicense.impl.binding.xml;

import science.unlicense.impl.binding.xml.XMLInputStream;
import science.unlicense.impl.binding.xml.FName;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.path.Path;
import science.unlicense.api.model.tree.Node;
import science.unlicense.impl.binding.xml.dom.DomComment;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.binding.xml.dom.DomReader;

/**
 *
 * @author Johann Sorel
 */
public class DomReaderTest {

     private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void readTest() throws Exception {

        final XMLInputStream stream = new XMLInputStream();
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE));

        final DomReader reader = new DomReader();
        reader.setInput(stream);

        Node node = reader.read();
        Assert.assertTrue(node instanceof DomElement);
        
        // <human gender = "male" > --------------------------------------------
        final DomElement human = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("human", ENC)), human.getName());
        Assert.assertEquals(null, human.getText());
        Assert.assertEquals(1, human.getProperties().getSize());
        Assert.assertEquals(5,  human.getChildren().getSize());
        
        // <!-- someone very important --> -------------------------------------
        node = (Node) human.getChildren().get(0);
        Assert.assertTrue(node instanceof DomComment);
        DomComment cmt = (DomComment) human.getChildren().get(0);
        Assert.assertEquals(new Chars("someone very important", ENC), cmt.getText());
        
        // <firstName>hubert</firstName>----------------------------------------
        node = (Node) human.getChildren().get(1);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement firstName = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("firstName", ENC)), firstName.getName());
        Assert.assertEquals(new Chars("hubert", ENC), firstName.getText());
        Assert.assertEquals(0, firstName.getProperties().getSize());
        
        // <lastName>dupont</lastName> -----------------------------------------
        node = (Node) human.getChildren().get(2);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement lastName = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("lastName", ENC)), lastName.getName());
        Assert.assertEquals(new Chars("dupont", ENC), lastName.getText());
        Assert.assertEquals(0, lastName.getProperties().getSize());
        
        // <children>0</children> -----------------------------------------
        node = (Node) human.getChildren().get(3);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement children = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("children", ENC)), children.getName());
        Assert.assertEquals(new Chars("0", ENC), children.getText());
        Assert.assertEquals(0, children.getProperties().getSize());
        
        // <single/> -----------------------------------------------------------
        node = (Node) human.getChildren().get(4);
        Assert.assertTrue(node instanceof DomElement);
        final DomElement single = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("single", ENC)), single.getName());
        Assert.assertEquals(null, single.getText());
        Assert.assertEquals(0, single.getProperties().getSize());

        System.out.println(human);
    }
    
    @Test
    public void readTypedTest() throws Exception {

        final XMLInputStream stream = new XMLInputStream();
        stream.setEncoding(ENC);
        stream.setInput(new ArrayInputStream(XMLTestConstants.SAMPLE_DOCTYPE));

        final DomReader reader = new DomReader();
        reader.setInput(stream);

        Node node = reader.read();
        Assert.assertTrue(node instanceof DomElement);
        
        // <human gender = "male" > --------------------------------------------
        final DomElement human = (DomElement) node;
        Assert.assertEquals(new FName(null,new Chars("human", ENC)), human.getName());
        Assert.assertEquals(null, human.getText());
        Assert.assertEquals(0, human.getProperties().getSize());
        Assert.assertEquals(0,  human.getChildren().getSize());
        
        System.out.println(human);
    }
}
