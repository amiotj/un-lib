
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLConstants;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDInclude extends XSDAnnotated{

    public static final FName NAME = new FName(NS_BASE,new Chars("include",CharEncodings.US_ASCII));
    
    public static final FName ATT_SCHEMA_LOCATION = new FName(XMLConstants.NS_NONE,new Chars("schemaLocation",CharEncodings.US_ASCII));
    
    /**
     * <xs:attribute name="schemaLocation" type="xs:anyURI" use="required"/>
     */
    public Chars schemaLocation;
}
