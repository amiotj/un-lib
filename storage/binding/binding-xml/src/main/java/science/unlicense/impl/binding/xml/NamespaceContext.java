
package science.unlicense.impl.binding.xml;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public class NamespaceContext {

    private final NamespaceContext parent;
    private Dictionary prefixToNs;
    private Dictionary nsToPrefix;
    private Chars defaultNamespace = null;

    public NamespaceContext() {
        this(null);
        //default namespace mandatory
        //this namespace is not defined in xml files
        prefixToNs = new HashDictionary();
        nsToPrefix = new HashDictionary();
        add(new Chars("xml"), XMLConstants.NS_NAMESPACE);
    }

    public NamespaceContext(NamespaceContext parent) {
        this.parent = parent;
    }

    public NamespaceContext getParent() {
        return parent;
    }
    
    public NamespaceContext createChildContext(){
        return new NamespaceContext(this);
    }

    public Chars getDefaultNamespace() {
        if(defaultNamespace==null && parent!=null){
            return parent.getDefaultNamespace();
        } else{
            return defaultNamespace;
        }
    }
    
    public Chars getNamespace(Chars prefix){
        if(prefix==null && defaultNamespace!=null) return defaultNamespace;
        Chars c = null;
        if(prefixToNs!=null) c = (Chars) prefixToNs.getValue(prefix);
        if(c==null && parent!=null) c = parent.getNamespace(prefix);
        if(c==null) throw new InvalidArgumentException("Unmapped prefix : "+ prefix);
        return c;
    }
    
    public Chars getPrefix(Chars namespace){
        if(namespace==XMLConstants.NS_NONE) return null;
        Chars c = null;
        if(nsToPrefix!=null) c = (Chars) nsToPrefix.getValue(namespace);
        if(c==null && parent!=null) c = parent.getPrefix(namespace);
        if(c==null && CObjects.equals(getDefaultNamespace(), namespace)) return null;
        if(c==null) throw new InvalidArgumentException("Unmapped namespace : "+ namespace);
        return c;
    }
    
    public void add(Chars prefix, Chars namespace){
        if(prefixToNs==null){
            prefixToNs = new HashDictionary();
            nsToPrefix = new HashDictionary();
        }
        prefixToNs.add(prefix, namespace);
        nsToPrefix.add(namespace, prefix);
        if(prefix==null){
            defaultNamespace = namespace;
        }
    }
    
    /**
     * 
     * @param qname in form 'prefix:localPart'
     * @return FName
     */
    public FName toFName(Chars qname){
        final int idx = qname.getFirstOccurence(':');
        return new FName(getNamespace(qname.truncate(0, idx)), qname.truncate(idx+1, qname.getCharLength()));
    }
    
    public FName toFName(QName qname){
        return new FName(getNamespace(qname.getPrefix()), qname.getLocalPart());
    }
    
    public QName toQName(FName fname){
        return new QName(getPrefix(fname.getNamespace()), fname.getLocalPart());
    }
    
    public Chars toQNameChars(FName fname){
        final Chars prefix = getPrefix(fname.getNamespace());
        if(prefix==null||prefix.isEmpty()){
            return fname.getLocalPart();
        }else{
            return new CharBuffer().append(prefix).append(':').append(fname.getLocalPart()).toChars();
        }
    }
    
    public boolean equals(FName fname, Chars qname){
        final int idx = qname.getFirstOccurence(':');
        if(idx<0){
            if(!CObjects.equals(fname.getNamespace(), getNamespace(null))){
                return false;
            }
            return CObjects.equals(fname.getLocalPart(), qname);
        }else{
            final Chars prefix = qname.truncate(0, idx);
            if(!CObjects.equals(fname.getNamespace(), getNamespace(prefix))){
                return false;
            }
            final Chars localPart = qname.truncate(idx+1, qname.getCharLength());
            return CObjects.equals(fname.getLocalPart(), localPart);
        }
    }
    
    public boolean equals(FName fname, QName qname){
        return  CObjects.equals(fname.getLocalPart(), qname.getLocalPart()) &&
                CObjects.equals(fname.getNamespace(), getNamespace(qname.getPrefix()));
    }
    
}
