
package science.unlicense.impl.binding.xml.dom;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.model.tree.Node;

/**
 *
 * @author Johann Sorel
 */
public abstract class DomNode extends DefaultNode{

    private final DomNamespaceContext context = new DomNamespaceContext();

    public DomNode() {
        super(true);
    }

    public DomNode(Node[] children) {
        super(children);
    }

    public DomNamespaceContext getNamespaceContext() {
        return context;
    }

    public Chars thisToChars() {
        return new Chars("DomNode");
    }

}
