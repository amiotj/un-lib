
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 *
 * @author Johann Sorel
 */
public abstract class XSDOpenAttrs {

    /**
     * FName -> Chars
     * 
     * <xs:restriction base="xs:anyType">
     * <xs:anyAttribute namespace="##other" processContents="lax"/>
     * </xs:restriction>
     */
    public Dictionary otherAttributes = new HashDictionary();

}
