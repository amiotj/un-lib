
package science.unlicense.impl.binding.xml.schema;

import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.model.tree.DefaultNodeType;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.NodeType;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.schema.model.XSDSchema;

/**
 * List of XSD schemas.
 * Schemas nearly always have cross references.
 *
 * @author Johann Sorel
 */
public class XSDStack {

    private final Path baseXSD;
    private final Path basePath;
    private final boolean nsInNames;

    //Path > XSDSchema
    private final Dictionary schemas = new HashDictionary();
    //FName > NodeType
    private final Dictionary cacheType = new HashDictionary();
    //FName > NodeCardinality
    private final Dictionary cacheDesc = new HashDictionary();
    //FName > XSDBuilder
    private final Dictionary elements = new HashDictionary();
    //FName > XSDBuilder
    private final Dictionary types = new HashDictionary();

    public XSDStack(Path baseXSD, boolean nsInNames){
        this.baseXSD = baseXSD;
        this.basePath = baseXSD.getParent();
        this.nsInNames = nsInNames;
    }

    /**
     * Get the FName of all declared elements.
     * @return Collection
     */
    public Collection getElementsFName(){
        return elements.getKeys();
    }

    /**
     * Get the FName of all declared elements.
     * @return Collection
     */
    public Collection getTypesFName(){
        return types.getKeys();
    }

    public NodeCardinality read() throws IOException{
        final FName mainElement = explore(baseXSD);
        return getDescriptor(mainElement);
    }

    public Dictionary getSchemas() throws IOException {
        final FName mainElement = explore(baseXSD);
        return schemas;
    }
    /**
     *
     * @return the first declared element
     */
    private FName explore(Path candidate) throws IOException{
        
        final XSDReader xr = new XSDReader();
        xr.setInput(candidate);
        final XSDSchema xsd = xr.read();
        schemas.add(candidate, xsd);
        
        final XSDBuilder builder = new XSDBuilder(candidate,xsd,nsInNames);
        builder.read();

        FName first = null;

        //fill index
        final Sequence declared = builder.getDeclaredElements();
        if(!declared.isEmpty()){
            first = (FName) declared.get(0);
            for(int i=0,n=declared.getSize();i<n;i++){
                elements.add(declared.get(i), builder);
            }
        }

        final Sequence declaredTypes = builder.getDeclaredTypes();
        for(int i=0,n=declaredTypes.getSize();i<n;i++){
            types.add(declaredTypes.get(i), builder);
        }

        //explore sub xsd
        final Sequence related = new ArraySequence(builder.getRelatedSchemas());

        for(int i=0,n=related.getSize();i<n;i++){
            final Path str = (Path) related.get(i);
            explore(str);
        }

        return first;
    }

    public NodeCardinality getDescriptor(FName id) throws IOException{
        NodeCardinality desc = (NodeCardinality) cacheDesc.getValue(id);

        if(desc == null){
            //read and add it in the cache.
            final XSDBuilder source = (XSDBuilder) elements.getValue(id);
            if(source==null){
                throw new IOException("No XSD contains type : "+ id);
            }
            desc = source.readDescriptor(id,this);
            cacheDesc.add(id, desc);
        }

        return desc;
    }

    public NodeType getType(FName id) throws IOException{
        NodeType type = (NodeType) cacheType.getValue(id);

        if(type == null){
            //read and add it in the cache.
            final XSDBuilder source = (XSDBuilder) types.getValue(id);
            if(source == null){
                throw new IllegalStateException("No source for type id :" + id);
            }
            //precache node type to avoid infinite loop when cyclic references.
            final DefaultNodeType nt = new DefaultNodeType();
            cacheType.add(id, nt);
            source.readType(id,this,nt);
            //freeze type to avoid any more modifications
            nt.freeze();
            type = nt;
        }

        return type;
    }

}
