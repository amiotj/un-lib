
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public class FormChoice {

    public static final FormChoice QUALIFIED = new FormChoice(new Chars("qualified"));
    public static final FormChoice UNQUALIFIED = new FormChoice(new Chars("unqualified"));

    private final Chars text;

    private FormChoice(Chars text) {
        this.text = text;
    }

    public Chars getText() {
        return text;
    }

    public static FormChoice fromText(Chars text){
        if(text==null) return null;
        
        if(QUALIFIED.text.equals(text, true, true)){
            return QUALIFIED;
        }else if(UNQUALIFIED.text.equals(text, true, true)){
            return UNQUALIFIED;
        }else{
            throw new InvalidArgumentException("Unknowned value : "+text);
        }
    }

}
