
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLConstants;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * 
 * @author Johann Sorel
 */
public class XSDComplexType extends XSDAnnotated implements GRedefinable,GXNamed{

    public static final FName NAME = new FName(NS_BASE,new Chars("complexType",CharEncodings.US_ASCII));
    
    public static final FName ATT_NAME = new FName(XMLConstants.NS_NONE,new Chars("name",CharEncodings.US_ASCII));
    public static final FName ATT_MIXED = new FName(XMLConstants.NS_NONE,new Chars("mixed",CharEncodings.US_ASCII));
    public static final FName ATT_ABSTRACT = new FName(XMLConstants.NS_NONE,new Chars("abstract",CharEncodings.US_ASCII));
    public static final FName ATT_FINAL = new FName(XMLConstants.NS_NONE,new Chars("final",CharEncodings.US_ASCII));
    public static final FName ATT_BLOCK = new FName(XMLConstants.NS_NONE,new Chars("block",CharEncodings.US_ASCII));
    
    /**
     * <xs:group ref="xs:complexTypeModel"/>
     */
    public XSDSimpleContent simpleContent;
    public XSDComplexContent complexContent;
    public GTypeDefParticle typeDefParticle;
    /**
     *   <xs:choice minOccurs="0" maxOccurs="unbounded">
     *     <xs:element name="attribute" type="xs:attribute"/>
     *     <xs:element name="attributeGroup" type="xs:attributeGroupRef"/>
     *   </xs:choice>
     */
    public final Sequence attrDecls = new ArraySequence();
    /**
     *  <xs:element ref="xs:anyAttribute" minOccurs="0"/>
     */
    public XSDWildCard anyAttribute;
    
    /**
     * <xs:attribute name="name" type="xs:NCName">
     */
    public FName name;
    
    /**
     * <xs:attribute name="mixed" type="xs:boolean" use="optional" default="false">
     */
    public Boolean mixed;
    
    /**
     * <xs:attribute name="abstract" type="xs:boolean" use="optional" default="false"/>
     */
    public Boolean abstrct;
    
    /**
     * <xs:attribute name="final" type="xs:derivationSet"/>
     */
    public Chars finale;
    
    /**
     * <xs:attribute name="block" type="xs:derivationSet"/>
     */
    public Chars block;
    
    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }
}
