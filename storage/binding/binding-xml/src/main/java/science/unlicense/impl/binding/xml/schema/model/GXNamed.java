
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.impl.binding.xml.FName;

/**
 * Not part of the XSD model, but convinient to regroup all types that have names.
 * 
 * @author Johann Sorel
 */
public interface GXNamed {
 
    FName getName();
    
    void setName(FName name);
    
}
