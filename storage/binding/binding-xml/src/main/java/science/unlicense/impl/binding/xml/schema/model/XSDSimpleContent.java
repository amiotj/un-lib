
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.xml.FName;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * @author Johann Sorel
 */
public class XSDSimpleContent extends XSDAnnotated implements GComplexTypeModel{

    public static final FName NAME = new FName(NS_BASE,new Chars("simpleContent",CharEncodings.US_ASCII));
    
    /**
     * <xs:element name="restriction" type="xs:simpleRestrictionType"/>
     */
    public XSDSimpleRestrictionType restriction;

    /**
     * <xs:element name="extension" type="xs:simpleExtensionType"/>
     */
    public XSDSimpleExtensionType extension;
    
}
