
package science.unlicense.impl.binding.xml.dom;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLElement;
import science.unlicense.impl.binding.xml.XMLInputStream;

/**
 *
 * @author Johann Sorel
 */
public class DomReader {

    private boolean close;
    private Object input;
    private XMLInputStream stream;

    public DomReader() {
    }

    public final void setInput(Object source) {
        this.input = source;
        if(source instanceof XMLInputStream){
            this.stream = (XMLInputStream) source;
            close = false;
        }else{
            this.stream = new XMLInputStream();
            stream.setInput(source);
            this.stream.setEncoding(CharEncodings.UTF_8);
            close = true;
        }        
    }
    
    public Object getInput() {
        return input;
    }

    public DomNode read() throws IOException{
        return fill(stream);
    }

    public void dispose() throws IOException{
        if(close){
            stream.dispose();
        }
    }
    
    private DomNode fill(XMLInputStream input) throws IOException{
        input.setSkipEmptyText(true); //we don't want blank texts
        DomNode node = null;
        XMLElement element;

        final Sequence stack = new ArraySequence();

        for(;;){
            element = input.readElement();
            if(element == null){
                break;
            }

            if(element.getType() == XMLElement.TYPE_ELEMENT_START){
                //element
                final FName name = new FName(null, element.getName());
                DomNode current = new DomElement(name,input.readProperties());
                stack.add(current);
                
                if(stack.getSize()>1){
                    //add in parent
                    ((DomNode)stack.get(stack.getSize()-2)).getChildren().add(current);
                }
            }else if(element.getType() == XMLElement.TYPE_ELEMENT_END){
                
                if(stack.getSize() == 1){
                    node = (DomNode)stack.get(stack.getSize()-1);
                }
                stack.remove(stack.getSize()-1);
            }else if(element.getType() == XMLElement.TYPE_COMMENT){
                //comment
                DomComment current = new DomComment(input.readText());
                if(stack.getSize()>0){
                    //add in parent
                    ((DomNode)stack.get(stack.getSize()-1)).getChildren().add(current);
                }
            }else if(element.getType() == XMLElement.TYPE_TEXT){
                if(stack.getSize()>0){
                    final Chars text = input.readText();
                    DomElement n = (DomElement)stack.get(stack.getSize()-1);
                    n.setText(input.readText());
                }
            }
        }

        return node;
    }

}
