
package science.unlicense.impl.binding.xml;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Namespace extends CObject {

    private final Chars fullname;
    private final Chars prefix;

    public Namespace(final Chars namespace, final Chars prefix) {
        this.fullname = namespace;
        this.prefix = prefix;
    }

    public Chars getName() {
        return fullname;
    }

    public Chars getPrefix() {
        return prefix;
    }

    public Chars toChars() {
        return fullname;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Namespace other = (Namespace) obj;
        if (this.fullname != other.fullname && (this.fullname == null || !this.fullname.equals(other.fullname))) {
            return false;
        }
        return true;
    }


}
