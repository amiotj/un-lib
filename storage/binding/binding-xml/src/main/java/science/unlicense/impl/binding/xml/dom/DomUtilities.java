
package science.unlicense.impl.binding.xml.dom;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public final class DomUtilities {

    private DomUtilities(){}

    public static DomElement getNodeForName(DomNode parent, Chars name){
        final Iterator ite = parent.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if(next instanceof DomElement) {
                final DomElement element = (DomElement)next;
                if(element.getName().getLocalPart().equals(name)){
                    return element;
                }
            }
        }
        return null;
    }
    
}
