
package science.unlicense.impl.binding.xml.dom;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.impl.binding.xml.FName;

/**
 *
 * @author Johann Sorel
 */
public class DomElement extends DomNode {

    private final Dictionary properties = new HashDictionary();
    private FName name;
    private Chars text;

    public DomElement(FName name) {
        this.name = name;
    }

    public DomElement(FName name, Chars text) {
        this.name = name;
        this.text = text;
    }

    public DomElement(FName name, Dictionary properties) {
        this.name = name;
        if(properties != null){
            this.properties.addAll(properties);
        }
    }

    public FName getName() {
        return name;
    }

    public Chars getText() {
        return text;
    }

    public void setText(Chars text) {
        this.text = text;
    }

    public Dictionary getProperties() {
        return properties;
    }

    public Chars thisToChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append(new Chars(getName().toString(), CharEncodings.US_ASCII));
        if(text != null){
            sb.append(text);
        }
        final Iterator ite = properties.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            sb.append(new Char(' ', CharEncodings.US_ASCII));
            sb.append((Chars)pair.getValue1());
            sb.append(new Char(':', CharEncodings.US_ASCII));
            sb.append((Chars)pair.getValue2());
        }
        return sb.toChars();
    }
}
