
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLConstants;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * Part of group : simpleDerivation
 *
 * @author Johann Sorel
 */
public class XSDRestriction extends XSDAnnotated implements GSimpleDerivation{

    public static final FName NAME = new FName(NS_BASE,new Chars("restriction",CharEncodings.US_ASCII));

    public static final FName ATT_BASE = new FName(XMLConstants.NS_NONE,new Chars("base",CharEncodings.US_ASCII));

    /**
     * <xs:group ref="xs:simpleRestrictionModel"/>
     */
    public XSDLocalSimpleType simpleType;
    public final Sequence facets = new ArraySequence();

    /**
     * <xs:attribute name="base" type="xs:QName" use="optional"/>
     */
    public FName base;

}
