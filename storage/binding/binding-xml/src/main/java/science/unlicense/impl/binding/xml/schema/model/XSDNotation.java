
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.xml.FName;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 *
 * @author Johann Sorel
 */
public class XSDNotation implements GSchemaTop{

    public static final FName NAME = new FName(NS_BASE,new Chars("notation",CharEncodings.US_ASCII));
    
}
