
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLConstants;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 *
 * @author Johann Sorel
 */
public class XSDElement extends XSDAnnotated implements GSchemaTop,GXNamed{

    public static final FName NAME = new FName(NS_BASE,new Chars("element",CharEncodings.US_ASCII));

    public static final FName ATT_NAME = new FName(XMLConstants.NS_NONE,new Chars("name",CharEncodings.US_ASCII));
    public static final FName ATT_REF = new FName(XMLConstants.NS_NONE,new Chars("ref",CharEncodings.US_ASCII));
    public static final FName ATT_TYPE = new FName(XMLConstants.NS_NONE,new Chars("type",CharEncodings.US_ASCII));
    public static final FName ATT_SUBSTITUTION_GROUP = new FName(XMLConstants.NS_NONE,new Chars("substitutionGroup",CharEncodings.US_ASCII));
    public static final FName ATT_MINOCCURS = new FName(XMLConstants.NS_NONE,new Chars("minOccurs",CharEncodings.US_ASCII));
    public static final FName ATT_MAXOCCURS = new FName(XMLConstants.NS_NONE,new Chars("maxOccurs",CharEncodings.US_ASCII));
    public static final FName ATT_DEFAULT = new FName(XMLConstants.NS_NONE,new Chars("default",CharEncodings.US_ASCII));
    public static final FName ATT_FIXED = new FName(XMLConstants.NS_NONE,new Chars("fixed",CharEncodings.US_ASCII));
    public static final FName ATT_NILLABLE = new FName(XMLConstants.NS_NONE,new Chars("nillable",CharEncodings.US_ASCII));
    public static final FName ATT_ABSTRACT = new FName(XMLConstants.NS_NONE,new Chars("abstract",CharEncodings.US_ASCII));
    public static final FName ATT_FINAL = new FName(XMLConstants.NS_NONE,new Chars("final",CharEncodings.US_ASCII));
    public static final FName ATT_BLOCK = new FName(XMLConstants.NS_NONE,new Chars("block",CharEncodings.US_ASCII));
    public static final FName ATT_FORM = new FName(XMLConstants.NS_NONE,new Chars("form",CharEncodings.US_ASCII));
    
    /**
     * <xs:choice minOccurs="0">
     *   <xs:element name="simpleType" type="xs:localSimpleType"/>
     *   <xs:element name="complexType" type="xs:localComplexType"/>
     * </xs:choice>
     */
    public XSDSimpleType simpleType;
    public XSDComplexType complexType;

    /**
     * <xs:group ref="xs:identityConstraint" minOccurs="0" maxOccurs="unbounded"/>
     */
    public final Sequence identityConstraints = new ArraySequence();

    /**
     * <xs:attributeGroup ref="xs:defRef"/>
     */
    public FName name;
    public FName ref;

    /**
     * <xs:attribute name="type" type="xs:QName"/>
     */
    public FName type;

    /**
     * <xs:attribute name="substitutionGroup" type="xs:QName"/>
     */
    public FName substitutionGroup;

    /**
     * <xs:attributeGroup ref="xs:occurs"/>
     */
    public Chars minOccurs;
    public Chars maxOccurs;

    /**
     * <xs:attribute name="default" type="xs:string"/>
     */
    public Chars defaut;

    /**
     * <xs:attribute name="fixed" type="xs:string"/>
     */
    public Chars fixed;

    /**
     * <xs:attribute name="nillable" type="xs:boolean" use="optional" default="false"/>
     */
    public Boolean nillable;

    /**
     * <xs:attribute name="abstract" type="xs:boolean" use="optional" default="false"/>
     */
    public Boolean abstrat;

    /**
     * <xs:attribute name="final" type="xs:derivationSet"/>
     */
    public Chars finale;

    /**
     * <xs:attribute name="block" type="xs:blockSet"/>
     */
    public Chars block;
    
    /**
     * <xs:attribute name="form" type="xs:formChoice"/>
     */
    public FormChoice form;

    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }
}
