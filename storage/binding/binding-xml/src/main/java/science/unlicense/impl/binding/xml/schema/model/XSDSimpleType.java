
package science.unlicense.impl.binding.xml.schema.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.binding.xml.FName;
import science.unlicense.impl.binding.xml.XMLConstants;
import static science.unlicense.impl.binding.xml.schema.XSDConstants.NS_BASE;

/**
 *
 * 
 * @author Johann Sorel
 */
public class XSDSimpleType extends XSDAnnotated implements GRedefinable,GXNamed{

    public static final FName NAME = new FName(NS_BASE,new Chars("simpleType",CharEncodings.US_ASCII));
    
    public static final FName ATT_FINAL = new FName(XMLConstants.NS_NONE,new Chars("final",CharEncodings.US_ASCII));
    public static final FName ATT_NAME = new FName(XMLConstants.NS_NONE,new Chars("name",CharEncodings.US_ASCII));
    
    /**
     * <xs:group ref="xs:simpleDerivation"/>
     */
    public GSimpleDerivation derivation;

    /**
     * <xs:attribute name="final" type="xs:simpleDerivationSet"/>
     */
    public Chars finale;
    
    /**
     * <xs:attribute name="name" type="xs:NCName">
     */
    public FName name;
    
    public FName getName() {
        return name;
    }

    public void setName(FName name) {
        this.name = name;
    }
    
}
