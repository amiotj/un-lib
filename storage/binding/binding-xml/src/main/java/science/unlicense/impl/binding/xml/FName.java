
package science.unlicense.impl.binding.xml;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;

/**
 * Fully qualified name.
 * 
 * Namespace + local part
 * 
 * @author Johann Sorel
 */
public class FName extends CObject{

    private final Chars namespace;
    private final Chars localPart;

    public FName() {
        this.namespace = null;
        this.localPart = null;
    }

    public FName(Chars namespace, Chars localPart) {
        this.namespace = namespace;
        this.localPart = localPart;
    }

    public Chars getNamespace() {
        return namespace;
    }

    public Chars getLocalPart() {
        return localPart;
    }

    public int getHash() {
        int hash = 45;
        if(namespace!=null) hash+=namespace.getHash();
        if(localPart!=null) hash+=localPart.getHash();
        return hash;
    }

    public boolean equals(Object obj) {
        if(!(obj instanceof FName)) return false;
        final FName other = (FName) obj;
        return CObjects.equals(namespace, other.namespace) && CObjects.equals(localPart, other.localPart);
    }
    
    public Chars toChars() {
        if(namespace==null){
            return localPart;
        }else{
            return namespace.toChars().concat(' ').concat(localPart);
        }
    }
}
