
package science.unlicense.impl.fs.fuse;

import jnr.ffi.Pointer;
import jnr.ffi.types.off_t;
import jnr.ffi.types.size_t;
import ru.serce.jnrfuse.ErrorCodes;
import ru.serce.jnrfuse.FuseFillDir;
import ru.serce.jnrfuse.FuseStubFS;
import ru.serce.jnrfuse.struct.FileStat;
import ru.serce.jnrfuse.struct.FuseFileInfo;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class FusePathFS extends FuseStubFS {

    private final Path mountedPath;

    public FusePathFS(Path mountedPath) {
        this.mountedPath = mountedPath;
    }
    
    @Override
    public int getattr(String path, FileStat stat) {
        final Path candidate = mountedPath.resolve(new Chars(path));

        try {
            if (!candidate.exists()) {
                return -ErrorCodes.ENOENT();
            }

            int res = 0;
            if (candidate.isContainer()) {
                stat.st_mode.set(FileStat.S_IFDIR | 0755);
                stat.st_nlink.set(2);
            } else {
                stat.st_mode.set(FileStat.S_IFREG | 0444);
                stat.st_nlink.set(1);
                stat.st_size.set((Number)candidate.getPathInfo(Path.INFO_OCTETSIZE));
            }
            return res;

        } catch (IOException ex) {
            ex.printStackTrace();
            return -ErrorCodes.ENOENT();
        }

    }

    @Override
    public int readdir(String path, Pointer buf, FuseFillDir filter, @off_t long offset, FuseFileInfo fi) {
        final Path candidate = mountedPath.resolve(new Chars(path));

        try {
            if (!candidate.exists()) {
                return -ErrorCodes.ENOENT();
            } else if (!candidate.isContainer()) {
                return -ErrorCodes.ENOTDIR();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return -ErrorCodes.ENOENT();
        }

        filter.apply(buf, ".", null, 0);
        filter.apply(buf, "..", null, 0);
        final Iterator ite = candidate.getChildren().createIterator();
        while (ite.hasNext()) {
            final Path next = (Path) ite.next();
            filter.apply(buf, next.getName().toString(), null, 0);
        }

        return 0;
    }

    @Override
    public int open(String path, FuseFileInfo fi) {
        final Path candidate = mountedPath.resolve(new Chars(path));

        try {
            if (!candidate.exists() || candidate.isContainer()) {
                return -ErrorCodes.ENOENT();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return -ErrorCodes.ENOENT();
        }
        return 0;
    }

    @Override
    public int read(String path, Pointer buf, @size_t long size, @off_t long offset, FuseFileInfo fi) {

        final Path candidate = mountedPath.resolve(new Chars(path));

        try {
            if (!candidate.exists()) {
                return -ErrorCodes.ENOENT();
            } else if (candidate.isContainer()) {
                return -ErrorCodes.EISDIR();
            }

            byte[] buffer = new byte[(int)size];
            
            final DataInputStream ds = new DataInputStream(candidate.createInputStream());
            int nbRead = 0;
            try {
                ds.skipFully(offset);
                while (nbRead < size) {
                    final int r = ds.read(buffer, nbRead, (int)size-nbRead);
                    if(r==-1) break;
                    nbRead+=r;
                }
            }catch(EOSException ex) {
                ex.printStackTrace();
            }
            ds.close();
            
            buf.put(0, buffer, 0, buffer.length);
            return nbRead;
        } catch (IOException ex) {
            ex.printStackTrace();
            return -ErrorCodes.ENOENT();
        }

    }
}
