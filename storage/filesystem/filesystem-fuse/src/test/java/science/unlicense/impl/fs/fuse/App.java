
package science.unlicense.impl.fs.fuse;

import java.net.URISyntaxException;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;
import science.unlicense.system.path.VirtualPath;

/**
 *
 * @author Johann Sorel
 */
public class App {

    public static void main(String[] args) throws URISyntaxException, InterruptedException, IOException {

        final VirtualPath vp = new VirtualPath(new Chars("folder"));
        vp.createContainer();
        final Path file = vp.resolve(new Chars("hello.txt"));
        ByteOutputStream out = file.createOutputStream();
        out.write(new Chars("Hello Fuse!").toBytes());
        out.flush();
        out.close();

        final Path mountPoint = Paths.resolve(new Chars("file:/.../fusepoint2"));
        mountPoint.createContainer();

        final FuseMountPoint mp = new FuseMountPoint(mountPoint,vp);
        mp.enable();

        try{
            Thread.sleep(50000);
        }finally{
            mp.disable();
        }


    }

}
