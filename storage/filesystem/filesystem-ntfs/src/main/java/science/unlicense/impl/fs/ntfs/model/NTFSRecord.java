
package science.unlicense.impl.fs.ntfs.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.impl.fs.ntfs.NTFSConstants;

/**
 *
 * @author Johann Sorel
 */
public class NTFSRecord extends CObject {

    /** 4 bytes : 
     * Magic Number: "FILE"  
     * Can be "BAAD" if record should be ignored
     */
    public Chars magic;
    /** 2 bytes : Offset to the update sequence. */
    public int updateSeqOffset;
    /** 2 bytes : Number of entries in fixup array */
    public int nbArrayEntries;
    /** 8 bytes : $LogFile Sequence Number (LSN) */
    public long logFileSeqNum;
    /** 2 bytes : Sequence number */
    public int seqNum;
    /** 2 bytes : Hard link count */
    public int hardLinkCount;
    /** 2 bytes : Offset to first attribute */
    public int attributeOffset;
    /** 2 bytes : Flags: 0x01: record in use, 0x02 directory. */
    public int flags;
    /** 4 bytes : Used size of MFT entry */
    public int entrySize;
    /** 4 bytes : Allocated size of MFT entry. */
    public int allocatedSize;
    /** 8 bytes : File reference to the base FILE record */
    public long fileRef;
    /** 2 bytes : Next attribute ID */
    public int nextAttributeId;
    /** 2 bytes : Align to 4B boundary (Windows XP) */
    /** 4 bytes : Number of this MFT record (Windows XP) */
    public int recordNum;
    
    public final Sequence attributes = new ArraySequence();

    public void read(DataInputStream ds) throws IOException{
        long baseOffset = ds.getByteOffset();
        
        magic           = new Chars(ds.readFully(new byte[4]));
        if(!magic.equals(NTFSConstants.MTF_SIGNATURE)){
            throw new IOException("Wrong MFT signature : "+magic);
        }
        
        updateSeqOffset = ds.readUShort();
        nbArrayEntries  = ds.readUShort();
        logFileSeqNum   = ds.readLong();
        seqNum          = ds.readUShort();
        hardLinkCount   = ds.readUShort();
        attributeOffset = ds.readUShort();
        flags           = ds.readUShort();
        entrySize       = ds.readInt();
        allocatedSize   = ds.readInt();
        fileRef         = ds.readLong();
        nextAttributeId = ds.readUShort();
        ds.realign(4);
        recordNum       = ds.readInt();      
        
        readAttributes(ds, baseOffset);
    }
    
    private void readAttributes(DataInputStream ds, long baseOffset) throws IOException {
        
        //go to attribute start
        ds.skipFully((baseOffset+attributeOffset)-ds.getByteOffset());
        
        for(;;){
            long pos = ds.getByteOffset();
            final NTFSAttribute att = new NTFSAttribute();
            att.read(ds);
            attributes.add(att);
            
            //move to next attribute
            //TODO : offset is wrong
            ds.skipFully((pos+att.length-16)-ds.getByteOffset());
            if(ds.read()==0xFF){
                //no more attribute
                break;
            }
        }
    }

    public Chars toChars() {
        return Nodes.toChars(magic, attributes);
    }

}
