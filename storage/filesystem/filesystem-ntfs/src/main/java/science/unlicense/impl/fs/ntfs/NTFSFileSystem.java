
package science.unlicense.impl.fs.ntfs;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.store.AbstractStore;
import science.unlicense.impl.fs.ntfs.model.NTFSPartitionBootSector;
import science.unlicense.impl.fs.ntfs.model.NTFSRecord;

/**
 *
 * @author Johann Sorel
 */
public class NTFSFileSystem extends AbstractStore{

    public NTFSPartitionBootSector bootSector;

    public NTFSFileSystem(Object input) {
        super(null, input);
    }

    public void read() throws IOException{
        
        final boolean[] close = new boolean[1];
        final ByteInputStream bs = IOUtilities.toInputStream(getInput(), close);
        try{
            final DataInputStream ds = new DataInputStream(bs, NumberEncoding.LITTLE_ENDIAN);

            //read boot sector
            bootSector = new NTFSPartitionBootSector();
            bootSector.read(ds);
            System.out.println(bootSector);
            
            //read MFT
            final long mftOffset = bootSector.getMFTBteOffset();
            ds.skipFully(mftOffset-ds.getByteOffset());
            
            final NTFSRecord record = new NTFSRecord();
            record.read(ds);
            
            System.out.println(record);
            
            
        }finally{
            bs.close();
        }
        
    }

}
