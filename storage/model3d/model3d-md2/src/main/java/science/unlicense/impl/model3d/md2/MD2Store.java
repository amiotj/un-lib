
package science.unlicense.impl.model3d.md2;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.gpu.opengl.GLC;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.math.Angles;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.impl.model3d.md2.model.MD2Frame;
import science.unlicense.impl.model3d.md2.model.MD2GLCommand;
import science.unlicense.impl.model3d.md2.model.MD2GLCommandVertex;
import science.unlicense.impl.model3d.md2.model.MD2Header;
import science.unlicense.impl.model3d.md2.model.MD2TextureCoordinate;
import science.unlicense.impl.model3d.md2.model.MD2Triangle;
import science.unlicense.impl.model3d.md2.model.MD2Vertex;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * Quake 2 model store.
 *
 * @author Johann Sorel
 */
public class MD2Store extends AbstractModel3DStore{

    private MD2Header header;
    private Chars[] skins;
    private MD2TextureCoordinate[] texCoordinates;
    private MD2Triangle[] triangles;
    private MD2Frame[] frames;
    private MD2GLCommand[] commands;
    //not the real neumber of vertex, this is the number of vertex used by commands
    //wich is superior to the number of vertices since vertices are used more then once.
    //we can't reused triangles because normals changes.
    private int nbVertex;

    public MD2Store(Object input) {
        super(MD2Format.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {
        if(header == null){
            try {
                parseFile();
            } catch (IOException ex) {
                throw new StoreException("Failed to read file : "+ex.getMessage(),ex);
            }
        }

        GLNode root;
        try {
            root = rebuildModel();
            
            root.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Angles.degreeToRadian(-90), new Vector(1,0,0)));
            root.getNodeTransform().notifyChanged();
        } catch (IOException ex) {
            throw new StoreException("Failed to rebuild model : "+ex.getMessage(),ex);
        }

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private GLNode rebuildModel() throws IOException{
        //rebuild model
        final Path file = ((Path)input);
        final Path parent = file.getParent();

        //start by textures, we will need to convert U/V coordinates
        final Texture2D[] textures;
        if(skins.length==0){
            //seems like the skin name is not defined if it has the same name as the model
            textures = new Texture2D[1];
            final Chars textureName = file.getName().replaceAll(new Chars(".md2"), Chars.EMPTY);
            final Image image = findTexture(parent, textureName);
            if(image!=null){
                textures[0] = new Texture2D(image);
            }
        }else{
            textures = new Texture2D[skins.length];
            for(int i=0;i<textures.length;i++){
                Image image = null;
                Chars fileName = skins[i];
                try{
                    image = Images.read(parent.resolve(fileName));
                }catch(IOException ex){
                    //try files with a different extension
                    fileName = fileName.split('.')[0];
                    image = findTexture(parent, fileName);
                }
                if(image!=null){
                    textures[i] = new Texture2D(image);
                }
            }
        }

        //read frames and commands
        //TODO for now create one VBO for first frame
        //TODO rebuild the animation
        final MD2Frame frame = frames[0];
        final FloatCursor varray = DefaultBufferFactory.INSTANCE.createFloat(nbVertex*3).cursorFloat();
        final FloatCursor vnormals = DefaultBufferFactory.INSTANCE.createFloat(nbVertex*3).cursorFloat();
        final FloatCursor vuvs = DefaultBufferFactory.INSTANCE.createFloat(nbVertex*2).cursorFloat();

        int inc = -1;
        final IntSequence idbuffer = new IntSequence();
        IndexRange[] ranges = new IndexRange[header.numGlCommands];

        for(int i=0;i<header.numGlCommands;i++){
            final MD2GLCommand command = commands[i];
            if(command.type == MD2GLCommand.TYPE_END){
                ranges = (IndexRange[]) Arrays.copy(ranges, 0, i-1,new IndexRange[i-1],0);
                break;
            }

            //rebuild range definition
            if(command.type == MD2GLCommand.TYPE_TRIANGLE_STRIP){
                ranges[i] = new IndexRange(GLC.GL_TRIANGLE_STRIP, GLC.GL_UNSIGNED_INT,
                        3, idbuffer.getSize(), command.vertCommand.length);
            }else if(command.type == MD2GLCommand.TYPE_TRIANGLE_FAN){
                ranges[i] = new IndexRange(GLC.GL_TRIANGLE_FAN, GLC.GL_UNSIGNED_INT,
                        3, idbuffer.getSize(), command.vertCommand.length);
            }

            //rebuild vertices, normals and uvs
            for(int k=0;k<command.vertCommand.length;k++){
                final MD2GLCommandVertex cmdVertex = command.vertCommand[k];
                //find vertex in frame
                final MD2Vertex vertex = frame.vertices[cmdVertex.vertexIndex];
                //copy normals data
                vnormals.write(MD2Constants.NORMALS,vertex.lightNormalIndex*3,3);
                //rebuild vertex real position
                final float[] vertice = new float[]{
                    vertex.vertex[0]&0xFF,
                    vertex.vertex[1]&0xFF,
                    vertex.vertex[2]&0xFF};
                Vectors.multiply(vertice, frame.scale, vertice);
                Vectors.add(vertice, frame.translate, vertice);
                varray.write(vertice);
                //UV coords
                vuvs.write(cmdVertex.s);
                vuvs.write(cmdVertex.t);
                idbuffer.put(++inc);
            }

        }

        //rebuild shell
        final VBO vertices = new VBO(varray.getBuffer(), 3);
        final VBO normals = new VBO(vnormals.getBuffer(), 3);
        final IBO index = new IBO(idbuffer.toArrayInt(), 3);
        final Shell shell = new Shell();
        shell.setVertices(vertices);
        shell.setNormals(normals);
        shell.setIndexes(index, ranges);
        shell.setUVs(new VBO(vuvs.getBuffer(), 2));

        final Mesh mesh = new Mesh();
        mesh.setShape(shell);
        
        //rebuild texture paint
        if(textures[0]!=null){
            final UVMapping paint = new UVMapping(textures[0]);
            mesh.getMaterial().putOrReplaceLayer(new Layer(paint));
        }

        mesh.getShape().calculateBBox();
        return mesh;
    }

    private void parseFile() throws IOException{
        final ByteInputStream stream = getSourceAsInputStream();
        final BacktrackInputStream bt = new BacktrackInputStream(stream);
        bt.mark();
        final DataInputStream ds = new DataInputStream(bt, NumberEncoding.LITTLE_ENDIAN);

        final int signature = ds.readInt();
        if(signature != MD2Constants.SIGNATURE){
            throw new IOException("File is not an MD2.");
        }

        //reader header
        header = new MD2Header();
        header.read(ds);

        //read skins
        bt.rewind();
        ds.skipFully(header.offsetSkins);
        skins = new Chars[header.numSkins];
        for(int i=0;i<header.numSkins;i++){
            skins[i] = ds.readBlockZeroTerminatedChars(64, CharEncodings.US_ASCII);
        }

        //read texture coords
        bt.rewind();
        ds.skipFully(header.offsetTexCoords);
        texCoordinates = new MD2TextureCoordinate[header.numTexCoords];
        for(int i=0;i<header.numTexCoords;i++){
            texCoordinates[i] = new MD2TextureCoordinate();
            texCoordinates[i].read(ds);
        }

        //read triangles
        bt.rewind();
        ds.skipFully(header.offsetTriangles);
        triangles = new MD2Triangle[header.numTriangles];
        for(int i=0;i<header.numTriangles;i++){
            triangles[i] = new MD2Triangle();
            triangles[i].read(ds);
        }

        //read frames
        bt.rewind();
        ds.skipFully(header.offsetFrames);
        frames = new MD2Frame[header.numFrames];
        for(int i=0;i<header.numFrames;i++){
            frames[i] = new MD2Frame();
            frames[i].read(ds,header);
        }

        //read commands
        bt.rewind();
        ds.skipFully(header.offsetGlCommands);
        commands = new MD2GLCommand[header.numGlCommands];
        for(int i=0;i<header.numGlCommands;i++){
            commands[i] = new MD2GLCommand();
            int size = ds.readInt();
            if(size==0){
                commands[i].type = MD2GLCommand.TYPE_END;
                break;
            }else if(size > 0){
                commands[i].type = MD2GLCommand.TYPE_TRIANGLE_STRIP;
            }else if(size < 0){
                commands[i].type = MD2GLCommand.TYPE_TRIANGLE_FAN;
                size = -size;
            }
            nbVertex += size;
            commands[i].vertCommand = new MD2GLCommandVertex[size];
            for(int k=0;k<size;k++){
                commands[i].vertCommand[k] = new MD2GLCommandVertex();
                commands[i].vertCommand[k].s = ds.readFloat();
                commands[i].vertCommand[k].t = ds.readFloat();
                commands[i].vertCommand[k].vertexIndex = ds.readInt();
            }
        }

    }

    private Image findTexture(Path parent, Chars baseName){
        Image image = null;
        Iterator ite = parent.getChildren().createIterator();
        while(ite.hasNext()){
            //search for an image file with same name
            Path p = (Path) ite.next();
            if(!p.getName().startsWith(baseName)){
                continue;
            }
            try{
                image = Images.read(p);
                break;
            }catch(IOException ex){
                //do nothing
            }
        }
        return image;
    }
    
}
