
package science.unlicense.impl.model3d.md2;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Quake 2 MD2 format.
 *
 * Documentation from :
 * https://en.wikipedia.org/wiki/MD2_(file_format)
 * http://linux.ucla.edu/~phaethon/q3a/formats/md2-schoenblum.html
 * Thanks to Daniel E. Schoenblum.
 *
 * @author Johann Sorel
 */
public class MD2Format extends AbstractModel3DFormat{

    public static final MD2Format INSTANCE = new MD2Format();

    private MD2Format() {
        super(new Chars("MD2"),
              new Chars("MD2"),
              new Chars("MD2"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("md2")
              },
              new byte[][]{MD2Constants.BSIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new MD2Store(input);
    }

}
