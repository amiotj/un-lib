
package science.unlicense.impl.model3d.md2.model;

/**
 * At offsetGlCommands bytes into the file, there is the gl command list,
 * which is made up of a series of numGlCommands int's and float's,
 * organized into groups. Each group starts with an int. If it is positive,
 * it is followed by that many glCommandVertex_t structures, which form a triangle strip.
 * If it is negative, it is followed by -x glCommandVertex_t structures,
 * which fo rm a triangle fan. A 0 indicates the end of the list.
 * The list is an optimized way of issuing commands when rendering with OpenGl.
 *
 * @author Johann Sorel
 */
public class MD2GLCommandVertex {

    /**
     * These two floats are used to map a vertex onto a skin.
     * The horizontal axis position is given by s,
     * and the vertical axis position is given by t.
     * The range for s and for t is 0.0 to 1.0. Note that the ranges are different
     * than in the textureCoordinate_t structure. They are stored as floats here
     * because that's the way Quake2 passes them to OpenGl.
     */
    public float s;
    public float t;
    /**
     * Index into the array of vertices stored in each frame.
     */
    public int vertexIndex;
}
