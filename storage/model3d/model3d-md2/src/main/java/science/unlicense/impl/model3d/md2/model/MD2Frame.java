
package science.unlicense.impl.model3d.md2.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * Each frame contains the positions in 3D space for each vertex of each triangle
 * that makes up the model. Quake 2 (and Quake) models contain only triangles.
 * 
 * @author Johann Sorel
 */
public class MD2Frame {

    /**
     * Size 3 :
     * This is a scale used by the vertex member of the triangleVertex_t structure.
     */
    public float[] scale;
    /**
     * Size 3 :
     * This is a translation used by the vertex member of the triangleVertex_t structure.
     */
    public float[] translate;
    /**
     * Size 16 bytes :
     * This is a name for the frame.
     */
    public Chars name;
    /**
     * An array of numVertices triangleVertex_t structures.
     */
    public MD2Vertex[] vertices;

    public void read(DataInputStream ds, MD2Header header) throws IOException {
        scale = ds.readFloat(3);
        translate = ds.readFloat(3);
        name = ds.readBlockZeroTerminatedChars(16, CharEncodings.US_ASCII);
        vertices = new MD2Vertex[header.numVertices];
        for(int k=0;k<header.numVertices;k++){
            vertices[k] = new MD2Vertex();
            vertices[k].vertex = ds.readFully(new byte[3]);
            vertices[k].lightNormalIndex = ds.readUByte();
        }
    }
}
