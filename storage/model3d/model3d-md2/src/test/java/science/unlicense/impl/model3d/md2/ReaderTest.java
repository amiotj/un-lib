
package science.unlicense.impl.model3d.md2;

import science.unlicense.impl.model3d.md2.MD2Store;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.model3d.Model3Ds;
import science.unlicense.api.store.StoreException;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ReaderTest {

    /**
     * Reading test
     */
    @Test
    @Ignore
    public void readTest() throws IOException, StoreException{

        //TODO must find a public model to use in tests
        final Path path = Paths.resolve(new Chars("..."));

        final MD2Store store = (MD2Store) Model3Ds.read(path);
        final GLNode node = (GLNode) store.getElements().createIterator().next();

    }

}
