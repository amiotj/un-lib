

package science.unlicense.impl.model3d.md3;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * Useful docs :
 * http://en.wikipedia.org/wiki/MD3_(file_format)
 * http://www.icculus.org/~phaethon/q3/collection.html
 * 
 * @author Johann Sorel
 */
public class MD3Format extends AbstractModel3DFormat{

    public static final MD3Format INSTANCE = new MD3Format();

    private MD3Format() {
        super(new Chars("MD3"),
              new Chars("MD3"),
              new Chars("MD3"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("md3")
              },
              new byte[][]{MD3Constants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new MD3Store(input);
    }

}
