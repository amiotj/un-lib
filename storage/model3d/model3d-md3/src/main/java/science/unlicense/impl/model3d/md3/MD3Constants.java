

package science.unlicense.impl.model3d.md3;

import science.unlicense.api.character.Chars;

/**
 * MD3 constants
 * 
 * @author Johann Sorel
 */
public class MD3Constants {
    
    public static final byte[] SIGNATURE = new byte[]{'I','D','P','3'};
    
    public static final Chars QC_MODEL = new Chars("$model");
    public static final Chars QC_FRAMES = new Chars("$frames");
    public static final Chars QC_FLAGS = new Chars("$flags");
    public static final Chars QC_NUMSKINS = new Chars("$numskins");
    public static final Chars QC_MESH = new Chars("$mesh");
    public static final Chars QC_SKIN = new Chars("$skin");
    
    private MD3Constants(){}
    
}
