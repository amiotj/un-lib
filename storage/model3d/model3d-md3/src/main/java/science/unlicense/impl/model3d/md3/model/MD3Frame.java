

package science.unlicense.impl.model3d.md3.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;

/**
 * Animation frame properties.
 * 
 * @author Johann Sorel
 */
public class MD3Frame {
    
    public Vector bboxMin;
    public Vector bboxMax;
    public Vector origin;
    public float radius;
    public Chars name;
    
    public void read(DataInputStream ds) throws IOException{
        bboxMin = new Vector(ds.readFloat(3));
        bboxMax = new Vector(ds.readFloat(3));
        origin  = new Vector(ds.readFloat(3));
        radius  = ds.readFloat();
        name    = ds.readBlockZeroTerminatedChars(16, CharEncodings.US_ASCII);
    }
    
    
}
