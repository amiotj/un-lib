

package science.unlicense.impl.model3d.md3.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.md3.MD3Constants;

/**
 * Someking of anchor point.
 * 
 * @author Johann Sorel
 */
public class MD3Surface {
    
    /** 0 terminated string, object name */
    public Chars name;
    /** Int32 flags */
    public int flags;
    /** Int32 */
    public int nbFrames;
    /** Int32 maximum of 256 */
    public int nbShaders;
    /** Int32 maximum of 4096 */
    public int nbVertices;
    /** Int32 maximum of 8192 */
    public int nbTriangles;
    /** Int32 */
    public int offsetTriangles;
    /** Int32 */
    public int offsetShaders;
    /** Int32 */
    public int offsetSurfaceStart;
    /** Int32 */
    public int offsetVertices;
    /** Int32 */
    public int offsetEnd;
    
    public MD3Shader[] shaders;
    /** 3 int for a triangle */
    public int[] triangles;
    /** 2 float */
    public float[] sts;
    public MD3Vertex[] vertices;
    
    public void read(BacktrackInputStream bs, DataInputStream ds) throws IOException{
        final int currentOffset = bs.position();
        
        final byte[] sign = ds.readFully(new byte[4]);
        if(!Arrays.equals(sign, MD3Constants.SIGNATURE)){
            throw new IOException("Not an MD3 file");
        }
        name                = ds.readBlockZeroTerminatedChars(64, CharEncodings.US_ASCII);
        flags               = ds.readInt();
        nbFrames            = ds.readInt();
        nbShaders           = ds.readInt();
        nbVertices          = ds.readInt();
        nbTriangles         = ds.readInt();
        offsetTriangles     = ds.readInt();
        offsetShaders       = ds.readInt();
        offsetSurfaceStart  = ds.readInt();
        offsetVertices      = ds.readInt();
        offsetEnd           = ds.readInt();
        
        //read shaders
        bs.rewind();
        ds.skipFully(currentOffset+offsetShaders);
        shaders = new MD3Shader[nbShaders];
        for(int i=0;i<nbShaders;i++){
            shaders[i] = new MD3Shader();
            shaders[i].read(ds);
        }
                
        //read triangles
        bs.rewind();
        ds.skipFully(currentOffset+offsetTriangles);
        triangles = ds.readInt(3*nbTriangles);        
        
        //read textures STS (similar to UV but Y swap)
        bs.rewind();
        ds.skipFully(currentOffset+offsetSurfaceStart);
        sts = ds.readFloat(2*nbVertices); 
                
        //read vertices
        bs.rewind();
        ds.skipFully(currentOffset+offsetVertices);
        vertices = new MD3Vertex[nbVertices*nbFrames];
        for(int i=0;i<vertices.length;i++){
            vertices[i] = new MD3Vertex();
            vertices[i].read(ds);
        }
        
    }
}
