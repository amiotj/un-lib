
package science.unlicense.impl.model3d.md3;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;

/**
 * Read QC shader file.
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class QCReader extends AbstractReader {
    
    
    public Chars readTexture() throws IOException{
        
        final CharInputStream cs = getInputAsCharStream(CharEncodings.US_ASCII);
        
        Chars textureFile = null;
        for(Chars line=cs.readLine();line!=null;line=cs.readLine()){
            if(line.startsWith(MD3Constants.QC_SKIN)){
                textureFile = line.split(' ')[1].trim();
                break;
            }
        }
        
        return textureFile;
    }
    
    
}
