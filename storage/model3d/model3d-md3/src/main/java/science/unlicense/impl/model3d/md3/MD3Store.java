

package science.unlicense.impl.model3d.md3;

import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Angles;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.model3d.md3.model.MD3Frame;
import science.unlicense.impl.model3d.md3.model.MD3Header;
import science.unlicense.impl.model3d.md3.model.MD3Surface;
import science.unlicense.impl.model3d.md3.model.MD3Tag;
import science.unlicense.impl.model3d.md3.model.MD3Vertex;

/**
 *
 * Useful resource :
 * http://projets.developpez.com/projects/castor3d
 * 
 * @author Johann Sorel
 */
public class MD3Store extends AbstractModel3DStore{

    private MD3Header header;
    private MD3Frame[] frames;
    private MD3Tag[] tags;
    private MD3Surface[] surfaces;
    
    public MD3Store(Object input) {
        super(MD3Format.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {
        final Collection col = new ArraySequence();
        
        try {
            read();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }
        
        MultipartMesh mpm = rebuildModel();
        if(mpm!=null){
            col.add(mpm);
        }
        
        return col;
    }
    
    private void read() throws IOException{
        if(header!=null) return;
        
        final ByteInputStream bi = getSourceAsInputStream();
        final BacktrackInputStream bs = new BacktrackInputStream(bi);
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs,NumberEncoding.LITTLE_ENDIAN);
                
        //read header
        header = new MD3Header();
        header.read(ds);
        
        //read frames
        bs.rewind();
        ds.skipFully(header.offsetFrames);
        frames = new MD3Frame[header.nbFrames];
        for(int i=0;i<header.nbFrames;i++){
            frames[i] = new MD3Frame();
            frames[i].read(ds);
        }
        
        //read tags
        bs.rewind();
        ds.skipFully(header.offsetTags);
        tags = new MD3Tag[header.nbTags];
        for(int i=0;i<header.nbTags;i++){
            tags[i] = new MD3Tag();
            tags[i].read(ds);
        }
        
        //read surfaces
        bs.rewind();
        ds.skipFully(header.offsetSurfaces);
        surfaces = new MD3Surface[header.nbSurfaces];
        for(int i=0;i<header.nbSurfaces;i++){
            surfaces[i] = new MD3Surface();
            surfaces[i].read(bs,ds);
        }
        
    }
    
    private MultipartMesh rebuildModel(){
        if(header==null) return null;
        
        final MultipartMesh mpm = new MultipartMesh();
        
        for(int i=0;i<surfaces.length;i++){
            final MD3Surface surface = surfaces[i];
            
            final Mesh mesh = new Mesh();
            mpm.getChildren().add(mesh);
            final Shell shell = new Shell();
            mesh.setShape(shell);
            ((MeshRenderer)mesh.getRenderers().get(0)).getState().setCulling(GLC.CULLING.FRONT);
            
            //TODO thre are multiple frames, we should rebuild then as animation
            //limit to one for now
            final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(surface.nbVertices*3).cursorFloat();
            final FloatCursor normals = DefaultBufferFactory.INSTANCE.createFloat(surface.nbVertices*3).cursorFloat();
            for(int k=0;k<surface.nbVertices;k++){
                final MD3Vertex vertex = surface.vertices[k];
                vertices.write(vertex.coords[0]/64f);
                vertices.write(vertex.coords[1]/64f);
                vertices.write(vertex.coords[2]/64f);
                normals.write(vertex.normalVector());
            }            
            shell.setVertices(new VBO(vertices.getBuffer(), 3));
            shell.setNormals(new VBO(normals.getBuffer(), 3));
            
            //rebuild UVS
            final FloatCursor uvs = DefaultBufferFactory.INSTANCE.createFloat(surface.nbVertices*2).cursorFloat();
            for(int k=0;k<surface.nbVertices;k++){
                uvs.write( surface.sts[k*2+0]);
                uvs.write( surface.sts[k*2+1]);
            }
            shell.setUVs(new VBO(uvs.getBuffer(), 2));
            
            //rebuild indices
            final IBO ibo = new IBO(surface.triangles, 3);
            shell.setIndexes(ibo, IndexRange.TRIANGLES(0, (int) ibo.getPrimitiveBuffer().getPrimitiveCount()));
            
            //rebuild texture
            if(surface.shaders.length>0){
                Chars shaderFile = surface.shaders[0].name;
                final int index = shaderFile.getLastOccurence(new Char('/'));
                if(index>=0){
                    shaderFile = shaderFile.truncate(index, shaderFile.getCharLength());
                }
                
                Chars textureName = shaderFile;
                if(shaderFile.endsWith(new Chars("qc"))){
                    //shader path
                    final Path shaderPath = ((Path)input).getParent().resolve(shaderFile);
                    try{
                        if(shaderPath!=null){
                            final QCReader reader = new QCReader();
                            reader.setInput(shaderPath);
                            textureName = reader.readTexture();
                        }
                    }catch(IOException ex){
                        getLogger().warning(ex);
                    }
                }
                
                //read image
                if(textureName!=null){
                    try{
                        final Path texturePath = ((Path)input).getParent().resolve(textureName);
                        final Image image = Images.read(texturePath);
                        if(image!=null){
                            mesh.getMaterial().putOrReplaceLayer(new Layer(new UVMapping(new Texture2D(image))));
                        }
                    }catch(IOException ex){
                        getLogger().warning(ex);
                    }
                }
            }      
            mesh.getShape().calculateBBox();
            
        }
        
        mpm.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Angles.degreeToRadian(-90), new Vector(1,0,0)));
        mpm.getNodeTransform().notifyChanged();
        
        return mpm;
    }
    
}
