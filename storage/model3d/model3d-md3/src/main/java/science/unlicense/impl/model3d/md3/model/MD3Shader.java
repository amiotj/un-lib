

package science.unlicense.impl.model3d.md3.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * Shader external file information.
 * 
 * @author Johann Sorel
 */
public class MD3Shader {
    
    public Chars name;
    public int index;
    
    public void read(DataInputStream ds) throws IOException{
        name   = ds.readBlockZeroTerminatedChars(64, CharEncodings.US_ASCII);
        index  = ds.readInt();
    }
}
