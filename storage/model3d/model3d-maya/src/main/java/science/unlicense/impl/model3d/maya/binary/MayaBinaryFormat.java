
package science.unlicense.impl.model3d.maya.binary;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class MayaBinaryFormat extends AbstractModel3DFormat{

    public static final MayaBinaryFormat INSTANCE = new MayaBinaryFormat();

    private MayaBinaryFormat() {
        super(new Chars("Maya-Binary"),
              new Chars("Maya-Binary"),
              new Chars("Maya Binary Format"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("mb")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new MayaBinaryStore((Path)input);
    }

}
