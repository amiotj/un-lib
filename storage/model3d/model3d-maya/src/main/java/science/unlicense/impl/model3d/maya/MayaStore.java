
package science.unlicense.impl.model3d.maya;

import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.model3d.Model3DFormat;


/**
 *
 * @author Johann Sorel
 */
public abstract class MayaStore extends AbstractModel3DStore {

    public MayaStore(Model3DFormat format, Object input) {
        super(format, input);
    }



}
