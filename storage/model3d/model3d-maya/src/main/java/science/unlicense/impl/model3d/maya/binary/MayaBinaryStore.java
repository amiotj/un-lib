
package science.unlicense.impl.model3d.maya.binary;

import science.unlicense.api.collection.Collection;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.model3d.maya.MayaStore;
import science.unlicense.api.path.Path;
import science.unlicense.impl.model3d.maya.ascii.MayaAsciiFormat;

/**
 *
 * @author Johann Sorel
 */
public class MayaBinaryStore extends MayaStore {

    public MayaBinaryStore(Path path) {
        super(MayaAsciiFormat.INSTANCE, path);
    }

    @Override
    public Collection getElements() {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
