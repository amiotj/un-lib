
package science.unlicense.impl.model3d.maya.ascii;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.path.Path;

/**
 * 
 * Reference :
 * http://download.autodesk.com/us/maya/2010help/index.html?url=Maya_ASCII_file_format.htm,topicNumber=d0e677706
 * 
 * @author Johann Sorel
 */
public class MayaAsciiFormat extends AbstractModel3DFormat{

    public static final MayaAsciiFormat INSTANCE = new MayaAsciiFormat();

    private MayaAsciiFormat() {
        super(new Chars("Maya-Ascii"),
              new Chars("Maya-Ascii"),
              new Chars("Maya Ascii Format"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ma")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new MayaAsciiStore((Path)input);
    }

}
