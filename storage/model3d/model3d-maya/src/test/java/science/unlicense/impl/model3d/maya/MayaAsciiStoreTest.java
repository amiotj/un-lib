
package science.unlicense.impl.model3d.maya;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.model3d.maya.ascii.MayaAsciiStore;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class MayaAsciiStoreTest {

    @Ignore
    @Test
    public void readCubeTest() throws StoreException{
     
        final Path path = Paths.resolve(new Chars("todo find a file"));
        final MayaAsciiStore store = new MayaAsciiStore(path);
        
        store.getElements();
        
    }
    
}
