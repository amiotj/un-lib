
package science.unlicense.impl.model3d.m3ds.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class M3DSFaces extends M3DSChunk {


    /** faces : a1,b1,c1 ,a2,b2,c2  ... */
    public int[] faces;
    /** flag for each face : f1, f2, f3 */
    public int[] flag;

    public void read(DataInputStream ds) throws IOException {
        size = ds.readInt();

        final int nbFaces = ds.readUShort();
        faces = new int[nbFaces*3];
        flag = new int[nbFaces];
        for(int i=0,k=0;i<nbFaces;i++,k+=3){
            ds.readUShort(faces,k,3);
            ds.readUShort(flag,i,1);
        }

        final int position = 6 + 2 + nbFaces*3*2 + nbFaces*2;
        readSubChunks(position, ds);
    }

}
