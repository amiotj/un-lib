
package science.unlicense.impl.model3d.m3ds.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.predicate.AbstractPredicate;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.m3ds.M3DSConstants;

/**
 * 3DS Chunk.
 *
 * @author Johann Sorel
 */
public class M3DSChunk extends CObject{

    private static final Dictionary CHUNKS = new HashDictionary();
    static {
        CHUNKS.add(M3DSConstants.CHUNK_MAIN,                                        M3DSMain.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR,                                      M3DSEditor.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL,                             M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_AMBIANT,                     M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_BUMP,                        M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_DIFFUSE,                     M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_NAME,                        M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_REFLECTION,                  M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_REFLECTION_FILENAME,         M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_REFLECTION_PARAMETERS,       M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_SPECULAR,                    M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_MATERIAL_TEXTURE,                     M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT,                               M3DSObject.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_CAMERA,                        M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_LIGHT,                         M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_LIGHT_SPOT,                    M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH,                  M3DSTriangularMesh.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES,            M3DSFaces.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES_MATERIAL,   M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES_SMOOTHING,  M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_LOCALCOORDS,      M3DSChunk.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_MAPPINGCOODS,     M3DSMapping.class);
        CHUNKS.add(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_VERTICES,         M3DSVertices.class);

    }

    /** 2 bytes */
    public int id;
    /** 4 bytes */
    public int size;

    /** sub chunks */
    public final Sequence children = new ArraySequence();

    public M3DSChunk getChunk(final int id){
        for(int i=0,n=children.getSize();i<n;i++){
            final M3DSChunk chunk = (M3DSChunk) children.get(i);
            if(chunk.id == id){
                return chunk;
            }
        }
        return null;
    }

    public Sequence getChunks(final int id){
        return Collections.filter(children, new AbstractPredicate() {
            public Boolean evaluate(Object candidate) {
                return ((M3DSChunk)candidate).id == id;
            }
        });
    }

    /**
     * Read the size and skip data + sub chunk.
     * Override to properly parse chunk.
     *
     * @param ds
     * @throws IOException
     */
    public void read(DataInputStream ds) throws IOException{
        size = ds.readInt();
        ds.skipFully(size-6);
    }

    /**
     * Read children chunks.
     * @param position, current position
     * @param ds
     * @throws IOException
     */
    public void readSubChunks(int position, DataInputStream ds) throws IOException{

        while(position < size){
            int chunkId = ds.readUShort();

            Class clazz = (Class) CHUNKS.getValue(chunkId);
            if(clazz==null){
                System.out.println("unknowned tag "+chunkId);
                clazz = M3DSChunk.class;
            }

            M3DSChunk chunk;
            try {
                chunk = (M3DSChunk) clazz.newInstance();
            } catch (InstantiationException ex) {
                throw new IOException(ex);
            } catch (IllegalAccessException ex) {
                throw new IOException(ex);
            }
            chunk.read(ds);
            chunk.id = chunkId;
            position+=chunk.size;
            children.add(chunk);
        }

    }

    public Chars toChars() {
        Chars chars = Nodes.toChars(new Chars(this.getClass().getSimpleName() +" "+Integer.toHexString(id)), children);
        return chars;
    }

}
