
package science.unlicense.impl.model3d.m3ds;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GLC;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.model3d.m3ds.model.M3DSChunk;
import science.unlicense.impl.model3d.m3ds.model.M3DSFaces;
import science.unlicense.impl.model3d.m3ds.model.M3DSMain;
import science.unlicense.impl.model3d.m3ds.model.M3DSMapping;
import science.unlicense.impl.model3d.m3ds.model.M3DSObject;
import science.unlicense.impl.model3d.m3ds.model.M3DSVertices;

/**
 * 3DS storage.
 *
 * @author Johann Sorel
 */
public class M3DSStore extends AbstractModel3DStore{

    public M3DSStore(Object input) {
        super(M3DSFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException{

        M3DSChunk mainchunk = null;
        try {
            final ByteInputStream stream = getSourceAsInputStream();
            final DataInputStream ds = new DataInputStream(stream, NumberEncoding.LITTLE_ENDIAN);

            final int id = ds.readShort();
            if(id == M3DSConstants.CHUNK_MAIN){
                mainchunk = new M3DSMain();
                mainchunk.read(ds);
            }
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        if(mainchunk==null) return null;

        final MultipartMesh root = new MultipartMesh();

        final M3DSChunk editor = mainchunk.getChunk(M3DSConstants.CHUNK_EDITOR);
        if(editor!=null){
            final Sequence objects = editor.getChunks(M3DSConstants.CHUNK_EDITOR_OBJECT);

            for(int i=0,n=objects.getSize();i<n;i++){
                final M3DSObject object = (M3DSObject) objects.get(i);
                final M3DSChunk tm = object.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH);
                final M3DSVertices vertices = (M3DSVertices) tm.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_VERTICES);
                final M3DSFaces faces = (M3DSFaces) tm.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_FACES);
                final M3DSMapping uvs = (M3DSMapping) tm.getChunk(M3DSConstants.CHUNK_EDITOR_OBJECT_TRIANGLEMESH_MAPPINGCOODS);

                final VBO vboVertex = new VBO(vertices.vertices, 3);
                final VBO vboNormal = new VBO(vertices.vertices, 3); //TODO , not right fix this
                final IBO iboFaces = new IBO(faces.faces, 2);

                final Shell shell = new Shell();
                shell.setVertices(vboVertex);
                shell.setNormals(vboNormal);
                shell.setIndexes(iboFaces, new IndexRange(GLC.GL_TRIANGLES, GLC.GL_UNSIGNED_INT, 3, 0, faces.faces.length));

                final Mesh mesh = new Mesh();
                mesh.setName(object.name);
                mesh.setShape(shell);
                mesh.getShape().calculateBBox();
                root.getChildren().add(mesh);
            }

        }

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

}
