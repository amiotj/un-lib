
package science.unlicense.impl.model3d.m3ds;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * 3DS format.
 *
 * @author Johann Sorel
 */
public class M3DSFormat extends AbstractModel3DFormat{

    public static final M3DSFormat INSTANCE = new M3DSFormat();

    private M3DSFormat() {
        super(new Chars("3DS"),
              new Chars("3DS"),
              new Chars("Autodesk 3ds Max"),
              new Chars[]{
                  new Chars("application/x-3ds"),
                  new Chars("image/x-3ds"),
              },
              new Chars[]{
                  new Chars("3ds"),
                  new Chars("max")
              },
              new byte[][]{M3DSConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new M3DSStore(input);
    }

}
