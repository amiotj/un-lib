
package science.unlicense.impl.model3d.m3ds.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class M3DSMain extends M3DSChunk {

    public void read(DataInputStream ds) throws IOException {
        size = ds.readInt();
        readSubChunks(6, ds);
    }

}
