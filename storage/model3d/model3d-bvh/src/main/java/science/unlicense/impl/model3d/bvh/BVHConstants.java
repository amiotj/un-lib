
package science.unlicense.impl.model3d.bvh;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.RegexTokenType;
import science.unlicense.api.lexer.TokenGroup;
import science.unlicense.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public final class BVHConstants {

    public static final Chars KW_HIERARCHY = new Chars("HIERARCHY");
    public static final Chars KW_ROOT = new Chars("ROOT");
    public static final Chars KW_JOINT = new Chars("JOINT");
    public static final Chars KW_CHANNELS = new Chars("CHANNELS");
    public static final Chars KW_OFFSET = new Chars("OFFSET");
    public static final Chars KW_END_SITE = new Chars("End Site");
    public static final Chars KW_MOTION = new Chars("MOTION");
    public static final Chars KW_FRAMES = new Chars("Frames");
    public static final Chars KW_FRAME_TIME = new Chars("Frame Time");

    public static final Chars KW_POSITION_X = new Chars("Xposition");
    public static final Chars KW_POSITION_Y = new Chars("Yposition");
    public static final Chars KW_POSITION_Z = new Chars("Zposition");
    public static final Chars KW_ROTATION_X = new Chars("Xrotation");
    public static final Chars KW_ROTATION_Y = new Chars("Yrotation");
    public static final Chars KW_ROTATION_Z = new Chars("Zrotation");

    //Lexer token types
    static final TokenType TT_HIERARCHY     = RegexTokenType.keyword(KW_HIERARCHY,KW_HIERARCHY);
    static final TokenType TT_ROOT          = RegexTokenType.keyword(KW_ROOT,KW_ROOT);
    static final TokenType TT_JOINT         = RegexTokenType.keyword(KW_JOINT,KW_JOINT);
    static final TokenType TT_CHANNELS      = RegexTokenType.keyword(KW_CHANNELS,KW_CHANNELS);
    static final TokenType TT_OFFSET        = RegexTokenType.keyword(KW_OFFSET,KW_OFFSET);
    static final TokenType TT_END           = RegexTokenType.keyword(new Chars("End"),new Chars("End"));
    static final TokenType TT_SITE          = RegexTokenType.keyword(new Chars("Site"),new Chars("Site"));
    static final TokenType TT_MOTION        = RegexTokenType.keyword(KW_MOTION,KW_MOTION);
    static final TokenType TT_FRAMES        = RegexTokenType.keyword(KW_FRAMES,KW_FRAMES);
    static final TokenType TT_FRAME         = TT_SITE;
    static final TokenType TT_TIME          = RegexTokenType.keyword(new Chars("Time"),new Chars("Time"));
    static final TokenType TT_POSITION_X    = RegexTokenType.keyword(KW_POSITION_X,KW_POSITION_X);
    static final TokenType TT_POSITION_Y    = RegexTokenType.keyword(KW_POSITION_Y,KW_POSITION_Y);
    static final TokenType TT_POSITION_Z    = RegexTokenType.keyword(KW_POSITION_Z,KW_POSITION_Z);
    static final TokenType TT_ROTATION_X    = RegexTokenType.keyword(KW_ROTATION_X,KW_ROTATION_X);
    static final TokenType TT_ROTATION_Y    = RegexTokenType.keyword(KW_ROTATION_Y,KW_ROTATION_Y);
    static final TokenType TT_ROTATION_Z    = RegexTokenType.keyword(KW_ROTATION_Z,KW_ROTATION_Z);

    static final TokenType TT_DOUBLEDOT     = RegexTokenType.keyword(new Chars(":"),new Chars(":"));
    static final TokenType TT_NUMBER        = RegexTokenType.decimal();
    static final TokenType TT_SPACE         = RegexTokenType.space();
    static final TokenType TT_BRACKET_START = RegexTokenType.keyword(new Chars("{"),new Chars("{"));
    static final TokenType TT_BRACKET_END   = RegexTokenType.keyword(new Chars("}"),new Chars("}"));
    static final TokenType TT_WORD          = RegexTokenType.word();

    static void prepareLexer(Lexer lexer){
        final TokenGroup tts = lexer.getTokenGroup();
        tts.add(TT_HIERARCHY);
        tts.add(TT_ROOT);
        tts.add(TT_JOINT);
        tts.add(TT_CHANNELS);
        tts.add(TT_OFFSET);
        tts.add(TT_END);
        tts.add(TT_SITE);
        tts.add(TT_MOTION);
        tts.add(TT_FRAMES);
        //tts.add(TT_FRAME);
        tts.add(TT_TIME);
        tts.add(TT_POSITION_X);
        tts.add(TT_POSITION_Y);
        tts.add(TT_POSITION_Z);
        tts.add(TT_ROTATION_X);
        tts.add(TT_ROTATION_Y);
        tts.add(TT_ROTATION_Z);
        tts.add(TT_DOUBLEDOT);
        tts.add(TT_NUMBER);
        tts.add(TT_SPACE);
        tts.add(TT_BRACKET_START);
        tts.add(TT_BRACKET_END);
        tts.add(TT_WORD);
    }

    private BVHConstants() {}

}
