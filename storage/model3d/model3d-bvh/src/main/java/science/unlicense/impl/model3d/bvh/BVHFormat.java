
package science.unlicense.impl.model3d.bvh;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Biovision hierarchical data format.
 *
 * Useful links :
 * http://research.cs.wisc.edu/graphics/Courses/cs-838-1999/Jeff/BVH.html
 * http://www.character-studio.net/bvh_file_specification.htm
 * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.103.2097&rep=rep1&type=pdf
 * http://www.cs.man.ac.uk/~toby/bvh/
 *
 * And public resources :
 * https://sites.google.com/a/cgspeed.com/cgspeed/motion-capture/cmu-bvh-conversion
 *
 * @author Johann Sorel
 */
public class BVHFormat extends AbstractModel3DFormat{

    public static final BVHFormat INSTANCE = new BVHFormat();

    private BVHFormat() {
        super(new Chars("bvh"),
              new Chars("bvh"),
              new Chars("Biovision hierarchical data"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("bvh")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        final BVHStore store = new BVHStore(input);
        return store;
    }

}
