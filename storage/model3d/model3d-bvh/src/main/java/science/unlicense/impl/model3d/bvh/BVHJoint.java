

package science.unlicense.impl.model3d.bvh;

import science.unlicense.api.character.Chars;
import science.unlicense.api.physic.skeleton.Joint;

/**
 *
 * @author Johann Sorel
 */
public class BVHJoint extends Joint{

    public Chars[] channels;

    public BVHJoint() {
        super(3);
    }



}
