
package science.unlicense.impl.model3d.bvh;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.Token;
import static science.unlicense.impl.model3d.bvh.BVHConstants.*;
import science.unlicense.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public class BVHStore extends AbstractModel3DStore{

    private Skeleton skeleton;

    public BVHStore(Object input) {
        super(BVHFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {
        final Collection col = new ArraySequence();

        try {
            read();
            col.add(skeleton);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return col;
    }

    private void read() throws IOException{

        final Lexer lexer = new Lexer();
        lexer.setInput(input);
        BVHConstants.prepareLexer(lexer);
        lexer.init();

        Token token = lexer.next();
        if(token.type == TT_HIERARCHY){
            readSkeleton(lexer);
        }

        //TODO parse motion

    }

    private void readSkeleton(Lexer lexer) throws IOException{
        expectNext(lexer, TT_ROOT);

        skeleton = new Skeleton();
        final BVHJoint root = new BVHJoint();
        skeleton.getChildren().add(root);

        final Token next = next(lexer);
        if(next.type == TT_WORD){
            //bone name
            root.setName(next.value);
            expectNext(lexer, TT_BRACKET_START);
            readBoneDesc(lexer, root);
        }else if(next.type == TT_BRACKET_START){
            readBoneDesc(lexer, root);
        }else{
            throw new IOException("Unexpected token "+next.type.getName());
        }
    }

    private void readBoneDesc(final Lexer lexer, final BVHJoint bone) throws IOException{
        while(true){
            Token next = next(lexer);

            if(next.type == TT_OFFSET){
                final Token offsetX = expectNext(lexer, TT_NUMBER);
                final Token offsetY = expectNext(lexer, TT_NUMBER);
                final Token offsetZ = expectNext(lexer, TT_NUMBER);
                bone.getNodeTransform().getTranslation().setXYZ(
                        Float64.decode(offsetX.value),
                        Float64.decode(offsetY.value),
                        Float64.decode(offsetZ.value));
                bone.getNodeTransform().notifyChanged();
            }else if(next.type == TT_CHANNELS){
                final Token number = expectNext(lexer, TT_NUMBER);
                bone.channels = new Chars[Int32.decode(number.value)];
                for(int i=0;i<bone.channels.length;i++){
                    final Token channel = next(lexer);
                    if(  channel.type != TT_POSITION_X
                      && channel.type != TT_POSITION_Y
                      && channel.type != TT_POSITION_Z
                      && channel.type != TT_ROTATION_X
                      && channel.type != TT_ROTATION_Y
                      && channel.type != TT_ROTATION_Z){
                        throw new IOException("Unexpected token "+next.type.getName());
                    }
                    bone.channels[i] = channel.value;
                }

            }else if(next.type == TT_JOINT){
                final BVHJoint child = new BVHJoint();
                bone.getChildren().add(child);

                next = next(lexer);
                if(next.type == TT_WORD){
                    //bone name
                    child.setName(next.value);
                    expectNext(lexer, TT_BRACKET_START);
                    readBoneDesc(lexer, child);
                }else if(next.type == TT_BRACKET_START){
                    readBoneDesc(lexer, child);
                }else{
                    throw new IOException("Unexpected token "+next.type.getName());
                }
            }else if(next.type == TT_END){
                expectNext(lexer, TT_SITE);
                expectNext(lexer, TT_BRACKET_START);
                BVHJoint end = new BVHJoint();
                end.setName(new Chars("End Site"));
                bone.getChildren().add(end);
                readBoneDesc(lexer, end);

            }else if(next.type == TT_BRACKET_END){
                //end of bone desc
                return;
            }else{
                throw new IOException("Unexpected token "+next.type.getName());
            }
        }

    }


    private Token expectNext(Lexer lexer, TokenType type) throws IOException{
        Token next = next(lexer);
        if(next.type != type){
            throw new IOException("Was expecting a token of type "+type.getName()+" but was "+next.type.getName());
        }
        return next;
    }

    private Token next(Lexer lexer) throws IOException{
        for(;;){
            Token token = lexer.next();
            if(token == null){
                throw new IOException("Was expecting more tokens");
            }
            if(token.type != TT_SPACE){
                return token;
            }
        }
    }

}
