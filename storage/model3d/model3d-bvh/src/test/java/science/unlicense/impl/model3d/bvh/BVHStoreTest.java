

package science.unlicense.impl.model3d.bvh;

import science.unlicense.impl.model3d.bvh.BVHStore;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.physic.SkeletonAnimation;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class BVHStoreTest {

    @Test
    public void readSkeletonTest() throws StoreException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/bvh/sample.bvh"));

        final BVHStore store = new BVHStore(path);

        final Collection elements = store.getElements();
        // should find a skeleton and skeletion animation
        final Iterator ite = elements.createIterator();
        final Skeleton skeleton = (Skeleton) ite.next();

        final Sequence joints = skeleton.getAllJoints();
        Assert.assertEquals(23,joints.getSize());

        int i=0;

        testEquals((Joint) joints.get(i++), new Chars("Hips"), 0, 0, 0);
        testEquals((Joint) joints.get(i++), new Chars("Chest"), 0, 5.21, 0);
        testEquals((Joint) joints.get(i++), new Chars("Neck"), 0, 18.65, 0);
        testEquals((Joint) joints.get(i++), new Chars("Head"), 0, 5.45, 0);
        testEquals((Joint) joints.get(i++), new Chars("End Site"), 0, 3.87, 0);
        testEquals((Joint) joints.get(i++), new Chars("LeftCollar"), 1.12, 16.23, 1.87);
        testEquals((Joint) joints.get(i++), new Chars("LeftUpArm"), 5.54, 0, 0);
        testEquals((Joint) joints.get(i++), new Chars("LeftLowArm"), 0, -11.96, 0);
        testEquals((Joint) joints.get(i++), new Chars("LeftHand"), 0, -9.93, 0);
        testEquals((Joint) joints.get(i++), new Chars("End Site"), 0, -7.0, 0);
        testEquals((Joint) joints.get(i++), new Chars("RightCollar"), -1.12, 16.23,1.87);
        testEquals((Joint) joints.get(i++), new Chars("RightUpArm"), -6.07, 0, 0);
        testEquals((Joint) joints.get(i++), new Chars("RightLowArm"), 0, -11.82, 0);
        testEquals((Joint) joints.get(i++), new Chars("RightHand"), 0, -10.65, 0);
        testEquals((Joint) joints.get(i++), new Chars("End Site"), 0, -7.0, 0);
        testEquals((Joint) joints.get(i++), new Chars("LeftUpLeg"), 3.91, 0, 0);
        testEquals((Joint) joints.get(i++), new Chars("LeftLowLeg"), 0, -18.34, 0);
        testEquals((Joint) joints.get(i++), new Chars("LeftFoot"), 0, -17.37, 0);
        testEquals((Joint) joints.get(i++), new Chars("End Site"), 0, -3.46, 0);
        testEquals((Joint) joints.get(i++), new Chars("RightUpLeg"), -3.91, 0, 0);
        testEquals((Joint) joints.get(i++), new Chars("RightLowLeg"), 0, -17.63, 0);
        testEquals((Joint) joints.get(i++), new Chars("RightFoot"), 0, -17.14, 0);
        testEquals((Joint) joints.get(i++), new Chars("End Site"), 0, -3.75, 0);

    }

    private static void testEquals(Joint joint, Chars name, double x, double y, double z){
        Assert.assertEquals(name, joint.getName());
        Assert.assertEquals(new Vector(x,y,z), joint.getNodeTransform().getTranslation());
    }

    @Ignore
    @Test
    public void readAnimationTest() throws StoreException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/bvh/sample.bvh"));

        final BVHStore store = new BVHStore(path);

        final Collection elements = store.getElements();
        // should find a skeleton and skeletion animation
        final Iterator ite = elements.createIterator();
        final Skeleton skeleton = (Skeleton) ite.next();
        final SkeletonAnimation animation = (SkeletonAnimation) ite.next();




    }

}
