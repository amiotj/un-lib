
package science.unlicense.impl.model3d.unity;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.number.Int32;
import science.unlicense.api.path.Path;
import science.unlicense.impl.model3d.unity.model.asset.UnityClass;
import science.unlicense.impl.model3d.unity.model.asset.UnityClasses;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;
import static science.unlicense.impl.model3d.unity.model.asset.UnityNodeType.*;
import science.unlicense.system.path.Paths;


/**
 * Generate classes for unity model.
 * 
 * TODO
 *
 * @author Johann Sorel
 */
public class UnityGenerator {
    
    private final Dictionary classVersion = new HashDictionary();
    private final Dictionary classType = new HashDictionary();
    private final Dictionary classCode = new HashDictionary();
    
    private String basePath;
    
    public void execute(Dictionary params){
        System.out.println("Generating unity model classes.");
        
        basePath = (String) params.getValue("modulePath");
        
        final UnityClasses classes = new UnityClasses();
        classes.readFolder(basePath+"/src/main/resources/science/unlicense/impl/model3d/unity");
        final Iterator ite = classes.getClasses().createIterator();
        while(ite.hasNext()){
            final UnityClass unityClass = (UnityClass) ite.next();
            System.out.println("Generate classes for "+unityClass.id+" "+unityClass.name+" "+unityClass.version);
            final UnityNodeType type = unityClass.type;
            final Chars name = unityClass.name;
            generateClass(name, type);
        }
        
//        try {
//            //rewrite classes, unversion might have been fixed
//            classes.writeFolder("/..folderpath");
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        
    }
    
    private static final boolean isValueNode(UnityNodeType type){
        final Chars id = type.type;
                        
        if(TYPE_BOOLEAN.equals(id, true, true) || TYPE_SINT8.equals(id, true, true)
                || TYPE_UINT8.equals(id, true, true) || TYPE_CHAR.equals(id, true, true)
                || TYPE_SINT16.equals(id, true, true) || TYPE_SHORT.equals(id, true, true)
                || TYPE_UINT16.equals(id, true, true) || TYPE_USHORT.equals(id, true, true)
                || TYPE_SINT32.equals(id, true, true) || TYPE_INT.equals(id, true, true)
                || TYPE_UINT32.equals(id, true, true) || TYPE_UINT.equals(id, true, true)
                || TYPE_SINT64.equals(id, true, true) || TYPE_LONG.equals(id, true, true)
                || TYPE_UINT64.equals(id, true, true) || TYPE_ULONG.equals(id, true, true)
                || TYPE_FLOAT.equals(id, true, true)
                || TYPE_DOUBLE.equals(id, true, true)){
            return true;
        }        
        
        for(int i=0;i<PRIMITIVE_TYPES.length;i++){
            if(PRIMITIVE_TYPES[i].match(id)){
                return true;
            }
        }
        
        return false;
    }
    
    private Chars generateClass(UnityNodeType type){
        final Chars id = type.type;
                        
        if(TYPE_BOOLEAN.equals(id, true, true)){
            return new Chars("Boolean");
        }else if(TYPE_SINT8.equals(id, true, true)){
            return new Chars("Byte");
        }else if(TYPE_UINT8.equals(id, true, true) || TYPE_CHAR.equals(id, true, true)){
            return new Chars("Integer");
        }else if(TYPE_SINT16.equals(id, true, true) || TYPE_SHORT.equals(id, true, true)){
            return new Chars("Short");
        }else if(TYPE_UINT16.equals(id, true, true) || TYPE_USHORT.equals(id, true, true)){
            return new Chars("Integer");
        }else if(TYPE_SINT32.equals(id, true, true) || TYPE_INT.equals(id, true, true)){
            return new Chars("Integer");
        }else if(TYPE_UINT32.equals(id, true, true) || TYPE_UINT.equals(id, true, true)){
            return new Chars("Long");
        }else if(TYPE_SINT64.equals(id, true, true) || TYPE_LONG.equals(id, true, true)){
            return new Chars("Long");
        }else if(TYPE_UINT64.equals(id, true, true) || TYPE_ULONG.equals(id, true, true)){
            //TODO not exact
            return new Chars("Long");
        }else if(TYPE_FLOAT.equals(id, true, true)){
            return new Chars("Float");
        }else if(TYPE_DOUBLE.equals(id, true, true)){
            return new Chars("Double");
        }        
        
        for(int i=0;i<PRIMITIVE_TYPES.length;i++){
            if(PRIMITIVE_TYPES[i].match(id)){
                final String name = PRIMITIVE_TYPES[i].valueClass().getSimpleName();
                generateClassWrapped(type);
                return new Chars(name);
            }
        }
        
        final Chars name = generateClass(id,type);
        return name;
    }
    
    private void generateClassWrapped(final UnityNodeType type){
        //accesor for each attribute
        final NodeCardinality[] childrenTypes = type.getChildrenTypes();
        for(int i=0;i<childrenTypes.length;i++){            
            final UnityNodeType childType = (UnityNodeType) childrenTypes[i].getType();
            generateClass(childType);
        }
    }
    
    private Chars generateClass(final Chars className, final UnityNodeType type){
                
        Integer version = (Integer) classVersion.getValue(className);
        if(version==null){
            //first time we see this class, generate an interface
            final Chars java = generateInterface(className);
            try{
                final Path p = Paths.resolve(new Chars("file:"+basePath+"/target/generated-sources/gen/science/unlicense/impl/model3d/unity/model/primitive/"+className+".java"));
                new DataOutputStream(p.createOutputStream()).write(java.toBytes());
            }catch(IOException ex){
                ex.printStackTrace();
            }

            version = 1;
        }
        type.unversion = version;
            
        final Chars previousCode = (Chars) classCode.getValue(className);
        Chars versionedName;
        if(previousCode!=null){
            
            //we check equality on the generated code, since primitive types like color
            //may vary but won't change the class in the end
            versionedName = className.concat(Int32.encode(version));
            final Chars java = generateCode(className, versionedName, type);
            
            if(previousCode.equals(java)){
                return versionedName;
            }else{
                version++;
                versionedName = className.concat(Int32.encode(version));
            }
        }else{
            versionedName = className.concat(Int32.encode(version));
        }
        type.unversion = version;
        
        System.out.println("Generate class "+className);
        final Chars java = generateCode(className, versionedName, type);
        
        try{
            final Path p = Paths.resolve(new Chars("file:"+basePath+"/target/generated-sources/gen/science/unlicense/impl/model3d/unity/model/primitive/"+versionedName+".java"));
            new DataOutputStream(p.createOutputStream()).write(java.toBytes());
        }catch(IOException ex){
            ex.printStackTrace();
        }
        
        classCode.add(className,java);
        classType.add(className,type);
        classVersion.add(className,version);
        
        return versionedName;
    }

    private Chars generateInterface(Chars className){

        final CharBuffer fileCb = new CharBuffer();
        fileCb.append("package science.unlicense.impl.model3d.unity.model.primitive;\n");
        fileCb.append("\n");
        fileCb.append("import science.unlicense.api.model.tree.TypedNode;\n");
        fileCb.append("\n");
        fileCb.append("public interface "+className+" extends TypedNode {\n");
        fileCb.append("}");
        return fileCb.toChars();
    }

    private Chars generateCode(Chars className, Chars versionedName, UnityNodeType type){
        
        final CharBuffer fileCb = new CharBuffer();
        final CharBuffer attCb = new CharBuffer();
        final CharBuffer methodCb = new CharBuffer();
        fileCb.append("package science.unlicense.impl.model3d.unity.model.primitive;\n");
        fileCb.append("\n");
        fileCb.append("import science.unlicense.api.color.Color;\n");
        fileCb.append("import science.unlicense.api.character.Chars;\n");
        fileCb.append("import science.unlicense.api.collection.*;\n");
        fileCb.append("import science.unlicense.api.collection.primitive.*;\n");
        fileCb.append("import science.unlicense.api.model.tree.DefaultTypedNode;\n");
        fileCb.append("import science.unlicense.api.geometry.BBox;\n");
        fileCb.append("import science.unlicense.impl.geometry.s2d.Rectangle;\n");
        fileCb.append("import science.unlicense.api.math.*;\n");
        fileCb.append("import science.unlicense.impl.math.*;\n");
        fileCb.append("import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;\n");
        fileCb.append("import science.unlicense.impl.model3d.unity.model.type.UnityPointer;\n");
        fileCb.append("\n");
        fileCb.append("public class "+versionedName+" extends DefaultTypedNode implements "+className+"{\n");

        //accesor for each attribute
        final NodeCardinality[] childrenTypes = type.getChildrenTypes();
        for(int i=0;i<childrenTypes.length;i++){
            final Chars id = childrenTypes[i].getId();
            Chars sid = id.replaceAll(' ', '_');
            sid = sid.replaceAll('[', '_');
            sid = sid.replaceAll(']', '_');
            sid = sid.replaceAll(':', '_');
            sid = sid.replaceAll('?', '_');
            sid = sid.replaceAll('-', '_');
            sid = sid.replaceAll('&', '_');
            sid = sid.replaceAll('(', '_');
            sid = sid.replaceAll(')', '_');
            
            //reserved names
            if(sid.equals(new Chars("Type"))){
                sid = new Chars("TType");
            }
            
            final UnityNodeType childType = (UnityNodeType) childrenTypes[i].getType();
            final Chars varName = new Chars("ATT_"+sid.toUpperCase());
            
            //static property name
            fileCb.append("    public static final Chars "+varName+" = new Chars(\""+id+"\");\n");
            
            //method
            final Chars childTypeName = generateClass(childType);
            
            methodCb.append("    public "+childTypeName+" get"+sid+"(){\n");
            if(isValueNode(childType)){
                methodCb.append("        return ("+childTypeName+") getChild("+varName+").getValue();\n");
            }else{
                methodCb.append("        return ("+childTypeName+") getChild("+varName+");\n");
            }
            methodCb.append("    }\n\n");

        }

        fileCb.append(attCb.toChars());
        
        fileCb.append(
            "\n    public "+versionedName+"(UnityNodeType type) {\n" +
            "        super(type);\n" +
            "    }\n\n");
        fileCb.append(methodCb.toChars());
        fileCb.append("\n}");

        return fileCb.toChars();
    }
    
}
