
package science.unlicense.impl.model3d.unity.model.bundle;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;
import science.unlicense.api.archive.AbstractArchiveFormat;
import science.unlicense.api.archive.Archive;
import science.unlicense.impl.model3d.unity.UnityConstants;

/**
 *
 * @author Johann Sorel
 */
public class UnityBundleFormat extends AbstractArchiveFormat {

    public static final UnityBundleFormat INSTANCE = new UnityBundleFormat();
    
    private UnityBundleFormat() {
        super(new Chars("unitybundle"),
              new Chars("Unity-Bundle"),
              new Chars("Unity-Bundle"),
              new Chars[]{
                  new Chars("application/x-unity-bundle")
              },
              new Chars[]{
                  new Chars("unity3d")
              },
              new byte[][]{
                  UnityConstants.SIGNATURE_RAW,
                  UnityConstants.SIGNATURE_WEB});
    }

    public boolean canOpen(Path candidate) {
        return candidate.getName().toLowerCase().endsWith(new Chars(".unity3d"));
    }

    public Archive open(Path candidate, Document params) {
        return new UnityBundle(candidate);
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean canCreate(Path base) throws IOException {
        return false;
    }

    public PathResolver createResolver(Path base) throws IOException {
        return open(base,null);
    }

}
