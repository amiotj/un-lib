
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class MapType implements UnityValueType {

    private static final Chars NAME = new Chars("map");
    private static final Chars ATT_Array = new Chars("Array");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Dictionary.class;
    }
    
    public Object toValue(TypedNode node) {
        final Object[] pairs = (Object[]) node.getChild(ATT_Array).getValue();
        final Dictionary dico = new HashDictionary();
        for(int i=0;i<pairs.length;i++){
            final Pair p = (Pair) ((TypedNode)pairs[i]).getValue();
            dico.add(p.getValue1(), p.getValue2());
        }
        return dico;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
