
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.api.CObject;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.io.ClipInputStream;

/**
 *
 * @author Johann Sorel
 */
public class UnityAssetObject extends CObject{

    public final UnityAsset asset;
    public final UnityObjectReference ref;
    public byte[] objectBytes;

    public UnityAssetObject(UnityAsset asset, UnityObjectReference reference) {
        this.asset = asset;
        this.ref = reference;
    }
    
    public UnityNodeType getUnityNodeType(){
        UnityNodeType type = (UnityNodeType) asset.types.getValue(ref.typeID);
        if(type==null){
            //get from class list
            final UnityClass clazz = asset.getClasses().getClass(ref.classID, asset.unityVersion,false);
            if(clazz!=null){
                type = clazz.type;
                //make type name match class name
                type.type = clazz.name;
            }
        }
        return type;
    }
    
    /**
     * Load node bytes in memory.
     * 
     * @throws IOException 
     */
    public void load() throws IOException{
        final ByteInputStream is = asset.input.createInputStream();
        final DataInputStream ds = new DataInputStream(is, NumberEncoding.LITTLE_ENDIAN);
        ds.skipFully(asset.header.dataOffset + ref.offset);
        objectBytes = ds.readFully(new byte[(int)ref.length]);
    }
    
    public ByteInputStream getAsInputStream() throws IOException{
        final ByteInputStream is = asset.input.createInputStream();
        final DataInputStream ds = new DataInputStream(is, NumberEncoding.LITTLE_ENDIAN);
        ds.skipFully(asset.header.dataOffset + ref.offset);
        return new ClipInputStream(is, (int)ref.length);
    }
    
    public void setBytes(byte[] datas) throws IOException{
        final SeekableByteBuffer sb = asset.input.createSeekableBuffer(true, true, true);
        sb.skip(asset.header.dataOffset + ref.offset);
        sb.remove((int)ref.length);
        sb.insert(datas);
        ref.length = datas.length;
    }
    
    public TypedNode decode(boolean simplify) throws IOException{
        
        final ByteInputStream is = getAsInputStream();
        final DataInputStream ds = new DataInputStream(is, NumberEncoding.LITTLE_ENDIAN);

        //find object type
        final UnityNodeType type = getUnityNodeType();
        if(type==null){
            return null;
        }
        
        return type.readInstance(this,ds,simplify);
    }
        
    public void updateByte(TypedNode node) throws IOException{
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out, NumberEncoding.LITTLE_ENDIAN);
        ((UnityNodeType)node.getType()).writeInstance(ds, this, (TypedNode) node);
        objectBytes = out.getBuffer().toArrayByte();
    }
    
}
