
package science.unlicense.impl.model3d.unity.model.bundle;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;
import science.unlicense.api.archive.Archive;
import science.unlicense.impl.model3d.unity.UnityConstants;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityBundle extends AbstractPath implements Archive {

    private final Object input;
    private Chars signature;
    private UnityBundleHeader header;
    private UnityBundleEntry[] index;

    public UnityBundle(Object input) {
        this.input = input;
    }

    public Object getInput() {
        return input;
    }

    private synchronized void read() throws IOException{
        if(header!=null) return;
        final boolean[] close = new boolean[1];
        final ByteInputStream in = IOUtilities.toInputStream(input, close);
        final DataInputStream ds = new DataInputStream(in);
        
        //check signature
        signature = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        if(!Arrays.equals(UnityConstants.SIGNATURE_RAW,signature.toBytes())){
            throw new IOException("Input is not a Unity3D file.");
        }
        
        //read header
        header = new UnityBundleHeader();
        header.read(ds);
        
        //read entry index
        final int nbEntry = ds.readInt();
        index = new UnityBundleEntry[nbEntry];
        for(int i=0;i<nbEntry;i++){
            index[i] = new UnityBundleEntry(this);
            index[i].read(ds);
        }
        
        if(close[0]){
            in.close();
        }
    }
    
    public UnityBundleHeader getHeader() {
        return header;
    }

    @Override
    public Collection getChildren() {
        try {
            read();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return Collections.staticCollection(index);
    }

    @Override
    public Chars getName() {
        return new Chars("bundle");
    }

    @Override
    public Path getParent() {
        return ((Path)input).getParent();
    }

    @Override
    public boolean isContainer() throws IOException {
        return true;
    }

    public boolean exists() throws IOException {
        return true;
    }
    
    @Override
    public boolean createContainer() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Path resolve(Chars address) {
        if(address==null) return this;

        if(address.startsWith('/')){
            address = address.truncate(1,-1);
        }

        final Chars searchName = new Chars(address);
        for(int i=0;i<index.length;i++){
            if(index[i].name.equals(searchName)){
                return index[i];
            }
        }
        
        return null;
    }

    @Override
    public PathResolver getResolver() {
        return this;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        if(input instanceof Path){
            return ((Path)input).createInputStream();
        }else{
            throw new IOException("Insupported input");
        }
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        if(input instanceof Path){
            return ((Path)input).createOutputStream();
        }else{
            throw new IOException("Insupported input");
        }
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        if(input instanceof Path){
            return ((Path)input).createSeekableBuffer(read, write, resize);
        }else{
            throw new IOException("Insupported input");
        }
    }
    
    @Override
    public Chars toURI() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public PathFormat getFormat() {
        return UnityBundleFormat.INSTANCE;
    }
    
}
