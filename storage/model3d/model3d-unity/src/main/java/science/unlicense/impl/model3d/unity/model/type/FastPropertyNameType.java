
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class FastPropertyNameType implements UnityValueType {

    private static final Chars NAME = new Chars("FastPropertyName");
    private static final Chars ATT_name = new Chars("name");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Chars.class;
    }
    
    public Object toValue(TypedNode node) {
        return node.getChild(ATT_name).getValue();
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
