
package science.unlicense.impl.model3d.unity.adaptor;

import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.anim.KeyFrameNumberTimeSerie;
import science.unlicense.impl.anim.KeyFrameQuaternionTimeSerie;
import science.unlicense.impl.anim.KeyFrameTupleTimeSerie;
import science.unlicense.impl.anim.NumberKeyFrame;
import science.unlicense.impl.anim.NumberTimeSerie;
import science.unlicense.impl.anim.TupleKeyFrame;
import science.unlicense.impl.anim.TupleTimeSerie;
import science.unlicense.impl.model3d.unity.model.primitive.*;

/**
 *
 * @author Johann Sorel
 */
public class AnimationAdaptor {
    
    public static KeyFrameQuaternionTimeSerie read(QuaternionCurve1 curve){
        final KeyFrameQuaternionTimeSerie serie = new KeyFrameQuaternionTimeSerie();
        
        final AnimationCurve1 anim = curve.getcurve();
        final Sequence frames = anim.getm_Curve();
        for(int i=0,n=frames.getSize();i<n;i++){
            final QuaternionKeyframe1 kf = (QuaternionKeyframe1) frames.get(i);
            //TODO handle in/out slopes
            final TupleKeyFrame tkf = new TupleKeyFrame(kf.gettime(), kf.getvalue());
            serie.getFrames().add(tkf);
        }
        
        return serie;
    }
    
    public static TupleTimeSerie read(Vector3Curve1 curve){
        final KeyFrameTupleTimeSerie serie = new KeyFrameTupleTimeSerie();
        
        final AnimationCurve1 anim = curve.getcurve();
        final Sequence frames = anim.getm_Curve();
        for(int i=0,n=frames.getSize();i<n;i++){
            final VectorKeyframe1 kf = (VectorKeyframe1) frames.get(i);
            //TODO handle in/out slopes
            final TupleKeyFrame tkf = new TupleKeyFrame(kf.gettime(), kf.getvalue());
            serie.getFrames().add(tkf);
        }
        
        return serie;
    }
    
    public static NumberTimeSerie read(FloatCurve1 curve){
        final KeyFrameNumberTimeSerie serie = new KeyFrameNumberTimeSerie();
        
        final AnimationCurve1 anim = curve.getcurve();
        final Sequence frames = anim.getm_Curve();
        for(int i=0,n=frames.getSize();i<n;i++){
            final Keyframe1 kf = (Keyframe1) frames.get(i);
            //TODO handle in/out slopes
            final NumberKeyFrame tkf = new NumberKeyFrame(kf.gettime(), kf.getvalue());
            serie.getFrames().add(tkf);
        }
        
        return serie;
    }
    
    
}
