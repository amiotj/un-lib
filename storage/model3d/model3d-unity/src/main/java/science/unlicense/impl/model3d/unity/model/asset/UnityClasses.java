
package science.unlicense.impl.model3d.unity.model.asset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.path.Path;
import science.unlicense.impl.archive.xz.LZMA2Options;
import science.unlicense.impl.archive.xz.XZInputStream;
import science.unlicense.impl.archive.xz.XZOutputStream;
import science.unlicense.impl.model3d.unity.model.UnityVersion;
import science.unlicense.impl.model3d.unity.model.ioclass.JSONClassReader;
import science.unlicense.impl.model3d.unity.model.ioclass.JSONClassWriter;
import science.unlicense.impl.system.jvm.JVMInputStream;
import science.unlicense.impl.system.jvm.JVMOutputStream;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class UnityClasses {

    private final Sequence classes = new ArraySequence();

    public UnityClasses() {
    }

    public Sequence getClasses() {
        return classes;
    }
    
    public void readFolder(String path){
        JSONClassReader reader = new JSONClassReader();
        for(int i=0;i<2000;i++){
            try{
                //TODO convert XZ Archive to science.unlicense.api.io
                final String filePath = path+"/"+i+".json.xz";
                InputStream is = UnityClasses.class.getResourceAsStream(filePath);
                if(is==null){
                    File file = new File(filePath);
                    if(file.exists()){
                        is = new FileInputStream(file);
                    }
                }
                if(is==null) continue;
                final XZInputStream s = new XZInputStream(is);
                reader.setInput(new JVMInputStream(s));
                reader.fill(this);
            }catch(Exception ex){
                throw new RuntimeException(ex);
            }
        }
    }
    
    public void writeFolder(String path) throws FileNotFoundException, IOException, science.unlicense.api.io.IOException{
        //group classes by number
        final Dictionary sorted = new HashDictionary();        
        for(int i=0,n=classes.getSize();i<n;i++){
            final UnityClass clazz = (UnityClass) classes.get(i);
            UnityClasses set = (UnityClasses) sorted.getValue(clazz.id);
            if(set==null){
                set = new UnityClasses();
                sorted.add(clazz.id, set);
            }
            set.add(clazz);
        }
        
        //sort by version number and write
        final Iterator ite = sorted.getValues().createIterator();
        while(ite.hasNext()){
            final UnityClasses set = (UnityClasses) ite.next();
            Collections.sort(set.classes);
            
            final int id = ((UnityClass)set.classes.get(0)).id;
            final Path jsonPath = Paths.resolve(new Chars("file:"+path+"/"+id+".json"));
            final Path xzPath = Paths.resolve(new Chars("file:"+path+"/"+id+".json.xz"));
            
            final JSONClassWriter writer = new JSONClassWriter();
            writer.setOutput(jsonPath);
            writer.write(set);
            writer.dispose();
                        
            //compress it
            final OutputStream bout = new FileOutputStream(xzPath.toURI().replaceAll(new Chars("file:"), Chars.EMPTY).toString());
            final XZOutputStream out = new XZOutputStream(bout, new LZMA2Options());
            final JVMOutputStream jout = new JVMOutputStream(out);
            IOUtilities.copy(jsonPath.createInputStream(), jout);
            jout.flush();
            jout.close();
            
            jsonPath.delete();
        }
        
    }
    
    /**
     * 
     * @param id
     * @param version
     * @param strict allow to return the nearest older version of the type if any
     * @return 
     */
    public UnityClass getClass(int id, UnityVersion version, boolean strict){
        final Iterator ite = classes.createIterator();
        UnityClass clazz = null;
        while(ite.hasNext()){
            final UnityClass cdt = (UnityClass) ite.next();
            if(cdt.id==id){
                final int order = cdt.version.order(version);
                if(order==0) return cdt;
                if(!strict && order<0){
                    //keep the closes version
                    if(clazz==null || cdt.version.order(clazz.version)>0) clazz = cdt;
                }
            }
        }
        
        return strict ? null : clazz;
    }

    public void add(UnityClass clazz) {
        classes.add(clazz);
    }
        
    private static UnityClasses CLASSES;
    public static synchronized UnityClasses getDefaultClasses() {
        if(CLASSES==null){
            CLASSES = new UnityClasses();
            CLASSES.readFolder("/science/unlicense/impl/model3d/unity");
        }
        return CLASSES;
    }
    
}
