
package science.unlicense.impl.model3d.unity.model.asset;

import java.lang.reflect.Array;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.DefaultNodeCardinality;
import science.unlicense.api.model.tree.DefaultNodeType;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.primitive.UnityNodes;
import science.unlicense.impl.model3d.unity.model.type.ArrayType;
import science.unlicense.impl.model3d.unity.model.type.ColorRGBAType;
import science.unlicense.impl.model3d.unity.model.type.FastPropertyNameType;
import science.unlicense.impl.model3d.unity.model.type.Matrix3x4fType;
import science.unlicense.impl.model3d.unity.model.type.Matrix4x4fType;
import science.unlicense.impl.model3d.unity.model.type.PPtrType;
import science.unlicense.impl.model3d.unity.model.type.StringType;
import science.unlicense.impl.model3d.unity.model.type.TypelessDataType;
import science.unlicense.impl.model3d.unity.model.type.UnityValueType;
import science.unlicense.impl.model3d.unity.model.type.Vector2fType;
import science.unlicense.impl.model3d.unity.model.type.Vector3fType;
import science.unlicense.impl.model3d.unity.model.type.VectorType;
import science.unlicense.impl.model3d.unity.model.type.LocalAABBType;
import science.unlicense.impl.model3d.unity.model.type.MapType;
import science.unlicense.impl.model3d.unity.model.type.OffsetPPtrType;
import science.unlicense.impl.model3d.unity.model.type.PairType;
import science.unlicense.impl.model3d.unity.model.type.SetType;
import science.unlicense.impl.model3d.unity.model.type.StaticVectorType;
import science.unlicense.impl.model3d.unity.model.type.UnityPointer;
import science.unlicense.impl.model3d.unity.model.type.Vector4fType;
import science.unlicense.impl.model3d.unity.model.type.Float4Type;
import science.unlicense.impl.model3d.unity.model.type.QuaternionType;
import science.unlicense.impl.model3d.unity.model.type.RectfType;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityNodeType extends DefaultNodeType{
    
    public static final Chars TYPE_BOOLEAN = new Chars("bool");
    public static final Chars TYPE_SINT8 = new Chars("SInt8");
    public static final Chars TYPE_UINT8 = new Chars("UInt8");
    public static final Chars TYPE_CHAR = new Chars("char");
    public static final Chars TYPE_SINT16 = new Chars("SInt16");
    public static final Chars TYPE_SHORT = new Chars("short");
    public static final Chars TYPE_UINT16 = new Chars("UInt16");
    public static final Chars TYPE_USHORT = new Chars("unsigned short");
    public static final Chars TYPE_SINT32 = new Chars("SInt32");
    public static final Chars TYPE_INT = new Chars("int");
    public static final Chars TYPE_UINT32 = new Chars("UInt32");
    public static final Chars TYPE_UINT = new Chars("unsigned int");
    public static final Chars TYPE_SINT64 = new Chars("SInt64");
    public static final Chars TYPE_LONG = new Chars("long");
    public static final Chars TYPE_UINT64 = new Chars("UInt64");
    public static final Chars TYPE_ULONG = new Chars("unsigned long");
    public static final Chars TYPE_FLOAT = new Chars("float");
    public static final Chars TYPE_DOUBLE = new Chars("double");
    private static final Set ATOM_TYPES = new HashSet();
    static {
        ATOM_TYPES.add(TYPE_BOOLEAN);
        ATOM_TYPES.add(TYPE_SINT8);
        ATOM_TYPES.add(TYPE_UINT8);
        ATOM_TYPES.add(TYPE_CHAR);
        ATOM_TYPES.add(TYPE_SINT16);
        ATOM_TYPES.add(TYPE_SHORT);
        ATOM_TYPES.add(TYPE_UINT16);
        ATOM_TYPES.add(TYPE_USHORT);
        ATOM_TYPES.add(TYPE_SINT32);
        ATOM_TYPES.add(TYPE_INT);
        ATOM_TYPES.add(TYPE_UINT32);
        ATOM_TYPES.add(TYPE_UINT);
        ATOM_TYPES.add(TYPE_SINT64);
        ATOM_TYPES.add(TYPE_LONG);
        ATOM_TYPES.add(TYPE_UINT64);
        ATOM_TYPES.add(TYPE_ULONG);
        ATOM_TYPES.add(TYPE_FLOAT);
        ATOM_TYPES.add(TYPE_DOUBLE);
    }
        
    public static final int BYTE_ALIGN = 0x4000;
    
    public static final UnityValueType[] PRIMITIVE_TYPES = new UnityValueType[]{
        new ArrayType(),
        new ColorRGBAType(),
        new FastPropertyNameType(),
        new LocalAABBType(),
        new Matrix3x4fType(),
        new Matrix4x4fType(),
        new OffsetPPtrType(),
        new PPtrType(),
        new PairType(),
        new StringType(),
        new TypelessDataType(),
        new Vector2fType(),
        new Vector3fType(),
        new Vector4fType(),
        new Float4Type(),
        new QuaternionType(),
        new RectfType(),
        //collection types
        new MapType(),
        new SetType(),
        new StaticVectorType(),
        new VectorType(),
    };
    
    public Chars type;
    public int size;
    public int index;
    public boolean isArray;
    public int version;
    public int metaFlag;
    
    // Unity 5+
    // the level of this type within the type tree
    private int treeLevel;    
    // offset to the type string
    private int typeOffset;    
    // offset to the name string
    private int nameOffset;
    
    //INTERNAL : used to map correct class version
    public int unversion;
    
    public boolean isByteAlign() {
        return (metaFlag & BYTE_ALIGN) != 0;
    }
    
    public void read(UnityAsset asset, DataInputStream ds) throws IOException{
        if (asset.header.version > 13) {
            version     = ds.readShort();
            treeLevel   = ds.readUByte();
            isArray     = ds.readInt()==1;
            typeOffset  = ds.readInt();
            nameOffset  = ds.readInt();
            size        = ds.readInt();
            index       = ds.readInt();
            metaFlag    = ds.readInt();
        } else {
            type        = ds.readZeroTerminatedChars(256,CharEncodings.US_ASCII);
            setId(ds.readZeroTerminatedChars(256,CharEncodings.US_ASCII));
            setTitle(getId());
            size        = ds.readInt();
            index       = ds.readInt();
            isArray     = ds.readInt()==1;
            version     = ds.readInt();
            metaFlag    = ds.readInt();
        }
        
        //read childrens
        final NodeCardinality[] children = new NodeCardinality[ds.readInt()];
        for(int i=0;i<children.length;i++){
            final UnityNodeType type = new UnityNodeType();
            type.read(asset,ds);
            children[i] = new DefaultNodeCardinality(type, 1, 1);
        }
        
        setChildrenTypes(children);
    }
    
    public void write(UnityAsset asset, DataOutputStream ds) throws IOException {
        if (asset.header.version > 13) {
            ds.writeShort((short)version);
            ds.writeUByte(treeLevel);
            ds.writeInt(isArray?0:1);
            ds.writeInt(typeOffset);
            ds.writeInt(nameOffset);
            ds.writeInt(size);
            ds.writeInt(index);
            ds.writeInt(metaFlag);
        } else {
            ds.writeZeroTerminatedChars(type, 256, CharEncodings.US_ASCII);
            ds.writeZeroTerminatedChars(getId(), 256, CharEncodings.US_ASCII);
            ds.writeInt(size);
            ds.writeInt(index);
            ds.writeInt(isArray?0:1);
            ds.writeInt(version);
            ds.writeInt(metaFlag);
        }

        //write childrens
        final NodeCardinality[] children = getChildrenTypes();
        ds.writeInt(children.length);
        for(int i=0;i<children.length;i++){
            final UnityNodeType type = (UnityNodeType) children[i].getType();
            type.write(asset,ds);
        }
        
    }
    
    public TypedNode readInstance(UnityAssetObject obj, DataInputStream ds, boolean simplify) throws IOException{
        final TypedNode node = UnityNodes.createNode(this);
                
        final NodeCardinality[] childTypes = getChildrenTypes();
        if(childTypes.length==0 && ATOM_TYPES.contains(type)){
            //value node
            if(isArray){
                final Object value = readPrimitiveArray(ds,type,size);
                if(obj.asset.header.version > 5){
                    // arrays always need to be aligned in newer versions
                    ds.realign(4);
                }
                node.setValue(value);
            }else{
                final Object value = readPrimitive(ds,type);
                node.setValue(value);
                if(isByteAlign()) ds.realign(4);
            }
            
        }else{
            //sub nodes
            for(int i=0;i<childTypes.length;i++){
                final UnityNodeType childType = (UnityNodeType) childTypes[i].getType();
                
                // Check if the current node is an array and if the current field is
                // "data". In that case, "data" needs to be read "size" times.
                if (isArray && childType.getId().equals(new Chars("data"))) {
                    final int size = (Integer)node.getChild(new Chars("size")).getValue();

                    final TypedNode childNode = new DefaultTypedNode(childType);

                    // if the child type has no children, it has a primitve array
                    if (childType.getChildrenTypes().length==0) {
                        childNode.setValue(readPrimitiveArray(ds, childType.type, size));
                        if (obj.asset.header.version > 5) {
                            // arrays always need to be aligned in newer versions
                            ds.realign(4);
                        }
                    } else {
                        // read list of object nodes
                        TypedNode[] childFieldNodes = new TypedNode[size];
                        for (int k = 0; k < size; k++) {
                            childFieldNodes[k] = childType.readInstance(obj, ds, simplify);
                        }
                        childNode.setValue(childFieldNodes);
                    }

                    node.getChildren().add(childNode);
                } else {
                    node.getChildren().add(childType.readInstance(obj,ds, simplify));
                }
                
            }
        }
        
        if(simplify){
            //convert known types
            for(int i=0;i<PRIMITIVE_TYPES.length;i++){
                if(PRIMITIVE_TYPES[i].match(type)){
                    final Object cnt = PRIMITIVE_TYPES[i].toValue(node);
                    if(cnt instanceof UnityPointer){
                        ((UnityPointer)cnt).makeAbsolute(obj.asset);
                    }
                    node.setValue(cnt);
                    node.getChildren().removeAll();
                }
            }
        }
        
        return node;
    }

    private static Object readPrimitiveArray(DataInputStream ds, Chars type, int size) throws IOException{
        if(TYPE_BOOLEAN.equals(type, true, true)){
            final boolean[] array = new boolean[size];
            for(int i=0;i<size;i++) array[i] = ds.readByte()!=0;
            return array;
        }else if(TYPE_SINT8.equals(type, true, true)){
            return ds.readFully(new byte[size]);
        }else if(TYPE_UINT8.equals(type, true, true) || TYPE_CHAR.equals(type, true, true)){
            return ds.readFully(new byte[size]);
        }else if(TYPE_SINT16.equals(type, true, true) || TYPE_SHORT.equals(type, true, true)){
            return ds.readShort(size);
        }else if(TYPE_UINT16.equals(type, true, true) || TYPE_USHORT.equals(type, true, true)){
            return ds.readUShort(size);
        }else if(TYPE_SINT32.equals(type, true, true) || TYPE_INT.equals(type, true, true)){
            return ds.readInt(size);
        }else if(TYPE_UINT32.equals(type, true, true) || TYPE_UINT.equals(type, true, true)){
            return ds.readUInt(size);
        }else if(TYPE_SINT64.equals(type, true, true) || TYPE_LONG.equals(type, true, true)){
            return ds.readLong(size);
        }else if(TYPE_UINT64.equals(type, true, true) || TYPE_ULONG.equals(type, true, true)){
            //TODO not exact
            return ds.readLong(size);
        }else if(TYPE_FLOAT.equals(type, true, true)){
            return ds.readFloat(size);
        }else if(TYPE_DOUBLE.equals(type, true, true)){
            return ds.readDouble(size);
        }else{
            throw new IOException("Unknowed type : "+ type);
        }
    }
    
    private static Object readPrimitive(DataInputStream ds, Chars type) throws IOException{
        Object value = null;
        if(TYPE_BOOLEAN.equals(type, true, true)){
            value = ds.readByte()!=0;
        }else if(TYPE_SINT8.equals(type, true, true)){
            value = ds.readByte();
        }else if(TYPE_UINT8.equals(type, true, true) || TYPE_CHAR.equals(type, true, true)){
            value = ds.readUByte();
        }else if(TYPE_SINT16.equals(type, true, true) || TYPE_SHORT.equals(type, true, true)){
            value = ds.readShort();
        }else if(TYPE_UINT16.equals(type, true, true) || TYPE_USHORT.equals(type, true, true)){
            value = ds.readUShort();
        }else if(TYPE_SINT32.equals(type, true, true) || TYPE_INT.equals(type, true, true)){
            value = ds.readInt();
        }else if(TYPE_UINT32.equals(type, true, true) || TYPE_UINT.equals(type, true, true)){
            value = ds.readUInt();
        }else if(TYPE_SINT64.equals(type, true, true) || TYPE_LONG.equals(type, true, true)){
            value = ds.readLong();
        }else if(TYPE_UINT64.equals(type, true, true) || TYPE_ULONG.equals(type, true, true)){
            //TODO not exact
            value = ds.readLong();
        }else if(TYPE_FLOAT.equals(type, true, true)){
            value = ds.readFloat();
        }else if(TYPE_DOUBLE.equals(type, true, true)){
            value = ds.readDouble();
        }else{
            throw new UnimplementedException("Unknowed type : "+ type);
        }
        return value;
    }
    
    public void writeInstance(DataOutputStream ds, UnityAssetObject obj, TypedNode node) throws IOException{
        final NodeCardinality[] childTypes = getChildrenTypes();
        if(childTypes.length==0){
            //value node
            if(isArray){
                final Object value = node.getValue();
                if(obj.asset.header.version > 5){
                    // arrays always need to be aligned in newer versions
                    ds.realign(4);
                }
                writePrimitiveArray(ds, type, value);
            }else{
                final Object value =  node.getValue();
                writePrimitive(ds, type, value);
                if(isByteAlign()) ds.realign(4);
            }
            
        }else{
            //sub nodes
            for(int i=0;i<childTypes.length;i++){
                final UnityNodeType childType = (UnityNodeType) childTypes[i].getType();
                final TypedNode childNode = (TypedNode) ((Sequence)node.getChildren()).get(i);
                
                // Check if the current node is an array and if the current field is
                // "data". In that case, "data" needs to be read "size" times.
                if (isArray && childType.getId().equals(new Chars("data"))) {
                    final int childSize = (Integer)node.getChild(new Chars("size")).getValue();
                    
                    // if the child type has no children, it has a primitve array
                    if (childType.getChildrenTypes().length==0) {
                        writePrimitiveArray(ds,  childType.type, childNode.getValue());
                        if (obj.asset.header.version > 5) {
                            // arrays always need to be aligned in newer versions
                            ds.realign(4);
                        }
                    } else {
                        // read list of object nodes
                        TypedNode[] childFieldNodes = (TypedNode[]) childNode.getValue();
                        for (int k = 0; k < childSize; k++) {
                            ((UnityNodeType)childFieldNodes[k].getType()).writeInstance(ds, obj, childFieldNodes[k]);
                        }
                    }

                    node.getChildren().add(childNode);
                } else {
                    childType.writeInstance(ds, obj, childNode);
                }
            }
        }
    }
    
    private void writePrimitiveArray(DataOutputStream ds, Chars type, Object value) throws IOException{
        final int length = Array.getLength(value);
        for(int i=0;i<length;i++){
            writePrimitive(ds,type, Array.get(value, i));
        }
    }
    
    private void writePrimitive(DataOutputStream ds, Chars type, Object value) throws IOException{
        if(TYPE_BOOLEAN.equals(type, true, true)){
            ds.write((byte) (Boolean.TRUE.equals(value)? 1 : 0));
        }else if(TYPE_SINT8.equals(type, true, true)){
            ds.writeByte( ((Number)value).byteValue() );
        }else if(TYPE_UINT8.equals(type, true, true) || TYPE_CHAR.equals(type, true, true)){
            ds.writeUByte( ((Number)value).intValue() & 0xFF);
        }else if(TYPE_SINT16.equals(type, true, true) || TYPE_SHORT.equals(type, true, true)){
            ds.writeShort( ((Number)value).shortValue());
        }else if(TYPE_UINT16.equals(type, true, true) || TYPE_USHORT.equals(type, true, true)){
            ds.writeUShort( ((Number)value).intValue());
        }else if(TYPE_SINT32.equals(type, true, true) || TYPE_INT.equals(type, true, true)){
            ds.writeInt( ((Number)value).intValue());
        }else if(TYPE_UINT32.equals(type, true, true) || TYPE_UINT.equals(type, true, true)){
            ds.writeUInt( ((Number)value).longValue());
        }else if(TYPE_SINT64.equals(type, true, true) || TYPE_LONG.equals(type, true, true)){
            ds.writeLong( ((Number)value).longValue());
        }else if(TYPE_UINT64.equals(type, true, true) || TYPE_ULONG.equals(type, true, true)){
            ds.writeLong(( ((Number)value).longValue() ));
        }else if(TYPE_FLOAT.equals(type, true, true)){
            ds.writeFloat( ((Number)value).floatValue() );
        }else if(TYPE_DOUBLE.equals(type, true, true)){
            ds.writeDouble( ((Number)value).doubleValue());
        }else{
            throw new IOException("Unknowed type : "+ type);
        }
    }
    
}
