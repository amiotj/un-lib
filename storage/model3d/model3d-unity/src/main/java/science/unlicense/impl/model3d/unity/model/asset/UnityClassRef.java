
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.unity.model.UnityHash128;

/**
 *
 * @author Johann Sorel
 */
public class UnityClassRef {
    
    public int classID;
    public UnityHash128 scriptID;
    public UnityHash128 oldTypeHash;
    public UnityNodeType nodetype;

    public void read(UnityAsset asset, DataInputStream ds) throws IOException{
        if(asset.header.version> 13){
            //Unity 5+
            classID = ds.readInt();

            if (classID < 0) {
                scriptID = new UnityHash128();
                scriptID.read(ds);
            }

            oldTypeHash = new UnityHash128();
            oldTypeHash.read(ds);

            if (asset.embedded!=0) {
                nodetype = new UnityNodeType();
                nodetype.read(asset,ds);
            }

        }else{
            classID = ds.readInt();
            nodetype = new UnityNodeType();
            nodetype.read(asset,ds);
        }
    }

    public void write(UnityAsset asset, DataOutputStream ds) throws IOException{
        if(asset.header.version> 13){
            //Unity 5+
            ds.writeInt(classID);

            if (classID < 0) {
                scriptID.write(ds);
            }

            oldTypeHash.write(ds);

            if (asset.embedded!=0) {
                nodetype.write(asset,ds);
            }
        }else{
            ds.writeInt(classID);
            nodetype.write(asset,ds);
        }

    }


}
