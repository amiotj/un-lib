
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.unity.model.type.UnityPointer;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityObjectReference {

    public UnityAsset asset;
    public long pathId;
    public long offset;
    public long length;
    public long typeID;
    public int classID;
    public short isDestroyed;
    /** Unity5+ : unknown */
    public byte unknown1;
    
    public void read(UnityAsset asset, DataInputStream ds) throws IOException{
        this.asset = asset;
        ds.realign(4);
        if(asset.header.version > 13) {
            pathId = ds.readLong();
        } else {
            pathId = ds.readUInt();
        }
        offset = ds.readUInt();
        length = ds.readUInt();
        typeID = ds.readUInt();
        classID = ds.readShort();
        isDestroyed = ds.readShort();
        if(asset.header.version > 13) {
            unknown1 = ds.readByte();
        }
    }

    public void write(UnityAsset asset, DataOutputStream ds) throws IOException {
        ds.realign(4);
        if(asset.header.version > 13) {
            ds.writeLong(pathId);
        } else {
            ds.writeUInt(pathId);
        }
        ds.writeUInt(offset);
        ds.writeUInt(length);
        ds.writeUInt(typeID);
        ds.writeShort((short) classID);
        ds.writeShort(isDestroyed);
        if(asset.header.version > 13) {
            ds.writeByte(unknown1);
        }
    }
    
    public UnityPointer toPointer(){
        UnityPointer pptr = new UnityPointer();
        pptr.pathId = pathId;
        pptr.makeAbsolute(asset);
        return pptr;
    }
    
}
