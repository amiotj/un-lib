
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class ArrayType implements UnityValueType {

    private static final Chars NAME = new Chars("Array");
    private static final Chars ATT_size = new Chars("size");
    private static final Chars ATT_data = new Chars("data");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Object.class;
    }
    
    public Object toValue(TypedNode node) {
        final TypedNode child1 = node.getChild(ATT_size);
        final TypedNode child2 = node.getChild(ATT_data);
        return child2.getValue();
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
