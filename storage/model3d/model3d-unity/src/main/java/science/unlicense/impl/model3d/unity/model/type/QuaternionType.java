
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class QuaternionType implements UnityValueType {

    private static final Chars NAME = new Chars("Quaternionf");
    public static final Chars ATT_X = new Chars("x");
    public static final Chars ATT_Y = new Chars("y");
    public static final Chars ATT_Z = new Chars("z");
    public static final Chars ATT_W = new Chars("w");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Quaternion.class;
    }
    
    public Quaternion toValue(TypedNode node) {
        final Quaternion vector = new Quaternion();
        vector.set(0,(Float)node.getChild(ATT_X).getValue());
        vector.set(1,(Float)node.getChild(ATT_Y).getValue());
        vector.set(2,(Float)node.getChild(ATT_Z).getValue());
        vector.set(3,(Float)node.getChild(ATT_W).getValue());
        return vector;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
