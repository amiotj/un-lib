
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class LocalAABBType implements UnityValueType {

    private static final Chars NAME = new Chars("AABB");
    private static final Chars ATT_m_Center = new Chars("m_Center");
    private static final Chars ATT_m_Extent = new Chars("m_Extent");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return BBox.class;
    }
    
    public Object toValue(TypedNode node) {
        final BBox bbox = new BBox(3);
        final Vector center = (Vector) node.getChild(ATT_m_Center).getValue();
        final Vector extent = (Vector) node.getChild(ATT_m_Extent).getValue();
        
        for(int i=0;i<3;i++){
            final double c = center.get(i);
            final double x = extent.get(i);
            bbox.setRange(i, c-x, c+x);
        }

        return bbox;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
