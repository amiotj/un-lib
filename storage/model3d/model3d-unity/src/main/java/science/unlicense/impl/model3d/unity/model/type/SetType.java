
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.collection.primitive.DoubleSequence;
import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.collection.primitive.LongSequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 * vector types contains a single array type node named 'Array';
 * @author Johann Sorel
 */
public class SetType implements UnityValueType {

    private static final Chars NAME = new Chars("set");
    private static final Chars ATT_Array = new Chars("Array");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Sequence.class;
    }
    
    public Object toValue(TypedNode node) {
        final Object array = node.getChild(ATT_Array).getValue();
        if(array instanceof byte[]){
            return new ByteSequence((byte[]) array);
        }else if(array instanceof int[]){
            return new IntSequence((int[]) array);
        }else if(array instanceof long[]){
            return new LongSequence((long[]) array);
        }else if(array instanceof float[]){
            return new FloatSequence((float[]) array);
        }else if(array instanceof double[]){
            return new DoubleSequence((double[]) array);
        }else if(array instanceof Object[]){
            return new ArraySequence((Object[]) array);
        }
        throw new RuntimeException("Unexpected array type : "+array.getClass());
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
