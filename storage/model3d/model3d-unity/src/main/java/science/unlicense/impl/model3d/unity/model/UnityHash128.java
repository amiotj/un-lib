
package science.unlicense.impl.model3d.unity.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Int32;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de>
 */
public class UnityHash128 extends CObject {
    
    private final byte[] hash = new byte[16];

    public UnityHash128() {
    }
    
    public byte[] hash() {
        return hash;
    }

    public void read(DataInputStream in) throws IOException {
        in.readFully(hash);
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(hash);
    }

    public Chars toChars() {
        return Int32.encodeHexa(hash);
    }
    
}
