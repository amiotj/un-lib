
package science.unlicense.impl.model3d.unity;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Unity Engine package format.
 * 
 * Many decoding informations are from :
 * https://github.com/ata4/disunity (also in public domain)
 * 
 *
 * @author Johann Sorel
 */
public class UnityBundleFormat extends AbstractModel3DFormat{

    public static final UnityBundleFormat INSTANCE = new UnityBundleFormat();

    private UnityBundleFormat() {
        super(new Chars("UNITY_BUNDLE"),
              new Chars("Unity bundle"),
              new Chars("Unity engine package Format"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("unity3d")
              },
              new byte[][]{
                UnityConstants.SIGNATURE_RAW,
                UnityConstants.SIGNATURE_WEB
                });
    }

    public Model3DStore open(Object input) throws IOException {
        return new UnityStore(input);
    }

}
