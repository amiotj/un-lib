
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.impl.model3d.unity.model.UnityVersion;
import science.unlicense.api.CObject;
import science.unlicense.api.Orderable;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class UnityClass extends CObject implements Orderable{
    
    public int id;
    public Chars name;
    public UnityVersion version;
    public UnityNodeType type;
    
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnityClass other = (UnityClass) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.version != other.version && (this.version == null || !this.version.equals(other.version))) {
            return false;
        }
        return true;
    }

    public int getHash() {
        int hash = 7;
        hash = 59 * hash + this.id;
        hash = 59 * hash + (this.version != null ? this.version.hashCode() : 0);
        return hash;
    }

    public int order(Object other) {
        final UnityClass o = (UnityClass) other;
        if(o.id==id){
            return version.order(o.version);
        }else{
            return id-o.id;
        }
    }
}
