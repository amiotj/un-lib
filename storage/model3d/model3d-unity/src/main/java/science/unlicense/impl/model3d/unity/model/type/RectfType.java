
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class RectfType implements UnityValueType {

    private static final Chars NAME = new Chars("Rectf");
    public static final Chars ATT_X = new Chars("x");
    public static final Chars ATT_Y = new Chars("y");
    public static final Chars ATT_WIDTH = new Chars("width");
    public static final Chars ATT_HEIGHT = new Chars("height");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Rectangle.class;
    }
    
    public Rectangle toValue(TypedNode node) {
        final Rectangle rect = new Rectangle();
        rect.setX((Float)node.getChild(ATT_X).getValue());
        rect.setY((Float)node.getChild(ATT_Y).getValue());
        rect.setWidth((Float)node.getChild(ATT_WIDTH).getValue());
        rect.setHeight((Float)node.getChild(ATT_HEIGHT).getValue());
        return rect;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
