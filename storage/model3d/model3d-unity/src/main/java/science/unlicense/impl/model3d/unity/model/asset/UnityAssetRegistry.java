
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.api.collection.Iterator;
import science.unlicense.impl.model3d.unity.model.type.UnityPointer;

/**
 *
 * @author Johann Sorel
 */
public interface UnityAssetRegistry {
    
    /**
     * Iterator over all unity asset objects in the registry.
     * @return 
     */
    Iterator getIterator();
    
    /**
     * Find object for given pointer.
     * 
     * @param pptr
     * @return 
     */
    UnityAssetObject get(UnityPointer pptr);
    
}
