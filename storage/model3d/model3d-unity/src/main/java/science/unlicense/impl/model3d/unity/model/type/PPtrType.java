
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class PPtrType implements UnityValueType {

    private static final Chars NAME = new Chars("PPtr");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return name.startsWith(NAME);
    }

    public Class valueClass() {
        return UnityPointer.class;
    }
    
    public Object toValue(TypedNode node) {
        final UnityPointer pptr = new UnityPointer();
        pptr.fileId = ((Number)node.getChild(new Chars("m_FileID")).getValue()).longValue();
        pptr.pathId = ((Number)node.getChild(new Chars("m_PathID")).getValue()).longValue();
        return pptr;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
