
package science.unlicense.impl.model3d.unity.model.bundle;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;
import science.unlicense.impl.io.ClipInputStream;
import science.unlicense.impl.io.ClipSeekableByteBuffer;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityBundleEntry extends AbstractPath {

    private final UnityBundle bundle;
    public Chars name;
    public long offset;
    public long size;

    public UnityBundleEntry(UnityBundle bundle) {
        this.bundle = bundle;
    }
    
    public void read(DataInputStream ds) throws IOException{
        name = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        offset = ds.readUInt();
        size = ds.readUInt();
    }

    @Override
    public Collection getChildren() {
        return Collections.emptyCollection();
    }

    @Override
    public Chars getName() {
        return name;
    }

    @Override
    public Path getParent() {
        return bundle;
    }

    @Override
    public boolean isContainer() throws IOException {
        return false;
    }

    @Override
    public boolean exists() throws IOException {
        return true;
    }
    
    @Override
    public boolean createContainer() throws IOException {
        throw new IOException("Not supported.");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new IOException("Not supported.");
    }

    @Override
    public Path resolve(Chars address) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public PathResolver getResolver() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        final ByteInputStream in = IOUtilities.toInputStream(bundle.getInput(), new boolean[1]);
        final DataInputStream ds = new DataInputStream(in);
        ds.skipFully(bundle.getHeader().headerSize);
        
        //TODO LZMA if Web bundle
        ds.skipFully(offset);
        
        return new ClipInputStream(in, (int)size);
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        final SeekableByteBuffer parent = bundle.createSeekableBuffer(read, write, resize);
        return new ClipSeekableByteBuffer(parent, bundle.getHeader().headerSize+offset, size);        
    }
    
    @Override
    public Chars toURI() {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
