
package science.unlicense.impl.model3d.unity.model.primitive;

import java.lang.reflect.InvocationTargetException;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.number.Int32;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class UnityNodes {

    private static final Chars BASE = new Chars("uscience.unlicense.impl.model3d.unity.model.primitive.");
    private static final Dictionary CLASSES_CACHE = new HashDictionary();
    
    public static TypedNode createNode(UnityNodeType type){
        final Chars nid = type.type.concat(Int32.encode(type.unversion));
        final Chars className = BASE.concat(nid);
        Class clazz = null;
        synchronized(CLASSES_CACHE) {
            clazz = (Class) CLASSES_CACHE.getValue(className);
            if(clazz==null) {
                try {
                    clazz = Class.forName(className.toString());
                } catch (ClassNotFoundException ex) {
                    clazz = Object.class;
                }
                CLASSES_CACHE.add(className, clazz);
            }
        }
        
        if(clazz==null || clazz==Object.class){
            return new DefaultTypedNode(type);
        }else{
            try {
                return (TypedNode) clazz.getConstructor(UnityNodeType.class).newInstance(type);
            } catch (InstantiationException ex) {
                throw new RuntimeException(ex);
            } catch (IllegalAccessException ex) {
                throw new RuntimeException(ex);
            } catch (InvalidArgumentException ex) {
                throw new RuntimeException(ex);
            } catch (InvocationTargetException ex) {
                throw new RuntimeException(ex);
            } catch (NoSuchMethodException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
