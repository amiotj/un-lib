
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.impl.model3d.unity.model.UnityVersion;
import science.unlicense.api.CObject;
import science.unlicense.api.Sorter;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.io.SeekableInputStream;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.impl.model3d.unity.model.UnityHash128;
import science.unlicense.impl.model3d.unity.model.type.UnityPointer;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityAsset extends CObject implements UnityAssetRegistry {

    public Path input;
    public UnityAssetHeader header = new UnityAssetHeader();
    
    //structures
    public final Dictionary types = new OrderedHashDictionary();
    public int attributes;
    public int embedded;
    public UnityVersion unityVersion;
    public final Sequence catalog = new ArraySequence();
    public final Sequence assetRefs = new ArraySequence();
    //objects
    private final Sequence objects = new ArraySequence();
    private final Dictionary objectsIndex = new HashDictionary();

    //used for decoding
    private UnityClasses classes;
    
    public UnityAsset(Path input) {
        this.input = input;
    }

    public UnityClasses getClasses() {
        if(classes==null){
            classes = UnityClasses.getDefaultClasses();
        }
        return classes;
    }

    public int getObjectCount(){
        return objects.getSize();
    }
    
    public void setClasses(UnityClasses classes) {
        this.classes = classes;
    }
    
    public Iterator getIterator() {
        return objects.createIterator();
    }
    
    public UnityAssetObject get(UnityPointer pptr){
        if(pptr.fileAbsolutePath==null){
            throw new RuntimeException("Pointer is not absolute");
        }
        
        if(!pptr.fileAbsolutePath.equals(input)) return null;
        
        for(int i=0,n=objects.getSize();i<n;i++){
            final UnityAssetObject obj = (UnityAssetObject) objects.get(i);
            if(obj.ref.pathId==pptr.pathId){
                return obj;
            }
        }
        return null;
    }


    public void read() throws IOException{
        
        final SeekableByteBuffer ss = input.createSeekableBuffer(true, false, false);
        final SeekableInputStream stream = new SeekableInputStream(ss);
        final DataInputStream ds = new DataInputStream(stream,NumberEncoding.BIG_ENDIAN);
        
        //read header
        header.read(ds);
                
        if (header.version < 9) {
            ss.setPosition(header.fileSize-header.metaSize+1);
        }
        
        //read structure infos
        ds.setEncoding( (header.version<=5) ? NumberEncoding.BIG_ENDIAN : NumberEncoding.LITTLE_ENDIAN);
        readTypes(ds);
        readInfos(ds);
        readCatalog(ds);
        readReferences(ds);
    }
        
    private void readTypes(DataInputStream ds) throws IOException{
        if(header.version >= 7){
            unityVersion = new UnityVersion(ds.readZeroTerminatedChars(255, CharEncodings.US_ASCII));
            attributes = ds.readInt();
        }
        
        if(header.version> 13){
            //Unity 5+
            embedded = ds.read();
            final int nbClass = ds.readInt();
            for(int i=0;i<nbClass;i++){                
                final UnityClassRef classRef = new UnityClassRef();
                classRef.read(this, ds);
                types.add(classRef.classID, classRef);
            }
            
        }else{
            final int nbClass = ds.readInt();
            for(int i=0;i<nbClass;i++){
                final UnityClassRef classRef = new UnityClassRef();
                classRef.read(this, ds);
                types.add(classRef.classID, classRef);
            }
            if(header.version>=7){
                ds.readInt();
            }
        }
    }
    
    private void readInfos(DataInputStream ds) throws IOException{
        final int nbInfo = ds.readInt();
        
        if (header.version > 13) {
            ds.realign(4);
        }
        
        for(int i=0; i<nbInfo; i++){
            final UnityObjectReference info = new UnityObjectReference();
            info.read(this,ds);
            final UnityAssetObject obj = new UnityAssetObject(this, info);
            objects.add(obj);
            objectsIndex.add(obj.ref.pathId, obj);
        }
    }
    
    private void readCatalog(DataInputStream ds) throws IOException {
        // Unity 5+
        if (header.version > 13) {
            final int nb = ds.readInt();
            ds.realign(4);
            for(int i=0;i<nb;i++){
                final UnityCatalogRecord rec = new UnityCatalogRecord();
                rec.read(ds);
                catalog.add(rec);
            }
        }
    }
    
    private void readReferences(DataInputStream ds) throws IOException{
        final int nb = ds.readInt();
        if (header.version > 13) {
            ds.realign(4);
        }
            
        for(int i=0;i<nb;i++){
            final UnityAssetReference info = new UnityAssetReference();
            info.read(this,ds);
            assetRefs.add(info);
        }
    }
    
    public void write() throws IOException{
        
        final ByteOutputStream bs = input.createOutputStream();
        final DataOutputStream ds = new DataOutputStream(bs,NumberEncoding.BIG_ENDIAN);
                
        //write header
        header.write(ds);
                
        if (header.version < 9) {
            final long pos = header.fileSize-header.metaSize+1;
            ds.skipFully(pos - ds.getByteOffset());
        }
        
        //write structure infos
        ds.setEncoding( (header.version<5) ? NumberEncoding.BIG_ENDIAN : NumberEncoding.LITTLE_ENDIAN);
        writeTypes(ds);
        writeInfos(ds);
        writeCatalog(ds);
        writeReferences(ds);
        
        //write objects
        //sort by offset
        Collections.sort(objects, new Sorter() {
            public int sort(Object first, Object second) {
                return (int) (((UnityAssetObject)first).ref.offset - ((UnityAssetObject)second).ref.offset);
            }
        });
        
        for(int i=0;i<objects.getSize();i++){
            final UnityAssetObject obj = (UnityAssetObject) objects.get(i);
            final long absOffset = header.dataOffset + obj.ref.offset;
            ds.skipFully(absOffset-ds.getByteOffset());
            ds.write(obj.objectBytes);
        }
        
    }
        
    private void writeTypes(DataOutputStream ds) throws IOException{
        if(header.version >= 7){
            ds.writeZeroTerminatedChars(unityVersion.str,255, CharEncodings.US_ASCII);
            ds.writeInt(attributes);
        }

        if(header.version> 13){
            //Unity 5+
            ds.writeByte((byte) embedded);
            final int nbClass = types.getSize();
            ds.writeInt(nbClass);

            final Iterator ite = types.getPairs().createIterator();
            while(ite.hasNext()){
                final Pair pair = (Pair) ite.next();
                final UnityClassRef classRef = (UnityClassRef) pair.getValue2();
                classRef.write(this, ds);
            }

        }else{
            final int nbClass = types.getSize();
            ds.writeInt(nbClass);

            final Iterator ite = types.getPairs().createIterator();
            while(ite.hasNext()){
                final Pair pair = (Pair) ite.next();
                final UnityClassRef classRef = (UnityClassRef) pair.getValue2();
                classRef.write(this, ds);
            }

            if(header.version>=7){
                ds.writeInt(0);
            }
        }

    }
    
    private void writeInfos(DataOutputStream ds) throws IOException{
        final int nbInfo = objects.getSize();
        ds.writeInt(nbInfo);

        if (header.version > 13) {
            ds.realign(4);
        }

        for(int i=0; i<nbInfo; i++){
            final UnityAssetObject obj = (UnityAssetObject) objects.get(i);
            obj.ref.write(this,ds);
        }
    }

    private void writeCatalog(DataOutputStream ds) throws IOException {
        // Unity 5+
        if (header.version > 13) {
            final int nb = catalog.getSize();
            ds.writeInt(nb);
            ds.realign(4);
            for(int i=0;i<nb;i++){
                final UnityCatalogRecord rec = (UnityCatalogRecord) catalog.get(i);
                rec.write(ds);
            }
        }
    }

    private void writeReferences(DataOutputStream ds) throws IOException{
        ds.writeInt(assetRefs.getSize());
        if (header.version > 13) {
            ds.realign(4);
        }
        
        for(int i=0,n=assetRefs.getSize();i<n;i++){
            final UnityAssetReference info = (UnityAssetReference) assetRefs.get(i);
            info.write(this,ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("UnityAsset"), new Object[]{
            new Chars("types : "+types.getSize()),
            new Chars("objects : "+objects.getSize()),
            new Chars("references : "+assetRefs.getSize()),
        });
    }
    
}
