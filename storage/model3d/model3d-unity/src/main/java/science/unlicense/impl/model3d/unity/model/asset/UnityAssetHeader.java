
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityAssetHeader {

    public long metaSize;
    public long fileSize;
    public int version;
    public int dataOffset;
    public byte endianness;
    
    public void read(DataInputStream ds) throws IOException {
        metaSize = ds.readInt();
        fileSize = ds.readUInt();
        version = ds.readInt();
        dataOffset = ds.readInt();
        if (version >= 9) {
            endianness = ds.readByte();
            ds.skipFully(3);
        }
    }
    
    public void write(DataOutputStream ds) throws IOException{
        ds.writeUInt(metaSize);
        ds.writeUInt(fileSize);
        ds.writeInt(version);
        ds.writeInt(dataOffset);
        if (version >= 9) {
            ds.writeByte(endianness);
            ds.writeByte((byte)0);
            ds.writeByte((byte)0);
            ds.writeByte((byte)0);
        }
    }

}
