
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class Vector3fType implements UnityValueType {

    private static final Chars NAME = new Chars("Vector3f");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return Vector.class;
    }
    
    public Object toValue(TypedNode node) {
        final Vector vector = new Vector(3);
        vector.set(0,(Float)node.getChild(QuaternionType.ATT_X).getValue());
        vector.set(1,(Float)node.getChild(QuaternionType.ATT_Y).getValue());
        vector.set(2,(Float)node.getChild(QuaternionType.ATT_Z).getValue());
        return vector;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
