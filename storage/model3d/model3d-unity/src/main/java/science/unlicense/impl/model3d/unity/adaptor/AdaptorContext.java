
package science.unlicense.impl.model3d.unity.adaptor;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.model3d.unity.UnityConstants;
import static science.unlicense.impl.model3d.unity.adaptor.UnityMeshAdaptor.*;
import science.unlicense.impl.model3d.unity.model.asset.UnityAssetObject;
import science.unlicense.impl.model3d.unity.model.asset.UnityAssetRegistry;
import science.unlicense.impl.model3d.unity.model.type.UnityPointer;

/**
 *
 * @author Johann Sorel
 */
public class AdaptorContext {
    
    public final UnityAssetRegistry registry;
    /**
     * UnityPPtr -> Texture2D
     */
    public final Dictionary textureCache = new HashDictionary();
    /**
     * UnityPPtr -> Shell
     */
    public final Dictionary meshCache = new HashDictionary();
    /**
     * UnityPPtr -> GLNode
     */
    public final Dictionary glNodeCache = new HashDictionary();

    private final Sequence roots = new ArraySequence();
    
    public AdaptorContext(UnityAssetRegistry registry) {
        this.registry = registry;
    }
    
    /**
     * Rebuild parent -> child scene.
     * @return 
     */
    public GLNode aggregateScene(){
        final GLNode root = new GLNode();
        root.getChildren().addAll(roots);
        return root;
    }
    
    public GLNode getOrReadGameObject(UnityPointer pptr) throws IOException{
        if(pptr==null) return null;
        GLNode glNode = (GLNode) glNodeCache.getValue(pptr);
        
        if(glNode==null){
            final TypedNode node = getNode(pptr);
            Object[] vals = UnityMeshAdaptor.readGameObject(this, node);
            glNode = (GLNode) vals[0];
            glNodeCache.add(pptr, glNode);
            if(Boolean.TRUE.equals(vals[1])){
                roots.add(glNode);
            }
        }
        return glNode;
    }
    
    public Shell getOrReadShell(UnityPointer pptr) throws IOException{
        if(pptr==null) return null;
        Shell shell = (Shell) meshCache.getValue(pptr);
        if(shell==null){
            System.out.println("load mesh :"+pptr);
            UnityAssetObject mobj = registry.get(pptr);
            if(mobj!=null && mobj.ref.classID == UnityConstants.CLASS_MESH){
                TypedNode decode = mobj.decode(true);
                if(decode instanceof Mesh){
                    shell =  readMesh(this,(science.unlicense.impl.model3d.unity.model.primitive.Mesh)decode);
                    meshCache.add(pptr, shell);
                }
            }
        }
        return shell;
    }
    
    public Texture2D getOrReadTexture(UnityPointer pptr) throws IOException{
        if(pptr==null) return null;
        Texture2D tex = (Texture2D) textureCache.getValue(pptr);
        if(tex==null){
            System.out.println("load texture :"+pptr);
            final UnityAssetObject textureObj = registry.get(pptr);
            final TypedNode textureNode = textureObj.decode(true);
            final Image textureImage = UnityTextureAdaptor.read((science.unlicense.impl.model3d.unity.model.primitive.Texture2D) textureNode, false);
            tex = new Texture2D(textureImage);
            textureCache.add(pptr, tex);
        }
        return tex;
    }
    
    public TypedNode getNode(UnityPointer pptr) throws IOException{
        final UnityAssetObject mobj = registry.get(pptr);
        if(mobj!=null){
            return mobj.decode(true);
        }
        return null;
    }
        
}
