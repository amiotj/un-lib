
package science.unlicense.impl.model3d.unity.model.asset;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Nico Bergemann <barracuda415 at yahoo.de> (from disunity project)
 * @author Johann Sorel
 */
public class UnityAssetReference {

    public Chars assetPath;
    public long[] guid;
    public int type;
    public Chars filePath;
    
    public void read(UnityAsset asset, DataInputStream ds) throws IOException{
        if(asset.header.version>5){
            assetPath = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        }
        guid = new long[]{ds.readLong(NumberEncoding.BIG_ENDIAN),ds.readLong(NumberEncoding.BIG_ENDIAN)};
        type = ds.readInt();
        filePath = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
    }
    
    public void write(UnityAsset asset, DataOutputStream ds) throws IOException{
        if(asset.header.version>5){
            ds.writeZeroTerminatedChars(assetPath, 0, CharEncodings.US_ASCII);
        }
        ds.writeLong(guid, NumberEncoding.BIG_ENDIAN);
        ds.writeInt(type);
        ds.writeZeroTerminatedChars(filePath, 0, CharEncodings.US_ASCII);
    }

    public String toString() {
        return "AssetReference("+assetPath+","+guid[0]+","+guid[1]+","+type+","+filePath+")";
    }
 
}
