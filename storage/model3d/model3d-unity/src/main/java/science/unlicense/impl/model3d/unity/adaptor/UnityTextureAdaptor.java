
package science.unlicense.impl.model3d.unity.adaptor;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.DefaultImageReadParameters;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.material.mapping.SpriteInfo;
import science.unlicense.impl.image.RawImageReader;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.impl.image.dxt.DXTConstants;
import science.unlicense.impl.image.dxt.DXTHeader;
import science.unlicense.impl.image.dxt.DXTImageReadParameters;
import science.unlicense.impl.image.dxt.DXTImageReader;
import science.unlicense.impl.image.dxt.DXTPixelFormat;
import static science.unlicense.impl.model3d.unity.UnityConstants.*;
import science.unlicense.impl.model3d.unity.model.primitive.*;

/**
 *
 * @author Johann Sorel
 */
public class UnityTextureAdaptor {

    private static final Chars ATT_m_Width = new Chars("m_Width");
    private static final Chars ATT_m_Height = new Chars("m_Height");
    private static final Chars ATT_m_CompleteImageSize = new Chars("m_CompleteImageSize");
    private static final Chars ATT_m_TextureFormat = new Chars("m_TextureFormat");
    private static final Chars ATT_m_MipMap = new Chars("m_MipMap");
    private static final Chars ATT_m_IsReadable = new Chars("m_IsReadable");
    private static final Chars ATT_m_ImageCount = new Chars("m_ImageCount");
    private static final Chars ATT_m_TextureDimension = new Chars("m_TextureDimension");
    private static final Chars ATT_m_TextureSettings = new Chars("m_TextureSettings");
        private static final Chars ATT_m_FilterMode = new Chars("m_FilterMode");
        private static final Chars ATT_m_Aniso = new Chars("m_Aniso");
        private static final Chars ATT_m_MipBias = new Chars("m_MipBias");
        private static final Chars ATT_m_WrapMode = new Chars("m_WrapMode");
    private static final Chars ATT_m_LightmapFormat = new Chars("m_LightmapFormat");
    private static final Chars ATT_m_ColorSpace = new Chars("m_ColorSpace");
    private static final Chars ATT_image_data = new Chars("image data");

    public static Image read(Texture2D tex, boolean uncompress) throws IOException{
        final int width;
        final int height;
        final int format;
        final boolean mipmap;
        final int nbImage;
        final int dimension;
        final byte[] buffer;
        
        if(tex instanceof Texture2D1){
            final Texture2D1 t = (Texture2D1) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();
        }else if(tex instanceof Texture2D2){
            final Texture2D2 t = (Texture2D2) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();
            
        }else if(tex instanceof Texture2D3){
            final Texture2D3 t = (Texture2D3) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();
            
        }else if(tex instanceof Texture2D4){
            final Texture2D4 t = (Texture2D4) tex;
            width = t.getm_Width();
            height = t.getm_Height();
            format = t.getm_TextureFormat();
            mipmap = t.getm_MipMap();
            nbImage = t.getm_ImageCount();
            dimension = t.getm_TextureDimension();
            buffer = t.getimage_data();
            
        }else{
            throw new IOException("Unexpected texture type : "+tex);
        }
        
        if(format==TEXFORMAT_DXT1){
            final DXTImageReader reader = new DXTImageReader();
            reader.setInput(buffer);
            
            final DXTHeader header = new DXTHeader();
            header.width = width;
            header.height = height;
            header.pixelFormat = new DXTPixelFormat();
            header.pixelFormat.fourCC = DXTConstants.D3DFMT_DXT1;
            final DXTImageReadParameters params = new DXTImageReadParameters();
            params.setDecompress(uncompress);
            params.setHeader(header);
            return reader.read(params);
        }else if(format==TEXFORMAT_DXT5){
            final DXTImageReader reader = new DXTImageReader();
            reader.setInput(buffer);
            
            final DXTHeader header = new DXTHeader();
            header.width = width;
            header.height = height;
            header.pixelFormat = new DXTPixelFormat();
            header.pixelFormat.fourCC = DXTConstants.D3DFMT_DXT5;
            final DXTImageReadParameters params = new DXTImageReadParameters();
            params.setDecompress(uncompress);
            params.setHeader(header);
            return reader.read(params);
        }
        
        final Extent.Long ext = new Extent.Long(width, height);
        
        final RawImageReader reader;
        if(format==TEXFORMAT_ARGB32)        reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_ARGB32);
        else if(format==TEXFORMAT_BGRA32)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_BGRA32);
        else if(format==TEXFORMAT_RGB24)    reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGB24);
        else if(format==TEXFORMAT_RGBA32)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGBA32);
        else if(format==TEXFORMAT_RGB565)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGB565);
        else if(format==TEXFORMAT_ALPHA8)   reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_ALPHA8);
        else if(format==TEXFORMAT_RGBA4444) reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_RGBA4444);
        else if(format==TEXFORMAT_ARGB4444) reader = new RawImageReader(ext, RawImageReader.IMAGE_TYPE_ARGB4444);
        else                                throw new IOException("Unsupported texture format : "+format);
        
        final ImageReadParameters params = new DefaultImageReadParameters();
        params.setBufferFactory(GLBufferFactory.INSTANCE);
        params.setDecompress(uncompress);
        reader.setInput(buffer);
        final Image img = reader.read(params);
        
        return img;
    }

    public static SpriteInfo read(Sprite sprite){
        
        if(sprite instanceof Sprite1){
            
        }else if(sprite instanceof Sprite2){
            
        }
        throw new UnimplementedException("TODO");
    }
    
}
