
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class ColorRGBAType implements UnityValueType {

    private static final Chars NAME = new Chars("ColorRGBA");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }

    public Class valueClass() {
        return Color.class;
    }
    
    public Object toValue(TypedNode node) {
        //version 2
        final TypedNode rgbaNode = node.getChild(new Chars("rgba"));
        if(rgbaNode!=null){
            return new Color( ((Number)rgbaNode.getValue()).intValue() );
        }
        
        //version 1
        final TypedNode rNode = node.getChild(new Chars("r"));
        final TypedNode gNode = node.getChild(new Chars("g"));
        final TypedNode bNode = node.getChild(new Chars("b"));
        final TypedNode aNode = node.getChild(new Chars("a"));
        if(rNode!=null){
            return new Color(
                    (Float)rNode.getValue(),
                    (Float)gNode.getValue(),
                    (Float)bNode.getValue(),
                    (Float)aNode.getValue()
            );
        }
        
        return null;
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnimplementedException("Not supported yet.");
    }

}
