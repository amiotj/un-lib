
package science.unlicense.impl.model3d.unity;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Unity Engine asset format.
 * 
 * Many decoding informations are from :
 * https://github.com/ata4/disunity (also in public domain)
 * 
 *
 * @author Johann Sorel
 */
public class UnityAssetFormat extends AbstractModel3DFormat{

    public static final UnityAssetFormat INSTANCE = new UnityAssetFormat();

    private UnityAssetFormat() {
        super(new Chars("UNITY_ASSET"),
              new Chars("Unity asset"),
              new Chars("Unity engine asset Format"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("asset"),
                new Chars("assets")
              },
              new byte[][]{});
    }

    public Model3DStore open(Object input) throws IOException {
        return new UnityStore(input);
    }

}
