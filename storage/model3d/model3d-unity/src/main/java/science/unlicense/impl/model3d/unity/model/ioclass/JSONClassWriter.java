
package science.unlicense.impl.model3d.unity.model.ioclass;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.impl.binding.json.JSONWriter;
import science.unlicense.impl.model3d.unity.model.asset.UnityClass;
import science.unlicense.impl.model3d.unity.model.asset.UnityClasses;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class JSONClassWriter extends AbstractWriter {
    
    public void write(UnityClasses classes) throws IOException{
        final JSONWriter writer = new JSONWriter();
        writer.setOutput(getOutput());
        
        writer.writeObjectBegin();
        writer.writeName(new Chars("classes"));
        writer.writeArrayBegin();
        
        final Iterator ite = classes.getClasses().createIterator();
        while(ite.hasNext()){
            final UnityClass clazz = (UnityClass) ite.next();
            write(writer,clazz);
        }
        
        writer.writeArrayEnd();
        writer.writeObjectEnd();
        writer.dispose();
    }
    
    public static void write(JSONWriter writer, UnityClass clazz) throws IOException{
        writer.writeObjectBegin();
        writer.writeName(new Chars("id"));       writer.writeValue(clazz.id);
        writer.writeName(new Chars("name"));     writer.writeValue(clazz.name);
        writer.writeName(new Chars("version"));  writer.writeValue(clazz.version.str);
        writer.writeName(new Chars("type"));     writeNode(writer,clazz.type);
        writer.writeObjectEnd();
    }
    
    private static void writeNode(JSONWriter writer, UnityNodeType node) throws IOException{
        writer.writeObjectBegin();
        
        writer.writeName(new Chars("name"));
        writer.setFormatted(false);
        writer.writeValue(node.getId());
        writer.writeName(new Chars("type"));    writer.writeValue(node.type);
        writer.writeName(new Chars("size"));    writer.writeValue(node.size);
        writer.writeName(new Chars("array"));   writer.writeValue(node.isArray);
        writer.writeName(new Chars("version")); writer.writeValue(node.version);
        writer.writeName(new Chars("unversion")); writer.writeValue(node.unversion);
        writer.writeName(new Chars("index")); writer.writeValue(node.index);
        writer.writeName(new Chars("flags"));   writer.writeValue(node.metaFlag);
        writer.setFormatted(true);
        
        //children
        final NodeCardinality[] children = node.getChildrenTypes();
        if(children.length>0){
            writer.writeName(new Chars("nodes"));
            writer.writeArrayBegin();
            for(NodeCardinality child : children){
                writeNode(writer, (UnityNodeType) child.getType());
            }
            writer.writeArrayEnd();
        }
        
        writer.writeObjectEnd();
    }
    
}
