
package science.unlicense.impl.model3d.unity.model.type;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.unity.model.asset.UnityNodeType;

/**
 *
 * @author Johann Sorel
 */
public class TypelessDataType implements UnityValueType {

    private static final Chars NAME = new Chars("TypelessData");

    public Chars getName() {
        return NAME;
    }

    public boolean match(Chars name) {
        return NAME.equals(name);
    }
    
    public Class valueClass() {
        return byte[].class;
    }
    
    public Object toValue(TypedNode node) {
        final TypedNode child1 = node.getChild(new Chars("size"));
        final TypedNode child2 = node.getChild(new Chars("data"));
        return (byte[])child2.getValue();
    }

    public TypedNode toNode(Object value, UnityNodeType type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
