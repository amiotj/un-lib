package science.unlicense.impl.model3d.collada;

import science.unlicense.impl.binding.xml.dom.DomNode;
import science.unlicense.impl.binding.xml.dom.DomReader;

/**
 * @author Johann Sorel
 */
public class ColladaReader {


    public static ColladaStore read(final Object f) throws Exception {
        //read the xml
        final DomReader reader = new DomReader();
        reader.setInput(f);
        final DomNode root = reader.read();
        reader.dispose();

        //rebuild scene and objects
        final ColladaStore store = new ColladaStore();
        System.out.println(root);

        return store;
    }



}
