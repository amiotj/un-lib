
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class LWOThumbnail extends LWOChunk {
    
    /** UInt2 */
    public int encoding;
    /** UInt2 */
    public int width;
    /** byte* */
    public byte[] data;
    
    public void readInternal(DataInputStream ds) throws IOException {
        encoding = ds.readUShort();
        width = ds.readUShort();
        int height = (int) ((length-4)/width);
        data = ds.readFully(new byte[width*height*3]);
    }
    
}
