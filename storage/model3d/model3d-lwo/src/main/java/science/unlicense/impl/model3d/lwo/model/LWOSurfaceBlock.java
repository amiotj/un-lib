
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.lwo.LWOConstants;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOSurfaceBlock extends LWOChunk {
    
    
    private static final Dictionary BLOCK_SUBCHUNKS = new HashDictionary();
    private static final Dictionary HEADER_SUBCHUNKS = new HashDictionary();
    private static final Dictionary TMAP_SUBCHUNKS = new HashDictionary();
    static {
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_IMAP, BlockHeader.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_PROC, BlockHeader.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_GRAD, BlockHeader.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_SHDR, BlockHeader.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP, TextureMapping.class);
        
        //HEADER
        HEADER_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_CHAN, Channel.class);
        HEADER_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_ENAB, EnableState.class);
        HEADER_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_OPAC, Opacity.class);
        HEADER_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_AXIS, DisplacementAxis.class);
        HEADER_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_HEADER_NEGA, Negative.class);
        
        //TMAP
        TMAP_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP_CNTR, Center.class);
        TMAP_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP_CSYS, CoordinateSystem.class);
        TMAP_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP_FALL, Falloff.class);
        TMAP_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP_OREF, ObjectReference.class);
        TMAP_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP_ROTA, Rotation.class);
        TMAP_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_TMAP_SIZE, Size.class);
        
        //IMAP
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_AAST, AntiAliasingStrenght.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_AXIS, MajorAxis.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_IMAG, ImageMap.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_PIXB, PixelBlending.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_PROJ, Projection.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_STCK, StickyProjection.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_TAMP, TextureAmplitude.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_VMAP, UVVertexMap.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_WRAP, ImageWrap.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_WRPH, WrapHeight.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_WRPW, WrapWidth.class);
        
        //PROC
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_PTEX_AXIS, MajorAxis.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_PTEX_FUNC, Function.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_PTEX_VALU, BasicValue.class);
        
        //GRD
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_FKEY, KeyValues.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_GREN, GradientRangeEnd.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_GRPT, RepeatMode.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_GRST, GradientRangeStart.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_IKEY, KeyParameters.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_INAM, ItemName.class);
        BLOCK_SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOK_GTEX_PNAM, ParameterName.class);
        
    }
    
        
    public void readInternal(DataInputStream ds) throws IOException {
        readSubChunks(ds, BLOCK_SUBCHUNKS);
    }
    
    public static final class BlockHeader extends LWOChunk{        
        public Chars ordinal;
        public void readInternal(DataInputStream ds) throws IOException {
            ordinal = LWOUtils.readChars(ds);
            readSubChunks(ds, HEADER_SUBCHUNKS);
        }
        public Chars toChars() {
            return Nodes.toChars(code.concat(' ').concat(ordinal), subs);
        }
    }
    public static final class Channel extends LWOChunk{        
        public Chars textureChannel;
        public void readInternal(DataInputStream ds) throws IOException {
            textureChannel = ds.readBlockZeroTerminatedChars(4, CharEncodings.US_ASCII);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(textureChannel);
        }
    }
    public static final class EnableState extends LWOChunk{        
        public int enable;
        public void readInternal(DataInputStream ds) throws IOException {
            enable = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(enable));
        }
    }
    public static final class Opacity extends LWOChunk{        
        public int type;
        public float opacity;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readUShort();
            opacity = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(opacity));
        }
    }
    public static final class DisplacementAxis extends LWOChunk{        
        public int displacementAxis;
        public void readInternal(DataInputStream ds) throws IOException {
            displacementAxis = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(displacementAxis));
        }
    }
    public static final class Negative extends LWOChunk{        
        public int enable;
        public void readInternal(DataInputStream ds) throws IOException {
            enable = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(enable));
        }
    }
    
    public static final class TextureMapping extends LWOChunk{
        public void readInternal(DataInputStream ds) throws IOException {
            readSubChunks(ds, TMAP_SUBCHUNKS);
        }
    }
    
    public static final class Center extends LWOChunk{        
        public Vector vector;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            vector = LWOUtils.readVec3(ds);
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(CObjects.toChars(vector));
        }
    }
    public static final class Rotation extends LWOChunk{        
        public Vector vector;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            vector = LWOUtils.readVec3(ds);
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(CObjects.toChars(vector));
        }
    }
    public static final class Size extends LWOChunk{        
        public Vector vector;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            vector = LWOUtils.readVec3(ds);
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(CObjects.toChars(vector));
        }
    }
    public static final class ObjectReference extends LWOChunk{        
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }
    public static final class CoordinateSystem extends LWOChunk{        
        public int type;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(type));
        }
    }
    public static final class Falloff extends LWOChunk{        
        public int type;
        public Vector vector;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readUShort();
            vector = LWOUtils.readVec3(ds);
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(type)).concat(' ').concat(vector.toChars()).concat(' ').concat(Int32.encode(type));
        }
    }
        
    public static final class Projection extends LWOChunk{        
        public int mode;
        public void readInternal(DataInputStream ds) throws IOException {
            mode = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(mode));
        }
    }
    public static final class MajorAxis extends LWOChunk{        
        public int textureAxis;
        public void readInternal(DataInputStream ds) throws IOException {
            textureAxis = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(textureAxis));
        }
    }
    public static final class ImageMap extends LWOChunk{        
        public int textureImage;
        public void readInternal(DataInputStream ds) throws IOException {
            textureImage = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(textureImage));
        }
    }
    public static final class ImageWrap extends LWOChunk{        
        public int widthWrap; 
        public int heightWrap;
        public void readInternal(DataInputStream ds) throws IOException {
            widthWrap = ds.readUShort();
            heightWrap = ds.readUShort();
        }
    }
    public static final class WrapHeight extends LWOChunk{        
        public float cycles;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            cycles = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
    }
    public static final class WrapWidth extends LWOChunk{        
        public float cycles;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            cycles = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
    }
    public static final class UVVertexMap extends LWOChunk{        
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }
    public static final class AntiAliasingStrenght extends LWOChunk{        
        public int flags;  
        public float strength;
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
            strength = ds.readFloat();
        }
    }
    public static final class PixelBlending extends LWOChunk{        
        public int flags;  
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
        }
    }
    public static final class StickyProjection extends LWOChunk{        
        public int onOff;  
        public float time;
        public void readInternal(DataInputStream ds) throws IOException {
            onOff = ds.readUShort();
            time = ds.readFloat();
        }
    }
    public static final class TextureAmplitude extends LWOChunk{        
        public float amplitude;
        public int envelope;  
        public void readInternal(DataInputStream ds) throws IOException {
            amplitude = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
    }
        
    public static final class Axis extends LWOChunk{        
        public int axis;
        public void readInternal(DataInputStream ds) throws IOException {
            axis = ds.readUShort();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Int32.encode(axis));
        }
    }
    public static final class BasicValue extends LWOChunk{        
        public float value;
        public void readInternal(DataInputStream ds) throws IOException {
            value = ds.readFloat();
        }
    }
    public static final class Function extends LWOChunk{        
        public Chars name;
        public byte[] data;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
            data = ds.readFully(new byte[(int)((offset+length)-ds.getByteOffset())]);
        }
    }
    
    public static final class ParameterName extends LWOChunk{        
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }
    public static final class ItemName extends LWOChunk{        
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            name = LWOUtils.readChars(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }
    public static final class KeyValues extends LWOChunk{        
        public float[] inOut;
        public void readInternal(DataInputStream ds) throws IOException {
            inOut = ds.readFloat((int)length/4);
        }
    }
    public static final class GradientRangeStart extends LWOChunk{        
        public float inputRange;
        public void readInternal(DataInputStream ds) throws IOException {
            inputRange = ds.readFloat();
        }
    }
    public static final class GradientRangeEnd extends LWOChunk{        
        public float inputRange;
        public void readInternal(DataInputStream ds) throws IOException {
            inputRange = ds.readFloat();
        }
    }
    public static final class RepeatMode extends LWOChunk{        
        public int mode;
        public void readInternal(DataInputStream ds) throws IOException {
            mode = ds.readUShort();
        }
    }
    public static final class KeyParameters extends LWOChunk{         
        public int[] inOut;
        public void readInternal(DataInputStream ds) throws IOException {
            inOut = ds.readUShort((int)length/2);
        }
    }
    
}
