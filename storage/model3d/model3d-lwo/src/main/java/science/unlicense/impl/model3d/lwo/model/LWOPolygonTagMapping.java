
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOPolygonTagMapping extends LWOChunk {
    
    
    public static final class Element{
        /** VX */
        public int poly;
        /** UInt 2 */
        public int tag;
    }
    
    /** ID4 */
    public Chars type;
    
    public Sequence elements = new ArraySequence();
    
    public void readInternal(DataInputStream ds) throws IOException {
        type = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
        
        while( ds.getByteOffset() < (offset+length)){
            final Element ele = new Element();
            ele.poly = LWOUtils.readVarInt(ds);
            ele.tag = ds.readUShort();
            elements.add(ele);
        }
    }
    
    public Chars toChars() {
        return super.toChars().concat(' ').concat(type);
    }
}
