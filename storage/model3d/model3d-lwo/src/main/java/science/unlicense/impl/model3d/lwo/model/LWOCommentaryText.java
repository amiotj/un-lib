
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOCommentaryText extends LWOChunk {
        
    /** String */
    public Chars cmt;
    
    public void readInternal(DataInputStream ds) throws IOException {
        cmt = LWOUtils.readChars(ds);
    }

    public Chars toChars() {
        return super.toChars().concat(' ').concat(cmt);
    }
    
}
