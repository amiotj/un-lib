

package science.unlicense.impl.model3d.lwo;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class LWOConstants {
    
    public static final byte[] SIGNATURE = new byte[]{'F','O','R','M'};

    public static final Chars CHUNK_FORM            = new Chars(new byte[]{'F','O','R','M'});
    public static final Chars CHUNK_LAYER           = new Chars(new byte[]{'L','A','Y','R'});
    public static final Chars CHUNK_POINT_LIST      = new Chars(new byte[]{'P','N','T','S'});
    public static final Chars CHUNK_VERTEX_MAPPING  = new Chars(new byte[]{'V','M','A','P'});
    public static final Chars CHUNK_POLYGON_LIST    = new Chars(new byte[]{'P','O','L','S'});
    public static final Chars CHUNK_TAGS            = new Chars(new byte[]{'T','A','G','S'});
    public static final Chars CHUNK_POLYGON_TAG_MAPPING = new Chars(new byte[]{'P','T','A','G'});
    public static final Chars CHUNK_DISC_VERTEX_MAPPING = new Chars(new byte[]{'V','M','A','D'});
    public static final Chars CHUNK_VERTEX_MAP_PARAM= new Chars(new byte[]{'V','M','P','A'});
    public static final Chars CHUNK_ENVELOPE        = new Chars(new byte[]{'E','N','V','L'});
    public static final Chars CHUNK_IMAGE           = new Chars(new byte[]{'C','L','I','P'});
    public static final Chars CHUNK_SURFACE         = new Chars(new byte[]{'S','U','R','F'});
    public static final Chars CHUNK_BOUNDINGBOX     = new Chars(new byte[]{'B','B','O','X'});
    public static final Chars CHUNK_DESCRIPTION     = new Chars(new byte[]{'D','E','S','C'});
    public static final Chars CHUNK_COMMENTARY      = new Chars(new byte[]{'T','E','X','T'});
    public static final Chars CHUNK_THUMBNAIL       = new Chars(new byte[]{'I','C','O','N'});
    
    public static final Chars SUBCHUNK_ENV_TYPE       = new Chars(new byte[]{'T','Y','P','E'});
    public static final Chars SUBCHUNK_ENV_PRE       = new Chars(new byte[]{'P','R','E',' '});
    public static final Chars SUBCHUNK_ENV_POST       = new Chars(new byte[]{'P','O','S','T'});
    public static final Chars SUBCHUNK_ENV_KEY       = new Chars(new byte[]{'K','E','Y',' '});
    public static final Chars SUBCHUNK_ENV_SPAN       = new Chars(new byte[]{'S','P','A','N'});
    public static final Chars SUBCHUNK_ENV_CHAN       = new Chars(new byte[]{'C','H','A','N'});
    public static final Chars SUBCHUNK_ENV_NAME       = new Chars(new byte[]{'N','A','M','E'});
    
    public static final Chars SUBCHUNK_CLIP_STIL        = new Chars(new byte[]{'S','T','I','L'});
    public static final Chars SUBCHUNK_CLIP_ISEQ        = new Chars(new byte[]{'I','S','E','Q'});
    public static final Chars SUBCHUNK_CLIP_ANIM        = new Chars(new byte[]{'A','N','I','M'});
    public static final Chars SUBCHUNK_CLIP_XREF        = new Chars(new byte[]{'X','R','E','F'});
    public static final Chars SUBCHUNK_CLIP_STCC        = new Chars(new byte[]{'S','T','C','C'});
    public static final Chars SUBCHUNK_CLIP_TIME        = new Chars(new byte[]{'T','I','M','E'});
    public static final Chars SUBCHUNK_CLIP_CSRGB       = new Chars(new byte[]{'C','L','R','S'});
    public static final Chars SUBCHUNK_CLIP_CSALPHA     = new Chars(new byte[]{'C','L','R','A'});
    public static final Chars SUBCHUNK_CLIP_FILT        = new Chars(new byte[]{'F','I','L','T'});
    public static final Chars SUBCHUNK_CLIP_DITH        = new Chars(new byte[]{'D','I','T','H'});
    public static final Chars SUBCHUNK_CLIP_CONST       = new Chars(new byte[]{'C','O','N','T'});
    public static final Chars SUBCHUNK_CLIP_BRIT        = new Chars(new byte[]{'B','R','I','T'});
    public static final Chars SUBCHUNK_CLIP_SATR        = new Chars(new byte[]{'S','A','T','R'});
    public static final Chars SUBCHUNK_CLIP_HUE         = new Chars(new byte[]{'H','U','E',' '});
    public static final Chars SUBCHUNK_CLIP_GAMM        = new Chars(new byte[]{'G','A','M','M'});
    public static final Chars SUBCHUNK_CLIP_NEGA        = new Chars(new byte[]{'N','E','G','A'});
    public static final Chars SUBCHUNK_CLIP_IFLT        = new Chars(new byte[]{'I','F','L','T'});
    public static final Chars SUBCHUNK_CLIP_PFLT        = new Chars(new byte[]{'P','F','L','T'});
    
    public static final Chars SUBCHUNK_SURF_COLOR        = new Chars(new byte[]{'C','O','L','R'});
    public static final Chars SUBCHUNK_SURF_DIFF        = new Chars(new byte[]{'D','I','F','F'});
    public static final Chars SUBCHUNK_SURF_LUMI        = new Chars(new byte[]{'L','U','M','I'});
    public static final Chars SUBCHUNK_SURF_SPEC        = new Chars(new byte[]{'S','P','E','C'});
    public static final Chars SUBCHUNK_SURF_REFL        = new Chars(new byte[]{'R','E','F','L'});
    public static final Chars SUBCHUNK_SURF_TRAN        = new Chars(new byte[]{'T','R','A','N'});
    public static final Chars SUBCHUNK_SURF_TRNL        = new Chars(new byte[]{'T','R','N','L'});
    public static final Chars SUBCHUNK_SURF_GLOS        = new Chars(new byte[]{'G','L','O','S'});
    public static final Chars SUBCHUNK_SURF_SHRP        = new Chars(new byte[]{'S','H','R','P'});
    public static final Chars SUBCHUNK_SURF_BUMP        = new Chars(new byte[]{'B','U','M','P'});
    public static final Chars SUBCHUNK_SURF_SIDE        = new Chars(new byte[]{'S','I','D','E'});
    public static final Chars SUBCHUNK_SURF_SMAN        = new Chars(new byte[]{'S','M','A','N'});
    public static final Chars SUBCHUNK_SURF_RFOP        = new Chars(new byte[]{'R','F','O','P'});
    public static final Chars SUBCHUNK_SURF_RIMG        = new Chars(new byte[]{'R','I','M','G'});
    public static final Chars SUBCHUNK_SURF_RSAN        = new Chars(new byte[]{'R','S','A','N'});
    public static final Chars SUBCHUNK_SURF_RBLR        = new Chars(new byte[]{'R','B','L','R'});
    public static final Chars SUBCHUNK_SURF_RIND        = new Chars(new byte[]{'R','I','N','D'});
    public static final Chars SUBCHUNK_SURF_TROP        = new Chars(new byte[]{'T','R','O','P'});
    public static final Chars SUBCHUNK_SURF_TIMG        = new Chars(new byte[]{'T','I','M','G'});
    public static final Chars SUBCHUNK_SURF_TBLR        = new Chars(new byte[]{'T','B','L','R'});
    public static final Chars SUBCHUNK_SURF_CLRH        = new Chars(new byte[]{'C','L','R','H'});
    public static final Chars SUBCHUNK_SURF_CLRF        = new Chars(new byte[]{'C','L','R','F'});
    public static final Chars SUBCHUNK_SURF_ADTR        = new Chars(new byte[]{'A','D','T','R'});
    public static final Chars SUBCHUNK_SURF_GLOW        = new Chars(new byte[]{'G','L','O','W'});
    public static final Chars SUBCHUNK_SURF_LINE        = new Chars(new byte[]{'L','I','N','E'});
    public static final Chars SUBCHUNK_SURF_ALPH        = new Chars(new byte[]{'A','L','P','H'});
    public static final Chars SUBCHUNK_SURF_VCOL        = new Chars(new byte[]{'V','C','O','L'});
    public static final Chars SUBCHUNK_SURF_BLOCK        = new Chars(new byte[]{'B','L','O','K'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_IMAP        = new Chars(new byte[]{'I','M','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_PROC        = new Chars(new byte[]{'P','R','O','C'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_GRAD        = new Chars(new byte[]{'G','R','A','D'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_SHDR        = new Chars(new byte[]{'S','H','D','R'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_CHAN        = new Chars(new byte[]{'C','H','A','N'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_ENAB        = new Chars(new byte[]{'E','N','A','B'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_OPAC        = new Chars(new byte[]{'O','P','A','C'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_AXIS        = new Chars(new byte[]{'A','X','I','S'});
    public static final Chars SUBCHUNK_SURF_BLOK_HEADER_NEGA        = new Chars(new byte[]{'N','E','G','A'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP             = new Chars(new byte[]{'T','M','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_CNTR        = new Chars(new byte[]{'C','N','T','R'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_SIZE        = new Chars(new byte[]{'S','I','Z','E'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_ROTA        = new Chars(new byte[]{'R','O','T','A'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_OREF        = new Chars(new byte[]{'O','R','E','F'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_FALL        = new Chars(new byte[]{'F','A','L','L'});
    public static final Chars SUBCHUNK_SURF_BLOK_TMAP_CSYS        = new Chars(new byte[]{'C','S','Y','S'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_PROJ        = new Chars(new byte[]{'P','R','O','J'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_AXIS        = new Chars(new byte[]{'A','X','I','S'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_IMAG        = new Chars(new byte[]{'I','M','A','G'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_WRAP        = new Chars(new byte[]{'W','R','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_WRPW        = new Chars(new byte[]{'W','R','P','W'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_WRPH        = new Chars(new byte[]{'W','R','P','H'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_VMAP        = new Chars(new byte[]{'V','M','A','P'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_AAST        = new Chars(new byte[]{'A','A','S','T'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_PIXB        = new Chars(new byte[]{'P','I','X','B'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_STCK        = new Chars(new byte[]{'S','T','C','K'});
    public static final Chars SUBCHUNK_SURF_BLOK_IMAP_TAMP        = new Chars(new byte[]{'T','A','M','P'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_PTEX_AXIS        = new Chars(new byte[]{'A','X','I','S'});
    public static final Chars SUBCHUNK_SURF_BLOK_PTEX_VALU        = new Chars(new byte[]{'V','A','L','U'});
    public static final Chars SUBCHUNK_SURF_BLOK_PTEX_FUNC        = new Chars(new byte[]{'F','U','N','C'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_PNAM        = new Chars(new byte[]{'P','N','A','M'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_INAM        = new Chars(new byte[]{'I','N','A','M'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_GRST        = new Chars(new byte[]{'G','R','S','T'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_GREN        = new Chars(new byte[]{'G','R','E','N'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_GRPT        = new Chars(new byte[]{'G','R','P','T'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_FKEY        = new Chars(new byte[]{'F','K','E','Y'});
    public static final Chars SUBCHUNK_SURF_BLOK_GTEX_IKEY        = new Chars(new byte[]{'I','K','E','Y'});
    
    public static final Chars SUBCHUNK_SURF_BLOK_SHDR        = new Chars(new byte[]{'S','H','D','R'});
    public static final Chars SUBCHUNK_SURF_BLOK_SHDR_FUNC        = new Chars(new byte[]{'F','U','N','C'});
    
    private LWOConstants() {
    }
    
}
