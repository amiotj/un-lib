
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.number.Float64;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.impl.model3d.lwo.LWOConstants;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOSurfaceDefinition extends LWOChunk {
    
    
    private static final Dictionary SUBCHUNKS = new HashDictionary();
    static {
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_COLOR, BaseColor.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_DIFF, ShadingDiffuse.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_LUMI, ShadingLuminosity.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_SPEC, ShadingSpecular.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_REFL, ShadingReflection.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_TRAN, ShadingTransparency.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_TRNL, ShadingTranslucency.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_GLOS, SpecularGlossiness.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_SHRP, DiffuseSharpness.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BUMP, BumpIntensity.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_SIDE, PolygonSidedness.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_SMAN, MaxSmoothingAngle.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_RFOP, ReflectionOptions.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_RIMG, ReflectionMapImage.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_RSAN, ReflectionMapImageSeamAngle.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_RBLR, ReflectionBlurring.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_RIND, RefractiveIndex.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_TROP, TransparencyOptions.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_TIMG, RefractionMapImage.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_TBLR, RefractionBlurring.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_CLRH, ColorHighlights.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_CLRF, ColorFilter.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_ADTR, AdditiveTransparency.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_GLOW, GlowEffect.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_LINE, RenderOutlines.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_ALPH, AlphaMode.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_VCOL, VertexColorMap.class);
        SUBCHUNKS.add(LWOConstants.SUBCHUNK_SURF_BLOCK, LWOSurfaceBlock.class);
    }
    
    /** String */
    public Chars name;
    /** String */
    public Chars source;
        
    public void readInternal(DataInputStream ds) throws IOException {
        name = LWOUtils.readChars(ds);
        source = LWOUtils.readChars(ds);
        readSubChunks(ds, SUBCHUNKS);
    }

    public Chars toChars() {
        return Nodes.toChars(code.concat(' ').concat(name).concat(' ').concat(source), subs);
    }
    
    public static final class BaseColor extends LWOChunk{        
        public Color color;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            color = LWOUtils.readColor(ds);
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(color.toChars());
        }
    }
    public static final class ShadingDiffuse extends LWOChunk{        
        public float intensity;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(intensity));
        }
    }
    public static final class ShadingLuminosity extends LWOChunk{        
        public float intensity;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(intensity));
        }
    }
    public static final class ShadingSpecular extends LWOChunk{        
        public float intensity;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(intensity));
        }
    }
    public static final class ShadingReflection extends LWOChunk{        
        public float intensity;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(intensity));
        }
    }
    public static final class ShadingTransparency extends LWOChunk{        
        public float intensity;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(intensity));
        }
    }
    public static final class ShadingTranslucency extends LWOChunk{        
        public float intensity;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(intensity));
        }
    }
    public static final class SpecularGlossiness extends LWOChunk{        
        public float glossiness;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            glossiness = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(glossiness));
        }
    }
    public static final class DiffuseSharpness extends LWOChunk{        
        public float sharpness;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            sharpness = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(sharpness));
        }
    }
    public static final class BumpIntensity extends LWOChunk{        
        public float strength;
        public int envelopeIndex;
        public void readInternal(DataInputStream ds) throws IOException {
            strength = ds.readFloat();
            envelopeIndex = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(strength));
        }
    }
    public static final class PolygonSidedness extends LWOChunk{        
        public int sidedness;
        public void readInternal(DataInputStream ds) throws IOException {
            sidedness = ds.readUShort();
        }
    }
    public static final class MaxSmoothingAngle extends LWOChunk{        
        public float maxSmoothingAngle;
        public void readInternal(DataInputStream ds) throws IOException {
            maxSmoothingAngle = ds.readFloat();
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(maxSmoothingAngle));
        }
    }
    public static final class ReflectionOptions extends LWOChunk{        
        public int options;
        public void readInternal(DataInputStream ds) throws IOException {
            options = ds.readUShort();
        }
    }
    public static final class ReflectionMapImage extends LWOChunk{        
        public int image;
        public void readInternal(DataInputStream ds) throws IOException {
            image = LWOUtils.readVarInt(ds);
        }
    }
    public static final class ReflectionMapImageSeamAngle extends LWOChunk{        
        public float seamAngle;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            seamAngle = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
    }
    public static final class ReflectionBlurring extends LWOChunk{        
        public float percent;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            percent = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(percent));
        }
    }
    public static final class RefractiveIndex extends LWOChunk{        
        public float refractiveIndex;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            refractiveIndex = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
    }
    public static final class TransparencyOptions extends LWOChunk{        
        public int options;
        public void readInternal(DataInputStream ds) throws IOException {
            options = ds.readUShort();
        }
    }
    public static final class RefractionMapImage extends LWOChunk{        
        public int image;
        public void readInternal(DataInputStream ds) throws IOException {
            image = LWOUtils.readVarInt(ds);
        }
    }
    public static final class RefractionBlurring extends LWOChunk{        
        public float percent;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            percent = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(percent));
        }
    }
    public static final class ColorHighlights extends LWOChunk{        
        public float highlights;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            highlights = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(highlights));
        }
    }
    public static final class ColorFilter extends LWOChunk{        
        public float colorFilter;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            colorFilter = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(colorFilter));
        }
    }
    public static final class AdditiveTransparency extends LWOChunk{        
        public float additive;
        public int envelope;
        public void readInternal(DataInputStream ds) throws IOException {
            additive = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(Float64.encode(additive));
        }
    }
    public static final class GlowEffect extends LWOChunk{        
        public int type;
        public float intensity;
        public int intensityEnvelope;
        public float size;
        public int sizeEnvelope;
        public void readInternal(DataInputStream ds) throws IOException {
            type = ds.readUByte();
            intensity = ds.readFloat();
            intensityEnvelope = LWOUtils.readVarInt(ds);
            size = ds.readFloat();
            sizeEnvelope = LWOUtils.readVarInt(ds);
        }
    }
    public static final class RenderOutlines extends LWOChunk{        
        public int flags;
        public float size;
        public int sizeEnvelope;
        public Color color;
        public int colorEnvelope;
        public void readInternal(DataInputStream ds) throws IOException {
            flags = ds.readUShort();
            if(length>2){
                size = ds.readFloat();
                sizeEnvelope = LWOUtils.readVarInt(ds);
            }else if(length>8){
                color = LWOUtils.readColor(ds);
                colorEnvelope = LWOUtils.readVarInt(ds);
            }
        }
    }
    public static final class AlphaMode extends LWOChunk{        
        public int mode;
        public float value;
        public void readInternal(DataInputStream ds) throws IOException {
            mode = ds.readUShort();
            value = ds.readFloat();
        }
    }
    public static final class VertexColorMap extends LWOChunk{        
        public float intensity;
        public int envelope;
        public Chars vmapType;
        public Chars name;
        public void readInternal(DataInputStream ds) throws IOException {
            intensity = ds.readFloat();
            envelope = LWOUtils.readVarInt(ds);
            vmapType = ds.readBlockZeroTerminatedChars(4, CharEncodings.US_ASCII);
            name = LWOUtils.readChars(ds);
        }
        public Chars toChars() {
            return super.toChars().concat(' ').concat(name);
        }
    }
 
    
}
