

package science.unlicense.impl.model3d.lwo;

import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.impl.model3d.lwo.model.LWOBoundingBox;
import science.unlicense.impl.model3d.lwo.model.LWOChunk;
import science.unlicense.impl.model3d.lwo.model.LWOImage;
import science.unlicense.impl.model3d.lwo.model.LWOLayer;
import science.unlicense.impl.model3d.lwo.model.LWOPointList;
import science.unlicense.impl.model3d.lwo.model.LWOPolygonList;
import science.unlicense.impl.model3d.lwo.model.LWOPolygonTagMapping;
import science.unlicense.impl.model3d.lwo.model.LWOSurfaceBlock;
import science.unlicense.impl.model3d.lwo.model.LWOSurfaceDefinition;
import science.unlicense.impl.model3d.lwo.model.LWOTags;
import science.unlicense.impl.model3d.lwo.model.LWOVertexMapping;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * @author Johann Sorel
 */
public class LWOStore extends AbstractModel3DStore{

    private static class WorkLayer {        
        private LWOLayer layer;
        private LWOBoundingBox bbox;
        private LWOPointList points;
        private LWOPolygonList polygons;
        private LWOVertexMapping uvmapping;
        private LWOPolygonTagMapping tagmapping;
        private final Sequence others = new ArraySequence();
    }
    
    private LWOTags tags = null;
    private final Sequence layers = new ArraySequence();
    private final Sequence images = new ArraySequence();
    private final Dictionary surfaces = new HashDictionary();
    private final Path rootPath;
    
    public LWOStore(Object input) {
        super(LWOFormat.INSTANCE,input);
        rootPath = ((Path)input).getParent();
    }

    public Collection getElements() throws StoreException {
        try {
            read();
        } catch (IOException ex) {
            throw new StoreException(ex);
        } catch (StoreException ex) {
            throw ex;
        }
        
        final Sequence result = new ArraySequence();
        result.add(rebuild());
        
        return result;
    }
    
    /**
     * Decode all chunks.
     * 
     * @throws IOException 
     */
    private void read() throws IOException, StoreException{
        if(tags!=null) return;
        
        final ByteInputStream bi = getSourceAsInputStream();
        final DataInputStream ds = new DataInputStream(bi);
        
        final Chars form = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
        if(!LWOConstants.CHUNK_FORM.equals(form)){
            throw new IOException("Stream is not an LWO");
        }
        final long length = ds.readUInt();
        
        final Chars lwo2 = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
        if(!new Chars("LWO2").equals(lwo2)){
            throw new IOException("Stream is not an LWO");
        }
        
        //while decoding, we separate layer chunks
        WorkLayer layer = null;        
        while(ds.getByteOffset() < length){
            final Chars ckName = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
            final long ckLength = ds.readUInt();
            final LWOChunk chunk = LWOUtils.getChunk(ckName);
            chunk.code = ckName;
            chunk.length = ckLength;
            chunk.offset = ds.getByteOffset();
            chunk.readInternal(ds);
            
            if(chunk instanceof LWOTags){
                tags = (LWOTags) chunk;
            }else if(chunk instanceof LWOLayer){
                layer = new WorkLayer();
                layers.add(layer);
                layer.layer = (LWOLayer) chunk;
            }else if(chunk instanceof LWOBoundingBox){
                layer.bbox = (LWOBoundingBox) chunk;
            }else if(chunk instanceof LWOPointList){
                layer.points = (LWOPointList) chunk;
            }else if(chunk instanceof LWOPolygonList){
                layer.polygons = (LWOPolygonList) chunk;
            }else if(chunk instanceof LWOVertexMapping){
                layer.uvmapping = (LWOVertexMapping) chunk;
            }else if(chunk instanceof LWOPolygonTagMapping){
                layer.tagmapping = (LWOPolygonTagMapping) chunk;
            }else if(chunk instanceof LWOSurfaceDefinition){
                final LWOSurfaceDefinition suf = (LWOSurfaceDefinition) chunk;
                surfaces.add(suf.name,suf);
            }else if(chunk instanceof LWOImage){
                images.add(chunk);
            }else{
                layer.others.add(chunk);
            }
        }
    }
    
    /**
     * Rebuild mesh from chunks.
     * @return MultipartMesh
     */
    private MultipartMesh rebuild(){
        final MultipartMesh root = new MultipartMesh();
        
        //rebuild a mesh from each layer
        for(int i=0,n=layers.getSize();i<n;i++){
            final WorkLayer layer = (WorkLayer) layers.get(i);
            root.getChildren().add(rebuildMesh(layer));
        }
        
        return root;
    }
    
    private Mesh rebuildMesh(final WorkLayer layer){
        
        final Mesh mesh = new Mesh();
        mesh.setName(layer.layer.name);
        
        //rebuild shell
        final Shell shell = new Shell();
        mesh.setShape(shell);  
        
        final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(layer.points.points.length*3).cursorFloat();
        for(int i=0;i<layer.points.points.length;i++){
            vertices.write(layer.points.points[i].toArrayFloat());
        }
        
        final IntSequence ibuff = new IntSequence();
        for(int i=0,n=layer.polygons.elements.getSize();i<n;i++){
            final LWOPolygonList.Element ele = (LWOPolygonList.Element) layer.polygons.elements.get(i);
            if(ele.vertCount==3){
                ibuff.put(ele.indices);
            }else{
                for(int k=2;k<ele.indices.length;k++){
                    ibuff.put(ele.indices[0]);
                    ibuff.put(ele.indices[1]);
                    ibuff.put(ele.indices[k]);
                }
            }
        }
        final IBO ibo = new IBO(ibuff.toArrayInt(), 3);
        shell.setIndexes(ibo, IndexRange.TRIANGLES(0, ibo.getCapacity()));
        shell.setVertices(new VBO(vertices.getBuffer(), 3));
        shell.calculateNormals();        
        mesh.setShape(shell);
        
        //rebuild uv mapping
        if(layer.uvmapping!=null){
            final Sequence seq = layer.uvmapping.elements;
            final FloatCursor uvs = DefaultBufferFactory.INSTANCE.createFloat(seq.getSize()*2).cursorFloat();
            for(int i=0,n=seq.getSize();i<n;i++){
                final LWOVertexMapping.Element ele = (LWOVertexMapping.Element) seq.get(i);
                uvs.setPosition(ele.index*2);
                uvs.write(ele.values[0]);
                uvs.write(1-ele.values[1]);
            }
            shell.setUVs(new VBO(uvs.getBuffer(), 2));
        }
        
        //rebuild texture
        //TODO this only handle material with uv image
        surfif:
        if(layer.tagmapping!=null){
            final LWOPolygonTagMapping.Element ele = (LWOPolygonTagMapping.Element) layer.tagmapping.elements.get(0);
            final Chars surfaceName = (Chars) tags.tags.get(ele.tag);
            final LWOSurfaceDefinition surface = (LWOSurfaceDefinition) surfaces.getValue(surfaceName);
            if(surface==null) break surfif;
            final LWOSurfaceBlock block = (LWOSurfaceBlock) surface.getSubChunk(LWOConstants.SUBCHUNK_SURF_BLOCK);
            if(block==null) break surfif;
            final LWOSurfaceBlock.ImageMap imag = (LWOSurfaceBlock.ImageMap) block.getSubChunk(LWOConstants.SUBCHUNK_SURF_BLOK_IMAP_IMAG);
            if(imag==null) break surfif;
            final LWOImage image = (LWOImage) images.get(imag.textureImage-1);
            if(image==null) break surfif;
            final LWOImage.StilImage stil = (LWOImage.StilImage) image.getSubChunk(LWOConstants.SUBCHUNK_CLIP_STIL);
            if(stil==null) break surfif;
            Chars textureName = stil.name;
            Chars[] parts = textureName.split('/');
            textureName = parts[parts.length-1];
            final Path texturePath = rootPath.resolve(textureName);
            try {
                final Image img = Images.read(texturePath);
                mesh.getMaterial().putOrReplaceLayer(new Layer(new UVMapping(new Texture2D(img))));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        mesh.getShape().calculateBBox();
        return mesh;
    }
    
    
}
