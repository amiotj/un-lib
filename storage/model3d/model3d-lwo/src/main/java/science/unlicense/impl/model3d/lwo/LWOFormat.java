

package science.unlicense.impl.model3d.lwo;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Lightwave object format.
 * 
 * Docs :
 * http://content.gpwiki.org/index.php/LWO
 * http://muskrat.middlebury.edu/lt/cr/ongoing/sciviz/LightWave/SDK/docs/filefmts/lwo2.html
 * 
 * @author Johann Sorel
 */
public class LWOFormat extends AbstractModel3DFormat{

    public static final LWOFormat INSTANCE = new LWOFormat();

    private LWOFormat() {
        super(new Chars("LWO"),
              new Chars("LWO"),
              new Chars("Lightwave Object"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("lwo")
              },
              new byte[][]{LWOConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new LWOStore(input);
    }
    
}
