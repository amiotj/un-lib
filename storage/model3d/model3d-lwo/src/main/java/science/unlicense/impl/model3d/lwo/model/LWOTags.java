
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOTags extends LWOChunk {

    public final Sequence tags = new ArraySequence();
    
    public void readInternal(DataInputStream ds) throws IOException {
        while(ds.getByteOffset() < (offset+length) ){
            tags.add(LWOUtils.readChars(ds));
        }
    }
    
}
