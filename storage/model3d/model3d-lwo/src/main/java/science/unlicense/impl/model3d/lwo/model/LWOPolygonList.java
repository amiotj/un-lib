
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOPolygonList extends LWOChunk {
    
    public static final class Element{
        public int flags;
        public int vertCount;
        public int[] indices;
    }
    
    /** ID4 */
    public Chars type;
    
    public Sequence elements = new ArraySequence();
    
    public void readInternal(DataInputStream ds) throws IOException {
        type = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
        
        while( ds.getByteOffset() < (offset+length)){
            final Element ele = new Element();
            ele.flags = ds.readBits(6);
            ele.vertCount = ds.readBits(10);
            ele.indices = new int[ele.vertCount];
            for(int i=0;i<ele.vertCount;i++){
                ele.indices[i] = LWOUtils.readVarInt(ds);
            }
            elements.add(ele);
        }
    }
    
    public Chars toChars() {
        return super.toChars().concat(' ').concat(type);
    }
}
