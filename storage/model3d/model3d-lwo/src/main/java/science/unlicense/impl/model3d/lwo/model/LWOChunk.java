

package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOChunk extends CObject{
    
    public Chars code;
    public long length;
    /** file data offset */
    public long offset;

    /**
     * Sub chunks
     */
    public final Sequence subs = new ArraySequence();
    
    public LWOChunk() {
    }

    public LWOChunk getSubChunk(Chars code){
        for(int i=0,n=subs.getSize();i<n;i++){
            final LWOChunk chunk = (LWOChunk) subs.get(i);
            if(chunk.code.equals(code)) return chunk;
        }
        return null;
    }
    
    public final void read(DataInputStream ds) throws IOException{
        readInternal(ds);
        fitOddSize(ds);
    }
    
    public void readInternal(DataInputStream ds) throws IOException{
        ds.skipFully(length);
    }
    
    private void fitOddSize(DataInputStream ds) throws IOException{
        if(length%2!=0){
            ds.skipFully(1);
        }
    }
    
    protected void readSubChunks(DataInputStream ds, Dictionary chunkMap) throws IOException{
        while(ds.getByteOffset() < (offset+length) ){
            final Chars ckName = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
            final long ckLength = ds.readUShort();
            final LWOChunk chunk = LWOUtils.getChunk(ckName,chunkMap);
            chunk.code = ckName;
            chunk.length = ckLength;
            chunk.offset = ds.getByteOffset();
            chunk.readInternal(ds);
            subs.add(chunk);
        }
    }
    
    public void write(DataOutputStream ds) throws IOException{
        
    }

    public Chars toChars() {
        return Nodes.toChars(code, subs);
    }
    
}
