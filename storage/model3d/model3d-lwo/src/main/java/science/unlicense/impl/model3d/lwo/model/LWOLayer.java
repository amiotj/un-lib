
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOLayer extends LWOChunk {
    
    /** UInt2 */
    public int number;
    /** UInt2 */
    public int flag;
    /** Vec12 */
    public Vector pivot;
    /** String */
    public Chars name;
    /** UInt2 */
    public int parent;
    
    public void readInternal(DataInputStream ds) throws IOException {
        number = ds.readUShort();
        flag = ds.readUShort();
        pivot = LWOUtils.readVec3(ds);
        name = LWOUtils.readChars(ds);
        if(offset+length < ds.getByteOffset() ){
            parent = ds.readUShort();
        }else{
            parent = -1;
        }
    }
    
    public Chars toChars() {
        return super.toChars().concat(' ').concat(name);
    }
}
