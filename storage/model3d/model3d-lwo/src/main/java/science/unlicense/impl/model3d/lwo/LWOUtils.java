

package science.unlicense.impl.model3d.lwo;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.color.Color;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;
import static science.unlicense.impl.model3d.lwo.LWOConstants.*;
import science.unlicense.impl.model3d.lwo.model.LWOBoundingBox;
import science.unlicense.impl.model3d.lwo.model.LWOChunk;
import science.unlicense.impl.model3d.lwo.model.LWOCommentaryText;
import science.unlicense.impl.model3d.lwo.model.LWODescriptionLine;
import science.unlicense.impl.model3d.lwo.model.LWODiscVertexMapping;
import science.unlicense.impl.model3d.lwo.model.LWOEnvelopeDefinition;
import science.unlicense.impl.model3d.lwo.model.LWOImage;
import science.unlicense.impl.model3d.lwo.model.LWOLayer;
import science.unlicense.impl.model3d.lwo.model.LWOPointList;
import science.unlicense.impl.model3d.lwo.model.LWOPolygonList;
import science.unlicense.impl.model3d.lwo.model.LWOPolygonTagMapping;
import science.unlicense.impl.model3d.lwo.model.LWOSurfaceDefinition;
import science.unlicense.impl.model3d.lwo.model.LWOTags;
import science.unlicense.impl.model3d.lwo.model.LWOThumbnail;
import science.unlicense.impl.model3d.lwo.model.LWOVertexMapParameter;
import science.unlicense.impl.model3d.lwo.model.LWOVertexMapping;

/**
 *
 * @author Johann Sorel
 */
public final class LWOUtils {

    private static final Dictionary CHUNKS = new HashDictionary();
    static {
        CHUNKS.add(CHUNK_LAYER,                 LWOLayer.class);
        CHUNKS.add(CHUNK_POINT_LIST,            LWOPointList.class);
        CHUNKS.add(CHUNK_VERTEX_MAPPING,        LWOVertexMapping.class);
        CHUNKS.add(CHUNK_POLYGON_LIST,          LWOPolygonList.class);
        CHUNKS.add(CHUNK_TAGS,                  LWOTags.class);
        CHUNKS.add(CHUNK_POLYGON_TAG_MAPPING,   LWOPolygonTagMapping.class);
        CHUNKS.add(CHUNK_DISC_VERTEX_MAPPING,   LWODiscVertexMapping.class);
        CHUNKS.add(CHUNK_VERTEX_MAP_PARAM,      LWOVertexMapParameter.class);
        CHUNKS.add(CHUNK_ENVELOPE,              LWOEnvelopeDefinition.class);
        CHUNKS.add(CHUNK_IMAGE,                 LWOImage.class);
        CHUNKS.add(CHUNK_SURFACE,               LWOSurfaceDefinition.class);
        CHUNKS.add(CHUNK_BOUNDINGBOX,           LWOBoundingBox.class);
        CHUNKS.add(CHUNK_DESCRIPTION,           LWODescriptionLine.class);
        CHUNKS.add(CHUNK_COMMENTARY,            LWOCommentaryText.class);
        CHUNKS.add(CHUNK_THUMBNAIL,             LWOThumbnail.class);
    }
    
    private LWOUtils() {}
    
    public static int readVarInt(DataInputStream ds) throws IOException{
        final int b1 = ds.readUByte();
        if(b1==0xFF){
            //4 bytes int
            final int b2 = ds.readUByte();
            final int b3 = ds.readUByte();
            final int b4 = ds.readUByte();
            return (b2<<16) +  (b3<<8) + b4;
        }else{
            //2 bytes int
            final int b2 = ds.readUByte();
            return (b1<<8) + b2;
        }
    }
    
    public static Color readColor(DataInputStream ds) throws IOException{
        final float r = Maths.clamp(ds.readFloat(), 0, 1);
        final float g = Maths.clamp(ds.readFloat(), 0, 1);
        final float b = Maths.clamp(ds.readFloat(), 0, 1);
        return new Color(r, g, b);
    }
    
    public static Vector readVec3(DataInputStream ds) throws IOException{
        return new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
    }
    
    public static Chars readChars(DataInputStream ds) throws IOException{
        final Chars cs = ds.readZeroTerminatedChars(0, CharEncodings.US_ASCII);
        if(ds.getByteOffset()%2!=0){
            ds.skipFully(1);
        }
        return cs;
    }
    
    public static LWOChunk getChunk(Chars name) throws IOException{
        return getChunk(name, CHUNKS);
    }
    
    public static LWOChunk getChunk(Chars name, Dictionary chunkMap) throws IOException {
        Class c = (Class) chunkMap.getValue(name);
        if(c==null){
            System.out.println("Unknwoned chunk "+ name);
        }
        if(c==null) c = LWOChunk.class;
        try {
            return (LWOChunk) c.newInstance();
        } catch (InstantiationException ex) {
            throw new IOException(ex);
        } catch (IllegalAccessException ex) {
            throw new IOException(ex);
        }
    }
     
}
