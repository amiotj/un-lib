
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOBoundingBox extends LWOChunk {
    
    /** Vec12 */
    public Vector upper;
    /** Vec12 */
    public Vector lower;
    
    public void readInternal(DataInputStream ds) throws IOException {
        upper = LWOUtils.readVec3(ds);
        lower = LWOUtils.readVec3(ds);
    }

    public Chars toChars() {
        return super.toChars().concat(' ').concat(lower.toChars()).concat(' ').concat(upper.toChars());
    }
    
}
