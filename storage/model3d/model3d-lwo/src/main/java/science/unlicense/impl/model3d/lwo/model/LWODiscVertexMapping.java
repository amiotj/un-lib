
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWODiscVertexMapping extends LWOChunk {
    
    public static final class Element{
        public int vertice;
        public int poly;
        public float[] values;
    }
    
    /** ID4 */
    public Chars type;
    /** UInt2 */
    public int dimension;
    /** String */
    public Chars name;

    public Sequence elements = new ArraySequence();
    
    public void readInternal(DataInputStream ds) throws IOException {
        type = ds.readZeroTerminatedChars(4, CharEncodings.US_ASCII);
        dimension = ds.readUShort();
        name = LWOUtils.readChars(ds);
        
        while( ds.getByteOffset() < (offset+length)){
            final Element ele = new Element();
            ele.vertice = LWOUtils.readVarInt(ds);
            ele.poly = LWOUtils.readVarInt(ds);
            ele.values = ds.readFloat(dimension);
            elements.add(ele);
        }
    }

    public Chars toChars() {
        return super.toChars().concat(' ').concat(type).concat(' ').concat(name);
    }
    
}
