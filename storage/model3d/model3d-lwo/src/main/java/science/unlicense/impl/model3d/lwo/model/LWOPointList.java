
package science.unlicense.impl.model3d.lwo.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.lwo.LWOUtils;

/**
 *
 * @author Johann Sorel
 */
public class LWOPointList extends LWOChunk {
    
    public Vector[] points;
    
    public void readInternal(DataInputStream ds) throws IOException {
        points = new Vector[(int)(length/12)];
        for(int i=0;i<points.length;i++){
            points[i] = LWOUtils.readVec3(ds);
        }
    }
    
}
