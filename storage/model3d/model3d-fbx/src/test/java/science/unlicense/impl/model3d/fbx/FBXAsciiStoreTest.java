
package science.unlicense.impl.model3d.fbx;

import science.unlicense.impl.model3d.fbx.FBXStore;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.model3d.fbx.ascii.FBXAsciiStore;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class FBXAsciiStoreTest {

    @Ignore
    @Test
    public void readCubeTest() throws StoreException{
     
        final Path path = Paths.resolve(new Chars("todo generate an fbx file"));
        final FBXStore store = new FBXAsciiStore(path);
        
        store.getElements();
        
    }
    
}
