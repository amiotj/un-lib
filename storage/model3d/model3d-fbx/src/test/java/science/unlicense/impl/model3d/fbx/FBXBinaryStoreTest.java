
package science.unlicense.impl.model3d.fbx;

import science.unlicense.impl.model3d.fbx.FBXStore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.model3d.fbx.binary.FBXBinaryStore;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class FBXBinaryStoreTest {

    @Test
    public void readCubeTest() throws StoreException{
     
        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/fbx/cube.fbx"));
        final FBXStore store = new FBXBinaryStore(path);
        
        store.getElements();
        
    }
    
}
