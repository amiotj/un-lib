
package science.unlicense.impl.model3d.fbx.ascii;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.impl.model3d.fbx.FBXConstants;
/**
 *
 * Some doc :
 * http://code.blender.org/index.php/2013/08/fbx-binary-file-format-specification/
 * http://wiki.blender.org/index.php/User:Mont29/Foundation/FBX_File_Structure
 * 
 * @author Johann Sorel
 */
public class FBXAsciiFormat extends AbstractModel3DFormat{

    public static final FBXAsciiFormat INSTANCE = new FBXAsciiFormat();

    private FBXAsciiFormat() {
        super(new Chars("FBX-ASCII"),
              new Chars("Fbx ascii"),
              new Chars("FBX Ascii"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("fbx")
              },
              new byte[][]{FBXConstants.SIGNATURE_ASCII});
    }

    public Model3DStore open(Object input) throws IOException {
        return new FBXAsciiStore(input);
    }

}
