
package science.unlicense.impl.model3d.fbx;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.model3d.Model3DFormat;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MeshUtilities;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public abstract class FBXStore extends AbstractModel3DStore{
    
    private NamedNode root;
    
    public FBXStore(Model3DFormat format, Object input) {
        super(format,input);
    }

    @Override
    public Collection getElements() throws StoreException {
        try {
            root = read();
            return convert(root);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }
    }

    protected abstract NamedNode read() throws IOException;
    
    private Collection convert(NamedNode root) throws StoreException{
        final Sequence models = new ArraySequence();
                
        final NamedNode objects = root.getNamedChild(FBXConstants.NODE_OBJECTS);
        final Sequence modelNodes = objects.getNamedChildren(FBXConstants.NODE_MODEL);
        for(int i=0,n=modelNodes.getSize();i<n;i++){
            final Mesh mesh = convertModel((NamedNode) modelNodes.get(i));
            if(mesh!=null){
                models.add(mesh);
            }
        }
        
        return models;
    }
    
    private Mesh convertModel(NamedNode model) throws StoreException{
        
        final NamedNode verticesNode = model.getNamedChild(FBXConstants.NODE_MODEL_VERTICES);
        final NamedNode indexNode = model.getNamedChild(FBXConstants.NODE_MODEL_INDEX);
        
        if(verticesNode==null || indexNode==null){
            return null;
        }
        
        //convert vertices
        final Object[] vs = (Object[]) verticesNode.getValue();
        final float[] vertices = new float[vs.length];
        for(int i=0;i<vertices.length;i++){
            vertices[i] = ((Number)vs[i]).floatValue();
        }
        
        //convert indexes
        final Object[] is = (Object[]) indexNode.getValue();
        final IntSequence index = new IntSequence();
        final IntSequence polygon = new IntSequence();
        for(int i=0;i<is.length;){
            polygon.removeAll();
            int d;
            do{
               d = ((Number)is[i]).intValue();
               i++;
               if(d>=0){
                   polygon.put(d);
               }else{
                   polygon.put(d*-1+1);
               }
            }while(d>=0);
            
            if(polygon.getSize()==3){
                index.put(polygon.toArrayInt());
            }else if(polygon.getSize()==4){
                final int[] array = polygon.toArrayInt();
                index.put(array[0]);
                index.put(array[1]);
                index.put(array[2]);
                index.put(array[0]);
                index.put(array[2]);
                index.put(array[3]);
            }else{
                final int[] polyvi = polygon.toArrayInt();
                final Tuple[] contour = new Tuple[polyvi.length];
                final VBO vbo = new VBO(vertices, 3);
                for(int k=0;k<polyvi.length;k++){
                    contour[k] = new Vector(vbo.getTupleFloat(polyvi[k], null));
                }
                try {
                    final int[][] idx = MeshUtilities.triangulate(contour);
                    for(int k=0;k<idx.length;k++){
                        index.put(polyvi[idx[k][0]]);
                        index.put(polyvi[idx[k][1]]);
                        index.put(polyvi[idx[k][2]]);
                    }
                } catch (OperationException ex) {
                    throw new StoreException(ex);
                }
            }
        }
        
        final Mesh mesh = new Mesh();
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices, 3));
        shell.setIndexes(new IBO(index.toArrayInt(), 3), IndexRange.TRIANGLES(0, index.getSize()/3));
        shell.calculateNormals();
        
        mesh.setShape(shell);
        return mesh;
    }
    
}
