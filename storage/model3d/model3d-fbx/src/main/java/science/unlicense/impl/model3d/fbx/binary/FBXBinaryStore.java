
package science.unlicense.impl.model3d.fbx.binary;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.model.tree.DefaultNamedNode;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.impl.io.zlib.ZlibInputStream;
import science.unlicense.impl.model3d.fbx.FBXConstants;
import science.unlicense.impl.model3d.fbx.FBXStore;

/**
 *
 * @author Johann Sorel
 */
public class FBXBinaryStore extends FBXStore{

    public FBXBinaryStore(Object input) {
        super(FBXBinaryFormat.INSTANCE,input);
    }

    public NamedNode read() throws IOException{
        final BacktrackInputStream bs = new BacktrackInputStream(getSourceAsInputStream());
        final DataInputStream ds = new DataInputStream(bs,NumberEncoding.LITTLE_ENDIAN);
        final byte[] signature = ds.readFully(new byte[21]);
        
        if(!Arrays.equals(signature, FBXConstants.SIGNATURE_BINARY)){
            throw new IOException("Wrong file signature");
        }
        
        ds.skipFully(2);
        final int version = ds.readInt();        
        
        final NamedNode root = new DefaultNamedNode(Chars.EMPTY, true);
        bs.mark();
        while(bs.read()!=-1){
            bs.rewind();
            final Node child = readNode(ds);
            if(child==null) break; //end of file
            root.getChildren().add(child);
            bs.mark();
        }
        
        return root;
    }
    
    private Node readNode(DataInputStream ds) throws IOException{
     
        //read node
        final byte[] header = ds.readFully(new byte[13]);
        if(Arrays.equals(FBXConstants.NULL, header)) return null;
        
        final long endOffset = NumberEncoding.LITTLE_ENDIAN.readUInt(header, 0);
        final long nbProperties = NumberEncoding.LITTLE_ENDIAN.readUInt(header, 4);
        final long propertyListLength = NumberEncoding.LITTLE_ENDIAN.readUInt(header, 8);
        final int nameLength = NumberEncoding.LITTLE_ENDIAN.readUByte(header, 12);
        final Chars name = ds.readBlockZeroTerminatedChars(nameLength, CharEncodings.US_ASCII);        
        final NamedNode node = new DefaultNamedNode(name, true);
        
        //read properties
        if(nbProperties>0){
            if(nbProperties==1){
                //common case
                node.setValue(readProperty(ds));
            }else{
                final Object[] properties = new Object[(int)nbProperties];
                for(int i=0;i<nbProperties;i++){
                    properties[i] = readProperty(ds);
                }
                node.setValue(properties);
            }
        }
        
        
        //read sub nodes if any
        while(ds.getByteOffset()!=endOffset){
            final Node child = readNode(ds);
            if(child!=null) node.getChildren().add(child);
        }
                
        return node;
    }
    
    private Object readProperty(DataInputStream ds) throws IOException{
        
        final int type = ds.readUByte();
        
        Object value = null;
        //primitive types
        if(type=='Y'){
            value = ds.readShort();
        }else if(type=='C'){
            value = ds.readByte()!=0;
        }else if(type=='I'){
            value = ds.readInt();
        }else if(type=='F'){
            value = ds.readFloat();
        }else if(type=='D'){
            value = ds.readDouble();
        }else if(type=='L'){
            value = ds.readLong();
        }
        //string/bytes types
        else if(type=='S'){
            final int length = ds.readInt();
            value = ds.readBlockZeroTerminatedChars(length, CharEncodings.US_ASCII);
        }else if(type=='R'){
            final int length = ds.readInt();
            value = ds.readFully(new byte[length]);
        }
        //array types
        else if(type=='f' || type=='d' || type=='l' || type=='i' || type=='b'){
            value = readArray(ds, type);
        }
        //unknowned
        else{
            throw new IOException("Unexpected property type : "+type);
        }
        return value;
    }
    
    public Object readArray(DataInputStream ds, int type) throws IOException{
        final int length = ds.readInt();
        final int encoding = ds.readInt();
        final int compressedLength = ds.readInt();
        
        if(encoding==1){
            final byte[] data = ds.readFully(new byte[compressedLength]);
            final ZlibInputStream deflate = new ZlibInputStream(new ArrayInputStream(data));
            ds = new DataInputStream(deflate,NumberEncoding.LITTLE_ENDIAN);
        }
        
        Object value = null;
        if(type=='f'){
            value = ds.readFloat(length);
        }else if(type=='d'){
            value = ds.readDouble(length);
        }else if(type=='l'){
            value = ds.readLong(length);
        }else if(type=='i'){
            value = ds.readInt(length);
        }else if(type=='b'){
            value = ds.readFully(new byte[length]);
        }
        
        return value;
    }
    
    
}
