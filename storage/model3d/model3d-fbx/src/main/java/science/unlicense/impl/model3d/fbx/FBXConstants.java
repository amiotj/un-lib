
package science.unlicense.impl.model3d.fbx;

import science.unlicense.api.character.Chars;

/**
 * 
 * @author Johann Sorel
 */
public class FBXConstants {
    
    public static final byte[] SIGNATURE_BINARY = new byte[]{'K','a','y','d','a','r','a',' ','F','B','X',' ','B','i','n','a','r','y',' ',' ','\0'};
    public static final byte[] SIGNATURE_ASCII = new byte[]{';',' ','F','B','X'};

    public static final byte[] NULL = new byte[13];
    
    public static final Chars NODE_OBJECTS = new Chars("Objects");
    public static final Chars NODE_MODEL = new Chars("Model");
    public static final Chars NODE_MODEL_VERTICES = new Chars("Vertices");
    public static final Chars NODE_MODEL_INDEX = new Chars("PolygonVertexIndex");
    public static final Chars NODE_MODEL_EDGES = new Chars("Edges");
    public static final Chars NODE_MODEL_LAYER_NORMAL = new Chars("LayerElementNormal");
    public static final Chars NODE_MODEL_LAYER_COLOR = new Chars("LayerElementColor");
    public static final Chars NODE_MODEL_LAYER_UV = new Chars("LayerElementUV");
    public static final Chars NODE_MODEL_LAYER_TRSUV = new Chars("LayerElementTransparentUV");
    public static final Chars NODE_MODEL_LAYER_SMOOTHING = new Chars("LayerElementSmoothing"); 
    public static final Chars NODE_MODEL_LAYER_VISIBILITY = new Chars("LayerElementVisibility");
    public static final Chars NODE_MODEL_LAYER_MATERIAL = new Chars("LayerElementMaterial");    
    public static final Chars NODE_MODEL_LAYER_TEXTURE = new Chars("LayerElementTexture");    
    public static final Chars NODE_MODEL_LAYER_TRSTEXTURE = new Chars("LayerElementTransparentTextures");
    public static final Chars NODE_VIDEO = new Chars("Video");
    public static final Chars NODE_TEXTURE = new Chars("Texture");
    public static final Chars NODE_TEXTURE_FILENAME = new Chars("FileName");
            
            
            
            
    private FBXConstants(){}
}
