
package science.unlicense.impl.model3d.fbx.ascii;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.impl.model3d.fbx.FBXStore;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class FBXAsciiStore extends FBXStore{

    public FBXAsciiStore(Object input) {
        super(FBXAsciiFormat.INSTANCE,input);
    }

    protected NamedNode read() throws IOException {
        final ByteInputStream in = getSourceAsInputStream();
        
        //parse text content
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/model3d/fbx/fbx.gr")));
        final OrderedHashDictionary tokens = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokens, rules);
             
        final Rule rule = (Rule) rules.getValue(new Chars("file"));
                
        //prepare lexer
        final Lexer lexer = new Lexer();
        lexer.setInput(in);
        
        //prepare parser
        final Parser parser = new Parser(rule);
        parser.setInput(lexer);
        final SyntaxNode node = parser.parse();
        
        final TokenType whitespace = (TokenType) tokens.getValue(new Chars("WS"));
        
        node.trim(new Predicate() {
            public Boolean evaluate(Object candidate) {
                final SyntaxNode sn = (SyntaxNode) candidate;
                final Token token = sn.getToken();
                return token!=null && token.type == whitespace;
            }
        });
        System.out.println(node.toCharsTree(20));
        
        return null;
    }

}
