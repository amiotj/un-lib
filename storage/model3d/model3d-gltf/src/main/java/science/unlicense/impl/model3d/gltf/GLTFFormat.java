
package science.unlicense.impl.model3d.gltf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * GLTF Format.
 * 
 * resource :
 * https://www.khronos.org/gltf
 * 
 * @author Johann Sorel
 */
public class GLTFFormat extends AbstractModel3DFormat{

    public static final GLTFFormat INSTANCE = new GLTFFormat();

    private GLTFFormat() {
        super(new Chars("GLTF"),
              new Chars("GLTF"),
              new Chars("GLTF"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("gltf")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        throw new IOException("Not supported yet.");
    }

}
