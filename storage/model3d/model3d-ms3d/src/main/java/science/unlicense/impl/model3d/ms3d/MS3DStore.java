
package science.unlicense.impl.model3d.ms3d;

import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.path.Path;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 *
 * @author Johann Sorel
 */
public class MS3DStore extends AbstractModel3DStore{

    public Path base;

    //header
    public int version;

    //mesh
    public MS3DVertex[] vertices;
    public MS3DTriangle[] triangles;
    public MS3DGroup[] groups;
    public MS3DMaterial[] materials;

    //animation
    public float fAnimationFPS;
    public float fCurrentTime;
    public int iTotalFrames;
    public MS3DJoint[] joints;

    //comment extension
    public int commentSubversion;
    public Chars comment;

    //vertex extension
    public int vertexSubversion;

    //joint extension
    public int jointSubversion;

    //model extension
    public int modelSubversion;
    // joint size, since subVersion == 1
    public float jointSize;
    // 0 = simple, 1 = depth buffered with alpha ref, 2 = depth sorted triangles, since subVersion == 1
    public int transparencyMode;
    // alpha reference value for transparencyMode = 1, since subVersion == 1
    public float alphaRef;


    public MS3DStore(Object input) {
        super(MS3DFormat.INSTANCE,input);
    }

    public Collection getElements() {
        final MultipartMesh root = new MultipartMesh();

        // Mesh Transformation:
        // 0. Build the transformation matrices from the rotation and position
        // 1. Multiply the vertices by the inverse of local reference matrix (lmatrix0)
        // 2. then translate the result by (lmatrix0 * keyFramesTrans)
        // 3. then multiply the result by (lmatrix0 * keyFramesRot)
        // For normals skip step 2.

        //TODO work by group to create part mesh rather then triangle
        for(int i=0;i<groups.length;i++){
            final MS3DGroup group = groups[i];
            final Mesh mesh = buildGroup(group);
            root.getChildren().add(mesh);
        }

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private Mesh buildGroup(final MS3DGroup group){
        final Mesh mesh = new Mesh();
        final FloatCursor vertexBuffer = DefaultBufferFactory.INSTANCE.createFloat(group.numtriangles*3*3).cursorFloat();
        final FloatCursor normalBuffer = DefaultBufferFactory.INSTANCE.createFloat(group.numtriangles*3*3).cursorFloat();
        final IntCursor indexBuffer = DefaultBufferFactory.INSTANCE.createInt(group.numtriangles*3).cursorInt();
        final FloatCursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat(group.numtriangles*3*2).cursorFloat();

        int k=-1;
        for(int i=0;i<group.numtriangles;i++){
            final MS3DTriangle triangle = triangles[group.triangleIndices[i]];
            final MS3DVertex v0 = vertices[triangle.vertexIndices[0]];
            final MS3DVertex v1 = vertices[triangle.vertexIndices[1]];
            final MS3DVertex v2 = vertices[triangle.vertexIndices[2]];
            vertexBuffer.write(v0.position.toArrayFloat());
            vertexBuffer.write(v1.position.toArrayFloat());
            vertexBuffer.write(v2.position.toArrayFloat());
            normalBuffer.write(triangle.normals[0].toArrayFloat());
            normalBuffer.write(triangle.normals[1].toArrayFloat());
            normalBuffer.write(triangle.normals[2].toArrayFloat());
            indexBuffer.write(++k);
            indexBuffer.write(++k);
            indexBuffer.write(++k);
            uvBuffer.write(triangle.s[0]);
            uvBuffer.write(triangle.t[0]);
            uvBuffer.write(triangle.s[1]);
            uvBuffer.write(triangle.t[1]);
            uvBuffer.write(triangle.s[2]);
            uvBuffer.write(triangle.t[2]);
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertexBuffer.getBuffer(),3));
        shell.setNormals(new VBO(normalBuffer.getBuffer(),3));
        shell.setIndexes(new IBO(indexBuffer.getBuffer(),3),IndexRange.TRIANGLES(0, (int) indexBuffer.getBuffer().getPrimitiveCount()));
        shell.setUVs(new VBO(uvBuffer.getBuffer(), 2));

        //build the material
        try{
            final MS3DMaterial material = materials[group.materialIndex];
            final Image image = Images.read(base.resolve(material.texture));
            final UVMapping paint = new UVMapping(new Texture2D(image));
            mesh.getMaterial().putOrReplaceLayer(new Layer(paint));
        }catch(IOException ex){
            ex.printStackTrace();
        }


        mesh.setShape(shell);
        mesh.getShape().calculateBBox();
        return mesh;
    }

}