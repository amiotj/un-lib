
package science.unlicense.impl.model3d.ms3d;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.color.Color;
import science.unlicense.api.path.Path;
import science.unlicense.impl.math.Vector;

/**
 * MS3D Reader
 * 
 * @author Johann Sorel
 */
public class MS3DReader extends AbstractReader{

    private MS3DStore storage;
    
    public MS3DStore read(MS3DStore storage) throws IOException {
        setInput(storage.getInput());
        this.storage = storage;
        storage.base = ((Path) storage.getInput()).getParent();
        
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        
        // First comes the header (sizeof(ms3d_header_t) == 14)
        final byte[] signature = ds.readFully(new byte[10]);
        if(!Arrays.equals(signature, MS3DConstants.SIGNATURE)){
            throw new IOException("File is not a valid milkshape 3d.");
        }
        storage.version = ds.readInt();
        if(storage.version!=4){
            throw new IOException("Unsupported version "+storage.version);
        }
        
        // Then comes the number of vertices, 2 bytes
        final int nbVertices = ds.readUShort();
        storage.vertices = new MS3DVertex[nbVertices];
        
        // Then come nNumVertices times ms3d_vertex_t structs (sizeof(ms3d_vertex_t) == 15)
        for(int i=0;i<nbVertices;i++){
            final MS3DVertex vertex = new MS3DVertex();
            vertex.flags = ds.readByte();
            vertex.position = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
            vertex.boneId = ds.readByte();
            vertex.referenceCount = ds.readByte();
            storage.vertices[i] = vertex;
        }
        
        // Then comes the number of triangles, 2 bytes
        final int nbTriangles = ds.readUShort();
        storage.triangles = new MS3DTriangle[nbTriangles];
        
        // Then come nNumTriangles times ms3d_triangle_t structs (sizeof(ms3d_triangle_t) == 70)
        for(int i=0;i<nbTriangles;i++){
            final MS3DTriangle triangle = new MS3DTriangle();
            triangle.flags = ds.readUShort();
            triangle.vertexIndices = new int[]{ds.readUShort(),ds.readUShort(),ds.readUShort()};
            triangle.normals = new Vector[]{
                new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat()),
                new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat()),
                new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat())
            };
            triangle.s = new float[]{ds.readFloat(),ds.readFloat(),ds.readFloat()};
            triangle.t = new float[]{ds.readFloat(),ds.readFloat(),ds.readFloat()};
            triangle.smoothingGroup = ds.readByte();
            triangle.groupIndex = ds.readByte();
            storage.triangles[i] = triangle;
        }
        
        // Then comes the number of groups, 2 bytes
        final int nbGroups = ds.readUShort();
        storage.groups = new MS3DGroup[nbGroups];

        // Then come nNumGroups times groups (the sizeof a group is dynamic, because of triangleIndices is numtriangles long)
        for(int i=0;i<nbGroups;i++){
            final MS3DGroup group = new MS3DGroup();
            group.flags = ds.readByte();
            group.name = new Chars(ds.readFully(new byte[32]));
            group.numtriangles = ds.readUShort();
            group.triangleIndices = new int[group.numtriangles];
            for(int k=0;k<group.numtriangles;k++){
                group.triangleIndices[k] = ds.readUShort();
            }
            group.materialIndex = ds.readByte();
            storage.groups[i] = group;
        }
        
        // number of materials, 2 bytes
        final int nbMaterials = ds.readUShort();
        storage.materials = new MS3DMaterial[nbMaterials];
        
        // Then come nNumMaterials times ms3d_material_t structs (sizeof(ms3d_material_t) == 361)
        for(int i=0;i<nbMaterials;i++){
            final MS3DMaterial material = new MS3DMaterial();
            material.name = new Chars(ds.readFully(new byte[32]));
            material.ambient = new Color(ds.readFloat(), ds.readFloat(), ds.readFloat(), ds.readFloat());
            material.diffuse = new Color(ds.readFloat(), ds.readFloat(), ds.readFloat(), ds.readFloat());
            material.specular = new Color(ds.readFloat(), ds.readFloat(), ds.readFloat(), ds.readFloat());
            material.emissive = new Color(ds.readFloat(), ds.readFloat(), ds.readFloat(), ds.readFloat());
            material.shininess = ds.readFloat();
            material.transparency = ds.readFloat();
            material.mode = ds.readByte();
            material.texture = cut(new Chars(ds.readFully(new byte[128])));
            material.alphamap = cut(new Chars(ds.readFully(new byte[128])));
            storage.materials[i] = material;
        }
        
        // save some keyframer data
        storage.fAnimationFPS = ds.readFloat();
        storage.fCurrentTime = ds.readFloat();
        storage.iTotalFrames = ds.readInt();
        
        // number of joints, 2 bytes
        final int nbJoint = ds.readUShort();
        storage.joints = new MS3DJoint[nbJoint];
        
        // Then come nNumJoints joints (the size of joints are dynamic, because each joint has a differnt count of keys
        for(int i=0;i<nbJoint;i++){
            final MS3DJoint joint = new MS3DJoint();
            joint.flags = ds.readByte();
            joint.name = new Chars(ds.readFully(new byte[32]));
            joint.parentName = new Chars(ds.readFully(new byte[32]));
            joint.rotation = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
            joint.position = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
            joint.numKeyFramesRot = ds.readUShort();
            joint.numKeyFramesTrans = ds.readUShort();
            joint.keyFramesRot = new MS3DJoint.RotationFrame[joint.numKeyFramesRot];
            joint.keyFramesTrans = new MS3DJoint.PositionFrame[joint.numKeyFramesTrans];
            for(int k=0;k<joint.numKeyFramesRot;k++){
                joint.keyFramesRot[k] = new MS3DJoint.RotationFrame();
                joint.keyFramesRot[k].time = ds.readFloat();
                joint.keyFramesRot[k].rotation = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
            }
            for(int k=0;k<joint.numKeyFramesTrans;k++){
                joint.keyFramesTrans[k] = new MS3DJoint.PositionFrame();
                joint.keyFramesTrans[k].time = ds.readFloat();
                joint.keyFramesTrans[k].position = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
            }
            
            storage.joints[i] = joint;
        }
        
        
        // Then comes the subVersion of the comments part, which is not available in older files
        storage.commentSubversion = ds.readInt(); // subVersion is = 1, 4 bytes
        
        if(storage.commentSubversion!=1){
            throw new IOException("Unsupported subversion "+storage.commentSubversion);
        }
        
        // Then comes the numer of group comments, 4bytes
        final int nbGroupComments = ds.readInt();
        // Then come nNumGroupComments times group comments, which are dynamic, because the comment can be any length
        for(int i=0;i<nbGroupComments;i++){
            // index of group, material or joint
            final int index = ds.readInt();
            // length of comment (terminating '\0' is not saved), "MC" has comment length of 2 (not 3)
            final int commentLength = ds.readInt();
            storage.groups[index].comment = new Chars(ds.readFully(new byte[commentLength]));
        }
        
        // Then comes the number of material comments, 4 bytes
        final int nNumMaterialComments = ds.readInt();
        // Then come nNumMaterialComments times material comments, which are dynamic, because the comment can be any length
        for(int i=0;i<nNumMaterialComments;i++){
            final int index = ds.readInt();
            final int commentLength = ds.readInt();
            storage.materials[index].comment = new Chars(ds.readFully(new byte[commentLength]));
        }
        
        // Then comes the number of joint comments, 4 bytes
        final int nNumJointComments = ds.readInt();
        // Then come nNumJointComments times joint comments, which are dynamic, because the comment can be any length
        for(int i=0;i<nNumJointComments;i++){
            final int index = ds.readInt();
            final int commentLength = ds.readInt();
            storage.joints[index].comment = new Chars(ds.readFully(new byte[commentLength]));
        }
        
        // Then comes the number of model comments, which is always 0 or 1, 4 bytes
        final int nHasModelComment = ds.readInt();
        // Then come nHasModelComment times model comments, which are dynamic, because the comment can be any length
        for(int i=0;i<nHasModelComment;i++){
            final int index = ds.readInt();
            final int commentLength = ds.readInt();
            storage.comment = new Chars(ds.readFully(new byte[commentLength]));
        }
        
        
        // Then comes the subversion of the vertex extra information like bone weights, extra etc.
        // subVersion is = 2, 4 bytes
        storage.vertexSubversion = ds.readInt();
        // Then comes nNumVertices times ms3d_vertex_ex_t structs (sizeof(ms3d_vertex_ex_t) == 10)
        for(int i=0;i<nbVertices;i++){
            final MS3DVertex vertex = storage.vertices[i];
            vertex.boneIds = new int[]{ds.readByte(),ds.readByte(), ds.readByte()};
            vertex.weights = new int[]{ds.readUByte(),ds.readUByte(), ds.readUByte()};
            if(storage.vertexSubversion==2){
                vertex.extra = ds.readUInt();
            }
        }
        
        // Then comes the subversion of the joint extra information like color etc.
        // subVersion is = 2, 4 bytes
        storage.jointSubversion = ds.readInt();
        // Then comes nNumJoints times ms3d_joint_ex_t structs (sizeof(ms3d_joint_ex_t) == 12)
        for(int i=0;i<nbJoint;i++){
            final MS3DJoint joint = storage.joints[i];
            joint.rgb = new float[]{ds.readFloat(),ds.readFloat(),ds.readFloat()};
        }
        
        // Then comes the subversion of the model extra information
        // subVersion is = 1, 4 bytes
        storage.modelSubversion = ds.readInt();
        storage.jointSize = ds.readFloat();
        storage.transparencyMode = ds.readInt();
        storage.alphaRef = ds.readFloat();
        
        return storage;
    }
    
    private Chars cut(Chars candidate){
        final int index = candidate.getFirstOccurence(0);
        if(index>=0){
            return candidate.truncate(0, index);
        }else{
            return candidate;
        }
    }
}