
package science.unlicense.impl.model3d.ms3d;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class MS3DMaterial {
    
    //size 32 bytes
    public Chars name;
    public Color ambient;
    public Color diffuse;
    public Color specular;
    public Color emissive;
    // 0.0f - 128.0f
    public float shininess;
    // 0.0f - 1.0f
    public float transparency;
    // 0, 1, 2 is unused now
    public byte mode;                       
    // texture.bmp 128 bytes
    public Chars texture;
    // alpha.bmp 128 bytes
    public Chars alphamap;
    
    //comment
    public Chars comment;
    
}