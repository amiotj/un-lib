
package science.unlicense.impl.model3d.ms3d;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class MS3DFormat extends AbstractModel3DFormat{

    public static final MS3DFormat INSTANCE = new MS3DFormat();

    private MS3DFormat() {
        super(new Chars("MS3D"),
              new Chars("MS3D"),
              new Chars("MilkShape 3D"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ms3d")
              },
              new byte[][]{MS3DConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        final MS3DStore store = new MS3DStore(input);
        final MS3DReader reader = new MS3DReader();
        reader.read(store);
        return store;
    }

}