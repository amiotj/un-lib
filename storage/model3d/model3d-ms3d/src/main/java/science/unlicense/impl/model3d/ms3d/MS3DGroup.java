
package science.unlicense.impl.model3d.ms3d;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class MS3DGroup {
    // SELECTED | HIDDEN
    public byte flags;  
    //32 bytes
    public Chars name;
    //2 bytes
    public int numtriangles;
    // the groups group the triangles, size = numtriangles
    public int[] triangleIndices;     
    // -1 = no material
    public byte materialIndex;        
    
    //comment
    public Chars comment;
}