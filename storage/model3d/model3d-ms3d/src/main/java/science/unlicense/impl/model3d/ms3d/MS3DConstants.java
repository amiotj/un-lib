
package science.unlicense.impl.model3d.ms3d;

/**
 * MilkShape 3D constants.
 * http://chumbalum.swissquake.ch/ms3d/ms3dspec.txt
 * 
 * @author Johann Sorel
 */
public final class MS3DConstants {
   
    public static final byte[] SIGNATURE = new byte[]{'M','S','3','D','0','0','0','0','0','0'};
    
    public static final int SELECTED    = 1;
    public static final int HIDDEN      = 2;
    public static final int SELECTED2   = 4;
    public static final int DIRTY       = 8;
    
    private MS3DConstants(){}
    
}