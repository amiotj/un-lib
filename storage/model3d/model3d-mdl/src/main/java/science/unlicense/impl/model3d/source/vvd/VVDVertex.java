

package science.unlicense.impl.model3d.source.vvd;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.source.mdl2.MDLReader;

/**
 *
 * @author Johann Sorel
 */
public class VVDVertex {
    
    public VVDBoneWeight weights;
    /** Note : strange format, position are in inches */
    public Vector position;
    public Vector normal;
    public Vector uv;
    
    public void read(DataInputStream ds) throws IOException{
        weights  = new VVDBoneWeight();
        weights.read(ds);
        position = MDLReader.readVector3(ds);
        normal   = MDLReader.readVector3(ds);
        uv       = MDLReader.readVector2(ds);
    }
    
}
