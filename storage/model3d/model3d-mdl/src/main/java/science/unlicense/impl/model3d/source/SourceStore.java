

package science.unlicense.impl.model3d.source;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.path.Path;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.model3d.source.mdl2.MDLBone;
import science.unlicense.impl.model3d.source.mdl2.MDLMeshPart;
import science.unlicense.impl.model3d.source.mdl2.MDLReader;
import science.unlicense.impl.model3d.source.vtf.VTFReader;
import science.unlicense.impl.model3d.source.vtx.VTXMeshPart;
import science.unlicense.impl.model3d.source.vtx.VTXReader;
import science.unlicense.impl.model3d.source.vvd.VVDLod;
import science.unlicense.impl.model3d.source.vvd.VVDReader;
import science.unlicense.impl.model3d.source.vvd.VVDVertex;

/**
 *
 * @author Johann Sorel
 */
public class SourceStore extends AbstractModel3DStore {

    public SourceStore(Object input) {
        super(SourceFormat.INSTANCE, input);
    }

    public Collection getElements() throws StoreException {
        final Sequence elements = new ArraySequence();
        
        final Path mdlPath = (Path) getInput();
        
        final Path parentPath = mdlPath.getParent();
        final Path vvdPath = parentPath.resolve(mdlPath.getName().replaceAll(new Chars("mdl"), new Chars("vvd")));
        final Path vtxPath = parentPath.resolve(mdlPath.getName().replaceAll(new Chars("mdl"), new Chars("sw.vtx")));
        
        final MDLReader mdlReader = new MDLReader();
        final VVDReader vvdReader = new VVDReader();
        final VTXReader vtxReader = new VTXReader();

        final Skeleton skeleton = new Skeleton();
        try {
            mdlReader.setInput(mdlPath);
            mdlReader.read();
            vvdReader.setInput(vvdPath);
            vvdReader.read();
            vtxReader.setInput(vtxPath);
            vtxReader.read();
            
            //TODO add LOD api in 3d engine, or at least provide something
            //to select the loaded LOD level
            final int lodLevel = 0;
            final VVDLod vvdLod = vvdReader.lods[lodLevel];
            
            final MultipartMesh mpm = new MultipartMesh();
            elements.add(mpm);
            
            //rebuild joints
            final Dictionary dico = new HashDictionary();
            for(int i=0;i<mdlReader.bones.length;i++){
                final MDLBone mdlbone = mdlReader.bones[i];
                final Joint joint = new Joint(3);
                joint.setName(mdlbone.name);
                joint.getNodeTransform().getTranslation().set(mdlbone.position);
                joint.getNodeTransform().getRotation().set(mdlbone.rotation.toMatrix3());
                joint.getNodeTransform().notifyChanged();
                dico.add(i, joint);
                
                if(mdlbone.parentBone>=0){
                    final Joint parent = (Joint) dico.getValue(mdlbone.parentBone);
                    parent.getChildren().add(joint);
                }else{
                    //root joint
                    skeleton.getChildren().add(joint);
                }
            }
            //build skeleton
            skeleton.updateBindPose();
            skeleton.updateInvBindPose();            
            mpm.setSkeleton(skeleton);
                        
            //keep track of index since the same structure is in MDL/VTX/VVD
            for(int p=0;p<mdlReader.parts.length;p++){
                final MDLMeshPart mdlPart = mdlReader.parts[p];
                final VTXMeshPart vtxPart = vtxReader.parts[p];
                
                for(int d=0;d<mdlPart.datas.length;d++){
                    final MDLMeshPart.MDLModel mdlModel = mdlPart.datas[d];
                    final VTXMeshPart.VTXModel vtxModel = vtxPart.models[d];
                    
                    for(int m=0;m<mdlModel.meshes.length;m++){
                        final MDLMeshPart.MDLMesh mdlMesh = mdlModel.meshes[m];
                        
                        final VTXMeshPart.VTXLod vtxLod = vtxModel.lods[lodLevel];
                        final VTXMeshPart.VTXMesh vtxMesh = vtxLod.meshes[m];
                                
                        //Now link VTX indexes with VVD datas
                        //Note : Source engine models are really a pain to rebuild.
                        // This storage format is definitly not man made, it's more
                        // like an automatic binary binding.
                        
                        final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(mdlMesh.vertexNb*3).cursorFloat();
                        final FloatCursor normals = DefaultBufferFactory.INSTANCE.createFloat(mdlMesh.vertexNb*3).cursorFloat();
                        final FloatCursor uvs = DefaultBufferFactory.INSTANCE.createFloat(mdlMesh.vertexNb*2).cursorFloat();
                        final IntCursor jointIndexes = DefaultBufferFactory.INSTANCE.createInt(mdlMesh.vertexNb*3).cursorInt();
                        final FloatCursor jointWeights = DefaultBufferFactory.INSTANCE.createFloat(mdlMesh.vertexNb*3).cursorFloat();
                        science.unlicense.api.collection.primitive.IntSequence indexBuf = new science.unlicense.api.collection.primitive.IntSequence();

                        int idxOffset = 0;
                        for(VTXMeshPart.VTXStripGroup stripGroup : vtxMesh.stripGroups){
                            for(VTXMeshPart.VTXVertex vtxVertex : stripGroup.vertices){
                                final int vertexIdx = mdlMesh.vertexOffset + vtxVertex.meshVertexId;
                                final VVDVertex vvdVertex = vvdLod.vertices[vertexIdx];
                                vertices.write(vvdVertex.position.toArrayFloat());
                                normals.write(vvdVertex.normal.toArrayFloat());
                                uvs.write(vvdVertex.uv.toArrayFloat());
                                jointIndexes.write(vvdVertex.weights.bones);
                                jointWeights.write(vvdVertex.weights.weights);
                            }
                            
                            for(int idx : stripGroup.indices) indexBuf.put(idx+idxOffset);
                            idxOffset += stripGroup.indices.length;
                        }
                        final Buffer index = DefaultBufferFactory.wrap(indexBuf.toArrayInt());
                        
                        final SkinShell shell = new SkinShell();
                        shell.setSkeleton(skeleton);
                        shell.setVertices(new VBO(vertices.getBuffer(), 3));
                        shell.setNormals(new VBO(normals.getBuffer(), 3));
                        shell.setUVs(new VBO(uvs.getBuffer(), 2));
                        shell.setIndexes(new IBO(index, 3), IndexRange.TRIANGLES(0, (int) index.getPrimitiveCount()));
                        shell.setMaxWeightPerVertex(3);
                        shell.setJointIndexes(new VBO(jointIndexes.getBuffer(), 3));
                        shell.setWeights(new VBO(jointWeights.getBuffer(), 3));

                        final Mesh mesh = new Mesh();
                        ((MeshRenderer)mesh.getRenderers().get(0)).getState().setCulling(-1);
                        mesh.setShape(shell);
                        
                        
                        
                        if(true){
                            try{
                                //draft rebuild texture
                                final Path baseTextPath = parentPath.resolve(mdlReader.textureLibPaths[0].replaceAll('\\', '/'));
                                final Path vtfDiffusePath = baseTextPath.resolve(mdlReader.textures[m].name.concat(new Chars(".vtf")));
                                final VTFReader vtfDiffuseReader = new VTFReader();
                                vtfDiffuseReader.setInput(vtfDiffusePath);
                                vtfDiffuseReader.read();
                                mesh.getMaterial().putOrReplaceLayer(new Layer(new UVMapping(new Texture2D(vtfDiffuseReader.image))));
                                
                                //search for a normal file
                                {
                                    final Path vtfNormalPath = baseTextPath.resolve(mdlReader.textures[m].name.replaceAll(new Chars("diff"), new Chars("norm")).concat(new Chars(".vtf")));
                                    final VTFReader vtfNormalReader = new VTFReader();
                                    vtfNormalReader.setInput(vtfNormalPath);
                                    vtfNormalReader.read();
                                    mesh.getMaterial().putOrReplaceLayer(new Layer(new UVMapping(new Texture2D(vtfNormalReader.image)), Layer.TYPE_NORMAL));
                                }
                                
                            }catch(IOException ex){
                                ex.printStackTrace();
                            }
                        }
                        
                        shell.calculateTangents();
                        
                        mpm.getChildren().add(mesh);
                    }
                }
            }
                  
        } catch (IOException ex) {
            throw new StoreException(ex);
        }
        
        return elements;
    }
    
}
