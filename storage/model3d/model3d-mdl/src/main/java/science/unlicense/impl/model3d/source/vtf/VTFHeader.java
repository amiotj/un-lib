

package science.unlicense.impl.model3d.source.vtf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class VTFHeader {

    /** file signature, should be VTF0 */
    public Chars    signature;
    /** version, should be 7.2 */
    public int      version1;
    public int      version2;
    /**Size of the header struct (16 byte aligned; currently 80 bytes). */
    public int      headerSize;		
    /**Width of the largest mipmap in pixels. Must be a power of 2. */
    public int      width;		
    /**Height of the largest mipmap in pixels. Must be a power of 2. */
    public int      height;		
    /**VTF flags. */
    public int      flags;		
    /**Number of frames, if animated (1 for no animation). */
    public int      frames;		
    /**First frame in animation (0 based). */
    public int      firstFrame;		
    /**reflectivity padding (16 byte alignment). */
    public byte[]   padding0;		
    /**reflectivity vector. */
    public Vector   reflectivity;	
    /**reflectivity padding (8 byte packing). */
    public byte[]   padding1;		
    /**Bumpmap scale. */
    public float    bumpmapScale;	
    /**High resolution image format. */
    public int      highResImageFormat;	
    /**Number of mipmaps. */
    public byte     mipmapCount;	
    /**Low resolution image format (always DXT1). */
    public int      lowResImageFormat;	
    /**Low resolution image width. */
    public int      lowResImageWidth;	
    /**Low resolution image height. */
    public int      lowResImageHeight;	
    /**Depth of the largest mipmap in pixels. */
    
    public int      depth;		
                                        
    
    public void read(DataInputStream ds) throws IOException{
        signature           = new Chars(ds.readFully(new byte[4]));
        version1            = ds.readInt();
        version2            = ds.readInt();
        headerSize          = ds.readInt();
        width               = ds.readUShort();
        height              = ds.readUShort();
        flags               = ds.readInt();
        frames              = ds.readUShort();
        firstFrame          = ds.readUShort();
        padding0            = ds.readFully(new byte[4]);
        reflectivity        = new Vector(ds.readFloat(),ds.readFloat(),ds.readFloat());
        padding1            = ds.readFully(new byte[4]);
        bumpmapScale        = ds.readFloat();
        highResImageFormat  = ds.readInt();
        mipmapCount         = ds.readByte();
        lowResImageFormat   = ds.readInt();
        lowResImageWidth    = ds.readUByte();
        lowResImageHeight   = ds.readUByte();
        depth               = ds.readUShort();
    }
    
}
