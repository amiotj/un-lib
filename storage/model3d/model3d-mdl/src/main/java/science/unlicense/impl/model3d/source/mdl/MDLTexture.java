
package science.unlicense.impl.model3d.source.mdl;

/**
 *
 * @author Johann Sorel
 */
public class MDLTexture {
    
    public float time;
    public byte[] data;
    
}
