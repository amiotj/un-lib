
package science.unlicense.impl.model3d.source.mdl;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;

/**
 * LMP color palette.
 * 
 * @author Johann Sorel
 */
public class LMPColorIndex {
    
    public final Color[] index = new Color[256];
    
    public void read(ByteInputStream stream) throws IOException{
        for(int i=0;i<256;i++){
            int r = stream.read() & 0xff;
            int g = stream.read() & 0xff;
            int b = stream.read() & 0xff;
            index[i] = new Color(r, g, b, 255);
        }
    }
    
    
}
