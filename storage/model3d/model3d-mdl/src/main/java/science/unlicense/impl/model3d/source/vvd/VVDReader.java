
package science.unlicense.impl.model3d.source.vvd;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * 
 * Doc :
 * https://developer.valvesoftware.com/wiki/VVD
 * 
 * @author Johann Sorel
 */
public class VVDReader extends AbstractReader {
    
    public VVDHeader header;
    public VVDLod[] lods;
    
    public void read() throws IOException{
        
        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, NumberEncoding.LITTLE_ENDIAN);
        
        //read header
        header = new VVDHeader();
        header.read(ds);
        
        //read fixup tables
        final VVDFixup[] fixups = new VVDFixup[header.nbFixup];
        for(int i=0;i<fixups.length;i++){
            fixups[i] = new VVDFixup();
            fixups[i].read(ds);
        }
        
        //read lods
        lods = new VVDLod[header.nbLod];
        for(int i=0;i<header.nbLod;i++){
            lods[i] = new VVDLod();
            lods[i].vertices = new VVDVertex[header.nbLodVertex[i]];
            
            if(header.nbFixup!=0){
                for(int f=0,inc=0;f<header.nbFixup;f++){
                    final VVDFixup fixup = fixups[f];
                    //move to vertex table start
                    bs.rewind();
                    ds.skipFully(header.vertexTableOffset);
                    //offset to first fixup vertex : 48 bytes per vertex
                    ds.skipFully(fixup.sourceVertexId*48);
                    //read vertices
                    for(int k=0;k<fixup.nbVertex;k++){
                        lods[i].vertices[inc] = new VVDVertex();
                        lods[i].vertices[inc].read(ds);
                        inc++;
                    }
                }
                
            }else{
                //move to vertex table start
                bs.rewind();
                ds.skipFully(header.vertexTableOffset);
                //read vertices
                for(int k=0;k<lods[i].vertices.length;k++){
                    lods[i].vertices[k] = new VVDVertex();
                    lods[i].vertices[k].read(ds);
                }
            }
        }
        
        //TODO read tangents
        //are tangents affected by fixup too ? need more files to check this
        //nothing more at this point in some file ... invalid files ? different version structure ?
        
    }
    
}
