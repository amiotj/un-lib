
package science.unlicense.impl.model3d.source.vtf;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * TODO
 * 
 * Doc :
 * https://developer.valvesoftware.com/wiki/Valve_Texture_Format
 * 
 * @author Johann Sorel
 */
public class VTFReader extends AbstractReader {
    
    public Image image;
    
    public void read() throws IOException {
    
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        final VTFHeader header = new VTFHeader();
        header.read(ds);
        //header is 65bytes long
        //skip remaining to low res image
        ds.skipFully(header.headerSize-65);
        
        //read low res image
        //TODO, just skip it for now
        // 16x16 preview (16*16*3)/8 = 96 bytes
        ds.skipFully(96);
        
        //there are 32bytes here, don't know for what, it's not mentionned in the doc.
        ds.skipFully(32);
        
        //TODO : read all levels, we only keep the last one
        final int[] sizes = new int[header.mipmapCount+1];
        sizes[0] = header.width;
        for(int i=1;i<sizes.length;i++) sizes[i] = sizes[i-1]/2;
        Arrays.reverse(sizes, 0, sizes.length);

        for(int i=0;i<sizes.length;i++){
            image = read(ds, sizes[i], sizes[i], header.highResImageFormat);
        }
                    
    }
        
    public static Image read(DataInputStream ds, int width, int height, int format) throws IOException{
        if(format==VTFConstants.IMAGE_FORMAT_BGR888){
            final byte[] buffer = ds.readFully(new byte[width*height*3]);
            final Buffer bank = DefaultBufferFactory.wrap(buffer);
            final RawModel sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
            final ColorModel cm = new DirectColorModel(sm, new int[]{2,1,0,-1},false);
            return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
            
        }else if(format==VTFConstants.IMAGE_FORMAT_BGRA8888){
            
            final byte[] buffer = ds.readFully(new byte[width*height*4]);
            final Buffer bank = DefaultBufferFactory.wrap(buffer);
            final RawModel sm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
            final ColorModel cm = new DirectColorModel(sm, new int[]{2,1,0,3},false);
            return new DefaultImage(bank, new Extent.Long(width, height), sm, cm);
            
        }else{
            throw new IOException("not supported yet : "+format);
        }
    }
    
    
}
