

package science.unlicense.impl.model3d.source.vvd;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VVDFixup {

    public int lod;
    public int sourceVertexId;
    public int nbVertex;
    
    public void read(DataInputStream ds) throws IOException {
        lod             = ds.readInt();
        sourceVertexId  = ds.readInt();
        nbVertex        = ds.readInt();
    }
    
}
