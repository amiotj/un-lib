
package science.unlicense.impl.model3d.source.vtx;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Doc and example decompiler in php :
 * https://developer.valvesoftware.com/wiki/VTX
 * 
 * @author Johann Sorel
 */
public class VTXReader extends AbstractReader {
 
    public VTXHeader header;
    //same structure as in MDL
    public VTXMeshPart[] parts;
    
    public void read() throws IOException{
        
        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, NumberEncoding.LITTLE_ENDIAN);
        
        //read header
        header = new VTXHeader();
        header.read(ds);
        
        //read mesh parts
        parts = new VTXMeshPart[header.nbBodyParts];
        for(int i=0;i<parts.length;i++){
            final int subOffset = header.bodyPartOffset+i*VTXMeshPart.BYTE_SIZE;
            bs.rewind();
            ds.skipFully(subOffset);
            parts[i] = new VTXMeshPart();
            parts[i].read(bs,ds,subOffset);
        }
        
    }
    
}