

package science.unlicense.impl.model3d.source.mdl2;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;

/**
 *
 * 
 * @author Johann Sorel
 */
public class MDLReader extends AbstractReader {
    
    public MDLHeader header;
    public MDLHeaderOpt headerOpt;
    public MDLBone[] bones;
    public MDLBoneController[] boneControllers;
    public MDLHitBox[] hitboxes;
    public MDLLocalAnim[] anims;
    public MDLLocalSeq[] sequences;
    public MDLTexture[] textures;
    public Chars[] textureLibPaths;
    public MDLSkin[] skins;
    public MDLMeshPart[] parts;
    private MDLAttachment[] attachments;
    private MDLLocalNode[] localNodes;
    private MDLFlexDesc[] flexDescs;
    private MDLFlexController[] flexControllers;
    private MDLFlexRule[] flexRules;
    private MDLIKChain[] ikChains;
    private MDLMouth[] mouths;
    private MDLLocalPoseParam[] localPoseParams;
    private MDLKeyValue[] keyValues;
    private MDLIKLock[] iklocks;
    private MDLIncludeModel[] includeModels;
    private MDLAnimBlock[] animBlocks;
    private MDLFlexControllerUI[] flexcontrollerUis;
    
    public void read() throws IOException {
        
        final BacktrackInputStream bs = getInputAsBacktrackStream();
        bs.mark();
        final DataInputStream ds = new DataInputStream(bs, NumberEncoding.LITTLE_ENDIAN);
        
        //read header
        header = new MDLHeader();
        header.read(ds);
        
        //read optional header
        headerOpt = null;
        if(header.studiohdr2Offset>0){
            bs.rewind();
            ds.skipFully(header.studiohdr2Offset);
            headerOpt = new MDLHeaderOpt();
            headerOpt.read(ds);
        }
        
        //read bones
        bs.rewind();
        ds.skipFully(header.boneOffset);
        bones = new MDLBone[header.boneNb];
        for(int i=0;i<bones.length;i++){
            bones[i] = new MDLBone();
            bones[i].read(ds);
        }
        //second pass : read bone names
        for(int i=0;i<header.boneNb;i++){
            bs.rewind();
            ds.skipFully(header.boneOffset+bones[i].nameOffset+i*MDLBone.BYTE_SIZE);
            bones[i].name = ds.readZeroTerminatedChars(256, CharEncodings.US_ASCII);
        }
        
        //read bone controllers
        bs.rewind();
        ds.skipFully(header.boneControllerOffset);
        boneControllers = new MDLBoneController[header.boneControllerNb];
        for(int i=0;i<boneControllers.length;i++){
            boneControllers[i] = new MDLBoneController();
            boneControllers[i].read(ds);
        }
        
        //read hit boxes
        bs.rewind();
        ds.skipFully(header.hitboxOffset);
        hitboxes = new MDLHitBox[header.hitboxNb];
        for(int i=0;i<hitboxes.length;i++){
            hitboxes[i] = new MDLHitBox();
            hitboxes[i].read(ds);
        }
        
        //read local anims
        bs.rewind();
        ds.skipFully(header.localAnimOffset);
        anims = new MDLLocalAnim[header.localAnimNb];
        for(int i=0;i<anims.length;i++){
            anims[i] = new MDLLocalAnim();
            anims[i].read(ds);
        }
        
        //read local seqs
        bs.rewind();
        ds.skipFully(header.localSeqOffset);
        sequences = new MDLLocalSeq[header.localSeqNb];
        for(int i=0;i<sequences.length;i++){
            sequences[i] = new MDLLocalSeq();
            sequences[i].read(ds);
        }
        
        //read textures
        bs.rewind();
        ds.skipFully(header.textureOffset);
        textures = new MDLTexture[header.textureNb];
        for(int i=0;i<header.textureNb;i++){
            textures[i] = new MDLTexture();
            textures[i].read(ds);
        }
        //second pass : read texture names
        for(int i=0;i<header.textureNb;i++){
            bs.rewind();
            ds.skipFully(header.textureOffset+textures[i].nameOffset+i*MDLTexture.BYTE_SIZE);
            textures[i].name = ds.readZeroTerminatedChars(256, CharEncodings.US_ASCII);
        }
        
        //read textures directories
        textureLibPaths = new Chars[header.textureDirNb];
        for(int i=0;i<header.textureDirNb;i++){
            final int offset = header.texturedirOffset + i*4;
            bs.rewind();
            ds.skipFully(offset);
            final int offset2 = ds.readInt();
            bs.rewind();
            ds.skipFully(offset2);
            textureLibPaths[i] = ds.readZeroTerminatedChars(256, CharEncodings.UTF_8);
        }
        
        //read skins
        bs.rewind();
        ds.skipFully(header.skinReferenceOffset);
        skins = new MDLSkin[header.skinReferenceNb];
        for(int i=0;i<skins.length;i++){
            skins[i] = new MDLSkin();
            skins[i].read(ds);
        }
        
        //read mesh parts
        bs.rewind();
        ds.skipFully(header.bodyPartOffset);
        parts = new MDLMeshPart[header.bodyPartNb];
        for(int i=0;i<header.bodyPartNb;i++){
            parts[i] = new MDLMeshPart();
            parts[i].read(ds);
        }
        
        //second pass : read each part name
        for(int i=0;i<header.bodyPartNb;i++){
            bs.rewind();
            ds.skipFully(header.bodyPartOffset+parts[i].nameOffset+i*MDLMeshPart.BYTE_SIZE);
            parts[i].name = ds.readZeroTerminatedChars(256, CharEncodings.US_ASCII);
        }
        
        //third pass : read each part datas
        for(int i=0;i<header.bodyPartNb;i++){
            parts[i].datas = new MDLMeshPart.MDLModel[parts[i].subPartNb];
            final int partSubsOffset = header.bodyPartOffset + i*MDLMeshPart.BYTE_SIZE + parts[i].subPartOffset;
            
            bs.rewind();
            ds.skipFully(partSubsOffset);
            for(int k=0;k<parts[i].datas.length;k++){
                final MDLMeshPart.MDLModel data = new MDLMeshPart.MDLModel();
                parts[i].datas[k] = data;
                data.read(ds);
                
                //read sub blocks
                bs.rewind();
                ds.skipFully(partSubsOffset+data.meshOffset);
                data.meshes = new MDLMeshPart.MDLMesh[data.meshNb];
                for(int p=0;p<data.meshNb;p++){
                    data.meshes[p] = new MDLMeshPart.MDLMesh();
                    data.meshes[p].read(ds);
                }
            }
        }
        
        //read attachments
        bs.rewind();
        ds.skipFully(header.attachmentOffset);
        attachments = new MDLAttachment[header.attachmentNb];
        for(int i=0;i<attachments.length;i++){
            attachments[i] = new MDLAttachment();
            attachments[i].read(ds);
        }
        
        //read local nodes
        bs.rewind();
        ds.skipFully(header.localNodeOffset);
        localNodes = new MDLLocalNode[header.localNodeNb];
        for(int i=0;i<localNodes.length;i++){
            localNodes[i] = new MDLLocalNode();
            localNodes[i].read(ds);
        }
        
        //read flex descs
        bs.rewind();
        ds.skipFully(header.flexDescOffset);
        flexDescs = new MDLFlexDesc[header.flexDescNb];
        for(int i=0;i<flexDescs.length;i++){
            flexDescs[i] = new MDLFlexDesc();
            flexDescs[i].read(ds);
        }
        
        //read flex controllers
        bs.rewind();
        ds.skipFully(header.flexControllerOffset);
        flexControllers = new MDLFlexController[header.flexControllerNb];
        for(int i=0;i<flexControllers.length;i++){
            flexControllers[i] = new MDLFlexController();
            flexControllers[i].read(ds);
        }
        
        //read flex rules
        bs.rewind();
        ds.skipFully(header.flexRulesOffset);
        flexRules = new MDLFlexRule[header.flexRulesNb];
        for(int i=0;i<flexRules.length;i++){
            flexRules[i] = new MDLFlexRule();
            flexRules[i].read(ds);
        }
        
        //read IK Chains
        bs.rewind();
        ds.skipFully(header.ikChainOffset);
        ikChains = new MDLIKChain[header.ikChainNb];
        for(int i=0;i<ikChains.length;i++){
            ikChains[i] = new MDLIKChain();
            ikChains[i].read(ds);
        }
        
        //read mouths
        bs.rewind();
        ds.skipFully(header.mouthsOffset);
        mouths = new MDLMouth[header.mouthsNb];
        for(int i=0;i<mouths.length;i++){
            mouths[i] = new MDLMouth();
            mouths[i].read(ds);
        }
        
        //read local pose params
        bs.rewind();
        ds.skipFully(header.localPoseParamOffset);
        localPoseParams = new MDLLocalPoseParam[header.localPoseParamNb];
        for(int i=0;i<localPoseParams.length;i++){
            localPoseParams[i] = new MDLLocalPoseParam();
            localPoseParams[i].read(ds);
        }
        
        //read key-values
        bs.rewind();
        ds.skipFully(header.keyvalueOffset);
        keyValues = new MDLKeyValue[header.keyvalueNb];
        for(int i=0;i<keyValues.length;i++){
            keyValues[i] = new MDLKeyValue();
            keyValues[i].read(ds);
        }
        
        //read IK Locks
        bs.rewind();
        ds.skipFully(header.iklockOffset);
        iklocks = new MDLIKLock[header.iklockNb];
        for(int i=0;i<iklocks.length;i++){
            iklocks[i] = new MDLIKLock();
            iklocks[i].read(ds);
        }
        
        //read include models
        bs.rewind();
        ds.skipFully(header.includeModelOffset);
        includeModels = new MDLIncludeModel[header.includeModelNb];
        for(int i=0;i<includeModels.length;i++){
            includeModels[i] = new MDLIncludeModel();
            includeModels[i].read(ds);
        }
        
        //read anim blocks
        bs.rewind();
        ds.skipFully(header.animblocksOffset);
        animBlocks = new MDLAnimBlock[header.animblocksNb];
        for(int i=0;i<animBlocks.length;i++){
            animBlocks[i] = new MDLAnimBlock();
            animBlocks[i].read(ds);
        }
        
        //read flex controller uis
        bs.rewind();
        ds.skipFully(header.flexcontrolleruiOffset);
        flexcontrollerUis = new MDLFlexControllerUI[header.flexcontrolleruiNb];
        for(int i=0;i<flexcontrollerUis.length;i++){
            flexcontrollerUis[i] = new MDLFlexControllerUI();
            flexcontrollerUis[i].read(ds);
        }
        
    }
    
    public static Vector readVector3(DataInputStream ds) throws IOException{
        return new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
    }
    
    public static Vector readVector2(DataInputStream ds) throws IOException{
        return new Vector(ds.readFloat(), ds.readFloat());
    }
    
    public static Quaternion readQuaternion(DataInputStream ds) throws IOException{
        return new Quaternion(ds.readFloat(), ds.readFloat(), ds.readFloat(), ds.readFloat());
    }
    
}
