

package science.unlicense.impl.model3d.source.mdl2;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class MDLHeader {

    public Chars signature;
    public int version;
    public int checksum;
    public int flags;
    public Chars name;
    public int fileSize;
    public Vector eyePosition;
    public Vector illuPosition;
    public Vector hullMin;
    public Vector hullMax;
    public Vector bboxMin;
    public Vector bboxMax;


    // mstudiobone_t
    // Number of data sections (of type mstudiobone_t)
    public int boneNb;
    // Offset of first data section
    public int boneOffset;

    // mstudiobonecontroller_t
    public int boneControllerNb;
    public int boneControllerOffset;

    // mstudiohitboxset_t
    public int hitboxNb;
    public int hitboxOffset;

    // mstudioanimdesc_t
    public int localAnimNb;
    public int localAnimOffset;

    // mstudioseqdesc_t
    public int localSeqNb;
    public int localSeqOffset;

    public int activitylistversion;
    public int eventsindexed;

    // VMT texture filenames
    // mstudiotexture_t
    public int textureNb;
    public int textureOffset;

    // This offset points to a series of ints.
    // Each int value, in turn, is an offset relative to the start of this header/the-file,
    // At which there is a null-terminated string.
    public int textureDirNb;
    public int texturedirOffset;

    // Each skin-family assigns a texture-id to a skin location
    public int skinReferenceNb;
    public int skinFamilyNb;
    public int skinReferenceOffset;

    // mstudiobodyparts_t
    public int bodyPartNb;
    public int bodyPartOffset;

    // Local attachment points 
    // mstudioattachment_t
    public int attachmentNb;
    public int attachmentOffset;

    // Node values appear to be single bytes, while their names are null-terminated strings.
    public int localNodeNb;
    public int localNodeOffset;
    public int localNodeNameOffset;

    // mstudioflexdesc_t
    public int flexDescNb;
    public int flexDescOffset;

    // mstudioflexcontroller_t
    public int flexControllerNb;
    public int flexControllerOffset;

    // mstudioflexrule_t
    public int flexRulesNb;
    public int flexRulesOffset;

    // IK probably referse to inverse kinematics
    // mstudioikchain_t
    public int ikChainNb;
    public int ikChainOffset;

    // Information about any "mouth" on the model for speech animation
    // More than one sounds pretty creepy.
    // mstudiomouth_t
    public int mouthsNb;
    public int mouthsOffset;

    // mstudioposeparamdesc_t
    public int localPoseParamNb;
    public int localPoseParamOffset;
    
    /*
     * For anyone trying to follow along, as of this writing,
     * the next "surfacePropOffset" value is at position 0x0134 (308)
     * from the start of the file.
     */

    // Surface property value (single null-terminated string)
    public int surfacePropOffset;

    // Unusual: In this one index comes first, then count.
    // Key-value data is a series of strings. If you can't find
    // what you're interested in, check the associated PHY file as well.
    public int keyvalueOffset;
    public int keyvalueNb;	

    // More inverse-kinematics
    // mstudioiklock_t
    public int iklockNb;
    public int iklockOffset;


    public float mass;  // Mass of object (4-bytes)
    public int contents;	// ??

    // Other models can be referenced for re-used sequences and animations
    // (See also: The $includemodel QC option.)
    // mstudiomodelgroup_t
    public int includeModelNb;
    public int includeModelOffset;

    public int virtualModel;	// Placeholder for mutable-void*

    // mstudioanimblock_t
    public int animblocksNameOffset;
    public int animblocksNb;
    public int animblocksOffset;

    public int animblockModel; // Placeholder for mutable-void*

    // Points to a series of bytes?
    public int bonetablenameOffset;

    public int vertexBase;	// Placeholder for void*
    public int offsetBase;	// Placeholder for void*

    // Used with $constantdirectionallight from the QC 
    // Model should have flag #13 set if enabled
    public byte directionaldotproduct;

    public byte rootLod;	// Preferred rather than clamped

    // 0 means any allowed, N means Lod 0 -> (N-1)
    public byte numAllowedRootLods;	

    public byte unused1; // ??
    public int unused2; // ??

    // mstudioflexcontrollerui_t
    public int flexcontrolleruiNb;
    public int flexcontrolleruiOffset;

    /**
     * Offset for additional header information.
     * May be zero if not present, or also 408 if it immediately 
     * follows this studiohdr_t
     */
    // studiohdr2_t
    public int studiohdr2Offset;

    public int unused3; // ??
    
    public void read(DataInputStream ds) throws IOException{
        
        signature = new Chars(ds.readFully(new byte[4]));
        version = ds.readInt();
        checksum = ds.readInt(); //DOC difference
        name = ds.readBlockZeroTerminatedChars(64, CharEncodings.UTF_8);
        fileSize = ds.readInt();
        
        eyePosition = MDLReader.readVector3(ds);
        illuPosition = MDLReader.readVector3(ds);
        hullMin = MDLReader.readVector3(ds);
        hullMax = MDLReader.readVector3(ds);
        bboxMin = MDLReader.readVector3(ds);
        bboxMax = MDLReader.readVector3(ds);
        
        //doc seems wrong, flags are located before the file name
        flags = ds.readInt();
        
	boneNb = ds.readInt();
	boneOffset = ds.readInt();
        
	boneControllerNb = ds.readInt();
	boneControllerOffset = ds.readInt();
        
	hitboxNb = ds.readInt();
	hitboxOffset = ds.readInt();
        
	localAnimNb = ds.readInt();
	localAnimOffset = ds.readInt();
	localSeqNb = ds.readInt();
	localSeqOffset = ds.readInt();
        
	activitylistversion = ds.readInt();
	eventsindexed = ds.readInt();
        
	textureNb = ds.readInt();
	textureOffset = ds.readInt();
        
	textureDirNb = ds.readInt();
	texturedirOffset = ds.readInt();
 
	skinReferenceNb = ds.readInt();
	skinFamilyNb = ds.readInt();
	skinReferenceOffset = ds.readInt();
 
	bodyPartNb = ds.readInt();
	bodyPartOffset = ds.readInt();
 
	attachmentNb = ds.readInt();
	attachmentOffset = ds.readInt();
 
	localNodeNb = ds.readInt();
	localNodeOffset = ds.readInt();
	localNodeNameOffset = ds.readInt();
 
	flexDescNb = ds.readInt();
	flexDescOffset = ds.readInt();
 
	flexControllerNb = ds.readInt();
	flexControllerOffset = ds.readInt();
 
	flexRulesNb = ds.readInt();
	flexRulesOffset = ds.readInt();
 
	ikChainNb = ds.readInt();
	ikChainOffset = ds.readInt();
 
	mouthsNb = ds.readInt();
	mouthsOffset = ds.readInt();
 
	localPoseParamNb = ds.readInt();
	localPoseParamOffset = ds.readInt();
        
        /*
	 * For anyone trying to follow along, as of this writing,
	 * the next "surfacePropOffset" value is at position 0x0134 (308)
	 * from the start of the file.
	 */
	surfacePropOffset = ds.readInt();
 
	keyvalueOffset = ds.readInt();
	keyvalueNb = ds.readInt();
 
	iklockNb = ds.readInt();
	iklockOffset = ds.readInt();
 
	mass = ds.readFloat();
	contents = ds.readInt();
 
	includeModelNb = ds.readInt();
	includeModelOffset = ds.readInt();
 
	virtualModel = ds.readInt();
 
	animblocksNameOffset = ds.readInt();
	animblocksNb = ds.readInt();
	animblocksOffset = ds.readInt();
	animblockModel = ds.readInt();
 
	bonetablenameOffset = ds.readInt();
	vertexBase = ds.readInt();
	offsetBase = ds.readInt();
 
	directionaldotproduct = ds.readByte();
	rootLod = ds.readByte();
	numAllowedRootLods = ds.readByte();
	unused1 = ds.readByte();
	unused2 = ds.readInt();
 
	flexcontrolleruiNb = ds.readInt();
        flexcontrolleruiOffset = ds.readInt();
 
	studiohdr2Offset = ds.readInt();
	unused3 = ds.readInt();
        
        
    }
    
}
