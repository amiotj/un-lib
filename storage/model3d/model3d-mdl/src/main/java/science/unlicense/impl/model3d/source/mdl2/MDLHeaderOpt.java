

package science.unlicense.impl.model3d.source.mdl2;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MDLHeaderOpt {
    
    public int srcBoneTransformNb;
    public int srcBoneTransformOffset;
    public int illumPositionAttachmentIndex;
    public float flMaxEyeDeflection;
    public int linearBoneOffset;
    /** size 64 */
    public int[] unknowed;
    
    public void read(DataInputStream ds) throws IOException{
        srcBoneTransformNb           = ds.readInt();
        srcBoneTransformOffset       = ds.readInt();
        illumPositionAttachmentIndex = ds.readInt();
        flMaxEyeDeflection           = ds.readFloat();
        linearBoneOffset             = ds.readInt();
        unknowed                     = ds.readInt(64);
    }
    
}
