

package science.unlicense.impl.model3d.source.mdl2;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;

/**
 * TODO
 * Need to find doc on this.
 * 
 * @author Johann Sorel
 */
public class MDLBone {

    public static final int BYTE_SIZE = 216;
    
    public int nameOffset;
    public int parentBone;
    /** up to 6 */
    public int[] controllers;
    public Vector position;
    public Quaternion rotation;
    // compression scale
    public Vector positionFactor;
    public Vector rotationFactor;
    /** matrix 3*4 */
    public float[] poseMatrix;
    public Quaternion alignement;
    /** size 17*/
    public int[] unknowned;
    
    //readed by MDLReader afterward
    public Chars name;
    
    public void read(DataInputStream ds) throws IOException {
        nameOffset = ds.readInt();
        parentBone = ds.readInt();
        controllers = ds.readInt(6);
        position = MDLReader.readVector3(ds);
        rotation = MDLReader.readQuaternion(ds);
        positionFactor = MDLReader.readVector3(ds);
        rotationFactor = MDLReader.readVector3(ds);
        poseMatrix = ds.readFloat(12);
        alignement = MDLReader.readQuaternion(ds);
        unknowned = ds.readInt(17);
    }
    
}
