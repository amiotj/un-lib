

package science.unlicense.impl.model3d.source.vvd;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VVDHeader {
    
    public static final Chars SIGNATURE = new Chars("VSDI");
    
    public Chars signature;
    public int version;
    public int checksum;
    public int nbLod;
    /** 8 lod maximum*/
    public int[] nbLodVertex;
    public int nbFixup;
    public int fixupTableOffset;
    public int vertexTableOffset;
    public int tangentTableOffset;
    
    public void read(DataInputStream ds) throws IOException{
        signature           = new Chars(ds.readFully(new byte[4]));
        version             = ds.readInt();
        checksum            = ds.readInt();
        nbLod               = ds.readInt();
        nbLodVertex         = ds.readInt(8);
        nbFixup             = ds.readInt();
        fixupTableOffset    = ds.readInt();
        vertexTableOffset   = ds.readInt();
        tangentTableOffset  = ds.readInt();
    }
    
    
}
