
package science.unlicense.impl.model3d.source.mdl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class MDLHeader {
    
  /** file signature, must be 'IDPO' */
  public Chars signature;
  /** file version, expect 6 */
  public int version;
  /** model scale */
  public Vector scale;
  /** model translation*/ 
  public Vector translate;
  /** radius of the bounding sphere */
  public float boundingradius;
  /** ??? */
  public Vector somethingPosition;
  /** number of skins in the file */
  public int nbSkin;
  /** texture width, all have the same size */
  public int textureWidth;
  /** texture height, all have the same size */
  public int textureHeigth;
  /** number of vertices */
  public int nbVertex;
  /** number of triangles */
  public int nbTriangle;
  /** number of frames */
  public int nbFrame;
  /** sync ? with what ??? */
  public int sync;
  /** some flags */
  public int stat;
  /** file size ? */
  public float size;
  
  public final Sequence skins = new ArraySequence();
    
}
