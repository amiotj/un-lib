package science.unlicense.impl.model3d.source.mdl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import science.unlicense.impl.math.Vector;

/**
 * @author Johann Sorel
 */
public class MDLReader {

    private static final Chars SIGNATURE = new Chars(new byte[]{'I','D','P','O'});


    public static MDLHeader read(final Path f) throws Exception {

        final ByteInputStream inStream = f.createInputStream();
        final DataInputStream ds = new DataInputStream(inStream, NumberEncoding.BIG_ENDIAN);

        final MDLHeader store = new MDLHeader();

        store.signature = new Chars(ds.readFully(new byte[4]));
        store.version = ds.readInt();

        if(!SIGNATURE.equals(store.signature) || store.version != 6){
            throw new IOException("File is not a MDL.");
        }

        store.scale = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
        store.translate = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
        store.boundingradius = ds.readFloat();
        store.somethingPosition = new Vector(ds.readFloat(), ds.readFloat(), ds.readFloat());
        store.nbSkin = ds.readInt();
        store.textureWidth = ds.readInt();
        store.textureHeigth = ds.readInt();
        store.nbVertex = ds.readInt();
        store.nbTriangle = ds.readInt();
        store.nbFrame = ds.readInt();
        store.sync = ds.readInt();
        store.stat = ds.readInt();
        store.size = ds.readFloat();

        //calculate the size of a texture in bytes
        final int textureSize = store.textureWidth * store.textureHeigth;

        //read skins
        for(int i=0;i<store.nbSkin;i++){
            final MDLSkin skin = new MDLSkin();
            //read textures
            final int group = ds.readInt();
            if(group == 0){
                //single texture
                final MDLTexture texture = new MDLTexture();
                texture.data = new byte[textureSize];
                ds.readFully(texture.data);
                skin.textures.add(texture);

            }else if(group==1){
                //multiple
                int nb = ds.readInt();
                for(int k=0;k<nb;k++){
                    final MDLTexture texture = new MDLTexture();
                    texture.time = ds.readFloat();
                    texture.data = new byte[textureSize];
                    ds.readFully(texture.data);
                    skin.textures.add(texture);
                }
            }else{
                throw new IOException("unexpected texture group type :"+ group);
            }
        }

        //TODO read texture coordinates
        //TODO read triangles
        //TODO read vertices
        //TODO read frames


        return store;
    }

}
