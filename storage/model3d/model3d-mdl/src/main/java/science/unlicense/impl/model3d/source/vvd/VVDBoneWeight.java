

package science.unlicense.impl.model3d.source.vvd;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VVDBoneWeight {
    
    /** size 3 */
    public float[] weights;
    public int[] bones;
    public int nbBones;
    
    public void read(DataInputStream ds) throws IOException{
        weights = ds.readFloat(3);
        bones   = ds.readUByte(3);
        nbBones = ds.readUByte();
    }
    
}
