
package science.unlicense.impl.model3d.source.mdl;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Johann Sorel
 */
public class MDLSkin {
 
    public final List<MDLTexture> textures = new ArrayList<MDLTexture>();
    
}
