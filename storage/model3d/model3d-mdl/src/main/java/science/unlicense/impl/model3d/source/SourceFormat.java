

package science.unlicense.impl.model3d.source;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.io.IOException;

/**
 *
 * Source MDL format.
 * 
 * Docs :
 * https://developer.valvesoftware.com/wiki/MDL (uncomplete and contains bugs)
 * 
 * 
 * @author Johann Sorel
 */
public class SourceFormat extends AbstractModel3DFormat{

    public static final SourceFormat INSTANCE = new SourceFormat();

    private SourceFormat() {
        super(new Chars("SourceMDL"),
              new Chars("SourceMDL"),
              new Chars("SourceMDL"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("mdl")
              },
              new byte[][]{
                  {'I','D','S','T'}
              });
    }

    public Model3DStore open(Object input) throws IOException {
        final SourceStore store = new SourceStore(input);
        return store;
    }
    
}
