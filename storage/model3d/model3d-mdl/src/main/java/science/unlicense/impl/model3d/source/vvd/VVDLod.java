

package science.unlicense.impl.model3d.source.vvd;

import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class VVDLod {
    
    public VVDVertex[] vertices;
    /** vectors of size 4 */
    public Vector[] tangents;
    
}
