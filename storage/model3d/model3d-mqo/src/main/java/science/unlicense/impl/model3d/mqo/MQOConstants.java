
package science.unlicense.impl.model3d.mqo;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.parser.SyntaxNode;

/**
 * Metasequoia constants.
 * 
 * @author Johann Sorel
 */
public class MQOConstants {
    
    public static final Chars HEADER_TYPE = new Chars("Metasequoia Document");
    public static final Chars HEADER_VERSION = new Chars("Format Text");
    
    public static final Chars FORMAT_TEXT = new Chars("text");
    public static final Chars FORMAT_COMPRESS = new Chars("compress");
    
    
    public static final Chars TOKEN_SCENE       = new Chars("SCENE");
    public static final Chars TOKEN_BACKIMAGE   = new Chars("BACKIMAGE");
    public static final Chars TOKEN_OBJECT      = new Chars("OBJECT");
    public static final Chars TOKEN_MATERIAL    = new Chars("MATERIAL");
    public static final Chars TOKEN_VERTEX      = new Chars("VERTEX");
    public static final Chars TOKEN_FACE        = new Chars("FACE");
    public static final Chars TOKEN_NUMBER      = new Chars("NUMBER");
    public static final Chars TOKEN_WORD        = new Chars("WORD");
    public static final Chars TOKEN_ESCWORD     = new Chars("ESCWORD");

    public static final Chars RULE_VALUE       = new Chars("value");
    public static final Chars RULE_FUNCTION    = new Chars("function");
    public static final Chars RULE_PROPERTY    = new Chars("property");
    public static final Chars RULE_SCENE       = new Chars("scene");
    public static final Chars RULE_BACKIMAGE   = new Chars("backimage");
    public static final Chars RULE_MATERIAL    = new Chars("material");
    public static final Chars RULE_MATERIALS   = new Chars("materials");
    public static final Chars RULE_VERTEX      = new Chars("vertex");
    public static final Chars RULE_VERTICES    = new Chars("vertices");
    public static final Chars RULE_FACE        = new Chars("face");
    public static final Chars RULE_FACES       = new Chars("faces");
    public static final Chars RULE_OBJECT      = new Chars("object");
    public static final Chars RULE_INCLUDE     = new Chars("include");
    public static final Chars RULE_FILE        = new Chars("file");

    
    
    
    public static final Chars SCENE = new Chars("scene");
    public static final Chars SCENE_POS = new Chars("pos");
    public static final Chars SCENE_LOOKAT = new Chars("lookat");
    public static final Chars SCENE_HEAD = new Chars("head");
    public static final Chars SCENE_PICH = new Chars("pich");
    public static final Chars SCENE_ORTHO = new Chars("ortho");
    public static final Chars SCENE_ZOOM = new Chars("zoom");
    public static final Chars SCENE_AMB = new Chars("amb");
    
    public static final Chars MATERIAL = new Chars("material");
    public static final Chars MATERIAL_SHADER = new Chars("shader");
    public static final Chars MATERIAL_VCOL = new Chars("vcol");
    public static final Chars MATERIAL_DBLS = new Chars("dbls");
    public static final Chars MATERIAL_COL = new Chars("col");
    public static final Chars MATERIAL_DIF = new Chars("dif");
    public static final Chars MATERIAL_AMB = new Chars("amb");
    public static final Chars MATERIAL_EMI = new Chars("emi");
    public static final Chars MATERIAL_SPC = new Chars("spc");
    public static final Chars MATERIAL_POWER = new Chars("power");
    public static final Chars MATERIAL_TEX = new Chars("tex");
    public static final Chars MATERIAL_APLANE = new Chars("aplane");
    public static final Chars MATERIAL_BUMP = new Chars("bump");
    public static final Chars MATERIAL_PROJ_TYPE = new Chars("proj_type");
    public static final Chars MATERIAL_PROJ_POS = new Chars("proj_pos");	
    public static final Chars MATERIAL_PROJ_SCALE = new Chars("proj_scale");
    public static final Chars MATERIAL_PROJ_ANGLE = new Chars("proj_angle");
    
    public static final Chars OBJECT = new Chars("object");
    public static final Chars OBJECT_UID = new Chars("uid");
    public static final Chars OBJECT_DEPTH = new Chars("depth");
    public static final Chars OBJECT_FOLDING = new Chars("folding");
    public static final Chars OBJECT_SCALE = new Chars("scale");
    public static final Chars OBJECT_ROTATION = new Chars("rotation");
    public static final Chars OBJECT_TRANSLATION = new Chars("translation");
    public static final Chars OBJECT_PATCH = new Chars("patch");
    public static final Chars OBJECT_PATCHTRI = new Chars("patchtri");
    public static final Chars OBJECT_SEGMENT = new Chars("segment");
    public static final Chars OBJECT_VISIBLE = new Chars("visible");
    public static final Chars OBJECT_LOCKING = new Chars("locking");
    public static final Chars OBJECT_SHADING = new Chars("shading");
    public static final Chars OBJECT_FACET = new Chars("facet");
    public static final Chars OBJECT_COLOR = new Chars("color");
    public static final Chars OBJECT_COLOR_TYPE = new Chars("color_type");
    public static final Chars OBJECT_MIRROR = new Chars("mirror");
    public static final Chars OBJECT_MIRROR_AXIS = new Chars("mirror_axis");
    public static final Chars OBJECT_MIRROR_DIS = new Chars("mirror_dis");
    public static final Chars OBJECT_LATHE = new Chars("lathe");
    public static final Chars OBJECT_LATHE_AXIS = new Chars("lathe_axis");
    public static final Chars OBJECT_LATHE_SEG = new Chars("lathe_seg");    
    public static final Chars OBJECT_VERTEX = new Chars("vertex");
    public static final Chars OBJECT_BVERTEX = new Chars("bvertex");
    public static final Chars OBJECT_FACE = new Chars("face");
    
    public static final Chars EOF = new Chars("eof");
    
    
    
    public static float[] childAsFloat(SyntaxNode n) throws StoreException{
        final float[] array = new float[n.getChildren().getSize()];
        for(int i=0;i<array.length;i++){
            final SyntaxNode sn = (SyntaxNode)n.getChildren().get(i);
            final Token token = sn.getToken();
            if(token!=null){
                array[i] = (float)Float64.decode(token.value);
            }else{
                throw new StoreException("Unexpected node "+n);
            }
            
        }
        return array;
    }
    
    public static int[] childAsInt(SyntaxNode n) throws StoreException{
        final int[] array = new int[n.getChildren().getSize()];
        for(int i=0;i<array.length;i++){
            final SyntaxNode sn = (SyntaxNode)n.getChildren().get(i);
            final Token token = sn.getToken();
            if(token!=null){
                array[i] = Int32.decode(token.value);
            }else{
                throw new StoreException("Unexpected node "+n);
            }
        }
        return array;
    }
    
    public static float[] childValueAsFloat(SyntaxNode n) throws StoreException{
        final Sequence children = n.getChildrenByRule(RULE_VALUE);
        final float[] array = new float[children.getSize()];
        for(int i=0;i<array.length;i++){
            SyntaxNode sn = (SyntaxNode)children.get(i);
            sn = (SyntaxNode) sn.getChildren().get(0);
            final Token token = sn.getToken();
            if(token!=null){
                array[i] = (float)Float64.decode(token.value);
            }else{
                throw new StoreException("Unexpected node "+n);
            }
        }
        return array;
    }
    
    public static int[] childValueAsInt(SyntaxNode n) throws StoreException{
        final Sequence children = n.getChildrenByRule(RULE_VALUE);
        final int[] array = new int[children.getSize()];
        for(int i=0;i<array.length;i++){
            SyntaxNode sn = (SyntaxNode)children.get(i);
            sn = (SyntaxNode) sn.getChildren().get(0);
            final Token token = sn.getToken();
            if(token!=null){
                array[i] = Int32.decode(token.value);
            }else{
                throw new StoreException("Unexpected node "+n);
            }
        }
        return array;
    }
    
}
