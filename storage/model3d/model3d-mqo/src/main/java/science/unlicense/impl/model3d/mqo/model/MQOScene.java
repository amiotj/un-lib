
package science.unlicense.impl.model3d.mqo.model;

import science.unlicense.api.math.Tuple;


/**
 *
 * @author Johann Sorel
 */
public class MQOScene {
    
    public Tuple position;
    public Tuple lookAt;
    public float head;
    public float pich;
    public float ortho;
    public float zoom2;
    public Tuple ambiantColor;
    
}
