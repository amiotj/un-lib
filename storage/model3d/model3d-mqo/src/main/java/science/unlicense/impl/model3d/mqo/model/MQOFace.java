
package science.unlicense.impl.model3d.mqo.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.number.Int32;
import science.unlicense.api.parser.ParserStream;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.api.regex.NFAStep;
import static science.unlicense.api.parser.NFARuleState.*;
import static science.unlicense.api.parser.NFATokenState.*;
import static science.unlicense.impl.model3d.mqo.MQOConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class MQOFace {
 
    private static final Chars FCT_VERTEX = new Chars("V");
    private static final Chars FCT_MATERIAL = new Chars("M");
    private static final Chars FCT_UV = new Chars("UV");
    
    //3 or 4
    public int size;
    public int[] vertexIndex;
    public int materialIndex;
    public float[] uv;
    public int colorMask;
    //Catmull-Clark
    public int crs;

    void read(SyntaxNode ast) throws StoreException {

        size = Int32.decode( ((SyntaxNode)ast.getChildren().get(0)).getToken().value);
        
        final Sequence functions = ast.getChildrenByRule(RULE_FUNCTION);
        for(int i=0,n=functions.getSize();i<n;i++){
            final SyntaxNode sn = (SyntaxNode) functions.get(i);
            final Chars name = ((SyntaxNode)sn.getChildren().get(0)).getToken().value;
            
            if(FCT_VERTEX.equals(name)){
                vertexIndex = childValueAsInt(sn);
            }else if(FCT_UV.endsWith(name)){
                uv = childValueAsFloat(sn);
            }else if(FCT_MATERIAL.endsWith(name)){
                materialIndex = childValueAsInt(sn)[0];
            }
        }
    }
    
    public void read(ParserStream stream) throws IOException{
        for(NFAStep step=stream.next();step!=null;step=stream.next()){
            if(isToken(step.state, TOKEN_NUMBER)){
                size = Int32.decode( ((Token)step.value).value);
            }if(isRuleStart(step.state, RULE_FUNCTION)){
                final MQOFunction fct = new MQOFunction();
                fct.read(stream);
                if(FCT_VERTEX.equals(fct.name)){
                    vertexIndex = new int[fct.params.length];
                    for(int i=0;i<vertexIndex.length;i++){
                        vertexIndex[i] = ((Double)fct.params[i]).intValue();
                    }
                }else if(FCT_UV.endsWith(fct.name)){
                    uv = new float[fct.params.length];
                    for(int i=0;i<vertexIndex.length;i++){
                        uv[i] = ((Double)fct.params[i]).floatValue();
                    }
                }else if(FCT_MATERIAL.endsWith(fct.name)){
                    materialIndex = ((Double)fct.params[0]).intValue();
                }
                
            }else if(isRuleEnd(step.state, RULE_FACE)){
                break;
            }
        }
    }
    
}
