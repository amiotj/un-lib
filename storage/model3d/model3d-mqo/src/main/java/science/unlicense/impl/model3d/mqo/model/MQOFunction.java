
package science.unlicense.impl.model3d.mqo.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.parser.ParserStream;
import science.unlicense.api.regex.NFAStep;
import static science.unlicense.api.parser.NFARuleState.*;
import static science.unlicense.api.parser.NFATokenState.*;
import static science.unlicense.impl.model3d.mqo.MQOConstants.*;
import science.unlicense.impl.model3d.mqo.MQOStore;

/**
 *
 * @author Johann Sorel
 */
public class MQOFunction {

    public Chars name;
    public Object[] params = new Object[0];
    
    public void read(ParserStream stream) throws IOException{
        for(NFAStep step=stream.next();step!=null;step=stream.next()){
            if(isToken(step.state, TOKEN_WORD)){
                name = ((Token)step.value).value;
            }else if(isRuleStart(step.state, RULE_VALUE)){
                params = Arrays.insert(params, params.length, MQOStore.readValue(stream));
            }else if(isRuleEnd(step.state, RULE_FUNCTION)){
                break;
            }
        }
    }
    
}
