
package science.unlicense.impl.model3d.mqo;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.number.Float64;
import science.unlicense.api.parser.NFATokenState;
import science.unlicense.api.parser.ParserStream;
import science.unlicense.api.parser.Rule;
import science.unlicense.system.path.Paths;
import science.unlicense.impl.model3d.mqo.model.MQOFace;
import science.unlicense.impl.model3d.mqo.model.MQOMaterial;
import science.unlicense.impl.model3d.mqo.model.MQOObject;
import static science.unlicense.api.parser.NFARuleState.*;
import static science.unlicense.api.parser.NFATokenState.*;
import science.unlicense.api.regex.NFAStep;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import static science.unlicense.impl.model3d.mqo.MQOConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class MQOStore extends AbstractModel3DStore {

    private final Path base;
    
    private int matId = 0;
    private final Dictionary materials = new HashDictionary();
    private final Dictionary materialTexs = new HashDictionary();
    private final Sequence objects = new ArraySequence();
    

    public MQOStore(Object input) {
        super(MQOFormat.INSTANCE,input);
        this.base = (Path) input;
    }

    public Collection getElements() throws StoreException {
        if(objects.isEmpty()){
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        final GLNode node = rebuildRoot();
        final Sequence elements = new ArraySequence();
        elements.add(node);
        return elements;
    }
    
    private void read() throws IOException, StoreException{
        
        matId=0;
        materials.removeAll();
        materialTexs.removeAll();
        objects.removeAll();
        
        final ByteInputStream in = getSourceAsInputStream();        
        //parse text content
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/storage/model3d/mqo/mqo.gr")));
        final OrderedHashDictionary tokens = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokens, rules);
        
        // grammar parser is already tested        
        final Rule rule = (Rule) rules.getValue(new Chars("file"));
                
        //prepare lexer
        final Lexer lexer = new Lexer(CharEncodings.SHIFT_JIS);
        lexer.setInput(in);
        
        
        final TokenType[] toExclude = new TokenType[]{
            (TokenType) tokens.getValue(new Chars("EOF")),
            (TokenType) tokens.getValue(new Chars("HEADERA")),
            (TokenType) tokens.getValue(new Chars("HEADERB")),
            (TokenType) tokens.getValue(new Chars("WS")),
            (TokenType) tokens.getValue(new Chars("CRLF")),
            (TokenType) tokens.getValue(new Chars("AL")),
            (TokenType) tokens.getValue(new Chars("AR")),
            (TokenType) tokens.getValue(new Chars("PL")),
            (TokenType) tokens.getValue(new Chars("PR"))
        };
        
        final Predicate exclude = new Predicate() {
            public Boolean evaluate(Object candidate) {
                return candidate instanceof NFATokenState && 
                        Arrays.containsIdentity(toExclude, 0, toExclude.length, ((NFATokenState)candidate).getTokenType());
            }
        };
        
        final Rule lsRule = (Rule) rules.getValue(new Chars("ls"));
        
        //prepare parser
        final ParserStream stream = new ParserStream(lexer, rule, exclude);
        NFAStep step = stream.next();
        if(!isRuleStart(step.state, RULE_FILE)){
            throw new IOException("File rule not found");
        }
                
        for(step=stream.next();step!=null;step=stream.next()){
            if(isRuleStart(step.state, RULE_MATERIALS)){
                readMaterials(stream);
            }else if(isRuleStart(step.state, RULE_OBJECT)){
                final MQOObject obj = new MQOObject();
                obj.read(stream);
                objects.add(obj);
            }
        }
        
        //read textures
        final Iterator matite = materials.getPairs().createIterator();
        while(matite.hasNext()){
            final Pair pair = (Pair) matite.next();
            final int id = (Integer)pair.getValue1();
            final MQOMaterial material = (MQOMaterial)pair.getValue2();
            
            final Chars tex = material.getTex();
            if(tex!=null){
                final Path imgPath = base.getParent().resolve(tex);
                System.out.println(imgPath);
                final Image img;
                try {
                    img = Images.read(imgPath);
                    final Texture2D texture = new Texture2D(img);
                    materialTexs.add(id, texture);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    private void readMaterials(ParserStream stream) throws IOException{
        NFAStep step;
        for(step=stream.next();step!=null;step=stream.next()){
            if(isRuleStart(step.state, RULE_MATERIAL)){
                final MQOMaterial material = new MQOMaterial();
                materials.add(matId, material);
                matId++;
                material.read(stream);
            }else if(isRuleEnd(step.state, RULE_MATERIALS)){
                break;
            }
        }
    }
        
    public static Object readValue(ParserStream stream) throws IOException{
        Object val = null;
        NFAStep step;
        for(step=stream.next();step!=null;step=stream.next()){
            if(isToken(step.state, TOKEN_NUMBER)){
                val = Float64.decode(((Token)step.value).value);
            }else if(isToken(step.state, TOKEN_ESCWORD)){
                val = ((Token)step.value).value;
            }else if(isRuleEnd(step.state, MQOConstants.RULE_VALUE)){
                break;
            }
        }
        return val;
    }
    
    private GLNode rebuildRoot() throws StoreException{
        final GLNode root = new GLNode();
        for(int i=0,n=objects.getSize();i<n;i++){
            final MQOObject mo = (MQOObject) objects.get(i);
            root.getChildren().add(rebuildMesh(mo));
        }
        return root;
    }
    
    private GLNode rebuildMesh(MQOObject obj) throws StoreException{
        
        final GLNode mpm = new GLNode();
        
        //rebuild vertex buffer
        final int vertSize = obj.vertex.getSize();
        final FloatCursor vertBuffer = DefaultBufferFactory.INSTANCE.createFloat(vertSize*3).cursorFloat();
        final float[] fb = new float[3];
        for(int i=0,n=obj.vertex.getSize();i<n;i++){
            final Vector v = (Vector) obj.vertex.get(i);
            vertBuffer.write(v.toArrayFloat(fb));
        }
        final VBO vbo = new VBO(vertBuffer.getBuffer(), 3);
        
        //regroup faces by material
        final int nbFaces = obj.faces.getSize();
        final Dictionary groups = new HashDictionary();
        for(int i=0;i<nbFaces;i++){
            final MQOFace face = (MQOFace) obj.faces.get(i);
            Sequence seq = (Sequence) groups.getValue(face.materialIndex);
            if(seq==null){
                seq = new ArraySequence();
                groups.add(face.materialIndex, seq);
            }
            seq.add(face);
        }
        
        final Iterator ite = groups.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final int matIndex = (Integer)pair.getValue1();
            final Sequence faces = (Sequence) pair.getValue2();
            
            //rebuild faces
            final FloatCursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat(vertSize*2).cursorFloat();
            final IntSequence indexBuffer = new IntSequence();

            for(int i=0,n=faces.getSize();i<n;i++){
                final MQOFace face = (MQOFace)faces.get(i);

                if(face.size==3){
                    indexBuffer.put(face.vertexIndex);                                 
                }else if(face.size==4){
                    indexBuffer.put(face.vertexIndex[0]);
                    indexBuffer.put(face.vertexIndex[1]);
                    indexBuffer.put(face.vertexIndex[2]);

                    indexBuffer.put(face.vertexIndex[0]);
                    indexBuffer.put(face.vertexIndex[2]);
                    indexBuffer.put(face.vertexIndex[3]);
                    
                }else{
                    indexBuffer.put(face.vertexIndex[0]);
                    indexBuffer.put(face.vertexIndex[1]);
                    indexBuffer.put(face.vertexIndex[1]);
                    //throw new StoreException("Unsupported number of element in face : "+face.size);
                }
                
                if(face.uv!=null){
                    int k=0;
                    for(int id : face.vertexIndex){
                        uvBuffer.setPosition(id*2);
                        uvBuffer.write(face.uv[k]);
                        uvBuffer.write(face.uv[k+1]);
                        k+=2;
                    }
                }
            }

            //rebuild mesh
            final Mesh mesh = new Mesh();
            ((MeshRenderer)mesh.getRenderers().get(0)).getState().setCulling(-1);
            final Shell shell = new Shell();
            shell.setVertices(vbo); 
            shell.setUVs(new VBO(uvBuffer.getBuffer(), 2));
            shell.setIndexes(new IBO(indexBuffer.toArrayInt(), 3), IndexRange.TRIANGLES(0, indexBuffer.getSize()));
            shell.calculateNormals();
            mesh.setShape(shell);
            
            //rebuild material
            final Texture2D tex = (Texture2D) materialTexs.getValue(matIndex);
            if(tex!=null){
                mesh.getMaterial().putOrReplaceLayer(new Layer(new UVMapping(tex)));
            }
            
            mpm.getChildren().add(mesh);
        }
        
        return mpm;
    }
    
}
