
package science.unlicense.impl.model3d.mqo;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * Specification :
 * http://metaseq.net/en/format.html
 * 
 * @author Johann Sorel
 */
public class MQOFormat extends AbstractModel3DFormat{

    public static final MQOFormat INSTANCE = new MQOFormat();

    private MQOFormat() {
        super(new Chars("MQO"),
              new Chars("MQO"),
              new Chars("MQO"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("mqo")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new MQOStore(input);
    }

}
