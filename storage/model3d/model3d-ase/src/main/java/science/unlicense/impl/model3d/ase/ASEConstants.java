

package science.unlicense.impl.model3d.ase;

/**
 *
 * @author Johann Sorel
 */
public final class ASEConstants {
 
    public static final byte[] SIGNATURE = new byte[]{'*','3','D','S','M','A','X','_','A','S','C','I','I','E','X','P','O','R','T'};

    private ASEConstants() {
    }
    
}
