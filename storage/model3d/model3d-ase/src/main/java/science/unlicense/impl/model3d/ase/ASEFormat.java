/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package science.unlicense.impl.model3d.ase;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * 
 * Docs :
 * http://wiki.beyondunreal.com/Legacy:ASE_File_Format
 * 
 * @author Johann Sorel
 */
public class ASEFormat extends AbstractModel3DFormat{

    public static final ASEFormat INSTANCE = new ASEFormat();

    private ASEFormat() {
        super(new Chars("ASE"),
              new Chars("ASE"),
              new Chars("ASCII Scene Exporter"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("ase")
              },
              new byte[][]{ASEConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new ASEStore(input);
    }

}
