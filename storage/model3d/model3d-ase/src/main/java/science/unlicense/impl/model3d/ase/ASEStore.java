

package science.unlicense.impl.model3d.ase;

import science.unlicense.api.collection.Collection;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model3d.AbstractModel3DStore;

/**
 *
 * @author Johann Sorel
 */
public class ASEStore extends AbstractModel3DStore{

    public ASEStore(Object input) {
        super(ASEFormat.INSTANCE,input);
    }

    public Collection getElements() {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
