
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class MD5MeshReader extends AbstractReader {
    
    private CharInputStream stream;
    private int meshIndex = 0;
    private MD5Mesh mesh = null;
    
    public MD5Mesh read() throws IOException{
        mesh = new MD5Mesh();
        
        stream = getInputAsCharStream(CharEncodings.US_ASCII, new Char('\n'));
        
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            
            if(line.startsWith(MD5Constants.MD5_VERSION)){
                line = line.truncate(MD5Constants.MD5_VERSION.getCharLength(), line.getCharLength()).trim();
                mesh.version = Int32.decode(line);
            }else if(line.startsWith(MD5Constants.COMMAND_LINE)){
                line = line.truncate(MD5Constants.COMMAND_LINE.getCharLength(), line.getCharLength()).trim();
                mesh.commandline = line;
            }else if(line.startsWith(MD5Constants.NUM_JOINTS)){
                line = line.truncate(MD5Constants.NUM_JOINTS.getCharLength(), line.getCharLength()).trim();
                mesh.joints = new MD5Mesh.Joint[Int32.decode(line)];
            }else if(line.startsWith(MD5Constants.NUM_MESHES)){
                line = line.truncate(MD5Constants.NUM_MESHES.getCharLength(), line.getCharLength()).trim();
                mesh.meshes = new MD5Mesh.Mesh[Int32.decode(line)];
            }else if(line.startsWith(MD5Constants.JOINTS)){
                readJoints();
            }else if(line.startsWith(MD5Constants.MESH)){
                readMesh(meshIndex);
                meshIndex++;
            }
            
        }
        
        return mesh;
    }
    
    private void readJoints() throws IOException{
        int i = 0;
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            line = line.trim();
            if(line.startsWith(new Char('}'))){
                break;
            }
            
            line = line.replaceAll('\t', ' ');
            final Chars[] parts = line.split(' ');
            final MD5Mesh.Joint joint = new MD5Mesh.Joint();
            joint.name = parts[0].truncate(1, parts[0].getCharLength()-1);
            joint.parent = Int32.decode(parts[1]);
            joint.position = new Vector(
                    Float64.decode(parts[3]), 
                    Float64.decode(parts[4]), 
                    Float64.decode(parts[5]));
            joint.orientation = new Quaternion(
                    Float64.decode(parts[8]), 
                    Float64.decode(parts[9]), 
                    Float64.decode(parts[10]),
                    0);
            MD5Utilities.rebuildQuaternionW(joint.orientation);
            mesh.joints[i] = joint;
            
            i++;
        }
    }
    
    private void readMesh(int meshIndex) throws IOException{
        final MD5Mesh.Mesh part = new MD5Mesh.Mesh();
        
        int vindex = 0;
        int tindex = 0;
        int windex = 0;
        
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            line = line.trim();
            if(line.startsWith(new Char('}'))){
                break;
            }
            if(line.startsWith(MD5Constants.SHADER)){
                line = line.truncate(MD5Constants.SHADER.getCharLength(), line.getCharLength()).trim();
                part.shader = line.truncate(1, line.getCharLength()-1);
            }else if(line.startsWith(MD5Constants.NUM_VERTEX)){
                line = line.truncate(MD5Constants.NUM_VERTEX.getCharLength(), line.getCharLength()).trim();
                part.vertices = new MD5Mesh.Vertice[Int32.decode(line)];
            }else if(line.startsWith(MD5Constants.NUM_TRIANGLE)){
                line = line.truncate(MD5Constants.NUM_TRIANGLE.getCharLength(), line.getCharLength()).trim();
                part.triangles = new MD5Mesh.Triangle[Int32.decode(line)];
            }else if(line.startsWith(MD5Constants.NUM_WEIGHTS)){
                line = line.truncate(MD5Constants.NUM_WEIGHTS.getCharLength(), line.getCharLength()).trim();
                part.weights = new MD5Mesh.Weight[Int32.decode(line)];
            }else if(line.startsWith(MD5Constants.VERTEX)){
                final Chars[] parts = line.split(' ');
                final MD5Mesh.Vertice vertex = new MD5Mesh.Vertice();
                vertex.index = Int32.decode(parts[1]);
                vertex.textureUV = new Vector(
                    Float64.decode(parts[3]), 
                    Float64.decode(parts[4]));
                vertex.weightStart = Int32.decode(parts[6]);
                vertex.weightCount = Int32.decode(parts[7]);
                part.vertices[vindex] = vertex;
                vindex++;
            }else if(line.startsWith(MD5Constants.TRIANGLE)){
                final Chars[] parts = line.split(' ');
                final MD5Mesh.Triangle triangle = new MD5Mesh.Triangle();
                triangle.index = Int32.decode(parts[1]);
                triangle.verticeIndexes = new int[]{
                    Int32.decode(parts[2]),
                    Int32.decode(parts[3]),
                    Int32.decode(parts[4])
                };
                part.triangles[tindex] = triangle;
                tindex++;
            }else if(line.startsWith(MD5Constants.WEIGHT)){
                final Chars[] parts = line.split(' ');
                final MD5Mesh.Weight weight = new MD5Mesh.Weight();
                weight.index = Int32.decode(parts[1]);
                weight.jointIndex = Int32.decode(parts[2]);
                weight.weightBias = (float)Float64.decode(parts[3]);
                weight.weightPosition = new Vector(
                    Float64.decode(parts[5]), 
                    Float64.decode(parts[6]),
                    Float64.decode(parts[7]));
                part.weights[windex] = weight;
                windex++;
            }
            
        }
        
        mesh.meshes[meshIndex] = part;
    }
    
}
