
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class MD5Format extends AbstractModel3DFormat{

    public static final MD5Format INSTANCE = new MD5Format();

    private MD5Format() {
        super(new Chars("MD5"),
              new Chars("MD5"),
              new Chars("MD5"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("md5mesh")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        final MD5Store store = new MD5Store(input);
        return store;
    }

}
