
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.engine.opengl.physic.JointTimeSerie;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.physic.SkeletonAnimation;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class MD5Store extends AbstractModel3DStore{

    private MD5Mesh mesh;
    private MD5Animation anim;

    public MD5Store(Object input) {
        super(MD5Format.INSTANCE,input);
    }

    public Collection getElements() {

        if(mesh==null){
            readModel();
        }

        //rebuild the skeleton
        final Skeleton skeleton = new Skeleton();
        for(int i=0;i<mesh.joints.length;i++){
            final MD5Mesh.Joint meshjoint = mesh.joints[i];
            final Joint joint = new Joint(3,meshjoint.name);
            joint.getNodeTransform().getRotation().set(meshjoint.orientation.toMatrix3());
            joint.getNodeTransform().getTranslation().set(meshjoint.position);
            joint.getNodeTransform().notifyChanged();
            if(meshjoint.parent>=0){
                final Chars parentName = mesh.joints[meshjoint.parent].name;
                final Joint parent = skeleton.getJoint(parentName);
                parent.getChildren().add(joint);
            }else{
                //root joint
                skeleton.getChildren().add(joint);
            }
        }

        //rebuild mesh
        final MultipartMesh root = new MultipartMesh();

        try{
            for(int m=0;m<mesh.meshes.length;m++){
                final MD5Mesh.Mesh part = mesh.meshes[m];
                final Mesh node = buildMeshPart(part, skeleton);
                root.getChildren().add(node);
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }

        //rebuild animation
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
        final SkeletonAnimation glanim = buildAnimation(skeleton);
        root.getUpdaters().add(glanim);
        root.setSkeleton(skeleton);

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private void readModel(){
        final MD5MeshReader meshReader = new MD5MeshReader();
        try{
            meshReader.setInput(input);
            mesh = meshReader.read();
        }catch(IOException ex){
            ex.printStackTrace();
        }

        final MD5AnimationReader animReader = new MD5AnimationReader();
        try{
            final Path pi = ((Path)input);
            Path p = pi.getParent().resolve(pi.getName().replaceAll(new Chars("md5mesh"), new Chars("md5anim")));
            animReader.setInput(p);
            anim = animReader.read();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    private Mesh buildMeshPart(final MD5Mesh.Mesh part, Skeleton skeleton) throws IOException{
        final Mesh glmesh = new Mesh();

        final FloatCursor vertexBuffer = DefaultBufferFactory.INSTANCE.createFloat(part.vertices.length*3).cursorFloat();
        final FloatCursor normalBuffer = DefaultBufferFactory.INSTANCE.createFloat(part.vertices.length*3).cursorFloat();
        final FloatCursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat(part.vertices.length*2).cursorFloat();
        final IntCursor indexBuffer = DefaultBufferFactory.INSTANCE.createInt(part.triangles.length*3).cursorInt();

        int maxweights = 0;
        //calculate vertex positions
        for(int i=0;i<part.vertices.length;i++){
            final MD5Mesh.Vertice mv = part.vertices[i];
            mv.position = new Vector(3);
            mv.normal = new Vector(3);
            maxweights = Maths.max(maxweights,mv.weightCount);

            for(int w=0;w<mv.weightCount;w++){
                final MD5Mesh.Weight weight = part.weights[mv.weightStart+w];
                final MD5Mesh.Joint joint = mesh.joints[weight.jointIndex];
                //use weigth to calculate vertex position
                final Vector rotpos = joint.orientation.rotate(weight.weightPosition,null);
                final Vector sumpos = joint.position.add(rotpos, null).localScale(weight.weightBias);
                mv.position.localAdd(sumpos);
            }

            vertexBuffer.write(mv.position.toArrayFloat());
            uvBuffer.write(mv.textureUV.toArrayFloat());
        }
        //we must have at least 2 weights
        maxweights = Maths.max(2, maxweights);

        //calculate normals
        for(int i=0;i<part.triangles.length;i++){
            final MD5Mesh.Triangle triangle = part.triangles[i];
            final Vector v0 = part.vertices[triangle.verticeIndexes[0]].position;
            final Vector v1 = part.vertices[triangle.verticeIndexes[1]].position;
            final Vector v2 = part.vertices[triangle.verticeIndexes[2]].position;
            final Vector v2v0 = v2.subtract(v0, null);
            final Vector v1v0 = v1.subtract(v0, null);
            final Vector normal = v2v0.cross(v1v0, null);
            part.vertices[triangle.verticeIndexes[0]].normal.localAdd(normal);
            part.vertices[triangle.verticeIndexes[1]].normal.localAdd(normal);
            part.vertices[triangle.verticeIndexes[2]].normal.localAdd(normal);
            indexBuffer.write(triangle.verticeIndexes);
        }

        //normalize normals
        for(int i=0;i<part.vertices.length;i++){
            final MD5Mesh.Vertice mv = part.vertices[i];
            final Vector normal = part.vertices[i].normal.normalize(null);
            part.vertices[i].normal.setToZero();

            //set the bind pose normal
            for(int w=0;w<mv.weightCount;w++){
                final MD5Mesh.Weight weight = part.weights[mv.weightStart+w];
                final MD5Mesh.Joint joint = mesh.joints[weight.jointIndex];
                final Vector temp = joint.orientation.rotate(normal, null).localScale(weight.weightBias);
                part.vertices[i].normal.localAdd(temp);
            }

            normalBuffer.write(normal.toArrayFloat());
        }

        //load texture
        final Image image = Images.read(Paths.resolve(part.shader));
        final UVMapping paint = new UVMapping(new Texture2D(image));
        glmesh.getMaterial().putOrReplaceLayer(new Layer(paint));

        //build the skin
        final SkinShell skin = new SkinShell();
        final FloatCursor weightBuffer = DefaultBufferFactory.INSTANCE.createFloat(part.vertices.length*maxweights).cursorFloat();
        final IntCursor jointBuffer = DefaultBufferFactory.INSTANCE.createInt(part.vertices.length*maxweights).cursorInt();
        for(int i=0;i<part.vertices.length;i++){
            final MD5Mesh.Vertice mv = part.vertices[i];

            int w=0;
            for(;w<mv.weightCount;w++){
                final MD5Mesh.Weight weight = part.weights[mv.weightStart+w];
                weightBuffer.write(weight.weightBias);
                jointBuffer.write(weight.jointIndex);
            }
            //fill remaining weigths with zeros
            for(;w<maxweights;w++){
                weightBuffer.write(0f);
                jointBuffer.write(0);
            }
        }

        skin.setSkeleton(skeleton);
        skin.setMaxWeightPerVertex(maxweights);
        skin.setVertices(new VBO(vertexBuffer.getBuffer(),3));
        skin.setNormals(new VBO(normalBuffer.getBuffer(),3));
        skin.setUVs(new VBO(uvBuffer.getBuffer(), 2));
        skin.setWeights(new VBO(weightBuffer.getBuffer(),maxweights));
        skin.setJointIndexes(new VBO(jointBuffer.getBuffer(),maxweights));
        skin.setIndexes(new IBO(indexBuffer.getBuffer(),3), IndexRange.TRIANGLES(0, (int) indexBuffer.getBuffer().getPrimitiveCount()));

        //build mesh object
//        final DirectShell shell = new DirectShell();
//        shell.setVertexBuffer(vertexBuffer);
//        shell.setNormalBuffer(normalBuffer);
//        shell.setIndexBuffer(indexBuffer);
//        glmesh.setShell(shell);
        glmesh.setShape(skin);
        glmesh.getShape().calculateBBox();

        return glmesh;
    }

    private SkeletonAnimation buildAnimation(final Skeleton meshSkeleton){
        final SkeletonAnimation glanim = new SkeletonAnimation(meshSkeleton);

        //build the base skeleton
        for(int i=0;i<anim.joints.length;i++){
            final MD5Animation.Joint animJoint = anim.joints[i];
            final Joint gljoint = meshSkeleton.getJoint(animJoint.name);
            gljoint.getNodeTransform().getRotation().set(anim.baseFrame.orientation[i].toMatrix3());
            gljoint.getNodeTransform().getTranslation().set(anim.baseFrame.position[i]);
            gljoint.getNodeTransform().notifyChanged();
        }

        //build each joint serie
        for(int i=0;i<anim.joints.length;i++){
            final MD5Animation.Joint animJoint = anim.joints[i];
            final Joint gljoint = meshSkeleton.getJoint(animJoint.name);

            //create a time serie for each joint
            final JointTimeSerie serie = new JointTimeSerie(gljoint);
            glanim.getSeries().add(serie);

            //create each frame
            for(int k=0;k<anim.frames.length;k++){
                final MD5Animation.Frame frame = anim.frames[k];
                final JointKeyFrame glframe = new JointKeyFrame();
                glframe.setValue(new NodeTransform(3));
                glframe.setTime((int)( (float)k/anim.frameRate*1000));
                glframe.setJoint(gljoint);
                final NodeTransform poseTrs = glframe.getValue();
                poseTrs.getTranslation().set(anim.baseFrame.position[i]);
                
                final Quaternion q = new Quaternion();

                int offset = 0;
                if((animJoint.flags &  1) != 0) poseTrs.getTranslation().setX(frame.data[animJoint.startIndex+offset++]);
                if((animJoint.flags &  2) != 0) poseTrs.getTranslation().setY(frame.data[animJoint.startIndex+offset++]);
                if((animJoint.flags &  4) != 0) poseTrs.getTranslation().setZ(frame.data[animJoint.startIndex+offset++]);
                if((animJoint.flags &  8) != 0) q.setX(frame.data[animJoint.startIndex+offset++]);
                if((animJoint.flags & 16) != 0) q.setY(frame.data[animJoint.startIndex+offset++]);
                if((animJoint.flags & 32) != 0) q.setZ(frame.data[animJoint.startIndex+offset++]);
                MD5Utilities.rebuildQuaternionW(q);

                poseTrs.getRotation().set(q.toMatrix3());
                serie.getFrames().add(glframe);
                poseTrs.notifyChanged();
            }
        }

        return glanim;
    }

}
