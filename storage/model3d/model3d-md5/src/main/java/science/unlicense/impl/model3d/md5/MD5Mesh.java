
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;

/**
 * MD5 mesh.
 * 
 * @author Johann Sorel
 */
public class MD5Mesh {
    
    public int version;
    public Chars commandline;
    /**
     * Skeleton joints.
     */
    public Joint[] joints;
    /**
     * Mesh part, one by texture.
     */
    public Mesh[] meshes;
    
    public static final class Joint{
        public Chars name;
        public int parent;
        public Vector position;
        public Quaternion orientation;
    }
    
    public static final class Mesh{
        public Chars shader;
        public Vertice[] vertices;
        public Triangle[] triangles;
        public Weight[] weights;
    }
    
    public static final class Vertice{
        public int index;
        public Vector textureUV; //size 2
        public int weightStart;
        public int weightCount;
        
        //calculated field
        public Vector position;
        public Vector normal;
    }
    
    public static final class Triangle{
        public int index;
        public int[] verticeIndexes; //size 3
    }
    
    public static final class Weight{
        public int index;
        public int jointIndex;
        /** Bias is the sum of all weights ,it should be 1 for a vertex */
        public float weightBias;
        public Vector weightPosition; //size 3
    }
    
}
