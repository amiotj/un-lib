
package science.unlicense.impl.model3d.md5;

import science.unlicense.impl.math.Quaternion;

/**
 *
 * @author Johann Sorel
 */
public class MD5Utilities {
    
    public static void rebuildQuaternionW(Quaternion q){
        final double x = q.getX();
        final double y = q.getY();
        final double z = q.getZ();
        final double t = 1.0 - (x*x) - (y*y) - (z*z); 
        final double w = (t<0)? 0 : -Math.sqrt(t);
        q.setW(w);
    }
    
}
