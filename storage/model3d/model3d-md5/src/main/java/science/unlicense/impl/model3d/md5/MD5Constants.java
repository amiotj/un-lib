
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class MD5Constants {
    
    public static final Chars MD5_VERSION = new Chars("MD5Version");
    public static final Chars COMMAND_LINE = new Chars("commandline");
    public static final Chars NUM_JOINTS = new Chars("numJoints");
    
    //Mesh constants
    public static final Chars NUM_MESHES = new Chars("numMeshes");
    public static final Chars JOINTS = new Chars("joints");
    public static final Chars MESH = new Chars("mesh");
    public static final Chars SHADER = new Chars("shader");
    public static final Chars VERTEX = new Chars("vert");
    public static final Chars NUM_VERTEX = new Chars("numverts");
    public static final Chars NUM_TRIANGLE = new Chars("numtris");
    public static final Chars TRIANGLE = new Chars("tri");
    public static final Chars NUM_WEIGHTS = new Chars("numweights");
    public static final Chars WEIGHT = new Chars("weight");
    
    //Anim constants
    public static final Chars NUM_FRAMES = new Chars("numFrames");
    public static final Chars FRAME_RATE = new Chars("frameRate");
    public static final Chars NUM_ANIMATEDCOMP = new Chars("numAnimatedComponents");
    public static final Chars HIERARCHY = new Chars("hierarchy");
    public static final Chars BOUNDS = new Chars("bounds");
    public static final Chars BASEFRAME = new Chars("baseframe");
    public static final Chars FRAME = new Chars("frame");
    
    
    
    private MD5Constants() {}
    
}
