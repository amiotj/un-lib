
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;

/**
 * MD5 animation reader.
 * 
 * @author Johann Sorel
 */
public class MD5AnimationReader extends AbstractReader {
    
    private CharInputStream stream;
    private int frameIndex = 0;
    private MD5Animation animation = null;
    
    public MD5Animation read() throws IOException{
        animation = new MD5Animation();
        
        stream = getInputAsCharStream(CharEncodings.US_ASCII, new Char('\n'));
        
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            
            if(line.startsWith(MD5Constants.MD5_VERSION)){
                line = line.truncate(MD5Constants.MD5_VERSION.getCharLength(), line.getCharLength()).trim();
                animation.version = Int32.decode(line);
            }else if(line.startsWith(MD5Constants.COMMAND_LINE)){
                line = line.truncate(MD5Constants.COMMAND_LINE.getCharLength(), line.getCharLength()).trim();
                animation.commandline = line;
            }else if(line.startsWith(MD5Constants.NUM_FRAMES)){
                line = line.truncate(MD5Constants.NUM_FRAMES.getCharLength(), line.getCharLength()).trim();
                animation.frames = new MD5Animation.Frame[Int32.decode(line)];
                animation.bounds = new BBox[Int32.decode(line)];
            }else if(line.startsWith(MD5Constants.NUM_JOINTS)){
                line = line.truncate(MD5Constants.NUM_JOINTS.getCharLength(), line.getCharLength()).trim();
                animation.joints = new MD5Animation.Joint[Int32.decode(line)];
                animation.baseFrame = new MD5Animation.BaseFrame();
                animation.baseFrame.position = new Vector[animation.joints.length];
                animation.baseFrame.orientation = new Quaternion[animation.joints.length];
            }else if(line.startsWith(MD5Constants.FRAME_RATE)){
                line = line.truncate(MD5Constants.FRAME_RATE.getCharLength(), line.getCharLength()).trim();
                animation.frameRate = Int32.decode(line);
            }else if(line.startsWith(MD5Constants.NUM_ANIMATEDCOMP)){
                line = line.truncate(MD5Constants.NUM_ANIMATEDCOMP.getCharLength(), line.getCharLength()).trim();
                animation.numAnimatedComponents = Int32.decode(line);
            }else if(line.startsWith(MD5Constants.HIERARCHY)){
                readHierarchy();
            }else if(line.startsWith(MD5Constants.BOUNDS)){
                readBounds();
            }else if(line.startsWith(MD5Constants.BASEFRAME)){
                readBaseFrame();
            }else if(line.startsWith(MD5Constants.FRAME)){
                readFrame();
                frameIndex++;
            }
            
        }
        
        return animation;
    }
    
    private void readHierarchy() throws IOException{
        int i = 0;
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            line = line.trim();
            if(line.startsWith(new Char('}'))){
                break;
            }
            
            line = line.replaceAll('\t', ' ');
            final Chars[] parts = line.split(' ');
            final MD5Animation.Joint joint = new MD5Animation.Joint();
            joint.name = parts[0].truncate(1, parts[0].getCharLength()-1);
            joint.parent = Int32.decode(parts[1]);
            joint.flags = Int32.decode(parts[2]);
            joint.startIndex = Int32.decode(parts[3]);
            animation.joints[i] = joint;
            
            i++;
        }
    }
    
    private void readBounds() throws IOException{
        int i = 0;
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            line = line.trim();
            if(line.startsWith(new Char('}'))){
                break;
            }
            
            line = line.replaceAll('\t', ' ');
            final Chars[] parts = line.split(' ');
            final BBox bbox = new BBox(3);
            bbox.setRange(0, Float64.decode(parts[1]), Float64.decode(parts[6]));
            bbox.setRange(1, Float64.decode(parts[2]), Float64.decode(parts[7]));
            bbox.setRange(2, Float64.decode(parts[3]), Float64.decode(parts[8]));
            animation.bounds[i] = bbox;
            
            i++;
        }
    }
    
    private void readBaseFrame() throws IOException{
        int i = 0;
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            line = line.trim();
            
            if(line.startsWith(new Char('}'))){
                break;
            }
            
            line = line.replaceAll('\t', ' ');
            final Chars[] parts = line.split(' ');
            
            animation.baseFrame.position[i] = new Vector(
                    Float64.decode(parts[1]),
                    Float64.decode(parts[2]),
                    Float64.decode(parts[3])
                    );
            animation.baseFrame.orientation[i] = new Quaternion(
                    Float64.decode(parts[6]),
                    Float64.decode(parts[7]),
                    Float64.decode(parts[8]),
                    0
                    );
            MD5Utilities.rebuildQuaternionW(animation.baseFrame.orientation[i]);
            
            i++;
        }
    }
    
    private void readFrame() throws IOException{
        final MD5Animation.Frame frame = new MD5Animation.Frame();
        frame.data = new float[animation.numAnimatedComponents];
        
        int i = 0;
        for(Chars line = stream.readLine();line!=null;line=stream.readLine()){
            line = line.trim();
            if(line.startsWith(new Char('}'))){
                break;
            }
            
            line = line = line.replaceAll('\t', ' ');
            final Chars[] parts = line.split(' ');
            
            for(int k=0;k<parts.length;k++){
                frame.data[i] = (float)Float64.decode(parts[k]);
                i++;
            }
            
        }
        
        animation.frames[frameIndex] = frame;
    }
    
}
