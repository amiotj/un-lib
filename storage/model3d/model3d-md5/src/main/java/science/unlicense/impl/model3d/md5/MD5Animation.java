
package science.unlicense.impl.model3d.md5;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;

/**
 * MD5 animation.
 * 
 * @author Johann Sorel
 */
public class MD5Animation {
    
    public int version;
    public Chars commandline;
    /**
     * number of frame per second.
     */
    public int frameRate;
    public int numAnimatedComponents;

    /**
     * Animation joints.
     */
    public Joint[] joints;
    /**
     * Define the bounding box of the model for each frame.
     */
    public BBox[] bounds;
    /**
     * Define the joint position and rotation before frame data modification.
     */
    public BaseFrame baseFrame;
    /**
     * Each frame transformation.
     */
    public Frame[] frames;

    /**
     * An animation joint.
     */
    public static final class Joint{
        public Chars name;
        /**
         * parent joint index, -1 for root joint 
         */
        public int parent;
        /** 
         * Indication of how to build position and rotation.
         * Bit 1 : the x value of the baseframe position must be replace by startIndex+0 frame data value.
         * Bit 2 : the y value of the baseframe position must be replace by startIndex+1 frame data value.
         * Bit 3 : the z value of the baseframe position must be replace by startIndex+2 frame data value.
         * Bit 4 : the x value of the baseframe rotation must be replace by startIndex+3 frame data value.
         * Bit 5 : the y value of the baseframe rotation must be replace by startIndex+4 frame data value.
         * Bit 6 : the z value of the baseframe rotation must be replace by startIndex+5 frame data value.
         */
        public int flags;
        /**
         * first index of values for this joint in the frame data array 
         */
        public int startIndex;
    }
        
    /**
     * Animation base frame. Holds the position and rotation of 
     * each joint at 'sleep'.
     */
    public static final class BaseFrame{
        public Vector[] position; //size numjoints
        public Quaternion[] orientation; //size numjoints
    }
    
    /**
     * Animation frame.
     */
    public static final class Frame{
        public float[] data; //size numAnimatedComponents
    }
    
}
