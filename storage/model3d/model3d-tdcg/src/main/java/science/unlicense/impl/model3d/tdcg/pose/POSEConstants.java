
package science.unlicense.impl.model3d.tdcg.pose;

/**
 *
 * @author Johann Sorel
 */
public class POSEConstants {
    
    public static final byte[] SIGNATURE = new byte[]{'T','A','H','2'};
    
}
