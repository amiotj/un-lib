
package science.unlicense.impl.model3d.tdcg.tah;

/**
 *
 * @author Johann Sorel
 */
public class TAHConstants {
    
    public static final byte[] SIGNATURE = new byte[]{'T','A','H','2'};
    
}
