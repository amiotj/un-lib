
package science.unlicense.impl.model3d.tdcg.tso;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class TSOConstants {
    
    public static final byte[] SIGNATURE = new byte[]{'T','S','O','1'};
    
    public static Matrix4x4 readMatrix(DataInputStream ds) throws IOException{
        final Matrix4x4 m = new Matrix4x4(
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat(),
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat(),
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat(),
                    ds.readFloat(),ds.readFloat(),ds.readFloat(),ds.readFloat()  );
        //matrix are stored in column order, we must transpose it since our implementation
        //was expecting row order in the constructor
        return m.transpose();
    }
    
}
