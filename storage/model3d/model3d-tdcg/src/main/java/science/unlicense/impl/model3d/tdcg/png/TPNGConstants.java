
package science.unlicense.impl.model3d.tdcg.png;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class TPNGConstants {
    
    public static final Chars CHUNK_TaOb = new Chars(new byte[]{'t','a','O','b'});
    
    public static final Chars TYPE_TDCG = new Chars("TDCG");
    public static final Chars TYPE_HSAV = new Chars("HSAV");
    public static final Chars TYPE_FTSO = new Chars("FTSO");
    
    
    private TPNGConstants(){}
    
}
