
package science.unlicense.impl.model3d.tdcg.tso;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.model3d.tdcg.tso.model.TSOMaterial;
import science.unlicense.impl.model3d.tdcg.tso.model.TSOMesh;
import science.unlicense.impl.model3d.tdcg.tso.model.TSOShader;
import science.unlicense.impl.model3d.tdcg.tso.model.TSOTexture;

/**
 * TSO reader. 
 * 
 * Note : the file structure is very similar to PMD.
 * 
 * 
 * @author Johann Sorel
 */
public class TSOReader extends AbstractReader{
    
    private Chars[] names;
    private Matrix4x4[] transforms;
    private TSOTexture[] textures;
    private TSOShader[] shaders;
    private TSOMaterial[] materials;
    private TSOMesh[] meshes;
    
    public Object read() throws IOException{
        decodeFile();
        return rebuildModel();
    }
    
    private void decodeFile() throws IOException{
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        
        if(!Arrays.equals(TSOConstants.SIGNATURE, ds.readFully(new byte[4]))){
            throw new IOException("Stream is not a TSO.");
        }
        
        //read scene objets names/path
        names = new Chars[ds.readInt()];
        for(int i=0;i<names.length;i++){
            names[i] = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        }
        //read object transforms
        transforms = new Matrix4x4[ds.readInt()];
        for(int i=0;i<transforms.length;i++){
            transforms[i] = TSOConstants.readMatrix(ds);
        }
        //read textures
        textures = new TSOTexture[ds.readInt()];
        for(int i=0;i<textures.length;i++){
            textures[i] = new TSOTexture();
            textures[i].read(ds);
        }
        //read shaders
        shaders = new TSOShader[ds.readInt()];
        for(int i=0;i<shaders.length;i++){
            shaders[i] = new TSOShader();
            shaders[i].read(ds);
        }
        //read materials
        materials = new TSOMaterial[ds.readInt()];
        for(int i=0;i<materials.length;i++){
            materials[i] = new TSOMaterial();
            materials[i].read(ds);
        }
        //read meshes
        meshes = new TSOMesh[ds.readInt()];
        for(int i=0;i<meshes.length;i++){
            meshes[i] = new TSOMesh();
            meshes[i].read(ds);
        }
    }
    
    /**
     * TODO, rebuild this better.
     */
    private Object rebuildModel(){
        
        GLNode node = new GLNode();
        
        for(int i=0;i<meshes.length;i++){
            final Mesh mesh = new Mesh();
            final Shell shell = new Shell();
            mesh.setShape(shell);
            mesh.getNodeTransform().set(meshes[i].trs);
            node.getChildren().add(mesh);
            
            final FloatSequence vertices = new FloatSequence();
            final FloatSequence normals = new FloatSequence();
            final FloatSequence uvs = new FloatSequence();
            final IntSequence index = new IntSequence();
            int cnt = 0;
            for(int p=0;p<meshes[i].parts.length;p++){
                for(int v=0;v<meshes[i].parts[p].vertices.length;v++){
                    vertices.put(meshes[i].parts[p].vertices[v].coordinate);
                    normals .put(meshes[i].parts[p].vertices[v].normal);
                    uvs     .put(meshes[i].parts[p].vertices[v].uv);
                    index.add(cnt);
                    cnt++;
                }
            }
            
            shell.setVertices(new VBO(vertices.toArrayFloat(),3));
            shell.setNormals(new VBO(normals.toArrayFloat(),3));
            shell.setUVs(new VBO(uvs.toArrayFloat(),2));
            shell.setIndexes(new IBO(index.toArrayInt(), 1), IndexRange.TRIANGLES(0, cnt));
            mesh.getMaterial().setDiffuse(Color.BLUE);
        }
        
        return node;
    }
    
}
