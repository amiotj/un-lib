
package science.unlicense.impl.model3d.tdcg.png;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.image.png.PNGConstants;

/**
 * TSO Bundled model in a png chunk.
 * 
 * @author Johann Sorel
 */
public class TPNGFormat extends AbstractModel3DFormat{

    public static final TPNGFormat INSTANCE = new TPNGFormat();
    
    public TPNGFormat() {
        super(new Chars("tsopng"),
              new Chars("TSO-PNG"),
              new Chars("TSO-PNG"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("png")
              },
              new byte[][]{PNGConstants.SIGNATURE});
    }

    public boolean supportReading() {
        return true;
    }

    public boolean supportWriting() {
        return false;
    }

    public boolean canDecode(Object input) throws IOException {
        if(super.canDecode(input)){
            //check if this png contains an acTL chunk
            boolean isTpng = false;
            
            final boolean[] closeStream = new boolean[1];
            final ByteInputStream is = IOUtilities.toInputStream(input, closeStream);
            final DataInputStream ds = new DataInputStream(is, NumberEncoding.BIG_ENDIAN);
            //skip signature
            ds.skipFully(8);
            //quick loop on chunks
            while(true){
                //read chunk header
                final int length = ds.readInt();
                final byte[] btype = ds.readFully(new byte[4]);
                final Chars type = new Chars(btype);
                //skip chunk data + crc
                ds.skipFully(length + 4);

                if(TPNGConstants.CHUNK_TaOb.equals(type)){
                    isTpng = true;
                    break;
                }else if(PNGConstants.CHUNK_IEND.equals(type)){
                    //we have finish reading file
                    break;
                }
            }
            if(closeStream[0]) is.close();
            
            return isTpng;
        }
        return false;
    }

    public Model3DStore open(Object input) throws IOException {
        return new TPNGStore(input);
    }
    
}
