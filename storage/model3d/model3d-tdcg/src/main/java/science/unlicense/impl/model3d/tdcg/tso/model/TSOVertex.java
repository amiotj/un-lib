
package science.unlicense.impl.model3d.tdcg.tso.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TSOVertex {
    public float[] coordinate;
    public float[] normal;
    public float[] uv;
    public int[] jointIndex;
    public float[] jointWeights;
    
    public void read(DataInputStream ds) throws IOException{
        coordinate = ds.readFloat(3);
        normal = ds.readFloat(3);
        uv = ds.readFloat(2);
        jointIndex = new int[ds.readInt()];
        jointWeights = new float[jointIndex.length];
        for(int i=0;i<jointIndex.length;i++){
            jointIndex[i] = ds.readInt();
            jointWeights[i] = ds.readFloat();
        }
    }
    
}
