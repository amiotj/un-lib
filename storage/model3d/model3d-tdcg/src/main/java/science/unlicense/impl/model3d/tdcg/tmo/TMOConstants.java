
package science.unlicense.impl.model3d.tdcg.tmo;

/**
 *
 * @author Johann Sorel
 */
public class TMOConstants {
    
    public static final byte[] SIGNATURE = new byte[]{'T','M','O','1'};
    
}
