
package science.unlicense.impl.model3d.tdcg.tso;


/**
 * This format is from a several years old adult game called : 3D Custom Girl.
 * 
 * History note : When I first encounter this format in 2014 I ignored it 
 * because of it's mature content and considered it as a game specific format
 * which would soon disappear. But now in 2016 the format is also used for any 
 * type of models and animations and has spread considerably as an exchange format,
 * pretty much like what happened with MikuMikuDance a few years before.
 * Models can be found on this format on usual website such as tf3dm and deviantart.
 * 
 * Resources :
 * http://3dcg.info.tm/wiki/Main_Page
 * 
 * TODO
 */
public class TSOFormat {
    
}
