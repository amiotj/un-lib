
package science.unlicense.impl.model3d.tdcg.tso.model;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TSOTexture {
    
    public Chars name;
    public Chars path;
    public Image image;
    
    
    public void read(DataInputStream ds) throws IOException{
        name = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        path = ds.readZeroTerminatedChars(0, CharEncodings.SHIFT_JIS);
        final int width = ds.readInt();
        final int height = ds.readInt();
        final int format = ds.readInt();
        final Buffer buffer = DefaultBufferFactory.wrap(ds.readFully(new byte[width*height*format]));
        final RawModel rm;
        final ColorModel cm;
        if(format==3){
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
            cm = new DirectColorModel(rm, new int[]{0,1,2,-1}, false);
        }else if(format==4){
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
            cm = new DirectColorModel(rm, new int[]{0,1,2,3}, false);
        }else{
            throw new IOException("Unexpected image format "+format);
        }
        
        image = new DefaultImage(buffer, new Extent.Long(width, height), rm, cm);
    }
    
}
