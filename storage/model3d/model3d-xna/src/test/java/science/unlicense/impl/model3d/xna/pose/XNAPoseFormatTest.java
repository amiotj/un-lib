
package science.unlicense.impl.model3d.xna.pose;

import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.model3d.Model3DFormat;
import science.unlicense.api.model3d.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class XNAPoseFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final Model3DFormat[] formats = Model3Ds.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof XNAPoseFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("XNA Pose format not found.");
    }

}
