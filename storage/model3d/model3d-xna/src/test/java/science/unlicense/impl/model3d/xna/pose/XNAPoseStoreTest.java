

package science.unlicense.impl.model3d.xna.pose;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.physic.RelativeSkeletonPose;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class XNAPoseStoreTest {

    @Ignore
    @Test
    public void readTest() throws IOException, StoreException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/xna/pose/sample.pose"));

        final XNAPoseStore store = new XNAPoseStore(path);
        final RelativeSkeletonPose pose = (RelativeSkeletonPose) store.getElements().createIterator().next();
        Assert.assertNotNull(pose);

        final Sequence seq = pose.getJointPoses();
        Assert.assertEquals(5, seq.getSize());
        test((JointKeyFrame)seq.get(0),new Chars("elbow"),0,1,2,3,4,5);
        test((JointKeyFrame)seq.get(1),new Chars("head"),0.1,45,-2.3,40,0,8);
        test((JointKeyFrame)seq.get(2),new Chars("left knee"),-3.8,-3,21,-0.01,0,-5.9);
        test((JointKeyFrame)seq.get(3),new Chars("t-shirt 3"),1,0,4,0,9,0);
        test((JointKeyFrame)seq.get(4),new Chars("$ù[_ĝe-*!"),5,3.12,-8,4,0.008,-1.3);

    }

    private void test(JointKeyFrame pose, Chars name, double rx, double ry,
            double rz, double x, double y, double z){
        Assert.assertEquals(name, pose.getJoint());
        Assert.assertEquals(new Vector(x, y, z), pose.getValue().getTranslation());
        Assert.assertEquals(Matrix3x3.createRotationEuler(new Vector(rx, ry, rz)), pose.getValue().getRotation());
    }

    @Ignore
    @Test
    public void writeTest() throws IOException, StoreException{

        final ArrayOutputStream out = new ArrayOutputStream();

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/xna/pose/sample.pose"));

        final XNAPoseStore store = new XNAPoseStore(out);
        final RelativeSkeletonPose pose = new RelativeSkeletonPose();
        pose.getJointPoses().add(new JointKeyFrame(new Chars("elbow"),
                new Vector(0, 1, 2), Quaternion.createFromEuler(new Vector(4, 5, 6)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("head"),
                new Vector(0.1,45,-2), Quaternion.createFromEuler(new Vector(3,40,0,8)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("left knee"),
                new Vector(-3.8,-3,21), Quaternion.createFromEuler(new Vector(-0.01,0,-5.9)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("t-shirt 3"),
                new Vector(1,0,4), Quaternion.createFromEuler(new Vector(0,9,0)),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("$ù[_ĝe-*!"),
                new Vector(5,3.12,-8), Quaternion.createFromEuler(new Vector(4,0.008,-1.3)),
                JointKeyFrame.FROM_BASE));

        final Sequence col = new ArraySequence();
        col.add(pose);
        store.writeElements(col);

        final byte[] result = out.getBuffer().toArrayByte();
        final byte[] expected = IOUtilities.readAll(path.createInputStream());
        Assert.assertArrayEquals(expected, result);

    }

}
