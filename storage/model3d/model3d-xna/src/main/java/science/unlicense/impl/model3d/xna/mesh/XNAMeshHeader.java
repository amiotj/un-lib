
package science.unlicense.impl.model3d.xna.mesh;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class XNAMeshHeader extends CObject{
    
    /**
     * should be XNAaraL
     */
    public Chars name;
    public int nb1;
    public int nb2;
    public Chars str1;
    public Chars str2;
    public Chars str3;

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("XPS Header");
        cb.append(name).append("\n");
        cb.append(nb1).append("\n");
        cb.append(nb2).append("\n");
        cb.append(str1).append("\n");
        cb.append(str2).append("\n");
        cb.append(str3);
        return cb.toChars();
    }
    
}
