

package science.unlicense.impl.model3d.xna;

import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.CSUtilities;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.model3d.Model3DFormat;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * Models are in Right-Handed coordinate system. 
 * 
 * @author Johann Sorel
 */
public abstract class AbstractXNAStore extends AbstractModel3DStore{

    private static final int[] TEXTURE_ORDER = new int[]{
        Layer.TYPE_DIFFUSE,
        Layer.TYPE_SPECULAR,
        Layer.TYPE_NORMAL,
    };

    public Path base;
    public XNABone[] bones;
    public XNAMesh[] meshes;

    public AbstractXNAStore(Model3DFormat format,Object input) {
        super(format,input);
        base = ((Path)input).getParent();
    }

    public Collection getElements() throws StoreException {
        if(meshes==null){
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }

        final MultipartMesh root = new MultipartMesh();

        final boolean hasSkeleton = bones.length>0;

        //rebuild skeleton
        Skeleton skeleton = null;
        if(hasSkeleton){
            skeleton = new Skeleton();
            for(int i=0;i<bones.length;i++){
                final Joint joint = new Joint(3,bones[i].name);
                joint.getNodeTransform().getTranslation().set(bones[i].position);
                joint.getNodeTransform().notifyChanged();
                skeleton.getChildren().add(joint);
            }
            //rebuild hierarchy
            for(int i=bones.length-1;i>=0;i--){
                final Joint jt = (Joint) skeleton.getChildren().get(i);
                if(bones[i].parentId>=0){
                    final Joint parent = (Joint) skeleton.getChildren().get(bones[i].parentId);
                    parent.getChildren().add(jt);
                }
            }
            root.setSkeleton(skeleton);
        }

        //rebuil meshes
        final Dictionary cache = new HashDictionary();

        for(int i=0;i<meshes.length;i++){
            //rebuild mesh faces
            final XNAMesh xnamesh = meshes[i];

            final Mesh glmesh = new Mesh();
            glmesh.setName(xnamesh.name);

            //rebuild vertex and normals
            final int maxBonePerVertex = 4;
            final FloatCursor vertex = DefaultBufferFactory.INSTANCE.createFloat(xnamesh.vertices.length*3).cursorFloat();
            final FloatCursor normals = DefaultBufferFactory.INSTANCE.createFloat(xnamesh.vertices.length*3).cursorFloat();
            final FloatCursor boneweights = hasSkeleton ? DefaultBufferFactory.INSTANCE.createFloat(xnamesh.vertices.length*4).cursorFloat() : null;
            final IntCursor boneindexes = hasSkeleton ? DefaultBufferFactory.INSTANCE.createInt(xnamesh.vertices.length*4).cursorInt() : null;

            for(int k=0;k<xnamesh.vertices.length;k++){
                vertex.write(xnamesh.vertices[k].position.toArrayFloat());
                normals.write(xnamesh.vertices[k].normal.toArrayFloat());
                if(hasSkeleton){
                    boneweights.write(xnamesh.vertices[k].boneWeight);
                    boneindexes.write(xnamesh.vertices[k].boneIndex);
                }
            }

            //rebuil triangle indices
            final IntCursor indices = DefaultBufferFactory.INSTANCE.createInt(xnamesh.faces.length*3).cursorInt();
            for(int k=0;k<xnamesh.faces.length;k++){
                indices.write(xnamesh.faces[k].v1);
                indices.write(xnamesh.faces[k].v2);
                indices.write(xnamesh.faces[k].v3);
            }

            //rebuild material
            final FloatCursor uv = DefaultBufferFactory.INSTANCE.createFloat(xnamesh.vertices.length*2).cursorFloat();
            final FloatCursor tangents = DefaultBufferFactory.INSTANCE.createFloat(xnamesh.vertices.length*4).cursorFloat();
            for(int l=0;l<xnamesh.vertices.length;l++){
                uv.write(xnamesh.vertices[l].uvs[0].toArrayFloat());
                final float[] normal = xnamesh.vertices[l].normal.toArrayFloat();
                final float[] tangent = xnamesh.vertices[l].tangents[0].toArrayFloat();
                tangents.write(tangent);
            }

            final Shell shell;
            if(hasSkeleton){
                final SkinShell sshell = new SkinShell();
                sshell.setVertices(new VBO(vertex.getBuffer(),3));
                sshell.setNormals(new VBO(normals.getBuffer(),3));
                sshell.setIndexes(new IBO(indices.getBuffer(),3),IndexRange.TRIANGLES(0, (int) indices.getBuffer().getPrimitiveCount()));
                sshell.setUVs(new VBO(uv.getBuffer(), 2));
                sshell.setTangents(new VBO(tangents.getBuffer(),4));

                sshell.setMaxWeightPerVertex(maxBonePerVertex);
                sshell.setSkeleton(skeleton);
                sshell.setWeights(new VBO(boneweights.getBuffer(),maxBonePerVertex));
                sshell.setJointIndexes(new VBO(boneindexes.getBuffer(),maxBonePerVertex));
                shell = sshell;
            }else{
                final Shell dshell = new Shell();
                dshell.setVertices(new VBO(vertex.getBuffer(),3));
                dshell.setNormals(new VBO(normals.getBuffer(),3));
                dshell.setIndexes(new IBO(indices.getBuffer(),3),IndexRange.TRIANGLES(0, (int) indices.getBuffer().getPrimitiveCount()));
                dshell.setUVs(new VBO(uv.getBuffer(), 2));
                dshell.setTangents(new VBO(tangents.getBuffer(),4));
                shell = dshell;
            }
            glmesh.setShape(shell);
            // TODO : only triangle clockwise seams to be inverted.
            // find a way to confirm this
            CSUtilities.invertHandCS(shell.getIndexes(), shell.getModes());

            for(int k=0;k<xnamesh.textures.length && k<3;k++){
                final XNATexture texture = xnamesh.textures[k];

                try{
                    Texture2D tex = (Texture2D) cache.getValue(texture.name.toString());
                    if(tex == null){
                        final String[] parts = texture.name.toString().split("\\\\");
                        Path imagePath = base.resolve(new Chars(parts[parts.length-1]));
                        imagePath = checkExist(imagePath);
                        Image image = Images.read(imagePath);
                        tex = new Texture2D(image);
                        cache.add(texture.name.toString(), tex);
                    }

                    final UVMapping paint = new UVMapping(tex);
                    glmesh.getMaterial().putOrReplaceLayer(new Layer(paint,TEXTURE_ORDER[k]));

                }catch(IOException ex){
                    throw new StoreException(ex);
                }
            }

            glmesh.getShape().calculateBBox();
            root.getChildren().add(glmesh);
        }

        
        if(skeleton!=null){
            skeleton.reverseWorldPose();
            skeleton.updateBindPose();
            skeleton.updateInvBindPose();
        }
        
        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    protected abstract void read() throws IOException ;

    private Path checkExist(final Path path){
        final Path parent = path.getParent();
        //try to open this path
        try{
            final ByteInputStream stream = path.createInputStream();
            stream.close();
            return path;
        }catch(IOException ex){
            //file do not exist, try to find a file with same name but case insensitive.
        }

        final Iterator ite = parent.getChildren().createIterator();
        while (ite.hasNext()) {
            final Path child = (Path) ite.next();
            if(child.getName().equals(path.getName(),true,true)){
                return child;
            }
        }

        return path;
    }

}
