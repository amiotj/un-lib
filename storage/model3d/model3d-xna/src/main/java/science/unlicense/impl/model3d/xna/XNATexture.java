
package science.unlicense.impl.model3d.xna;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class XNATexture {
    
    public Chars name;
    public int uvID;
    
}
