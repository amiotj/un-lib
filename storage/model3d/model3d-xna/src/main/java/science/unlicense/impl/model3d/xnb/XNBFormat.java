
package science.unlicense.impl.model3d.xnb;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * Specification :
 * http://xbox.create.msdn.com/en-US/sample/xnb_format
 * 
 * @author Johann Sorel
 */
public class XNBFormat extends AbstractModel3DFormat{

    public static final XNBFormat INSTANCE = new XNBFormat();

    private XNBFormat() {
        super(new Chars("xnb"),
              new Chars("XNA-GameStudio-Binary"),
              new Chars("XNA Game studio Binary file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("xnb")
              },
              new byte[][]{});
    }

    public Model3DStore open(Object input) throws IOException {
        return new XNBStore(input);
    }

}

