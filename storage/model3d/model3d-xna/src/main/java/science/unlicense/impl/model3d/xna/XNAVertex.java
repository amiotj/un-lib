
package science.unlicense.impl.model3d.xna;

import science.unlicense.impl.math.Vector;


/**
 *
 * @author Johann Sorel
 */
public class XNAVertex {

    /** size 3 */
    public Vector position;
    /** size 3 */
    public Vector normal;
    /** size 4 */
    public int[] rgba;

    /** one for each uv layer, each element size 2 */
    public Vector[] uvs;
    /** one for each uv layer, each element size 4 */
    public Vector[] tangents;

    /** rigging infos, only if bones are present, size 4 */
    public int[] boneIndex;
    public float[] boneWeight;

}
