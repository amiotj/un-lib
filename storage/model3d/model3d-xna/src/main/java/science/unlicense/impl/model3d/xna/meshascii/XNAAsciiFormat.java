

package science.unlicense.impl.model3d.xna.meshascii;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class XNAAsciiFormat extends AbstractModel3DFormat{

    public static final XNAAsciiFormat INSTANCE = new XNAAsciiFormat();

    private XNAAsciiFormat() {
        super(new Chars("xna_mesh"),
              new Chars("XNA-Mesh-ASCII"),
              new Chars("XNA Model ASCII file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("mesh.ascii"),
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new XNAAsciiStore(input);
    }

}
