
package science.unlicense.impl.model3d.xna;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class XNABone extends CObject{
    
    public Chars name;
    public int parentId;
    public Vector position;

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("bone : ");
        cb.append(name).append(" ").append(parentId).append(" ").append(position);
        return cb.toChars();
    }
    
}
