
package science.unlicense.impl.model3d.xna;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class XNAMesh extends CObject{
    
    public Chars name;
    public int nbUV;
    public XNATexture[] textures;
    public XNAVertex[] vertices;
    public XNAFace[] faces;

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Mesh ");
        cb.append(name);
        cb.append(" textures:").append(textures.length);
        cb.append(" vertices:").append(vertices.length);
        cb.append(" faces:").append(faces.length);
        return cb.toChars();
    }
    
}
