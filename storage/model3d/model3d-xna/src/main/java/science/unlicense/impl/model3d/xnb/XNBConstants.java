
package science.unlicense.impl.model3d.xnb;

/**
 *
 * @author Johann Sorel
 */
public class XNBConstants {

    public static final byte[] SIGNATURE = new byte[]{'X','N','B'};

}
