
package science.unlicense.impl.model3d.xna;

/**
 *
 * @author Johann Sorel
 */
public class XNAFace {
    
    public int v1;
    public int v2;
    public int v3;
    
}
