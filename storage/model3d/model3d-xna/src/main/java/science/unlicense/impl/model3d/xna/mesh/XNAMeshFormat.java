
package science.unlicense.impl.model3d.xna.mesh;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class XNAMeshFormat extends AbstractModel3DFormat{

    public static final XNAMeshFormat INSTANCE = new XNAMeshFormat();

    private XNAMeshFormat() {
        super(new Chars("xna_mesh"),
              new Chars("XNA-Mesh-Binary"),
              new Chars("XNA Model Binary file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("mesh"),
                new Chars("xps")
              },
              new byte[][]{XNAMeshConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new XNAMeshStore(input);
    }

}
