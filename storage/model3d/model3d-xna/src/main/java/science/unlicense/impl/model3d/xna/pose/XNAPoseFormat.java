
package science.unlicense.impl.model3d.xna.pose;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class XNAPoseFormat extends AbstractModel3DFormat{

    public static final XNAPoseFormat INSTANCE = new XNAPoseFormat();

    private XNAPoseFormat() {
        super(new Chars("xna_pose"),
              new Chars("XNA-Pose"),
              new Chars("XNA Model Pose file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("pose")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new XNAPoseStore(input);
    }

}
