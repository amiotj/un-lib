
package science.unlicense.impl.model3d.stl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * STL, STereoLithography or Standard Tessellation Language format.
 *
 * http://en.wikipedia.org/wiki/STL_(file_format)
 *
 * @author Johann Sorel
 */
public class STLBinaryFormat extends AbstractModel3DFormat{

    public static final STLBinaryFormat INSTANCE = new STLBinaryFormat();

    private STLBinaryFormat() {
        super(new Chars("STL-BIN"),
              new Chars("STL-BIN"),
              new Chars("STereoLithography"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("stl")
              },
              new byte[][]{STLConstants.START_SOLID.toBytes()});
    }

    public Model3DStore open(Object input) throws IOException {
        return new STLBinaryStore(input);
    }

}
