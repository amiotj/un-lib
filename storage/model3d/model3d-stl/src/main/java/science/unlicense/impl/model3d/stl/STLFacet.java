
package science.unlicense.impl.model3d.stl;

/**
 * STL Facet.
 * A triangle defined by 3 vertices, a normal and a hint/flag short value.
 *
 * @author Johann Sorel
 */
public class STLFacet {

    /** size 3 */
    public float[] normal;
    /** size 3*3 : x1,y1,z1, x2,y2,z2 */
    public float[] vertices;
    /** additional value, could be used for anything */
    public short hint;
    
}
