
package science.unlicense.impl.model3d.stl;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class STLConstants {

    public static final Chars START_SOLID = new Chars(new byte[]{'s','o','l','i','d'});
    public static final Chars END_SOLID = new Chars(new byte[]{'e','n','d','s','o','l','i','d'});
    public static final Chars START_FACET = new Chars(new byte[]{'f','a','c','e','t'});
    public static final Chars END_FACET = new Chars(new byte[]{'e','n','d','f','a','c','e','t'});
    public static final Chars NORMAL = new Chars(new byte[]{'n','o','r','m','a','l'});
    public static final Chars START_LOOP = new Chars(new byte[]{'o','u','t','e','r',' ','l','o','o','p'});
    public static final Chars END_LOOP = new Chars(new byte[]{'e','n','d','l','o','o','p'});
    public static final Chars VERTEX = new Chars(new byte[]{'v','e','r','t','e','x'});

    private STLConstants(){}

}
