
package science.unlicense.impl.model3d.stl;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.model3d.Model3DFormat;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * Abstract STL store for ASCII and Binary format.
 *
 * @author Johann Sorel
 */
public abstract class STLAbstractStore extends AbstractModel3DStore{

    protected Sequence facets;
    protected Chars name;

    public STLAbstractStore(Model3DFormat format,Object input) {
        super(format,input);
    }

    protected abstract void readFacets() throws IOException;

    public Collection getElements() {

        if(facets == null){
            try {
                readFacets();
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }

        //rebuild the mesh
        final int nbFacets = facets.getSize();
        final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(nbFacets*3*3).cursorFloat();
        final FloatCursor normals = DefaultBufferFactory.INSTANCE.createFloat(nbFacets*3*3).cursorFloat();
        final IntCursor indices = DefaultBufferFactory.INSTANCE.createInt(nbFacets*3).cursorInt();
        for(int i=0,k=0;i<nbFacets;i++,k+=3){
            final STLFacet facet = (STLFacet) facets.get(i);
            vertices.write(facet.vertices);
            normals.write(facet.normal);
            normals.write(facet.normal);
            normals.write(facet.normal);
            indices.write(k);
            indices.write(k+1);
            indices.write(k+2);
        }
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices.getBuffer(), 3));
        shell.setNormals(new VBO(normals.getBuffer(), 3));
        shell.setIndexes(new IBO(indices.getBuffer(), 3),IndexRange.TRIANGLES(0, (int) indices.getBuffer().getPrimitiveCount()));
        final Mesh mesh = new Mesh();
        mesh.setShape(shell);
        mesh.getShape().calculateBBox();

        if(name != null){
            mesh.setName(name);
        }

        final Collection col = new ArraySequence();
        col.add(mesh);
        return col;
    }

}
