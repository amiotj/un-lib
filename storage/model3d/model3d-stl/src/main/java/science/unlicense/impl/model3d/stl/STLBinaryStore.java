
package science.unlicense.impl.model3d.stl;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * STL Binary Model store.
 *
 * @author Johann Sorel
 */
public class STLBinaryStore extends STLAbstractStore {

    public STLBinaryStore(Object input) {
        super(STLBinaryFormat.INSTANCE,input);
    }

    protected void readFacets() throws IOException {
        facets = new ArraySequence();

        final byte[] header = new byte[80];
        final DataInputStream ds = new DataInputStream(getSourceAsInputStream(), NumberEncoding.LITTLE_ENDIAN);
        ds.readFully(header);

        int nbTriangle = ds.readInt();
        for(int i=0;i<nbTriangle;i++){
            final STLFacet facet = new STLFacet();
            facet.normal = ds.readFloat(3);
            facet.vertices = ds.readFloat(9);
            facet.hint = ds.readShort();
            facets.add(facet);
        }
    }

}
