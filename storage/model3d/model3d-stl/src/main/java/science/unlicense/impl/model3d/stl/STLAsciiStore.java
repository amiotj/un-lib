
package science.unlicense.impl.model3d.stl;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.number.Float64;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;

/**
 * STL ASCII Model store.
 *
 * @author Johann Sorel
 */
public class STLAsciiStore extends STLAbstractStore{


    public STLAsciiStore(Object input) {
        super(STLAsciiFormat.INSTANCE,input);
    }

    protected void readFacets() throws IOException {

        final ByteInputStream stream = getSourceAsInputStream();
        final CharInputStream cs = new CharInputStream(stream, CharEncodings.US_ASCII, new Char('\n'));

        STLFacet currentFacet = null;
        int vertexIndex = 0;
        for(Chars line=cs.readLine();line != null; line=cs.readLine()){
            line = line.trim();
            if(line.startsWith(STLConstants.START_SOLID)){
                facets = new ArraySequence();
                line = line.truncate(STLConstants.START_SOLID.getCharLength(), line.getCharLength()).trim();
                name = line;
            }else if(line.startsWith(STLConstants.START_FACET)){
                currentFacet = new STLFacet();
                vertexIndex = 0;
                line = line.truncate(STLConstants.START_SOLID.getCharLength(), line.getCharLength()).trim();
                if(!line.startsWith(STLConstants.NORMAL)){
                    throw new IOException("Missing normal on facet.");
                }
                line = line.truncate(STLConstants.NORMAL.getCharLength(), line.getCharLength()).trim();
                final Chars[] values = line.split(' ');
                currentFacet.normal = new float[]{
                    (float)Float64.decode(values[0]),
                    (float)Float64.decode(values[1]),
                    (float)Float64.decode(values[2])
                };
                currentFacet.vertices = new float[9];

            }else if(line.startsWith(STLConstants.END_FACET)){
                facets.add(currentFacet);
            }else if(line.startsWith(STLConstants.VERTEX)){
                facets.add(currentFacet);
                line = line.truncate(STLConstants.VERTEX.getCharLength(), line.getCharLength()).trim();
                final Chars[] values = line.split(' ');
                currentFacet.vertices[vertexIndex+0] = (float)Float64.decode(values[0]);
                currentFacet.vertices[vertexIndex+1] = (float)Float64.decode(values[1]);
                currentFacet.vertices[vertexIndex+2] = (float)Float64.decode(values[2]);
                vertexIndex +=3;
            }

        }


    }

}
