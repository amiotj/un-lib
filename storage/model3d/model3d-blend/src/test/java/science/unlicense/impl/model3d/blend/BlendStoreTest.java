
package science.unlicense.impl.model3d.blend;

import science.unlicense.impl.model3d.blend.BlendConstants;
import science.unlicense.impl.model3d.blend.BlendStore;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.gpu.opengl.GLC;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.model3d.Model3Ds;
import science.unlicense.api.physic.skeleton.InverseKinematic;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.engine.opengl.physic.JointTimeSerie;
import science.unlicense.engine.opengl.physic.SkeletonAnimation;
import science.unlicense.engine.opengl.physic.Skeletons;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.engine.opengl.tessellation.DisplacementTessellator;
import science.unlicense.engine.opengl.tessellation.Tessellator;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class BlendStoreTest {

    private static final float DELTA = 0.000001f;
    
    @Test
    public void readCubeTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleScene.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        //TODO check vertices

    }
    
    @Test
    public void readSceneTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/MultipleMeshScene.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        Assert.assertEquals(5, scene.getChildren().getSize());
        
        final Mesh mc = (Mesh) scene.getChildren().get(0);
        final Mesh mx = (Mesh) scene.getChildren().get(1);
        final Mesh my = (Mesh) scene.getChildren().get(2);
        final Mesh mz = (Mesh) scene.getChildren().get(3);
        final Mesh mxyz = (Mesh) scene.getChildren().get(4);
        Assert.assertEquals(new Chars("Centered"), mc.getName());
        Assert.assertEquals(new Chars("OffsetX"), mx.getName());
        Assert.assertEquals(new Chars("OffsetY"), my.getName());
        Assert.assertEquals(new Chars("OffsetZ"), mz.getName());
        Assert.assertEquals(new Chars("OffsetXYZ"), mxyz.getName());

        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            mc.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0,10,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            mx.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0,10,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            my.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1,10,
                0, 0, 0, 1).toArrayDouble(), 
            mz.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                0.1, 0, 0,10,
                0, 0.2, 0,10,
                0, 0, 0.3,10,
                0, 0, 0, 1).toArrayDouble(), 
            mxyz.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
    }
    
    /**
     * Test reading diffuse and specular colors.
     */
    @Test
    public void readColorTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleColor.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final Mesh mesh = (Mesh) scene.getChildren().get(0);
        
        final Material material = mesh.getMaterial();
        final Sequence layers = material.getLayers();
        Assert.assertEquals(1, layers.getSize());
        
        //diffuse/specular base
        Assert.assertEquals(new Color(1.0f, 0.0f, 0.0f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new Color(0.0f, 1.0f, 0.0f, 1.0f), material.getSpecular());
                
        final Layer alphaLayer = (Layer) layers.get(0);
        Assert.assertEquals(Layer.TYPE_DIFFUSE, alphaLayer.getType());
        Assert.assertEquals(Layer.METHOD_MULTIPLY, alphaLayer.getMethod());
        final PlainColorMapping alphaCM = (PlainColorMapping) alphaLayer.getMapping();
        final float[] alphaRGB = alphaCM.getColor().toRGBAPreMul();
        Assert.assertArrayEquals(new float[]{0.6f,0.6f,0.6f,0.6f}, alphaRGB, DELTA);
                
    }
    
    /**
     * Test reading mirror and specular colors.
     */
    @Test
    public void readMirrorTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleMirror.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final Mesh mesh = (Mesh) scene.getChildren().get(0);
        
        
        final Material material = mesh.getMaterial();
        final Sequence layers = material.getLayers();
        Assert.assertEquals(1, layers.getSize());
        
        //diffuse/specular base
        Assert.assertEquals(new Color(1.0f, 1.0f, 1.0f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new Color(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());
                
        final Layer mirrorLayer = (Layer) layers.get(0);        
        Assert.assertEquals(Layer.TYPE_DIFFUSE, mirrorLayer.getType());
        final ReflectionMapping refMapping = (ReflectionMapping) mirrorLayer.getMapping();
                
        
    }
    
    /**
     * Test reading texture.
     */
    @Test
    public void readTextureTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleTexture.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final Mesh mesh = (Mesh) scene.getChildren().get(0);
        
        final Material material = mesh.getMaterial();
        final Sequence layers = material.getLayers();
        Assert.assertEquals(2, layers.getSize());
        
        //diffuse/specular base
        Assert.assertEquals(new Color(0.8f, 0.8f, 0.8f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new Color(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());
        
        //diffuse texture
        final Layer diffuseLayer = (Layer) layers.get(0);
        Assert.assertEquals(Layer.TYPE_DIFFUSE, diffuseLayer.getType());
        final UVMapping diffuseCM = (UVMapping) diffuseLayer.getMapping();
        final Texture2D diffusetex = diffuseCM.getTexture();
        Assert.assertEquals(256, diffusetex.getWidth());
        Assert.assertEquals(256, diffusetex.getHeight());
        
        //normal
        final Layer normalLayer = (Layer) layers.get(1);
        Assert.assertEquals(Layer.TYPE_NORMAL, normalLayer.getType());
        final UVMapping normalCM = (UVMapping) normalLayer.getMapping();
        final Texture2D normaltex = normalCM.getTexture();
        Assert.assertEquals(256, normaltex.getWidth());
        Assert.assertEquals(256, normaltex.getHeight());
                
    }

    /**
     * Test reading texture with clip, offset and scale values.
     */
    @Test
    public void readTextureClipScaleOffsetTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/TextureClipOffsetScale.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final Mesh mesh = (Mesh) scene.getChildren().get(0);
        
        
        final Material material = mesh.getMaterial();
        final Sequence layers = material.getLayers();
        Assert.assertEquals(1, layers.getSize());
        
        //diffuse/specular base
        Assert.assertEquals(new Color(0.8f, 0.8f, 0.8f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new Color(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());
                
        //diffuse
        final Layer diffuseLayer = (Layer) layers.get(0);
        Assert.assertEquals(Layer.TYPE_DIFFUSE, diffuseLayer.getType());
        final UVMapping diffuseCM = (UVMapping) diffuseLayer.getMapping();
        final Texture2D diffusetex = diffuseCM.getTexture();
        Assert.assertEquals(256, diffusetex.getWidth());
        Assert.assertEquals(256, diffusetex.getHeight());
        Assert.assertEquals(new Vector(4.0,2.0), diffuseCM.getScale());
        //y should be flipped
        Assert.assertArrayEquals(new float[]{0.54321f,-0.12345f}, diffuseCM.getOffset().toArrayFloat(),DELTA);
        Assert.assertTrue(diffusetex.getInfo().isClipped());
        
    }
    
    /**
     * Test displacement mapping modifier.
     */
    @Test
    public void readDisplacementMappingTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleDisplacement.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final Mesh mesh = (Mesh) scene.getChildren().get(0);
        
        final Material material = mesh.getMaterial();
        final Sequence layers = material.getLayers();
        Assert.assertEquals(2, layers.getSize());
        
        //diffuse/specular base
        Assert.assertEquals(new Color(0.8f, 0.8f, 0.8f, 1.0f), material.getDiffuse());
        Assert.assertEquals(new Color(1.0f, 1.0f, 1.0f, 1.0f), material.getSpecular());
        
        
        //diffuse
        final Layer diffuseLayer = (Layer) layers.get(0);
        Assert.assertEquals(Layer.TYPE_DIFFUSE, diffuseLayer.getType());
        final UVMapping diffuseCM = (UVMapping) diffuseLayer.getMapping();
        final Texture2D diffusetex = diffuseCM.getTexture();
        Assert.assertEquals(256, diffusetex.getWidth());
        Assert.assertEquals(256, diffusetex.getHeight());
        
        //normal
        final Layer normalLayer = (Layer) layers.get(1);
        Assert.assertEquals(Layer.TYPE_NORMAL, normalLayer.getType());
        final UVMapping normalCM = (UVMapping) normalLayer.getMapping();
        final Texture2D normaltex = normalCM.getTexture();
        Assert.assertEquals(256, normaltex.getWidth());
        Assert.assertEquals(256, normaltex.getHeight());
                        
        //displacement
        final Tessellator tess = mesh.getTessellator();
        Assert.assertTrue(tess instanceof DisplacementTessellator);
        final DisplacementTessellator dt = (DisplacementTessellator) tess;
        Assert.assertEquals(1.0f, dt.getFactor(), DELTA);
        
    }
    
    /**
     * Test reading a skeleton.
     */
    @Test
    public void readSkeletonTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleSkeleton.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final MultipartMesh mpm = (MultipartMesh) scene.getChildren().get(0);
        
                
        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(1, joints.getSize());
        
        final Joint firstBone = (Joint) joints.get(0);
        Assert.assertEquals(1, firstBone.getChildren().getSize());
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        Assert.assertEquals(1, secondBone.getChildren().getSize());
        final Joint thirdBone = (Joint) secondBone.getChildren().get(0);
        Assert.assertEquals(0, thirdBone.getChildren().getSize());
        
        Assert.assertEquals(new Chars("first bone"), firstBone.getName());
        Assert.assertEquals(new Chars("second bone"), secondBone.getName());
        Assert.assertEquals(new Chars("third bone"), thirdBone.getName());
        
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        
        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final Mesh mesh = (Mesh) mpm.getChildren().get(1);
        final SkinShell skin = (SkinShell) mesh.getShape();
        
        Assert.assertArrayEquals(new float[]{
            -1,-1, 0,
             1,-1, 0,
            -1, 0, 0,
             1, 0, 0,
            -1, 1, 0,
             1, 1, 0
        }, skin.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        Assert.assertEquals(1, skin.getMaxWeightPerVertex());  
        Assert.assertArrayEquals(new int[]{
            0,0,
            1,1,
            2,2,
        }, skin.getJointIndexes().getPrimitiveBuffer().toIntArray());
        
        Assert.assertArrayEquals(new float[]{
            1,1,
            1,1,
            1,1,
        }, skin.getWeights().getPrimitiveBuffer().toFloatArray(), DELTA);
        
    }
    
    /**
     * Test reading shape keys (morph targets)
     */
    @Test
    public void readShapeKeysTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleMorph.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();      
        assertBlendCS(scene);
        final Mesh mesh = (Mesh) scene.getChildren().get(0);
        
        final Shell skin = (Shell) mesh.getShape();
        final Sequence morphs = skin.getMorphs().getMorphs();
        Assert.assertEquals(3, morphs.getSize());
        
        final MorphTarget morph1 = (MorphTarget) morphs.get(0);
        final MorphTarget morph2 = (MorphTarget) morphs.get(1);
        final MorphTarget morph3 = (MorphTarget) morphs.get(2);
        Assert.assertEquals(new Chars("Basis"), morph1.getName());
        Assert.assertEquals(new Chars("move1"), morph2.getName());
        Assert.assertEquals(new Chars("move2"), morph3.getName());
        
        Assert.assertArrayEquals(
                new float[]{
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0},
                morph1.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        Assert.assertArrayEquals(
                new float[]{
                0, 0, 1,
                0, 0, 1,
                0, 0, 0,
                0, 0, 0,
                0, 0, 1,
                0, 0, 1},
                morph2.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        Assert.assertArrayEquals(
                new float[]{
                0, 0, 0,
                0, 0, 0,
                2, 0, 0,
                2, 0, 0,
                0, 0, 0,
                0, 0, 0},
                morph3.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        
        
    }
    
    /**
     * Test reading an animation.
     */
    @Test
    public void readAnimationTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleAnimation.blend"));

        final Chars bone1Name = new Chars("first bone");
        final Chars bone2Name = new Chars("second bone");
        final Chars bone3Name = new Chars("third bone");
        
        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final Sequence elements = new ArraySequence(store.getElements());
        final GLNode scene = (GLNode) elements.get(0);
        assertBlendCS(scene);
        final MultipartMesh mpm = (MultipartMesh) scene.getChildren().get(0);
        final SkeletonAnimation animation = (SkeletonAnimation) mpm.getAnimationSet().getAnimations().get(0);
        
        
        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(1, joints.getSize());
        
        final Joint firstBone = (Joint) joints.get(0);
        Assert.assertEquals(1, firstBone.getChildren().getSize());
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        Assert.assertEquals(1, secondBone.getChildren().getSize());
        final Joint thirdBone = (Joint) secondBone.getChildren().get(0);
        Assert.assertEquals(0, thirdBone.getChildren().getSize());
        
        Assert.assertEquals(bone1Name, firstBone.getName());
        Assert.assertEquals(bone2Name, secondBone.getName());
        Assert.assertEquals(bone3Name, thirdBone.getName());
        
        //check parent to node transform
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        //check bind pose
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(firstBone.getBindPose().invert().toMatrix().toArrayDouble(),
            firstBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 1,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(secondBone.getBindPose().invert().toMatrix().toArrayDouble(),
            secondBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 2,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(thirdBone.getBindPose().invert().toMatrix().toArrayDouble(),
            thirdBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        
        
        
        //check reset to base
        skeleton.resetToBase();
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        
        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final Mesh mesh = (Mesh) mpm.getChildren().get(1);
        final SkinShell skin = (SkinShell) mesh.getShape();
        
        Assert.assertEquals(1, skin.getMaxWeightPerVertex());
        
        Assert.assertArrayEquals(new int[]{
            0,0,
            1,1,
            2,2,
        }, skin.getJointIndexes().getPrimitiveBuffer().toIntArray());
        
        Assert.assertArrayEquals(new float[]{
            1,1,
            1,1,
            1,1,
        }, skin.getWeights().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        //check animation
        final JointTimeSerie serie1 = (JointTimeSerie) animation.getSeries().get(0);
        final JointTimeSerie serie2 = (JointTimeSerie) animation.getSeries().get(1);
        final JointTimeSerie serie3 = (JointTimeSerie) animation.getSeries().get(2);
        Assert.assertEquals(bone1Name, serie1.getJointIdentifier());
        Assert.assertEquals(bone2Name, serie2.getJointIdentifier());
        Assert.assertEquals(bone3Name, serie3.getJointIdentifier());
        Assert.assertEquals(3, serie1.getFrames().getSize());
        Assert.assertEquals(3, serie2.getFrames().getSize());
        Assert.assertEquals(3, serie3.getFrames().getSize());
        
        //first bone serie
        final Iterator serie1Ite = serie1.getFrames().createIterator();
        final JointKeyFrame s1f1 = (JointKeyFrame) serie1Ite.next();
        final JointKeyFrame s1f2 = (JointKeyFrame) serie1Ite.next();
        final JointKeyFrame s1f3 = (JointKeyFrame) serie1Ite.next();
        Assert.assertFalse(serie1Ite.hasNext());
        Assert.assertEquals(   0.0, s1f1.getTime(), DELTA);
        Assert.assertEquals(2000.0, s1f2.getTime(), DELTA);
        Assert.assertEquals(4000.0, s1f3.getTime(), DELTA);
        Assert.assertEquals(bone1Name, s1f1.getJoint());
        Assert.assertEquals(bone1Name, s1f2.getJoint());
        Assert.assertEquals(bone1Name, s1f3.getJoint());
        Assert.assertEquals(new Vector(0, 0, 0), s1f1.getValue().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 0), s1f2.getValue().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 0), s1f3.getValue().getTranslation());
        Assert.assertEquals(new Vector(1, 1, 1), s1f1.getValue().getScale());
        Assert.assertEquals(new Vector(1, 1, 1), s1f2.getValue().getScale());
        Assert.assertEquals(new Vector(1, 1, 1), s1f3.getValue().getScale());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s1f1p.getRotation());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s1f2p.getRotation());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s1f3p.getRotation());
        
        //second bone serie
        final Iterator serie2Ite = serie2.getFrames().createIterator();
        final JointKeyFrame s2f1 = (JointKeyFrame) serie2Ite.next();
        final JointKeyFrame s2f2 = (JointKeyFrame) serie2Ite.next();
        final JointKeyFrame s2f3 = (JointKeyFrame) serie2Ite.next();
        Assert.assertFalse(serie2Ite.hasNext());
        Assert.assertEquals(   0.0, s2f1.getTime(), DELTA);
        Assert.assertEquals(2000.0, s2f2.getTime(), DELTA);
        Assert.assertEquals(4000.0, s2f3.getTime(), DELTA);
        Assert.assertEquals(bone2Name, s2f1.getJoint());
        Assert.assertEquals(bone2Name, s2f2.getJoint());
        Assert.assertEquals(bone2Name, s2f3.getJoint());
        Assert.assertEquals(new Vector(0, 0, 0), s2f1.getValue().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 0), s2f2.getValue().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 0), s2f3.getValue().getTranslation());
        Assert.assertEquals(new Vector(1, 1, 1), s2f1.getValue().getScale());
        Assert.assertEquals(new Vector(1, 1, 1), s2f2.getValue().getScale());
        Assert.assertEquals(new Vector(1, 1, 1), s2f3.getValue().getScale());
//        Assert.assertEquals(new Quaternion(0,0,0,1), s2f1p.getRotation());
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(40)).toArrayDouble(), 
//                s2f2p.getRotation().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(-40)).toArrayDouble(), 
//                s2f3p.getRotation().toArrayDouble(), DELTA);
        
        //third bone serie
        final Iterator serie3Ite = serie3.getFrames().createIterator();
        final JointKeyFrame s3f1 = (JointKeyFrame) serie3Ite.next();
        final JointKeyFrame s3f2 = (JointKeyFrame) serie3Ite.next();
        final JointKeyFrame s3f3 = (JointKeyFrame) serie3Ite.next();
        Assert.assertFalse(serie3Ite.hasNext());
        Assert.assertEquals(   0.0, s3f1.getTime(), DELTA);
        Assert.assertEquals(2000.0, s3f2.getTime(), DELTA);
        Assert.assertEquals(4000.0, s3f3.getTime(), DELTA);
        Assert.assertEquals(bone3Name, s3f1.getJoint());
        Assert.assertEquals(bone3Name, s3f2.getJoint());
        Assert.assertEquals(bone3Name, s3f3.getJoint());
        Assert.assertEquals(new Vector(0, 0, 0), s3f1.getValue().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 0), s3f2.getValue().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 0), s3f3.getValue().getTranslation());
        Assert.assertEquals(new Vector(1, 1, 1), s3f1.getValue().getScale());
        Assert.assertEquals(new Vector(1, 1, 1), s3f2.getValue().getScale());
        Assert.assertEquals(new Vector(1, 1, 1), s3f3.getValue().getScale());
//        Assert.assertArrayEquals(
//                new Quaternion(0,0,0,1).getValues(), 
//                s3f1p.getRotation().getValues(), DELTA);
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(-70)).getValues(), 
//                s3f2p.getRotation().getValues(), DELTA);
//        Assert.assertArrayEquals(
//                Quaternion.createFromAxis(new Vector(0,0,1),Angles.degreeToRadian(70)).getValues(), 
//                s3f3p.getRotation().getValues(), DELTA);
        
        
        //test the animation
        Skeletons.mapAnimation(animation, skeleton, mpm, null);
        animation.start();
        animation.setTime(0);
        animation.update();
        
        //check parent to node transform
        //this node transform must have been converted to relative -> identity
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        //check bind pose
        //Skeletons.mapAnimation(animation, skeleton, scene);
        //this node transform must have been converted to relative -> identity
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 0,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(), 
//            firstBone.getBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(firstBone.getBindPose().invert().toArrayDouble(), 
//            firstBone.getInvertBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 0,-1, 0,
//                0, 1, 0, 1,
//                0, 0, 0, 1).toArrayDouble(), 
//            secondBone.getBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(secondBone.getBindPose().invert().toArrayDouble(), 
//            secondBone.getInvertBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 0,-1, 0,
//                0, 1, 0, 2,
//                0, 0, 0, 1).toArrayDouble(), 
//            thirdBone.getBindPose().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(thirdBone.getBindPose().invert().toArrayDouble(), 
//            thirdBone.getInvertBindPose().toArrayDouble(), DELTA);
//        
//        animation.update(2000*1000000, mpm);
        
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 0,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(), 
//            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 1,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(), 
//            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
//        Assert.assertArrayEquals(new Matrix4(
//                1, 0, 0, 0,
//                0, 1, 0, 1,
//                0, 0, 1, 0,
//                0, 0, 0, 1).toArrayDouble(), 
//            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
    }
    
    /**
     * Test reading an animation.
     */
    @Test
    public void readInstancingTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SimpleInstancing.blend"));
        
        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final Sequence elements = new ArraySequence(store.getElements());
        for(int i=0;i<elements.getSize();i++){
            final Object ele = elements.get(i);
            if(ele instanceof SceneNode){
                assertBlendCS((SceneNode) ele);
            }
        }
    }
    
    @Test
    public void readNgonTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/Ngon.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final Mesh ngon = (Mesh) scene.getChildren().get(0);
        
        final Shell shell = (Shell) ngon.getShape();
        final VBO vertices = shell.getVertices();
        final VBO normals = shell.getNormals();
        final IBO indexes = shell.getIndexes();
        final IndexRange[] modes = shell.getModes();
        
        Assert.assertEquals(6, vertices.getTupleCount());
        Assert.assertEquals(6, normals.getTupleCount());
        Assert.assertArrayEquals(new float[]{-2f,0f,2.7f}, vertices.getTupleFloat(0, null), DELTA);
        Assert.assertArrayEquals(new float[]{0.1f,1f,1.4f}, vertices.getTupleFloat(1, null), DELTA);
        Assert.assertArrayEquals(new float[]{-1.3f,-2.5f,0.75f}, vertices.getTupleFloat(2, null), DELTA);
        Assert.assertArrayEquals(new float[]{-0.45f,1.4f,3.9f}, vertices.getTupleFloat(3, null), DELTA);
        Assert.assertArrayEquals(new float[]{-0.45f,1.2f,2.8f}, vertices.getTupleFloat(4, null), DELTA);
        Assert.assertArrayEquals(new float[]{-0.3f,1.2f,1.6f}, vertices.getTupleFloat(5, null), DELTA);
        
        Assert.assertArrayEquals(new int[]{3,0,2, 4,3,2, 5,4,2, 1,5,2}, indexes.getPrimitiveBuffer().toIntArray());
        Assert.assertEquals(1, modes.length);
        Assert.assertEquals(GLC.GL_TRIANGLES, modes[0].getMode());
        
    }
    
    /**
     * Test reading a skeleton and morph with vertex groups.
     */
    @Test
    public void readSkeletonAndMorphTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/SkeletonAndMorph.blend"));

        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final GLNode scene = (GLNode) store.searchElements(GLNode.class).createIterator().next();
        assertBlendCS(scene);
        final MultipartMesh mpm = (MultipartMesh) scene.getChildren().get(0);
        
                
        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(1, joints.getSize());
        
        final Joint firstBone = (Joint) joints.get(0);
        Assert.assertEquals(1, firstBone.getChildren().getSize());
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        Assert.assertEquals(0, secondBone.getChildren().getSize());
        
        Assert.assertEquals(new Chars("b0"), firstBone.getName());
        Assert.assertEquals(new Chars("b1"), secondBone.getName());
        
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        
        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final Mesh mesh = (Mesh) mpm.getChildren().get(1);
        final SkinShell skin = (SkinShell) mesh.getShape();
        
        /*
        +  4
        | \  
        +--+ 0/1 , 3/5
        | /
        +   2
        */
        Assert.assertArrayEquals(new float[]{
             1,-1, 1,
             1, 1, 1,
             1,-1, 0,
             1,-1, 1,
             1,-1, 2,
             1, 1, 1
        }, skin.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        Assert.assertArrayEquals(new int[]{
            0,1,2,
            3,4,5
        }, skin.getIndexes().getPrimitiveBuffer().toIntArray());
        
        Assert.assertEquals(2, skin.getMaxWeightPerVertex());  
        Assert.assertArrayEquals(new int[]{
            0,1,
            0,1,
            0,0,
            
            0,1,
            1,0,
            0,1,
        }, skin.getJointIndexes().getPrimitiveBuffer().toIntArray());
        Assert.assertArrayEquals(new float[]{
            0.5f,0.5f,
            0.5f,0.5f,
            1.0f,0.0f,
            
            0.5f,0.5f,
            1.0f,0.0f,
            0.5f,0.5f,
        }, skin.getWeights().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        final MorphSet morphs = skin.getMorphs();
        final MorphTarget mbas = (MorphTarget) morphs.getMorphs().get(0);
        final MorphTarget mk1 = (MorphTarget) morphs.getMorphs().get(1);
        final MorphTarget mk2 = (MorphTarget) morphs.getMorphs().get(2);
        
        Assert.assertEquals(new Chars("Basis"), mbas.getName());
        Assert.assertEquals(new Chars("key1"), mk1.getName());
        Assert.assertEquals(new Chars("key2"), mk2.getName());
        
        //the basis morph is the same as original vertices
        Assert.assertArrayEquals(new float[]{
             0, 0, 0,
             0, 0, 0,
             0, 0, 0,
             0, 0, 0,
             0, 0, 0,
             0, 0, 0
        }, mbas.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        Assert.assertArrayEquals(new float[]{
             1, 0, 0,
             1, 0, 0,
             1, 0, 0,
             1, 0, 0,
             0, 0, 0,
             1, 0, 0
        }, mk1.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        Assert.assertArrayEquals(new float[]{
             1, 0, 0,
             1, 0, 0,
             0, 0, 0,
             1, 0, 0,
             1, 0, 0,
             1, 0, 0
        }, mk2.getVertices().getPrimitiveBuffer().toFloatArray(), DELTA);
        
    }
    
    /**
     * Test reading an inverse kinematic chain.
     */
    @Test
    public void readInverseKinematicTest() throws StoreException, IOException{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/blend/InverseKinematic.blend"));

        final Chars bone1Name = new Chars("first bone");
        final Chars bone2Name = new Chars("second bone");
        final Chars bone3Name = new Chars("third bone");
        final Chars targetName = new Chars("target");
        
        final Model3DStore store = Model3Ds.read(path);
        Assert.assertTrue(store instanceof BlendStore);

        final Sequence elements = new ArraySequence(store.getElements());
        final GLNode scene = (GLNode) elements.get(0);
        assertBlendCS(scene);
        final MultipartMesh mpm = (MultipartMesh) scene.getChildren().get(0);
        
        
        //check skeleton structure
        final Skeleton skeleton = mpm.getSkeleton();
        final Sequence joints = skeleton.getChildren();
        Assert.assertEquals(2, joints.getSize());
        
        final Joint firstBone = (Joint) joints.get(0);
        final Joint secondBone = (Joint) firstBone.getChildren().get(0);
        final Joint thirdBone = (Joint)  secondBone.getChildren().get(0);
        final Joint targetBone = (Joint)  joints.get(1);
        
        Assert.assertEquals(bone1Name, firstBone.getName());
        Assert.assertEquals(bone2Name, secondBone.getName());
        Assert.assertEquals(bone3Name, thirdBone.getName());
        Assert.assertEquals(targetName, targetBone.getName());
        
        //check parent to node transform
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        //check bind pose
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(firstBone.getBindPose().invert().toMatrix().toArrayDouble(),
            firstBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 1,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(secondBone.getBindPose().invert().toMatrix().toArrayDouble(),
            secondBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 2,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(thirdBone.getBindPose().invert().toMatrix().toArrayDouble(),
            thirdBone.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        
        
        
        //check reset to base
        skeleton.resetToBase();
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            firstBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            secondBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            thirdBone.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        
        //check the mapping with the mesh
        Assert.assertEquals(2, mpm.getChildren().getSize());
        final Mesh mesh = (Mesh) mpm.getChildren().get(1);
        final SkinShell skin = (SkinShell) mesh.getShape();
        
        Assert.assertEquals(1, skin.getMaxWeightPerVertex());
        
        Assert.assertArrayEquals(new int[]{
            0,0,
            1,1,
            2,2,
        }, skin.getJointIndexes().getPrimitiveBuffer().toIntArray());
        
        Assert.assertArrayEquals(new float[]{
            1,1,
            1,1,
            1,1,
        }, skin.getWeights().getPrimitiveBuffer().toFloatArray(), DELTA);
        
        //check inverse kinematic
        Assert.assertEquals(1, skeleton.getIks().getSize());
        
        final InverseKinematic ik = (InverseKinematic) skeleton.getIks().get(0);
        final Joint[] chain = ik.getChain();
        Assert.assertEquals(2, chain.length);
        Assert.assertEquals(chain[0], secondBone);
        Assert.assertEquals(chain[1], firstBone);
        Assert.assertEquals(ik.getEffector(), thirdBone);
        Assert.assertEquals(ik.getTarget(), targetBone);
        
    }
    
    private static void assertBlendCS(SceneNode node){
        final NodeVisitor visitor = new DefaultNodeVisitor(){
            public Object visit(Node node, Object context) {
                if(node instanceof SceneNode){
                    final CoordinateSystem cs = ((SceneNode)node).getCoordinateSystem();
                    Assert.assertEquals(BlendConstants.COORDSYS, cs);
                }
                if(node instanceof MultipartMesh){
                    final Skeleton skeleton = ((MultipartMesh)node).getSkeleton();
                    if(skeleton!=null){
                        final Sequence roots = skeleton.getChildren();
                        for(int i=0;i<roots.getSize();i++){
                            ((Node)roots.get(i)).accept(this, null);
                        }
                    }
                }
                return super.visit(node, context);
            }
        };
        node.accept(visitor, null);
    }
    
}
