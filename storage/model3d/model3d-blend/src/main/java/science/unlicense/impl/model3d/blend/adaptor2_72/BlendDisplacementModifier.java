
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.tessellation.DisplacementTessellator;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.model3d.blend.model.BlendFile;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendDisplacementModifier extends BlendFileBlock{

    public BlendDisplacementModifier(BlendFileBlock block) {
        super(block);
    }
    
    public BlendTexture getTexture(){
        return (BlendTexture)parsedData.getChild(Blend_2_72.DISPLACEMEMODIFIERDATA.TEXTURE).getValue();
    }

    public float getStrength(){
        return ((Number)parsedData.getChild(Blend_2_72.DISPLACEMEMODIFIERDATA.STRENGTH).getValue()).floatValue();
    }
    
    public DisplacementTessellator rebuild(BlendFile file,Dictionary textureCache){
        for(BlendImage bi : getTexture().getImages()){
            final Chars displacementPath = bi.getImageName();
            if(displacementPath!=null && !displacementPath.isEmpty()){
                final Path path = file.filePath.getParent().resolve(displacementPath);
                Texture2D tex = (Texture2D) textureCache.getValue(path.toURI());
                if(tex==null){
                    try {
                        final Image image = Images.read(path);
                        tex = new Texture2D(image);
                        textureCache.add(path.toURI(),tex);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                if(tex!=null){
                    final DisplacementTessellator tes = new DisplacementTessellator(tex);
                    tes.setFactor(getStrength());
                    return tes;
                }
            }
        }
        return null;
    }
    
}
