
package science.unlicense.impl.model3d.blend;

import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public interface BlendAdaptor {

    BlendFileBlock adapt(BlendFileBlock block);
    
}
