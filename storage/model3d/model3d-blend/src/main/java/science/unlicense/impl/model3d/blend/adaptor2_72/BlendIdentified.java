
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendIdentified extends BlendFileBlock{

    public BlendIdentified(BlendFileBlock block) {
        super(block);
    }

    public BlendId getId(){
        return new BlendId(parsedData.getChild(Blend_2_72.OBJECT.ID));
    }
        
}
