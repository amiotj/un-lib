
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendScene extends BlendIdentified {

    public BlendScene(BlendFileBlock block) {
        super(block);
    }

    public BlendBase[] getBases(){
        final Sequence seq = getRefList(Blend_2_72.SCENE.BASE);
        final BlendBase[] bases = new BlendBase[seq.getSize()];
        Collections.copy(seq, bases, 0);
        return bases;
    }
    
}
