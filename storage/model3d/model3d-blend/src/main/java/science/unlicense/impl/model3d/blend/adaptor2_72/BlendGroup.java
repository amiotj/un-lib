
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendGroup extends BlendIdentified{

    public BlendGroup(BlendFileBlock block) {
        super(block);
    }
        
    public BlendGroupObject[] getBases(){
        final Sequence seq = getRefList(Blend_2_72.GROUP.GOBJECT);
        final BlendGroupObject[] bases = new BlendGroupObject[seq.getSize()];
        Collections.copy(seq, bases, 0);
        return bases;
    }
    
}
