
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.path.Path;
import science.unlicense.api.physic.skeleton.IKSolver;
import science.unlicense.api.physic.skeleton.IKSolverCCD;
import science.unlicense.api.physic.skeleton.InverseKinematic;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MeshUtilities;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.mesh.InstancingShell;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.engine.opengl.physic.JointTimeSerie;
import science.unlicense.engine.opengl.physic.SkeletonAnimation;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.model3d.blend.BlendAdaptor;
import science.unlicense.impl.model3d.blend.BlendConstants;
import science.unlicense.impl.model3d.blend.adaptor2_72.Blend_2_72.*;
import science.unlicense.impl.model3d.blend.model.BlendFile;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 * TODO an adaptor works for a given version of blender : make an interface
 * 
 * @author Johann Sorel
 */
public class Adaptor_2_72 implements BlendAdaptor {

    private final BlendFile file;
    
    /** BlendFileBlock(Object) > GLNode */
    private final Dictionary objects = new HashDictionary();
    private final Dictionary index = new HashDictionary();
    
    /** cache read textures */
    private final Dictionary textureCache = new HashDictionary();
    
    private Sequence result;
    
    public Adaptor_2_72(BlendFile file) {
        this.file = file;
        
        for(int i=0,n=file.blocks.getSize();i<n;i++){
            file.blocks.replace(i, adapt((BlendFileBlock) file.blocks.get(i)));
        }
        
        this.file.resolvePointers();
    }

    public BlendFileBlock adapt(BlendFileBlock block) {
        if(block.parsedData==null){
            return block;
        }
        
        final Chars type = block.parsedData.getType().getId();
        
        if(Blend_2_72.SCENE.SCENE.equals(type)){
            return new BlendScene(block);
        }else if(Blend_2_72.BASE.BASE.equals(type)){
            return new BlendBase(block);
        }else if(Blend_2_72.OBJECT.OBJECT.equals(type)){
            return new BlendObject(block);
        }else if(Blend_2_72.MESH.MESH.equals(type)){
            return new BlendMesh(block);
        }else if(Blend_2_72.BONE_ARMATURE.ARMATURE.equals(type)){
            return new BlendArmature(block);
        }else if(Blend_2_72.MATERIAL.MATERIAL.equals(type)){
            return new BlendMaterial(block);
        }else if(Blend_2_72.TEXTURE.TEXTURE.equals(type)){
            return new BlendTexture(block);
        }else if(Blend_2_72.MTEXTURE.MTEXTURE.equals(type)){
            return new BlendMTexture(block);
        }else if(Blend_2_72.IMAGE.IMAGE.equals(type)){
            return new BlendImage(block);
        }else if(Blend_2_72.GROUP.GROUP.equals(type)){
            return new BlendGroup(block);
        }else if(Blend_2_72.GROUP_OBJECT.GROUP_OBJECT.equals(type)){
            return new BlendGroupObject(block);
        }else if(Blend_2_72.ARMATURE_MODIFIER_DATA.ARMATUREMODIFIERDATA.equals(type)){
            return new BlendArmatureModifier(block);
        }else if(Blend_2_72.BONE_POSE.BPOSE.equals(type)){
            return new BlendBonePose(block);
        }else if(Blend_2_72.BONE_POSE_CHANNEL.BPOSECHANNEL.equals(type)){
            return new BlendBonePoseChannel(block);
        }else if(Blend_2_72.BONE_CONSTRAINT.BCONSTRAINT.equals(type)){
            return new BlendBoneConstraint(block);
        }else if(Blend_2_72.BONE_KINEMATIC_CONSTRAINT.BKINEMATICCONSTRAINT.equals(type)){
            return new BlendBoneKinematicConstraint(block);
        }else if(Blend_2_72.DISPLACEMEMODIFIERDATA.DISPLACEMEMODIFIERDATA.equals(type)){
            return new BlendDisplacementModifier(block);
        }else{
            return block;
        }
    }
    
    public Collection convert() throws StoreException{
        objects.removeAll();
        index.removeAll();
        result = new ArraySequence();
        
        final GLNode scene = new GLNode();
        scene.setLocalCoordinateSystem(BlendConstants.COORDSYS);
        
        //get the scene block
        final BlendScene sceneBlock = (BlendScene)file.getBlockByCode(SCENE.CODE);
        
        //convert all bases
        final BlendBase[] bases = sceneBlock.getBases();
        for(int i=0;i<bases.length;i++){
            getConvertedBlock(bases[i]);
        }
                
        //rebuild parent>child structure
        final Iterator ite = objects.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final BlendFileBlock block = (BlendFileBlock) pair.getValue1();
            final Object candidate = pair.getValue2();
            if(block instanceof BlendObject && candidate instanceof GLNode){
                final GLNode node = (GLNode)candidate;
                if(node.getParent()==null){
                    //parent might have been set already by an armature modifier
                    //check if there is a parent
                    final BlendFileBlock parentAdr = (BlendFileBlock)block.parsedData.getChild(OBJECT.PARENT).getValue();
                    if(parentAdr!=null){
                        final GLNode parent = (GLNode) index.getValue(parentAdr.header.oldMemoryAddress);
                        parent.getChildren().add(node);
                    }else{
                        scene.getChildren().add(node);
                    }
                }
            }
        }
               
        result.add(scene);
        
        return result;
    }
    
    private Object getConvertedBlock(BlendFileBlock block) throws StoreException{
        Object value = objects.getValue(block);
        if(value == null){
            if(block instanceof BlendBase){
                return convertObject(((BlendBase)block).getBlendObject());
            }else if(block instanceof BlendObject){
                return convertObject((BlendObject)block);
            }else if(block instanceof BlendArmature){
                return convertArmature((BlendArmature)block);
            }else if(block instanceof BlendMesh){
                return convertMesh((BlendMesh)block);
            }
        }
        return value;
    }
            
    private GLNode convertObject(final BlendObject block) throws StoreException{
        
        final BlendFileBlock subBlock = block.getDataBlock();
        final Matrix4x4 parentToNode = block.getMatrix();
        final Chars name = block.getId().getName();
        
        //check if it's an instanced object
        final BlendGroup duplicate = block.getDuplicateGroup();
        if(duplicate!=null){
            final BlendObject original = duplicate.getBases()[0].getBlendObject();
            final GLNode origNode = (GLNode) objects.getValue(original);
            
            if(origNode instanceof Mesh){
                final Mesh mesh = (Mesh) origNode;
                Shape shape = mesh.getShape();
                
                //ensure we have en instancing shell
                InstancingShell istShell;
                if(shape instanceof InstancingShell){
                    istShell = (InstancingShell) shape;
                }else if(shape instanceof Shell){
                    //convert it to a instancing shell
                    istShell = new InstancingShell();
                    istShell.set((Shell) shape);      
                    final float[] identity = new Matrix4x4().setToIdentity().toArrayFloatColOrder();
                    istShell.setInstanceTransforms(new VBO(identity, 16));
                    istShell.getInstanceTransforms().setInstancingDividor(1);
                }else{
                    throw new StoreException("Instancing of "+shape+" not supported yet.");
                }
                mesh.setShape(istShell);
                
                //add a new instance, TODO matrix is already multiplied with original object matrix
                Matrix4x4 matrix = block.getMatrix();
                final VBO vbo = istShell.getInstanceTransforms();
                float[] array = vbo.getPrimitiveBuffer().toFloatArray();
                array = Arrays.concatenate(new float[][]{array,matrix.toArrayFloatColOrder()});
                vbo.setBuffer(DefaultBufferFactory.wrap(array), 16);
            }
            
            return null;
        }
                
        GLNode node = null;
        if(subBlock instanceof BlendMesh){
            node = (GLNode) getConvertedBlock(subBlock);
        }else if(subBlock instanceof BlendArmature){
            node = (GLNode) getConvertedBlock(subBlock);
        }
        if(node==null){
            //TODO unsupported type
            node = new GLNode(BlendConstants.COORDSYS);
        }
        
        node.setName(name);
        node.getNodeTransform().set(parentToNode);

        objects.add(block, node);
        index.add(block.header.oldMemoryAddress, node);

        //read modifiers
        final Sequence modifiers = block.getModifierList(OBJECT.MODIFIERS);
        for(int i=0,n=modifiers.getSize();i<n;i++){
            final BlendFileBlock modifier = (BlendFileBlock) modifiers.get(i);
            if(modifier instanceof BlendArmatureModifier){
                //read armature
                final BlendObject armature = ((BlendArmatureModifier)modifier).getArmature();
                final MultipartMesh mpm = (MultipartMesh) getConvertedBlock(armature);
                mpm.getChildren().add(node);
                rebuildRigging(block, mpm, (Mesh)node);
            }else if(modifier instanceof BlendDisplacementModifier){
                //read displacement mapping
                final BlendDisplacementModifier bdm = (BlendDisplacementModifier) modifier;
                ((Mesh)node).setTessellator(bdm.rebuild(file, textureCache));
            }
        }
        
        //read animations
        final BlendFileBlock aniBlock = (BlendFileBlock) block.parsedData.getChild(OBJECT.ADT).getValue();
        if(aniBlock!=null){
            //this node is only on armatures
            convertAnimation(aniBlock, (MultipartMesh) node);
        }
        
        //read bone constraints and ik
        final BlendBonePose poseBlock = (BlendBonePose) block.parsedData.getChild(OBJECT.POSE).getValue();
        if(poseBlock!=null){
            //this node is only on armatures
            convertBonePose(poseBlock, (MultipartMesh) node);
        }
        
        return node;
    }
        
    private Mesh convertMesh(BlendMesh block) throws StoreException{

        final Buffer[] vn = block.readVNJW();
        if(vn==null) return null;

        //convert key frames
        final MorphSet ms = new MorphSet();
        final BlendFileBlock key = (BlendFileBlock) block.parsedData.getChild(MESH.KEY).getValue();
        if(key!=null){
            ms.getMorphs().addAll(convertShapeKeys(key));
            //keyshapes are in absolute coordinate, we substract the original position to be in relative
            final VBO vbo = new VBO(vn[0], 3);
            final float[] vo = new float[3];
            final float[] vm = new float[3];
            for(int i=0,n=ms.getMorphs().getSize();i<n;i++){
                final MorphTarget mt = (MorphTarget) ms.getMorphs().get(i);
                final VBO vbm = mt.getVertices();
                for(int k=0,kn=vbm.getTupleCount();k<kn;k++){
                    vbo.getTupleFloat(k, vo);
                    vbm.getTupleFloat(k, vm);
                    vm[0] -= vo[0];
                    vm[1] -= vo[1];
                    vm[2] -= vo[2];
                    vbm.setTupleFloat(k, vm);
                }
            }
        }
        
        //build index from faces
        Buffer verts = vn[0];
        Buffer norms = vn[1];
        Buffer jindx = vn[2];
        Buffer jweigt = vn[3];
        Buffer ib = block.readFaces();
        Buffer uv = null;
        if(ib==null){
            //build index from mpoly and loops
            //this might update the vother buffers
            final Buffer[] objs = block.readLoops(verts,norms,jindx,jweigt,ms.getMorphs());
            verts = objs[0];
            norms = objs[1];
            jindx = objs[2];
            jweigt = objs[3];
            ib = objs[4];
            uv = objs[5];
        }
        
        final Mesh mesh = new Mesh();
        mesh.setLocalCoordinateSystem(BlendConstants.COORDSYS);
        Shell shell;
        if(jindx!=null){
            shell = new SkinShell();
            final int jtsize = (int) (jindx.getPrimitiveCount() / (verts.getPrimitiveCount()/3));
            ((SkinShell)shell).setJointIndexes(new VBO(jindx,jtsize));
            ((SkinShell)shell).setWeights(new VBO(jweigt,jtsize));
            ((SkinShell)shell).setMaxWeightPerVertex(jtsize);
            
            //blender weights are auto normalize, but they are not stored as normalized
            //it's only at rendering that they are.
            // -> understand 'normalize' as the sum equals one.
            //see : http://wiki.blender.org/index.php/Doc:2.6/Manual/Modeling/Meshes/Vertex_Groups/Weight_Paint
            MeshUtilities.sumToOne(((SkinShell)shell).getWeights());
            
        }else{
            shell = new Shell();
        }
        shell.setVertices(new VBO(verts,3));
        shell.setNormals(new VBO(norms,3));
        shell.setIndexes(new IBO(ib,3),IndexRange.TRIANGLES(0, (int) ib.getPrimitiveCount()));
        shell.setMorphs(ms);
        if(uv!=null) shell.setUVs(new VBO(uv, 2));
        mesh.setShape(shell);

        //convert material
        BlendMaterial adrM = block.getMaterial();
        if(adrM!=null){
            final Material material = convertMaterial(adrM);
            mesh.setMaterial(material);
            
            if(material.getLayer(Layer.TYPE_NORMAL)!=null){
                //we need to rebuld the tangents
                shell.calculateTangents();
            }
            
            final Iterator ite = material.getLayers().createIterator();
            while(ite.hasNext()){
                final Layer layer = (Layer) ite.next();
                if(layer.getMapping() instanceof ReflectionMapping){
                    //this mapping requieres the vertices and normal to remain loaded
                    shell.getVertices().setForgetOnLoad(false);
                    shell.getNormals().setForgetOnLoad(false);
                }
            }
        }
        
        objects.add(block, mesh);
        return mesh;
    }
    
    private Sequence convertShapeKeys(BlendFileBlock key){
        final TypedNode keyblocks = key.parsedData.getChild(KEY.BLOCK);
                
        final Sequence morphs = new ArraySequence();
        
        BlendFileBlock keyBlock = (BlendFileBlock)keyblocks.getChild(KEY.BLOCK_FIRST).getValue();
        while(keyBlock!=null){
            final MorphTarget m = convertShapeKey(keyBlock);            
            if(m!=null) morphs.add(m);
            //check if there is a next key
            keyBlock = (BlendFileBlock)keyBlock.parsedData.getChild(KEYBLOCK.NEXT).getValue();
        }
        
        return morphs;
    }
    
    private MorphTarget convertShapeKey(BlendFileBlock keyBlock){
        final Chars name = (Chars) keyBlock.parsedData.getChild(KEYBLOCK.NAME).getValue();
        final Integer type = ((Number)keyBlock.parsedData.getChild(KEYBLOCK.TYPE).getValue()).intValue();
        final BlendFileBlock data = (BlendFileBlock) keyBlock.parsedData.getChild(KEYBLOCK.DATA).getValue();
        
        if(type==1){
            //vertex modification
            int nb = data.rawdata.length/4;
            final float[] buffer = new float[nb];
            for(int i=0;i<nb;i++){
                buffer[i] = file.encoding.readFloat(data.rawdata, i*4);
            }
            return new MorphTarget(name, new VBO(buffer, 3));
        }
        
        return null;
    }
        
    private void convertAnimation(BlendFileBlock block, MultipartMesh mpm){
        final BlendFileBlock actionBlock = (BlendFileBlock) block.parsedData.getChild(ANIMDATA.ACTION).getValue();
        if(actionBlock==null) return;
        
        final TypedNode id = actionBlock.parsedData.getChild(BACTION.ID);
        final Chars name = (Chars) id.getChild(BACTION.ID_NAME).getValue();
        
        //create skeleton animation
        final Skeleton skeleton = mpm.getSkeleton();
        final SkeletonAnimation animation = new SkeletonAnimation(skeleton);
        animation.setName(name);
        mpm.getAnimationSet().getAnimations().add(animation);
        
        //groups, one for each bone
        final TypedNode groupsNode = actionBlock.parsedData.getChild(BACTION.GROUPS);
        BlendFileBlock groupBlock = (BlendFileBlock)groupsNode.getChild(BACTION.GROUPS_FIRST).getValue();
        while(groupBlock!=null){
            convertJointTimeSerie(groupBlock, animation, skeleton);
            //check if there is a next group
            groupBlock = (BlendFileBlock)groupBlock.parsedData.getChild(KEYBLOCK.NEXT).getValue();
        }
        
    }
    
    private void convertJointTimeSerie(BlendFileBlock groupBlock, SkeletonAnimation animation, Skeleton skeleton){
        //bActionGroup
        final Chars boneName = (Chars) groupBlock.parsedData.getChild(BACTIONGROUP.NAME).getValue();
        final TypedNode channelsNode = groupBlock.parsedData.getChild(BACTIONGROUP.CHANNELS);

        //create joint time serie          
        final JointTimeSerie serie = new JointTimeSerie();
        serie.setJointIdentifier(boneName);
        animation.getSeries().add(serie);

        boolean isAxisAngle = false;
        
        //10 possible curves blocks for each bone information
        // - position X,Y,Z
        // - quaternion W,X,Y,Z or axis angle A,X,Y,Z
        // - scale X,Y,Z
        BlendFileBlock curveBlock = (BlendFileBlock)channelsNode.getChild(BACTIONGROUP.CHANNELS_FIRST).getValue();
        BlendFileBlock lastCurveBlock = (BlendFileBlock)channelsNode.getChild(BACTIONGROUP.CHANNELS_LAST).getValue();
        while(curveBlock!=null){

            final BlendFileBlock rnaPath = (BlendFileBlock)curveBlock.parsedData.getChild(FCURVE.RNA_PATH).getValue();
            final Chars propertyName = new Chars(rnaPath.rawdata,CharEncodings.US_ASCII);
            final int valueIndex = ((Number)curveBlock.parsedData.getChild(FCURVE.ARRAY_INDEX).getValue()).intValue();
            final BlendFileBlock bezier = (BlendFileBlock)curveBlock.parsedData.getChild(FCURVE.BEZT).getValue();

            //extract time and values
            final TypedNode[] vectors = bezier.parsedData.getChildren(BEZTRIPLE.VEC);
            //values are a bezier point(index:1) and it's 2 control points(index:0 and 2)
            //we take the time and values at bezier point
            for(int i=0;i<vectors.length;i++){
                final float[][] array = (float[][]) vectors[i].getValue();
                //time is in 10th of seconds
                final float time = array[1][0] *100;
                final float value = array[1][1];
                
                //cget or create keyframe
                JointKeyFrame keyFrame = (JointKeyFrame) serie.getFrame(time);
                if(keyFrame==null){
                    keyFrame = new JointKeyFrame();
                    keyFrame.setValue(new NodeTransform(3));
                    keyFrame.setTime(time);
                    serie.getFrames().add(keyFrame);
                }
                keyFrame.setJoint(boneName);
                final NodeTransform poseTrs = keyFrame.getValue();
                
                //set keyframe value
                //we cheat for a short time, storing a quaternion in the matrix
                
                if(propertyName.getFirstOccurence(BlendConstants.RNA_PATH_LOCATION)>=0){
                    poseTrs.getTranslation().set(valueIndex, value);
                }else if(propertyName.getFirstOccurence(BlendConstants.RNA_PATH_ROTATION_QUATERNION)>=0){
                    //blender use order w,x,y,z.
                    if(valueIndex==0){
                        poseTrs.getRotation().set(0, 0, value);
                    }else{
                        poseTrs.getRotation().set(1, valueIndex-1, value);
                    }
                }else if(propertyName.getFirstOccurence(BlendConstants.RNA_PATH_ROTATION_AXIS_ANGLE)>=0){
                    if(valueIndex==0){
                        poseTrs.getRotation().set(0, 0, value);
                    }else{
                        poseTrs.getRotation().set(1, valueIndex-1, value);
                    }
                    isAxisAngle = true;
                }else if(propertyName.getFirstOccurence(BlendConstants.RNA_PATH_SCALE)>=0){
                    poseTrs.getScale().set(valueIndex, value);
                }
                
                //la rotation_quaternion ou rotation_axis_angle
                //http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/        
            }

            if(curveBlock==lastCurveBlock)break;
            //check if there is a next curve
            curveBlock = (BlendFileBlock)curveBlock.parsedData.getChild(KEYBLOCK.NEXT).getValue();
        }
        
        //convert the axis angle or quaternion rotations in matrix
        //NOTE : the rotation seems to be stored as absolute, not just a complement
        //to bone rotation, we expect those to be relative so we remove the transform.
        final Iterator ite = serie.getFrames().createIterator();
        while(ite.hasNext()){
            final JointKeyFrame jointFrame = (JointKeyFrame)ite.next();
            final MatrixRW rm = jointFrame.getValue().getRotation();
            if(isAxisAngle){
                //axis angle
                final Vector axis = new Vector(3);
                axis.set(rm.getRow(1));
                if(axis.isZero()){
                    rm.setToIdentity();
                }else{
                    Vectors.normalize(axis.getValues(), axis.getValues());
                    final double angle = rm.get(0, 0);
                    rm.set(Matrices.createRotation3(angle, axis, null));
                }
            }else{
                //quaternion
                final Quaternion q = new Quaternion();
                q.set(0,rm.get(1,1));
                q.set(1,rm.get(1,2));
                q.set(2,rm.get(1,0));
                q.set(3, rm.get(0, 0));
                rm.set(BlendConstants.toMatrix3(q));
            }
            
            //make rotation relative
            final MatrixRW baseRot = skeleton.getJoint(boneName).getNodeTransform().getRotation();
            rm.set(baseRot.invert().multiply(rm));
            
            jointFrame.getValue().notifyChanged();
        }
        
    }
    
    private MultipartMesh convertArmature(BlendArmature block){
        final MultipartMesh mpm = new MultipartMesh(BlendConstants.COORDSYS);
        final Skeleton skeleton = convertSkeleton(block);
        mpm.setSkeleton(skeleton);
        objects.add(block, mpm);
        return mpm;
    }
    
    public Material convertMaterial(BlendMaterial block){   
        final Material material = new Material();
        material.setName(block.getId().getName());
        
        //set base colors
        material.setDiffuse(block.getDiffuse());
        material.setSpecular(block.getSpecular());
        material.setShininess(block.getSpecularIntensity());
        
        //rebuild diffuse textures if any
        final BlendMTexture[] materialTextures = block.getMaterialTextures();
        for(BlendMTexture materialTexture : materialTextures){
            if(materialTexture==null) continue;
            final BlendTexture texture = materialTexture.getTexture();
            if(texture==null || texture.getImages().length==0) continue;            
            final Chars imageName = texture.getImages()[0].getImageName();
            if(imageName==null || imageName.isEmpty()) continue;
            
            //rebuild the texture
            final Path path = file.filePath.getParent().resolve(imageName);
            Texture2D tex = (Texture2D) textureCache.getValue(path.toURI());
            if(tex==null){
                try {
                    final boolean clip = texture.getExtension() == 2;
                    final Image image = Images.read(path);
                    tex = new Texture2D(image,true,clip);
                    tex.setName(texture.getId().getName());
                    textureCache.add(path.toURI(),tex);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    continue;
                }
            }
            
            final UVMapping mapping = new UVMapping(tex);
            mapping.getScale().set(materialTexture.getScale());
            mapping.getOffset().set(materialTexture.getOffset());
            mapping.getOffset().setY(-mapping.getOffset().getY());
            
            if((materialTexture.getMapto() & 0x1) !=0){
                //diffuse texture
                final Layer texDiffuse = new Layer(mapping,Layer.TYPE_DIFFUSE);
                material.getLayers().add(texDiffuse);
                
            }else if((materialTexture.getMapto() & 0x2) != 0){
                //normal texture
                final Layer texNormal = new Layer(mapping,Layer.TYPE_NORMAL);
                material.getLayers().add(texNormal);
            }
        }
                
        //rebuild alpha if not 1.0
        float alpha = block.getAlpha();
        if(alpha!=1.0f){
            final Layer layerDiffuse = new Layer(new PlainColorMapping(new Color(1, 1, 1,alpha)),Layer.TYPE_DIFFUSE);
            layerDiffuse.setMethod(Layer.METHOD_MULTIPLY);
            material.getLayers().add(layerDiffuse);
        }
        
                        
        //rebuild mirror if any
        final float reflection = block.getReflection();
        if(reflection>0f){
            final ReflectionMapping reflecMapping = new ReflectionMapping();
            final Layer reflectLayer = new Layer(reflecMapping, Layer.TYPE_DIFFUSE, Layer.METHOD_ADDITIVE, reflection);
            material.getLayers().add(reflectLayer);
        }
        
        return material;
    }

    private Skeleton convertSkeleton(BlendArmature block) {
        final Skeleton skeleton = new Skeleton(BlendConstants.COORDSYS);
        
        final Iterator boneRoots = block.getRefList(BONE_ARMATURE.BONE).createIterator();
        while(boneRoots.hasNext()){
            final BlendFileBlock boneBlock = (BlendFileBlock) boneRoots.next();
            //rebuild joints
            final Joint joint = convertJoint(null,boneBlock);
            skeleton.getChildren().add(joint);
        }
        
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
        
        return skeleton;
    }
    
    private Joint convertJoint(final Joint parent, BlendFileBlock block){
        
        final Chars name = (Chars) block.parsedData.getChild(BONE.NAME).getValue();
        final float[][] matrixA = (float[][]) block.parsedData.getChild(BONE.MATRIX_ARM).getValue();
        final Matrix4x4 matrix = new Matrix4x4();
        matrix.setCol(0, Arrays.reformatDouble(matrixA[0]));
        matrix.setCol(1, Arrays.reformatDouble(matrixA[1]));
        matrix.setCol(2, Arrays.reformatDouble(matrixA[2]));
        matrix.setCol(3, Arrays.reformatDouble(matrixA[3]));
        matrix.localInvert();
        
        final Joint joint = new Joint(3);
        joint.setLocalCoordinateSystem(BlendConstants.COORDSYS);
        if(parent!=null)parent.getChildren().add(joint);
        joint.setName(name);
        joint.setRootToNodeSpace(new Affine3(matrix));
        
        //read children bones
        final TypedNode baseBone = block.parsedData.getChild(BONE.CHILD);
        final BlendFileBlock childAdr = (BlendFileBlock)baseBone.getChild(BONE.CHILD_FIRST).getValue();
        if(childAdr!=null){
            convertJoint(joint, childAdr);
        }
        
        //read next bone
        final BlendFileBlock nextAdr = (BlendFileBlock)block.parsedData.getChild(BONE.NEXT).getValue();
        if(nextAdr!=null){
            convertJoint(parent, nextAdr);
        }
        
        objects.add(block, joint);
        return joint;
    }
    
    private void convertBonePose(BlendBonePose poseBlock, MultipartMesh mpm) {
        final Skeleton skeleton = mpm.getSkeleton();
        final Iterator ite = poseBlock.getChannels().createIterator();
        while(ite.hasNext()){
            final BlendBonePoseChannel bChannel = (BlendBonePoseChannel) ite.next();
            final Chars boneName = bChannel.getBoneName();
            final Joint joint = skeleton.getJoint(boneName);
            final Iterator cite = bChannel.getConstraints().createIterator();
            while(cite.hasNext()){
                final BlendBoneConstraint bcst = (BlendBoneConstraint) cite.next();
                final BlendFileBlock cst = bcst.getConstraint();
                if(cst instanceof BlendBoneKinematicConstraint){
                    final BlendBoneKinematicConstraint kcst = (BlendBoneKinematicConstraint) cst;
                    final int chainLength = kcst.getChainLength();
                    final Joint target = skeleton.getJoint(kcst.getSubTarget());
                    final Joint[] chain;
                    Joint jt = joint;
                    if(chainLength<=0){
                        //until we reach the root
                        final Sequence chainJoints = new ArraySequence();
                        while(true){
                            Object cdt = jt.getParent();
                            if(!(cdt instanceof Joint))break;
                            chainJoints.add(cdt);
                            jt = (Joint) cdt;
                        }
                        chain = new Joint[chainJoints.getSize()];
                        Collections.copy(chainJoints, chain, 0);
                    }else{
                        chain = new Joint[chainLength-1];
                        for(int i=0;i<chain.length;i++){
                            jt = (Joint) jt.getParent();
                            chain[i] = jt;
                        }
                    }
                    
                    
                    final IKSolver solver = new IKSolverCCD(10, 100, 3);
                    final InverseKinematic ik = new InverseKinematic(target, joint, chain, solver);
                    skeleton.getIks().add(ik);
                }
            }
        }
        
    }
    
    private void rebuildRigging(BlendFileBlock block, MultipartMesh mpm, Mesh mesh){
        
        //in the case of armature the matrix is not relation to it like in
        //a classic parent > node relation matrix
        //our api expect a classic relation so we fix the child transform
        final Matrix prmatrix = mpm.getNodeTransform().asMatrix();
        final Matrix jtmatrix = mesh.getNodeTransform().asMatrix();
        final MatrixRW cal = prmatrix.copy();
        cal.localInvert();
        cal.localMultiply(jtmatrix);
        mesh.getNodeTransform().set(cal);
        
        
        //read deform groups
        final TypedNode defbase = block.parsedData.getChild(OBJECT.DEFORMBASE);
        final Dictionary defGroupBoneIndex = new HashDictionary();
        final Sequence joints = mpm.getSkeleton().getAllJoints();
        
        BlendFileBlock defblock = (BlendFileBlock)defbase.getChild(OBJECT.DEFORMBASE_FIRST).getValue();
        int i = 0;
        while(defblock!=null){
            final Chars groupName = (Chars) defblock.parsedData.getChild(BDEFORMGROUP.NAME).getValue();
            
            //find the index of this group in the skeleton bone list
            for(int k=0,kn=joints.getSize();k<kn;k++){
                final Joint jt = (Joint) joints.get(k);
                if(groupName.equals(jt.getName())){
                    defGroupBoneIndex.add(i, k);
                }
            }
            
            //check if there is a next group
            defblock = (BlendFileBlock)defblock.parsedData.getChild(BASE.NEXT).getValue();
            i++;
        }
        
        final SkinShell skin = (SkinShell) mesh.getShape();
        
        //fix joint indexes
        final int[] jointIndex = skin.getJointIndexes().getPrimitiveBuffer().toIntArray();
        for(int p=0;p<jointIndex.length;p++){
            jointIndex[p] = (Integer)defGroupBoneIndex.getValue(jointIndex[p]);
        }
        skin.getJointIndexes().getPrimitiveBuffer().writeInt(jointIndex, 0);
        
        
        skin.setSkeleton(mpm.getSkeleton());
    }

}
