
package science.unlicense.impl.model3d.blend;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Extend data input stream with blender pointer reader.
 *
 * @author Johann Sorel
 */
public class BlendDataInputStream extends DataInputStream{

    private byte pointerSize = BlendConstants.POINTER_4;

    private final ByteSequence buffer = new ByteSequence();

    public BlendDataInputStream(ByteInputStream in) {
        super(in);
    }

    public BlendDataInputStream(ByteInputStream in, NumberEncoding encoding) {
        super(in, encoding);
    }

    public void setPointerSize(byte pointerSize) {
        this.pointerSize = pointerSize;
    }

    public byte getPointerSize() {
        return pointerSize;
    }

    public long readPointer() throws IOException{
        if(pointerSize == BlendConstants.POINTER_4){
            return readUInt();
        }else if(pointerSize == BlendConstants.POINTER_8){
            return readLong();
        }else{
            throw new IOException("Unknowed pointer size : "+pointerSize);
        }
    }
    
    public long[] readPointers(long[] buffer) throws IOException{
        if(pointerSize == BlendConstants.POINTER_4){
            return readUInt(buffer);
        }else if(pointerSize == BlendConstants.POINTER_8){
            return readLong(buffer);
        }else{
            throw new IOException("Unknowed pointer size : "+pointerSize);
        }
    }

    public Chars readChars(int size) throws IOException {
        return new Chars(readFully(new byte[size]));
    }

    public Chars readCharsEndZero() throws IOException {
        buffer.removeAll();
        for(byte b=readByte();b!=0;b=readByte()){
            buffer.put(b);
        }
        return new Chars(buffer.toArrayByte());
    }

    public static byte[] trimAtZero(byte[] ba){
        final int index = Arrays.getFirstOccurence(ba, 0, ba.length, (byte)0);
        if(index>=0){
            ba = Arrays.copy(ba, 0, index);
        }
        return ba;
    }
    
}
