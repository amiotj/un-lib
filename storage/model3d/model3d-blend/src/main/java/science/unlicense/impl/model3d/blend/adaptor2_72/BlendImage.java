
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendImage extends BlendIdentified{

    public BlendImage(BlendFileBlock block) {
        super(block);
    }
    
    public Chars getImageName(){
        Chars name = (Chars) parsedData.getChild(Blend_2_72.IMAGE.NAME).getValue();
        name = name.replaceAll(new Chars("//"), Chars.EMPTY);
        return name;
    }
    
}
