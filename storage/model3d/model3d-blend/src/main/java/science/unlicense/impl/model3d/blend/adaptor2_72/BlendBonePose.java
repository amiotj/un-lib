
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBonePose extends BlendFileBlock{

    public BlendBonePose(BlendFileBlock block) {
        super(block);
    }

    public Sequence getChannels(){
        return getRefList(Blend_2_72.BONE_POSE.CHANBASE);
    }
    
}
