

package science.unlicense.impl.model3d.blend.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.NodeType;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.Node;
import science.unlicense.impl.model3d.blend.BlendConstants;
import science.unlicense.impl.model3d.blend.BlendDataInputStream;

/**
 *
 * @author Johann Sorel
 */
public class BlendFileBlock extends CObject{

    public BlendFileBlockHeader header;
    public long fileOffset;
    /**
     * Contains the raw byte datas of this file block, excluding the header.
     */
    public byte[] rawdata;
    /**
     * Contains a transformed version of the raw data as a typed node.
     * The NodeType is dynamicly rebuild from the SDNA block definition.
     */
    public TypedNode parsedData;

    public BlendFileBlock() {
    }
    
    public BlendFileBlock(BlendFileBlock block) {
        this.header = block.header;
        this.fileOffset = block.fileOffset;
        this.rawdata = block.rawdata;
        this.parsedData = block.parsedData;
    }
    
    public void read(BlendDataInputStream ds, Chars code) throws IOException, StoreException{
        header = new BlendFileBlockHeader();
        header.code = code;
        header.size = ds.readInt();
        header.oldMemoryAddress = ds.readPointer();
        header.SDNAIndex = ds.readInt();
        header.count = ds.readInt();
        rawdata = ds.readFully(new byte[header.size]);
    }

    public void parse(BlendFileHeader fileHeader, BlendSDNA dna) throws IOException, StoreException{

        //prepare stream
        final ArrayInputStream as = new ArrayInputStream(rawdata);
        final BlendDataInputStream ds = new BlendDataInputStream(
                as,BlendFileHeader.getEncoding(fileHeader.endianess));
        ds.setPointerSize(fileHeader.pointerSize);

        //get type parsed in SDNA
        final NodeType type = dna.getStructures()[header.SDNAIndex];
        

        //rebuild node values
        parsedData = new DefaultTypedNode(type);
        final NodeCardinality[] properties = type.getChildrenTypes();

//        if(BlendConstants.BLOCK_TYPE_LINK.equals(typeName)){
//            final int bytesize = BlendConstants.getBtePointerSize(fileHeader.pointerSize)*2;
//            if(header.count==1 && header.size < bytesize){
//                //empty link block
//                return;
//            }
//        }
        
        for(int k=0;k<header.count;k++){
            final Node[] children = new Node[properties.length];
            try{
                for(int i=0;i<properties.length;i++){
                    final BlendNodeCardinality card = (BlendNodeCardinality) properties[i];
                    children[i] = card.read(ds);
                }
                parsedData.getChildren().addAll(children);
            }catch(IOException ex){
                for(int i=0;i<children.length;i++){
                    if(children[i]!=null){
                        parsedData.getChildren().add(children[i]);
                    }
                }
                final Chars typeName = type.getId();
                if(BlendConstants.BLOCK_TYPE_LINK.equals(typeName)){
                    //link blocks are not always filled
                    //TODO find some doc about this
                    return;
                }else{
                    throw ex;
                }
            }catch(RuntimeException ex){
                final Chars typeName = type.getId();
                if(BlendConstants.BLOCK_TYPE_LINK.equals(typeName)){
                    //link blocks are not always filled
                    //TODO find some doc about this
                    return;
                }else{
                    throw ex;
                }
            }
        }

    }
    
    /**
     * Get a list of references.
     * 
     * @param propName
     * @return 
     */
    public Sequence getRefList(Chars propName){
        final TypedNode base = parsedData.getChild(propName);        
        final BlendFileBlock firstBlock = (BlendFileBlock)base.getChild(BlendConstants.FIRST).getValue();        
        final BlendFileBlock lastBlock = (BlendFileBlock)base.getChild(BlendConstants.LAST).getValue();
        
        final Sequence blocks = new ArraySequence();
        if(firstBlock==null) return blocks;
        
        BlendFileBlock block = firstBlock;
        while(block!=lastBlock){
            blocks.add(block);
            //get next block
            final TypedNode tn = block.parsedData.getChild(BlendConstants.NEXT);
            block = (BlendFileBlock)tn.getValue();
        }
        blocks.add(lastBlock);
        
        return blocks;
    }
    
    public Sequence getModifierList(Chars propName){
        final TypedNode base = parsedData.getChild(propName);        
        final BlendFileBlock firstBlock = (BlendFileBlock)base.getChild(BlendConstants.FIRST).getValue();        
        final BlendFileBlock lastBlock = (BlendFileBlock)base.getChild(BlendConstants.LAST).getValue();
        
        final Sequence blocks = new ArraySequence();
        if(firstBlock==null) return blocks;
        
        BlendFileBlock block = firstBlock;
        while(block!=lastBlock){
            blocks.add(block);
            final TypedNode tn = block.parsedData.getChild(BlendConstants.MODIFIER).getChild(BlendConstants.NEXT);
            block = (BlendFileBlock)tn.getValue();
        }
        blocks.add(lastBlock);
        
        return blocks;
    }
    
    public Chars toChars() {
        Chars txt = new Chars("BLOCK : ").concat(header.toChars()).concat(' ');
        if(parsedData!=null) txt = txt.concat(parsedData.toCharsTree(20));
        return txt;
    }
    
}