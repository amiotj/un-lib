
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendArmature extends BlendIdentified{

    public BlendArmature(BlendFileBlock block) {
        super(block);
    }

}
