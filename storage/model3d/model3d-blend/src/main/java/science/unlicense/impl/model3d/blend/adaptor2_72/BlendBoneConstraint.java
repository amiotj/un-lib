
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBoneConstraint extends BlendFileBlock{

    public BlendBoneConstraint(BlendFileBlock block) {
        super(block);
    }

    public Chars getName(){
        return (Chars) parsedData.getChild(Blend_2_72.BONE_CONSTRAINT.NAME).getValue();
    }
    
    public BlendFileBlock getConstraint(){
        return (BlendFileBlock) parsedData.getChild(Blend_2_72.BONE_CONSTRAINT.DATA).getValue();
    }
    
}
