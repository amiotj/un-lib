
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendArmatureModifier extends BlendFileBlock{

    public BlendArmatureModifier(BlendFileBlock block) {
        super(block);
    }
    
    public Chars getModifierName(){
        final TypedNode m = parsedData.getChild(Blend_2_72.ARMATURE_MODIFIER_DATA.MODIFIER);
        final TypedNode name = m.getChild(Blend_2_72.ARMATURE_MODIFIER_DATA.MODIFIER_NAME);
        return (Chars) name.getValue();
    }
    
    public BlendObject getArmature(){
        return (BlendObject) parsedData.getChild(Blend_2_72.ARMATURE_MODIFIER_DATA.OBJECT).getValue();
    }
    
}
