
package science.unlicense.impl.model3d.blend;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Blender Format.
 *
 * Only resources available :
 * http://www.blender.org/development/architecture/blender-file-format/
 * http://www.atmind.nl/blender/mystery_ot_blend.html
 *
 * @author Johann Sorel
 */
public class BlendFormat extends AbstractModel3DFormat{

    public static final BlendFormat INSTANCE = new BlendFormat();

    private BlendFormat() {
        super(new Chars("BLEND"),
              new Chars("Blend"),
              new Chars("Blender3D"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("blend")
              },
              new byte[][]{BlendConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new BlendStore(input);
    }

}