

package science.unlicense.impl.model3d.blend.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.NodeCardinality;
import science.unlicense.api.model.tree.NodeType;
import science.unlicense.impl.model3d.blend.BlendConstants;
import science.unlicense.impl.model3d.blend.BlendDataInputStream;
import static science.unlicense.impl.model3d.blend.model.BlendFile.realign;

/**
 *
 * @author Johann Sorel
 */
public class BlendSDNA extends BlendFileBlock {
    
    private NodeType[] structs;
    
    public NodeType[] getStructures() {
        return structs;
    }
    
    public void parse(BlendFileHeader fileHeader, BlendSDNA dna) throws IOException, StoreException{
        //prepare stream
        final ArrayInputStream as = new ArrayInputStream(rawdata);
        final BlendDataInputStream ds = new BlendDataInputStream(
                as,BlendFileHeader.getEncoding(fileHeader.endianess));
        ds.setPointerSize(fileHeader.pointerSize);

        Chars temp = ds.readChars(4);
        if(!BlendConstants.DNA_SDNA.equals(temp)){
            throw new IOException("Was expecting 'SDNA' value but was "+temp);
        }

        //parse names
        temp = ds.readChars(4);
        if(!BlendConstants.DNA_NAME.equals(temp)){
            throw new IOException("Was expecting 'NAME' value but was "+temp);
        }
        final int nbName = ds.readInt();
        final Chars[] names = new Chars[nbName];
        for(int i=0;i<nbName;i++){
            names[i] = ds.readCharsEndZero();
        }
        realign(as);

        //parse data types
        temp = ds.readChars(4);
        if(!BlendConstants.DNA_TYPE.equals(temp)){
            throw new IOException("Was expecting 'TYPE' value but was "+temp);
        }
        final int nbType = ds.readInt();
        final Chars[] types = new Chars[nbType];
        for(int i=0;i<nbType;i++){
            types[i] = ds.readCharsEndZero();
        }
        realign(as);

        //parse type length
        temp = ds.readChars(4);
        if(!BlendConstants.DNA_TLEN.equals(temp)){
            throw new IOException("Was expecting 'TLEN' value but was "+temp);
        }
        final int[] lengths = new int[nbType];
        for(int i=0;i<nbType;i++){
            lengths[i] = ds.readUShort();
        }
        realign(as);

        //parse structure
        temp = ds.readChars(4);
        if(!BlendConstants.DNA_STRC.equals(temp)){
            throw new IOException("Was expecting 'STRC' value but was "+temp);
        }
        final int nbStruct = ds.readInt();
        structs = new NodeType[nbStruct];
        final Dictionary dico = new HashDictionary();
        for(int i=0;i<nbStruct;i++){

            final Chars structName = types[ds.readUShort()];
            final BlendNodeType nodeType = new BlendNodeType(structName);
            structs[i] = nodeType;
            dico.add(structName, nodeType);

            final int nbField = ds.readUShort();
            final NodeCardinality[] fildDescs = new NodeCardinality[nbField];
            for(int k=0;k<nbField;k++){
                final int typeIndex = ds.readUShort();
                final int nameIndex = ds.readUShort();
                final Chars fieldName = names[nameIndex];
                final Chars fieldType = types[typeIndex];
                final int fieldLength = lengths[typeIndex];

                final BlendNodeCardinality card = new BlendNodeCardinality(fieldName, fieldType, fieldLength);
                fildDescs[k] = card;
            }
            nodeType.setChildrenTypes(fildDescs);
        }

        //make a second pass to set the sub complex types
        for(int i=0;i<nbStruct;i++){
            final NodeType nodeType = structs[i];
            final NodeCardinality[] properties = nodeType.getChildrenTypes();

            for(int k=0;k<properties.length;k++){
                final BlendNodeCardinality card = (BlendNodeCardinality) properties[k];
                final BlendNodeType type = (BlendNodeType) card.getType();
                if(type.isComplex()){
                    final Chars typeName = type.getId();
                    final BlendNodeType bnt = (BlendNodeType) dico.getValue(typeName);
                    if(bnt == null){
                        throw new IOException("unknowned type : "+typeName);
                    }
                    card.setType((BlendNodeType)dico.getValue(typeName));
                }
            }
        }


    }
}
