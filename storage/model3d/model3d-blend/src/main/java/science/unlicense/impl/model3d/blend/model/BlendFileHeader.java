
package science.unlicense.impl.model3d.blend.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.model3d.blend.BlendConstants;

/**
 *
 * @author Johann Sorel
 */
public class BlendFileHeader {

    /** 1 byte */
    public byte pointerSize;
    /** 1 byte */
    public byte endianess;
    /** 3 byte */
    public Chars version;

    public static NumberEncoding getEncoding(byte endianess) throws StoreException{
        if(endianess == BlendConstants.LITTLE_ENDIAN){
            return NumberEncoding.LITTLE_ENDIAN;
        }else if(endianess == BlendConstants.BIG_ENDIAN){
            return NumberEncoding.BIG_ENDIAN;
        }else{
            throw new StoreException("Unknowned encoding : "+endianess);
        }
    }


}