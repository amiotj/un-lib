
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBone extends BlendFileBlock{

    public BlendBone(BlendFileBlock block) {
        super(block);
    }

}
