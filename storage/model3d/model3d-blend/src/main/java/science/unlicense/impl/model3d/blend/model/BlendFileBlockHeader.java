

package science.unlicense.impl.model3d.blend.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class BlendFileBlockHeader extends CObject {

    /** char[4]	Identifier of the file-block */
    public Chars code;
    /** integer	Total length of the data after the file-block-header */
    public int size;
    /** void*	Memory address the structure was located when written to disk, pointer-size (4/8) */
    public long oldMemoryAddress;
    /** integer	Index of the SDNA structure */
    public int SDNAIndex;
    /** integer	Number of structure located in this file-block */
    public int count;

    public Chars toChars() {
        return code.concat(new Chars(" (")).concat(new Chars(Long.toString(oldMemoryAddress))).concat(new Chars("*)"));
    }

}