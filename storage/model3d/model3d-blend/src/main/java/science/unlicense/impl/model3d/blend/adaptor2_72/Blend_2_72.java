
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.model3d.blend.BlendConstants;

/**
 * Model of a Blender file version 2.72b.
 * 
 * @author Johann Sorel
 */
public class Blend_2_72 extends Blend {

    //blocks
    public static final Chars BLOCK_DATA = new Chars(new byte[]{'D','A','T','A'});
    
    public interface IDNODE{        
        public static final Chars NEXT     = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        /** name is prefixed by 2chars which indicate the type */
        public static final Chars NAME     = new Chars("name");
    }
    
    public interface SCENE{        
        public static final Chars CODE = new Chars(new byte[]{'S','C'});
        public static final Chars SCENE     = new Chars("Scene");
        public static final Chars BASA      = new Chars("*basa");
        public static final Chars BASE      = new Chars("base");
        public static final Chars BASE_FIRST= BlendConstants.FIRST;
        public static final Chars BASE_LAST = BlendConstants.LAST;
    }
    
    public interface BASE{        
        public static final Chars BASE          = new Chars("Base");
        /** This property points to an object */
        public static final Chars OBJECT   = new Chars("*object");
        public static final Chars NEXT     = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
    }
    
    public interface GROUP{        
        public static final Chars GROUP         = new Chars("Group");
        /** Name is prefixed by GR */
        public static final Chars ID            = BlendConstants.ID;
        public static final Chars GOBJECT       = new Chars("gobject");
        public static final Chars GOBJECT_FIRST = BlendConstants.FIRST;
        public static final Chars GOBJECT_LAST  = BlendConstants.LAST;
    }
    
    public interface GROUP_OBJECT{        
        public static final Chars GROUP_OBJECT = new Chars("GroupObject");
        public static final Chars NEXT     = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        /** This property points to an object */
        public static final Chars OBJECT   = new Chars("*ob");
    }
    
    public interface OBJECT{        
        public static final Chars OBJECT = new Chars("Object");
        /** This property points to the real type, can be a mesh, armature, ... */
        public static final Chars DATA   = new Chars("*data");
        /** object parent to node matrix */
        public static final Chars MATRIX = new Chars("obmat");
        public static final Chars PARENT = BlendConstants.PARENT;
        public static final Chars ID     = new Chars("id");
        /** 
         * bone deform model : 
         * Object.defbase > N:bDeformGroup  : groups have a name which is the same as the bone
         * Mesh.dve > MDeformVert > (for each vertex):MDeformWeight
         * MDeformWeight.def_nr : point to the index of a bDeformGroup
         */
        public static final Chars DEFORMBASE = new Chars("defbase");
        public static final Chars DEFORMBASE_FIRST = BlendConstants.FIRST;
        public static final Chars DEFORMBASE_LAST = BlendConstants.LAST;
        /** Links to an AnimData */
        public static final Chars ADT = new Chars("*adt");
        /** This is used to indicate an instance rendering */
        public static final Chars DUPLICATE_GROUP = new Chars("*dup_group");
        /** used by armature object type, link to a bPose */
        public static final Chars POSE = new Chars("*pose");
        /** modifiers list */
        public static final Chars MODIFIERS = new Chars("modifiers");
        public static final Chars MODIFIERS_FIRST = BlendConstants.FIRST;
        public static final Chars MODIFIERS_LAST = BlendConstants.LAST;
    }
    
    public interface MESH{        
        public static final Chars MESH     = new Chars("Mesh");
        public static final Chars MATERIAL = new Chars("**mat");
        public static final Chars MVERT    = new Chars("*mvert");
        public static final Chars MFACE    = new Chars("*mface");
        public static final Chars MPOLY    = new Chars("*mpoly");
        public static final Chars MLOOP    = new Chars("*mloop");
        //used for uv
        public static final Chars MLOOPUV  = new Chars("*mloopuv");
        /** links to a MDeformVert for bone weights */
        public static final Chars DVERT    = new Chars("*dvert");
        /** links to Key for shapekeys */
        public static final Chars KEY      = new Chars("*key");
    }
    
    public interface ARMATURE_MODIFIER_DATA{        
        public static final Chars ARMATUREMODIFIERDATA = new Chars("ArmatureModifierData");
        public static final Chars MODIFIER = new Chars("modifier");
        /** name of the armature object */
        public static final Chars MODIFIER_NAME = new Chars("name");
        /** link to the object with this modifier */
        public static final Chars OBJECT = new Chars("*object");
    }
        
    public interface MVERT{        
        public static final Chars MVERT   = new Chars("MVert");
        public static final Chars CO      = new Chars("co");
        public static final Chars NO      = new Chars("no");
        public static final Chars FLAG    = BlendConstants.FLAG;
        public static final Chars WEIGHT  = new Chars("bweight");
    }
    
    public interface MFACE{        
        public static final Chars MFACE   = new Chars("MFace");
        public static final Chars V1      = new Chars("v1");
        public static final Chars V2      = new Chars("v2");
        public static final Chars V3      = new Chars("v3");
        public static final Chars V4      = new Chars("v4");
        public static final Chars MAT_NR  = new Chars("mat_nr");
        public static final Chars EDCODE  = new Chars("edcode");
        public static final Chars FLAG    = new Chars("flag");
    }
    
    public interface LOOP{   
        public static final Chars VERTEX  = new Chars("v");
        public static final Chars EDGE    = new Chars("e");
    }
    
    public interface MLOOPUV{   
        public static final Chars UV    = new Chars("uv");
        public static final Chars FLAGS = BlendConstants.FLAG;
    }
    
    public interface MPOLY{   
        public static final Chars LOOPSTART    = new Chars("loopstart");
        public static final Chars TOTALLOOP    = new Chars("totloop");
        public static final Chars MAT_NR       = new Chars("mat_nr");
        public static final Chars FLAG         = new Chars("flag");
        public static final Chars PAD          = new Chars("pad");
    }
    
    public interface MATERIAL{ 
        public static final Chars MATERIAL = new Chars("Material");
        public static final Chars R = new Chars("r");
        public static final Chars G = new Chars("g");
        public static final Chars B = new Chars("b");
        public static final Chars SPECULAR_R = new Chars("specr");
        public static final Chars SPECULAR_G = new Chars("specg");
        public static final Chars SPECULAR_B = new Chars("specb");
        /** specular intensity : ~shininess */
        public static final Chars SPECULAR = new Chars("spec");
        /** transparency alpha */
        public static final Chars ALPHA = new Chars("alpha");
        /** ray_mirror : called reflectivity in the mirror panel */
        public static final Chars RAY_MIRROR = new Chars("ray_mirror");
        //something odd here, textures are not a list
        public static final Chars MTEX = new Chars("*mtex");
        public static final Chars NODETREE = new Chars("*nodetree");
        public static final Chars IPO = new Chars("*ipo");
    }
    
    public interface MTEXTURE{ 
        public static final Chars MTEXTURE = new Chars("MTex");
        public static final Chars TEX = new Chars("*tex");
        /**
         * This contains the texture use : 
         * - 1 : diffuse
         * - 2 : normal
         * - X : TODO find others
         */
        public static final Chars MAPTO = new Chars("mapto");
        /** texture offset */
        public static final Chars OFS = new Chars("ofs");
        /** texture scale */
        public static final Chars SIZE = new Chars("size");
    }
    
    public interface TEXTURE{ 
        public static final Chars TEXTURE = new Chars("Tex");
        public static final Chars IMA = new Chars("*ima");
        /** type of extension :
         * 2 : clip
         * 3 : repeat
         * x : TODO find others
         */
        public static final Chars EXTEND = new Chars("extend");
    }
    
    public interface IMAGE{ 
        public static final Chars IMAGE = new Chars("Image");
        public static final Chars NAME = BlendConstants.NAME;
    }
    
    public interface BONE_ARMATURE{ 
        public static final Chars ARMATURE = new Chars("bArmature");
        public static final Chars BONE = new Chars("bonebase");
        public static final Chars BONE_FIRST = BlendConstants.FIRST;
        public static final Chars BONE_LAST = BlendConstants.LAST;
    }
    
    public interface BONE{ 
        public static final Chars BONE = new Chars("Bone");
        public static final Chars NAME = BlendConstants.NAME;
        public static final Chars PARENT = BlendConstants.PARENT;
        public static final Chars NEXT = BlendConstants.NEXT;
        public static final Chars CHILD = new Chars("childbase");
        public static final Chars CHILD_FIRST = BlendConstants.FIRST;
        public static final Chars CHILD_LAST = BlendConstants.LAST;
        public static final Chars _ROLL = new Chars("roll");
        /** column order 2D array for a Matrix 3x3 */
        public static final Chars MATRIX_BONE = new Chars("bone_mat");
        public static final Chars LENGTH = new Chars("length");
        /** column order 2D array for a Matrix 4x4 : root to node matrix */
        public static final Chars MATRIX_ARM = new Chars("arm_mat");
    }
    
    public interface MDEFORMVERT{ 
        public static final Chars MDEFORMVERT = new Chars("MDeformVert");
        /** link to a MDeformWeight */
        public static final Chars DW = new Chars("*dw");
        public static final Chars TOTWEIGHT = new Chars("totweight");
        public static final Chars FLAG = BlendConstants.FLAG;
    }
        
    public interface MDEFORMWEIGHT{ 
        public static final Chars MDEFORMWEIGHT = new Chars("MDeformWeight");
        public static final Chars DEF = new Chars("def_nr");
        public static final Chars WEIGHT = new Chars("weight");
    }
    
    public interface BDEFORMGROUP{ 
        /** Those are VertexGRoup in Blender : Bone deform name must match bone name */
        public static final Chars BDEFORMGROUP = new Chars("bDeformGroup");
        public static final Chars NAME = BlendConstants.NAME;
    }
    
    public interface BONE_POSE{ 
        public static final Chars BPOSE = new Chars("bPose");
        /** liste of bPoseChannel */
        public static final Chars CHANBASE = new Chars("chanbase");
        public static final Chars CHANBASE_FIRST = BlendConstants.FIRST;
        public static final Chars CHANBASE_LAST = BlendConstants.LAST;
    }
    
    public interface BONE_POSE_CHANNEL{ 
        public static final Chars BPOSECHANNEL = new Chars("bPoseChannel");
        public static final Chars NEXT = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        public static final Chars NAME = BlendConstants.NAME;
        public static final Chars PARENT = BlendConstants.PARENT;
        public static final Chars CHILD = BlendConstants.CHILD;
        /** liste of bConstraint */
        public static final Chars CONSTRAINTS = new Chars("constraints");
        public static final Chars CONSTRAINTS_FIRST = BlendConstants.FIRST;
        public static final Chars CONSTRAINTS_LAST = BlendConstants.LAST;
    }
    
    public interface BONE_CONSTRAINT{ 
        public static final Chars BCONSTRAINT = new Chars("bConstraint");
        public static final Chars NEXT = BlendConstants.NEXT;
        public static final Chars PREVIOUS = BlendConstants.PREVIOUS;
        public static final Chars NAME = BlendConstants.NAME;
        /** point to various constraints type : bKinematicConstraint... */
        public static final Chars DATA = new Chars("*data");
        public static final Chars TYPE = new Chars("type");
        public static final Chars FLAG = BlendConstants.FLAG;
    }
    
    public interface BONE_KINEMATIC_CONSTRAINT{ 
        public static final Chars BKINEMATICCONSTRAINT = new Chars("bKinematicConstraint");
        /** target : armature object */
        public static final Chars TARGET = new Chars("*tar");
        /** kinematic chain length */
        public static final Chars ROOTBONE = new Chars("rootbone");
        /** target bone name in the armature */
        public static final Chars SUBTARGET = new Chars("subtarget");
    }
    
    public interface KEY{ 
        public static final Chars KEY = new Chars("Key");
        /** links to KeyBlocks */
        public static final Chars BLOCK = new Chars("block");
        public static final Chars BLOCK_FIRST = BlendConstants.FIRST;
        public static final Chars BLOCK_LAST = BlendConstants.LAST;
    }
    
    public interface KEYBLOCK{ 
        public static final Chars KEYBLOCK  = new Chars("KeyBlock");
        public static final Chars NEXT      = BlendConstants.NEXT;
        public static final Chars PREVIOUS  = BlendConstants.PREVIOUS;
        public static final Chars NAME      = BlendConstants.NAME;
        public static final Chars TYPE      = new Chars("type");
        public static final Chars DATA      = new Chars("*data");
    }
    
    public interface ANIMDATA{
        /** contains animation/motions informations */
        public static final Chars ANIMDATA = new Chars("AnimData");
        /** links to bAction */
        public static final Chars ACTION = new Chars("*action");
    }
    
    public interface BACTION{
        /** contains animation/motions informations */
        public static final Chars BDEFORMGROUP = new Chars("bAction");
        public static final Chars ID = BlendConstants.ID;
        public static final Chars ID_NAME = BlendConstants.NAME;
        /** links to FCurve */
        public static final Chars CURVES = new Chars("curves");
        public static final Chars CURVES_FIRST = BlendConstants.FIRST;
        public static final Chars CURVES_LAST = BlendConstants.LAST;
        /** links to bActionGroup */
        public static final Chars GROUPS = new Chars("groups");
        public static final Chars GROUPS_FIRST = BlendConstants.FIRST;
        public static final Chars GROUPS_LAST = BlendConstants.LAST;
        
    }
    
    public interface FCURVE{
        /** keyframe */
        public static final Chars FCURVE = new Chars("FCurve");
        /** link to a BezTriple */
        public static final Chars BEZT = new Chars("*bezt");
        /** Contains the name of the property modified by this curve */
        public static final Chars RNA_PATH = new Chars("*rna_path");
        /** array index in property vector/quaternion */
        public static final Chars ARRAY_INDEX = new Chars("array_index");
    }
    
    public interface BACTIONGROUP{
        /** keyframe */
        public static final Chars BACTIONGROUP = new Chars("bActionGroup");
        /** links to FCurve */
        public static final Chars CHANNELS = new Chars("channels");
        public static final Chars CHANNELS_FIRST = BlendConstants.FIRST;
        public static final Chars CHANNELS_LAST = BlendConstants.LAST;
        /** name of a bone */
        public static final Chars NAME = BlendConstants.NAME;
        
    }
    
    public interface BEZTRIPLE{
        /** keyframe */
        public static final Chars BEZTRIPLE = new Chars("BezTriple");
        /** 2D array, seems to contains time in second array*/
        public static final Chars VEC = new Chars("vec");
        
    }

    /**
     * Mesh subdivision modifier.
     * Unused
     */
    public interface SUBSURFMODIFIERDATA{

        public static final Chars DISPLACEMEMODIFIERDATA = new Chars("SubsurfModifierData");

    }
    
    /**
     * Mesh displacement mapping.
     */
    public interface DISPLACEMEMODIFIERDATA{

        public static final Chars DISPLACEMEMODIFIERDATA = new Chars("DisplaceModifierData");
        /**
         * texture of the displacement.
         */
        public static final Chars TEXTURE = new Chars("*texture");
        public static final Chars STRENGTH = new Chars("strength");

    }

}
