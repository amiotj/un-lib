
package science.unlicense.impl.model3d.blend;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.coordsys.Axis;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.Direction;
import science.unlicense.api.math.Angles;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.unit.Units;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Quaternion;

/**
 * Blender format constants.
 *
 * @author Johann Sorel
 */
public final class BlendConstants {

    /** Blender file signature */
    public static final byte[] SIGNATURE = new byte[]{'B','L','E','N','D','E','R'};

    public static final CoordinateSystem COORDSYS = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.UP, Units.METER)
            }
    );
    
    /** Possible blocks */
    public static final Chars BLOCK_SDNA = new Chars(new byte[]{'D','N','A','1'});
    public static final Chars BLOCK_ENDB = new Chars(new byte[]{'E','N','D','B'});

    public static final byte LITTLE_ENDIAN = 'v';
    public static final byte BIG_ENDIAN = 'V';

    public static final byte POINTER_4 = '_';
    public static final byte POINTER_8 = '-';

    public static final Chars DNA_SDNA = new Chars(new byte[]{'S','D','N','A'});
    public static final Chars DNA_NAME = new Chars(new byte[]{'N','A','M','E'});
    public static final Chars DNA_TYPE = new Chars(new byte[]{'T','Y','P','E'});
    public static final Chars DNA_TLEN = new Chars(new byte[]{'T','L','E','N'});
    public static final Chars DNA_STRC = new Chars(new byte[]{'S','T','R','C'});

    //data types
    public static final Chars TYPE_INT = new Chars(new byte[]{'i','n','t'});
    public static final Chars TYPE_UINT64 = new Chars(new byte[]{'u','i','n','t','6','4','_','t'});
    public static final Chars TYPE_CHAR = new Chars(new byte[]{'c','h','a','r'});
    public static final Chars TYPE_SHORT = new Chars(new byte[]{'s','h','o','r','t'});
    public static final Chars TYPE_FLOAT = new Chars(new byte[]{'f','l','o','a','t'});
    public static final Chars TYPE_DOUBLE = new Chars(new byte[]{'d','o','u','b','l','e'});
    public static final Chars TYPE_VOID = new Chars(new byte[]{'v','o','i','d'});
    public static final int TYPE_POINTER = '*';
    public static final int TYPE_ARRAY_START = '[';
    public static final int TYPE_ARRAY_END = ']';
    
    public static final Chars BLOCK_TYPE_LINK = new Chars("Link");
    
    public static final Chars RNA_PATH_LOCATION = new Chars("location",CharEncodings.US_ASCII);
    public static final Chars RNA_PATH_ROTATION_QUATERNION = new Chars("rotation_quaternion",CharEncodings.US_ASCII);
    public static final Chars RNA_PATH_ROTATION_AXIS_ANGLE = new Chars("rotation_axis_angle",CharEncodings.US_ASCII);
    public static final Chars RNA_PATH_SCALE = new Chars("scale",CharEncodings.US_ASCII);
    
    private static final double COS45_2 = Math.cos(Angles.degreeToRadian(45.0))*2.0;
        
    //common tag names
    public static final Chars ID = new Chars("id");
    public static final Chars FLAG = new Chars("flag");
    public static final Chars NEXT = new Chars("*next");
    public static final Chars PREVIOUS = new Chars("*prev");
    public static final Chars PARENT = new Chars("*parent");
    public static final Chars CHILD = new Chars("*child");
    public static final Chars FIRST = new Chars("*first");
    public static final Chars LAST = new Chars("*last");
    public static final Chars NAME = new Chars("name");
    public static final Chars MODIFIER = new Chars("modifier");
    
    
    private BlendConstants(){}

    public static int getBtePointerSize(int pointersize) throws StoreException{
        if(pointersize==POINTER_4){
            return 4;
        }else if(pointersize==POINTER_8){
            return 8;
        }else{
            throw new StoreException("Unvalid pointer size : "+pointersize);
        }
    }
    
    /**
     * There are multiple variants to convert a quaternion to a matrix.
     * This is the formula used in blender.
     * We convert it the same way to reduce mathematical differences.
     * 
     * @param q
     * @return 
     */
    public static Matrix3x3 toMatrix3(Quaternion q){

	final double x = COS45_2 * q.getX();
	final double y = COS45_2 * q.getY();
	final double z = COS45_2 * q.getZ();
	final double w = COS45_2 * q.getW();
	final double xx = x * x;
	final double xy = x * y;
	final double xz = x * z;
	final double xw = x * w;
	final double yy = y * y;
	final double yz = y * z;
	final double yw = y * w;
	final double zz = z * z;
	final double zw = z * w;

        final double[][] m = new double[3][3];
	m[0][0] = 1.0 - yy - zz;
	m[0][1] =       zw + xy;
	m[0][2] =      -yw + xz;

	m[1][0] =      -zw + xy;
	m[1][1] = 1.0 - xx - zz;
	m[1][2] =       xw + yz;

	m[2][0] =       yw + xz;
	m[2][1] =      -xw + yz;
	m[2][2] = 1.0 - xx - yy;
        
        return new Matrix3x3(m);
    }
    
}