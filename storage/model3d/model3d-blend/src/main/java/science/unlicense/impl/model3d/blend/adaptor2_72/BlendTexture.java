
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendTexture extends BlendIdentified{

    public BlendTexture(BlendFileBlock block) {
        super(block);
    }

    public BlendImage[] getImages(){
        final Sequence lst = new ArraySequence();
        final TypedNode[] images = parsedData.getChildren(Blend_2_72.TEXTURE.IMA);
        for(TypedNode ti : images){
            final BlendImage imageBlock = (BlendImage)ti.getValue();
            if(imageBlock==null) continue;
            lst.add(imageBlock);
        }
        
        final BlendImage[] imgs = new BlendImage[lst.getSize()];
        Collections.copy(lst, imgs, 0);
        return imgs;
    }
        
    public int getExtension(){
        return ((Number)parsedData.getChild(Blend_2_72.TEXTURE.EXTEND).getValue()).intValue();
    }
    
}
