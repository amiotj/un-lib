
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.engine.opengl.mesh.MeshUtilities;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.blend.BlendConstants;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendMesh extends BlendIdentified{

    public BlendMesh(BlendFileBlock block) {
        super(block);
    }

    public BlendMaterial getMaterial(){
        BlendFileBlock adrM = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.MATERIAL).getValue();
        if(adrM!=null){
            //material has a double pointer
            adrM = (BlendFileBlock) adrM.parsedData.getChild(BlendConstants.NEXT).getValue();
            return (BlendMaterial) adrM;
        }
        return null;
    }
    
    /**
     * Read MVert block, convert to vertex and normal array.
     * 
     * @return 
     */
    public Buffer[] readVNJW(){
        final BlendFileBlock block = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.MVERT).getValue();
        if(block==null) return null;
        
        final TypedNode[] coords    = block.parsedData.getChildren(Blend_2_72.MVERT.CO);
        final TypedNode[] norms     = block.parsedData.getChildren(Blend_2_72.MVERT.NO);
        //final TypedNode[] flags     = block.parsedData.getChildren(Blend_2_72.MVERT.FLAG);
        //final TypedNode[] weights   = block.parsedData.getChildren(Blend_2_72.MVERT.WEIGHT);
        
        final FloatCursor vertex = DefaultBufferFactory.INSTANCE.createFloat(coords.length*3).cursorFloat();
        final FloatCursor normal = DefaultBufferFactory.INSTANCE.createFloat(coords.length*3).cursorFloat();
        
        for(int i=0;i<coords.length;i++){
            vertex.write((float[])coords[i].getValue());
            final short[] n = (short[])norms[i].getValue();
            final Vector nor = new Vector(n[0],n[1],n[2]);
            normal.write(nor.localNormalize().toArrayFloat());
        }
        
        //check if we have joint and weights
        IntCursor jointIndex = null;
        FloatCursor jointWeights = null;
        final BlendFileBlock dve = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.DVERT).getValue();
        if(dve!=null){
            final TypedNode[] vertexWeights = dve.parsedData.getChildren(Blend_2_72.MDEFORMVERT.DW);
            final TypedNode[] nbWeights = dve.parsedData.getChildren(Blend_2_72.MDEFORMVERT.TOTWEIGHT);

            //find nax number of weights
            int maxWeightPerBone = 0;
            for(int k=0;k<nbWeights.length;k++){
                maxWeightPerBone = Maths.max(maxWeightPerBone,(Integer)nbWeights[k].getValue() );
            }
            
            jointIndex = DefaultBufferFactory.INSTANCE.createInt(coords.length*maxWeightPerBone).cursorInt();
            jointWeights = DefaultBufferFactory.INSTANCE.createFloat(coords.length*maxWeightPerBone).cursorFloat();
            
            //read vertex weights and bone index
            for(int k=0,pos=0; k<nbWeights.length; k++,pos+= maxWeightPerBone){
                final BlendFileBlock vertexWeightBlock = (BlendFileBlock)vertexWeights[k].getValue();

                if(vertexWeightBlock==null){
                    //no weights for this node
                    continue;
                }
                
                final TypedNode[] idx = vertexWeightBlock.parsedData.getChildren(Blend_2_72.MDEFORMWEIGHT.DEF);
                final TypedNode[] weights = vertexWeightBlock.parsedData.getChildren(Blend_2_72.MDEFORMWEIGHT.WEIGHT);

                for(int m=0;m<idx.length;m++){
                    //index of a vertex group, will be fixed later in rebuildRiggin method of Adaptor
                    int bidx = (Integer)idx[m].getValue();
                    jointIndex.write(bidx, pos+m);
                    jointWeights.write((Float)weights[m].getValue(), pos+m);
                }
            }
        }
        
        return new Buffer[]{
            vertex.getBuffer(),
            normal.getBuffer(),
            jointIndex!=null ? jointIndex.getBuffer() : null,
            jointWeights!=null ? jointWeights.getBuffer() : null
        };
    }
    
    /**
     * Read MFace block, convert to index buffer.
     * 
     * @return 
     */
    public Buffer readFaces(){
        final BlendFileBlock block = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.MFACE).getValue();
        if(block==null) return null;
        
        final TypedNode[] v1    = block.parsedData.getChildren(Blend_2_72.MFACE.V1);
        final TypedNode[] v2    = block.parsedData.getChildren(Blend_2_72.MFACE.V2);
        final TypedNode[] v3    = block.parsedData.getChildren(Blend_2_72.MFACE.V3);
        final TypedNode[] v4    = block.parsedData.getChildren(Blend_2_72.MFACE.V4);
        final TypedNode[] mats  = block.parsedData.getChildren(Blend_2_72.MFACE.MAT_NR);
        final TypedNode[] edges = block.parsedData.getChildren(Blend_2_72.MFACE.EDCODE);
        final TypedNode[] flags = block.parsedData.getChildren(Blend_2_72.MFACE.FLAG);
        
        final IntCursor ib;
        if(v4.length==0){
            //triangles
            ib = DefaultBufferFactory.INSTANCE.createInt(v1.length*3).cursorInt();
            for(int i=0;i<v1.length;i++){
                ib.write((Integer)v1[i].getValue());
                ib.write((Integer)v2[i].getValue());
                ib.write((Integer)v3[i].getValue());
            }
        }else{
            //quads
            ib = DefaultBufferFactory.INSTANCE.createInt(v1.length*3*2).cursorInt();
            for(int i=0;i<v1.length;i++){
                ib.write((Integer)v1[i].getValue());
                ib.write((Integer)v2[i].getValue());
                ib.write((Integer)v3[i].getValue());
                
                ib.write((Integer)v3[i].getValue());
                ib.write((Integer)v4[i].getValue());
                ib.write((Integer)v1[i].getValue());
            }
        }
        
        return ib.getBuffer();
    }
    
    /**
     * Read loops and convert them in triangles.
     * 
     * @param vertices
     * @param normals
     * @param jindex
     * @param jweight
     * @return Vertex, Normal, Index and UV Buffer
     * @throws StoreException 
     */
    public Buffer[] readLoops(Buffer vertices, Buffer normals, Buffer jindex, Buffer jweight, Sequence morphs) throws StoreException{
        
        final BlendFileBlock loopBlock = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.MLOOP).getValue();
        final BlendFileBlock polyBlock = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.MPOLY).getValue();
        final BlendFileBlock uvBlock   = (BlendFileBlock)parsedData.getChild(Blend_2_72.MESH.MLOOPUV).getValue();
        
        //TODO there might be multiple materials, different for each loop
        final TypedNode[] vertexNodes    = loopBlock.parsedData.getChildren(Blend_2_72.LOOP.VERTEX);
        final TypedNode[] loopStartNodes = polyBlock.parsedData.getChildren(Blend_2_72.MPOLY.LOOPSTART);
        final TypedNode[] loopNbNodes    = polyBlock.parsedData.getChildren(Blend_2_72.MPOLY.TOTALLOOP);
        final TypedNode[] uvNodes        = (uvBlock==null) ? null : uvBlock.parsedData.getChildren(Blend_2_72.MLOOPUV.UV);
                
        final float[] vtemp = new float[3];
        final float[] ntemp = new float[3];
        
        if(uvBlock==null){
            //we can reuse vertex and normal buffers
            final IntSequence indexSeq = new IntSequence();
        
            for(int i=0;i<loopStartNodes.length;i++){
                final int start = (Integer)loopStartNodes[i].getValue();
                final int nb = (Integer)loopNbNodes[i].getValue();
                final int[] loopVertices = new int[nb];

                for(int k=0;k<loopVertices.length;k++){
                    loopVertices[k] = (Integer)vertexNodes[start+k].getValue();
                }

                if(loopVertices.length==3){
                    indexSeq.put(loopVertices);

                }else if(loopVertices.length==4){
                    indexSeq.put(loopVertices[0]);
                    indexSeq.put(loopVertices[1]);
                    indexSeq.put(loopVertices[2]);
                    indexSeq.put(loopVertices[0]);
                    indexSeq.put(loopVertices[2]);
                    indexSeq.put(loopVertices[3]);

                }else{
                    // N-gon
                    //calculate the best 2D projection plan using the normals
                    final Tuple[] contour = new Tuple[loopVertices.length];
                    final VBO vbo = new VBO(vertices, 3);
                    for(int k=0;k<loopVertices.length;k++){
                        contour[k] = new Vector(vbo.getTupleFloat(loopVertices[k], vtemp));
                    }
                    
                    final Plane plane = Plane.computeBestFittingPlan(contour);
                    Vector bestPlanNormal = plane.getNormal();

                    bestPlanNormal.localNormalize();
                    try {
                        final int[][] idx = MeshUtilities.triangulate(contour,bestPlanNormal);
                        for(int k=0;k<idx.length;k++){
                            indexSeq.put(loopVertices[idx[k][0]]);
                            indexSeq.put(loopVertices[idx[k][1]]);
                            indexSeq.put(loopVertices[idx[k][2]]);
                        }
                    } catch (OperationException ex) {
                        throw new StoreException(ex);
                    }
                }
            }
            
            return new Buffer[]{
                vertices,
                normals,
                jindex,
                jweight,
                DefaultBufferFactory.wrap(indexSeq.toArrayInt()),
                null
            };
            
        }else{
            //vertices and normals may be reused with different uv
            //we will need to duplicate them
            final RebuildPair verticesPair = new RebuildPair(new VBO(vertices, 3),new FloatSequence());
            final RebuildPair normalsPair = new RebuildPair(new VBO(normals, 3),new FloatSequence());
            RebuildPair jidxPair = null;
            RebuildPair jwgPair = null;
            if(jindex!=null){
                final int jtsize = (int) (jindex.getPrimitiveCount() / (vertices.getPrimitiveCount()/3));
                jidxPair = new RebuildPair(new VBO(jindex, jtsize),new IntSequence());
                jwgPair = new RebuildPair(new VBO(jweight, jtsize),new FloatSequence());
            }
            
            final FloatSequence uvSeq = new FloatSequence();
            final IntSequence indexSeq = new IntSequence();
            
            
            final Sequence rebuilds = new ArraySequence();
            rebuilds.add(verticesPair);
            rebuilds.add(normalsPair);
            rebuilds.add(jidxPair);
            rebuilds.add(jwgPair);
            
            //morphs
            for(int i=0,n=morphs.getSize();i<n;i++){
                final MorphTarget mt = (MorphTarget) morphs.get(i);
                rebuilds.add(new RebuildPair(mt.getVertices(), new FloatSequence()));
            }
            
            //TODO try to reuse vertices, there are always duplicated here
            int idx = 0;
            for(int i=0;i<loopStartNodes.length;i++){
                final int start = (Integer)loopStartNodes[i].getValue();
                final int nb = (Integer)loopNbNodes[i].getValue();
                final int[] loopVertices = new int[nb];
                final float[][] uvs = new float[nb][2];

                for(int k=0;k<loopVertices.length;k++){
                    loopVertices[k] = (Integer)vertexNodes[start+k].getValue();
                    final float[] uvf = (float[])uvNodes[start+k].getValue();
                    uvs[k][0] = uvf[0];
                    uvs[k][1] = 1f-uvf[1];
                }

                if(loopVertices.length==3){
                    append(uvSeq, indexSeq, loopVertices, uvs, 0, 1, 2, idx, rebuilds);
                    idx+=3;

                }else if(loopVertices.length==4){
                    append(uvSeq, indexSeq, loopVertices, uvs, 0, 1, 2, idx, rebuilds);
                    idx+=3;
                    append(uvSeq, indexSeq, loopVertices, uvs, 0, 2, 3, idx, rebuilds);
                    idx+=3;
                    
                }else{
                    // N-gon
                    //calculate the best 2D projection plan using the normals
                    final Tuple[] contour = new Tuple[loopVertices.length];
                    for(int k=0;k<loopVertices.length;k++){
                        contour[k] = new Vector(verticesPair.vbo.getTupleFloat(loopVertices[k], vtemp));
                    }

                    final Plane plane = Plane.computeBestFittingPlan(contour);
                    Vector bestPlanNormal = plane.getNormal();
                    
                    bestPlanNormal.localNormalize();
                    try {
                        final int[][] triIdx = MeshUtilities.triangulate(contour,bestPlanNormal);
                        for(int k=0;k<triIdx.length;k++){
                            append(uvSeq, indexSeq, loopVertices, uvs, triIdx[k][0], triIdx[k][1], triIdx[k][2], idx, rebuilds);
                            idx+=3;
                        }
                    } catch (OperationException ex) {
                        throw new StoreException(ex);
                    }
                }
            }

            //morphs
            for(int i=0,n=morphs.getSize();i<n;i++){
                final MorphTarget mt = (MorphTarget) morphs.get(i);
                final RebuildPair rp = (RebuildPair) rebuilds.get(4+i);
                mt.getVertices().setBuffer(DefaultBufferFactory.wrap(rp.fseq.toArrayFloat()), mt.getVertices().getSampleCount());
            }
            
            return new Buffer[]{
                DefaultBufferFactory.wrap(verticesPair.fseq.toArrayFloat()),
                DefaultBufferFactory.wrap(normalsPair.fseq.toArrayFloat()),
                jindex!=null ? DefaultBufferFactory.wrap(jidxPair.iseq.toArrayInt()) : null,
                jindex!=null ? DefaultBufferFactory.wrap(jwgPair.fseq.toArrayFloat()) : null,
                DefaultBufferFactory.wrap(indexSeq.toArrayInt()),
                DefaultBufferFactory.wrap(uvSeq.toArrayFloat())
            };
        }
        
    }
    
    private static void append(final FloatSequence uvSeq, final IntSequence indexSeq, 
            int[] loopVertices,float[][] uvs, int i0, int i1, int i2, int idx, Sequence rebuilds){
        
        uvSeq.put(uvs[i0]);
        uvSeq.put(uvs[i1]);
        uvSeq.put(uvs[i2]);
        indexSeq.put(idx+0);
        indexSeq.put(idx+1);
        indexSeq.put(idx+2);
        
        for(int i=0,n=rebuilds.getSize();i<n;i++){
            RebuildPair rp = (RebuildPair) rebuilds.get(i);
            if(rp==null)continue;
            rp.put(loopVertices[i0]);
            rp.put(loopVertices[i1]);
            rp.put(loopVertices[i2]);
        }
    }
    
    private static class RebuildPair{
        private final VBO vbo;
        private final FloatSequence fseq;
        private final IntSequence iseq;
        
        public RebuildPair(VBO vbo, FloatSequence fseq) {
            this.vbo = vbo;
            this.fseq = fseq;
            this.iseq = null;
        }
        
        public RebuildPair(VBO vbo, IntSequence iseq) {
            this.vbo = vbo;
            this.fseq = null;
            this.iseq = iseq;
        }
        
        public void put(int idx){
            if(fseq!=null) fseq.put(vbo.getTupleFloat(idx, null));
            if(iseq!=null) iseq.put(vbo.getTupleInt(idx, null));
        }
        
    }
    
}
