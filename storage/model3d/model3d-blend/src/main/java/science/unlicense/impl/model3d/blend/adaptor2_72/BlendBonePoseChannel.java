
package science.unlicense.impl.model3d.blend.adaptor2_72;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.model3d.blend.model.BlendFileBlock;

/**
 *
 * @author Johann Sorel
 */
public class BlendBonePoseChannel extends BlendFileBlock{

    public BlendBonePoseChannel(BlendFileBlock block) {
        super(block);
    }
    
    public Chars getBoneName(){
        return (Chars) parsedData.getChild(Blend_2_72.BONE_POSE_CHANNEL.NAME).getValue();
    }

    public Sequence getConstraints(){
        return getRefList(Blend_2_72.BONE_POSE_CHANNEL.CONSTRAINTS);
    }
    
}
