
package science.unlicense.impl.model3d.off;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Object File Format.
 * Old 3D model format, only contain vertices and faces.
 * 
 * resource :
 * http://en.wikipedia.org/wiki/OFF_%28file_format%29
 * http://pgl10.chez.com/format_off.html
 * 
 * @author Johann Sorel
 */
public class OFFFormat extends AbstractModel3DFormat{

    public static final OFFFormat INSTANCE = new OFFFormat();

    private OFFFormat() {
        super(new Chars("OFF"),
              new Chars("OFF"),
              new Chars("OFF"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("off")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        final OFFStore store = new OFFStore(input);
        return store;
    }

}
