
package science.unlicense.impl.model3d.off;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class OFFConstants {
    
    public static final Chars SIGNATURE = new Chars(new byte[]{'O','F','F'});
    public static final Chars COMMENT = new Chars(new byte[]{'#'});
    
    private OFFConstants(){}
    
}