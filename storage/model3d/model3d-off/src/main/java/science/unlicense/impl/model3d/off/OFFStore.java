
package science.unlicense.impl.model3d.off;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import static science.unlicense.impl.model3d.off.OFFConstants.*;

/**
 * 
 * @author Johann Sorel
 */
public class OFFStore extends AbstractModel3DStore {

    /** file vertices : Vector 3 */
    public Vector[] vertexCoord;
    /** file faces */
    public OFFFace[] faces;
        
    public OFFStore(Object input) {
        super(OFFFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {

        if(vertexCoord==null){
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        
        final Collection col = new ArraySequence();
        col.add(rebuildMesh());
        return col;
    }

    private void read() throws IOException{
        final ByteInputStream stream = getSourceAsInputStream();
        final CharInputStream charStream = new CharInputStream(stream,CharEncodings.US_ASCII);

        int vinc = 0;
        int finc = 0;
        Chars cs = null;
        while( (cs=charStream.readLine()) != null){
            cs = cs.trim();
            if(cs.isEmpty() || cs.startsWith(COMMENT) || cs.startsWith(SIGNATURE)){
                continue;
            }
            
            if(vertexCoord==null){
                //this is the sizes line, contain number of vertice, number of face, number of edge
                Chars[] parts = cs.split(' ');
                int nbVertice = Int32.decode(parts[0]);
                int nbFace = Int32.decode(parts[1]);
                vertexCoord = new Vector[nbVertice];
                faces = new OFFFace[nbFace];
            }else if(vinc<vertexCoord.length){
                vertexCoord[vinc] = new Vector(3);
                for(int i=0;i<3;i++){
                    int off = cs.getFirstOccurence(' ');
                    if(off<0) off = cs.getCharLength();
                    Chars txt = cs.truncate(0, off);
                    vertexCoord[vinc].set(i, Float64.decode(txt));
                    cs = cs.truncate(off, cs.getCharLength()).trim();
                }
                vinc++;
            }else if(finc<faces.length){
                faces[finc] = new OFFFace();
                int off = cs.getFirstOccurence(' ');
                    if(off<0) off = cs.getCharLength();
                Chars txt = cs.truncate(0, off);
                faces[finc].vertexIdx = new int[Int32.decode(txt)];
                cs = cs.truncate(off, cs.getCharLength()).trim();
                
                for(int i=0;i<3;i++){
                    off = cs.getFirstOccurence(' ');
                    if(off<0) off = cs.getCharLength();
                    txt = cs.truncate(0, off);
                    faces[finc].vertexIdx[i] = Int32.decode(txt);
                    cs = cs.truncate(off, cs.getCharLength()).trim();
                }
                finc++;
            }
        }
    }
    
    private Mesh rebuildMesh(){
        final Mesh mesh = new Mesh();
                        
        //count number of triangles
        //some files contain quad or polygons in face definition
        int nbPoint=0;
        int nbIndices=0;
        for(int i=0,n=faces.length;i<n;i++){
            final OFFFace face = faces[i];
            nbPoint += face.vertexIdx.length;
            nbIndices += (face.vertexIdx.length-2)*3;
        }
                
        //prepare buffers
        final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(nbPoint*3).cursorFloat();
        final IntCursor indices = DefaultBufferFactory.INSTANCE.createInt(nbIndices).cursorInt();
        final VBO meshVertex = new VBO(vertices.getBuffer(),3);
        final IBO meshIndice = new IBO(indices.getBuffer(),3);

        int indice = 0;
        for(int i=0,n=faces.length;i<n;i++){
            final OFFFace face = faces[i];
            
            final int[] temp = new int[face.vertexIdx.length];
            for(int k=0;k<temp.length;k++){
                temp[k] = indice++;
            }
            
            for(int k=2,kn=face.vertexIdx.length;k<kn;k++){
                final boolean firstTriangle = k==2;
                final Tuple v1 = vertexCoord[face.vertexIdx[0  ]];
                final Tuple v2 = vertexCoord[face.vertexIdx[k-1]];
                final Tuple v3 = vertexCoord[face.vertexIdx[k  ]]; 
                if(firstTriangle){
                    vertices.write(v1.toArrayFloat());
                    vertices.write(v2.toArrayFloat());
                }   
                vertices.write(v3.toArrayFloat());

                indices.write(temp[0  ]);
                indices.write(temp[k-1]);
                indices.write(temp[k  ]);
            }
        }

        final Shell shell = new Shell();
        shell.setVertices(meshVertex);
        shell.setIndexes(meshIndice,IndexRange.TRIANGLES(0, meshIndice.getCapacity()));
        mesh.setShape(shell);
        ((Shell)mesh.getShape()).calculateNormals();
        mesh.getShape().calculateBBox();
        
        return mesh;
    }
    
}