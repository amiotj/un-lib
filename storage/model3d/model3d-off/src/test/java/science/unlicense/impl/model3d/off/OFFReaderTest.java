
package science.unlicense.impl.model3d.off;

import science.unlicense.impl.model3d.off.OFFStore;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class OFFReaderTest {

    @Test
    public void readTest() throws Exception{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/off/cube.off"));
        
        final OFFStore store = new OFFStore(path);
        
        final Collection elements = store.getElements();
        Assert.assertEquals(1, elements.getSize());
        
        final Object obj = elements.createIterator().next();
        Assert.assertTrue(obj instanceof Mesh);        
        final Mesh mesh = (Mesh) obj;
        
        final Shell shell = (Shell) mesh.getShape();
        final float[] vertices = (float[])shell.getVertices().getPrimitiveBuffer().toFloatArray();
        final int[] indices = shell.getIndexes().getPrimitiveBuffer().toIntArray();
        
        //TODO
        

    }

}