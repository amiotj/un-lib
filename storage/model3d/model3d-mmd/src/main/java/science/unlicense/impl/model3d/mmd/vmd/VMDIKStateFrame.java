

package science.unlicense.impl.model3d.mmd.vmd;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VMDIKStateFrame {
    
    public int frameNumber;
    public int visible;
    public VMDIKState[] ikStates;
    
    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        visible     = ds.readUByte();
        ikStates = new VMDIKState[ds.readInt()];
        for(int i=0;i<ikStates.length;i++){
            ikStates[i] = new VMDIKState();
            ikStates[i].name = ds.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
            ikStates[i].enable = ds.readUByte();
        }
    }
    
    public static class VMDIKState{
        public Chars name;
        public int enable;
    }
    
}
