package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.math.Maths;

/**
 * PMD IK is an inverse kinematic using CCD method.
 * 
 * @author Johann Sorel
 */
public class PMXIK {
    
    /** 
     * effector bone. 
     * The target is the bone with this ik.
     */
    public int effectorBone;
    /** Maximum number of loops for the CCD solver. */
    public int maxLoop;
    /** Max angle in each loop for CCD solver. */
    public float maxAngle;
    /** Joints in the kinematic chain. */
    public PMXIKLink[] links;
    
    /**
     * A joint and it's contraints in the ik chain.
     */
    public static class PMXIKLink{
        /** Bone index part of the chain. */
        public int boneIndex;
        /** Indicate if this bone has angle limits. */
        public boolean hasLimit;
        /** 
         * if limit is true, contains the min/max angle limit on x/y/z. 
         * in order : minX,minY,minZ,maxX,maxY,maxZ
         */
        public float[] limits;
    }
    
    public void setToDefault(){
        effectorBone = 0;
        maxLoop = 10;
        maxAngle = (float) Maths.TWO_PI;
        links = new PMXIKLink[0];
    }
    
    public void read(PMXInputStream ds) throws IOException, StoreException{
        effectorBone= ds.readBoneIndex();
        maxLoop     = ds.readInt();
        maxAngle    = ds.readFloat();
        links = new PMXIK.PMXIKLink[ds.readInt()];
        for(int i=0;i<links.length;i++){
            links[i] = new PMXIK.PMXIKLink();
            links[i].boneIndex = ds.readBoneIndex();
            links[i].hasLimit  = ds.readByte() == 1;
            if(links[i].hasLimit){
                links[i].limits = ds.readFloat(6);
            }
        }
    }
    
    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeBoneIndex(effectorBone);
        ds.writeInt(maxLoop);
        ds.writeFloat(maxAngle);
        ds.writeInt(links.length);
        for(int i=0;i<links.length;i++){
            ds.writeBoneIndex(links[i].boneIndex);
            ds.writeByte((byte)(links[i].hasLimit ? 1 : 0));
            if(links[i].hasLimit){
                ds.writeFloat(links[i].limits);
            }
        }
    }
    
}
