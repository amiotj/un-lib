
package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.model3d.mmd.MMDUtilities;

/**
 * A PMD Model.
 * 
 * @author Johann Sorel
 */
public class PMDModel {
    
    //file type header
    public Chars header;
    public float version;
    //model header
    public Chars name;
    public Chars comment;
    public Chars engName;
    public Chars engComment;
    //data parts
    public PMDVertex[] vertices;
    public int[] faceIndex;
    public PMDMaterial[] materials;
    public PMDBone[] bones;
    public PMDIK[] kinematics;
    public PMDMorph[] morphs;
    /** display informations, used in editor */
    public int[] morphsDisplay;
    public PMDBoneGroup[] boneGroups;
    public Chars[] toonTextureReplaces;
    public PMDRigidBody[] rigidBodies;
    public PMDPhysicConstraint[] physicConstraints;
    
    public void setToDefault(){
        header                = new Chars(PMDConstants.SIGNATURE);
        version               = 1.0f;
        name                  = Chars.EMPTY;
        comment               = Chars.EMPTY;
        engName               = Chars.EMPTY;
        engComment            = Chars.EMPTY;
        boneGroups            = new PMDBoneGroup[0];
        bones                 = new PMDBone[0];
        faceIndex             = new int[0];
        kinematics            = new PMDIK[0];
        morphs                = new PMDMorph[0];
        morphsDisplay         = new int[0];
        physicConstraints     = new PMDPhysicConstraint[0];
        rigidBodies           = new PMDRigidBody[0];
        toonTextureReplaces   = new Chars[10];
        vertices              = new PMDVertex[0];
        materials             = new PMDMaterial[0];
    }
    
    public void read(ByteInputStream inStream) throws IOException{
        final DataInputStream ds = new DataInputStream(inStream, NumberEncoding.LITTLE_ENDIAN);


        // FILE HEADER
        final Chars type = new Chars(
                new byte[]{
                    ds.readByte(),
                    ds.readByte(),
                    ds.readByte()});
        header = type;
        version = ds.readFloat();

        // MODEL HEADER
        name = ds.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
        comment = ds.readBlockZeroTerminatedChars(256, CharEncodings.SHIFT_JIS);

        // VERTEX INFORMATIONS
        vertices = new PMDVertex[(int) ds.readUInt()];
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = new PMDVertex();
            vertices[i].read(ds);
        }
        
        // FACE INFORMATION
        faceIndex = new int[(int) ds.readUInt()];
        ds.readUShort(faceIndex);
        
        // MATERIAL INFORMATION
        materials = new PMDMaterial[(int) ds.readUInt()];
        int materialArrayIndex = 0;
        for (int i = 0; i < materials.length; i++) {
            materials[i] = new PMDMaterial();
            materials[i].read(ds);
            //copy indexes affect by this material
            System.arraycopy(faceIndex, materialArrayIndex, materials[i].indices, 0, materials[i].indices.length);
            materialArrayIndex += materials[i].indices.length;
        }
        
        // BONES INFORMATIONS
        bones = new  PMDBone[ds.readUShort()];
        for (int i = 0; i < bones.length; i++) {
            bones[i] = new PMDBone();
            bones[i].read(ds);
        }
        
        // INVERSE KINEMATICS INFORMATIONS
        kinematics = new PMDIK[ds.readUShort()];
        for (int i = 0; i < kinematics.length; i++) {
            kinematics[i] = new PMDIK();
            kinematics[i].read(ds);
        }
        
        // MORPH INFORMATION
        morphs = new PMDMorph[ds.readUShort()];
        for (int i = 0; i < morphs.length; i++) {
            morphs[i] = new PMDMorph();
            morphs[i].read(ds);
        }
        
        // MORPH DISPLAY 
        morphsDisplay = new int[ds.readUByte()];
        ds.readUShort(morphsDisplay);
        
        // BONE GROUP NAMES
        boneGroups = new PMDBoneGroup[ds.readUByte()];
        //names
        for (int i = 0; i < boneGroups.length; i++) {
            boneGroups[i] = new PMDBoneGroup();
            boneGroups[i].name = ds.readBlockZeroTerminatedChars(50, CharEncodings.SHIFT_JIS);
        }
        //bone indexes
        final int nb = ds.readInt();
        for (int i = 0; i < nb; i++) {
            final int boneIndex = ds.readUShort() -1;
            final int groupId = ds.readUByte() -1;
            if(boneIndex>=0 && groupId>=0){
                boneGroups[groupId].boneIndexes = Arrays.resize(
                        boneGroups[groupId].boneIndexes, boneGroups[groupId].boneIndexes.length+1);
                boneGroups[groupId].boneIndexes[boneGroups[groupId].boneIndexes.length-1] = boneIndex;
            }
        }
        
        //after this point elements migh not exist in the model
        if(ds.read()==-1){
            toonTextureReplaces = new Chars[10];
            rigidBodies = new PMDRigidBody[0];
            physicConstraints = new PMDPhysicConstraint[0];
            return;
        }
        
        //some files do have some datas here but not with this definition,
        //TODO need to find infos
        try{
            //model metas
            engName = ds.readBlockZeroTerminatedChars(20, CharEncodings.US_ASCII);
            engComment = ds.readBlockZeroTerminatedChars(256, CharEncodings.US_ASCII);

            //bone names in english
            for (int i = 0; i < bones.length; i++) {
                final Chars engName = ds.readBlockZeroTerminatedChars(20, CharEncodings.US_ASCII);
                if(!engName.isEmpty())bones[i].englishName = engName;
            }

            //face names in english, wihtout base face
            for (int i = 1; i < morphs.length; i++) {
                final Chars engName = ds.readBlockZeroTerminatedChars(20, CharEncodings.US_ASCII);
                if(!engName.isEmpty())morphs[i].englishName = engName;
            }

            //bone group names in english
            for (int i = 0; i < boneGroups.length; i++) {
                final Chars engName = ds.readBlockZeroTerminatedChars(50, CharEncodings.US_ASCII);
                if(!engName.isEmpty())boneGroups[i].englishName = engName;
            }

            //toon textures, replacing those by default
            //TODO how do we use that ?
            toonTextureReplaces = new Chars[10];
            for(int i=0;i<toonTextureReplaces.length;i++){
                toonTextureReplaces[i] = ds.readBlockZeroTerminatedChars(100, CharEncodings.US_ASCII);
            }

            //physics parts
            rigidBodies = new PMDRigidBody[ds.readInt()];
            for(int i=0;i<rigidBodies.length;i++){
                rigidBodies[i] = new PMDRigidBody();
                rigidBodies[i].read(ds);
            }
            physicConstraints = new PMDPhysicConstraint[ds.readInt()];
            for(int i=0;i<physicConstraints.length;i++){
                physicConstraints[i] = new PMDPhysicConstraint();
                physicConstraints[i].read(ds);
            }
        }catch(Throwable ex){
            //we catch throwable because we may have an outof memory if an array size is too large
            //might happen if the datas are not organized as expected above.
            System.out.println("Failed to parse extended datas for model : "+name);
            toonTextureReplaces = new Chars[10];
            rigidBodies = new PMDRigidBody[0];
            physicConstraints = new PMDPhysicConstraint[0];
        }

    }
    
    public void write(ByteOutputStream out) throws IOException{
        final DataOutputStream ds = new DataOutputStream(out, NumberEncoding.LITTLE_ENDIAN);

        // FILE HEADER
        ds.write(PMDConstants.SIGNATURE);
        ds.writeFloat(version);

        // MODEL HEADER
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(name,CharEncodings.SHIFT_JIS,20), 20, CharEncodings.SHIFT_JIS);
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(comment,CharEncodings.SHIFT_JIS,256), 256, CharEncodings.SHIFT_JIS);

        // VERTEX INFORMATIONS
        ds.writeInt(vertices.length);
        for (int i = 0; i < vertices.length; i++) {
            vertices[i].write(ds);
        }
        
        // FACE INFORMATION
        ds.writeInt(faceIndex.length);
        ds.writeUShort(faceIndex);
        
        // MATERIAL INFORMATION
        ds.writeInt(materials.length);
        for (int i = 0; i < materials.length; i++) {
            materials[i].write(ds);
        }
        
        // BONES INFORMATIONS
        ds.writeUShort(bones.length);
        for (int i = 0; i < bones.length; i++) {
            bones[i].write(ds);
        }
        
        // INVERSE KINEMATICS INFORMATIONS
        ds.writeUShort(kinematics.length);
        for (int i = 0; i < kinematics.length; i++) {
            kinematics[i].write(ds);
        }
        
        // MORPH INFORMATION
        ds.writeUShort(morphs.length);
        for (int i = 0; i < morphs.length; i++) {
            morphs[i].write(ds);
        }
        
        // MORPH DISPLAY 
        ds.writeUByte(morphsDisplay.length);
        ds.writeUShort(morphsDisplay);
        
        // BONE GROUP NAMES
        ds.writeUByte(boneGroups.length);
        //names
        int nb = 0;
        for (int i = 0; i < boneGroups.length; i++) {
            ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(boneGroups[i].name,CharEncodings.SHIFT_JIS,50), 50, CharEncodings.SHIFT_JIS);
            nb += boneGroups[i].boneIndexes.length;
        }
        //bone indexes
        ds.writeInt(nb);
        for (int i = 0; i < boneGroups.length; i++) {
            PMDBoneGroup group = boneGroups[i];
            for(int k=0;k<group.boneIndexes.length;k++){
                ds.writeUShort(group.boneIndexes[k]+1);
                ds.writeUByte(i+1);
            }
        }
                
        //model metas
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(engName,CharEncodings.US_ASCII,20), 20, CharEncodings.US_ASCII);
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(engComment,CharEncodings.US_ASCII,256), 256, CharEncodings.US_ASCII);
        
        //bone names in english
        for (int i = 0; i < bones.length; i++) {
            ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(bones[i].englishName,CharEncodings.US_ASCII,20), 20, CharEncodings.US_ASCII);
        }
        
        //face names in english, wihtout base face
        for (int i = 1; i < morphs.length; i++) {
            ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(morphs[i].englishName,CharEncodings.US_ASCII,20), 20, CharEncodings.US_ASCII);
        }
        
        //bone group names in english
        for (int i = 0; i < boneGroups.length; i++) {
            ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(boneGroups[i].englishName,CharEncodings.US_ASCII,50), 50, CharEncodings.US_ASCII);
        }
        
        //toon textures, replacing those by default
        for(int i=0;i<toonTextureReplaces.length;i++){
            ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(toonTextureReplaces[i],CharEncodings.US_ASCII,100), 100, CharEncodings.US_ASCII);
        }
        
        //physics parts
        ds.writeInt(rigidBodies.length);
        for(int i=0;i<rigidBodies.length;i++){
            rigidBodies[i].write(ds);
        }
        ds.writeInt(physicConstraints.length);
        for(int i=0;i<physicConstraints.length;i++){
            physicConstraints[i].write(ds);
        }
    }
    
}
