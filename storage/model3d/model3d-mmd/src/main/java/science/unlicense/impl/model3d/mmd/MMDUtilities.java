package science.unlicense.impl.model3d.mmd;

import java.util.ResourceBundle;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.coordsys.Axis;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.Direction;
import science.unlicense.api.unit.Units;

/**
 * Various methods useful for MMD parsing.
 * @author Johann Sorel
 */
public class MMDUtilities {

    public static final CoordinateSystem COORDSYS = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.DECIMETER),
                new Axis(Direction.UP, Units.DECIMETER),
                new Axis(Direction.FORWARD, Units.DECIMETER)
            }
    );
    
    private static final ResourceBundle JP_TO_EN = ResourceBundle.getBundle("un/storage/model3d/mmd/translation");

    private MMDUtilities() {}

    public static Chars truncate(Chars text, CharEncoding encoding, int byteLength){
        if(text==null) return text;
        text = text.recode(encoding);
        //TODO we can write this more efficient
        while(text.toBytes().length>byteLength){
            text = text.truncate(0, text.getCharLength()-1);
        }
        return text;
    }
    
    public static Chars toEnglish(Chars jpString){
        return new Chars(MMDUtilities.toEnglish(jpString.toString()));
    }
    
    private static String toEnglish(String jpString){

        if(JP_TO_EN.containsKey(jpString)){
            return JP_TO_EN.getString(jpString);
        }

        for(String key : JP_TO_EN.keySet()){
            jpString = jpString.replaceAll(key, " "+JP_TO_EN.getString(key));
        }
        return jpString.trim();
    }

}
