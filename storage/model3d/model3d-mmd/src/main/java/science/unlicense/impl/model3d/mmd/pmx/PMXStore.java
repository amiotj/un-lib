package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.DefaultLChars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.country.Country;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.path.Path;
import science.unlicense.api.physic.body.BodyGroup;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.constraint.AngleLimitConstraint;
import science.unlicense.api.physic.constraint.Constraint;
import science.unlicense.api.physic.constraint.CopyRotationConstraint;
import science.unlicense.api.physic.constraint.CopyTransformConstraint;
import science.unlicense.api.physic.constraint.CopyTranslationConstraint;
import science.unlicense.api.physic.force.Force;
import science.unlicense.api.physic.force.Spring;
import science.unlicense.api.physic.skeleton.IKSolver;
import science.unlicense.api.physic.skeleton.IKSolverCCD;
import science.unlicense.api.physic.skeleton.InverseKinematic;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.SphericalMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.engine.opengl.renderer.SilhouetteRendererBackFaceMethod;
import science.unlicense.api.scenegraph.SceneUtils;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.model3d.mmd.MMDUtilities;

/**
 * @author Johann Sorel
 */
public class PMXStore extends AbstractModel3DStore{

    private Path basePath;
    private PMXModel model;

    //cache loaded images
    private final Dictionary imageCache = new HashDictionary();

    public PMXStore(Object input) {
        super(PMXFormat.INSTANCE,input);
        basePath = ((Path)getInput()).getParent();
    }

    public Collection getElements() throws StoreException {
        if(model== null){
            model = new PMXModel();
            try {
                model.read(getSourceAsInputStream());
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        
        final MultipartMesh node = new MultipartMesh();
        node.setLocalCoordinateSystem(MMDUtilities.COORDSYS);

        //rebuild skeleton
        final Skeleton skeleton = new Skeleton(MMDUtilities.COORDSYS);
        final Joint[] joints = new Joint[model.bones.length];
        for(int i=0;i<model.bones.length;i++){
            final PMXBone bone = model.bones[i];
            final Joint joint = new Joint(3);
            joint.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
            
            final Dictionary trs = new HashDictionary();
            trs.add(Country.JPN.asLanguage(), bone.name);
            trs.add(Country.GBR.asLanguage(), bone.englishName);
            final LChars name = new DefaultLChars(trs, Country.JPN.asLanguage());
            joint.setName(name);
            joint.getNodeTransform().getTranslation().set(bone.position);
            joint.getNodeTransform().notifyChanged();
            joint.setId(i);
            joints[i] = joint;
        }

        //rebuild node constraints
        for(int i=0;i<model.bones.length;i++){
            final PMXBone bone = model.bones[i];
            final Joint joint = (Joint) skeleton.getJoint(i);

            if( (bone.flag & PMXConstants.FLAG_IS_EXTERNAL_ROTATION)!=0 ){
                final Joint toCopy = (Joint) skeleton.getJoint(bone.effectIndex);
                if(toCopy!=null){
                    //some files have invalide indexes, ensure we don't have an index error
                    final CopyRotationConstraint cst = new CopyRotationConstraint(joint, toCopy, bone.effectFactor);
                    joint.getConstraints().add(cst);
                }
            }
            if( (bone.flag & PMXConstants.FLAG_IS_EXTERNAL_TRANSLATION)!=0 ){
                final Joint toCopy = (Joint) skeleton.getJoint(bone.effectIndex);
                if(toCopy!=null){
                    final CopyTranslationConstraint cst = new CopyTranslationConstraint(joint, toCopy, bone.effectFactor);
                    joint.getConstraints().add(cst);
                }
            }
//
//            if((bone.flag & PMXConstants.FLAG_HAS_FIXED_AXIS)!=0){
//                final FixedAxisConstraint cst = new FixedAxisConstraint(joint, bone.fixedAxis);
//                joint.getConstraints().add(cst);
//            }
//
//            if((bone.flag & PMXConstants.FLAG_HAS_LOCAL_COORDINATE)!=0){
//                //TODO need to find a model with such thing to rebuild it correctly
//                //System.out.println("todo");
//            }
//
            if((bone.flag & PMXConstants.FLAG_IS_EXTERNAL_PARENT_DEFORM)!=0){
                final Joint toCopy = (Joint) skeleton.getJoint(bone.externalKey);
                if(toCopy!=null){
                    final CopyTransformConstraint cst = new CopyTransformConstraint(joint, toCopy, 1f);
                    joint.getConstraints().add(cst);
                }
            }

        }

        //rebuild hierarchy
        for(int i=0;i<model.bones.length;i++){
            if(model.bones[i].parentIndex < 255){
                final Joint child = (Joint) joints[i];
                final Joint parent = (Joint) joints[model.bones[i].parentIndex];
                parent.getChildren().add(child);
            }else{
                //root joint
                skeleton.getChildren().add(joints[i]);
            }
        }
        node.setSkeleton(skeleton);

        //rebuild inverse kinematic
        for(int i=0;i<model.bones.length;i++){
            final PMXBone bone = model.bones[i];
            if(bone.ik != null){
                final Joint effector = (Joint) skeleton.getJoint(bone.ik.effectorBone);
                final Joint target = (Joint) skeleton.getJoint(i);
                final Joint[] chain = new Joint[bone.ik.links.length];
                for(int k=0;k<chain.length;k++){
                    PMXIK.PMXIKLink link = bone.ik.links[k];
                    chain[k] = (Joint) skeleton.getJoint(link.boneIndex);
                    //PMXIK contains bone angle limitations and a float array minX,minY,minZ,maxX,maxY,maxZ
                    if(link.hasLimit){
                        final BBox limits = new BBox(3);
                        limits.setRange(0, link.limits[0], link.limits[3]);
                        limits.setRange(1, link.limits[1], link.limits[4]);
                        limits.setRange(2, link.limits[2], link.limits[5]);
                        final Constraint cst = new AngleLimitConstraint(chain[k], limits);
                        chain[k].getConstraints().add(cst);
                    }
                }
                final IKSolver solver = new IKSolverCCD(bone.ik.maxLoop, bone.ik.maxAngle, 3);
                final InverseKinematic ik = new InverseKinematic(target, effector, chain, solver);
                skeleton.getIks().add(ik);
           }
        }

        //invert skeleton and physics world transforms
        //build bind and invert bind pose.
        //we do it before reading part because silhouette will requiere bone positions
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        for(int i=0;i<model.materials.length;i++){
            node.getChildren().add(buildPart(model.materials[i],skeleton));
        }

        //rebuild rigid bodies
        final RigidBody[] bodies = new RigidBody[model.rigidBodies.length];
        if(model.rigidBodies.length>0){
            //create all body groups
            final Dictionary groups = new HashDictionary();
            for(int i=0;i<32;i++){
                groups.add(i, new BodyGroup(new Chars(""+i)));
            }

            for(int i=0;i<model.rigidBodies.length;i++){
                final PMXRigidBody body = model.rigidBodies[i];

                //rigid body shape
                final Geometry shape;
                if(body.shapeType == PMXConstants.SHAPE_SPHERE){
                    shape = new Sphere(body.shapeSize[0]);
                }else if(body.shapeType == PMXConstants.SHAPE_CAPSULE){
                    shape = new Capsule(body.shapeSize[1],body.shapeSize[0]);
                }else if(body.shapeType == PMXConstants.SHAPE_BOX){
                    final double[] lower = new Vector(body.shapeSize).scale(-1).toArrayDouble();
                    final double[] upper = Arrays.reformatDouble(body.shapeSize);
                    shape = new BBox(lower,upper);

                }else{
                    throw new StoreException("unknowed body type : "+ body.shapeType);
                }
                final RigidBody physicbody = new RigidBody(shape, body.mass);
                physicbody.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
                physicbody.setUpdateMode(RigidBody.UPDATE_PARENT);   
                physicbody.setFixed(body.mode==0);
                
                bodies[i] = physicbody;

                //place the physic object
                physicbody.getNodeTransform().getTranslation().set(body.shapePosition);
                final Matrix m;
                if(Vectors.isFinite(body.shapeRotation)){//some files contains NaN values
                    m = Matrix3x3.createFromAngles(body.shapeRotation[0],body.shapeRotation[1],body.shapeRotation[2]);
                    physicbody.getNodeTransform().getRotation().set(m);
                }
                
                physicbody.getNodeTransform().notifyChanged();

                //set material informations
                physicbody.getMaterial().setDensity(1);
                physicbody.getMaterial().setRestitution(body.restitution);
                physicbody.getMaterial().setLinearAttenuation(body.linearAttenuation);
                physicbody.getMaterial().setAngularAttenuation(body.angularAttenuation);
                physicbody.getMaterial().setDynamicFriction(body.friction);
                physicbody.getMaterial().setStaticFriction(body.friction);
                //rebuild groups
                if(body.collisionGroup!=0){
                    final BodyGroup group = (BodyGroup) groups.getValue(body.collisionGroup);
                    physicbody.setGroup(group);
                    //rebuild no collision groups
                    for(int k=0;k<32;k++){
                        final boolean nocollide = ((1<<k) & body.noCollisionGroup) == 0;
                        if(nocollide){
                            final BodyGroup no = (BodyGroup) groups.getValue(k);
                            if(no==null){
                                throw new NullPointerException("group should not be null : "+k);
                            }
                            physicbody.addNoCollisionGroup(no);
                        }
                    }
                }

                //attach the related joint
                final Joint joint = skeleton.getJoint(body.boneIndex);
                if(joint != null){
                    joint.getChildren().add(physicbody);
                    //the physic body is in world space, we must convert it to joint space
                    SceneUtils.reverseWorldToNodeTrs(physicbody,false);
                }else{
                    System.out.println("Physic body is not attached to any bone : "+body);
                }
            }
        }
        
        //rebuild physic constraints
        for(int i=0;i<model.constraints.length;i++){
            final PMXPhysicConstraint cst = model.constraints[i];
            if(cst.rigidbodyIndexA>=bodies.length || cst.rigidbodyIndexB>=bodies.length){
                System.out.println("Physic constraints using body index "+cst.rigidbodyIndexA+":"+cst.rigidbodyIndexB+" but there are only "+bodies.length+" rigid bodies.");
                continue;
            }
            
            final RigidBody rbA = bodies[cst.rigidbodyIndexA];
            final RigidBody rbB = bodies[cst.rigidbodyIndexB];
            final Tuple centerA = rbA.getWorldPosition(null);
            final Tuple centerB = rbB.getWorldPosition(null);
            final Force force = new Spring(rbA, rbB, 5, 0.5, Distance.distance(centerA, centerB));
            
            node.getConstraints().add(force);
        }
               
        final Collection col = new ArraySequence();
        col.add(node);
        return col;
    }

    private Mesh buildPart(final PMXMaterial material, Skeleton skeleton) {
        final Mesh mesh = new Mesh();
        mesh.setLocalCoordinateSystem(MMDUtilities.COORDSYS);

        if((material.flag & PMXConstants.MATERIAL_BOTHFACE)==1){
            ((MeshRenderer)mesh.getRenderers().get(0)).getState().setCulling(-1);
        }else{
            ((MeshRenderer)mesh.getRenderers().get(0)).getState().setCulling(GLC.CULLING.BACK);
        }
        
        //find max number of joint per vertex
        int maxJoint = 0;
        for(int i=0;i<material.indices.length;i++) {
            final BoneDeform deform = model.vertices[material.indices[i]].deform;
            maxJoint = Maths.max(maxJoint, deform.getNbJoint());
        }
        
        final int nbVertice = material.indices.length*3;
        final int nbJoint = material.indices.length*maxJoint;
        final FloatCursor vertexCursor = DefaultBufferFactory.INSTANCE.createFloat(nbVertice).cursorFloat();
        final FloatCursor normalCursor = DefaultBufferFactory.INSTANCE.createFloat(nbVertice).cursorFloat();
        final IntCursor jointCursor  = DefaultBufferFactory.INSTANCE.createInt(nbJoint).cursorInt();
        final FloatCursor weightCursor = DefaultBufferFactory.INSTANCE.createFloat(nbJoint).cursorFloat();
        final FloatCursor deformCursor = DefaultBufferFactory.INSTANCE.createFloat(nbJoint*4).cursorFloat();
        final IntCursor indexCursor  = DefaultBufferFactory.INSTANCE.createInt(material.indices.length).cursorInt();

        for(int i=0;i<material.indices.length;i++) {
            final PMXVertex vertex = model.vertices[material.indices[i]];
            vertexCursor.write(vertex.position);
            normalCursor.write(vertex.normal);
            indexCursor.write(i);

            final BoneDeform deform = vertex.deform;
            if(deform instanceof BoneSphericalDeform){
                final BoneSphericalDeform def = (BoneSphericalDeform) deform;
                jointCursor.write(def.indexes);
                weightCursor.write(def.weights);
                deformCursor.write(1.0f).write(def.sdef_c);
            }else if(deform instanceof BoneWeightDeform){
                final BoneWeightDeform def = (BoneWeightDeform) deform;
                jointCursor.write(def.indexes);
                weightCursor.write(def.weights);
                deformCursor.write(0).write(0).write(0).write(0);
            }else{
                throw new InvalidArgumentException("Should not happen");
            }
            //fill remaining weigths with zeros
            for(int w=deform.getNbJoint();w<maxJoint;w++){
                jointCursor.write(0);
                weightCursor.write(0f);
            }
        }

        final SkinShell shell = new SkinShell();
        shell.setMaxWeightPerVertex(maxJoint);
        shell.setSkeleton(skeleton);
        shell.setVertices(new VBO(vertexCursor.getBuffer(),3));
        shell.setNormals(new VBO(normalCursor.getBuffer(),3));
        shell.setIndexes(new IBO(indexCursor.getBuffer(),3),IndexRange.TRIANGLES(0, (int) indexCursor.getBuffer().getPrimitiveCount()));
        shell.setJointIndexes(new VBO(jointCursor.getBuffer(),maxJoint));
        shell.setWeights(new VBO(weightCursor.getBuffer(),maxJoint));
        shell.setDeformVBO(new VBO(deformCursor.getBuffer(),4));
        mesh.setShape(shell);

        //set default colors
        mesh.getMaterial().setAmbient(new Color(material.ambiant_RGB));
        mesh.getMaterial().setDiffuse(new Color(material.diffuse_RGB));
        mesh.getMaterial().setSpecular(new Color(material.specular_RGB));
        
        if(!material.englishName.isEmpty()){
            mesh.setName(material.englishName);
        }else if(!material.name.isEmpty()){
            mesh.setName(material.name);
        }else{
            mesh.setName(Chars.EMPTY);
        }
        
        //start clean
        mesh.getMaterial().getLayers().removeAll();      
        
        try {
            textured:
            if (material.textureIndex >= 0 && material.textureIndex<255) {
                final Chars textureName = model.textures[material.textureIndex]; 
                Chars baseName = textureName;

                mesh.setName(mesh.getName().concat(' ').concat(baseName));
                
                final Sequence imageFiles = splitTextures(basePath, baseName);
                if(imageFiles.isEmpty()){
                    getLogger().info(new Chars("No image files found for : "+textureName));
                    break textured;
                }
                final Sequence images = new ArraySequence();
                for(int i=0,n=imageFiles.getSize();i<n;i++){
                    Path p = (Path) imageFiles.get(i);
                    Texture2D texture = (Texture2D) imageCache.getValue(p);
                    if(texture==null){
                        //load image
                        Image img = loadImage(p);
                        texture = new Texture2D(img);
                        imageCache.add(p, texture);
                    }
                    images.add(texture);
                }
                if(images.isEmpty()) throw new IOException("No image files could be read.");
                final Layer[] materialLayers = PMXStore.buildTexture(imageFiles,images);

                final FloatCursor uvBuffer = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*2).cursorFloat();
                for(int i=0;i<material.indices.length;i++) {
                    final int m = material.indices[i];
                    uvBuffer.write(model.vertices[m].uv);
                }
                shell.setUVs(new VBO(uvBuffer.getBuffer(), 2));
                for(int i=0;i<materialLayers.length;i++){
                    mesh.getMaterial().getLayers().add(materialLayers[i]);
                }
            }
        } catch (IOException ex) {
            getLogger().warning(ex);
        }

        //rebuild morph frames
        rebuildMorphFrames(mesh, material, vertexCursor.getBuffer());
        
        if(material.edgeSize>0 && (material.flag & PMXConstants.MATERIAL_EDGE) !=0){
            //models are in decimeter units, edges size seems to be in millimeter : TODO need to confirm this
            //so we divide by 100 on have it in decimeter
            mesh.getRenderers().add(new SilhouetteRendererBackFaceMethod(mesh, 
                    new Color(material.edgeColor_RGBA), material.edgeSize/100));
        }
        
        mesh.getShape().calculateBBox();
        return mesh;
    }

    private void rebuildMorphFrames(Mesh mesh, PMXMaterial material, Buffer vertexBuffer){
        if(model.morphs.length==0) return;
        
        //cache vertex indexes from material for faster morph reconstruction
        int minIdx = Integer.MAX_VALUE;
        int maxIdx = Integer.MIN_VALUE;
        for(int i=0,idx,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            if(idx<minIdx) minIdx = idx;
            if(idx>maxIdx) maxIdx = idx;
        }
        final int[][] dico = new int[maxIdx-minIdx+1][0];
        for(int i=0,idx,did,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            did = idx-minIdx;
            dico[did] = Arrays.insert(dico[did], dico[did].length, i);
        }
        
        
        final MorphSet ms = new MorphSet();
        for(int k=0;k<model.morphs.length;k++){
            final PMXMorph morph = model.morphs[k];
            
            final FloatCursor copy = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*3).cursorFloat();
            copy.setPosition(0);
            
            //TODO only support vertex morph for now
            boolean modified = false;
            for(PMXMorph.PMXMorphElement ele : morph.elements){
                if(ele instanceof PMXMorph.PMXMorphVertex){
                    final PMXMorph.PMXMorphVertex vm = (PMXMorph.PMXMorphVertex) ele;
                    if(vm.index<minIdx || vm.index>maxIdx) continue;
                    final int[] ref = dico[vm.index-minIdx];
                    if(ref.length>0){
                        modified = true;
                        //replace coords
                        for(int i=0;i<ref.length;i++) {
                            int ri = ref[i]*3;
                            copy.setPosition(ri);
                            copy.write(vm.position);
                        }
                    }
                }
            }
            if(modified){
                final MorphTarget mf = new MorphTarget();
                mf.setFactor(0);
                mf.setName(morph.name);
                mf.setVertices(new VBO(copy.getBuffer(), 3));
                ms.getMorphs().add(mf);
            }
        }
        ((Shell)mesh.getShape()).setMorphs(ms);
    }
    
    /**
     * Writing will search and write only the first MultiPartMesh found in the collection.
     * 
     * @param elements
     * @throws IOException 
     */
    public void writeElements(Collection elements) throws StoreException {
        MultipartMesh mpm = null;
        for(Iterator ite=elements.createIterator();ite.hasNext();){
            final Object obj = ite.next();
            if(obj instanceof MultipartMesh){
                mpm = (MultipartMesh) obj;
                break;
            }
        }
        
        if(mpm!=null){
            try{
                final PMXModel model = toModel(mpm);
                final ByteOutputStream out = getSourceAsOutputStream();
                model.write(out);
                out.close();
            }catch(IOException ex){
                throw new StoreException(ex);
            }
        }
    }
    
    
    private PMXModel toModel(MultipartMesh mpm) throws IOException{
        final PMXModel model = new PMXModel();
        model.setToDefault();
        
        //counters for dynamic names
        int texturecounter = 0;
        
        //transform the skeleton and rigid bodies
        final Skeleton skeleton = mpm.getSkeleton();
        if(skeleton!=null){
            final Dictionary buildDico = new HashDictionary();
            final Sequence roots = skeleton.getChildren();
            final Sequence result = new ArraySequence();
            for(int i=0;i<roots.getSize();i++){
                buildSkeleton((Joint)roots.get(i), buildDico, result);
            }
            
            model.bones = new PMXBone[result.getSize()];
            Collections.copy(result, model.bones, 0);
        }
        
        //transform the mesh
        final Iterator children = mpm.getChildren().createIterator();
        while (children.hasNext()) {
            final Object next = children.next();
            if(next instanceof Mesh){
                final Mesh mesh = (Mesh) next;
                final Shell shell = (Shell) mesh.getShape();
                
                final int vertexBaseOffset = model.vertices.length;
                
                //rebuild vertices
                final VBO vertices = shell.getVertices();
                final VBO normals = shell.getNormals();
                final VBO uvs = shell.getUVs();
                VBO jointIndexes = null;
                VBO jointWeights = null;
                int maxWeitgh = 0;
                int bestNbWeight = 0;
                if(shell instanceof SkinShell){
                    jointIndexes = ((SkinShell)shell).getJointIndexes();
                    jointWeights = ((SkinShell)shell).getWeights();
                    maxWeitgh = ((SkinShell)shell).getMaxWeightPerVertex();
                    bestNbWeight = maxWeitgh>=3 ? 4 : Maths.max(maxWeitgh,1);
                }
                
                final int vn = (int) (vertices.getPrimitiveBuffer().getPrimitiveCount()/3);
                model.vertices = (PMXVertex[])Arrays.resize(model.vertices, vertexBaseOffset+vn);
                
                for(int v=0;v<vn;v++){
                    final PMXVertex vertex = new PMXVertex();
                    vertex.setToDefault();
                    model.vertices[vertexBaseOffset+v] = vertex;
                    vertices.getTupleFloat(v, vertex.position);
                    if(normals!=null) normals.getTupleFloat(v, vertex.normal);
                    if(uvs!=null)uvs.getTupleFloat(v, vertex.uv);
                    if(jointIndexes!=null){
                        final float[] weights = jointWeights.getTupleFloat(v, null);
                        final int[] indexes = jointIndexes.getTupleInt(v, null);
                        final BoneWeightDeform deform = new BoneWeightDeform(bestNbWeight);
                        vertex.deform = deform;
                        Arrays.copy(weights, 0, Maths.min(weights.length, bestNbWeight),deform.weights, 0);
                        Arrays.copy(indexes, 0, Maths.min(indexes.length, bestNbWeight),deform.indexes, 0);
                    }
                }
                
                //rebuild material
                final Material material = mesh.getMaterial();
                final PMXMaterial pmxMaterial = new PMXMaterial();
                pmxMaterial.setToDefault();
                model.materials = (PMXMaterial[]) Arrays.insert(model.materials, model.materials.length, pmxMaterial);
                
                //texture informations
                for(Iterator ite=material.getLayers().createIterator();ite.hasNext();){
                    final Layer layer = (Layer) ite.next();
                    final int type = layer.getType();
                    if(type==Layer.TYPE_DIFFUSE){
                        final Mapping mapping = layer.getMapping();
                        if(mapping instanceof PlainColorMapping){
                            final PlainColorMapping cm = (PlainColorMapping) mapping;
                            pmxMaterial.diffuse_RGB = cm.getColor().toRGBA();
                        }else if(mapping instanceof UVMapping){
                            
                            final int texIndex = model.textures.length;
                            final Chars texName = new Chars("texture"+(texturecounter++)+".bmp");
                            model.textures = (Chars[]) Arrays.insert(model.textures,texIndex,texName);
                            
                            final UVMapping uvm = (UVMapping) mapping;
                            final Image texture = uvm.getTexture().getImage();
                            pmxMaterial.textureIndex = texIndex ;
                            //save texture aside pmd file
                            final Path texturePath = basePath.resolve(texName);
                            Images.write(texture, new Chars("bmp"), texturePath);
                        }
                    }
                }
                
                //face indices
                final int[] indexes = shell.getIndexes().getPrimitiveBuffer().toIntArray();
                pmxMaterial.indices = new int[indexes.length];
                for(int t=0;t<indexes.length;t++){
                    pmxMaterial.indices[t] = vertexBaseOffset + indexes[t];
                }
                model.faceIndices = Arrays.concatenate(new int[][]{model.faceIndices,pmxMaterial.indices});
            }
        }
        
        return model;
    }
    
    private void buildSkeleton(Joint joint, Dictionary all, Sequence result){
        final PMXBone bone = new PMXBone();
        bone.setToDefault();
        bone.name = joint.getName().toChars();
        bone.englishName = bone.name;
        bone.ik = null;
        
        final Affine matrix = joint.getNodeToRootSpace();
        bone.position = Arrays.reformatToFloat(matrix.transform(new double[3],null));
        
        final Integer parentIndex = (Integer) all.getValue(joint.getParent());
        if(parentIndex!=null){
            bone.parentIndex = parentIndex.shortValue();
        }
        
        all.add(result.getSize(), bone);
        result.add(bone);
        
        //loop on children
        final Iterator ite = joint.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if(next instanceof Joint){
                buildSkeleton((Joint)next, all, result);
            }else if(next instanceof RigidBody){
                //TODO
            }
        }
        
    }
    
    public static Sequence splitTextures(Path parentPath, Chars baseName){
        final Sequence valids = new ArraySequence();

        //pmd/pmx paths are case insensitive
        Chars str = baseName.replaceAll('\\', '/');
        // slash in japanese encoding, normaly we should not have those, but
        // some files mix encodings, so it may happen
        str = str.replaceAll('¥', '/'); 
        Chars[] fileparts = str.split('\\');
        loop:
        for(Chars filepart : fileparts){
            //remove relative path
            if(filepart.startsWith(new Chars("./"))) filepart = filepart.truncate(2,-1);
            if(filepart.startsWith('/')) filepart = filepart.truncate(1,-1);
            Path imageFile = parentPath;
            final Chars[] pathParts = filepart.split('/');
            nextPart:
            for(Chars part : pathParts){
                Iterator ite = imageFile.getChildren().createIterator();
                while(ite.hasNext()){
                    Path p = (Path) ite.next();
                    if(p.getName().equals(part,true,true)){
                        imageFile = p;
                        continue nextPart;
                    }
                }
                //not found
                imageFile = null;
                continue loop;
            }

            valids.add(imageFile);
        }
        return valids;
    }

    public static Image loadImage(Path path) throws IOException{
        return Images.read(path);
    }

    public static Layer[] buildTexture(Sequence paths, Sequence textures){
        final Layer[] layers = new Layer[paths.getSize()];
        for(int i=0;i<layers.length;i++){
            final Texture2D texture = (Texture2D) textures.get(i);
            final Path path = (Path) paths.get(i);
            layers[i] = new Layer();
            layers[i].setType(Layer.TYPE_DIFFUSE);
            
            final Chars name = path.getName().toLowerCase();
            
            if(name.endsWith(new Chars(".spa"))){
                //additive sphere map
                layers[i].setMapping(new SphericalMapping(texture));
                layers[i].setMethod(Layer.METHOD_ADDITIVE);
            }else if(name.endsWith(new Chars(".sph"))){
                //multiply sphere map
                layers[i].setMapping(new SphericalMapping(texture));
                layers[i].setMethod(Layer.METHOD_MULTIPLY);
            }else{
                //normal uv mapping, multiplication of the base mesh color
                layers[i].setMapping(new UVMapping(texture));
                layers[i].setMethod(Layer.METHOD_MULTIPLY);
            }
            
        }
        return layers;
    }

}