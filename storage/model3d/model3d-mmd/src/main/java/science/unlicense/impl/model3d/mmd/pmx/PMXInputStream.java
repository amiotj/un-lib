package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.store.StoreException;

/**
 * @author Johann Sorel
 */
public class PMXInputStream extends DataInputStream {

    private final int indexSizeVertex;
    private final int indexSizeTexture;
    private final int indexSizeMaterial;
    private final int indexSizeBone;
    private final int indexSizeMorph;
    private final int indexSizeRigidBody;

    public PMXInputStream(ByteInputStream in, int indexSizeVertex, 
            int indexSizeTexture, int indexSizeMaterial, 
            int indexSizeBone, int indexSizeMorph, int indexSizeRigidBody) {
        super(in,NumberEncoding.LITTLE_ENDIAN);
        this.indexSizeVertex = indexSizeVertex;
        this.indexSizeTexture = indexSizeTexture;
        this.indexSizeMaterial = indexSizeMaterial;
        this.indexSizeBone = indexSizeBone;
        this.indexSizeMorph = indexSizeMorph;
        this.indexSizeRigidBody = indexSizeRigidBody;
    }
    
    public Chars readText() throws IOException{
        final int size = readInt();
        final byte[] buffer = readFully(new byte[size]);
        return new Chars(buffer,CharEncodings.UTF_16LE).recode(CharEncodings.UTF_8);
    }
    
    public int readValue(int size) throws IOException,StoreException{
        if(size == 1)      return readUByte();
        else if(size == 2) return readUShort();
        else if(size == 4) return readInt();
        throw new StoreException("unsupported number size " + size);
    }
    
    public int readVertexIndex() throws IOException, StoreException{
        return readValue(indexSizeVertex);
    }
    
    public int readTextureIndex() throws IOException, StoreException{
        return readValue(indexSizeTexture);
    }
    
    public int readMaterialIndex() throws IOException, StoreException{
        return readValue(indexSizeMaterial);
    }
    
    public int readBoneIndex() throws IOException, StoreException{
        return readValue(indexSizeBone);
    }
    
    public int readMorphIndex() throws IOException, StoreException{
        return readValue(indexSizeMorph);
    }
    
    public int readRigidBodyIndex() throws IOException, StoreException{
        return readValue(indexSizeRigidBody);
    }

}
