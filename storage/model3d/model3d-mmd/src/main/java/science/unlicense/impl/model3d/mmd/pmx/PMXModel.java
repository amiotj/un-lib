

package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class PMXModel {
    
    public Chars type;
    public float version;
    public int flag_bytes;
    public int text_encoding;
    public int extended_uv;
    
    public Chars name;
    public Chars englishName;
    public Chars comment;
    public Chars englishComment;
    public PMXVertex[] vertices;
    public int[] faceIndices;
    public Chars[] textures;
    public PMXMaterial[] materials;
    public PMXBone[] bones;
    public PMXMorph[] morphs;
    public PMXDisplaySlot[] displaySlots;
    public PMXRigidBody[] rigidBodies;
    public PMXPhysicConstraint[] constraints;
    
    public void setToDefault(){
        type            = new Chars("PMX ");
        version         = 0;
        flag_bytes      = 8;
        text_encoding   = 0;
        extended_uv     = 0;
        name            = null;
        englishName     = null;
        comment         = null;
        englishComment  = null;
        vertices        = new PMXVertex[0];
        faceIndices     = new int[0];
        textures        = new Chars[0];
        materials       = new PMXMaterial[0];
        bones           = new PMXBone[0];
        morphs          = new PMXMorph[0];
        displaySlots    = new PMXDisplaySlot[0];
        rigidBodies     = new PMXRigidBody[0];
        constraints     = new PMXPhysicConstraint[0];
    }
    
    public void read(ByteInputStream inStream) throws IOException, StoreException{
        
        final DataInputStream ds = new DataInputStream(inStream, NumberEncoding.LITTLE_ENDIAN);

        // READ HEADER
        type                = ds.readBlockZeroTerminatedChars(4, CharEncodings.US_ASCII);
        version             = ds.readFloat();

        // READ FLAG : ??
        flag_bytes          = ds.readByte();
        if (flag_bytes!=8) {
            throw new StoreException("invalid flag length" + flag_bytes);
        }
        text_encoding = ds.readByte();
        extended_uv   = ds.readByte();

        // READ INDEX SIZES
        final int indexSizeVertex         = ds.readByte();
        final int indexSizeTexture        = ds.readByte();
        final int indexSizeMaterial       = ds.readByte();
        final int indexSizeBone           = ds.readByte();
        final int indexSizeMorph          = ds.readByte();
        final int indexSizeRigidBody      = ds.readByte();

        final PMXInputStream decoder = new PMXInputStream(inStream, indexSizeVertex, indexSizeTexture, 
                indexSizeMaterial, indexSizeBone, indexSizeMorph, indexSizeRigidBody);
        
        name                = decoder.readText();
        englishName         = decoder.readText();
        comment             = decoder.readText();
        englishComment      = decoder.readText();

        //read vertices
        vertices = new PMXVertex[ds.readInt()];
        for(int i=0; i<vertices.length; i++){
            vertices[i] = new PMXVertex();
            vertices[i].read(decoder);
        }
        //read faces
        faceIndices = new int[ds.readInt()];
        for (int i = 0; i < faceIndices.length; i++) {
            faceIndices[i] = decoder.readVertexIndex();
        }
        //read textures
        textures = new Chars[ds.readInt()];
        for (int i = 0; i < textures.length; i++) {
            textures[i] = decoder.readText();
        }
        //read materials
        materials = new PMXMaterial[ds.readInt()];
        int materialArrayIndex = 0;
        for (int i = 0; i < materials.length; i++) {
            materials[i] = new PMXMaterial();
            materials[i].read(decoder);
            //copy indexes affect by this material
            materials[i].indices = new int[materials[i].vertexCount];
            System.arraycopy(faceIndices, materialArrayIndex, materials[i].indices, 0, materials[i].vertexCount);
            materialArrayIndex += materials[i].vertexCount;
        }
        //read bones
        bones = new PMXBone[ds.readInt()];
        for (int i = 0; i < bones.length; i++) {
            bones[i] = new PMXBone();
            bones[i].read(decoder);
        }
        //read morphs
        morphs = new PMXMorph[ds.readInt()];
        for(int i=0;i<morphs.length;i++){
            morphs[i] = new PMXMorph();
            morphs[i].read(decoder);
        }
        //read interface slots
        displaySlots = new PMXDisplaySlot[ds.readInt()];
        for(int i=0;i<displaySlots.length;i++){
            displaySlots[i] = new PMXDisplaySlot();
            displaySlots[i].read(decoder);
        }
        //read rigid bodies
        rigidBodies = new PMXRigidBody[ds.readInt()];
        for(int i=0;i<rigidBodies.length;i++){
            rigidBodies[i] = new PMXRigidBody();
            rigidBodies[i].read(decoder);
        }
        //read joints
        constraints = new PMXPhysicConstraint[ds.readInt()];
        for(int i=0;i<constraints.length;i++){
            constraints[i] = new PMXPhysicConstraint();
            constraints[i].read(decoder);
        }
        
    }
    
    public void write(ByteOutputStream out) throws IOException, StoreException{
        
        
        DataOutputStream bds = new DataOutputStream(out, NumberEncoding.LITTLE_ENDIAN);

        // READ HEADER
        bds.write(PMXConstants.SIGNATURE);
        bds.write((byte)0);
        bds.writeFloat(version);

        // READ FLAG : ??
        bds.writeByte((byte)flag_bytes);
        bds.writeByte((byte)text_encoding);
        bds.writeByte((byte)extended_uv);

        // READ INDEX SIZES
        //TODO find the number of each element to compress pointer size
        final int indexSizeVertex         = 4;
        final int indexSizeTexture        = 4;
        final int indexSizeMaterial       = 4;
        final int indexSizeBone           = 4;
        final int indexSizeMorph          = 4;
        final int indexSizeRigidBody      = 4;

        PMXOutputStream ds = new PMXOutputStream(out, indexSizeVertex, indexSizeTexture, 
                indexSizeMaterial, indexSizeBone, indexSizeMorph, indexSizeRigidBody);
        ds.writeUByte(indexSizeVertex);
        ds.writeUByte(indexSizeTexture);
        ds.writeUByte(indexSizeMaterial);
        ds.writeUByte(indexSizeBone);
        ds.writeUByte(indexSizeMorph);
        ds.writeUByte(indexSizeRigidBody);
        
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeText(comment);
        ds.writeText(englishComment);

        //read vertices
        ds.writeInt(vertices.length);
        for(int i=0; i<vertices.length; i++){
            vertices[i].write(ds);
        }
        //read faces
        ds.writeInt(faceIndices.length);
        for (int i = 0; i < faceIndices.length; i++) {
            ds.writeVertexIndex(faceIndices[i]);
        }
        //read textures
        ds.writeInt(textures.length);
        for (int i = 0; i < textures.length; i++) {
            ds.writeText(textures[i]);
        }
        //read materials
        ds.writeInt(materials.length);
        for (int i = 0; i < materials.length; i++) {
            materials[i].write(ds);
        }
        //read bones
        ds.writeInt(bones.length);
        for (int i = 0; i < bones.length; i++) {
            bones[i].write(ds);
        }
        //read morphs
        ds.writeInt(morphs.length);
        for(int i=0;i<morphs.length;i++){
            morphs[i].write(ds);
        }
        //read interface slots
        ds.writeInt(displaySlots.length);
        for(int i=0;i<displaySlots.length;i++){
            displaySlots[i].write(ds);
        }
        //read rigid bodies
        ds.writeInt(rigidBodies.length);
        for(int i=0;i<rigidBodies.length;i++){
            rigidBodies[i].write(ds);
        }
        //read joints
        ds.writeInt(constraints.length);
        for(int i=0;i<constraints.length;i++){
            constraints[i].write(ds);
        }
        
    }
    
    /**
     * recalculate/validate values.
     * 
     * TODO
     */
    public void validate(){
        
        //check spherical bone deforms
        for(int i=0;i<vertices.length;i++){
            final PMXVertex vertex = vertices[i];
            final BoneDeform deform = vertex.deform;
            if(deform instanceof BoneSphericalDeform){
                final BoneSphericalDeform sdeform = (BoneSphericalDeform) deform;
                final PMXBone bone0 = bones[sdeform.indexes[0]];
                final PMXBone bone1 = bones[sdeform.indexes[1]];
                                
                //TODO : need to find infos on r0 and r1, what are those values exactly ???
                                
            }
        }
        
    }
    
}
