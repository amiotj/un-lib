package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.mmd.MMDUtilities;


/**
 * PMD face morph.
 * 
 * @author Johann Sorel
 */
public class PMDMorph {
    
    /**
     * The first morph is named base.
     * the indexes in VertexPosition are relative to the base morph.
     */
    public Chars name;
    /** translated value, if possible */
    public Chars englishName;
    /** morh type : 
     * - 0 Other
     * - 1 Eyebrow
     * - 2 Eye
     * - 3 Lip */
    public byte skinType;
    public VertexPosition[] movements;
    
    public static class VertexPosition{
        public int index;
        public float[] position_3f;
    }
    
    public void setToDefault(){
        name = null;
        englishName = null;
        skinType = 0;
        movements = new VertexPosition[0];
    }
    
    public void read(DataInputStream ds) throws IOException{
        name = ds.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
        englishName = MMDUtilities.toEnglish(name);
        movements = new PMDMorph.VertexPosition[ds.readInt()];
        skinType = ds.readByte();
        for (int v = 0; v < movements.length; v++) {
            movements[v] = new PMDMorph.VertexPosition();
            movements[v].index = ds.readInt();
            movements[v].position_3f = ds.readFloat(3);
        }
    }
    
    public void write(DataOutputStream ds) throws IOException{
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(name,CharEncodings.SHIFT_JIS,20),20,CharEncodings.SHIFT_JIS);
        ds.writeInt(movements.length);
        ds.writeByte(skinType);
        for(int i=0;i<movements.length;i++){
            ds.writeInt(movements[i].index);
            ds.writeFloat(movements[i].position_3f);
        }
    }
    
}
