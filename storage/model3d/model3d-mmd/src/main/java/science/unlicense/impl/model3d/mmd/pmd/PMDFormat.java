
package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * 
 * Some resource :
 * http://mikumikudance.wikia.com/wiki/PMD_file_format
 *
 * @author Johann Sorel
 */
public class PMDFormat extends AbstractModel3DFormat{

    public static final PMDFormat INSTANCE = new PMDFormat();

    private PMDFormat() {
        super(new Chars("mmd_pmd"),
              new Chars("MMD-PMD"),
              new Chars("MikuMikuDance Model file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("pmd")
              },
              new byte[][]{PMDConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new PMDStore(input);
    }

}
