package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * PMD Vertex, contains, position,normal,uv and skinning informations.
 * 
 * @author Johann Sorel
 */
public class PMDVertex {

    /** Vertex position */
    public float[] position;
    /** Vertex normal */
    public float[] normal;
    /** Vertex texture uv */
    public float[] uv;
    /** skining bones, size always 2 */
    public final int[] boneIndexes = new int[2];
    /** skining bones, size always 2 */
    public final float[] boneWeights = new float[2];
    public byte flag;
    
    public void setToDefault(){
        position    = new float[3];
        normal      = new float[3];
        uv          = new float[2];
        Arrays.fill(boneIndexes, 0);
        Arrays.fill(boneWeights, 0);
        flag        = 0;
    }
    
    public void read(DataInputStream ds) throws IOException {
        position       = ds.readFloat(3);
        normal         = ds.readFloat(3);
        uv             = ds.readFloat(2);
        boneIndexes[0] = ds.readUShort();
        boneIndexes[1] = ds.readUShort();
        //weight is a value [0-100] affected to bone_0
        //weight for bone_1 is calculated : 100-weight
        boneWeights[0] = ((float)ds.readByte())/100f;
        boneWeights[1] = 1f - boneWeights[0];
        flag           = ds.readByte();
    }
    
    public void write(DataOutputStream ds) throws IOException{
        ds.writeFloat(position);
        ds.writeFloat(normal);
        ds.writeFloat(uv);
        ds.writeUShort(boneIndexes);
        ds.writeByte((byte)(boneWeights[0]*100f));
        ds.writeByte(flag);
    }
    
}
