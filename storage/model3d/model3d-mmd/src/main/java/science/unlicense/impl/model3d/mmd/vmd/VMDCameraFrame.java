

package science.unlicense.impl.model3d.mmd.vmd;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class VMDCameraFrame {
    
    public int frameNumber;
    public Vector position;
    public Vector rotation;
    //TODO : find more infos on this
    public byte[] interpolation;
    
    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        position = new Vector(ds.readFloat(3));
        rotation = new Vector(ds.readFloat(3));
        interpolation = ds.readFully(new byte[33]);
    }
    
}

