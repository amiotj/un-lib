

package science.unlicense.impl.model3d.mmd.vmd;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class VMDLightFrame {
    
    public int frameNumber;
    public float[] color;
    public Vector position;
    
    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        color       = ds.readFloat(3);
        position    = new Vector(ds.readFloat(3));
    }
    
}
