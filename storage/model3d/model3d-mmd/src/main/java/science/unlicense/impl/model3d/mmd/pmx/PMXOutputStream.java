package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.store.StoreException;

/**
 * @author Johann Sorel
 */
public class PMXOutputStream extends DataOutputStream{

    private final int indexSizeVertex;
    private final int indexSizeTexture;
    private final int indexSizeMaterial;
    private final int indexSizeBone;
    private final int indexSizeMorph;
    private final int indexSizeRigidBody;

    public PMXOutputStream(ByteOutputStream out, int indexSizeVertex, 
            int indexSizeTexture, int indexSizeMaterial, 
            int indexSizeBone, int indexSizeMorph, int indexSizeRigidBody) {
        super(out, NumberEncoding.LITTLE_ENDIAN);
        this.indexSizeVertex = indexSizeVertex;
        this.indexSizeTexture = indexSizeTexture;
        this.indexSizeMaterial = indexSizeMaterial;
        this.indexSizeBone = indexSizeBone;
        this.indexSizeMorph = indexSizeMorph;
        this.indexSizeRigidBody = indexSizeRigidBody;
    }
        
    public void writeText(Chars text) throws IOException{
        byte[] data = Arrays.ARRAY_BYTE_EMPTY;
        if(text!=null){
            data = text.toBytes(CharEncodings.UTF_16LE);
        }
        writeInt(data.length);
        write(data);
    }
    
    public void writeValue(int size, int value) throws IOException, StoreException{
        if(size == 1){
            writeUByte(value);
        }else if(size == 2){
            writeUShort(value);
        }else if(size == 4){
            writeInt(value);
        }else{
            throw new StoreException("unsupported number size " + size);
        }
    }
    
    public void writeVertexIndex(int value) throws IOException, StoreException{
        writeValue(indexSizeVertex, value);
    }
    
    public void writeTextureIndex(int value) throws IOException, StoreException{
        writeValue(indexSizeTexture, value);
    }
    
    public void writeMaterialIndex(int value) throws IOException, StoreException{
        writeValue(indexSizeMaterial, value);
    }
    
    public void writeBoneIndex(int value) throws IOException, StoreException{
        writeValue(indexSizeBone, value);
    }
    
    public void writeMorphIndex(int value) throws IOException, StoreException{
        writeValue(indexSizeMorph, value);
    }
    
    public void writeRigidBodyIndex(int value) throws IOException, StoreException{
        writeValue(indexSizeRigidBody, value);
    }
    
}
