
package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class PMXFormat extends AbstractModel3DFormat{

    public static final PMXFormat INSTANCE = new PMXFormat();

    private PMXFormat() {
        super(new Chars("mmd_pmx"),
              new Chars("MMD-PMX"),
              new Chars("MikuMikuDance New Model file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("pmx")
              },
              new byte[][]{PMXConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new PMXStore(input);
    }

}
