package science.unlicense.impl.model3d.mmd.vmd;

import science.unlicense.api.anim.CompoundAnimation;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.engine.opengl.physic.JointTimeSerie;
import science.unlicense.engine.opengl.physic.MorphAnimation;
import science.unlicense.engine.opengl.physic.MorphTimeSerie;
import science.unlicense.engine.opengl.physic.SkeletonAnimation;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.anim.NumberKeyFrame;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.impl.model3d.mmd.MMDUtilities;


/**
 * @author Johann Sorel
 */
public class VMDStore extends AbstractModel3DStore{

    public VMDBoneFrame[] motionframes;
    public VMDFaceMorphFrame[] faceframes;
    public VMDCameraFrame[] cameraframes;
    public VMDShadowFrame[] shadowframes;
    public VMDLightFrame[] lightframes;
    public VMDIKStateFrame[] ikframes;
    public Chars name;
    public Chars dataType;

    private SkeletonAnimation skeletonAnimation;
    private MorphAnimation morphAnimation;

    public VMDStore(Object input) {
        super(VMDFormat.INSTANCE, input);
    }

    public Collection getElements() throws StoreException {
        final ArraySequence col = new ArraySequence();

        try {
            read();
            final CompoundAnimation compound = new CompoundAnimation();
            compound.getElements().add(skeletonAnimation);
            compound.getElements().add(morphAnimation);
            col.add(compound);
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        return col;
    }

    private void read() throws IOException{

        final ByteInputStream inStream = getSourceAsInputStream();
        final DataInputStream dataStream = new DataInputStream(inStream,NumberEncoding.LITTLE_ENDIAN);

        dataType = dataStream.readBlockZeroTerminatedChars(30, CharEncodings.SHIFT_JIS);
        name = dataStream.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
        readBoneFrames(dataStream);
        readFaceFrames(dataStream);
        
        //TODO those do not always exist, have to find how to determinate this
        try{
            readCameraFrames(dataStream);
            readLightFrames(dataStream);
            readShadowFrames(dataStream);
            try{
                //never seen a file with this so far, new version of pmx editor, of special MikuMikuMotion editor ?
                readIKFrames(dataStream);
            }catch(EOSException ex){
                System.out.println("VMD : no extra info found (ik)");
            }
        }catch(Exception ex){
            System.out.println("VMD : no extra info found (camera,light,shadow)");
        }

        //rebuild the skeleton animation
        skeletonAnimation = new SkeletonAnimation();
        skeletonAnimation.setCoordinateSystem(MMDUtilities.COORDSYS);

        for(int i=0;i<motionframes.length;i++){
            final VMDBoneFrame bf = motionframes[i];
            final int time = bf.getTime();

            JointTimeSerie serie = skeletonAnimation.getJointTimeSerie(bf.boneName);
            if(serie == null){
                serie = new JointTimeSerie();
                serie.setJointIdentifier(bf.boneName);
                skeletonAnimation.getSeries().add(serie);
            }
            
            final JointKeyFrame jf = new JointKeyFrame();
            jf.setTime(time);
            jf.setValue(new NodeTransform(3));
            final NodeTransform poseTrs = jf.getValue();
            poseTrs.getTranslation().set(bf.position_3f);
            poseTrs.getRotation().set(bf.rotation_4f.toMatrix3());
            poseTrs.notifyChanged();
            jf.setFrom(JointKeyFrame.FROM_BASE);
            serie.getFrames().add(jf);
        }

        //rebuild morph frames
        morphAnimation = new MorphAnimation();
        for(VMDFaceMorphFrame f : faceframes){
            final double time = f.getTime();
            MorphTimeSerie serie = (MorphTimeSerie) morphAnimation.getSeries().getValue(f.name);
            if(serie==null){
                serie = new MorphTimeSerie(f.name);
            }
            serie.getFrames().add(new NumberKeyFrame(time, f.factor));
        }
                
//        //second pass, we copy values from one step to the other if missing
//        final OrderedSet pairs = serie.getFrames();
//        MorphKeyFrame previous = null;
//        for(Iterator soi=pairs.createIterator();soi.hasNext();){
//            final MorphKeyFrame mf = (MorphKeyFrame) soi.next();
//            if(previous!=null){
//                final Dictionary dico = mf.getMorphRatios();
//                final Dictionary pdico = previous.getMorphRatios();
//                
//                for(Iterator ite=pdico.getPairs().createIterator();ite.hasNext();){
//                    final Pair pair = (Pair) ite.next();
//                    final Object key = pair.getValue1();
//                    if(dico.getValue(key)==null){
//                        dico.add(key, pair.getValue2());
//                    }
//                }
//            }
//            previous = mf;
//        }
        
    }

    private void readBoneFrames(final DataInputStream ds) throws IOException {
        motionframes = new VMDBoneFrame[ds.readInt()];
        for(int k=0;k<motionframes.length; k++) {
            motionframes[k] = new VMDBoneFrame();
            motionframes[k].read(ds);
        }
    }

    private void readFaceFrames(final DataInputStream ds) throws IOException {
        faceframes = new VMDFaceMorphFrame[ds.readInt()];
        for (int i=0; i<faceframes.length; i++) {
            faceframes[i] = new VMDFaceMorphFrame();
            faceframes[i].read(ds);
        }
    }
    
    private void readCameraFrames(final DataInputStream ds) throws IOException {
        cameraframes = new VMDCameraFrame[ds.readInt()];
        for (int i=0; i<cameraframes.length; i++) {
            cameraframes[i] = new VMDCameraFrame();
            cameraframes[i].read(ds);
        }
    }
    
    private void readShadowFrames(final DataInputStream ds) throws IOException {
        shadowframes = new VMDShadowFrame[ds.readInt()];
        for (int i=0; i<shadowframes.length; i++) {
            shadowframes[i] = new VMDShadowFrame();
            shadowframes[i].read(ds);
        }
    }
    
    private void readLightFrames(final DataInputStream ds) throws IOException {
        lightframes = new VMDLightFrame[ds.readInt()];
        for (int i=0; i<lightframes.length; i++) {
            lightframes[i] = new VMDLightFrame();
            lightframes[i].read(ds);
        }
    }
    
    private void readIKFrames(final DataInputStream ds) throws IOException {
        final int size = ds.readInt();
        ikframes = new VMDIKStateFrame[size];
        for (int i=0; i<ikframes.length; i++) {
            ikframes[i] = new VMDIKStateFrame();
            ikframes[i].read(ds);
        }
    }


    public void writeElements(Collection elements) throws StoreException {
        final Iterator ite = elements.createIterator();
        while(ite.hasNext()){
            final Object candidate = ite.next();
            if(candidate instanceof SkeletonAnimation){
                try {
                    write((SkeletonAnimation) candidate);
                } catch (IOException ex) {
                    throw new StoreException(ex);
                }
                break;
            }else{
                throw new StoreException("Unsupported object type : "+candidate);
            }
        }
    }

    private void write(SkeletonAnimation animation) throws IOException {

        final ByteOutputStream bs = getSourceAsOutputStream();
        final DataOutputStream ds = new DataOutputStream(bs,NumberEncoding.LITTLE_ENDIAN);

        //write header
        final byte[] sign = VMDFormat.SIGNATURE.toBytes();
        ds.write(sign);
        ds.write((byte)0x00);
        //write name
        //TODO store a name on the animation ?
        ds.write((byte)0x00);

        //write bone frames
        writeBoneFrames(ds, animation);

        //write face frames
        //TODO
        ds.writeInt(0);

        //TODO camera

        //end
        ds.flush();
        ds.close();

    }

    private void writeBoneFrames(DataOutputStream ds, SkeletonAnimation animation) throws IOException{

        //count number of bone frames
        int nb = 0;
        for(int i=0,n=animation.getSeries().getSize();i<n;i++){
            final JointTimeSerie serie = (JointTimeSerie) animation.getSeries().get(i);
            nb += serie.getFrames().getSize();
        }

        ds.writeInt(nb);
        for(int i=0,n=animation.getSeries().getSize();i<n;i++){
            final JointTimeSerie serie = (JointTimeSerie) animation.getSeries().get(i);
            final Iterator ite = serie.getFrames().createIterator();
            while(ite.hasNext()){
                final JointKeyFrame jf = (JointKeyFrame) ite.next();
                final NodeTransform poseTrs = jf.getValue();

                //write bone name
                final CharArray boneName = (jf.getJoint() instanceof Joint) ? ((Joint)jf.getJoint()).getName() : (Chars)jf.getJoint();
                final byte[] namebytes = boneName.toBytes(CharEncodings.SHIFT_JIS);
                ds.write(namebytes);
                ds.write((byte)0x00);
                //time, convert it back to 30FPS
                ds.writeInt( (int)(jf.getTime() / (100f/3f)));
                //translation
                ds.writeFloat(poseTrs.getTranslation().toArrayFloat());
                //rotation
                ds.writeFloat(new Matrix3x3(poseTrs.getRotation()).toQuaternion().toArrayFloat());
                // transform
                for(int k=0;k<16;k++) ds.writeFloat(0f);
            }
        }
    }

}
