
package science.unlicense.impl.model3d.mmd.vpd;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class VPDConstants {

    public static final Chars HEADER = new Chars("Vocaloid Pose Data file");
    public static final Chars BONE = new Chars("Bone");
    public static final Chars COMMENT = new Chars("//");

    private VPDConstants() {}



}
