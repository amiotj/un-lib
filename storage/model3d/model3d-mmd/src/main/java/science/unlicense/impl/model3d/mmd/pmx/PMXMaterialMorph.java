package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.io.IOException;

/**
 * @author Johann Sorel
 */
public class PMXMaterialMorph {
    
    public int materialIndex;
    public int calcMode;
    public int diffuse;
    public int specular;
    public int specularFactor;
    public int ambiant;
    public int edgeColor;
    public int edgeSize;
    public int textureFactor;
    public int sphereTextureFactor;
    public int toonTextureFactor;
    
    public void setToDefault(){
        materialIndex       = 0;
        calcMode            = 0;
        diffuse             = 0;
        specular            = 0;
        specularFactor      = 0;
        ambiant             = 0;
        edgeColor           = 0;
        edgeSize            = 0;
        textureFactor       = 0;
        sphereTextureFactor = 0;
        toonTextureFactor   = 0;
    }
    
    public void read(PMXInputStream ds) throws IOException{
        throw new IOException("Not implement yet.");
    }
    
    public void write(PMXOutputStream ds) throws IOException{
        throw new IOException("Not implement yet.");
    }
    
}
