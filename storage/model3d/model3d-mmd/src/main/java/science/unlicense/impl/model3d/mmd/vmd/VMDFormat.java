
package science.unlicense.impl.model3d.mmd.vmd;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class VMDFormat extends AbstractModel3DFormat{

    public static final Chars SIGNATURE = new Chars("Vocaloid Motion Data 0002",CharEncodings.US_ASCII);

    public static final VMDFormat INSTANCE = new VMDFormat();

    private VMDFormat() {
        super(new Chars("mmd_vmd"),
              new Chars("MMD-VMD"),
              new Chars("MikuMikuDance Motion Data"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("vmd")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new VMDStore(input);
    }

}
