package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.DefaultLChars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.country.Country;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.physic.body.BodyGroup;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.constraint.AngleLimitConstraint;
import science.unlicense.api.physic.constraint.Constraint;
import science.unlicense.api.physic.force.Force;
import science.unlicense.api.physic.force.Spring;
import science.unlicense.api.physic.skeleton.IKSolver;
import science.unlicense.api.physic.skeleton.IKSolverCCD;
import science.unlicense.api.physic.skeleton.InverseKinematic;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.math.Affine;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Matrix;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.model3d.mmd.pmx.PMXStore;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.engine.opengl.renderer.SilhouetteRendererBackFaceMethod;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.model3d.mmd.MMDUtilities;
import science.unlicense.system.path.Paths;

/**
 * @author Johann Sorel
 */
public class PMDStore extends AbstractModel3DStore {

    // special limit for knees IK
    // values are arbitrary, after looking at multiple PMX files (where the limits is expressed)
    // those values seems to come up the most.
    private static final Chars KNEES = new Chars("ひざ");
    private static final double KNEES_ANGLE_MIN = -Maths.PI;
    private static final double KNEES_ANGLE_MAX = -0.008726646;

    private final Path pmdPath;
    //base parent
    private Path basePath;

    private PMDModel model;


    //cache loaded images
    private final Dictionary imageCache = new HashDictionary();

    public PMDStore(Object input) {
        super(PMDFormat.INSTANCE,input);
        pmdPath = ((Path)getInput());
        basePath = pmdPath.getParent();
    }

    public Collection getElements() throws StoreException {
        if(model==null){
            try{
                model = new PMDModel();
                model.read(getSourceAsInputStream());
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        
        final MultipartMesh node = new MultipartMesh();
        node.setLocalCoordinateSystem(MMDUtilities.COORDSYS);

        //rebuild skeleton
        final Skeleton skeleton = new Skeleton(MMDUtilities.COORDSYS);
        final Joint[] joints = new Joint[model.bones.length];
        for (int i = 0; i < model.bones.length; i++) {
            final PMDBone bone = model.bones[i];

            final Dictionary trs = new HashDictionary();
            trs.add(Country.JPN.asLanguage(), bone.name);
            trs.add(Country.GBR.asLanguage(), bone.englishName);
            final LChars name = new DefaultLChars(trs, Country.JPN.asLanguage());
            final Joint jt = new Joint(3,name);
            jt.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
            jt.getNodeTransform().getTranslation().set(bone.position);
            jt.getNodeTransform().notifyChanged();
            jt.setId(i);
            joints[i] = jt;
        }

        //Second pass to build joints hierarchy
        for(int i=joints.length-1;i>=0;i--){
            final PMDBone bone = model.bones[i];
            if(bone.parentBone >=0){
                joints[bone.parentBone].getChildren().add(joints[i]);
            }else{
                skeleton.getChildren().add(joints[i]);
            }
        }

        //rebuild inverse kinematics
        Arrays.sort(model.kinematics);
        for(int i=0;i<model.kinematics.length;i++){
            final PMDIK pmdik = model.kinematics[i];
            final Joint[] targetJoints = new Joint[pmdik.links.length];
            for(int k=0;k<pmdik.links.length;k++){
                targetJoints[k] = (Joint)skeleton.getJoint(pmdik.links[k]);
                if(targetJoints[k].getName().getFirstOccurence(KNEES)>=0){
                    //joint is in the IK chain, PMD more or less expect ik joint to move
                    //only on the X axis for knees joints
                    final BBox limits = new BBox(3);
                    limits.setRange(0, KNEES_ANGLE_MIN, KNEES_ANGLE_MAX);
                    limits.setRange(1, 0, 0);
                    limits.setRange(2, 0, 0);
                    final Constraint cst = new AngleLimitConstraint(targetJoints[k], limits);
                    targetJoints[k].getConstraints().add(cst);
                }
            }
            final IKSolver solver = new IKSolverCCD(pmdik.maxLoop, pmdik.maxAngle, 3);
            final Joint target = (Joint)skeleton.getJoint(pmdik.targetBone);
            final Joint effector = (Joint)skeleton.getJoint(pmdik.effectBone);
            final InverseKinematic ik = new InverseKinematic(target,effector,targetJoints,solver);

            skeleton.getIks().add(ik);
        }
        
        //we do it before reading part because silhouette will requiere bone positions
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        //rebuild parts
        for(PMDMaterial material : model.materials){
            final Mesh part = buildPartMesh(material);
            node.getChildren().add(part);

            //rebuild skin
            //each vertex is affected by two joints
            final FloatCursor weightCursor = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*2).cursorFloat();
            final IntCursor jointCursor = DefaultBufferFactory.INSTANCE.createInt(material.indices.length*2).cursorInt();

            for(int i=0;i<material.indices.length;i++) {
                final PMDVertex vertex = model.vertices[material.indices[i]];
                jointCursor.write(vertex.boneIndexes);
                weightCursor.write(vertex.boneWeights);
            }

            final Shell shell = (Shell) part.getShape();
            final SkinShell skin = new SkinShell();
            skin.setSkeleton(skeleton);
            skin.setMaxWeightPerVertex(2);
            skin.setVertices(shell.getVertices());
            skin.setNormals(shell.getNormals());
            skin.setIndexes(shell.getIndexes(),shell.getModes());
            skin.setJointIndexes(new VBO(jointCursor.getBuffer(),2));
            skin.setWeights(new VBO(weightCursor.getBuffer(),2));
            skin.setUVs(shell.getUVs());
            skin.setMorphs(shell.getMorphs());
            part.setShape(skin);
            part.getShape().calculateBBox();
            
            if(material.edgeFlags>0){
                //models are in decimeter units, edges size seems to be in millimeter : TODO need to confirm this
                //so we divide by 100 on have it in decimeter
                //which color ? which size ?
                part.getRenderers().add(new SilhouetteRendererBackFaceMethod(part, 
                        Color.BLACK, 2.0f/100));
            }
        }

        node.setSkeleton(skeleton);
        
        
        //rebuild physics
        final RigidBody[] bodies = new RigidBody[model.rigidBodies.length];
        if(model.rigidBodies.length>0){
            //create all body groups
            final Dictionary groups = new HashDictionary();
            for(int i=0;i<32;i++){
                groups.add(i, new BodyGroup(new Chars(""+i)));
            }

            for(int i=0;i<model.rigidBodies.length;i++){
                final PMDRigidBody body = model.rigidBodies[i];

                final Geometry shape;
                if(body.shapeType == PMDConstants.SHAPE_SPHERE){
                    shape = new Sphere(body.shapeSize[0]);
                }else if(body.shapeType == PMDConstants.SHAPE_CAPSULE){
                    shape = new Capsule(body.shapeSize[1], body.shapeSize[0]);
                }else if(body.shapeType == PMDConstants.SHAPE_BOX){
                    final double[] lower = new Vector(body.shapeSize).scale(-1).toArrayDouble();
                    final double[] upper = Arrays.reformatDouble(body.shapeSize);
                    shape = new BBox(lower,upper);

                }else{
                    throw new StoreException("unknowed body type : "+ body.shapeType);
                }
                final RigidBody physicbody = new RigidBody(shape, body.mass);
                physicbody.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
                physicbody.setUpdateMode(RigidBody.UPDATE_PARENT);        
                physicbody.setFixed(body.bodyType==0);

                bodies[i] = physicbody;
                
                //place the physic object
                physicbody.getNodeTransform().getTranslation().set(body.position);
                final Matrix m = Matrix3x3.createFromAngles(body.rotation[0],body.rotation[1],body.rotation[2]);

                physicbody.getNodeTransform().getRotation().set(m);
                physicbody.getNodeTransform().notifyChanged();

                //set material informations
                physicbody.getMaterial().setDensity(1);
                physicbody.getMaterial().setRestitution(body.restitution);
                physicbody.getMaterial().setLinearAttenuation(body.linearAttenuation);
                physicbody.getMaterial().setAngularAttenuation(body.angularAttenuation);
                physicbody.getMaterial().setDynamicFriction(body.friction);
                physicbody.getMaterial().setStaticFriction(body.friction);
                //rebuild groups
                if(body.collisionGroupId!=0){
                    final BodyGroup group = (BodyGroup) groups.getValue(body.collisionGroupId);
                    physicbody.setGroup(group);
                    //rebuild no collision groups
                    for(int k=0;k<32;k++){
                        final boolean nocollide = ((1<<k) & body.collisionGroupMask) == 0;
                        if(nocollide){
                            final BodyGroup no = (BodyGroup) groups.getValue(k);
                            if(no==null){
                                throw new NullPointerException("group should not be null : "+k);
                            }
                            physicbody.addNoCollisionGroup(no);
                        }
                    }
                }

                //attach the related joint
                Joint joint = skeleton.getJoint(body.boneId);
                if(joint!=null){
                    joint.getChildren().add(physicbody);
                    //the physic body is in joint space already
                }else{
                    System.out.println("Physic body is not attached to any bone : "+body);
                }
            }
        }
        
        //rebuild physic constraints
        for(int i=0;i<model.physicConstraints.length;i++){
            final PMDPhysicConstraint cst = model.physicConstraints[i];
            final RigidBody rbA = bodies[cst.rigidBodyId1];
            final RigidBody rbB = bodies[cst.rigidBodyId2];
            final Tuple centerA = rbA.getWorldPosition(null);
            final Tuple centerB = rbB.getWorldPosition(null);
            final Force force = new Spring(rbA, rbB, 5, 0.5, Distance.distance(centerA, centerB));
            
            node.getConstraints().add(force);
        }
        
                
        final Collection col = new ArraySequence();
        col.add(node);
        return col;
    }

    private Mesh buildPartMesh(final PMDMaterial material) {
        final Mesh mesh = new Mesh();
        mesh.setLocalCoordinateSystem(MMDUtilities.COORDSYS);
        
        //TODO seems like we are missing an information,
        //some texture should obviously be back culled, yet we don't know which ones
        ((MeshRenderer)mesh.getRenderers().get(0)).getState().setCulling(-1);
        
        if(material.textureName != null){
            mesh.setName(Paths.stripExtension(material.textureName));
        }

        final FloatCursor vertexBuffer = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*3).cursorFloat();
        final FloatCursor normalBuffer = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*3).cursorFloat();
        final IntCursor indexBuffer = DefaultBufferFactory.INSTANCE.createInt(material.indices.length).cursorInt();

        for(int i=0;i<material.indices.length;i++) {
            vertexBuffer.write(model.vertices[material.indices[i]].position);
            normalBuffer.write(model.vertices[material.indices[i]].normal);
            indexBuffer.write(i);
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertexBuffer.getBuffer(),3));
        shell.setNormals(new VBO(normalBuffer.getBuffer(),3));
        shell.setIndexes(new IBO(indexBuffer.getBuffer(),3),IndexRange.TRIANGLES(0, (int) indexBuffer.getBuffer().getPrimitiveCount()));
        mesh.setShape(shell);

        
        //set default colors
        mesh.getMaterial().setAmbient(new Color(material.ambiant_RGB));
        mesh.getMaterial().setDiffuse(new Color(material.diffuse_RGBA));
        mesh.getMaterial().setSpecular(new Color(material.specular_RGB));
                
        Path p = null;
        try {
            textured:
            if (material.textureName != null && !material.textureName.isEmpty()) {
                Chars baseName = material.textureName;

                final Sequence imageFiles = PMXStore.splitTextures(basePath, baseName);
                if(imageFiles.isEmpty()){
                    getLogger().info(new Chars("No image files found for : "+baseName));
                    break textured;
                }
                final Sequence images = new ArraySequence();
                for(int i=0,n=imageFiles.getSize();i<n;i++){
                    p = (Path) imageFiles.get(i);
                    Texture2D texture = (Texture2D) imageCache.getValue(p);
                    if(texture==null){
                        //load image
                        Image img = PMXStore.loadImage(p);
                        texture = new Texture2D(img);
                        imageCache.add(p, texture);
                    }
                    images.add(texture);
                }
                if(images.isEmpty()) throw new IOException("No image files could be read.");
                final Layer[] materialLayers = PMXStore.buildTexture(imageFiles,images);

                final FloatCursor uvCursor = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*2).cursorFloat();
                for(int i=0;i<material.indices.length;i++) {
                    final int m = material.indices[i];
                    uvCursor.write(model.vertices[m].uv);
                }
                shell.setUVs(new VBO(uvCursor.getBuffer(), 2));
                for(int i=0;i<materialLayers.length;i++){
                    mesh.getMaterial().getLayers().add(materialLayers[i]);
                }
            }
        } catch (IOException ex) {
            getLogger().warning(new Chars("Failed to load image for path" + CObjects.toChars(p)+""+ex.getMessage()),ex);
        }

        //rebuild morph frames
        rebuildMorphFrames(mesh, material, vertexBuffer.getBuffer());
        
        mesh.getShape().calculateBBox();
        return mesh;
    }

    
    private void rebuildMorphFrames(Mesh mesh, PMDMaterial material, Buffer vertexBuffer){
        if(model.morphs.length==0) return;
        
        //cache vertex indexes from material for faster morph reconstruction
        int minIdx = Integer.MAX_VALUE;
        int maxIdx = Integer.MIN_VALUE;
        for(int i=0,idx,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            if(idx<minIdx) minIdx = idx;
            if(idx>maxIdx) maxIdx = idx;
        }
        final int[][] dico = new int[maxIdx-minIdx+1][0];
        for(int i=0,idx,did,n=material.indices.length;i<n;i++) {
            idx = material.indices[i];
            did = idx-minIdx;
            dico[did] = Arrays.insert(dico[did], dico[did].length, i);
        }
        
        
        final MorphSet ms = new MorphSet();
        
        for(int k=0;k<model.morphs.length;k++){
            final PMDMorph morph = model.morphs[k];
            
            final FloatCursor copy = DefaultBufferFactory.INSTANCE.createFloat(material.indices.length*3).cursorFloat();
            copy.setPosition(0);
            
            boolean modified = false;
            for(PMDMorph.VertexPosition ele : morph.movements){
                //indexes after the first morph are indirected from the base morph
                final int trueIdx;
                if(k==0){
                    trueIdx = ele.index;
                }else{
                    trueIdx = model.morphs[0].movements[ele.index].index;
                }
                
                if(trueIdx<minIdx || trueIdx>maxIdx) continue;
                final int[] ref = dico[trueIdx-minIdx];
                if(ref.length>0){
                    modified = true;
                    //replace coords
                    for(int i=0;i<ref.length;i++) {
                        int ri = ref[i]*3;
                        copy.setPosition(ri);
                        copy.write(ele.position_3f);
                    }
                }
            }
            if(modified){
                final MorphTarget mf = new MorphTarget();
                mf.setFactor(0);
                mf.setName(morph.name);
                mf.setVertices(new VBO(copy.getBuffer(), 3));
                ms.getMorphs().add(mf);
            }
        }
        
        ((Shell)mesh.getShape()).setMorphs(ms);
    }

    /**
     * Writing will search and write only the first MultiPartMesh found in the collection.
     * 
     * @param elements
     * @throws IOException 
     */
    public void writeElements(Collection elements) throws StoreException {
        MultipartMesh mpm = null;
        for(Iterator ite=elements.createIterator();ite.hasNext();){
            final Object obj = ite.next();
            if(obj instanceof MultipartMesh){
                mpm = (MultipartMesh) obj;
                break;
            }
        }
        
        if(mpm!=null){
            try{
                PMDModel model = toModel(mpm);
                final ByteOutputStream out = getSourceAsOutputStream();
                model.write(out);
                out.close();
            }catch(IOException ex){
                throw new StoreException(ex);
            }
        }
    }
    
    private PMDModel toModel(MultipartMesh mpm) throws IOException{
        final PMDModel model = new PMDModel();
        model.setToDefault();
        
        //counters for dynamic names
        int texturecounter = 0;
        
        //transform the skeleton and rigid bodies
        final Skeleton skeleton = mpm.getSkeleton();
        if(skeleton!=null){
            final Dictionary buildDico = new HashDictionary();
            final Sequence roots = skeleton.getChildren();
            final Sequence result = new ArraySequence();
            for(int i=0;i<roots.getSize();i++){
                buildSkeleton((Joint)roots.get(i), buildDico, result);
            }
            
            model.bones = new PMDBone[result.getSize()];
            Collections.copy(result, model.bones, 0);
        }
        
        //transform the mesh
        final Iterator children = mpm.getChildren().createIterator();
        while (children.hasNext()) {
            final Object next = children.next();
            if(next instanceof Mesh){
                final Mesh mesh = (Mesh) next;
                final Shell shell = (Shell) mesh.getShape();
                
                final int vertexBaseOffset = model.vertices.length;
                
                //rebuild vertices
                final VBO vertices = shell.getVertices();
                final VBO normals = shell.getNormals();
                final VBO uvs = shell.getUVs();
                VBO jointIndexes = null;
                VBO jointWeights = null;
                if(shell instanceof SkinShell){
                    jointIndexes = ((SkinShell)shell).getJointIndexes();
                    jointWeights = ((SkinShell)shell).getWeights();
                }
                
                final int vn = (int) (vertices.getPrimitiveBuffer().getPrimitiveCount()/3);
                model.vertices = (PMDVertex[])Arrays.resize(model.vertices, vertexBaseOffset+vn);
                
                for(int v=0;v<vn;v++){
                    final PMDVertex vertex = new PMDVertex();
                    vertex.setToDefault();
                    model.vertices[vertexBaseOffset+v] = vertex;
                    vertices.getTupleFloat(v, vertex.position);
                    if(normals!=null) normals.getTupleFloat(v, vertex.normal);
                    if(uvs!=null)uvs.getTupleFloat(v, vertex.uv);
                    if(jointIndexes!=null){
                        final float[] weights = jointWeights.getTupleFloat(v, null);
                        final int[] indexes = jointIndexes.getTupleInt(v, null);
                        Arrays.copy(weights, 0, Maths.min(weights.length, 2),vertex.boneWeights, 0);
                        Arrays.copy(indexes, 0, Maths.min(indexes.length, 2),vertex.boneIndexes, 0);
                    }
                }
                
                //rebuild material
                final Material material = mesh.getMaterial();
                final PMDMaterial pmdMaterial = new PMDMaterial();
                pmdMaterial.setToDefault();
                model.materials = (PMDMaterial[]) Arrays.insert(model.materials, model.materials.length, pmdMaterial);
                
                //texture informations
                for(Iterator ite=material.getLayers().createIterator();ite.hasNext();){
                    final Layer layer = (Layer) ite.next();
                    final int type = layer.getType();
                    if(type==Layer.TYPE_DIFFUSE){
                        final Mapping mapping = layer.getMapping();
                        if(mapping instanceof PlainColorMapping){
                            final PlainColorMapping cm = (PlainColorMapping) mapping;
                            pmdMaterial.diffuse_RGBA = cm.getColor().toRGBA();
                        }else if(mapping instanceof UVMapping){
                            final UVMapping uvm = (UVMapping) mapping;
                            final Image texture = uvm.getTexture().getImage();
                            pmdMaterial.textureName = new Chars("texture"+(texturecounter++)+".bmp");
                            //save texture aside pmd file
                            final Path texturePath = basePath.resolve(pmdMaterial.textureName);
                            Images.write(texture, new Chars("bmp"), texturePath);
                        }
                    }
                }
                
                //face indices
                final int[] indexes = shell.getIndexes().getPrimitiveBuffer().toIntArray();
                pmdMaterial.indices = new int[indexes.length];
                for(int t=0;t<indexes.length;t++){
                    pmdMaterial.indices[t] = vertexBaseOffset + indexes[t];
                }
                model.faceIndex = Arrays.concatenate(new int[][]{model.faceIndex,pmdMaterial.indices});
            }
        }
        
        return model;
    }
    
    private void buildSkeleton(Joint joint, Dictionary all, Sequence result){
        final PMDBone bone = new PMDBone();
        bone.setToDefault();
        bone.name = joint.getName().toChars();
        bone.englishName = bone.name;
        bone.ik = 0;
        
        final Affine matrix = joint.getNodeToRootSpace();
        bone.position = Arrays.reformatToFloat(matrix.transform(new double[3],null));
        
        final Integer parentIndex = (Integer) all.getValue(joint.getParent());
        if(parentIndex!=null){
            bone.parentBone = parentIndex.shortValue();
        }
        
        all.add(result.getSize(), bone);
        result.add(bone);
        
        //loop on children
        final Iterator ite = joint.getChildren().createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if(next instanceof Joint){
                buildSkeleton((Joint)next, all, result);
            }else if(next instanceof RigidBody){
                //TODO
            }
        }
        
    }
    
}