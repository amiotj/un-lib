
package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;

/**
 *
 * @author Johann Sorel
 */
public interface BoneDeform {
 
    int getNbJoint();
    
    void read(PMXInputStream ds) throws IOException, StoreException;
    
    void write(PMXOutputStream ds) throws IOException, StoreException;
    
}
