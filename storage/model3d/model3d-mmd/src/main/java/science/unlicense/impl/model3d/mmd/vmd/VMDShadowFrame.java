

package science.unlicense.impl.model3d.mmd.vmd;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class VMDShadowFrame {
 
    public int frameNumber;
    public int type;
    public float distance;
    
    public void read(DataInputStream ds) throws IOException{
        frameNumber = ds.readInt();
        type        = ds.readUByte();
        distance    = ds.readFloat();
    }
    
}
