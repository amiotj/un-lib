

package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.mmd.MMDUtilities;

/**
 *
 * @author Johann Sorel
 */
public class PMDPhysicConstraint {
    
    /** constraint name */
    public Chars name;
    /** first affected rigid body */
    public int rigidBodyId1;
    /** second affected rigid body */
    public int rigidBodyId2;
    /** constraint position, relative to first rigid body */
    public float[] postion;
    public float[] rotation;
    /** postion and rotation limits */
    public float[] positionLower;
    public float[] positionUpper;
    public float[] rotationLower;
    public float[] rotationUpper;
    /** Spring stiffness */
    public float[] positionStiffness;
    public float[] rotationStiffness;
    
    public void setToDefault(){
        name                = null;
        rigidBodyId1        = -1;
        rigidBodyId1        = -1;
        postion             = new float[3];
        rotation            = new float[3];
        positionLower       = new float[3];
        positionUpper       = new float[3];
        rotationLower       = new float[3];
        rotationUpper       = new float[3];
        positionStiffness   = new float[3];
        rotationStiffness   = new float[3];
    }
    
    public void read(DataInputStream ds) throws IOException{
        name                = ds.readBlockZeroTerminatedChars(20, CharEncodings.US_ASCII);
        rigidBodyId1        = ds.readInt();
        rigidBodyId2        = ds.readInt();
        postion             = ds.readFloat(3);
        rotation            = ds.readFloat(3);
        positionLower       = ds.readFloat(3);
        positionUpper       = ds.readFloat(3);
        rotationLower       = ds.readFloat(3);
        rotationUpper       = ds.readFloat(3);
        positionStiffness   = ds.readFloat(3);
        rotationStiffness   = ds.readFloat(3);
    }
 
    public void write(DataOutputStream ds) throws IOException{
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(name,CharEncodings.SHIFT_JIS,20), 20, CharEncodings.SHIFT_JIS);
        ds.writeInt(rigidBodyId1);
        ds.writeInt(rigidBodyId2);
        ds.writeFloat(postion);
        ds.writeFloat(rotation);
        ds.writeFloat(positionLower);
        ds.writeFloat(positionUpper);
        ds.writeFloat(rotationLower);
        ds.writeFloat(rotationUpper);
        ds.writeFloat(positionStiffness);
        ds.writeFloat(rotationStiffness);
    }
    
}
