package science.unlicense.impl.model3d.mmd.pmx;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.model3d.mmd.MMDUtilities;

/**
 * @author Johann Sorel
 */
public class PMXBone {
        
    public Chars name;
    public Chars englishName;
    /** size 3*/
    public float[] position;
    /** parent bone index */
    public int parentIndex;
    public int layer;
    public int flag;
    
    /** size 3*/
    public float[] tailPosition;
    public int tailIndex = -1;
    public int effectIndex = -1;
    public float effectFactor = 0f;
    /** size 3*/
    public float[] fixedAxis;
    /** size 3*/
    public float[] localXVector;
    /** size 3*/
    public float[] localZVector;
    public int externalKey = -1;
    /** inverse kinematic */
    public PMXIK ik;
    
    public int index;
    
    
    public void setToDefault(){
        name            = null;
        englishName     = null;
        position        = new float[3];
        parentIndex     = 255;
        layer           = 0;
        flag            = 0;
        tailPosition    = new float[3];
        tailIndex       = -1;
        effectIndex     = -1;
        effectFactor    = 0f;
        fixedAxis       = new float[3];
        localXVector    = new float[3];
        localZVector    = new float[3];
        externalKey     = -1;
        ik              = null;
        index           = 0;
    }
    
    public void read(PMXInputStream ds) throws IOException, StoreException{
        name          = ds.readText();
        englishName   = ds.readText();

        //try to translate bone name
        if(englishName.isEmpty()){
            englishName = MMDUtilities.toEnglish(name);
        }

        position      = ds.readFloat(3);
        parentIndex   = ds.readBoneIndex();
        layer         = ds.readInt();
        flag          = ds.readUShort();
        if((flag & PMXConstants.FLAG_TAILPOS_IS_BONE)==1){
            tailIndex = ds.readBoneIndex();
        }else{
            tailPosition = ds.readFloat(3);
        }

        if(  (flag & PMXConstants.FLAG_IS_EXTERNAL_ROTATION)!=0
          || (flag & PMXConstants.FLAG_IS_EXTERNAL_TRANSLATION)!=0){
            effectIndex  = ds.readBoneIndex();
            effectFactor = ds.readFloat();
        }

        if((flag & PMXConstants.FLAG_HAS_FIXED_AXIS)!=0){
            fixedAxis = ds.readFloat(3);
        }

        if((flag & PMXConstants.FLAG_HAS_LOCAL_COORDINATE)!=0){
            localXVector = ds.readFloat(3);
            localZVector = ds.readFloat(3);
        }

        if((flag & PMXConstants.FLAG_IS_EXTERNAL_PARENT_DEFORM)!=0){
            externalKey = ds.readInt();
        }

        if((flag & PMXConstants.FLAG_IS_IK)!=0){
            ik = new PMXIK();
            ik.read(ds);
        }
    }
    
    public void write(PMXOutputStream ds) throws IOException, StoreException{
        ds.writeText(name);
        ds.writeText(englishName);
        ds.writeFloat(position);
        ds.writeBoneIndex(parentIndex);
        ds.writeInt(layer);
        ds.writeUShort(flag);
        
        if((flag & PMXConstants.FLAG_TAILPOS_IS_BONE)==1){
            ds.writeBoneIndex(tailIndex);
        }else{
            ds.writeFloat(tailPosition);
        }
        
        if(  (flag & PMXConstants.FLAG_IS_EXTERNAL_ROTATION)!=0
          || (flag & PMXConstants.FLAG_IS_EXTERNAL_TRANSLATION)!=0){
            ds.writeBoneIndex(effectIndex);
            ds.writeFloat(effectFactor);
        }
        
        if((flag & PMXConstants.FLAG_HAS_FIXED_AXIS)!=0){
            ds.writeFloat(fixedAxis);
        }

        if((flag & PMXConstants.FLAG_HAS_LOCAL_COORDINATE)!=0){
            ds.writeFloat(localXVector);
            ds.writeFloat(localZVector);
        }

        if((flag & PMXConstants.FLAG_IS_EXTERNAL_PARENT_DEFORM)!=0){
            ds.writeInt(externalKey);
        }

        if((flag & PMXConstants.FLAG_IS_IK)!=0){
            ik.write(ds);
        }
    }
    
}
