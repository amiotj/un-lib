
package science.unlicense.impl.model3d.mmd.vpd;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class VPDFormat extends AbstractModel3DFormat{

    public static final VPDFormat INSTANCE = new VPDFormat();

    private VPDFormat() {
        super(new Chars("mmd_vpd"),
              new Chars("MMD-VPD"),
              new Chars("MikuMikuDance Pose file"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("vpd")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new VPDStore(input);
    }

}
