package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.model3d.mmd.MMDUtilities;
import static science.unlicense.impl.model3d.mmd.MMDUtilities.*;

/**
 * @author Johann Sorel
 */
public class PMDBone {
    
    public Chars name;
    /** translated value, if possible */
    public Chars englishName;
    public short parentBone;
    public short nbChildNo;
    public byte boneType;
    public short ik;
    public float[] position;
    
    public void setToDefault(){
        name = null;
        englishName = null;
        parentBone = -1;
        nbChildNo = 0;
        boneType = 0;
        ik = 0;
        position = new float[3];
    }
    
    public void read(DataInputStream ds) throws IOException{
        name        = ds.readBlockZeroTerminatedChars(20, CharEncodings.SHIFT_JIS);
        englishName = toEnglish(name);
        parentBone  = ds.readShort();
        nbChildNo   = ds.readShort();
        boneType    = ds.readByte();
        ik          = ds.readShort();
        position    = ds.readFloat(3);
    }
 
    public void write(DataOutputStream ds) throws IOException{
        ds.writeBlockZeroTerminatedChars(MMDUtilities.truncate(name,CharEncodings.SHIFT_JIS,20), 20, CharEncodings.SHIFT_JIS);
        ds.writeShort(parentBone);
        ds.writeShort(nbChildNo);
        ds.writeByte(boneType);
        ds.writeShort(ik);
        ds.writeFloat(position);
    }
    
}