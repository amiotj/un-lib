
package science.unlicense.impl.model3d.mmd.vpd;

import science.unlicense.impl.model3d.mmd.vpd.VPDStore;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.physic.RelativeSkeletonPose;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class VPDStoreTest {

    @Test
    public void readTest() throws IOException, StoreException{

        Path input = Paths.resolve(new Chars("mod:/un/storage/model3d/mmd/vpd/pose.vpd"));

        final VPDStore store = new VPDStore(input);

        final RelativeSkeletonPose pose = (RelativeSkeletonPose) store.getElements().createIterator().next();
        Assert.assertEquals(new Chars("modelfile"), store.getOrigin());
        final Sequence joints = pose.getJointPoses();

        Assert.assertEquals(joints.getSize(), 2);
        final JointKeyFrame bone0 = (JointKeyFrame) joints.get(0);
        final JointKeyFrame bone1 = (JointKeyFrame) joints.get(1);

        Assert.assertEquals(new Chars("first bone"), bone0.getJoint());
        Assert.assertEquals(new Vector(1,2,3), bone0.getValue().getTranslation());
        //assertEquals(new Quaternion(4,5,6,7), bone0.getRotation()); TODO

        Assert.assertEquals(new Chars("second bone"), bone1.getJoint());
        Assert.assertEquals(new Vector(3,2,1), bone1.getValue().getTranslation());
        //assertEquals(new Quaternion(9,8,7,6), bone1.getRotation()); TODO


    }

    @Test
    public void writeTest() throws IOException, StoreException{

        final ArrayOutputStream out = new ArrayOutputStream();


        final VPDStore store = new VPDStore(out);
        final RelativeSkeletonPose pose = new RelativeSkeletonPose();
        pose.getJointPoses().add(new JointKeyFrame(new Chars("first bone"),
                new Vector(1,2,3), new Quaternion(1, 0, 0, 1),
                JointKeyFrame.FROM_BASE));
        pose.getJointPoses().add(new JointKeyFrame(new Chars("second bone"),
                new Vector(3,2,1), new Quaternion(0, 1, 0, 1),
                JointKeyFrame.FROM_BASE));

        final Sequence col = new ArraySequence();
        col.add(pose);
        store.writeElements(col);

        final Chars result = new Chars(out.getBuffer().toArrayByte());
        final Chars expected = new Chars(
                "Vocaloid Pose Data file\n" +
                "\n" +
                "unknowned.osm;\n" +
                "2;\n" +
                "\n" +
                "Bone0{first bone\n" +
                "  1.0,2.0,3.0;\n" +
                "  1.0,0.0,0.0,-1.0;\n" +
                "}\n" +
                "\n" +
                "Bone1{second bone\n" +
                "  3.0,2.0,1.0;\n" +
                "  0.0,1.0,0.0,1.0;\n" +
                "}\n" +
                "\n");

        Assert.assertEquals(expected, result);

    }

}
