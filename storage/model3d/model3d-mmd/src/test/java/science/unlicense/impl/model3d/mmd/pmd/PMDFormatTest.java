
package science.unlicense.impl.model3d.mmd.pmd;

import science.unlicense.impl.model3d.mmd.pmd.PMDFormat;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.model3d.Model3DFormat;
import science.unlicense.api.model3d.Model3Ds;

/**
 *
 * @author Johann Sorel
 */
public class PMDFormatTest {

    @Test
    public void testRegistered() throws Exception{
        final Model3DFormat[] formats = Model3Ds.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof PMDFormat){
                //ok, found it
                return;
            }
        }

        Assert.fail("PMD format not found.");
    }

}
