
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XFrameTransformMatrix extends XObject{

    public XFrameTransformMatrix() {
        super(XConstants.TYPE_FRAME_TRANSFORM_MATRIX);
    }
    
    
    
}
