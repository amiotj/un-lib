
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XColorRGBA extends XObject{

    public XColorRGBA() {
        super(XConstants.TYPE_COLOR_RGBA);
    }
    
    
    
}
