
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XCoords2D extends XObject{

    public XCoords2D() {
        super(XConstants.TYPE_COORDS2D);
    }
    
    
    
}
