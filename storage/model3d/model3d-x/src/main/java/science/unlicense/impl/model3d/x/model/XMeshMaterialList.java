
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XMeshMaterialList extends XObject{

    public XMeshMaterialList() {
        super(XConstants.TYPE_MESH_MATERIAL_LIST);
    }
    
    
    
}
