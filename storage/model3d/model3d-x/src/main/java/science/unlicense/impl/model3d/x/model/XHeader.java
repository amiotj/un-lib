
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XHeader extends XObject{

    public XHeader() {
        super(XConstants.TYPE_HEADER);
    }
    
}
