
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XTextureFileName extends XObject{

    public XTextureFileName() {
        super(XConstants.TYPE_TEXTURE_FILENAME);
    }
    
    
    
}
