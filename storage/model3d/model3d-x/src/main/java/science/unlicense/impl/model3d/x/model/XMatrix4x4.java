
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XMatrix4x4 extends XObject{

    public XMatrix4x4() {
        super(XConstants.TYPE_MATRIX_4X4);
    }
    
    
    
}
