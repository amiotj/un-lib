
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XMaterial extends XObject{

    public XMaterial() {
        super(XConstants.TYPE_MATERIAL);
    }
    
    
    
}
