
package science.unlicense.impl.model3d.x.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.model3d.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XFileHeader {
    
    public int majorVersion;
    public int minorVersion;
    public Chars format;
    public int floatSize;

    public XFileHeader() {
    }

    public XFileHeader(int majorVersion, int minorVersion, Chars format, int floatSize) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
        this.format = format;
        this.floatSize = floatSize;
    }
    
    /**
     * Set header to default values
     */
    public void reset(){
        majorVersion = 3;
        minorVersion = 2;
        format = XConstants.FORMAT_BIN;
        floatSize = 32;
    }
    
    public void read(DataInputStream ds) throws IOException{
        //skip xof
        ds.skipFully(4);
        final byte[] buffer = ds.readFully(new byte[12]);
        majorVersion    = Int32.decode(new Chars(Arrays.copy(buffer, 0, 2)));
        minorVersion    = Int32.decode(new Chars(Arrays.copy(buffer, 2, 2)));
        format          = new Chars(Arrays.copy(buffer, 4, 4));
        floatSize       = Int32.decode(new Chars(Arrays.copy(buffer, 8, 4)));
    }
    
    
}
