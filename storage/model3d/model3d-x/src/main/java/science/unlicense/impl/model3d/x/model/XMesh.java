
package science.unlicense.impl.model3d.x.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.impl.model3d.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XMesh extends XObject{

    public XMesh() {
        super(XConstants.TYPE_MESH);
    }
    
    public float[] getVertices(){
         final Object[] xvertex = (Object[]) getFieldValue(new Chars("vertices"));

         float[] vertices = new float[xvertex.length*3];
         for(int i=0,k=0;i<xvertex.length;i++,k+=3){
             vertices[k+0] = ((Number)((XVector)xvertex[i]).getFieldValue(new Chars("x"))).floatValue();
             vertices[k+1] = ((Number)((XVector)xvertex[i]).getFieldValue(new Chars("y"))).floatValue();
             vertices[k+2] = ((Number)((XVector)xvertex[i]).getFieldValue(new Chars("z"))).floatValue();
         }

         return vertices;
    }

    public int[] getFaces(){
         final Object[] xFaces = (Object[]) getFieldValue(new Chars("faces"));

         final IntSequence faces = new IntSequence();
         for(int i=0,k=0;i<xFaces.length;i++,k+=3){
             final XMeshFace face = ((XMeshFace)xFaces[i]);
             faces.put(face.getIndices());
         }

         return faces.toArrayInt();
    }

    public float[] getNormals(){
         final Object[] children = (Object[]) getFieldValue(new Chars("children"));
         if(children!=null){
            for(int i=0;i<children.length;i++){
                if(children[i] instanceof XMeshNormals){
                    return ((XMeshNormals)children[i]).getNormals();
                }
            }
         }

         return null;
    }

}
