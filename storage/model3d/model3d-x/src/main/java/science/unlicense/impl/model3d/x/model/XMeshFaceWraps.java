
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XMeshFaceWraps extends XObject{

    public XMeshFaceWraps() {
        super(XConstants.TYPE_MESH_FACE_WRAPS);
    }
    
    
    
}
