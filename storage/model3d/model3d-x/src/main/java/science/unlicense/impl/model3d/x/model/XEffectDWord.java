
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XEffectDWord extends XObject{

    public XEffectDWord() {
        super(XConstants.TYPE_EFFECT_DWORD);
    }
    
    
    
}
