
package science.unlicense.impl.model3d.x.model;

import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.DocumentType;

/**
 *
 * @author Johann Sorel
 */
public class XObject extends DefaultDocument{

    public XObject(DocumentType type) {
        super(true,type);
    }
    
}
