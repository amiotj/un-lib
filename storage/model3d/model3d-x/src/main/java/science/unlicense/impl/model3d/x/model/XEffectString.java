
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XEffectString extends XObject{

    public XEffectString() {
        super(XConstants.TYPE_EFFECT_STRING);
    }
    
    
    
}
