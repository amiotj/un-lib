
package science.unlicense.impl.model3d.x;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.DefaultFieldType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class XTxtReader implements XReader{

    private static Predicate TRIM;
    private static final OrderedHashDictionary TOKENS = new OrderedHashDictionary();
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    private static TokenType TOKEN_ID;
    private static TokenType TOKEN_UID;
    private static Rule RULE_FILE;
    private static Rule RULE_TEMPLATE;
    private static Rule RULE_OBJECT;
    private static Rule RULE_UUID;
    private static Rule RULE_PARAM;
    private static Rule RULE_CONSTRAINT;
    private static Rule RULE_TYPE;
    private static Rule RULE_ARRAY;
    private static Rule RULE_VALUE;
    private static Rule RULE_REF;


    private static synchronized void init(){
        if(TRIM!=null) return;

        final UNGrammarReader reader = new UNGrammarReader();
        try{
            reader.setInput(Paths.resolve(new Chars("mod:/un/storage/model3d/x/xgrammar.gr")));
            reader.read(TOKENS, RULES);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(),ex);
        }

        // grammar parser is already tested
        RULE_FILE = (Rule) RULES.getValue(new Chars("file"));

        //tokens to ignore
        final Dictionary tokenTypes = (Dictionary) TOKENS.getValue(new Chars("DEFAULT"));
        final TokenType whitespace = (TokenType) tokenTypes.getValue(new Chars("WS"));
        final TokenType semicolon = (TokenType) tokenTypes.getValue(new Chars("SEMICOLON"));
        final TokenType template = (TokenType) tokenTypes.getValue(new Chars("TEMPLATE"));
        final TokenType bracketLeft = (TokenType) tokenTypes.getValue(new Chars("BL"));
        final TokenType bracketRight = (TokenType) tokenTypes.getValue(new Chars("BR"));
        final TokenType arrayLeft = (TokenType) tokenTypes.getValue(new Chars("AL"));
        final TokenType arrayRight = (TokenType) tokenTypes.getValue(new Chars("AR"));
        final TokenType arrowLeft = (TokenType) tokenTypes.getValue(new Chars("UL"));
        final TokenType arrowRight = (TokenType) tokenTypes.getValue(new Chars("UR"));
        final Rule ws = (Rule) RULES.getValue(new Chars("ws"));

        TOKEN_ID = (TokenType) tokenTypes.getValue(new Chars("ID"));
        TOKEN_UID = (TokenType) tokenTypes.getValue(new Chars("UID"));

        RULE_TEMPLATE = (Rule) RULES.getValue(new Chars("template"));
        RULE_OBJECT = (Rule) RULES.getValue(new Chars("object"));
        RULE_UUID = (Rule) RULES.getValue(new Chars("uuid"));
        RULE_PARAM = (Rule) RULES.getValue(new Chars("param"));
        RULE_CONSTRAINT = (Rule) RULES.getValue(new Chars("constraint"));
        RULE_TYPE = (Rule) RULES.getValue(new Chars("type"));
        RULE_ARRAY = (Rule) RULES.getValue(new Chars("array"));
        RULE_VALUE = (Rule) RULES.getValue(new Chars("value"));
        RULE_REF = (Rule) RULES.getValue(new Chars("ref"));

        TRIM = new Predicate() {
            @Override
            public Boolean evaluate(Object candidate) {
                final SyntaxNode sn = (SyntaxNode) candidate;
                if(sn.getRule()==ws) return true;
                final Token token = sn.getToken();
                return token!=null && (token.type == whitespace
                        || token.type==semicolon
                        || token.type==template
                        || token.type==bracketLeft || token.type==bracketRight
                        || token.type==arrayLeft || token.type==arrayRight
                        || token.type==arrowLeft || token.type==arrowRight) ;
            }
        };
    }

    private Object in;

    public void setInput(Object input) throws IOException {
        this.in = input;
    }

    public Object getInput() {
        return in;
    }

    public Document getConfiguration() {
        return null;
    }

    public void setConfiguration(Document configuration) {
        //ignore it
    }
    
    public Sequence read() throws IOException{
        init();

        //prepare lexer
        final Lexer lexer = new Lexer();
        lexer.setInput(in);
        //prepare parser
        final Parser parser = new Parser(RULE_FILE);
        parser.setInput(lexer);

        final SyntaxNode node = parser.parse();
        node.trim(TRIM);


        final Dictionary typesByName = new HashDictionary();
        final Dictionary typesByUID = new HashDictionary();
        final Sequence docs = new ArraySequence();

        final Iterator ite = node.getChildren().createIterator();
        while(ite.hasNext()){
            final SyntaxNode sn = (SyntaxNode) ite.next();
            if(sn.getRule() == RULE_TEMPLATE){
                final Chars id = sn.getChildByToken(TOKEN_ID).getTokensChars();
                final Chars uuid = sn.getChildByRule(RULE_UUID).getChildByToken(TOKEN_UID).getTokensChars();

                final Sequence params = sn.getChildrenByRule(RULE_PARAM);
                final SyntaxNode constraint = sn.getChildByRule(RULE_CONSTRAINT);

                final FieldType[] fields = new FieldType[params.getSize() + (constraint==null?0:1)];
                for(int i=0;i<params.getSize();i++){
                    final SyntaxNode fn = (SyntaxNode) params.get(i);
                    final SyntaxNode type = fn.getChildByRule(RULE_TYPE);
                    final SyntaxNode array = fn.getChildByRule(RULE_ARRAY);
                    final Sequence ids = fn.getChildrenByToken(TOKEN_ID);

                    //decode array type
                    Chars desc = null;
                    int minOcc = 1;
                    int maxOcc = 1;
                    if(array!=null){
                        SyntaxNode t = (SyntaxNode) array.getChildren().get(0);
                        if(t.getToken().type == TOKEN_ID){
                            //dynamic size
                            desc = t.getTokensChars();
                            minOcc = -1;
                            maxOcc = -1;
                        }else{
                            minOcc = Int32.decode(t.getTokensChars());
                            maxOcc = minOcc;
                        }
                    }

                    //decode type
                    Chars fid;
                    Class valueType;
                    DocumentType ref = null;
                    if(type==null){
                        Chars valueTypeName = ((SyntaxNode)ids.get(0)).getTokensChars();
                        ref = (DocumentType) typesByName.getValue(valueTypeName);
                        if(ref==null) throw new InvalidArgumentException("type reference "+valueTypeName+" not found");
                        valueType = Document.class;
                        fid = ((SyntaxNode)ids.get(1)).getTokensChars();
                    }else{
                        fid = ((SyntaxNode)ids.get(0)).getTokensChars();
                        Chars typeName = type.getTokensChars().toUpperCase();
                        if(XConstants.KW_DWORD.equals(typeName))                valueType = Integer.class;
                        else if(XConstants.KW_UCHAR.equals(typeName))           valueType = Integer.class;
                        else if(XConstants.KW_BINARY.equals(typeName))          valueType = byte[].class;
                        else if(XConstants.KW_FLOAT.equals(typeName))           valueType = Float.class;
                        else if(XConstants.KW_ULONGLONG.equals(typeName))       valueType = Long.class;
                        else if(XConstants.KW_BINARY_RESOURCE.equals(typeName)) valueType = byte[].class;
                        else if(XConstants.KW_SDWORD.equals(typeName))          valueType = Integer.class;
                        else if(XConstants.KW_UNICODE.equals(typeName))         valueType = String.class;
                        else if(XConstants.KW_CHAR.equals(typeName))            valueType = Integer.class;
                        else if(XConstants.KW_STRING.equals(typeName))          valueType = String.class;
                        else if(XConstants.KW_WORD.equals(typeName))            valueType = Integer.class;
                        else if(XConstants.KW_CSTRING.equals(typeName))         valueType = String.class;
                        else if(XConstants.KW_SWORD.equals(typeName))           valueType = Integer.class;
                        else if(XConstants.KW_DOUBLE.equals(typeName))          valueType = Double.class;
                        else throw new InvalidArgumentException("Unexpeted type "+typeName);
                    }

                    fields[i] = new DefaultFieldType(fid, null, desc, minOcc, maxOcc, valueType, null, ref,null);
                }

                //constraint type
                if(constraint!=null){
                    //allow sub objects
                    fields[fields.length-1] = new DefaultFieldType(new Chars("children"), null, null, 0, Integer.MAX_VALUE, Document.class, null, null,null);
                }

                final DocumentType type = new DefaultDocumentType(uuid, id, id, false, fields, null);
                typesByName.add(id, type);
                typesByUID.add(uuid, type);
                docs.add(type);
            }else if(sn.getRule() == RULE_OBJECT){
                final Document doc = readObject(sn, typesByName);
                docs.add(doc);
            }
        }

        return docs;
    }

    private Document readObject(final SyntaxNode sn, final Dictionary typesByName){
        if(sn.getRule() != RULE_OBJECT){
            throw new InvalidArgumentException("AST node is not an object");
        }

        final Sequence ids = sn.getChildrenByToken(TOKEN_ID);
        final Chars typeName = ((SyntaxNode)ids.get(0)).getTokensChars();
        final DocumentType docType = (DocumentType) typesByName.getValue(typeName);
        final Iterator valuesIte = sn.getChildrenByRule(RULE_VALUE).createIterator();

        Document doc = readObject(docType, valuesIte, typesByName);
        return doc;
    }

    private Document readObject(final DocumentType docType, Iterator valuesIte, final Dictionary typesByName){

        final Document doc = XConstants.createDoc(docType);

        final FieldType[] fields = docType.getFields();
        for(int i=0;i<fields.length;i++){
            int maxOcc = fields[i].getMaxOccurences();

            if(maxOcc==-1){
                //dynamic size, provided in previous property
                maxOcc = (Integer)doc.getFieldValue(fields[i].getDescription().toChars());
            }

            if(maxOcc==1){
                final Object value = readSingle(fields[i], valuesIte, typesByName);
                doc.setFieldValue(fields[i].getId(), value);
            }else if(maxOcc>0 && maxOcc!=Integer.MAX_VALUE){
                //fixed size
                final Object[] fieldValues = new Object[maxOcc];
                for(int k=0;k<maxOcc;k++){
                    fieldValues[k] = readSingle(fields[i], valuesIte, typesByName);
                }
                doc.setFieldValue(fields[i].getId(), fieldValues);
            }else if(maxOcc==Integer.MAX_VALUE){
                //constraint type, sub childrens
                final Sequence children = new ArraySequence();
                while(valuesIte.hasNext()){
                    final SyntaxNode valueNode = (SyntaxNode) valuesIte.next();
                    final SyntaxNode candidate = (SyntaxNode) valueNode.getChildren().get(0);
                    if(candidate.getRule() == RULE_OBJECT){
                        final Document child = readObject(candidate, typesByName);
                        children.add(child);
                    }else if(candidate.getRule() == RULE_REF){
                        children.add(candidate.getTokensChars());
                    }else{
                        throw new UnsupportedOperationException("Unexpected type "+candidate);
                    }

                }
                if(!children.isEmpty()){
                    doc.setFieldValue(fields[i].getId(), children.toArray());
                }
            }else{
                throw new UnsupportedOperationException("Unexpected size "+maxOcc);
            }
        }

        return doc;
    }

    private Object readSingle(FieldType field, Iterator valuesIte, final Dictionary typesByName){
        final Class valueClass = field.getValueClass();
        if(Integer.class.equals(valueClass)){
            final SyntaxNode sn = (SyntaxNode) valuesIte.next();
            return Int32.decode(sn.getTokensChars());
        }else if(Float.class.equals(valueClass)){
            final SyntaxNode sn = (SyntaxNode) valuesIte.next();
            return (float)Float64.decode(sn.getTokensChars());
        }else if(Double.class.equals(valueClass)){
            final SyntaxNode sn = (SyntaxNode) valuesIte.next();
            return Float64.decode(sn.getTokensChars());
        }else if(Document.class.equals(valueClass)){
            final DocumentType refType = field.getReferenceType();
            return readObject(refType, valuesIte, typesByName);
        }else{
            throw new UnsupportedOperationException("not supported yet." + valueClass);
        }

    }

    public void dispose() throws IOException {
    }

}
