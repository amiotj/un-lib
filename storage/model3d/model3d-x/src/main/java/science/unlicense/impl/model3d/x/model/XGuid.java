
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XGuid extends XObject{

    public XGuid() {
        super(XConstants.TYPE_GUID);
    }
    
    
    
}
