
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XAnimationKey extends XObject{

    public XAnimationKey() {
        super(XConstants.TYPE_ANIMATION_KEY);
    }
    
    
    
}
