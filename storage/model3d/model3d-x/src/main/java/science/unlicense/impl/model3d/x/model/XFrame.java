
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XFrame extends XObject{

    public XFrame() {
        super(XConstants.TYPE_FRAME);
    }
    
    
    
}
