
package science.unlicense.impl.model3d.x;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.number.Int32;
import science.unlicense.api.number.NumberEncoding;
import static science.unlicense.impl.model3d.x.XConstants.*;

/**
 * DirectX binary format.
 * 
 * @author Johann Sorel
 */
public class XBinReader extends AbstractReader implements XReader{

    private boolean doublePrecision = false;

    private final Dictionary typesByName = new HashDictionary();
    private DataInputStream ds;
    private int token = -1;

    private final Sequence valueStack = new ArraySequence();

    public XBinReader() {
        //populate with default templates
        //files in binary form rarely contain the templates
        final Iterator ite = XConstants.TYPES_BYNAME_MAP.getValues().createIterator();
        while(ite.hasNext()){
            final DocumentType docType = (DocumentType) ite.next();
            typesByName.add(docType.getTitle(), docType);
        }
    }

    @Override
    public Sequence read() throws IOException {
        ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);

        final Sequence docs = new ArraySequence();
        for(;;){
            try{
                peekToken();
            }catch(EOSException ex){
                break;
            }
            if(peekToken()==TOKEN_TEMPLATE){
                final DocumentType docType = readTemplate(ds);
                System.out.println(docType);
                docs.add(docType);
            }else if(peekToken()==TOKEN_NAME){
                final Document doc = readObject();
                System.out.println(doc);
                docs.add(doc);
            }else{
                throw new IOException("Unexpected token "+peekToken());
            }
        }

        return docs;
    }

    private int peekToken() throws IOException{
        if(token==-1){
            token = ds.readUShort();
        }
        return token;
    }

    private int pullToken() throws IOException{
        peekToken();
        int t = token;
        token = -1;
        return t;
    }

    private Object pullValue() throws IOException{
        if(valueStack.isEmpty()){
            if(peekToken()==TOKEN_INTEGER_LIST){
                valueStack.addAll(readIntegerList());
            }else if(peekToken()==TOKEN_REALNUM_LIST){
                valueStack.addAll(readRealList());
            }else if(peekToken()==TOKEN_COMMA){
                pullToken();
            }else if(peekToken()==TOKEN_SEMICOLON){
                pullToken();
            }else{
                throw new IOException("Unexpected token "+peekToken());
            }
        }

        final Object c = valueStack.get(0);
        valueStack.remove(0);
        return c;
    }

    private DocumentType readTemplate(DataInputStream ds) throws IOException{
        token(TOKEN_TEMPLATE);
        final Chars name = readName();
        token(TOKEN_OBRACE);
        final Chars classId = readGuid();

        for(;;){
            int t = peekToken();
            if (   t==TOKEN_WORD
                || t==TOKEN_DWORD
                || t==TOKEN_FLOAT
                || t==TOKEN_DOUBLE
                || t==TOKEN_CHAR
                || t==TOKEN_UCHAR
                || t==TOKEN_SWORD
                || t==TOKEN_SDWORD
                || t==TOKEN_LPSTR
                || t==TOKEN_UNICODE
                || t==TOKEN_CSTRING){
                //simple type
                Class valueClass = toClass(pullToken());
                Chars fieldName = null;
                if(peekToken()==TOKEN_NAME){
                    fieldName = readName();

                }
                token(TOKEN_SEMICOLON);

            }else if(t==TOKEN_ARRAY){
                //array type
                token(TOKEN_ARRAY);
                Chars typeName = null;
                Class valueClass = null;
                if(peekToken()==TOKEN_NAME){
                    typeName = readName();
                }else{
                    valueClass = toClass(pullToken());
                }
                Chars fieldName = readName();

                final Sequence sizes = new ArraySequence();
                while(peekToken()==TOKEN_OBRACKET){
                    token(TOKEN_OBRACKET);
                    if(peekToken()==TOKEN_NAME){
                        sizes.add(readName());
                    }else{
                        sizes.add(readInteger());
                    }
                    token(TOKEN_CBRACKET);
                }
                token(TOKEN_SEMICOLON);

            }else if(t==TOKEN_NAME){
                Chars refName = readName();
                Chars optName = null;
                if(peekToken()==TOKEN_NAME){
                    optName = readName();
                }
                token(TOKEN_SEMICOLON);

            }else if(t==TOKEN_OBRACE){
                //constraints
                token(TOKEN_OBRACE);
                if(peekToken()==TOKEN_DOT){
                    //ellipsis
                    token(TOKEN_DOT);
                    token(TOKEN_DOT);
                    token(TOKEN_DOT);
                }else{
                    //restrictions
                    while(peekToken()==TOKEN_NAME){
                        Chars optName = readName();
                        Chars optClassId = readGuid();
                    }
                }
                token(TOKEN_CBRACE);

            }else if(t==TOKEN_OBRACE){
                break;
            }else{
                throw new IOException("Unexpected token "+t);
            }
        }

        token(TOKEN_CBRACE);

        throw new UnsupportedOperationException("TODO");
    }

    private Document readObject() throws IOException{
        Chars typeName = null;
        if(peekToken()==TOKEN_NAME){
            typeName = readName();
        }else{
            throw new IOException("Unexpected token "+peekToken());
        }

        final DocumentType docType = (DocumentType) typesByName.getValue(typeName);
        final Document doc = XConstants.createDoc(docType);

        Chars optName = null;
        if(peekToken()==TOKEN_NAME){
            optName = readName();
        }
        token(TOKEN_OBRACE);

        Chars classId = null;
        if(peekToken()==TOKEN_GUID){
            classId = readGuid();
        }


        final FieldType[] fields = docType.getFields();
        for(int i=0;i<fields.length;i++){
            int maxOcc = fields[i].getMaxOccurences();

            if(maxOcc==-1){
                //dynamic size, provided in previous property
                maxOcc = (Integer)doc.getFieldValue(fields[i].getDescription().toChars());
            }

            if(maxOcc==1){
                final Object value = readSingle(fields[i]);
                doc.setFieldValue(fields[i].getId(), value);
            }else if(maxOcc>0 && maxOcc!=Integer.MAX_VALUE){
                //fixed size
                final Object[] fieldValues = new Object[maxOcc];
                for(int k=0;k<maxOcc;k++){
                    fieldValues[k] = readSingle(fields[i]);
                }
                doc.setFieldValue(fields[i].getId(), fieldValues);
            }else if(maxOcc==Integer.MAX_VALUE){
                //constraint type, sub childrens
                final Sequence children = new ArraySequence();
                while(peekToken()==TOKEN_NAME){
                    Document child = readObject();
                    children.add(child);
                }
                if(!children.isEmpty()){
                    doc.setFieldValue(fields[i].getId(), children.toArray());
                }
            }else{
                throw new UnsupportedOperationException("Unexpected size "+maxOcc);
            }
        }

        token(TOKEN_CBRACE);
        return doc;
    }

    private Document readObject(DocumentType docType) throws IOException{

        final Document doc = XConstants.createDoc(docType);

        final FieldType[] fields = docType.getFields();
        for(int i=0;i<fields.length;i++){
            int maxOcc = fields[i].getMaxOccurences();

            if(maxOcc==-1){
                //dynamic size, provided in previous property
                maxOcc = (Integer)doc.getFieldValue(fields[i].getDescription().toChars());
            }

            if(maxOcc==1){
                final Object value = readSingle(fields[i]);
                doc.setFieldValue(fields[i].getId(), value);
            }else if(maxOcc>0 && maxOcc!=Integer.MAX_VALUE){
                //fixed size
                final Object[] fieldValues = new Object[maxOcc];
                for(int k=0;k<maxOcc;k++){
                    fieldValues[k] = readSingle(fields[i]);
                }
                doc.setFieldValue(fields[i].getId(), fieldValues);
            }else if(maxOcc==Integer.MAX_VALUE){
                //constraint type, sub childrens
                final Sequence children = new ArraySequence();
                while(peekToken()==TOKEN_NAME){
                    Document child = readObject();
                    children.add(child);
                }
                if(!children.isEmpty()){
                    doc.setFieldValue(fields[i].getId(), children.toArray());
                }
            }else{
                throw new UnsupportedOperationException("Unexpected size "+maxOcc);
            }
        }

        return doc;
    }

    private Object readSingle(FieldType fieldType) throws IOException{
        Class valueClass = fieldType.getValueClass();
        if(Document.class.equals(valueClass)){
            final DocumentType docType = fieldType.getReferenceType();
            return readObject(docType);
        }else{
            return pullValue();
        }
    }

    private Chars readName() throws IOException{
        token(TOKEN_NAME);
        final int size = ds.readInt();
        return ds.readBlockZeroTerminatedChars(size, CharEncodings.US_ASCII);
    }

    private Chars readString() throws IOException{
        token(TOKEN_STRING);
        final int size = ds.readInt();
        final Chars text = ds.readBlockZeroTerminatedChars(size, CharEncodings.US_ASCII);
        final int t = ds.readUShort();
        if(t != TOKEN_SEMICOLON && t!= TOKEN_COMMA){
            throw new IOException("Unexpected token "+t);
        }
        return text;
    }

    private int readInteger() throws IOException{
        token(TOKEN_INTEGER);
        return ds.readInt();
    }

    private Chars readGuid() throws IOException{
        token(TOKEN_GUID);
        final CharBuffer cb = new CharBuffer();
        cb.append(Int32.encodeHexa(ds.read(new byte[4])));
        cb.append('-');
        cb.append(Int32.encodeHexa(ds.read(new byte[2])));
        cb.append('-');
        cb.append(Int32.encodeHexa(ds.read(new byte[2])));
        cb.append('-');
        cb.append(Int32.encodeHexa(ds.read(new byte[8])));
        return cb.toChars();
    }

    private Object[] readIntegerList() throws IOException{
        token(TOKEN_INTEGER_LIST);
        final int size = ds.readInt();
        final Object[] values = new Object[size];
        for(int i=0;i<size;i++){
            values[i] = ds.readInt();
        }
        return values;
    }

    private Object[] readRealList() throws IOException{
        token(TOKEN_REALNUM_LIST);
        final int size = ds.readInt();
        final Object[] values = new Object[size];
        for(int i=0;i<size;i++){
            values[i] = doublePrecision ? ds.readDouble() : ds.readFloat();
        }
        return values;
    }

    private void token(int token) throws IOException{
        final int t = pullToken();
        if(t != token){
            throw new IOException("Unexpected token "+t);
        }
    }

    private static Class toClass(int token) throws IOException{
        switch(token){
            case TOKEN_WORD : return Integer.class;
            case TOKEN_DWORD : return Integer.class;
            case TOKEN_FLOAT : return Float.class;
            case TOKEN_DOUBLE : return Double.class;
            case TOKEN_CHAR : return Integer.class;
            case TOKEN_UCHAR : return Integer.class;
            case TOKEN_SWORD : return Integer.class;
            case TOKEN_SDWORD : return Integer.class;
            case TOKEN_LPSTR : return Integer.class;
            case TOKEN_UNICODE : return Chars.class;
            case TOKEN_CSTRING : return Chars.class;
            default : throw new IOException("Unexpected primitive type "+token);
        }
    }

}
