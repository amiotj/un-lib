
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XFloatKeys extends XObject{

    public XFloatKeys() {
        super(XConstants.TYPE_FLOAT_KEYS);
    }
    
    
    
}
