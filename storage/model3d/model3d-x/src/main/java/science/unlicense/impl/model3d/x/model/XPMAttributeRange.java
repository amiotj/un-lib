
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XPMAttributeRange extends XObject{

    public XPMAttributeRange() {
        super(XConstants.TYPE_PM_ATTRIBUTE_RANGE);
    }
    
    
    
}
