
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XIndexedColor extends XObject{

    public XIndexedColor() {
        super(XConstants.TYPE_INDEXED_COLOR);
    }
    
    
    
}
