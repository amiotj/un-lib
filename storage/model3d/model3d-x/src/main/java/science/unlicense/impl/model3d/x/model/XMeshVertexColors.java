
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XMeshVertexColors extends XObject{

    public XMeshVertexColors() {
        super(XConstants.TYPE_MESH_VERTEX_COLORS);
    }
    
    
    
}
