
package science.unlicense.impl.model3d.x;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.Reader;

/**
 *
 * @author Johann Sorel
 */
public interface XReader extends Reader {

    public Sequence read() throws IOException;
    
}
