
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XPatchMesh9 extends XObject{

    public XPatchMesh9() {
        super(XConstants.TYPE_PATCH_MESH9);
    }
    
    
    
}
