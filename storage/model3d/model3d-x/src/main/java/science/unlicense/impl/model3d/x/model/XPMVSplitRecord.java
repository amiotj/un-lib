
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XPMVSplitRecord extends XObject{

    public XPMVSplitRecord() {
        super(XConstants.TYPE_PM_SPLIT_RECORD);
    }
    
    
    
}
