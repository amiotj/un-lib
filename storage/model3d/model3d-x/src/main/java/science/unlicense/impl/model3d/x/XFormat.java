
package science.unlicense.impl.model3d.x;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.path.Path;

/**
 *
 * Reference :
 * http://msdn.microsoft.com/en-us/library/windows/desktop/bb173014(v=vs.85).aspx
 * http://msdn.microsoft.com/en-us/library/windows/desktop/bb174837(v=vs.85).aspx
 * http://paulbourke.net/dataformats/directx/
 * http://www.xbdev.net/3dformats/x/xfileformat.php
 * http://www.cgdev.net/axe/x-file.html
 * 
 * @author Johann Sorel
 */
public class XFormat extends AbstractModel3DFormat{

    public static final XFormat INSTANCE = new XFormat();

    private XFormat() {
        super(new Chars("X"),
              new Chars("DirectX"),
              new Chars("DirectX"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("x"),
                new Chars("xch")
              },
              new byte[][]{XConstants.SIGNATURE});
    }

    public Model3DStore open(Object input) throws IOException {
        return new XStore((Path)input);
    }

}
