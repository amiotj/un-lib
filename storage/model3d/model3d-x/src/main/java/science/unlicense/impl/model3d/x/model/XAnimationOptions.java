
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XAnimationOptions extends XObject{

    public XAnimationOptions() {
        super(XConstants.TYPE_ANIMATION_OPTIONS);
    }
    
    
    
}
