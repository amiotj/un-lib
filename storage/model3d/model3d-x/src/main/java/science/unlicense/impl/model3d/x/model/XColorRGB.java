
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XColorRGB extends XObject{

    public XColorRGB() {
        super(XConstants.TYPE_COLOR_RGB);
    }
    
    
    
}
