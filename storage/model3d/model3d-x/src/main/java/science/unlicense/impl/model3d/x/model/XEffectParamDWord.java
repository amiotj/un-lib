
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XEffectParamDWord extends XObject{

    public XEffectParamDWord() {
        super(XConstants.TYPE_EFFECT_PARAM_DWORD);
    }
    
    
    
}
