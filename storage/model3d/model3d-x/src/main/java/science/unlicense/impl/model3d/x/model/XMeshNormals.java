
package science.unlicense.impl.model3d.x.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.model3d.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XMeshNormals extends XObject{

    public XMeshNormals() {
        super(XConstants.TYPE_MESH_NORMALS);
    }
    
    public float[] getNormals(){
         final Object[] xnormals = (Object[]) getFieldValue(new Chars("normals"));

         float[] normals = new float[xnormals.length*3];
         for(int i=0,k=0;i<xnormals.length;i++,k+=3){
             normals[k+0] = ((Number)((XVector)xnormals[i]).getFieldValue(new Chars("x"))).floatValue();
             normals[k+1] = ((Number)((XVector)xnormals[i]).getFieldValue(new Chars("y"))).floatValue();
             normals[k+2] = ((Number)((XVector)xnormals[i]).getFieldValue(new Chars("z"))).floatValue();
         }

         return normals;
    }
    
}
