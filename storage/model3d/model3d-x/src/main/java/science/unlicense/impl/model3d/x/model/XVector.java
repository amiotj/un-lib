
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XVector extends XObject{

    public XVector() {
        super(XConstants.TYPE_VECTOR);
    }
    
    
    
}
