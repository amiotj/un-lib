
package science.unlicense.impl.model3d.x.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.model3d.x.XConstants;

/**
 *
 * @author Johann Sorel
 */
public class XMeshFace extends XObject{

    public XMeshFace() {
        super(XConstants.TYPE_MESH_FACE);
    }

    public int[] getIndices(){
        final Object[] xFaces = (Object[]) getFieldValue(new Chars("faceVertexIndices"));

        if(xFaces.length==3){
            return new int[]{
                (Integer)xFaces[0],
                (Integer)xFaces[1],
                (Integer)xFaces[2]
            };
        }else if(xFaces.length==4){
            return new int[]{
                (Integer)xFaces[0],
                (Integer)xFaces[1],
                (Integer)xFaces[2],

                (Integer)xFaces[0],
                (Integer)xFaces[2],
                (Integer)xFaces[3]
            };
        }else{
            throw new RuntimeException("Not supported yet");
        }

    }
    
}
