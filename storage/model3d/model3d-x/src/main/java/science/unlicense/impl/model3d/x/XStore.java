
package science.unlicense.impl.model3d.x;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.io.deflate.DeflateInputStream;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.model3d.x.model.XFileHeader;
import science.unlicense.impl.model3d.x.model.XFrame;
import science.unlicense.impl.model3d.x.model.XMesh;


/**
 * DirectX 3D Format store.
 * 
 * @author Johann Sorel
 */
public class XStore extends AbstractModel3DStore {


    public XStore(Object input) {
        super(XFormat.INSTANCE, input);
    }


    public Collection getElements() throws StoreException {
        final Sequence all = new ArraySequence();

        //parse fil to docs
        final Sequence docs;
        try {
            docs = read();
        } catch (IOException ex) {
            throw new StoreException(ex);
        }

        //convert to models
        for(int i=0,n=docs.getSize();i<n;i++){
            Object candidate = docs.get(i);

            if(candidate instanceof XFrame){
                final Object[] children = (Object[]) ((XFrame)candidate).getFieldValue(new Chars("children"));
                for(Object o : children){
                    if(o instanceof XMesh){
                        all.add(toMesh((XMesh) o));
                    }
                }
            }else if(candidate instanceof XMesh){
                all.add(toMesh((XMesh)candidate));
            }
        }

        return all;
    }

    private Mesh toMesh(XMesh xmesh){
        final float[] vertices = xmesh.getVertices();
        final float[] normals = xmesh.getNormals();
        final int[] faces = xmesh.getFaces();

        final Mesh mesh = new Mesh();
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices, 3));
        shell.setIndexes(new IBO(faces, 3), IndexRange.TRIANGLES(0, faces.length));
        mesh.setShape(shell);

        if(normals==null){
            shell.calculateNormals();
        }else{
            shell.setNormals(new VBO(normals, 3));
        }
        
        shell.calculateBBox();
        mesh.getMaterial().setDiffuse(Color.GRAY_NORMAL);
        return mesh;
    }

    /**
     * Parse file to DocumentType and Document.
     *
     * @return
     * @throws IOException
     */
    Sequence read() throws IOException{
        
        //read file header
        ByteInputStream in = getSourceAsInputStream();
        final XFileHeader header = new XFileHeader();
        header.read(new DataInputStream(in));

        final XReader reader;
        if(XConstants.FORMAT_TXT.equals(header.format)){
            reader = new XTxtReader();
        }else if(XConstants.FORMAT_TZIP.equals(header.format)){
            in = new DeflateInputStream(in);
            reader = new XTxtReader();
        }else if(XConstants.FORMAT_BIN.equals(header.format)){
            reader = new XBinReader();
        }else if(XConstants.FORMAT_BZIP.equals(header.format)){
            in = new DeflateInputStream(in);
            reader = new XBinReader();
        }else{
            throw new IOException("Unknowned format "+header.format);
        }
        reader.setInput(in);
        return reader.read();
    }

}
