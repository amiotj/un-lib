
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XDeclData extends XObject{

    public XDeclData() {
        super(XConstants.TYPE_DECL_DATA);
    }
    
    
    
}
