
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XTimedFloatKeys extends XObject{

    public XTimedFloatKeys() {
        super(XConstants.TYPE_TIMED_FLOATKEYS);
    }
    
    
    
}
