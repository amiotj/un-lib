
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XAnimTicksPerSecond extends XObject{

    public XAnimTicksPerSecond() {
        super(XConstants.TYPE_ANIM_TICK_PER_SECOND);
    }
    
    
    
}
