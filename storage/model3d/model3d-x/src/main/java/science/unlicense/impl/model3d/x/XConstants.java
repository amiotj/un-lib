
package science.unlicense.impl.model3d.x;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.impl.model3d.x.model.XAnimTicksPerSecond;
import science.unlicense.impl.model3d.x.model.XAnimation;
import science.unlicense.impl.model3d.x.model.XAnimationKey;
import science.unlicense.impl.model3d.x.model.XAnimationOptions;
import science.unlicense.impl.model3d.x.model.XAnimationSet;
import science.unlicense.impl.model3d.x.model.XBoolean;
import science.unlicense.impl.model3d.x.model.XBoolean2D;
import science.unlicense.impl.model3d.x.model.XColorRGB;
import science.unlicense.impl.model3d.x.model.XColorRGBA;
import science.unlicense.impl.model3d.x.model.XCompressedAnimationSet;
import science.unlicense.impl.model3d.x.model.XCoords2D;
import science.unlicense.impl.model3d.x.model.XDeclData;
import science.unlicense.impl.model3d.x.model.XEffectDWord;
import science.unlicense.impl.model3d.x.model.XEffectFloats;
import science.unlicense.impl.model3d.x.model.XEffectInstance;
import science.unlicense.impl.model3d.x.model.XEffectParamDWord;
import science.unlicense.impl.model3d.x.model.XEffectParamFloats;
import science.unlicense.impl.model3d.x.model.XEffectParamString;
import science.unlicense.impl.model3d.x.model.XEffectString;
import science.unlicense.impl.model3d.x.model.XFVFData;
import science.unlicense.impl.model3d.x.model.XFaceAdjacency;
import science.unlicense.impl.model3d.x.model.XFloatKeys;
import science.unlicense.impl.model3d.x.model.XFrame;
import science.unlicense.impl.model3d.x.model.XFrameTransformMatrix;
import science.unlicense.impl.model3d.x.model.XGuid;
import science.unlicense.impl.model3d.x.model.XHeader;
import science.unlicense.impl.model3d.x.model.XIndexedColor;
import science.unlicense.impl.model3d.x.model.XMaterial;
import science.unlicense.impl.model3d.x.model.XMaterialWraps;
import science.unlicense.impl.model3d.x.model.XMatrix4x4;
import science.unlicense.impl.model3d.x.model.XMesh;
import science.unlicense.impl.model3d.x.model.XMeshFace;
import science.unlicense.impl.model3d.x.model.XMeshFaceWraps;
import science.unlicense.impl.model3d.x.model.XMeshMaterialList;
import science.unlicense.impl.model3d.x.model.XMeshNormals;
import science.unlicense.impl.model3d.x.model.XMeshTextureCoords;
import science.unlicense.impl.model3d.x.model.XMeshVertexColors;
import science.unlicense.impl.model3d.x.model.XPMAttributeRange;
import science.unlicense.impl.model3d.x.model.XPMInfo;
import science.unlicense.impl.model3d.x.model.XPMVSplitRecord;
import science.unlicense.impl.model3d.x.model.XPatch;
import science.unlicense.impl.model3d.x.model.XPatchMesh;
import science.unlicense.impl.model3d.x.model.XPatchMesh9;
import science.unlicense.impl.model3d.x.model.XSkinMeshHeader;
import science.unlicense.impl.model3d.x.model.XSkinWeights;
import science.unlicense.impl.model3d.x.model.XTextureFileName;
import science.unlicense.impl.model3d.x.model.XTimedFloatKeys;
import science.unlicense.impl.model3d.x.model.XVector;
import science.unlicense.impl.model3d.x.model.XVertexDuplicationIndices;
import science.unlicense.impl.model3d.x.model.XVertexElement;
import science.unlicense.system.path.Paths;

/**
 * 
 * @author Johann Sorel
 */
public final class XConstants {
    
    public static final byte[] SIGNATURE = new byte[]{'x','o','f',' '};
    
    public static final Chars KW_ARRAY              = new Chars("ARRAY");
    public static final Chars KW_BINARY             = new Chars("BINARY");
    public static final Chars KW_BINARY_RESOURCE    = new Chars("BINARY_RESOURCE");
    public static final Chars KW_CHAR               = new Chars("CHAR");
    public static final Chars KW_CSTRING            = new Chars("CSTRING");
    public static final Chars KW_DOUBLE             = new Chars("DOUBLE");
    public static final Chars KW_DWORD              = new Chars("DWORD");
    public static final Chars KW_FLOAT              = new Chars("FLOAT");
    public static final Chars KW_SDWORD             = new Chars("SDWORD");
    public static final Chars KW_STRING             = new Chars("STRING");
    public static final Chars KW_SWORD              = new Chars("SWORD");
    public static final Chars KW_TEMPLATE           = new Chars("TEMPLATE");
    public static final Chars KW_UCHAR              = new Chars("UCHAR");
    public static final Chars KW_ULONGLONG          = new Chars("ULONGLONG");
    public static final Chars KW_UNICODE            = new Chars("UNICODE");
    public static final Chars KW_WORD               = new Chars("WORD");
    
    public static final Chars KW_COMMENT1           = new Chars("#");
    public static final Chars KW_COMMENT2           = new Chars("//");
    public static final Chars KW_BRACKET_START      = new Chars("{");
    public static final Chars KW_BRACKET_END        = new Chars("}");
    public static final Chars KW_ARRAY_START        = new Chars("[");
    public static final Chars KW_ARRAY_END          = new Chars("]");
    public static final Chars KW_PAR_START          = new Chars("(");
    public static final Chars KW_PAR_END            = new Chars(")");
    public static final Chars KW_ANY                = new Chars("...");
    
    public static final Chars FORMAT_TXT    = new Chars("txt ");
    public static final Chars FORMAT_BIN    = new Chars("bin ");
    public static final Chars FORMAT_TZIP   = new Chars("tzip");
    public static final Chars FORMAT_BZIP   = new Chars("bzip");

    //binary format tokens
    public static final int TOKEN_NAME          =1;
    public static final int TOKEN_STRING        =2;
    public static final int TOKEN_INTEGER       =3;
    public static final int TOKEN_GUID          =5;
    public static final int TOKEN_INTEGER_LIST  =6;
    public static final int TOKEN_REALNUM_LIST  =7;
    
    public static final int TOKEN_OBRACE        =10;
    public static final int TOKEN_CBRACE        =11;
    public static final int TOKEN_OPAREN        =12;
    public static final int TOKEN_CPAREN        =13;
    public static final int TOKEN_OBRACKET      =14;
    public static final int TOKEN_CBRACKET      =15;
    public static final int TOKEN_OANGLE        =16;
    public static final int TOKEN_CANGLE        =17;
    public static final int TOKEN_DOT           =18;
    public static final int TOKEN_COMMA         =19;
    public static final int TOKEN_SEMICOLON     =20;
    public static final int TOKEN_TEMPLATE      =31;
    public static final int TOKEN_WORD          =40;
    public static final int TOKEN_DWORD         =41;
    public static final int TOKEN_FLOAT         =42;
    public static final int TOKEN_DOUBLE        =43;
    public static final int TOKEN_CHAR          =44;
    public static final int TOKEN_UCHAR         =45;
    public static final int TOKEN_SWORD         =46;
    public static final int TOKEN_SDWORD        =47;
    public static final int TOKEN_VOID          =48;
    public static final int TOKEN_LPSTR         =49;
    public static final int TOKEN_UNICODE       =50;
    public static final int TOKEN_CSTRING       =51;
    public static final int TOKEN_ARRAY         =52;

    public static final DocumentType TYPE_HEADER;
    public static final DocumentType TYPE_ANIMATION;
    public static final DocumentType TYPE_ANIMATION_KEY;
    public static final DocumentType TYPE_ANIMATION_OPTIONS;
    public static final DocumentType TYPE_ANIMATION_SET;
    public static final DocumentType TYPE_ANIM_TICK_PER_SECOND;
    public static final DocumentType TYPE_BOOLEAN;
    public static final DocumentType TYPE_BOOLEAN2D;
    public static final DocumentType TYPE_COLOR_RGB;
    public static final DocumentType TYPE_COLOR_RGBA;
    public static final DocumentType TYPE_COMPRESSED_ANIMATION_SET;
    public static final DocumentType TYPE_COORDS2D;
    public static final DocumentType TYPE_DECL_DATA;
    public static final DocumentType TYPE_EFFECT_DWORD;
    public static final DocumentType TYPE_EFFECT_FLOATS;
    public static final DocumentType TYPE_EFFECT_INSTANCE;
    public static final DocumentType TYPE_EFFECT_PARAM_DWORD;
    public static final DocumentType TYPE_EFFECT_PARAM_FLOATS;
    public static final DocumentType TYPE_EFFECT_PARAM_STRING;
    public static final DocumentType TYPE_EFFECT_STRING;
    public static final DocumentType TYPE_FACE_ADJACENCY;
    public static final DocumentType TYPE_FLOAT_KEYS;
    public static final DocumentType TYPE_FRAME;
    public static final DocumentType TYPE_FRAME_TRANSFORM_MATRIX;
    public static final DocumentType TYPE_FVFDATA;
    public static final DocumentType TYPE_GUID;
    public static final DocumentType TYPE_INDEXED_COLOR;
    public static final DocumentType TYPE_MATERIAL;
    public static final DocumentType TYPE_MATERIAL_WRAP;
    public static final DocumentType TYPE_MATRIX_4X4;
    public static final DocumentType TYPE_MESH;
    public static final DocumentType TYPE_MESH_FACE;
    public static final DocumentType TYPE_MESH_FACE_WRAPS;
    public static final DocumentType TYPE_MESH_MATERIAL_LIST;
    public static final DocumentType TYPE_MESH_NORMALS;
    public static final DocumentType TYPE_MESH_TEXTURE_COORDS;
    public static final DocumentType TYPE_MESH_VERTEX_COLORS;
    public static final DocumentType TYPE_PATCH;
    public static final DocumentType TYPE_PATCH_MESH;
    public static final DocumentType TYPE_PATCH_MESH9;
    public static final DocumentType TYPE_PM_ATTRIBUTE_RANGE;
    public static final DocumentType TYPE_PM_INFO;
    public static final DocumentType TYPE_PM_SPLIT_RECORD;
    public static final DocumentType TYPE_SKIN_WEIGHTS;
    public static final DocumentType TYPE_TEXTURE_FILENAME;
    public static final DocumentType TYPE_TIMED_FLOATKEYS;
    public static final DocumentType TYPE_VECTOR;
    public static final DocumentType TYPE_VERTEX_DUPLICATION_INDICES;
    public static final DocumentType TYPE_VERTEX_ELEMENT;
    public static final DocumentType TYPE_XSKIN_MESH_HEADER;
    public static final Dictionary TYPES_BYNAME_MAP = new HashDictionary();
    public static final Dictionary TYPES_CLASS_MAP = new HashDictionary();
    static {
        final XStore store = new XStore(Paths.resolve(new Chars("mod:/un/storage/model3d/x/templates.x")));
        try {
            final Sequence types = store.read();
            int i=0;
            TYPE_HEADER                   = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_HEADER                    .getTitle(),TYPE_HEADER);
            TYPE_ANIMATION                = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_ANIMATION                 .getTitle(),TYPE_ANIMATION);
            TYPE_FLOAT_KEYS               = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_FLOAT_KEYS                .getTitle(),TYPE_FLOAT_KEYS);
            TYPE_TIMED_FLOATKEYS          = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_TIMED_FLOATKEYS           .getTitle(),TYPE_TIMED_FLOATKEYS);
            TYPE_ANIMATION_KEY            = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_ANIMATION_KEY             .getTitle(),TYPE_ANIMATION_KEY);
            TYPE_ANIMATION_OPTIONS        = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_ANIMATION_OPTIONS         .getTitle(),TYPE_ANIMATION_OPTIONS);
            TYPE_ANIMATION_SET            = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_ANIMATION_SET             .getTitle(),TYPE_ANIMATION_SET);
            TYPE_ANIM_TICK_PER_SECOND     = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_ANIM_TICK_PER_SECOND      .getTitle(),TYPE_ANIM_TICK_PER_SECOND);
            TYPE_BOOLEAN                  = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_BOOLEAN                   .getTitle(),TYPE_BOOLEAN);
            TYPE_BOOLEAN2D                = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_BOOLEAN2D                 .getTitle(),TYPE_BOOLEAN2D);
            TYPE_COLOR_RGB                = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_COLOR_RGB                 .getTitle(),TYPE_COLOR_RGB);
            TYPE_COLOR_RGBA               = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_COLOR_RGBA                .getTitle(),TYPE_COLOR_RGBA);
            TYPE_COMPRESSED_ANIMATION_SET = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_COMPRESSED_ANIMATION_SET  .getTitle(),TYPE_COMPRESSED_ANIMATION_SET);
            TYPE_COORDS2D                 = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_COORDS2D                  .getTitle(),TYPE_COORDS2D);
            TYPE_VERTEX_ELEMENT           = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_VERTEX_ELEMENT            .getTitle(),TYPE_VERTEX_ELEMENT);
            TYPE_DECL_DATA                = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_DECL_DATA                 .getTitle(),TYPE_DECL_DATA);
            TYPE_EFFECT_DWORD             = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_DWORD              .getTitle(),TYPE_EFFECT_DWORD);
            TYPE_EFFECT_FLOATS            = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_FLOATS             .getTitle(),TYPE_EFFECT_FLOATS);
            TYPE_EFFECT_INSTANCE          = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_INSTANCE           .getTitle(),TYPE_EFFECT_INSTANCE);
            TYPE_EFFECT_PARAM_DWORD       = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_PARAM_DWORD        .getTitle(),TYPE_EFFECT_PARAM_DWORD);
            TYPE_EFFECT_PARAM_FLOATS      = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_PARAM_FLOATS       .getTitle(),TYPE_EFFECT_PARAM_FLOATS);
            TYPE_EFFECT_PARAM_STRING      = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_PARAM_STRING       .getTitle(),TYPE_EFFECT_PARAM_STRING);
            TYPE_EFFECT_STRING            = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_EFFECT_STRING             .getTitle(),TYPE_EFFECT_STRING);
            TYPE_FACE_ADJACENCY           = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_FACE_ADJACENCY            .getTitle(),TYPE_FACE_ADJACENCY);
            TYPE_FRAME                    = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_FRAME                     .getTitle(),TYPE_FRAME);
            TYPE_MATRIX_4X4               = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MATRIX_4X4                .getTitle(),TYPE_MATRIX_4X4);
            TYPE_FRAME_TRANSFORM_MATRIX   = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_FRAME_TRANSFORM_MATRIX    .getTitle(),TYPE_FRAME_TRANSFORM_MATRIX);
            TYPE_FVFDATA                  = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_FVFDATA                   .getTitle(),TYPE_FVFDATA);
            TYPE_GUID                     = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_GUID                      .getTitle(),TYPE_GUID);
            TYPE_INDEXED_COLOR            = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_INDEXED_COLOR             .getTitle(),TYPE_INDEXED_COLOR);
            TYPE_MATERIAL                 = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MATERIAL                  .getTitle(),TYPE_MATERIAL);
            TYPE_MATERIAL_WRAP            = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MATERIAL_WRAP             .getTitle(),TYPE_MATERIAL_WRAP);
            TYPE_VECTOR                   = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_VECTOR                    .getTitle(),TYPE_VECTOR);
            TYPE_MESH_FACE                = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH_FACE                 .getTitle(),TYPE_MESH_FACE);
            TYPE_MESH                     = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH                      .getTitle(),TYPE_MESH);
            TYPE_MESH_FACE_WRAPS          = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH_FACE_WRAPS           .getTitle(),TYPE_MESH_FACE_WRAPS);
            TYPE_MESH_MATERIAL_LIST       = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH_MATERIAL_LIST        .getTitle(),TYPE_MESH_MATERIAL_LIST);
            TYPE_MESH_NORMALS             = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH_NORMALS              .getTitle(),TYPE_MESH_NORMALS);
            TYPE_MESH_TEXTURE_COORDS      = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH_TEXTURE_COORDS       .getTitle(),TYPE_MESH_TEXTURE_COORDS);
            TYPE_MESH_VERTEX_COLORS       = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_MESH_VERTEX_COLORS        .getTitle(),TYPE_MESH_VERTEX_COLORS);
            TYPE_PATCH                    = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_PATCH                     .getTitle(),TYPE_PATCH);
            TYPE_PATCH_MESH               = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_PATCH_MESH                .getTitle(),TYPE_PATCH_MESH);
            TYPE_PATCH_MESH9              = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_PATCH_MESH9               .getTitle(),TYPE_PATCH_MESH9);
            TYPE_PM_ATTRIBUTE_RANGE       = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_PM_ATTRIBUTE_RANGE        .getTitle(),TYPE_PM_ATTRIBUTE_RANGE);
            TYPE_PM_SPLIT_RECORD          = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_PM_SPLIT_RECORD           .getTitle(),TYPE_PM_SPLIT_RECORD);
            TYPE_PM_INFO                  = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_PM_INFO                   .getTitle(),TYPE_PM_INFO);
            TYPE_SKIN_WEIGHTS             = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_SKIN_WEIGHTS              .getTitle(),TYPE_SKIN_WEIGHTS);
            TYPE_TEXTURE_FILENAME         = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_TEXTURE_FILENAME          .getTitle(),TYPE_TEXTURE_FILENAME);
            TYPE_VERTEX_DUPLICATION_INDICES=(DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_VERTEX_DUPLICATION_INDICES.getTitle(),TYPE_VERTEX_DUPLICATION_INDICES);
            TYPE_XSKIN_MESH_HEADER        = (DocumentType)types.get(i++); TYPES_BYNAME_MAP.add(TYPE_XSKIN_MESH_HEADER         .getTitle(),TYPE_XSKIN_MESH_HEADER);



            TYPES_CLASS_MAP.add(TYPE_HEADER                   .getId().toUpperCase(),XHeader.class);
            TYPES_CLASS_MAP.add(TYPE_ANIMATION                .getId().toUpperCase(),XAnimation.class);
            TYPES_CLASS_MAP.add(TYPE_FLOAT_KEYS               .getId().toUpperCase(),XFloatKeys.class);
            TYPES_CLASS_MAP.add(TYPE_TIMED_FLOATKEYS          .getId().toUpperCase(),XTimedFloatKeys.class);
            TYPES_CLASS_MAP.add(TYPE_ANIMATION_KEY            .getId().toUpperCase(),XAnimationKey.class);
            TYPES_CLASS_MAP.add(TYPE_ANIMATION_OPTIONS        .getId().toUpperCase(),XAnimationOptions.class);
            TYPES_CLASS_MAP.add(TYPE_ANIMATION_SET            .getId().toUpperCase(),XAnimationSet.class);
            TYPES_CLASS_MAP.add(TYPE_ANIM_TICK_PER_SECOND     .getId().toUpperCase(),XAnimTicksPerSecond.class);
            TYPES_CLASS_MAP.add(TYPE_BOOLEAN                  .getId().toUpperCase(),XBoolean.class);
            TYPES_CLASS_MAP.add(TYPE_BOOLEAN2D                .getId().toUpperCase(),XBoolean2D.class);
            TYPES_CLASS_MAP.add(TYPE_COLOR_RGB                .getId().toUpperCase(),XColorRGB.class);
            TYPES_CLASS_MAP.add(TYPE_COLOR_RGBA               .getId().toUpperCase(),XColorRGBA.class);
            TYPES_CLASS_MAP.add(TYPE_COMPRESSED_ANIMATION_SET .getId().toUpperCase(),XCompressedAnimationSet.class);
            TYPES_CLASS_MAP.add(TYPE_COORDS2D                 .getId().toUpperCase(),XCoords2D.class);
            TYPES_CLASS_MAP.add(TYPE_VERTEX_ELEMENT           .getId().toUpperCase(),XVertexElement.class);
            TYPES_CLASS_MAP.add(TYPE_DECL_DATA                .getId().toUpperCase(),XDeclData.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_DWORD             .getId().toUpperCase(),XEffectDWord.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_FLOATS            .getId().toUpperCase(),XEffectFloats.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_INSTANCE          .getId().toUpperCase(),XEffectInstance.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_PARAM_DWORD       .getId().toUpperCase(),XEffectParamDWord.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_PARAM_FLOATS      .getId().toUpperCase(),XEffectParamFloats.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_PARAM_STRING      .getId().toUpperCase(),XEffectParamString.class);
            TYPES_CLASS_MAP.add(TYPE_EFFECT_STRING            .getId().toUpperCase(),XEffectString.class);
            TYPES_CLASS_MAP.add(TYPE_FACE_ADJACENCY           .getId().toUpperCase(),XFaceAdjacency.class);
            TYPES_CLASS_MAP.add(TYPE_FRAME                    .getId().toUpperCase(),XFrame.class);
            TYPES_CLASS_MAP.add(TYPE_MATRIX_4X4               .getId().toUpperCase(),XMatrix4x4.class);
            TYPES_CLASS_MAP.add(TYPE_FRAME_TRANSFORM_MATRIX   .getId().toUpperCase(),XFrameTransformMatrix.class);
            TYPES_CLASS_MAP.add(TYPE_FVFDATA                  .getId().toUpperCase(),XFVFData.class);
            TYPES_CLASS_MAP.add(TYPE_GUID                     .getId().toUpperCase(),XGuid.class);
            TYPES_CLASS_MAP.add(TYPE_INDEXED_COLOR            .getId().toUpperCase(),XIndexedColor.class);
            TYPES_CLASS_MAP.add(TYPE_MATERIAL                 .getId().toUpperCase(),XMaterial.class);
            TYPES_CLASS_MAP.add(TYPE_MATERIAL_WRAP            .getId().toUpperCase(),XMaterialWraps.class);
            TYPES_CLASS_MAP.add(TYPE_VECTOR                   .getId().toUpperCase(),XVector.class);
            TYPES_CLASS_MAP.add(TYPE_MESH_FACE                .getId().toUpperCase(),XMeshFace.class);
            TYPES_CLASS_MAP.add(TYPE_MESH                     .getId().toUpperCase(),XMesh.class);
            TYPES_CLASS_MAP.add(TYPE_MESH_FACE_WRAPS          .getId().toUpperCase(),XMeshFaceWraps.class);
            TYPES_CLASS_MAP.add(TYPE_MESH_MATERIAL_LIST       .getId().toUpperCase(),XMeshMaterialList.class);
            TYPES_CLASS_MAP.add(TYPE_MESH_NORMALS             .getId().toUpperCase(),XMeshNormals.class);
            TYPES_CLASS_MAP.add(TYPE_MESH_TEXTURE_COORDS      .getId().toUpperCase(),XMeshTextureCoords.class);
            TYPES_CLASS_MAP.add(TYPE_MESH_VERTEX_COLORS       .getId().toUpperCase(),XMeshVertexColors.class);
            TYPES_CLASS_MAP.add(TYPE_PATCH                    .getId().toUpperCase(),XPatch.class);
            TYPES_CLASS_MAP.add(TYPE_PATCH_MESH               .getId().toUpperCase(),XPatchMesh.class);
            TYPES_CLASS_MAP.add(TYPE_PATCH_MESH9              .getId().toUpperCase(),XPatchMesh9.class);
            TYPES_CLASS_MAP.add(TYPE_PM_ATTRIBUTE_RANGE       .getId().toUpperCase(),XPMAttributeRange.class);
            TYPES_CLASS_MAP.add(TYPE_PM_SPLIT_RECORD          .getId().toUpperCase(),XPMVSplitRecord.class);
            TYPES_CLASS_MAP.add(TYPE_PM_INFO                  .getId().toUpperCase(),XPMInfo.class);
            TYPES_CLASS_MAP.add(TYPE_SKIN_WEIGHTS             .getId().toUpperCase(),XSkinWeights.class);
            TYPES_CLASS_MAP.add(TYPE_TEXTURE_FILENAME         .getId().toUpperCase(),XTextureFileName.class);
            TYPES_CLASS_MAP.add(TYPE_VERTEX_DUPLICATION_INDICES.getId().toUpperCase(),XVertexDuplicationIndices.class);
            TYPES_CLASS_MAP.add(TYPE_XSKIN_MESH_HEADER        .getId().toUpperCase(),XSkinMeshHeader.class);

        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }
    
    private XConstants(){}

    public static Document createDoc(DocumentType docType){
        final Class clazz = (Class) TYPES_CLASS_MAP.getValue(docType.getId().toUpperCase());
        if(clazz!=null){
            try {
                return (Document) clazz.newInstance();
            } catch (InstantiationException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }
        return new DefaultDocument(true, docType);
    }
    
    
}
