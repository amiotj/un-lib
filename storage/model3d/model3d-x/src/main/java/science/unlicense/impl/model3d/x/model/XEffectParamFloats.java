
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XEffectParamFloats extends XObject{

    public XEffectParamFloats() {
        super(XConstants.TYPE_EFFECT_PARAM_FLOATS);
    }
    
    
    
}
