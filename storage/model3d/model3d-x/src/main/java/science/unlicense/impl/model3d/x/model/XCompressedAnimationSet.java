
package science.unlicense.impl.model3d.x.model;

import science.unlicense.impl.model3d.x.XConstants;
import science.unlicense.impl.model3d.x.model.XObject;

/**
 *
 * @author Johann Sorel
 */
public class XCompressedAnimationSet extends XObject{

    public XCompressedAnimationSet() {
        super(XConstants.TYPE_COMPRESSED_ANIMATION_SET);
    }
    
    
    
}
