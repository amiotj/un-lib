
package science.unlicense.impl.model3d.x;

import science.unlicense.impl.model3d.x.XStore;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.store.StoreException;
import science.unlicense.system.path.Paths;

/**
 * 
 * @author Johann Sorel
 */
public class XReaderTest {

    @Test
    public void readTest() throws StoreException{
        
        final XStore store = new XStore(Paths.resolve(new Chars("mod:/un/storage/model3d/x/testFile.x")));
        
        final Collection res = store.getElements();

    }
    
}
