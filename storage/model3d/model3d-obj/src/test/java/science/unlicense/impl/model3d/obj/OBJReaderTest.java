
package science.unlicense.impl.model3d.obj;

import science.unlicense.impl.model3d.obj.OBJStore;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class OBJReaderTest {

    @Ignore
    @Test
    public void readTest() throws Exception{

        final Path path = Paths.resolve(new Chars("mod:/un/storage/model3d/obj/cube.obj"));
        
        final OBJStore store = new OBJStore(path);
        
        final Collection elements = store.getElements();
        Assert.assertEquals(1, elements.getSize());
        
        final Object obj = elements.createIterator().next();
        Assert.assertTrue(obj instanceof MultipartMesh);        
        final MultipartMesh mpm = (MultipartMesh) obj;
        
        Assert.assertEquals(1, mpm.getChildren().getSize());
        final Mesh mesh = (Mesh) mpm.getChildren().get(0);
        
        final Shell shell = (Shell) mesh.getShape();
        final float[] vertices = (float[])shell.getVertices().getPrimitiveBuffer().toFloatArray();
        final float[] normals = (float[])shell.getNormals().getPrimitiveBuffer().toFloatArray();
        final float[] uvs = (float[])shell.getUVs().getPrimitiveBuffer().toFloatArray();
        final IndexRange[] ranges = shell.getModes();
        final int[] indices = shell.getIndexes().getPrimitiveBuffer().toIntArray();
        
        Assert.assertArrayEquals(new float[]{
//             1,-1, 0,     1,-1, 2,    -1,-1, 2, //1,2,3
//             1,-1, 0,    -1,-1, 2,    -1,-1, 0, //1,3,4
//             
//             1, 1, 0,    -1, 1, 0,    -1, 1, 2, //5,8,7
//             1, 1, 0,    -1, 1, 2,     1, 1, 2, //5,7,6
//             
//             1,-1, 0,     1, 1, 0,     1, 1, 2, //1,5,6
//             1,-1, 0,     1, 1, 2,     1,-1, 2, //1,6,2
//             
//             1,-1, 2,     1, 1, 2,    -1, 1, 2, //2,6,7
//             1,-1, 2,    -1, 1, 2,    -1,-1, 2, //2,7,3
//             
//            -1,-1, 2,    -1, 1, 2,    -1, 1, 0, //3,7,8
//            -1,-1, 2,    -1, 1, 0,    -1,-1, 0, //3,8,4
//             
//             1, 1, 0,     1,-1, 0,    -1,-1, 0, //5,1,4
//             1, 1, 0,    -1,-1, 0,    -1, 1, 0  //5,4,8
            
            //new approach use less elements
             1,-1, 0,     1,-1, 2,    -1,-1, 2,    -1,-1, 0, //1,2,3,4             
             1, 1, 0,    -1, 1, 0,    -1, 1, 2,     1, 1, 2, //5,8,7,6
             1,-1, 0,     1, 1, 0,     1, 1, 2,     1,-1, 2, //1,5,6,2
             1,-1, 2,     1, 1, 2,    -1, 1, 2,    -1,-1, 2, //2,6,7,3
            -1,-1, 2,    -1, 1, 2,    -1, 1, 0,    -1,-1, 0, //3,7,8,4
             1, 1, 0,     1,-1, 0,    -1,-1, 0,    -1, 1, 0  //5,1,4,8
                }, 
                vertices, 0.00001f);
        Assert.assertArrayEquals(new int[]{
//             0, 1, 2,
//             3, 4, 5,
//             6, 7, 8,
//             9,10,11,
//            12,13,14,
//            15,16,17,
//            18,19,20,
//            21,22,23,
//            24,25,26,
//            27,28,29,
//            30,31,32,
//            33,34,35
            //new approach use less elements
              0,  1,  2, 
              0,  2,  3, 
              4,  5,  6, 
              4,  6,  7, 
              8,  9, 10, 
              8, 10, 11, 
             12, 13, 14, 
             12, 14, 15, 
             16, 17, 18, 
             16, 18, 19,
             20, 21, 22, 
             20, 22, 23
            
                }, 
                indices);
        

    }

}