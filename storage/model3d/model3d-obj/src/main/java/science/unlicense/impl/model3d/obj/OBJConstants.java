
package science.unlicense.impl.model3d.obj;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class OBJConstants {
    
    public static final Chars COMMENT = new Chars(new byte[]{'#'});
    
    //vertex data
    public static final Chars VERTEX_COORD      = new Chars(new byte[]{'v'});
    public static final Chars VERTEX_NORMAL     = new Chars(new byte[]{'v','n'});
    public static final Chars VERTEX_TEXTURE    = new Chars(new byte[]{'v','t'});
    public static final Chars VERTEX_PARAMETER  = new Chars(new byte[]{'v','p'});
    public static final Chars CURVE_SURFACE_TYPE= new Chars(new byte[]{'c','s','t','y','p','e'});
    public static final Chars DEGREE            = new Chars(new byte[]{'d','e','g'});
    public static final Chars BASIC_MATRIX      = new Chars(new byte[]{'b','m','a','t'});
    public static final Chars STEP              = new Chars(new byte[]{'s','t','e','p'});
    
    //elements
    public static final Chars POINT     = new Chars(new byte[]{'p'});
    public static final Chars LINE      = new Chars(new byte[]{'l'});
    public static final Chars FACE      = new Chars(new byte[]{'f'});
    public static final Chars CURVE     = new Chars(new byte[]{'c','u','r','v'});
    public static final Chars CURVE2D   = new Chars(new byte[]{'c','u','r','v','2'});
    public static final Chars SURFACE   = new Chars(new byte[]{'s','u','r','f'});
    
    public static final Chars ON        = new Chars(new byte[]{'o','n'});
    public static final Chars OFF       = new Chars(new byte[]{'o','f','f'});
    
    //Free-form curve/surface body statements
    /** parameter values */
    public static final Chars PARAMETER     = new Chars(new byte[]{'p','a','r','m'});
    /** outer trimming loop */
    public static final Chars TRIM          = new Chars(new byte[]{'t','r','i','m'});
    /** inner trimming loop */
    public static final Chars HOLE          = new Chars(new byte[]{'h','o','l','e'});
    /** special curve */
    public static final Chars SPECIAL_CURVE = new Chars(new byte[]{'s','c','r','v'});
    /** special point */
    public static final Chars SPECIAL_POINT = new Chars(new byte[]{'s','p'});
    /** end statement */
    public static final Chars END           = new Chars(new byte[]{'e','n','d'});

    //Connectivity between free-form surfaces
    /** connect */
    public static final Chars CONNECT = new Chars(new byte[]{'c','o','n'});

    //Grouping
    /** group name */
    public static final Chars GROUP             = new Chars(new byte[]{'g'});
    /** smoothing group */
    public static final Chars SMOOTHING_GROUP   = new Chars(new byte[]{'s'});
    /** merging group */
    public static final Chars MERGING_GROUP     = new Chars(new byte[]{'m','g'});
    /** object name */
    public static final Chars OBJECT            = new Chars(new byte[]{'o'});

    //Display/render attributes
    /**  bevel interpolation */
    public static final Chars INTERP_BEVEL      = new Chars(new byte[]{'b','e','v','e','l'});
    /**  color interpolation */
    public static final Chars INTERP_COLOR      = new Chars(new byte[]{'c','_','i','n','t','e','r','p'});
    /**  dissolve interpolation */
    public static final Chars INTERP_DISSOLVE   = new Chars(new byte[]{'d','_','i','n','t','e','r','p'});
    /**  level of detail */
    public static final Chars LOD               = new Chars(new byte[]{'l','o','d'});
    /**  material name */
    public static final Chars MTL_NAME          = new Chars(new byte[]{'u','s','e','m','t','l'});
    /**  material library */
    public static final Chars MTL_LIB           = new Chars(new byte[]{'m','t','l','l','i','b'});
    /**  shadow casting */
    public static final Chars SHADOW            = new Chars(new byte[]{'s','h','a','d','o','w','_','o','b','j'});
    /**  ray tracing */
    public static final Chars RAYTRACE          = new Chars(new byte[]{'t','r','a','c','e','_','o','b','j'});
    /**  curve approximation technique */
    public static final Chars APP_CURVE         = new Chars(new byte[]{'c','t','e','c','h'});
    /**  surface approximation technique */
    public static final Chars APP_SURF          = new Chars(new byte[]{'s','t','e','c','h'});
    
    
    private OBJConstants(){}
    
}