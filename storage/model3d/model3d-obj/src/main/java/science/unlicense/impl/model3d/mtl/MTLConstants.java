package science.unlicense.impl.model3d.mtl;

import science.unlicense.api.character.Chars;

/**
 * MTL file constants.
 *
 * @author Johann Sorel
 */
public final class MTLConstants {

    public static final Chars COMMENT = new Chars(new byte[]{'#'});
    
    public static final Chars NEW = new Chars(new byte[]{'n','e','w','m','t','l'});
    public static final Chars COLOR_AMBIANT = new Chars(new byte[]{'K','a'});
    public static final Chars COLOR_DIFFUSE = new Chars(new byte[]{'K','d'});
    public static final Chars COLOR_SPECULAR = new Chars(new byte[]{'K','s'});
    
    /**
     * Illumination types :
     * 0 - use color, no ambiant
     * 1 - use color, with ambiant
     * 2 - use lights
     * 3 - ?
     * 4 - ?
     * 5 - ?
     * 6 - ?
     * 7 - ?
     * 8 - ?
     * 9 - ?
     * 10- ?
     */
    public static final Chars ILLUMINATION = new Chars(new byte[]{'i','l','l','u','m'});
    /** Specular coefficient of diffuse color */
    public static final Chars DIFFUSE_COEFF = new Chars(new byte[]{'N','s'});
    public static final Chars Ni = new Chars(new byte[]{'N','i'});
    
    /** 'd' and 'Tr' are both the same thing */
    public static final Chars DISOLVE = new Chars(new byte[]{'d'});
    public static final Chars TRANSPARENCY = new Chars(new byte[]{'T','r'});
    
    public static final Chars SHARPNESS = new Chars(new byte[]{'s','h','a','r','p','n','e','s','s'});
    
    public static final Chars MAP_AMBIANT = new Chars(new byte[]{'m','a','p','_','K','a'});
    public static final Chars MAP_DIFFUSE = new Chars(new byte[]{'m','a','p','_','K','d'});
    public static final Chars MAP_SPECULAR = new Chars(new byte[]{'m','a','p','_','K','s'});
    public static final Chars MAP_ALPHA = new Chars(new byte[]{'m','a','p','_','d'});
    /** displacement map */
    public static final Chars DISP = new Chars(new byte[]{'d','i','s','p'});
    /** stencil decal ? */
    public static final Chars DECAL = new Chars(new byte[]{'d','e','c','a','l'});
    
    /** 'bump' or 'map_bump' for bump paiing */
    public static final Chars BUMP = new Chars(new byte[]{'b','u','m','p'});
    public static final Chars MAP_BUMP = new Chars(new byte[]{'m','a','p','_','b','u','m','p'});

    private MTLConstants() {}
    
}
