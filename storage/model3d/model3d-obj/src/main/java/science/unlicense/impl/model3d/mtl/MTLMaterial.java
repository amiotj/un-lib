
package science.unlicense.impl.model3d.mtl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Float64;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class MTLMaterial {
    
    private Chars name;
    private Path basePath;
    private final Dictionary properties = new HashDictionary();

    public MTLMaterial() {
    }

    public MTLMaterial(Chars name) {
        this.name = name;
    }
    
    public Chars getName() {
        return name;
    }

    public Path getBasePath() {
        return basePath;
    }

    public void setBasePath(Path basePath) {
        this.basePath = basePath;
    }

    public Dictionary getProperties() {
        return properties;
    }
        
    /**
     * Ambiant color : Ka
     * @return Color, may be null
     */
    public Color getAmbiantColor(){
        return asColor(MTLConstants.COLOR_AMBIANT);
    }
    
    /**
     * Diffuse color : Kd
     * @return Color, may be null
     */
    public Color getDiffuseColor(){
        return asColor(MTLConstants.COLOR_DIFFUSE);
    }
    
    /**
     * Specular color : Ks
     * @return Color, may be null
     */
    public Color getSpecularColor(){
        return asColor(MTLConstants.COLOR_SPECULAR);
    }
    
    /**
     * Ambiant texture map : map_Ka
     * @return Path, may be null
     */
    public Path getAmbiantMap(){
        return asPath(MTLConstants.MAP_AMBIANT);
    }
    
    /**
     * Diffuse texture map : map_Kd
     * @return Path, may be null
     */
    public Path getDiffuseMap(){
        return asPath(MTLConstants.MAP_DIFFUSE);
    }
    
    /**
     * Specular texture map : map_Ks
     * @return Path, may be null
     */
    public Path getSpecularMap(){
        return asPath(MTLConstants.MAP_SPECULAR);
    }
    
    public Material createMaterial() throws StoreException{
        return createMaterial(null);
    }
    
    public Material createMaterial(Dictionary textureCache) throws StoreException{
        final Material material = new Material();
        final Path mapDiffuse = getDiffuseMap();
        if(mapDiffuse!=null){
            Texture2D texture = null;
            if(textureCache!=null){
                texture = (Texture2D) textureCache.getValue(mapDiffuse);
            }
            if(texture==null){
                try {
                    System.out.println("Loading texture : "+mapDiffuse);
                    final Image image = Images.read(mapDiffuse);
                    texture = new Texture2D(image);
                    if(textureCache!=null){textureCache.add(mapDiffuse, texture);}
                    final UVMapping mapping = new UVMapping(texture);
                    material.putOrReplaceLayer(new Layer(mapping,Layer.TYPE_DIFFUSE));
                } catch (IOException ex) {
                    throw new StoreException(ex);
                }
            }else{
                final UVMapping mapping = new UVMapping(texture);
                material.putOrReplaceLayer(new Layer(mapping,Layer.TYPE_DIFFUSE));
            }
        }else{
            Color colorDiffuse = getDiffuseColor();
            if(colorDiffuse!=null){
                material.putOrReplaceLayer(new Layer(new PlainColorMapping(colorDiffuse), Layer.TYPE_DIFFUSE));
            }
        }
        
        return material;
    }
    
    private Color asColor(Chars property){
        final Chars val = (Chars) properties.getValue(property);
        if(val==null) return null;
        final Chars[] rgb = val.split(' ');
        return new Color((float)Float64.decode(rgb[0]), (float)Float64.decode(rgb[1]), (float)Float64.decode(rgb[2]));
    }
    
    private Path asPath(Chars property){
        final Chars path = (Chars) properties.getValue(property);
        if(path==null) return null;
        return basePath.resolve(path);
    }
    
}
