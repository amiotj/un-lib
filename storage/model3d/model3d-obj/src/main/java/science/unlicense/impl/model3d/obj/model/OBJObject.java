
package science.unlicense.impl.model3d.obj.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class OBJObject {
    
    public Chars name;
    public Sequence groups = new ArraySequence();
    
    public OBJObject() {
    }

    public OBJObject(final Chars name) {
        this.name = name;
    }
    
}
