
package science.unlicense.impl.model3d.mtl;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.store.AbstractStore;
import static science.unlicense.impl.model3d.mtl.MTLConstants.COMMENT;
import static science.unlicense.impl.model3d.mtl.MTLConstants.NEW;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;

/**
 * MTL library of materials.
 * 
 * Resource : 
 * http://en.wikipedia.org/wiki/Wavefront_.obj_file
 * http://paulbourke.net/dataformats/mtl/
 * 
 * @author Johann Sorel
 */
public class MTLStore extends AbstractStore{
    
    private final Path basePath;
    
    private boolean readed = false;
    
    /** sequence of MTLMaterial */
    private final Dictionary materials = new HashDictionary();

    public MTLStore(Path path) {
        super(null, path);
        this.basePath = path.getParent();
    }
    
    public MTLMaterial getMaterial(Chars name) throws StoreException{
        if(!readed){
            readed = true;
            try {
                read();
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        return (MTLMaterial) materials.getValue(name);
    }
    
    private void read() throws IOException{
        
        final boolean[] mustClose = new boolean[1];
        final ByteInputStream bi = IOUtilities.toInputStream(getInput(), mustClose);        
        final CharInputStream charStream = new CharInputStream(bi, CharEncodings.US_ASCII);

        MTLMaterial material = null;
        Chars cs = null;
        while( (cs=charStream.readLine()) != null){
            cs = cs.trim();
            if(cs.isEmpty()){
                continue;
            }
            if(cs.startsWith(COMMENT)){
                continue;
            }
            
            int pe = cs.getFirstOccurence(' ');            
            int pt = cs.getFirstOccurence('\t');
            if(pt>0 && pt<pe) pe = pt;
            if(pe<0) {pe = cs.getByteLength();}
            final Chars prefix = cs.truncate(0, pe);
            cs = cs.truncate(pe+1,cs.getCharLength());

            
            if(NEW.equals(prefix)) {
                //new material
                material = new MTLMaterial(cs);
                material.setBasePath(basePath);
                materials.add(cs,material);
            } else {
                //material property
                material.getProperties().add(prefix, cs);
            }            
        }     
    }
    
    
}
