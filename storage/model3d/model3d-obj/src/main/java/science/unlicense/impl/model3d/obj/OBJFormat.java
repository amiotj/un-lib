
package science.unlicense.impl.model3d.obj;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 *
 * @author Johann Sorel
 */
public class OBJFormat extends AbstractModel3DFormat{

    public static final OBJFormat INSTANCE = new OBJFormat();

    private OBJFormat() {
        super(new Chars("OBJ"),
              new Chars("OBJ"),
              new Chars("OBJ"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("obj")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        final OBJStore store = new OBJStore(input);
        return store;
    }

}
