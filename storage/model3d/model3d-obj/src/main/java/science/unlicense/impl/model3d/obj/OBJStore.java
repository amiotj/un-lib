
package science.unlicense.impl.model3d.obj;

import science.unlicense.impl.model3d.obj.model.OBJGroup;
import science.unlicense.impl.model3d.obj.model.OBJFile;
import science.unlicense.impl.model3d.obj.model.OBJFace;
import science.unlicense.impl.model3d.obj.model.OBJObject;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vectors;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.model3d.mtl.MTLMaterial;
import science.unlicense.impl.model3d.mtl.MTLStore;

/**
 * 
 * @author Johann Sorel
 */
public class OBJStore extends AbstractModel3DStore {

    private OBJFile obj = null;
    
    public OBJStore(Object input) {
        super(OBJFormat.INSTANCE,input);
    }

    public Collection getElements() throws StoreException {

        if(obj==null){
            obj = new OBJFile();
            try {
                obj.read(this);
            } catch (IOException ex) {
                throw new StoreException(ex);
            }
        }
        
        final Collection col = new ArraySequence();
        final Dictionary materialCache = new HashDictionary();
        final Dictionary textureCache = new HashDictionary();
        
        int nb=0;
        for(int i=0,n=obj.objects.getSize();i<n;i++){
            final OBJObject obj = (OBJObject) this.obj.objects.get(i);
            final GLNode mpm = rebuildObject(obj,materialCache,textureCache);
            nb+= mpm.getChildren().getSize();
            col.add(mpm);
        }
        
        return col;
    }
    
    private GLNode rebuildObject(OBJObject obj, Dictionary materialCache, Dictionary textureCache) throws StoreException{
        final GLNode mpm = new GLNode();
        
        for(int i=0,n=obj.groups.getSize();i<n;i++){
            final OBJGroup group = (OBJGroup) obj.groups.get(i);
            final GLNode mesh = rebuildGroup(group,materialCache,textureCache);
            mpm.getChildren().add(mesh);
        }
        
        return mpm;
    }
    
    private GLNode rebuildGroup(OBJGroup group, Dictionary materialCache, Dictionary textureCache) throws StoreException{
        
        final Sequence faces = group.faces;
                
        //count number of triangles
        //some files contain quad or polygons in face definition
        int nbPoint=0;
        int nbIndices=0;
        for(int i=0,n=faces.getSize();i<n;i++){
            final OBJFace face = (OBJFace) faces.get(i);
            nbPoint += face.vertexIdx.length;
            nbIndices += (face.vertexIdx.length-2)*3;
        }
        
        //size of each data array, used to flip negative references
        final int[] sizes = new int[]{obj.vertexCoord.getSize(),obj.vertexNormal.getSize(),obj.vertexTexture.getSize()};
        
        //prepare buffers
        final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(nbPoint*3).cursorFloat();
        final FloatCursor normals = DefaultBufferFactory.INSTANCE.createFloat(nbPoint*3).cursorFloat();
        final FloatCursor uv = DefaultBufferFactory.INSTANCE.createFloat(nbPoint*2).cursorFloat();
        final IntCursor indices = DefaultBufferFactory.INSTANCE.createInt(nbIndices).cursorInt();
        final VBO meshVertex = new VBO(vertices.getBuffer(),3);
        final VBO meshNormal = new VBO(normals.getBuffer(),3);
        final VBO meshUV = new VBO(uv.getBuffer(),2);
        final IBO meshIndice = new IBO(indices.getBuffer(),3);

        int indice = 0;
        for(int i=0,n=faces.getSize();i<n;i++){
            final OBJFace face = (OBJFace) faces.get(i);
            
            
            flipNegative(face.vertexIdx,sizes);
            if(face.uvIdx!=null)flipNegative(face.uvIdx,sizes);
            if(face.normalIdx!=null)flipNegative(face.normalIdx,sizes);

            final int[] temp = new int[face.vertexIdx.length];
            for(int k=0;k<temp.length;k++){
                temp[k] = indice++;
            }
            
            for(int k=2,kn=face.vertexIdx.length;k<kn;k++){
                final boolean firstTriangle = k==2;
                final Tuple v1 = (Tuple)obj.vertexCoord.get(face.vertexIdx[0  ]-1);
                final Tuple v2 = (Tuple)obj.vertexCoord.get(face.vertexIdx[k-1]-1);
                final Tuple v3 = (Tuple)obj.vertexCoord.get(face.vertexIdx[k  ]-1); 
                if(firstTriangle){
                    vertices.write(v1.toArrayFloat());
                    vertices.write(v2.toArrayFloat());
                }   
                vertices.write(v3.toArrayFloat());

                if(face.uvIdx!=null){
                    if(firstTriangle){
                        final Tuple uv1 = (Tuple)obj.vertexTexture.get(face.uvIdx[0  ]-1);
                        final Tuple uv2 = (Tuple)obj.vertexTexture.get(face.uvIdx[k-1]-1);
                        uv.write((float)uv1.getX());uv.write(1f-(float)uv1.getY());
                        uv.write((float)uv2.getX());uv.write(1f-(float)uv2.getY());
                    }
                    final Tuple uv3 = (Tuple)obj.vertexTexture.get(face.uvIdx[k  ]-1);
                    uv.write((float)uv3.getX());uv.write(1f-(float)uv3.getY());
                }

                if(face.normalIdx!=null){
                    if(firstTriangle){
                        final Tuple n1 = (Tuple)obj.vertexNormal.get(face.normalIdx[0  ]-1);
                        final Tuple n2 = (Tuple)obj.vertexNormal.get(face.normalIdx[k-1]-1);
                        normals.write(n1.toArrayFloat());
                        normals.write(n2.toArrayFloat());
                    }
                    final Tuple n3 = (Tuple)obj.vertexNormal.get(face.normalIdx[k  ]-1);
                    normals.write(n3.toArrayFloat());
                }else{
                    //calculate normal
                    double[] v2v1 = Vectors.subtract(v2.getValues(), v1.getValues(), null);
                    double[] v3v1 = Vectors.subtract(v3.getValues(), v1.getValues(), null);
                    double[] normal = Vectors.cross(v2v1, v3v1, null);
                    if(k==2){
                        normals.write((float)normal[0]);
                        normals.write((float)normal[1]);
                        normals.write((float)normal[2]);
                        normals.write((float)normal[0]);
                        normals.write((float)normal[1]);
                        normals.write((float)normal[2]);
                    }
                    normals.write((float)normal[0]);
                    normals.write((float)normal[1]);
                    normals.write((float)normal[2]);
                }

                indices.write(temp[0  ]);
                indices.write(temp[k-1]);
                indices.write(temp[k  ]);
            }
        }

        if(group.materialRanges.isEmpty()){
            final Mesh mesh = new Mesh();
            final Shell shell = new Shell();
            shell.setVertices(meshVertex);
            shell.setNormals(meshNormal);
            shell.setUVs(meshUV);
            shell.setIndexes(meshIndice,IndexRange.TRIANGLES(0, meshIndice.getCapacity()));
            mesh.setShape(shell);
            mesh.getShape().calculateBBox();
            return mesh;
        }else{
            //rebuild one mesh for each material
            final GLNode base = new GLNode();

            if(obj.materialStores!=null && !obj.materialStores.isEmpty()){
                final MTLStore mtl = (MTLStore) obj.materialStores.get(0);                
                for(int i=0,n=group.materialRanges.getSize();i<n;i++){
                    final OBJGroup.MaterialRange range = (OBJGroup.MaterialRange) group.materialRanges.get(i);
                    if(range.end==-1) range.end = group.faces.getSize();
                    if(range.end-range.start <= 0) continue;
                    
                    Material material = (Material) materialCache.getValue(range.name);
                    if(material==null){
                        //load the material
                        final MTLMaterial mtlMat = mtl.getMaterial(range.name);
                        if(mtlMat!=null){
                            material = mtlMat.createMaterial(textureCache);
                            materialCache.add(range.name, material);
                        }
                    }
                    
                    System.out.println("-- "+range.start+"  "+range.end);
                    
                    //build mesh
                    final Mesh mesh = new Mesh();
                    final Shell shell = new Shell();
                    shell.setVertices(meshVertex);
                    shell.setNormals(meshNormal);
                    shell.setUVs(meshUV);
                    shell.setIndexes(meshIndice,IndexRange.TRIANGLES(range.start*3, range.end*3));
                    mesh.setShape(shell);
                    if(material!=null){
                        mesh.setMaterial(material);
                    }
                    mesh.getShape().calculateBBox();
                    base.getChildren().add(mesh);
                }
            }
            
            return base;
        }
        
    }
    
    /**
     * If indexes are negative, they are starting from the end of the list.
     * @param index
     * @return
     */
    private static void flipNegative(int[] index, int[] sizes){
        for(int i=0;i<index.length;i++){
            if(index[i]<0){
                index[i] += sizes[i];
            }
        }
    }

    /**
     * If indexes are negative, they are starting from the end of the list.
     * @param index
     * @return
     */
    private static int flipNegative(int index, int size){
        if(index<0){
            return size+index;
        }
        return index;
    }

}