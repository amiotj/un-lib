
package science.unlicense.impl.model3d.ply;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * A ply element is a set of properties
 * @author Johann Sorel
 */
public class PLYElement {
    
    public Chars name;
    public long number;
    public final Sequence properties = new ArraySequence();
    
}
