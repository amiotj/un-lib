
package science.unlicense.impl.model3d.ply;

import science.unlicense.api.character.Chars;

/**
 * PLY constants.
 * 
 * @author Johann Sorel
 */
public final class PLYConstants {
    
    private PLYConstants(){}
    
    public static final Chars TAG_HEADER_START = new Chars(new byte[]{'p','l','y'});
    public static final Chars TAG_HEADER_END = new Chars(new byte[]{'e','n','d','_','h','e','a','d','e','r'});
    public static final Chars TAG_FORMAT = new Chars(new byte[]{'f','o','r','m','a','t'});
    public static final Chars TAG_COMMENT = new Chars(new byte[]{'c','o','m','m','e','n','t'});
    public static final Chars TAG_ELEMENT = new Chars(new byte[]{'e','l','e','m','e','n','t'});
    public static final Chars TAG_PROPERTY = new Chars(new byte[]{'p','r','o','p','e','r','t','y'});
    
    public static final Chars FORMAT_ASCII = new Chars(new byte[]{'a','s','c','i','i'});
    public static final Chars FORMAT_LITTLE_ENDIAN = new Chars(new byte[]{'b','i','n','a','r','y','_','l','i','t','t','l','e','_','e','n','d','i','a','n'});
    public static final Chars FORMAT_BIG_ENDIAN = new Chars(new byte[]{'b','i','n','a','r','y','_','b','i','g','_','e','n','d','i','a','n'});
        
    public static final Chars TYPE_CHAR = new Chars(new byte[]{'c','h','a','r'});
    public static final Chars TYPE_UCHAR = new Chars(new byte[]{'u','c','h','a','r'});
    public static final Chars TYPE_SHORT = new Chars(new byte[]{'s','h','o','r','t'});
    public static final Chars TYPE_USHORT = new Chars(new byte[]{'u','s','h','o','r','t'});
    public static final Chars TYPE_INT = new Chars(new byte[]{'i','n','t'});
    public static final Chars TYPE_UINT = new Chars(new byte[]{'u','i','n','t'});
    public static final Chars TYPE_FLOAT = new Chars(new byte[]{'f','l','o','a','t'});
    public static final Chars TYPE_DOUBLE = new Chars(new byte[]{'d','o','u','b','l','e'});    
    public static final Chars TYPE_INT8 = new Chars(new byte[]{'i','n','t','8'});
    public static final Chars TYPE_UINT8 = new Chars(new byte[]{'u','i','n','t','8'});
    public static final Chars TYPE_INT16 = new Chars(new byte[]{'i','n','t','1','6'});
    public static final Chars TYPE_UINT16 = new Chars(new byte[]{'u','i','n','t','1','6'});
    public static final Chars TYPE_INT32 = new Chars(new byte[]{'i','n','t','3','2'});
    public static final Chars TYPE_UINT32 = new Chars(new byte[]{'u','i','n','t','3','2'});
    public static final Chars TYPE_FLOAT32 = new Chars(new byte[]{'f','l','o','a','t','3','2'});
    public static final Chars TYPE_FLOAT64 = new Chars(new byte[]{'f','l','o','a','t','6','4'});        
    public static final Chars TYPE_LIST = new Chars(new byte[]{'l','i','s','t'});
            
    
    
    
}
