
package science.unlicense.impl.model3d.ply;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class PLYProperty {
    
    public Chars name;
    public Chars type;
    //if list type
    public Chars sizeType;
    public Chars valueType;
    
}
