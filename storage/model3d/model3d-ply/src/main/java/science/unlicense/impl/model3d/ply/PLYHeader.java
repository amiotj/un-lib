
package science.unlicense.impl.model3d.ply;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class PLYHeader {

    public Chars format;
    public final Sequence comments = new ArraySequence();
    public final Sequence elements = new ArraySequence();

    /**
     * Get element for given name.
     * @param name
     * @return PLYElement or null
     */
    public PLYElement getElement(Chars name){
        for(int i=0,n=elements.getSize();i<n;i++){
            final PLYElement ele = (PLYElement) elements.get(i);
            if(ele.name.equals(name)){
                return ele;
            }
        }
        return null;
    }

}
