
package science.unlicense.impl.model3d.ply;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.model3d.AbstractModel3DStore;
import science.unlicense.api.store.StoreException;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import static science.unlicense.impl.model3d.ply.PLYConstants.FORMAT_ASCII;
import static science.unlicense.impl.model3d.ply.PLYConstants.FORMAT_BIG_ENDIAN;
import static science.unlicense.impl.model3d.ply.PLYConstants.FORMAT_LITTLE_ENDIAN;
import static science.unlicense.impl.model3d.ply.PLYConstants.TAG_COMMENT;
import static science.unlicense.impl.model3d.ply.PLYConstants.TAG_ELEMENT;
import static science.unlicense.impl.model3d.ply.PLYConstants.TAG_FORMAT;
import static science.unlicense.impl.model3d.ply.PLYConstants.TAG_HEADER_END;
import static science.unlicense.impl.model3d.ply.PLYConstants.TAG_PROPERTY;


/**
 *
 * @author Johann Sorel
 */
public class PLYStore extends AbstractModel3DStore {

    public final PLYHeader header = new PLYHeader();

    public float[] vertices;
    public float[] normals;
    public Buffer faces;
    private IntCursor facecursor;

    private int nbVertex;
    private int nbFace;

    public PLYStore(Object base) {
        super(PLYFormat.INSTANCE,base);
    }

    public Collection getElements() throws StoreException {
        try {
            read((Path)input);
        } catch (IOException ex) {
            throw new StoreException(ex.getMessage(),ex);
        }

        final VBO vertexVBO = new VBO(vertices, 3);
        final VBO normalVBO = new VBO(normals, 3);

        final GLNode root = new GLNode();
        final Mesh mesh = new Mesh();
        root.getChildren().add(mesh);

        final Shell shell = new Shell();
        shell.setVertices(vertexVBO);
        shell.setNormals(normalVBO);
        shell.setIndexes(new IBO(faces,3),IndexRange.TRIANGLES(0, (int) faces.getPrimitiveCount()));
        mesh.setShape(shell);
        mesh.getShape().calculateBBox();
//        mesh.getMaterial().setLightVulnerable(false);

        final Collection col = new ArraySequence();
        col.add(root);
        return col;
    }

    private void read(final Path path) throws IOException {

        final ByteInputStream inStream = path.createInputStream();

        final ByteSequence headerData = findHeader(inStream);

        //parse header
        final CharInputStream charStream = new CharInputStream(
                new ArrayInputStream(headerData.toArrayByte()),
                CharEncodings.US_ASCII, new Char("\n".getBytes(),CharEncodings.US_ASCII));
        parseHeader(charStream);

        //get number of vertex and faces
        nbVertex = (int) header.getElement(new Chars("vertex")).number;
        nbFace = (int) header.getElement(new Chars("face")).number;


        if(FORMAT_ASCII.equals(header.format)){
            final CharInputStream ds = new CharInputStream(inStream, CharEncodings.US_ASCII,
                    new Char("\n".getBytes(),CharEncodings.US_ASCII));
            parseASCII(ds);
        }else if(FORMAT_BIG_ENDIAN.equals(header.format)){
            final DataInputStream ds = new DataInputStream(inStream, NumberEncoding.BIG_ENDIAN);
            parseBinary(ds,NumberEncoding.BIG_ENDIAN);
        }else if(FORMAT_LITTLE_ENDIAN.equals(header.format)){
            final DataInputStream ds = new DataInputStream(inStream, NumberEncoding.LITTLE_ENDIAN);
            parseBinary(ds,NumberEncoding.LITTLE_ENDIAN);
        }

    }

    /**
     *
     * @param charStream
     * @return header length
     * @throws IOException
     */
    private void parseHeader(CharInputStream charStream) throws IOException{

        PLYElement currentElement = null;
        Chars line = null;
        Chars[] parts = null;
        while( (line=charStream.readLine()) != null){
            line = line.trim();
            parts = line.split(' ');

            if(TAG_FORMAT.equals(parts[0])){
                header.format = parts[1];

            }else if(TAG_COMMENT.equals(parts[0])){
                header.comments.add(parts[1]);

            }else if(TAG_ELEMENT.equals(parts[0])){
                currentElement = new PLYElement();
                currentElement.name = parts[1];
                currentElement.number = Int32.decode(parts[2]);
                header.elements.add(currentElement);

            }else if(TAG_PROPERTY.equals(parts[0])){
                final PLYProperty prop = new PLYProperty();
                prop.type = parts[1];
                if(parts.length==3){
                    //simple type
                    prop.name = parts[2];
                }else{
                    //list type
                    prop.sizeType = parts[2];
                    prop.valueType = parts[3];
                    prop.name = parts[4];
                }
                currentElement.properties.add(prop);

            }else if(TAG_HEADER_END.equals(parts[0])){
                return;
            }

        }

        throw new IOException("Reached end of file before findind header end.");
    }

    private void parseASCII(CharInputStream charStream) throws IOException{
        Chars line = null;
        Chars[] parts = null;

        vertices = new float[nbVertex*3];

        int k=0;
        for(int i=0;i<nbVertex;){
            line = charStream.readLine().trim();
            if(line.isEmpty()) continue;
            i++;
            parts = line.split(' ');
            vertices[k++] = (float)Float64.decode(parts[0]);
            vertices[k++] = (float)Float64.decode(parts[1]);
            vertices[k++] = (float)Float64.decode(parts[2]);
        }

        for(int i=0;i<nbFace;){
            line = charStream.readLine().trim();
            if(line.isEmpty()) continue;
            i++;
            parts = line.split(' ');

            final int nb = Int32.decode(parts[0]);
            final int[] indexes = new int[nb];
            for(int p=0;p<nb;p++){
                indexes[p] = Int32.decode(parts[p]);
            }
            addFace(indexes);
        }
    }

    private void parseBinary(DataInputStream ds, final NumberEncoding encoding) throws IOException{

        vertices = ds.readFloat(nbVertex*3);

        for(int i=0;i<nbFace;i++){
            final int nb = ds.readUByte();
            addFace(ds.readInt(nb));
        }
    }

    private void addFace(int[] indices) throws IOException{
        if(faces == null){
            if(indices.length==3){
                faces = DefaultBufferFactory.INSTANCE.createInt(nbFace*3);
            }else{
                faces = DefaultBufferFactory.INSTANCE.createInt(nbFace*2*3);
            }
            facecursor = faces.cursorInt();
            normals = new float[nbVertex*3];
        }

        if(indices.length==3){
            final float[] normal = calculateNormal(indices[0], indices[1], indices[2]);
            Arrays.copy(normal,0,3,normals,indices[0]*3);
            Arrays.copy(normal,0,3,normals,indices[1]*3);
            Arrays.copy(normal,0,3,normals,indices[2]*3);
            facecursor.write(indices);
        }else if(indices.length==4){
            float[] normal = calculateNormal(indices[0], indices[1], indices[2]);
            Arrays.copy(normal,0,3,normals,indices[0]*3);
            Arrays.copy(normal,0,3,normals,indices[1]*3);
            Arrays.copy(normal,0,3,normals,indices[2]*3);
            facecursor.write(indices[0]).write(indices[1]).write(indices[2]);

            normal = calculateNormal(indices[2], indices[3], indices[0]);
            Arrays.copy(normal,0,3,normals,indices[2]*3);
            Arrays.copy(normal,0,3,normals,indices[3]*3);
            Arrays.copy(normal,0,3,normals,indices[0]*3);
            facecursor.write(indices[2]).write(indices[3]).write(indices[0]);
        }else{
            throw new IOException("Unsupported number of elements : "+indices.length);
        }

    }

    private float[] calculateNormal(int i1, int i2, int i3){
        final Vector v1 = new Vector(vertices[i1*3], vertices[i1*3+1], vertices[i1*3+2]);
        final Vector v2 = new Vector(vertices[i2*3], vertices[i2*3+1], vertices[i2*3+2]);
        final Vector v3 = new Vector(vertices[i3*3], vertices[i3*3+1], vertices[i3*3+2]);
        final Vector v12 = v1.subtract(v2, null);
        final Vector v13 = v1.subtract(v3, null);
        return v12.cross(v13, null).localNormalize().toArrayFloat();
    }

    private static ByteSequence findHeader(ByteInputStream stream) throws IOException{
        final ByteSequence headerData = new ByteSequence();

        final byte[] tagByte = TAG_HEADER_END.toBytes();
        final byte[] end = TAG_HEADER_END.getCharacter(TAG_HEADER_END.getCharLength()-1).toBytes();

        for(int b=stream.read();b!=-1;b=stream.read()){
            headerData.put((byte)b);
            if(headerData.endWidth(tagByte)){
                //search for line end
                for(b=stream.read();b!='\n';b=stream.read());
                return headerData;
            }
        }

        throw new IOException("Reached end of file before findind header end.");
    }



}
