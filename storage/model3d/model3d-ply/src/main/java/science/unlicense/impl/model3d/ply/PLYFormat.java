
package science.unlicense.impl.model3d.ply;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model3d.AbstractModel3DFormat;
import science.unlicense.api.model3d.Model3DStore;

/**
 * Specification :
 * http://paulbourke.net/dataformats/ply/
 *
 * @author Johann Sorel
 */
public class PLYFormat extends AbstractModel3DFormat{

    public static final PLYFormat INSTANCE = new PLYFormat();

    private PLYFormat() {
        super(new Chars("PLY"),
              new Chars("PLY"),
              new Chars("Polygon File Format"),
              new Chars[]{
              },
              new Chars[]{
                new Chars("ply")
              },
              new byte[0][0]);
    }

    public Model3DStore open(Object input) throws IOException {
        return new PLYStore(input);
    }

}
