
package science.unlicense.impl.model3d.ply;

import science.unlicense.impl.model3d.ply.PLYStore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.path.Path;
import science.unlicense.api.model3d.Model3Ds;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class PLYReaderTest {

    @Test
    public void readTest() throws Exception{

        final Chars address = new Chars("mod:/un/storage/model3d/ply/cube.ply");

        final Path path = Paths.resolve(address);
        final PLYStore store = (PLYStore) Model3Ds.read(path);
        final GLNode node = (GLNode) store.getElements().createIterator().next();

    }

}
