
package science.unlicense.impl.media.vp8;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class VP8Reader extends AbstractReader{
    
    public VP8Frame readFrame() throws IOException{
        final VP8Frame frame = new VP8Frame();
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        ds.setBitsDirection(DataInputStream.LSB);
        frame.read(ds);
        return frame;
    }
    
}
