

package science.unlicense.impl.media.vp8;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * Specification :
 * http://tools.ietf.org/html/rfc6386#section-9.1
 * 
 * @author Johann Sorel
 */
public class VP8Frame {
    
    /**
     * Intra frame is a self sufficient frame.
     * Also called key frames.
     */
    public static final int TYPE_INTRA_FRAME = 1;
    
    /**
     * Inter frame are frames which pixels are deduced from previous frames.
     * Also called prediction frames.
     */
    public static final int TYPE_INTER_FRAME = 2;
    
    public static final int RECFILTER_BICUBIC = 0;
    public static final int RECFILTER_BILINEAR = 1;
    public static final int RECFILTER_NONE = 2;
    public static final int LOOPFILTER_NORMAL = 0;
    public static final int LOOPFILTER_SIMPLE = 1;
    public static final int LOOPFILTER_NONE = 2;
    
    /**
     * Start code pattern.
     */
    public static final int STARTCODE = 0x9d012a;
    
    
    public boolean keyframe;
    public int version;
    public boolean showframe;
    public int firstPartSize;
    
    //only if it is a keyframe
    public int startCode;
    public int width;
    public int horizontalScale;
    public int height;
    public int verticalScale;
        
    
    public VP8FrameHeader header;
    /** [row][col] */
    public VP8MacroBlock[][] macroblocks;
    
    public int getReconstructionFilter(){
        if(version==0) return RECFILTER_BICUBIC;
        if(version==1 || version==2) return RECFILTER_BILINEAR;
        return RECFILTER_NONE;
    }
    
    public int getLoopFilter(){
        if(version==0) return LOOPFILTER_NORMAL;
        if(version==1) return LOOPFILTER_SIMPLE;
        return LOOPFILTER_NONE;
    }
    
    public float getHorizontalUpscaleFactor(){
        if(horizontalScale==0) return 1f;
        if(horizontalScale==1) return 5f/4f;
        if(horizontalScale==2) return 5f/3f;
        return 2f;
    }
    
    public float getVerticalUpscaleFactor(){
        if(verticalScale==0) return 1f;
        if(verticalScale==1) return 5f/4f;
        if(verticalScale==2) return 5f/3f;
        return 2f;
    }
    
    
    public void read(DataInputStream ds) throws IOException{
        //3 bytes for an inter frame
        keyframe        = ds.readBits(1)==0;
        version         = ds.readBits(3);
        showframe       = ds.readBits(1)==1;
        firstPartSize   = ds.readBits(19);
        
        if(keyframe){
            // 10bytes for an intra frame
            startCode       = ds.readBits(24);
            width           = ds.readBits(14);
            horizontalScale = ds.readBits(2);
            height          = ds.readBits(14);
            verticalScale   = ds.readBits(2);
        }
        
        final BoolReader br = new BoolReader(ds,NumberEncoding.LITTLE_ENDIAN);
        br.initBool();
        
        //read header
        header = new VP8FrameHeader();
        header.read(br, keyframe);
        
        
        //Read macroblocks
        final int nbBlockX = (int) Math.ceil((double)width/16.0);
        final int nbBlockY = (int) Math.ceil((double)height/16.0);
        macroblocks = new VP8MacroBlock[nbBlockY][nbBlockX];
        
        for(int y=0;y<nbBlockY;y++){
            for(int x=0;x<nbBlockX;x++){
                macroblocks[y][x] = new VP8MacroBlock(this,x,y);
                macroblocks[y][x].read(br, header);
            }
        }
        
    }
    
}
