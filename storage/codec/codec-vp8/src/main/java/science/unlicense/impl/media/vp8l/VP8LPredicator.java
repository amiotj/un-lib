

package science.unlicense.impl.media.vp8l;

import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;

/**
 *
 * @author Johann Sorel
 */
public abstract class VP8LPredicator {
    
    public static final VP8LPredicator BLACK = new BLACK();
    public static final VP8LPredicator LEFT = new LEFT();
    public static final VP8LPredicator TOP = new TOP();
    public static final VP8LPredicator TOPLEFT = new TOPRIGHT();
    public static final VP8LPredicator TOPRIGHT = new TOPLEFT();
    public static final VP8LPredicator AVG_L_TR_T = new AVG_L_TR_T();
    public static final VP8LPredicator AVG_L_TL = new AVG_L_TL();
    public static final VP8LPredicator AVG_L_T = new AVG_L_T();
    public static final VP8LPredicator AVG_TL_T = new AVG_TL_T();
    public static final VP8LPredicator AVG_T_TR = new AVG_T_TR();
    public static final VP8LPredicator AVG_L_TL_T_TR = new AVG_L_TL_T_TR();
    public static final VP8LPredicator SELECT = new SELECT();
    public static final VP8LPredicator CLAMP_FULL = new CLAMP_FULL();
    public static final VP8LPredicator CLAMP_HALF = new CLAMP_HALF();
    
    public abstract int evaluate(int tl, int t, int tr, int l);
    
    private static int average2(int a, int b){
        return (a + b) / 2;
    }
    
    // Clamp the input value between 0 and 255.
    private static int clamp(int a) {
        return (a < 0) ? 0 : (a > 255) ?  255 : a;
    }

    private static int clampAddSubtractFull(int a, int b, int c) {
        return clamp(a + b - c);
    }

    private static int clampAddSubtractHalf(int a, int b) {
        return clamp(a + (a - b) / 2);
    }
    
    
    private static final class BLACK extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return Color.BLACK.toARGB();
        }
    }
    private static final class LEFT extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return l;
        }
    }
    private static final class TOP extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return t;
        }
    }
    private static final class TOPRIGHT extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return tr;
        }
    }
    private static final class TOPLEFT extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return tl;
        }
    }
    private static final class AVG_L_TR_T extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return average2(average2(l, tr), t);
        }
    }
    private static final class AVG_L_TL extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return average2(l, tl);
        }
    }
    private static final class AVG_L_T extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return average2(l, t);
        }
    }
    private static final class AVG_TL_T extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return average2(tl, t);
        }
    }
    private static final class AVG_T_TR extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return average2(t, tr);
        }
    }
    private static final class AVG_L_TL_T_TR extends VP8LPredicator{
        public int evaluate(int tl, int t, int tr, int l) {
            return average2(average2(l, tl), average2(t, tr));
        }
    }
    private static final class SELECT extends VP8LPredicator{
        public int evaluate(int TL, int T, int TR, int L) {
            final int[] tlRGBA = Colors.toRGBA(TL, (int[])null);
            final int[] tRGBA = Colors.toRGBA(T, (int[])null);
            final int[] lRGBA = Colors.toRGBA(L, (int[])null);
            
            // ARGB component estimates for prediction.
            int pAlpha = lRGBA[3] + tRGBA[3] - tlRGBA[3];
            int pRed   = lRGBA[0] + tRGBA[0] - tlRGBA[0];
            int pGreen = lRGBA[1] + tRGBA[1] - tlRGBA[1];
            int pBlue  = lRGBA[2] + tRGBA[2] - tlRGBA[2];

            // Manhattan distances to estimates for left and top pixels.
            int pL = Math.abs(pAlpha - lRGBA[3]) + Math.abs(pRed - lRGBA[0]) +
                     Math.abs(pGreen - lRGBA[1]) + Math.abs(pBlue - lRGBA[2]);
            int pT = Math.abs(pAlpha - tRGBA[3]) + Math.abs(pRed - tRGBA[0]) +
                     Math.abs(pGreen - tRGBA[1]) + Math.abs(pBlue - tRGBA[2]);

            // Return either left or top, the one closer to the prediction.
            if (pL <= pT) {
              return L;
            } else {
              return T;
            }
        }
    }
    private static final class CLAMP_FULL extends VP8LPredicator{
        public int evaluate(int TL, int T, int TR, int L) {
            final int[] tlRGBA = Colors.toRGBA(TL, (int[])null);
            final int[] tRGBA = Colors.toRGBA(T, (int[])null);
            final int[] lRGBA = Colors.toRGBA(L, (int[])null);
            
            final int[] res = new int[]{
                clampAddSubtractFull(lRGBA[0], tRGBA[0], tlRGBA[0]),
                clampAddSubtractFull(lRGBA[1], tRGBA[1], tlRGBA[1]),
                clampAddSubtractFull(lRGBA[2], tRGBA[2], tlRGBA[2]),
                clampAddSubtractFull(lRGBA[3], tRGBA[3], tlRGBA[3])};
            
            return Colors.toARGB(res);
        }
    }
    private static final class CLAMP_HALF extends VP8LPredicator{
        public int evaluate(int TL, int T, int TR, int L) {
            final int[] tlRGBA = Colors.toRGBA(TL, (int[])null);
            final int[] tRGBA = Colors.toRGBA(T, (int[])null);
            final int[] lRGBA = Colors.toRGBA(L, (int[])null);
            
            final int[] res = new int[]{
                clampAddSubtractHalf(average2(lRGBA[0], tRGBA[0]), tlRGBA[0]),
                clampAddSubtractHalf(average2(lRGBA[1], tRGBA[1]), tlRGBA[1]),
                clampAddSubtractHalf(average2(lRGBA[2], tRGBA[2]), tlRGBA[2]),
                clampAddSubtractHalf(average2(lRGBA[3], tRGBA[3]), tlRGBA[3])};
            
            return Colors.toARGB(res);
        }
    }
    
}
