

package science.unlicense.impl.media.vp8;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Extend DataInputStream with Bool decoding.
 * 
 * Specification :
 * http://tools.ietf.org/html/rfc6386#section-7
 * 
 * @author Johann Sorel
 */
public class BoolReader extends DataInputStream {

    public int range;      /* always identical to encoder's range */
    public int value;      /* contains at least 8 significant bits */
    public int bit_count;  /* # of bits shifted out of
                            value, at most 7 */
    
    public BoolReader(ByteInputStream in) {
        super(in);
    }
    
    public BoolReader(ByteInputStream in, NumberEncoding encoding) {
        super(in, encoding);
    }

    public void initBool() throws IOException {
        value = readUByte()<<8 | readUByte(); /* value = first 2 input bytes */
        range = 255; /* initial range is full */
        bit_count = 0; /* have not yet shifted out any bits */
    }
    
    public int readBool(int prob) throws IOException{
        /* range and split are identical to the corresponding values
        used by the encoder when this bool was written */
        int split = 1 + (((range - 1) * prob) >> 8);
        int SPLIT = split << 8;
        int retval;           /* will be 0 or 1 */

        if (value >= SPLIT) {  /* encoded a one */
            retval = 1;
            range -= split; /* reduce range */
            value -= SPLIT;  /* subtract off left endpoint of interval */
        } else {              /* encoded a zero */
            retval = 0;
            range = split;  /* reduce range, no change in left endpoint */
        }

        while (range < 128) { /* shift out irrelevant value bits */
            value <<= 1;
            range <<= 1;
            if (++bit_count == 8) { /* shift in new bits 8 at a time */
                bit_count = 0;
                value |=  readUByte();
            }
        }
        return retval;
    }
    
    public boolean readFlag() throws IOException{
        return readBool(128)==1;
    }
    
    /**
     * Read an unsigned literal coded used bool.
     * @param nbBits
     * @return
     */
    public int readULiteral(int nbBits) throws IOException{
        int v = 0;
        while(nbBits--!=0){
            v = (v << 1) + readBool(128);
        }
        return v;
    }
    
    /**
     * Read an unsigned literal on given number of bits.
     * Then read a single bit flag for the sign
     * @param nbBits
     * @return
     * @throws IOException 
     */
    public int readULiteralAndSign(int nbBits) throws IOException{
        final int val = readULiteral(nbBits);
        return readFlag() ? -val : val;
    }

    /**
     * Read a signed literal coded used bool.
     * @param nbBits
     * @return 
     */
   public int readLiteral(int nbBits) throws IOException{
     if(nbBits==0) return 0;     
     int v = (readBool(128)==1) ? 1 : 0;     
     while(--nbBits!=0){
         v = (v << 1) + readBool(128);
     }
     return v;
   }
   
   /**
    * http://tools.ietf.org/html/rfc6386#section-8.1
    * 
    * @param t tree
    * @param p probability
    * @return
    * @throws IOException 
    */
   public int readTree(int[] t, int[] p) throws IOException{
        int i = 0;   /* begin at root */
        /* Descend tree until leaf is reached */
        while ((i = t[ i + readBool( p[i>>1])]) > 0){}
        return -i;     /* return value is negation of nonpositive index */
   }
    
}
