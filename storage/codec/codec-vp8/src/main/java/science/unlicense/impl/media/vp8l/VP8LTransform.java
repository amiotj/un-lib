

package science.unlicense.impl.media.vp8l;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class VP8LTransform {
    
    public void read(DataInputStream ds, VP8LReader reader) throws IOException{throw new IOException("TODO");}
    
    public void write(DataOutputStream ds) throws IOException{throw new IOException("TODO");}
    
    
    public static class TransformPredicator extends VP8LTransform{

        public int sizeBits;
        public int blockWidth;
        public int blockHeight;
        public int blockXSize;
        public ByteSequence data;
        
        public void read(DataInputStream ds, VP8LReader reader) throws IOException {
            sizeBits = ds.readBits(3) + 2;
            final int den = 1 << sizeBits;
            blockWidth = den;
            blockHeight = den;
            blockXSize = VP8LReader.divRoundUp(reader.imgWidth,den);
            data = reader.readEntropyCodedImage(ds, blockWidth, blockHeight, 1);
        }
        
        public int getBlockIndex(int x, int y){
            return (y >> sizeBits) * blockXSize + (x >> sizeBits);
        }
    }
    
    public static class TransformColor extends VP8LTransform{
        
        public static class ColorTransformElement{
            byte greenToRed;
            byte greenToBlue;
            byte redToBlue;
        }
        
        public int blockWidth;
        public int blockHeight;
        public ByteSequence data;
        
        public void read(DataInputStream ds, VP8LReader reader) throws IOException {
            final int sizeBits =ds.readBits(3) + 2;
            final int den = 1 << sizeBits;
            blockWidth = den;
            blockHeight = den;
            data = reader.readEntropyCodedImage(ds, blockWidth, blockHeight, 3);
        }
        
        public void transform(int[] rgba, ColorTransformElement trans, int[] buffer) {
            // Transformed values of red and blue components
            int tmp_red = rgba[0];
            int tmp_blue = rgba[2];

            // Applying transform is just adding the transform deltas
            tmp_red  += colorTransformDelta(trans.greenToRed, rgba[1]);
            tmp_blue += colorTransformDelta(trans.greenToBlue, rgba[1]);
            tmp_blue += colorTransformDelta(trans.redToBlue, rgba[0]);

            buffer[0] = tmp_red & 0xFF;
            buffer[1] = rgba[1];
            buffer[2] = tmp_blue & 0xFF;
            buffer[3] = rgba[3];
        }
        
        public void itransform(int[] rgba, ColorTransformElement trans, int[] buffer) {
            // Transformed values of red and blue components
            int tmp_red = rgba[0];
            int tmp_blue = rgba[2];

            // Applying transform is just adding the transform deltas
            tmp_red  -= colorTransformDelta(trans.greenToRed, rgba[1]);
            tmp_blue -= colorTransformDelta(trans.greenToBlue, rgba[1]);
            tmp_blue -= colorTransformDelta(trans.redToBlue, rgba[0]);

            buffer[0] = tmp_red & 0xFF;
            buffer[1] = rgba[1];
            buffer[2] = tmp_blue & 0xFF;
            buffer[3] = rgba[3];
        }
        
        private static int colorTransformDelta(int t, int c) {
            return (t * c) >> 5;
        }
    }
    
    public static class TransformSubstractGreen extends VP8LTransform{
        
        public void read(DataInputStream ds, VP8LReader reader) throws IOException {
            
        }
        
        public void transform(int[] rgba, int[] buffer) {
            buffer[0] = (rgba[0] + rgba[1]) & 0xFF;
            buffer[1] = rgba[1];
            buffer[2] = (rgba[2] + rgba[1]) & 0xFF;
            buffer[3] = rgba[3];
        }
        
    }
    
    public static class TransformIndex extends VP8LTransform{

        private byte[] data;

        public void read(DataInputStream ds, VP8LReader reader) throws IOException {
            // 8 bit value for color table size
            int color_table_size = ds.readBits(8) + 1;
            final int width_bits = nbPixelCombined(color_table_size);

            data = reader.readEntropyCodedImage(ds, color_table_size,1,4).getBackArray();

            //TODO remap colors correctly
            //doc is just ...
            if(width_bits==3){

            }else if(width_bits==2){

            }else if(width_bits==1){

            }

            throw new IOException("No supported.");

        }
        
        public void itransform(int[] rgba, int[] buffer){
            final int index = rgba[1]*3;
            buffer[0] = data[index+0] & 0xFF;
            buffer[1] = data[index+1] & 0xFF;
            buffer[2] = data[index+2] & 0xFF;
            buffer[3] = data[index+3] & 0xFF;
        }
        
        /**
         * if the number of color is small, pixels can be compacted.
         * values are packed in the green component.
         * @return 
         */
        private static int nbPixelCombined(int color_table_size){
            int width_bits;
            if (color_table_size <= 2) {
                width_bits = 3;
            } else if (color_table_size <= 4) {
                width_bits = 2;
            } else if (color_table_size <= 16) {
                width_bits = 1;
            } else {
                width_bits = 0;
            }
            return width_bits;
        }
        
    }
    
}
