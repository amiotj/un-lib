
package science.unlicense.impl.media.vp8l;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import static science.unlicense.impl.media.vp8l.VP8LConstants.*;

/**
 * 
 * VP8L specification :
 * https://developers.google.com/speed/webp/docs/webp_lossless_bitstream_specification
 * 
 * @author Johann Sorel
 */
public class VP8LReader extends AbstractReader {
    
    public int imgWidth;
    public int imgHeight;
    public boolean hasAlpha;
    public int versionNumber;
    
    /** up to 4 transforms */
    public VP8LTransform[] transforms = new VP8LTransform[0];
    
    public Image read() throws IOException{
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.LITTLE_ENDIAN);
        ds.setBitsDirection(DataInputStream.LSB);
        
        //signature 0x2F
        final byte signature = ds.readByte();
        if(signature != SIGNATURE){
            throw new IOException("Stream is not a VP8L");
        }
        
        //header
        imgWidth = ds.readBits(14)+1;
        imgHeight = ds.readBits(14)+1;
        
        return read(ds,imgWidth,imgHeight);
        
    }

    Image read(DataInputStream ds, int width, int height) throws IOException{

        hasAlpha = ds.readBits(1) == 1;
        versionNumber = ds.readBits(3);

        //transforms
        while(ds.readBits(1)==1){
            final int type = ds.readBits(2);

            final VP8LTransform transform;
            if(TRANSFORM_PREDICTOR==type){
                transform = new VP8LTransform.TransformPredicator();
            }else if(TRANSFORM_COLOR==type){
                transform = new VP8LTransform.TransformColor();
            }else if(TRANSFORM_SUBSTRACTGREEN==type){
                transform = new VP8LTransform.TransformSubstractGreen();
            }else if(TRANSFORM_INDEX==type){
                transform = new VP8LTransform.TransformIndex();
            }else{
                throw new IOException("Unknwon tranform : "+type);
            }
            transform.read(ds,this);
        }

        //read color cache info
        VP8LColorCache colorCache = null;
        if(ds.readBits(1)==1){
            colorCache = new VP8LColorCache();
            colorCache.read(ds);
        }

        //read meta huffman
        final VP8LHuffman huffman = new VP8LHuffman();
        huffman.read(ds);


        //read huffman codes or groups
        //TODO

        //LZ77 coded image

        return null;
    }

    public ByteSequence readEntropyCodedImage(DataInputStream ds, int imageWidth,
            int imageHeight, int nbSample) throws IOException{
        //read color cache info
        VP8LColorCache colorCache = null;
        if(ds.readBits(1)==1){
            colorCache = new VP8LColorCache();
            colorCache.read(ds);
        }

        //read meta huffman
        final VP8LHuffman huffman = new VP8LHuffman();
        huffman.read(ds);


        //read huffman codes or groups
        //TODO

        //LZ77 coded image

        return null;
    }

    public static int divRoundUp(int num, int den){
        return (num+den-1) / den;
    }
    
}
