

package science.unlicense.impl.media.vp8;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.IOException;


/**
 *
 * Specification :
 * http://tools.ietf.org/html/rfc6386#section-19.2
 * 
 * @author Johann Sorel
 */
public class VP8FrameHeader {
    
    public static final class Segmentation{
        public boolean updateMbSegmentationMap;
        public boolean update_segment_feature_data;
        public boolean segment_feature_mode;
        public int[] quantizer_update_value = new int[4];
        public int[] lf_update_value = new int[4];
        public int[] segment_prob = new int[3];
    }
    public static final class Adjustement{
        public boolean loop_filter_adj_enable;
        public boolean mode_ref_lf_delta_update;
        public int[] frame_delta_magnitude = new int[4];
        public int[] mode_delta_magnitude = new int[4];
    }
    public static final class QuantificationIndices{
        public int y_ac_qi;
        public int y_dc_delta_magnitude;
        public int y2_dc_delta_magnitude;
        public int y2_ac_delta_magnitude;
        public int uv_dc_delta_magnitude;
        public int uv_ac_delta_magnitude;
    }
    public static final class TokenProbUpdate{
        public int[][][][] coeff_prob = new int[4][8][3][11];
        public TokenProbUpdate(){
            //set to default values
            for(int i=0;i<4;i++){
                for(int j=0;j<8;j++){
                    coeff_prob[i][j] = Arrays.copy(VP8Constants.DEFAULT_COEFF_PROBS[i][j]);
                }
            }
        }
    }
    public static final class MVProbUpdate{
        public int[][] prob = new int[2][19];
        public MVProbUpdate(){
            //set to default values
            prob = Arrays.copy(VP8Constants.DEFAULT_MV_CONTEXT);
        }
    }
    
    public boolean isKeyFrame;
    
    public int colorSpace;
    public int clampingType;
    public Segmentation segmentation;
    public int filterType;
    public int loopFilterLevel;
    public int sharpnessLevel;
    public Adjustement adjustement;
    public int log2NbrOfDctPartitions;
    public QuantificationIndices qatIndices;
    public boolean refreshEntropyProbs;
    public boolean refreshGoldenFrame;
    public boolean refreshAlternateFrame;
    public int copyBufferToGolden;
    public int copyBufferToAlternate;
    public boolean signBiasGolden;
    public boolean signBiasAlternate;
    public boolean refreshLast;
    public TokenProbUpdate tokenProbUpdate;
    public boolean mbNoSkipCoeff;
    public int probSkipFalse;
    public int probIntra;
    public int probLast;
    public int probGf;
    public int[] intra16x16Prob;
    public int[] intraChromaProb;
    public MVProbUpdate mvProbUpdate;
    
    public void read(BoolReader ds, boolean isKeyFrame) throws IOException {
        this.isKeyFrame = isKeyFrame;
        
        if(isKeyFrame){
            colorSpace   = ds.readULiteral(1);
            clampingType = ds.readULiteral(1);
        }
        if(ds.readFlag()){
            updateSegmentation(ds);
        }
        filterType      = ds.readULiteral(1);
        loopFilterLevel = ds.readULiteral(6);
        sharpnessLevel  = ds.readULiteral(3);
        mbLfAdjustments(ds);
        log2NbrOfDctPartitions = ds.readULiteral(2);
        quantIndices(ds);
        if(isKeyFrame){
            refreshEntropyProbs   = ds.readFlag();
        }else{
            refreshGoldenFrame    = ds.readFlag();
            refreshAlternateFrame = ds.readFlag();
            if (!refreshGoldenFrame)    copyBufferToGolden = ds.readULiteral(2);
            if (!refreshAlternateFrame) copyBufferToAlternate = ds.readULiteral(2);
            signBiasGolden        = ds.readFlag();
            signBiasAlternate     = ds.readFlag();
            refreshEntropyProbs   = ds.readFlag();
            refreshLast           = ds.readFlag();
        }        
        tokenProbUpdate(ds);
        mbNoSkipCoeff = ds.readFlag();
        if (mbNoSkipCoeff) {
            probSkipFalse = ds.readULiteral(8);
        }
        
        if(!isKeyFrame){
            probIntra = ds.readULiteral(8);
            probLast  = ds.readULiteral(8);
            probGf    = ds.readULiteral(8);
            if (ds.readFlag()) {         
                intra16x16Prob = new int[4];
                for(int i=0; i<4; i++){
                    intra16x16Prob[i] = ds.readULiteral(8);
                }         
            }
            if (ds.readFlag()) {         
                intraChromaProb = new int[4];
                for(int i=0; i<3; i++){
                    intraChromaProb[i] = ds.readULiteral(8);
                }         
            }
            mvProbUpdate(ds);
        }
    }
    
    private void updateSegmentation(BoolReader ds) throws IOException {
        segmentation = new Segmentation();
        segmentation.updateMbSegmentationMap = ds.readFlag();
        segmentation.update_segment_feature_data = ds.readFlag();
        if (segmentation.update_segment_feature_data) {
            segmentation.segment_feature_mode = ds.readFlag();
            for (int i = 0; i < 4; i++) {
                if (ds.readFlag()) segmentation.quantizer_update_value[i] = ds.readULiteralAndSign(7);
            }
            for (int i = 0; i < 4; i++) {
                if (ds.readFlag()) segmentation.lf_update_value[i] = ds.readULiteralAndSign(6);
            }
        }
        if (segmentation.updateMbSegmentationMap) {
            for (int i = 0; i < 3; i++) {
                if (ds.readFlag()) segmentation.segment_prob[i] = ds.readULiteral(8);
            }
        }
    }
    
    private void mbLfAdjustments(BoolReader ds) throws IOException {
        adjustement = new Adjustement();
        adjustement.loop_filter_adj_enable = ds.readFlag();
        if (adjustement.loop_filter_adj_enable) {
            adjustement.mode_ref_lf_delta_update = ds.readFlag();
            if (adjustement.mode_ref_lf_delta_update) {
                for(int i=0; i<4; i++){
                    if (ds.readFlag()) {
                        adjustement.frame_delta_magnitude[i] = ds.readULiteralAndSign(6);
                    }
                }
                for(int i=0; i<4; i++){
                    if (ds.readFlag()) {
                        adjustement.mode_delta_magnitude[i] = ds.readULiteralAndSign(6);
                    }
                }
            }
        }
    }
    
    private void quantIndices(BoolReader ds) throws IOException {
        qatIndices = new QuantificationIndices();
        qatIndices.y_ac_qi = ds.readULiteral(7);
        if (ds.readFlag())qatIndices.y_dc_delta_magnitude  = ds.readULiteralAndSign(4);
        if (ds.readFlag())qatIndices.y2_dc_delta_magnitude = ds.readULiteralAndSign(4);
        if (ds.readFlag())qatIndices.y2_ac_delta_magnitude = ds.readULiteralAndSign(4);
        if (ds.readFlag())qatIndices.uv_dc_delta_magnitude = ds.readULiteralAndSign(4);
        if (ds.readFlag())qatIndices.uv_ac_delta_magnitude = ds.readULiteralAndSign(4);
    }

    private void tokenProbUpdate(BoolReader ds) throws IOException {
        tokenProbUpdate = new TokenProbUpdate();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 11; l++) {
                        if (ds.readBool(VP8Constants.COEFF_UPDATE_PROBS[i][j][k][l])==1) {
                            tokenProbUpdate.coeff_prob[i][j][k][l] = ds.readULiteral(8);
                        }
                    }
                }
            }
        }
    }
    
    private void mvProbUpdate(BoolReader ds) throws IOException {
        mvProbUpdate = new MVProbUpdate();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 19; j++) {
                if (ds.readFlag()) mvProbUpdate.prob[i][j] = ds.readULiteral(7);
            }
        }
    }

}
