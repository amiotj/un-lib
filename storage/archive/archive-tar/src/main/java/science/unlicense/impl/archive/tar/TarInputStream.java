package science.unlicense.impl.archive.tar;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.WrapInputStream;

/**
 * The TarInputStream reads a UNIX tar archive as an InputStream. methods are
 * provided to position at each successive entry in the archive, and the read
 * each entry as a normal input stream using read().
 *
 * Kerry Menzel <kmenzel@cfl.rr.com> Contributed the code to support file sizes
 * greater than 2GB (longs versus ints).
 *
 * @author Timothy Gerard Endres, <time@gjt.org>
 */
public class TarInputStream extends WrapInputStream {

    protected final byte[] oneBuf = new byte[1];    
    protected boolean hasHitEOF;
    protected long entrySize;
    protected long entryOffset;
    protected byte[] readBuf;
    protected TarBuffer buffer;
    protected TarEntry currEntry;

    public TarInputStream(final ByteInputStream is) {
        this(is, TarBuffer.DEFAULT_BLKSIZE, TarBuffer.DEFAULT_RCDSIZE);
    }

    public TarInputStream(final ByteInputStream is, final int blockSize) {
        this(is, blockSize, TarBuffer.DEFAULT_RCDSIZE);
    }

    public TarInputStream(final ByteInputStream is, final int blockSize, final int recordSize) {
        super(is);
        this.buffer = new TarBuffer(is, blockSize, recordSize);
        this.readBuf = null;
        this.hasHitEOF = false;
    }

    /**
     * Closes this stream. Calls the TarBuffer's close() method.
     */
    public void close() throws IOException {
        this.buffer.close();
    }

    /**
     * Get the record size being used by this stream's TarBuffer.
     *
     * @return The TarBuffer record size.
     */
    public int getRecordSize() {
        return this.buffer.getRecordSize();
    }

    /**
     * Get the available data that can be read from the current entry in the
     * archive. This does not indicate how much data is left in the entire
     * archive, only in the current entry. This value is determined from the
     * entry's size header field and the amount of data already read from the
     * current entry.
     *
     *
     * @return The number of available bytes for the current entry.
     */
    public int available() throws IOException {
        return (int) (this.entrySize - this.entryOffset);
    }

    /**
     * Skip bytes in the input buffer. This skips bytes in the current entry's
     * data, not the entire archive, and will stop at the end of the current
     * entry's data if the number to skip extends beyond that point.
     *
     * @param numToSkip The number of bytes to skip.
     * @return The actual number of bytes skipped.
     */
    public long skip(long numToSkip) throws IOException {
        // REVIEW
        // This is horribly inefficient, but it ensures that we
        // properly skip over bytes via the TarBuffer...
        //

        final byte[] skipBuf = new byte[8 * 1024];
        long num = numToSkip;
        for (; num > 0;) {
            final int numRead = this.read(skipBuf, 0, (num > skipBuf.length ? skipBuf.length : (int) num));

            if (numRead == -1) {
                break;
            }

            num -= numRead;
        }

        return (numToSkip - num);
    }

    /**
     * Get the number of bytes into the current TarEntry. This method returns
     * the number of bytes that have been read from the current TarEntry's data.
     *
     * @returns The current entry offset.
     */
    public long getEntryPosition() {
        return this.entryOffset;
    }

    /**
     * Get the number of bytes into the stream we are currently at. This method
     * accounts for the blocking stream that tar uses, so it represents the
     * actual position in input stream, as opposed to the place where the tar
     * archive parsing is.
     *
     * @returns The current file pointer.
     */
    public long getStreamPosition() {
        return (buffer.getBlockSize() * buffer.getCurrentBlockNum()) + buffer.getCurrentRecordNum();
    }

    /**
     * Get the next entry in this tar archive. This will skip over any remaining
     * data in the current entry, if there is one, and place the input stream at
     * the header of the next entry, and read the header and instantiate a new
     * TarEntry from the header bytes and return that entry. If there are no
     * more entries in the archive, null will be returned to indicate that the
     * end of the archive has been reached.
     *
     * @return The next TarEntry in the archive, or null.
     */
    public TarEntry getNextEntry() throws IOException {
        if (this.hasHitEOF) {
            return null;
        }

        if (this.currEntry != null) {
            final long numToSkip = (this.entrySize - this.entryOffset);

            if (numToSkip > 0) {
                this.skip(numToSkip);
            }

            this.readBuf = null;
        }

        final byte[] headerBuf = this.buffer.readRecord();

        if (headerBuf == null || this.buffer.isEOFRecord(headerBuf)) {
            this.hasHitEOF = true;
        }

        if (this.hasHitEOF) {
            this.currEntry = null;
        } else {
            try {
                this.currEntry = new TarEntry(headerBuf);
                this.entryOffset = 0;
                this.entrySize = this.currEntry.getSize();
            } catch (InvalidHeaderException ex) {
                this.entrySize = 0;
                this.entryOffset = 0;
                this.currEntry = null;
                throw new InvalidHeaderException("bad header in block "
                        + this.buffer.getCurrentBlockNum()
                        + " record "
                        + this.buffer.getCurrentRecordNum()
                        + ", " + ex.getMessage());
            }
        }

        return this.currEntry;
    }

    /**
     * Reads a byte from the current tar archive entry.
     *
     * This method simply calls read( byte[], int, int ).
     *
     * @return The byte read, or -1 at EOF.
     */
    public int read() throws IOException {
        final int num = this.read(this.oneBuf, 0, 1);
        if (num == -1) {
            return num;
        } else {
            return (int) this.oneBuf[0];
        }
    }

    /**
     * Reads bytes from the current tar archive entry.
     *
     * This method simply calls read( byte[], int, int ).
     *
     * @param buf The buffer into which to place bytes read.
     * @return The number of bytes read, or -1 at EOF.
     */
    public int read(byte[] buf) throws IOException {
        return this.read(buf, 0, buf.length);
    }

    /**
     * Reads bytes from the current tar archive entry.
     *
     * This method is aware of the boundaries of the current entry in the
     * archive and will deal with them as if they were this stream's start and
     * EOF.
     *
     * @param buf The buffer into which to place bytes read.
     * @param offset The offset at which to place bytes read.
     * @param numToRead The number of bytes to read.
     * @return The number of bytes read, or -1 at EOF.
     */
    public int read(byte[] buf, int offset, int numToRead) throws IOException {
        int totalRead = 0;

        if (this.entryOffset >= this.entrySize) {
            return -1;
        }

        if ((numToRead + this.entryOffset) > this.entrySize) {
            numToRead = (int) (this.entrySize - this.entryOffset);
        }

        if (this.readBuf != null) {
            int sz = (numToRead > this.readBuf.length)
                    ? this.readBuf.length : numToRead;

            System.arraycopy(this.readBuf, 0, buf, offset, sz);

            if (sz >= this.readBuf.length) {
                this.readBuf = null;
            } else {
                int newLen = this.readBuf.length - sz;
                byte[] newBuf = new byte[newLen];
                System.arraycopy(this.readBuf, sz, newBuf, 0, newLen);
                this.readBuf = newBuf;
            }

            totalRead += sz;
            numToRead -= sz;
            offset += sz;
        }

        for (; numToRead > 0;) {
            byte[] rec = this.buffer.readRecord();
            if (rec == null) {
                // Unexpected EOF!
                throw new IOException("unexpected EOF with " + numToRead + " bytes unread");
            }

            int sz = numToRead;
            int recLen = rec.length;

            if (recLen > sz) {
                System.arraycopy(rec, 0, buf, offset, sz);
                this.readBuf = new byte[recLen - sz];
                System.arraycopy(rec, sz, this.readBuf, 0, recLen - sz);
            } else {
                sz = recLen;
                System.arraycopy(rec, 0, buf, offset, recLen);
            }

            totalRead += sz;
            numToRead -= sz;
            offset += sz;
        }

        this.entryOffset += totalRead;

        return totalRead;
    }

    /**
     * Copies the contents of the current tar archive entry directly into an
     * output stream.
     *
     * @param out The OutputStream into which to write the entry's data.
     */
    public void copyEntryContents(ByteOutputStream out) throws IOException {
        final byte[] buf = new byte[32 * 1024];

        for (;;) {
            int numRead = this.read(buf, 0, buf.length);
            if (numRead == -1) {
                break;
            }
            out.write(buf, 0, numRead);
        }
    }

}
