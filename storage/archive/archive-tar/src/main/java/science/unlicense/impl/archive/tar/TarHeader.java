package science.unlicense.impl.archive.tar;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;

/**
 * This class encapsulates the Tar Entry Header used in Tar Archives. The class
 * also holds a number of tar constants, used mostly in headers.
 *
 * @author Timothy Gerard Endres, <time@gjt.org>
 */
public class TarHeader {

    /**
     * The entry's name.
     */
    public Chars name;
    /**
     * The entry's permission mode.
     */
    public int mode;
    /**
     * The entry's user id.
     */
    public int userId;
    /**
     * The entry's group id.
     */
    public int groupId;
    /**
     * The entry's size.
     */
    public long size;
    /**
     * The entry's modification time.
     */
    public long modTime;
    /**
     * The entry's checksum.
     */
    public int checkSum;
    /**
     * The entry's link flag.
     */
    public byte linkFlag;
    /**
     * The entry's link name.
     */
    public Chars linkName;
    /**
     * The entry's magic tag.
     */
    public Chars magic;
    /**
     * The entry's user name.
     */
    public Chars userName;
    /**
     * The entry's group name.
     */
    public Chars groupName;
    /**
     * The entry's major device number.
     */
    public int devMajor;
    /**
     * The entry's minor device number.
     */
    public int devMinor;

    public TarHeader() {
        this.magic = TarConstants.TMAGIC;

        this.name = Chars.EMPTY;
        this.linkName = Chars.EMPTY;

        String user = System.getProperty("user.name", "");

        if (user.length() > 31) {
            user = user.substring(0, 31);
        }

        this.userId = 0;
        this.groupId = 0;
        this.userName = new Chars(user);
        this.groupName = Chars.EMPTY;
    }

    /**
     * Get the name of this entry.
     *
     * @return Teh entry's name.
     */
    public Chars getName() {
        return this.name;
    }

    /**
     * Parse an octal string from a header buffer. This is used for the file
     * permission mode value.
     *
     * @param header The header buffer from which to parse.
     * @param offset The offset into the buffer from which to parse.
     * @param length The number of header bytes to parse.
     * @return The long value of the octal string.
     */
    public static long parseOctal(byte[] header, int offset, int length) throws InvalidHeaderException {
        long result = 0;
        boolean stillPadding = true;

        int end = offset + length;
        for (int i = offset; i < end; ++i) {
            if (header[i] == 0) {
                break;
            }

            if (header[i] == (byte) ' ' || header[i] == '0') {
                if (stillPadding) {
                    continue;
                }

                if (header[i] == (byte) ' ') {
                    break;
                }
            }

            stillPadding = false;

            result = (result << 3) + (header[i] - '0');
        }

        return result;
    }

    /**
     * Parse a file name from a header buffer. This is different from
     * parseName() in that is recognizes 'ustar' names and will handle adding on
     * the "prefix" field to the name.
     *
     * Contributed by Dmitri Tikhonov <dxt2431@yahoo.com>
     *
     * @param header The header buffer from which to parse.
     * @param offset The offset into the buffer from which to parse.
     * @param length The number of header bytes to parse.
     * @return The header's entry name.
     */
    public static Chars parseFileName(byte[] header) {
        final CharBuffer result = new CharBuffer();

        // If header[345] is not equal to zero, then it is the "prefix"
        // that 'ustar' defines. It must be prepended to the "normal"
        // name field. We are responsible for the separating '/'.
        //
        if (header[345] != 0) {
            for (int i = 345; i < 500 && header[i] != 0; ++i) {
                result.append((char) header[i]);
            }

            result.append("/");
        }

        for (int i = 0; i < 100 && header[i] != 0; ++i) {
            result.append((char) header[i]);
        }

        return result.toChars();
    }

    /**
     * Parse an entry name from a header buffer.
     *
     * @param header The header buffer from which to parse.
     * @param offset The offset into the buffer from which to parse.
     * @param length The number of header bytes to parse.
     * @return The header's entry name.
     */
    public static Chars parseName(byte[] header, int offset, int length) throws InvalidHeaderException {
        final CharBuffer result = new CharBuffer();

        int end = offset + length;
        for (int i = offset; i < end; ++i) {
            if (header[i] == 0) {
                break;
            }
            result.append((char) header[i]);
        }

        return result.toChars();
    }

    /**
     * This method, like getNameBytes(), is intended to place a name into a
     * TarHeader's buffer. However, this method is sophisticated enough to
     * recognize long names (name.length() > NAMELEN). In these cases, the
     * method will break the name into a prefix and suffix and place the name in
     * the header in 'ustar' format. It is up to the TarEntry to manage the
     * "entry header format". This method assumes the name is valid for the type
     * of archive being generated.
     *
     * @param outbuf The buffer containing the entry header to modify.
     * @param newName The new name to place into the header buffer.
     * @return The current offset in the tar header (always TarHeader.NAMELEN).
     * @throws InvalidHeaderException If the name will not fit in the header.
     */
    public static int getFileNameBytes(Chars newName, byte[] outbuf) throws InvalidHeaderException {
        if (newName.getByteLength()> 100) {
            // Locate a pathname "break" prior to the maximum name length...
            int index = newName.getFirstOccurence('/', newName.getByteLength()- 100);
            if (index == -1) {
                throw new InvalidHeaderException("file name is greater than 100 characters, " + newName);
            }

            // Get the "suffix subpath" of the name.
            Chars name = newName.truncate(index + 1,-1);

            // Get the "prefix subpath", or "prefix", of the name.
            Chars prefix = newName.truncate(0, index);
            if (prefix.getByteLength()> TarConstants.PREFIXLEN) {
                throw new InvalidHeaderException("file prefix is greater than 155 characters");
            }

            TarHeader.getNameBytes(name, outbuf, TarConstants.NAMEOFFSET, TarConstants.NAMELEN);

            TarHeader.getNameBytes(prefix, outbuf, TarConstants.PREFIXOFFSET, TarConstants.PREFIXLEN);
        } else {
            TarHeader.getNameBytes(newName, outbuf, TarConstants.NAMEOFFSET, TarConstants.NAMELEN);
        }

        // The offset, regardless of the format, is now the end of the
        // original name field.
        //
        return TarConstants.NAMELEN;
    }

    /**
     * Move the bytes from the name StringBuffer into the header's buffer.
     *
     * @param header The header buffer into which to copy the name.
     * @param offset The offset into the buffer at which to store.
     * @param length The number of header bytes to store.
     * @return The new offset (offset + length).
     */
    public static int getNameBytes(Chars name, byte[] buf, int offset, int length) {
        int i;

        for (i = 0; i < length && i < name.getByteLength(); ++i) {
            buf[ offset + i] = (byte) name.getByte(i);
        }

        for (; i < length; ++i) {
            buf[ offset + i] = 0;
        }

        return offset + length;
    }

    /**
     * Parse an octal integer from a header buffer.
     *
     * @param header The header buffer from which to parse.
     * @param offset The offset into the buffer from which to parse.
     * @param length The number of header bytes to parse.
     * @return The integer value of the octal bytes.
     */
    public static int getOctalBytes(long value, byte[] buf, int offset, int length) {
        byte[] result = new byte[length];

        int idx = length - 1;

        buf[ offset + idx] = 0;
        --idx;
        buf[ offset + idx] = (byte) ' ';
        --idx;

        if (value == 0) {
            buf[ offset + idx] = (byte) '0';
            --idx;
        } else {
            for (long val = value; idx >= 0 && val > 0; --idx) {
                buf[ offset + idx] = (byte) ((byte) '0' + (byte) (val & 7));
                val = val >> 3;
            }
        }

        for (; idx >= 0; --idx) {
            buf[ offset + idx] = (byte) ' ';
        }

        return offset + length;
    }

    /**
     * Parse an octal long integer from a header buffer.
     *
     * @param header The header buffer from which to parse.
     * @param offset The offset into the buffer from which to parse.
     * @param length The number of header bytes to parse.
     * @return The long value of the octal bytes.
     */
    public static int getLongOctalBytes(long value, byte[] buf, int offset, int length) {
        final byte[] temp = new byte[length + 1];
        TarHeader.getOctalBytes(value, temp, 0, length + 1);
        System.arraycopy(temp, 0, buf, offset, length);
        return offset + length;
    }

    /**
     * Parse the checksum octal integer from a header buffer.
     *
     * @param header The header buffer from which to parse.
     * @param offset The offset into the buffer from which to parse.
     * @param length The number of header bytes to parse.
     * @return The integer value of the entry's checksum.
     */
    public static int getCheckSumOctalBytes(long value, byte[] buf, int offset, int length) {
        TarHeader.getOctalBytes(value, buf, offset, length);
        buf[ offset + length - 1] = (byte) ' ';
        buf[ offset + length - 2] = 0;
        return offset + length;
    }
}
