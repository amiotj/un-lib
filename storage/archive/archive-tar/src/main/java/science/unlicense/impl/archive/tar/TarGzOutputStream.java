package science.unlicense.impl.archive.tar;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.io.gzip.GZipOutputStream;


// we extend TarOutputStream to have the same type, 
// BUT, we don't use ANY methods. It's all about 
// typing.
/**
 * Outputs tar.gz files. Added functionality that it doesn't need to know the
 * size of an entry. If an entry has zero size when it is put in the Tar, then
 * it buffers it until it's closed and it knows the size.
 *
 * @author "Bay" <bayard@generationjava.com>
 */
public class TarGzOutputStream extends TarOutputStream {

    private TarOutputStream tos = null;
    private GZipOutputStream gzip = null;
    private ArrayOutputStream bos = null;
    private TarEntry currentEntry = null;

    public TarGzOutputStream(ByteOutputStream out) throws IOException {
        super(null);
        this.gzip = new GZipOutputStream(out);
        this.tos = new TarOutputStream(this.gzip);
        this.bos = new ArrayOutputStream();
    }

    public void finish() throws IOException {
        if (this.currentEntry != null) {
            closeEntry();
        }

        this.tos.finish();
    }

    public void close() throws IOException {
        this.tos.close();
        this.gzip.finish();
    }

    public int getRecordSize() {
        return this.tos.getRecordSize();
    }

    public void putNextEntry(TarEntry entry) throws IOException {
        if (entry.getSize() != 0) {
            this.tos.putNextEntry(entry);
        } else {
            this.currentEntry = entry;
        }
    }

    public void closeEntry() throws IOException {
        if (this.currentEntry == null) {
            this.tos.closeEntry();
        } else {
            final ByteSequence buffer = bos.getBuffer();
            this.currentEntry.setSize(buffer.getSize());
            this.tos.putNextEntry(this.currentEntry);
            this.tos.write(buffer.toArrayByte());
            this.tos.closeEntry();
            this.currentEntry = null;
            this.bos = new ArrayOutputStream();
        }
    }

    public void write(byte b) throws IOException {
        if (this.currentEntry == null) {
            this.tos.write(b);
        } else {
            this.bos.write(b);
        }
    }

    public void write(byte[] b) throws IOException {
        if (this.currentEntry == null) {
            this.tos.write(b);
        } else {
            this.bos.write(b);
        }
    }

    public void write(byte[] b, int start, int length) throws IOException {
        if (this.currentEntry == null) {
            this.tos.write(b, start, length);
        } else {
            this.bos.write(b, start, length);
        }
    }
}
