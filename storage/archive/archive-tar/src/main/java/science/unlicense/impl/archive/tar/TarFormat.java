

package science.unlicense.impl.archive.tar;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.archive.AbstractArchiveFormat;
import science.unlicense.api.path.Path;
import science.unlicense.api.archive.Archive;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class TarFormat extends AbstractArchiveFormat{

    private static final DocumentType PARAMETERS_TYPE = new DefaultDocumentType();
    private static final Chars SUFFIX = new Chars(".tar");
    
    public TarFormat() {
        super(new Chars("tar"),
              new Chars("TAR"),
              new Chars("TAR"),
              new Chars[]{
                  new Chars("application/x-tar")
              },
              new Chars[]{
                  new Chars("tar")
              },
              new byte[0][0]);
    }

    public DocumentType getParameters() {
        return PARAMETERS_TYPE;
    }

    public boolean canOpen(Path candidate) {
        return candidate.getName().toLowerCase().endsWith(SUFFIX);
    }

    public Archive open(Path candidate, Document params) {
        return null;
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    public PathResolver createResolver(Path base) throws IOException {
        return open(base,null);
    }

}