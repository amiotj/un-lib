

package science.unlicense.impl.archive.ar;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.archive.Archive;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class ARArchive extends AbstractPath implements Archive{

    private final Path base;

    public ARArchive(Path base) {
        this.base = base;
    }
    
    public PathFormat getFormat() {
        return ARFormat.INSTANCE;
    }

    public boolean canHaveChildren() {
        return true;
    }

    public Chars getName() {
        return base.getName();
    }

    public Path getParent() {
        return base.getParent();
    }

    public boolean isContainer() throws IOException {
        return true;
    }

    public boolean exists() throws IOException {
        return base.exists();
    }

    public Class[] getEventClasses() {
        return new Class[]{NodeMessage.class};
    }

    public Collection getChildren() {
        throw new RuntimeException("todo");
    }

    public Path resolve(Chars address) {
        if(address==null) return this;

        if(address.startsWith('/')){
            address = address.truncate(1,-1);
        }

        throw new RuntimeException("todo");
    }

    public PathResolver getResolver() {
        return this;
    }

    public ByteInputStream createInputStream() throws IOException {
        return base.createInputStream();
    }

    public Chars thisToChars(){
        return new Chars("AR archive : "+getName());
    }

    public Chars toURI() {
        return base.toURI();
    }

    public Chars toChars() {
        return base.toChars();
    }

    // WRITING OPERATIONS NOT SUPPORTED YET ////////////////////////////////////

    public boolean createContainer() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public boolean createLeaf() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}