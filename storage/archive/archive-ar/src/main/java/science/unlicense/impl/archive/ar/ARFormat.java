

package science.unlicense.impl.archive.ar;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.archive.AbstractArchiveFormat;
import science.unlicense.api.path.Path;
import science.unlicense.api.archive.Archive;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class ARFormat extends AbstractArchiveFormat{

    public static final ARFormat INSTANCE = new ARFormat();
    private static final Chars SUFFIX = new Chars(".ar");

    private ARFormat() {
        super(new Chars("ar"),
              new Chars("AR"),
              new Chars("AR"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("ar")
              },
              new byte[][]{ARConstants.SIGNATURE});
    }

    public boolean canOpen(Path candidate) {
        return candidate.getName().toLowerCase().endsWith(SUFFIX);
    }

    public Archive open(Path candidate, Document params) {
        final ARArchive arc = new ARArchive(candidate);
        return arc;
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    public PathResolver createResolver(Path base) throws IOException {
        return open(base,null);
    }

}