
package science.unlicense.impl.archive.sevenz;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/7z
 * http://cpansearch.perl.org/src/BJOERN/Compress-Deflate7-1.0/7zip/DOC/7zFormat.txt
 *
 * @author Johann Sorel
 */
public class SevenZConstants {

    public static final byte[] SIGNATURE = new byte[]{'7', 'z', (byte)0xBC, (byte)0xAF, (byte)0x27, (byte)0x1C};

    

}
