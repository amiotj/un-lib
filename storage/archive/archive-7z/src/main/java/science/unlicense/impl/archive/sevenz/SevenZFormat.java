

package science.unlicense.impl.archive.sevenz;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.archive.AbstractArchiveFormat;
import science.unlicense.api.path.Path;
import science.unlicense.api.archive.Archive;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class SevenZFormat extends AbstractArchiveFormat{

    public static final SevenZFormat INSTANCE = new SevenZFormat();
    private static final Chars SUFFIX = new Chars(".7z");

    private SevenZFormat() {
        super(new Chars("7z"),
              new Chars("7ZIP"),
              new Chars("7ZIP"),
              new Chars[]{
                  new Chars("application/x-7z-compressed")
              },
              new Chars[]{
                  new Chars("7z")
              },
              new byte[0][0]);
    }

    public boolean canOpen(Path candidate) {
        return candidate.getName().toLowerCase().endsWith(SUFFIX);
    }

    public Archive open(Path candidate, Document params) {
        final SevenZArchive arc = new SevenZArchive(candidate);
        return arc;
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    public PathResolver createResolver(Path base) throws IOException {
        return open(base,null);
    }

}