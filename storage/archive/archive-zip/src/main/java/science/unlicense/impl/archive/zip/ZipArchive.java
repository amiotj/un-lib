

package science.unlicense.impl.archive.zip;

import science.unlicense.api.CObjects;
import science.unlicense.api.Sorter;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.archive.Archive;
import science.unlicense.impl.archive.zip.model.ZipCentralDirectory;
import science.unlicense.impl.archive.zip.model.ZipFileEntry;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class ZipArchive extends AbstractPath implements Archive{

    /**
     * Sort zip entries by path length.
     */
    private static final Sorter ENTRY_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            final Chars[] p1 = ((ZipFileEntry)first).getPathChars();
            final Chars[] p2 = ((ZipFileEntry)second).getPathChars();
            final int diff = Math.min(p1.length,p2.length);
            for(int i=0;i<diff;i++){
                //sort by path names
                int c = p1[i].order(p2[i]);
                if(c!=0) return c;
            }
            if(p2.length>p1.length){
                return -1;
            }else if(p2.length<p1.length){
                return +1;
            }else{
                throw new RuntimeException("2 identical paths, not possible"+((ZipFileEntry)first).name);
            }
        }
    };

    private final PathFormat format;
    private final Path base;
    private Sequence children;
    private final Dictionary entries = new HashDictionary();
    private CharEncoding encoding = CharEncodings.DEFAULT;

    public ZipArchive(PathFormat format, Path base) {
        this.format = format;
        this.base = base;
    }

    /**
     * Must be set before any reading.
     * 
     * @param enc not null
     */
    public void setEncoding(CharEncoding enc) {
        CObjects.ensureNotNull(enc);
        this.encoding = enc;
    }

    public CharEncoding getEncoding() {
        return encoding;
    }
    
    public PathFormat getFormat() {
        return format;
    }

    public boolean canHaveChildren() {
        return true;
    }

    public Chars getName() {
        return base.getName();
    }

    public Path getParent() {
        return base.getParent();
    }

    public boolean isContainer() throws IOException {
        return true;
    }

    public boolean exists() throws IOException {
        return base.exists();
    }

    public Class[] getEventClasses() {
        return new Class[]{NodeMessage.class};
    }

    public Sequence getChildren() {
        analyze();
        return Collections.readOnlySequence(children);
    }

    public Path resolve(Chars address) {
        if(address==null) return this;

        if(address.startsWith('/')){
            address = address.truncate(1,-1);
        }

        analyze();
        Path candidate = (Path)entries.getValue(address);
        if(candidate == null){
            candidate = (Path)entries.getValue(address.concat('/'));
        }
        return candidate;
    }

    public PathResolver getResolver() {
        return this;
    }

    public ByteInputStream createInputStream() throws IOException {
        return base.createInputStream();
    }

    public Chars thisToChars(){
        return new Chars("Zip archive : "+getName());
    }

    public Chars toURI() {
        return base.toURI();
    }

    private void analyze(){
        if(children!=null) return;

        children = new ArraySequence();

        final ByteInputStream bi;
        try {
            bi = base.createInputStream();

            final ZipIterator ite = new ZipIterator(bi,encoding);
            final Sequence entries = new ArraySequence();
            while(ite.hasNext()){
                final Object obj = ite.next();

                if(obj instanceof ZipCentralDirectory){
                    final ZipCentralDirectory dir = (ZipCentralDirectory) obj;
                    //TODO we should use this, but for now we use the found file entries directly

                }else if(obj instanceof ZipFileEntry){
                    final ZipFileEntry entry = (ZipFileEntry) obj;
                    entries.add(entry);
                }
            }
            bi.close();

            //sort entries by path length
            Collections.sort(entries, ENTRY_SORTER);

            //rebuild archive paths
            for(int i=0,n=entries.getSize();i<n;i++){
                create((ZipFileEntry)entries.get(i));
            }

        } catch (IOException ex) {
            Loggers.get().log(ex, science.unlicense.api.logging.Logger.LEVEL_WARNING);
        }

    }

    private void create(ZipFileEntry entry){
        final Chars[] path = entry.getPathChars();

        Path parent;
        if(path.length==1){
            parent = this;
        }else{
            parent = resolve(entry.getParentPath());
        }

        if(parent == null){
            //path structure is not or only partialy declared in file entries
            parent = this;
            Chars fullPath = Chars.EMPTY;
            for(int i=0;i<path.length-1;i++){
                fullPath = fullPath.concat(path[i]).concat('/');
                parent = getOrCreateDirectory(parent, path[i], fullPath);
            }
        }

        final ZipArchivePath zp = new ZipArchivePath(parent, null, entry);
        entries.add(entry.name, zp);

        if(parent == this){
            children.add(zp);
        }else{
            ((ZipArchivePath)parent).addChild(zp);
        }

    }

    private Path getOrCreateDirectory(Path parent, Chars name, Chars basePath){
        Path path = resolve(name);
        if(path == null){
            //create it
            final ZipFileEntry entry = new ZipFileEntry();
            entry.name = basePath;
            entry.compression = ZipMetaModel.COMPRESSION_NONE;
            final ZipArchivePath zp = new ZipArchivePath(parent, null, entry);
            path = zp;

            if(parent == this){
                children.add(zp);
                entries.add(basePath, zp);
            }else{
                ((ZipArchivePath)parent).addChild(zp);
            }
        }

        return path;
    }

    public Chars toChars() {
        return base.toChars();
    }

    // WRITING OPERATIONS NOT SUPPORTED YET ////////////////////////////////////

    public boolean createContainer() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public boolean createLeaf() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}