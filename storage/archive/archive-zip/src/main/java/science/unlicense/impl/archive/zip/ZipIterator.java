
package science.unlicense.impl.archive.zip;

import science.unlicense.impl.archive.zip.model.ZipCentralDirectoryEnd;
import science.unlicense.impl.archive.zip.model.ZipFileEntry;
import science.unlicense.impl.archive.zip.model.ZipBlock;
import science.unlicense.impl.archive.zip.model.Zip64CentralDirectoryLocator;
import science.unlicense.impl.archive.zip.model.ZipCentralDirectory;
import science.unlicense.impl.archive.zip.model.Zip64CentralDirectoryRecord;
import science.unlicense.impl.archive.zip.model.ZipDigitalSignature;
import science.unlicense.impl.archive.zip.model.ZipExtraRecord;
import science.unlicense.api.CObject;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.impl.io.CounterInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Iterator on each block in the zip.
 *
 * @author Johann Sorel
 */
public class ZipIterator extends CObject {

    private final CounterInputStream cs;
    private final DataInputStream ds;
    private final CharEncoding encoding;
    private ZipBlock next = null;

    public ZipIterator(ByteInputStream ds, CharEncoding encoding) {
        this.cs = new CounterInputStream(ds);
        this.ds = new DataInputStream(cs, NumberEncoding.LITTLE_ENDIAN);
        this.encoding = encoding;
    }

    public boolean hasNext() throws IOException {
        findNext();
        return next != null;
    }

    public Object next() throws IOException {
        findNext();
        if(next == null) throw new InvalidArgumentException("No more elements.");
        ZipBlock b = next;
        next = null;
        return b;
    }

    private void findNext() throws IOException{
        if(next != null) return;

        //[local file header 1]
        //[encryption header 1]
        //[file data 1]
        //[data descriptor 1]
        //.
        //.
        //.
        //[local file header n]
        //[encryption header n]
        //[file data n]
        //[data descriptor n]
        //[archive decryption header]
        //[archive extra data record]
        //[central directory header 1]
        //.
        //.
        //.
        //[central directory header n]
        //[zip64 end of central directory record]
        //[zip64 end of central directory locator]
        //[end of central directory record]

        while(true){
            final long fileOffset = cs.getCounterValue();
            final int signature;
            try{
                signature = ds.readInt();
            }catch(IOException ex){
                break;
            }
            if(signature == ZipMetaModel.SIGNATURE_LOCAL_FILE){
                final ZipFileEntry entry = readFileEntry();
                //TODO skip compressed data
                ds.skipFully(entry.sizeCompressed);
                next = entry;
                next.fileOffset = fileOffset;
                break;

            }else if(signature == ZipMetaModel.SIGNATURE_EXTRA_RECORD){
                next = readExtraRecord();
                next.fileOffset = fileOffset;
                break;

            }else if(signature == ZipMetaModel.SIGNATURE_CENTRAL_DIRECTORY){
                next = readCentralDirectory();
                next.fileOffset = fileOffset;
                break;

            }else if(signature == ZipMetaModel.SIGNATURE_DIGITAL_SIGNATURE){
                next = readDigitalSignature();
                next.fileOffset = fileOffset;
                break;

            }else if(signature == ZipMetaModel.SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_RECORD){
                next = readCentralDirectoryRecord();
                next.fileOffset = fileOffset;
                break;

            }else if(signature == ZipMetaModel.SIGNATURE_ZIP64_END_CENTRAL_DIRECTORY_LOCATOR){
                next = readCentralDirectoryLocator();
                next.fileOffset = fileOffset;
                break;

            }else if(signature == ZipMetaModel.SIGNATURE_END_CENTRAL_DIRECTORY){
                next = readCentralDirectoryEnd();
                next.fileOffset = fileOffset;
                break;
            }else {
                throw new IOException("Unknowned tag : " + signature);
            }
        }
    }

    private ZipFileEntry readFileEntry() throws IOException{
        //local file header signature     4 bytes  (0x04034b50)
        //version needed to extract       2 bytes
        //general purpose bit flag        2 bytes
        //compression method              2 bytes
        //last mod file time              2 bytes
        //last mod file date              2 bytes
        //crc-32                          4 bytes
        //compressed size                 4 bytes
        //uncompressed size               4 bytes
        //file name length                2 bytes
        //extra field length              2 bytes
        //
        //file name (variable size)
        //extra field (variable size)

        final ZipFileEntry entry = new ZipFileEntry();
        entry.version = ds.readUShort();
        entry.flags = ds.readUShort();
        entry.compression = ds.readUShort();
        entry.last_modified_time = ds.readUShort();
        entry.last_modified_date = ds.readUShort();
        entry.crc32 = ds.readInt();
        entry.sizeCompressed = ds.readInt();
        entry.sizeUncompressed = ds.readInt();
        final int filenameLength = ds.readUShort();
        final int extraLength = ds.readUShort();

        entry.name = new Chars(ds.readFully(new byte[filenameLength]),encoding);
        entry.extra = ds.readFully(new byte[extraLength]);

        //store the data file offset
        entry.dataFileOffset = cs.getCounterValue();

        return entry;
    }

    private ZipExtraRecord readExtraRecord() throws IOException {
        //archive extra data signature    4 bytes  (0x08064b50)
        //extra field length              4 bytes
        //extra field data                (variable size)
        final ZipExtraRecord data = new ZipExtraRecord();
        final int fieldLength = ds.readUShort();
        data.fieldData = ds.readFully(new byte[fieldLength]);

        return data;
    }

    private ZipDigitalSignature readDigitalSignature() throws IOException {
        //header signature                4 bytes  (0x05054b50)
        //size of data                    2 bytes
        //signature data (variable size)
        final ZipDigitalSignature signature = new ZipDigitalSignature();
        final int signatureSize = ds.readUShort();
        signature.signature =  ds.readFully(new byte[signatureSize]);

        return signature;
    }

    private ZipCentralDirectory readCentralDirectory() throws IOException{
        //central file header signature   4 bytes  (0x02014b50)
        //version made by                 2 bytes
        //version needed to extract       2 bytes
        //general purpose bit flag        2 bytes
        //compression method              2 bytes
        //last mod file time              2 bytes
        //last mod file date              2 bytes
        //crc-32                          4 bytes
        //compressed size                 4 bytes
        //uncompressed size               4 bytes
        //file name length                2 bytes
        //extra field length              2 bytes
        //file comment length             2 bytes
        //disk number start               2 bytes
        //internal file attributes        2 bytes
        //external file attributes        4 bytes
        //relative offset of local header 4 bytes
        //
        //file name (variable size)
        //extra field (variable size)
        //file comment (variable size)

        final ZipCentralDirectory dir = new ZipCentralDirectory();
        dir.version_made = ds.readUShort();
        dir.version_extract = ds.readUShort();
        dir.flags = ds.readUShort();
        dir.compression = ds.readUShort();
        dir.last_modified_time = ds.readUShort();
        dir.last_modified_date = ds.readUShort();
        dir.crc32 = ds.readInt();
        dir.sizeCompressed = ds.readInt();
        dir.sizeUncompressed = ds.readInt();
        final int filenameLength = ds.readUShort();
        final int extraLength = ds.readUShort();
        final int commentLength = ds.readUShort();
        dir.diskNumberStart = ds.readUShort();
        dir.internalFileAttributes = ds.readUShort();
        dir.externalFileAttributes = ds.readInt();
        dir.relativeOffsetLocalHeader = ds.readInt();

        dir.name = new Chars(ds.readFully(new byte[filenameLength]),encoding);
        dir.extra = ds.readFully(new byte[extraLength]);
        dir.comment = new Chars(ds.readFully(new byte[commentLength]),encoding);

        return dir;
    }

    private Zip64CentralDirectoryRecord readCentralDirectoryRecord() throws IOException{
        //zip64 end of central dir
        //signature                       4 bytes  (0x06064b50)
        //size of zip64 end of central
        //directory record                8 bytes
        //version made by                 2 bytes
        //version needed to extract       2 bytes
        //number of this disk             4 bytes
        //number of the disk with the
        //start of the central directory  4 bytes
        //total number of entries in the
        //central directory on this disk  8 bytes
        //total number of entries in the
        //central directory               8 bytes
        //size of the central directory   8 bytes
        //offset of start of central
        //directory with respect to
        //the starting disk number        8 bytes
        //zip64 extensible data sector    (variable size)
        final Zip64CentralDirectoryRecord record = new Zip64CentralDirectoryRecord();
        record.directoryRecord = ds.readLong();
        record.version_made = ds.readUShort();
        record.version_extract = ds.readUShort();
        record.numberDisk = ds.readInt();
        record.numberDiskInCentralDir = ds.readInt();
        record.numberEntriesOnDisk = ds.readLong();
        record.numberEntriesInCentralDir = ds.readLong();
        record.centralDirSize = ds.readLong();
        record.centralDirOffset = ds.readLong();

        return record;
    }

    private Zip64CentralDirectoryLocator readCentralDirectoryLocator() throws IOException{
        //zip64 end of central dir locator
        //signature                       4 bytes  (0x07064b50)
        //number of the disk with the
        //start of the zip64 end of
        //central directory               4 bytes
        //relative offset of the zip64
        //end of central directory record 8 bytes
        //total number of disks           4 bytes
        final Zip64CentralDirectoryLocator locator = new Zip64CentralDirectoryLocator();
        locator.diskNumber = ds.readInt();
        locator.relativeOffset = ds.readLong();
        locator.numberOfDisks = ds.readInt();
        return locator;
    }

    private ZipCentralDirectoryEnd readCentralDirectoryEnd() throws IOException{
        //end of central dir signature    4 bytes  (0x06054b50)
        //number of this disk             2 bytes
        //number of the disk with the
        //start of the central directory  2 bytes
        //total number of entries in the
        //central directory on this disk  2 bytes
        //total number of entries in
        //the central directory           2 bytes
        //size of the central directory   4 bytes
        //offset of start of central
        //directory with respect to
        //the starting disk number        4 bytes
        //.ZIP file comment length        2 bytes
        //.ZIP file comment       (variable size)
        final ZipCentralDirectoryEnd end = new ZipCentralDirectoryEnd();
        end.numberDisk = ds.readUShort();
        end.numberDiskWithCentralDir = ds.readUShort();
        end.numberEntryOnDisk = ds.readUShort();
        end.numberEntryInCentralDir = ds.readUShort();
        end.directorySize = ds.readInt();
        end.offsetCentralDir = ds.readInt();
        final int commentLength = ds.readUShort();
        end.comment = new Chars(ds.readFully(new byte[commentLength]),encoding);

        return end;
    }

}