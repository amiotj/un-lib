
package science.unlicense.impl.archive.zip.model;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;

/**
 * A File entry in a Zip archive.
 *
 * @author Johann Sorel
 */
public class ZipFileEntry extends ZipBlock {

    private static final Char DIRECTORY_SUFFIX = new Char('/');

    public int version;
    public int flags;
    public int compression;
    public int last_modified_time;
    public int last_modified_date;
    public int crc32;
    public int sizeCompressed;
    public int sizeUncompressed;

    public Chars name;
    public byte[] extra;

    //additional information for the compressed data file offset
    public long dataFileOffset;

    /**
     * File entry path chars from archive root
     * @return Chars array
     */
    public Chars[] getPathChars(){

        Chars name = this.name;
        if(isContainer()){
            name = name.truncate(0, name.getCharLength()-1);
        }

        return name.split('/');
    }

    /**
     * Name without full path.
     * @return
     */
    public Chars getSimpleName() {
        final Chars[] path = getPathChars();
        return path[path.length-1];
    }

    /**
     * Path to entry, without file entry simple name.
     * @return
     */
    public Chars getParentPath(){
        final int size = getSimpleName().getCharLength();
        return name.truncate(0, name.getCharLength() - size - ((isContainer())?1:0) );
    }

    /**
     * Indicate if this file entry is a container/folder.
     * @return true if entry is a container
     */
    public boolean isContainer(){
        return name.endsWith(DIRECTORY_SUFFIX);
    }

    public Chars toChars() {
        return new Chars(getClass().getSimpleName()).concat(' ').concat(name);
    }

}