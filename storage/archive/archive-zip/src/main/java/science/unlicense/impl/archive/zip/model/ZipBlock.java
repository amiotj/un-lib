
package science.unlicense.impl.archive.zip.model;

import science.unlicense.api.CObject;

/**
 * A Zip Block element.
 *
 * @author Johann Sorel
 */
public abstract class ZipBlock extends CObject {

    public long fileOffset;

}