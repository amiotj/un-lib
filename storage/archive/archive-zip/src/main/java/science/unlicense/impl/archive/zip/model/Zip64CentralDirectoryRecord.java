
package science.unlicense.impl.archive.zip.model;

/**
 *
 * @author Johann Sorel
 */
public class Zip64CentralDirectoryRecord extends ZipBlock {

    public long directoryRecord;
    public int version_made;
    public int version_extract;
    public int numberDisk;
    public int numberDiskInCentralDir;
    public long numberEntriesOnDisk;
    public long numberEntriesInCentralDir;
    public long centralDirSize;
    public long centralDirOffset;
    public byte[] extensionDataSector;
}