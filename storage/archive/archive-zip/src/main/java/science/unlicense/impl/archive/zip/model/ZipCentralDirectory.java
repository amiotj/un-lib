
package science.unlicense.impl.archive.zip.model;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class ZipCentralDirectory extends ZipBlock {

    public int version_made;
    public int version_extract;
    public int flags;
    public int compression;
    public int last_modified_time;
    public int last_modified_date;
    public int crc32;
    public int sizeCompressed;
    public int sizeUncompressed;
    public int diskNumberStart;
    public int internalFileAttributes;
    public int externalFileAttributes;
    public int relativeOffsetLocalHeader;

    public Chars name;
    public byte[] extra;
    public Chars comment;

    public Chars toChars() {
        return new Chars(getClass().getSimpleName()).concat(' ').concat(name);
    }
}