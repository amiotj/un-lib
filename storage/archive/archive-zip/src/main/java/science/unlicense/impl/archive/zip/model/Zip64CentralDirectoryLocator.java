
package science.unlicense.impl.archive.zip.model;

/**
 *
 * @author Johann Sorel
 */
public class Zip64CentralDirectoryLocator extends ZipBlock {

    public int diskNumber;
    public long relativeOffset;
    public int numberOfDisks;

}