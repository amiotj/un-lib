
package science.unlicense.impl.archive.zip.model;


/**
 *
 * @author Johann Sorel
 */
public class ZipDigitalSignature extends ZipBlock {

    public byte[] signature;

}