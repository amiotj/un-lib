
package science.unlicense.impl.archive.zip;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.impl.io.ClipInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.impl.io.deflate.DeflateInputStream;
import science.unlicense.impl.archive.zip.model.ZipCentralDirectory;
import science.unlicense.impl.archive.zip.model.ZipFileEntry;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;

/**
 * A Path inside a zip.
 *
 * @author Johann Sorel
 */
public class ZipArchivePath extends AbstractPath {

    private final ZipCentralDirectory directory;
    private final ZipFileEntry entry;
    private final Path parent;
    private final Sequence children = new ArraySequence();

    public ZipArchivePath(Path parent, ZipCentralDirectory directory, ZipFileEntry entry) {
        this.parent = parent;
        this.directory = directory;
        this.entry = entry;
    }

    public Chars getName() {
        final Chars[] path = entry.getPathChars();
        return path[path.length-1];
    }

    public Path getParent() {
        return parent;
    }

    public boolean isContainer() throws IOException {
        return entry.isContainer();
    }

    public boolean exists() throws IOException {
        return true;
    }

    public Path resolve(Chars address) {
        return getResolver().resolve(entry.name.concat(address));
    }

    public PathResolver getResolver() {
        return parent.getResolver();
    }

    public ByteInputStream createInputStream() throws IOException {
        final ByteInputStream clip = createRawInputStream();

        if(entry.compression == ZipMetaModel.COMPRESSION_NONE){
            //no compression, we can return the stream
            return clip;
        }if(entry.compression == ZipMetaModel.COMPRESSION_DEFLATE){
            //Deflate compression
            final DeflateInputStream decompressed = new DeflateInputStream(clip);
            return decompressed;
        }else{
            throw new IOException("Compression  "+entry.compression+" not supported.");
        }
    }

    /**
     * Raw stream, not decompressed.
     * @return
     * @throws IOException
     */
    public ByteInputStream createRawInputStream() throws IOException{
        final ZipArchive archive = (ZipArchive) getResolver();
        final ByteInputStream bi = archive.createInputStream();
        final DataInputStream ds = new DataInputStream(bi);
        ds.skipFully(entry.dataFileOffset);

        //ensure the stream will not bo out of the expected size
        final ClipInputStream clip = new ClipInputStream(ds, entry.sizeCompressed);
        return clip;
    }


    public Chars toURI() {
        if(parent instanceof ZipArchive){
            //root element inside the zip
            return parent.toURI().concat(new Chars("!/")).concat(getName());
        }else{
            return parent.toURI().concat('/').concat(getName());
        }
    }

    public Chars thisToChars(){
        return new Chars("Zip entry : "+getName());
    }

    public Chars toChars() {
        return thisToChars();
    }

    public boolean createContainer() throws IOException {
        throw new IOException("Not supported yet.");
    }

    public boolean createLeaf() throws IOException {
        throw new IOException("Not supported yet.");
    }
    
    public ByteOutputStream createOutputStream() throws IOException {
        throw new IOException("Not supported yet.");
    }

    public void delete() throws IOException {
        throw new IOException("Not supported yet.");
    }

    public Object getPathInfo(Chars key) {
        return null;
    }
    
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        throw new IOException("Not supported yet.");
    }

    @Override
    public Collection getChildren() {
        return Collections.readOnlyCollection(children);
    }

    void addChild(Path child){
        children.add(child);
    }

}
