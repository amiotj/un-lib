
package science.unlicense.impl.archive.zip.model;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class ZipCentralDirectoryEnd extends ZipBlock {

    public int numberDisk;
    public int numberDiskWithCentralDir;
    public int numberEntryOnDisk;
    public int numberEntryInCentralDir;
    public int directorySize;
    public int offsetCentralDir;
    public Chars comment;

}