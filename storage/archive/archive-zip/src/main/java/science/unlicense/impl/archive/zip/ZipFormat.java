

package science.unlicense.impl.archive.zip;

import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.archive.AbstractArchiveFormat;
import science.unlicense.api.path.Path;
import science.unlicense.api.archive.Archive;
import science.unlicense.api.path.PathResolver;

/**
 * http://www.pkware.com/documents/casestudies/APPNOTE.TXT
 *
 * @author Johann Sorel
 */
public class ZipFormat extends AbstractArchiveFormat{

    public static final ZipFormat INSTANCE = new ZipFormat();
    private static final Chars SUFFIX = new Chars(".zip");

    private static final DocumentType PARAMETERS_TYPE = new DefaultDocumentType(new Chars("zip"), null,null,true,new FieldType[]{PARAM_ENCODING},null);
    
    private ZipFormat() {
        super(new Chars("zip"),
              new Chars("ZIP"),
              new Chars("ZIP"),
              new Chars[]{
                  new Chars("application/zip")
              },
              new Chars[]{
                  new Chars("zip"),
                  new Chars("zipx")
              },
              new byte[0][0]);
    }

    public DocumentType getParameters() {
        return PARAMETERS_TYPE;
    }

    public boolean canOpen(Path candidate) {
        return candidate.getName().toLowerCase().endsWith(SUFFIX);
    }

    public Archive open(Path candidate, Document params) {
        CharEncoding enc = CharEncodings.DEFAULT;
        if(params!=null){
            Object value = params.getFieldValue(PARAM_ENCODING.getId());
            if(value!=null) enc = (CharEncoding) value;
        }
        final ZipArchive arc = new ZipArchive(this, candidate);
        arc.setEncoding(enc);
        return arc;
    }

    public boolean isAbsolute() {
        return false;
    }

    public boolean canCreate(Path base) throws IOException {
        return base.getName().toLowerCase().endsWith(SUFFIX);
    }

    public PathResolver createResolver(Path base) throws IOException {
        return open(base,null);
    }

}