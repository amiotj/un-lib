
package science.unlicense.impl.archive.zip.model;

/**
 *
 * @author Johann Sorel
 */
public class ZipExtraRecord extends ZipBlock {

    public byte[] fieldData;

}