

package science.unlicense.impl.protocol.imap;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;


/**
 * IMAP protocol client.
 * 
 * @author Johann Sorel
 */
public class IMAPClient {
    
    private final ClientSocket socket;
    
    /**
     * Create an IMAP client on default port (143).
     * 
     * @param host imap server name
     * @throws IOException 
     */
    public IMAPClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }   
    
    /**
     * Create an IMAP client on default port (143).
     * 
     * @param address imap server address
     * @throws IOException 
     */
    public IMAPClient(IPAddress address) throws IOException {
        this(address,143);
    }   
    
    /**
     * Create an IMAP client.
     * 
     * @param host imap server name
     * @param port imap server port
     * @throws IOException 
     */
    public IMAPClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host),port);
    }   
    
    /**
     * Create an IMAP client.
     * 
     * @param address imap server address
     * @param port imap server port
     * @throws IOException 
     */
    public IMAPClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
    }    
        
    /**
     * Close imap client.
     * 
     * @throws IOException 
     */
    public void close() throws IOException{
        socket.close();
    }
}
