

package science.unlicense.impl.protocol.imap;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class IMAPConstants {
    
    /** Used to delimite message and commands parts */
    public static final Chars CRLF = new Chars(new byte[]{0x0D,0x0A});
    /** Used to indicate continuation */
    public static final Chars CONTINUATION = new Chars(new byte[]{'+'});
    /** Used to indicate untagged response */
    public static final Chars UNTAGGED = new Chars(new byte[]{'*'});
    
    //use as message markers and delimiters
    public static final Chars BRACKET_OPEN = new Chars(new byte[]{'{'});
    public static final Chars BRACKET_CLOSE = new Chars(new byte[]{'}'});
    public static final Chars QUOTE = new Chars(new byte[]{'"'});
    public static final Chars LIST_OPEN = new Chars(new byte[]{'('});
    public static final Chars LIST_CLOSE = new Chars(new byte[]{')'});
    public static final Chars NIL = new Chars("NIL");
    public static final Chars WILDCARD1 = new Chars(new byte[]{'%'});
    public static final Chars WILDCARD2 = new Chars(new byte[]{'*'});
    public static final Chars AND = new Chars(new byte[]{'&'});
    public static final Chars DIESE = new Chars(new byte[]{'#'});
    
    /** Command success */
    public static final Chars RESPONSE_OK = new Chars("OK");
    /** Command failure */
    public static final Chars RESPONSE_NO = new Chars("NO");
    /** Command error, syntax or parameters */
    public static final Chars RESPONSE_BAD = new Chars("BAD");
    /** */
    public static final Chars RESPONSE_PREAUTH = new Chars("PREAUTH");
    /** */
    public static final Chars RESPONSE_BYE = new Chars("BYE");
    
    /** Prefix for system flags */
    public static final Chars SYSTEM_FLAG = new Chars("\\");
    /** Message flag : seen */
    public static final Chars FLAG_SEEN = new Chars("\\Seen");
    /** Message flag : answered */
    public static final Chars FLAG_ANSWERED = new Chars("\\Answered");
    /** Message flag : flagged : means urgent or special */
    public static final Chars FLAG_FLAGGED = new Chars("\\Flagged");
    /** Message flag : deleted, ready for remove by an expunge command */
    public static final Chars FLAG_DELETED = new Chars("\\Deleted");
    /** Message flag : draft : uncomplete, not send yet */
    public static final Chars FLAG_DRAFT = new Chars("\\Draft");
    /** Message flag : recent */
    public static final Chars FLAG_RECENT = new Chars("\\Recent");
    
    
    public static final Chars COMMAND_CAPABILITY    = new Chars("CAPABILITY");
    public static final Chars COMMAND_NOOP          = new Chars("NOOP");
    public static final Chars COMMAND_LOGOUT        = new Chars("LOGOUT");
    
    public static final Chars COMMAND_STARTTLS      = new Chars("STARTTLS");
    public static final Chars COMMAND_AUTHENTICATE  = new Chars("AUTHENTICATE");
    public static final Chars COMMAND_LOGIN         = new Chars("LOGIN");
    
    public static final Chars COMMAND_SELECT        = new Chars("SELECT");
    public static final Chars COMMAND_EXAMINE       = new Chars("EXAMINE");
    public static final Chars COMMAND_CREATE        = new Chars("CREATE");
    public static final Chars COMMAND_DELETE        = new Chars("DELETE");
    public static final Chars COMMAND_RENAME        = new Chars("RENAME");
    public static final Chars COMMAND_SUBSCRIBE     = new Chars("SUBSCRIBE");
    public static final Chars COMMAND_UNSUBSCRIBE   = new Chars("UNSUBSCRIBE");
    public static final Chars COMMAND_LIST          = new Chars("LIST");
    public static final Chars COMMAND_LSUB          = new Chars("LSUB");
    public static final Chars COMMAND_STATUS        = new Chars("STATUS");
    public static final Chars COMMAND_APPEND        = new Chars("APPEND");
    
    public static final Chars COMMAND_CHECK         = new Chars("CHECK");
    public static final Chars COMMAND_CLOSE         = new Chars("CLOSE");
    public static final Chars COMMAND_EXPUNGE       = new Chars("EXPUNGE");
    public static final Chars COMMAND_SEARCH        = new Chars("SEARCH");
    public static final Chars COMMAND_FETCH         = new Chars("FETCH");
    public static final Chars COMMAND_STORE         = new Chars("STORE");
    public static final Chars COMMAND_COPY          = new Chars("COPY");
    public static final Chars COMMAND_UID           = new Chars("UID");
    
    private IMAPConstants(){}
    
}
