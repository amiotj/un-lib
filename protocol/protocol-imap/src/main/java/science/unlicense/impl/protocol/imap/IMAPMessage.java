

package science.unlicense.impl.protocol.imap;

import java.util.Dictionary;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class IMAPMessage {
    
    public long uid;
    public int messageSequenceId;
    public Dictionary attributs;
    public Chars text;
    
}
