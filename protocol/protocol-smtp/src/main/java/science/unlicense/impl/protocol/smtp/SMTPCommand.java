

package science.unlicense.impl.protocol.smtp;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;

/**
 * SMTP Client command definition.
 * 
 * @author Johann Sorel
 */
public class SMTPCommand {
    
    public Chars verb;
    public Chars[] arguments;

    public SMTPCommand() {
    }

    public SMTPCommand(Chars verb, Chars[] arguments) {
        this.verb = verb;
        this.arguments = arguments;
    }
    
    /**
     * Encode the command in text ready to be send to server.
     * @return encoded command
     */
    public Chars encode(){
        final CharBuffer cb = new CharBuffer(CharEncodings.US_ASCII);
        cb.append(verb);
        if(arguments!=null){
            for(Chars arg : arguments){
                cb.append(SMTPConstants.SPACE);
                cb.append(arg);
            }
        }
        
        cb.append(SMTPConstants.CRLF);
        
        return cb.toChars();
    }
    
    public static SMTPCommand createHELO(Chars domain){
        return new SMTPCommand(SMTPConstants.COMMAND_HELO, new Chars[]{domain});
    }
    
    public static SMTPCommand createEHLO(Chars domainOrAddress){
        return new SMTPCommand(SMTPConstants.COMMAND_EHLO, new Chars[]{domainOrAddress});
    }
    
}
