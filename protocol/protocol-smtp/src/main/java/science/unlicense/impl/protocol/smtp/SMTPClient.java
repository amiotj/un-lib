

package science.unlicense.impl.protocol.smtp;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 * SMTP protocol client.
 * 
 * @author Johann Sorel
 */
public class SMTPClient {
    
    private final ClientSocket socket;
    
    /**
     * Create an SMTP client on default port (25).
     * 
     * @param host smtp server name
     * @throws IOException 
     */
    public SMTPClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }   
    
    /**
     * Create an SMTP client on default port (25).
     * 
     * @param address smtp server address
     * @throws IOException 
     */
    public SMTPClient(IPAddress address) throws IOException {
        this(address,25);
    }   
    
    /**
     * Create an SMTP client.
     * 
     * @param host smtp server name
     * @param port smtp server port
     * @throws IOException 
     */
    public SMTPClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host),port);
    }   
    
    /**
     * Create an SMTP client.
     * 
     * @param address smtp server address
     * @param port smtp server port
     * @throws IOException 
     */
    public SMTPClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
    }    
        
    /**
     * Close smtp client.
     * 
     * @throws IOException 
     */
    public void close() throws IOException{
        socket.close();
    }
}
