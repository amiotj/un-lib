

package science.unlicense.impl.protocol.smtp;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class SMTPResponse {
    
    public int code;
    public Chars[] arguments;
    
}
