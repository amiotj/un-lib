

package science.unlicense.impl.protocol.pop;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class POPResponse {
    
    public Chars status;
    public Chars keyword;
    public Chars[] arguments;
    
}
