

package science.unlicense.impl.protocol.pop;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

/**
 * POP Client command definition.
 * 
 * @author Johann Sorel
 */
public class POPCommand {
    
    public Chars keyword;
    public Chars[] arguments;

    public POPCommand() {
    }

    public POPCommand(Chars keyword) {
        this(keyword,null);
    }
    
    public POPCommand(Chars keyword, Chars[] arguments) {
        this.keyword = keyword;
        this.arguments = arguments;
    }
    
    /**
     * Encode the command in text ready to be send to server.
     * @return encoded command
     */
    public Chars encode(){
        final CharBuffer cb = new CharBuffer(CharEncodings.US_ASCII);
        cb.append(keyword);
        if(arguments!=null){
            for(Chars arg : arguments){
                cb.append(POPConstants.SPACE);
                cb.append(arg);
            }
        }
        
        cb.append(POPConstants.CRLF);
        
        return cb.toChars();
    }
    
    public static POPCommand createQUIT(){
        return new POPCommand(POPConstants.COMMAND_QUIT);
    }
    
    public static POPCommand createSTAT(){
        return new POPCommand(POPConstants.COMMAND_STAT);
    }
    
    public static POPCommand createSTAT(Integer messageNumber){
        if(messageNumber!=null){
            return new POPCommand(POPConstants.COMMAND_LIST, new Chars[]{Int32.encode(messageNumber)});
        }else{
            return new POPCommand(POPConstants.COMMAND_LIST);
        }
    }
    
    public static POPCommand createRETR(int messageNumber){
        return new POPCommand(POPConstants.COMMAND_RETR, new Chars[]{Int32.encode(messageNumber)});
    }
    
    public static POPCommand createDELE(int messageNumber){
        return new POPCommand(POPConstants.COMMAND_DELE, new Chars[]{Int32.encode(messageNumber)});
    }
    
    public static POPCommand createNOOP(){
        return new POPCommand(POPConstants.COMMAND_NOOP);
    }
    
    public static POPCommand createRSET(){
        return new POPCommand(POPConstants.COMMAND_RSET);
    }
    
    public static POPCommand createTOP(int messageNumber,int numberOfLine){
        return new POPCommand(POPConstants.COMMAND_TOP, new Chars[]{
            Int32.encode(messageNumber),Int32.encode(numberOfLine)});
    }
    
    public static POPCommand createUIDL(Integer messageNumber){
        if(messageNumber!=null){
            return new POPCommand(POPConstants.COMMAND_UIDL, new Chars[]{Int32.encode(messageNumber)});
        }else{
            return new POPCommand(POPConstants.COMMAND_UIDL);
        }
    }
    
    public static POPCommand createUSER(Chars user){
        return new POPCommand(POPConstants.COMMAND_USER, new Chars[]{user});
    }
    
    public static POPCommand createPASS(Chars password){
        return new POPCommand(POPConstants.COMMAND_PASS, new Chars[]{password});
    }
    
    public static POPCommand createAPOP(Chars name, Chars digest){
        return new POPCommand(POPConstants.COMMAND_APOP, new Chars[]{name,digest});
    }
    
}
