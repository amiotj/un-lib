

package science.unlicense.impl.protocol.pop;

import science.unlicense.api.character.Chars;

/**
 * Reference :
 * http://tools.ietf.org/html/rfc1939
 * 
 * @author Johann Sorel
 */
public class POPConstants {
    
    /** Used for argument separation */
    public static final int SPACE = ' ';
    /** Used to delimite message and commands parts */
    public static final Chars CRLF = new Chars(new byte[]{0x0D,0x0A});
    /** Used to delimite end of data */
    public static final Chars END_OF_DATA = CRLF.concat('.').concat(CRLF);
    
    public static final Chars STATUS_OK = new Chars("+OK");
    public static final Chars STATUS_ERROR = new Chars("-ERR");
    
    public static final Chars COMMAND_STAT = new Chars("STAT");
    public static final Chars COMMAND_LIST = new Chars("LIST");
    public static final Chars COMMAND_RETR = new Chars("RETR");
    public static final Chars COMMAND_DELE = new Chars("DELE");
    public static final Chars COMMAND_NOOP = new Chars("NOOP");
    public static final Chars COMMAND_RSET = new Chars("RSET");
    public static final Chars COMMAND_QUIT = new Chars("QUIT");
    public static final Chars COMMAND_TOP = new Chars("TOP");
    public static final Chars COMMAND_UIDL = new Chars("UIDL");
    public static final Chars COMMAND_USER = new Chars("USER");
    public static final Chars COMMAND_PASS = new Chars("PASS");
    public static final Chars COMMAND_APOP = new Chars("APOP");
    
}
