
package science.unlicense.impl.protocol.http;

import science.unlicense.impl.protocol.http.HTTPFormat;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.path.PathFormat;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class HTTPFormatTest {

    /**
     * Test format is declared.
     */
    @Test
    public void formatTest(){

        final PathFormat[] formats = Paths.getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i] instanceof HTTPFormat){
                return;
            }
        }

        Assert.fail("HTTP Format not found");
    }

}