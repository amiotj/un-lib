
package science.unlicense.impl.protocol.http;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.path.Path;

/**
 * Multipart message body.
 * 
 * @author Johann Sorel
 */
public class MultiPartMessageBody implements HTTPMessageBody {

    private static final Chars BTAG = new Chars("--");
    private static final Chars FORMDATA = new Chars("Content-Disposition: form-data");

    private final Sequence parts = new ArraySequence();

    private Chars boundary = new Chars("azertyqsdgwxcvb");

    public MultiPartMessageBody() {
    }

    public Sequence getMessageParts() {
        return parts;
    }

    public Chars getBoundary() {
        return boundary;
    }

    public void setBoundary(Chars boundary) {
        this.boundary = boundary;
    }
    
    @Override
    public void write(ByteOutputStream out) throws IOException {

        final CharOutputStream cs = new CharOutputStream(out);

        for (int i=0,n=parts.getSize();i<n;i++) {
            final Part part = (Part) parts.get(0);
            cs.write(BTAG).write(boundary).write(HTTPConstants.CRLF);
            part.write(cs, out);
        }

        //final boundary
        cs.write(BTAG).write(boundary).write(BTAG).write(HTTPConstants.CRLF);
        cs.flush();
    }

    @Override
    public void read(ByteInputStream out) throws IOException {
        throw new IOException("Not supported.");
    }

    /**
     * One message part.
     * 
     * Resources :
     * https://tools.ietf.org/html/rfc6266
     */
    public static class Part {

        private final Dictionary parameters = new HashDictionary();
        private final Dictionary headers = new HashDictionary();
        private Object data;

        public Dictionary getParameters() {
            return parameters;
        }

        public Dictionary getHeaders() {
            return headers;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public void write(CharOutputStream cs, ByteOutputStream out) throws IOException {

            //write header
            cs.write(FORMDATA);
            final Iterator paramIte = parameters.getPairs().createIterator();
            while (paramIte.hasNext()) {
                final Pair pair = (Pair) paramIte.next();
                cs.write(new Chars("; "));
                cs.write(CObjects.toChars(pair.getValue1()));
                cs.write(new Chars("=\""));
                cs.write(CObjects.toChars(pair.getValue2()));
                cs.write(new Chars("\""));
            }
            cs.write(HTTPConstants.CRLF);

            //write other headers
            final Iterator headerIte = headers.getPairs().createIterator();
            while (headerIte.hasNext()) {
                final Pair pair = (Pair) headerIte.next();
                cs.write(CObjects.toChars(pair.getValue1()));
                cs.write(new Chars(": "));
                cs.write(CObjects.toChars(pair.getValue2()));
                cs.write(HTTPConstants.CRLF);
            }

            //write datas
            cs.write(HTTPConstants.CRLF).flush();
            if (data instanceof Path) {
                final Path file = (Path) data;
                IOUtilities.copy(file.createInputStream(), out);
                out.flush();
            } else if (data instanceof byte[]) {
                out.write((byte[])data);
                out.flush();
            } else if (data instanceof CharArray) {
                cs.write( ((CharArray)data).toChars() );
            } else {
                throw new IOException("Unexpected data type :" + data);
            }
            cs.write(HTTPConstants.CRLF).flush();

        }

    }

}