

package science.unlicense.impl.protocol.http;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class HTTPPath extends AbstractPath{

    private final PathResolver resolver;
    private final Chars urlPath;

    public HTTPPath(final PathResolver resolver, Chars url) {
        this.resolver = resolver;
        this.urlPath = url;
    }

    public boolean canHaveChildren() {
        return false;
    }

    public Chars getName() {
        final int index = urlPath.getLastOccurence('/');
        if(index<0){
            return urlPath;
        }else{
            return urlPath.truncate(index+1,-1);
        }
    }

    public Path getParent() {
        final int index = urlPath.getLastOccurence('/');
        if(index<0){
            //no parent
            return null;
        }else{
            return getResolver().resolve(urlPath.truncate(0,index));
        }
    }

    public boolean isContainer() throws IOException {
        return false;
    }

    public boolean exists() throws IOException {
        //TODO check for http error to see if it exists
        return true;
    }
    
    public boolean createContainer() throws IOException {
        return false;
    }

    public boolean createLeaf() throws IOException {
        return false;
    }
    
    public Path resolve(Chars address) {
        if(urlPath.endsWith('/')){
            return getResolver().resolve(urlPath.concat(address));
        }else{
            return getResolver().resolve(urlPath.concat('/').concat(address));
        }
    }

    public PathResolver getResolver() {
        return resolver;
    }

    public ByteInputStream createInputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public ByteOutputStream createOutputStream() throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public Collection getChildren() {
        return Collections.emptyCollection();
    }

    public Chars toURI() {
        return urlPath;
    }

    public Class[] getEventClasses() {
        return new Class[0];
    }

    public Chars toChars() {
        return new Chars("Http path : "+urlPath);
    }

}