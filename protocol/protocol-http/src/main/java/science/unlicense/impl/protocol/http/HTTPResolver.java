
package science.unlicense.impl.protocol.http;

import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class HTTPResolver implements PathResolver{

    private final HTTPFormat format;

    public HTTPResolver(HTTPFormat format) {
        this.format = format;
    }

    public PathFormat getFormat() {
        return format;
    }

    public Path resolve(Chars path) {
        if(!path.startsWith(new Chars("http:"))){
            return null;
        }

        return null;
    }

}