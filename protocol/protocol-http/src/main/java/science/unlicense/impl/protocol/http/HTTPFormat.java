
package science.unlicense.impl.protocol.http;

import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public class HTTPFormat implements PathFormat {

    private final HTTPResolver resolver = new HTTPResolver(this);

    public boolean isAbsolute() {
        return true;
    }

    public boolean canCreate(Path path) throws IOException {
        return false;
    }

    public PathResolver createResolver(Path path) throws IOException {
        return resolver;
    }

}