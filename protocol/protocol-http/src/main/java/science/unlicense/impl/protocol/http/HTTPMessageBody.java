
package science.unlicense.impl.protocol.http;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;

/**
 * Request and response message body.
 *
 * @author Johann Sorel
 */
public interface HTTPMessageBody {

    void write(ByteOutputStream out) throws IOException;

    void read(ByteInputStream out) throws IOException;

}