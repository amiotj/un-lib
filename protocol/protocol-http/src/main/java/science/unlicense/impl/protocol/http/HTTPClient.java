
package science.unlicense.impl.protocol.http;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 *
 * @author Johann Sorel
 */
public class HTTPClient {

    private final ClientSocket socket;

    /**
     * Create an http client on default port (80).
     *
     * @param host http server name
     * @throws IOException
     */
    public HTTPClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }

    /**
     * Create an http client on default port (80).
     *
     * @param address http server address
     * @throws IOException
     */
    public HTTPClient(IPAddress address) throws IOException {
        this(address,80);
    }

    /**
     * Create an http client.
     *
     * @param host http server name
     * @param port http server port
     * @throws IOException
     */
    public HTTPClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host),port);
    }

    /**
     * Create an http client.
     *
     * @param address http server address
     * @param port http server port
     * @throws IOException
     */
    public HTTPClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
    }

    /**
     * Send an http request.
     *
     * @param request
     * @return HTTPResponse
     * @throws IOException
     */
    public HTTPResponse send(HTTPRequest request) throws IOException{

        //write request
        final ByteOutputStream bo = socket.getOutputStream();        
        request.write(bo);
        
        //get response
        final ByteInputStream is = socket.getInputStream();        
        final HTTPResponse response = new HTTPResponse();
        response.read(is);
        return response;
    }

    public void close() throws IOException{
        socket.close();
    }
}