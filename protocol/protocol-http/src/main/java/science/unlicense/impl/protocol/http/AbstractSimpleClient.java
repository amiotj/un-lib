
package science.unlicense.impl.protocol.http;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;

/**
 * The most simple client handler possible.
 * This class starts a thread which process the current client.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractSimpleClient extends Thread {

    protected final ClientSocket socket;
    private ByteInputStream in;
    private ByteOutputStream out;

    public AbstractSimpleClient(ClientSocket socket) {
        this.socket = socket;
        start();
    }

    public final void run() {
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();

            while (true) {
                final HTTPRequest request = new HTTPRequest();
                request.read(in);
                final HTTPResponse response = treat(request);
                response.write(out);
                out.flush();
                in.close();
                out.close();
                socket.close();
                break;
            }
        } catch (IOException ex) {
            //connection close or failed
            return;
        }
    }

    protected abstract HTTPResponse treat(HTTPRequest request);

}
