
package science.unlicense.impl.protocol.http;

import science.unlicense.api.character.Chars;

/**
 *
 * Resources :
 * https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#cite_note-30
 *
 * @author Johann Sorel
 */
public final class HTTPConstants {

    public interface Request {

        public static final Chars METHOD_OPTIONS    = new Chars("OPTIONS");
        public static final Chars METHOD_GET        = new Chars("GET");
        public static final Chars METHOD_HEAD       = new Chars("HEAD");
        public static final Chars METHOD_POST       = new Chars("POST");
        public static final Chars METHOD_PUT        = new Chars("PUT");
        public static final Chars METHOD_DELETE     = new Chars("DELETE");
        public static final Chars METHOD_TRACE      = new Chars("TRACE");
        public static final Chars METHOD_CONNECT    = new Chars("CONNECT");

        public static final Chars HEADER_ACCEPT             = new Chars("Accept");
        public static final Chars HEADER_ACCEPT_CHARSET     = new Chars("Accept-Charset");
        public static final Chars HEADER_ACCEPT_ENCODING    = new Chars("Accept-Encoding");
        public static final Chars HEADER_ACCEPT_LANGUAGE    = new Chars("Accept-Language");
        public static final Chars HEADER_ACCEPT_DATETIME    = new Chars("Accept-Datetime");
        public static final Chars HEADER_AUTHORIZATION      = new Chars("Authorization");
        public static final Chars HEADER_CACHE_CONTROL      = new Chars("Cache-Control");
        public static final Chars HEADER_CONNECTION         = new Chars("Connection");
        public static final Chars HEADER_COOKIE             = new Chars("Cookie");
        public static final Chars HEADER_CONTENT_LENGTH     = new Chars("Content-Length");
        public static final Chars HEADER_CONTENT_MD5        = new Chars("Content-MD5");
        public static final Chars HEADER_CONTENT_TYPE       = new Chars("Content-Type");
        public static final Chars HEADER_DATE               = new Chars("Date");
        public static final Chars HEADER_EXPECT             = new Chars("Expect");
        public static final Chars HEADER_FORWARDED          = new Chars("Forwarded");
        public static final Chars HEADER_FROM               = new Chars("From");
        public static final Chars HEADER_HOST               = new Chars("Host");
        public static final Chars HEADER_IF_MATCH           = new Chars("If-Match");
        public static final Chars HEADER_IF_MODIFIED_SINCE  = new Chars("If-Modified-Since");
        public static final Chars HEADER_IF_NONE_MATCH      = new Chars("If-None-Match");
        public static final Chars HEADER_IF_RANGE           = new Chars("If-Range");
        public static final Chars HEADER_IF_UNMODIFIED_SINCE= new Chars("If-Unmodified-Since");
        public static final Chars HEADER_MAX_FORWARDS       = new Chars("Max-Forwards");
        public static final Chars HEADER_ORIGIN             = new Chars("Origin");
        public static final Chars HEADER_PRAGMA             = new Chars("Pragma");
        public static final Chars HEADER_PROXY_AUTHORIZATION= new Chars("Proxy-Authorization");
        public static final Chars HEADER_RANGE              = new Chars("Range");
        public static final Chars HEADER_REFERER            = new Chars("Referer");
        public static final Chars HEADER_TE                 = new Chars("TE");
        public static final Chars HEADER_USER_AGENT         = new Chars("User-Agent");
        public static final Chars HEADER_UPGRADE            = new Chars("Upgrade");
        public static final Chars HEADER_VIA                = new Chars("Via");
        public static final Chars HEADER_WARNING            = new Chars("Warning");

    }

    public interface Response {
        public static final Chars HEADER_ACCESS_CONTROL_ALLOW_ORIGIN= new Chars("Access-Control-Allow-Origin");
        public static final Chars HEADER_ACCEPT_PATCH               = new Chars("Accept-Patch");
        public static final Chars HEADER_ACCEPT_RANGES              = new Chars("Accept-Ranges");
        public static final Chars HEADER_AGE                        = new Chars("Age");
        public static final Chars HEADER_ALLOW                      = new Chars("Allow");
        public static final Chars HEADER_ALT_SVC                    = new Chars("Alt-Svc");
        public static final Chars HEADER_CACHE_CONTROL              = Request.HEADER_CACHE_CONTROL;
        public static final Chars HEADER_CONNECTION                 = Request.HEADER_CONNECTION;
        public static final Chars HEADER_CONTENT_DISPOSITION        = new Chars("Content-Disposition");
        public static final Chars HEADER_CONTENT_ENCODING           = new Chars("Content-Encoding");
        public static final Chars HEADER_CONTENT_LANGUAGE           = new Chars("Content-Language");
        public static final Chars HEADER_CONTENT_LENGTH             = Request.HEADER_CONTENT_LENGTH;
        public static final Chars HEADER_CONTENT_LOCATION           = new Chars("Content-Location");
        public static final Chars HEADER_CONTENT_MD5                = Request.HEADER_CONTENT_MD5;
        public static final Chars HEADER_CONTENT_RANGE              = new Chars("Content-Range");
        public static final Chars HEADER_CONTENT_TYPE               = Request.HEADER_CONTENT_TYPE;
        public static final Chars HEADER_DATE                       = Request.HEADER_DATE;
        public static final Chars HEADER_ETAG                       = new Chars("ETag");
        public static final Chars HEADER_EXPIRES                    = new Chars("Expires");
        public static final Chars HEADER_LAST_MODIFIED              = new Chars("Last-Modified");
        public static final Chars HEADER_LINK                       = new Chars("Link");
        public static final Chars HEADER_LOCATION                   = new Chars("Location");
        public static final Chars HEADER_P3P                        = new Chars("P3P");
        public static final Chars HEADER_PRAGMA                     = Request.HEADER_PRAGMA;
        public static final Chars HEADER_PROXY_AUTHENTICATE         = new Chars("Proxy-Authenticate");
        public static final Chars HEADER_PUBLIC_KEY_PINS            = new Chars("Public-Key-Pins");
        public static final Chars HEADER_REFRESH                    = new Chars("Refresh");
        public static final Chars HEADER_RETRY_AFTER                = new Chars("Retry-After");
        public static final Chars HEADER_SERVER                     = new Chars("Server");
        public static final Chars HEADER_SET_COOKIE                 = new Chars("Set-Cookie");
        public static final Chars HEADER_STATUS                     = new Chars("Status");
        public static final Chars HEADER_STRICT_TRANSPORT_SECURITY  = new Chars("Strict-Transport-Security");
        public static final Chars HEADER_TRAILER                    = new Chars("Trailer");
        public static final Chars HEADER_TRANSFER_ENCODING          = new Chars("Transfer-Encoding");
        public static final Chars HEADER_TSV                        = new Chars("TSV");
        public static final Chars HEADER_UPGRADE                    = Request.HEADER_UPGRADE;
        public static final Chars HEADER_VARY                       = new Chars("Vary");
        public static final Chars HEADER_VIA                        = Request.HEADER_VIA;
        public static final Chars HEADER_WARNING                    = Request.HEADER_WARNING;
        public static final Chars HEADER_WWW_AUTHENTICATE           = new Chars("WWW-Authenticate");
        public static final Chars HEADER_X_FRAME_OPTIONS            = new Chars("X-Frame-Options");

    }

    /** RFC:2616 Section 10.1.1: Continue */
    public static final int STATUS_100_CONTINUE = 100;
    /** RFC:2616 Section 10.1.2: Switching Protocols */
    public static final int STATUS_101_SWITCH = 101;
    /** RFC:2616 Section 10.2.1: OK */
    public static final int STATUS_200_OK = 200;
    /** RFC:2616 Section 10.2.2: Created */
    public static final int STATUS_201_CREATED = 201;
    /** RFC:2616 Section 10.2.3: Accepted */
    public static final int STATUS_202_ACCEPTED = 202;
    /** RFC:2616 Section 10.2.4: Non-Authoritative Information */
    public static final int STATUS_203_NONAUTH_INFO = 203;
    /** RFC:2616 Section 10.2.5: No Content */
    public static final int STATUS_204_NO_CONTENT = 204;
    /** RFC:2616 Section 10.2.6: Reset Content */
    public static final int STATUS_205_RESET_CONTENT = 205;
    /** RFC:2616 Section 10.2.7: Partial Content */
    public static final int STATUS_206_PARTIAL_CONTENT = 206;
    /** RFC:2616 Section 10.3.1: Multiple Choices */
    public static final int STATUS_300_MULTIPLE_CHOICE = 300;
    /** RFC:2616 Section 10.3.2: Moved Permanently */
    public static final int STATUS_301_MOVED = 301;
    /** RFC:2616 Section 10.3.3: Found */
    public static final int STATUS_302_FOUND = 302;
    /** RFC:2616 Section 10.3.4: See Other */
    public static final int STATUS_303_SEE_OTHER = 303;
    /** RFC:2616 Section 10.3.5: Not Modified */
    public static final int STATUS_304_NOT_MODIFIED = 304;
    /** RFC:2616 Section 10.3.6: Use Proxy */
    public static final int STATUS_305_USE_PROXY = 305;
    /** RFC:2616 Section 10.3.8: Temporary Redirect */
    public static final int STATUS_307_TEMP_REDIRECT = 307;
    /** RFC:2616 Section 10.4.1: Bad Request */
    public static final int STATUS_400_BAD_REQUEST = 400;
    /** RFC:2616 Section 10.4.2: Unauthorized */
    public static final int STATUS_401_UNAUTH = 401;
    /** RFC:2616 Section 10.4.3: Payment Required */
    public static final int STATUS_402_PAYMENT_REQUIRED = 402;
    /** RFC:2616 Section 10.4.4: Forbidden */
    public static final int STATUS_403_FORBIDDEN = 403;
    /** RFC:2616 Section 10.4.5: Not Found */
    public static final int STATUS_404_NOT_FOUND = 404;
    /** RFC:2616 Section 10.4.6: Method Not Allowed */
    public static final int STATUS_405_METHOD_NOT_ALLOWED = 405;
    /** RFC:2616 Section 10.4.7: Not Acceptable */
    public static final int STATUS_406_NOT_ACCEPTABLE = 406;
    /** RFC:2616 Section 10.4.8: Proxy Authentication Required */
    public static final int STATUS_407_PROXY_AUTH_REQUIRED = 407;
    /** RFC:2616 Section 10.4.9: Request Time-out */
    public static final int STATUS_408_TIMEOUT = 408;
    /** RFC:2616 Section 10.4.10: Conflict */
    public static final int STATUS_409_CONFLICT = 409;
    /** RFC:2616 Section 10.4.11: Gone */
    public static final int STATUS_410_GONE = 410;
    /** RFC:2616 Section 10.4.12: Length Required */
    public static final int STATUS_411_LENGTH_REQUIRED = 411;
    /** RFC:2616 Section 10.4.13: Precondition Failed */
    public static final int STATUS_412_PRECONDITION_FAILED = 412;
    /** RFC:2616 Section 10.4.14: Request Entity Too Large */
    public static final int STATUS_413_ENTITY_TOO_LARGE = 413;
    /** RFC:2616 Section 10.4.15: Request-URI Too Large */
    public static final int STATUS_414_URI_TOO_LARGE = 414;
    /** RFC:2616 Section 10.4.16: Unsupported Media Type */
    public static final int STATUS_415_UNSUP_MEDIA_TYPE = 415;
    /** RFC:2616 Section 10.4.17: Requested range not satisfiable */
    public static final int STATUS_416_REQ_RANGE_NOT_SAT = 416;
    /** RFC:2616 Section 10.4.18: Expectation Failed */
    public static final int STATUS_417_EXPECTED_FAILED = 417;
    /** RFC:2616 Section 10.5.1: Internal Server Error */
    public static final int STATUS_500_INTERNAL_ERROR = 500;
    /** RFC:2616 Section 10.5.2: Not Implemented */
    public static final int STATUS_501_NOT_IMPLEMENTED = 501;
    /** RFC:2616 Section 10.5.3: Bad Gateway */
    public static final int STATUS_502_BAD_GATEWAY = 502;
    /** RFC:2616 Section 10.5.4: Service Unavailable */
    public static final int STATUS_503_SERVICE_UNAVAILABLE = 503;
    /** RFC:2616 Section 10.5.5: Gateway Time-out */
    public static final int STATUS_504_GATEWAY_TIMEOUT = 504;
    /** RFC:2616 Section 10.5.6: HTTP Version not supported */
    public static final int STATUS_505_UNSUPPORTED_VERSION = 505;

    /**
     * An encoding format produced by the file compression program
     * "gzip" (GNU zip) as described in RFC 1952 [25].
     * This format is a Lempel-Ziv coding (LZ77) with a 32 bit CRC.
     */
    public static final Chars CONTENT_CODING_GZIP = new Chars("gzip");
    /**
     * The encoding format produced by the common UNIX file compression
     * program "compress". This format is an adaptive Lempel-Ziv-Welch
     * coding (LZW).
     *
     * Use of program names for the identification of encoding formats
     * is not desirable and is discouraged for future encodings. Their
     * use here is representative of historical practice, not good
     * design. For compatibility with previous implementations of HTTP,
     * applications SHOULD consider "x-gzip" and "x-compress" to be
     * equivalent to "gzip" and "compress" respectively.
     */
    public static final Chars CONTENT_CODING_COMPRESS = new Chars("compress");
    /**
     * The "zlib" format defined in RFC 1950 [31] in combination with
     * the "deflate" compression mechanism described in RFC 1951 [29].
     */
    public static final Chars CONTENT_CODING_DEFLATE = new Chars("deflate");
    /**
     * The default (identity) encoding; the use of no transformation
     * whatsoever. This content-coding is used only in the Accept-
     * Encoding header, and SHOULD NOT be used in the Content-Encoding
     * header.
     */
    public static final Chars CONTENT_CODING_IDENTITY = new Chars("identity");

    /**
     * CariageReturn(13) + NewLine(10).
     */
    public static final Chars CRLF = new Chars(new byte[]{13,10});

    private HTTPConstants(){}

}