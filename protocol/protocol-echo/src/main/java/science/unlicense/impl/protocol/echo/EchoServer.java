
package science.unlicense.impl.protocol.echo;

import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.ServerSocket;


/**
 * An echo server as defined by :
 * RFC 862 : http://tools.ietf.org/html/rfc862
 * RFC 347 : http://tools.ietf.org/html/rfc347
 * 
 * @author Johann Sorel
 */
public class EchoServer implements ServerSocket.ClientHandler {
    
    private int port;
    private ServerSocket socket;
    
    /**
     * Create an echo server on default port (7)
     */
    public EchoServer(){
        this(7);
    }

    /**
     * Create an echo server on given port.
     * @param port port to listen
     */
    public EchoServer(int port) {
        this.port = port;
    }

    /**
     * Server port.
     * If zero was passed at creation, the port will change after starting server.
     * 
     * @return int
     */
    public int getPort() {
        return port;
    }
    
    /**
     * Start the server.
     */
    public void start() throws IOException{
        if(socket==null){
            socket = science.unlicense.system.System.get().getSocketManager().createServerSocket(port, this);
            port = socket.getPort();//port might have change if 0.
        }
    }
    
    /**
     * Stop the server.
     */
    public void stop() throws IOException{
        if(socket==null) return;
        
        socket.close();
        socket = null;
    }

    public void create(ClientSocket clientsocket) {
        //TODO need a thread api
        //TODO explore HelenOS fibrile approach
        new EchoThread(clientsocket).start();
    }
    
    private static class EchoThread extends Thread{

        private final ClientSocket socket;

        public EchoThread(ClientSocket socket) {
            this.socket = socket;
        }
        
        public void run() {
            try{
                while(true){
                    final int b = socket.getInputStream().read();
                    if(b==-1) break;
                    
                    socket.getOutputStream().write((byte)b);
                    socket.getOutputStream().flush();
                }
            }catch(IOException ex){
                //connection close or failed
                return;
            }
        }
        
    }
    
    
}
