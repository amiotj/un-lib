
package science.unlicense.impl.protocol.echo;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 * Echo protocol client.
 * 
 * @author Johann Sorel
 */
public class EchoClient {
    
    private final ClientSocket socket;
    
    /**
     * Create an echo client on default port (7).
     * 
     * @param host echo server name
     * @throws IOException 
     */
    public EchoClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }   
    
    /**
     * Create an echo client on default port (7).
     * 
     * @param address echo server address
     * @throws IOException 
     */
    public EchoClient(IPAddress address) throws IOException {
        this(address,7);
    }   
    
    /**
     * Create an echo client.
     * 
     * @param host echo server name
     * @param port echo server port
     * @throws IOException 
     */
    public EchoClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host),port);
    }   
    
    /**
     * Create an echo client.
     * 
     * @param address echo server address
     * @param port echo server port
     * @throws IOException 
     */
    public EchoClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
    }    
    
    /**
     * Send datas and wait for echo response.
     * 
     * @param data byte array to send.
     * @return byte[] should contain the same values that was send.
     * @throws IOException 
     */
    public byte[] send(byte[] data) throws IOException{
        socket.getOutputStream().write(data);
        socket.getOutputStream().flush();
        
        byte[] result = new byte[data.length];
        final DataInputStream ds = new DataInputStream(socket.getInputStream());
        ds.readFully(result);
        return result;
    }
    
    /**
     * Close echo client.
     * 
     * @throws IOException 
     */
    public void close() throws IOException{
        socket.close();
    }
    
}
