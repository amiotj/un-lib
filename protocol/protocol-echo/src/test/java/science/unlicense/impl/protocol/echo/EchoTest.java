
package science.unlicense.impl.protocol.echo;

import science.unlicense.impl.protocol.echo.EchoClient;
import science.unlicense.impl.protocol.echo.EchoServer;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;

/**
 * Test echo client and server.
 * 
 * @author Johann Sorel
 */
public class EchoTest {
    
    @Test
    public void echotest() throws IOException{
        
        //port 0 for automatic.
        final EchoServer server = new EchoServer(0);
        server.start();
        final int port = server.getPort();
        
        final EchoClient client = new EchoClient(new Chars("localhost"),port);        
        final byte[] message = new Chars("Hello echo!").toBytes();        
        final byte[] result = client.send(message);
        
        Assert.assertArrayEquals(message, result);
        
        server.stop();
        client.close();
    }
    
    
}
