
package science.unlicense.impl.protocol.irc;

import java.util.Scanner;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Aurélien Lombardo
 */
public class Main {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            IRCClient client = new IRCClient(new Chars("barjavel.freenode.net"), 6667);
            boolean isOK = false;
            while (!isOK) {
                System.out.println("Renseigner un nickname :");
                String nick = sc.nextLine();
                isOK = client.connect(new Chars(nick));
            }
            
            client.join(new Chars("#botwar"));
            
            Chars line = null;
            while ((line = client.receive()) != null) {
                System.out.println(line);
                if (line.getFirstOccurence(new Chars("PING")) >= 0) {
                    client.pong();
                }
            }
            
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
