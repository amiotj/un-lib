
package science.unlicense.impl.protocol.ftp;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public final class FTPConstants {

    public static final Chars CRLF = new Chars("\r\n");
    static final byte[] CRLFB = CRLF.toBytes();



    public static final Chars CMD_USER = new Chars("USER");
    public static final Chars CMD_PASS = new Chars("PASS");
    public static final Chars CMD_ACCT = new Chars("ACCT");
    public static final Chars CMD_CWD = new Chars("CWD");
    public static final Chars CMD_CDUP = new Chars("CDUP");
    public static final Chars CMD_SMNT = new Chars("SMNT");
    public static final Chars CMD_REIN = new Chars("REIN");
    public static final Chars CMD_QUIT = new Chars("QUIT");

    public static final Chars CMD_PORT = new Chars("PORT");
    public static final Chars CMD_PASV = new Chars("PASV");
    public static final Chars CMD_TYPE = new Chars("TYPE");
    public static final Chars CMD_STRU = new Chars("STRU");
    public static final Chars CMD_MODE = new Chars("MODE");

    public static final Chars CMD_RETR = new Chars("RETR");
    public static final Chars CMD_STOR = new Chars("STOR");
    public static final Chars CMD_STOU = new Chars("STOU");
    public static final Chars CMD_APPE = new Chars("APPE");
    public static final Chars CMD_ALLO = new Chars("ALLO");
    public static final Chars CMD_REST = new Chars("REST");
    public static final Chars CMD_RNFR = new Chars("RNFR");
    public static final Chars CMD_RNTO = new Chars("RNTO");
    public static final Chars CMD_ABOR = new Chars("ABOR");
    public static final Chars CMD_DELE = new Chars("DELE");
    public static final Chars CMD_RMD = new Chars("RMD");
    public static final Chars CMD_MKD = new Chars("MKD");
    public static final Chars CMD_PWD = new Chars("PWD");
    public static final Chars CMD_LIST = new Chars("LIST");
    public static final Chars CMD_NLST = new Chars("NLST");
    public static final Chars CMD_SITE = new Chars("SITE");
    public static final Chars CMD_SYST = new Chars("SYST");
    public static final Chars CMD_STAT = new Chars("STAT");
    public static final Chars CMD_HELP = new Chars("HELP");
    public static final Chars CMD_NOOP = new Chars("NOOP");

    public static final Chars TYPE_ASCII = new Chars("A");
    public static final Chars TYPE_EBCDIC = new Chars("E");
    public static final Chars TYPE_IMAGE = new Chars("I");
    public static final Chars TYPE_LOCAL = new Chars("L");

    public static final Chars STYPE_NON_PRINT = new Chars("N");
    public static final Chars STYPE_TELNET = new Chars("T");
    public static final Chars STYPE_ASA = new Chars("C");

    public static final int POSITIVE_PRELIMINARY_REPLY = '1';
    public static final int POSITIVE_COMPLETION_REPLY = '2';
    public static final int POSITIVE_INTERMEDIATE_REPLY = '3';
    public static final int TRANSIENT_NEGATIVE_COMPLETION_REPLY = '4';
    public static final int PPERMANENT_NEGATIVE_COMPLETION_REPLY = '5';

    public static final int SYNTAX = '0';
    public static final int INFORMATION = '1';
    public static final int CONNECTIONS = '2';
    public static final int AUTHENTICATION = '3';
    public static final int UNSPECIFIED = '4';
    public static final int FILE_SYSTEM = '5';

    private FTPConstants(){}

}
