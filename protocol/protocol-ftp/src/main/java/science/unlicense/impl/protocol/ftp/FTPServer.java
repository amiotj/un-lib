
package science.unlicense.impl.protocol.ftp;


import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.ServerSocket;

/**
 *
 * Specification : https://www.ietf.org/rfc/rfc959.txt
 *
 * @author Johann Sorel
 */
public class FTPServer implements ServerSocket.ClientHandler {

    public static void main(String[] args) throws IOException {
        new FTPServer(6000).start();
    }

    private ServerSocket socket;
    private int portControl;

    /**
     * Create an ftp server on default portControl (21)
     */
    public FTPServer(){
        this(21);
    }

    /**
     * Create an ftp server on given portControl.
     * @param port portControl to listen
     */
    public FTPServer(int port) {
        this.portControl = port;
    }

    /**
     * Server portControl.
     * If zero was passed at creation, the portControl will change after starting server.
     *
     * @return int
     */
    public int getPort() {
        return portControl;
    }

    /**
     * Start the server.
     */
    public void start() throws IOException{
        if(socket==null){
            socket = science.unlicense.system.System.get().getSocketManager().createServerSocket(portControl, this);
            portControl = socket.getPort();//port might have change if 0.
        }
    }

    /**
     * Stop the server.
     */
    public void stop() throws IOException{
        if(socket==null) return;

        socket.close();
        socket = null;
    }

    public void create(ClientSocket clientsocket) {
        System.out.println("new client");
        new FTPServerThread(clientsocket).start();
    }


}
