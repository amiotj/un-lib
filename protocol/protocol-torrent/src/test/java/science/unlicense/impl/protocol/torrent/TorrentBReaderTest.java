

package science.unlicense.impl.protocol.torrent;

import science.unlicense.impl.protocol.torrent.TorrentBReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class TorrentBReaderTest {
        
    @Test
    public void readTest() throws IOException {
        
        final Chars text = new Chars("d5:abcdel4:abcd2:hae3:idxi346ee",CharEncodings.UTF_8);
        final ArrayInputStream in = new ArrayInputStream(text.toBytes());
        
        final TorrentBReader reader = new TorrentBReader();
        reader.setInput(in);
        final Dictionary dico = reader.read();
        
        Assert.assertEquals(2, dico.getSize());
        
        Sequence val1 = (Sequence) dico.getValue(new Chars("abcde"));
        Long val2 = (Long) dico.getValue(new Chars("idx"));
        Assert.assertNotNull(val1);
        Assert.assertNotNull(val2);
        
        Assert.assertEquals(2, val1.getSize());
        Assert.assertEquals(new Chars("abcd"), val1.get(0));
        Assert.assertEquals(new Chars("ha"), val1.get(1));
        Assert.assertEquals(346l, (long)val2);
                
    }
    
}
