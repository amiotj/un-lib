

package science.unlicense.impl.protocol.torrent;

/**
 *
 * @author Johann Sorel
 */
public final class TorrentConstants {
    
    public static final int BENC_STRING_SEPARATOR = ':';
    public static final int BENC_INTEGER_START = 'i';
    public static final int BENC_INTEGER_END = 'e';
    public static final int BENC_LIST_START = 'l';
    public static final int BENC_LIST_END = 'e';
    public static final int BENC_DICO_START = 'd';
    public static final int BENC_DICO_END = 'e';
    
    
    private TorrentConstants(){}
    
}
