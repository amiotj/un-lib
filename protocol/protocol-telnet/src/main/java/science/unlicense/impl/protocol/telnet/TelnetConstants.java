
package science.unlicense.impl.protocol.telnet;

/**
 * Telnet specification constants.
 * 
 * Resources :
 * https://tools.ietf.org/html/rfc854
 * https://tools.ietf.org/html/rfc855
 * https://tools.ietf.org/html/rfc857
 * https://tools.ietf.org/html/rfc858
 * https://tools.ietf.org/html/rfc727
 * https://tools.ietf.org/html/rfc1073
 * https://tools.ietf.org/html/rfc1091
 * https://tools.ietf.org/html/rfc1184
 * https://tools.ietf.org/html/rfc1143
 * https://tools.ietf.org/html/rfc1572
 * 
 * 
 * @author Dave Wilkinson (original source from https://github.com/wilkie/djehuty/blob/master/networking/telnet.d
 * @author Johann Sorel
 */
public final class TelnetConstants {
    
    /** Interpret as Command */
    public static final byte CODE_IAC = (byte)255;        
    /** End of subnegotiation parameters. */
    public static final int CODE_SE  = (byte)240;        
    /** No operation. */
    public static final int CODE_NOP = (byte)241;        
    /**
     * The data stream portion of a Synch.
     * This should always be accompanied
     * by a TCP Urgent notification.
     */
    public static final byte CODE_DM  = (byte)242;        
    /** NVT character BRK. */
    public static final byte CODE_BR  = (byte)243;        
    /** The function IP (Interrupt Process). */
    public static final byte CODE_IP  = (byte)244;        
    /** The function AO (About Output). */
    public static final byte CODE_AO  = (byte)245;        
    /** The function AYT (Are You There???). */
    public static final byte CODE_AYT = (byte)246;        
    /** The function EC (Erase Character). */
    public static final byte CODE_EC  = (byte)247;        
    /** The function EL (Erase Line). */
    public static final byte CODE_EL  = (byte)248;        
    /** The GA signal (Go Ahead). */
    public static final byte CODE_GA  = (byte)249;        
    /**
     * Indicates that what follows is
     * subnegotiation of the indicated
     * option.
     */
    public static final byte CODE_SB  = (byte)250;        


    // Option Code
    /**
     * Indicates the desire to begin performing, or confirmation that
     * you are now performing, the indicated option.
     */
    public static final byte CODE_WILL = (byte)251;       
    /**
     * Indicates the refusal to perform,
     * or continue performing, the indicated option. 
     */
    public static final byte CODE_WONT = (byte)252;        
    /** 
     * Indicates the request that the other party perform, or
     * confirmation that you are expecting the other party to perform, the
     * indicated option.
     */
    public static final byte CODE_DO   = (byte)253;        
    /**
     * Indicates the demand that the other party stop performing,
     * or confirmation that you are no longer expecting the other party
     * to perform, the indicated option.
     */
    public static final byte CODE_DONT = (byte)254;        


    /** Extended with RFC 857 */
    public static final byte CODE_ECHO = (byte)1;
    /** Extended with RFC **/
    public static final byte CODE_SUPPRESS_GO_AHEAD = (byte)3;
    
}
