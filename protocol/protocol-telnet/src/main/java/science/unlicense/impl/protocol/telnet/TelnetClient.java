
package science.unlicense.impl.protocol.telnet;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.IPAddress;

/**
 *
 * @author Johann Sorel
 */
public class TelnetClient {
    
    private final ClientSocket socket;
    
    /**
     * Create an telnet client on default port (7).
     * 
     * @param host telnet server name
     * @throws IOException 
     */
    public TelnetClient(Chars host) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host));
    }   
    
    /**
     * Create an telnet client on default port (23).
     * 
     * @param address telnet server address
     * @throws IOException 
     */
    public TelnetClient(IPAddress address) throws IOException {
        this(address,23);
    }   
    
    /**
     * Create an telnet client.
     * 
     * @param host telnet server name
     * @param port telnet server port
     * @throws IOException 
     */
    public TelnetClient(Chars host, int port) throws IOException {
        this(science.unlicense.system.System.get().getSocketManager().resolve(host),port);
    }   
    
    /**
     * Create an telnet client.
     * 
     * @param address telnet server address
     * @param port telnet server port
     * @throws IOException 
     */
    public TelnetClient(IPAddress address, int port) throws IOException {
        socket = science.unlicense.system.System.get().getSocketManager().createClientSocket(address, port);
    }    
    
    /**
     * Send datas.
     * 
     * @param data byte array to send.
     * @throws IOException 
     */
    public void send(byte[] data) throws IOException{
        socket.getOutputStream().write(data);
        socket.getOutputStream().flush();
    }
    
    
    public void sendCommandWord(byte optionWord, byte commandWord) throws IOException {
        send(new byte[]{TelnetConstants.CODE_IAC,optionWord,commandWord});
    }
    
    /**
     * Close telnet client.
     * 
     * @throws IOException 
     */
    public void close() throws IOException{
        socket.close();
    }
}
