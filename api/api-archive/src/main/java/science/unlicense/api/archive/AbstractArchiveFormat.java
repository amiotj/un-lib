
package science.unlicense.api.archive;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.store.DefaultFormat;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractArchiveFormat extends DefaultFormat implements ArchiveFormat {

    private static final DocumentType EMPTY = new DefaultDocumentType();
    
    public AbstractArchiveFormat(Chars identifier, Chars shortName,Chars longName, 
            Chars[] mimeTypes, Chars[] extensions, byte[][] signatures) {
        super(identifier,shortName,longName,mimeTypes,extensions, signatures);
    }

    public DocumentType getParameters() {
        return EMPTY;
    }

    public boolean canDecode(Object input) throws IOException {
        if(input instanceof Path){
            return canOpen((Path)input);
        }
        return false;
    }

}
