
package science.unlicense.api.archive;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;

import science.unlicense.system.util.ModuleSeeker;

/**
 * Convenient methods for archive manipulation.
 *
 * @author Johann Sorel
 */
public final class Archives {

    private Archives(){}

    /**
     * Lists available archive formats.
     * @return array of ImageFormat, never null but can be empty.
     */
    public static ArchiveFormat[] getFormats(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.api.archive.ArchiveFormat"), 
                Predicates.instanceOf(ArchiveFormat.class));
        final ArchiveFormat[] formats = new ArchiveFormat[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }

    /**
     * Convenient method to obtain an archive path.
     * The method will loop on available formats until one can open the input.
     *
     * @param path
     * @return Archive, never null
     * @throws IOException if not format could support the source.
     */
    public static Archive open(final Path path) throws IOException {
        return open(path,null);
    }
    
    /**
     * Convenient method to obtain an archive path.
     * The method will loop on available formats until one can open the input.
     *
     * @param path
     * @return Archive, never null
     * @throws IOException if not format could support the source.
     */
    public static Archive open(final Path path, Dictionary parameters) throws IOException {
        final ArchiveFormat[] formats = getFormats();
        if(formats.length == 0){
            throw new IOException("No archive formats available.");
        }

        for(int i=0;i<formats.length;i++){
            if(formats[i].canOpen(path)){
                //prepare parameters
                final DocumentType type = formats[i].getParameters();
                final Document p = new DefaultDocument(type);
                if(parameters!=null){
                    for(FieldType param : p.getType().getFields()){
                        Object value = parameters.getValue(param.getId());
                        if(value!=null){
                            p.setFieldValue(param.getId(), value);
                        }
                    }
                }
                
                return formats[i].open(path,p);
            }
        }

        throw new IOException("No archive formats can read given input : "+path);
    }

    public static void extract(Path archive, final Path outFolder) throws IOException{
        if(!(archive instanceof Archive)){
            archive = open(archive);
        }

        recursiveExtract(archive, outFolder);

    }
    
    public static void extract(Path archive, final Path outFolder, Dictionary parameters) throws IOException{
        if(!(archive instanceof Archive)){
            archive = open(archive,parameters);
        }
        
        final Path outPath = outFolder;
        outPath.createContainer();
        final Iterator ite = archive.getChildren().createIterator();
        while(ite.hasNext()){
            recursiveExtract((Path) ite.next(), outPath);
        }
    }

    public static void recursiveExtract(Path source, Path target) throws IOException{

        if(source.isContainer()){
            final Path outPath = target.resolve(source.getName());
            outPath.createContainer();

            final Iterator ite = source.getChildren().createIterator();
            while(ite.hasNext()){
                recursiveExtract((Path) ite.next(), outPath);
            }

        }else{
            final Path outPath = target.resolve(source.getName());
            final ByteInputStream in = source.createInputStream();
            final ByteOutputStream out = outPath.createOutputStream();
            IOUtilities.copy(in,out);
        }

    }

}