

package science.unlicense.api.archive;

import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public interface Archive extends Path, PathResolver {



}