

package science.unlicense.api.archive;

import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.store.Format;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;

/**
 *
 * @author Johann Sorel
 */
public interface ArchiveFormat extends PathFormat, Format{

    public static final FieldType PARAM_ENCODING = new FieldTypeBuilder(new Chars("encoding")).valueClass(CharEncoding.class).build();
    
    /**
     * Archive can be opened with different parameters.
     * Get the different supported parameters for this archive format.
     * 
     * @return DocumentType
     */
    DocumentType getParameters();
    
    /**
     * Test if a path can open by an archive.
     * @param candidate
     * @return true if given path can be opened
     */
    boolean canOpen(Path candidate);

    /**
     * Open archive at given path.
     * The archive is not necessarly open and read right away.
     *
     * @param candidate
     * @param params
     * @return Archive path
     */
    Archive open(Path candidate, Document params);

}