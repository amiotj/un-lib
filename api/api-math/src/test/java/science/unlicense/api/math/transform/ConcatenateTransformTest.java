
package science.unlicense.api.math.transform;

import science.unlicense.api.math.transform.ConcatenateTransform;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.math.Affine;
import org.junit.Test;
import science.unlicense.impl.math.Matrix2x2;
import science.unlicense.impl.math.Vector;
import org.junit.Assert;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.math.Affine1;
import science.unlicense.impl.math.Affine3;

/**
 *
 * @author Johann Sorel
 */
public class ConcatenateTransformTest {
 
    private static final double DELTA = 0.0000001;
    
    /**
     * Check one tuple transform
     */
    @Test
    public void oneValueTest(){
        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform concat = ConcatenateTransform.create(trs1, trs2);
        
        TupleRW tuple = new Vector(1);
        tuple.setX(3);
        concat.transform(tuple, tuple);
        
        Assert.assertEquals(35.0, tuple.getX(), DELTA);
    }
    
    /**
     * Check a array transform.
     */
    @Test
    public void arrayTest(){
        
        final Transform trs1 = new Affine3(
                        2, 0, 0, 0,
                        0, 3, 0, 0,
                        0, 0, 4, 0
                );
        final Transform trs2 = new Affine3(
                        1, 0, 0, 10,
                        0,-1, 0, 20,
                        0, 0, 1, 30
                );      
        final Transform concat = ConcatenateTransform.create(trs1, trs2);
        
        float[] array = {
            0,1,2,
            3,4,5,
            6,7,8        
        };
        
        array = concat.transform(array,0,new float[9],0,3);
        
        Assert.assertArrayEquals(new float[]{
            10, 17, 38,
            16,  8, 50,
            22, -1, 62,
        
        }, array, (float)DELTA);
    }
    
    /**
     * Concatenate should be smart enough to merge the 2 affine transform
     */
    @Test
    public void affineMergeTest(){
        
        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform concat = ConcatenateTransform.create(trs1, trs2);
        
        Assert.assertTrue(concat instanceof Affine);
        final Matrix merge = ((Affine)concat).toMatrix();
        Assert.assertEquals(new Matrix2x2(10, 5, 0, 1), merge);
        
    }
    
}
