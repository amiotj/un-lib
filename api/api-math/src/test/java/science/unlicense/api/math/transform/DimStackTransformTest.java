
package science.unlicense.api.math.transform;

import science.unlicense.api.math.transform.DimStackTransform;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.math.Affine;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Affine1;

/**
 *
 * @author Johann Sorel
 */
public class DimStackTransformTest {
    
    private static final double DELTA = 0.0000001;
    
    @Test
    public void valueTest(){
        
        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform trs3 = new Affine1(2, -12);
        
        final Transform trs = DimStackTransform.create(new Transform[]{trs1,trs2,trs3});
        
        Tuple tuple = new Vector(1,2,3);
        tuple = trs.transform(tuple, null);
        
        Assert.assertEquals(10, tuple.getX(), DELTA);
        Assert.assertEquals(7, tuple.getY(), DELTA);
        Assert.assertEquals(-6, tuple.getZ(), DELTA);
        
        
    }
    
    /**
     * Stack should be smart enough to merge affine transforms
     */
    @Test
    public void mergeTest(){
        
        final Transform trs1 = new Affine1(10, 0);
        final Transform trs2 = new Affine1(1, 5);
        final Transform trs3 = new Affine1(2, -12);
        
        final Transform concat = DimStackTransform.create(new Transform[]{trs1,trs2,trs3});
        
        Assert.assertTrue(concat instanceof Affine);
        final Matrix merge = ((Affine)concat).toMatrix();        
        Assert.assertEquals(new Matrix4x4(
                10, 0, 0, 0,
                 0, 1, 0, 5,
                 0, 0, 2, -12,
                 0, 0, 0, 1), 
                merge);
        
        Tuple tuple = new Vector(1,2,3);
        tuple = concat.transform(tuple, null);
        
        Assert.assertEquals(10, tuple.getX(), DELTA);
        Assert.assertEquals(7, tuple.getY(), DELTA);
        Assert.assertEquals(-6, tuple.getZ(), DELTA);
        
        
    }
    
}
