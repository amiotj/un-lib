
package science.unlicense.api.unit;

import science.unlicense.api.unit.Units;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class UnitsTest {

    private static final double DELTA = 0.0000001;
    
    @Test
    public void createTransformTest(){
        
        Transform trs = Units.getTransform(Units.CENTIMETER, Units.METER);
        
        TupleRW t = new Vector(1);
        t.setX(1);        
        t = trs.transform(t, t);        
        Assert.assertEquals(0.01, t.getX(),DELTA);
     
        trs = Units.getTransform(Units.KILOMETER, Units.METER);        
        t = new Vector(1);
        t.setX(1);        
        t = trs.transform(t, t);        
        Assert.assertEquals(1000, t.getX(),DELTA);
        
    }
    
}
