
package science.unlicense.api.math.transform;

import science.unlicense.api.math.transform.RearrangeTransform;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.math.Affine;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class RearrangeTransformTest {
    
    private static final double DELTA = 0.0000001;
    
    /**
     * Check reordering values
     */
    @Test
    public void reorderTest(){
        
        final int[] mapping = {2,0,1};
        final Transform trs = RearrangeTransform.create(mapping);
        Assert.assertEquals(3, trs.getInputDimensions());
        Assert.assertEquals(3, trs.getOutputDimensions());
        
        Tuple tuple = new Vector(0,1,2);
        tuple = trs.transform(tuple, null);
        
        Assert.assertEquals(1, tuple.getX(), DELTA);
        Assert.assertEquals(2, tuple.getY(), DELTA);
        Assert.assertEquals(0, tuple.getZ(), DELTA);
        
    }
    
    @Test
    public void reorderAndReduceTest(){
        final int[] mapping = {-1,1,0};
        final Transform trs = RearrangeTransform.create(mapping);
        Assert.assertEquals(3, trs.getInputDimensions());
        Assert.assertEquals(2, trs.getOutputDimensions());
        
        Tuple tuple = new Vector(0,1,2);
        tuple = trs.transform(tuple, null);
        Assert.assertEquals(2, tuple.getSize());
        
        Assert.assertEquals(2, tuple.getX(), DELTA);
        Assert.assertEquals(1, tuple.getY(), DELTA);
    }
    
    @Test
    public void toMatrixtext(){
        final int[] mapping = {2,0,1};
        final Affine trs = (Affine) RearrangeTransform.create(mapping);
        final Matrix m = trs.toMatrix();
        
        Tuple tuple = new Vector(0,1,2,1);
        tuple = m.transform(tuple, null);
        
        Assert.assertEquals(1, tuple.getX(), DELTA);
        Assert.assertEquals(2, tuple.getY(), DELTA);
        Assert.assertEquals(0, tuple.getZ(), DELTA);
        Assert.assertEquals(1, tuple.getW(), DELTA);
    }
    
}
