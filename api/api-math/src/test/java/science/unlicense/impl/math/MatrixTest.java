package science.unlicense.impl.math;

import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Angles;
import science.unlicense.api.math.Tuple;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.TupleRW;


/**
 *
 * @author Johann Sorel
 */
public class MatrixTest {

    private static final double DELTA = 0.000000001;

    @Test
    public void testIsIdentity(){
        DefaultMatrix m = new DefaultMatrix(
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1);
        Assert.assertTrue(m.isIdentity());

        m = new DefaultMatrix(
                1,0,0,0,
                0,1,0,2,
                0,0,-1,0,
                0,0,0,1);
        Assert.assertFalse(m.isIdentity());
    }

    @Test
    public void testAdd(){

        final DefaultMatrix expected = new DefaultMatrix(
                11,22,33,44,
                0,55,66,77,
                0,0,88,99,
                0,0,0,0);

        DefaultMatrix m1 = new DefaultMatrix(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        DefaultMatrix m2 = new DefaultMatrix(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        Matrix res = m1.add(m2,null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testAddArrayCreation(){

        final double[][] m1 = new double[][]{
            {1,2,3,4},
            {0,5,6,7},
            {0,0,8,9}};

        final double[][] m2 = new double[][]{
            {10,20,30,40},
            {0,50,60,70},
            {0,0,80,90}};

        final double[][] expected = new double[][]{
            {11,22,33,44},
            {0,55,66,77},
            {0,0,88,99}};

        double[][] res = Matrices.add(m1, m2, null);

        Assert.assertArrayEquals(expected, res);
    }

    @Test
    public void testAddLocal(){

        final DefaultMatrix expected = new DefaultMatrix(
                11,22,33,44,
                0,55,66,77,
                0,0,88,99,
                0,0,0,0);

        DefaultMatrix m1 = new DefaultMatrix(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        DefaultMatrix m2 = new DefaultMatrix(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        m1.localAdd(m2);

        Assert.assertEquals(expected, m1);
    }

    @Test
    public void testSubtract(){

        final DefaultMatrix expected = new DefaultMatrix(
                 9,18,27,36,
                0,45,54,63,
                0,0,72,81,
                0,0,0,0);

        DefaultMatrix m1 = new DefaultMatrix(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        DefaultMatrix m2 = new DefaultMatrix(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        Matrix res = m1.subtract(m2, null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testSubtractLocal(){

        final DefaultMatrix expected = new DefaultMatrix(
                 9,18,27,36,
                0,45,54,63,
                0,0,72,81,
                0,0,0,0);

        DefaultMatrix m1 = new DefaultMatrix(
                10,20,30,40,
                0,50,60,70,
                0,0,80,90,
                0,0,0,0);
        DefaultMatrix m2 = new DefaultMatrix(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,0);
        m1.localSubtract(m2);

        Assert.assertEquals(expected, m1);
    }

    @Test
    public void testScale(){

        final DefaultMatrix expected = new DefaultMatrix(
                2,4,6,8,
                0,10,12,14,
                0,0,16,18,
                0,0,0,20);

        DefaultMatrix m1 = new DefaultMatrix(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,10);
        Matrix res = m1.scale(2, null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testScaleLocal(){

        final DefaultMatrix expected = new DefaultMatrix(
                2,4,6,8,
                0,10,12,14,
                0,0,16,18,
                0,0,0,20);

        DefaultMatrix m1 = new DefaultMatrix(
                1,2,3,4,
                0,5,6,7,
                0,0,8,9,
                0,0,0,10);
        m1.localScale(2);

        Assert.assertEquals(expected, m1);
    }

    @Test
    public void testTranspose(){

        final DefaultMatrix expected = new DefaultMatrix(4,3);
        expected.set(new double[]{
                        1,0,0,
                        2,5,0,
                        3,6,8,
                        4,7,9 });

        DefaultMatrix m1 = new DefaultMatrix(3,4);
        m1.set(new double[]{
                1,2,3,4,
                0,5,6,7,
                0,0,8,9});
        DefaultMatrix res = m1.transpose();

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testVectorTransform(){

        final Vector v = new Vector(1,2,3,1);
        final DefaultMatrix m = new DefaultMatrix(4,4);
        final Vector res = new Vector(4);


        //identity test
        m.set(new double[]{
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1});
        m.transform(v, res);
        Assert.assertEquals(1, res.get(0), DELTA);
        Assert.assertEquals(2, res.get(1), DELTA);
        Assert.assertEquals(3, res.get(2), DELTA);

        //translation test
        m.set(new double[]{
                1,0,0,50,
                0,1,0,60,
                0,0,1,70,
                0,0,0,1});
        m.transform(v, res);
        Assert.assertEquals(51, res.get(0), DELTA);
        Assert.assertEquals(62, res.get(1), DELTA);
        Assert.assertEquals(73, res.get(2), DELTA);

        //scale test
        m.set(new double[]{
                10,0,0,0,
                0,100,0,0,
                0,0,1000,0,
                0,0,0,1});
        m.transform(v, res);
        Assert.assertEquals(10, res.get(0), DELTA);
        Assert.assertEquals(200, res.get(1), DELTA);
        Assert.assertEquals(3000, res.get(2), DELTA);

    }

    @Test
    public void testMultiply(){
        final DefaultMatrix expected = new DefaultMatrix(2,2);
        expected.set(new double[]{
                        9, 7,
                        23, 9
                    });

        final DefaultMatrix m1 = new DefaultMatrix(2,3);
        m1.set(new double[]{
                1, 2, 0,
                4, 3,-1
                });
        final DefaultMatrix m2 = new DefaultMatrix(3,2);
        m2.set(new double[]{
                5,1,
                2,3,
                3,4
                });

        Matrix res = m1.multiply(m2, null);

        Assert.assertEquals(expected, res);
    }

    @Test
    public void testRotationMatrix(){
        final double angle = 12.37;
        double[][] res;

        double[][] expected;
        expected = new double[][]{
        {1,    0,                  0,                  0},
        {0,    Math.cos(angle),    -Math.sin(angle),   0},
        {0,    Math.sin(angle),    Math.cos(angle),    0},
        {0,    0,                  0,                  1}
        };

        res = Matrices.createRotation4(angle, new Vector(1, 0, 0),null);
        Assert.assertEquals(new DefaultMatrix(expected), new DefaultMatrix(res));

        expected = new double[][]{
            {Math.cos(angle),  0,    Math.sin(angle),    0},
            {0,                1,    0,                  0},
            {-Math.sin(angle), 0,    Math.cos(angle),    0},
            {0,                0,    0,                  1}
        };

        res = Matrices.createRotation4(angle, new Vector(0, 1, 0),null);
        Assert.assertEquals(new DefaultMatrix(expected), new DefaultMatrix(res));

        expected = new double[][]{
          {Math.cos(angle),  -Math.sin(angle),   0,  0},
          {Math.sin(angle),  Math.cos(angle),    0,  0},
          {0,                0,                  1,  0},
          {0,                0,                  0,  1}
        };
        res = Matrices.createRotation4(angle, new Vector(0, 0, 1),null);
        Assert.assertEquals(new DefaultMatrix(expected), new DefaultMatrix(res));

    }

    @Test
    public void testTransform2(){
        final double angle = Math.toRadians(90);

        DefaultMatrix matrix = new DefaultMatrix(Matrices.createRotation4(angle, new Vector(0, 1, 0), null));


        final Vector v = new Vector(0, 0, 1, 0);

        matrix.transform(v,v);

        Assert.assertEquals(4, v.getSize());
        Assert.assertEquals(1, v.get(0), DELTA);
        Assert.assertEquals(0, v.get(1), DELTA);
        Assert.assertEquals(0, v.get(2), DELTA);
        Assert.assertEquals(0, v.get(2), DELTA);

    }

    @Test
    public void testFromEuler(){
        final TupleRW euler = new DefaultTuple(3);
        final Matrix3x3 rotation = new Matrix3x3();

        //(0, 0, 0) test identity
        euler.setXYZ(0, 0, 0);
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    {+1,  0,  0},
                    { 0, +1,  0},
                    { 0,  0, +1}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // 90° around X
        euler.setXYZ(0, 0, Angles.degreeToRadian(90));
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    {+1,  0,  0},
                    { 0,  0, -1},
                    { 0, +1,  0}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // 90° around Y
        euler.setXYZ(0, Angles.degreeToRadian(90), 0);
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    { 0,  0, +1},
                    { 0, +1,  0},
                    {-1,  0,  0}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // -90° around Z
        euler.setXYZ(Angles.degreeToRadian(-90), 0, 0);
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                new double[][]{
                    { 0, +1,  0},
                    {-1,  0,  0},
                    { 0,  0, +1}},
                rotation.getValuesCopy(),
                DELTA)
                );

        // 30° around X, 55° around Z
        euler.setXYZ(Angles.degreeToRadian(55), 0, Angles.degreeToRadian(30));
        rotation.fromEuler(euler);
        Assert.assertTrue(
                Arrays.equals(
                    Matrix3x3.createRotation3(
                        Angles.degreeToRadian(55), new DefaultTuple(0,0,1))
                    .multiply(
                        Matrix3x3.createRotation3(
                            Angles.degreeToRadian(30), new DefaultTuple(1,0,0))
                    ).getValuesCopy(),
                    rotation.getValuesCopy(),
                    DELTA)
                );
        
    }

    @Test
    public void testFromAndToEuler(){
        Vector euler;
        Vector res;
        
        euler = new Vector(0, 0, 0);
        res = Matrix3x3.createRotationEuler(euler).toEuler();
        Assert.assertArrayEquals(euler.getValues(), res.getValues(), DELTA);
        
        //test full range on each axis YAW -180/+180
        for(int i=-180;i<=180;i++){
            euler = new Vector(tr(i),0,0);
            res = Matrix3x3.createRotationEuler(euler).toEuler();
            Assert.assertArrayEquals("fail for "+euler,euler.getValues(), res.getValues(), DELTA);
        }
        
        //PITCH -90/+90
        for(int i=-90;i<=90;i++){
            euler = new Vector(0,tr(i),0);
            res = Matrix3x3.createRotationEuler(euler).toEuler();
            Assert.assertArrayEquals("fail for "+euler,euler.getValues(), res.getValues(), DELTA);
        }
        
        //ROLL -180/+180
        for(int i=-180;i<=180;i++){            
            euler = new Vector(0,0,tr(i));
            res = Matrix3x3.createRotationEuler(euler).toEuler();
            Assert.assertArrayEquals("fail for "+euler,euler.getValues(), res.getValues(), DELTA);
        }
        
    }
    
    @Test
    public void testRotationVectorToVector(){

        Vector v1 = new Vector(1, 0, 0);
        Vector v2 = new Vector(0, 0, 1);

        Matrix3x3 m = Matrix3x3.createRotation(v1, v2);

        Tuple res = m.transform(v1);
        Assert.assertArrayEquals(res.getValues(), v2.values, DELTA);

        //test colinear vectors, we must obtain an identity matrix
        v1 = new Vector(0, 1, 0);
        v2 = new Vector(0, 5, 0);

        m = Matrix3x3.createRotation(v1, v2);
        Assert.assertTrue(m.isIdentity());

    }

    private static double tr(double value){
        return Angles.degreeToRadian(value);
    }
}
