
package science.unlicense.impl.math;

import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.TupleRW;

/**
 * @author Bertrand COTE
 */
public class MatricesTest {
    
    private static final double EPSILON = 1e-12;
    
    private static float[] doubleArrayToFloatArray( double[] source ) {
        float[] target = new float[source.length];
        for( int i=0; i<source.length; i++ ) target[i] = (float)source[i];
        return target;
    }
    
//    private static double[][] rotateXMatrix3D( double theta ) {
//        double[][] rotate = new double[3][3];
//        rotate[0][0] =  1.;
//        rotate[1][1] =  Math.cos(theta);
//        rotate[2][2] =  Math.cos(theta);
//        
//        rotate[1][2] = -Math.sin(theta);
//        rotate[2][1] =  Math.sin(theta);
//        return rotate;
//    };
//    
//    private static double[][] rotateYMatrix3D( double theta ) {
//        double[][] rotate = new double[3][3];
//        rotate[0][0] =  Math.cos(theta);
//        rotate[1][1] =  1.;
//        rotate[2][2] =  Math.cos(theta);
//        
//        rotate[2][0] = -Math.sin(theta);
//        rotate[0][2] =  Math.sin(theta);
//        return rotate;
//    };
//    
//    private static double[][] rotateZMatrix3D( double theta ) {
//        double[][] rotate = new double[3][3];
//        rotate[0][0] =  Math.cos(theta);
//        rotate[1][1] =  Math.cos(theta);
//        rotate[2][2] =  1.;
//        
//        rotate[0][1] = -Math.sin(theta);
//        rotate[1][0] =  Math.sin(theta);
//        return rotate;
//    };
    
    private static final double[][][] matricesData = new double[][][]{
        { 
            { 1,  0,  0 },
            { 0,  1,  0 },
            { 0,  0,  1 }
        },
        { 
            { 1,  0,  0 },
            { 0, -1,  0 },
            { 0,  0,  2 }
        },
        { 
            { -2,  8, -7 },
            {  3,  1,  4 },
            { -1,  3, -4 }
        },
        {
            {  3,  5, -9, -1 },
            { -8, -4,  3,  5 },
            { -6,  5,  7, -9 }, 
        },
    };
    
    private static final boolean[] isIdentityTest = new boolean[]{
        true,
        false,
        false,
        false,
    };
    
    private static final double[][][][] addSubtractTest = new double[][][][]{
        { // ========== test datas 1 ==========
            {   // m1
                {  3,  5, -9, -1 }, 
                { -8, -4,  3,  5 }, 
                { -6,  5,  7, -9 }, 
            },
            {   // m2
                {  1,  2,  3, -4 }, 
                { -4, -5, -6, -5 }, 
                {  7,  8,  9,  1 }, 
            },
            {   // add(m1, m2)
                {  4,  7, -6, -5 }, 
                {-12, -9, -3,  0 }, 
                {  1, 13, 16, -8 },
            },
            {   // subtract(m1, m2)
                {  2,  3,-12,  3 }, 
                { -4,  1,  9, 10 }, 
                {-13, -3, -2,-10 },
            },
            {   // scale factor
                { 0.5 } 
            },
            {   // scale m1 expResult with above scale factor
                {  1.5,  2.5, -4.5, -0.5 }, 
                { -4.0, -2.0,  1.5,  2.5 }, 
                { -3.0,  2.5,  3.5, -4.5 },
            },
            {   // tuple scale factors
                { 0.5, -.25, -2, 2 }
            }, 
            {   // scale m1 expResult with above tuple
                {  1.5, -1.25,  18,  -2 }, 
                { -4.0,  1.00,  -6,  10 }, 
                { -3.0, -1.25, -14, -18 }, 
            },
            {  // transpose m1
                {  3, -8, -6 }, 
                {  5, -4,  5 }, 
                { -9,  3,  7 }, 
                { -1,  5, -9 },
            },
            {   // dot(m1, m2)
                {  51 },
            },
            {   // multiplication: m1*transpose(m2)
                { -10, 22, -21 }, 
                { -27,  9, -56 }, 
                {  61,  2,  52 },
            },
        },
        {// ========== test datas 2 ==========
            {   // m1
                {  3,  5 },
                { -8, -4 },
                { -6,  5 },
            },
            {   // m2
                {  1,  2 },
                { -4, -5 },
                {  7,  8 },
            },
            {   // add(m1, m2)
                {  4,  7 },
                {-12, -9 },
                {  1, 13 },
            },
            {   // subtract(m1, m2)
                {  2,  3 },
                { -4,  1 },
                {-13, -3 },
            },
            {   // scale factor
                { 2.0 }
            },
            {   // scale m1 expResult with above scale factor
                {  6,  10 },
                { -16, -8 },
                { -12, 10 },
            },
            {   // tuple scale factors
                { 0.25, -.75 }
            }, 
            {   // scale m1 expResult with above tuple
                {  0.75, -3.75 },
                { -2.0,   3.0 },
                { -1.5 , -3.75 },
            },
            {  // transpose m1
                {  3, -8, -6 },
                {  5, -4,  5 },
            },
            {   // dot(m1, m2)
                {  63 },
            },
            {   // multiplication: m1*transpose(m2)
                {  13, -37,  61 },
                { -16,  52, -88 },
                {   4,  -1,  -2 },
            },
        },
    };
    
    private static final double[][][][] powerTest = new double[][][][] {
        {
            {   // m1
                {   -2,     8,   -7 }, 
                {    3,    1,     4 }, 
                {   -1,     3,   -4 } 
            },
            {   // m1^2
                {   35,   -29,   74 }, 
                {   -7,   37,   -33 }, 
                {   15,   -17,   35 } 
            },
            {   // m1^3
                { -231,   473, -657 }, 
                {  158, -118,   329 }, 
                { -116,   208, -313 } 
            },
            {   // m1^4
                { 2538, -3346, 6137 }, 
                { -999, 2133, -2894 }, 
                { 1169, -1659, 2896 } 
            },
        },
    };
    
    private static final double [][][][] invertTest = new double[][][][] {
        {
            {   // origValues
                { -2,  8, -7 }, 
                {  3,  1,  4 }, 
                { -1,  3, -4 }},
            {   // invert(origValues)
                { -0.6153846153846154,  0.4230769230769230,  1.5 }, 
                {  0.3076923076923078,  0.0384615384615385, -0.5 }, 
                {  0.3846153846153846, -0.0769230769230769, -1.0 }, 
            }
        },
        {
            {   // origValues
                { Math.cos(1.), -Math.sin(1.) },
                { Math.sin(1.),  Math.cos(1.) },
            },
            {   // invert(origValues)
                { Math.cos(-1.), -Math.sin(-1.) },
                { Math.sin(-1.),  Math.cos(-1.) },
            }
        },
    };
    
    private static final double [][][][] transformTest = new double[][][][] {
        {
            {   // matrix: 0.5 radians rotation
                { 0.877582561890373, -0.479425538604203 },
                { 0.479425538604203,  0.877582561890373 },
            },
            {   // vector
                { 1., 0.}
            },
            {   // expResult
                { 0.877582561890373, 0.479425538604203 }
            },
        },
        {
            {   // matrix: -1.0 radians rotation
                {  0.540302305868140, 0.841470984807897 },
                { -0.841470984807897, 0.540302305868140 },
            },
            {   // vector
                { 0., 1.}
            },
            {   // expResult
                { 0.841470984807897, 0.540302305868140 }
            },
        },
        {
            {   // matrix
                { -7,  2,  6 },
                {  1, -5,  8 },
            },
            {   // vector
                { -2., 5, 10 }
            },
            {   // expResult
                { 84, 53}
            },
        },
        {
            {   // matrix
                { -7,  2,  6 },
                {  1, -5,  8 },
            },
            {   // vector
                { -2.,0 ,10 }
            },
            {   // expResult
                { 74 ,78 }
            },
        },
    };
    
    public static final double[][][] focusedOrbitTest = new double[][][]{
        {
            //input parameters
            {0.0,0.0,0.0,5.0},
            //expected final position
            {5.0,0.0,0.0,1.0},
            //expected matrix
            { 1, 0, 0, 5, 
              0, 1, 0, 0, 
              0, 0, 1, 0, 
              0, 0, 0, 1 }
        },
        {
            {Maths.HALF_PI,0.0,0.0,5.0},
            {0.0,5.0,0.0,1.0},
            { 0,-1, 0, 0, 
              1, 0, 0, 5, 
              0, 0, 1, 0, 
              0, 0, 0, 1 }
        },
        {
            {0.0,Maths.HALF_PI,0.0,5.0},
            {0.0,0.0,5.0,1.0},
            { 0, 0,-1, 0, 
              0, 1, 0, 0, 
              1, 0, 0, 5, 
              0, 0, 0, 1 }
        },
        {
            {Maths.HALF_PI,Maths.HALF_PI,0.0,5.0},
            {0.0,0.0,5.0,1.0},
            { 0,-1, 0, 0, 
              0, 0,-1, 0, 
              1, 0, 0, 5, 
              0, 0, 0, 1 }
        },
//         { //roll should have no effect on position
//            {0.0,Maths.HALF_PI,0.2,5.0},
//            {0.0,0.0,5.0,1.0},
//            { 0, 0,-1, 0, 
//              0, 1, 0, 0, 
//              1, 0, 0, 5, 
//              0, 0, 0, 1 }
//        },
    };
    
    
    public MatricesTest() {
    }

    /**
     * Test of isIdentity method, of class Matrices.
     */
    @Test
    public void testIsIdentity() {
        for( int i=0; i<isIdentityTest.length; i++ ) {
            double[][] m = matricesData[i];
            boolean expResult = isIdentityTest[i];
            boolean result = Matrices.isIdentity(m);
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of setToIdentity method, of class Matrices.
     */
    @Test
    public void testSetToIdentity() {
        for( int i=0; i<isIdentityTest.length; i++ ) {
            double[][] m = Arrays.copy(matricesData[i]);
            try {
                Matrices.setToIdentity(m);
                Assert.assertTrue( Matrices.isIdentity(m) );
            } catch ( InvalidArgumentException iae ) {
                Assert.assertTrue( m.length != m[0].length );
            }
        }
    }
    
    /**
     * Test of identity method, of class Matrices.
     */
    @Test
    public void testIdentity() {
        final int nMax = 10;
        for( int n=1; n<nMax; n++ ) {
            double[][] identity = Matrices.identity(n);
            Assert.assertTrue( Matrices.isIdentity(identity) );
        }
        
    }

    /**
     * Test of localAdd method, of class Matrices.
     */
    @Test
    public void testLocalAdd() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Arrays.copy(addSubtractTest[i][1]);
            double[][] expResult = addSubtractTest[i][2];
            double[][] result = Matrices.localAdd(m1, m2);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of add method, of class Matrices.
     */
    @Test
    public void testAdd() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Arrays.copy(addSubtractTest[i][1]);
            double[][] expResult = addSubtractTest[i][2];
            double[][] buffer = null;
            double[][] result = null;
            // with buffer null
            result = Matrices.add(m1, m2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            // with buffer not null
            buffer = new double[m1.length][m1[0].length];
            result = Matrices.add(m1, m2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer));
        }
    }

    /**
     * Test of localSubtract method, of class Matrices.
     */
    @Test
    public void testLocalSubtract() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Arrays.copy(addSubtractTest[i][1]);
            double[][] expResult = addSubtractTest[i][3];
            double[][] result = Matrices.localSubtract(m1, m2);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Matrices.
     */
    @Test
    public void testSubtract() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Arrays.copy(addSubtractTest[i][1]);
            double[][] expResult = addSubtractTest[i][3];
            double[][] buffer = null;
            double[][] result = null;
            // with buffer null
            result = Matrices.subtract(m1, m2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            // with buffer not null
            buffer = new double[m1.length][m1[0].length];
            result = Matrices.subtract(m1, m2, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer));
        }
    }

    /**
     * Test of localScale method, of class Matrices.
     */
    @Test
    public void testLocalScale_doubleArrArr_double() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double scale = addSubtractTest[i][4][0][0];
            double[][] expResult = Arrays.copy(addSubtractTest[i][5]);
            double[][] result = Matrices.localScale(m1, scale);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localScale method, of class Matrices.
     */
    @Test
    public void testLocalScale_doubleArrArr_doubleArr() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[] tuple = addSubtractTest[i][6][0];
            double[][] expResult = Arrays.copy(addSubtractTest[i][7]);
            double[][] result = Matrices.localScale(m1, tuple);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Matrices.
     */
    @Test
    public void testScale_3args_1() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[] tuple = addSubtractTest[i][6][0];
            double[][] expResult = Arrays.copy(addSubtractTest[i][7]);
            
            double[][] buffer = null;
            double[][] result = Matrices.scale(m1, tuple, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new double[m1.length][m1[0].length];
            result = Matrices.scale(m1, tuple, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer));
        }
    }

    /**
     * Test of scale method, of class Matrices.
     */
    @Test
    public void testScale_3args_2() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double scale = addSubtractTest[i][4][0][0];
            double[][] expResult = Arrays.copy(addSubtractTest[i][5]);
            
            double[][] buffer = null;
            double[][] result = Matrices.scale(m1, scale, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new double[m1.length][m1[0].length];
            result = Matrices.scale(m1, scale, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer));
        }
    }

    /**
     * Test of power method, of class Matrices.
     */
    @Test
    public void testPower() {
        for( int i=0; i<powerTest.length; i++ ) {
            double[][] m1 = powerTest[i][0];
            for( int n=0; n<powerTest[i].length; n++ ) {
                int power = n+1;
                double[][] expResult = powerTest[i][n];
                
                double[][] buffer = null;
                double[][] result = Matrices.power(m1, power, buffer);
                Assert.assertTrue( Arrays.equals(expResult, result));
                
                buffer = new double[m1.length][m1[0].length];
                result = Matrices.power(m1, power, buffer);
                Assert.assertTrue( Arrays.equals(expResult, result));
                Assert.assertTrue( Arrays.equals(expResult, buffer));
            }
        }
    }

    /**
     * Test of transpose method, of class Matrices.
     */
    @Test
    public void testTranspose() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] expResult = addSubtractTest[i][8];
            double[][] result = Matrices.transpose(m1);
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localInvert method, of class Matrices.
     */
    @Test
    public void testLocalInvert() {
        for( int i=0; i<invertTest.length; i++ ) {
            double[][] origValues = Arrays.copy(invertTest[i][0]);
            double[][] expResult = invertTest[i][1];
            double[][] result = Matrices.localInvert(origValues);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
        }
    }

    /**
     * Test of invert method, of class Matrices.
     */
    @Test
    public void testInvert() {
        for( int i=0; i<invertTest.length; i++ ) {
            double[][] origValues = Arrays.copy(invertTest[i][0]);
            double[][] expResult = invertTest[i][1];
            
            double[][] buffer = null;
            double[][] result = Matrices.invert(origValues, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            
            buffer = new double[origValues.length][origValues[0].length];
            result = Matrices.invert(origValues, buffer);
            Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            Assert.assertTrue( Arrays.equals(expResult, buffer, EPSILON));
        }
    }

    /**
     * Test of dot method, of class Matrices.
     */
    @Test
    public void testDot() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Arrays.copy(addSubtractTest[i][1]);
            double expResult = addSubtractTest[i][9][0][0];
            double result = Matrices.dot(m1, m2);
            Assert.assertEquals( expResult, result, EPSILON );
        }
    }

    /**
     * Test of transformLocal method, of class Matrices.
     */
    @Test
    public void testTransformLocal_doubleArrArr_Tuple() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = transformTest[i][0];
            TupleRW vector = new DefaultTuple( Arrays.copy(transformTest[i][1][0]));
            if ( vector.getSize() == matrix[0].length) {
                Tuple expResult = new DefaultTuple( Arrays.copy(transformTest[i][2][0]));
                Tuple result = Matrices.transformLocal(matrix, vector);
                Assert.assertArrayEquals(expResult.getValues(), Arrays.resize(result.getValues(),expResult.getSize()),EPSILON);
            }
        }
    }

    /**
     * Test of transformLocal method, of class Matrices.
     */
    @Test
    public void testTransformLocal_3args() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = Arrays.copy(transformTest[i][0]);
            TupleRW vector = new DefaultTuple( Arrays.copy(transformTest[i][1][0]));
            Tuple expResult = new DefaultTuple( Arrays.copy(transformTest[i][2][0]));
            
            Tuple result = Matrices.transformLocal(matrix, vector);
            Assert.assertArrayEquals(expResult.getValues(), Arrays.resize(result.getValues(),expResult.getSize()),EPSILON);
        }
    }

    /**
     * Test of transform method, of class Matrices.
     */
    @Test
    public void testTransform_3args_1() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = transformTest[i][0];
            Tuple vector = new DefaultTuple( Arrays.copy(transformTest[i][1][0]));
            Tuple expResult = new DefaultTuple( Arrays.copy(transformTest[i][2][0]));
            if ( vector.getSize() == matrix[0].length) {
                TupleRW buffer = null;
                Tuple result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue(expResult.equals(result));

                buffer = new DefaultTuple(matrix.length);
                result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue(expResult.equals(result));
                Assert.assertTrue(expResult.equals(buffer));
            }
        }
    }

    /**
     * Test of transform method, of class Matrices.
     */
    @Test
    public void testTransform_4args_1() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = Arrays.copy(transformTest[i][0]);
            Tuple vector = new DefaultTuple( Arrays.copy(transformTest[i][1][0]));
            Tuple expResult = new DefaultTuple( Arrays.copy(transformTest[i][2][0]));
                        
            TupleRW buffer = null;
            Tuple result = Matrices.transform(matrix, vector, buffer);
            Assert.assertTrue(expResult.equals(result));
            
            buffer = new DefaultTuple(matrix.length );
            result = Matrices.transform(matrix, vector, buffer);
            Assert.assertTrue(expResult.equals(result));
            Assert.assertTrue(expResult.equals(buffer));
        }
    }

    /**
     * Test of transform method, of class Matrices.
     */
    @Test
    public void testTransform_3args_2() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = Arrays.copy(transformTest[i][0]);
            double[] vector = Arrays.copy(transformTest[i][1][0]);
            double[] expResult = Arrays.copy(transformTest[i][2][0]);
            if ( vector.length == matrix[0].length) {
                double[] buffer = null;
                double[] result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue( Arrays.equals( expResult, result ) );

                buffer = new double[matrix.length];
                result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue( Arrays.equals( expResult, result ) );
                Assert.assertTrue( Arrays.equals( expResult, buffer ) );
            }
        }
    }

    /**
     * Test of transform method, of class Matrices.
     */
    @Test
    public void testTransform_4args_2() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = Arrays.copy(transformTest[i][0]);
            double[] vector = Arrays.copy(transformTest[i][1][0]);
            double[] expResult = Arrays.copy(transformTest[i][2][0]);
                        
            double[] buffer = null;
            double[] result = Matrices.transform(matrix, vector, buffer);
            Assert.assertTrue( Arrays.equals( expResult, result, EPSILON ) );
            
            buffer = new double[matrix.length];
            result = Matrices.transform(matrix, vector, buffer);
            Assert.assertTrue( Arrays.equals( expResult, result, EPSILON ) );
            Assert.assertTrue( Arrays.equals( expResult, buffer, EPSILON ) );
        }
    }

    /**
     * Test of transform method, of class Matrices.
     */
    @Test
    public void testTransform_3args_3() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = Arrays.copy(transformTest[i][0]);
            float[] vector = doubleArrayToFloatArray(transformTest[i][1][0]);
            float[] expResult = doubleArrayToFloatArray(transformTest[i][2][0]);
            if ( vector.length == matrix[0].length) {
                float[] buffer = null;
                float[] result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue( Arrays.equals( expResult, result ) );

                buffer = new float[matrix.length];
                result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue( Arrays.equals( expResult, result ) );
                Assert.assertTrue( Arrays.equals( expResult, buffer ) );
            }
        }
    }

    /**
     * Test of transform method, of class Matrices.
     */
    @Test
    public void testTransform_4args_3() {
        for( int i=0; i<transformTest.length; i++ ) {
            double[][] matrix = Arrays.copy(transformTest[i][0]);
            float[] vector = doubleArrayToFloatArray(transformTest[i][1][0]);
            float[] expResult = doubleArrayToFloatArray(transformTest[i][2][0]);
            if ( vector.length == matrix[0].length) {
                
                float[] buffer = null;
                float[] result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue( Arrays.equals( expResult, result ) );

                buffer = new float[matrix.length];
                result = Matrices.transform(matrix, vector, buffer);
                Assert.assertTrue( Arrays.equals( expResult, result ) );
                Assert.assertTrue( Arrays.equals( expResult, buffer ) );
            }
        }
    }

    /**
     * Test of localMultiply method, of class Matrices.
     */
    @Test
    public void testLocalMultiply() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Matrices.transpose( Arrays.copy(addSubtractTest[i][1]) );
            double[][] result;
            try {
                result = Matrices.localMultiply(m1, m2);
                double[][] expResult = addSubtractTest[i][10];
                Assert.assertTrue( Arrays.equals( expResult, result ));
            } catch (InvalidArgumentException iae) {
                Assert.assertTrue( (m1.length != m1[0].length) || (m2.length != m2[0].length) || (m1.length != m2.length) );
            }
        }
    }

    /**
     * Test of multiply method, of class Matrices.
     */
    @Test
    public void testMultiply() {
        for( int i=0; i<addSubtractTest.length; i++ ) {
            double[][] m1 = Arrays.copy(addSubtractTest[i][0]);
            double[][] m2 = Matrices.transpose( Arrays.copy(addSubtractTest[i][1]) );
            double[][] expResult = addSubtractTest[i][10];
            
            double[][] buffer = null;
            double[][] result = Matrices.multiply(m1, m2, buffer);
            Assert.assertTrue( Arrays.equals( expResult, result ));
            
            buffer = new double[m1.length][m2[0].length];
            result = Matrices.multiply(m1, m2, buffer);
            Assert.assertTrue( Arrays.equals( expResult, result ));
            Assert.assertTrue( Arrays.equals( expResult, buffer ));
        }
    }

    @Test
    public void testFocusedOrbit(){
        
        final double[] center = new double[]{0,0,0,1};
        final double[] forward = new double[]{1,0,0,0};
        
        for(double[][] testcase : focusedOrbitTest){
            final double xangle = testcase[0][0];
            final double yangle = testcase[0][1];
            final double rollangle = testcase[0][2];
            final double distance = testcase[0][3];
            
            final Matrix4x4 result = Matrix4x4.focusedOrbit(xangle, yangle, rollangle, distance);
            Assert.assertArrayEquals(testcase[1], result.transform(center),EPSILON);
            Assert.assertArrayEquals(testcase[2], result.toArrayDouble(),EPSILON);
        }
        
    }
    
//    /**
//     * Test of createRotation3 method, of class Matrices.
//     */
//    @Test
//    public void testCreateRotation3() {
//        System.out.println("createRotation3");
//        double angle = 0.0;
//        Tuple rotateAxis = null;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.createRotation3(angle, rotateAxis, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }

//    /**
//     * Test of createRotation4 method, of class Matrices.
//     */
//    @Test
//    public void testCreateRotation4() {
//        System.out.println("createRotation4");
//        double angle = 0.0;
//        Tuple rotateAxis = null;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.createRotation4(angle, rotateAxis, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }

//    /**
//     * Test of fromEuler method, of class Matrices.
//     */
//    @Test
//    public void testFromEuler() {
//        
//        // http://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
//        
//        /*octave:1> rotation_matrix_demo
//        Picking random Euler angles (radians)
//        x = -2.6337
//        y = -0.47158
//        z = -1.2795
//
//        Rotation matrix is:
//        R =
//
//         0.25581  -0.77351   0.57986
//         -0.85333  -0.46255  -0.24057
//         0.45429  -0.43327  -0.77839
//
//        Decomposing R
//        x2 = -2.6337
//        y2 = -0.47158
//        z2 = -1.2795
//
//        err = 0
//        Results are correct!*/
//        
//        // euler angles in radians (heading/yaw , elevation/pitch , bank/roll)
//        double roll = -1.2795;
//        double pitch = -2.6337;
//        double yaw = -0.47158;
//        double[][] rx = rotateXMatrix3D(pitch);
//        double[][] ry = rotateYMatrix3D(yaw);
//        double[][] rz = rotateZMatrix3D(roll);
//        
//        double[] euler = new double[]{ yaw, pitch, roll };
//        double[][] buffer = new double[3][3];
//        double[][] expResult = Matrices.localMultiply(rz, Matrices.localMultiply(rx,ry));
//        double[][] result = Matrices.fromEuler(euler, buffer);
//        Assert.assertArrayEquals(expResult, result);
        
//        System.out.println("fromEuler");
//        double[] euler = null;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.fromEuler(euler, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }

//    /**
//     * Test of toEuler method, of class Matrices.
//     */
//    @Test
//    public void testToEuler() {
//        System.out.println("toEuler");
//        double[][] mat = null;
//        double[] buffer = null;
//        double[] expResult = null;
//        double[] result = Matrices.toEuler(mat, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of lookAt method, of class Matrices.
//     */
//    @Test
//    public void testLookAt_4args() {
//        System.out.println("lookAt");
//        Tuple eye = null;
//        Tuple center = null;
//        Tuple up = null;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.lookAt(eye, center, up, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of lookAt method, of class Matrices.
//     */
//    @Test
//    public void testLookAt_3args() {
//        System.out.println("lookAt");
//        Tuple target = null;
//        Tuple up = null;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.lookAt(target, up, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of ortho2 method, of class Matrices.
//     */
//    @Test
//    public void testOrtho2() {
//        System.out.println("ortho2");
//        double fovy = 0.0;
//        double aspect = 0.0;
//        double zNear = 0.0;
//        double zFar = 0.0;
//        double[][] Result = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.ortho2(fovy, aspect, zNear, zFar, Result);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of orthogonal method, of class Matrices.
//     */
//    @Test
//    public void testOrthogonal() {
//        System.out.println("orthogonal");
//        double left = 0.0;
//        double right = 0.0;
//        double bottom = 0.0;
//        double top = 0.0;
//        double near = 0.0;
//        double far = 0.0;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.orthogonal(left, right, bottom, top, near, far, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of perspective method, of class Matrices.
//     */
//    @Test
//    public void testPerspective() {
//        System.out.println("perspective");
//        double fov = 0.0;
//        double width = 0.0;
//        double height = 0.0;
//        double near = 0.0;
//        double far = 0.0;
//        double[][] buffer = null;
//        boolean rightHand = false;
//        double[][] expResult = null;
//        double[][] result = Matrices.perspective(fov, width, height, near, far, buffer, rightHand);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of projection method, of class Matrices.
//     */
//    @Test
//    public void testProjection() {
//        System.out.println("projection");
//        double degrees = 0.0;
//        double aspectRatio = 0.0;
//        double near = 0.0;
//        double far = 0.0;
//        double[][] buffer = null;
//        double[][] expResult = null;
//        double[][] result = Matrices.projection(degrees, aspectRatio, near, far, buffer);
//        Assert.assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
    
    @Test
    public void testHadamard() {
        
        final double[][] first = new double[][]{ 
            {1., 3., 2.}, 
            {1., 0., 0.},
            {1., 2., 2.}};
        final double[][] second = new double[][]{ 
            {0., 0., 2.}, 
            {7., 5., 0.}, 
            {2., 1., 1.}};
        
        final double[][] result = Matrices.hadamard(first, second);
        
        final double[][] expected = new double[][]{
            {0., 0., 4.},
            {7., 0. , 0.},
            {2., 2., 2.}};
        
        Assert.assertArrayEquals(expected, result);
    }
    
    @Test
    public void testKronecker() {
        
        final double[][] first = new double[][]{ 
                {1., 2.}, 
                {3., 1.}};
        final double[][] second = new double[][]{
                {0., 3.}, 
                {2., 1.}};
        
        final double[][] result = Matrices.kronecker(first, second);
        
        final double[][] expected = new double[][]{ 
            {0., 3., 0., 6.},
            {2., 1., 4., 2.},
            {0., 9., 0., 3.},
            {6., 3., 2., 1.}};
        
        Assert.assertArrayEquals(expected, result);
    }
    
}
