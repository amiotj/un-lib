

package science.unlicense.impl.math.transform;

import science.unlicense.impl.math.transform.NodeTransform;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix2x2;

/**
 *
 * @author Johann Sorel
 */
public class NodeTransformTest {
    
    private static final double DELTA = 0.00000001;
    
    @Test
    public void transformIdentityDouble(){
        NodeTransform trs = new NodeTransform(2);
        
        final double[] in = {0,0, 1,0, 1,1};
        double[] out = new double[8];
        trs.transform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0, 0,0, 1,0, 1,1}, out, DELTA);
        
        out = new double[8];
        trs.inverseTransform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0, 0,0, 1,0, 1,1}, out, DELTA);
        
    }
    
    @Test
    public void transformDouble(){
        NodeTransform trs = new NodeTransform(2);
        trs.getTranslation().setXY(1, 2);
        trs.getScale().setXY(3, 5);
        
        final double[] in = {0,0, 1,0, 1,1};
        double[] out = new double[8];
        trs.transform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0, 1,2, 4,2, 4,7}, out, DELTA);
        
        out = new double[8];
        trs.inverseTransform(in, 0, out, 2, 3);
        Assert.assertArrayEquals(new double[]{0,0,-1.0/3.0, -2.0/5.0, 0, -2.0/5.0, 0, -0.2}, out, DELTA);
        
    }

    @Test
    public void rotation(){
        NodeTransform trs = new NodeTransform(2);

        Matrix2x2 rotation = Matrix2x2.fromAngle(60);
        trs.getRotation().set(rotation);
        trs.notifyChanged();

        System.out.println(trs.asMatrix());

        Assert.assertEquals(rotation,trs.getRotation());


    }
    
}
