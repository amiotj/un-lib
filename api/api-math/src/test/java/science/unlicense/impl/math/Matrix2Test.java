
package science.unlicense.impl.math;

import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Matrix2x2;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Maths;

/**
 * Test Matrix2 class.
 *
 * @author Johann Sorel
 */
public class Matrix2Test extends AbstractMatrixTest{

    private static final double DELTA = 0.0000001;

    @Override
    protected DefaultMatrix createMatrix(int nbRow, int nbCol) {
        return new Matrix2x2();
    }

    @Test
    public void fromAngleTest() {
        Assert.assertArrayEquals(new double[]{ 0,-1, 1, 0}, Matrix2x2.fromAngle(Maths.HALF_PI).toRowPackedCopy(), DELTA);
        Assert.assertArrayEquals(new double[]{-1, 0, 0,-1}, Matrix2x2.fromAngle(Maths.PI).toRowPackedCopy(), DELTA);
        Assert.assertArrayEquals(new double[]{ 0, 1,-1, 0}, Matrix2x2.fromAngle(Maths.PI+Maths.HALF_PI).toRowPackedCopy(), DELTA);
        Assert.assertArrayEquals(new double[]{ 1, 0, 0, 1}, Matrix2x2.fromAngle(Maths.TWO_PI).toRowPackedCopy(), DELTA);
    }

}
