
package science.unlicense.impl.math;

import science.unlicense.impl.math.LeastSquare;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Xavier PHILIPPEAU
 * @author Johann Sorel (UN project adaptation)
 */
public class LeastSquareTest {
    
    private static final double DELTA = 0.0000001;
    
    @Test
    public void ellipseTest(){

        // construction des points d'une ellipse
        final Sequence data = new ArraySequence();
        for (int i = 0; i < 100; i++) {
            double t = i / 100.0;
            double a = t * 2 * Maths.PI;

            // ellipse de demi-axe 17 x 10, centrée en (0,0)
            double xe = 17 * Math.cos(a);
            double ye = 10 * Math.sin(a);

            // + une rotation de 45 degrés
            double r = Math.toRadians(45);
            double xr = xe * Math.cos(r) + ye * Math.sin(r);
            double yr = -xe * Math.sin(r) + ye * Math.cos(r);

            // + une translation de (15,8)
            double x = xr + 15;
            double y = yr + 8;

            data.add(new double[]{x, y});
        }

        double[] E = LeastSquare.bestEllipsoid(data);

        Assert.assertEquals(15d, E[0], DELTA);
        Assert.assertEquals(8d, E[1], DELTA);
        Assert.assertEquals(45d, Math.toDegrees(E[2]), DELTA);
        Assert.assertEquals(17d, E[3], DELTA);
        Assert.assertEquals(10d, E[4], DELTA);

        //System.out.println("centre: " + E[0] + "," + E[1]);
        //System.out.println("angle: " + Math.toDegrees(E[2]) + " (degrés)");
        //System.out.println("demi-axe: " + E[3] + " x " + E[4]);
        // affiche:
        // centre: 14.999999999999954,8.000000000000052
        // angle: 44.99999999999916 (degrés)
        // demi-axe: 17.00000000000003 x 10.000000000000034
    }
    
}
