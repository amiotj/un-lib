
package science.unlicense.impl.math;

import science.unlicense.impl.math.DefaultTuple;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;

/**
 * @author Bertrand COTE
 */
public class DefaultTupleTest {
    
    private static final double[][] tupleDataTest = new double[][] {
        {  0. },
        {  5. },
        {  0.,  0. },
        {  2.,  4. },
        {  0.,  0.,  0. },
        { -5.,  8., -2. },
        {  0.,  0.,  0.,  0. },
        {  3., -9.,  4., -1. },
        {},
    };
    
    private static final boolean[][] tupleIsTest = new boolean[][] {
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  true,  true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
        {  false, true }, // { isZero(), isValid() }
    };
    
    private static final DefaultTuple[] tupleTest;
    static {
        tupleTest = new DefaultTuple[tupleDataTest.length];
        for( int i=0; i<tupleDataTest.length; i++ ) {
            tupleTest[i] = new DefaultTuple( tupleDataTest[i] );
        }
    }
    
    /**
     * Test constructors
     */
    public DefaultTupleTest() {
        DefaultTuple instance;
        
        for( int i=0; i<tupleDataTest.length; i++ ) {
            
            // public DefaultTuple(int size)
            instance = new DefaultTuple( tupleDataTest[i].length );
            Assert.assertEquals(tupleDataTest[i].length, instance.getValues().length);
            
            if( tupleDataTest[i].length >=1 ) {
                // public DefaultTuple(double x)
                instance = new DefaultTuple( tupleDataTest[i][0] );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 1 ),
                        instance.getValues()));
            }
            if( tupleDataTest[i].length >=2 ) {
                // public DefaultTuple(double x, double y)
                instance = new DefaultTuple( tupleDataTest[i][0], tupleDataTest[i][1] );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 2 ),
                        instance.getValues()));
            }
            if( tupleDataTest[i].length >=3 ) {
                // public DefaultTuple(double x, double y, double z)
                instance = new DefaultTuple( tupleDataTest[i][0], tupleDataTest[i][1], tupleDataTest[i][2] );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 3 ),
                        instance.getValues()));
            }
            if( tupleDataTest[i].length >=4 ) {
                // public DefaultTuple(double x, double y, double z, double w)
                instance = new DefaultTuple( tupleDataTest[i][0], tupleDataTest[i][1], tupleDataTest[i][2], tupleDataTest[i][3] );
                Assert.assertTrue( Arrays.equals(
                        Arrays.copy( tupleDataTest[i], 0, 4 ),
                        instance.getValues()));
            }
            
            // public DefaultTuple(final double[] values)
            instance = new DefaultTuple( tupleDataTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.getValues()));
            
            // public DefaultTuple(final Tuple v)
            instance = new DefaultTuple( tupleTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.getValues()));
        }
    }

    /**
     * Test of getValues method, of class DefaultTuple.
     */
    @Test
    public void testGetValues() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            double[] result = tupleTest[i].getValues();
            double[] expResult = tupleDataTest[i];
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of getSize method, of class DefaultTuple.
     */
    @Test
    public void testGetSize() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            int result = tupleTest[i].getSize();
            int expResult = tupleDataTest[i].length;
            Assert.assertEquals( expResult, result);
        }
    }

    /**
     * Test of getX method, of class DefaultTuple.
     */
    @Test
    public void testGetX() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            boolean outBounds = false;
            double result = Double.POSITIVE_INFINITY;
            try {
                result = tupleTest[i].getX();
            } catch ( IndexOutOfBoundsException ioobe ) {
                outBounds = true;
            }
            if( !outBounds ) {
                double expResult = tupleDataTest[i][0];
                Assert.assertEquals( expResult, result, 0.);
            } else {
                Assert.assertTrue( tupleDataTest[i].length<1);
            }
        }
    }

    /**
     * Test of getY method, of class DefaultTuple.
     */
    @Test
    public void testGetY() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            boolean outBounds = false;
            double result = Double.POSITIVE_INFINITY;
            try {
                result = tupleTest[i].getY();
            } catch ( IndexOutOfBoundsException ioobe ) {
                outBounds = true;
            }
            if( !outBounds ) {
                double expResult = tupleDataTest[i][1];
                Assert.assertEquals( expResult, result, 0.);
            } else {
                Assert.assertTrue( tupleDataTest[i].length<2);
            }
        }
    }

    /**
     * Test of getZ method, of class DefaultTuple.
     */
    @Test
    public void testGetZ() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            boolean outBounds = false;
            double result = Double.POSITIVE_INFINITY;
            try {
                result = tupleTest[i].getZ();
            } catch ( IndexOutOfBoundsException ioobe ) {
                outBounds = true;
            }
            if( !outBounds ) {
                double expResult = tupleDataTest[i][2];
                Assert.assertEquals( expResult, result, 0.);
            } else {
                Assert.assertTrue( tupleDataTest[i].length<3);
            }
        }
    }

    /**
     * Test of getW method, of class DefaultTuple.
     */
    @Test
    public void testGetW() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            boolean outBounds = false;
            double result = Double.POSITIVE_INFINITY;
            try {
                result = tupleTest[i].getW();
            } catch ( IndexOutOfBoundsException ioobe ) {
                outBounds = true;
            }
            if( !outBounds ) {
                double expResult = tupleDataTest[i][3];
                Assert.assertEquals( expResult, result, 0.);
            } else {
                Assert.assertTrue( tupleDataTest[i].length<4);
            }
        }
    }

    /**
     * Test of setX method, of class DefaultTuple.
     */
    @Test
    public void testSetX() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 0) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setX(tupleDataTest[i][0]);
                Assert.assertEquals(tupleDataTest[i][0], instance.getX(), 0.);
            }
        }
    }

    /**
     * Test of setY method, of class DefaultTuple.
     */
    @Test
    public void testSetY() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 1) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setY(tupleDataTest[i][1]);
                Assert.assertEquals(tupleDataTest[i][1], instance.getY(), 0.);
            }
        }
    }

    /**
     * Test of setZ method, of class DefaultTuple.
     */
    @Test
    public void testSetZ() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 2) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setZ(tupleDataTest[i][2]);
                Assert.assertEquals(tupleDataTest[i][2], instance.getZ(), 0.);
            }
        }
    }

    /**
     * Test of setW method, of class DefaultTuple.
     */
    @Test
    public void testSetW() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 3) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setW(tupleDataTest[i][3]);
                Assert.assertEquals(tupleDataTest[i][3], instance.getW(), 0.);
            }
        }
    }

    /**
     * Test of setXY method, of class DefaultTuple.
     */
    @Test
    public void testSetXY() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 1) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setXY(tupleDataTest[i][0], tupleDataTest[i][1]);
                Assert.assertEquals(tupleDataTest[i][0], instance.getX(), 0.);
                Assert.assertEquals(tupleDataTest[i][1], instance.getY(), 0.);
            }
        }
    }

    /**
     * Test of setXYZ method, of class DefaultTuple.
     */
    @Test
    public void testSetXYZ() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 2) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setXYZ(tupleDataTest[i][0], tupleDataTest[i][1], tupleDataTest[i][2]);
                Assert.assertEquals(tupleDataTest[i][0], instance.getX(), 0.);
                Assert.assertEquals(tupleDataTest[i][1], instance.getY(), 0.);
                Assert.assertEquals(tupleDataTest[i][2], instance.getZ(), 0.);
            }
        }
    }

    /**
     * Test of setXYZW method, of class DefaultTuple.
     */
    @Test
    public void testSetXYZW() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            if (tupleDataTest[i].length > 3) {
                DefaultTuple instance = new DefaultTuple(new double[tupleDataTest[i].length]);
                instance.setXYZW(tupleDataTest[i][0], tupleDataTest[i][1], tupleDataTest[i][2], tupleDataTest[i][3]);
                Assert.assertEquals(tupleDataTest[i][0], instance.getX(), 0.);
                Assert.assertEquals(tupleDataTest[i][1], instance.getY(), 0.);
                Assert.assertEquals(tupleDataTest[i][2], instance.getZ(), 0.);
                Assert.assertEquals(tupleDataTest[i][3], instance.getW(), 0.);
            }
        }
    }

    /**
     * Test of setToZero method, of class DefaultTuple.
     */
    @Test
    public void testSetToZero() {
        
        for( int i=0; i<tupleDataTest.length; i++ ) {
            DefaultTuple instance = new DefaultTuple(tupleTest[i]);
            instance.setToZero();
            Assert.assertTrue( Arrays.equals(new double[tupleDataTest[i].length], instance.getValues()));
        }
    }

    /**
     * Test of isZero method, of class DefaultTuple.
     */
    @Test
    public void testIsZero() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            DefaultTuple instance = tupleTest[i];
            boolean result = instance.isZero();
            boolean expResult = tupleIsTest[i][0];
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of isValid method, of class DefaultTuple.
     */
    @Test
    public void testIsValid() {
        for( int i=0; i<tupleDataTest.length; i++ ) {
            DefaultTuple instance = tupleTest[i];
            boolean result = instance.isValid();
            boolean expResult = tupleIsTest[i][1];
            Assert.assertEquals(expResult, result);
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ 0., 0., 0., Double.NEGATIVE_INFINITY} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ 0., 0., Double.POSITIVE_INFINITY, 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ 0., Double.NaN, 0., 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ Double.NEGATIVE_INFINITY, 0., 0., 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ 0., 0., Double.POSITIVE_INFINITY} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ 0., Double.NaN, 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ Double.NEGATIVE_INFINITY, 0., 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ 0., Double.POSITIVE_INFINITY} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ Double.NaN, 0.} );
            Assert.assertEquals(false, instance.isValid());
        }
        {
            DefaultTuple instance = new DefaultTuple( new double[]{ Double.NEGATIVE_INFINITY } );
            Assert.assertEquals(false, instance.isValid());
        }
    }

    /**
     * Test of get method, of class DefaultTuple.
     */
    @Test
    public void testGet() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = tupleTest[i];
            for( int indice = 0; indice<tupleDataTest[i].length; indice++ ) {
                double result = instance.get(indice);
                double expResult = tupleDataTest[i][indice];
                Assert.assertEquals( expResult, result, 0.);
            }
        }
    }

    /**
     * Test of set method, of class DefaultTuple.
     */
    @Test
    public void testSet_int_double() {
        
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = new DefaultTuple( new double[tupleDataTest[i].length] );
            for( int indice = 0; indice<tupleDataTest[i].length; indice++ ) {
                instance.set(indice, tupleDataTest[i][indice]);
                double expResult = tupleDataTest[i][indice];
                double result = instance.get(indice);
                Assert.assertEquals( expResult, result, 0.);
            }
        }
    }

    /**
     * Test of set method, of class DefaultTuple.
     */
    @Test
    public void testSet_Tuple() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = new DefaultTuple( new double[tupleDataTest[i].length] );
            instance.set( tupleTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.getValues(), 0.));
        }
    }

    /**
     * Test of set method, of class DefaultTuple.
     */
    @Test
    public void testSet_doubleArr() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = new DefaultTuple( new double[tupleDataTest[i].length] );
            instance.set( tupleDataTest[i] );
            Assert.assertTrue( Arrays.equals(tupleDataTest[i], instance.getValues(), 0.));
        }
    }

    /**
     * Test of toArrayDouble method, of class DefaultTuple.
     */
    @Test
    public void testToArrayDouble() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = tupleTest[i];
            double[] result = instance.toArrayDouble();
            double[] expResult = tupleDataTest[i];
            Assert.assertTrue( Arrays.equals(expResult, result, 0.));
        }
    }

    /**
     * Test of toArrayFloat method, of class DefaultTuple.
     */
    @Test
    public void testToArrayFloat() {
        
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = tupleTest[i];
            float[] result = instance.toArrayFloat();
            float[] expResult = new float[tupleDataTest[i].length];
            for( int indice=0; indice<tupleDataTest[i].length; indice++ ) expResult[indice] = (float)tupleDataTest[i][indice];
            Assert.assertTrue( Arrays.equals(expResult, result, 0.f));
        }
    }

    /**
     * Test of copy method, of class DefaultTuple.
     */
    @Test
    public void testCopy() {
        for (int i = 0; i < tupleDataTest.length; i++) {
            DefaultTuple instance = tupleTest[i];
            double[] result = instance.copy().getValues();
            double[] expResult = tupleDataTest[i];
            instance.set( tupleDataTest[i] );
            Assert.assertTrue( Arrays.equals(expResult, result, 0.));
        }
    }

    /**
     * Test of extend method, of class DefaultTuple.
     */
    @Test
    public void testExtend() {
        
        final double value = -25.36;
        
        for( int i=0; i<tupleDataTest.length; i++ ) {
            DefaultTuple instance = tupleTest[i].extend(value);
            double[] result = instance.getValues();
            double[] expResult = Arrays.copy(tupleDataTest[i], 0, tupleDataTest[i].length+1);
            expResult[tupleDataTest[i].length] = value;
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertEquals( result.length, tupleTest[i].getValues().length+1 );
        }
    }

    /**
     * Test of toChars method, of class DefaultTuple.
     */
    @Test
    public void testToChars() {
        DefaultTuple instance = new DefaultTuple( new double[]{ -2., 9. });
        byte[] expResult = new byte[]{'[', '-', '2', '.', '0', ',', '9', '.', '0', ']'};
        byte[] result = instance.toChars().toBytes();
        Assert.assertTrue( Arrays.equals( expResult, result ) );
    }

    /**
     * Test of equals method, of class DefaultTuple.
     */
    @Test
    public void testEquals() {
        
        for( int i=0; i<tupleDataTest.length; i++ ) {
            // test for equals null
            Object obj = null;
            DefaultTuple instance = tupleTest[i];
            boolean expResult = false;
            boolean result = instance.equals(obj);
            Assert.assertEquals(expResult, result);
            
            // test for equals other type of object
            obj = new Double( -36. );
            expResult = false;
            result = instance.equals(obj);
            Assert.assertEquals(expResult, result);
            
            // test for equals DefaultTuple
            for( int j=0; j<tupleDataTest.length; j++ ) {
                DefaultTuple other = tupleTest[j];
                result = instance.equals(other);
                if( i == j ) {
                    expResult = true;
                } else {
                    expResult = false;
                }
                Assert.assertEquals(expResult, result);
            }
        }
    }

    /**
     * Test of expand method, of class DefaultTuple.
     */
    @Test
    public void testExpand() {
        /*
        The method:
            protected static double[] expand(final double[] values, double value)
        is tested throught the test of the method public DefaultTuple extend(double value)
        */
    }
    
}
