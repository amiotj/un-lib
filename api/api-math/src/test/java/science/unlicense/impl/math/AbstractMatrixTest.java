
package science.unlicense.impl.math;

import science.unlicense.impl.math.DefaultMatrix;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Matrix;

/**
 * Test to verify a matrix implementation.
 * This test compare results with the default reference Matrix implementation.
 * 
 * TODO : make a full generic test for any matrix size.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractMatrixTest {
    
    protected abstract DefaultMatrix createMatrix(int nbRow, int nbCol);
    
    @Test
    public void testInvert(){
        final DefaultMatrix reference = new DefaultMatrix(2, 2);
        reference.set(0, 0, 15);
        reference.set(0, 1, -80);
        reference.set(1, 0, 0);
        reference.set(1, 1, 1);
        
        final DefaultMatrix matrix = createMatrix(2, 2);
        matrix.set(reference);
        Matrix invert = matrix.invert();
        Assert.assertTrue(matrix != invert);
        
        Assert.assertEquals(reference.invert(), invert);
        
        
        
    }
    
    
}
