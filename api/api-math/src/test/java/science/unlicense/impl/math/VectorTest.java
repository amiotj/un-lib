package science.unlicense.impl.math;

import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Bertrand COTE
 */
public class VectorTest {
    
    private static final double EPSILON = 1e-6;
    
    // =========================================================================
    // ===== Helper functions ==================================================
    
    private static boolean isNotZero( double[] v ) {
        for( int i=0; i<v.length; i++ ) {
            if( v[i] == 0. ) return false;
        }
        return true;
    }
    
    // =========================================================================
    // ===== data for tests ====================================================
    
    private static final double[][][] vectorsDataTest = new double[][][]{
    //  {{ vector.values }, { scale factor }, { scale expResult.values }, { normalize expResult } },
        { {  0. },                {  2. }, { 0. },                     { 0. } },
        { {  5. },                {  3. }, { 15. },                    { 1. } },
        { {  0.,  0. },           { -4. }, { 0., 0. },                 { 0., 0. } },
        { {  2.,  4. },           {  5. }, { 10., 20. },               { 0.447213595499958, 0.894427190999916 } },
        { {  0.,  0.,  0. },      {  6. }, { 0.,  0.,  0. },           { 0.,  0.,  0. } },
        { { -5.,  8., -2. },      { -7. }, { 35.,  -56.,  14. },       { -0.518475847365213, 0.829561355784340, -0.207390338946085} },
        { {  0.,  0.,  0.,  0. }, { -9. }, { 0.,  0.,  0.,  0. },      { 0.,  0.,  0.,  0. } },
        { {  3., -9.,  4., -1. }, { -4. }, { -12.,  36.,  -16.,  4. }, { 0.2900209467136990, -0.8700628401410971, 0.3866945956182654, -0.0966736489045664 } },
        { {  },                   { -2. }, {  },                       {  } },
    };
    
    private static final double[][][] addSubMultDivLerpTest = new double[][][]{
    // { { vector.values }, { other.values }, { add.values }, { subtract.values }, { multiply.values }, { divide.values }, { lerp ratio }, { lerp expResult } },
        { { 3. },           { 2. },           { 5. },         { 1. },              { 6. },              { 1.5 },            {  0.5 }, { 2.5 } },
        { { 3., 5. },       { 2., 10. },      { 5., 15. },    { 1., -5. },         { 6., 50. },         { 1.5, 0.5 },       { 0.25 }, {  2.75, 6.25 } },
        { { 3., 5., 9. },   { 2., 10., -3. }, { 5., 15., 6. },{ 1., -5., 12. },    { 6., 50., -27. },   { 1.5, 0.5, -3. },  { 0.75 }, {  2.25, 8.75, 0.0 } },
    };
    
    private static final double[][][] crossTest = new double[][][]{
        // { { v1 },       { v2 },          { v1 cross v2 } },
        { { -2., 0., 0. }, { 5., 0.,  0. }, {  0.,  0.,  0.} },
        { {  0., 9., 0. }, { 0., 3.,  0. }, {  0.,  0.,  0.} },
        { {  0., 0., 7. }, { 0., 0., -8. }, {  0.,  0.,  0.} },
        { {  2., 3., 4. }, { 7., 6.,  5. }, { -9., 18., -9.} },
    };
    
    private static final double[][][] angleTest = new double[][][]{
        //{ { v1 }, { v2 }, { angle }, },
        { { 1., 0. }, { 0., 1. }, { Maths.HALF_PI }, },
        { { 0., 1. }, { 1., 0. }, { Maths.HALF_PI }, },
        { { -2., 0. }, { 0., -2. }, { Maths.HALF_PI }, },
        { { 0., -2. }, { -2., 0. }, { Maths.HALF_PI }, },
        { { -2., -2. }, { -2., 0. }, { Maths.QUATER_PI }, },
        
        { { 5.2147, -19.2415 }, { -0.36510, 3.25050 }, { 2.98878910249942 } },
        
        { { 3., 0., 0. }, { 0., 2., 0. }, { Maths.HALF_PI }, },
        { { 3., 0., 0. }, { 0., 0., -5. }, { Maths.HALF_PI }, },
        { { 0., -7., 0. }, { 0., 0., 9. }, { Maths.HALF_PI }, },
        
        { { 5.2147, -19.2415, -0.3699 }, { -0.36510, 3.25050, 1.5747 }, { 2.68695055311836 }, },
    };
    
    private static final double[][][] projectTest = new double[][][]{
        //{ { v1 }, { v2 }, { projected vector }, },
        { { 1., 0. }, { 0., 1. }, { 0., 0. }, },
        { { 0., 1. }, { 0., 1. }, { 0., 1. }, },
        { { 0., 1. }, { 0.,-1. }, { 0.,-1. }, },
        { { 0., 1. }, {0.5, 0.5}, { 0., 0.5}, },
        { { 0., 12.}, { 21, 7.3}, { 0., 7.3}, }
    };
    
    private static final double[][][] cosSinTest = new double[][][]{
        // { { vec1 }, { vec2 }, { cosine, sinus },},
        { { 2., 0. }, { 3., 3. }, { Math.sqrt(2.)/2,  Math.sqrt(2.)/2 },},
        { { 3., 3. }, { 2., 0. }, { Math.sqrt(2.)/2, -Math.sqrt(2.)/2 },},
        
        { { 2., 0., 0. }, { 3., 3., 0. }, { Math.sqrt(2.)/2, Math.sqrt(2.)/2 },},
        { { 3., 3., 0. }, { 2., 0., 0. }, { Math.sqrt(2.)/2, Math.sqrt(2.)/2 },},
        
        { { 2., 2., 0. }, { 3., 3., 3. }, { Math.sqrt(6./9.), Math.sqrt(3./9.) },},
        { { 3., 3., 3. }, { 2., 2., 0. }, { Math.sqrt(6./9.), Math.sqrt(3./9.) },},
        
        { { 2., 0., 0. }, { 3., 3., 3. }, { Math.sqrt(3./9.), Math.sqrt(6./9.) },},
        { { 3., 3., 3. }, { 2., 0., 0. }, { Math.sqrt(3./9.), Math.sqrt(6./9.) },},
        
    };
    
    // =========================================================================
    // =========================================================================
    
    /**
     * Test of length method, of class Vector.
     */
    @Test
    public void testLength() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            Vector instance = new Vector(vector);
            double expResult = 0.0;
            for(int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            expResult = Math.sqrt(expResult);
            double result = instance.length();
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of lengthSquare method, of class Vector.
     */
    @Test
    public void testLengthSquare() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            Vector instance = new Vector(vector);
            double expResult = 0.0;
            for(int k=0; k<vector.length; k++) expResult += vector[k]*vector[k];
            double result = instance.lengthSquare();
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of dot method, of class Vector.
     */
    @Test
    public void testDot() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] v1 = addSubMultDivLerpTest[i][0];
            double[] v2 = addSubMultDivLerpTest[i][0];
            Vector instance = new Vector(v1);
            Tuple other = new DefaultTuple(v2);
            double result = instance.dot(other);
            double expResult = 0.0;
            for( int k=0; k<v1.length; k++ ) expResult += v1[k]*v2[k];
            Assert.assertEquals(expResult, result, 0.0);
        }
    }

    /**
     * Test of add method, of class Vector.
     */
    @Test
    public void testAdd_Tuple() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][2];
            double[] result = instance.add(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of subtract method, of class Vector.
     */
    @Test
    public void testSubtract_Tuple() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][3];
            double[] result = instance.subtract(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of multiply method, of class Vector.
     */
    @Test
    public void testMultiply_Tuple() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][4];
            double[] result = instance.multiply(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of divide method, of class Vector.
     */
    @Test
    public void testDivide_Tuple() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][5];
            double[] result = instance.divide(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of scale method, of class Vector.
     */
    @Test
    public void testScale_double() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];
            
            Vector instance = new Vector(vector);
            double[] result = instance.scale(scale).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of cross method, of class Vector.
     */
    @Test
    public void testCross_Tuple() {
        for( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = crossTest[i][0];
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];
            
            Vector instance = new Vector(v1);
            Tuple other = new DefaultTuple(v2);
            double[] result = instance.cross(other).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of lerp method, of class Vector.
     */
    @Test
    public void testLerp_Tuple_double() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = addSubMultDivLerpTest[i][0];
            double[] end = addSubMultDivLerpTest[i][1];
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];
            
            Vector instance = new Vector(start);
            Tuple other = new DefaultTuple(end);
            
            double[] result = instance.lerp(other, ratio).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of normalize method, of class Vector.
     */
    @Test
    public void testNormalize_0args() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            if( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];
                
                Vector instance = new Vector(vector);

                double[] result = instance.normalize().getValues();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            }
        }
    }

    /**
     * Test of negate method, of class Vector.
     */
    @Test
    public void testNegate_0args() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double[] expResult = new double[vector.length];
            for( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];
            
            Vector instance = new Vector(vector);

            double[] result = instance.negate().getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of shortestAngle method, of class Vector.
     */
    @Test
    public void testShortestAngle() {
        for( int i=0; i<angleTest.length; i++ ) {
            double[] vec1 = angleTest[i][0];
            double[] vec2 = angleTest[i][1];
            double expResult = angleTest[i][2][0];
            
            Vector instance = new Vector(vec1);
            Vector second = new Vector(vec2);
            
            double result = instance.shortestAngle(second);
            Assert.assertEquals(expResult, result, EPSILON);
        }
    }

    
    /**
     * Test of project method, of class Vector.
     */
    @Test
    public void testProject() {
        for( int i=0; i<projectTest.length; i++ ) {
            double[] vec1 = projectTest[i][0];
            double[] vec2 = projectTest[i][1];
            double[] expResult = projectTest[i][2];
            
            Vector instance = new Vector(vec1);
            Vector second = new Vector(vec2);
            
            Vector result = instance.project(second);
            Assert.assertArrayEquals(expResult,result.values, EPSILON);
        }
    }
    
    /**
     * Test of add method, of class Vector.
     */
    @Test
    public void testAdd_Tuple_Vector() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][2];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            Vector buffer = null;
            
            Vector resultVector = instance.add(otherInstance, buffer);
            double[] result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            resultVector = instance.add(otherInstance, buffer);
            result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of subtract method, of class Vector.
     */
    @Test
    public void testSubtract_Tuple_Vector() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][3];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            Vector buffer = null;
            
            Vector resultVector = instance.subtract(otherInstance, buffer);
            double[] result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            resultVector = instance.subtract(otherInstance, buffer);
            result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of multiply method, of class Vector.
     */
    @Test
    public void testMultiply_Tuple_Vector() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][4];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            Vector buffer = null;
            
            Vector resultVector = instance.multiply(otherInstance, buffer);
            double[] result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            resultVector = instance.multiply(otherInstance, buffer);
            result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of divide method, of class Vector.
     */
    @Test
    public void testDivide_Tuple_Vector() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = addSubMultDivLerpTest[i][0];
            double[] other = addSubMultDivLerpTest[i][1];
            double[] expResult = addSubMultDivLerpTest[i][5];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            Vector buffer = null;
            Vector resultVector = instance.divide(otherInstance, buffer);
            double[] result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            resultVector = instance.divide(otherInstance, buffer);
            result = resultVector.getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of scale method, of class Vector.
     */
    @Test
    public void testScale_double_Vector() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];
            Vector instance = new Vector(vector);
            
            Vector buffer = null;
            double[] result = instance.scale(scale, buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            result = instance.scale(scale, buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of cross method, of class Vector.
     */
    @Test
    public void testCross_Tuple_Vector() {
        for( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = crossTest[i][0];
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];
            
            Vector instance = new Vector(v1);
            Tuple other = new DefaultTuple(v2);
            
            Vector buffer = null;
            double[] result = instance.cross(other, buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            result = instance.cross(other, buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of lerp method, of class Vector.
     */
    @Test
    public void testLerp_3args() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = addSubMultDivLerpTest[i][0];
            double[] end = addSubMultDivLerpTest[i][1];
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];
            
            Vector instance = new Vector(start);
            Tuple other = new DefaultTuple(end);
            
            Vector buffer = null;
            double[] result = instance.lerp(other, ratio, buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result) );
            
            buffer = new Vector(instance.getSize());
            result = instance.lerp(other, ratio, buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result) );
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of normalize method, of class Vector.
     */
    @Test
    public void testNormalize_Vector() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            if( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];
                Vector instance = new Vector(vector);
                
                Vector buffer = null;
                double[] result = instance.normalize(buffer).getValues();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
                
                buffer = new Vector(instance.getSize());
                result = instance.normalize(buffer).getValues();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
                Assert.assertTrue( Arrays.equals(expResult, buffer.getValues(), EPSILON));
            }
        }
    }

    /**
     * Test of negate method, of class Vector.
     */
    @Test
    public void testNegate_Vector() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = vectorsDataTest[i][0];
            double[] expResult = new double[vector.length];
            for( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];
            
            Vector instance = new Vector(vector);
            
            Vector buffer = null;
            double[] result = instance.negate(buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            
            buffer = new Vector(instance.getSize());
            result = instance.negate(buffer).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
            Assert.assertTrue( Arrays.equals(expResult, buffer.getValues()));
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_double_double() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            
            if( vector.length>=2 ) {
                double[] other = addSubMultDivLerpTest[i][1];
                double x = other[0];
                double y = other[1];
                Vector instance = new Vector(vector);

                double[] expResult = Arrays.copy(vector);
                expResult[0] += x;
                expResult[1] += y;
                double[] result = instance.localAdd(x, y).getValues();
                Assert.assertTrue( Arrays.equals(expResult, result));
            }
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_3args() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            
            if( vector.length>=3 ) {
                double[] other = addSubMultDivLerpTest[i][1];
                double x = other[0];
                double y = other[1];
                double z = other[2];
                Vector instance = new Vector(vector);

                double[] expResult = Arrays.copy(vector);
                expResult[0] += x;
                expResult[1] += y;
                expResult[2] += z;
                double[] result = instance.localAdd(x, y, z).getValues();
                Assert.assertTrue( Arrays.equals(expResult, result));
            }
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_4args() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            
            if( vector.length>=4 ) {
                double[] other = addSubMultDivLerpTest[i][1];
                double x = other[0];
                double y = other[1];
                double z = other[2];
                double w = other[3];
                Vector instance = new Vector(vector);

                double[] expResult = Arrays.copy(vector);
                expResult[0] += x;
                expResult[1] += y;
                expResult[2] += z;
                expResult[3] += w;
                double[] result = instance.localAdd(x, y, z, w).getValues();
                Assert.assertTrue( Arrays.equals(expResult, result));
            }
        }
    }

    /**
     * Test of localAdd method, of class Vector.
     */
    @Test
    public void testLocalAdd_Tuple() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][2];
            double[] result = instance.localAdd(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localSubtract method, of class Vector.
     */
    @Test
    public void testLocalSubtract() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][3];
            double[] result = instance.localSubtract(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localMultiply method, of class Vector.
     */
    @Test
    public void testLocalMultiply() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][4];
            double[] result = instance.localMultiply(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localDivide method, of class Vector.
     */
    @Test
    public void testLocalDivide() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] vector = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] other = addSubMultDivLerpTest[i][1];
            
            Vector instance = new Vector(vector);
            Tuple otherInstance = new DefaultTuple(other);
            
            double[] expResult = addSubMultDivLerpTest[i][5];
            double[] result = instance.localDivide(otherInstance).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localScale method, of class Vector.
     */
    @Test
    public void testLocalScale() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = Arrays.copy(vectorsDataTest[i][0]);
            double scale =  vectorsDataTest[i][1][0];
            double[] expResult = vectorsDataTest[i][2];
            
            Vector instance = new Vector(vector);
            double[] result = instance.localScale(scale).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localCross method, of class Vector.
     */
    @Test
    public void testLocalCross() {
        for( int i=0; i<crossTest.length; i++ ) {
            double[] v1 = Arrays.copy(crossTest[i][0]);
            double[] v2 = crossTest[i][1];
            double[] expResult = crossTest[i][2];
            
            Vector instance = new Vector(v1);
            Tuple other = new DefaultTuple(v2);
            double[] result = instance.localCross(other).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localNormalize method, of class Vector.
     */
    @Test
    public void testLocalNormalize() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = Arrays.copy(vectorsDataTest[i][0]);
            if( isNotZero(vector) ){
                double[] expResult = vectorsDataTest[i][3];
                
                Vector instance = new Vector(vector);

                double[] result = instance.localNormalize().getValues();
                Assert.assertTrue( Arrays.equals(expResult, result, EPSILON));
            }
        }
    }

    /**
     * Test of localNegate method, of class Vector.
     */
    @Test
    public void testLocalNegate() {
        for( int i=0; i<vectorsDataTest.length; i++ ) {
            double[] vector = Arrays.copy(vectorsDataTest[i][0]);
            double[] expResult = new double[vector.length];
            for( int k=0; k<vector.length; k++ ) expResult[k] = -vector[k];
            
            Vector instance = new Vector(vector);

            double[] result = instance.localNegate().getValues();
            Assert.assertTrue( Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of localLerp method, of class Vector.
     */
    @Test
    public void testLocalLerp() {
        for( int i=0; i<addSubMultDivLerpTest.length; i++ ) {
            double[] start = Arrays.copy(addSubMultDivLerpTest[i][0]);
            double[] end = Arrays.copy(addSubMultDivLerpTest[i][1]);
            double ratio = addSubMultDivLerpTest[i][6][0];
            double[] expResult = addSubMultDivLerpTest[i][7];
            
            Vector instance = new Vector(start);
            Tuple other = new DefaultTuple(end);
            
            double[] result = instance.localLerp(other, ratio).getValues();
            Assert.assertTrue( Arrays.equals(expResult, result) );
        }
    }

    /**
     * Test of copy method, of class Vector.
     */
    @Test
    public void testCopy() {
        for ( int i=0; i<vectorsDataTest.length; i++) {
            double[] vec = vectorsDataTest[i][0];
            Vector instance = new Vector( Arrays.copy(vec) );
            double[] expResult = vec;
            double[] result = instance.copy().getValues();
            Assert.assertTrue(Arrays.equals(expResult, result));
        }
    }

    /**
     * Test of extend method, of class Vector.
     */
    @Test
    public void testExtend() {
        for ( int i=0; i<vectorsDataTest.length; i++) {
            double[] vec = vectorsDataTest[i][0];
            if( vec.length>1 ) {
                Vector instance = new Vector( Arrays.copy(vec, 0, vec.length-1 ) );
                double[] expResult = vec;
                double value = vec[vec.length-1];
                double[] result = instance.extend(value).getValues();
                Assert.assertTrue(Arrays.equals(expResult, result));
            }
        }
    }
    
}
