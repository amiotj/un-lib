
package science.unlicense.impl.math;

import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Quaternion;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class QuaternionTest {

    public static final double DELTA = 0.00000001;
    
    @Test
    public void testToMatrix3(){

        //test identity
        Quaternion qt = new Quaternion();
        Matrix3x3 result = qt.toMatrix3();
        Assert.assertEquals(new Matrix3x3( 1,  0,  0,
                             0,  1,  0,
                             0,  0,  1),
                result);

        
        qt = new Quaternion(1, 0, 0, 0);
        result = qt.toMatrix3();
        Assert.assertEquals(new Matrix3x3( 1,  0,  0,
                             0, -1,  0,
                             0,  0, -1),
                result);

        qt = new Quaternion(0, 1, 0, 0);
        result = qt.toMatrix3();
        Assert.assertEquals(new Matrix3x3(-1,  0,  0,
                             0,  1,  0,
                             0,  0, -1),
                result);

        qt = new Quaternion(0, 0, 1, 0);
        result = qt.toMatrix3();
        Assert.assertEquals(new Matrix3x3(-1,  0,  0,
                             0, -1,  0,
                             0,  0,  1),
                result);
    }

    @Test
    public void testFromMatrix(){
        final double angle = 12.37;

        final Matrix4x4 m = new Matrix4x4(new double[][]{
            {1,    0,                  0,                  0},
            {0,    Math.cos(angle),    -Math.sin(angle),   0},
            {0,    Math.sin(angle),    Math.cos(angle),    0},
            {0,    0,                  0,                  1}
            });

        Quaternion q = new Quaternion();
        q.fromMatrix(m);

        assertAEquals(m.toArrayDouble(),q.toMatrix4().toArrayDouble(),0.00000001);

    }

    private static void assertAEquals(double[] expected, double[] result, double tolerance){
        Assert.assertEquals(expected.length, result.length);
        for(int i=0;i<expected.length;i++){
            Assert.assertEquals(expected[i], result[i], tolerance);
        }
    }

    @Test
    public void testFromEuler(){

        Vector euler = new Vector(0, 0, 0);
        Quaternion q = Quaternion.createFromEuler(euler);
        Assert.assertArrayEquals(new double[]{0, 0, 0, 1}, q.values, DELTA);
        
        euler = new Vector(Maths.PI, 0, 0);
        q = Quaternion.createFromEuler(euler);
        Assert.assertArrayEquals(new double[]{0, 0, 1, 0}, q.values, DELTA);
        
        euler = new Vector(0, Maths.HALF_PI, 0);
        q = Quaternion.createFromEuler(euler);
        Assert.assertArrayEquals(new double[]{0, Math.cos(Maths.QUATER_PI), 0, Math.cos(Maths.QUATER_PI)}, q.values, DELTA);
        euler = q.toEuler();
        Assert.assertArrayEquals(new double[]{0, Maths.HALF_PI, 0}, euler.values, DELTA);
        
        
        euler = new Vector(0, 0, Maths.PI);
        q = Quaternion.createFromEuler(euler);
        Assert.assertArrayEquals(new double[]{1, 0, 0, 0}, q.values, DELTA);
        
    }
    
    @Test
    public void testToEuler(){
        
        Quaternion q = new Quaternion(0, 0, 0, 1);
        Vector euler = q.toEuler();
        Assert.assertArrayEquals(new double[]{0, 0, 0}, euler.values, DELTA);
                
        q = new Quaternion(0, 0, 1, 0);
        euler = q.toEuler();
        Assert.assertArrayEquals(new double[]{Maths.PI, 0, 0}, euler.values, DELTA);
        
//        q = new Quaternion(0, Math.cos(Angles.QPI), 0, Math.cos(Angles.QPI));
//        euler = q.toEuler();
//        Assert.assertArrayEquals(new double[]{0, Angles.HPI, 0}, euler.values, DELTA);
        
        q = new Quaternion(1, 0, 0, 0);
        euler = q.toEuler();
        Assert.assertArrayEquals(new double[]{0, 0, Maths.PI}, euler.values, DELTA);
        
        
    }


}
