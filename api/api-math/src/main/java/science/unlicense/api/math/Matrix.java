
package science.unlicense.api.math;

import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.malgebra.CholeskyDecomposition;
import science.unlicense.impl.math.malgebra.EigenvalueDecomposition;
import science.unlicense.impl.math.malgebra.LUDecomposition;
import science.unlicense.impl.math.malgebra.QRDecomposition;
import science.unlicense.impl.math.malgebra.SingularValueDecomposition;

/**
 *
 * @author Johann Sorel
 */
public interface Matrix extends Transform {

    int getNbRow();

    int getNbCol();

    ////////////////////////////////////////////////////////////////////////////
    // get/set matrix values ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    double[][] getValuesCopy();

    double[] getRow(int row);

    double[] getColumn(int col);

    Vector getColumnTuple(int col);

    double[] getLastColumn();

    Vector getLastColumnTuple();

    /**
     * Get a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param j0 Initial column index
     * @param j1 Final column index
     * @return A(i0:i1,j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    MatrixRW getMatrix(int i0, int i1, int j0, int j1);

    /**
     * Get a submatrix.
     *
     * @param r Array of row indices.
     * @param c Array of column indices.
     * @return A(r(:),c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    MatrixRW getMatrix(int[] r, int[] c);

    /**
     * Get a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param c Array of column indices.
     * @return A(i0:i1,c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    MatrixRW getMatrix(int i0, int i1, int[] c);

    /**
     * Get a submatrix.
     *
     * @param r Array of row indices.
     * @param j0 Initial column index
     * @param j1 Final column index
     * @return A(r(:),j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    MatrixRW getMatrix(int[] r, int j0, int j1);

    double get(int row, int col);

    /**
     * Returns true if matrix is identity.
     *
     * @return true if matrix is identity
     */
    boolean isIdentity();

    /**
     * Returns true if matrix do not contains any NaN or Infinite values.
     * 
     * @return true is matrix is finite
     */
    boolean isFinite();

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    MatrixRW add(Matrix other);

    MatrixRW subtract(Matrix other);

    MatrixRW scale(double scale);

    MatrixRW multiply(Matrix other);

    MatrixRW invert();

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    MatrixRW add(Matrix other, MatrixRW buffer);

    MatrixRW subtract(Matrix other, MatrixRW buffer);

    MatrixRW scale(double scale, MatrixRW buffer);

    MatrixRW multiply(Matrix other, MatrixRW buffer);

    /**
     * invert matrix
     */
    MatrixRW invert(MatrixRW buffer);

    MatrixRW transpose();

    double dot(Matrix other);

    ////////////////////////////////////////////////////////////////////////////
    // vector/tuple transformation /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    double[] transform(final double[] vector);

    TupleRW transform(final Tuple vector);

    TupleRW transform(final Tuple vector, TupleRW buffer);

    ////////////////////////////////////////////////////////////////////////////
    // Advanced algebra
    // Origin : JAMA http://math.nist.gov/javanumerics/jama/ placed in public domain.
    ////////////////////////////////////////////////////////////////////////////
    /**
     * One norm
     * @return maximum column sum.
     */
    double norm1();

    /**
     * Two norm
     * @return maximum singular value.
     */
    double norm2();

    /**
     * Infinity norm
     * @return maximum row sum.
     */
    double normInf();

    /**
     * Frobenius norm
     * @return sqrt of sum of squares of all elements.
     */
    double normF();

    /**
     * LU Decomposition
     * @return LUDecomposition
     * @see LUDecomposition
     */
    LUDecomposition lu();

    /**
     * QR Decomposition
     * @return QRDecomposition
     * @see QRDecomposition
     */
    QRDecomposition qr();

    /**
     * Cholesky Decomposition
     * @return CholeskyDecomposition
     * @see CholeskyDecomposition
     */
    CholeskyDecomposition chol();

    /**
     * Singular Value Decomposition
     * @return SingularValueDecomposition
     * @see SingularValueDecomposition
     */
    SingularValueDecomposition svd();

    /**
     * Eigenvalue Decomposition
     * @return EigenvalueDecomposition
     * @see EigenvalueDecomposition
     */
    EigenvalueDecomposition eig();

    /**
     * Solve A*X = B
     * @param B right hand side
     * @return solution if A is square, least squares solution otherwise
     */
    MatrixRW solve(MatrixRW B);

    /**
     * Solve X*A = B, which is also A'*X' = B'
     * @param B right hand side
     * @return solution if A is square, least squares solution otherwise.
     */
    MatrixRW solveTranspose(MatrixRW B);

    /**
     * Matrix determinant
     * @return determinant
     */
    double det();

    /**
     * Matrix rank
     * @return effective numerical rank, obtained from SVD.
     */
    int rank();

    /**
     * Matrix condition (2 norm)
     *
     * @return ratio of largest to smallest singular value.
     */
    double cond();

    /**
     * Matrix trace.
     * @return sum of the diagonal elements.
     */
    double trace();

    MatrixRW copy();

    /**
     * Make a one-dimensional column packed copy of the internal array.
     * @return Matrix elements packed in a one-dimensional array by columns.
     */
    double[] toColumnPackedCopy();

    /**
     * Make a one-dimensional row packed copy of the internal array.
     * @return Matrix elements packed in a one-dimensional array by rows.
     */
    double[] toRowPackedCopy();

    double[] toArrayDouble();

    /**
     * Matrix as 1D double array, in column order.
     *
     * @return 1D double array, in column order.
     */
    double[] toArrayDoubleColOrder();

    /**
     * Matrix as 1D float array, in row order.
     *
     * @return 1D float array, in row order.
     */
    float[] toArrayFloat();

    /**
     * Matrix as 1D float array, in column order.
     *
     * @return 1D float array, in column order.
     */
    float[] toArrayFloatColOrder();

    float[] toArrayFloatColOrder(float[] array);

    /**
     * Test if all cells in the matrix equals given value.
     * @param scalar scalar
     * @param tolerance tolerance
     * @return true if all values match
     */
    boolean allEquals(double scalar, double tolerance);

}