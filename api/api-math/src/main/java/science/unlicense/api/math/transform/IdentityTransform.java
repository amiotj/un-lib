
package science.unlicense.api.math.transform;

import science.unlicense.impl.math.AbstractAffine;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultMatrix;

/**
 * Transformation doing nothing
 * 
 * @author Johann Sorel
 */
public class IdentityTransform extends AbstractAffine {

    private final int dim;

    public IdentityTransform(int dim) {
        this.dim = dim;
    }

    @Override
    public boolean isIdentity() {
        return true;
    }

    public int getInputDimensions() {
        return dim;
    }

    public int getOutputDimensions() {
        return dim;
    }

    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        return Arrays.copy(source, sourceOffset, nbTuple*dim, dest, destOffset);
    }

    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        return Arrays.copy(source, sourceOffset, nbTuple*dim, dest, destOffset);
    }

    @Override
    public MatrixRW toMatrix() {
        return DefaultMatrix.create(dim+1, dim+1).setToIdentity();
    }

    @Override
    public double get(int row, int col) {
        return row==col ? 1 : 0;
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if(buffer==null) return toMatrix();
        buffer.setToIdentity();
        return buffer;
    }

}
