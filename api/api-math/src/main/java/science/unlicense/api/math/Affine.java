

package science.unlicense.api.math;

import science.unlicense.api.math.transform.Transform;

/**
 * 
 * @author Johann Sorel
 */
public interface Affine extends Transform {

    /**
     * Returns true if matrix is identity.
     *
     * @return true if matrix is identity
     */
    boolean isIdentity();
    
    /**
     * Get value at coordinate.
     * 
     * @param row
     * @param col
     * @return 
     */
    double get(int row, int col);

    double[] getRow(int col);

    double[] getCol(int col);

    /**
     * Create inverse affine transform.
     * 
     * @return new transform inverse
     */
    AffineRW invert();

    AffineRW invert(AffineRW buffer);

    /**
     * Create multiplied affine transform.
     *
     * @param other
     * @return new multiplied transform
     */
    AffineRW multiply(Affine other);

    /**
     * Create a square matrix of size dimentions+1
     * The last matrix line will be [0,...,1]
     *
     * @return
     */
    MatrixRW toMatrix();

    /**
     * Create a square matrix of size dimentions+1
     * The last matrix line will be [0,...,1]
     *
     * @return
     */
    MatrixRW toMatrix(MatrixRW buffer);

}
