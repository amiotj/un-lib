
package science.unlicense.api.math.transform;

import science.unlicense.api.math.Affine;
import science.unlicense.api.CObjects;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Matrix;

/**
 *
 * @author Johann Sorel
 */
public class ConcatenateTransform extends AbstractTransform {

    private final Transform trs1;
    private final Transform trs2;
    private final int inSize;
    private final int outSize;
    private final int bufferSize;

    private ConcatenateTransform(Transform trs1, Transform trs2) {
        CObjects.ensureNotNull(trs1);
        CObjects.ensureNotNull(trs2);
        if(trs1.getOutputDimensions()!= trs2.getInputDimensions()){
            throw new InvalidArgumentException("Transform 1 output size differ from transform 2 input size");
        }
        
        this.trs1 = trs1;
        this.trs2 = trs2;
        inSize = trs1.getInputDimensions();
        outSize = trs2.getOutputDimensions();
        bufferSize = trs1.getOutputDimensions();
    }
    
    public int getInputDimensions() {
        return inSize;
    }

    public int getOutputDimensions() {
        return outSize;
    }

    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        final double[] buffer = new double[bufferSize];
        for(int i=0;i<nbTuple;i++){
            trs1.transform(source, sourceOffset+i*inSize, buffer, 0, 1);
            trs2.transform(buffer, 0, dest, destOffset+i*outSize, 1);
        }
        return dest;
    }

    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        final float[] buffer = new float[bufferSize];
        for(int i=0;i<nbTuple;i++){
            trs1.transform(source, sourceOffset+i*inSize, buffer, 0, 1);
            trs2.transform(buffer, 0, dest, destOffset+i*outSize, 1);
        }
        return dest;
    }
    
    public static Transform create(Transform trs1, Transform trs2){

        if (trs1 instanceof IdentityTransform) {
            return trs2;
        } else if(trs2 instanceof IdentityTransform) {
            return trs1;
        }

        if (trs1 instanceof Affine && trs2 instanceof Affine) {
            final Affine aff1 = (Affine) trs1;
            final Affine aff2 = (Affine) trs2;
            return aff2.multiply(aff1);
        } else if(trs1 instanceof Matrix && trs2 instanceof Matrix) {
            return ((Matrix)trs2).multiply(((Matrix)trs1));
        } else {
            return new ConcatenateTransform(trs1, trs2);
        }
        
    }
    
}
