
package science.unlicense.api.math.transform;

import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.DefaultAffine;

/**
 *
 * @author Johann Sorel
 */
public class DimStackTransform extends AbstractTransform{

    private final Transform[] stack;
    private final int[] inSizes;
    private final int[] outSizes;
    private final int[] inOffset;
    private final int[] outOffset;
    private final int inSize;
    private final int outSize;
    
    private DimStackTransform(Transform[] stack) {
        this.stack = stack;
        
        inSizes = new int[stack.length];
        outSizes = new int[stack.length];
        for(int i=0;i<stack.length;i++){
            inSizes[i] = stack[i].getInputDimensions();
            outSizes[i] = stack[i].getInputDimensions();
        }
        inSize = Maths.sum(inSizes);
        outSize = Maths.sum(outSizes);
        
        inOffset = new int[stack.length];
        outOffset = new int[stack.length];
        for(int i=1;i<stack.length;i++){
            inOffset[i] = inOffset[i-1] + inSizes[i-1];
            outOffset[i] = outOffset[i-1] + outSizes[i-1];
        }
    }

    public int getInputDimensions() {
        return inSize;
    }

    public int getOutputDimensions() {
        return outSize;
    }

    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        int tinInc = sourceOffset;
        int toutInc = destOffset;
        for(int t=0;t<nbTuple;t++){
            for(int u=0;u<stack.length;u++){
                stack[u].transform(
                        source, tinInc + inOffset[u], 
                        dest,   toutInc + outOffset[u], 
                        1);
            }
            tinInc += inSize;
            toutInc += outSize;
        }
        return dest;
    }

    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        int tinInc = sourceOffset;
        int toutInc = destOffset;
        for(int t=0;t<nbTuple;t++){
            for(int u=0;u<stack.length;u++){
                stack[u].transform(
                        source, tinInc + inOffset[u], 
                        dest,   toutInc + outOffset[u], 
                        1);
            }
            tinInc += inSize;
            toutInc += outSize;
        }
        return dest;
    }
    
    public static Transform create(Transform[] stack){
        
        final Matrix[] m = new DefaultMatrix[stack.length];
        for(int i=0;i<stack.length;i++){
            if(stack[i] instanceof Affine && stack[i].getInputDimensions()==1){
                m[i] = ((Affine)stack[i]).toMatrix();
            }else{
                //can not merge transform
                return new DimStackTransform(stack);
            }
        }
        
        //merge matrices
        final MatrixRW result = DefaultMatrix.create(m.length+1, m.length+1);
        for(int i=0;i<stack.length;i++){
            result.set(i, i,        m[i].get(0, 0));
            result.set(i, m.length, m[i].get(0, 1));
        }
        result.set(m.length, m.length, 1);

        final AffineRW trs = DefaultAffine.create(m.length);
        trs.fromMatrix(result);
        return trs;
    }
    
}
