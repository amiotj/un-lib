
package science.unlicense.api.math.transform;

import science.unlicense.api.CObject;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;

/**
 * Abstract transformation, all calls fallback on N tuple transform methods.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractTransform extends CObject implements Transform{

    public double[] transform(double[] source, double[] dest) {
        if(dest == null){
            dest = new double[getOutputDimensions()];
        }
        transform(source, 0, dest, 0, 1);
        return dest;
    }

    public float[] transform(float[] source, float[] dest) {
        if(dest == null){
            dest = new float[getOutputDimensions()];
        }
        transform(source, 0, dest, 0, 1);
        return dest;
    }

    public TupleRW transform(Tuple source, TupleRW dest) {
        if(dest == null){
            dest = new DefaultTuple(getOutputDimensions());
        }
        transform(source.getValues(), dest.getValues());        
        return dest;
    }

}
