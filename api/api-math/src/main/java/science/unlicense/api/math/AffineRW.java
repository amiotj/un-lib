
package science.unlicense.api.math;

/**
 *
 * @author Johann Sorel
 */
public interface AffineRW extends Affine {

    void set(final Affine toCopy);

    void set(int row, int col, double value);

    void setRow(int row, double[] values);

    void setCol(int col, double[] values);

    /**
     * Set affine values to identity .
     *
     * @return this Affine
     */
    AffineRW setToIdentity();
    
    /**
     *
     * @param m
     */
    void fromMatrix(Matrix m);

    /**
     * Inverse affine transform.
     *
     * @return new transform inverse
     */
    AffineRW localInvert();

    AffineRW localScale(double[] tuple);

    AffineRW localScale(double scale);

    void localMultiply(Affine affine);
    
}
