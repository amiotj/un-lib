
package science.unlicense.api.unit;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.math.transform.Transform;

/**
 * Unit definition.
 * Reference : http://fr.wikipedia.org/wiki/Unités_dérivées_du_système_international
 * 
 * @author Johann Sorel
 */
public abstract class Unit extends CObject {
    
    private final Chars unitName;
    private final Chars physicName;
    private final Chars symbol;
    private final Chars expression;

    public Unit(Chars unitName, Chars physicName, Chars symbol, Chars expression) {
        this.unitName = unitName;
        this.physicName = physicName;
        this.symbol = symbol;
        this.expression = expression;
    }
    
    /**
     * Unit full name.
     * example : hertz
     * @return Chars, never null
     */
    public Chars getUnitName(){
        return unitName;
    }
    
    /**
     * Unit physical name.
     * example : Frequency
     * @return Chars, never null
     */
    public Chars getPhysicName(){
        return physicName;
    }
    
    /**
     * Unit Symbol.
     * example : Hz
     * @return Chars, never null
     */
    public Chars getSymbol(){
        return symbol;
    }
    
    /**
     * Textual formula representation of the unit.
     * example : s^-1
     * @return Chars, never null
     */
    public Chars getExpression(){
        return expression;
    }
    
    /**
     * Get base unit.
     * 
     * @return Unit using physical quantity mesures.
     */
    public abstract Unit getBaseUnit();
    
    /**
     * Indicate if conversion from first unit to second is possible.
     * 
     * @param candidate
     * @return 
     */
    public boolean isCompatible(Unit candidate){
        return getBaseUnit().equals(candidate.getBaseUnit());
    }
    
    /**
     * Transformation from this unit to base unit.
     * 
     * @return Transform
     */
    public abstract Transform getToBase();
    
    /**
     * Transformation from base unit to this unit.
     * 
     * @return Transform
     */
    public abstract Transform getFromBase();
    
    public Chars toChars() {
        return unitName.concat('(').concat(symbol).concat(')');
    }

    public int getHash() {
        int hash = 7;
        hash = 61 * hash + (this.unitName != null ? this.unitName.hashCode() : 0);
        hash = 61 * hash + (this.physicName != null ? this.physicName.hashCode() : 0);
        hash = 61 * hash + (this.symbol != null ? this.symbol.hashCode() : 0);
        hash = 61 * hash + (this.expression != null ? this.expression.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Unit other = (Unit) obj;
        if (this.unitName != other.unitName && (this.unitName == null || !this.unitName.equals(other.unitName))) {
            return false;
        }
        if (this.physicName != other.physicName && (this.physicName == null || !this.physicName.equals(other.physicName))) {
            return false;
        }
        if (this.symbol != other.symbol && (this.symbol == null || !this.symbol.equals(other.symbol))) {
            return false;
        }
        return !(this.expression != other.expression && (this.expression == null || !this.expression.equals(other.expression)));
    }
 
    
    
}
