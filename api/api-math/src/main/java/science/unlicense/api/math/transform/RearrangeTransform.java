
package science.unlicense.api.math.transform;

import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.DefaultAffine;

/**
 *
 * @author Johann Sorel
 */
public class RearrangeTransform extends AbstractTransform {

    protected final int[] mapping;
    protected int nbOutDim;

    private RearrangeTransform(int[] mapping) {
        this.mapping = mapping;
        
        for(int i=0;i<mapping.length;i++){
            if(mapping[i]>=0) nbOutDim++;
        }
    }
    
    public int getInputDimensions() {
        return mapping.length;
    }

    public int getOutputDimensions() {
        return nbOutDim;
    }

    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        //rearrange values
        int srco = sourceOffset;
        int dsto = destOffset;
        for(int i=0;i<nbTuple;i++){
            for(int k=0;k<mapping.length;k++){
                if(mapping[k]>=0) dest[dsto + mapping[k]] = source[srco +k];
            }
            srco += mapping.length;
            dsto += nbOutDim;
        }
        return dest;
    }

    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        //rearrange values
        int srco = sourceOffset;
        int dsto = destOffset;
        for(int i=0;i<nbTuple;i++){
            for(int k=0;k<mapping.length;k++){
                dest[dsto + mapping[k]] = source[srco + k];
            }
            srco += mapping.length;
            dsto += nbOutDim;
        }
        return dest;
    }

    public static Transform create(int[] mapping){
        if(sameSize(mapping)){
            final MatrixRW matrix = DefaultMatrix.create(mapping.length+1, mapping.length+1);
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]>=0){
                    matrix.set(mapping[i], i, 1);
                }else{
                    matrix.set(i, i, 1);
                }
            }
            matrix.set(mapping.length, mapping.length, 1);
            AffineRW trs = DefaultAffine.create(mapping.length);
            trs.fromMatrix(matrix);
            return trs;
        }else{
            return new RearrangeTransform(mapping);
        }
    }
    
    private static boolean sameSize(int[] mapping){
        for(int i=0;i<mapping.length;i++){
            if(mapping[i]<0) return false;
        }
        return true;
    }
    
}
