package science.unlicense.api.math;

/**
 *
 * @author Johann Sorel
 */
public interface MatrixRW extends Matrix{

    ////////////////////////////////////////////////////////////////////////////
    // get/set matrix values ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    void set(final Matrix toCopy);

    void set(final double[] values);

    void set(final double[][] values);

    void set(int row, int col, double value);
    
    void setRow(int row, double[] values);
    
    void setCol(int col, double[] values);

    /**
     * Set a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param j0 Initial column index
     * @param j1 Final column index
     * @param X A(i0:i1,j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    void setMatrix(int i0, int i1, int j0, int j1, Matrix X);

    /**
     * Set a submatrix.
     *
     * @param r Array of row indices.
     * @param c Array of column indices.
     * @param X A(r(:),c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    void setMatrix(int[] r, int[] c, Matrix X);

    /**
     * Set a submatrix.
     *
     * @param r Array of row indices.
     * @param j0 Initial column index
     * @param j1 Final column index
     * @param X A(r(:),j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    void setMatrix(int[] r, int j0, int j1, Matrix X);

    /**
     * Set a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param c Array of column indices.
     * @param X A(i0:i1,c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    void setMatrix(int i0, int i1, int[] c, Matrix X);

    /**
     * Set Matrix value to identity matrix.
     * @return this matrix
     */
    MatrixRW setToIdentity();

    ////////////////////////////////////////////////////////////////////////////
    // methods local  //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * invert matrix
     */
    MatrixRW localInvert();

    MatrixRW localAdd(MatrixRW other);

    MatrixRW localSubtract(MatrixRW other);

    MatrixRW localScale(double[] tuple);

    MatrixRW localScale(double scale);

    MatrixRW localMultiply(Matrix other);

    /**
     * Translate the matrix.
     * Add given tuple values in the last matrix column.
     * 
     * @param translation
     */
    void localTranslate(Tuple translation);


    ////////////////////////////////////////////////////////////////////////////
    // various transformed outputs /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public MatrixRW copy();

}