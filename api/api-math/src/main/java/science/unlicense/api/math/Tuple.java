
package science.unlicense.api.math;

/**
 * A tuple is an ordered list of numeric values.
 *
 * @author Johann Sorel
 */
public interface Tuple {

    /**
     * Get number of dimension.
     * 
     * @return tuple dimension
     */
    int getSize();

    /**
     * Get value at ordinate.
     *
     * @param indice ordinate
     * @return ordinate value
     */
    double get(final int indice);
    
    /**
     * Convenient method to get value at ordinate 0.
     * May throw an exception if dimension 0 do not exist.
     * @return double
     */
    double getX();

    /**
     * Convenient method to get value at ordinate 1.
     * May throw an exception if dimension 1 do not exist.
     * @return double
     */
    double getY();

    /**
     * Convenient method to get value at ordinate 2.
     * May throw an exception if dimension 2 do not exist.
     * @return double
     */
    double getZ();

    /**
     * Convenient method to get value at ordinate 3.
     * May throw an exception if dimension 3 do not exist.
     * @return double
     */
    double getW();
    
    /**
     * Convenient method to get values at ordinate [0,1].
     * May throw an exception if ordinates do not exist
     * @return double
     */
    Tuple getXY();
    
    /**
     * Convenient method to get values at ordinate [0,1,2].
     * May throw an exception if ordinates do not exist
     * @return double
     */
    Tuple getXYZ();
    
    /**
     * Convenient method to get values at ordinate [0,1,2,3].
     * May throw an exception if ordinates do not exist
     * @return double
     */
    Tuple getXYZW();

    /**
     * @return true if all values are set to zero.
     */
    boolean isZero();
    
    /**
     * @return true if all values are set to given value.
     */
    boolean isAll(double value);
    
    /**
     * Check that all values are not NaN or Infinites.
     * @return true if valid
     */
    boolean isValid();
    
    /**
     * Get the back array used by this tuple.
     * This is a direct access, modify with care.
     * 
     * @return back array
     */
    double[] getValues();

    /**
     * Get a copy of the tuple values.
     * 
     * @return double array
     */
    double[] toArrayDouble();

    /**
     * Copies and returns the array values.
     * 
     * @param buffer a buffer object, not null
     * @return a copy of the array values.
     */
    double[] toArrayDouble(double[] buffer);
    
    /**
     * Get a copy of the tuple values as floats.
     * 
     * @return float array
     */
    float[] toArrayFloat();

    /**
     * Returns a copy of the array values casted to float.
     * 
     * @param buffer a buffer object, not null
     * @return a copy of the array values casted to float.
     */
    float[] toArrayFloat(float[] buffer);
    
    /**
     * Copy this Tuple.
     * 
     * @return new tuple copy
     */
    Tuple copy();

    /**
     * Test equality with a tolerance margin.
     *
     * @param obj
     * @param tolerance
     * @return true if tuple are equal
     */
    boolean equals(Tuple obj, double tolerance);
    
}
