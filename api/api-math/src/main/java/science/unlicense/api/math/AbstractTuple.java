
package science.unlicense.api.math;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.math.Vector;

/**
 * Abstract tuple.
 *
 * Override 'set(indice,value)' method to implement a writable tuple.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTuple implements TupleRW {

    protected final int size;

    public AbstractTuple(int size) {
        this.size = size;
    }

    @Override
    public void set(int indice, double value) {
        throw new UnimplementedException("Not supported.");
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public double getX() {
        return get(0);
    }

    @Override
    public double getY() {
        return get(1);
    }

    @Override
    public double getZ() {
        return get(2);
    }

    @Override
    public double getW() {
        return get(3);
    }

    @Override
    public Tuple getXY() {
        return new Vector(get(0), get(1));
    }

    @Override
    public Tuple getXYZ() {
        return new Vector(get(0), get(1), get(2));
    }

    @Override
    public Tuple getXYZW() {
        return new Vector(get(0), get(1), get(2), get(3));
    }

    @Override
    public void setX(double x) {
        set(0, x);
    }

    @Override
    public void setY(double y) {
        set(1, y);
    }

    @Override
    public void setZ(double z) {
        set(2, z);
    }

    @Override
    public void setW(double w) {
        set(3, w);
    }

    @Override
    public void setXY(double x, double y) {
        set(0, x);
        set(1, y);
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        set(0, x);
        set(1, y);
        set(2, z);
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        set(0, x);
        set(1, y);
        set(2, z);
        set(3, w);
    }

    @Override
    public void setAll(double v) {
        for(int i=0;i<size;i++){
            set(i, 0);
        }
    }

    @Override
    public void setToZero() {
        setAll(0);
    }

    @Override
    public boolean isZero() {
        return isAll(0);
    }

    @Override
    public boolean isAll(double value) {
        for(int i=0;i<size;i++){
            if(get(i)!=value) return false;
        }
        return true;
    }

    @Override
    public void setToNaN() {
        setAll(Double.NaN);
    }

    @Override
    public boolean isValid() {
        for(int i=0;i<size;i++){
            double v = get(i);
            if(Double.isInfinite(v) || Double.isNaN(v)){
                return false;
            }
        }
        return true;
    }

    @Override
    public double[] getValues() {
        //TODO remove this method from interface
        throw new UnimplementedException("Not supported.");
    }

    @Override
    public void set(Tuple toCopy) {
        set(toCopy.toArrayDouble());
    }

    @Override
    public void set(double[] values) {
        for(int i=0,n=Maths.min(size, values.length);i<n;i++){
            set(i, values[i]);
        }
    }

    @Override
    public void set(float[] values) {
        for(int i=0,n=Maths.min(size, values.length);i<n;i++){
            set(i, values[i]);
        }
    }

    @Override
    public double[] toArrayDouble() {
        return toArrayDouble(null);
    }

    @Override
    public double[] toArrayDouble(double[] buffer) {
        if(buffer==null) buffer = new double[size];
        for(int i=0;i<size;i++){
            buffer[i] = get(i);
        }
        return buffer;
    }

    @Override
    public float[] toArrayFloat() {
        return toArrayFloat(null);
    }

    @Override
    public float[] toArrayFloat(float[] buffer) {
        if(buffer==null) buffer = new float[size];
        for(int i=0;i<size;i++){
            buffer[i] = (float) get(i);
        }
        return buffer;
    }

    @Override
    public TupleRW copy() {
        return new Vector(this);
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (size != obj.getSize()) {
            return false;
        }
        return Arrays.equals(toArrayDouble(), obj.getValues(), tolerance);
    }

}
