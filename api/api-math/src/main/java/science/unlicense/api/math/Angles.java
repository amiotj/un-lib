package science.unlicense.api.math;

/**
 *
 * @author Johann Sorel
 */
public final class Angles {

    private Angles(){}

    public static double degreeToRadian(double value){
        return Maths.PI * value / 180;
    }

    public static double degreeToGrade(double value){
        return 200 * value / 180;
    }

    public static double gradeToRadian(double value){
        return Maths.PI * value / 200;
    }

    public static double gradeToDegree(double value){
        return 180 * value / 200;
    }

    public static double radianToDegree(double value){
        return 180 * value / Maths.PI;
    }

    public static double radianToGrade(double value){
        return 200 * value / Maths.PI;
    }

    /**
     * Angle made by 3 points.
     * 
     * @param cx center point X
     * @param cy center point Y
     * @param pt1x first point X
     * @param pt1y first point Y
     * @param pt2x second point X
     * @param pt2y second point Y
     * @return angle in radian
     */
    public static double angle(double cx, double cy,
                                double pt1x, double pt1y,
                                double pt2x, double pt2y) {
        final double dx1 = pt1x-cx;
        final double dy1 = pt1y-cy;
        final double dx2 = pt2x-cx;
        final double dy2 = pt2y-cy;
        final double a = Math.sqrt((dx1*dx1)+(dy1*dy1));
        final double b = Math.sqrt((dx2*dx2)+(dy2*dy2));

        if(a==0 || b==0){
            return 0;
        }

        final double cos = (dx1*dx2 + dy1*dy2) / (a*b);
        return Math.acos(cos);
    }

    /**
     * Test if given angle made by 3 points is clockwise.
     * 
     * @param cx center point X
     * @param cy center point Y
     * @param pt1x first point X
     * @param pt1y first point Y
     * @param pt2x second point X
     * @param pt2y second point Y
     * @return +1 if clockwise, -1 if counterclockwise
     */
    public static int isClockWise (double cx, double cy,
                                     double pt1x, double pt1y,
                                     double pt2x, double pt2y) {
        final double eps = 1e-6;
        double dx1 = pt1x-cx;
        double dy1 = pt1y-cy;
        double dx2 = pt2x-cx;
        double dy2 = pt2y-cy;
        if(Math.abs(dx1)<eps) dx1=0;
        if(Math.abs(dx2)<eps) dx2=0;
        if(Math.abs(dy1)<eps) dy1=0;
        if(Math.abs(dy2)<eps) dy2=0;

        if ( (dx1*dy2) > (dy1*dx2) ){
            return -1;
        }else if ( (dx1*dy2) < (dy1*dx2) ){
            return +1;
        }else if ( ((dx1*dx2) < 0.0) || ((dy1*dy2) < 0.0) ){
            return +1;
        }else if ( (dx1*dx1+dy1*dy1) < (dx2*dx2+dy2*dy2) ){
            return -1;
        }else{
            return 0;
        }
    }

    /**
     * sqrt(a^2 + b^2) without under/overflow.
     * Origin ; JAMA 
     * http://math.nist.gov/javanumerics/jama/
     * 
     * @param a first edge
     * @param b second edge
     * @return hypothenuse
     */
    public static double hypot(final double a, final double b) {
        double r;
        if (Math.abs(a) > Math.abs(b)) {
            r = b / a;
            r = Math.abs(a) * Math.sqrt(1 + r*r);
        } else if (b != 0) {
            r = a / b;
            r = Math.abs(b) * Math.sqrt(1 + r*r);
        } else {
            r = 0.0;
        }
        return r;
    }

    /**
     * Convert a -180/+180 angle in 0/360.
     * 
     * @param angle -180/+180 angle
     * @return 0/360 angle
     */
    public static double to360(double angle){
        if(angle<0){
            return 360.0 + angle;
        }else{
            return angle;
        }
    }
}
