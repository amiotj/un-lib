
package science.unlicense.api.math;

import science.unlicense.api.array.Arrays;

/**
 * Convenient methods for numbers and maths.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public final class Maths {
    
    public static final double PI = 3.14159265358979323846;
    public static final double TWO_PI = 2.*PI;
    public static final double HALF_PI = PI/2.0;
    public static final double QUATER_PI = PI/4.0;
    
    public static final double E = 2.7182818284590452353602875;
    
    private Maths(){}
    
    /**
     * Give the minimum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the minimum between v1 and v2.
     */
    public static byte min(byte v1, byte v2){
        return (v1<v2) ? v1 : v2;
    }

    /**
     * Give the minimum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the minimum between v1 and v2.
     */
    public static short min(short v1, short v2){
        return (v1<v2) ? v1 : v2;
    }

    /**
     * Give the minimum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the minimum between v1 and v2.
     */
    public static int min(int v1, int v2){
        return (v1<v2) ? v1 : v2;
    }

    /**
     * Give the minimum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the minimum between v1 and v2.
     */
    public static long min(long v1, long v2){
        return (v1<v2) ? v1 : v2;
    }

    /**
     * Give the minimum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the minimum between v1 and v2.
     */
    public static float min(float v1, float v2){
        return (v1<v2) ? v1 : v2;
    }

    /**
     * Give the minimum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the minimum between v1 and v2.
     */
    public static double min(double v1, double v2){
        return (v1<v2) ? v1 : v2;
    }
    
    /**
     * Give the minimum value in the array
     * @param a the array.
     * @return the minimum value in the array.
     */
    public static double min( double[] a) {
        double mini = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] < mini ) {
                mini = a[i];
            }
        }
        return mini;
    }
    
    /**
     * Give the minimum value in the array
     * @param a the array.
     * @return the minimum value in the array.
     */
    public static float min( float[] a) {
        float mini = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] < mini ) {
                mini = a[i];
            }
        }
        return mini;
    }
    
    /**
     * Give the minimum value in the array
     * @param a the array.
     * @return the minimum value in the array.
     */
    public static long min( long[] a) {
        long mini = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] < mini ) {
                mini = a[i];
            }
        }
        return mini;
    }
    
    /**
     * Give the minimum value in the array
     * @param a the array.
     * @return the minimum value in the array.
     */
    public static int min( int[] a) {
        int mini = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] < mini ) {
                mini = a[i];
            }
        }
        return mini;
    }
    
    /**
     * Give the minimum value in the array
     * @param a the array.
     * @return the minimum value in the array.
     */
    public static short min( short[] a) {
        short mini = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] < mini ) {
                mini = a[i];
            }
        }
        return mini;
    }
    
    /**
     * Give the minimum value in the array
     * @param a the array.
     * @return the minimum value in the array.
     */
    public static byte min( byte[] a) {
        byte mini = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] < mini ) {
                mini = a[i];
            }
        }
        return mini;
    }
        
    /**
     * Give the maximum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the maximum between v1 and v2.
     */
    public static byte max(byte v1, byte v2){
        return (v1>v2) ? v1 : v2;
    }

    /**
     * Give the maximum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the maximum between v1 and v2.
     */
    public static short max(short v1, short v2){
        return (v1>v2) ? v1 : v2;
    }

    /**
     * Give the maximum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the maximum between v1 and v2.
     */
    public static int max(int v1, int v2){
        return (v1>v2) ? v1 : v2;
    }

    /**
     * Give the maximum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the maximum between v1 and v2.
     */
    public static long max(long v1, long v2){
        return (v1>v2) ? v1 : v2;
    }

    /**
     * Give the maximum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the maximum between v1 and v2.
     */
    public static float max(float v1, float v2){
        return (v1>v2) ? v1 : v2;
    }

    /**
     * Give the maximum between v1 and v2.
     * @param v1 the first value.
     * @param v2 the second value.
     * @return the maximum between v1 and v2.
     */
    public static double max(double v1, double v2){
        return (v1>v2) ? v1 : v2;
    }
    
    /**
     * Give the maximum value in the array
     * @param a the array.
     * @return the maximum value in the array.
     */
    public static double max( double[] a) {
        double maxi = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] > maxi ) {
                maxi = a[i];
            }
        }
        return maxi;
    }
    
    /**
     * Give the maximum value in the array
     * @param a the array.
     * @return the maximum value in the array.
     */
    public static float max( float[] a) {
        float maxi = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] > maxi ) {
                maxi = a[i];
            }
        }
        return maxi;
    }
    
    /**
     * Give the maximum value in the array
     * @param a the array.
     * @return the maximum value in the array.
     */
    public static long max( long[] a) {
        long maxi = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] > maxi ) {
                maxi = a[i];
            }
        }
        return maxi;
    }
    
    /**
     * Give the maximum value in the array
     * @param a the array.
     * @return the maximum value in the array.
     */
    public static int max( int[] a) {
        int maxi = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] > maxi ) {
                maxi = a[i];
            }
        }
        return maxi;
    }
    
    /**
     * Give the maximum value in the array
     * @param a the array.
     * @return the maximum value in the array.
     */
    public static short max( short[] a) {
        short maxi = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] > maxi ) {
                maxi = a[i];
            }
        }
        return maxi;
    }
    
    /**
     * Give the maximum value in the array
     * @param a the array.
     * @return the maximum value in the array.
     */
    public static byte max( byte[] a) {
        byte maxi = a[0];
        for( int i=1; i<a.length; i++ ) {
            if( a[i] > maxi ) {
                maxi = a[i];
            }
        }
        return maxi;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static byte sum( byte[] array ) {
        byte result = 0;
        for( int i=0; i<array.length; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static short sum( short[] array ) {
        short result = 0;
        for( int i=0; i<array.length; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static int sum( int[] array ) {
        int result = 0;
        for( int i=0; i<array.length; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static long sum( long[] array ) {
        long result = 0;
        for( int i=0; i<array.length; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static float sum( float[] array ) {
        float result = 0;
        for( int i=0; i<array.length; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static double sum( double[] array ) {
        double result = 0;
        for( int i=0; i<array.length; i++ ) result += array[i];
        return result;
    }
    
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @param startIndex array start index, inclusive
     * @param endIndex array end index, exclusive
     * @return the sum of all values in the given array.
     */
    public static short sum( short[] array, int startIndex, int endIndex ) {
        short result = 0;
        for( int i=startIndex; i<endIndex; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @return the sum of all values in the given array.
     */
    public static int sum( int[] array, int startIndex, int endIndex ) {
        int result = 0;
        for( int i=startIndex; i<endIndex; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @param startIndex array start index, inclusive
     * @param endIndex array end index, exclusive
     * @return the sum of all values in the given array.
     */
    public static long sum( long[] array, int startIndex, int endIndex ) {
        long result = 0;
        for( int i=startIndex; i<endIndex; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @param startIndex array start index, inclusive
     * @param endIndex array end index, exclusive
     * @return the sum of all values in the given array.
     */
    public static float sum( float[] array, int startIndex, int endIndex ) {
        float result = 0;
        for( int i=startIndex; i<endIndex; i++ ) result += array[i];
        return result;
    }
    
    /**
     * Computes and returns the sum of all values in the given array.
     * @param array values
     * @param startIndex array start index, inclusive
     * @param endIndex array end index, exclusive
     * @return the sum of all values in the given array.
     */
    public static double sum( double[] array, int startIndex, int endIndex ) {
        double result = 0;
        for( int i=startIndex; i<endIndex; i++ ) result += array[i];
        return result;
    }
    
    
    /**
     * Computes and returns the product of all values in the given array.
     * @param array values
     * @return the product of all values in the given array.
     */
    public static byte product( byte[] array ) {
        byte result = 1;
        for( int i=0; i<array.length; i++ ) result *= array[i];
        return result;
    }
    
    /**
     * Computes and returns the product of all values in the given array.
     * @param array values
     * @return the product of all values in the given array.
     */
    public static short product( short[] array ) {
        short result = 1;
        for( int i=0; i<array.length; i++ ) result *= array[i];
        return result;
    }
    
    /**
     * Computes and returns the product of all values in the given array.
     * @param array values
     * @return the product of all values in the given array.
     */
    public static int product( int[] array ) {
        int result = 1;
        for( int i=0; i<array.length; i++ ) result *= array[i];
        return result;
    }
    
    /**
     * Computes and returns the product of all values in the given array.
     * @param array values
     * @return the product of all values in the given array.
     */
    public static long product( long[] array ) {
        long result = 1;
        for( int i=0; i<array.length; i++ ) result *= array[i];
        return result;
    }
    
    /**
     * Computes and returns the product of all values in the given array.
     * @param array values
     * @return the product of all values in the given array.
     */
    public static float product( float[] array ) {
        float result = 1.f;
        for( int i=0; i<array.length; i++ ) result *= array[i];
        return result;
    }
    
    /**
     * Computes and returns the product of all values in the given array.
     * @param array values
     * @return the product of all values in the given array.
     */
    public static double product( double[] array ) {
        double result = 1.;
        for( int i=0; i<array.length; i++ ) result *= array[i];
        return result;
    }
    
    /**
     * Clamp returns:
     * <ul>
     * <li>min if the value is lower than min</li>
     * <li>value if the value is between min and max</li>
     * <li>max if the value is higher than max</li>
     * </ul>
     * @param val the value
     * @param min minimum value
     * @param max maximum value
     * @return clamped value
     */
    public static byte clamp(byte val, byte min, byte max){
        if(val<min) return min;
        if(val>max) return max;
        return val;
    }

    /**
     * Clamp returns:
     * <ul>
     * <li>min if the value is lower than min</li>
     * <li>value if the value is between min and max</li>
     * <li>max if the value is higher than max</li>
     * </ul>
     * @param val the value
     * @param min minimum value
     * @param max maximum value
     * @return clamped value
     */
    public static short clamp(short val, short min, short max){
        if(val<min) return min;
        if(val>max) return max;
        return val;
    }

    /**
     * Clamp returns:
     * <ul>
     * <li>min if the value is lower than min</li>
     * <li>value if the value is between min and max</li>
     * <li>max if the value is higher than max</li>
     * </ul>
     * @param val the value
     * @param min minimum value
     * @param max maximum value
     * @return clamped value
     */
    public static int clamp(int val, int min, int max){
        if(val<min) return min;
        if(val>max) return max;
        return val;
    }

    /**
     * Clamp returns:
     * <ul>
     * <li>min if the value is lower than min</li>
     * <li>value if the value is between min and max</li>
     * <li>max if the value is higher than max</li>
     * </ul>
     * @param val the value
     * @param min minimum value
     * @param max maximum value
     * @return clamped value
     */
    public static long clamp(long val, long min, long max){
        if(val<min) return min;
        if(val>max) return max;
        return val;
    }

    /**
     * Clamp returns:
     * <ul>
     * <li>min if the value is lower than min</li>
     * <li>value if the value is between min and max</li>
     * <li>max if the value is higher than max</li>
     * </ul>
     * @param val the value
     * @param min minimum value
     * @param max maximum value
     * @return clamped value
     */
    public static float clamp(float val, float min, float max){
        if(val<min) return min;
        if(val>max) return max;
        return val;
    }

    /**
     * Clamp returns:
     * <ul>
     * <li>min if the value is lower than min</li>
     * <li>value if the value is between min and max</li>
     * <li>max if the value is higher than max</li>
     * </ul>
     * @param val the value
     * @param min minimum value
     * @param max maximum value
     * @return clamped value
     */
    public static double clamp(double val, double min, double max){
        if(val<min) return min;
        if(val>max) return max;
        return val;
    }

    /**
     * Wrap returns the wrapped value in the given range.
     * This is used for cyclic coordinate systems like angles, color spaces or 
     * geographic coordinates.
     * 
     * Example :
     * wrap(-190,-180,+180) return +170
     * 
     * @param val the value
     * @param min lower cyclic value
     * @param max upper cyclic value
     * @return wrapped value
     */
    public static byte wrap(byte val, byte min, byte max){
        if(min<=val && val<=max) return val;
        final int range = max-min;
        if(val<min){
            return (byte) (max+((val-min) % range));
        }else{
            return (byte) (min+((val-max) % range));
        }
    }
    /**
     * Wrap returns the wrapped value in the given range.
     * This is used for cyclic coordinate systems like angles, color spaces or 
     * geographic coordinates.
     * 
     * Example :
     * wrap(-190,-180,+180) return +170
     * 
     * @param val the value
     * @param min lower cyclic value
     * @param max upper cyclic value
     * @return wrapped value
     */
    public static short wrap(short val, short min, short max){
        if(min<=val && val<=max) return val;
        final int range = max-min;
        if(val<min){
            return (short) (max+((val-min) % range));
        }else{
            return (short) (min+((val-max) % range));
        }
    }
    
    /**
     * Wrap returns the wrapped value in the given range.
     * This is used for cyclic coordinate systems like angles, color spaces or 
     * geographic coordinates.
     * 
     * Example :
     * wrap(-190,-180,+180) return +170
     * 
     * @param val the value
     * @param min lower cyclic value
     * @param max upper cyclic value
     * @return wrapped value
     */
    public static int wrap(int val, int min, int max){
        if(min<=val && val<=max) return val;
        final int range = max-min;
        if(val<min){
            return max+((val-min) % range);
        }else{
            return min+((val-max) % range);
        }
    }
    
    /**
     * Wrap returns the wrapped value in the given range.
     * This is used for cyclic coordinate systems like angles, color spaces or 
     * geographic coordinates.
     * 
     * Example :
     * wrap(-190,-180,+180) return +170
     * 
     * @param val the value
     * @param min lower cyclic value
     * @param max upper cyclic value
     * @return wrapped value
     */
    public static long wrap(long val, long min, long max){
        if(min<=val && val<=max) return val;
        final long range = max-min;
        if(val<min){
            return max+((val-min) % range);
        }else{
            return min+((val-max) % range);
        }
    }
    
    /**
     * Wrap returns the wrapped value in the given range.
     * This is used for cyclic coordinate systems like angles, color spaces or 
     * geographic coordinates.
     * 
     * Example :
     * wrap(-190,-180,+180) return +170
     * 
     * @param val the value
     * @param min lower cyclic value
     * @param max upper cyclic value
     * @return wrapped value
     */
    public static float wrap(float val, float min, float max){
        if(min<=val && val<=max) return val;
        final float range = max-min;
        if(val<min){
            return max+((val-min) % range);
        }else{
            return min+((val-max) % range);
        }
    }
    
    /**
     * Wrap returns the wrapped value in the given range.
     * This is used for cyclic coordinate systems like angles, color spaces or 
     * geographic coordinates.
     * 
     * Example :
     * wrap(-190,-180,+180) return +170
     * 
     * @param val the value
     * @param min lower cyclic value
     * @param max upper cyclic value
     * @return wrapped value
     */
    public static double wrap(double val, double min, double max){
        if(min<=val && val<=max) return val;
        final double range = max-min;
        if(val<min){
            return max+((val-min) % range);
        }else{
            return min+((val-max) % range);
        }
    }
    
    public static int round(float val){
        return (int) (val+0.5);
    }

    public static long round(double val){
        return (long) (val+0.5);
    }

    /**
     * Get fractional part of given number.
     * @param val value
     * @return fractional part
     */
    public static float fract(float val){
        return val - ((int)val);
    }

    /**
     * Get inverse fractional part of given number.
     * @param val value
     * @return inverse fractional part
     */
    public static float ifract(float val){
        return 1f - fract(val);
    }

    /**
     * Get fractional part of given number.
     * @param val value
     * @return inverse fractional part
     */
    public static double fract(double val){
        return val - ((int)val);
    }

    /**
     * Get inverse fractional part of given number.
     * @param val value
     * @return inverse fractional part
     */
    public static double ifract(double val){
        return 1d - fract(val);
    }

    /**
     * Faster version of Math.floor.
     * test is not as accurate. only making a naive floor operation correct
     * enough when exact bit number algebra is not needed.
     * @param x value
     * @return floor value
     */
    public static int fastFloor(float x) {
        final int xi = (int)x;
        return x<xi ? xi-1 : xi;
    }

    /**
     * Faster version of Math.floor.
     * test is not as accurate. only making a naive floor operation correct
     * enough when exact bit number algebra is not needed.
     * @param x value
     * @return floor value
     */
    public static int fastFloor(double x) {
        final int xi = (int)x;
        return x<xi ? xi-1 : xi;
    }
    
    /**
     * Power function for integer powers.
     * In mathematics, pow(0,0) is undetermined, but here pow(0,0) return 1.
     * (O( log n ) algorithm.)
     * @param x value
     * @param n the power ( can be inferior to 0 )
     * @return x power n
     */
    public static double pow( double x, int n ) {
        
        if( n == 0 ) return 1.;
        
        final boolean nIsNegative = n<0;
        if( nIsNegative ) n = -n;
        
        double result = 1.;
        double p = x;
        
        while( n != 0 ) {
            if( (n&1) == 1 )  result *= p;
            n = n>>1;
            p *= p;
        }
        
        if( nIsNegative ) return 1/result;
        return result;
    }
    
    /**
     * Power function for integer powers.
     * In mathematics, pow(0,0) is undetermined, but here pow(0,0) returns 1.
     * (O( log n ) algorithm.)
     * @param x value
     * @param n the power ( can be inferior to 0 )
     * @return x power n
     */
    public static float pow( float x, int n ) {
        
        if( n == 0 ) return 1.f;
        
        final boolean nIsNegative = n<0;
        if( nIsNegative ) n = -n;
        
        float result = 1.f;
        float p = x;
        
        while( n != 0 ) {
            if( (n&1) == 1 ) result *= p;
            n = n>>1;
            p *= p;
        }
        
        if( nIsNegative ) return 1/result;
        return result;
    }

    /**
     * Linear interpolation between a and b.
     * 
     * @param a start value
     * @param b end value
     * @param ratio : 0 is close to start, 1 is on end
     * @return interpolated value
     */
    public static double lerp(double a, double b, double ratio){
        return (1.0-ratio)*a + ratio*b;
    }
    
    /**
     * Linear interpolation between a and b.
     * 
     * @param a start value
     * @param b end value
     * @param ratio : 0 is close to start, 1 is on end
     * @return interpolated value
     */
    public static float lerp(float a, float b, float ratio){
        return (1f-ratio)*a + ratio*b;
    }
    
    /**
     * Generate a random value between given range.
     * 
     * @param min lower threshold
     * @param max upper threshold
     * @return random value
     */
    public static double random(double min, double max){
        return min + (max-min)*Math.random();
    }

    /**
     * Computes and returns the mean value of the given array.
     * @param array values
     * @return the mean of the given array.
     */
    public static double mean(int[] array){
        return sum(array)/array.length;
    }

    /**
     * Computes and returns the mean value of the given array.
     * @param array values
     * @return the mean of the given array.
     */
    public static float mean(float[] array){
        return sum(array)/array.length;
    }

    /**
     * Computes and returns the mean value of the given array.
     * @param array values
     * @return the mean of the given array.
     */
    public static double mean(double[] array){
        return sum(array)/array.length;
    }

    /**
     * Computes and returns the variance value of the given array.
     * @param array values
     * @return the variance of the given array.
     */
    public static double variance(int[] array){
        final double mean = mean(array);
        double val=0, t;
        for(int i=1;i<array.length;i++){
            t = mean-array[i];
            val += t*t;
        }
        return val/array.length;
    }

    /**
     * Computes and returns the variance value of the given array.
     * @param array values
     * @return the variance of the given array.
     */
    public static double variance(float[] array){
        final float mean = mean(array);
        double val=0, t;
        for(int i=1;i<array.length;i++){
            t = mean-array[i];
            val += t*t;
        }
        return val/array.length;
    }

    /**
     * Computes and returns the variance value of the given array.
     * @param array values
     * @return the variance of the given array.
     */
    public static double variance(double[] array){
        final double mean = mean(array);
        double val=0, t;
        for(int i=1;i<array.length;i++){
            t = mean-array[i];
            val += t*t;
        }
        return val/array.length;
    }

    /**
     * Computes and returns the standard deviation value of the given array.
     * @param array values
     * @return the standard deviation of the given array.
     */
    public static double standardDeviation(int[] array){
        return Math.sqrt(variance(array));
    }

    /**
     * Computes and returns the standard deviation value of the given array.
     * @param array values
     * @return the standard deviation of the given array.
     */
    public static double standardDeviation(float[] array){
        return Math.sqrt(variance(array));
    }

    /**
     * Computes and returns the standard deviation value of the given array.
     * @param array values
     * @return the standard deviation of the given array.
     */
    public static double standardDeviation(double[] array){
        return Math.sqrt(variance(array));
    }

    /**
     * Computes and returns the median value of the given array.
     * @param array values
     * @return the median value of the given array.
     */
    public static double median(int[] array){
        Arrays.sort(array);

        final int index = array.length/2;
        if(array.length%2 == 0){
            //calculate average of the 2 nearest values
            return (array[index-1]+array[index]) / 2.0;
        }else{
            return array[index];
        }
    }

    /**
     * Computes and returns the median value of the given array.
     * @param array values
     * @return the median value of the given array.
     */
    public static double median(float[] array){
        Arrays.sort(array);

        final int index = array.length/2;
        if(array.length%2 == 0){
            //calculate average of the 2 nearest values
            return (array[index-1]+array[index]) / 2.0;
        }else{
            return array[index];
        }
    }

    /**
     * Computes and returns the median value of the given array.
     * @param array values
     * @return the median value of the given array.
     */
    public static double median(double[] array){
        Arrays.sort(array);

        final int index = array.length/2;
        if(array.length%2 == 0){
            //calculate average of the 2 nearest values
            return (array[index-1]+array[index]) / 2.0;
        }else{
            return array[index];
        }
    }

    /**
     * Compute secant.
     *
     * @param angle in radians
     * @return secant
     */
    public static double sec(double angle) {
        return 1.0 / Math.cos(angle);
    }

    /**
     * Compute cotangent.
     *
     * @param angle in radians
     * @return cotangent
     */
    public static double cot(double angle) {
        return 1.0 / Math.tan(angle);
    }

    /**
     * Compute cosecant.
     *
     * @param angle in radians
     * @return cosecant
     */
    public static double csc(double angle) {
        return 1.0 / Math.sin(angle);
    }
}
