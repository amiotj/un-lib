
package science.unlicense.api.math;

import science.unlicense.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public abstract class InterpolationMethod {

    public static final int METHOD_LINEAR = 0;
    public static final int METHOD_QUADRATIC_EASE_OUT         = 1;
    public static final int METHOD_QUADRATIC_EASE_IN          = 2;
    public static final int METHOD_QUADRATIC_EASE_IN_OUT      = 3;
    public static final int METHOD_CUBIC_EASE_OUT             = 10;
    public static final int METHOD_CUBIC_EASE_IN              = 11;
    public static final int METHOD_CUBIC_EASE_IN_OUT          = 12;
    public static final int METHOD_QUARTIC_EASE_OUT           = 20;
    public static final int METHOD_QUARTIC_EASE_IN            = 21;
    public static final int METHOD_QUARTIC_EASE_IN_OUT        = 22;
    public static final int METHOD_QUINTIC_EASE_OUT           = 30;
    public static final int METHOD_QUINTIC_EASE_IN            = 31;
    public static final int METHOD_QUINTIC_EASE_IN_OUT        = 32;
    public static final int METHOD_SINUSOIDAL_EASE_OUT        = 40;
    public static final int METHOD_SINUSOIDAL_EASE_IN         = 41;
    public static final int METHOD_SINUSOIDAL_EASE_IN_OUT     = 42;
    public static final int METHOD_EXPONENTIAL_EASE_OUT       = 50;
    public static final int METHOD_EXPONENTIAL_EASE_IN        = 51;
    public static final int METHOD_EXPONENTIAL_EASE_IN_OUT    = 52;
    public static final int METHOD_CIRCULAR_EASE_OUT          = 60;
    public static final int METHOD_CIRCULAR_EASE_IN           = 61;
    public static final int METHOD_CIRCULAR_EASE_IN_OUT       = 62;


    /**
     * Interpolation between 0 and 1.
     *
     * @param t time between 0 and 1
     * @return interpolated value.
     */
    public abstract double interpolate(double t);

    /**
     * Interpolation between start and end.
     *
     * @param t time between 0 and 1
     * @param start
     * @param end
     * @return interpolated value.
     */
    public abstract double interpolate(double t, double start, double end);

    /**
     * Default interpolation methods.
     *
     * @param method
     * @return InterpolationMethod
     */
    public static InterpolationMethod create(final int method){
        return new InterpolationMethod() {
            public double interpolate(double t) {
                return interpolate(t, 0, 1);
            }
            public double interpolate(double t, double start, double end) {
                switch(method){
                    case METHOD_LINEAR :                    return InterpolationMethods.linear(t, start, end);
                    case METHOD_QUADRATIC_EASE_OUT :        return InterpolationMethods.quadraticEaseOut(t, start, end);
                    case METHOD_QUADRATIC_EASE_IN :         return InterpolationMethods.quadraticEaseIn(t, start, end);
                    case METHOD_QUADRATIC_EASE_IN_OUT :     return InterpolationMethods.quadraticEaseInOut(t, start, end);
                    case METHOD_CUBIC_EASE_OUT :            return InterpolationMethods.cubicEaseOut(t, start, end);
                    case METHOD_CUBIC_EASE_IN :             return InterpolationMethods.cubicEaseIn(t, start, end);
                    case METHOD_CUBIC_EASE_IN_OUT :         return InterpolationMethods.cubicEaseInOut(t, start, end);
                    case METHOD_QUARTIC_EASE_OUT :          return InterpolationMethods.quarticEaseOut(t, start, end);
                    case METHOD_QUARTIC_EASE_IN :           return InterpolationMethods.quarticEaseIn(t, start, end);
                    case METHOD_QUARTIC_EASE_IN_OUT :       return InterpolationMethods.quarticEaseInOut(t, start, end);
                    case METHOD_QUINTIC_EASE_OUT :          return InterpolationMethods.quinticEaseOut(t, start, end);
                    case METHOD_QUINTIC_EASE_IN :           return InterpolationMethods.quinticEaseIn(t, start, end);
                    case METHOD_QUINTIC_EASE_IN_OUT :       return InterpolationMethods.quinticEaseInOut(t, start, end);
                    case METHOD_SINUSOIDAL_EASE_OUT :       return InterpolationMethods.sinusoidalEaseOut(t, start, end);
                    case METHOD_SINUSOIDAL_EASE_IN :        return InterpolationMethods.sinusoidalEaseIn(t, start, end);
                    case METHOD_SINUSOIDAL_EASE_IN_OUT :    return InterpolationMethods.sinusoidalEaseInOut(t, start, end);
                    case METHOD_EXPONENTIAL_EASE_OUT :      return InterpolationMethods.exponentialEaseOut(t, start, end);
                    case METHOD_EXPONENTIAL_EASE_IN :       return InterpolationMethods.exponentialEaseIn(t, start, end);
                    case METHOD_EXPONENTIAL_EASE_IN_OUT :   return InterpolationMethods.exponentialEaseInOut(t, start, end);
                    case METHOD_CIRCULAR_EASE_OUT :         return InterpolationMethods.circularEaseOut(t, start, end);
                    case METHOD_CIRCULAR_EASE_IN :          return InterpolationMethods.circularEaseIn(t, start, end);
                    case METHOD_CIRCULAR_EASE_IN_OUT :      return InterpolationMethods.circularEaseInOut(t, start, end);
                    default: throw new InvalidArgumentException("Unknonw method : "+method);
                }
            }
        };
    }

}
