
package science.unlicense.api.math.transform;

/**
 *
 * @author Johann Sorel
 */
public final class Transforms {
    
    public static final Transform IDENTITY_1D = new IdentityTransform(1);
    public static final Transform IDENTITY_2D = new IdentityTransform(2);
    public static final Transform IDENTITY_3D = new IdentityTransform(3);
    public static final Transform IDENTITY_4D = new IdentityTransform(4);
    public static final Transform IDENTITY_5D = new IdentityTransform(5);
    
}
