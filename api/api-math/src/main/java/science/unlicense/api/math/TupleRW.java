
package science.unlicense.api.math;

/**
 * A tuple is an ordered list of numeric values.
 *
 * @author Johann Sorel
 */
public interface TupleRW extends Tuple {

    /**
     * Convenient method to set value at ordinate 0.
     * May throw an exception if dimension 0 do not exist.
     * @param x first ordinate value
     */
    void setX(double x);

    /**
     * Convenient method to set value at ordinate 1.
     * May throw an exception if dimension 1 do not exist.
     * @param y second ordinate value
     */
    void setY(double y);

    /**
     * Convenient method to set value at ordinate 2.
     * May throw an exception if dimension 2 do not exist.
     * @param z third ordinate value
     */
    void setZ(double z);

    /**
     * Convenient method to set value at ordinate 3.
     * May throw an exception if dimension 3 do not exist.
     * @param w fourth ordinate value
     */
    void setW(double w);

    /**
     * Convenient method to set value at ordinate 0,1.
     * May throw an exception if a dimension do not exist.
     * 
     * @param x first ordinate value
     * @param y second ordinate value
     */
    void setXY(double x, double y);

    /**
     * Convenient method to set value at ordinate 0,1,3.
     * May throw an exception if a dimension do not exist.
     * 
     * @param x first ordinate value
     * @param y second ordinate value
     * @param z third ordinate value
     */
    void setXYZ(double x, double y, double z);

    /**
     * Convenient method to set value at ordinate 0,1,2,3.
     * May throw an exception if a dimension do not exist.
     * 
     * @param x first ordinate value
     * @param y second ordinate value
     * @param z third ordinate value
     * @param w fourth ordinate value
     */
    void setXYZW(double x, double y, double z, double w);

    /**
     * Set all values to given value.
     * 
     * @param v new value for all elements.
     */
    void setAll(double v);
    
    /**
     * Reset all values to 0.
     */
    void setToZero();

    /**
     * Reset all values to NaN.
     */
    void setToNaN();
    
    /**
     * Set value at given ordinate.
     * 
     * @param indice ordinate
     * @param value new value
     */
    void set(final int indice, final double value);

    /**
     * Copy values from tuple.
     * If toCopy.values.length superior to this.values.length, then the only 
     * this.values.length values of toCopy are copied.
     * 
     * @param toCopy tuple to copy
     */
    void set(Tuple toCopy);

    /**
     * Copy values from array.
     * If values.length superior to this.values.length, then the only 
     * this.values.length values of values are copied.
     * 
     * @param values array to copy
     */
    void set(double[] values);

    /**
     * Copy values from array.
     * If values.length superior to this.values.length, then the only 
     * this.values.length values of values are copied.
     * 
     * @param values array to copy
     */
    void set(float[] values);
    
    /**
     * Copy this Tuple.
     * 
     * @return new tuple copy
     */
    TupleRW copy();

    
}
