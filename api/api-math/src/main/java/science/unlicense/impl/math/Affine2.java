
package science.unlicense.impl.math;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class Affine2 extends AbstractAffine.RW {

    private double m00;
    private double m01;
    private double m02;
    private double m10;
    private double m11;
    private double m12;

    public Affine2() {
        this.m00 = 1.0;
        this.m01 = 0.0;
        this.m02 = 0.0;
        this.m10 = 0.0;
        this.m11 = 1.0;
        this.m12 = 0.0;
    }

    public Affine2(double m00, double m01, double m02, double m10, double m11, double m12) {
        this.m00 = m00;
        this.m01 = m01;
        this.m02 = m02;
        this.m10 = m10;
        this.m11 = m11;
        this.m12 = m12;
    }

    public Affine2(Affine affine) {
        this.m00 = affine.get(0, 0);
        this.m01 = affine.get(0, 1);
        this.m02 = affine.get(0, 2);
        this.m10 = affine.get(1, 0);
        this.m11 = affine.get(1, 1);
        this.m12 = affine.get(1, 2);
    }

    public Affine2(Matrix m) {
        fromMatrix(m);
    }

    public void setM00(double m00) {
        this.m00 = m00;
    }

    public void setM01(double m01) {
        this.m01 = m01;
    }

    public void setM02(double m02) {
        this.m02 = m02;
    }

    public void setM10(double m10) {
        this.m10 = m10;
    }

    public void setM11(double m11) {
        this.m11 = m11;
    }

    public void setM12(double m12) {
        this.m12 = m12;
    }

    public double getM00() {
        return m00;
    }

    public double getM01() {
        return m01;
    }

    public double getM02() {
        return m02;
    }

    public double getM10() {
        return m10;
    }

    public double getM11() {
        return m11;
    }

    public double getM12() {
        return m12;
    }

    @Override
    public int getInputDimensions() {
        return 2;
    }

    @Override
    public int getOutputDimensions() {
        return 2;
    }

    @Override
    public double get(int row, int col) {
        switch(row){
            case 0 : switch(col){case 0:return m00;case 1:return m01;case 2:return m02;}
            case 1 : switch(col){case 0:return m10;case 1:return m11;case 2:return m12;}
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public void set(int row, int col, double value) {
        switch(row){
            case 0 : switch(col){case 0:m00=value;break;case 1:m01=value;break;case 2:m02=value;break;} return;
            case 1 : switch(col){case 0:m10=value;break;case 1:m11=value;break;case 2:m12=value;break;} return;
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }
    
    @Override
    public double[] transform(double[] source, int srcOffset, double[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=2) {
            dest[destOffset+off+0] = m00*source[srcOffset+off+0] + m01*source[srcOffset+off+1] + m02;
            dest[destOffset+off+1] = m10*source[srcOffset+off+0] + m11*source[srcOffset+off+1] + m12;
        }
        return dest;
    }

    @Override
    public float[] transform(float[] source, int srcOffset, float[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=2) {
            dest[destOffset+off+0] = (float) (m00*source[srcOffset+off+0] + m01*source[srcOffset+off+1] + m02);
            dest[destOffset+off+1] = (float) (m10*source[srcOffset+off+0] + m11*source[srcOffset+off+1] + m12);
        }
        return dest;
    }

    public void fromMatrix(Matrix m) {
        m00 = m.get(0, 0);
        m01 = m.get(0, 1);
        m02 = m.get(0, 2);
        m10 = m.get(1, 0);
        m11 = m.get(1, 1);
        m12 = m.get(1, 2);
    }

    @Override
    public MatrixRW toMatrix() {
        return new Matrix3x3(m00, m01, m02, m10, m11, m12, 0, 0, 1);
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if(buffer==null) return toMatrix();
        buffer.set(0, 0, m00);
        buffer.set(0, 1, m01);
        buffer.set(0, 2, m02);
        buffer.set(1, 0, m10);
        buffer.set(1, 1, m11);
        buffer.set(1, 2, m12);
        buffer.set(2, 0, 0);
        buffer.set(2, 1, 0);
        buffer.set(2, 2, 1);
        return buffer;
    }

    public Matrix4x4 toMatrix4() {
        return new Matrix4x4(
                m00, m01,   0, m02,
                m10, m11,   0, m12,
                  0,   0,   1,   0,
                  0,   0,   0,   1);
    }

}
