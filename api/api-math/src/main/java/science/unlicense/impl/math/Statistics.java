
package science.unlicense.impl.math;

import science.unlicense.api.math.Maths;

/**
 * Compute statistics for a large number of values.
 * 
 * @author Johann Sorel
 */
public class Statistics {
    
    private double sum;
    private double min;
    private double max;
    private int nb;
    
    //for variance
    // formula from https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance : Computing shifted data
    public double k;
    public double ex;
    public double ex2;
    
    
    public void add(double val){
        if(nb==0){
            k = val;
            min = val;
            max = val;
        }else{
            min = Maths.min(val, min);
            max = Maths.max(val, max);
        }
        sum += val;
        nb++;
        
        ex += val-k;
        ex2 += (val-k) * (val-k);
    }
    
    public double getCount(){
        return nb;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }
    
    public double getSum(){
        return sum;
    }
    
    public double getMean(){
        return sum/nb;
        //return return k+ex/nb ;
    }
    
    public double getVariance(){
        return (ex2 - (ex*ex)/nb) / (nb-1);
    }
    
    public double getStandardDeviation(){
        return Math.sqrt(getVariance());
    }
    
}
