
package science.unlicense.impl.math;

import science.unlicense.api.math.Tuple;
import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.number.Float64;

/**
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class DefaultTuple extends CObject implements TupleRW {

    protected final double[] values;

    public DefaultTuple(int size){
        values = new double[size];
    }

    public DefaultTuple(double x){
        values = new double[]{x};
    }

    public DefaultTuple(double x, double y){
        values = new double[]{x,y};
    }

    public DefaultTuple(double x, double y, double z){
        values = new double[]{x,y,z};
    }

    public DefaultTuple(double x, double y, double z, double w){
        values = new double[]{x,y,z,w};
    }

    /**
     * Use given array for values.
     * Warning : no copy made of the array.
     * 
     * @param values no copy made of the array.
     */
    public DefaultTuple(final double[] values){
        this.values = values;
    }

    /**
     * Use given array for values.
     * Warning : copy is made of the array.
     * 
     * @param values copy is made of the array.
     */
    public DefaultTuple(final float[] values){
        this.values = new double[values.length];
        for(int i=0;i<this.values.length;i++){
            this.values[i] = values[i];
        }
    }

    /**
     * Use given tuple's array for values.
     * Warning : copy is made of the array.
     * 
     * @param tuple no copy made of tuple.values.
     */
    public DefaultTuple(final Tuple tuple){
        this(tuple.getSize());
        set(tuple);
    }

    /**
     * {@inheritDoc }
     */
    public double[] getValues() {
        return values;
    }

    /**
     * {@inheritDoc }
     */
    public int getSize() {
        return values.length;
    }

    /**
     * {@inheritDoc }
     */
    public double getX(){
        return values[0];
    }

    /**
     * {@inheritDoc }
     */
    public double getY(){
        return values[1];
    }

    /**
     * {@inheritDoc }
     */
    public double getZ(){
        return values[2];
    }

    /**
     * {@inheritDoc }
     */
    public double getW(){
        return values[3];
    }

    /**
     * {@inheritDoc }
     */
    public Tuple getXY(){
        return new DefaultTuple(values[0], values[1]);
    }
    
    /**
     * {@inheritDoc }
     */
    public Tuple getXYZ(){
        return new DefaultTuple(values[0], values[1], values[2]);
    }
    
    /**
     * {@inheritDoc }
     */
    public Tuple getXYZW(){
        return new DefaultTuple(values[0], values[1], values[2], values[3]);
    }
    
    /**
     * {@inheritDoc }
     */
    public void setX(double x){
        values[0] = x;
    }

    /**
     * {@inheritDoc }
     */
    public void setY(double y){
        values[1] = y;
    }

    /**
     * {@inheritDoc }
     */
    public void setZ(double z){
        values[2] = z;
    }

    /**
     * {@inheritDoc }
     */
    public void setW(double w){
        values[3] = w;
    }

    /**
     * {@inheritDoc }
     */
    public void setXY(double x, double y) {
        values[0] = x;
        values[1] = y;
    }

    /**
     * {@inheritDoc }
     */
    public void setXYZ(double x, double y, double z) {
        values[0] = x;
        values[1] = y;
        values[2] = z;
    }

    /**
     * {@inheritDoc }
     */
    public void setXYZW(double x, double y, double z, double w) {
        values[0] = x;
        values[1] = y;
        values[2] = z;
        values[3] = w;
    }
    
    /**
     * {@inheritDoc }
     */
    public void setAll(double v){
        Arrays.fill(values, v);
    }
    
    /**
     * {@inheritDoc }
     */
    public void setToZero(){
        Arrays.fill(values, 0.);
    }

    /**
     * {@inheritDoc }
     */
    public boolean isZero(){
        if (values.length == 0)  return false;
        for (int i = 0; i < values.length; i++) {
            if (values[i] != 0.) return false;
        }
        return true;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isAll(double value){
        if (values.length == 0)  return false;
        for (int i = 0; i < values.length; i++) {
            if (values[i] != value) return false;
        }
        return true;
    }
    
    /**
     * {@inheritDoc }
     */
    public void setToNaN(){
        Arrays.fill(values, Double.NaN);
    }
    
    /**
     * {@inheritDoc }
     */
    public boolean isValid() {
        for(int i=0;i<values.length;i++){
            if(Double.isInfinite(values[i]) || Double.isNaN(values[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc }
     */
    public double get(final int index) {
        return values[index];
    }

    /**
     * {@inheritDoc }
     */
    public void set(final int indice, final double value) {
        values[indice] = value;
    }

    /**
     * {@inheritDoc }
     */
    public void set(Tuple toCopy){
        set( toCopy.getValues() );
    }

    /**
     * {@inheritDoc }
     */
    public void set(double[] values){
        Arrays.copy(values, 0, Maths.min(this.values.length, values.length), this.values, 0);
    }
    
    /**
     * {@inheritDoc }
     */
    public void set(float[] values){
        for(int i=0,n=Maths.min(this.values.length, values.length);i<n;i++){
            this.values[i] = values[i];
        }
    }

    /**
     * {@inheritDoc }
     */
    public double[] toArrayDouble(){
        return Arrays.copy(this.values);
    }
    
    /**
     * {@inheritDoc }
     */
    public double[] toArrayDouble(double[] buffer){
        return Arrays.copy(this.values,0,this.values.length,buffer,0);
    }
    
    /**
     * {@inheritDoc }
     */
    public float[] toArrayFloat(){
        return toArrayFloat(new float[this.values.length]);
    }
    
    /**
     * {@inheritDoc }
     */
    public float[] toArrayFloat(float[] buffer){
        for(int i=0;i<this.values.length;i++){
            buffer[i] = (float) values[i];
        }
        return buffer;
    }

    /**
     * {@inheritDoc }
     */
    public DefaultTuple copy() {
        return new DefaultTuple(this);
    }

    /**
     * Creates a new tuple with current tuple values and extends it with the
     * given value.
     * 
     * @param value to add at the end of tuple.
     * @return new tuple
     */
    public DefaultTuple extend(double value){
        return new DefaultTuple(expand(values, value));
    }

    /**
     * 
     * @return the Chars representation of this.
     */
    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append("[");
        for(int i=0;i<this.values.length-1;i++){
            sb.append(Float64.encode(get(i)));
            sb.append(',');
        }
        sb.append(Float64.encode(get(this.values.length-1)));
        sb.append(']');
        return sb.toChars();
    }

    /**
     * {@inheritDoc }
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Tuple)) {
            return false;
        }
        final Tuple other = (Tuple) obj;
        if (values.length != other.getSize()) {
            return false;
        }
        return Arrays.equals(values, other.getValues());
    }

    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (values.length != obj.getSize()) {
            return false;
        }
        return Arrays.equals(values, obj.getValues(), tolerance);
    }

    protected static double[] expand(final double[] values, double value){
        final double[] exp = Arrays.copy(values, 0, values.length+1);
        exp[values.length] = value;
        return exp;
    }

}
