
package science.unlicense.impl.math;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class Affine1 extends AbstractAffine.RW {

    private double m00;
    private double m01;

    public Affine1() {
        this.m00 = 1.0;
        this.m01 = 0.0;
    }

    public Affine1(double m00, double m01) {
        this.m00 = m00;
        this.m01 = m01;
    }

    public Affine1(Affine affine) {
        this.m00 = affine.get(0, 0);
        this.m01 = affine.get(0, 1);
    }

    public Affine1(Matrix m) {
        fromMatrix(m);
    }

    public void setM00(double m00) {
        this.m00 = m00;
    }

    public void setM01(double m01) {
        this.m01 = m01;
    }

    public double getM00() {
        return m00;
    }

    public double getM01() {
        return m01;
    }

    @Override
    public int getInputDimensions() {
        return 1;
    }

    @Override
    public int getOutputDimensions() {
        return 1;
    }

    @Override
    public double get(int row, int col) {
        switch(row){
            case 0 : switch(col){case 0:return m00;case 1:return m01;}
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public void set(int row, int col, double value) {
        switch(row){
            case 0 : switch(col){case 0:m00=value;break;case 1:m01=value;break;} return;
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public double[] transform(double[] source, int srcOffset, double[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off++) {
            dest[destOffset+off] = m00*source[srcOffset+off] + m01;
        }
        return dest;
    }

    @Override
    public float[] transform(float[] source, int srcOffset, float[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off++) {
            dest[destOffset+off] = (float) (m00*source[srcOffset+off] + m01);
        }
        return dest;
    }

    public void fromMatrix(Matrix m) {
        m00 = m.get(0, 0);
        m01 = m.get(0, 1);
    }

    @Override
    public MatrixRW toMatrix() {
        return new Matrix2x2(m00, m01, 0, 1);
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if(buffer==null) return toMatrix();
        buffer.set(0, 0, m00);
        buffer.set(0, 1, m01);
        buffer.set(1, 0, 0);
        buffer.set(1, 1, 1);
        return buffer;
    }

}
