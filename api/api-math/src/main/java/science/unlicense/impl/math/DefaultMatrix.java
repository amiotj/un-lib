package science.unlicense.impl.math;

import science.unlicense.api.math.Tuple;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;

/**
 *
 * @author Johann Sorel
 * @aurhor Bertrand COTE
 */
public class DefaultMatrix extends AbstractMatrix{

    protected final double[][] values;

    /**
     * Create a new matrix of given size.
     * This methods create the most efficient implementation available.
     * 
     * @param nbrow
     * @param nbcol
     * @return 
     */
    public static MatrixRW create(int nbrow, int nbcol){
        if (nbrow==nbcol) {
            switch(nbrow){
                case 2 : return new Matrix2x2();
                case 3 : return new Matrix3x3();
                case 4 : return new Matrix4x4();
            }
        }
        return new DefaultMatrix(nbrow, nbcol);
    }
    
    /**
     * Create a new matrix of given size.
     * It is recommended to use statice create method to benefit from specialized 
     * matrices implementations such as Matric2,Matric4,Matrix4.
     * 
     * @param nbrow
     * @param nbcol 
     */
    public DefaultMatrix(final int nbrow, final int nbcol) {
        super(nbrow,nbcol);
        values = new double[nbrow][nbcol];
    }

    protected DefaultMatrix(final double m00, final double m01,
                  final double m10, final double m11){
        this(new double[][]{
            {m00,m01},
            {m10,m11}
        });
    }

    protected DefaultMatrix(final double m00, final double m01, final double m02,
                  final double m10, final double m11, final double m12,
                  final double m20, final double m21, final double m22){
        this(new double[][]{
            {m00,m01,m02},
            {m10,m11,m12},
            {m20,m21,m22}
        });
    }

    protected DefaultMatrix(final double m00, final double m01, final double m02, final double m03,
                  final double m10, final double m11, final double m12, final double m13,
                  final double m20, final double m21, final double m22, final double m23,
                  final double m30, final double m31, final double m32, final double m33){
        this(new double[][]{
            {m00,m01,m02,m03},
            {m10,m11,m12,m13},
            {m20,m21,m22,m23},
            {m30,m31,m32,m33}
        });
    }

    public DefaultMatrix(final double[][] values) {
        super(values.length,values[0].length);
        this.values = values;
    }

    public DefaultMatrix(Matrix m) {
        super(m.getNbRow(),m.getNbCol());
        values = m.getValuesCopy();
    }

    ////////////////////////////////////////////////////////////////////////////
    // get/set matrix values ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public double[][] getValues() {
        return values;
    }

    public double[][] getValuesCopy() {
        final double[][] C = new double[nbRow][nbCol];
        for (int r = 0; r < nbRow; r++) {
            for (int c = 0; c < nbCol; c++) {
                C[r][c] = values[r][c];
            }
        }
        return C;
    }

    public double[] getRow(int row){
        final double[] rowval = new double[nbCol];
        for(int i=0;i<nbCol;i++) rowval[i] = values[row][i];
        return rowval;
    }

    public double[] getColumn(int col){
        final double[] colval = new double[nbRow];
        for(int i=0;i<nbRow;i++) colval[i] = values[i][col];
        return colval;
    }

    /**
     * Get a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param j0 Initial column index
     * @param j1 Final column index
     * @return A(i0:i1,j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public DefaultMatrix getMatrix(int i0, int i1, int j0, int j1) {
        final DefaultMatrix X = new DefaultMatrix(i1 - i0 + 1, j1 - j0 + 1);
        final double[][] B = X.values;
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = j0; j <= j1; j++) {
                    B[i-i0][j-j0] = values[i][j];
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    /**
     * Get a submatrix.
     *
     * @param r Array of row indices.
     * @param c Array of column indices.
     * @return A(r(:),c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public DefaultMatrix getMatrix(int[] r, int[] c) {
        final DefaultMatrix X = new DefaultMatrix(r.length, c.length);
        final double[][] B = X.values;
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = 0; j < c.length; j++) {
                    B[i][j] = values[r[i]][c[j]];
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    /**
     * Get a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param c Array of column indices.
     * @return A(i0:i1,c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public DefaultMatrix getMatrix(int i0, int i1, int[] c) {
        final DefaultMatrix X = new DefaultMatrix(i1 - i0 + 1, c.length);
        final double[][] B = X.values;
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = 0; j < c.length; j++) {
                    B[i-i0][j] = values[i][c[j]];
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    /**
     * Get a submatrix.
     *
     * @param r Array of row indices.
     * @param j0 Initial column index
     * @param j1 Final column index
     * @return A(r(:),j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public DefaultMatrix getMatrix(int[] r, int j0, int j1) {
        final DefaultMatrix X = new DefaultMatrix(r.length, j1 - j0 + 1);
        final double[][] B = X.values;
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = j0; j <= j1; j++) {
                    B[i][j - j0] = values[r[i]][j];
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    public double get(int row, int col) {
        return values[row][col];
    }

    public void set(int row, int col, double value) {
        values[row][col] = value;
    }
    
    public void setRow(int row, double[] values){
        for(int i=0;i<values.length;i++){
            this.values[row][i] = values[i];
        }
    }
    
    public void setCol(int col, double[] values){
        for(int i=0;i<values.length;i++){
            this.values[i][col] = values[i];
        }
    }

    /**
     * Set a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param j0 Initial column index
     * @param j1 Final column index
     * @param X A(i0:i1,j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int i0, int i1, int j0, int j1, Matrix X) {
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = j0; j <= j1; j++) {
                    values[i][j] = X.get(i - i0, j - j0);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set a submatrix.
     *
     * @param r Array of row indices.
     * @param c Array of column indices.
     * @param X A(r(:),c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int[] r, int[] c, Matrix X) {
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = 0; j < c.length; j++) {
                    values[r[i]][c[j]] = X.get(i, j);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set a submatrix.
     *
     * @param r Array of row indices.
     * @param j0 Initial column index
     * @param j1 Final column index
     * @param X A(r(:),j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int[] r, int j0, int j1, Matrix X) {
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = j0; j <= j1; j++) {
                    values[r[i]][j] = X.get(i, j - j0);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param c Array of column indices.
     * @param X A(i0:i1,c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int i0, int i1, int[] c, Matrix X) {
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = 0; j < c.length; j++) {
                    values[i][c[j]] = X.get(i - i0, j);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set Matrix value to identity matrix.
     * @return this matrix
     */
    public DefaultMatrix setToIdentity(){
        Matrices.setToIdentity(values);
        return this;
    }

    public boolean isIdentity(){
        return Matrices.isIdentity(values);
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * invert matrix
     */
    public MatrixRW invert(MatrixRW buffer){
        double[][] bufferValues;
        if(buffer == null){
            buffer = this.copy();
            bufferValues = ((DefaultMatrix)buffer).values;
        }else{
            bufferValues = dArray(buffer);
        }
        bufferValues = Matrices.invert(values,bufferValues);
        if(bufferValues == null){
            throw new InvalidArgumentException("Can not inverse");
        }
        buffer.set(bufferValues);
        return buffer;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods local  //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * invert matrix
     */
    public DefaultMatrix localInvert(){
        final double[][] inverse = Matrices.localInvert(values);
        if(inverse == null){
            throw new InvalidArgumentException("Can not inverse");
        }
        return this;
    }

    public DefaultMatrix localAdd(MatrixRW other){
        Matrices.localAdd(values, dArray(other));
        return this;
    }

    public DefaultMatrix localSubtract(MatrixRW other){
        Matrices.localSubtract(values, dArray(other));
        return this;
    }

    public DefaultMatrix localScale(double[] tuple){
        Matrices.localScale(values, tuple);
        return this;
    }

    public DefaultMatrix localScale(double scale){
        Matrices.localScale(values, scale);
        return this;
    }

    public DefaultMatrix localMultiply(Matrix other){
        Matrices.localMultiply(values, dArray(other));
        return this;
    }

    /**
     * Translate the matrix.
     * Add given tuple values in the last matrix column.
     * 
     * @param translation
     */
    public void localTranslate(Tuple translation){
        for(int r=0,n=translation.getSize();r<n;r++){
            values[r][nbCol-1] += translation.get(r);
        }
    }

    public DefaultMatrix transpose(){
        return new DefaultMatrix(Matrices.transpose(values));
    }

    public double dot(Matrix other){
        return Matrices.dot(values, dArray(other));
    }

    ////////////////////////////////////////////////////////////////////////////
    // various transformed outputs /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public DefaultMatrix copy() {
        return new DefaultMatrix(this);
    }

    public float[] toArrayFloatColOrder(float[] array){
        if(array==null) array = new float[nbRow*nbCol];
        for(int p=0,c=0;c<nbCol;c++){
            for(int r=0;r<nbRow;r++,p++){
                array[p] = (float)values[r][c];
            }
        }
        return array;
    }

    /**
     * Test if all cells in the matrix equals given value.
     * @param scalar scalar
     * @param tolerance tolerance
     * @return true if all values match
     */
    public boolean allEquals(double scalar, double tolerance) {
        for(int r=0; r<nbRow; r++){
            for(int c=0; c<nbCol; c++){
                if(Math.abs(values[r][c]-scalar) > tolerance){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DefaultMatrix)) {
            return false;
        }
        final DefaultMatrix other = (DefaultMatrix) obj;
        if (this.nbRow != other.getNbRow()) {
            return false;
        }
        if (this.nbCol != other.getNbCol()) {
            return false;
        }

        //check values
        if(!Arrays.equals(toArrayDouble(), other.toArrayDouble())){
            return false;
        }

        return true;
    }

    public double[] transform(double[] vector, double[] buffer) {
        return Matrices.transform(values, vector, buffer);
    }

}