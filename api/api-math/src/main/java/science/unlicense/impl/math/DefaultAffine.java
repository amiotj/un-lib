
package science.unlicense.impl.math;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class DefaultAffine extends AbstractAffine.RW {

    private final int dim;

    public static AffineRW create(int dimension) {
        switch (dimension) {
            case 1 : return new Affine1();
            case 2 : return new Affine2();
            case 3 : return new Affine3();
            default : return new DefaultAffine(dimension);
        }
    }

    public static AffineRW create(Affine toCopy) {
        final AffineRW cp = create(toCopy.getInputDimensions());
        cp.set(toCopy);
        return cp;
    }

    public DefaultAffine(int dimension) {
        this.dim = dimension;
    }

    @Override
    public MatrixRW toMatrix() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void fromMatrix(Matrix m) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int getInputDimensions() {
        return dim;
    }

    @Override
    public int getOutputDimensions() {
        return dim;
    }

    @Override
    public double get(int row, int col) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void set(int row, int col, double value) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        throw new UnimplementedException("Not supported yet.");
    }

}
