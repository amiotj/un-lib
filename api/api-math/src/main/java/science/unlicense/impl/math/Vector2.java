
package science.unlicense.impl.math;

import science.unlicense.api.exception.InvalidIndexException;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class Vector2 implements TupleRW {

    public double x;
    public double y;

    public Vector2() {
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int getSize() {
        return 2;
    }

    @Override
    public double get(int indice) {
        switch (indice) {
            case 0 : return x;
            case 1 : return y;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        throw new InvalidIndexException();
    }

    @Override
    public double getW() {
        throw new InvalidIndexException();
    }

    @Override
    public Tuple getXY() {
        return new DefaultTuple(x, y);
    }

    @Override
    public Tuple getXYZ() {
        throw new InvalidIndexException();
    }

    @Override
    public Tuple getXYZW() {
        throw new InvalidIndexException();
    }

    @Override
    public boolean isZero() {
        return x==0 && y==0;
    }

    @Override
    public boolean isAll(double value) {
        return x==value && y==value;
    }

    @Override
    public boolean isValid() {
        return !( Double.isInfinite(x) || Double.isNaN(x)
               || Double.isInfinite(y) || Double.isNaN(y) );
    }

    @Override
    public double[] getValues() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] toArrayDouble() {
        return new double[]{x,y};
    }

    @Override
    public double[] toArrayDouble(double[] buffer) {
        buffer[0] = x;
        buffer[1] = y;
        return buffer;
    }

    @Override
    public float[] toArrayFloat() {
        return new float[]{(float)x,(float)y};
    }

    @Override
    public float[] toArrayFloat(float[] buffer) {
        buffer[0] = (float) x;
        buffer[1] = (float) y;
        return buffer;
    }

    @Override
    public Vector2 copy() {
        return new Vector2(x, y);
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public void setZ(double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setW(double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXY(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setXYZ(double x, double y, double z) {
        throw new InvalidIndexException();
    }

    @Override
    public void setXYZW(double x, double y, double z, double w) {
        throw new InvalidIndexException();
    }

    @Override
    public void setAll(double v) {
        x = v;
        y = v;
    }

    @Override
    public void setToZero() {
        x = 0.0;
        y = 0.0;
    }

    @Override
    public void setToNaN() {
        x = Double.NaN;
        y = Double.NaN;
    }

    @Override
    public void set(int indice, double value) {
        switch (indice) {
            case 0 : x = value; break;
            case 1 : y = value; break;
            default : throw new InvalidIndexException();
        }
    }

    @Override
    public void set(Tuple toCopy) {
        x = toCopy.getX();
        y = toCopy.getY();
    }

    @Override
    public void set(double[] values) {
        x = values[0];
        y = values[1];
    }

    @Override
    public void set(float[] values) {
        x = values[0];
        y = values[1];
    }

    @Override
    public boolean equals(Tuple obj, double tolerance) {
        if (obj == null) {
            return false;
        }
        if (obj.getSize() != 2) {
            return false;
        }

        final double diffx = x - obj.getX();
        if (diffx < -tolerance || diffx > tolerance) {
            return false;
        }

        final double diffy = y - obj.getY();
        if (diffy < -tolerance || diffy > tolerance) {
            return false;
        }

        return true;
    }
    
}
