package science.unlicense.impl.math;

import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.Maths;
import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public final class Matrices {

    private Matrices() {
    }

    /**
     * Checks if the given matrix is the identity matrix.
     * 
     * @param m matrix to test.
     * @return true if matrix is an Identity matrix.
     */
    public static boolean isIdentity(final double[][] m){
        if( m.length!=m[0].length ) return false; // m must be a square matrix
        for( int x=0; x<m[0].length; x++) {
            for( int y=0; y<m.length; y++) {
                if(x==y){
                    if( m[y][x] != 1 ) return false;
                }else{
                    if( m[y][x] != 0 ) return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Sets the given matrix to identity matrix.
     * 
     * @param m a square matrix.
     * @throws InvalidArgumentException when m is not a square matrix.
     */
    public static void setToIdentity( double[][] m ){
        if (m.length != m[0].length) {
            throw new InvalidArgumentException("The m matrix must be a square matrix.");
        }
        for (int x = 0; x < m[0].length; x++) {
            for (int y = 0; y < m.length; y++) {
                if (x == y) {
                    m[y][x] = 1.;
                } else {
                    m[y][x] = 0.;
                }
            }
        }
    }
    
    /**
     * Creates a n*n identity matrix.
     * 
     * @param n number of rows and columns.
     * @return an n*n identity matrix.
     */
    public static double[][] identity(int n) {
        double[][] identity = new double[n][n];
        for (int i = 0; i < n; i++) {
            identity[i][i] = 1.;
        }
        return identity;
    }

    /**
     * Adds m1 and m2, the result is stored in m1, returns m1.
     * see {@link Matrices#add(un.science.math.Matrix, un.science.math.Matrix, un.science.math.Matrix)}
     * @param m1 first matrix
     * @param m2 second matrix
     * @return m1 + m2 result in m1 matrix.
     */
    public static double[][] localAdd(final double[][] m1, final double[][] m2){
        return add(m1,m2,m1);
    }

    /**
     * Adds m1 and m2 matrices, the result is stored in buffer, returns buffer.
     * If buffer is null, a new matrix is created.
     * Matrices must have the same size.
     * @param m1 first matrix
     * @param m2 second matrix
     * @param buffer result buffer, can be null
     * @return m1 + m2 result in buffer matrix.
     * @throws InvalidArgumentException when matrices size differ.
     */
    public static double[][] add(final double[][] m1, final double[][] m2, double[][] buffer) throws InvalidArgumentException {
        if( m1.length != m2.length || m1[0].length != m2[0].length ) {
            throw new InvalidArgumentException( "The two given matrix must have same dimensions." );
        }
        
        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        if(buffer == null){
            buffer = new double[nbRow][nbCol];
        }

        for(int x=0; x<nbCol;x++){
            for(int y=0; y<nbRow; y++){
                buffer[y][x] = m1[y][x] + m2[y][x];
            }
        }
        return buffer;
    }

    /**
     * Subtracts m2 to m1, the result is stored in m1, returns m1.
     * see {@link Matrices#subtract(un.science.math.Matrix, un.science.math.Matrix, un.science.math.Matrix)}
     * @param m1 first matrix
     * @param m2 second matrix
     * @return m1 - m2 result in m1 matrix.
     */
    public static double[][] localSubtract(final double[][] m1, final double[][] m2){
        return subtract(m1,m2,m1);
    }

    /**
     * Subtracts m2 to m2 matrices, result is stored in buffer, returns buffer.
     * If buffer is null, a new matrix is created.
     * Matrices must have the same size.
     * @param m1 first matrix
     * @param m2 second matrix
     * @param buffer result buffer, can be null
     * @return m1 - m2 result in buffer matrix.
     * @throws InvalidArgumentException when matrices size differ
     */
    public static double[][] subtract(final double[][] m1, final double[][] m2, double[][] buffer) throws InvalidArgumentException {
        if( m1.length != m2.length || m1[0].length != m2[0].length ) {
            throw new InvalidArgumentException( "The two given matrix must have same dimensions." );
        }
        
        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        if(buffer == null){
            buffer = new double[nbRow][nbCol];
        }

        for(int x=0; x<nbCol;x++){
            for(int y=0; y<nbRow; y++){
                buffer[y][x] = m1[y][x] - m2[y][x];
            }
        }
        return buffer;
    }

    /**
     * Scales m1 by scaleFactor, result is stored in m1, returns m1.
     * 
     * see {@link Matrices#scale(un.science.math.Matrix, un.science.math.Matrix, double)}
     * @param m1 input matrix
     * @param scaleFactor scale factor
     * @return matrix m1
     */
    public static double[][] localScale(final double[][] m1, final double scaleFactor) {
        return scale(m1,scaleFactor,m1);
    }

    /**
     * Scales matrix by columns.
     * 
     * @param m1 input matrix
     * @param tuple scale factors (one by columns).
     * @return matrix m1
     */
    public static double[][] localScale(double[][] m1, double[] tuple) {
        return scale(m1,tuple,m1);
    }

    /**
     * Scales matrix by columns.
     * @param m1 input matrix
     * @param tuple scale
     * @param buffer result buffer, can be null
     * @return scaled matrix
     */
    public static double[][] scale( double[][] m1, double[] tuple, double[][] buffer) throws InvalidArgumentException {
        
        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        if( buffer != null && ( nbRow != buffer.length || nbCol != buffer[0].length ) ) {
            throw new InvalidArgumentException( "The two given matrix must have same dimensions." );
        }
        if( nbCol != tuple.length ) {
            throw new InvalidArgumentException( "tuple's argument size is incorrect." );
        }

        if(buffer == null){
            buffer = new double[nbRow][nbCol];
        }

        for(int x=0; x<nbCol;x++){
            for(int y=0; y<nbRow; y++){
                buffer[y][x] = m1[y][x] * tuple[x];
            }
        }
        return buffer;
    }

    /**
     * Scale m1 by scale, result is stored in buffer.
     * if buffer is null, a new matrix is created.
     * Matrices must have the same size
     * @param m1 input matrix
     * @param scale scale
     * @param buffer result buffer, can be null
     * @return scaled matrix
     * @throws InvalidArgumentException when matrices size differ
     */
    public static double[][] scale(final double[][] m1, final double scale, double[][] buffer) throws InvalidArgumentException {

        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        if(buffer == null){
            buffer = new double[nbRow][nbCol];
        }

        for(int x=0; x<nbCol;x++){
            for(int y=0; y<nbRow; y++){
                buffer[y][x] = m1[y][x] * scale;
            }
        }
        return buffer;
    }

    /**
     * Multiply given matrix by itself the given number of time.
     * @param m1 input matrix
     * @param power power value
     * @param buffer result buffer, can be null
     * @return result matrix
     */
    public static double[][] power(final double[][] m1, final int power, double[][] buffer) throws InvalidArgumentException{

        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        if(nbCol != nbRow){
            throw new InvalidArgumentException("Matrice row and col number must be the same.");
        }

        if(buffer == null){
            buffer = new double[nbRow][nbCol];
        }

        if(isIdentity(m1) || power == 0){
            setToIdentity(buffer);
            return buffer;
        }

        if(power < 0){
            throw new InvalidArgumentException("Negative power not supported.");
        }else{
            Arrays.copy(m1, 0, 0, nbCol, nbRow, buffer, 0, 0);
            for(int i=1;i<power;i++){
                multiply(m1, buffer, buffer);
            }
        }

        return buffer;
    }

    /**
     * return the transposed matrix.
     * (flips row/col values)
     * @param m1 input matrix
     * @return result matrix
     */
    public static double[][] transpose(final double[][] m1) throws InvalidArgumentException{

        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        final double[][] res = new double[nbCol][nbRow];

        for(int x=0; x<nbCol;x++){
            for(int y=0; y<nbRow; y++){
                res[x][y] = m1[y][x];
            }
        }
        return res;
    }

    /**
     * Matrix inversion using Gauss.
     *
     * @param origValues input matrix
     * @return inverted matrix or null if not possible.
     * @author Xavier Philippeau
     */
    public static double[][] localInvert(double[][] origValues) {
        return invert(origValues, origValues);
    }

    /**
     * Matrix inversion using Gauss.
     *
     * @param origValues input matrix
     * @param buffer result buffer, can be null
     * @return inverted matrix or null if not possible.
     * @author Xavier Philippeau
     */
    public static double[][] invert(double[][] origValues, double[][] buffer) {

        final int origNbRow = origValues.length;
        final int origNbCol = origValues[0].length;

        //Matrix can be inverted only if it is a square matrix.
        if (origNbRow != origNbCol) {
            return null;
        }

        // Create a temporary work matrix T = [ this | Identity ]
        final int tNbRow = origNbCol;
        final int tNbCol = origNbCol * 2;
        final double[][] tValues = new double[tNbRow][tNbCol];
        for (int y = 0; y < origNbRow; y++) {
            for (int x = 0; x < origNbCol; x++) {
                tValues[y][x] = origValues[y][x];
                if (x == y) {
                    tValues[y][origNbCol + x] = 1;
                }
            }
        }

        // Pour chaque ligne de la matrice T
        for (int x = 0; x < tNbRow; x++) {

            // Cherche la ligne avec le pivot max (en valeur absolue)
            int bestline = x;
            double pivot = tValues[bestline][x];
            for (int y = x + 1; y < tNbRow; y++) {
                if (Math.abs(tValues[y][x]) > Math.abs(pivot)) {
                    bestline = y;
                    pivot = tValues[bestline][x];
                }
            }

            if (pivot == 0) {
                System.err.println("Inversion : Le pivot est nul,inversion impossible !!");
                return null;
            }

            // Echange des lignes (si necessaire)
            if (bestline != x) {
                double tmp;
                for (int t = 0; t < tNbCol; t++) {
                    tmp = tValues[x][t];
                    tValues[x][t] = tValues[bestline][t];
                    tValues[bestline][t] = tmp;
                }
            }

            // Normalisation de la ligne du pivot
            for (int t = 0; t < tNbCol; t++) {
                tValues[x][t] /= pivot;
            }

            // elimination des autres lignes
            for (int y = 0; y < tNbRow; y++) {
                if (y == x) {
                    continue;
                }
                double coef = tValues[y][x];
                for (int t = 0; t < tNbCol; t++) {
                    tValues[y][t] -= coef * tValues[x][t];
                }
            }
        }

        // recupere la partie droite de T qui contient l'inverse de la matrice
        // (la partie gauche devrait contenir l'identité)
        if(buffer==null){
            buffer = new double[origNbRow][origNbCol];
        }
        for (int y = 0; y < origNbRow; y++) {
            for (int x = 0; x < origNbCol; x++) {
                buffer[y][x] = tValues[y][origNbCol + x];
            }
        }

        return buffer;
    }

    /**
     * Matrices dot product.
     *
     * @param m1 first matrix
     * @param m2 second matrix
     * @return dot product
     */
    public static double dot(double[][] m1, double[][] m2){
        final int nbRow = m1.length;
        final int nbCol = m1[0].length;

        double sum = 0;
        for(int r=0; r<nbRow; r++){
            for(int c=0; c<nbCol; c++){
                sum += m1[r][c] * m2[r][c];
            }
        }

        return sum;
    }

    /**
     * replace valeus close to 0 with zero, removing -0 if present
     * @param matrix 
     * @param epsilon 
     */
    public static void roundZeros(double[][] matrix, double epsilon){
        for(int x=0;x<matrix.length;x++){
            for(int y=0;y<matrix[0].length;y++){
                if(!(matrix[x][y]>epsilon || matrix[x][y]<-epsilon)){
                    matrix[x][y] = 0.0;
                }
            }
        }
    }
    
    public static TupleRW transformLocal(final double[][] matrix, final TupleRW vector) {
        return transform(matrix, vector, vector);
    }

    public static TupleRW transform(final double[][] matrix, final Tuple vector, TupleRW buffer) {
        if(buffer==null) buffer = new DefaultTuple(matrix.length);
        transform(matrix, vector.getValues(), buffer.getValues());
        return buffer;
    }

    /**
     * Transform given vector.
     *
     * @param matrix input matrix
     * @param vector vector to transform
     * @param buffer result vector buffer, can be null
     * @return the product of matrix and vector.
     */
    public static double[] transform(final double[][] matrix, final double[] vector, double[] buffer) {
        if(vector.length != matrix[0].length){
            throw new InvalidArgumentException("matrix column size and vector size differ : "+matrix[0].length+","+vector.length);
        }
        
        final int nbRow = matrix.length;
        final int nbCol = matrix[0].length;
        final double[] res = new double[nbRow];

        for (int r = 0; r < nbRow; r++) {
            double s = 0;
            for (int c = 0; c < nbCol; c++) {
                s += matrix[r][c] * vector[c];
            }
            res[r] = s;
        }

        if( buffer == null ) {
            buffer = new double[matrix.length];
        }
        Arrays.copy(res, 0, res.length, buffer, 0);
        
        return buffer;
    }

    /**
     * Transforms given vector.
     *
     * @param matrix transformation matrix.
     * @param vector considered as a column matrix.
     * @param buffer result vector buffer, can be null
     * @return the product of matrix and vector.
     */
    public static float[] transform(final double[][] matrix, final float[] vector, float[] buffer) {
        if(vector.length != matrix[0].length){
            throw new InvalidArgumentException("matrix column size and vector size differ : "+matrix[0].length+","+vector.length);
        }

        if (buffer == null) {
            buffer = new float[matrix.length];
        }

        final int nbRow = matrix.length;
        final int nbCol = matrix[0].length;
        final float[] res = new float[nbRow];

        for (int r = 0; r < nbRow; r++) {
            double s = 0;
            for (int c = 0; c < nbCol; c++) {
                s += matrix[r][c] * vector[c];
            }
            res[r] = (float) s;
        }

        Arrays.copy(res, 0, res.length, buffer, 0);
        return buffer;
    }


    /**
     * Multiply m1 by m2, result is stored in m1, returns m1.
     * see {@link Matrices#multiply(un.science.math.Matrix, un.science.math.Matrix, un.science.math.Matrix)}
     */
    public static double[][] localMultiply(final double[][] m1, final double[][] m2){
        //
        if ( (m1.length != m1[0].length) || (m2.length != m2[0].length) || (m1.length != m2.length) ){
            throw new InvalidArgumentException("Matrices must both be square and have same size.");
        }
        return multiply(m1,m2, m1);
    }

    /**
     * Multiply m1 by m2 matrices, result is stored in buffer.
     * If buffer is null, a new matrix is created.
     */
    public static double[][] multiply(final double[][] m1, final double[][] m2, double[][] buffer) {

        final int m1r = m1.length;
        final int m1c = m1[0].length;
        final int m2r = m2.length;
        final int m2c = m2[0].length;

        if (m1c != m2r){
            throw new InvalidArgumentException("m1.nbCol is not equal to m2.nbRow");
        }

        if (buffer == null) {
            buffer = new double[m1r][m2c];
        }else{
            //check buffer size
            if (buffer.length != m1r || buffer[0].length != m2[0].length){
                throw new InvalidArgumentException("buffer.nbRow is not equal to m1.nbRow or buffer.nbCol is not equal to m2.nbCol");
            }
        }

        final double[][] temp = new double[m1r][m2c];
        for (int r=0; r<m1r; r++) {
            for (int c=0; c<m2c; c++) {
                temp[r][c] = 0.0;
                for (int k = 0; k < m1c; k++) {
                    temp[r][c] += m1[r][k] * m2[k][c];
                }
            }
        }

        for (int r=0; r<m1r; r++) {
            for (int c=0; c<m2c; c++) {
                buffer[r][c] = temp[r][c];
            }
        }

        return buffer;
    }

    /**
     * Create a rotation matrix from given angle and axis.
     *
     * http://en.wikipedia.org/wiki/Rotation_matrix
     *
     * @param angle rotation angle in radians
     * @param rotationAxis Tuple 3
     * @param buffer Matrix 3x3
     * @return rotation matrix
     */
    public static double[][] createRotation3(final double angle, final Tuple rotationAxis, double[][] buffer) {

        if(buffer == null){
            buffer = new double[3][3];
        }

        final double fCos = Math.cos(angle);
        final double fSin = Math.sin(angle);
        final double fOneMinusCos = (1.0) - fCos;
        final double fX2 = rotationAxis.get(0) * rotationAxis.get(0);
        final double fY2 = rotationAxis.get(1) * rotationAxis.get(1);
        final double fZ2 = rotationAxis.get(2) * rotationAxis.get(2);
        final double fXYM = rotationAxis.get(0) * rotationAxis.get(1) * fOneMinusCos;
        final double fXZM = rotationAxis.get(0) * rotationAxis.get(2) * fOneMinusCos;
        final double fYZM = rotationAxis.get(1) * rotationAxis.get(2) * fOneMinusCos;
        final double fXSin = rotationAxis.get(0) * fSin;
        final double fYSin = rotationAxis.get(1) * fSin;
        final double fZSin = rotationAxis.get(2) * fSin;

        buffer[0][0] = fX2 * fOneMinusCos + fCos;
        buffer[0][1] = fXYM - fZSin;
        buffer[0][2] = fXZM + fYSin;
        buffer[1][0] = fXYM + fZSin;
        buffer[1][1] = fY2 * fOneMinusCos + fCos;
        buffer[1][2] = fYZM - fXSin;
        buffer[2][0] = fXZM - fYSin;
        buffer[2][1] = fYZM + fXSin;
        buffer[2][2] = fZ2 * fOneMinusCos + fCos;

        return buffer;
    }

    /**
     * Create a rotation matrix from given angle and axis.
     *
     * http://en.wikipedia.org/wiki/Rotation_matrix
     *
     * @param angle rotation angle in radians
     * @param rotationAxis Tuple 3
     * @param buffer Matrix 4x4
     * @return rotation matrix
     */
    public static double[][] createRotation4(final double angle, final Tuple rotationAxis, double[][] buffer) {

        if(buffer == null){
            buffer = new double[4][4];
        }

        final double fCos = Math.cos(angle);
        final double fSin = Math.sin(angle);
        final double fOneMinusCos = (1.0) - fCos;
        final double fX2 = rotationAxis.get(0) * rotationAxis.get(0);
        final double fY2 = rotationAxis.get(1) * rotationAxis.get(1);
        final double fZ2 = rotationAxis.get(2) * rotationAxis.get(2);
        final double fXYM = rotationAxis.get(0) * rotationAxis.get(1) * fOneMinusCos;
        final double fXZM = rotationAxis.get(0) * rotationAxis.get(2) * fOneMinusCos;
        final double fYZM = rotationAxis.get(1) * rotationAxis.get(2) * fOneMinusCos;
        final double fXSin = rotationAxis.get(0) * fSin;
        final double fYSin = rotationAxis.get(1) * fSin;
        final double fZSin = rotationAxis.get(2) * fSin;

        buffer[0][0] = fX2 * fOneMinusCos + fCos;
        buffer[0][1] = fXYM - fZSin;
        buffer[0][2] = fXZM + fYSin;
        buffer[0][3] = 0;
        buffer[1][0] = fXYM + fZSin;
        buffer[1][1] = fY2 * fOneMinusCos + fCos;
        buffer[1][2] = fYZM - fXSin;
        buffer[1][3] = 0;
        buffer[2][0] = fXZM - fYSin;
        buffer[2][1] = fYZM + fXSin;
        buffer[2][2] = fZ2 * fOneMinusCos + fCos;
        buffer[2][3] = 0;
        buffer[3][0] = 0;
        buffer[3][1] = 0;
        buffer[3][2] = 0;
        buffer[3][3] = 1;

        return buffer;
    }

    /**
     * Build rotation matrix from Euler angle.
     * Sources : 
     * http://en.wikipedia.org/wiki/Axes_conventions
     * http://jeux.developpez.com/faq/math/?page=transformations#Q36
     * http://en.wikipedia.org/wiki/Euler_angles
     * http://mathworld.wolfram.com/EulerAngles.html
     * http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToMatrix/
     *
     * Euler angle convention is : (Z-Y’-X’’) ISO 1151–2:1985
     * - Heading (yaw)      [-180°...+180°]
     * - Elevation (pitch)  [ -90°... +90°]
     * - Bank (roll)        [-180°...+180°]
     * 
     * @param euler angles in radians (heading/yaw , elevation/pitch , bank/roll)
     * @param buffer size 4x4 or 3x3
     * @return matrix, never null
     */
    public static double[][] fromEuler(final double[] euler, double[][] buffer) {

        if(buffer == null){
            buffer = new double[4][4];
        }

        final double angle_x = euler[2];
        final double angle_y = euler[1];
        final double angle_z = euler[0];
        //check the given values are valid
        CObjects.ensureBetween(angle_x, -Maths.PI, +Maths.PI);
        CObjects.ensureBetween(angle_y, -Maths.HALF_PI, +Maths.HALF_PI);
        CObjects.ensureBetween(angle_z, -Maths.PI, +Maths.PI);
        
        final double cx = Math.cos(angle_x);
        final double sx = Math.sin(angle_x);
        final double cy = Math.cos(angle_y);
        final double sy = Math.sin(angle_y);
        final double cz = Math.cos(angle_z);
        final double sz = Math.sin(angle_z);


        // cy*cz    sx*sy*cz - cx*sz    cx*sy*cz + sx*sz
        // cy*sz    sx*sy*sz + cx*cz    cx*sy*sz - sx*cz
        // -sy            sx*cy              cx*cy
        
        buffer[0][0] = cy*cz;
        buffer[0][1] = sx*sy*cz - cx*sz;
        buffer[0][2] = cx*sy*cz + sx*sz;

        buffer[1][0] = cy*sz;
        buffer[1][1] = sx*sy*sz + cx*cz;
        buffer[1][2] = cx*sy*sz - sx*cz;

        buffer[2][0] = -sy;
        buffer[2][1] = sx*cy;
        buffer[2][2] = cx*cy;

        //fill 4 dimension
        if(buffer.length>3){
            buffer[0][3]  =  0;
            buffer[1][3]  =  0;
            buffer[2][3]  =  0;

            buffer[3][0]  =  0;
            buffer[3][1]  =  0;
            buffer[3][2]  =  0;
            buffer[3][3]  =  1;
        }

        return buffer;
    }

    /**
     * Calculate Euler angle of given matrix.
     * Source : 
     * http://www.soi.city.ac.uk/~sbbh653/publications/euler.pdf
     * http://jeux.developpez.com/faq/math/?page=transformations#Q37
     *
     * @param mat input matrix
     * @param buffer euler buffer, can be null
     * @return euler angle in radians (heading/yaw , elevation/pitch , bank/roll)
     */
    public static double[] toEuler(double[][] mat, double[] buffer){

        if(buffer == null){
            buffer = new double[3];
        }

        double angle_x;
        double angle_y;
        double angle_z;
        
        if(mat[2][0] != -1 && mat[2][0] != +1){
            //first possible solution
            angle_y = -Math.asin(mat[2][0]);
            double cosy1 = Math.cos(angle_y);
            angle_x = Math.atan2(mat[2][1]/cosy1, mat[2][2]/cosy1);
            angle_z = Math.atan2(mat[1][0]/cosy1, mat[0][0]/cosy1);
            
            //second possible solution
            //angle_y = Angles.PI - y1;
            //double cosy2 = Math.cos(angle_y);
            //angle_x = Math.atan2(mat[2][1]/cosy2, mat[2][2]/cosy2);
            //angle_z = Math.atan2(mat[1][0]/cosy2, mat[0][0]/cosy2);
            
        }else{
            // Gimbal lock
            angle_z = 0;
            if(mat[2][0] == -1){
                angle_y = Maths.HALF_PI;
                angle_x = angle_z + Math.atan2(mat[0][1], mat[0][2]);
            }else{
                angle_y = -Maths.HALF_PI;
                angle_x = -angle_z + Math.atan2(-mat[0][1], -mat[0][2]);
            }
        }
                
        
        buffer[0] = angle_z;
        buffer[1] = angle_y;
        buffer[2] = angle_x;
        return buffer;
    }

    /**
     * Decompose a matrix in rotation, scale and translation.
     * The matrix is expected to be orthogonal of size 3x3 or 4x4.
     * 
     * @param trs
     * @param rotation
     * @param scale
     * @param translation 
     */
    public static void decomposeMatrix(Matrix trs, MatrixRW rotation, TupleRW scale, TupleRW translation){
        final int dimension = trs.getNbCol()-1;
        if(dimension == 2){
            final double scaleX = Math.sqrt(trs.get(0,0)*trs.get(0,0)
                                          + trs.get(1,0)*trs.get(1,0));
            final double scaleY = Math.sqrt(trs.get(0,1)*trs.get(0,1)
                                          + trs.get(1,1)*trs.get(1,1));
            
            final double[] invertScale = new double[]{1d/scaleX,1d/scaleY};
            final MatrixRW rot = trs.getMatrix(0, 1, 0, 1);
            rot.localScale(invertScale);

            rotation.set(rot);
            scale.setXY(scaleX, scaleY);
            translation.setXY(trs.get(0,2), trs.get(1,2));
        }else if(dimension == 3){
            final double scaleX = Math.sqrt(trs.get(0,0)*trs.get(0,0)
                                          + trs.get(1,0)*trs.get(1,0)
                                          + trs.get(2,0)*trs.get(2,0));
            final double scaleY = Math.sqrt(trs.get(0,1)*trs.get(0,1)
                                          + trs.get(1,1)*trs.get(1,1)
                                          + trs.get(2,1)*trs.get(2,1));
            final double scaleZ = Math.sqrt(trs.get(0,2)*trs.get(0,2)
                                          + trs.get(1,2)*trs.get(1,2)
                                          + trs.get(2,2)*trs.get(2,2));
            final double[] invertScale = new double[]{1d/scaleX,1d/scaleY,1d/scaleZ};
            final MatrixRW rot = trs.getMatrix(0, 2, 0, 2);
            rot.localScale(invertScale);

            rotation.set(rot);
            scale.setXYZ(scaleX, scaleY, scaleZ);
            translation.setXYZ(trs.get(0,3), trs.get(1,3), trs.get(2,3));

        }else{
            throw new RuntimeException("Only works for 2D and 3D for now. TODO");
        }
    }
    
    /**
     * Calculate a view matrix, often used by rendering engines as the
     * World to Camera transform.
     *
     * @param eye : viewer position, Tuple 3
     * @param center : position looked at, Tuple 3
     * @param up : up direction, Tuple 3
     * @param buffer : storage, Matrix 4x4
     * @return Matrix4x4d
     */
    public static double[][] lookAt(final Tuple eye, final Tuple center,
            final Tuple up, double[][] buffer) {

        if(buffer == null){
            buffer = new double[4][4];
        }

        final Vector forward = new Vector(center);
        forward.localSubtract(eye);
        forward.localNormalize();
        final Vector upward = new Vector(up).localNormalize();
        final Vector side = forward.cross(upward);
        side.localNormalize();
        side.cross(forward, upward);

        buffer[0][0]=side.get(0);           buffer[0][1]=upward.get(0);           buffer[0][2]=forward.get(0);          buffer[0][3]=0;
        buffer[1][0]=side.get(1);           buffer[1][1]=upward.get(1);           buffer[1][2]=forward.get(1);          buffer[1][3]=0;
        buffer[2][0]=side.get(2);           buffer[2][1]=upward.get(2);           buffer[2][2]=forward.get(2);          buffer[2][3]=0;
        buffer[3][0]=0;                     buffer[3][1]=0;                       buffer[3][2]=0;                       buffer[3][3]=1;

        final double[][] eyeTranslation = new double[][]{
            {1,0,0,0},
            {0,1,0,0},
            {0,0,1,0},
            {eye.get(0),eye.get(1),eye.get(2),1}
        };

        multiply(eyeTranslation, buffer, buffer);

        return buffer;
    }

    public static double[][] lookAt(final Tuple target, final Tuple up, double[][] buffer) {

        if(buffer == null){
            buffer = new double[4][4];
        }

        Vector N = new Vector(target);
        N.localNormalize();
        Vector U = new Vector(up);
        U.localNormalize();
        U = U.cross(N, null);
        Vector V = N.cross(U,null);

        buffer[0][0] = U.get(0);   buffer[0][1] = U.get(1);   buffer[0][2] = U.get(2);   buffer[0][3] = 0.0f;
        buffer[1][0] = V.get(0);   buffer[1][1] = V.get(1);   buffer[1][2] = V.get(2);   buffer[1][3] = 0.0f;
        buffer[2][0] = N.get(0);   buffer[2][1] = N.get(1);   buffer[2][2] = N.get(2);   buffer[2][3] = 0.0f;
        buffer[3][0] = 0.0f;       buffer[3][1] = 0.0f;       buffer[3][2] = 0.0f;       buffer[3][3] = 1.0f;

        return buffer;
    }

    public static double[][] ortho2(double fovy, double aspect, double zNear, double zFar, double[][] buffer){
        if(buffer==null) buffer = new double[4][4];
        
        double rad = Math.toRadians(fovy);

        double range = Math.tan(rad / 2d) * zNear;
        double left = -range * aspect;
        double right = range * aspect;
        double bottom = -range;
        double top = range;

        buffer[0][0] = (2d * zNear) / (right - left);
        buffer[0][1] = 0;
        buffer[0][2] = 0;
        buffer[0][3] = 0;
        buffer[1][0] = 0;
        buffer[1][1] = (2d * zNear) / (top - bottom);
        buffer[1][2] = 0;
        buffer[1][3] = 0;
        buffer[2][0] = 0;
        buffer[2][1] = 0;
        buffer[2][2] = - (zFar + zNear) / (zFar - zNear);
        buffer[2][3] = - 1d;
        buffer[3][0] = 0;
        buffer[3][1] = 0;
        buffer[3][2] = - (2d * zFar * zNear) / (zFar - zNear);
        buffer[3][3] = 1;

        return buffer;
    }


    /**
     * Calculate a projection matrix, often used by rendering engines as the
     * Camera to Homogeneous transform.
     *
     * Matrix as defined here :
     * http://en.wikipedia.org/wiki/Orthographic_projection_(geometry)
     *
     * @param left field of view left
     * @param right field of view right
     * @param bottom field of view bottom
     * @param top field of view top
     * @param near field of view near
     * @param far field of view far
     * @param buffer result buffer, can be null
     * @return 
     */
    public static double[][] orthogonal(final double left, final double right,
            final double bottom, final double top, final double near, final double far,
            double[][] buffer) {

        if(buffer == null){
            buffer = new double[4][4];
        }


        buffer[0][0]=2.0/(right-left); buffer[0][1]=0.0;              buffer[0][2]=0.0;             buffer[0][3]=-(right+left)/(right-left);
        buffer[1][0]=0.0;              buffer[1][1]=2.0/(top-bottom); buffer[1][2]=0.0;             buffer[1][3]=-(top+bottom)/(top-bottom);
        buffer[2][0]=0.0;              buffer[2][1]=0.0;              buffer[2][2]=-2.0/(far-near); buffer[2][3]=-(far+near)/(far-near);
        buffer[3][0]=0.0;              buffer[3][1]=0.0;              buffer[3][2]=0.0;             buffer[3][3]=1.0;

        return buffer;
    }

    /**
     * Calculate a perspective matrix.
     * 
     * @param fov : field of view
     * @param width : canvas width
     * @param height : canvas height
     * @param near : frustrum near plan
     * @param far : frustrum far plan
     * @param buffer : matrix buffer, can be null
     * @param rightHand true for right handed coordinate system, false for leftHanded.
     * @return perspective 4x4 matrix
     */
    public static double[][] perspective(double fov, double width, double height,
            double near, double far, double[][] buffer, boolean rightHand){

        if(buffer == null){
            buffer = new double[4][4];
        }

        //aspect ratio
        final double ar = width / height;
        //z range
        final double zr = near - far;
        final double f = 1.0 / Math.tan(Math.toRadians(fov / 2.0));
        
        if(rightHand){
            buffer[0][0] = f/ar;  buffer[0][1] = 0.0;   buffer[0][2] = 0.0;           buffer[0][3] = 0.0;
            buffer[1][0] = 0.0;   buffer[1][1] = f;     buffer[1][2] = 0.0;           buffer[1][3] = 0.0;
            buffer[2][0] = 0.0;   buffer[2][1] = 0.0;   buffer[2][2] = (far+near)/zr; buffer[2][3] = (2.0*far*near)/zr;
            buffer[3][0] = 0.0;   buffer[3][1] = 0.0;   buffer[3][2] = -1.0;          buffer[3][3] = 0.0;
        }else{
            buffer[0][0] = f/ar;  buffer[0][1] = 0.0;   buffer[0][2] = 0.0;           buffer[0][3] = 0.0;
            buffer[1][0] = 0.0;   buffer[1][1] = f;     buffer[1][2] = 0.0;           buffer[1][3] = 0.0;
            buffer[2][0] = 0.0;   buffer[2][1] = 0.0;   buffer[2][2] = (-near-far)/zr;buffer[2][3] = (2.0*far*near)/zr;
            buffer[3][0] = 0.0;   buffer[3][1] = 0.0;   buffer[3][2] = 1.0;           buffer[3][3] = 0.0;
        }
        
        return buffer;
    }

    /**
     * Calculate a projection matrix, often used by rendering engines as the
     * Camera to Homogeneous transform.
     *
     * @param degrees field of view angle
     * @param aspectRatio screen aspect ratio
     * @param near near plan
     * @param far far plan
     * @param buffer result buffer, can be null
     * @return Matrix4x4d
     */
    public static double[][] projection(final double degrees, final double aspectRatio,
            final double near, final double far, double[][] buffer){

        if(buffer == null){
            buffer = new double[4][4];
        }

        //calculate edges
        final double top = near * Math.tan(degrees * Maths.PI / 360.0);
        final double bottom = -top;
        final double left = bottom * aspectRatio;
        final double right = top * aspectRatio;

        //calculate frustrum
        final double a = 2 * near / (right - left);
        final double b = 2 * near / (top - bottom);
        final double c = (right + left) / (right - left);
        final double d = (top + bottom) / (top - bottom);
        final double e = - (far + near) / (far - near);
        final double f = -2 * far * near / (far - near);

        buffer[0][0]=a; buffer[0][1]=0; buffer[0][2]=0; buffer[0][3]= 0;
        buffer[1][0]=0; buffer[1][1]=b; buffer[1][2]=0; buffer[1][3]= 0;
        buffer[2][0]=c; buffer[2][1]=d; buffer[2][2]=e; buffer[2][3]=-1;
        buffer[3][0]=0; buffer[3][1]=0; buffer[3][2]=f; buffer[3][3]= 1;

        return buffer;
    }

    /**
     * Create and orbit matrix 4x4 focus on the root point (0,0,0).
     * 
     * @param xAngle horizontal angle
     * @param yAngle vertical angle
     * @param rollAngle roll angle
     * @param distance distance from base
     * @return orbit matrix 4x4
     */
    public static double[][] focusedOrbit(final double xAngle, final double yAngle, 
            double rollAngle, double distance){
        
        final Vector forward = new Vector(1*distance, 0, 0, 0);
        
        //calculate rotation matrix
        final DefaultMatrix rotateMatrix = new DefaultMatrix(createRotation4(xAngle, new Vector(0, 0, 1),null));
        final DefaultMatrix mv = new DefaultMatrix(createRotation4(yAngle, new Vector(0, -1, 0), null));
        rotateMatrix.localMultiply(mv);
        
        //calculate translation vector
        final Tuple translation = rotateMatrix.transform(forward);
        
        //calculate roll matrix
//        final Matrix4 rollMatrix = new Matrix4();
//        Matrices.createRotation4(rollAngle, new Vector(translation).normalize(), rollMatrix.getValues());
        
        //apply in reverse order
        final DefaultMatrix result = new DefaultMatrix(4,4).setToIdentity();
//        result.localMultiply(rollMatrix);
        result.localMultiply(rotateMatrix);
        result.set(0,3, translation.get(0));
        result.set(1,3, translation.get(1));
        result.set(2,3, translation.get(2));
        return result.getValues();
    } 
    
    /**
     * Hadamard product for 2d order matrices.
     * 
     * Hadamard product is computed by simple multiplication of components between
     * both matrices of the same size.
     * 
     * h(i,j) = a(i,j) * b(i,j)
     * 
     * @param a
     * @param b
     * @return 
     */
    public static double[][] hadamard(final double[][] a, final double[][] b){
        final double[][] r = new double[a.length][a[0].length];
        for(int i=0; i<r.length; i++){
            for(int j=0; j<r[i].length; j++){
                r[i][j] = a[i][j] * b[i][j];
            }
        }
        return r;
    }
    
    //==========================================================================
    
    /**
     * Kronecker product for 2d order matrices.
     * 
     * Kronecker product is computed between two 2d order matrices wich size does
     * not matter. The number of rows (i.e. columns) or the result matrix is 
     * equal to the product of the number of rows (i.e. columns) columns of both 
     * operand matrices.
     * 
     * @param a
     * @param b
     * @return 
     */
    public static double[][] kronecker(final double[][] a, final double[][] b){
        final int la = a.length;
        final int ca = a[0].length;
        final int lb = b.length;
        final int cb = b[0].length;
        final int l = a.length * b.length;
        final int c = a[0].length * b[0].length;
        final double[][] r = new double[l][c];
        for(int i=0; i<l; i++){
            for(int j=0; j<c; j++){
                r[i][j] = a[i/lb][j/cb] * b[i%la][j%ca];
            }
        }
        return r;
    }
    
}