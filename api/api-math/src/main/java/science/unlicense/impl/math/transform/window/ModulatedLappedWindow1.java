

package science.unlicense.impl.math.transform.window;

import science.unlicense.api.math.Maths;

/**
 * Window transofm used by : MP3, MPEG-2, AAC
 * 
 * resource : window function part
 * http://en.wikipedia.org/wiki/Modified_discrete_cosine_transform
 * 
 * TODO many more window functions :
 * http://en.wikipedia.org/wiki/Window_function
 * 
 * @author Johann Sorel
 */
public class ModulatedLappedWindow1 {
    
    private final int N;
    private final float[] window;
    
    public ModulatedLappedWindow1(int N){
        this.N = N;
        window = new float[N];
        
        for(int n=0;n<N;n++){
            window[n] = (float)Math.sin( (Maths.PI/2*N) * (n+0.5f) );
        }
    }
        
    public void transform(float[] data){
        for(int n=0;n<N;n++){
            data[n] *= window[n];
        }
    }
    
}
