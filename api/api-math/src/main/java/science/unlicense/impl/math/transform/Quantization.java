

package science.unlicense.impl.math.transform;

/**
 * Quantization transform.
 * 
 * @author Johann Sorel
 */
public class Quantization {
    
    private final float[] qtable;

    public Quantization(float[] table) {
        this.qtable = table;
    }
    
    public void unQuantify(int[] source, float[] target){
        for(int k=0; k<qtable.length; k++){
            target[k] = source[k] * qtable[k];
        }
    }
    
}
