package science.unlicense.impl.number;

import science.unlicense.api.number.Arithmetic;
import science.unlicense.api.Orderable;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.number.Int64;
import science.unlicense.impl.math.Magnitude;

/**
 * LargeInteger is an immutable arbitrary length integer.
 *
 * @author Bertrand COTE
 */
public class LargeInteger implements Orderable, science.unlicense.api.number.Number{
    
    private static final int  INT_BITS_LENGTH = 32;
    private static final int LONG_BYTE_LENGTH = 8;
    private static final long     TWO_POW_32 = 0x0000000100000000L;
    private static final long   LOW_INT_MASK = 0x00000000ffffffffL;
    private static final long MAX_LONG_VALUE =  0x7fffffffffffffffL;
    private static final long MIN_LONG_VALUE =  0x8000000000000000L;
    private static final long[] MAX_LONG_MAGNITUDE = new long[]{ MAX_LONG_VALUE };
    private static final long[] MIN_LONG_MAGNITUDE = new long[]{ MIN_LONG_VALUE };
    
    // =========================================================================
    // ========== static members ===============================================
    // =========================================================================
    
    public static final LargeInteger ZERO;
    public static final LargeInteger  ONE;
    public static final LargeInteger  TWO;
    
    static {
        ZERO = new LargeInteger( 0x0L );
        ONE  = new LargeInteger( 0x1L );
        TWO  = new LargeInteger( 0x2L );
    }
    
    // =========================================================================
    // ========== data members =================================================
    // =========================================================================
    
    /**
     * this's sign:
     * <ul>
     * <li> 1 for positives numbers.</li>
     * <li>0 for zero.</li>
     * <li>-1 for negatives numbers.</li>
     * </ul>
     */
    private int sign;
    
    /**
     * this's magnitude (the absolute value) coded in a unsigned long array.
     * The least significant long is at index 0 and the most significant 
     * long is at index magnitude.length-1.
     */
    private long[] magnitude;
    
    // =========================================================================
    // ========== Constructors =================================================
    // =========================================================================
    
    /**
     * Default constructor. (Constructs ZERO element.)
     */
    public LargeInteger() {
        this( LargeInteger.ZERO );
    }
    
    /**
     * Copy constructor.
     * 
     * @param other any LargeInteger.
     */
    public LargeInteger( LargeInteger other ) {
        this( other.sign, other.magnitude );
    }
    
    /**
     * Constructor from a signed 64bits integer (long).
     * 
     * @param n a signed 64bits integer (long).
     */
    public LargeInteger( long n ) {
        if( n == 0 ) {
            
            this.sign = 0;
            this.magnitude = new long[]{ 0x0L };
            
        } else {
            
            if( n<0 ) {
                this.sign = -1;
                this.magnitude = new long[]{ -n };
            } else {
                this.sign = 1;
                this.magnitude = new long[]{ n };
            }
        }
    }
    
    /**
     * 
     * @param sign 1 for positive, 0 for zero and -1 for negative.
     * @param magnitude a long array (LSB at index 0 and MSB at last index).
     */
    public LargeInteger( int sign, long[] magnitude ) {
        this.sign = sign;
        this.magnitude = Magnitude.reductMagnitude(magnitude);
        if (this.magnitude.length == 1 && this.magnitude[0] == 0x0L) {
            this.sign = 0;
        }
    }
    
    /**
     * 
     * @param sign 1 for positive, 0 for zero and -1 for negative.
     * @param magnitudeBytes a byte array (LSB at index 0 and MSB at last index).
     */
    public LargeInteger( int sign, byte[] magnitudeBytes ) {
        
        if( magnitudeBytes==null || magnitudeBytes.length==0 || (magnitudeBytes.length==1 && magnitudeBytes[0]==0) ) {
            
            this.sign = 0;
            this.magnitude = new long[]{ 0x0L };
            
        } else {
            
            this.sign = sign;

            int magLength = magnitudeBytes.length/LONG_BYTE_LENGTH;
            if( magnitudeBytes.length%LONG_BYTE_LENGTH!= 0 ) magLength++;
            this.magnitude = new long[magLength];

            for( int i=0; i<this.magnitude.length; i++) {
                this.magnitude[i] = bytesToLong( magnitudeBytes, LONG_BYTE_LENGTH*i);
            }
            
            this.magnitude = Magnitude.reductMagnitude(this.magnitude);
            if( this.magnitude.length == 1 && this.magnitude[0] == 0x0L ) {
                this.sign = 0;
            }
        }
    }
    
    /**
     * Converts the given char array to its LargeInteger value in the given radix.
     * 
     * @param cArray
     * @param radix the base in which the value in cArray is given (E.g.: 16 for hexadecimal, 10 for decimal or 8 for octal).
     */
    public LargeInteger( char[] cArray, int radix ) {
        int offset = findSign( cArray );
        buildFromCharArray( cArray, radix, offset );
    }
    
    /**
     * Converts the given char array to its LargeIneger value.
     * By default, radix is set to 10. If cArray begins with "0x", then radix is set 
     * to 16 (hexadecimal); and if cArray only begins with "0", then the radix is set 
     * to 8 (octal).
     * <br>
     * Examples:
     * <ul>
     * <li> "327" will give the LargeInteger +327</li>
     * <li> "0x147" will give the LargeInteger +327</li>
     * <li> "0507" will give the LargeInteger +327</li>
     * </ul>
     * 
     * @param cArray
     */
    public LargeInteger( char[] cArray ) {
        int offset = findSign( cArray );
        int[] radixOffset = findRadix( cArray, offset );
        buildFromCharArray( cArray, radixOffset[0], radixOffset[1] );
    }
    
    public LargeInteger( String s, int radix ) {
        this(s.replaceAll(" |_", "" ).toLowerCase().toCharArray(), radix);
    }
    
    public LargeInteger( String s ) {
        this(s.replaceAll(" |_", "" ).toLowerCase().toCharArray());
    }
    
    // =========================================================================
    // ========== display ======================================================
    // =========================================================================
    
    public String toHexString() {
        CharBuffer sb = new CharBuffer();
        
        if( sign>0 ) {
            sb.append('+');
        } else if ( sign<0 ) {
            sb.append('-');
        }
        
        for( int i=this.magnitude.length-1; i>-1; i-- ) {
            Chars encodeHexa = Int64.encodeHexa(this.magnitude[i]);
            
            sb.append( encodeHexa );
        }
        
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return this.toHexString();
    }
    
    // =========================================================================
    // ========== getters ======================================================
    // =========================================================================
    
    /**
     * <ul>
     * <li> 1 for positives numbers.</li>
     * <li>0 for zero.</li>
     * <li>-1 for negatives numbers.</li>
     * </ul>
     * @return the sign of this.
     */
    public int getSign() {
        return this.sign;
    }
    
    /**
     * @return a copy of the magnitude.
     */
    public long[] getMagnitude() {
        return Arrays.copy( this.magnitude );
    }
    
    /**
     * Computes the number of bytes needed to encode this's magnitude.
     * 
     * @return the number of bytes needed to encode this's magnitude.
     */
    public int getByteLength() {
        return Magnitude.getByteLength( this.magnitude );
    }
    
    /**
     * Computes the number of bits needed to encode this's magnitude.
     * 
     * @return the number of bits needed to encode this's magnitude.
     */
    public int getBitLength() {
        return Magnitude.getBitLength(this.magnitude);
    }
    
    /**
     * Returns the state of the bit at index bitIndex.
     * 
     * @param bitIndex
     * @return 
     */
    public boolean getBit( int bitIndex ) {
        return Magnitude.getBit( this.sign, this.magnitude, bitIndex);
    }
    
    // =========================================================================
    // ========== Orderable ====================================================
    // =========================================================================
    
    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (!(otherObject instanceof LargeInteger)) {
            return false;
        }
        LargeInteger other = (LargeInteger) otherObject;
        
        return 
                this.sign==other.sign
                &&
                Magnitude.compareMagnitudes( this.magnitude, other.magnitude ) == 0;
    }

    @Override
    public int hashCode() {
        int hash = 511 + this.sign;
        int magHash = 1;
        for (long mag_i : this.magnitude) {
            magHash = 29 * magHash + (int)(mag_i ^ (mag_i >>> 32));
        }
        hash = 73 * hash + magHash;
        
        return hash;
    }
    
    @Override
    public int order(Object otherObject) {
        
        LargeInteger other = (LargeInteger)otherObject;
        
        if( this.sign != other.sign ) {
            return (this.sign > other.sign) ? 1: -1;
        }
        
        return this.sign*Magnitude.compareMagnitudes(this.magnitude, other.magnitude);
    }
    
    // =========================================================================
    // ========== Arithmetic interface methods =================================
    // =========================================================================
    
    /**
     * {@link un.science.math.Arithmetic#add Arithmetic.add}.
     * 
     * @param otherArithmetic a LargeInteger.
     * @return this + otherArithmetic.
     */
    @Override
    public LargeInteger add( Arithmetic otherArithmetic ) {
        
        LargeInteger other = (LargeInteger)otherArithmetic;
        
        if( this.isZero() ) {
            return other;
        } else if( other.isZero() ) {
            return this;
        }
        
        final int cmpMags = Magnitude.compareMagnitudes(this.magnitude, other.magnitude);
        final boolean signTest = this.sign == other.sign;
        
        if( cmpMags==0 && !signTest ) {
            return LargeInteger.ZERO;
        }
        
        final LargeInteger highest, lowest;
        if (Magnitude.compareMagnitudes(this.magnitude, other.magnitude) > 0) {
            highest = this;
            lowest = other;
        } else {
            highest = other;
            lowest = this;
        }
        
        return new LargeInteger( highest.sign, Magnitude.addMagnitudes(highest.magnitude, lowest.magnitude, 0, signTest ) );
    }
    
    /**
     * {@link un.science.math.Arithmetic#subtract Arithmetic.subtract}.
     * 
     * @param otherArithmetic a LargeInteger.
     * @return this - otherArithmetic.
     */
    @Override
    public LargeInteger subtract( Arithmetic otherArithmetic ) {
        
        LargeInteger other = (LargeInteger)otherArithmetic;
        
        return this.add(new LargeInteger( -other.sign, other.magnitude ));
    }
    
    /**
     * {@link un.science.math.Arithmetic#mult Arithmetic.mult}.
     * 
     * @param otherArithmetic a LargeInteger.
     * @return this * otherArithmetic.
     */
    @Override
    public LargeInteger mult( Arithmetic otherArithmetic ) {
        
        LargeInteger other = (LargeInteger)otherArithmetic;
        
        if( this.isZero()|| other.isZero() ) {
            return LargeInteger.ZERO;
        } else if( this.isOne() ) {
            return other;
        } else if( other.isOne() ) {
            return this;
        }
        
        long[] resultMagnitude = Magnitude.multMagnitude(this.magnitude, other.magnitude);
        
        return new LargeInteger( this.sign*other.sign, resultMagnitude );
    }
    
    /**
     * {@link un.science.math.Arithmetic#divide Arithmetic.divide}.
     * 
     * @param otherArithmetic a LargeInteger (different from ZERO).
     * @return this / otherArithmetic.
     */
    @Override
    public LargeInteger divide( Arithmetic otherArithmetic ) {
        
        LargeInteger other = (LargeInteger)otherArithmetic;
        
        return this.divideAndRemainder(other)[0];
    }
    
    /**
     * {@link un.science.math.Arithmetic#zero Arithmetic.zero}.
     * @return 
     */
    @Override
    public LargeInteger zero() {
        return LargeInteger.ZERO;
    }
    
    /**
     * {@link un.science.math.Arithmetic#isZero Arithmetic.isZero}.
     * @return 
     */
    @Override
    public boolean isZero() {
        return this.sign == 0;
    }
    
    /**
     * {@link un.science.math.Arithmetic#one Arithmetic.one}.
     * @return 
     */
    @Override
    public LargeInteger one() {
        return LargeInteger.ONE;
    }
    
    /**
     * {@link un.science.math.Arithmetic#isOne Arithmetic.isOne}.
     * @return 
     */
    @Override
    public boolean isOne() {
        return this.magnitude.length == 1 && this.sign == 1 && this.magnitude[0] == 1L;
    }
    
    /**
     * {@link un.science.math.Arithmetic#pow Arithmetic.pow}.
     * @param n
     * @return 
     */
    @Override
    public LargeInteger pow( int n ) {
        
        return this.pow( new LargeInteger( n ));
    }
    
    // =========================================================================
    // ========== bitwise operators ============================================
    // =========================================================================
    
    /**
     * 
     * @return ~this.
     */
    public LargeInteger not() {
        return this.add(ONE).negate();
    }
    
    /**
     * 
     * @param other
     * @return this and other.
     */
    public LargeInteger and( LargeInteger other ) {
        
        long[][] init = initiateBitwiseOperator( other );
        long[] thisMag = init[0], otherMag = init[1];
        
        long[] resultMag = Magnitude.and(thisMag, otherMag);
        int resultSign = this.sign & other.sign;
        
        return finishBitwiseOperator( resultMag, resultSign );
    }
    
    /**
     * 
     * @param other
     * @return this and ~other.
     */
    public LargeInteger andNot( LargeInteger other ) {
        
        long[][] init = initiateBitwiseOperator( other );
        long[] thisMag = init[0], otherMag = init[1];
        
        long[] resultMag = Magnitude.andNot(thisMag, otherMag);
        int resultSign;
        if( this.sign==-1 && other.sign!=-1 ) {
            resultSign = -1;
        } else {
            resultSign = 1;
        }
        
        return finishBitwiseOperator( resultMag, resultSign );
    }
    
    /**
     * 
     * @param other
     * @return this | other.
     */
    public LargeInteger or( LargeInteger other ) {
        
        long[][] init = initiateBitwiseOperator( other );
        long[] thisMag = init[0], otherMag = init[1];
        
        long[] resultMag = Magnitude.or(thisMag, otherMag);
        int resultSign = this.sign | other.sign;
        
        return finishBitwiseOperator( resultMag, resultSign );
    }
    
    /**
     * 
     * @param other
     * @return this ^^other.
     */
    public LargeInteger xor( LargeInteger other ) {
        
        long[][] init = initiateBitwiseOperator( other );
        long[] thisMag = init[0], otherMag = init[1];
        
        long[] resultMag = Magnitude.xor(thisMag, otherMag);
        
        int resultSign;// = this.sign | other.sign;
        if( this.sign==-1 ^ other.sign==-1 ) {
            resultSign = -1;
        } else {
            resultSign = 1;
        }
        
        return finishBitwiseOperator( resultMag, resultSign );
    }
    
    /**
     * Right shift of bitShift bits (equivalent of this >> bitShift).
     * 
     * @param bitShift
     * @return this >> bitShift.
     */
    public LargeInteger shiftRight( int bitShift ) {
        
        long[] resultMagnitude = Magnitude.shiftRight( this.magnitude, bitShift );
        
        if( this.sign < 0 ) {
            resultMagnitude = Magnitude.addMagnitudes(resultMagnitude, new long[]{ 0x1L }, 0, true);
        }
                
        return new LargeInteger( this.sign, resultMagnitude );
    }
    
    /**
     * Left shift of bitShift bits (equivalent of this << bitShift).
     * 
     * @param bitShift
     * @return this << bitShift.
     */
    public LargeInteger shiftLeft( int bitShift ) {
        
        long[] resultMagnitude = Magnitude.shiftLeft( this.magnitude, bitShift );
                
        return new LargeInteger( this.sign, resultMagnitude );
    }
    
    // =========================================================================
    // ========== to primitive types ===========================================
    // =========================================================================

    public int toInteger() {
        return (int) toLong();
    }

    /**
     * Convert this to it's long value.
     * 
     * @return (long)this.
     */
    public long toLong() {

        if ( this.sign == 1 && Magnitude.compareMagnitudes(this.magnitude, MAX_LONG_MAGNITUDE) >= 0) {
                return MAX_LONG_VALUE;
        }
        if ( this.sign == -1 && Magnitude.compareMagnitudes(this.magnitude, MIN_LONG_MAGNITUDE) >= 0) {
                return MIN_LONG_VALUE;
        }

        if (this.sign < 0) {
            return -this.magnitude[0];
        } else {
            return this.magnitude[0];
        }
    }
    
    /**
     * Convert this to it's float value.
     * 
     * @return (float)this.
     */
    public float toFloat() {
        
        float result = this.magnitude[this.magnitude.length-1] >>> INT_BITS_LENGTH;
        result = result * TWO_POW_32 + (this.magnitude[this.magnitude.length-1] & LOW_INT_MASK);
        
        for( int i=this.magnitude.length-2; i>-1; i-- ) {
            result = result * TWO_POW_32 + (this.magnitude[i] >>> INT_BITS_LENGTH);
            result = result * TWO_POW_32 + (this.magnitude[i] & LOW_INT_MASK);
        }
        
        if( this.sign < 0 ) {
            result *= -1.f;
        }
        
        return result;
    }
    
    /**
     * Convert this to it's float value.
     * 
     * @return (double)this.
     */
    public double toDouble() {
        
        double result = this.magnitude[this.magnitude.length-1] >>> INT_BITS_LENGTH;
        result = result * TWO_POW_32 + (this.magnitude[this.magnitude.length-1] & LOW_INT_MASK);
        
        for( int i=this.magnitude.length-2; i>-1; i-- ) {
            result = result * TWO_POW_32 + (this.magnitude[i] >>> INT_BITS_LENGTH);
            result = result * TWO_POW_32 + (this.magnitude[i] & LOW_INT_MASK);
        }
        
        if( this.sign < 0 ) {
            result *= -1.;
        }
        
        return result;
    }
    
    // =========================================================================
    // ========== Other methods ================================================
    // =========================================================================
    
    /**
     * Divides this by other and returns the quotient and the remainder.
     * <ul>
     * <li>quotient = this / other</li>
     * <li>remainder = this % other</li>
     * </ul>
     * 
     * @param other
     * @return { quotient, remainder }
     */
    public LargeInteger[] divideAndRemainder( LargeInteger other ) {
        
        if( other.isZero() ) {
            throw new InvalidArgumentException( "Division by 0 exception." );
        } 
        
        LargeInteger quotient, remainder;
        if( this.isZero() ) {
            quotient = LargeInteger.ZERO;
            remainder = LargeInteger.ZERO;
        } else if ( other.isOne() ) {
            quotient = this;
            remainder = LargeInteger.ZERO;
        } else {
            long[][] resultMagnitudes = Magnitude.divideAndRemainderMagnitude( this.magnitude, other.magnitude );
            int resultQuotientSign = this.sign*other.sign;

            quotient = new LargeInteger( resultQuotientSign, resultMagnitudes[0]);
            remainder = new LargeInteger( this.sign, resultMagnitudes[1]);
        }
        
        return new LargeInteger[]{ quotient, remainder };
    }
    
    /**
     * Computes this modulus other.
     * 
     * @param other
     * @return this % other.
     */
    public LargeInteger mod( LargeInteger other ) {
        return this.divideAndRemainder(other)[1];
    }
    
    /**
     * Negates this.
     * 
     * @return -this.
     */
    public LargeInteger negate() {
        return new LargeInteger( -this.sign, this.magnitude );
    }
    
    public LargeInteger abs() {
        if( this.sign<0) {
            return new LargeInteger( -this.sign, this.magnitude );
        } else {
            return this;
        }
    }
    
    /**
     * Computes and returns this raised to the power exponent.
     * 
     * @param exponent
     * @return this^n
     */
    public LargeInteger pow( LargeInteger exponent ) {
        
        final int cmpToZero = exponent.order( LargeInteger.ZERO );
        
        if ( cmpToZero<0 ) {
            return LargeInteger.ZERO;
        } else if( cmpToZero==0 ) {
            return LargeInteger.ONE;
        }
        
        if( this.isZero() ) {
            return LargeInteger.ZERO;
        } else if( this.isOne() ) {
            return LargeInteger.ONE;
        }
        
        LargeInteger result = LargeInteger.ONE;
        LargeInteger tempo = new LargeInteger( this );
        
        for( long m : exponent.magnitude ) {
            while( m != 0 ) {
                if( (m & 1) == 1 ) {
                    result = result.mult(tempo);
                }
                tempo = tempo.mult(tempo);
                m = m >>> 1;
            }
        }
        
        return result;
    }

    /**
     * Computes and returns this raised to the power exponent, modulo modulus.
     * 
     * @param exponent
     * @param modulus
     * @return this^n modulo modulus
     */
    public LargeInteger modPow( LargeInteger exponent, LargeInteger modulus ) {
        
        final int cmpToZero = exponent.order( LargeInteger.ZERO );
        
        if ( cmpToZero<0 ) {
            throw new RuntimeException("modPow is not yet implemented for exponent < 0."); // <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<<
        } else if( cmpToZero==0 ) {
            return LargeInteger.ONE;
        }
        
        if( this.isZero() ) {
            return LargeInteger.ZERO;
        } else if( this.isOne() ) {
            return LargeInteger.ONE;
        }
        
        LargeInteger result = LargeInteger.ONE;
        LargeInteger tempo = new LargeInteger( this ).mod(modulus);
        
        for( long m : exponent.magnitude ) {
            while( m != 0 ) {
                if( (m & 1) == 1 ) {
                    result = result.mult(tempo).mod(modulus);
                }
                tempo = tempo.mult(tempo).mod(modulus);
                m = m >>> 1;
            }
        }
        
        return result;
    }
    
    /**
     * Computes a*b mod m
     * Assume that a and b are both numbers in montgomery world.
     */
    private static long[] montgomeryStep(long[] magA, long[] magB, long[] magM, int r) {
        long[] magResult = new long[1];
        
        int j = 0;
        while(r > 0) {
            if(Magnitude.getBit(1, magA, j)) {
                magResult = Magnitude.addMagnitudes(magResult, magB, 0, true);
            }

            if((magResult[0] & 0x01L) == 1) {
                magResult = Magnitude.addMagnitudes(magResult, magM, 0, true);
            }

            ++j;
            --r;
            magResult = fastShr(magResult);
        }

        final int cmpMags = Magnitude.compareMagnitudes(magResult, magM);

        if(cmpMags == 0 || cmpMags == 1) {
            magResult = Magnitude.addMagnitudes(magResult, magM, 0, false);
        }
        
        return magResult;
    }
    
    private static long[] fastShr(long[] mag) {
        long lastBit = 0;
        long tmp;
        for(int i = mag.length - 1; i >= 0; --i) {
            tmp = mag[i] & 0x01L;
            mag[i] >>>= 1;
            mag[i] |= (lastBit == 1 ? 0x8000000000000000L : 0);
            lastBit = tmp;
        }
        
        return Magnitude.reductMagnitude(mag);
    }

    /** Perform fast modular exponentiation using Montgomery reduction.
     *  source: https://en.wikipedia.org/wiki/Montgomery_modular_multiplication
     * 
     * @param exponent
     * @param modulus
     * @return this^exponent % modulus
     */
    public LargeInteger modPowFast(LargeInteger exponent, LargeInteger modulus) {
        
        final int cmpToZero = exponent.order( LargeInteger.ZERO );
        
        if ( cmpToZero<0 ) {
            throw new RuntimeException("modPow is not yet implemented for exponent < 0."); // <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<< TODO <<<<<<<<<<
        } else if( cmpToZero==0 ) {
            return LargeInteger.ONE;
        }

        if( this.isZero() ) {
            return LargeInteger.ZERO;
        } else if( this.isOne() ) {
            return LargeInteger.ONE;
        }
        
        final int r = 8 * modulus.getByteLength();
        
        long[] magModulus = modulus.magnitude;
        
        // montA = (a << r) % modulus
        long[] magMontA = Arrays.copy(this.magnitude);
        magMontA = Magnitude.divideAndRemainderMagnitude(Magnitude.shiftLeft(magMontA, r), magModulus)[1];
                
        long[] tmp = null;
        int j = 0;
        int n = exponent.getBitLength();
        while(j <= n) {
            if(exponent.getBit(j)) {
                if(tmp != null) {
                    tmp = montgomeryStep(tmp, magMontA, magModulus, r);
                } else {
                    tmp = Arrays.copy(magMontA);
                }
            }
            
          ++j;
          if(j <= n)
              magMontA = montgomeryStep(magMontA, magMontA, magModulus, r);
        }

        return new LargeInteger(1, montgomeryStep(tmp, new long[] { 0x01}, magModulus, r));
    }
    
    /**
     * Computes n such that this*n == 1 mod modulus. If n doesn't exists, returns null.
     * @param modulus
     * @return n such that this*n == 1 mod modulus. If n doesn't exists, returns null.
     */
    public LargeInteger modInverse( LargeInteger modulus ) {
        
        LargeInteger[] extEuclid = this.extendedEuclid ( modulus );
        
        if( extEuclid[2].isOne() ) {
            return extEuclid[1];
        } else {
            return null;
        }
        
    }
    
    /**
     * Computes floor( sqrt( this ) ) using the Babylonian method. 
     * https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Babylonian_method
     * 
     * @return floor( sqrt( this ) )
     */
    public LargeInteger floorSqrt() {
        
        if( this.sign < 0 ) {
            throw new InvalidArgumentException("this must be positive LargeInteger.");
        } else if ( this.isZero() ) {
            return LargeInteger.ZERO;
        } else if ( this.isOne() ) {
            return LargeInteger.ONE;
        }
        
        // magnitude one:
        final long[] magONE = new long[]{ 0x1L };
        final long[] magTWO = new long[]{ 0x2L };
        
        // estimate result magnitude:
        long[] resultMag = new long[this.magnitude.length/2 + 1];
        resultMag[resultMag.length-1] = 1;
        
        // resultSquare = result*result
        long[] resultMagSquare = Magnitude.multMagnitude(resultMag, resultMag);
        // resultPlusOneSquare = (result+1) * (result+1) = resultSquare + 2*result + 1
        long[] resultMagPlusOneSquare = Magnitude.addMagnitudes(resultMagSquare, 
                Magnitude.addMagnitudes(Magnitude.shiftLeft(resultMag, 1), magONE, 0, true),
                0,
                true );
        
        while( ! (
                Magnitude.compareMagnitudes(this.magnitude, resultMagSquare)>0
                &&
                Magnitude.compareMagnitudes(this.magnitude, resultMagPlusOneSquare)<0 ) ){
            
            // result = ( result + this/result ) / 2
            resultMag = Magnitude.divide(Magnitude.addMagnitudes(resultMag, Magnitude.divide(this.magnitude, resultMag), 0, true), 
                    magTWO
            );
            
            // resultSquare = result*result
            resultMagSquare = Magnitude.multMagnitude(resultMag, resultMag);
            // resultPlusOneSquare = (result+1) * (result+1) = resultSquare + 2*result + 1
            resultMagPlusOneSquare = Magnitude.addMagnitudes(resultMagSquare, 
                Magnitude.addMagnitudes(Magnitude.shiftLeft(resultMag, 1), magONE, 0, true),
                0,
                true );
        }
        
        return new LargeInteger( 1, resultMag );
    }
    
    /**
     * Computes a, b and gcd(this,other) such that 
     * a*max(this,other) + b*min(this,other) = gcd(this,other)
     * using extended Euclid algorithm.
     * 
     * this and other must be positive LargeIntegers.
     * 
     * @param other a positive LargeIntegers.
     * @return { a, b, gcd(this, other) } such that a*max(this,other) + b*min(x,other) = gcd(this,other).
     */
    public LargeInteger[] extendedEuclid ( LargeInteger other ) {
        
        LargeInteger q, ra, rb, r, sa, sb, s, ta, tb, t;
        
        // Inititalisation
        r = LargeInteger.ONE;
        if( Magnitude.compareMagnitudes(this.magnitude, other.magnitude) >= 0 ) {
            ra = this;
            rb = other;
        } else {
            ra = other;
            rb = this;
        }
        sa = LargeInteger.ONE;
        sb = LargeInteger.ZERO;
        ta = LargeInteger.ZERO;
        tb = LargeInteger.ONE;
        
        while( !r.isZero() ) {
            // ***** process loop *****
            q = ra.divide(rb);
            r = ra.subtract( q.mult(rb) );
            s = sa.subtract( q.mult(sb) );
            t = ta.subtract( q.mult(tb) );
            
            // ***** prepare next loop *****
            ra = rb;
            rb = r;
            sa = sb;
            sb = s;
            ta = tb;
            tb = t;
        }
        
        return new LargeInteger[]{ sa, ta, ra };
    }
    
    /**
     * The gcd of this and other is the largest positive integer that 
     * divides those numbers without remainder.
     * <br>
     * https://en.wikipedia.org/wiki/Greatest_common_divisor
     * 
     * @param other a positive LargeIntegers.
     * @return the largest positive integer that divides this and other without a remainder.
     */
    public LargeInteger gcd( LargeInteger other ) {
        return extendedEuclid ( other )[2];
    }
    
    /**
     * Computes n such that n is the higher integer such that this can be 
     * written as LargeInteger*2.pow(n).(n is the count of 0 bits beginning mag)<br>
     * (Used in simplifications in LargeDecimal)
     * 
     * @return n the count of 0 bits beginning the magnitude.
     */
    public int simplify() {
        return Magnitude.simplifyMagnitude(this.magnitude);
    }
    
    // =========================================================================
    // ========== helper functions =============================================
    // =========================================================================
    
    /**
     * E.g.:
     * bytesToLong( { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b }, 4 ) = 0x0b0a090807060504L
     * bytesToLong( { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b }, 8 ) = 0x00000000000b0a09L
     * 
     * @param array a byte array (LSB at index 0 and MSB at last index).
     * @param offset where to begin to long transform.
     * @return 
     */
    private long bytesToLong( byte[] array, int offset ) {
        long result = 0;
        for( int i=8; i>-1; i-- ) {
            if( offset+i < array.length ) {
                result <<= 8;
                result &= 0xffffffffffffff00L;
                result |= (long) (array[offset+i]) & 0xff;
            }
        }
        return result;
    }
    
    /**
     * Gives the integer value of the given char.
     * E.g.:
     * <ul>
     * <li>'0' ---> 0</li>
     * <li>'1' ---> 1</li>
     * <li>...</li>
     * <li>'f' ---> 15</li>
     * <li></li>
     * </ul>
     * @param c
     * @return 
     */
    private int getCharValue( char c ) {
        if( c>='0' && c<='9' ) {
            return c-'0';
        } else {
            return c-'a'+10;
        }
    }
    
    /**
     * Determine the sign of the given value if the char array.
     * 
     * @param cArray
     * @return the offset where to continue processing in cArray.
     */
    private int findSign( char[] cArray ) {
        this.sign = 1;
        if( cArray[0] == '-' ) {
            this.sign = -1;
            return 1; // offset = 1
        } else if( cArray[0] == '+' ) {
            return 1; // offset = 1
        }
        return 0; // offset = 0
    }
    
    /**
     * Determine if the radix is 8, 10 or 16.
     * 
     * @param cArray
     * @param offset where to begin processing in cArray.
     * @return { radix, offset }
     */
    private int[] findRadix( char[] cArray, int offset ) {
        
        int radix = 10;
        
        if ( cArray.length > 1 && cArray[0] == '0' ) {
            if( cArray[1] == 'x' ) {
                // hexadecimal
                radix = 16;
                offset += 2;
            } else {
                // octal
                radix = 8;
                offset += 1;
            }
        }
        
        return new int[]{ radix, offset };
    }
    
    /**
     * Determine the value of the magnitude given in the char array from 
     * the given offset.
     * If the result magnitude's value is zero, then then sign is set to 0.
     * 
     * @param cArray
     * @param radix 
     * @param offset where to begin processing in cArray.
     */
    private void buildFromCharArray( char[] cArray, int radix, int offset ) {
        
        LargeInteger base = new LargeInteger( radix );
        LargeInteger result = LargeInteger.ZERO;
        
        for( ; offset<cArray.length; offset++ ) {
            int unit = getCharValue( cArray[offset] );
            result = result.mult(base).add(new LargeInteger( unit ));
        }
        
        this.magnitude = Magnitude.reductMagnitude(result.magnitude);
        if( this.magnitude.length==1 && this.magnitude[0]==0) {
            this.sign = 0;
        }
    }
    
    /**
     * Function used to avoid code duplication in bitwise operators.
     * 
     * @param other
     * @return { thisMag, otherMag }
     */
    private long[][] initiateBitwiseOperator( LargeInteger other ) {
        
        int maxLength = Maths.max( this.magnitude.length, other.magnitude.length );
        long[] thisMag, otherMag;
        
        if( this.sign < 0 ) {
            thisMag = Magnitude.toNegative(this.magnitude, maxLength);
        } else {
            if( this.magnitude.length == maxLength ) {
                thisMag = this.magnitude;
            } else {
                thisMag = Arrays.copy(this.magnitude, 0, maxLength);
            }
        }
        
        if( other.sign < 0 ) {
            otherMag = Magnitude.toNegative(other.magnitude, maxLength);
        } else {
            if( other.magnitude.length == maxLength ) {
                otherMag = other.magnitude;
            } else {
                otherMag = Arrays.copy(other.magnitude, 0, maxLength);
            }
        }
        
        return new long[][]{ thisMag, otherMag };
    }
    
    /**
     * Function used to avoid code duplication in bitwise operators.
     * 
     * @param resultMag
     * @param resultSign
     * @return result LargeInteger.
     */
    private LargeInteger finishBitwiseOperator( long[] resultMag, int resultSign ) {
        
        if ( resultSign < 0 ) {
            resultMag = Magnitude.toNegative(resultMag, resultMag.length);
        }
        return new LargeInteger( resultSign, resultMag );
    }
    
}
