
package science.unlicense.impl.math;

/**
 * Structure d'une equation lineaire
 * 
 *  coef[0]*X0 +  coef[1]*X1 + ... + coef[n]*Xn = residu
 *  
 *  ou X=(X0,X1,...,Xn) est le vecteur "inconnu"
 *
 * @author Xavier PHILIPPEAU
 */
public class LinearEquation {

    /**
     * Les coefficients de l'equation
     */
    public double[] coefficents;
    /**
     * Le résidu (= le résultat) de l'equation
     */
    public double residual;

    /**
     * Constructeur
     *
     * @param unknownsCount Le nombre d'inconnues
     */
    public LinearEquation(int unknownsCount) {
        this.coefficents = new double[unknownsCount];
    }

    /**
     * Constructeur
     *
     * @param coefficents Les coefficients de l'equation
     * @param residual Le résidu (= le résultat) de l'equation
     */
    public LinearEquation(double[] coefficents, double residual) {
        this.coefficents = coefficents;
        this.residual = residual;
    }
    
}