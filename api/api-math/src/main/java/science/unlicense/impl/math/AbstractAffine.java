
package science.unlicense.impl.math;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.math.transform.AbstractTransform;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractAffine extends AbstractTransform implements Affine {

    @Override
    public double[] getCol(int col) {
        final double[] array = new double[getInputDimensions()];
        for (int i=0;i<array.length;i++) array[i] = get(i,col);
        return array;
    }

    @Override
    public double[] getRow(int row) {
        final double[] array = new double[getInputDimensions()+1];
        for (int i=0;i<array.length;i++) array[i] = get(row, i);
        return array;
    }

    @Override
    public boolean isIdentity() {
        final int size = getInputDimensions();

        for( int x=0; x<=size; x++) {
            for( int y=0; y<size; y++) {
                if(x==y){
                    if( get(y,x) != 1.0 ) return false;
                }else{
                    if( get(y,x) != 0.0 ) return false;
                }
            }
        }
        return true;
    }

    @Override
    public AffineRW multiply(Affine affine) {
        final MatrixRW res = this.toMatrix().multiply(affine.toMatrix());
        final AffineRW affr = DefaultAffine.create(this.getInputDimensions());
        affr.fromMatrix(res);
        return affr;
    }

    @Override
    public AffineRW invert() {
        final AffineRW affine = DefaultAffine.create(this);
        affine.localInvert();
        return affine;
    }

    @Override
    public AffineRW invert(AffineRW buffer) {
        if (buffer==null) buffer = DefaultAffine.create(getInputDimensions());
        buffer.fromMatrix(toMatrix().invert());
        return buffer;
    }

    @Override
    public Chars toChars() {
        return CObjects.toChars(toMatrix().getMatrix(0, getInputDimensions()-1, 0, getInputDimensions()));
    }

    public static abstract class RW extends AbstractAffine implements AffineRW {


        public void set(final Affine toCopy) {
            final int dim = toCopy.getInputDimensions();
            for(int x=0;x<=dim;x++) {
                for(int y=0;y<dim;y++) {
                    set(y, x, toCopy.get(y, x));
                }
            }
        }

        public void setRow(int row, double[] values) {
            for(int x=0;x<values.length;x++) {
                set(row, x, values[x]);
            }
            
        }

        public void setCol(int col, double[] values) {
            for(int y=0;y<values.length;y++) {
                set(y, col, values[y]);
            }
        }

        /**
         * Set affine values to identity transform.
         *
         * @return this affine
         */
        public AffineRW setToIdentity() {
            final int dim = getInputDimensions();
            for(int x=0;x<=dim;x++) {
                for(int y=0;y<dim;y++) {
                    set(y, x, x==y ? 1:0);
                }
            }
            return this;
        }

        @Override
        public AffineRW localScale(double scale) {
            fromMatrix(this.toMatrix().localScale(scale));
            return this;
        }

        @Override
        public AffineRW localScale(double[] tuple) {
            fromMatrix(this.toMatrix().localScale(tuple));
            return this;
        }

        public void localMultiply(Affine affine) {
            fromMatrix(this.toMatrix().multiply(affine.toMatrix()));
        }

        @Override
        public AffineRW localInvert() {
            fromMatrix(toMatrix().invert());
            return this;
        }

    }

}
