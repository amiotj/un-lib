
package science.unlicense.impl.number;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.number.Arithmetic;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Matrix2x2;

/**
 * Complex number.
 * http://en.wikipedia.org/wiki/Complex_number
 * 
 * @author Bertrand COTE
 */
public class Complex implements Arithmetic {
    
    /**
     * Margin of error on isZero and isOne methods
     */
    private static double epsilon;
    private static boolean epsilonIsSet;
    static {
        Complex.resetEpsilon();
    }
    
    private final double re, im;
    
    public Complex() {
        this.re = 0.;
        this.im = 0.;
    }
    
    public Complex( double re, double im ) {
        this.re = re;
        this.im = im;
    }
    
    /**
     * @return the real part.
     */
    public double getRe() {
        return re;
    }

    /**
     * @return the imaginary part.
     */
    public double getIm() {
        return im;
    }
    
    /**
     * The absolute value (or the modulus, or magnitude).
     * @return the absolute value
     */
    public double abs() {
        return Math.sqrt(this.re*this.re + this.im*this.im);
    }
    
    /**
     * The argument (or phase). (Range from -pi to pi)
     * Indeterminate if this.re and this.im are nulls.
     * @return the phase
     */
    public double argument() {
        if( this.re==0. && this.im==0. ) {
            throw new ArithmeticException("Argument indeterminate: real part and imaginary part are both zero.");
        }
        return Math.atan2( this.im, this.re);
    }
    
    /**
     * 
     * @return the conjugate of this.
     */
    public Complex conjugate() {
        return new Complex( this.re, 
                            -this.im );
    }
    
    /**
     * Reciprocal = 1/z
     * @return 1/this
     */
    public Complex reciprocal() {
        double denominator = this.re*this.re + this.im*this.im;
        if (denominator==0.) {
            throw new ArithmeticException("reciprocal indeterminate: real part and imaginary part are both zero.");
        }
        return new Complex( this.re/denominator, 
                            -this.im/denominator);
    }
    
    @Override
    public Complex add( Arithmetic other ) {
        Complex otherComplex = (Complex)other;
        return new Complex( this.re+otherComplex.re, 
                            this.im+otherComplex.im );
    }
    
    public Complex add( double real ) {
        return new Complex( this.re+real, 
                            this.im );
    }
    
    @Override
    public Complex subtract( Arithmetic other ) {
        Complex otherComplex = (Complex)other;
        return new Complex( this.re-otherComplex.re, 
                            this.im-otherComplex.im );
    }
    
    public Complex subtract( double real ) {
        return new Complex( this.re-real, this.im );
    }
    
    @Override
    public Complex mult( Arithmetic other ) {
        Complex otherComplex = (Complex)other;
        return new Complex( this.re*otherComplex.re-this.im*otherComplex.im, 
                            this.im*otherComplex.re+this.re*otherComplex.im );
    }
    
    public Complex mult( double real ) {
        return new Complex( this.re*real, 
                            this.im*real );
    }
    
    @Override
    public Complex divide( Arithmetic other ) {
        if( other.isZero() ) {
            throw new InvalidArgumentException("divide by zero: other is zero.");
        }
        Complex otherComplex = (Complex)other;
        double denominator = otherComplex.re*otherComplex.re + otherComplex.im*otherComplex.im;
        return new Complex( (this.re*otherComplex.re + this.im*otherComplex.im)/denominator,
                            (this.im*otherComplex.re - this.re*otherComplex.im)/denominator );
    }
    
    public Complex divide( double real ) {
        return new Complex( this.re/real,
                            this.im/real );
    }
    
    /**
     * calculate the square root with non-negative real part which is called the 
     * principal square root.
     * 
     * @return the square root with non-negative real part(principal square root)
     */
    public Complex sqrt() {
        double delta = Math.sqrt((this.re+this.abs())/2.);
        double gamma;
        if( this.im<0.) {
            gamma = -Math.sqrt((-this.re+this.abs())/2.);
        } else {
            gamma = Math.sqrt((-this.re+this.abs())/2.);
        }
        return new Complex(delta, gamma);
    }
    
    public DefaultMatrix toMatrix() {
        return new Matrix2x2(this.re, this.im, -this.im, this.re);
    }
    
    @Override
    public Complex zero() {
        return new Complex();
    }
    
    @Override
    public boolean isZero() { // Both re and im must be zero.
        if( Complex.epsilonIsSet ) {
            return     (this.re>-Complex.epsilon && this.re<Complex.epsilon) 
                    && (this.im>-Complex.epsilon && this.im<Complex.epsilon);
        } else {
            return (this.re==0.) && (this.im==0.);
        }
    }
    
    @Override
    public Complex one() {
        return new Complex(1., 0.);
    }
    
    @Override
    public boolean isOne() { // re must be one and im must be zero
        if( Complex.epsilonIsSet ) {
            return     (this.re>1.-Complex.epsilon && this.re<1.+Complex.epsilon) 
                    && (this.im>  -Complex.epsilon && this.im<   Complex.epsilon);
        } else {
            return (this.re==1.) && (this.im==0.);
        }
    }
    
    /**
     * {@inheritDoc }
     */
    @Override
    public Complex pow( int n ) {
        
        if( n==0 ) {
            return this.one();
        }
        
        boolean negative = n<0;
        if( negative ) n = -n;
        
        if( this.isZero() ) {
            if( negative ) {
                throw new InvalidArgumentException( "Divide by zero exception.");
            } else {
                return this.zero();
            }
        }
        
        double argu = n*this.argument();
        double modu = Maths.pow(this.abs(), n);
        
        Complex res = new Complex( modu*Math.cos(argu), modu*Math.sin(argu) );
        
        if( negative ) {
            double denom = res.re*res.re + res.im*res.im;
            return new Complex( res.re/denom, -res.im/denom );
        } else {
            return res;
        } 
    }
    
    public String toString() {
        return "" + this.re +" + i*"+ this.im;
    }
    
    /**
     * Sets the margin of error on isZero and isOne methods
     * @param epsilon tolerance
     */
    public static void setEpsilon( double epsilon ) {
        if( epsilon>=0.) {
            Complex.epsilon = epsilon;
        } else {
            Complex.epsilon = -epsilon;
        }
        Complex.epsilonIsSet = true;
    }
    
    public static void resetEpsilon() {
        Complex.epsilon = 0.;
        Complex.epsilonIsSet = false;
    }
    
}
