
package science.unlicense.impl.math.transform;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.impl.math.AbstractAffine;
import science.unlicense.impl.math.DefaultAffine;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;

/**
 * A node transform is the equivalent of a matrix but storing values
 * in 3 different elements.
 * - rotation matrix
 * - translation vector
 * - scale vector
 *
 * History : 
 * At first the project used a Matrix, but this approach starts to raise problems
 * when node are moved around very often (like billboards), the mathematic inaccuracy makes the
 * objects start to distord over time.
 *
 * A good description of the problem can be found here :
 * http://www.altdevblogaday.com/2012/07/03/matrices-rotation-scale-and-drifting/
 *
 *
 * @author Johann Sorel
 */
public class NodeTransform extends AbstractAffine implements EventSource {

    public static final Chars PROPERTY_MATRIX = new Chars("Matrix");
    
    //keep track of the matrix state
    private final Object lock = new Object();
    private EventManager eventManager;
    private final MatrixRW oldMatrix;
    
    private final int dimension;
    private final MatrixRW rotation;
    private final Vector scale;
    private final Vector translation;

    //store a view of the global matrix of size dimension+1
    //which group rotation,scale and translation
    private boolean dirty = true;
    private boolean inverseDirty = true;
    private final MatrixRW matrix;
    private final MatrixRW inverse;
    private final MatrixRW inverseRotation;

    public NodeTransform(int dimension){
        this.dimension = dimension;
        this.rotation = DefaultMatrix.create(dimension, dimension).setToIdentity();
        this.inverseRotation = rotation.copy();
        this.scale = new Vector(dimension);
        //set scale to 1 by default
        Arrays.fill(scale.getValues(), 1d);
        this.translation = new Vector(dimension);
        this.dirty = false;
        this.matrix = DefaultMatrix.create(dimension+1, dimension+1).setToIdentity();
        this.oldMatrix = this.matrix.copy();
        this.inverse = matrix.copy();
    }

    public NodeTransform(MatrixRW rotation, Vector scale, Vector translation) {
        this.dimension = scale.getSize();
        this.rotation = rotation;
        this.inverseRotation = this.rotation.copy();
        this.scale = scale;
        this.translation = translation;
        this.matrix = DefaultMatrix.create(dimension+1, dimension+1).setToIdentity();
        this.oldMatrix = this.matrix.copy();
        this.inverse = matrix.copy();
    }

    public NodeTransform(Quaternion rotation, Vector scale, Vector translation) {
        this.dimension = scale.getSize();
        this.rotation = rotation.toMatrix3();
        this.inverseRotation = this.rotation.copy();
        this.scale = scale;
        this.translation = translation;
        this.matrix = DefaultMatrix.create(dimension+1, dimension+1).setToIdentity();
        this.oldMatrix = this.matrix.copy();
        this.inverse = matrix.copy();
    }
    
    /**
     * Copy values from given transform.
     * @param trs
     */
    public void set(NodeTransform trs){
        if(rotation.equals(trs.rotation) && scale.equals(trs.scale) && translation.equals(trs.translation)){
            //nothing changes
            return;
        }
        rotation.set(trs.rotation);
        scale.set(trs.scale);
        translation.set(trs.translation);
        notifyChanged();
    }

    /**
     * Set transform from given matrix.
     * Matrix must be orthogonal of size dimension+1.
     * @param trs
     */
    public void set(Matrix trs){
        Matrices.decomposeMatrix(trs, rotation, scale, translation);
        notifyChanged();
    }

    /**
     * Set transform from given matrix.
     * Matrix must be orthogonal of size dimension+1.
     * @param trs
     */
    public void set(Affine trs){
        set(trs.toMatrix());
    }

    /**
     * Set to identity.
     * This method will send a change event if values have changed.
     */
    public void setToIdentity(){
        boolean change = false;
        if(!rotation.isIdentity()){
            change = true;
            rotation.setToIdentity();
        }
        if(!scale.isAll(1.0)){
            change = true;
            scale.setAll(1.0);
        }
        if(!translation.isZero()){
            change = true;
            translation.setToZero();
        }
        
        if(change) notifyChanged();
    }
    
    /**
     * Set this transform to given translation.
     * This will reset rotation and scale values.
     * 
     * This method will send a change event if values have changed.
     */
    public void setToTranslation(double[] trs){
        boolean change = false;
        if(!rotation.isIdentity()){
            change = true;
            rotation.setToIdentity();
        }
        if(!scale.isAll(1.0)){
            change = true;
            scale.setAll(1.0);
        }
        if(!Arrays.equals(trs, translation.getValues())){
            change = true;
            translation.set(trs);
        }
        
        if(change) notifyChanged();
    }

    @Override
    public double get(int row, int col) {
        return asMatrix().get(row, col);
    }
    
    /**
     * Dimension of the transform.
     * @return int
     */
    public int getDimension() {
        return dimension;
    }

    /**
     * Get transform rotation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Matrix
     */
    public MatrixRW getRotation() {
        return rotation;
    }

    /**
     * Get transform scale.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    public Vector getScale() {
        return scale;
    }

    /**
     * Get transform translation.
     * Call notifyChanged after if you modified the values.
     *
     * @return Vector
     */
    public Vector getTranslation() {
        return translation;
    }

    /**
     * Flag to indicate the transform parameters has changed.
     * This is used to recalculate the general matrix when needed.
     */
    public void notifyChanged(){
        dirty=true;
        inverseDirty=true;
        
        if(eventManager!=null && eventManager.hasListeners()){
            //we have listeners, we need to recalculate the transform now
            eventManager.sendPropertyEvent(this, PROPERTY_MATRIX, oldMatrix.copy(), asMatrix().copy());
        }
    }

    /**
     * Get a general matrix view of size : dimension+1
     * This matrix combine rotation, scale and translation
     *
     * [R*S, R*S, R*S, T]
     * [R*S, R*S, R*S, T]
     * [R*S, R*S, R*S, T]
     * [  0,   0,   0, 1]
     *
     * @return Matrix, never null
     */
    public Matrix asMatrix(){
        if(dirty){
            dirty = false;
            //update matrix
            matrix.setToIdentity();
            matrix.set(rotation);
            matrix.localScale(new Vector(scale,1).getValues());
            for(int i=0;i<dimension;i++){
                matrix.set(i, dimension, translation.get(i));
            }
            oldMatrix.set(matrix);
            if(!matrix.isFinite()){
                throw new RuntimeException("Matrix is not finite :\nRotation\n"+rotation+"Scale "+scale+"\nTranslate "+translation);
            }
        }
        return matrix;
    }

    /**
     * Get a general inverse matrix view of size : dimension+1
     * DO NOT MODIFY THIS MATRIX.
     *
     * @return Matrix, never null
     */
    public Matrix inverseAsMatrix(){
        if(dirty){
            asMatrix();
        }
        if(inverseDirty){
            inverseDirty = false;
            rotation.invert(inverseRotation);
            matrix.invert(inverse);
        }
        return inverse;
    }

    /**
     * Create a general matrix of a different dimension.
     * @param outDimension must be superior to this transformation dimension.
     * @return Matrix
     */
    public MatrixRW asMatrix(int outDimension){
        if(outDimension<=this.dimension){
            throw new InvalidArgumentException("Dimension must be superior to "+this.dimension);
        }
        final MatrixRW m = DefaultMatrix.create(outDimension+1, outDimension+1).setToIdentity();
        m.set(rotation);
        final double[] scale = new double[outDimension+1];
        Arrays.fill(scale, 1);
        Arrays.copy(this.scale.getValues(), 0, dimension, scale, 0);
        m.localScale(scale);
        for(int i=0;i<this.dimension;i++){
            m.set(i, outDimension, translation.get(i));
        }

        return m;
    }

    /**
     * {@inheritDoc }
     */
    public int getInputDimensions() {
        return dimension;
    }

    /**
     * {@inheritDoc }
     */
    public int getOutputDimensions() {
        return dimension;
    }

    /**
     * {@inheritDoc }
     */
    public double[] transform(double[] in, int sourceOffset, double[] out, int destOffset, int nbTuple) {
        //we could use this, but it is slower.
        //asMatrix().transform(in, sourceOffset, out, destOffset, nbTuple);
        
        //scale
        Vectors.multiplyRegular(in, out, sourceOffset, destOffset, scale.getValues(), nbTuple);
        //rotate
        rotation.transform(out, destOffset, out, destOffset, nbTuple);
        //translate
        Vectors.addRegular(out, out, destOffset, destOffset, translation.getValues(), nbTuple);
        
        return out;
    }

    /**
     * {@inheritDoc }
     */
    public float[] transform(float[] in, int sourceOffset, float[] out, int destOffset, int nbTuple) {
        //we could use this, but it is slower.
        //asMatrix().transform(source, sourceOffset, dest, destOffset, nbTuple);
        
        //scale
        Vectors.multiplyRegular(in, out, sourceOffset, destOffset, scale.toArrayFloat(), nbTuple);
        //rotate
        rotation.transform(out, destOffset, out, destOffset, nbTuple);
        //translate
        Vectors.addRegular(out, out, destOffset, destOffset, translation.toArrayFloat(), nbTuple);
        
        return out;
    }

        
    /**
     * Inverse transform a single tuple.
     * 
     * @param source tuple array, can not be null.
     * @param dest array, can be null.
     * @return destination tuple.
     */
    public double[] inverseTransform(double[] source, double[] dest){
        if(dest == null){
            dest = new double[getOutputDimensions()];
        }
        inverseTransform(source, 0, dest, 0, 1);
        return dest;
    }
    
    /**
     * Inverse transform N tuples.
     * 
     * @param source tuple array, can not be null.
     * @param sourceOffset index where to start.
     * @param dest array, can not be null.
     * @param destOffset index where start inserting converted values.
     * @param nbTuple number of tuples to transform.
     */
    public void inverseTransform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple){
        inverseAsMatrix();
        //inverse translate
        Vectors.subtractRegular(source, dest, sourceOffset, destOffset, translation.getValues(), nbTuple);
        //inverse rotate
        inverseRotation.transform(dest, destOffset, dest, destOffset, nbTuple);
        //invert scale
        Vectors.divideRegular(dest, dest, destOffset, destOffset, scale.getValues(), nbTuple);
    }
    
    /**
     * Inverse transform a single tuple.
     * 
     * @param source tuple array, can not be null.
     * @param dest array, can be null.
     * @return destination tuple.
     */
    public float[] inverseTransform(float[] source, float[] dest){
        if(dest == null){
            dest = new float[getOutputDimensions()];
        }
        inverseTransform(source, 0, dest, 0, 1);
        return dest;
    }
    
    /**
     * Inverse transform N tuples.
     * 
     * @param source tuple array, can not be null.
     * @param sourceOffset index where to start.
     * @param dest array, can not be null.
     * @param destOffset index where start inserting converted values.
     * @param nbTuple number of tuples to transform.
     */
    public void inverseTransform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple){
        inverseAsMatrix();
        //inverse translate
        Vectors.subtractRegular(source, dest, sourceOffset, destOffset, translation.toArrayFloat(), nbTuple);
        //inverse rotate
        inverseRotation.transform(dest, destOffset, dest, destOffset, nbTuple);
        //invert scale
        Vectors.divideRegular(dest, dest, destOffset, destOffset, scale.toArrayFloat(), nbTuple);
    }
        
    /**
     * Inverse transform a single tuple.
     * 
     * @param source tuple, can not be null.
     * @param dest tuple, can be null.
     * @return destination tuple.
     */
    public Tuple inverseTransform(Tuple source, Tuple dest){
        if(dest == null){
            dest = new DefaultTuple(getOutputDimensions());
        }
        inverseTransform(source.getValues(), dest.getValues());        
        return dest;
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    protected EventManager getEventManager() {
        return getEventManager(true);
    }
    
    protected EventManager getEventManager(boolean create) {
        synchronized (lock){
            if(eventManager==null && create) eventManager = new EventManager();
        }
        return eventManager;
    }

    public void addEventListener(Predicate predicate, EventListener listener) {
        getEventManager().addEventListener(predicate, listener);
    }

    public void removeEventListener(Predicate predicate, EventListener listener) {
        getEventManager().removeEventListener(predicate, listener);
    }
    
    public EventListener[] getListeners(Predicate predicate) {
        final EventManager manager = getEventManager(false);
        if(manager==null) return EventManager.EMPTY_EVENTLISTENER_ARRAY;
        return manager.getListeners(predicate);
    }

    @Override
    public AffineRW invert() {
        final AffineRW affine = DefaultAffine.create(this);
        affine.localInvert();
        return affine;
    }

    @Override
    public AffineRW multiply(Affine other) {
        final MatrixRW res = this.toMatrix().multiply(other.toMatrix());
        final AffineRW trs = DefaultAffine.create(dimension);
        trs.fromMatrix(res);
        return trs;
    }

    @Override
    public MatrixRW toMatrix() {
        return asMatrix().copy();
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if(buffer==null) return toMatrix();
        buffer.set(asMatrix());
        return buffer;
    }

}
