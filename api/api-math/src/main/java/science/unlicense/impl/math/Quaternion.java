
package science.unlicense.impl.math;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;

/**
 * Quaternion object.
 * 
 * Definition : http://en.wikipedia.org/wiki/Quaternion
 * 
 * @author Johann Sorel
 */
public class Quaternion extends DefaultTuple{

    /**
     * Default quaternion with values [0,0,0,1] .
     */
    public Quaternion() {
        super(0,0,0,1);
    }

    /**
     * Quaternion from 4 values.
     * @param x quaternion first value
     * @param y quaternion second value
     * @param z quaternion third value
     * @param w quaternion fourth value
     */
    public Quaternion(double x, double y, double z ,double w) {
        super(x,y,z,w);
    }

    /**
     * Expected an array of size 4.
     * Warning : no copy of the array is made.
     * @param values no copy of the array is made.
     */
    public Quaternion(double[] values) {
        super(values);
    }

    /**
     * Expect an array of size 4.
     * Warning : a copy of the array is made.
     * @param values a copy of the array is made.
     */
    public Quaternion(float[] values) {
        super(values);
    }

    /**
     * Expect an tuple of size 4.
     * @param v quaternion values to copy from
     */
    public Quaternion(Tuple v) {
        super(v);
        if(v.getSize() != 4){
            throw new InvalidArgumentException("Tuple must be of size 4");
        }
    }

    /**
     * Store the normalized values of this queternion in given buffer.
     * @param buffer, can be null
     * @return buffer quaternion
     */
    public Quaternion normalize(Quaternion buffer){
        if(buffer == null) buffer = new Quaternion();
        Quaternions.normalize(values, buffer.values);
        return buffer;
    }

    /**
     * Normalize this quaternion.
     * @return this quaternion
     */
    public Quaternion localNormalize(){
        Quaternions.normalize(values, values);
        return this;
    }

    /**
     * Calculate quaternion norm.
     * @return norm
     */
    public double norm() {
        return Quaternions.length(values);
    }

    /**
     * Calculate quaternion conjugate.
     * @return Quaternion
     */
    public Quaternion conjugate() {
        return new Quaternion(Quaternions.conjugate(values, null));
    }

    /**
     * Add two quaternions.
     * @param other
     * @return Quaternion
     */
    public Quaternion add(Quaternion other) {
        return new Quaternion(Quaternions.add(values, other.values, null));
    }


    /**
     * Multiply two quaternions.
     * @param other
     * @return Quaternion
     */
    public Quaternion multiply(Quaternion other) {
        return new Quaternion(Quaternions.multiplyQuaternion(values, other.values, null));
    }

    /**
     * Multiply two quaternions. store result in this quaternion.
     * @param other
     * @return Quaternion
     */
    public Quaternion localMultiply(Quaternion other) {
        Quaternions.multiplyQuaternion(values, other.values, values);
        return this;
    }


    /**
     * Calculate inverse quaternion.
     * @return Quaternion, never null
     */
    public Quaternion inverse() {
        return new Quaternion(Quaternions.inverse(values, null));
    }

    /**
     * Create a copy of this quaternion.
     * @return Quaternion, never null
     */
    public Quaternion copy() {
        return new Quaternion(this);
    }

    /**
     * Rotate the given vector, store the result in buffer.
     * @param v vector to rotate
     * @param buffer can be null
     * @return rotated vector
     */
    public Vector rotate(Vector v, Vector buffer){
        if(buffer == null) buffer = v.copy();
        Matrix3x3 m = toMatrix3();
        return (Vector) m.transform(v, buffer);
    }

    /**
     * Create an linear interpolated quaternion.
     * A ratio of 0 while return the same values as this quaternion
     * A ratio of 1 while return the same values as other quaternion
     * 
     * @param other not null
     * @param ratio range [0...1]
     * @param buffer can be null
     * @return interpolated quaternion
     */
    public Quaternion lerp(final Quaternion other, final double ratio, Quaternion buffer){
        if(buffer == null) buffer = new Quaternion();
        Quaternions.lerp(values, other.getValues(), ratio, buffer.values);
        return buffer;
    }

    /**
     * Create a spherical interpolated quaternion.
     * A ratio of 0 while return the same values as this quaternion
     * A ratio of 1 while return the same values as other quaternion
     * 
     * @param other not null
     * @param ratio range [0...1]
     * @param buffer can be null
     * @return interpolated quaternion
     */
    public Quaternion slerp(final Quaternion other, final double ratio, Quaternion buffer){
        if(buffer == null) buffer = new Quaternion();
        Quaternions.slerp(values, other.getValues(), ratio, buffer.values);
        return buffer;
    }

    /**
     * Transform quaternion in a rotation matrix 3x3.
     * Source : http://jeux.developpez.com/faq/math/?page=quaternions#Q54
     *
     * @return Matrix3
     */
    public Matrix3x3 toMatrix3() {
        return new Matrix3x3(Quaternions.toMatrix(values, null));
    }

    /**
     * Transform quaternion in a rotation matrix 4x4.
     * @return Matrix4
     */
    public Matrix4x4 toMatrix4() {
        final double[][] matrix = new double[4][4];
        Quaternions.toMatrix(values, matrix);
        matrix[3][3] = 1;
        return new Matrix4x4(matrix);
    }

    /**
     * Convert to euler angles.
     * @return euler angle in radians (heading/yaw , elevation/pitch , bank/roll)
     */
    public Vector toEuler(){
        return toMatrix3().toEuler();
    }

    /**
     * Convert quaternion to axis angle.
     * @param axisBuffer
     * @return angle in radians
     */
    public double toAxisAngle(Tuple axisBuffer){
        final double[] values = Vectors.normalize(this.values);
        return Quaternions.toAxisAngle(values,axisBuffer.getValues());
    }
    
    /**
     * Extract rotation from matrix.
     * http://jeux.developpez.com/faq/math/?page=quaternions#Q55
     *
     * @param matrix
     * @return Quaternion
     */
    public Quaternion fromMatrix(final Matrix matrix){
        final double[][] m = matrix.getValuesCopy();
        final double trace = m[0][0] + m[1][1] + m[2][2] + 1;

        final double s,x,y,z,w;
        if(trace>0){
            s = 0.5 / Math.sqrt(trace);
            x = ( m[2][1] - m[1][2] ) * s;
            y = ( m[0][2] - m[2][0] ) * s;
            z = ( m[1][0] - m[0][1] ) * s;
            w = 0.25 / s;
        } else if ((m[0][0] >= m[1][1]) && (m[0][0] >= m[2][2])) {
            s = Math.sqrt(1.0 + m[0][0] - m[1][1] - m[2][2]) * 2.0;
            x = 0.25 * s;
            y = (m[0][1] + m[1][0]) / s;
            z = (m[0][2] + m[2][0]) / s;
            w = (m[1][2] - m[2][1]) / s;
        } else if (m[1][1] >= m[2][2]) {
            s = Math.sqrt(1.0 - m[0][0] + m[1][1] - m[2][2]) * 2;
            x = (m[0][1] + m[1][0]) / s;
            y = 0.25 * s;
            z = (m[1][2] + m[2][1]) / s;
            w = (m[0][2] - m[2][0]) / s;
        } else {
            s = Math.sqrt(1.0 - m[0][0] - m[1][1] + m[2][2]) * 2;
            x = (m[0][2] + m[2][0]) / s;
            y = (m[1][2] + m[2][1]) / s;
            z = 0.25 * s;
            w = (m[0][1] - m[1][0]) / s;
        }
        setXYZW(x,y,z,w);
        return this;
    }

    /**
     * Set quaternion values from rotation axis and angle.
     * @param axis rotation axis, not null
     * @param angle rotation angle, in radians
     * @return this quaternion
     */
    public Quaternion fromAngle(Tuple axis, double angle){
        Quaternions.fromAngle(axis, angle, values);
        return this;
    }

    /**
     * Set quaternion values from euler angles.
     * 
     * @param euler angles in radians (heading/yaw , elevation/pitch , bank/roll)
     * @return this quaternion
     */
    public Quaternion fromEuler(Vector euler){
        final Matrix3x3 matrix = Matrix3x3.createRotationEuler(euler);
        return fromMatrix(matrix);
    }

    /**
     * Create a new quaternion from given euler angles.
     * 
     * @param euler angles in radians (heading/yaw , elevation/pitch , bank/roll)
     * @return Quaternion, never null
     */
    public static Quaternion createFromEuler(Vector euler){
        final Matrix3x3 matrix = Matrix3x3.createRotationEuler(euler);
        return matrix.toQuaternion();
    }
    
    /**
     * Create a new quaternion from given axis and angle.
     * 
     * @param axis
     * @param angle
     * @return Quaternion, never null
     */
    public static Quaternion createFromAxis(Vector axis, double angle){
        final Quaternion q = new Quaternion();
        return q.fromAngle(axis, angle);
    }

}
