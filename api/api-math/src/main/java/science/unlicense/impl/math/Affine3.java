
package science.unlicense.impl.math;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.math.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class Affine3 extends AbstractAffine.RW {

    private double m00;
    private double m01;
    private double m02;
    private double m03;
    private double m10;
    private double m11;
    private double m12;
    private double m13;
    private double m20;
    private double m21;
    private double m22;
    private double m23;

    public Affine3() {
        this.m00 = 1.0;
        this.m01 = 0.0;
        this.m02 = 0.0;
        this.m03 = 0.0;

        this.m10 = 0.0;
        this.m11 = 1.0;
        this.m12 = 0.0;
        this.m13 = 0.0;

        this.m20 = 0.0;
        this.m21 = 0.0;
        this.m22 = 1.0;
        this.m23 = 0.0;
    }

    public Affine3(
            double m00, double m01, double m02, double m03,
            double m10, double m11, double m12, double m13,
            double m20, double m21, double m22, double m23) {
        this.m00 = m00;
        this.m01 = m01;
        this.m02 = m02;
        this.m03 = m03;

        this.m10 = m10;
        this.m11 = m11;
        this.m12 = m12;
        this.m13 = m13;
        
        this.m20 = m20;
        this.m21 = m21;
        this.m22 = m22;
        this.m23 = m23;

    }

    public Affine3(Affine affine) {
        this.m00 = affine.get(0, 0);
        this.m01 = affine.get(0, 1);
        this.m02 = affine.get(0, 2);
        this.m03 = affine.get(0, 3);
        
        this.m10 = affine.get(1, 0);
        this.m11 = affine.get(1, 1);
        this.m12 = affine.get(1, 2);
        this.m13 = affine.get(1, 3);

        this.m20 = affine.get(2, 0);
        this.m21 = affine.get(2, 1);
        this.m22 = affine.get(2, 2);
        this.m23 = affine.get(2, 3);
    }

    public Affine3(Matrix m) {
        fromMatrix(m);
    }

    public void setM00(double m00) {
        this.m00 = m00;
    }

    public void setM01(double m01) {
        this.m01 = m01;
    }

    public void setM02(double m02) {
        this.m02 = m02;
    }

    public void setM03(double m03) {
        this.m03 = m03;
    }

    public void setM10(double m10) {
        this.m10 = m10;
    }

    public void setM11(double m11) {
        this.m11 = m11;
    }

    public void setM12(double m12) {
        this.m12 = m12;
    }

    public void setM13(double m13) {
        this.m13 = m13;
    }

    public void setM20(double m20) {
        this.m20 = m20;
    }

    public void setM21(double m21) {
        this.m21 = m21;
    }

    public void setM22(double m22) {
        this.m22 = m22;
    }

    public void setM23(double m23) {
        this.m23 = m23;
    }

    public double getM00() {
        return m00;
    }

    public double getM01() {
        return m01;
    }

    public double getM02() {
        return m02;
    }

    public double getM03() {
        return m03;
    }

    public double getM10() {
        return m10;
    }

    public double getM11() {
        return m11;
    }

    public double getM12() {
        return m12;
    }

    public double getM13() {
        return m13;
    }

    public double getM20() {
        return m20;
    }

    public double getM21() {
        return m21;
    }

    public double getM22() {
        return m22;
    }

    public double getM23() {
        return m23;
    }

    @Override
    public int getInputDimensions() {
        return 3;
    }

    @Override
    public int getOutputDimensions() {
        return 3;
    }

    @Override
    public double get(int row, int col) {
        switch(row){
            case 0 : switch(col){case 0:return m00;case 1:return m01;case 2:return m02;case 3:return m03;}
            case 1 : switch(col){case 0:return m10;case 1:return m11;case 2:return m12;case 3:return m13;}
            case 2 : switch(col){case 0:return m20;case 1:return m21;case 2:return m22;case 3:return m23;}
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }

    @Override
    public void set(int row, int col, double value) {
        switch(row){
            case 0 : switch(col){case 0:m00=value;break;case 1:m01=value;break;case 2:m02=value;break;case 3:m03=value;break;} return;
            case 1 : switch(col){case 0:m10=value;break;case 1:m11=value;break;case 2:m12=value;break;case 3:m13=value;break;} return;
            case 2 : switch(col){case 0:m20=value;break;case 1:m21=value;break;case 2:m22=value;break;case 3:m23=value;break;} return;
        }
        throw new InvalidArgumentException("Invalid row/col index "+row+":"+col);
    }
    
    @Override
    public double[] transform(double[] source, int srcOffset, double[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=3) {
            dest[destOffset+off+0] = m00*source[srcOffset+off+0] + m01*source[srcOffset+off+1] + m02*source[srcOffset+off+2] + m03;
            dest[destOffset+off+1] = m10*source[srcOffset+off+0] + m11*source[srcOffset+off+1] + m12*source[srcOffset+off+2] + m13;
            dest[destOffset+off+2] = m20*source[srcOffset+off+0] + m21*source[srcOffset+off+1] + m22*source[srcOffset+off+2] + m23;
        }
        return dest;
    }

    @Override
    public float[] transform(float[] source, int srcOffset, float[] dest, int destOffset, int nbTuple) {
        for (int i=0,off=0;i<nbTuple;i++,off+=3) {
            dest[destOffset+off+0] = (float) (m00*source[srcOffset+off+0] + m01*source[srcOffset+off+1] + m02*source[srcOffset+off+2] + m03);
            dest[destOffset+off+1] = (float) (m10*source[srcOffset+off+0] + m11*source[srcOffset+off+1] + m12*source[srcOffset+off+2] + m13);
            dest[destOffset+off+2] = (float) (m20*source[srcOffset+off+0] + m21*source[srcOffset+off+1] + m22*source[srcOffset+off+2] + m23);
        }
        return dest;
    }

    @Override
    public void fromMatrix(Matrix m) {
        m00 = m.get(0, 0);
        m01 = m.get(0, 1);
        m02 = m.get(0, 2);
        m03 = m.get(0, 3);

        m10 = m.get(1, 0);
        m11 = m.get(1, 1);
        m12 = m.get(1, 2);
        m13 = m.get(1, 3);

        m20 = m.get(2, 0);
        m21 = m.get(2, 1);
        m22 = m.get(2, 2);
        m23 = m.get(2, 3);
    }

    @Override
    public Matrix4x4 toMatrix() {
        return new Matrix4x4(
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                  0,   0,   0,   1);
    }

    @Override
    public MatrixRW toMatrix(MatrixRW buffer) {
        if(buffer==null) return toMatrix();
        buffer.set(0, 0, m00);
        buffer.set(0, 1, m01);
        buffer.set(0, 2, m02);
        buffer.set(0, 3, m03);

        buffer.set(1, 0, m10);
        buffer.set(1, 1, m11);
        buffer.set(1, 2, m12);
        buffer.set(1, 3, m13);
        
        buffer.set(2, 0, m20);
        buffer.set(2, 1, m21);
        buffer.set(2, 2, m22);
        buffer.set(2, 3, m23);

        buffer.set(3, 0, 0);
        buffer.set(3, 1, 0);
        buffer.set(3, 2, 0);
        buffer.set(3, 3, 1);
        return buffer;
    }

    @Override
    public Affine3 invert() {
        return (Affine3) super.invert();
    }

    /**
     * Create a rotation matrix from given angle and axis.
     *
     * http://en.wikipedia.org/wiki/Rotation_matrix
     *
     * @param angle rotation angle in radians
     * @param rotationAxis Tuple 3
     * @param buffer Matrix 4x4
     * @return this affine
     */
    public Affine3 setToRotation(final double angle, final Tuple rotationAxis) {

        final double[][] rot = Matrices.createRotation4(angle, rotationAxis, null);
        m00 = rot[0][0];
        m01 = rot[0][1];
        m02 = rot[0][2];
        m03 = rot[0][3];
        m10 = rot[1][0];
        m11 = rot[1][1];
        m12 = rot[1][2];
        m13 = rot[1][3];
        m20 = rot[2][0];
        m21 = rot[2][1];
        m22 = rot[2][2];
        m23 = rot[2][3];

        return this;
    }

}
