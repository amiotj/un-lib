package science.unlicense.impl.math;

import science.unlicense.api.math.Angles;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.malgebra.SingularValueDecomposition;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.transform.AbstractTransform;
import science.unlicense.impl.math.malgebra.CholeskyDecomposition;
import science.unlicense.impl.math.malgebra.EigenvalueDecomposition;
import science.unlicense.impl.math.malgebra.LUDecomposition;
import science.unlicense.impl.math.malgebra.QRDecomposition;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.math.TupleRW;

/**
 * Abstract writable matrix.
 * 
 * @author Johann Sorel
 * @aurhor Bertrand COTE
 */
public abstract class AbstractMatrix extends AbstractTransform implements MatrixRW{

    protected final int nbRow;
    protected final int nbCol;
    
    /**
     * Create a new abstract matrix of given size.
     * 
     * @param nbrow
     * @param nbcol 
     */
    protected AbstractMatrix(final int nbrow, final int nbcol) {
        this.nbRow = nbrow;
        this.nbCol = nbcol;
    }

    public int getNbRow() {
        return nbRow;
    }

    public int getNbCol() {
        return nbCol;
    }

    public int getInputDimensions() {
        return nbRow;
    }

    public int getOutputDimensions() {
        return nbRow;
    }

    ////////////////////////////////////////////////////////////////////////////
    // get/set matrix values ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public double[][] getValuesCopy() {
        final double[][] C = new double[nbRow][nbCol];
        for (int r = 0; r < nbRow; r++) {
            for (int c = 0; c < nbCol; c++) {
                C[r][c] = get(r,c);
            }
        }
        return C;
    }

    public double[] getRow(int row){
        final double[] rowval = new double[nbCol];
        for(int i=0;i<nbCol;i++) rowval[i] = get(row,i);
        return rowval;
    }

    public double[] getColumn(int col){
        final double[] colval = new double[nbRow];
        for(int i=0;i<nbRow;i++) colval[i] = get(i,col);
        return colval;
    }

    public Vector getColumnTuple(int col){
        return new Vector(getColumn(col));
    }

    public double[] getLastColumn(){
        return getColumn(nbCol-1);
    }

    public Vector getLastColumnTuple(){
        return new Vector(getLastColumn());
    }

    /**
     * Get a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param j0 Initial column index
     * @param j1 Final column index
     * @return A(i0:i1,j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public MatrixRW getMatrix(int i0, int i1, int j0, int j1) {
        final MatrixRW X = DefaultMatrix.create(i1 - i0 + 1, j1 - j0 + 1);
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = j0; j <= j1; j++) {
                    X.set(i-i0,j-j0, get(i,j));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    /**
     * Get a submatrix.
     *
     * @param r Array of row indices.
     * @param c Array of column indices.
     * @return A(r(:),c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public MatrixRW getMatrix(int[] r, int[] c) {
        final DefaultMatrix X = new DefaultMatrix(r.length, c.length);
        final double[][] B = X.values;
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = 0; j < c.length; j++) {
                    B[i][j] = get(r[i],c[j]);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    /**
     * Get a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param c Array of column indices.
     * @return A(i0:i1,c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public MatrixRW getMatrix(int i0, int i1, int[] c) {
        final DefaultMatrix X = new DefaultMatrix(i1 - i0 + 1, c.length);
        final double[][] B = X.values;
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = 0; j < c.length; j++) {
                    B[i-i0][j] = get(i,c[j]);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    /**
     * Get a submatrix.
     *
     * @param r Array of row indices.
     * @param j0 Initial column index
     * @param j1 Final column index
     * @return A(r(:),j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public MatrixRW getMatrix(int[] r, int j0, int j1) {
        final DefaultMatrix X = new DefaultMatrix(r.length, j1 - j0 + 1);
        final double[][] B = X.values;
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = j0; j <= j1; j++) {
                    B[i][j - j0] = get(r[i],j);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
        return X;
    }

    public void set(final Matrix toCopy){
        final int rm = Maths.min(this.nbRow, toCopy.getNbRow());
        final int cm = Maths.min(this.nbCol, toCopy.getNbCol());
        for(int r=0;r<rm;r++){
            for(int c=0;c<cm;c++){
                set(r, c, toCopy.get(r, c));
            }
        }
    }

    public void set(final double[] values){
        for(int i=0,r=0;r<nbRow;r++){
            for(int c=0;c<nbCol;c++,i++){
                set(r, c, values[i]);
            }
        }
    }

    public void set(final double[][] values){
        for(int r=0;r<nbRow;r++){
            for(int c=0;c<nbCol;c++){
                set(r, c, values[r][c]);
            }
        }
    }
    
    public void setRow(int row, double[] values){
        for(int i=0;i<values.length;i++){
            set(row,i,values[i]);
        }
    }
    
    public void setCol(int col, double[] values){
        for(int i=0;i<values.length;i++){
            set(i,col,values[i]);
        }
    }

    /**
     * Set a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param j0 Initial column index
     * @param j1 Final column index
     * @param X A(i0:i1,j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int i0, int i1, int j0, int j1, Matrix X) {
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = j0; j <= j1; j++) {
                    set(i,j, X.get(i - i0, j - j0));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set a submatrix.
     *
     * @param r Array of row indices.
     * @param c Array of column indices.
     * @param X A(r(:),c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int[] r, int[] c, Matrix X) {
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = 0; j < c.length; j++) {
                    set(r[i],c[j], X.get(i, j));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set a submatrix.
     *
     * @param r Array of row indices.
     * @param j0 Initial column index
     * @param j1 Final column index
     * @param X A(r(:),j0:j1)
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int[] r, int j0, int j1, Matrix X) {
        try {
            for (int i = 0; i < r.length; i++) {
                for (int j = j0; j <= j1; j++) {
                    set(r[i],j, X.get(i, j - j0));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set a submatrix.
     *
     * @param i0 Initial row index
     * @param i1 Final row index
     * @param c Array of column indices.
     * @param X A(i0:i1,c(:))
     * @exception ArrayIndexOutOfBoundsException Submatrix indices
     */
    public void setMatrix(int i0, int i1, int[] c, Matrix X) {
        try {
            for (int i = i0; i <= i1; i++) {
                for (int j = 0; j < c.length; j++) {
                    set(i,c[j], X.get(i - i0, j));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }
    }

    /**
     * Set Matrix value to identity matrix.
     * @return this matrix
     */
    public AbstractMatrix setToIdentity(){
        if (nbCol != nbRow) {
            throw new InvalidArgumentException("The m matrix must be a square matrix.");
        }
        for (int x = 0; x < nbCol; x++) {
            for (int y = 0; y < nbRow; y++) {
                if (x == y) {
                    set(y,x, 1.0);
                } else {
                    set(y,x, 0.0);
                }
            }
        }
        return this;
    }

    public boolean isIdentity(){
        if (nbCol != nbRow) return false; // m must be a square matrix
        for( int x=0; x<nbCol; x++) {
            for( int y=0; y<nbRow; y++) {
                if(x==y){
                    if( get(y,x) != 1.0 ) return false;
                }else{
                    if( get(y,x) != 0.0 ) return false;
                }
            }
        }
        return true;
    }

    public boolean isFinite() {
        for( int x=0; x<nbCol; x++) {
            for( int y=0; y<nbRow; y++) {
                double v = get(y,x);
                if(Double.isNaN(v) || Double.isInfinite(v)){
                    return false;
                }
            }
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public MatrixRW add(Matrix other){
        return add(other,null);
    }

    public MatrixRW subtract(Matrix other){
        return subtract(other, null);
    }

    public MatrixRW scale(double scale){
        return scale(scale,null);
    }

    public MatrixRW multiply(Matrix other){
        return multiply(other, null);
    }

    public MatrixRW invert(){
        return invert(null);
    }
    
    public MatrixRW transpose() {
        MatrixRW t = copy();
        for(int x=0; x<nbCol;x++){
            for(int y=0; y<nbRow; y++){
                t.set(x,y, get(y,x));
            }
        }
        return t;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public MatrixRW add(Matrix other, MatrixRW buffer){
        if(buffer == null) buffer = this.copy();
        final double[][] bufferValues = dArray(buffer);
        Matrices.add(dArray(this), dArray(other) , bufferValues);
        buffer.set(bufferValues);
        return buffer;
    }

    public MatrixRW subtract(Matrix other, MatrixRW buffer){
        if(buffer == null) buffer = this.copy();
        final double[][] bufferValues = dArray(buffer);
        Matrices.subtract(dArray(this), dArray(other) , bufferValues);
        buffer.set(bufferValues);
        return buffer;
    }

    public MatrixRW scale(double scale, MatrixRW buffer){
        if(buffer == null) buffer = this.copy();
        final double[][] bufferValues = dArray(buffer);
        Matrices.scale(dArray(this), scale , bufferValues);
        buffer.set(bufferValues);
        return buffer;
    }

    public MatrixRW multiply(Matrix other, MatrixRW buffer){
        if(buffer == null) buffer = new DefaultMatrix(nbRow, other.getNbCol());
        final double[][] bufferValues = dArray(buffer);
        Matrices.multiply(dArray(this), dArray(other) , bufferValues);
        buffer.set(bufferValues);
        return buffer;
    }

    /**
     * invert matrix
     */
    public MatrixRW invert(MatrixRW buffer){
        double[][] bufferValues;
        if(buffer == null){
            buffer = this.copy();
        }
        bufferValues = dArray(buffer);
        
        bufferValues = Matrices.invert(dArray(this),bufferValues);
        if(bufferValues == null){
            throw new InvalidArgumentException("Can not inverse");
        }
        buffer.set(bufferValues);
        return buffer;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods local  //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * invert matrix
     */
    public AbstractMatrix localInvert(){
        final double[][] inverse = Matrices.localInvert(getValuesCopy());
        if(inverse == null){
            throw new InvalidArgumentException("Can not inverse");
        }
        set(inverse);
        return this;
    }

    public AbstractMatrix localAdd(MatrixRW other){
        set(Matrices.localAdd(dArray(this), dArray(other)));
        return this;
    }

    public AbstractMatrix localSubtract(MatrixRW other){
        set(Matrices.localSubtract(dArray(this), dArray(other)));
        return this;
    }

    public AbstractMatrix localScale(double[] tuple){
        set(Matrices.localScale(dArray(this), tuple));
        return this;
    }

    public AbstractMatrix localScale(double scale){
        set(Matrices.localScale(dArray(this), scale));
        return this;
    }

    public AbstractMatrix localMultiply(Matrix other){
        set(Matrices.localMultiply(dArray(this), dArray(other)));
        return this;
    }

    /**
     * Translate the matrix.
     * Add given tuple values in the last matrix column.
     * 
     * @param translation
     */
    public void localTranslate(Tuple translation){
        for(int r=0,n=translation.getSize();r<n;r++){
            set(r,nbCol-1, get(r,nbCol-1) + translation.get(r));
        }
    }

    public double dot(Matrix other){
        return Matrices.dot(dArray(this), dArray(other));
    }

    ////////////////////////////////////////////////////////////////////////////
    // vector/tuple transformation /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public double[] transform(final double[] vector) {
        return Matrices.transform(dArray(this), vector, null);
    }

    public TupleRW transform(final Tuple vector) {
        return Matrices.transform(dArray(this), vector, null);
    }

    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        final int inDim = getInputDimensions();
        final int outDim = getOutputDimensions();
        double[] in = new double[inDim];
        double[] out = new double[outDim];
        for(int n=0,i=sourceOffset,o=destOffset; n<nbTuple; n++,i+=inDim,o+=outDim){
            Arrays.copy(source, i, inDim, in, 0);
            Matrices.transform(dArray(this), in, out);
            Arrays.copy(out, 0, outDim, dest, o);
        }
        return dest;
    }

    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        final int inDim = getInputDimensions();
        final int outDim = getOutputDimensions();
        float[] in = new float[inDim];
        float[] out = new float[outDim];
        for(int n=0,i=sourceOffset,o=destOffset; n<nbTuple; n++,i+=inDim,o+=outDim){
            Arrays.copy(source, i, inDim, in, 0);
            Matrices.transform(dArray(this), in, out);
            Arrays.copy(out, 0, outDim, dest, o);
        }
        return dest;
    }

    public TupleRW transform(final Tuple vector, TupleRW buffer) {
        if(buffer==null) buffer = (TupleRW) vector.copy();
        transform(vector.getValues(),buffer.getValues());
        return buffer;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Advanced algebra
    // Origin : JAMA http://math.nist.gov/javanumerics/jama/ placed in public domain.
    ////////////////////////////////////////////////////////////////////////////

    /**
     * One norm
     * @return maximum column sum.
     */
    public double norm1() {
        double f = 0;
        for (int j = 0; j < nbCol; j++) {
            double s = 0;
            for (int i = 0; i < nbRow; i++) {
                s += Math.abs(get(i,j));
            }
            f = Math.max(f, s);
        }
        return f;
    }

    /**
     * Two norm
     * @return maximum singular value.
     */
    public double norm2() {
        return (new SingularValueDecomposition(this).norm2());
    }

    /**
     * Infinity norm
     * @return maximum row sum.
     */
    public double normInf() {
        double f = 0;
        for (int i = 0; i < nbRow; i++) {
            double s = 0;
            for (int j = 0; j < nbCol; j++) {
                s += Math.abs(get(i,j));
            }
            f = Math.max(f, s);
        }
        return f;
    }

    /**
     * Frobenius norm
     * @return sqrt of sum of squares of all elements.
     */
    public double normF() {
        double f = 0;
        for (int i = 0; i < nbRow; i++) {
            for (int j = 0; j < nbCol; j++) {
                f = Angles.hypot(f, get(i,j));
            }
        }
        return f;
    }

    /**
     * LU Decomposition
     * @return LUDecomposition
     * @see LUDecomposition
     */
    public LUDecomposition lu() {
        return new LUDecomposition(this);
    }

    /**
     * QR Decomposition
     * @return QRDecomposition
     * @see QRDecomposition
     */
    public QRDecomposition qr() {
        return new QRDecomposition(this);
    }

    /**
     * Cholesky Decomposition
     * @return CholeskyDecomposition
     * @see CholeskyDecomposition
     */
    public CholeskyDecomposition chol() {
        return new CholeskyDecomposition(this);
    }

    /**
     * Singular Value Decomposition
     * @return SingularValueDecomposition
     * @see SingularValueDecomposition
     */
    public SingularValueDecomposition svd() {
        return new SingularValueDecomposition(this);
    }

    /**
     * Eigenvalue Decomposition
     * @return EigenvalueDecomposition
     * @see EigenvalueDecomposition
     */
    public EigenvalueDecomposition eig() {
        return new EigenvalueDecomposition(this);
    }

    /**
     * Solve A*X = B
     * @param B right hand side
     * @return solution if A is square, least squares solution otherwise
     */
    public MatrixRW solve(MatrixRW B) {
        return (nbRow == nbCol ? (new LUDecomposition(this)).solve(B)
                : (new QRDecomposition(this)).solve(B));
    }

    /**
     * Solve X*A = B, which is also A'*X' = B'
     * @param B right hand side
     * @return solution if A is square, least squares solution otherwise.
     */
    public MatrixRW solveTranspose(MatrixRW B) {
        return transpose().solve(B.transpose());
    }

    /**
     * Matrix determinant
     * @return determinant
     */
    public double det() {
        return new LUDecomposition(this).det();
    }

    /**
     * Matrix rank
     * @return effective numerical rank, obtained from SVD.
     */
    public int rank() {
        return new SingularValueDecomposition(this).rank();
    }

    /**
     * Matrix condition (2 norm)
     *
     * @return ratio of largest to smallest singular value.
     */
    public double cond() {
        return new SingularValueDecomposition(this).cond();
    }

    /**
     * Matrix trace.
     * @return sum of the diagonal elements.
     */
    public double trace() {
        double t = 0;
        for (int i = 0; i < Math.min(nbRow, nbCol); i++) {
            t += get(i,i);
        }
        return t;
    }


    ////////////////////////////////////////////////////////////////////////////
    // various transformed outputs /////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Make a one-dimensional column packed copy of the internal array.
     * @return Matrix elements packed in a one-dimensional array by columns.
     */
    public double[] toColumnPackedCopy() {
        final double[] vals = new double[nbRow * nbCol];
        for (int r = 0; r < nbRow; r++) {
            for (int c = 0; c < nbCol; c++) {
                vals[r + c * nbRow] = get(r, c);
            }
        }
        return vals;
    }

    /**
     * Make a one-dimensional row packed copy of the internal array.
     * @return Matrix elements packed in a one-dimensional array by rows.
     */
    public double[] toRowPackedCopy() {
        double[] vals = new double[nbRow * nbCol];
        for (int r = 0; r < nbRow; r++) {
            for (int c = 0; c < nbCol; c++) {
                vals[r * nbCol + c] = get(r, c);
            }
        }
        return vals;
    }

    public double[] toArrayDouble(){
        final double[] array = new double[nbRow*nbCol];
        for(int i=0,r=0;r<nbRow;r++){
            for(int c=0;c<nbCol;c++,i++){
                array[i] = get(r, c);
            }
        }
        return array;
    }
    
    /**
     * Matrix as 1D double array, in column order.
     * 
     * @return 1D double array, in column order.
     */
    public double[] toArrayDoubleColOrder(){
        final double[] array = new double[nbRow*nbCol];
        for(int p=0,c=0;c<nbCol;c++){
            for(int r=0;r<nbRow;r++,p++){
                array[p] = (float) get(r, c);
            }
        }
        return array;
    }
    
    /**
     * Matrix as 1D float array, in row order.
     * 
     * @return 1D float array, in row order.
     */
    public float[] toArrayFloat(){
        final float[] array = new float[nbRow*nbCol];
        for(int i=0,r=0;r<nbRow;r++){
            for(int c=0;c<nbCol;c++,i++){
                array[i] = (float) get(r, c);
            }
        }
        return array;
    }
    
    /**
     * Matrix as 1D float array, in column order.
     * 
     * @return 1D float array, in column order.
     */
    public float[] toArrayFloatColOrder(){
        return toArrayFloatColOrder(null);
    }
    
    public float[] toArrayFloatColOrder(float[] array){
        if(array==null) array = new float[nbRow*nbCol];
        for(int p=0,c=0;c<nbCol;c++){
            for(int r=0;r<nbRow;r++,p++){
                array[p] = (float)get(r,c);
            }
        }
        return array;
    }

    public Chars toChars() {
        return Arrays.toChars(getValuesCopy());
    }

    public int getHash() {
        int hash = 7;
        hash = 53 * hash + this.nbRow;
        hash = 53 * hash + this.nbCol;
        return hash;
    }

    /**
     * Test if all cells in the matrix equals given value.
     * @param scalar scalar
     * @param tolerance tolerance
     * @return true if all values match
     */
    public boolean allEquals(double scalar, double tolerance) {
        for(int r=0; r<nbRow; r++){
            for(int c=0; c<nbCol; c++){
                if(Math.abs(get(r,c)-scalar) > tolerance){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractMatrix)) {
            return false;
        }
        final AbstractMatrix other = (AbstractMatrix) obj;
        if (this.nbRow != other.getNbRow()) {
            return false;
        }
        if (this.nbCol != other.getNbCol()) {
            return false;
        }

        //check values
        if(!Arrays.equals(toArrayDouble(), other.toArrayDouble())){
            return false;
        }

        return true;
    }

    protected static double[][] dArray(Matrix matrix){
        return matrix instanceof DefaultMatrix ? ((DefaultMatrix)matrix).values : matrix.getValuesCopy();
    }
}