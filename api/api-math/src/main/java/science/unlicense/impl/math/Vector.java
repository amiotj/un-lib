package science.unlicense.impl.math;

import science.unlicense.api.math.Tuple;
import static science.unlicense.impl.math.DefaultTuple.expand;

/**
 *
 * @author Johann Sorel
 */
public class Vector extends DefaultTuple{

    public Vector(int size){
        super(size);
    }

    public Vector(double x, double y){
        super(x, y);
    }

    public Vector(double x, double y, double z){
        super(x, y, z);
    }

    public Vector(double x, double y, double z, double w){
        super(x, y, z, w);
    }

    /**
     * Construct a vector adding one dimension.
     * @param tuple
     * @param value1
     */
    public Vector(Tuple tuple, double value1){
        this(expand(tuple.getValues(), value1));
    }

    public Vector(final double[] values){
        super(values);
    }

    public Vector(final float[] values){
        super(values);
    }

    public Vector(final Tuple v){
        super(v);
    }

    /**
     * {@inheritDoc }
     */
    public Vector getXY(){
        return new Vector(values[0], values[1]);
    }
    
    /**
     * {@inheritDoc }
     */
    public Vector getXYZ(){
        return new Vector(values[0], values[1], values[2]);
    }
    
    /**
     * {@inheritDoc }
     */
    public Vector getXYZW(){
        return new Vector(values[0], values[1], values[2], values[3]);
    }
    
    public double length(){
        return Vectors.length(values);
    }

    public double lengthSquare(){
        return Vectors.lengthSquare(values);
    }
    
    public double dot(Tuple other){
        return Vectors.dot(values, other.getValues());
    }

    /**
     * Returns true if vector do not contains any NaN or Infinite values.
     *
     * @return true is vector is finite
     */
    public boolean isFinite(){
        for(int i=0;i<values.length;i++){
            if(Double.isNaN(values[0]) || Double.isInfinite(values[1])){
                return false;
            }
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods without buffer //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public Vector add(Tuple other){
        return add(other,null);
    }

    public Vector subtract(Tuple other){
        return subtract(other,null);
    }

    public Vector multiply(Tuple other){
        return multiply(other,null);
    }

    public Vector divide(Tuple other){
        return divide(other,null);
    }

    public Vector scale(double scale){
        return scale(scale,null);
    }

    public Vector cross(Tuple other){
        return cross(other,null);
    }

    public Vector lerp(Tuple other, double ratio){
        return lerp(other,ratio,null);
    }

    public Vector normalize(){
        return normalize(null);
    }

    public Vector negate(){
        return negate(null);
    }
    
    /**
     * Calculate shortest angle to given vector.
     * formula : acos(dot(vec1,vec2))
     * 
     * @param second
     * @return shortest angle in radian
     */
    public double shortestAngle(Vector second){
        return Vectors.shortestAngle(values, second.values);
    }

    /**
     * Calculate the projected vector of given vector on this vector.
     * 
     * formula :
     * v = this vector
     * u = other vector
     * ((u dot v) / |v|^2 ) * v
     * 
     * @param candidate vector to project
     * @return projected vector of candidate on this vector.
     */
    public Vector project(Tuple candidate){
        return scale(dot(candidate) / lengthSquare());
    }
    
    /**
     * Calculate reflected vector.
     * 
     * @param normal
     * @return 
     */
    public Vector reflect(Tuple normal){
        return new Vector(Vectors.reflect(values, normal.getValues()));
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods with buffer /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public Vector add(Tuple other, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.add(values, other.getValues(), buffer.values);
        return buffer;
    }

    public Vector subtract(Tuple other, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.subtract(values, other.getValues(), buffer.values);
        return buffer;
    }

    public Vector multiply(Tuple other, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.multiply(values, other.getValues(), buffer.values);
        return buffer;
    }

    public Vector divide(Tuple other, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.divide(values, other.getValues(), buffer.values);
        return buffer;
    }

    public Vector scale(double scale, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.scale(values, scale, buffer.values);
        return buffer;
    }

    public Vector cross(Tuple other, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.cross(values, other.getValues(), buffer.values);
        return buffer;
    }

    public Vector lerp(Tuple other, double ratio, Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.lerp(values, other.getValues(), ratio, buffer.values);
        return buffer;
    }

    public Vector normalize(Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.normalize(values, buffer.values);
        return buffer;
    }

    public Vector negate(Vector buffer){
        if(buffer == null) buffer = new Vector(values.length);
        Vectors.negate(values, buffer.values);
        return buffer;
    }

    ////////////////////////////////////////////////////////////////////////////
    // methods local  //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public Vector localAdd(double x, double y){
        values[0] += x;
        values[1] += y;
        return this;
    }

    public Vector localAdd(double x, double y, double z){
        values[0] += x;
        values[1] += y;
        values[2] += z;
        return this;
    }

    public Vector localAdd(double x, double y, double z, double w){
        values[0] += x;
        values[1] += y;
        values[2] += z;
        values[3] += w;
        return this;
    }
    
    public Vector localAdd(Tuple other){
        Vectors.add(values, other.getValues(), values);
        return this;
    }

    public Vector localSubtract(Tuple other){
        Vectors.subtract(values, other.getValues(), values);
        return this;
    }

    public Vector localMultiply(Tuple other){
        Vectors.multiply(values, other.getValues(), values);
        return this;
    }

    public Vector localDivide(Tuple other){
        Vectors.divide(values, other.getValues(), values);
        return this;
    }

    public Vector localScale(double scale){
        Vectors.scale(values, scale, values);
        return this;
    }

    public Vector localCross(Tuple other){
        Vectors.cross(values, other.getValues(), values);
        return this;
    }

    public Vector localNormalize(){
        Vectors.normalize(values, values);
        return this;
    }

    public Vector localNegate(){
        Vectors.negate(values, values);
        return this;
    }

    public Vector localLerp(Tuple other, double ratio){
        Vectors.lerp(values, other.getValues(), ratio, values);
        return this;
    }

    public Vector copy() {
        return new Vector(this);
    }

    /**
     * Create a new vector with current vector values and extend it with
     * given value.
     * @param value, value to add at the end of Vector
     * @return Vector
     */
    public Vector extend(double value){
        return new Vector(expand(values, value));
    }

}
