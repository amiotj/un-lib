
package science.unlicense.impl.math;

import science.unlicense.api.math.MatrixRW;

/**
 *
 * @author Johann Sorel
 */
public class Matrix2x2 extends DefaultMatrix{

    public Matrix2x2() {
        super(2,2);
    }
    
    public Matrix2x2(Matrix2x2 m) {
        super(m);
    }

    public Matrix2x2(double m00, double m01, double m10, double m11) {
        super(m00, m01, m10, m11);
    }
 
    public Matrix2x2 copy() {
        return new Matrix2x2(this);
    }
    
    /**
     * @return determinant
     */
    public double getDeterminant(){
        return values[0][0] * values[1][1] - values[0][1] * values[1][0];
    }

    public Matrix2x2 setToIdentity() {
        super.setToIdentity();
        return this;
    }
    
    public boolean isIdentity() {
        return values[0][0]==1.0 && values[0][1]==0.0 
            && values[1][0]==0.0 && values[1][1]==1.0 ;
    }

    @Override
    public MatrixRW invert(MatrixRW buffer) {
        if(values[1][0]==0 && values[1][1]==1){
            //affine transform
            double m00 = values[0][0];
            double m01 = values[0][1];
            if(buffer==null) buffer = new Matrix2x2();
            buffer.set(0,0, 1.0/m00);
            buffer.set(1,0, 0.0);
            buffer.set(0,1, -m01/m00);
            buffer.set(1,1, 1.0);
            return buffer;
        }

        return super.invert(buffer);
    }

    @Override
    public DefaultMatrix localInvert() {
        if(values[2][0]==0 && values[2][1]==0 && values[2][2]==1){
            //affine transform
            double m00 = values[0][0];
            double m01 = values[0][1];
            values[0][0] = 1.0/m00;
            values[1][0] = 0.0;
            values[0][1] = -m01/m00;
            values[1][1] = 1.0;
            return this;
        }else{
            return super.localInvert();
        }
    }

    /**
     * Create rotation matrix from angle.
     *
     * @param angle in radians
     * @return Matrix2
     */
    public static Matrix2x2 fromAngle(double angle) {
        final Matrix2x2 m = new Matrix2x2();
        m.set(0, 0, Math.cos(angle));
        m.set(0, 1, -Math.sin(angle));
        m.set(1, 0, Math.sin(angle));
        m.set(1, 1, Math.cos(angle));
        return m;
    }
    
}