
package science.unlicense.impl.math.transform;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.number.Numbers;
import science.unlicense.api.math.Maths;

/**
 * Fast Fourrier Transform and Inverse Fast Fourrier Transform.
 * (Implementing Danielson-Lanczos algorithm)
 * 
 * @author Bertrand COTE
 */
public class FFT {
    
    /**
     * Fast Fourrier Transform (Danielson-Lanczos algorithm). O(n.log(n) but only works for pow of 2 data length)
     * 
     * @param dataReal data real part array
     * @param dataImag data imaginary part array
     */
    public static void fft( double[] dataReal, double[] dataImag ) {
        
        if ( dataReal.length != dataImag.length ) {
            throw new InvalidArgumentException("dataReal.length and dataImag.length must be equals.");
        }
        
        if ( Numbers.isNotPowOfTwo(dataReal.length) ) {
            throw new InvalidArgumentException("The length of data must be a pow of 2.");
        }
        
        bitReversalReordering ( dataReal, dataImag );
        
        DanielsonLanczos ( dataReal, dataImag );
    }
    
    /**
     * Inverse Fast Fourrier Transform. (O(n.log(n) but only works for pow of 2 data length)
     * 
     * @param dataReal data real part array
     * @param dataImag data imaginary part array
     */
    public static void ifft( double[] dataReal, double[] dataImag ) {
        
        // Conjugate input (inverse sign of dataImag values).
        for( int i=0; i<dataImag.length; i++ ) {
            dataImag[i] = -dataImag[i];
        }
        
        // forward fft
        FFT.fft( dataReal, dataImag );
        
        // Conjugate again and divide by data length.
        for( int i=0; i<dataImag.length; i++ ) {
            dataReal[i] = dataReal[i]/dataReal.length;
            dataImag[i] = -dataImag[i]/dataImag.length;
        }
    }
    
    /**
     * Reorder an array elements by bit reversal index order.
     * (Length of dR and dI must be equals and a power of 2)
     * E.g.: for a 8 length array:
     * 000 -----> 000
     * 001 -----> 100
     * 010 -----> 010
     * 011 -----> 110
     * 100 -----> 001
     * 101 -----> 101
     * 110 -----> 011
     * 111 -----> 111
     * @param dataReal
     * @param dataImag
     */
    private static void bitReversalReordering( double[] dataReal, double[] dataImag ) {
        int iRev;
        int nBits = Integer.numberOfTrailingZeros(dataReal.length);
        for ( int i=1; i<dataReal.length-1; i++) {
            iRev = Numbers.reverseBits ( i, nBits );
            if ( iRev > i) {
                Arrays.swap( dataReal, i, iRev );
                Arrays.swap( dataImag, i, iRev );
            }
        }
    }
        
    /**
     * The Danielson-Lanczos algorithm.
     * 
     * @param dataReal data real part array
     * @param dataImag data imaginary part array
     */
    private static void DanielsonLanczos(double[] dataReal, double[] dataImag) {

        int istep;
        double theta, sinHalfTheta;
        double wpR, wpI;
        double wR, wI;
        double tempR, tempI;

        int mmax = 1;

        while (dataReal.length > mmax) {

            istep = 2 * mmax;
            theta = -Maths.PI / mmax;
            sinHalfTheta = Math.sin(0.5 * theta);

            // wp = -2.0 * SIN(0.5_8*theta)**2 + i* SIN(theta)
            wpR = -2.0 * sinHalfTheta * sinHalfTheta;
            wpI = Math.sin(theta);

            // w = 1. + i*0.
            wR = 1.0;
            wI = 0.0;

            for (int m=1; m<=mmax; m++) {

                for (int i=m-1; i<dataReal.length; i+=istep) {
                    
                    int j = i + mmax;

                    // temp = ws * data[j]
                    tempR = wR * dataReal[j] - wI * dataImag[j];
                    tempI = wR * dataImag[j] + wI * dataReal[j];

                    // data[j] = data[i] - ws*data[j]
                    dataReal[j] = dataReal[i] - tempR;
                    dataImag[j] = dataImag[i] - tempI;

                    // data[i] = data[i] + ws*data[j]
                    dataReal[i] = dataReal[i] + tempR;
                    dataImag[i] = dataImag[i] + tempI;
                }

                //w = w*wp + w
                tempR = wR;
                tempI = wI;
                wR = tempR * wpR - tempI * wpI + tempR;
                wI = tempR * wpI + tempI * wpR + tempI;
            }

            mmax = istep;
        }

    }
}