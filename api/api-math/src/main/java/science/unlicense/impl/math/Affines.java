
package science.unlicense.impl.math;

import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public final class Affines {

    public static Tuple transformNormal(Affine affine, Tuple t, TupleRW buffer) {
        final Vector v = new Vector(t, 0);
        affine.toMatrix().transform(v, v);
        if (buffer == null) {
            return v.getXYZ();
        } else {
            buffer.set(v);
            return buffer;
        }
    }

}
