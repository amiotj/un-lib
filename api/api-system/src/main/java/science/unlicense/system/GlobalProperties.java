
package science.unlicense.system;

/**
 * Underlying system can define multiple kind of properties.
 * 
 * The properties include :
 * - user name and home
 * - system properties
 * - vm properties
 * 
 * @author Johann Sorel
 */
public interface GlobalProperties {
    
    /**
     * System properties, this tree is unmodifiable.
     * 
     * @return MetaTree, never null, unmodifiable.
     */
    MetaTree getSystemTree();
    
    /**
     * Local properties, this tree is modifiable and values are persisted on
     * disk to be restored at each execution.
     * 
     * @return MetaTree, never null, modifiable.
     */
    MetaTree getPersistantTree();
    
    /**
     * Local execution properties, this tree is modifiable but not persisted.
     * 
     * @return MetaTree, never null, modifiable.
     */
    MetaTree getExecutionTree();
    
}
