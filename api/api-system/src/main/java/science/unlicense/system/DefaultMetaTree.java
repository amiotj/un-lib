

package science.unlicense.system;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class DefaultMetaTree implements MetaTree{

    private final NamedNode root;

    public DefaultMetaTree(NamedNode root) {
        this.root = root;
    }

    public NamedNode getRoot() {
        return root;
    }

    public NamedNode search(Chars path) {
        return Nodes.getPathNode(getRoot(), path, false);
    }

}
