

package science.unlicense.system.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;

/**
 * 
 * @author Johann Sorel
 */
public class VirtualPath extends AbstractPath{

    private final String vuid = "V"+System.identityHashCode(this);
    private final Chars name;
    private final Sequence children = new ArraySequence();
    
    private boolean isContainer = false;
    private boolean exist = false;
    private ByteSequence data = null;

    public VirtualPath(Chars name) {
        this.name = name;
    }
    
    public void addChildren(int index, Path[] children) {
        this.children.addAll(index, new ArraySequence(children));
    }

    public void removeChild(int index) {
        children.remove(index);
    }

    public Sequence getChildren() {
        return Collections.readOnlySequence(children);
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{};
    }

    @Override
    public Chars getName() {
        return name;
    }

    @Override
    public Path getParent() {
        return null;
    }

    @Override
    public boolean isContainer() throws IOException {
        return isContainer;
    }

    @Override
    public boolean exists() throws IOException {
        return exist;
    }

    @Override
    public Object getPathInfo(Chars key) {
        if(!isContainer && Path.INFO_OCTETSIZE.equals(key) && data!=null) {
            return data.getSize();
        }
        return super.getPathInfo(key);
    }
    
    @Override
    public boolean createContainer() throws IOException {
        if(exist){
            if(isContainer){
                return true;
            }else{
                throw new IOException("Path already exist and is not a container.");
            }
        }
        exist = true;
        isContainer = true;
        return true;
    }

    @Override
    public boolean createLeaf() throws IOException {
        if(exist){
            if(isContainer){
                throw new IOException("Path already exist and is a container.");
            }else{
                return true;
            }
        }
        exist = true;
        isContainer = false;
        data = new ByteSequence();
        return true;
    }
    
    @Override
    public Path resolve(Chars address) {
        Chars[] segments = address.split('/');
        Path base = this;

        loopsegment:
        for(int i=0;i<segments.length;i++){
            if(segments[i].isEmpty() || segments[i].equals(new Chars("."))) continue;
            if(segments[i].equals(new Chars(".."))){
                base = base.getParent();
            }else {
                Iterator ite = base.getChildren().createIterator();
                while(ite.hasNext()){
                    Path next = (Path) ite.next();
                    if(next.getName().equals(segments[i])){
                        base = next;
                        continue loopsegment;
                    }
                }
                //segment do not exist create it.
                final VirtualPath p = new VirtualPath(segments[i]);
                children.add(p);
                base = p;
            }
        }

        return base;
    }

    @Override
    public PathResolver getResolver() {
        throw new UnimplementedException("Not supported.");
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        if(exist && !isContainer){
            return new ArrayInputStream(data.toArrayByte());
        } else {
            throw new IOException("Path do not exist");
        }
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        if(exist) {
            if(isContainer) {
                throw new IOException("Path is a container.");
            } else {
                return new ArrayOutputStream(data);
            }
        } else {
            createLeaf();
            return new ArrayOutputStream(data);
        }
    }

    @Override
    public Chars toURI() {
        return new Chars(vuid+">/"+name+"/");
    }
    
}
