
package science.unlicense.system;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;

/**
 *
 * @author Johann Sorel
 */
public interface MetaTree {

    /**
     * Get root node.
     * @return NamedNode, never null
     */
    NamedNode getRoot();

    /**
     * Get node for given path.
     *
     * @param path
     * @return NamedNode or null.
     */
    NamedNode search(Chars path);

}
