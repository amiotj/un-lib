
package science.unlicense.system.util;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.system.MetaTree;

/**
 *
 * @author Johann Sorel
 */
public class ModuleSeeker {
    
    /**
     *  Search the main meta tree.
     * @param path
     * @param predicate
     * @return 
     */
    public static Sequence searchNodes(final Chars path, final Predicate predicate){
        return searchNodes(science.unlicense.system.System.get().getModuleManager().getMetadataRoot(), path, true, predicate);
    }
    
    /**
     * 
     * @param tree
     * @param path
     * @param recursive
     * @param predicate
     * @return 
     */
    public static Sequence searchNodes(final MetaTree tree, final Chars path, 
            final boolean recursive, final Predicate predicate){
        
        final Sequence results = new ArraySequence();
        final NamedNode nn = tree.search(path);
        
        if(nn==null) return results;
        
        if(recursive){
            //use a visitor to loop on all sub-nodes
            nn.accept(new DefaultNodeVisitor() {
                public Object visit(Node node, Object context) {
                    super.visit(node, context);
                    if(predicate==null || predicate.evaluate(node)){
                        results.add(node);
                    }
                    return null;
                }
            },null);
        }else{
            //simple loop
            for (Iterator ite=nn.getChildren().createIterator();ite.hasNext();) {
                final Object n = ite.next();
                if(predicate==null || predicate.evaluate(n)){
                    results.add(n);
                }
            }
        }
        
        return results;
    }
    
    /**
     *  Search the main meta tree.
     * @param path
     * @param predicate
     * @return 
     */
    public static Sequence searchValues(final Chars path, final Predicate predicate){
        return searchValues(science.unlicense.system.System.get().getModuleManager().getMetadataRoot(), path, true, predicate);
    }
    
    /**
     * 
     * @param tree
     * @param path
     * @param recursive
     * @param predicate
     * @return 
     */
    public static Sequence searchValues(final MetaTree tree, final Chars path, 
            final boolean recursive, final Predicate predicate){
        
        final Sequence results = new ArraySequence();
        final NamedNode nn = tree.search(path);
        
        if(nn==null) return results;
        
        if(recursive){
            //use a visitor to loop on all sub-nodes
            nn.accept(new DefaultNodeVisitor() {
                public Object visit(Node node, Object context) {
                    super.visit(node, context);
                    final NamedNode named = (NamedNode) node;
                    if(predicate==null || predicate.evaluate(named.getValue())){
                        results.add(named.getValue());
                    }
                    return null;
                }
            },null);
        }else{
            //simple loop
            for (Iterator ite=nn.getChildren().createIterator();ite.hasNext();) {
                final NamedNode named = (NamedNode)ite.next();
                if(predicate==null || predicate.evaluate(named.getValue())){
                    results.add(named.getValue());
                }
            }
        }
        
        return results;
    }
    
}
