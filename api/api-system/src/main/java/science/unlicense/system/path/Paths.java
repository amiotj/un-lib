
package science.unlicense.system.path;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.model.tree.Nodes;

import science.unlicense.system.System;

/**
 * Convinient methods for path manipulation.
 *
 * @author Johann Sorel
 */
public final class Paths {

    private Paths(){}

    /**
     * Lists available path formats.
     *
     * @return array of PathFormat, never null but can be empty.
     */
    public static PathFormat[] getFormats(){
        final NamedNode root = System.get().getModuleManager().getMetadataRoot().search(new Chars("services/science.unlicense.api.path.PathFormat"));
        if (root==null) {
            return new PathFormat[0];
        }
        return (PathFormat[]) Nodes.getChildrenValues(root, null).toArray(PathFormat.class);
    }

    /**
     * Resolve given string to a path.
     * A least the following protocols must be supported :
     * - mod : internal files
     * - file : operation system files
     *
     * @param path
     * @return
     */
    public static Path resolve(Chars strPath){

        Chars[] parts = strPath.split('!');
        if(parts[0].getFirstOccurence(':')<0){
            throw new InvalidArgumentException("Invalid string path, path pattern must match : 'resolver:path'  but was "+strPath);
        }

        strPath = parts[0];

        final PathFormat[] formats = getFormats();
        Path path = null;
        for(int i=0;i<formats.length;i++){
            if(formats[i].isAbsolute()){
                try {
                    path = formats[i].createResolver(null).resolve(strPath);
                } catch (IOException ex) {
                    Loggers.get().critical(ex);
                }
                if(path != null) break;
            }
        }

        if(parts.length>1){
            //resolve subpaths
            for(int i=1;i<parts.length;i++){
                path = new CrossingPath(path);
                path = path.resolve(parts[i]);
            }
        }

        return path;
    }

    /**
     * Remove the extension of a path.
     * example : for entry 'test.png' result is 'test'
     * 
     * @param name
     * @return 
     */
    public static Chars stripExtension(Chars name){
        final int index = name.getFirstOccurence('.');
        if(index>=0){
            return name.truncate(0, index);
        }else{
            return name;
        }
    }
    
    /**
     * get the extension of a path.
     * example : for entry 'test.png' result is 'png'
     * 
     * @param name
     * @return 
     */
    public static Chars getExtension(Chars name){
        final int index = name.getFirstOccurence('.');
        if(index>=0){
            return name.truncate(index+1,name.getCharLength());
        }else{
            return name;
        }
    }
    
    /**
     * 
     * 
     * @param base
     * @param child
     * @return 
     */
    public static Chars relativePath(Path base, Path child){
        final CharBuffer cb = new CharBuffer();
        final Sequence paths = new ArraySequence();

        while(!child.equals(base)){
            paths.add(child);
            child = child.getParent();
        }

        cb.append('.');
        for(int i=paths.getSize()-1;i>=0;i--){
            final Path cp = (Path) paths.get(i);
            cb.append('/');
            cb.append(cp.getName());
        }

        return cb.toChars();
    }
    
}
