
package science.unlicense.system;

import science.unlicense.api.desktop.TransferBag;

/**
 * Each environment should declare a System instance with exactly the same
 * methods  definition.
 *
 * @author Johann Sorel
 */
public abstract class System {

    public static final String TYPE_UNIX = "unix";
    public static final String TYPE_WINDOWS = "windows";
    
    private static System INSTANCE;

    public static System get(){

        if(INSTANCE == null){
            //we only have one implementation for now
            //TODO : this will have to change
            try {
                INSTANCE = (System) Class.forName("science.unlicense.impl.system.jvm.JVMSystem").newInstance();
            } catch (Exception ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }

        return INSTANCE;
    }

    protected System(){
    }

    /**
     * System type, windows,linux,...
     * TODO : find a better way to define the os.
     *
     * @return String, never null
     */
    public abstract String getType();

    /**
     * Returns the module manager.
     * 
     * @return ModuleManager, never null
     */
    public abstract ModuleManager getModuleManager();

    /**
     * Returns the socket manager.
     * 
     * @return SocketManager, never null
     */
    public abstract SocketManager getSocketManager();

    /**
     * Returns the system properties.
     * 
     * @return GlobalProperties, never null
     */
    public abstract GlobalProperties getProperties();
    
    /**
     * Returns the system clipboard.
     * 
     * @return TransferBag, never null
     */
    public abstract TransferBag getClipBoard();

    /**
     * Returns the system drag and drop bag.
     *
     * @return TransferBag, never null
     */
    public abstract TransferBag getDragAndDrapBag();
    
}
