
package science.unlicense.api.store;

import science.unlicense.api.store.Format;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.system.util.ModuleSeeker;

/**
 * Format utility class.
 * 
 * @author Johann Sorel
 */
public final class Formats {

    private Formats() {}
    
    /**
     * Lists available formats.
     * 
     * @return array of Format, never null but can be empty.
     */
    public static Format[] getFormats(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/"), 
                Predicates.instanceOf(Format.class));
        final Format[] formats = new Format[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }
    
    public static Format getFormat(Chars name) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i].getIdentifier().equals(name)){
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static Format getFormatForExtension(Chars ext) throws InvalidArgumentException{
        final Format[] formats = getFormats();
        for(int i=0;i<formats.length;i++){
            for(Chars e : formats[i].getExtensions()){
                if(e.equals(ext, true, true)){
                    return formats[i];
                }
            }
        }
        throw new InvalidArgumentException("Format "+ext+" not found.");
    }
    
    /**
     * Search known formats for one which can support the given path.
     * 
     * @param path
     * @return Format, can be null
     */
    public static Format findFormat(Path path){
        final Format[] formats = getFormats();
        for(Format f : formats){
            try{
                if(f.canDecode(path)){
                    return f;
                }
            }catch(IOException ex){
                //ignore this
            }
        }
        return null;
    }
    
    
}
