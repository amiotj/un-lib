
package science.unlicense.api.desktop;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * Default transfer bag using an ArraySequence as back end.
 *
 * @author Johann Sorel
 */
public class DefaultTransferBag implements TransferBag {

    private final Sequence bag = new ArraySequence();

    public Sequence getAttachments() {
        return bag;
    }

}
