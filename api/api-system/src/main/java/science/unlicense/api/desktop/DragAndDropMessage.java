
package science.unlicense.api.desktop;

import science.unlicense.api.event.DefaultEventMessage;

/**
 *
 * @author Johann Sorel
 */
public class DragAndDropMessage extends DefaultEventMessage {

    public static final int TYPE_ENTER = 1;
    public static final int TYPE_EXIT = 2;
    public static final int TYPE_DROP = 3;

    private final int type;

    public DragAndDropMessage(int type) {
        super(true);
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public TransferBag getBag(){
        return science.unlicense.system.System.get().getDragAndDrapBag();
    }

}
