
package science.unlicense.api.desktop;

import science.unlicense.api.collection.Sequence;

/**
 * A transfer bag is a place where objects are shared between objects and applications.
 * Operating systems offer two default bags know as the clipboard and drag&drop.
 *
 * @author Johann Sorel
 */
public interface TransferBag {

    /**
     * List of attachments in the bag.
     * This is a live sequence, not a snapshot, meaning events occurs
     * when new attachments are added and removed.
     *
     * @return sequence, never null
     */
    Sequence getAttachments();

}
