
package science.unlicense.api.desktop;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 * Default attachment.
 * 
 * @author Johann Sorel
 */
public class DefaultAttachment implements Attachment {

    private final Dictionary metas = new HashDictionary();
    private final Object value;

    public DefaultAttachment(Object value, Chars mimeType) {
        CObjects.ensureNotNull(mimeType, META_MIME_TYPE.toString());
        this.value = value;
        this.metas.add(META_MIME_TYPE, mimeType);
    }

    public Dictionary getMetadatas() {
        return metas;
    }

    public Object getObject() {
        return value;
    }

}
