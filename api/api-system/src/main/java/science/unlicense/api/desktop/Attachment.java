
package science.unlicense.api.desktop;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;

/**
 *
 *
 * @author Johann Sorel
 */
public interface Attachment {
    
    /**
     * Attached object mime type.
     */
    public static final Chars META_MIME_TYPE = new Chars("mimeType");
    /**
     * Object who provided this attachment.
     */
    public static final Chars META_SOURCE_OBJECT = new Chars("sourceObject");
    /**
     * Screen location from where the attachment has been added.
     * This property should be filled when the attachment is a drag and drop.
     */
    public static final Chars META_SOURCE_SCREEN_LOCATION = new Chars("sourceScreenLocation");

    /**
     * Get the attached object metadatas.
     * The only required property is the mime-type of type Chars.
     *
     * @return Dictionary, never null.
     */
    Dictionary getMetadatas();
    
    /**
     * 
     * @return attached object
     */
    Object getObject();
        
    
}
