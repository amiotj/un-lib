package science.unlicense.impl.character;

import science.unlicense.api.character.Chars;

/**
 * Algorithm to calculate distances between Strings.
 * http://fr.wikipedia.org/wiki/Distance_de_Levenshtein
 * https://github.com/awnist/distance
 *
 * @author Johann Sorel (UN Project adaptation)
 */
public final class Levenshtein {

    private Levenshtein() {}

    public static float distance(Chars s1, Chars s2) {
      final int s1length = s1.getCharLength();
      final int s2length = s2.getCharLength();
      if (s1length == 0) {
        return s2length;
      }
      if (s2length == 0) {
        return s1length;
      }
      
      int i, j; 
      final int[][] distance = new int[s1length+1][s2length+1];
      
      for(i=0; i<=s1length; i++){
            distance[i][0] = i;
      }
      for(j=1; j<=s2length; j++){
            distance[0][j] = j;
      }
      
      for(i=1; i<=s1length; i++){
          for(j=1; j<=s2length; j++){
              final int cost = (s1.getUnicode(i-1) == s2.getUnicode(j-1)) ? 0 : 1;
              int min = Math.min(distance[i-1][j]+1, distance[i][j-1]+1);
              min = Math.min(min, distance[i-1][j-1]+cost);
              distance[i][j] = min;
        }
      }
      
      return distance[s1length][s2length];
    }
}
