
package science.unlicense.api.code;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.inst.Instruction;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * A static function or class method.
 * 
 * @author Johann Sorel
 */
public class Function {
    
    public Chars id;
    
    public final Metas metas = new Metas();
    
    /**
     * Function input parameters.
     */
    public final Sequence inParameters = new ArraySequence();
    /**
     * Function output parameters.
     */
    public final Sequence outParameters = new ArraySequence();
    
    /**
     * Root instruction of the function.
     */
    public Instruction startInstruction;

}