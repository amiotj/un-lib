
package science.unlicense.api.code.inst;

/**
 * Exit of function.
 * All ends of a function instruction graph must be of this type.
 *
 * @author Johann Sorel
 */
public class Return extends AbstractInstruction {

    /**
     * May be null.
     * In such case it is equivalent to a 'return void'.
     */
    public Reference value;

    public Return() {
        super(true);
    }
    
    public Return(Reference value) {
        super(true);
        this.value = value;
    }

    public Instruction decompose() {
        return this;
    }
    
}
