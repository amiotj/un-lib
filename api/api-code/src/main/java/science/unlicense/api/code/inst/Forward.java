
package science.unlicense.api.code.inst;

/**
 * Instruction with a single next instruction.
 *
 * @author Johann Sorel
 */
public interface Forward extends Instruction {

    /**
     * Get next instruction.
     *
     * @return next instruction.
     */
    Instruction getNext();

    /**
     * Set next instruction.
     * 
     * @param next instruction.
     */
    void setNext(Instruction next);
    
}
