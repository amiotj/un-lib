
package science.unlicense.api.code.inst;

/**
 *
 * @author Johann Sorel
 */
public class Affectation extends AbstractInstruction {

    public Reference ref;

    public Instruction value;

    public Affectation() {
        super(true);
    }

    @Override
    public Instruction decompose() {
        return this;
    }

}
