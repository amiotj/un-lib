
package science.unlicense.api.code.inst;

import science.unlicense.api.character.Chars;

/**
 * Target of a jump instruction.
 *
 * labels have no effect, they as place holder is the function instruction graph
 * for jump instructions.
 *
 * @author Johann Sorel
 */
public class Label extends AbstractForward {

    /**
     * Convenient name of the label, can be null.
     */
    public Chars name;

    public Label() {
        super(true);
    }

    public Label(Chars name) {
        super(true);
        this.name = name;
    }
    
    public Label(Instruction next) {
        super(true, next);
    }
    
    public Label(Chars name, Instruction next) {
        super(true, next);
        this.name = name;
    }

    public Instruction decompose() {
        return this;
    }
    
}
