
package science.unlicense.api.code.inst;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractInstruction implements Instruction {

    private final boolean atomic;

    public AbstractInstruction(boolean atomic) {
        this.atomic = atomic;
    }
    
    public boolean isAtomic() {
        return atomic;
    }

    
}
