
package science.unlicense.api.code.meta;

import science.unlicense.api.character.Chars;

/**
 * General concept of abstraction.
 *
 *
 * @author Johann Sorel
 */
public class DefaultAbstract extends DefaultMeta implements Abstract {

    public static final Chars ID = new Chars("abstract");

    public DefaultAbstract() {
        super(ID);
    }

}
