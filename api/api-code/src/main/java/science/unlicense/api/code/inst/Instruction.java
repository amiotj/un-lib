
package science.unlicense.api.code.inst;

/**
 * An instruction is a
 * 
 * @author Johann Sorel
 */
public interface Instruction {

    /**
     * Atomic instructions are : 
     * - Declaration
     * - Affectation
     * - Call
     * - Label
     * - Jump
     * - Return
     * Those instructions can not be decomposed.
     * 
     * @return true if instruction is atomic
     */
    boolean isAtomic();
    
    /**
     * Decompose this instruction in atomic instructions.
     * Has no effect if instruction is already atomic.
     * 
     * @return 
     */
    Instruction decompose();

}