
package science.unlicense.api.code.inst;

/**
 * Plain value.
 * it's interpretation in based on the context.
 *
 * @author Johann Sorel
 */
public class Literal {

    /**
     * Raw bytes of the literal.
     */
    public byte[] value;

}
