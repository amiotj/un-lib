
package science.unlicense.api.code.meta;

/**
 * Define the scope of a Property,Class or Function.
 * 
 * @author Johann Sorel
 */
public interface Scope extends Constraint {
        
}