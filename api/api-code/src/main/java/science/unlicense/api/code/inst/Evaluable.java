
package science.unlicense.api.code.inst;

/**
 *
 * @author Johann Sorel
 */
public interface Evaluable  {

    Object evaluate(Object candidate);

}
