
package science.unlicense.api.code.meta;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultMeta extends CObject implements Meta {

    protected final Chars id;

    public DefaultMeta(Chars id) {
        this.id = id;
    }
    
    public Chars getId() {
        return id;
    }

}
