
package science.unlicense.api.code;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.meta.DefaultAbstract;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.Nodes;

/**
 * A class.
 *
 * @author Johann Sorel
 */
public class Class extends CObject{

    public Chars id;

    public final Metas metas = new Metas();
    /**
     * Parent classes.
     */
    public final Sequence parents = new ArraySequence();
    /**
     * This class properties, does not include parent properties.
     */
    public final Sequence properties = new ArraySequence();
    /**
     * This class functions, does not include parent functions.
     */
    public final Sequence functions = new ArraySequence();

    public boolean isAbstract(){
        return metas.get(DefaultAbstract.ID)!=null;
    }

    public Chars toChars() {
        final Sequence values = new ArraySequence();
        for(int i=0,n=properties.getSize();i<n;i++){
            final Property prop = (Property) properties.get(i);
            values.add(prop.id.concat(new Chars(" : ")).concat((prop.objclass!=null)?
                    prop.objclass.id : new Chars(prop.primclass.getSimpleName())));
        }
        return Nodes.toChars(id, values);
    }

}