
package science.unlicense.api.code.inst;

/**
 * Multiple solution branch instruction.
 * It is often the parent of branching operation which have not been unwrapped.
 * like : for, while, do/while, switch
 *
 * @author Johann Sorel
 */
public interface Branching extends Instruction {

    /**
     * List all next instructions.
     * @return
     */
    Instruction[] getNexts();

}
