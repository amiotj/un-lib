
package science.unlicense.api.code.inst;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractForward extends AbstractInstruction implements Forward {

    private Instruction next;

    public AbstractForward(boolean atomic) {
        super(atomic);
        this.next = next;
    }

    public AbstractForward(boolean atomic, Instruction next) {
        super(atomic);
        this.next = next;
    }

    public Instruction getNext() {
        return next;
    }

    public void setNext(Instruction next) {
        this.next = next;
    }
    
}
