
package science.unlicense.api.code.inst;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class Call extends AbstractInstruction {

    public Reference target;

    public Chars function;

    /** References or expressions */
    public Sequence arguments;

    public Call() {
        super(true);
    }

    public Instruction decompose() {
        return this;
    }

}
