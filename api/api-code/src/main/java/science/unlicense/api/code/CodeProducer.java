
package science.unlicense.api.code;

import science.unlicense.api.collection.Sequence;

/**
 * A code producer create CodeFiles from CodeContexts and
 * CodeContexts from CodeFiles.
 *
 * @author Johann Sorel
 */
public interface CodeProducer {

    /**
     * Convert the code context in CodeFiles.
     * The number of code files may not be the same as the context objects.
     *
     * @param context
     * @return sequence of CodeFile
     */
    Sequence createCodeFiles(CodeContext context) throws CodeException;

    /**
     * Convert code files to code context.
     * 
     * @param codeFile
     * @return 
     */
    CodeContext createCodeContext(CodeFile codeFile) throws CodeException;

    /**
     * Convert code files to code context.
     * 
     * @param codeFiles
     * @return 
     */
    CodeContext createCodeContext(Sequence codeFiles) throws CodeException;


}
