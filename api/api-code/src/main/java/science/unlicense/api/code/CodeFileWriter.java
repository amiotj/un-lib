
package science.unlicense.api.code;

import science.unlicense.api.io.IOException;
import science.unlicense.api.io.Writer;

/**
 *
 * @author Johann Sorel
 */
public interface CodeFileWriter extends Writer {

    void write(CodeFile file) throws IOException;
    
}
