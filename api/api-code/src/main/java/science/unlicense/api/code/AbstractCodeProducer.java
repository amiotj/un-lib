
package science.unlicense.api.code;

import science.unlicense.api.collection.Collections;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractCodeProducer implements CodeProducer {

    @Override
    public CodeContext createCodeContext(CodeFile codeFile) throws CodeException {
        return createCodeContext(Collections.singletonSequence(codeFile));
    }
    
}
