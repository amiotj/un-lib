
package science.unlicense.api.code;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.meta.Meta;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * Store object metas.
 * 
 * @author Johann Sorel
 */
public final class Metas extends ArraySequence{

    public Sequence getAll(Chars id){
        final Sequence res = new ArraySequence();
        for(int i=0,n=getSize();i<n;i++){
            final Meta meta = (Meta) get(i);
            if(meta.getId().equals(id)) res.add(meta);
        }
        return res;
    }
    
    public Meta get(Chars id){
        for(int i=0,n=getSize();i<n;i++){
            final Meta meta = (Meta) get(i);
            if(meta.getId().equals(id)) return meta;
        }
        return null;
    }
        
    public void remove(Chars id){
        for(int i=getSize()-1;i>=0;i--){
            final Meta meta = (Meta) get(i);
            if(meta.getId().equals(id)) remove(i);
        }
    }
    
}
