
package science.unlicense.api.code.meta;

/**
 * Indicate an abstract class or method.
 *
 * @author Johann Sorel
 */
public interface Abstract extends Meta {

}
