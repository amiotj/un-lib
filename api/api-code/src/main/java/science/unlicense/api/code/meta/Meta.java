
package science.unlicense.api.code.meta;

import science.unlicense.api.character.Chars;

/**
 * All languages used various metadata informations to define code condition,
 * state, scope, constraints, description...
 * Those are Metas, they are fundamental to code execution yet provide very useful
 * informations. 
 * 
 * @author Johann Sorel
 */
public interface Meta {

    /**
     * Unique identifier of a meta type.
     * 
     * @return unique id
     */
    Chars getId();
    
}
