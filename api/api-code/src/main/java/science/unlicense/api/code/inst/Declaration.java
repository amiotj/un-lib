
package science.unlicense.api.code.inst;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Declaration extends AbstractForward {
    
    private Chars id;

    public Declaration(Chars id) {
        super(true);
        this.id = id;
    }

    public Chars getId() {
        return id;
    }

    public void setId(Chars id) {
        this.id = id;
    }

    public Instruction decompose() {
        return this;
    }
    
}
