
package science.unlicense.api.code.inst;


/**
 * Most primitive branching instruction.
 *
 *
 * @author Johann Sorel
 */
public class Jump extends AbstractForward implements Instruction {

    /**
     * Condition may be null.
     * In this case, the jump is actually a goto.
     */
    public Reference condition;

    /**
     * Target instruction of the jump.
     */
    public Label label;

    public Jump() {
        super(true);
    }

    public Instruction decompose() {
        return this;
    }

}
