
package science.unlicense.api.code;

import science.unlicense.api.io.IOException;
import science.unlicense.api.io.Reader;

/**
 * Read code from input and split in tokens.
 * This step is not included in the CodeProducer to offer quick processing
 * such as syntax coloration or first stage validation.
 * 
 * @author Johann Sorel
 */
public interface CodeFileReader extends Reader {

    /**
     * Convert the content of input to CodeFile.
     * 
     * @return CodeFile
     * @throws IOException
     */
    CodeFile read() throws IOException;

}
