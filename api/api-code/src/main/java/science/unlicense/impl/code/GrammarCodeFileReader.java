
package science.unlicense.impl.code;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFile;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.api.path.Path;

/**
 * Code File reader using default grammar modele.
 * 
 * @author Johann Sorel
 */
public class GrammarCodeFileReader implements CodeFileReader{

    private final Parser parser;

    public GrammarCodeFileReader(Rule rootRule) {
        parser = new Parser(rootRule);
    }

    @Override
    public void setInput(Object input) throws IOException {
        parser.setInput(input);
    }

    @Override
    public Object getInput() {
        return parser.getInput();
    }

    public Document getConfiguration() {
        return parser.getConfiguration();
    }

    public void setConfiguration(Document configuration) {
        parser.setConfiguration(configuration);
    }

    @Override
    public CodeFile read() throws IOException {
        final SyntaxNode sn = parser.parse();
        final CodeFile file = new CodeFile();
        final Object input = getInput();
        if(input instanceof Path){
            file.path = new Chars(((Path)input).toURI());
        }
        file.syntax = sn;
        return file;
    }

    @Override
    public void dispose() throws IOException {
        parser.dispose();
    }

}
