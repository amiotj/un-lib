

package science.unlicense.api.linguistics;

import science.unlicense.api.character.Chars;

/**
 * A Word.
 *
 * @author Johann Sorel
 */
public interface Word {

    /**
     * The word as text.
     *
     * @return Chars never null.
     */
    Chars getText();

    /**
     * The word in phonetic alphabet.
     *
     * @return Chars, can be null.
     */
    Chars getPhoneticText();

    /**
     * The word as phonemes.
     *
     * @return Phoneme[], can be null.
     */
    Phoneme[] getPhonemes();

    /**
     * The word as diphones.
     *
     * @return Diphone[], can be null.
     */
    Diphone[] getDiphones();

}
