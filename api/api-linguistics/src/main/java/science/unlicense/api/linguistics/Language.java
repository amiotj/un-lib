

package science.unlicense.api.linguistics;

import science.unlicense.api.character.Chars;

/**
 * Language : en,fr,br,...
 *
 * @author Johann Sorel
 */
public class Language {

    private final Chars code;

    public Language(Chars code) {
        this.code = code;
    }

    public Chars getCode() {
        return code;
    }
    
}
