

package science.unlicense.api.linguistics;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 * Smallest piece of word which can be identified.
 *
 * @author Johann Sorel
 */
public class Phoneme extends CObject {

    /**
     * Phoneme which symbolize a short pause in the speech.
     */
    public static final Phoneme PAUSE = new Phoneme(new Chars("pause"));

    private final Chars code;

    public Phoneme(Chars code) {
        this.code = code;
    }

    public Chars getCode() {
        return code;
    }

    public boolean equals(Object obj) {
        if(obj instanceof Phoneme){
            return this.code.equals(((Phoneme)obj).code);
        }
        return false;
    }

    public int getHash() {
        return code.getHash();
    }

    public Chars toChars() {
        return code;
    }


}
