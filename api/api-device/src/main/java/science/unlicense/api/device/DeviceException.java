

package science.unlicense.api.device;

/**
 *
 * @author Johann Sorel
 */
public class DeviceException extends Exception {

    public DeviceException() {
        super();
    }
    
    public DeviceException(String message){
        super(message);
    }
    
    public DeviceException(Throwable ex){
        super(ex);
    }
    
    public DeviceException(String message,Throwable ex){
        super(message,ex);
    }
    
}
