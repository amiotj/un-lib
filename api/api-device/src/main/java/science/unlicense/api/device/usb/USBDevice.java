

package science.unlicense.api.device.usb;

import science.unlicense.api.device.DeviceException;
import science.unlicense.api.model.tree.TypedNode;

/**
 *
 * @author Johann Sorel
 */
public interface USBDevice {
    
    short getProductId();
    
    short getVendorId();
    
    USBControl createControl() throws DeviceException;
    
    TypedNode getDescription();
    
}
