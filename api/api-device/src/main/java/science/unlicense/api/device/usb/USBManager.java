

package science.unlicense.api.device.usb;

import science.unlicense.api.character.Chars;
import science.unlicense.api.device.DeviceException;

/**
 * Entry point to acces USB devices.
 * 
 * @author Johann Sorel
 */
public interface USBManager {
    
    /**
     * Get USB manager name.
     * @return Chars never null
     */
    Chars getName();
    
    /**
     * List available USB devices.
     * @return USBDevice array, never null but can be empty.
     * @throws science.unlicense.api.device.DeviceException
     */
    USBDevice[] getDevices() throws DeviceException;
    
}
