

package science.unlicense.api.device.usb;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public final class USBManagers {
    
    
    private USBManagers(){}
    
    
    /**
     * Lists available usb managers.
     * There is normaly only one.
     * 
     * @return array of ImageFormat, never null but can be empty.
     */
    public static USBManager[] getManagers(){
        final NamedNode root = science.unlicense.system.System.get().getModuleManager().getMetadataRoot().search(new Chars("services/un.api.device.usb.USBManager"));
        if(root==null){
            return new USBManager[0];
        }
        return (USBManager[]) Nodes.getChildrenValues(root, null).toArray(USBManager.class);
    }
    
    /**
     * Get first USB Manager.
     * @return 
     */
    public static USBManager getManager(){
        final USBManager[] array = getManagers();
        if(array.length==0) return null;
        return array[0];
    }
    
}
