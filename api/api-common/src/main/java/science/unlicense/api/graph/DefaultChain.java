
package science.unlicense.api.graph;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * Default chain implementation.
 * 
 * @author Johann Sorel
 */
public class DefaultChain extends AbstractChain{

    private final Sequence edges = new ArraySequence();

    public DefaultChain() {
    }
    
    @Override
    public Sequence getEdges() {
        return edges;
    }
    
}
