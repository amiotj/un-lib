
package science.unlicense.api.buffer;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBuffer extends CObject implements Buffer{
    
    protected final int primitiveType;
    protected final NumberEncoding encoding;
    protected final BufferFactory factory;

    public AbstractBuffer(int primitiveType, NumberEncoding encoding, BufferFactory factory) {
        this.primitiveType = primitiveType;
        this.encoding = encoding;
        this.factory = factory;
    }

    public BufferFactory getFactory() {
        return factory;
    }
    
    public int getPrimitiveType() {
        return primitiveType;
    }

    public NumberEncoding getEncoding() {
        return encoding;
    }
    
    public long getPrimitiveCount() {
        return getPrimitiveCount(primitiveType);
    }
    
    public long getPrimitiveCount(int primitiveType) {
        final long byteLength = getBufferByteSize();
        return (long) (byteLength/Primitive.getSizeInBytes(primitiveType));
    }

    public DataCursor cursor() {
        return cursor(primitiveType,encoding);
    }

    public DataCursor cursor(int primitive, NumberEncoding encoding) {
        return new DefaultCursor(this,encoding);
    }

    
    public IntCursor cursorInt(){
        return new DefaultIntCursor(this, encoding);
    }
    
    public FloatCursor cursorFloat(){
        return new DefaultFloatCursor(this, encoding);
    }
    
    public DoubleCursor cursorDouble(){
        return new DefaultDoubleCursor(this, encoding);
    }
    
    public Buffer copy(int primitive, NumberEncoding encoding) {
        final double nbElement = getBufferByteSize() / Primitive.getSizeInBytes(primitive);
        final Buffer newBuffer = DefaultBufferFactory.create((int)nbElement, primitive, encoding);
        newBuffer.writeByte(toByteArray(), 0);
        return newBuffer;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public byte[] toByteArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_BYTE);
        final byte[] array = new byte[(int)size];
        readByte(array, 0);
        return array;
    }
    
    public int[] toUByteArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_UBYTE);
        final int[] array = new int[(int)size];
        readUByte(array, 0);
        return array;
    }
    
    public short[] toShortArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_SHORT);
        final short[] array = new short[(int)size];
        readShort(array, 0);
        return array;
    }
    
    public int[] toUShortArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_USHORT);
        final int[] array = new int[(int)size];
        readUShort(array, 0);
        return array;
    }
    
    public int[] toInt24Array(){
        final long size = getPrimitiveCount(Primitive.TYPE_INT24);
        final int[] array = new int[(int)size];
        readInt24(array, 0);
        return array;
    }
    
    public int[] toUInt24Array(){
        final long size = getPrimitiveCount(Primitive.TYPE_UINT24);
        final int[] array = new int[(int)size];
        readUInt24(array, 0);
        return array;
    }
    
    public int[] toIntArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_INT);
        final int[] array = new int[(int)size];
        readInt(array, 0);
        return array;
    }
    
    public long[] toUIntArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_UINT);
        final long[] array = new long[(int)size];
        readUInt(array, 0);
        return array;
    }
    
    public long[] toLongArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_LONG);
        final long[] array = new long[(int)size];
        readLong(array, 0);
        return array;
    }
    
    public float[] toFloatArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_FLOAT);
        final float[] array = new float[(int)size];
        readFloat(array, 0);
        return array;
    }
    
    public double[] toDoubleArray(){
        final long size = getPrimitiveCount(Primitive.TYPE_DOUBLE);
        final double[] array = new double[(int)size];
        readDouble(array, 0);
        return array;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public void readByte(byte[] array, long offset) {
        readByte(array, 0, array.length, offset);
    }

    public void readByte(byte[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readByte(offset+k);
        }
    }

    public int readUByte(long offset) {
        return readByte(offset) &0xFF;
    }

    public void readUByte(int[] array, long offset) {
        readUByte(array, 0, array.length, offset);
    }

    public void readUByte(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readUByte(offset+k);
        }
    }

    public short readShort(long offset) {
        final byte[] array = new byte[2];
        readByte(array, offset);
        return encoding.readShort(array, 0);
    }

    public void readShort(short[] array, long offset) {
        readShort(array, 0, array.length, offset);
    }

    public void readShort(short[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readShort(offset+k*2);
        }
    }

    public int readUShort(long offset) {
        final byte[] array = new byte[2];
        readByte(array, offset);
        return encoding.readUShort(array, 0);
    }

    public void readUShort(int[] array, long offset) {
        readUShort(array, 0, array.length, offset);
    }

    public void readUShort(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readUShort(offset+k*2);
        }
    }

    public int readInt24(long offset) {
        final byte[] array = new byte[3];
        readByte(array, offset);
        return encoding.readInt24(array, 0);
    }

    public void readInt24(int[] array, long offset) {
        readInt24(array, 0, array.length, offset);
    }

    public void readInt24(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readInt24(offset+k*3);
        }
    }

    public int readUInt24(long offset) {
        final byte[] array = new byte[3];
        readByte(array, offset);
        return encoding.readUInt24(array, 0);
    }

    public void readUInt24(int[] array, long offset) {
        readUInt24(array, 0, array.length, offset);
    }

    public void readUInt24(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readUInt24(offset+k*3);
        }
    }

    public int readInt(long offset) {
        final byte[] array = new byte[4];
        readByte(array, offset);
        return encoding.readInt(array, 0);
    }

    public void readInt(int[] array, long offset) {
        readInt(array, 0, array.length, offset);
    }

    public void readInt(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readInt(offset+k*4);
        }
    }

    public long readUInt(long offset) {
        final byte[] array = new byte[4];
        readByte(array, offset);
        return encoding.readUInt(array, 0);
    }

    public void readUInt(long[] array, long offset) {
        readUInt(array, 0, array.length, offset);
    }

    public void readUInt(long[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readUInt(offset+k*4);
        }
    }

    public long readLong(long offset) {
        final byte[] array = new byte[8];
        readByte(array, offset);
        return encoding.readLong(array, 0);
    }

    public void readLong(long[] array, long offset) {
        readLong(array, 0, array.length, offset);
    }

    public void readLong(long[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readLong(offset+k*8);
        }
    }

    public float readFloat(long offset) {
        final byte[] array = new byte[4];
        readByte(array, offset);
        return encoding.readFloat(array, 0);
    }

    public void readFloat(float[] array, long offset) {
        readFloat(array, 0, array.length, offset);
    }

    public void readFloat(float[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readFloat(offset+k*4);
        }
    }

    public double readDouble(long offset) {
        final byte[] array = new byte[8];
        readByte(array, offset);
        return encoding.readDouble(array, 0);
    }

    public void readDouble(double[] array, long offset) {
        readDouble(array, 0, array.length, offset);
    }

    public void readDouble(double[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            array[arrayOffset+k] = readDouble(offset+k*8);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public void writeByte(byte[] array, long offset) {
        writeByte(array, 0, array.length, offset);
    }

    public void writeByte(byte[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeByte(array[arrayOffset+k],offset+k);
        }
    }

    public void writeUByte(int value, long offset) {
        writeByte((byte)value, offset);
    }

    public void writeUByte(int[] array, long offset) {
        writeUByte(array, 0, array.length, offset);
    }

    public void writeUByte(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeUByte(array[arrayOffset+k],offset+k);
        }
    }

    public void writeShort(short value, long offset) {
        final byte[] array = new byte[2];
        encoding.writeShort(value, array, 0);
        writeByte(array, offset);
    }

    public void writeShort(short[] array, long offset) {
        writeShort(array, 0, array.length, offset);
    }

    public void writeShort(short[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeShort(array[arrayOffset+k],offset+k*2);
        }
    }

    public void writeUShort(int value, long offset) {
        final byte[] array = new byte[2];
        encoding.writeUShort(value, array, 0);
        writeByte(array, offset);
    }

    public void writeUShort(int[] array, long offset) {
        writeUShort(array, 0, array.length, offset);
    }

    public void writeUShort(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeUShort(array[arrayOffset+k],offset+k*2);
        }
    }

    public void writeInt24(int value, long offset) {
        final byte[] array = new byte[3];
        encoding.writeInt24(value, array, 0);
        writeByte(array, offset);
    }

    public void writeInt24(int[] array, long offset) {
        writeInt24(array, 0, array.length, offset);
    }

    public void writeInt24(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeInt24(array[arrayOffset+k],offset+k*3);
        }
    }

    public void writeUInt24(int value, long offset) {
        final byte[] array = new byte[3];
        encoding.writeUInt24(value, array, 0);
        writeByte(array, offset);
    }

    public void writeUInt24(int[] array, long offset) {
        writeUInt24(array, 0, array.length, offset);
    }

    public void writeUInt24(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeUInt24(array[arrayOffset+k],offset+k*3);
        }
    }

    public void writeInt(int value, long offset) {
        final byte[] array = new byte[4];
        encoding.writeInt(value, array, 0);
        writeByte(array, offset);
    }

    public void writeInt(int[] array, long offset) {
        writeInt(array, 0, array.length, offset);
    }

    public void writeInt(int[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeInt(array[arrayOffset+k],offset+k*4);
        }
    }

    public void writeUInt(long value, long offset) {
        final byte[] array = new byte[4];
        encoding.writeUInt(value, array, 0);
        writeByte(array, offset);
    }

    public void writeUInt(long[] array, long offset) {
        writeUInt(array, 0, array.length, offset);
    }

    public void writeUInt(long[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeUInt(array[arrayOffset+k],offset+k*4);
        }
    }

    public void writeLong(long value, long offset) {
        final byte[] array = new byte[8];
        encoding.writeLong(value, array, 0);
        writeByte(array, offset);
    }

    public void writeLong(long[] array, long offset) {
        writeLong(array, 0, array.length, offset);
    }

    public void writeLong(long[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeLong(array[arrayOffset+k],offset+k*8);
        }
    }

    public void writeFloat(float value, long offset) {
        final byte[] array = new byte[4];
        encoding.writeFloat(value, array, 0);
        writeByte(array, offset);
    }

    public void writeFloat(float[] array, long offset) {
        writeFloat(array, 0, array.length, offset);
    }

    public void writeFloat(float[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeFloat(array[arrayOffset+k],offset+k*4);
        }
    }

    public void writeDouble(double value, long offset) {
        final byte[] array = new byte[8];
        encoding.writeDouble(value, array, 0);
        writeByte(array, offset);
    }

    public void writeDouble(double[] array, long offset) {
        writeDouble(array, 0, array.length, offset);
    }

    public void writeDouble(double[] array, int arrayOffset, int length, long offset) {
        for(int k=0;k<length;k++){
            writeDouble(array[arrayOffset+k],offset+k*8);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Buffer other = (Buffer) obj;
        if (this.primitiveType != other.getPrimitiveType()) {
            return false;
        }
        if (this.encoding != other.getEncoding() && (this.encoding == null || !this.encoding.equals(other.getEncoding()))) {
            return false;
        }
        
        if(!Arrays.equals(toByteArray(), other.toByteArray())){
            return false;
        }
        
        return true;
    }

    public int getHash() {
        int hash = 5;
        hash = 13 * hash + this.primitiveType;
        hash = 13 * hash + (this.encoding != null ? this.encoding.hashCode() : 0);
        return hash;
    }
    
}