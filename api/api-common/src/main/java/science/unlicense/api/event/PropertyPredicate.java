
package science.unlicense.api.event;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Predicate;

/**
 * Predicate for a single property.
 * 
 * @author Johann Sorel
 */
public class PropertyPredicate extends CObject implements Predicate {

    private final Chars propName;

    public PropertyPredicate(Chars propName) {
        this.propName = propName;
    }

    public Chars getPropertyName() {
        return propName;
    }
    
    public Boolean evaluate(Object candidate) {
        if (candidate instanceof Event) {
            final EventMessage message = ((Event) candidate).getMessage();
            return message instanceof PropertyMessage && propName.equals(((PropertyMessage)message).getPropertyName());
        }
        return false;
    }

    public int getHash() {
        int hash = 7;
        hash = 61 * hash + (this.propName != null ? this.propName.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PropertyPredicate other = (PropertyPredicate) obj;
        if (this.propName != other.propName && (this.propName == null || !this.propName.equals(other.propName))) {
            return false;
        }
        return true;
    }

}
