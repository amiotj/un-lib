
package science.unlicense.api.event;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.predicate.VariableSyncException;
import science.unlicense.api.predicate.Variables;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractProperty implements Property {
    
    private final EventSource source;
    private final Chars name;
    private final Class valueClass;
    private final boolean readable;
    private final boolean writable;

    public AbstractProperty(EventSource source, Chars name, Class valueClass, boolean readable, boolean writable) {
        this.source = source;
        this.name = name;
        this.valueClass = valueClass;
        this.readable = readable;
        this.writable = writable;
    }

    public EventSource getHolder() {
        return source;
    }
    
    public Chars getName() {
        return name;
    }

    public Class getValueClass(){
        return valueClass;
    }
        
    public boolean isReadable(){
        return readable;
    }
    
    public boolean isWritable(){
        return writable;
    }
        
    public void sync(Variable var) {
        sync(var, false);
    }

    public void sync(Variable var, boolean readOnly) {
        final Variables.VarSync sync = getVarSync();
        if(sync!=null) throw new VariableSyncException("Variable is already synchronized");
        Variables.sync(this, var, readOnly);
    }
    
    public void unsync() {
        final Variables.VarSync sync = getVarSync();
        if(sync!=null) sync.release();
    }
    
    private Variables.VarSync getVarSync(){
        final EventListener[] listeners = source.getListeners(new PropertyPredicate(name));
        if(listeners.length==0) return null;
        for(int i=0;i<listeners.length;i++){
            if(listeners[i] instanceof Variables.VarSync){
                final Variables.VarSync sync = ((Variables.VarSync)listeners[i]);
                if(sync.getVar1().equals(this)){
                    return sync;
                }
            }
        }
        return null;
    }
     
    public void addListener(EventListener listener){
        addEventListener(new PropertyPredicate(name), listener);
    }
    
    public void removeListener(EventListener listener){
        source.removeEventListener(new PropertyPredicate(name), listener);
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    public void addEventListener(Predicate predicate, EventListener listener) {
        source.addEventListener(predicate, listener);
    }

    public void removeEventListener(Predicate predicate, EventListener listener) {
        source.removeEventListener(predicate, listener);
    }

    public EventListener[] getListeners(Predicate predicate) {
        return source.getListeners(predicate);
    }
    
}
