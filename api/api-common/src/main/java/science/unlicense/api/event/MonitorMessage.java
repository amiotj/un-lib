
package science.unlicense.api.event;

import science.unlicense.api.character.Chars;

/**
 * Listen to object progress.
 *
 * @author Johann Sorel
 */
public interface MonitorMessage extends EventMessage {

    public static final int STATE_START = 0;
    public static final int STATE_PROGRESS = 1;
    public static final int STATE_END = 2;
    public static final int STATE_CANCEL = 3;
    public static final int STATE_FAIL = 4;

    /**
     * Object state.
     * @return int
     */
    int getState();

    /**
     * Object progress.
     * @return float between 0 and 1.
     */
    float getProgress();

    /**
     * Object progress message.
     * @return Chars, may be null.
     */
    Chars getMessage();

    /**
     * Get the intermediate results.
     * Some processing states may start returning result even if they are not finished.
     * @return Parameter, can be null
     */
    Object getResult();
    
    /**
     * Object progress exception if failed or minor error occurred.
     * @return Exception, can be null.
     */
    Exception getError();

}
