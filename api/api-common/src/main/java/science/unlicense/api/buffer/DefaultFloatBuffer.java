
package science.unlicense.api.buffer;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloatBuffer extends AbstractBuffer{

    private static final int[] MASKS = {
        0xFF000000,
        0x00FF0000,
        0x0000FF00,
        0x000000FF};
    private static final int[] OFFSET = {
        24,16,8,0};
    
    private final float[] buffer;

    public DefaultFloatBuffer(float[] buffer) {
        super(Primitive.TYPE_FLOAT, NumberEncoding.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }
    
    public boolean isWritable() {
        return true;
    }

    public Object getBackEnd() {
        return buffer;
    }

    public long getBufferByteSize() {
        return buffer.length*4;
    }

    public byte readByte(long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        final float f = buffer[index];
        final int i = Float.floatToRawIntBits(f);
        return (byte)( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    public float readFloat(long offset) {
        if(offset%4==0){
            return buffer[(int)(offset/4)];
        }else{
            return super.readFloat(offset);
        }
    }

    public void readFloat(float[] array, int arrayOffset, int length, long offset) {
        if(offset%4==0){
            Arrays.copy(buffer, (int)(offset/4), length, array, arrayOffset);
        }else{
            super.readFloat(array, arrayOffset, length, offset);
        }
    }
    
    public void writeByte(byte value, long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        final float f = buffer[index];
        int i = Float.floatToRawIntBits(f);
        i &= ~MASKS[mod];
        i |= (value&0xFF) << OFFSET[mod];
        buffer[index] = Float.intBitsToFloat(i);
    }

    public void writeFloat(float value, long offset) {
        if(offset%4==0){
            buffer[(int)(offset/4)] = value;
        }else{
            super.writeFloat(value, offset);
        }
    }

    public void writeFloat(float[] array, int arrayOffset, int length, long offset) {
        if(offset%4==0){
            Arrays.copy(array, arrayOffset, length, buffer, (int)(offset/4));
        }else{
            super.writeFloat(array, arrayOffset, length, offset);
        }
    }

    public Buffer copy() {
        return new DefaultFloatBuffer(Arrays.copy(buffer));
    }

}
