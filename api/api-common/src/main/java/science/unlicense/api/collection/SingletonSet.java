
package science.unlicense.api.collection;

import science.unlicense.api.exception.UnmodifiableException;

/**
 *  Optimized singleton set.
 *
 * @author Johann Sorel
 */
class SingletonSet extends AbstractCollection implements Set {

    private final Object singleton;

    public SingletonSet(Object singleton) {
        this.singleton = singleton;
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Singleton set, writing not supported");
    }

    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Single set, writing not supported");
    }

    public Iterator createIterator() {
        return Collections.singletonIterator(singleton);
    }

    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Single set, writing not supported");
    }

    public int getSize() {
        return 1;
    }

}
