package science.unlicense.api;

import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;

/**
 * Java classes utilities.
 *
 * @author Johann Sorel
 */
public class Classes {

    public static Set getAllImplementOrExtendClasses(Class candidate) {
        final HashSet hs = new HashSet();
        loopAllImplementOrExtendClasses(candidate, hs);
        return hs;
    }
    
    private static void loopAllImplementOrExtendClasses(Class candidate, Set hs) {
        //loop on parent hierarchy
        do{
            hs.add(candidate);
            final Class[] interfaces = candidate.getInterfaces();
            for(int i=0;i<interfaces.length;i++){
                loopAllImplementOrExtendClasses(interfaces[i],hs);
            }
            candidate = candidate.getSuperclass();
        }while(candidate != null);
    }

}
