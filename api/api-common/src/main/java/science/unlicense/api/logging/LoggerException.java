
package science.unlicense.api.logging;

/**
 * Logging exception.
 * 
 * @author Johann Sorel
 */
public class LoggerException extends Exception{
    
}
