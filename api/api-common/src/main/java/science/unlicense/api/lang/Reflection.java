
package science.unlicense.api.lang;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * Utility methods for reflection.
 *
 * @author Johann Sorel
 */
public final class Reflection {

    private Reflection(){}

    /**
     * Get all implemented interfaces.
     *
     * @return
     */
    public static Sequence getInterfaces(Class clazz){
        final Sequence seq = new ArraySequence();
        getInterfaces(clazz, seq);
        return seq;
    }

    private static void getInterfaces(Class clazz, Sequence seq){
        if(clazz==null) return;
        if(clazz.isInterface()){
            seq.add(clazz);
        }

        for(Class c : clazz.getInterfaces()) getInterfaces(c,seq);
        getInterfaces(clazz.getSuperclass(),seq);
    }

}
