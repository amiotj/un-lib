
package science.unlicense.api.number;

import science.unlicense.api.exception.UnimplementedException;

/**
 * 32bit decimal, called float.
 * Encoded in IEEE754
 * http://en.wikipedia.org/wiki/Single-precision_floating-point_format
 *
 * @author Johann Sorel
 */
public final class Float32 implements Number {

    public static final int MASK_FRACTION = 0x007FFFFFF;
    public static final int MASK_EXPONENT = 0x7F800000;
    public static final int MASK_SIGN = 1 >> 31;

    public static final int FRACTION_SIZE = 23;
    public static final int EXPONENT_SIZE = 8;
    public static final int EXP_BIAS = 127;


    public static IEEE754 decompose(float value){
        return decompose(value, null);
    }

    public static IEEE754 decompose(float value, IEEE754 buffer){
        if(buffer == null) buffer = new IEEE754();
        final int bits = Float.floatToRawIntBits(value);
        buffer.fraction = bits & MASK_FRACTION;
        buffer.exponent = ((bits & MASK_EXPONENT) >> FRACTION_SIZE);
        buffer.sign = bits >> 31;

        return buffer;
    }

    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    public int toInteger() {
        throw new UnimplementedException("Not supported yet.");
    }

    public long toLong() {
        throw new UnimplementedException("Not supported yet.");
    }

    public float toFloat() {
        throw new UnimplementedException("Not supported yet.");
    }

    public double toDouble() {
        throw new UnimplementedException("Not supported yet.");
    }

}