
package science.unlicense.api.model.tree;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.MultiplicityType;

/**
 *
 * @author Johann Sorel
 */
public interface NodeCardinality extends MultiplicityType {

    NodeType getType();

    Chars toChars();

    /**
     * Get a char sequence representation.
     * @param depth : maximum sub nodes depth to print.
     * @return CharSequence
     */
    Chars toCharsTree(int depth);

}
