
package science.unlicense.api.model.doc;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.MultiplicityType;
import science.unlicense.api.model.ValueType;

/**
 *
 * @author Johann Sorel
 */
public interface FieldType extends ValueType, MultiplicityType {
    
    /**
     * If the value type is of type Document the reference type
     * indicate the type of document referenced.
     * 
     * @return DocumentType
     */
    DocumentType getReferenceType();

    /**
     * Dictionary of field attributes.
     * Keys are of Chars type.
     *
     * @return Dictionary, never null, can be empty.
     */
    Dictionary getAttributes();

    /**
     * Parameter constraints.
     * @return Sequence, never null.
     */
    Sequence getConstraints();
    
}
