
package science.unlicense.api.model.tree;

/**
 * A Visitor, allows to recursivly explore a tree model.
 * 
 * @author Johann Sorel
 */
public interface NodeVisitor {
    
    /**
     * Visit the given node.
     * 
     * @param node, node to visit, not null
     * @param context, context information object, may be null.
     * @return visit result, may be null
     */
    Object visit(Node node, Object context);
    
}
