
package science.unlicense.api.buffer;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractDataCursor extends AbstractCursor implements DataCursor {

    //used for type conversion
    private final byte[] buffer = new byte[8];
    
    public AbstractDataCursor(NumberEncoding encoding) {
        super(encoding);
    }
    
    public void skipToByteEnd() {
        final int offset = getBitOffset();
        if(offset!=0) skipBits(8-offset);
    }

    public void skipBits(long nb) {
        final long nbBit = nb % 8;
        final long nbByte = nb / 8;
        skipBytes(nbByte);
        readBit((int)nbBit);
    }

    public void skipByte() {
        skipBytes(1);
    }
    
    public void skipBytes(long nb) {
        while(nb>0){
            if(nb>8){
                readByte(buffer,0,8);
            }else{
                readByte(buffer,0,(int)nb);
            }
            nb-=8;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    
    public int readBit() {
        return readBit(1,Buffer.LEFT_TO_RIGHT);
    }

    public int readBit(int nbBits) {
        return readBit(nbBits,Buffer.LEFT_TO_RIGHT);
    }
    
    public int[] readBit(int[] buffer, int nbBits) {
        return readBit(buffer, 0, buffer.length, nbBits);
    }

    public int[] readBit(int[] buffer, int offset, int length, int nbBits) {
        for(int i=0;i<length;i++){
            buffer[offset+i] = readBit(nbBits);
        }
        return buffer;
    }

    public void readByte(byte[] array) {
        readByte(array, 0, array.length);
    }

    public void readByte(byte[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readByte();
        }
    }

    public int readUByte() {
        readByte(buffer, 0, 1);
        return encoding.readUByte(buffer, 0);
    }

    public void readUByte(int[] array) {
        readUByte(array, 0, array.length);
    }

    public void readUByte(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readUByte();
        }
    }

    public short readShort() {
        readByte(buffer, 0, 2);
        return encoding.readShort(buffer, 0);
    }

    public void readShort(short[] array) {
        readShort(array, 0, array.length);
    }

    public void readShort(short[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readShort();
        }
    }

    public int readUShort() {
        readByte(buffer, 0, 2);
        return encoding.readUShort(buffer, 0);
    }

    public void readUShort(int[] array) {
        readUShort(array, 0, array.length);
    }

    public void readUShort(int[] array, int arrayOffset, int length) {
        
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readUShort();
        }
    }

    public int readInt24() {
        readByte(buffer, 0, 3);
        return encoding.readInt24(buffer, 0);
    }

    public void readInt24(int[] array) {
        readInt24(array, 0, array.length);
    }

    public void readInt24(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readInt24();
        }
    }

    public int readUInt24() {
        readByte(buffer, 0, 3);
        return encoding.readUInt24(buffer, 0);
    }

    public void readUInt24(int[] array) {
        readUInt24(array, 0, array.length);
    }

    public void readUInt24(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readUInt24();
        }
    }

    public int readInt() {
        readByte(buffer, 0, 4);
        return encoding.readInt(buffer, 0);
    }

    public void readInt(int[] array) {
        readInt(array, 0, array.length);
    }

    public void readInt(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readInt();
        }
    }

    public long readUInt() {
        readByte(buffer, 0, 4);
        return encoding.readUInt(buffer, 0);
    }

    public void readUInt(long[] array) {
        readUInt(array, 0, array.length);
    }

    public void readUInt(long[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readUInt();
        }
    }

    public long readLong() {
        readByte(buffer, 0, 8);
        return encoding.readLong(buffer, 0);
    }

    public void readLong(long[] array) {
        readLong(array, 0, array.length);
    }

    public void readLong(long[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readLong();
        }
    }

    public float readFloat() {
        readByte(buffer, 0, 4);
        return encoding.readFloat(buffer, 0);
    }

    public void readFloat(float[] array) {
        readFloat(array, 0, array.length);
    }

    public void readFloat(float[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readFloat();
        }
    }

    public double readDouble() {
        readByte(buffer, 0, 8);
        return encoding.readDouble(buffer, 0);
    }

    public void readDouble(double[] array) {
        readDouble(array, 0, array.length);
    }

    public void readDouble(double[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            array[i] = readDouble();
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    
    public DataCursor writeByte(byte[] array) {
        return writeByte(array, 0, array.length);
    }

    public DataCursor writeByte(byte[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeByte(array[i]);
        }
        return this;
    }

    public DataCursor writeUByte(int value) {
        return writeByte((byte)value);
    }

    public DataCursor writeUByte(int[] array) {
        return writeUByte(array, 0, array.length);
    }

    public DataCursor writeUByte(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeUByte(array[i]);
        }
        return this;
    }

    public DataCursor writeShort(short value) {
        encoding.writeShort(value, buffer, 0);
        return writeByte(buffer,0,2);
    }

    public DataCursor writeShort(short[] array) {
        return writeShort(array, 0, array.length);
    }

    public DataCursor writeShort(short[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeShort(array[i]);
        }
        return this;
    }

    public DataCursor writeUShort(int value) {
        encoding.writeUShort(value, buffer, 0);
        return writeByte(buffer,0,2);
    }

    public DataCursor writeUShort(int[] array) {
        return writeUShort(array, 0, array.length);
    }

    public DataCursor writeUShort(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeUShort(array[i]);
        }
        return this;
    }

    public DataCursor writeInt24(int value) {
        encoding.writeInt24(value, buffer, 0);
        return writeByte(buffer,0,3);
    }

    public DataCursor writeInt24(int[] array) {
        return writeInt24(array, 0, array.length);
    }

    public DataCursor writeInt24(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeInt24(array[i]);
        }
        return this;
    }

    public DataCursor writeUInt24(int value) {
        encoding.writeUInt24(value, buffer, 0);
        return writeByte(buffer,0,3);
    }

    public DataCursor writeUInt24(int[] array) {
        return writeUInt24(array, 0, array.length);
    }

    public DataCursor writeUInt24(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeUInt24(array[i]);
        }
        return this;
    }

    public DataCursor writeInt(int value) {
        encoding.writeInt(value, buffer, 0);
        return writeByte(buffer,0,4);
    }

    public DataCursor writeInt(int[] array) {
        return writeInt(array, 0, array.length);
    }

    public DataCursor writeInt(int[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeInt(array[i]);
        }
        return this;
    }

    public DataCursor writeUInt(long value) {
        encoding.writeUInt(value, buffer, 0);
        return writeByte(buffer,0,4);
    }

    public DataCursor writeUInt(long[] array) {
        return writeUInt(array, 0, array.length);
    }

    public DataCursor writeUInt(long[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeUInt(array[i]);
        }
        return this;
    }

    public DataCursor writeLong(long value) {
        encoding.writeLong(value, buffer, 0);
        return writeByte(buffer,0,8);
    }

    public DataCursor writeLong(long[] array) {
        return writeLong(array, 0, array.length);
    }

    public DataCursor writeLong(long[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeLong(array[i]);
        }
        return this;
    }

    public DataCursor writeFloat(float value) {
        encoding.writeFloat(value, buffer, 0);
        return writeByte(buffer,0,4);
    }

    public DataCursor writeFloat(float[] array) {
        return writeFloat(array, 0, array.length);
    }

    public DataCursor writeFloat(float[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeFloat(array[i]);
        }
        return this;
    }

    public DataCursor writeDouble(double value) {
        encoding.writeDouble(value, buffer, 0);
        return writeByte(buffer,0,8);
    }

    public DataCursor writeDouble(double[] array) {
        return writeDouble(array, 0, array.length);
    }

    public DataCursor writeDouble(double[] array, int arrayOffset, int length) {
        for(int i=arrayOffset,n=arrayOffset+length;i<n;i++){
            writeDouble(array[i]);
        }
        return this;
    }
    
}