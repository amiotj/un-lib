
package science.unlicense.api.model.doc;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;

/**
 * Single property of a document.
 *
 * @author Johann Sorel
 */
public interface Field extends Variable {

    /**
     * Get field name.
     *
     * @return name, never null
     */
    Chars getName();

    /**
     * Get field type.
     *
     * @return field type, can be null if field is not defined in the document type.
     */
    FieldType getType();
        
}
