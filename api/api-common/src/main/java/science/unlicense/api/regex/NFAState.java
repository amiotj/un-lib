

package science.unlicense.api.regex;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.Languages;

/**
 * Thompson NFA automaton state.
 * 
 * @author Johann Sorel
 */
public interface NFAState {
        
    /**
     * A forward state, is a state with only one next state.
     * Opposite of a fork which have 2 possible states.
     */
    public interface Forward extends NFAState{
        NFAState getNextState();
    }
    
    /**
     * Special kind of token which are not processed, they are usually subclassed
     * to store informations in the nfa path.
     */
    public interface FastForward extends Forward{
        
    }
    
    public interface Evaluator extends Forward{
                
        boolean evaluate(Object cp);
                
    }
    
    public interface Fork extends NFAState{
        NFAState getNext1State();
        NFAState getNext2State();
    }
    
    
    static abstract class AbstractForward extends CObject implements NFAState{
                
        protected final NextRef next = new NextRef(null);

        public NextRef getNextRef() {
            return next;
        }

        public final NFAState getNextState() {
            return getNextRef().state;
        }
        
    }
    
    public static class DefaultFastForward extends AbstractForward implements FastForward{
        
        public DefaultFastForward(){
        }
                
        public Chars toChars() {
            return new Chars("FastForward");
        }
        
    }
    
    /**
     * Marker state to indicate the successful end.
     */
    public static final class Match extends AbstractForward{
        
        public final Chars name;
        
        public Match(){
            this.name = Chars.EMPTY;
        }
        
        public Match(Chars name) {
            this.name = name;
        }

        public Chars toChars() {
            return new Chars("MATCH(").concat(name).concat(')');
        }
        
    }
        
    /**
     * Case-sensitive char.
     */
    public static final class Char extends AbstractForward implements Evaluator{
        
        public final int cp;
        
        public Char(int cp) {
            this.cp = cp;
        }
        
        public boolean evaluate(Object cp) {
            return this.cp == (Integer)cp;
        }
                
        public Chars toChars() {
            return new Chars("Char[").concat(cp).concat(']');
        }
    }
    
    /**
     * Case-insensitive char.
     */
    public static final class InsChar extends AbstractForward implements Evaluator{
        
        public final int cpLower;
        public final int cpUpper;
        
        public InsChar(int cp) {
            this.cpLower = Languages.UNSET.toLowerCase(cp);
            this.cpUpper = Languages.UNSET.toUpperCase(cp);
        }
        
        public boolean evaluate(Object cp) {
            final int i = (Integer)cp;
            return this.cpLower == i || this.cpUpper == i;
        }
                
        public Chars toChars() {
            return new Chars("InsChar[").concat(cpLower).concat(']');
        }
    }
    
    /**
     * Case-insensitive char.
     */
    public static final class AnyChar extends AbstractForward implements Evaluator{
                
        public AnyChar() {
        }
        
        public boolean evaluate(Object cp) {
            return true;
        }
                
        public Chars toChars() {
            return new Chars("AnyChar");
        }
    }
    
    public static final class Range extends AbstractForward implements Evaluator{
        
        public final int start;
        public final int end;
        public final boolean negate;
        
        public Range(int start, int end) {
            this(start,end,false);
        }
        
        public Range(int start, int end, boolean negate) {
            this.start = start;
            this.end = end;
            this.negate = negate;
        }
        
        public boolean evaluate(Object cp) {
            int icp = (Integer)cp;
            final boolean val = start <= icp && icp <= end;
            return negate ? !val : val;
        }
                
        public Chars toChars() {
            int nstr = negate? '-' : '+';
            return new Chars("Range").concat(nstr).concat('[').concat(start).concat('-').concat(end).concat(']');
        }
    }
    
    public static final class List extends AbstractForward implements Evaluator{
        
        public final int[] values;
        public final boolean negate;
                
        public List(int[] values, boolean negate) {
            this.values = values;
            Arrays.sort(values);
            this.negate = negate;
        }
        
        public boolean evaluate(Object cp) {
            final boolean val = Arrays.contains(values, (Integer)cp);
            return negate ? !val : val;
        }
                
        public Chars toChars() {
            int nstr = negate? '-' : '+';
            return new Chars("List").concat(nstr).concat(Arrays.toChars(values));
        }
    }
    
    public static final class DefaultFork extends CObject implements Fork{
        
        public final NextRef next1 = new NextRef();
        public final NextRef next2 = new NextRef();
        
        public DefaultFork(NFAState state1) {
            this.next1.state = state1;
        }
        
        public DefaultFork(NFAState state1, NFAState state2) {
            this.next1.state = state1;
            this.next2.state = state2;
        }
        
        public Chars toChars() {
            return new Chars("Fork");
        }

        public NextRef getNext1Ref() {
            return next1;
        }

        public NFAState getNext1State() {
            return getNext1Ref().state;
        }

        public NextRef getNext2Ref() {
            return next2;
        }

        public NFAState getNext2State() {
            return getNext2Ref().state;
        }
    }
    
    public static class NextRef{
        public NFAState state;
        public NextRef() {}
        public NextRef(NFAState state) {
            this.state = state;
        }
    }
    
}
