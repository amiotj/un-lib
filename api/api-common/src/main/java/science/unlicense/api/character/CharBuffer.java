
package science.unlicense.api.character;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

import java.io.UnsupportedEncodingException;
import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.number.Int64;

/**
 * Used to build larger Character sequence from a stack of sequences.
 * @author Johann Sorel
 */
public final class CharBuffer extends CObject {

    private final Sequence sequence = new ArraySequence();
    private final CharEncoding encoding;
    private int size = 0;

    /**
     * CharBuffer with default encoding.
     */
    public CharBuffer(){
        this(CharEncodings.DEFAULT);
    }

    /**
     * Charbuffer initiated with given chars text and encoding.
     * @param chars initial text and encoding
     */
    public CharBuffer(Chars chars){
        this(chars.getEncoding());
        append(chars);
    }
    
    public CharBuffer(CharEncoding encoding){
        this.encoding = encoding;
    }

    public void reset(){
        sequence.removeAll();
        size = 0;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public CharBuffer append(final int unicode){
        append(encoding.toBytes(unicode));
        return this;
    }

    public CharBuffer append(final CharArray candidate){
        if(candidate==null)return this;
        append(candidate.toBytes(encoding));
        return this;
    }
    
    public CharBuffer append(final Chars candidate){
        if(candidate==null)return this;
        append(candidate.toBytes(encoding));
        return this;
    }

    public CharBuffer append(final Char candidate){
        if(candidate==null)return this;
        append(candidate.toBytes(encoding));
        return this;
    }

    public CharBuffer append(final Object candidate){
        if(candidate==null)return this;
        append(CObjects.toChars(candidate));
        return this;
    }
    
    public CharBuffer append(final CObject candidate){
        if(candidate==null)return this;
        append(candidate.toChars());
        return this;
    }

    /**
     * For JVM compatibility, to remove in the futur.
     * @param candidate string to append
     * @return this buffer
     */
    public CharBuffer append(final String candidate){
        if(candidate==null)return this;
        try {
            append(candidate.getBytes(encoding.getName().toString()));
        } catch (UnsupportedEncodingException ex) {
            Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
        }
        return this;
    }

    /**
     * Append given bytes, expected to be in the given encoding.
     * @param candidate encoded array to append
     * @return this buffer
     */
    public CharBuffer append(final byte[] candidate){
        if(candidate==null)return this;
        if(candidate.length == 0){
            //avoid wasting space
            return this;
        }
        sequence.add(candidate);
        size += candidate.length;
        return this;
    }

    public CharBuffer appendNumber(final int n){
        append(Int32.encode(n));
        return this;
    }
    
    public CharBuffer appendNumber(final long n){
        append(Int64.encode(n));
        return this;
    }
    
    public CharBuffer appendNumber(final float n){
        append(Float64.encode(n));
        return this;
    }
    
    public CharBuffer appendNumber(final double n){
        append(Float64.encode(n));
        return this;
    }
    
    public Chars toChars(){
        return new Chars(toBytes(),encoding);
    }

    public Chars toChars(CharEncoding encoding){
        return new Chars(toBytes(encoding),encoding);
    }

    public byte[] toBytes(){
        final byte[] buffer = new byte[size];
        int s=0;
        for(int i=0,n=sequence.getSize(); i<n; i++){
            final byte[] data = (byte[])sequence.get(i);
            Arrays.copy(data, 0, data.length, buffer, s);
            s+= data.length;
        }
        return buffer;
    }

    public byte[] toBytes(CharEncoding encoding){
        final byte[] buffer = toBytes();
        if(this.encoding.equals(encoding)){
            return buffer;
        }else{
            return Characters.recode(buffer, this.encoding, encoding);
        }
    }

}
