
package science.unlicense.api.event;

/**
 *
 * @author Johann Sorel
 */
public interface EventMessage {

    /**
     * Some events may be consumable.
     * Once consumed they should be ignored or discarded.
     *
     * @return true if event consumable
     */
    boolean isConsumable();

    /**
     * @return true is event has been consumed.
     */
    boolean isConsumed();

    /**
     * Consume the event.
     */
    void consume();
    
}
