
package science.unlicense.api.graph;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractVertex implements Vertex {


    public Sequence getEdges() {
        final Graph graph = getGraph();
        return graph.getEdges(this);
    }

    public boolean isAdjacent(Vertex vertex) {
        final Iterator ite = getEdges().createIterator();
        while(ite.hasNext()){
            final Edge edge = (Edge) ite.next();
            if(edge.getVertex2().equals(vertex)){
                return true;
            }
        }
        return false;
    }

    public int getDegree() {
        return getInDegree() + getOutDegree();
    }

    public int getInDegree() {
        int count = 0;
        final Iterator ite = getEdges().createIterator();
        while(ite.hasNext()){
            final Edge edge = (Edge) ite.next();
            if(edge.getVertex2() == this){
                count++;
            }
        }
        return count;
    }

    public int getOutDegree() {
        int count = 0;
        final Iterator ite = getEdges().createIterator();
        while(ite.hasNext()){
            final Edge edge = (Edge) ite.next();
            if(edge.getVertex1() == this){
                count++;
            }
        }
        return count;
    }

    public boolean isIsolated() {
        return getDegree() == 0;
    }

    public boolean isPendant() {
        return getDegree() == 1;
    }

}
