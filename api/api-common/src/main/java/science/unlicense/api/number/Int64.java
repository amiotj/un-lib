
package science.unlicense.api.number;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public final class Int64 implements Number {
    
    public static Chars encode(long candidate) {
        final ByteSequence s = new ByteSequence();

        final boolean negative = (candidate < 0);
        if (negative) {
            candidate *= -1;
        }

        long remainder;
        while (true) {
            remainder = candidate % 10;
            candidate = candidate / 10;
            s.put((byte)(remainder+48));
            if (candidate == 0) {
                break;
            }
        }

        byte[] array = s.toArrayByte();
        Arrays.reverse(array, 0, array.length);
        if(negative){
            array = Arrays.copy(array, 0, array.length, new byte[array.length+1], 1);
            array[0] = 45;
        }
        return new Chars(array);
    }
    
    public static Chars encodeHexa(long candidate) {
        if (candidate == 0){
            return new Chars(new byte[]{48});
        }
        final ByteSequence buffer = new ByteSequence();
        while (candidate > 0) {
            buffer.put(Numbers.HEXA[(int)(candidate % 16)]);
            candidate = candidate / 16;
        }
        final byte[] bytes = buffer.toArrayByte();
        Arrays.reverse(bytes, 0, bytes.length);
        return new Chars(bytes);
    }
    
    public static long decode(Chars candidate) {
        return decode(candidate.createIterator(),true);
    }
    
    /**
     * 
     * @param candidate text de decode
     * @param fromIndex start offset
     * @param toIndex last character excluded, 
     *          or use -1 to stop on sequence end or invalid character.
     * @return decoded value
     */
    public static long decode(Chars candidate, int fromIndex, int toIndex) {
        return decode(candidate.createIterator(fromIndex,toIndex),true);
    }

    public static long decode(CharIterator ite, boolean strict){
        if(!ite.hasNext()){
            throw new RuntimeException("Not an number");
        }
        long value = 0;
        int sign = 1;
        
        int c = ite.peekToUnicode();
        if(c == 43){ // +
            ite.skip();
        }else if(c == 45){ // -
            sign = -1;
            ite.skip();
        }

        while(ite.hasNext()){
            c = ite.peekToUnicode();
            if(c < 48 || c > 57){
                if(strict){
                    throw new RuntimeException("Not a number");
                }else{
                    break;
                }
            }
            value *= 10;
            value += c - 48;
            ite.skip();
        }

        value *= sign;
        return value;
    }
    
    public static long decodeHexa(Chars candidate) {
        candidate = candidate.recode(CharEncodings.US_ASCII).toUpperCase();
        final CharIterator ite = candidate.createIterator();
        return decodeHexa(ite);
    }
    
    public static long decodeHexa(Chars candidate, int fromIndex, int toIndex) {
        candidate = candidate.recode(CharEncodings.US_ASCII).toUpperCase();
        final CharIterator ite = candidate.createIterator(fromIndex,toIndex);
        return decodeHexa(ite);
    }
    
    public static long decodeHexa(CharIterator ite) {
        long value = 0;
        while(ite.hasNext()){
            int b = ite.nextToBytes()[0] - 48;
            if(b>9) b-=7;
            value = 16*value + b;
        }
        return value;
    }

    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    public int toInteger() {
        throw new UnimplementedException("Not supported yet.");
    }

    public long toLong() {
        throw new UnimplementedException("Not supported yet.");
    }

    public float toFloat() {
        throw new UnimplementedException("Not supported yet.");
    }

    public double toDouble() {
        throw new UnimplementedException("Not supported yet.");
    }
}
