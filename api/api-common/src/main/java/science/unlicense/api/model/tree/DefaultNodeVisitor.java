
package science.unlicense.api.model.tree;

import science.unlicense.api.collection.Iterator;

/**
 * Default Node visitor.
 * Recursively visit each child nodes.
 * Extends this class to add specific operations.
 * 
 * @author Johann Sorel
 */
public class DefaultNodeVisitor implements NodeVisitor{

    public Object visit(Node node, Object context) {
        
        Object result = null;
        final Iterator children = node.getChildren().createIterator();
        while(children.hasNext()) {
            Object next = children.next();
            if(next!=null){
                result = ((Node)next).accept(this, context);
            }
        }
        return result;
    }
    
}
