
package science.unlicense.api.character;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;

import java.io.UnsupportedEncodingException;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 * Default character array implementation.
 *
 * @author Johann Sorel
 */
public final class Chars extends AbstractCharArray {

    public static final Chars EMPTY = new Chars(new byte[0]);
    
    final CharEncoding encoding;
    final byte[] data;

    public Chars(byte[] data, final CharEncoding encoding) {
        if(data == null){
            throw new NullPointerException("Byte array must not be null.");
        }
        if(encoding == null){
            throw new NullPointerException("Char encoding must not be null.");
        }

        this.encoding = encoding;
        this.data = data;
    }

    /**
     * Character sequence in US-ASCII.
     * @param data encoded array
     */
    public Chars(byte[] data) {
        if(data == null){
            throw new NullPointerException("Byte array must not be null.");
        }
        this.encoding = null;
        this.data = data;
    }

    /**
     * Default encoding char sequence.
     * @param codepoints characters as codepoints.
     */
    public Chars(int[] codepoints) {
        this(codepoints,CharEncodings.DEFAULT);
    }

    public Chars(int[] codepoints, final CharEncoding encoding) {
        if(encoding == null){
            throw new NullPointerException("Char encoding must not be null.");
        }
        this.encoding = encoding;
        this.data = encoding.toBytes(codepoints);
    }

    /**
     * Only provided while dependent of the jvm.
     * @param data java string to copy
     */
    public Chars(final String data) {
        this(data,CharEncodings.UTF_8);
    }

    /**
     * Only provided while dependent of the jvm.
     * @param data java string to copy
     * @param encoding store encoding
     */
    public Chars(final String data, final CharEncoding encoding) {
        if(data == null){
            throw new NullPointerException("String must not be null.");
        }
        if(encoding == null){
            throw new NullPointerException("Char encoding must not be null.");
        }

        this.encoding = encoding;
        try{
            final byte[] buffer = data.getBytes("UTF-8");
            this.data = Characters.recode(buffer, CharEncodings.UTF_8, encoding);
        }catch(Exception ex){
            throw new RuntimeException("Should not happen.",ex);
        }
    }

    /**
     * Copy characters from given char array.
     * @param ca array to copy
     */
    public Chars(CharArray ca){
        this.encoding = ca.getEncoding();
        this.data = ca.toBytes();
    }

    protected boolean isByteInternalCopy() {
        return false;
    }

    protected byte[] getBytesInternal() {
        return data;
    }
    
    /**
     * {@inheritDoc}
     */
    public CharEncoding getEncoding() {
        if(encoding == null){
            return CharEncodings.US_ASCII;
        }
        return encoding;
    }

    /**
     * {@inheritDoc}
     */
    public int getByteLength(){
        return data.length;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEmpty(){
        return data.length == 0;
    }

    /**
     * {@inheritDoc}
     */
    public Char getCharacter(int index){
        final int offset = toByteOffset(index);
        final CharEncoding encoding = getEncoding();
        final int size = encoding.charlength(data, offset);
        return new Char(Arrays.copy(data, offset, size, new byte[size],0), encoding);
    }

    /**
     * {@inheritDoc}
     */
    public int getUnicode(int index){
        final int offset = toByteOffset(index);
        final int[] cp = new int[2];
        getEncoding().toUnicode(data, cp, offset);
        return cp[0];
    }

    /**
     * {@inheritDoc}
     */
    public byte getByte(int index){
        return data[index];
    }

    /**
     * {@inheritDoc}
     */
    public Chars truncate(int start, int end){
        if(end!=-1 && end<start) throw new InvalidArgumentException("Unvalid indexes start:"+start+" end:"+end);
        final CharEncoding encoding = getEncoding();
        int offset = 0;
        for(int i=0;i<start;i++){
            offset += encoding.charlength(data, offset);
        }
        int offsetend;
        if (end==-1) {
            offsetend = getByteLength();
        } else {
            offsetend = offset;
            for(int i=start;i<end;i++){
                offsetend += encoding.charlength(data, offsetend);
            }
        }

        final int datasize = offsetend-offset;
        return new Chars(Arrays.copy(data,offset,datasize,new byte[datasize],0),encoding);
    }

    /**
     * {@inheritDoc}
     */
    public boolean startsWith(CharArray cs){
        return startsWith(toThisEncodingBytes(cs));
    }

    /**
     * {@inheritDoc}
     */
    public boolean startsWith(Char ch){
        return startsWith(ch.toEncodingBytes(getEncoding()));
    }

    /**
     * {@inheritDoc}
     */
    public boolean startsWith(byte[] cdata){
        return Arrays.equals(cdata,0,cdata.length,data,0);
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(CharArray cs){
        return endsWith(toThisEncodingBytes(cs));
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(Char ch){
        return endsWith(ch.toEncodingBytes(getEncoding()));
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(int ch){
        return endsWith(getEncoding().toBytes(ch));
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(byte[] cdata){
        return Arrays.equals(cdata,0,cdata.length,data,data.length-cdata.length);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(CharArray cs){
        return getFirstOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(CharArray cs, int startPosition){
        return getFirstOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(Char ch){
        return getFirstOccurence(ch.toEncodingBytes(getEncoding()), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(Char ch, int startPosition){
        return getFirstOccurence(ch.toEncodingBytes(getEncoding()), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(int codepoint){
        return getFirstOccurence(getEncoding().toBytes(codepoint),0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(int codepoint, int startPosition){
        return getFirstOccurence(getEncoding().toBytes(codepoint),startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(byte[] cd){
        return getFirstOccurence(cd, 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(byte[] cd, int startPosition){
        final CharEncoding enc = getEncoding();
        int offset = toByteOffset(startPosition);
        for(int charindex=0; offset<data.length;charindex++){
            if(Arrays.equals(cd,0,cd.length,data,offset)){
                //found it
                return startPosition+charindex;
            }
            offset += enc.charlength(data,offset);
        }

        //not found
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(CharArray cs){
        return getLastOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(CharArray cs, int startPosition){
        return getLastOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(Char ch){
        return getLastOccurence(ch.toEncodingBytes(getEncoding()), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(Char ch, int startPosition){
        return getLastOccurence(ch.toEncodingBytes(getEncoding()), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(int codepoint){
        return getLastOccurence(getEncoding().toBytes(codepoint),0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(int codepoint, int startPosition){
        return getLastOccurence(getEncoding().toBytes(codepoint),startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(byte[] cd){
        return getLastOccurence(cd, 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(byte[] cd, int startPosition){
        final CharEncoding enc = getEncoding();
        int offset = toByteOffset(startPosition);
        int lastOcc = -1;
        for(int charindex=0; offset<data.length;charindex++){
            if(Arrays.equals(cd,0,cd.length,data,offset)){
                //found it
                lastOcc = startPosition+charindex;
            }
            offset += enc.charlength(data,offset);
        }

        return lastOcc;
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(int original, int replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(int original, int replacement, int startPosition){
        return replaceAll(getEncoding().toBytes(original), getEncoding().toBytes(replacement), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(Char original, Char replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(Char original, Char replacement, int startPosition){
        return replaceAll(original.toBytes(getEncoding()), replacement.toBytes(getEncoding()), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(CharArray original, CharArray replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(CharArray original, CharArray replacement, int startPosition){
        return replaceAll(toThisEncodingBytes(original), toThisEncodingBytes(replacement), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(byte[] original, byte[] replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public Chars replaceAll(byte[] original, byte[] replacement, int startPosition){
        final CharEncoding enc = getEncoding();
        final CharBuffer buffer = new CharBuffer(enc);
        int offset = toByteOffset(startPosition);
        buffer.append(Arrays.copy(data, 0, offset));

        for(;offset<data.length;){
            if(Arrays.equals(original,0,original.length,data,offset)){
                //found one
                buffer.append(replacement);
                offset += original.length;
            }else{
                final int length = enc.charlength(data,offset);
                buffer.append(Arrays.copy(data, offset, length));
                offset += length;
            }
        }

        return buffer.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public byte[] toBytes(){
        return Arrays.copy(data, 0, data.length, new byte[data.length], 0);
    }

    /**
     * {@inheritDoc}
     */
    public byte[] toBytes(CharEncoding encoding){
        if(getEncoding().equals(encoding)){
            return toBytes();
        }else{
            return Characters.recode(data, getEncoding(), encoding);
        }
    }

    /**
     * {@inheritDoc}
     */
    public Chars toUpperCase(){
        return toUpperCase(Languages.UNSET);
    }

    /**
     * {@inheritDoc}
     */
    public Chars toUpperCase(CharCase lf){
        return lf.toUpperCase(this);
    }

    /**
     * {@inheritDoc}
     */
    public Chars toLowerCase(){
        return toLowerCase(Languages.UNSET);
    }

    /**
     * {@inheritDoc}
     */
    public Chars toLowerCase(CharCase lf){
        return lf.toLowerCase(this);
    }

    /**
     * {@inheritDoc}
     */
    public Chars trim(){
        final CharIterator ite = createIterator();
        int start = 0;
        int end = 0;
        int index = 0;
        while (ite.hasNext()) {
            int uni = ite.nextToUnicode();
            if (uni==9 || uni==32 || uni==10 || uni==13) {
                if(index==start){
                    start++;
                }
            } else {
                end = index+1;
            }
            index++;
        }
        if(end<start) {
            //empty string
            final CharEncoding enc = getEncoding();
            if (enc==CharEncodings.DEFAULT) {
                return Chars.EMPTY;
            } else {
                return new Chars(Arrays.ARRAY_BYTE_EMPTY,enc);
            }
        }
        return truncate(start, end);
    }

    /**
     * {@inheritDoc}
     */
    public Chars trimStart(){
        final CharIterator ite =createIterator();
        int start = 0;
        int end = getCharLength();
        while(ite.hasNext()){
            int uni = ite.nextToUnicode();
            if(uni==9 || uni==32 || uni==10 || uni==13){
                start++;
            }else{
                break;
            }
        }
        return truncate(start, end);
    }

    /**
     * {@inheritDoc}
     */
    public Chars trimEnd(){
        final CharIterator ite =createIterator();
        int start=0;
        int end =0;
        int index = 0;
        while(ite.hasNext()){
            int uni = ite.nextToUnicode();
            if(uni==9 || uni==32 || uni==10 || uni==13){
                if(index==start){
                    start++;
                }
            }else{
                end = index+1;
            }
            index++;
        }
        return truncate(0, end);
    }

    /**
     * {@inheritDoc}
     */
    public Chars concat(int unicode){
        final CharBuffer cb = new CharBuffer(getEncoding());
        cb.append(this);
        cb.append(unicode);
        return cb.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public Chars concat(Char other){
        final CharBuffer cb = new CharBuffer(getEncoding());
        cb.append(this);
        cb.append(other);
        return cb.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public Chars concat(CharArray other){
        final CharBuffer cb = new CharBuffer(getEncoding());
        cb.append(this);
        cb.append(other);
        return cb.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public Chars recode(final CharEncoding encoding){
        return Characters.recode(this,encoding);
    }

    /**
     * {@inheritDoc}
     */
    public CharIterator createIterator(){
        return new CharSequenceIterator(0,-1);
    }

    /**
     * {@inheritDoc}
     */
    public CharIterator createIterator(int fromIndex, int toIndex){
        return new CharSequenceIterator(fromIndex,toIndex);
    }

    /**
     * {@inheritDoc}
     */
    public Chars toChars() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public String toJVMString(){
        if(encoding == null){
            return new String(data);
        }
        try {
            return new String(data,getEncoding().getName().toString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj) {
        return obj==this || equalsInt(obj, false, true,null);
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding) {
        return obj==this || equalsInt(obj, ignoreCase, ignoreEncoding, null);
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding, CharCase language) {
        return obj==this || equalsInt(obj, ignoreCase, ignoreEncoding, language);
    }
    
    /**
     * {@inheritDoc}
     */
    private boolean equalsInt(Object obj, boolean ignoreCase, boolean ignoreEncoding, CharCase language) {
        
        if (!CharArray.class.isInstance(obj)){
            return false;
        }
        CharArray other = (CharArray) obj;

        //check hashes, we can do this because hashes are in a fixed encoding and case sensitive
        if(ignoreEncoding && !ignoreCase && getHash()!=other.getHash()){
            return false;
        }

        if(getCharLength()!=other.getCharLength()){
            return false;
        }

        //quick tests to avoid characters iteration
        if(!ignoreEncoding){
            if(!this.getEncoding().equals(other.getEncoding())){
                return false;
            }            
            if(!ignoreCase){
                //compare bytes directly
                if(other instanceof AbstractCharArray){
                    return Arrays.equals(this.data, ((AbstractCharArray)other).getBytesInternal());
                }else{
                    return Arrays.equals(this.data, other.toBytes());
                }
            }
        }

        final CharIterator ite1 = this.createIterator();
        final CharIterator ite2 = other.createIterator();

        if(ignoreCase){
            if(language == null){
                language = Languages.UNSET;
            }

            boolean n1,n2;
            int c1,c2;
            for(;;){
                n1 = ite1.hasNext();
                n2 = ite2.hasNext();
                if(n1 && n2){
                    c1 = ite1.nextToUnicode();
                    c2 = ite2.nextToUnicode();
                    if(c1==c2) continue;
                    if(language.toLowerCase(c1) == c2) continue;
                    if(language.toUpperCase(c1) == c2) continue;
                    if(language.toLowerCase(c2) == c1) continue;
                    if(language.toUpperCase(c2) == c1) continue;
                    return false;
                }
                //check both are not finished, same length
                return !n1 && !n2;
            }

        }else{

            boolean n1,n2;
            for(;;){
                n1 = ite1.hasNext();
                n2 = ite2.hasNext();
                if(n1 && n2){
                    if(ite1.nextToUnicode()==ite2.nextToUnicode()) continue;
                    return false;
                }
                //check both are not finished, same length
                return !n1 && !n2;
            }
        }

    }

    private byte[] toThisEncodingBytes(CharArray ch){
        byte[] cdata = ch.toBytes();
        final CharEncoding thisEnc = getEncoding();
        final CharEncoding otherEnc = ch.getEncoding();
        if(thisEnc != otherEnc){
            //different encoding,recode char array
            cdata = Characters.recode(cdata,otherEnc,thisEnc);
        }
        return cdata;
    }

    /**
     * {@inheritDoc}
     */
    public int toByteOffset(int charIndex){
        if(charIndex==0) return 0;
        int offset = 0;
        final CharEncoding encoding = getEncoding();
        for(int i=0;i<charIndex;i++){
            offset += encoding.charlength(data, offset);
        }
        return offset;
    }

    /**
     * {@inheritDoc}
     */
    public Chars[] split(int unicode){
        final Sequence parts = new ArraySequence();

        final CharEncoding enc = getEncoding();
        final byte[] cd = enc.toBytes(unicode);
        int last = 0;
        int offset = 0;
        for(; offset<data.length;){
            int charlength = enc.charlength(data,offset);
            if(Arrays.equals(cd,0,cd.length,data,offset)){
                //found one
                parts.add(new Chars(Arrays.copy(data, last, offset-last), enc));
                last = offset+charlength;
            }
            offset += charlength;
        }

        //last
        parts.add(new Chars(Arrays.copy(data, last, data.length-last), enc));

        final Chars[] cs = new Chars[parts.getSize()];
        Collections.copy(parts, cs, 0);
        return cs;

    }

    /**
     * {@inheritDoc}
     */
    public int order(Object other) {
        final Chars co = (Chars) other;

        final CharIterator ite = this.createIterator();
        final CharIterator oite = co.createIterator();

        while(ite.hasNext()){
            if(oite.hasNext()){
                final int uni = ite.nextToUnicode();
                final int ouni = oite.nextToUnicode();
                if(uni<ouni){
                    return -1;
                }else if(uni>ouni){
                    return +1;
                }
                //continue to next char
            }else{
                //other chars is shorter.
                return +1;
            }
        }

        if(oite.hasNext()){
            //other char is longer
            return -1;
        }else{
            //same length and same chars
            return 0;
        }
    }

    private final class CharSequenceIterator extends AbstractCharIterator{

        private final int[] buffer = new int[2];
        private int byteoffset;
        private final int toIndex;
        private int inc = 0;

        public CharSequenceIterator(int fromIndex, int toIndex) {
            super(Chars.this.getEncoding());
            this.byteoffset = toByteOffset(fromIndex);
            this.toIndex = toIndex;
            this.inc = fromIndex;
        }

        public boolean hasNext() {
            if(toIndex>0 && inc>=toIndex){
                return false;
            }
            return byteoffset < data.length;
        }

        public byte[] nextToBytes() {
            inc++;
            final int charsize = encoding.charlength(data,byteoffset);
            final byte[] cd = Arrays.copy(data,byteoffset,charsize,new byte[charsize],0);
            byteoffset+=charsize;
            return cd;
        }

        public void nextToBuffer(ByteSequence buffer) {
            inc++;
            final int charsize = encoding.charlength(data,byteoffset);
            buffer.put(data,byteoffset,charsize);
            byteoffset+=charsize;
        }

        public int nextToUnicode() {
            inc++;
            encoding.toUnicode(data, buffer, byteoffset);
            byteoffset+=buffer[1];
            return buffer[0];
        }

        public int[] nextBulk(ByteSequence bytebuffer) {
            encoding.toUnicode(data, buffer, byteoffset);
            bytebuffer.put(data,byteoffset,buffer[1]);
            inc++;
            byteoffset+=buffer[1];
            return buffer;
        }
        
        public void skip(){
            inc++;
            byteoffset += encoding.charlength(data,byteoffset);
        }

        public byte[] peekToBytes() {
            final int charsize = encoding.charlength(data,byteoffset);
            final byte[] cd = Arrays.copy(data,byteoffset,charsize,new byte[charsize],0);
            return cd;
        }

        public void peekToBuffer(ByteSequence buffer) {
            final int charsize = encoding.charlength(data,byteoffset);
            buffer.put(data,byteoffset,charsize);
        }

        public int peekToUnicode() {
            encoding.toUnicode(data, buffer, byteoffset);
            return buffer[0];
        }
        
        public int[] peekBulk(ByteSequence bytebuffer) {
            encoding.toUnicode(data, buffer, byteoffset);
            bytebuffer.put(data,byteoffset,buffer[1]);
            return buffer;
        }

        public boolean nextEquals(byte[] cdata) {
            return Arrays.equals(cdata,0,cdata.length,data,byteoffset);
        }

    }
    
}
