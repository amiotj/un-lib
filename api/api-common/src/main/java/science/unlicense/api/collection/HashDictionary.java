

package science.unlicense.api.collection;

import science.unlicense.api.exception.UnimplementedException;

/**
 * Dictionary using hash values for faster access.
 * 
 * This implementation is inspired from :
 * http://www.monkey-x.com/Community/posts.php?topic=3929
 * 
 * @author Johann Sorel
 */
public class HashDictionary extends AbstractDictionary {

    protected static final int DEFAULT_SIZE = 11;
    
    protected static final float LOAD_MAX = 1.75f;
    protected static final float LOAD_MIN = 0.3f;
    protected static final int RESIZE_UP = 3;
    protected static final float RESIZE_DOWN = 0.5f;

    protected final Hasher hasher;
    protected HPair[] table;
    protected int size = 0;

    private KeySet keySet = null;
    private ValueSet valueSet = null;
    private PairSet pairSet = null;

    public HashDictionary() {
        this(Hasher.DEFAULT,DEFAULT_SIZE);
    }

    public HashDictionary(Hasher hasher) {
        this(hasher,DEFAULT_SIZE);
    }
    
    public HashDictionary(Hasher hasher, int size){
        this.table = new HPair[size];
        this.hasher = hasher;
    }
    
    @Override
    public int getSize() {
        return size;
    }

    public boolean containsKey(Object key){
        final int keyhash = hasher.getHash(key);
        HPair pair = table[offset(keyhash)];
        while(pair!=null){
            if(pair.hash==keyhash && hasher.equals(pair.value1, key)){
                return true;
            }
            pair = pair.next;
        }
        return false;
    }
    
    
    public Object getValue(Object key){
        final int keyhash = hasher.getHash(key);
        HPair pair = table[offset(keyhash)];
        if(pair==null) return null;
        while(pair!=null){
            if(pair.hash==keyhash && hasher.equals(pair.value1, key)){
                return pair.value2;
            }
            pair = pair.next;
        }
        return null;
    }

    public void add(Object key, Object value){
        final int keyhash = hasher.getHash(key);
        final int offset = offset(keyhash);        
        HPair pair = table[offset(keyhash)];
        
        //search if the key is already here
        while(pair!=null){
            if(pair.hash==keyhash && hasher.equals(pair.value1, key)){
                pair.value1 = key; //change the key too, it might not be the same object
                pair.value2 = value;
                return;
            }
            pair = pair.next;
        }
        
        //add a new value
        table[offset] = new HPair(keyhash, key, value, table[offset]);
        size++;
        if(loadRatio()>=LOAD_MAX){
            resize(table.length * RESIZE_UP);
        }
    }

    public Object remove(Object key){
        final int keyhash = hasher.getHash(key);
        final int offset = offset(keyhash);
        HPair pair = table[offset];
        HPair prev = null;

        Object oldValue = null;
        while(pair != null){
            if(pair.hash==keyhash && hasher.equals(pair.value1,key)){
                if(prev!=null){
                    prev.next = pair.next;
                }else{
                    table[offset] = table[offset].next;
                }
                size--;
                oldValue = pair.value2;
                break;
            }
            prev = pair;
            pair = pair.next;
        }
        
        if(loadRatio()<=LOAD_MIN){
            resize((int) Math.ceil(table.length * RESIZE_DOWN));
        }
        
        return oldValue;
    }
    
    @Override
    public Set getKeys() {
        if(keySet==null) keySet = new KeySet();
        return keySet;
    }

    @Override
    public Collection getValues() {
        if(valueSet==null) valueSet = new ValueSet();
        return valueSet;
    }

    @Override
    public Collection getPairs() {
        if(pairSet==null) pairSet = new PairSet();
        return pairSet;
    }

    @Override
    public void removeAll() {
        table = new HPair[DEFAULT_SIZE];
        size = 0;
    }
    
    final void resize(int size){
        HPair tp;
        
        final HPair[] temp = table;
        table = new HPair[size];
        for(int i=0;i<temp.length;i++){
            HPair pair = temp[i];
            while(pair!=null){
                tp = pair.next;
                addRecycle(pair);
                pair = tp;
            }
        }
    }
    
    private void addRecycle(HPair old){
        final int offset = offset(old.hash);        
        old.next = table[offset];
        table[offset] = old;
    }
    

    final int offset(int keyHash){
        return Math.abs(keyHash % table.length);
    }
    
    final float loadRatio(){
        return ((float)size/(float)table.length);
    }
        
    static final class HPair extends Pair {
        final int hash;
        HPair next;
        
        HPair(int hash, Object key, Object value, HPair next) {
            super(key, value);
            this.hash = hash;
            this.next = next;
        }
        
        Pair simple(){
            return new Pair(value1, value2);
        }
        
    }

    private class KeySet extends AbstractCollection implements Set{

        public boolean add(Object candidate) {
            throw new UnimplementedException("Not supported.");
        }

        public boolean remove(Object candidate) {
            return HashDictionary.this.remove(candidate)!=null;
        }

        public Iterator createIterator() {
            return new AbstractIterator() {
                int currentIndex = 0;
                HPair currentPair;

                protected void findNext() {
                    if(currentPair!=null){
                        currentPair = currentPair.next;
                    }
                    while(currentPair==null && currentIndex<table.length){
                        currentPair = table[currentIndex];
                        currentIndex++;
                    }
                    nextValue = (currentPair==null) ? null : currentPair.value1;
                }
            };
        }

    }

    private class ValueSet extends AbstractCollection {

        public boolean add(Object candidate) {
            throw new UnimplementedException("Not supported.");
        }

        public boolean remove(Object candidate) {
            return HashDictionary.this.remove(candidate)!=null;
        }

        public Iterator createIterator() {
            return new AbstractIterator() {
                int currentIndex = 0;
                HPair currentPair;

                protected void findNext() {
                    if(currentPair!=null){
                        currentPair = currentPair.next;
                    }
                    while(currentPair==null && currentIndex<table.length){
                        currentPair = table[currentIndex];
                        currentIndex++;
                    }
                    nextValue = (currentPair==null) ? null : currentPair.value2;
                }
            };
        }

    }

    private class PairSet extends AbstractCollection {

        public boolean add(Object candidate) {
            throw new UnimplementedException("Not supported.");
        }

        public boolean remove(Object candidate) {
            return HashDictionary.this.remove(candidate)!=null;
        }

        public Iterator createIterator() {
            return new AbstractIterator() {
                int currentIndex = 0;
                HPair currentPair;

                protected void findNext() {
                    if(currentPair!=null){
                        currentPair = currentPair.next;
                    }
                    while(currentPair==null && currentIndex<table.length){
                        currentPair = table[currentIndex];
                        currentIndex++;
                    }
                    nextValue = (currentPair==null) ? null : currentPair.simple();
                }
            };
        }

    }

}
