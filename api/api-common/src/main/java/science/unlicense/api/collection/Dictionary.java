package science.unlicense.api.collection;

import science.unlicense.api.event.EventSource;

/**
 * Similar to books or databases index.
 * The name Dictionary has been choosen by ISO.
 * 
 * @author Johann Sorel
 */
public interface Dictionary extends EventSource{
    
    /**
     * The size of the index, number of key-value pairs.
     * 
     * @return int number of pairs in the dictionary
     */
    int getSize();
    
    /**
     * @return collection of all keys.
     */
    Set getKeys();
    
    /**
     * @return collection of all values.
     */
    Collection getValues();
    
    /**
     * @return collection of all Pair.
     */
    Collection getPairs();
        
    /**
     * Find value for a given key.
     * 
     * @param key search key
     * @return value registered with this key.
     */
    Object getValue(Object key);
        
    /**
     * Register the given value with this key.
     * if key does not exist it is created.
     * 
     * @param key dictionary key
     * @param value value associated
     */
    void add(Object key, Object value);
        
    /**
     * Copy all keys and values from given index,
     * this might override values from this index.
     * 
     * @param index dictionary to copy from
     */
    void addAll(Dictionary index);
    
    /**
     * Remove value from the given key.
     * The value might still be registered in other keys.
     * 
     * @param key dictionary key
     * @return removed value, can be null
     */
    Object remove(Object key);
   
    /**
     * Remove all values and keys.
     */
    void removeAll();
    
}
