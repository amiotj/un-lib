
package science.unlicense.api.regex;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.regex.NFAStateExec.Seq;
import science.unlicense.api.model.tree.Nodes;

/**
 * NFA graph executor.
 * This executor may be used by with any kind of objects.
 * Can be characters codepoint in a regex or tokens in a parser.
 * 
 * This class is not concurrent.
 * 
 * @author Johann Sorel
 */
public class NFAPathExec {
    
    protected final NFAState state;
    protected Seq buffer1 = new Seq();
    protected Seq buffer2 = new Seq();
    //TODO cached start states
    //private Object[] baseStates;
    
    public NFAPathExec(final NFAState state) {
        this.state = state;
    }
    
    public void init(){
        //cache the starting states, there are often a lot of forks at the beginning
        //since those conditions never change, caching those bring a huge performance
        //increase when parsing large files.
        //TODO this is bugged, we need a deep but smart copy of the paths to avoid unecesary
        //duplicated nodes, performance gain migh not be that great after all, need profiling
        addPath(buffer1, createPath(state));
        
        Object[] baseStates = buffer1.toArray();
        buffer2.setMinSize(baseStates.length);
        buffer2.setMinSize(baseStates.length);
        buffer1.removeAll();
    }

    protected NFAPath createPath(NFAState state){
        return new NFAPath(state);
    }
    
    public NFAState getState() {
        return state;
    }

    /**
     * Return the list of current automaton states.
     * This list can contain NFAPath objects.
     * 
     * @return Sequence current automaton states.
     */
    public Sequence getProcessState() {
        return buffer1;
    }
    
    /**
     * Reset executor state.
     * Must be called before any text is evaluated.
     */
    public void reset(){
        //reset buffers
        buffer1.removeAll();
        buffer2.removeAll();
        addPath(buffer1, createPath(state));
    }
    
    /**
     * Process a new elements.
     * This returned list can contain NFAState or NFAPath objects if
     * the createPath parameter has been set to True in the constructor.
     * 
     * @param candidate unicode code point
     * @return Collection of current NFA paths
     */
    public Sequence process(Object candidate){
        buffer2.removeAll();
        stepPath(candidate);

        //swap buffers
        final Seq temp = buffer1; 
        buffer1 = buffer2; 
        buffer2 = temp;
        return buffer1;
    }
     
    private static void addPath(final Sequence states, NFAPath path) {
        //TODO the contains operation is expensive here, it could be removed but 
        //this may duplicate possible paths
        //if (path == null || states.contains(path)) {
        if (path == null) {
            return;
        }

        NFAState state = path.state;
        while (state instanceof NFAState.FastForward) {
            path = path.child(path.getNextState());
            state = path.state;
        }

        if (state instanceof NFAState.Fork) {
            //do not store forks in the path, they don't hold any information
            addPath(states, path.link.child(path.getNextForkState1()));
            addPath(states, path.link.child(path.getNextForkState2()));
        } else {
            states.add(path);
        }
    }

    /**
     * Conditions : 
     * - buffer1 must contain previous States
     * - buffer2 will contain next States
     * 
     * @param cp
     * @param 
     * @param buffer2 
     */
    private void stepPath(final Object cp) {
        final Object[] obj = buffer1.getValues();
        NFAPath path;
        for (int i = 0, n = buffer1.getSize(); i < n; i++) {
            path = (NFAPath)obj[i];
            if (path.state instanceof NFAState.Evaluator) {
                if(((NFAState.Evaluator)path.state).evaluate(cp)){
                    path.setValue(cp);
                    addPath(buffer2, path.child(path.getNextState()));
                }
            }
        }
    }
    
    /**
     * Produce a nice print graph of the current result paths.
     * @param result
     * @return 
     */
    public static Chars printResult(Sequence result){
        final CharBuffer cb = new CharBuffer();
        
        result = new ArraySequence(result);
        //start by reversing all paths
        for(int i=0;i<result.getSize();i++){
            result.replace(i,((NFAPath) result.get(i)).reverse());
        }
        
        cb.append(print(result));
                
        return cb.toChars();
    }
    
    private static Chars print(Sequence result){        
        NFAPath p = (NFAPath) result.get(0);
        final Chars root = p.toChars(false);
        
        //remove one link
        final Sequence remain = new ArraySequence();
        for(int i=0;i<result.getSize();i++){
            final NFAPath pl = ((NFAPath) result.get(i)).link;
            if(pl!=null){
                remain.add(pl);
            }
        }
        
        final Sequence subs = new ArraySequence();
        while(!remain.isEmpty()){
            final NFAPath group = (NFAPath) remain.get(0);
            final Sequence groupElements = new ArraySequence();
            groupElements.add(group);
            remain.remove(0);
            
            for(int i=remain.getSize()-1; i>=0;i--){
                final NFAPath pl = ((NFAPath) remain.get(i));
                if(pl.equals(group)){
                    remain.remove(i);
                    groupElements.add(pl);
                }
            }
            
            subs.add(print(groupElements));
        }
        
        if(subs.isEmpty()){
            return root;
        }else{
            return Nodes.toChars(root, subs);
        }
    }
    
}
