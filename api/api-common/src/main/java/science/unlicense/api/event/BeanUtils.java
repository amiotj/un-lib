
package science.unlicense.api.event;

import java.lang.reflect.Method;
import science.unlicense.api.character.Chars;

/**
 * Regroup all introspection calls here.
 * 
 * TODO need to define an API and push implementation in the jvm module.
 * 
 * @author Johann Sorel
 */
final class BeanUtils {
    
    public static Method getReadMethod(Class clazz, Chars name){
        final String name1 = "is"+name;
        final String name2 = "get"+name;
        for(Method m : clazz.getMethods()){
            if(m.getName().equals(name1) || m.getName().equals(name2)){
                return m;
            }
        }
        return null;
    }
    
    public static Method getWriteMethod(Class clazz, Chars name){
        final String name1 = "set"+name;
        for(Method m : clazz.getMethods()){
            if(m.getName().equals(name1) && m.getParameterTypes().length == 1){
                return m;
            }
        }
        return null;
    }
    
}
