
package science.unlicense.api.buffer;

/**
 *
 * @author johann Sorel
 */
public interface BufferFactory {
    
    Buffer createByte(long nbValue);
    
    Buffer createInt(long nbValue);
    
    Buffer createLong(long nbValue);
    
    Buffer createFloat(long nbValue);
    
    Buffer createDouble(long nbValue);
    
}
