
package science.unlicense.api.model.doc;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.predicate.VariableSyncException;
import science.unlicense.api.predicate.Variables;
import science.unlicense.api.predicate.Variables.VarSync;

/**
 * Default document implementation using an HashDictionary as backend.
 * 
 * @author Johann Sorel
 */
public class DefaultDocument extends AbstractDocument{

    /**
     * Stored properties values.
     * CAUTION : modify with care, send events when necessary.
     */
    protected final Dictionary properties;

    public DefaultDocument() {
        this(false);
    }

    public DefaultDocument(boolean preserveOrder) {
        this(preserveOrder,null);
    }
    
    /**
     * Create a document, do not preserve field order.
     * 
     * @param docType 
     */
    public DefaultDocument(DocumentType docType) {
        this(false,docType);
    }

    public DefaultDocument(boolean preserveOrder, DocumentType docType) {
        super(docType);
        if(preserveOrder){
            properties = new OrderedHashDictionary();
        }else{
            properties = new HashDictionary();
        }
    }
    
    public Set getFieldNames() {
        return properties.getKeys();
    }
    
    public Field getField(Chars name) {
        return new IProperty(name);
    }

    public Object getFieldValue(Chars id){
        return properties.getValue(id);
    }
    
    public void setFieldValue(Chars name, Object value) {
        //check field exist
        if(docType!=null && docType.isStrict() && docType.getField(name)==null){
            throw new InvalidArgumentException("No field for name : "+name);
        }
        final DocMessage.FieldChange evt = ((IProperty)getField(name)).setValueAndEvent(value);
        if(evt!=null) sendDocEvent(evt);
    }
            
    public void set(Document doc) {
        final Collection baseNames = new ArraySequence(getFieldNames());
        
        final Sequence changes = hasListeners() ? new ArraySequence() : null;
        
        //replace properties
        final Iterator docIte = doc.getFieldNames().createIterator();
        while(docIte.hasNext()){
            final Chars name = (Chars) docIte.next();
            final DocMessage.FieldChange c = ((IProperty)getField(name)).setValueAndEvent(doc.getField(name).getValue());
            if(changes!=null && c!=null) changes.add(c);
            baseNames.remove(name);
        }
        
        //remove properties that do not exist any more
        final Iterator ite = baseNames.createIterator();
        while(ite.hasNext()){
            final Chars name = (Chars) ite.next();
            final DocMessage.FieldChange c = ((IProperty)getField(name)).setValueAndEvent(null);
            if(changes!=null && c!=null) changes.add(c);
        }
        
        sendDocEvent(changes);
    }
    
    public void removeProperty(Chars id){
        properties.remove(id);
    }
    
    public void removeAllProperties(){
        properties.removeAll();
    }
    
    private class IProperty implements Field {

        private final Chars name;

        public IProperty(Chars id) {
            this.name = id;
        }
        
        public Chars getName() {
            return name;
        }

        public FieldType getType(){
            return (docType==null) ? null : docType.getField(name);
        }

        public EventSource getHolder() {
            return DefaultDocument.this;
        }
        
        public Object getValue() {
            return properties.getValue(name);
        }

        public void setValue(Object value) {
            final DocMessage.FieldChange c = setValueAndEvent(value);
            sendDocEvent(c);
        }
        
        public DocMessage.FieldChange setValueAndEvent(Object value) {
            if (value!=null) {
                //check type
                final FieldType type = getType();
                if (type!=null) {
                    if (type.getMaxOccurences()>1) {
                        if (!(value instanceof Collection)){
                            throw new InvalidArgumentException("Unvalid value class, expected a Collection but was a "+value.getClass());
                         }
                    } else {
                        if (!type.getValueClass().isInstance(value)){
                            throw new InvalidArgumentException("Unvalid value class, extected a "+type.getValueClass()+" but was a "+value.getClass());
                        }
                    }
                }
            }

            Object base = properties.getValue(name);
            Object old = base;
            if(CObjects.equals(old, value)) return null;
            
            if(value==null){
                properties.remove(name);
            }else{
                properties.add(name, value);
            }
            if(hasListeners()){
                return new DocMessage.FieldChange(name, old, value);
            }else{
                return null;
            }
        }
        
        public boolean isReadable(){
            return true;
        }

        public boolean isWritable(){
            return true;
        }

        public void sync(Variable var) {
            sync(var, false);
        }

        public void sync(Variable var, boolean readOnly) {
            final VarSync sync = getVarSync();
            if(sync!=null) throw new VariableSyncException("Variable is already synchronized");
            Variables.sync(this, var, readOnly);
        }

        public void unsync() {
            final VarSync sync = getVarSync();
            if(sync!=null) sync.release();
        }

        private VarSync getVarSync(){
            final EventSource holder = DefaultDocument.this;
            final EventListener[] listeners = holder.getListeners(new PropertyPredicate(name));
            if(listeners.length==0) return null;
            for(int i=0;i<listeners.length;i++){
                if(listeners[i] instanceof VarSync){
                    final VarSync sync = ((VarSync)listeners[i]);
                    if(sync.getVar1().equals(this)){
                        return sync;
                    }
                }
            }
            return null;
        }

        public void addListener(EventListener listener){
            addEventListener(new PropertyPredicate(name), listener);
        }

        public void removeListener(EventListener listener){
            DefaultDocument.this.removeEventListener(new PropertyPredicate(name), listener);
        }

        public Class[] getEventClasses() {
            return new Class[]{PropertyMessage.class};
        }

        public void addEventListener(Predicate predicate, EventListener listener) {
            DefaultDocument.this.addEventListener(predicate, listener);
        }

        public void removeEventListener(Predicate predicate, EventListener listener) {
            DefaultDocument.this.removeEventListener(predicate, listener);
        }

        public EventListener[] getListeners(Predicate predicate) {
            return DefaultDocument.this.getListeners(predicate);
        }


    }

    public int getHash() {
        return 456413;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultDocument other = (DefaultDocument) obj;
        
        if(properties.getSize() != other.properties.getSize()){
            return false;
        }
        
        //check pairs
        final Iterator ite = properties.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            Object val = pair.getValue2();
            Object valo = other.properties.getValue(pair.getValue1());
            if(!CObjects.equals(val, valo)) return false;
        }
        
        return true;
    }
    
}
