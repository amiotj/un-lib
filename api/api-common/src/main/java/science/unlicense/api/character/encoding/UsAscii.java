
package science.unlicense.api.character.encoding;

import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class UsAscii implements CharEncoding{

    private final Chars name = new Chars(new byte[]{'U','S','-','A','S','C','I','I'});
    private final byte nomatch = (byte)95; //underscore used to match unsupported characters

    public Chars getName() {
        return name;
    }

    public int charlength(byte[] array, int offset) {
        return 1;
    }

    public int length(byte[] array) {
        return array.length;
    }

    public int toUnicode(byte[] array) {
        return array[0] & 0xFF;
    }

    public void toUnicode(byte[] array, int[] codePoint, int offset) {
        codePoint[0] = array[offset] & 0xFF;
        codePoint[1] = 1;
    }

    public byte[] toBytes(int codePoint) {
        //TODO check range
        return new byte[]{(byte)codePoint};
    }

    public void toBytes(int[] codePoint, byte[] array, int offset) {
        if(codePoint[0]<128){
            array[offset] = (byte)codePoint[0];
        }else{
            //can not map this character
            array[offset] = nomatch;
        }
        codePoint[1] = 1;
    }

    public byte[] toBytes(int[] codePoints) {
        final byte[] array = new byte[codePoints.length];
        for(int i=0;i<codePoints.length;i++){
            if(codePoints[i]<128){
                array[i] = (byte)codePoints[i];
            }else{
                //can not map this character
                array[i] = nomatch;
            }
        }
        return array;
    }

    public boolean isComplete(byte[] array, int offset, int length) {
        return length == 1;
    }
    
}
