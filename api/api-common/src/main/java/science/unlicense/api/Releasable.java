
package science.unlicense.api;

/**
 * Releasable resource.
 * 
 * @author Johann Sorel
 */
public interface Releasable {
   
    void release();
    
}
