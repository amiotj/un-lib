
package science.unlicense.api.exception;

/**
 *
 * @author Johann Sorel
 */
public class ParseException extends Exception {

    public ParseException() {
    }

    public ParseException(String s) {
        super(s);
    }

    public ParseException(Throwable cause) {
        super(cause);
    }

    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
