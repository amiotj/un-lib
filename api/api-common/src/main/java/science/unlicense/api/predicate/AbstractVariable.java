
package science.unlicense.api.predicate;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyPredicate;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractVariable extends AbstractEventSource implements Variable{
    
    private static final PropertyPredicate PREDICATE = new PropertyPredicate(new Chars("Value"));
    
    private Variables.VarSync sync;


    public EventSource getHolder() {
        return this;
    }

    public boolean isReadable() {
        return true;
    }

    public boolean isWritable() {
        return true;
    }

    public void sync(Variable var) {
        sync(var, false);
    }

    public void sync(Variable var, boolean readOnly) {
        if(sync!=null) throw new VariableSyncException("Variable is already synchronized");
        sync = Variables.syncInternal(this, var, readOnly);
    }

    public void unsync() {
        if(sync!=null){
            sync.release();
            sync = null;
        }
    }

    public void addListener(EventListener listener){
        addEventListener(PREDICATE, listener);
    }
    
    public void removeListener(EventListener listener){
        removeEventListener(PREDICATE, listener);
    }

    protected void fireValueChanged(Object oldVal, Object newVal){
        sendPropertyEvent(this, PREDICATE.getPropertyName(), oldVal, newVal);
    }

}
