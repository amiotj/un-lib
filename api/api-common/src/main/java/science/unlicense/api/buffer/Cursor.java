
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;

/**
 * Cursors are not thread safe.
 * 
 * @author Johann Sorel
 */
public interface Cursor {

    /**
     * Get buffer this cursor moves on.
     * @return Buffer
     */
    Buffer getBuffer();

    /**
     * Get cursor number encoding.
     * @return NumberEncoding
     */
    NumberEncoding getEncoding();

    /**
     * @return cursor offset in bytes.
     */
    long getByteOffset();
    
}
