
package science.unlicense.api;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.NullArgumentException;

/**
 * Convinient functions on objects.
 * 
 * @author Johann Sorel
 */
public final class CObjects {
    
    private static final Chars CHARS_NULL = new Chars(new byte[]{'n','u','l','l'});
    
    private CObjects(){}
 
    public static Chars toChars(Object object){
        return toChars(object, 30);
    }
    
    /**
     * 
     * @param object
     * @param arrayLength maximum number of values in array displayed
     * @return 
     */
    public static Chars toChars(Object object,int arrayLength){
        if(object==null){
            return CHARS_NULL;
        }else if(object.getClass().isArray()){
            return Arrays.toChars(object,arrayLength);
        }else if(object instanceof CObject){
            return ((CObject)object).toChars();
        }else{
            return new Chars(object.toString());
        }
    }
    
    
    public static boolean equals(Object first, Object second){
        return first==second
            || (!(first==null || second==null) && first.equals(second));
    }
    
    /**
     * Compare two objects.
     * Check for possible null.
     * 
     * @param first first object to compare
     * @param second second object to compare
     * @return true if both objects are equal or null
     */
    public static boolean equals(CObject first, CObject second){
        if(first==second){
            return true;
        }else if(first==null || second==null){
            return false;
        }else{
            return first.equals(second);
        }
    }
    
    public static int getHash(Object candidate){
        if (candidate instanceof CObject) {
            return ((CObject)candidate).getHash();
        } else if (candidate != null) {
            return candidate.hashCode();
        } else {
            return 0;
        }
    }
    
    /**
     * Ensure given object is not null.
     * Raise error otherwise.
     * 
     * @param candidate object to test
     * @throws InvalidArgumentException if tested object is null
     */
    public static void ensureNotNull(Object candidate) throws InvalidArgumentException {
        if(candidate==null){
            throw new NullArgumentException("Object should not be null.");
        }
    }
    
    /**
     * Ensure given object is not null.
     * Raise error otherwise.
     * 
     * @param candidate object to test
     * @throws InvalidArgumentException if tested object is null
     */
    public static void ensureNotNull(Object candidate, String name) throws InvalidArgumentException {
        if(candidate==null){
            throw new NullArgumentException("Object "+name+" should not be null.");
        }
    }
    
    /**
     * Ensure given objects are the same
     * 
     * @param a
     * @param b
     * @throws InvalidArgumentException if objects are different
     */
    public static void ensureSame(Object a, Object b) throws InvalidArgumentException {
        if (a != b){
            throw new InvalidArgumentException("Objects are different");
        }
    }
    
    /**
     * Ensure given objects are the same
     * 
     * @param a
     * @param b
     * @param c
     * @throws InvalidArgumentException if objects are different
     */
    public static void ensureSame(Object a, Object b, Object c) throws InvalidArgumentException {
        if (a != b || b != c){
            throw new InvalidArgumentException("Objects are different");
        }
    }
    
    /**
     * Ensure given value is in given range.
     * Raise error otherwise.
     * 
     * @param candidate value to test
     * @param min lower range
     * @param max upper range
     * @throws InvalidArgumentException if value is not between bounds
     */
    public static void ensureBetween(double candidate, double min, double max) throws InvalidArgumentException {
        if(candidate<min || candidate>max){
            throw new InvalidArgumentException("Value "+candidate+" is not in range ["+min+"..."+max+"]");
        }
    }
    
    /**
     * Ensure given value is positive
     * Raise error otherwise.
     * 
     * @param candidate value to test
     * @throws InvalidArgumentException if value is not positive
     */
    public static void ensurePositive(double candidate) throws InvalidArgumentException {
        if(candidate<0){
            throw new InvalidArgumentException("Value "+candidate+" is not positive.");
        }
    }
    
    /**
     * Ensure given value is negative
     * Raise error otherwise.
     * 
     * @param candidate value to test
     * @throws InvalidArgumentException if value is not negative
     */
    public static void ensureNegative(double candidate) throws InvalidArgumentException {
        if(candidate>-0){
            throw new InvalidArgumentException("Value "+candidate+" is not negative.");
        }
    }
    
}
