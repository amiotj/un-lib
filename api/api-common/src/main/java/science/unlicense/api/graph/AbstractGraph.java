
package science.unlicense.api.graph;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractGraph implements Graph {

    public boolean isSimpleGraph() {
        return !isPseudoGraph();
    }
    
    public boolean isMultiGraph() {
        final Iterator vite = getVertices().createIterator();
        while(vite.hasNext()){
            final Vertex vertice = (Vertex) vite.next();
            final Sequence edges = vertice.getEdges();
            for(int i=0,n=edges.getSize();i<n;i++){
                //check parallale edge
                for(int j=i+1;j<n;j++){
                    if(((Edge)edges.get(i)).isParallale((Edge)edges.get(j)) ){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isPseudoGraph() {
        final Iterator vite = getVertices().createIterator();
        while(vite.hasNext()){
            final Vertex vertice = (Vertex) vite.next();
            final Sequence edges = vertice.getEdges();
            for(int i=0,n=edges.getSize();i<n;i++){
                //chek loop edge
                if(((Edge)edges.get(i)).isLoop()){
                    return true;
                }
                //check parallale edge
                for(int j=i+1;j<n;j++){
                    if(((Edge)edges.get(i)).isParallale((Edge)edges.get(j)) ){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isComplete() {
        final Sequence vertices = getVertices();
        final Sequence remains = new ArraySequence();
        for(int i=0,n=vertices.getSize();i<n;i++){
            final Vertex vertex = (Vertex) vertices.get(i);
            remains.addAll(vertices);
            remains.remove(vertex);
            final Sequence edges = vertex.getEdges();
            for(int j=0,k=edges.getSize();j<k;j++){
                final Edge edge = (Edge) edges.get(i);
                remains.remove(edge.getVertex1());
                remains.remove(edge.getVertex2());
            }
            if(remains.getSize() != 0){
                //missing edge to be comple
                return false;
            }
        }
        return true;
    }

}
