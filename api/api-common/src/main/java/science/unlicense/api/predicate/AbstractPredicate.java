
package science.unlicense.api.predicate;

import science.unlicense.api.CObject;

/**
 * @author Yann D'Isanto
 */
public abstract class AbstractPredicate extends CObject implements Predicate {

}
