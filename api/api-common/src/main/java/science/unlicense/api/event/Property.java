
package science.unlicense.api.event;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;

/**
 * Convinient class to manipulate a single property of a bean.
 * 
 * @author Johann Sorel
 */
public interface Property extends Variable {
        
    public Chars getName();

    public Class getValueClass();
        
}
