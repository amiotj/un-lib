package science.unlicense.api.logging;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;

/**
 * Manage loggers.
 *
 * @author Johann Sorel
 */
public class Loggers {

    private static final LoggerFactory FACTORY = new DefaultLoggerFactory();

    public static final Chars DEFAULT = new Chars("default");
    private static final Dictionary LOGGERS = new HashDictionary();

    /**
     * Avoid instanciation.
     */
    private Loggers() {
    }

    /**
     * Get the default logger.
     * @return Logger, never null
     */
    public static Logger get(){
        return getLogger(DEFAULT);
    }

    /**
     * Get the logger associated with given label.
     * If no logger is associated with this label, a new one will be created.
     *
     * @param label logger label
     * @return Logger, never null
     */
    public static Logger getLogger(Chars label) {
        Logger logger = (Logger) LOGGERS.getValue(label);
        if(logger == null){
            //TODO this should rely on the module registry to find factories
            //but it's a sub module, have to find a clean solution.
            logger = FACTORY.create();
        }
        return logger;
    }


}
