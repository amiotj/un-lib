
package science.unlicense.api.collection;

import science.unlicense.api.Orderable;
import science.unlicense.api.Sorter;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.InvalidIndexException;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.lang.Handler;
import science.unlicense.api.lang.Option;

/**
 * A collections utility class.
 *
 * @author Johann Sorel
 * @author Yann D'Isanto
 */
public final class Collections {

        
    private Collections() {
    }

    /**
     * Attach a listener to the given collection and check all added values
     * pass the given predicate.
     * 
     * @param col
     * @param predicate
     */
    public static void constraint(Collection col, final Predicate predicate){
        col.addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final CollectionMessage ce = (CollectionMessage) event.getMessage();
                final int type = ce.getType();
                if(type == CollectionMessage.TYPE_ADD 
                    || type == CollectionMessage.TYPE_REPLACE 
                    || type == CollectionMessage.TYPE_REPLACEALL){
                    Object[] ele = ce.getNewElements();
                    for(int i=ele.length-1;i>=0;i--){
                        if(!predicate.evaluate(ele[i])){
                            throw new InvalidArgumentException("Element do not pass predicate : "+predicate);
                        }
                    }
                }
            }
        });
    }
    
    /**
     * Flatten the specified collection of collections.
     * 
     * @param collections the collections to flatten.
     * @return a list with all flattened items.
     */
    public static Sequence flatten(Collection collections) {
        final Sequence flatList = new ArraySequence();
        final Iterator ite = collections.createIterator();
        while(ite.hasNext()) {
            final Collection col = (Collection) ite.next();
            flatList.addAll(col);
        }
        return flatList;
    }

    /**
     * Binds all items with the specified mapping.
     *
     * @param source collection of objects to bind
     * @param mapping the mapping to apply when binding.
     * @return a new list resulting from the binding.
     */
    public static Sequence bind(Collection source, final Handler.SafeInOut mapping) {
        final Sequence bound = new ArraySequence();
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            bound.addAll((Collection)mapping.perform(ite.next()));
        }
        return bound;
    }

    /**
     * @param source collection
     * @param predicate the predicate.
     * @return true if an item matches the specified predicate, false otherwise.
     */
    public static boolean exists(Collection source, Predicate predicate) {
        return find(source,predicate).isDefined();
    }

    /**
     * Filters this collection according to the specified predicate.
     *
     * @param source collection
     * @param predicate the predicate.
     * @return a list which contains the item matching the predicate.
     */
    public static Sequence filter(Collection source, final Predicate predicate) {
        final Sequence filtered = new ArraySequence();
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            final Object item = ite.next();
            if (predicate.evaluate(item)) {
                filtered.add(item);
            }
        }
        return filtered;
    }

    /**
     * Finds the first item of the collection matching the specified predicate.
     *
     * @param source collection
     * @param predicate the predicate.
     * @return an optional item.
     */
    public static Option find(Collection source, final Predicate predicate) {
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            final Object item = ite.next();
            if (predicate.evaluate(item)) {
                return Option.some(item);
            }
        }
        return Option.none();
    }

    /**
     * Folds the items of this collection using the specified associative binary
     * operator. The order in which operations are performed on items is
     * unspecified and may be nondeterministic.
     *
     * @param source collection
     * @param op an associative binary operator.
     * @param startValue the start value for the fold operation.
     * @return the result of applying the specified fold operator between all
     * the items of this collection and the specified startValue.
     */
    public static Object fold(Collection source, Handler.SafeBinaryOut op, Object startValue) {
        Object value = startValue;
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            value = op.perform(value, ite.next());
        }
        return value;
    }

    /**
     * Evaluates the specified predicate for all items.
     *
     * @param source collection
     * @param predicate the predicate.
     * @return true if this entire collection (all items) matches the specified
     * predicate.
     */
    public static boolean forAll(Collection source, final Predicate predicate) {
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            if (!predicate.evaluate(ite.next())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Applies the specified Batch for each item of this collection.
     *
     * @param source collection
     * @param batch the batch to perform on each item.
     */
    public static void forEach(Collection source, final Handler.SafeIn batch) {
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            batch.perform(ite.next());
        }
    }

    /**
     * Calls the specified callback for each item of this collection. The for
     * each loop stop iterating as soon as a callback return a defined value.
     *
     * @param source collection
     * @param callback the callback to call on each item.
     * @return an optional value.
     */
    public static Option forEach(Collection source, final Handler.SafeInOut callback) {
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            final Option b = (Option) callback.perform(ite.next());
            if (b.isDefined()) {
                return b;
            }
        }
        return Option.none();
    }

    /**
     * @param source collection
     * @return the first element of this collection as an optional value.
     */
    public static Option headOption(Collection source) {
        final Iterator ite = source.createIterator();
        if (ite.hasNext()) {
            return Option.some(ite.next());
        }
        return Option.none();
    }

    /**
     * Maps this collection's items according to the specified mapping.
     *
     * @param source collection
     * @param mapping the mapping to apply.
     * @return a list which contains the mapping result items.
     */
    public static Sequence map(Collection source, final Handler.SafeInOut mapping) {
        final Sequence mapped = new ArraySequence();
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            mapped.add(mapping.perform(ite.next()));
        }
        return mapped;
    }

    /**
     * Partitions this list according to the specified predicate.
     *
     * @param source collection
     * @param predicate the predicate.
     * @return a pair of list. The first contains the items matching the
     * predicate and the second contains the items not matching the predicate.
     */
    public static Pair partition(Collection source, final Predicate predicate) {
        final Sequence match = new ArraySequence();
        final Sequence unmatch = new ArraySequence();
        final Iterator ite = source.createIterator();
        while (ite.hasNext()) {
            final Object item = ite.next();
            if (predicate.evaluate(item)) {
                match.add(item);
            } else {
                unmatch.add(item);
            }
        }
        return new Pair(match, unmatch);
    }

    /**
     * Reduces the items of this collection using the specified associative
     * binary operator. The order in which operations are performed on items is
     * unspecified and may be nondeterministic.
     *
     * @param source collection
     * @param op an associative binary operator.
     * @return the result of applying the specified reduce operator between all
     * the items of this collection.
     * @throws UnimplementedException if this collection is empty.
     */
    public static Object reduce(Collection source, Handler.SafeBinaryOut op) {
        final Iterator ite = source.createIterator();
        if (!ite.hasNext()) {
            throw new UnimplementedException("cannot reduce an empty list");
        }
        Object value = ite.next();
        while (ite.hasNext()) {
            value = op.perform(value, ite.next());
        }
        return value;
    }

    /**
     * Reduces the items of this collection using the specified associative
     * binary operator. The order in which operations are performed on items is
     * unspecified and may be nondeterministic.
     *
     * @param source collection
     * @param op an associative binary operator.
     * @return the optional result of applying the specified reduce operator
     * between all the items of this collection.
     */
    public static Option reduceOption(Collection source, Handler.SafeBinaryOut op) {
        final Iterator ite = source.createIterator();
        if (!ite.hasNext()) {
            return Option.none();
        }
        Object value = ite.next();
        while (ite.hasNext()) {
            value = op.perform(value, ite.next());
        }
        return Option.some(value);
    }

    /**
     * Folds the items of this collection using the specified binary operator
     * going left to right.
     *
     * @param source collection
     * @param op a binary operator.
     * @param startValue the start value for the fold operation.
     * @return the result of applying the specified fold operator between all
     * the items of this list and the specified startValue.
     */
    public static Object foldLeft(Sequence source, Handler.SafeBinaryOut op, Object startValue) {
        return fold(source.createIterator(), op, startValue);
    }

    /**
     * Folds the items of this collection using the specified binary operator
     * going right to left.
     *
     * @param source collection
     * @param op a binary operator.
     * @param startValue the start value for the fold operation.
     * @return the result of applying the specified fold operator between all
     * the items of this list and the specified startValue.
     */
    public static Object foldRight(Sequence source, Handler.SafeBinaryOut op, Object startValue) {
        return fold(source.createReverseIterator(), op, startValue);
    }

    /**
     * Returns the optional item at the specified index.
     *
     * @param source collection
     * @param index the index.
     * @return the optional item corresponding to the specified index.
     */
    public static Option getOption(Sequence source, int index) {
        if (index < source.getSize()) {
            return Option.some(source.get(index));
        }
        return Option.none();
    }

    /**
     * @param source collection
     * @return the optional last item of this list.
     */
    public static Option lastOption(Sequence source) {
        if (source.isEmpty()) {
            return Option.none();
        }
        return Option.some(source.get(source.getSize() - 1));
    }

    /**
     * Reduces the items of this collection using the specified binary operator
     * going left to right. <b>WARNING</b>: be sure that B type is a super type
     * of this collection items type or an exception will be thrown.
     *
     * @param source collection
     * @param op a binary operator.
     * @return the result of applying the specified reduce operator between all
     * the items of this list.
     * @throws ClassCastException if A doesn't extends B.
     * @throws UnimplementedException if this collection is empty.
     */
    public static Object reduceLeft(Sequence source, Handler.SafeBinaryOut op) throws ClassCastException {
        return reduce(source.createIterator(), op);
    }

    /**
     * Reduces the items of this collection using the specified binary operator
     * going left to right. <b>WARNING</b>: be sure that B type is a super type
     * of this collection items type or an exception will be thrown.
     *
     * @param source collection
     * @param op a binary operator.
     * @return the optional result of applying the specified reduce operator
     * between all the items of this list.
     * @throws ClassCastException if A doesn't extends B.
     */
    public static Option reduceLeftOption(Sequence source, Handler.SafeBinaryOut op) throws ClassCastException {
        return reduceOption(source.createIterator(), op);
    }

    /**
     * Reduces the items of this collection using the specified binary operator
     * going right to left. <b>WARNING</b>: be sure that B type is a super type
     * of this collection items type or an exception will be thrown.
     *
     * @param source collection
     * @param op a binary operator.
     * @return the result of applying the specified reduce operator between all
     * the items of this list.
     * @throws ClassCastException if A doesn't extends B.
     * @throws UnimplementedException if this collection is empty.
     */
    public static Object reduceRight(Sequence source, Handler.SafeBinaryOut op) throws ClassCastException {
        return reduce(source.createReverseIterator(), op);
    }

    /**
     * Reduces the items of this collection using the specified binary operator
     * going right to left. <b>WARNING</b>: be sure that B type is a super type
     * of this collection items type or an exception will be thrown.
     *
     * @param source collection
     * @param op a binary operator.
     * @return the optional result of applying the specified reduce operator
     * between all the items of this list.
     * @throws ClassCastException if A doesn't extends B.
     */
    public static Option reduceRightOption(Sequence source, Handler.SafeBinaryOut op) throws ClassCastException {
        return reduceOption(source.createReverseIterator(), op);
    }

    /**
     * @param source collection
     * @return a list which items are this list items in reverse order.
     */
    public static Sequence reverse(Sequence source) {
        final Sequence reverse = new ArraySequence();
        for(int i=source.getSize()-1;i>=0;i--){
            reverse.add(source.get(i));
        }
        return reverse;
    }

    /**
     * Slices this list according to the specified from and until indexes.
     *
     * @param source collection
     * @param from the index to slice from (inclusive).
     * @param until the index to slice until (exclusive).
     * @return a sublist from this list according to the specified from and
     * until indexes.
     */
    public static Sequence slice(Sequence source, int from, int until) {
        if (from < 0 || until > source.getSize() || from > until) {
            throw new InvalidIndexException();
        }
        final Sequence slice = new ArraySequence();
        for(int i=from;i<until;i++){
            slice.add(source.get(i));
        }
        return slice;
    }

    /**
     * @param source collection
     * @return a list containing all items from this list but the first one.
     */
    public static Sequence tail(Sequence source) {
        if (source.isEmpty()) {
            throw new UnimplementedException("list is empty");
        }
        return slice(source, 1, source.getSize());
    }

    public static Object fold(Iterator ite, Handler.SafeBinaryOut op, Object startValue) {
        Object value = startValue;
        while (ite.hasNext()) {
            value = op.perform(value, ite.next());
        }
        return value;
    }

    public static Object reduce(Iterator ite, Handler.SafeBinaryOut op) throws ClassCastException {
        if(!ite.hasNext()) {
            throw new UnimplementedException("cannot reduce an empty list");
        }
        Object value = ite.next();
        while (ite.hasNext()) {
            value = op.perform(value, ite.next());
        }
        return value;
    }

    public static Option reduceOption(Iterator ite, Handler.SafeBinaryOut op) throws ClassCastException {
        if(!ite.hasNext()) {
            return Option.none();
        }
        Object value = ite.next();
        while (ite.hasNext()) {
            value = op.perform(value, ite.next());
        }
        return Option.some(value);
    }

    /**
     * Sort given sequence on given range.
     * Object in sequence are expected to by instances of Orderable.
     * 
     * @param seq not null
     * @param startIndex inclusive
     * @param endIndex exclusive
     */
    public static void sort(Sequence seq, int startIndex, int endIndex){
        quickSort(seq, startIndex, endIndex);
    }

    /**
     * Sort given array on given range.
     * 
     * @param seq not null
     * @param startIndex inclusive
     * @param endIndex exclusive
     * @param sorter Sorter to use
     */
    public static void sort(Sequence seq, int startIndex, int endIndex, Sorter sorter){
        quickSort(seq, startIndex, endIndex, sorter);
    }


    // ===== Object array sort =================================================
    
    private static void swap(Sequence seq, int idx1, int idx2){
        final Object temp = seq.replace(idx1, seq.get(idx2));
        seq.replace(idx2, temp);
    }
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(Sequence array, int leftIndex, int rightIndex, int pivotIndex) {
        Object pivotValue = array.get(pivotIndex);
        swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (((Orderable)array.get(i)).order((Orderable)pivotValue) <= 0) {
                swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( Sequence array, int leftIndex, int rightIndex ) {
        if (array == null || array.getSize() < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( Sequence array ) {
        quickSort( array, 0, array.getSize()-1 );
    }
    
    // ===== Object array sort with Sorter =====================================
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(Sequence array, int leftIndex, int rightIndex, int pivotIndex, Sorter sorter) {
        Object pivotValue = array.get(pivotIndex);
        swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (sorter.sort(array.get(i), pivotValue) <= 0) {
                swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm using Sorter.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     * @param sorter the Sorter
     */
    public static void quickSort( Sequence array, int leftIndex, int rightIndex, Sorter sorter ) {
        if( sorter == null ) {
            quickSort( array, leftIndex, rightIndex );
            return;
        }
        if (array == null || array.getSize() < 2) {
            return;
        }
        
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex  = partition(array, leftIndex, rightIndex, pivotIndex, sorter);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1, sorter);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex, sorter);
        }
    }
    
    /**
     * Sort given array using Sorter (QuickSort algorithm).
     * @param array the array to be sorted.
     * @param sorter the Sorter
     */
    public static void sort( Sequence array, Sorter sorter ) {
        quickSort( array, 0, array.getSize()-1, sorter );
    }

    public static Collection sort(Collection col, Sorter eva) {
        return new SortCollection(col, eva);
    }

    // =========================================================================
    
    /**
     * Copy content of given collection in given array, starting at given position.
     *
     * @param collection collection to copy from
     * @param array Array where to insert elements
     * @param offset index where to start inserting elements
     */
    public static void copy(Collection collection, Object[] array, int offset){
        final Iterator ite = collection.createIterator();
        while(ite.hasNext()){
            array[offset] = ite.next();
            offset++;
        }
    }

    public static Iterator serie(Iterator[] iterators){
        return new SerieIterator(iterators);
    }
    
    /**
     * Create a serie of iterators.
     * @param iterators
     * @return 
     */
    public static Iterator serie(Iterator iterators){
        return new SerieIterator(iterators);
    }

    // =========================================================================

    public static Iterator emptyIterator(){
        return EmptyIterator.INSTANCE;
    }

    public static Collection emptyCollection(){
        return EmptyCollection.INSTANCE;
    }

    public static Sequence emptySequence(){
        return EmptyCollection.INSTANCE;
    }

    public static Set emptySet(){
        return EmptyCollection.INSTANCE;
    }

    // =========================================================================

    public static Iterator singletonIterator(final Object obj){
        return new SingletonIterator(obj);
    }

    public static Collection singletonCollection(final Object obj){
        return new SingletonCollection(obj);
    }

    public static Sequence singletonSequence(final Object obj){
        return new SingletonSequence(obj);
    }

    public static Set singletonSet(final Object obj){
        return new SingletonSet(obj);
    }

    // =========================================================================

    public static Iterator filterIterator(Iterator iterator, Predicate predicate){
        return new FilterIterator(iterator, predicate);
    }

    public static Collection filterCollection(Collection collection, Predicate predicate){
        return new FilterCollection(collection, predicate);
    }

    // =========================================================================

    public static Dictionary readOnlyDictionary(Dictionary dico) {
        return new ReadOnlyDictionary(dico);
    }
    
    public static Iterator readOnlyIterator(Iterator iterator) {
        return new ReadOnlyIterator(iterator);
    }

    public static Collection readOnlyCollection(Collection collection) {
        return new ReadOnlyCollection(collection);
    }

    public static Sequence readOnlySequence(Sequence sequence) {
        return new ReadOnlySequence(sequence);
    }

    public static Set readOnlySet(Set set) {
        return new ReadOnlySet(set);
    }

    // =========================================================================

    public static Collection staticCollection(Object[] values) {
        return new StaticCollection(values);
    }

    public static Sequence staticSequence(Object[] values) {
        return new StaticCollection(values);
    }
    
    public static Set staticSet(Object[] values) {
        //TODO : make a special implementation for better performances
        return new StaticCollection(values);
    }

    // =========================================================================

    public static Collection offset(Collection col, int offset) {
        return new OffsetCollection(col, offset);
    }

    public static Iterator offset(Iterator ite, int offset) {
        return new OffsetIterator(ite, offset);
    }

    // =========================================================================

    public static Collection limit(Collection col, int count) {
        return new LimitCollection(col, count);
    }

    public static Iterator limit(Iterator ite, int count) {
        return new LimitIterator(ite, count);
    }

}
