

package science.unlicense.api.collection;

import science.unlicense.api.exception.UnimplementedException;

/**
 * HashDictionnary preserving insertion order.
 * 
 * @author Johann Sorel
 */
public class OrderedHashDictionary extends HashDictionary {

    private final Sequence order = new ArraySequence();
    private KeySet keySet = null;
    private ValueSet valueSet = null;
    
    public OrderedHashDictionary() {
        super();
    }

    public OrderedHashDictionary(Hasher hasher) {
        super(hasher);
    }
    
    public OrderedHashDictionary(Hasher hasher, int size){
        super(hasher,size);
    }

    @Override
    public Set getKeys() {
        if(keySet==null) keySet = new KeySet();
        return keySet;
    }
    
    @Override
    public Collection getValues() {
        if(valueSet==null) valueSet = new ValueSet();
        return valueSet;
    }

    @Override
    public Collection getPairs() {
        final Sequence lst = new ArraySequence(size);
        for(int i=0;i<size;i++){
            lst.add( ((HPair)order.get(i)).simple());
        }
        return lst;
    }
    
    public void add(Object key, Object value){
        final int keyhash = hasher.getHash(key);
        final int offset = offset(keyhash);        
        HPair pair = table[offset(keyhash)];
        
        //search if the key is already here
        while(pair!=null){
            if(pair.hash==keyhash && hasher.equals(pair.value1, key)){
                order.remove(pair);
                pair.value1 = key; //change the key too, it might not be the same object
                pair.value2 = value;
                order.add(pair);
                return;
            }
            pair = pair.next;
        }
        
        //add a new value
        table[offset] = new HPair(keyhash, key, value, table[offset]);
        size++;
        order.add(table[offset]);
        if(loadRatio()>=LOAD_MAX){
            resize(table.length * RESIZE_UP);
        }
    }

    public Object remove(Object key){
        final int keyhash = hasher.getHash(key);
        final int offset = offset(keyhash);
        HPair pair = table[offset];
        HPair prev = null;

        Object oldValue = null;
        while(pair != null){
            if(pair.hash==keyhash && hasher.equals(pair.value1,key)){
                if(prev!=null){
                    prev.next = pair.next;
                }else{
                    table[offset] = table[offset].next;
                }
                order.remove(pair);
                size--;
                oldValue = pair.value2;
                break;
            }
            prev = pair;
            pair = pair.next;
        }
        
        if(loadRatio()<=LOAD_MIN){
            resize((int) Math.ceil(table.length * RESIZE_DOWN));
        }
        
        return oldValue;
    }
    
    private class KeySet extends AbstractCollection implements Set{

        public boolean add(Object candidate) {
            throw new UnimplementedException("Not supported.");
        }

        public boolean remove(Object candidate) {
            return OrderedHashDictionary.this.remove(candidate)!=null;
        }

        public Iterator createIterator() {
            final Iterator ite = order.createIterator();
            return new Iterator() {
                public boolean hasNext() {
                    return ite.hasNext();
                }
                public Object next() {
                    return ((HPair)ite.next()).value1;
                }
                public boolean remove() {
                    throw new UnimplementedException("Not supported.");
                }
                public void close() {
                    ite.close();
                }
            };
        }

    }

    private class ValueSet extends AbstractCollection {

        public boolean add(Object candidate) {
            throw new UnimplementedException("Not supported.");
        }

        public boolean remove(Object candidate) {
            throw new UnimplementedException("Not supported.");
        }

        public Iterator createIterator() {
            final Iterator ite = order.createIterator();
            return new Iterator() {
                public boolean hasNext() {
                    return ite.hasNext();
                }
                public Object next() {
                    return ((HPair)ite.next()).value2;
                }
                public boolean remove() {
                    throw new UnimplementedException("Not supported.");
                }
                public void close() {
                    ite.close();
                }
            };
        }

    }
    
}
