
package science.unlicense.api.model;

import science.unlicense.api.model.tree.Node;

/**
 * A metadata is a structure providing informations on the data.
 * Metadatas can hold various informations such as comments, quality, decoding parameters.
 * 
 * 
 * @author Johann Sorel
 */
public interface Metadata {
   
    /**
     * Convert this metadata to recursively explorable node model.
     * 
     * @return Node or null if metadata is to complex to be exposed as a tree.
     */
    Node toNode();
    
}
