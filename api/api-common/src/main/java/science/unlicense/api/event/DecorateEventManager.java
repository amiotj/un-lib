
package science.unlicense.api.event;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.predicate.Predicate;

/**
 * Decorate event sources, use in ReadOnlyCollections.
 *
 * @author Johann Sorel
 */
public class DecorateEventManager {

    private final EventSource source;
    private final EventSource decorator;

    private final Set lsts = new HashSet(Hasher.IDENTITY);

    public DecorateEventManager(EventSource source, EventSource decorator) {
        this.source = source;
        this.decorator = decorator;
    }

    public EventListener[] getListeners(Predicate predicate) {
        EventListener[] listeners = source.getListeners(predicate);
        Sequence seq = new ArraySequence();
        for (EventListener lst : listeners) {
            if (lsts.contains(lst)) {
                seq.add(lst);
            }
        }
        final EventListener[] array = new EventListener[seq.getSize()];
        Collections.copy(lsts, array, 0);
        return array;
    }

    /**
     * Register a listener for the given type of event.
     *
     * @param predicate, wanted event filter, can be null.
     * @param listener, listener to register, can not be null
     */
    public void addEventListener(Predicate predicate, EventListener listener) {
        final DecorateListener lst = new DecorateListener(listener);
        lsts.add(lst);
        source.addEventListener(predicate, lst);
    }

    /**
     * Unregister a listener for the given type of event.
     *
     * @param predicate, event filter, can be null.
     * @param listener, listener to unregister
     */
    public void removeEventListener(Predicate predicate, EventListener listener) {
        Iterator ite = lsts.createIterator();
        while(ite.hasNext()) {
            final DecorateListener lst = (DecorateListener) ite.next();
            if(lst.lst == listener) {
                source.removeEventListener(predicate, lst);
                lsts.remove(lst);
                break;
            }
        }
    }

    private class DecorateListener implements EventListener {

        private final EventListener lst;

        public DecorateListener(EventListener lst) {
            this.lst = lst;
        }

        public void receiveEvent(Event event) {
            lst.receiveEvent(new Event(decorator,event.getMessage()));
        }

    }
}
