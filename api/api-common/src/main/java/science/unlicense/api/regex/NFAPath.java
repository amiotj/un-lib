

package science.unlicense.api.regex;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.regex.NFAState.Forward;

/**
 * When executing a nfa graph when can store the path informations, this allows
 * to keep track on the position of each elements while decoding.
 * 
 * @author Johann Sorel
 */
public class NFAPath extends NFAStep {
    
    public NFAPath link;

    /**
     * 
     * @param state not null
     */
    public NFAPath(NFAState state) {
        super(state);
        this.link = null;
    }
    
    /**
     * 
     * @param parent
     * @param state not null
     */
    protected NFAPath(NFAPath parent, NFAState state) {
        super(state);
        this.link = parent;
    }

    /**
     * 
     * @param parent
     * @param state
     * @param value 
     */
    public NFAPath(NFAPath parent, NFAState state, Object value) {
        super(state, value);
        this.link = parent;
    }

    /**
     * Create a path which is child of this one.
     * @param state
     * @return 
     */
    public NFAPath child(NFAState state){
        return new NFAPath(this, state);
    }
        
    public NFAState getNextState(){
        return ((Forward)state).getNextState();
    }
    
    public NFAState getNextForkState1(){
        return ((NFAState.Fork)state).getNext1State();
    }
    
    public NFAState getNextForkState2(){
        return ((NFAState.Fork)state).getNext2State();
    }

    /**
     * Create a reversed path
     * @return 
     */
    public NFAPath reverse(){
        NFAPath parent = new NFAPath(null, state, value);
        NFAPath link = this.link;
        while(link!=null){
            parent = new NFAPath(parent, link.state, link.value);
            link = link.link;
        }
        return parent;
    }

    
    public Chars toChars() {
        return toChars(true);
    }
    
    public Chars toChars(boolean withLink) {
        final CharBuffer cb = new CharBuffer();
        cb.append(CObjects.toChars(state));
        if(value!=null){
            cb.append('[');
            cb.append(CObjects.toChars(value));
            cb.append(']');
        }
        
        if(withLink && link!=null){
            cb.append(" <-> ");
            cb.append(link);
        }
        
        return cb.toChars();
    }

    public int getHash() {
        return state.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        final NFAPath other = (NFAPath) obj;
        return this.state == other.state || (this.state != null && this.state.equals(other.state));
    }

}
