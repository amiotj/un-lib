package science.unlicense.api.number;

/**
 * Numbers can be encoded in Little-Endian or Big-Endian.
 * This interface and implementation allow to read and write primitive types
 * from /to byte arrays.
 * 
 * @author Johann Sorel
 */
public interface NumberEncoding {

    public static final LittleEndian LITTLE_ENDIAN = new LittleEndian();
    public static final BigEndian BIG_ENDIAN = new BigEndian();

    /**
     * Read a boolean value.
     * @param b data
     * @return value
     */
    boolean readBoolean(int b);

    /**
     * Read a boolean value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    boolean readBoolean(byte[] buffer, int offset);

    /**
     * Read a byte value.
     * @param b data
     * @return value
     */
    byte readByte(int b);

    /**
     * Read a byte value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    byte readByte(byte[] buffer, int offset);

    /**
     * Read an unsigned byte value.
     * @param b data
     * @return value
     */
    int readUByte(int b);

    /**
     * Read an unsigned byte value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    int readUByte(byte[] buffer, int offset);

    /**
     * Read a short value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    short readShort(byte[] buffer, int offset);

    /**
     * Read an unsigned short value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    int readUShort(byte[] buffer, int offset);

    /**
     * Read a integer on 24 bits value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    int readInt24(byte[] buffer, int offset);

    /**
     * Read an unsigned integer on 24 bits value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    int readUInt24(byte[] buffer, int offset);
    
    /**
     * Read an integer value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    int readInt(byte[] buffer, int offset);

    /**
     * Read an unsigned integer value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    long readUInt(byte[] buffer, int offset);

    /**
     * Read a long value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    long readLong(byte[] buffer, int offset);

    /**
     * Read a float value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    float readFloat(byte[] buffer, int offset);

    /**
     * Read a double value at given offset in array.
     * @param buffer data array
     * @param offset position where to start reading
     * @return value
     */
    double readDouble(byte[] buffer, int offset);

    /**
     * Write a byte value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeByte(byte value, byte[] buffer, int offset);

    /**
     * Write an unsigned byte value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeUByte(int value, byte[] buffer, int offset);

    /**
     * Write a short value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeShort(short value, byte[] buffer, int offset);

    /**
     * Write an unsigned short value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeUShort(int value, byte[] buffer, int offset);

    /**
     * Write an integer on 24 bits value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeInt24(int value, byte[] buffer, int offset);

    /**
     * Write an unsigned integer on 24bits value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeUInt24(int value, byte[] buffer, int offset);
    
    /**
     * Write an integer value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeInt(int value, byte[] buffer, int offset);

    /**
     * Write an unsigned integer value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeUInt(long value, byte[] buffer, int offset);

    /**
     * Write a long value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeLong(long value, byte[] buffer, int offset);

    /**
     * Write a float value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeFloat(float value, byte[] buffer, int offset);

    /**
     * Write a double value at given offset in array.
     * @param value to write
     * @param buffer byte array to write into
     * @param offset position where to start writing
     */
    void writeDouble(double value, byte[] buffer, int offset);


    public static class BigEndian implements NumberEncoding {

        protected BigEndian(){}

        /**
         * {@inheritDoc }
         */
        public boolean readBoolean(int b) {
            return b!=0;
        }

        /**
         * {@inheritDoc }
         */
        public boolean readBoolean(byte[] buffer, int offset) {
            return readBoolean(buffer[offset]);
        }

        /**
         * {@inheritDoc }
         */
        public byte readByte(int b){
            return (byte) b;
        }

        /**
         * {@inheritDoc }
         */
        public byte readByte(byte[] buffer, int offset){
            return readByte(buffer[offset]);
        }

        /**
         * {@inheritDoc }
         */
        public int readUByte(int b){
            return b & 0xff;
        }

        /**
         * {@inheritDoc }
         */
        public int readUByte(byte[] buffer, int offset){
            return readUByte(buffer[offset]);
        }

        /**
         * {@inheritDoc }
         */
        public short readShort(byte[] buffer, int offset){
            return (short) ( (buffer[offset+0] & 0xff) <<  8
                           | (buffer[offset+1] & 0xff) <<  0  );
        }

        /**
         * {@inheritDoc }
         */
        public int readUShort(byte[] buffer, int offset){
            return (buffer[offset+0] & 0xff) <<  8
                 | (buffer[offset+1] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public int readInt24(byte[] buffer, int offset){
            int v = (buffer[offset+0] & 0xff) << 16
                 | (buffer[offset+1] & 0xff) <<  8
                 | (buffer[offset+2] & 0xff) <<  0 ;
            return (v << 8) >> 8;
        }

        /**
         * {@inheritDoc }
         */
        public int readUInt24(byte[] buffer, int offset){
            return (buffer[offset+0] & 0xff) << 16
                 | (buffer[offset+1] & 0xff) <<  8
                 | (buffer[offset+2] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public int readInt(byte[] buffer, int offset){
            return (buffer[offset+0] & 0xff) << 24
                 | (buffer[offset+1] & 0xff) << 16
                 | (buffer[offset+2] & 0xff) <<  8
                 | (buffer[offset+3] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public long readUInt(byte[] buffer, int offset){
            return (long) (buffer[offset+0] & 0xff) << 24
                 | (long) (buffer[offset+1] & 0xff) << 16
                 | (long) (buffer[offset+2] & 0xff) <<  8
                 | (long) (buffer[offset+3] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public long readLong(byte[] buffer, int offset){
            return (long) (buffer[offset+0] & 0xff) << 56
                 | (long) (buffer[offset+1] & 0xff) << 48
                 | (long) (buffer[offset+2] & 0xff) << 40
                 | (long) (buffer[offset+3] & 0xff) << 32
                 | (long) (buffer[offset+4] & 0xff) << 24
                 | (long) (buffer[offset+5] & 0xff) << 16
                 | (long) (buffer[offset+6] & 0xff) <<  8
                 | (long) (buffer[offset+7] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public float readFloat(byte[] buffer, int offset){
            return Float.intBitsToFloat(readInt(buffer, offset));
        }

        /**
         * {@inheritDoc }
         */
        public double readDouble(byte[] buffer, int offset){
            return Double.longBitsToDouble(readLong(buffer,offset));
        }

        /**
         * {@inheritDoc }
         */
        public void writeByte(byte value, byte[] buffer, int offset){
            buffer[offset] = value;
        }

        /**
         * {@inheritDoc }
         */
        public void writeUByte(int value, byte[] buffer, int offset){
            buffer[offset] = (byte) value;
        }

        /**
         * {@inheritDoc }
         */
        public void writeShort(short value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 8) & 0xff);
            buffer[offset+1] = (byte) ((value >> 0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeUShort(int value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 8) & 0xff);
            buffer[offset+1] = (byte) ((value >> 0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeInt24(int value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+2] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeUInt24(int value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+2] = (byte) ((value >>  0) & 0xff);
        }
        
        /**
         * {@inheritDoc }
         */
        public void writeInt(int value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 24) & 0xff);
            buffer[offset+1] = (byte) ((value >> 16) & 0xff);
            buffer[offset+2] = (byte) ((value >>  8) & 0xff);
            buffer[offset+3] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeUInt(long value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 24) & 0xff);
            buffer[offset+1] = (byte) ((value >> 16) & 0xff);
            buffer[offset+2] = (byte) ((value >>  8) & 0xff);
            buffer[offset+3] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeLong(long value, byte[] buffer, int offset){
            buffer[offset+0] = (byte) ((value >> 56) & 0xff);
            buffer[offset+1] = (byte) ((value >> 48) & 0xff);
            buffer[offset+2] = (byte) ((value >> 40) & 0xff);
            buffer[offset+3] = (byte) ((value >> 32) & 0xff);
            buffer[offset+4] = (byte) ((value >> 24) & 0xff);
            buffer[offset+5] = (byte) ((value >> 16) & 0xff);
            buffer[offset+6] = (byte) ((value >>  8) & 0xff);
            buffer[offset+7] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeFloat(float value, byte[] buffer, int offset){
            writeInt(Float.floatToRawIntBits(value),buffer,offset);
        }

        /**
         * {@inheritDoc }
         */
        public void writeDouble(double value, byte[] buffer, int offset){
            writeLong(Double.doubleToRawLongBits(value),buffer,offset);
        }

    }

    public static class LittleEndian implements NumberEncoding {

        protected LittleEndian(){}

        /**
         * {@inheritDoc }
         */
        public boolean readBoolean(int b) {
            return b!=0;
        }

        /**
         * {@inheritDoc }
         */
        public boolean readBoolean(byte[] buffer, int offset) {
            return readBoolean(buffer[offset]);
        }

        /**
         * {@inheritDoc }
         */
        public byte readByte(int b){
            return (byte) b;
        }

        /**
         * {@inheritDoc }
         */
        public byte readByte(byte[] buffer, int offset){
            return readByte(buffer[offset]);
        }

        /**
         * {@inheritDoc }
         */
        public int readUByte(int b){
            return b & 0xff;
        }

        /**
         * {@inheritDoc }
         */
        public int readUByte(byte[] buffer, int offset){
            return readUByte(buffer[offset]);
        }

        /**
         * {@inheritDoc }
         */
        public short readShort(byte[] buffer, int offset){
            return (short) ( (buffer[offset+1] & 0xff) <<  8
                           | (buffer[offset+0] & 0xff) <<  0  );
        }

        /**
         * {@inheritDoc }
         */
        public int readUShort(byte[] buffer, int offset){
            return (buffer[offset+1] & 0xff) <<  8
                 | (buffer[offset+0] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public int readInt24(byte[] buffer, int offset){
            int v = (buffer[offset+2] & 0xff) << 16
                 | (buffer[offset+1] & 0xff) << 8
                 | (buffer[offset+0] & 0xff) << 0;
            return (v << 8) >> 8;
        }

        /**
         * {@inheritDoc }
         */
        public int readUInt24(byte[] buffer, int offset){
            return (buffer[offset+2] & 0xff) << 16
                 | (buffer[offset+1] & 0xff) <<  8
                 | (buffer[offset+0] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public int readInt(byte[] buffer, int offset){
            return (buffer[offset+3] & 0xff) << 24
                 | (buffer[offset+2] & 0xff) << 16
                 | (buffer[offset+1] & 0xff) << 8
                 | (buffer[offset+0] & 0xff) << 0;
        }

        /**
         * {@inheritDoc }
         */
        public long readUInt(byte[] buffer, int offset){
            return (long) (buffer[offset+3] & 0xff) << 24
                 | (long) (buffer[offset+2] & 0xff) << 16
                 | (long) (buffer[offset+1] & 0xff) <<  8
                 | (long) (buffer[offset+0] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public long readLong(byte[] buffer, int offset){
            return (long) (buffer[offset+7] & 0xff) << 56
                 | (long) (buffer[offset+6] & 0xff) << 48
                 | (long) (buffer[offset+5] & 0xff) << 40
                 | (long) (buffer[offset+4] & 0xff) << 32
                 | (long) (buffer[offset+3] & 0xff) << 24
                 | (long) (buffer[offset+2] & 0xff) << 16
                 | (long) (buffer[offset+1] & 0xff) <<  8
                 | (long) (buffer[offset+0] & 0xff) <<  0 ;
        }

        /**
         * {@inheritDoc }
         */
        public float readFloat(byte[] buffer, int offset){
            return Float.intBitsToFloat(readInt(buffer, offset));
        }

        /**
         * {@inheritDoc }
         */
        public double readDouble(byte[] buffer, int offset){
            return Double.longBitsToDouble(readLong(buffer,offset));
        }

        
        /**
         * {@inheritDoc }
         */
        public void writeByte(byte value, byte[] buffer, int offset){
            buffer[offset] = value;
        }

        /**
         * {@inheritDoc }
         */
        public void writeUByte(int value, byte[] buffer, int offset){
            buffer[offset] = (byte) value;
        }

        /**
         * {@inheritDoc }
         */
        public void writeShort(short value, byte[] buffer, int offset){
            buffer[offset+1] = (byte) ((value >> 8) & 0xff);
            buffer[offset+0] = (byte) ((value >> 0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeUShort(int value, byte[] buffer, int offset){
            buffer[offset+1] = (byte) ((value >> 8) & 0xff);
            buffer[offset+0] = (byte) ((value >> 0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeInt24(int value, byte[] buffer, int offset){
            buffer[offset+2] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+0] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeUInt24(int value, byte[] buffer, int offset){
            buffer[offset+2] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+0] = (byte) ((value >>  0) & 0xff);
        }
        
        /**
         * {@inheritDoc }
         */
        public void writeInt(int value, byte[] buffer, int offset){
            buffer[offset+3] = (byte) ((value >> 24) & 0xff);
            buffer[offset+2] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+0] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeUInt(long value, byte[] buffer, int offset){
            buffer[offset+3] = (byte) ((value >> 24) & 0xff);
            buffer[offset+2] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+0] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeLong(long value, byte[] buffer, int offset){
            buffer[offset+7] = (byte) ((value >> 56) & 0xff);
            buffer[offset+6] = (byte) ((value >> 48) & 0xff);
            buffer[offset+5] = (byte) ((value >> 40) & 0xff);
            buffer[offset+4] = (byte) ((value >> 32) & 0xff);
            buffer[offset+3] = (byte) ((value >> 24) & 0xff);
            buffer[offset+2] = (byte) ((value >> 16) & 0xff);
            buffer[offset+1] = (byte) ((value >>  8) & 0xff);
            buffer[offset+0] = (byte) ((value >>  0) & 0xff);
        }

        /**
         * {@inheritDoc }
         */
        public void writeFloat(float value, byte[] buffer, int offset){
            writeInt(Float.floatToRawIntBits(value),buffer,offset);
        }

        /**
         * {@inheritDoc }
         */
        public void writeDouble(double value, byte[] buffer, int offset){
            writeLong(Double.doubleToRawLongBits(value),buffer,offset);
        }

    }

}
