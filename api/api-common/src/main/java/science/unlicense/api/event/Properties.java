
package science.unlicense.api.event;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

import java.lang.reflect.Method;

/**
 * Utility methods related to properties.
 * 
 * @author Johann Sorel
 */
public final class Properties {
    
    
    
    /**
     * Find all properties with a getter and a setter.
     * 
     * @param candidate
     * @return
     */
    public static Sequence listProperties(Class candidate){
        final Method[] methods = candidate.getMethods();

        final Sequence names = new ArraySequence();

        for(Method method : methods){
            final String name = method.getName();
            if(name.startsWith("set") && Void.TYPE.equals(method.getReturnType()) && method.getParameterTypes().length==1){
                final String propertyName = name.substring(3);
                final Class propertyClass = method.getParameterTypes()[0];
                final String name1 = "get"+propertyName;
                final String name2 = "is"+propertyName;
                for(Method getterMethod : methods){
                    final String cname = getterMethod.getName();
                    final Class<?> returnType = getterMethod.getReturnType();
                    if((cname.equals(name1) || cname.equals(name2)) && getterMethod.getParameterTypes().length==0 && propertyClass.isAssignableFrom(returnType)){
                        names.add(new Chars(propertyName));
                    }
                }
            }
        }

        return names;
    }

    public static Class getBoxingClass(Class c){
        if(c.isPrimitive()){
            if(boolean.class.equals(c)) c = Boolean.class;
            else if(byte.class.equals(c)) c = Byte.class;
            else if(short.class.equals(c)) c = Short.class;
            else if(char.class.equals(c)) c = Character.class;
            else if(int.class.equals(c)) c = Integer.class;
            else if(long.class.equals(c)) c = Long.class;
            else if(float.class.equals(c)) c = Float.class;
            else if(double.class.equals(c)) c = Double.class;
        }
        return c;
    }

}
