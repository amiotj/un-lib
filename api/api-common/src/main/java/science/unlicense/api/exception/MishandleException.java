
package science.unlicense.api.exception;

/**
 * Mishandle exception are used to indicate a wrong use of a class or function.
 * The most common cases include invalid or null arguments.
 *
 * Those exceptions are caused by incorrect algorithms.
 *
 * @author Johann Sorel
 */
public class MishandleException extends RuntimeException {

    public MishandleException() {
    }

    public MishandleException(String s) {
        super(s);
    }

    public MishandleException(Throwable cause) {
        super(cause);
    }

    public MishandleException(String message, Throwable cause) {
        super(message, cause);
    }

}
