
package science.unlicense.api.logging;

import science.unlicense.api.character.Chars;
import science.unlicense.api.logging.Logger;

/**
 * Abstract logger.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractLogger implements Logger {


    /**
     * {@inheritDoc }
     */
    public void log(Throwable exception, float level) {
        log(exception.getMessage()!=null ? new Chars(exception.getMessage()) : null,exception,level);
    }

    /**
     * {@inheritDoc }
     */
    public void log(Chars message, float level) {
        log(message, null, level);
    }

    /**
     * {@inheritDoc }
     */
    public void debug(Chars message) {
        debug(message, null);
    }

    /**
     * {@inheritDoc }
     */
    public void debug(Chars message, Throwable exception) {
        log(message, exception, LEVEL_DEBUG);
    }
    
    /**
     * {@inheritDoc }
     */
    public void debug(Throwable exception) {
        log(exception, LEVEL_DEBUG);
    }

    /**
     * {@inheritDoc }
     */
    public void info(Chars message) {
        info(message, null);
    }

    /**
     * {@inheritDoc }
     */
    public void info(Chars message, Throwable exception) {
        log(message, exception, LEVEL_INFORMATION);
    }
    
    /**
     * {@inheritDoc }
     */
    public void info(Throwable exception) {
        log(exception, LEVEL_INFORMATION);
    }

    /**
     * {@inheritDoc }
     */
    public void warning(Chars message) {
        warning(message, null);
    }

    /**
     * {@inheritDoc }
     */
    public void warning(Chars message, Throwable exception) {
        log(message, exception, LEVEL_WARNING);
    }
    
    /**
     * {@inheritDoc }
     */
    public void warning(Throwable exception) {
        log(exception, LEVEL_WARNING);
    }

    /**
     * {@inheritDoc }
     */
    public void critical(Chars message) {
        critical(message, null);
    }

    /**
     * {@inheritDoc }
     */
    public void critical(Chars message, Throwable exception) {
        log(message, exception, LEVEL_CRITICAL);
    }
    
    /**
     * {@inheritDoc }
     */
    public void critical(Throwable exception) {
        log(exception, LEVEL_CRITICAL);
    }

}
