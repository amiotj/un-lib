
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDoubleCursor extends AbstractCursor implements DoubleCursor{

    final Buffer buffer;
    
    //current offset from start.
    private long byteOffset = 0;

    public DefaultDoubleCursor(Buffer buffer, NumberEncoding encoding) {
        super(encoding);
        this.buffer = buffer;
    }
    
    public Buffer getBuffer() {
        return buffer;
    }

    public long getByteOffset() {
        return byteOffset;
    }

    public long getPosition() {
        return byteOffset/4;
    }

    public void setPosition(long position) {
        byteOffset = position*4;
    }

    public double read(long position) {
        return buffer.readDouble(position*4);
    }

    public void read(double[] array, long position) {
        buffer.readDouble(array,position*4);
    }

    public void read(double[] array, int arrayOffset, int length, long position) {
        buffer.readDouble(array,arrayOffset,length,position*4);
    }

    public DoubleCursor write(double value, long position) {
        buffer.writeDouble(value, position*4);
        return this;
    }

    public DoubleCursor write(double[] array, long position) {
        return write(array, 0, array.length, position);
    }

    public DoubleCursor write(double[] array, int arrayOffset, int length, long position) {
        buffer.writeDouble(array,arrayOffset,length, position*4);
        return this;
    }

    public double read() {
        double v = buffer.readDouble(byteOffset);
        byteOffset +=4;
        return v;
    }

    public void read(double[] array) {
        read(array,0,array.length);
    }

    public void read(double[] array, int arrayOffset, int length) {
        buffer.readDouble(array,byteOffset);
        byteOffset+=length*4;
    }

    public DoubleCursor write(double value) {
        buffer.writeDouble(value, byteOffset);
        byteOffset += 4;
        return this;
    }

    public DoubleCursor write(double[] array) {
        return write(array, 0, array.length);
    }

    public DoubleCursor write(double[] array, int arrayOffset, int length) {
        buffer.writeDouble(array,arrayOffset,length, byteOffset);
        byteOffset += length*4;
        return this;
    }

}
