
package science.unlicense.api.collection;

import science.unlicense.api.CObjects;


/**
 * Objects always have a default hash value which is sufficient for most cases.
 * More optimized hashes could be build when various conditions are met.
 * 
 * Used by hash collections to compute hash value and equality between objects.
 * 
 * @author Johann Sorel
 */
public interface Hasher {

    /**
     * Default hasher, used objects hash and equals method.
     */
    public static final Hasher DEFAULT = new Hasher(){
        public int getHash(Object candidate) {
            return CObjects.getHash(candidate);
        }
        public boolean equals(Object a, Object b) {
            return a==b
                || (!(a==null || b==null) && a.equals(b));
        }
    };
    
    /**
     * Hasher which test equality with an identity test.
     */
    public static final Hasher IDENTITY = new Hasher(){
        public int getHash(Object candidate) {
            return CObjects.getHash(candidate);
        }
        public boolean equals(Object a, Object b) {
            return a == b;
        }
    };
    
    /**
     * Calculate hash of given object.
     * 
     * @param candidate
     * @return hash value
     */
    int getHash(Object candidate);
    
    /**
     * Test if the two given objects are equals.
     * Any two objects that are equal must have the same hash value.
     * 
     * @param a
     * @param b
     * @return true if objects are equal.
     */
    boolean equals(Object a, Object b);
    
}
