
package science.unlicense.api.character;

/**
 * Character encoding exception.
 * 
 * @author Johann Sorel
 */
public class CharEncodingException extends RuntimeException {

    public CharEncodingException() {
    }

    public CharEncodingException(String message) {
        super(message);
    }

    public CharEncodingException(Throwable cause) {
        super(cause);
    }

    public CharEncodingException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
