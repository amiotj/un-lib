
package science.unlicense.api.collection.primitive;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.AbstractSequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.InvalidIndexException;

/**
 *
 * @author Johann Sorel
 */
public final class BooleanSequence extends AbstractSequence implements PrimitiveSequence {

    private boolean[] buffer;
    private int size = 0;
    private int nextIndex = 0;

    public BooleanSequence() {
        buffer = new boolean[100];
        size = 0;
    }

    public BooleanSequence(boolean[] buffer) {
        CObjects.ensureNotNull(buffer);
        this.buffer = buffer;
        this.size = buffer.length;
    }

    private void growIfNecessary(int length){
        final int nindex = nextIndex+length;
        if(nindex >= buffer.length){
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
        if(nindex>=size) size = nindex;
    }

    /**
     * move one value ahead preserving buffer value or
     * filling it with zero if buffer was resized.
     */
    public void skip(){
        growIfNecessary(1);
        nextIndex++;
    }
    
    /**
     * move one value ahead preserving buffer value or
     * filling it with zero if buffer was resized.
     * @param nb
     */
    public void skip(int nb){
        growIfNecessary(nb);
        nextIndex+=nb;
    }

    public void put(boolean b){
        growIfNecessary(1);
        buffer[nextIndex] = b;
        nextIndex++;
    }

    public void put(boolean[] b){
        put(b, 0, b.length);
    }

    public void put(boolean[] b, int offset, int length){
        final int nindex = nextIndex+length;
        growIfNecessary(length);
        Arrays.copy(b, offset, length, buffer, nextIndex);
        nextIndex = nindex;
    }

    /**
     * @return the underlying boolean array,
     * it's size might be larger then the current buffer size.
     */
    public boolean[] getBackArray() {
        return buffer;
    }

    /**
     * @return boolean array
     */
    public boolean[] toArrayBoolean() {
        return Arrays.copy(buffer, 0, size, new boolean[size], 0);
    }

    public Object[] toArray() {
        final Object[] array = new Object[size];
        for(int i=0;i<array.length;i++){
            array[i] = buffer[i];
        }
        return array;
    }
    
    public boolean read(int index){
        return buffer[index];
    }

    public int read(int index, boolean[] buffer, int offset, int length){
        int nb = length;
        if(getSize()-index < nb) nb = getSize()-index;

        Arrays.copy(this.buffer, index, nb, buffer, offset);

        return nb;
    }

    /**
     * Test if boolean buffer starts by a given array.
     * @param buffer start array
     * @return true if the buffer starts with the given serie of booleans.
     */
    public boolean startWidth(boolean[] buffer){
        return startWidth(buffer, 0, buffer.length);
    }

    /**
     * Test if boolean buffer starts by a given array.
     * @param buffer start array
     * @param offset start offset
     * @param length number of elements to test
     * @return true if the buffer starts with the given serie of booleans.
     */
    public boolean startWidth(boolean[] buffer, int offset, int length){
        return Arrays.equals(buffer, offset, length, this.buffer, 0);
    }

    /**
     * Test if boolean buffer ends by a given array.
     * @param buffer end array
     * @return true if the buffer ends with the given serie of booleans.
     */
    public boolean endWidth(boolean[] buffer){
        return endWidth(buffer, 0, buffer.length);
    }

    /**
     * Test if boolean buffer ends by a given array.
     * @param buffer end array
     * @param offset end offset
     * @param length number of elements to test
     * @return true if the buffer ends with the given serie of booleans.
     */
    public boolean endWidth(boolean[] buffer, int offset, int length){
        return Arrays.equals(buffer, offset, length, this.buffer, size-length);
    }

    /**
     * Remove given number of values at buffer start.
     * @param nbVal number of values to remove
     */
    public void trimStart(int nbVal){
        Arrays.copy(buffer, nbVal, size-nbVal, buffer, 0);
        nextIndex -= nbVal;
        size -= nbVal;
    }

    /**
     * Remove given number of values at buffer end.
     * @param nbVal number of values to remove
     */
    public void trimEnd(int nbVal){
        nextIndex -= nbVal;
        size -= nbVal;
    }

    public int getSize(){
        return size;
    }

    public boolean removeAll(){
        final boolean hasListener = hasListeners();
        Object[] removed = null;
        if(hasListener){
            removed = toArray();
        }
        
        nextIndex = 0;
        size = 0;
        buffer = new boolean[1000];
        
        if(hasListener){
            fireRemove(-1, -1, removed);
        }
        return true;
    }

    private void growInsertIfNecessary(int length){
        final int nsize = size+length;
        if(nsize >= buffer.length){
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
        if(nsize>=size) size = nsize;
    }
    
    public BooleanSequence moveTo(int position){
        nextIndex = position;
        return this;
    }
    
    /**
     * Remove the N next elements.
     * @param nbElement 
     */
    public boolean removeNext(int nbElement){
        if(nextIndex+nbElement>size){
            throw new InvalidArgumentException("Too much elements to remove "+nbElement+", remaining size is "+(size-nextIndex));
        }
        if(nextIndex+nbElement<size){
            Arrays.copy(buffer, nextIndex+nbElement, nbElement, buffer, nextIndex);
        }
        size -= nbElement;
        return true;
    }
    
    public BooleanSequence insert(boolean b){
        growInsertIfNecessary(1);
        for(int i=size-1;i>=nextIndex;i--){
            buffer[i+1] = buffer[i];
        }
        buffer[nextIndex] = b;
        nextIndex++;
        return this;
    }

    public BooleanSequence insert(boolean[] b){
        return insert(b, 0, b.length);
    }

    public BooleanSequence insert(boolean[] b, int offset, int length){
        growInsertIfNecessary(length);
        for(int i=size-1;i>=nextIndex;i--){
            buffer[i+length] = buffer[i];
        }
        Arrays.copy(b, 0, b.length, buffer, nextIndex);
        return this;
    }
    
    public Object get(int index) {
        if(index<0 || index>=size) throw new InvalidIndexException(""+index);
        return read(index);
    }

    public boolean add(int index, Object value) {
        growIfNecessary(size + 1);
        for(int i=size-1;i>=index;i--){
            buffer[i+1] = buffer[i];
        }
        buffer[index] = (Boolean)value;
        if(hasListeners()) fireAdd(index, index, new Object[]{value});
        return true;
    }

    public boolean remove(int index) {
        if(index < 0 || index >= size) {
            throw new InvalidIndexException(""+index);
        }
        final Object removedValue = buffer[index];
        if(index+1 < size){
            Arrays.copy(buffer, index+1, size-index-1, buffer, index);
        }
        size--;
        if(hasListeners()) fireRemove(index, index, new Object[]{removedValue});
        return true;
    }

}
