
package science.unlicense.api.collection;

import science.unlicense.api.CObjects;
import science.unlicense.api.exception.InvalidIndexException;
import science.unlicense.api.exception.UnmodifiableException;

/**
 * Optimized singleton sequence.
 *
 * @author Johann Sorel
 */
final class SingletonSequence extends AbstractCollection implements Sequence {

    private final Object singleton;

    SingletonSequence(Object singleton) {
        this.singleton = singleton;
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Singleton sequence, writing not supported");
    }

    public boolean remove(Object candidate) {
        return false;
    }

    public Iterator createIterator() {
        return Collections.singletonIterator(singleton);
    }

    public Object get(int index) {
        if (index == 0) {
            return singleton;
        }
        throw new InvalidIndexException("Unvalid index");
    }

    public boolean add(int index, Object value) {
        throw new UnmodifiableException("Empty sequence, writing not supported");
    }

    public boolean addAll(int index, Collection candidate) {
        throw new UnmodifiableException("Empty sequence, writing not supported");
    }

    public boolean addAll(int index, Object[] candidate) {
        throw new UnmodifiableException("Empty sequence, writing not supported");
    }

    public Object replace(int index, Object item) {
        throw new UnmodifiableException("Empty sequence, writing not supported");
    }

    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Empty sequence, writing not supported");
    }

    public boolean remove(int index) {
        throw new UnmodifiableException("Empty sequence, writing not supported");
    }

    public int search(Object item) {
        return CObjects.equals(item, singleton) ? 0 : -1;
    }

    public Iterator createReverseIterator() {
        return createIterator();
    }

    public int getSize() {
        return 1;
    }

}
