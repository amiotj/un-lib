
package science.unlicense.api.exception;

/**
 * Special case of mishandle exception for attempts to alter an unmodifiable
 * property or object.
 * 
 * @author Johann Sorel
 */
public class UnmodifiableException extends MishandleException {

    public UnmodifiableException() {
    }

    public UnmodifiableException(String s) {
        super(s);
    }

    public UnmodifiableException(Throwable cause) {
        super(cause);
    }

    public UnmodifiableException(String message, Throwable cause) {
        super(message, cause);
    }

}
