
package science.unlicense.api.character;

/**
 * Represent a character encoding.
 * While in reality this class should be divided in :
 * - character repertoire
 * - character code
 * - character encoding
 * It is in practice hard to achieve since character can not be disociate from their
 * numeric representation.
 *
 * @author Johann Sorel
 */
public interface CharEncoding {

    /**
     * Encoding name, in US-ASCII
     * @return CharSequence
     */
    Chars getName();

    /**
     * return the size of the next character.
     * @param array encoded characters array
     * @param offset start offset
     * @return number of byte used for the character
     */
    int charlength(byte[] array, int offset);

    /**
     * return the number of character in the array.
     * @param array encoded characters array
     * @return number of characters
     */
    int length(byte[] array);

    /**
     * Decode the given bytes forming a character into a unicode code point.
     * @param array to read from
     * @param offset start read at this position
     * @param codePoint used to store result,
     *     int array : [0] unicode code point, [1] character byte length
     */
    void toUnicode(byte[] array, int[] codePoint, int offset);
    
    /**
     * Decode the given bytes forming a character into a unicode code point.
     * @param array to read from
     * @return unicode codepoint
     */
    int toUnicode(byte[] array);
    
    /**
     * Encode the given unicode code point in bytes.
     * @param codePoint unicode code point
     * @return byte array
     */
    byte[] toBytes(int codePoint);
    
    /**
     * Encode the given unicode code points in bytes.
     * @param codePoints unicode code point
     * @return byte array
     */
    byte[] toBytes(int[] codePoints);

    /**
     * Encode the given unicode code point in bytes.
     * @param codePoint int array : [0] unicode code point, [1] written character byte length
     * @param array to write in starting at index 0
     * @param offset start writing at this position
     */
    void toBytes(int[] codePoint, byte[] array, int offset);

    /**
     * Test if the given byte array form a complet character.
     * @param array to read from
     * @param offset start read at this position
     * @param length number of bytes to read
     * @return true if it define a complete character.
     */
    boolean isComplete(byte[] array, int offset, int length);
}
