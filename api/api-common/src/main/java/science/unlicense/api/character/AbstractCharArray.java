
package science.unlicense.api.character;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;

import java.io.UnsupportedEncodingException;
import science.unlicense.api.CObject;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 * Abstract character array implementation.
 * Supports all operations falling back on getBytes() and getEncoding() methods
 *
 * @author Johann Sorel
 */
public abstract class AbstractCharArray extends CObject implements CharArray{

    //compute at first request
    //TODO not perfect, very low risk of having an hash with this value, but might happen
    private int hash = Integer.MAX_VALUE;
    //compute at first request
    private int length = -1;
    
    /**
     * Access the underlying char sequence byte array.
     * @return byte array, never null
     */
    protected abstract byte[] getBytesInternal();
    
    /**
     * Indicate if the byte array returned by getBytesInternal is safe to be modified.
     * @return true if modifiable, false if it must not be modified
     */
    protected abstract boolean isByteInternalCopy();
    
    /**
     * {@inheritDoc}
     */
    public int getCharLength(){
        if(length==-1) length = getEncoding().length(getBytesInternal());
        return length;
    }

    /**
     * {@inheritDoc}
     */
    public int getByteLength(){
        return getBytesInternal().length;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEmpty(){
        return getBytesInternal().length == 0;
    }

    /**
     * {@inheritDoc}
     */
    public Char getCharacter(int index){
        final int offset = toByteOffset(index);
        final CharEncoding encoding = getEncoding();
        final byte[] data = getBytesInternal();
        final int size = encoding.charlength(data, offset);
        return new Char(Arrays.copy(data, offset, size, new byte[size],0), encoding);
    }

    /**
     * {@inheritDoc}
     */
    public int getUnicode(int index){
        final int offset = toByteOffset(index);
        final int[] cp = new int[2];
        getEncoding().toUnicode(getBytesInternal(), cp, offset);
        return cp[0];
    }

    /**
     * {@inheritDoc}
     */
    public byte getByte(int index){
        return getBytesInternal()[index];
    }

    /**
     * {@inheritDoc}
     */
    public CharArray truncate(int start, int end){
        if(end!=-1 && end<start) throw new InvalidArgumentException("Unvalid indexes start:"+start+" end:"+end);
        final CharEncoding encoding = getEncoding();
        final byte[] data = getBytesInternal();
        int offset = 0;
        for(int i=0;i<start;i++){
            offset += encoding.charlength(data, offset);
        }
        int offsetend;
        if (end==-1) {
            offsetend = getByteLength();
        } else {
            offsetend = offset;
            for(int i=start;i<end;i++){
                offsetend += encoding.charlength(data, offsetend);
            }
        }
        final int datasize = offsetend-offset;
        return new Chars(Arrays.copy(data,offset,datasize,new byte[datasize],0),encoding);
    }

    /**
     * {@inheritDoc}
     */
    public boolean startsWith(int cp){
        return startsWith(getEncoding().toBytes(cp));
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean startsWith(Char ch){
        return startsWith(ch.toEncodingBytes(getEncoding()));
    }

    /**
     * {@inheritDoc}
     */
    public boolean startsWith(CharArray cs){
        return startsWith(toThisEncodingBytes(cs));
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean startsWith(byte[] cdata){
        return Arrays.equals(cdata,0,cdata.length,getBytesInternal(),0);
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(CharArray cs){
        return endsWith(toThisEncodingBytes(cs));
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(Char ch){
        return endsWith(ch.toEncodingBytes(getEncoding()));
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(int ch){
        return endsWith(getEncoding().toBytes(ch));
    }

    /**
     * {@inheritDoc}
     */
    public boolean endsWith(byte[] cdata){
        final byte[] data = getBytesInternal();
        return Arrays.equals(cdata,0,cdata.length,data,data.length-cdata.length);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(CharArray cs){
        return getFirstOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(CharArray cs, int startPosition){
        return getFirstOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(Char ch){
        return getFirstOccurence(ch.toEncodingBytes(getEncoding()), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(Char ch, int startPosition){
        return getFirstOccurence(ch.toEncodingBytes(getEncoding()), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(int codepoint){
        return getFirstOccurence(getEncoding().toBytes(codepoint),0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(int codepoint, int startPosition){
        return getFirstOccurence(getEncoding().toBytes(codepoint),startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(byte[] cd){
        return getFirstOccurence(cd, 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getFirstOccurence(byte[] cd, int startPosition){
        final CharEncoding enc = getEncoding();
        final byte[] data = getBytesInternal();
        int offset = toByteOffset(startPosition);
        for(int charindex=0; offset<data.length;charindex++){
            if(Arrays.equals(cd,0,cd.length,data,offset)){
                //found it
                return startPosition+charindex;
            }
            offset += enc.charlength(data,offset);
        }

        //not found
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(CharArray cs){
        return getLastOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(CharArray cs, int startPosition){
        return getLastOccurence(toThisEncodingBytes(cs), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(Char ch){
        return getLastOccurence(ch.toEncodingBytes(getEncoding()), 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(Char ch, int startPosition){
        return getLastOccurence(ch.toEncodingBytes(getEncoding()), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(int codepoint){
        return getLastOccurence(getEncoding().toBytes(codepoint),0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(int codepoint, int startPosition){
        return getLastOccurence(getEncoding().toBytes(codepoint),startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(byte[] cd){
        return getLastOccurence(cd, 0);
    }

    /**
     * {@inheritDoc}
     */
    public int getLastOccurence(byte[] cd, int startPosition){
        final CharEncoding enc = getEncoding();
        final byte[] data = getBytesInternal();
        int offset = toByteOffset(startPosition);
        int lastOcc = -1;
        for(int charindex=0; offset<data.length;charindex++){
            if(Arrays.equals(cd,0,cd.length,data,offset)){
                //found it
                lastOcc = startPosition+charindex;
            }
            offset += enc.charlength(data,offset);
        }

        return lastOcc;
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(int original, int replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(int original, int replacement, int startPosition){
        return replaceAll(getEncoding().toBytes(original), getEncoding().toBytes(replacement), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(Char original, Char replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(Char original, Char replacement, int startPosition){
        return replaceAll(original.toBytes(getEncoding()), replacement.toBytes(getEncoding()), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(CharArray original, CharArray replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(CharArray original, CharArray replacement, int startPosition){
        return replaceAll(toThisEncodingBytes(original), toThisEncodingBytes(replacement), startPosition);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(byte[] original, byte[] replacement){
        return replaceAll(original, replacement, 0);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray replaceAll(byte[] original, byte[] replacement, int startPosition){
        final CharEncoding enc = getEncoding();
        final CharBuffer buffer = new CharBuffer(enc);
        final byte[] data = getBytesInternal();
        int offset = toByteOffset(startPosition);
        buffer.append(Arrays.copy(data, 0, offset));

        for(;offset<data.length;){
            if(Arrays.equals(original,0,original.length,data,offset)){
                //found one
                buffer.append(replacement);
                offset += original.length;
            }else{
                final int length = enc.charlength(data,offset);
                buffer.append(Arrays.copy(data, offset, length));
                offset += length;
            }
        }

        return buffer.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public byte[] toBytes(){
        final byte[] data = getBytesInternal();
        if(isByteInternalCopy()) return data;
        //make a copy
        return Arrays.copy(data, 0, data.length, new byte[data.length], 0);
    }

    /**
     * {@inheritDoc}
     */
    public byte[] toBytes(CharEncoding encoding){
        if(getEncoding().equals(encoding)){
            return toBytes();
        }else{
            return Characters.recode(getBytesInternal(), getEncoding(), encoding);
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean isUpperCase() {
        return isUpperCase(Languages.UNSET);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isUpperCase(CharCase lf) {
        return lf.isUpperCase(this);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isLowerCase() {
        return isLowerCase(Languages.UNSET);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isLowerCase(CharCase lf) {
        return lf.isLowerCase(this);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray toUpperCase(){
        return toUpperCase(Languages.UNSET);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray toUpperCase(CharCase lf){
        return lf.toUpperCase(this);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray toLowerCase(){
        return toLowerCase(Languages.UNSET);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray toLowerCase(CharCase lf){
        return lf.toLowerCase(this);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray trim(){
        final CharIterator ite =createIterator();
        int start=0;
        int end =0;
        int index = 0;
        while(ite.hasNext()){
            int uni = ite.nextToUnicode();
            if(uni==9 || uni==32 || uni==10 || uni==13){
                if(index==start){
                    start++;
                }
            }else{
                end = index+1;
            }
            index++;
        }
        if(end<start) {
            //empty string
            final CharEncoding enc = getEncoding();
            if (enc==CharEncodings.DEFAULT) {
                return Chars.EMPTY;
            } else {
                return new Chars(Arrays.ARRAY_BYTE_EMPTY,enc);
            }
        }
        return truncate(start, end);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray trimStart(){
        final CharIterator ite =createIterator();
        int start=0;
        int end =getCharLength();
        while(ite.hasNext()){
            int uni = ite.nextToUnicode();
            if(uni==9 || uni==32 || uni==10 || uni==13){
                start++;
            }else{
                break;
            }
        }
        return truncate(start, end);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray trimEnd(){
        final CharIterator ite =createIterator();
        int start=0;
        int end =0;
        int index = 0;
        while(ite.hasNext()){
            int uni = ite.nextToUnicode();
            if(uni==9 || uni==32 || uni==10 || uni==13){
                if(index==start){
                    start++;
                }
            }else{
                end = index+1;
            }
            index++;
        }
        return truncate(0, end);
    }

    /**
     * {@inheritDoc}
     */
    public CharArray concat(int unicode){
        final CharBuffer cb = new CharBuffer(getEncoding());
        cb.append((CharArray)this);
        cb.append(unicode);
        return cb.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public CharArray concat(Char other){
        final CharBuffer cb = new CharBuffer(getEncoding());
        cb.append((CharArray)this);
        cb.append(other);
        return cb.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public CharArray concat(CharArray other){
        final CharBuffer cb = new CharBuffer(getEncoding());
        cb.append((CharArray)this);
        cb.append(other);
        return cb.toChars();
    }

    /**
     * {@inheritDoc}
     */
    public CharArray recode(final CharEncoding encoding){
        return Characters.recode(this,encoding);
    }

    /**
     * {@inheritDoc}
     */
    public CharIterator createIterator(){
        return new CharSequenceIterator(0,-1);
    }

    /**
     * {@inheritDoc}
     */
    public CharIterator createIterator(int fromIndex, int toIndex){
        return new CharSequenceIterator(fromIndex,toIndex);
    }

    /**
     * {@inheritDoc}
     */
    public Chars toChars() {
        return new Chars(getBytesInternal(), getEncoding());
    }

    /**
     * {@inheritDoc}
     */
    public String toJVMString(){
        final byte[] data = getBytesInternal();
        if(getEncoding() == null){
            return new String(data);
        }
        try {
            return new String(data,getEncoding().getName().toString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj) {
        return equals(obj, false, true,null);
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding) {
        return equals(obj, ignoreCase, ignoreEncoding, null);
    }

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding, CharCase language) {
        
        if (!CharArray.class.isInstance(obj)){
            return false;
        }
        CharArray other = (CharArray) obj;
        
        //quick tests to avoid characters iteration
        if(!ignoreEncoding){
            if(!this.getEncoding().equals(other.getEncoding())){
                return false;
            }            
            if(!ignoreCase){
                //compare bytes directly
                if(other instanceof AbstractCharArray){
                    return Arrays.equals(this.getBytesInternal(), ((AbstractCharArray)other).getBytesInternal());
                }else{
                    return Arrays.equals(this.getBytesInternal(), other.toBytes());
                }
            }
        }

        final CharIterator ite1 = this.createIterator();
        final CharIterator ite2 = other.createIterator();

        if(ignoreCase){
            if(language == null){
                language = Languages.UNSET;
            }

            while(true){
                final boolean n1 = ite1.hasNext();
                final boolean n2 = ite2.hasNext();
                if(!n1 && !n2){
                    //both have finish
                    return true;
                }else if(n1 && n2){
                    final int c1 = ite1.nextToUnicode();
                    final int c2 = ite2.nextToUnicode();
                    if(c1==c2) continue;
                    if(language.toLowerCase(c1) == c2) continue;
                    if(language.toUpperCase(c1) == c2) continue;
                    if(language.toLowerCase(c2) == c1) continue;
                    if(language.toUpperCase(c2) == c1) continue;
                    return false;
                }else{
                    //chars do not have the same length
                    return false;
                }
            }

        }else{

            while(true){
                final boolean n1 = ite1.hasNext();
                final boolean n2 = ite2.hasNext();
                if(!n1 && !n2){
                    //both have finish
                    return true;
                }else if(n1 && n2){
                    final int c1 = ite1.nextToUnicode();
                    final int c2 = ite2.nextToUnicode();
                    if(c1==c2) continue;
                    return false;
                }else{
                    //chars do not have the same length
                    return false;
                }
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    public int getHash() {
        if(hash!=Integer.MAX_VALUE) return hash;
        //we use an hashcode always in the same encoding since encoding is not 
        //part of the default equals method
        byte[] data = getBytesInternal();
        final CharEncoding encoding = getEncoding();
        if(encoding!=CharEncodings.UTF_32BE){
            //compute character length in the same time
            final CharConverter converter = Characters.getConverter(encoding, CharEncodings.UTF_32BE);
            final ByteSequence buffer = new ByteSequence();

            final int[] size = new int[2];
            final byte[] out = new byte[4];
            int offset = 0;
            int length = 0;
            while(offset<data.length){
                converter.convert(data, offset, out, 0, size);
                offset += size[0];
                buffer.put(out, 0, size[1]);
                length++;
            }
            data = buffer.toArrayByte();
            this.length = length;
        }
        hash = Arrays.computeHash(data);
        return hash;
    }

    private byte[] toThisEncodingBytes(CharArray ch){
        byte[] cdata = ch.toBytes();
        final CharEncoding thisEnc = getEncoding();
        final CharEncoding otherEnc = ch.getEncoding();
        if(thisEnc != otherEnc){
            //different encoding,recode char array
            cdata = Characters.recode(cdata,otherEnc,thisEnc);
        }
        return cdata;
    }

    /**
     * {@inheritDoc}
     */
    public int toByteOffset(int charIndex){
        if(charIndex==0) return 0;
        int offset = 0;
        final CharEncoding encoding = getEncoding();
        final byte[] data = getBytesInternal();
        for(int i=0;i<charIndex;i++){
            offset += encoding.charlength(data, offset);
        }
        return offset;
    }

    /**
     * {@inheritDoc}
     */
    public CharArray[] split(int unicode){
        final Sequence parts = new ArraySequence();
        
        final byte[] data = getBytesInternal();
        final CharEncoding enc = getEncoding();
        final byte[] cd = enc.toBytes(unicode);
        int last = 0;
        int offset = 0;
        for(; offset<data.length;){
            int charlength = enc.charlength(data,offset);
            if(Arrays.equals(cd,0,cd.length,data,offset)){
                //found one
                parts.add(new Chars(Arrays.copy(data, last, offset-last), enc));
                last = offset+charlength;
            }
            offset += charlength;
        }

        //last
        parts.add(new Chars(Arrays.copy(data, last, data.length-last), enc));

        final CharArray[] cs = new CharArray[parts.getSize()];
        Collections.copy(parts, cs, 0);
        return cs;
    }

    /**
     * {@inheritDoc}
     */
    public int order(Object other) {
        final AbstractCharArray co = (AbstractCharArray) other;

        final CharIterator ite = this.createIterator();
        final CharIterator oite = co.createIterator();

        while(ite.hasNext()){
            if(oite.hasNext()){
                final int uni = ite.nextToUnicode();
                final int ouni = oite.nextToUnicode();
                if(uni<ouni){
                    return -1;
                }else if(uni>ouni){
                    return +1;
                }
                //continue to next char
            }else{
                //other chars is shorter.
                return +1;
            }
        }

        if(oite.hasNext()){
            //other char is longer
            return -1;
        }else{
            //same length and same chars
            return 0;
        }
    }

    private final class CharSequenceIterator extends AbstractCharIterator{

        private final int[] buffer = new int[2];
        private int byteoffset;
        private final int toIndex;
        private int inc = 0;
        private final byte[] data;

        public CharSequenceIterator(int fromIndex, int toIndex) {
            super(AbstractCharArray.this.getEncoding());
            this.data = getBytesInternal();
            this.byteoffset = toByteOffset(fromIndex);
            this.toIndex = toIndex;
            this.inc = fromIndex;
        }

        public boolean hasNext() {
            if(toIndex>0 && inc>=toIndex){
                return false;
            }
            return byteoffset < data.length;
        }

        public byte[] nextToBytes() {
            inc++;
            final int charsize = encoding.charlength(data,byteoffset);
            final byte[] cd = Arrays.copy(data,byteoffset,charsize,new byte[charsize],0);
            byteoffset+=charsize;
            return cd;
        }

        public void nextToBuffer(ByteSequence buffer) {
            inc++;
            final int charsize = encoding.charlength(data,byteoffset);
            buffer.put(data,byteoffset,charsize);
            byteoffset+=charsize;
        }

        public int nextToUnicode() {
            inc++;
            encoding.toUnicode(data, buffer, byteoffset);
            byteoffset+=buffer[1];
            return buffer[0];
        }

        public int[] nextBulk(ByteSequence bytebuffer) {
            encoding.toUnicode(data, buffer, byteoffset);
            bytebuffer.put(data,byteoffset,buffer[1]);
            inc++;
            byteoffset+=buffer[1];
            return buffer;
        }
        
        public void skip(){
            inc++;
            byteoffset += encoding.charlength(data,byteoffset);
        }

        public byte[] peekToBytes() {
            final int charsize = encoding.charlength(data,byteoffset);
            final byte[] cd = Arrays.copy(data,byteoffset,charsize,new byte[charsize],0);
            return cd;
        }

        public void peekToBuffer(ByteSequence buffer) {
            final int charsize = encoding.charlength(data,byteoffset);
            buffer.put(data,byteoffset,charsize);
        }

        public int peekToUnicode() {
            encoding.toUnicode(data, buffer, byteoffset);
            return buffer[0];
        }

        public int[] peekBulk(ByteSequence bytebuffer) {
            encoding.toUnicode(data, buffer, byteoffset);
            bytebuffer.put(data,byteoffset,buffer[1]);
            return buffer;
        }
        
        public boolean nextEquals(byte[] cdata) {
            return Arrays.equals(cdata,0,cdata.length,data,byteoffset);
        }

    }
    
}
