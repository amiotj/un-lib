
package science.unlicense.api;

/**
 * Sort objects.
 *
 * @author Johann Sorel
 */
public interface Sorter {

    /**
     * @param first first object to compare
     * @param second second object to compare
     * @return negative,zero or positive value if first object is
     *         before , same or after the given object.
     */
    int sort(Object first, Object second);

}
