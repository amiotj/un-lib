
package science.unlicense.api.regex;

import science.unlicense.api.character.BacktrackCharIterator;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;

/**
 * Regular expression executor.
 * 
 * Can be used two ways :
 * match(text)
 * Or in iterative calls with :
 * reset()
 * process(cp)
 * 
 * This class is not concurrent.
 * 
 * @author Johann Sorel
 */
public final class RegexExec extends NFAStateExec {
        
    public RegexExec(final NFAState state) {
        super(state);
        init();
    }

    /**
     * Check if given text match expression.
     * 
     * @param text to test
     * @return true if given string match regex
     */
    public boolean match(final Chars text){
        reset();
        
        final CharIterator ite = text.createIterator();
        while(ite.hasNext()){
            process(ite.nextToUnicode());
            if(buffer1.isEmpty()) return false;
        }
        
        return containsMatch();
    }
    
    /**
     * Split the text each time the pattern is found.
     *
     * TODO add argument to control empty split at begining and end
     *
     * @param text
     * @return splitted text
     */
    public Chars[] split(final Chars text){
        reset();
        
        final Sequence splits = new ArraySequence();        
        int startIdx=0;
        int endIdx=0;        
        int idx=0;
        int bestMatch = -1;
        
        final BacktrackCharIterator ite = new BacktrackCharIterator(text.createIterator());
        while(ite.hasNext()){
            idx++;
            process(ite.nextToUnicode());
            final boolean matches = containsMatch();
            
            if(bestMatch>=0 && !matches){
                if(startIdx!=endIdx){
                    splits.add(text.truncate(startIdx, endIdx));
                }
                startIdx = bestMatch;
                endIdx = bestMatch;
                idx = bestMatch;
                bestMatch = -1;
                ite.rewind();
                reset();
            }else if(matches){
                bestMatch = idx;
                ite.mark();
            }if(buffer1.isEmpty()){
                //not match
                endIdx = idx;
                reset();
            }else{
                //possible split
            }
        }
        
        if(startIdx!=endIdx){
            splits.add(text.truncate(startIdx, endIdx));
        }
        
        final Chars[] parts = new Chars[splits.getSize()];
        Collections.copy(splits, parts, 0);
        return parts;
    }

    /**
     * Replace all instances of matching elements in the char array by given text.
     *
     * @param text
     * @param replacement
     * @return
     */
    public Chars replace(Chars text, Chars replacement){
        reset();

        final CharBuffer cb = new CharBuffer(text.getEncoding());
        int startIdx=0;
        int endIdx=0;
        int idx=0;
        int bestMatch = -1;
        
        final BacktrackCharIterator ite = new BacktrackCharIterator(text.createIterator());
        while(ite.hasNext()){
            idx++;
            process(ite.nextToUnicode());
            final boolean matches = containsMatch();
            
            if(bestMatch>=0 && !matches){
                if(startIdx!=endIdx){
                    cb.append(text.truncate(startIdx, endIdx));
                }
                cb.append(replacement);
                startIdx = bestMatch;
                endIdx = bestMatch;
                idx = bestMatch;
                bestMatch = -1;
                ite.rewind();
                reset();
            }else if(matches){
                bestMatch = idx;
                ite.mark();
            }else if(buffer1.isEmpty()){
                //not match
                endIdx = idx;
                reset();
            }else{
                //possible match
            }
        }
        
        if(startIdx!=endIdx){
            cb.append(text.truncate(startIdx, endIdx));
        }
        
        if(bestMatch>=0){
            cb.append(replacement);
        }

        reset();
        return cb.toChars();
    }
    
    private boolean containsMatch(){
        for(int i=0,n=buffer1.getSize();i<n;i++){
            final NFAState state = (NFAState) buffer1.get(i);
            if(state instanceof NFAState.Match){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Process a new character.
     * @param cp unicode code point
     * @return Collection of current NFA states
     */
    public Sequence process(int cp){
        return process((Integer)cp);
    }
     
}
