
package science.unlicense.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface IntCursor extends Cursor {

    long getPosition();

    void setPosition(long position);

    ////////////////////////////////////////////////////////////////////////
    int read(long position);

    void read(int[] array, long position);

    void read(int[] array, int arrayOffset, int length, long position);

    IntCursor write(int value, long position);

    IntCursor write(int[] array, long position);

    IntCursor write(int[] array, int arrayOffset, int length, long position);

    ////////////////////////////////////////////////////////////////////////
    int read();

    void read(int[] array);

    void read(int[] array, int arrayOffset, int length);

    IntCursor write(int value);

    IntCursor write(int[] array);

    IntCursor write(int[] array, int arrayOffset, int length);
    
}
