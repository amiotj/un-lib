
package science.unlicense.api.collection;

import science.unlicense.api.Sorter;

/**
 *
 * @author Johann Sorel
 */
class SortCollection extends AbstractCollection {

    private final Collection base;
    private final Sorter eva;

    public SortCollection(Collection base, Sorter eva) {
        this.base = base;
        this.eva = eva;
    }

    @Override
    public int getSize() {
        return base.getSize();
    }

    @Override
    public Iterator createIterator() {
        final Sequence seq = new ArraySequence(base);
        Collections.sort(seq, eva);
        return seq.createIterator();
    }

}
