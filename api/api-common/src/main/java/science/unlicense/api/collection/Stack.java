

package science.unlicense.api.collection;

/**
 * @author Johann Sorel
 */
public interface Stack extends Collection {
    
    Object lookStart();
    
    Object lookEnd();
    
    Object pickStart();
    
    Object pickEnd();
    
}
