package science.unlicense.api.model.tree;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTypedNode extends DefaultNamedNode implements TypedNode{

    private final NodeCardinality card;
    private final NodeType type;

    public DefaultTypedNode(NodeCardinality card) {
        this(card,null,null,null);
    }

    public DefaultTypedNode(NodeCardinality card, Object value) {
        this(card,null,value,null);
    }

    public DefaultTypedNode(NodeCardinality card, Node[] children) {
        this(card,null,null,children);
    }

    public DefaultTypedNode(NodeType type) {
        this(null,type,null,null);
    }

    public DefaultTypedNode(NodeType type, Object value) {
        this(null,type,value,null);
    }

    public DefaultTypedNode(NodeType type, Node[] children) {
        this(null,type,null,children);
    }

    protected DefaultTypedNode(NodeCardinality card, NodeType type, Object value, Node[] children) {
        super(true);
        this.card = card;
        this.type = (card==null)?type:card.getType();
        this.value = value;
        if(children != null){
            for(int i=0;i<children.length;i++){
                this.children.add(children[i]);
            }
        }
    }

    public NodeCardinality getCardinality() {
        return card;
    }

    public NodeType getType() {
        return type;
    }

    public TypedNode addChild(NodeCardinality type, Object value) {
        final TypedNode sn = create(type);
        sn.setValue(value);
        children.add(sn);
        return sn;
    }

    public TypedNode getChild(NodeCardinality type) {
        return getChild(type.getId());
    }

    public TypedNode getChild(Chars name) {
        return getOrCreateChild(name,false);
    }

    public TypedNode getOrCreateChild(NodeCardinality type){
        return getOrCreateChild(type.getId());
    }

    public TypedNode getOrCreateChild(Chars name){
        return getOrCreateChild(name,true);
    }

    protected TypedNode getOrCreateChild(Chars name, boolean create){
        for(int i=0,in=children.getSize();i<in;i++){
            final Node n = (Node) children.get(i);
            if(n instanceof TypedNode){
                final TypedNode sn = (TypedNode) n;
                final NodeCardinality card = sn.getCardinality();
                if(card!=null){
                    if(name.equals(card.getId())){
                        return sn;
                    }
                }else if(name.equals(sn.getType().getId())){
                    return sn;
                }
            }
        }
        if(create){
            final NodeCardinality nc = type.getChild(name);
            final TypedNode node = create(nc);
            children.add(node);
            return node;
        }
        return null;
    }

    protected TypedNode create(NodeCardinality nc){
        final DefaultTypedNode tn =  new DefaultTypedNode(nc);
        tn.setValue(nc.getType().getDefaultValue());
        return tn;
    }

    public TypedNode[] getChildren(NodeCardinality type) {
        return getChildren(type.getId());
    }

    public TypedNode[] getChildren(Chars name) {
        final Sequence nodes = new ArraySequence();
        for(int i=0,in=children.getSize();i<in;i++){
            final Node n = (Node) children.get(i);
            if(n instanceof TypedNode){
                final TypedNode sn = (TypedNode) n;
                final NodeCardinality card = sn.getCardinality();
                if(card!=null){
                    if(name.equals(card.getId())){
                        nodes.add(n);
                    }
                }else if(name.equals(sn.getType().getId())){
                    nodes.add(n);
                }
            }
        }

        TypedNode[] array = new TypedNode[nodes.getSize()];
        for(int i=0;i<array.length;i++){
            array[i] = (TypedNode) nodes.get(i);
        }
        return array;
    }

    public Chars thisToChars() {
        final Chars name = (card==null) ? getType().getId() : getCardinality().getId();
        final Object val = getValue();
        if(val==null){
            return name;
        }else{
            return name.concat(':').concat(CObjects.toChars(val,50));
        }
    }

}
