
package science.unlicense.api.logging;

/**
 * Logging factory used to create loggers.
 * 
 * @author Johann Sorel
 */
public interface LoggerFactory {
    
    /**
     * Create a new logger instance.
     * @return Logger, never null.
     */
    Logger create();
    
}
