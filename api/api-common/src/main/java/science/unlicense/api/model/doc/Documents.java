
package science.unlicense.api.model.doc;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public class Documents {
 
    /**
     * Override fields values of base document by 'over' document.
     * 
     * @param base
     * @param over 
     */
    public static void overrideFields(Document base, Document over) {
        final Iterator ite = over.getFieldNames().createIterator();
        while(ite.hasNext()){
            final Chars name = (Chars) ite.next();
            base.setFieldValue(name, over.getFieldValue(name));
        }
    }
    
}
