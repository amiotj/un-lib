
package science.unlicense.api.model;

/**
 *
 * @author Johann Sorel
 */
public interface MultiplicityType extends Presentable {
    
    int getMinOccurences();
    
    int getMaxOccurences();
    
}
