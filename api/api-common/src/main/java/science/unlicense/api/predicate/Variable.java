
package science.unlicense.api.predicate;

import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;

/**
 * A variable is a value which may change overtime for any reason.
 * Variables produces events when they change.
 * 
 * @author Johann Sorel
 */
public interface Variable extends EventSource {
    
    /**
     * The main object that contains this variable.
     * Variables may be short life objects, when holding weak or soft reference
     * it is then important to keep a reference to the real object.
     * 
     * @return main object or itself if variable is self contained.
     */
    EventSource getHolder();
    
    /**
     * Indicate if getValue method is usable.
     * 
     * @return true if variable is readable
     */
    boolean isReadable();
    
    /**
     * Indicate if setValue method is usable.
     * 
     * @return true if variable is writable
     */
    boolean isWritable();
    
    /**
     * Synchronize the value of this variable with given one.
     * This synchronization in both in reading and writing.
     * 
     * @param var not null , value type must match
     */
    void sync(Variable var) throws VariableSyncException;
    
    /**
     * Synchronize the value of this variable with given one.
     * 
     * @param var not null , value type must match
     * @param readOnly true to synchronize reading only.
     */
    void sync(Variable var, boolean readOnly) throws VariableSyncException;

    /**
     * Remove any synchronization in place with this variable.
     * The method has no effect if there is no synchronization.
     */
    void unsync();

    /**
     * Get variable value.
     * 
     * @return value variable value
     * @throws RuntimeException if value is not readable or an error occured
     */
    Object getValue() throws RuntimeException;
    
    /**
     * Set variable value.
     * 
     * @param value new variable value
     * @throws RuntimeException if value is not writable or an error occured
     */
    void setValue(Object value) throws RuntimeException;
        
    /**
     * Add a listener which will receive VariableEvent when value changes.
     * 
     * @param listener 
     */
    void addListener(EventListener listener);
    
    /**
     * Remove variable listener.
     * 
     * @param listener 
     */
    void removeListener(EventListener listener);
    
}
