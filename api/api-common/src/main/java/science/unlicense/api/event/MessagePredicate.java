
package science.unlicense.api.event;

import science.unlicense.api.predicate.AbstractPredicate;

/**
 * Message predicate, tests the class of event message property.
 *
 * @author Johann Sorel
 */
public class MessagePredicate extends AbstractPredicate {

    private final Class clazz;

    public MessagePredicate(Class clazz) {
        this.clazz = clazz;
    }

    public Boolean evaluate(Object candidate) {
        return candidate instanceof Event && clazz.isInstance(((Event)candidate).getMessage());
    }

    public int getHash() {
        int hash = 5;
        hash = 29 * hash + (this.clazz != null ? this.clazz.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MessagePredicate other = (MessagePredicate) obj;
        if (this.clazz != other.clazz && (this.clazz == null || !this.clazz.equals(other.clazz))) {
            return false;
        }
        return true;
    }

}

