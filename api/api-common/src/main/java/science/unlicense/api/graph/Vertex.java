
package science.unlicense.api.graph;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;

/**
 * A Vertex (also called Node) is a junction in a graph where several edge
 * start or end.
 *
 * @author Johann Sorel
 */
public interface Vertex {

    /**
     * Get the graph which contains this vertex.
     * @return Graph
     */
    Graph getGraph();

    /**
     * Get all edges attached to this vertex.
     * @return Sequence
     */
    Sequence getEdges();

    /**
     * Get Properties attached to this vertex.
     * @return Dictionary
     */
    Dictionary getProperties();

    /**
     * Two vertices are adjacent when an edge links them.
     * @param vertex vertex to test
     * @return true if current vertex is adjacent with given vertex.
     */
    boolean isAdjacent(Vertex vertex);

    /**
     * The degree of a vertex is the number of edges ends which match this vertex.
     * @return int degree
     */
    int getDegree();

    /**
     * The degree of a vertex is the number of edges which start by this vertex.
     * @return int degree
     */
    int getOutDegree();

    /**
     * The degree of a vertex is the number of edges which end by this vertex.
     * @return int degree
     */
    int getInDegree();

    /**
     * A vertex is isolated when it's degree is zero.
     * @return true if vertex is isolated.
     */
    boolean isIsolated();

    /**
     * A vertex is pendant when it's degree is one.
     * @return true if vertex is pendant.
     */
    boolean isPendant();
}
