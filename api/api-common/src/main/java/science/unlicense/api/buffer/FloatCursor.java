
package science.unlicense.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface FloatCursor extends Cursor {

    long getPosition();

    void setPosition(long position);

    ////////////////////////////////////////////////////////////////////////
    float read(long position);

    void read(float[] array, long position);

    void read(float[] array, int arrayOffset, int length, long position);

    FloatCursor write(float value, long position);

    FloatCursor write(float[] array, long position);

    FloatCursor write(float[] array, int arrayOffset, int length, long position);

    ////////////////////////////////////////////////////////////////////////
    float read();

    void read(float[] array);

    void read(float[] array, int arrayOffset, int length);

    FloatCursor write(float value);

    FloatCursor write(float[] array);

    FloatCursor write(float[] array, int arrayOffset, int length);
    
}
