
package science.unlicense.api.model.doc;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Set;

/**
 * The document model is common way to structure object by fields.
 * A document is similar to a dictionary but provide a type which in turn
 * defines the possible fields and their types.
 *
 * @author Johann Sorel
 */
public interface Document {

    /**
     * Get document type.
     *
     * @return Document type, not null
     */
    DocumentType getType();

    /**
     * Get all field names.
     * If the document type is strict then the field names matches the document
     * type fields. If not this collection may contain additional names.
     *
     * @return
     */
    Set getFieldNames();

    /**
     * Get field object.
     * 
     * @param name field name
     * @return Field, never null
     * @throws FieldNotFoundException if field does not exist and document type is strict
     */
    Field getField(Chars name) throws FieldNotFoundException;

    /**
     * Get field value.
     * If field type exist and max occurences > 1, value is a collection.
     *
     * @param name field name, not null
     * @return field value, can be null
     */
    Object getFieldValue(Chars name);

    /**
     * Set field value.
     *
     * @param name field name, not null
     * @param value
     * @throws FieldNotFoundException if field does not exist and document type is strict
     */
    void setFieldValue(Chars name, Object value) throws FieldNotFoundException;
            
}
