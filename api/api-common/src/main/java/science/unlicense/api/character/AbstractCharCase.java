
package science.unlicense.api.character;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractCharCase implements CharCase{

    public boolean isUpperCase(Char c) {
        return isUpperCase(c.toUnicode());
    }

    public boolean isUpperCase(CharArray array) {
        final CharIterator ite = array.createIterator();
        while(ite.hasNext()){
            if(!isUpperCase(ite.nextToUnicode())){
                return false;
            }
        }
        return true;
    }

    public boolean isLowerCase(Char c) {
        return isLowerCase(c.toUnicode());
    }

    public boolean isLowerCase(CharArray array) {
        final CharIterator ite = array.createIterator();
        while(ite.hasNext()){
            if(!isLowerCase(ite.nextToUnicode())){
                return false;
            }
        }
        return true;
    }
    
    public Char toUpperCase(Char c) {
        final int cp = c.toUnicode();
        final int ucp = toUpperCase(cp);
        if(cp == ucp){
            return c;
        }else{
            return new Char(ucp,c.getEncoding());
        }
    }

    public Chars toUpperCase(CharArray sequence) {
        final int[] cps = new int[sequence.getCharLength()];
        final CharIterator ite = sequence.createIterator();
        for(int i=0;ite.hasNext();i++){
            cps[i] = toUpperCase(ite.nextToUnicode());
        }
        return new Chars(cps, sequence.getEncoding());
    }

    public Char toLowerCase(Char c) {
        final int cp = c.toUnicode();
        final int lcp = toLowerCase(cp);
        if(cp == lcp){
            return c;
        }else{
            return new Char(lcp,c.getEncoding());
        }
    }

    public Chars toLowerCase(CharArray sequence) {
        final int[] cps = new int[sequence.getCharLength()];
        final CharIterator ite = sequence.createIterator();
        for(int i=0;ite.hasNext();i++){
            cps[i] = toLowerCase(ite.nextToUnicode());
        }
        return new Chars(cps, sequence.getEncoding());
    }

}
