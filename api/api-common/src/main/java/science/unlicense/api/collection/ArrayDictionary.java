
package science.unlicense.api.collection;

import science.unlicense.api.CObjects;

/**
 *
 * @author Johann Sorel
 */
public class ArrayDictionary extends AbstractDictionary{
    
    /**
     * Contain APair values. first is the key, second is the value.
     */
    private final Sequence values = new ArraySequence();

    public int getSize() {
        return values.getSize();
    }

    public Set getKeys() {
        final Set keys = new HashSet();
        final Iterator ite = values.createIterator();
        while(ite.hasNext()){
            final APair vals = (APair) ite.next();
            keys.add(vals.value1);
        }
        return keys;
    }

    public Object getValue(Object key) {
        final Iterator ite = values.createIterator();
        while(ite.hasNext()){
            final APair vals = (APair) ite.next();
            if(vals!=null && CObjects.equals(vals.value1,key)){
                return vals.value2;
            }
        }
        return null;
    }

    public Collection getValues() {
        final Sequence candidates = new ArraySequence();
        final Iterator ite = values.createIterator();
        while(ite.hasNext()){
            final APair vals = (APair) ite.next();
            candidates.add(vals.value2);
        }
        return candidates;
    }

    public Collection getPairs() {
        return new ArraySequence(values);
    }

    public void add(Object key, Object value) {
        final Iterator ite = values.createIterator();
        while(ite.hasNext()){
            final APair pair = (APair) ite.next();
            if(pair!=null && CObjects.equals(pair.value1,key)){
                pair.value2 = value;
                if(hasListeners()) fireReplace(0, 0, null,new Object[]{pair});
                return;
            }
        }

        //key not found add it
        final Pair pair = new APair(key,value);
        values.add(pair);
        if(hasListeners()) fireAdd(0, 0, new Object[]{pair});
    }

    public void addAll(Dictionary index) {
        final Collection col = index.getPairs();
        final Iterator ite = col.createIterator();
        while(ite.hasNext()){
            final Pair p = (Pair) ite.next();
            add(p.getValue1(), p.getValue2());
        }
        ite.close();
    }

    public Object remove(Object key) {
        for(int i=0,n=values.getSize();i<n;i++){
            final APair pair = (APair) this.values.get(i);
            if(pair!=null && CObjects.equals(pair.value1,key)){
                values.remove(i);
                if(hasListeners()) fireRemove(0, 0, new Object[]{pair});
                return pair.value2;
            }
        }
        return null;
    }

    public void removeAll() {
        values.removeAll();
        if(hasListeners()) fireRemove(-1, -1, null);
    }
    
    private static class APair extends Pair{

        public APair(Object value1, Object value2) {
            super(value1, value2);
        }
        
    }
    
}
