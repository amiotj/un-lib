

package science.unlicense.api.collection;

/**
 * Set using hash values for faster acces.
 *
 * @author Johann Sorel
 */
public class HashSet extends AbstractCollection implements Set{

    private static final int DEFAULT_SIZE = 11;
    
    private static final float LOAD_MAX = 1.75f;
    private static final float LOAD_MIN = 0.3f;
    private static final int RESIZE_UP = 3;
    private static final float RESIZE_DOWN = 0.5f;

    private final Hasher hasher;
    private HPair[] table;
    private int size = 0;

    public HashSet() {
        this(Hasher.DEFAULT,DEFAULT_SIZE);
    }

    public HashSet(Hasher hasher) {
        this(hasher,DEFAULT_SIZE);
    }
    
    public HashSet(Hasher hasher, int size){
        this.table = new HPair[Math.max(size, DEFAULT_SIZE)];
        this.hasher = hasher;
    }
    
    public int getSize() {
        return size;
    }

    public boolean contains(Object key){
        final int keyhash = hasher.getHash(key);
        HPair pair = table[offset(keyhash)];
        while(pair!=null){
            if(pair.hash==keyhash && hasher.equals(pair.key, key)){
                return true;
            }
            pair = pair.next;
        }
        return false;
    }
    
    public boolean add(Object key){
        final int keyhash = hasher.getHash(key);
        final int offset = offset(keyhash);        
        HPair pair = table[offset(keyhash)];
        
        //search if the key is already here
        while (pair!=null) {
            if (pair.hash == keyhash && hasher.equals(pair.key, key)) {
                if (pair.key != key) {
                    pair.key = key; //change the key too, it might not be the same object
                    if (hasListeners()) {
                        fireAdd(-1, -1, new Object[]{key});
                    }
                    return true;
                } else {
                    return false;
                }
            }
            pair = pair.next;
        }
        
        //add a new value
        table[offset] = new HPair(keyhash, key, table[offset]);
        size++;
        if(loadRatio()>=LOAD_MAX){
            resize(table.length * RESIZE_UP);
        }
        
        if(hasListeners()){
            fireAdd(-1, -1, new Object[]{key});
        }
        
        return true;
    }

    public boolean remove(Object key){
        final int keyhash = hasher.getHash(key);
        final int offset = offset(keyhash);
        HPair pair = table[offset];
        HPair prev = null;

        boolean found = false;
        while(pair != null){
            if(pair.hash==keyhash && hasher.equals(pair.key,key)){
                if(prev!=null){
                    prev.next = pair.next;
                }else{
                    table[offset] = table[offset].next;
                }
                size--;
                found = true;
                break;
            }
            prev = pair;
            pair = pair.next;
        }
        
        if(loadRatio()<=LOAD_MIN){
            resize((int) Math.ceil(table.length * RESIZE_DOWN));
        }
        
        if(found && hasListeners()){
            fireRemove(-1, -1, new Object[]{key});
        }
        
        return found;
    }
    
    @Override
    public boolean removeAll() {
        if(size==0) return true;
        final boolean hasListener = hasListeners();
        Object[] removed = null;
        if(hasListener){
            removed = toArray();
        }
        
        table = new HPair[DEFAULT_SIZE];
        size = 0;
        
        if(hasListener){
            fireRemove(-1, -1, removed);
        }
        
        return true;
    }
    
    final void resize(int size){
        HPair tp;
        
        final HPair[] temp = table;
        table = new HPair[size];
        for(int i=0;i<temp.length;i++){
            HPair pair = temp[i];
            while(pair!=null){
                tp = pair.next;
                addRecycle(pair);
                pair = tp;
            }
        }
    }
    
    private void addRecycle(HPair old){
        final int offset = offset(old.hash);        
        old.next = table[offset];
        table[offset] = old;
    }
    
    private final int offset(int keyHash){
        return Math.abs(keyHash % table.length);
    }
    
    final float loadRatio(){
        return ((float)size/(float)table.length);
    }

    public Iterator createIterator() {
        return new AbstractIterator() {
            int currentIndex = 0;
            HPair currentPair;

            protected void findNext() {
                if(currentPair!=null){
                    currentPair = currentPair.next;
                }
                while(currentPair==null && currentIndex<table.length){
                    currentPair = table[currentIndex];
                    currentIndex++;
                }
                nextValue = (currentPair==null) ? null : currentPair.key;
            }
        };
    }

    private static final class HPair {
        final int hash;
        Object key;
        HPair next;
        
        HPair(int hash, Object key, HPair next) {
            this.key = key;
            this.hash = hash;
            this.next = next;
        }
    }
    
}
