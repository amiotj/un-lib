
package science.unlicense.api.predicate;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;

/**
 * Logic AND of several predicates.
 * 
 * @author Johann Sorel
 */
public class And extends CObject implements Predicate, Function{

    public static final Chars NAME = new Chars("AND");
    private final Predicate[] predicates;

    /**
     * Predicates to concatenate.
     * 
     * @param predicates not null
     */
    public And(Predicate[] predicates) {
        CObjects.ensureNotNull(predicates);
        this.predicates = predicates;
    }

    public Chars getName() {
        return NAME;
    }
    
    /**
     * And predicates.
     * Returned array is a copy.
     * 
     * @return Predicate[] not null
     */
    public Predicate[] getPrecidates() {
        return Arrays.copy(predicates, new Predicate[predicates.length]);
    }
    
    public Expression[] getParameters() {
        return getPrecidates();
    }
    
    public Boolean evaluate(Object candidate) {
        for(int i=0;i<predicates.length;i++){
            if(!predicates[i].evaluate(candidate)){
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final And other = (And) obj;
        return Arrays.equals(this.predicates, other.predicates);
    }

    public int getHI() {
        return 79 * 5 + Arrays.computeHash(this.predicates);
    }

}
