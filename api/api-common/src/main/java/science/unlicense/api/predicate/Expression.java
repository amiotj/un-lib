
package science.unlicense.api.predicate;

/**
 * An expression is a dynamic value which evaluation result change
 * based on the evaluated object and context.
 * 
 * @author Johann Sorel
 */
public interface Expression {
    
    /**
     * Evaluation given object in context.
     * 
     * @param candidate, object to evaluate
     * @param context the evaluation context, can be null
     * @return result of the evaluation
     */
    Object evaluate(Object candidate);
    
}
