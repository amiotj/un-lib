package science.unlicense.api.graph.operation;

import science.unlicense.api.Orderable;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.ArrayOrderedSet;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedSet;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.graph.Edge;
import science.unlicense.api.graph.Vertex;
import science.unlicense.api.predicate.Expression;

/**
 * Dijkstra algorithm to find the shortest path in a graph.
 * https://en.wikipedia.org/wiki/Dijkstra's_algorithm
 *
 * @author Johann Sorel
 */
public class Dijkstra {

    private static class VertexInfo implements Orderable{
        Vertex vertex;
        double distance;
        boolean visited;
        VertexInfo previous;

        public VertexInfo(Vertex vertex) {
            this.vertex = vertex;
            this.distance = Double.POSITIVE_INFINITY;
        }

        public int order(Object other) {
            return Double.compare(distance, ((VertexInfo)other).distance);
        }
    }

    private Dictionary stack = null;

    /**
     * Build the distance graph from given source node.
     * 
     * @param source origin of the dijkstra graph
     * @param distanceExp expression used to evaluate the distance for each edge
     */
    public void buildGraph(Vertex source, Expression distanceExp){
        buildGraph(source, null, distanceExp);
    }

    /**
     * Can only be used after a call to buildGraph.
     *
     * @param target vertex aimed
     * @return sequence of vertex from source to target.
     */
    public Sequence getShortestPath(Vertex target){
        Sequence s = new ArraySequence();
        VertexInfo vi = (VertexInfo) stack.getValue(target);
        while(vi != null){
            s.add(vi.vertex);
            vi = vi.previous;
        }
        s = Collections.reverse(s);
        return s;
    }

    /**
     * Build the distance graph from given source to target node.
     * If target is set the graph build will be stopped when target is found.
     *
     * @param source origin of the dijkstra graph
     * @param target vertex aimed, can be null
     * @param distanceExp expression used to evaluate the distance for each edge
     */
    private void buildGraph(final Vertex source, final Vertex target, final Expression distanceExp){
        stack = new HashDictionary(){
            public Object getValue(Object key) {
                VertexInfo vi = (VertexInfo) super.getValue(key);
                if(vi == null){
                    vi = new VertexInfo((Vertex)key);
                    add(key, vi);
                }
                return vi;
            }
        };

        final OrderedSet q = new ArrayOrderedSet();
        final VertexInfo si = (VertexInfo) stack.getValue(source);
        si.distance = 0;
        q.add(si);

        while (!q.isEmpty()) {
            final VertexInfo u = (VertexInfo)q.createIterator().next();
            q.remove(u);
            if(u.vertex.equals(target)){
                //we found the target, stop building the graph
                break;
            }
            u.visited = true;

            final Iterator edges = u.vertex.getEdges().createIterator();
            while(edges.hasNext()){
                final Edge edge = (Edge) edges.next();
                final Vertex v = edge.getVertex1().equals(u.vertex) ? edge.getVertex2() : edge.getVertex1();
                final VertexInfo vi = (VertexInfo) stack.getValue(v);
                final double vdist = (Double)distanceExp.evaluate(edge);
                double alt = u.distance + vdist;

                if(!vi.visited && alt < vi.distance){
                    vi.distance = alt;
                    vi.previous = u;
                    q.add(vi);
                }
            }
        }

    }

    /**
     * Get the shortest path from source to target.
     * 
     * @param source origin of the dijkstra graph
     * @param target vertex aimed, can be null
     * @param distanceExp expression used to evaluate the distance for each edge
     * @return sequence of vertex from source to target.
     */
    public static Sequence getShortestPath(Vertex source, Vertex target, Expression distanceExp){
        final Dijkstra dijkstra = new Dijkstra();
        dijkstra.buildGraph(source, target, distanceExp);
        return dijkstra.getShortestPath(target);
    }

}
