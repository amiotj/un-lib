

package science.unlicense.api.collection;

import science.unlicense.api.exception.MishandleException;
import science.unlicense.api.exception.UnmodifiableException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractIterator implements Iterator {

    protected Object nextValue = null;
    
    public boolean hasNext() {
        if(nextValue==null) findNext();
        return nextValue!=null;
    }
    
    public Object next() {
        if(nextValue==null) findNext();
        if(nextValue==null){
            throw new MishandleException("No more elements");
        }
        Object temp = nextValue;
        nextValue = null;
        return temp;
    }

    public boolean remove() {
        throw new UnmodifiableException("Not supported.");
    }

    public void close() {
    }
    
    protected abstract void findNext();
    
}
