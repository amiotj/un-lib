
package science.unlicense.api.predicate;

import science.unlicense.api.character.Chars;

/**
 * Functions resolvers provides a mecanism to enrich the collection
 * of available functions.
 * 
 * @author Johann Sorel
 */
public interface FunctionResolver {
    
    /**
     * Recreate function for given name and parameters.
     * May return null if name is unknwoned.
     * 
     * @param name function name
     * @param parameters function parameters
     * @return Function or null if it could not be resolved 
     */
    Function resolve(Chars name, Expression[] parameters); 
    
}
