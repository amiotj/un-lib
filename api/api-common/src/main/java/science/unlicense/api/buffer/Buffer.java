
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;

/**
 * Buffers are a way to store large number of primitive datas.
 * You should see a Buffer as a memory space of bytes which you can
 * explore, read and write freely.
 * Buffers can be more efficient for a give primitive type or may be more
 * optimized for reading or writing.
 * The most efficient primitive type is given by method : getPrimitiveType
 * 
 * 
 * @author Johann Sorel
 */
public interface Buffer {

    /** Indicate to read bit starting from the left most bit */
    public static final int LEFT_TO_RIGHT = 0;
    /** Indicate to read bit starting from the right most bit */
    public static final int RIGHT_TO_LEFT = 1;
    
    /**
     * Get the buffer factory which created this buffer.
     * 
     * @return BufferFactory, can be null
     */
    BufferFactory getFactory();
    
    /**
     * Get default primitive type.
     * The buffer should be optimized for this type.
     * 
     * @return one of science.unlicense.api.number.Primitive.*
     */
    int getPrimitiveType();
    
    /**
     * Get default encoding.
     * The buffer should be optimized for this encoding.
     * 
     * @return primitive encoding
     */
    NumberEncoding getEncoding();
    
    /**
     * Get the number of value stored in the buffer.
     * 
     * @return number of values in buffer
     */
    long getPrimitiveCount();
    
    /**
     * 
     * @return true if writing is supported
     */
    boolean isWritable();

    /**
     * Obtain a copy of this buffer.
     * 
     * @return Buffer
     */
    Buffer copy();

    /**
     * Obtain a copy of this buffer with a different primitive type and encoding.
     * The returned buffer is not linked to the parent buffer.
     * The new copy buffer should be optimized.
     * 
     * @param primitive
     * @param encoding
     * @return Buffer
     */
    Buffer copy(int primitive, NumberEncoding encoding);
    
    /**
     * Obtain a view of this buffer with the same primitive type and encoding.
     * This is a view, modifications made to one will change the parent buffer.
     * The view do not share the same cursor.
     * 
     * @return Cursor
     */
    DataCursor cursor();
    
    /**
     * Obtain a view of this buffer with a different primitive type and encoding.
     * This is a view, modifications made to one will change the parent buffer.
     * The view buffer may not be optimized.
     * The view do not share the same cursor.
     * 
     * @param primitive
     * @param encoding
     * @return Buffer
     */
    DataCursor cursor(int primitive, NumberEncoding encoding);
    
    IntCursor cursorInt();
    
    FloatCursor cursorFloat();
    
    DoubleCursor cursorDouble();
    
    /**
     * @return size of the buffer in bytes.
     */
    long getBufferByteSize();
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * Backend object of this store.
     * @return backend object, often an array, can be null
     */
    Object getBackEnd();
    
    /**
     * Convert the content of the buffer to a byte array.
     * 
     * @return byte array
     */
    byte[] toByteArray();
    
    /**
     * Convert the content of the buffer to an unsigned byte array.
     * 
     * @return unsigned byte array
     */
    int[] toUByteArray();
    
    /**
     * Convert the content of the buffer to a short array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return short array
     */
    short[] toShortArray();
    
    /**
     * Convert the content of the buffer to a unsigned short array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return unsigned short array
     */
    int[] toUShortArray();
    
    /**
     * Convert the content of the buffer to a 24bits int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return 24bits int array
     */
    int[] toInt24Array();
    
    /**
     * Convert the content of the buffer to a unsigned 24bits int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return unsigned 24bits int array
     */
    int[] toUInt24Array();
    
    /**
     * Convert the content of the buffer to a int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return int array
     */
    int[] toIntArray();
    
    /**
     * Convert the content of the buffer to a unsigned int array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return unsigned int array
     */
    long[] toUIntArray();
    
    /**
     * Convert the content of the buffer to a long array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return long array
     */
    long[] toLongArray();
    
    /**
     * Convert the content of the buffer to a float array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return float array
     */
    float[] toFloatArray();
    
    /**
     * Convert the content of the buffer to a double array.
     * If there are remaining bytes but not enough to make a primitive those
     * bytes will be ignore.
     * 
     * @return double array
     */
    double[] toDoubleArray();
    
    ////////////////////////////////////////////////////////////////////////////
    
    byte readByte(long offset);
    void readByte(byte[] array, long offset);
    void readByte(byte[] array, int arrayOffset, int length, long offset);
    
    int readUByte(long offset);
    void readUByte(int[] array, long offset);
    void readUByte(int[] array, int arrayOffset, int length, long offset);
        
    short readShort(long offset);
    void readShort(short[] array, long offset);
    void readShort(short[] array, int arrayOffset, int length, long offset);
        
    int readUShort(long offset);
    void readUShort(int[] array, long offset);
    void readUShort(int[] array, int arrayOffset, int length, long offset);
        
    int readInt24(long offset);
    void readInt24(int[] array, long offset);
    void readInt24(int[] array, int arrayOffset, int length, long offset);
    
    int readUInt24(long offset);
    void readUInt24(int[] array, long offset);
    void readUInt24(int[] array, int arrayOffset, int length, long offset);
      
    int readInt(long offset);
    void readInt(int[] array, long offset);
    void readInt(int[] array, int arrayOffset, int length, long offset);
        
    long readUInt(long offset);
    void readUInt(long[] array, long offset);
    void readUInt(long[] array, int arrayOffset, int length, long offset);
        
    long readLong(long offset);
    void readLong(long[] array, long offset);
    void readLong(long[] array, int arrayOffset, int length, long offset);
        
    float readFloat(long offset);
    void readFloat(float[] array, long offset);
    void readFloat(float[] array, int arrayOffset, int length, long offset);
        
    double readDouble(long offset);
    void readDouble(double[] array, long offset);
    void readDouble(double[] array, int arrayOffset, int length, long offset);
    
    ////////////////////////////////////////////////////////////////////////////
    
    void writeByte(byte value, long offset);
    void writeByte(byte[] array, long offset);
    void writeByte(byte[] array, int arrayOffset, int length, long offset);
    
    void writeUByte(int value, long offset);
    void writeUByte(int[] array, long offset);
    void writeUByte(int[] array, int arrayOffset, int length, long offset);
    
    void writeShort(short value, long offset);
    void writeShort(short[] array, long offset);
    void writeShort(short[] array, int arrayOffset, int length, long offset);
    
    void writeUShort(int value, long offset);
    void writeUShort(int[] array, long offset);
    void writeUShort(int[] array, int arrayOffset, int length, long offset);
    
    void writeInt24(int value, long offset);
    void writeInt24(int[] array, long offset);
    void writeInt24(int[] array, int arrayOffset, int length, long offset);
    
    void writeUInt24(int value, long offset);
    void writeUInt24(int[] array, long offset);
    void writeUInt24(int[] array, int arrayOffset, int length, long offset);
    
    void writeInt(int value, long offset);
    void writeInt(int[] array, long offset);
    void writeInt(int[] array, int arrayOffset, int length, long offset);
    
    void writeUInt(long value, long offset);
    void writeUInt(long[] array, long offset);
    void writeUInt(long[] array, int arrayOffset, int length, long offset);
    
    void writeLong(long value, long offset);
    void writeLong(long[] array, long offset);
    void writeLong(long[] array, int arrayOffset, int length, long offset);
    
    void writeFloat(float value, long offset);
    void writeFloat(float[] array, long offset);
    void writeFloat(float[] array, int arrayOffset, int length, long offset);
    
    void writeDouble(double value, long offset);
    void writeDouble(double[] array, long offset);
    void writeDouble(double[] array, int arrayOffset, int length, long offset);
    
}