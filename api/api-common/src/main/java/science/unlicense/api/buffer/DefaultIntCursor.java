
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class DefaultIntCursor extends AbstractCursor implements IntCursor{

    final Buffer buffer;
    
    //current offset from start.
    private long byteOffset = 0;

    public DefaultIntCursor(Buffer buffer, NumberEncoding encoding) {
        super(encoding);
        this.buffer = buffer;
    }
    
    public Buffer getBuffer() {
        return buffer;
    }

    public long getByteOffset() {
        return byteOffset;
    }

    public long getPosition() {
        return byteOffset/4;
    }

    public void setPosition(long position) {
        byteOffset = position*4;
    }

    public int read(long position) {
        return buffer.readInt(position*4);
    }

    public void read(int[] array, long position) {
        buffer.readInt(array,position*4);
    }

    public void read(int[] array, int arrayOffset, int length, long position) {
        buffer.readInt(array,arrayOffset,length,position*4);
    }

    public IntCursor write(int value, long position) {
        buffer.writeInt(value, position*4);
        return this;
    }

    public IntCursor write(int[] array, long position) {
        return write(array, 0, array.length, position);
    }

    public IntCursor write(int[] array, int arrayOffset, int length, long position) {
        buffer.writeInt(array,arrayOffset,length, position*4);
        return this;
    }

    public int read() {
        int v = buffer.readInt(byteOffset);
        byteOffset +=4;
        return v;
    }

    public void read(int[] array) {
        read(array,0,array.length);
    }

    public void read(int[] array, int arrayOffset, int length) {
        buffer.readInt(array,byteOffset);
        byteOffset+=length*4;
    }

    public IntCursor write(int value) {
        buffer.writeInt(value, byteOffset);
        byteOffset += 4;
        return this;
    }

    public IntCursor write(int[] array) {
        return write(array, 0, array.length);
    }

    public IntCursor write(int[] array, int arrayOffset, int length) {
        buffer.writeInt(array,arrayOffset,length, byteOffset);
        byteOffset += length*4;
        return this;
    }

}
