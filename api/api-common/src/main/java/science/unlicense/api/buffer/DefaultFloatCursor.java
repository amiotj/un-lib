
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloatCursor extends AbstractCursor implements FloatCursor{

    final Buffer buffer;
    
    //current offset from start.
    private long byteOffset = 0;

    public DefaultFloatCursor(Buffer buffer, NumberEncoding encoding) {
        super(encoding);
        this.buffer = buffer;
    }
    
    public Buffer getBuffer() {
        return buffer;
    }

    public long getByteOffset() {
        return byteOffset;
    }

    public long getPosition() {
        return byteOffset/4;
    }

    public void setPosition(long position) {
        byteOffset = position*4;
    }

    public float read(long position) {
        return buffer.readFloat(position*4);
    }

    public void read(float[] array, long position) {
        buffer.readFloat(array,position*4);
    }

    public void read(float[] array, int arrayOffset, int length, long position) {
        buffer.readFloat(array,arrayOffset,length,position*4);
    }

    public FloatCursor write(float value, long position) {
        buffer.writeFloat(value, position*4);
        return this;
    }

    public FloatCursor write(float[] array, long position) {
        return write(array, 0, array.length, position);
    }

    public FloatCursor write(float[] array, int arrayOffset, int length, long position) {
        buffer.writeFloat(array,arrayOffset,length, position*4);
        return this;
    }

    public float read() {
        float v = buffer.readFloat(byteOffset);
        byteOffset +=4;
        return v;
    }

    public void read(float[] array) {
        read(array,0,array.length);
    }

    public void read(float[] array, int arrayOffset, int length) {
        buffer.readFloat(array,byteOffset);
        byteOffset+=length*4;
    }

    public FloatCursor write(float value) {
        buffer.writeFloat(value, byteOffset);
        byteOffset += 4;
        return this;
    }

    public FloatCursor write(float[] array) {
        return write(array, 0, array.length);
    }

    public FloatCursor write(float[] array, int arrayOffset, int length) {
        buffer.writeFloat(array,arrayOffset,length, byteOffset);
        byteOffset += length*4;
        return this;
    }

}
