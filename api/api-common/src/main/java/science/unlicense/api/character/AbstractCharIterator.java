
package science.unlicense.api.character;

import science.unlicense.api.collection.primitive.ByteSequence;

/**
 * Abstract CharIterator.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractCharIterator implements CharIterator {

    protected final CharEncoding encoding;

    public AbstractCharIterator(CharEncoding encoding) {
        this.encoding = encoding;
    }
    
    public CharEncoding getEncoding() {
        return encoding;
    }

    public Char next() {
        return new Char(nextToBytes(),encoding);
    }

    public void nextToBuffer(ByteSequence buffer) {
        buffer.put(nextToBytes());
    }
    
    public int nextToUnicode() {
        return encoding.toUnicode(nextToBytes());
    }
    
    public Char peek() {
        return new Char(peekToBytes(),encoding);
    }
    
    public boolean nextEquals(Char ch) {
        return nextEquals(ch.toEncodingBytes(encoding));
    }

    public boolean nextEquals(int codepoint) {
        return nextEquals(getEncoding().toBytes(codepoint));
    }

    public boolean remove() {
        return false;
    }

    public void close() {        
    }
    
}
