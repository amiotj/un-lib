
package science.unlicense.api.predicate;

import science.unlicense.api.CObjects;

/**
 *
 * @author Johann Sorel
 */
public class DefaultVariable extends AbstractVariable{
        
    private Object value;

    public DefaultVariable() {
    }

    public DefaultVariable(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        if(CObjects.equals(value, this.value)) return;
        final Object old = this.value;
        this.value = value;
        fireValueChanged(old, value);
    }

}
