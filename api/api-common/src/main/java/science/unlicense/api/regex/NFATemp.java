

package science.unlicense.api.regex;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * Temporary object used when rebuilding NFA state graph.
 * 
 * @author Johann Sorel
 */
public final class NFATemp {
    public NFAState state;
    public final Sequence nexts;

    public NFATemp(NFAState state, NFAState.NextRef next) {
        this.state = state;
        this.nexts = new ArraySequence();
        this.nexts.add(next);
    }

    public NFATemp(NFAState start, Sequence nexts) {
        this.state = start;
        this.nexts = nexts;
    }

    public void setNext(NFAState state) {
        for (int i = 0, n = nexts.getSize(); i < n; i++) {
            ((NFAState.NextRef) nexts.get(i)).state = state;
        }
    }
    
}
