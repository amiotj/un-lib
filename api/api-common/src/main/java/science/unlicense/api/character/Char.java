
package science.unlicense.api.character;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;

/**
 * A single character.
 *
 * @author Johann Sorel
 */
public final class Char extends CObject{

    final CharEncoding encoding;
    final byte[] data;

    public Char(final byte[] data, final CharEncoding encoding) {
        if(data == null){
            throw new NullPointerException("Byte array must not be null.");
        }
        if(encoding == null){
            throw new NullPointerException("Char encoding must not be null.");
        }
        this.encoding = encoding;
        this.data = data;
    }

    public Char(final int cp) {
        this(cp,CharEncodings.DEFAULT);
    }
    
    public Char(final int cp, final CharEncoding encoding) {
        this.encoding = encoding;
        this.data = encoding.toBytes(cp);
    }
    
    /**
     * Number of bytes used by this character.
     * @return int
     */
    public int getByteLength(){
        return data.length;
    }

    /**
     * Get character encoding.
     * @return CharEncoding
     */
    public CharEncoding getEncoding() {
        return encoding;
    }

    /**
     * Encode this character with a different encoding.
     * @param encoding target encoding
     * @return Char
     */
    public Char recode(final CharEncoding encoding){
        return Characters.recode(this,encoding);
    }

    /**
     * @return true if character is uppercase
     */
    public boolean isUpperCase(){
        return isUpperCase(Languages.UNSET);
    }
    
    /**
     * @param lf character case
     * @return true if character is uppercase
     */
    public boolean isUpperCase(CharCase lf){
        return lf.isUpperCase(this);
    }
    
    /**
     * @return true if character is lowercase
     */
    public boolean isLowerCase(){
        return isLowerCase(Languages.UNSET);
    }
    
    /**
     * @param lf character case
     * @return true if character is lowercase
     */
    public boolean isLowerCase(CharCase lf){
        return lf.isLowerCase(this);
    }
    
    public Char toUpperCase(){
        return toUpperCase(Languages.UNSET);
    }

    public Char toUpperCase(CharCase lf){
        return lf.toUpperCase(this);
    }

    public Char toLowerCase(){
        return toLowerCase(Languages.UNSET);
    }

    public Char toLowerCase(CharCase lf){
        return lf.toLowerCase(this);
    }
    
    /**
     * Get the char as bytes in it's current encoding.
     * @return byte array
     */
    public byte[] toBytes() {
        return Arrays.copy(data, 0, data.length, new byte[data.length], 0);
    }

    /**
     * Get the char as bytes in given encoding.
     * @param encoding target encoding
     * @return byte array
     */
    public byte[] toBytes(final CharEncoding encoding) {
        if(getEncoding().equals(encoding)){
            return toBytes();
        }else{
            return Characters.recode(data, getEncoding(), encoding);
        }
    }

    public int toUnicode(){
        return encoding.toUnicode(data);
    }

    public Chars toChars() {
        return new Chars("Char "+Arrays.toChars(data));
    }
    
    public int getHash() {
        int hash = 7;
        hash = 97 * hash + getEncoding().hashCode();
        hash = 97 * hash + Arrays.computeHash(this.data);
        return hash;
    }

    /**
     * Default equals method.
     * Case sensitive and encoding unsensitive.
     * @param obj , object to test, can be null
     * @return true if equals
     */
    public boolean equals(Object obj) {
        return equals(obj, false, true,null);
    }

    /**
     * Equals method with current default language.
     * @param obj , object to test, can be null
     * @param ignoreCase set to true to ignore case
     * @param ignoreEncoding set to true to ignore encoding
     * @return true if equals
     */
    public boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding) {
        return equals(obj, ignoreCase, ignoreEncoding, null);
    }
    
    /**
     * Equals method with current default language.
     * @param obj , object to test, can be null
     * @param ignoreCase set to true to ignore case
     * @param ignoreEncoding set to true to ignore encoding
     * @param language language to use for case test, if null will use default language.
     * @return true if equals
     */
    public boolean equals(Object obj, boolean ignoreCase, boolean ignoreEncoding, CharCase language) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Char other = (Char) obj;
        
        
        if (!this.getEncoding().equals(other.getEncoding())) {
            if(ignoreEncoding){
                other = other.recode(this.getEncoding());
            }else{
                return false;
            }
        }
        
        if(language == null){
            language = Languages.UNSET;
        }
        
        if(!ignoreCase){
            return Arrays.equals(this.data, other.data);
        }else{
            final int c1 = this.toUnicode();
            final int c2 = other.toUnicode();
            if(c1==c2) return true;
            if(language.toLowerCase(c1) == c2) return true;
            if(language.toUpperCase(c1) == c2) return true;
            if(language.toLowerCase(c2) == c1) return true;
            if(language.toUpperCase(c2) == c1) return true;
            return false;
        }
    }

    /**
     * No byte array copy, do not modify.
     */
    byte[] toEncodingBytes(CharEncoding otherEnc){
        byte[] cdata = data;
        final CharEncoding thisEnc = getEncoding();
        if(thisEnc != otherEnc){
            //different encoding,recode char array
            return Characters.recode(cdata,otherEnc,thisEnc);
        }
        return cdata;
    }
    
    /**
     * Test if given character is a digit.
     * @param unicode character unicode codepoint
     * @return true if digit
     */
    public static boolean isDigit(int unicode){
        return !(unicode < 48 || unicode > 57);
    }
    
}
