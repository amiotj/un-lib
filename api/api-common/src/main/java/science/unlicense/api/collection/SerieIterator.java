
package science.unlicense.api.collection;

import science.unlicense.api.exception.MishandleException;
import science.unlicense.api.exception.UnmodifiableException;

/**
 * Concatenate multiple iterator.
 * 
 * @author Johann Sorel
 */
final class SerieIterator implements Iterator {

    private final Iterator wrapped;
    private Iterator current = null;

    SerieIterator(final Iterator[] wrapped) {
        this(Collections.staticCollection(wrapped).createIterator());
    }
    
    SerieIterator(final Iterator ite) {
        this.wrapped = ite;
        current = (Iterator) (ite.hasNext() ? ite.next() : null);
    }

    public Object next() {
        if (current == null) {
            throw new MishandleException("No more elements");
        } else {
            return current.next();
        }
    }

    public boolean hasNext() {
        if (current == null) {
            return false;
        }
        if (current.hasNext()) {
            return true;
        } else {
            current.close();
        }
        //search next valid iterator
        while(wrapped.hasNext()) {
            current = (Iterator) wrapped.next();
            if (current.hasNext()) {
                return true;
            } else {
                //empty iterator
                current.close();
            }
        }
        return false;
    }

    public boolean remove() {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public void close() {
        if(current!=null) current.close();
        
        while(wrapped.hasNext()) {
            Iterator n = (Iterator) wrapped.next();
            n.close();
        }
    }

}
