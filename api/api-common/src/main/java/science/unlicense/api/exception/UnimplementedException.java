
package science.unlicense.api.exception;

/**
 *
 * @author Johann Sorel
 */
public class UnimplementedException extends RuntimeException {

    public UnimplementedException() {
    }

    public UnimplementedException(String s) {
        super(s);
    }

    public UnimplementedException(Throwable cause) {
        super(cause);
    }

    public UnimplementedException(String message, Throwable cause) {
        super(message, cause);
    }
    
}