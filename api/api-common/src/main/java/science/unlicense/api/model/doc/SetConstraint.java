
package science.unlicense.api.model.doc;

import science.unlicense.api.collection.Collection;

/**
 *
 * @author Johann Sorel
 */
public interface SetConstraint extends Constraint {

    /**
     * Parameter possible values, if values are a finite set.
     * @return Collection or null.
     */
    Collection getValueSet();
}
