
package science.unlicense.api.number;

import science.unlicense.api.exception.UnimplementedException;

/**
 * 16bit decimal, called half.
 * Encoded in IEEE754
 * http://en.wikipedia.org/wiki/Half-precision_floating-point_format
 *
 * @author Johann Sorel
 */
public final class Float16 implements Number {

    public static final int MASK_FRACTION = 0x03FF;
    public static final int MASK_EXPONENT = 0x7C00;
    public static final int MASK_SIGN = 1 >> 15;

    public static final int FRACTION_SIZE = 10;
    public static final int EXPONENT_SIZE = 5;
    public static final int EXP_BIAS = 127;


    public static IEEE754 decompose(int value){
        return decompose(value, null);
    }

    public static IEEE754 decompose(int value, IEEE754 buffer){
        if(buffer == null) buffer = new IEEE754();
        final int bits = value; //java does not have Half type
        buffer.fraction = bits & MASK_FRACTION;
        buffer.exponent = ((bits & MASK_EXPONENT) >> FRACTION_SIZE);
        buffer.sign = bits >> 15;

        return buffer;
    }

    public static float compose(IEEE754 ieee){
        //TODO debugged
        int val = ieee.sign << 31;
        val |= ieee.exponent << 23;
        val |= ieee.fraction;
        return Float.intBitsToFloat(val);
    }

    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    public int toInteger() {
        throw new UnimplementedException("Not supported yet.");
    }

    public long toLong() {
        throw new UnimplementedException("Not supported yet.");
    }

    public float toFloat() {
        throw new UnimplementedException("Not supported yet.");
    }

    public double toDouble() {
        throw new UnimplementedException("Not supported yet.");
    }

}