
package science.unlicense.api.model.doc;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.predicate.VariableSyncException;
import science.unlicense.api.predicate.Variables;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractDocument extends AbstractEventSource implements Document {
    
    protected final DocumentType docType;

    public AbstractDocument() {
        this.docType = null;
    }

    public AbstractDocument(DocumentType docType) {
        this.docType = docType;
    }

    public DocumentType getType() {
        return docType;
    }

    @Override
    public Field getField(Chars name) throws FieldNotFoundException {
        return new IProperty(name);
    }
    
    protected void sendDocEvent(DocMessage.FieldChange fieldEvent){
        if(fieldEvent==null || !hasListeners()) return;
        final DocMessage msg = new DocMessage(false, new ArraySequence(new Object[]{fieldEvent}));
        getEventManager().sendEvent(new Event(this,msg));
    }
    
    protected void sendDocEvent(Sequence fieldEvents){
        if(fieldEvents==null || fieldEvents.isEmpty() || !hasListeners()) return;
        final DocMessage msg = new DocMessage(false, fieldEvents);
        getEventManager().sendEvent(new Event(this,msg));
    }
    public Chars toChars() {
        final CharBuffer buffer = new CharBuffer();
        buffer.append("Doc");
        if(docType!=null) buffer.append(" : ").append(docType.getTitle());
        buffer.append(" {");

        final Iterator ite = getFieldNames().createIterator();
        while(ite.hasNext()){
            final Chars fieldName = (Chars)ite.next();
            final Object value = getFieldValue(fieldName);
            buffer.append("\n  ");
            buffer.append(fieldName);
            buffer.append(" = ");
            Chars text = CObjects.toChars(value);
            text = text.replaceAll(new Chars("\n"), new Chars("\n  "));
            buffer.append(text);
        }
        buffer.append("\n}");
        return buffer.toChars();
    }

    private class IProperty implements Field {

        private final Chars name;

        public IProperty(Chars id) {
            this.name = id;
        }

        public Chars getName() {
            return name;
        }

        public FieldType getType(){
            return (docType==null) ? null : docType.getField(name);
        }

        public EventSource getHolder() {
            return AbstractDocument.this;
        }

        public Object getValue() {
            return getFieldValue(name);
        }

        public void setValue(Object value) {
            setFieldValue(name, value);
        }

        public boolean isReadable(){
            return true;
        }

        public boolean isWritable(){
            return true;
        }

        public void sync(Variable var) {
            sync(var, false);
        }

        public void sync(Variable var, boolean readOnly) {
            final Variables.VarSync sync = getVarSync();
            if(sync!=null) throw new VariableSyncException("Variable is already synchronized");
            Variables.sync(this, var, readOnly);
        }

        public void unsync() {
            final Variables.VarSync sync = getVarSync();
            if(sync!=null) sync.release();
        }

        private Variables.VarSync getVarSync(){
            final EventSource holder = AbstractDocument.this;
            final EventListener[] listeners = holder.getListeners(new PropertyPredicate(name));
            if(listeners.length==0) return null;
            for(int i=0;i<listeners.length;i++){
                if(listeners[i] instanceof Variables.VarSync){
                    final Variables.VarSync sync = ((Variables.VarSync)listeners[i]);
                    if(sync.getVar1().equals(this)){
                        return sync;
                    }
                }
            }
            return null;
        }

        public void addListener(EventListener listener){
            addEventListener(new PropertyPredicate(name), listener);
        }

        public void removeListener(EventListener listener){
            AbstractDocument.this.removeEventListener(new PropertyPredicate(name), listener);
        }

        public Class[] getEventClasses() {
            return new Class[]{PropertyMessage.class};
        }

        public void addEventListener(Predicate predicate, EventListener listener) {
            AbstractDocument.this.addEventListener(predicate, listener);
        }

        public void removeEventListener(Predicate predicate, EventListener listener) {
            AbstractDocument.this.removeEventListener(predicate, listener);
        }

        public EventListener[] getListeners(Predicate predicate) {
            return AbstractDocument.this.getListeners(predicate);
        }


    }

}
