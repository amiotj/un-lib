package science.unlicense.api.model.tree;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.event.EventSource;

/**
 * Basic node.
 *
 * @author Johann Sorel
 */
public interface Node extends EventSource{

    /**
     * 
     * @return Sequence of children nodes.
     */
    Collection getChildren();

    /**
     * Indicate if this node can contain sub nodes.
     * @return true if may have sub nodes.
     */
    boolean canHaveChildren();

    /**
     * Visit this node.
     *
     * @param visitor not null
     * @param context Context object, may be null
     * @return visit result, may be nul
     */
    Object accept(NodeVisitor visitor, Object context);

    Chars toChars();

    /**
     * Get a char sequence representation.
     * @param depth : maximum sub nodes depth to print.
     * @return CharSequence
     */
    Chars toCharsTree(int depth);

}
