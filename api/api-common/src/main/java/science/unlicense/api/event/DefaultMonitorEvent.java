
package science.unlicense.api.event;

import science.unlicense.api.character.Chars;

/**
 * Listen to object progress.
 *
 * @author Johann Sorel
 */
public class DefaultMonitorEvent extends DefaultEventMessage implements MonitorMessage {

    private final int state;
    private final float progress;
    private final Chars message;
    private final Exception error;
    private final Object result;

    /**
     *
     * @param state task execution state, one of STATE_X
     * @param progress execution progress, between 0 and 1
     * @param message progress message, can be null
     * @param result intermediate results, can be null
     * @param error execution exception, can be null
     */
    public DefaultMonitorEvent(int state, float progress,
            Chars message, Object result, Exception error) {
        super(false);
        this.state = state;
        this.progress = progress;
        this.message = message;
        this.error = error;
        this.result = result;
    }

    /**
     * {@inheritDoc}
     */
    public int getState() {
        return state;
    }

    /**
     * {@inheritDoc}
     */
    public float getProgress() {
        return progress;
    }

    /**
     * {@inheritDoc}
     */
    public Chars getMessage() {
        return message;
    }

    /**
     * {@inheritDoc}
     */
    public Object getResult() {
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public Exception getError() {
        return error;
    }

}
