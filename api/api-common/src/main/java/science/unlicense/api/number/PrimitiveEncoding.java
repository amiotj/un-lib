
package science.unlicense.api.number;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class PrimitiveEncoding extends CObject {
    
    private final int type;
    private final NumberEncoding enc;

    public PrimitiveEncoding(int type, NumberEncoding enc) {
        this.type = type;
        this.enc = enc;
    }

    public int getPrimitive() {
        return type;
    }

    public NumberEncoding getNumberEncoding() {
        return enc;
    }
    
    public int getBitSize() {
        return Primitive.getSizeInBits(type);
    }

    public Object read(byte[] buffer, int offset) {
        Object candidate = new Chars("");
        switch(type){
//            case TYPE_1_BIT     : candidate = ds.readBits(1); break;
//            case TYPE_2_BIT     : candidate = ds.readBits(2); break;
//            case TYPE_4_BIT     : candidate = ds.readBits(4); break;
            case Primitive.TYPE_BYTE      : candidate = enc.readByte(buffer, offset); break;
            case Primitive.TYPE_UBYTE     : candidate = enc.readUByte(buffer, offset); break;
            case Primitive.TYPE_SHORT     : candidate = enc.readShort(buffer, offset); break;
            case Primitive.TYPE_USHORT    : candidate = enc.readUShort(buffer, offset); break;
            case Primitive.TYPE_INT24     : candidate = enc.readInt24(buffer, offset); break;
            case Primitive.TYPE_UINT24    : candidate = enc.readUInt24(buffer, offset); break;
            case Primitive.TYPE_INT       : candidate = enc.readInt(buffer, offset); break;
            case Primitive.TYPE_UINT      : candidate = enc.readUInt(buffer, offset); break;
            case Primitive.TYPE_LONG      : candidate = enc.readLong(buffer, offset); break;
            case Primitive.TYPE_ULONG     : candidate = enc.readLong(buffer, offset); break;
            case Primitive.TYPE_FLOAT     : candidate = enc.readFloat(buffer, offset); break;
            case Primitive.TYPE_DOUBLE    : candidate = enc.readDouble(buffer, offset); break;
        }
        return candidate;
    }
    
    public Chars toChars() {
        Chars candidate = new Chars("");
        switch(type){
            case Primitive.TYPE_1_BIT     : candidate = new Chars("1Bit"); break;
            case Primitive.TYPE_2_BIT     : candidate = new Chars("2Bit"); break;
            case Primitive.TYPE_4_BIT     : candidate = new Chars("4Bit"); break;
            case Primitive.TYPE_BYTE      : candidate = new Chars("Byte"); break;
            case Primitive.TYPE_UBYTE     : candidate = new Chars("UByte"); break;
            case Primitive.TYPE_SHORT     : candidate = new Chars("Short"); break;
            case Primitive.TYPE_USHORT    : candidate = new Chars("UShort"); break;
            case Primitive.TYPE_INT24     : candidate = new Chars("Int24"); break;
            case Primitive.TYPE_UINT24    : candidate = new Chars("UInt24"); break;
            case Primitive.TYPE_INT       : candidate = new Chars("Int"); break;
            case Primitive.TYPE_UINT      : candidate = new Chars("UInt"); break;
            case Primitive.TYPE_LONG      : candidate = new Chars("Long"); break;
            case Primitive.TYPE_ULONG     : candidate = new Chars("Ulong"); break;
            case Primitive.TYPE_FLOAT     : candidate = new Chars("Float"); break;
            case Primitive.TYPE_DOUBLE    : candidate = new Chars("Double"); break;
        }
        if(type>5){
            if(enc==NumberEncoding.BIG_ENDIAN){
                candidate = candidate.concat(new Chars(" BE"));
            }else{
                candidate = candidate.concat(new Chars(" LE"));
            }
        }
        return candidate;
    }
    
}
