

package science.unlicense.api.number;

/**
 * Constant of the different primitive types.
 * 
 * @author Johann Sorel
 */
public final class Primitive {
    
    public static final int TYPE_UNKNOWNED = 0;
    public static final int TYPE_1_BIT = 1;
    public static final int TYPE_2_BIT = 2;
    public static final int TYPE_4_BIT = 3;
    public static final int TYPE_BYTE = 4;
    public static final int TYPE_UBYTE = 5;
    public static final int TYPE_SHORT = 6;
    public static final int TYPE_USHORT = 7;
    public static final int TYPE_INT24 = 8;
    public static final int TYPE_UINT24 = 9;
    public static final int TYPE_INT = 10;
    public static final int TYPE_UINT = 11;
    public static final int TYPE_LONG = 12;
    public static final int TYPE_ULONG = 13;
    public static final int TYPE_FLOAT = 14;
    public static final int TYPE_DOUBLE = 15;
    
    protected static final double[] NB_BYTES = new double[]{
        0.0,
        1.0/8.0,
        1.0/4.0,
        1.0/2.0,
        1.0,
        1.0,
        2.0,
        2.0,
        3.0,
        3.0,
        4.0,
        4.0,
        8.0,
        8.0,
        4.0,
        8.0
    };
    protected static final int[] NB_BITS = new int[]{
        0,
        1,
        2,
        4,
        8,
        8,
        16,
        16,
        24,
        24,
        32,
        32,
        64,
        64,
        32,
        64
    };
    
    private Primitive(){}
    
    public static double getSizeInBytes(int primitive){
        return NB_BYTES[primitive];
    }

    public static int getSizeInBits(int primitive){
        return NB_BITS[primitive];
    }
    
}