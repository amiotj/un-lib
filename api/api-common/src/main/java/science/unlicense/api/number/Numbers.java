

package science.unlicense.api.number;

import science.unlicense.api.exception.InvalidArgumentException;

/**
 * Utility functions for numbers.
 * 
 * @author Bertrand Cote
 * @author Johann Sorel
 */
public class Numbers {

    public static final byte[] HEXA = new byte[]{48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70};
    
    /**
     * Return True if n is not a power of 2.
     * 
     * @param n a positive integer
     * @return True if n is not a power of 2
     */
    public static boolean isNotPowOfTwo( int n ) {
        return ( n < 2 ) || ( (n&(n-1)) != 0 ); 
    }
    
    /**
     * Return the nearest pow of two higher or equal to n.
     * 
     * @param n value
     * @return nearest pow of two higher or equal to n.
     */
    public static int nextPowOfTwo( int n ) {
        if( !isNotPowOfTwo( n ) ) {
            return n;
        }
        int npt = 1;
        while( npt < n ) {
            npt <<= 1;
        }
        return npt;
    }
    
    /**
     * Reverses the bits order of x in a nBits length integer.
     * E.g:
     *      x=4 nBits=3
     *      x=4=100b then returns 001b=1
     * 
     *      x=7 nBits=4
     *      x=7=0111b then returns 1110b=13
     * 
     * @param i value
     * @param nBits number of bits of x
     * @return x in reverse bits order
     */
    public static int reverseBits ( int i, int nBits ) {
        int iRev = 0;
        while ( (i!=0) && (nBits-- >0) ){
            iRev <<= 1;
            iRev |= (i & 1);
            i >>= 1;
        }
        while (nBits-- > 0) {
            iRev <<= 1;
        }
        return iRev;
    }
    
    /**
     * Set one bit value in an int.
     * 
     * @param value
     * @param bit
     * @param offset
     * @return 
     */
    public static int setBit(int value, boolean bit, int offset){
        return bit ? value | (1 << offset) : value & ~(1 << offset);
    }

    /**
     * Set one bit value in an int.
     *
     * @param value
     * @param bit
     * @param offset
     * @return
     */
    public static long setBit(long value, boolean bit, int offset){
        return bit ? value | (1l << offset) : value & ~(1l << offset);
    }

    /**
     * Count number of bits set to 1 in given integer.
     * 
     * http://en.wikipedia.org/wiki/Hamming_weight
     * http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
     * 
     * @param i
     * @return number of bit set to 1 .
     */
    public static int countBitsSet(int i){
        i = i - ((i >>> 1) & 0x55555555);
        i = (i & 0x33333333) + ((i >>> 2) & 0x33333333);
        return (((i + (i >>> 4)) & 0x0F0F0F0F) * 0x01010101) >>> 24;
    }
    
    // ========== pow ==========================================================
    
    public static float pow( float x, int n ) {
        
        if( n==0 ) {
            return 1.f;
        }
        
        boolean negative = n<0;
        if( negative ) n = -n;
        
        if( x == 0.f ) {
            if( negative ) {
                throw new InvalidArgumentException( "Divide by zero exception.");
            } else {
                return 0.f;
            }
        }
        
        float res = 1.f;
        float tempo = x;
        
        while( n != 0 ) {
            if( (n&0x1) == 0x1 ) {
                res *= tempo;
            }
            tempo *= tempo;
            n >>= 1;
        }
        
        if( negative ) {
            return 1.f/res;
        } else {
            return res;
        } 
    }
    
    public static double pow( double x, int n ) {
        
        if( n==0 ) {
            return 1.;
        }
        
        boolean negative = n<0;
        if( negative ) n = -n;
        
        if( x == 0. ) {
            if( negative ) {
                throw new InvalidArgumentException( "Divide by zero exception.");
            } else {
                return 0.;
            }
        }
        
        double res = 1.;
        double tempo = x;
        
        while( n != 0 ) {
            if( (n&0x1) == 0x1 ) {
                res *= tempo;
            }
            tempo *= tempo;
            n >>= 1;
        }
        
        if( negative ) {
            return 1./res;
        } else {
            return res;
        } 
    }
    
}
