
package science.unlicense.api.model.doc;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.model.Presentable;

/**
 * Definition of a document fields.
 * 
 * @author Johann Sorel
 */
public interface DocumentType extends Presentable {
    
    /**
     * Indicate if document type is strict.
     * Document created with a strict type can not have any extra fields.
     * 
     * @return true if strict
     */
    boolean isStrict();

    /**
     * List document local field types.
     * This include only this type fields, not parent fields.
     *
     * @return FieldType array, never null, can be empty.
     */
    FieldType[] getLocalFields();

    /**
     * List document field types.
     * This include this type and parent types.
     * 
     * @return FieldType array, never null, can be empty.
     */
    FieldType[] getFields();

    /**
     * Get field type.
     * 
     * @param name
     * @return null if field do not exist
     * @throws FieldNotFoundException if field does not exist and document type is strict
     */
    FieldType getField(Chars name) throws FieldNotFoundException;
    
    /**
     * Dictionary of document type attributes.
     * Keys are of Chars type.
     * Values can be anything.
     * 
     * @return Dictionary, never null, can be empty.
     */
    Dictionary getAttributes();

    /**
     * Get parent types.
     *
     * @return DocumentType array, never null, can be empty
     */
    DocumentType[] getSuper();
    
}
