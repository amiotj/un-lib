
package science.unlicense.api.collection;

import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.exception.UnmodifiableException;

/**
 * Unmodifiable view of a sequence.
 * TODO handle events : see DecorateEventManager
 * 
 * @author Johann Sorel
 */
final class ReadOnlySequence extends AbstractEventSource implements Sequence{

    private final Sequence sub;

    ReadOnlySequence(Sequence sub) {
        this.sub = sub;
    }
    
    public Object get(int index) {
        return sub.get(index);
    }

    public boolean add(int index, Object value) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean addAll(int index, Object[] candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean addAll(int index, Collection candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean addAll(Object[] candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public Object replace(int index, Object item) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public void replaceAll(Object[] items) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean remove(int index) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public int search(Object item) {
        return sub.search(item);
    }

    public Iterator createReverseIterator() {
        return new ReadOnlyIterator(sub.createReverseIterator());
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean addAll(Collection candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean removeAll(Collection candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean removeAll(Object[] candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean removeAll() {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean contains(Object candidate) {
        return sub.contains(candidate);
    }

    public Iterator createIterator() {
        return new ReadOnlyIterator(sub.createIterator());
    }

    public int getSize() {
        return sub.getSize();
    }

    public boolean isEmpty() {
        return sub.isEmpty();
    }

    public Object[] toArray() {
        return sub.toArray();
    }

    public Object[] toArray(Class clazz) {
        return sub.toArray(clazz);
    }
    
    public Class[] getEventClasses() {
        return sub.getEventClasses();
    }
    
}
