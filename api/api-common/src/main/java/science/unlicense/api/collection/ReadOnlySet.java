
package science.unlicense.api.collection;

import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.exception.UnmodifiableException;

/**
 * Unmodifiable view of a set.
 * TODO handle events : see DecorateEventManager
 * 
 * @author Johann Sorel
 */
final class ReadOnlySet extends AbstractEventSource implements Set{
    
    private final Set sub;

    ReadOnlySet(Set sub) {
        this.sub = sub;
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean addAll(Collection candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean addAll(Object[] candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean removeAll(Collection candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean removeAll(Object[] candidate) {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public boolean removeAll() {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public void replaceAll(Object[] items) {
        throw new UnmodifiableException("Modifications not allowed.");
    }
    
    public boolean contains(Object candidate) {
        return sub.contains(candidate);
    }

    public Iterator createIterator() {
        return new ReadOnlyIterator(sub.createIterator());
    }

    public int getSize() {
        return sub.getSize();
    }

    public boolean isEmpty() {
        return sub.isEmpty();
    }

    public Object[] toArray() {
        return sub.toArray();
    }
    
    public Object[] toArray(Class clazz) {
        return sub.toArray(clazz);
    }

    public Class[] getEventClasses() {
        return sub.getEventClasses();
    }
    
}
