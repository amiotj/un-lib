
package science.unlicense.api.graph;

import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;

/**
 * Default vertex implementation.
 *
 * @author Johann Sorel
 */
public class DefaultVertex extends AbstractVertex {

    private final Graph graph;
    private final Dictionary properties = new HashDictionary();

    public DefaultVertex(Graph graph) {
        this.graph = graph;
    }

    public Graph getGraph() {
        return graph;
    }

    public Dictionary getProperties() {
        return properties;
    }

}
