
package science.unlicense.api.predicate;

import science.unlicense.api.character.Chars;

/**
 * A Function is an expression with sub expressions.
 * Functions have names and can be extended using FunctionResolvers.
 * 
 * @author Johann Sorel
 */
public interface Function extends Expression{
   
    /**
     * Get function name.
     * @return Chars, never null
     */
    Chars getName();
    
    /**
     * Get function parameters.
     * @return Expression array, never null but can be empty
     */
    Expression[] getParameters();
    
}
