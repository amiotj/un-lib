
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class DefaultBufferFactory implements BufferFactory {

    public static final DefaultBufferFactory INSTANCE = new DefaultBufferFactory();
        
    public Buffer createByte(long nbValue) {
        return create(nbValue, Primitive.TYPE_UBYTE, NumberEncoding.BIG_ENDIAN);
    }

    public Buffer createInt(long nbValue){
        return create(nbValue, Primitive.TYPE_INT, NumberEncoding.BIG_ENDIAN);
    }
    
    public Buffer createLong(long nbValue) {
        return create(nbValue, Primitive.TYPE_LONG, NumberEncoding.BIG_ENDIAN);
    }
    
    public Buffer createFloat(long nbValue){
        return create(nbValue, Primitive.TYPE_FLOAT, NumberEncoding.BIG_ENDIAN);
    }
    
    public Buffer createDouble(long nbValue){
        return create(nbValue, Primitive.TYPE_DOUBLE, NumberEncoding.BIG_ENDIAN);
    }
    
    public static Buffer wrap(byte[] buffer) {
        return new DefaultByteBuffer(buffer);
    }
    
    public static Buffer wrap(int[] buffer) {
        return new DefaultIntBuffer(buffer);
    }
    
    public static Buffer wrap(float[] buffer) {
        return new DefaultFloatBuffer(buffer);
    }
    
    public static Buffer wrap(double[] buffer) {
        return new DefaultDoubleBuffer(buffer);
    }
    
    public static Buffer wrap(byte[] buffer, int primitiveType, NumberEncoding encoding) {
        return new DefaultByteBuffer(buffer,primitiveType,encoding);
    }
    
    public static Buffer create(long nbValue, int primitiveType, NumberEncoding encoding) {
        if(primitiveType==Primitive.TYPE_INT && encoding == NumberEncoding.BIG_ENDIAN){
            return new DefaultIntBuffer(new int[(int)nbValue]);
        }else if(primitiveType==Primitive.TYPE_FLOAT && encoding == NumberEncoding.BIG_ENDIAN){
            return new DefaultFloatBuffer(new float[(int)nbValue]);
        }else if(primitiveType==Primitive.TYPE_DOUBLE && encoding == NumberEncoding.BIG_ENDIAN){
            return new DefaultDoubleBuffer(new double[(int)nbValue]);
        }else{
            return new DefaultByteBuffer((int)nbValue,primitiveType,encoding);
        }
    }
    
}