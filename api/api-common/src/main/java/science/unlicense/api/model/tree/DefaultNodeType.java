package science.unlicense.api.model.tree;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 * Modifiable node type, until freeze method is called.
 *
 * @author Johann Sorel
 */
public class DefaultNodeType extends CObject implements NodeType{

    private Chars name;
    private CharArray title;
    private CharArray description;
    private Class binding;
    private Object defaultValue;
    private boolean nullable;
    private NodeCardinality[] descriptors;

    private boolean freeze = false;

    public DefaultNodeType() {
        descriptors = new NodeCardinality[0];
    }

    public DefaultNodeType(Chars name, CharArray title, CharArray description, Class binding,
            boolean nullable, NodeCardinality[] descriptors) {
        this(name,title,description,binding,null,nullable,descriptors);
        freeze();
    }

    public DefaultNodeType(Chars name, CharArray title, CharArray description, Class binding, Object defaultValue,
            boolean nullable, NodeCardinality[] descriptors) {
        this.name = name;
        this.title = title;
        this.description = description;
        this.binding = binding;
        this.defaultValue = defaultValue;
        this.nullable = nullable;
        if(descriptors == null){
            descriptors = new NodeCardinality[0];
        }

        this.descriptors = descriptors;
        freeze();
    }

    public Chars getId() {
        return name;
    }

    public void setId(Chars name) {
        checkFreeze();
        this.name = name;
    }

    public CharArray getTitle() {
        return title;
    }

    public void setTitle(CharArray title) {
        this.title = title;
    }

    public CharArray getDescription() {
        return description;
    }

    public void setDescription(CharArray description) {
        this.description = description;
    }

    public Class getValueClass() {
        return binding;
    }

    public void setBinding(Class binding) {
        checkFreeze();
        this.binding = binding;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Object defaultValue) {
        checkFreeze();
        this.defaultValue = defaultValue;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        checkFreeze();
        this.nullable = nullable;
    }

    public boolean isComplexe() {
        return descriptors.length != 0;
    }

    public NodeCardinality[] getChildrenTypes() {
        return Arrays.copy(descriptors, new NodeCardinality[descriptors.length]);
    }

    public void setChildrenTypes(NodeCardinality[] descriptors) {
        checkFreeze();
        if(descriptors == null){
            descriptors = new NodeCardinality[0];
        }
        this.descriptors = descriptors;
    }

    public NodeCardinality getChild(Chars name) {
        if(descriptors!=null){
            for(int i=0;i<descriptors.length;i++){
                if(descriptors[i].getId().equals(name)){
                    return descriptors[i];
                }
            }
        }
        return null;
    }

    public Chars thisToChars(){
        return new Chars(""+name+"  ("+binding+")");
    }

    public Chars toChars() {
        return toCharsTree(3);
    }

    public Chars toCharsTree(int depth){
        return Nodes.toCharsTree(this, thisToChars(), depth);
    }

    /**
     * Lock node type.
     * Forbid any further modifications.
     */
    public void freeze(){
        freeze = true;
    }

    private void checkFreeze(){
        if(freeze){
            throw new InvalidArgumentException("Node type has been freezed, further modification are not allowed");
        }
    }

}
