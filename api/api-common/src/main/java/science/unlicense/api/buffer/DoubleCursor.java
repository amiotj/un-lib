
package science.unlicense.api.buffer;

/**
 *
 * @author Johann Sorel
 */
public interface DoubleCursor extends Cursor {

    long getPosition();

    void setPosition(long position);

    ////////////////////////////////////////////////////////////////////////
    double read(long position);

    void read(double[] array, long position);

    void read(double[] array, int arrayOffset, int length, long position);

    DoubleCursor write(double value, long position);

    DoubleCursor write(double[] array, long position);

    DoubleCursor write(double[] array, int arrayOffset, int length, long position);

    ////////////////////////////////////////////////////////////////////////
    double read();

    void read(double[] array);

    void read(double[] array, int arrayOffset, int length);

    DoubleCursor write(double value);

    DoubleCursor write(double[] array);

    DoubleCursor write(double[] array, int arrayOffset, int length);
    
}
