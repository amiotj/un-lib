
package science.unlicense.api.collection;

import science.unlicense.api.exception.UnmodifiableException;

/**
 * Unmodifiable view iterator.
 * 
 * @author Johann Sorel
 */
final class ReadOnlyIterator implements Iterator {
    
    private final Iterator sub;

    ReadOnlyIterator(Iterator sub) {
        this.sub = sub;
    }

    public boolean hasNext() {
        return sub.hasNext();
    }

    public Object next() {
        return sub.next();
    }

    public boolean remove() {
        throw new UnmodifiableException("Modifications not allowed.");
    }

    public void close() {
        sub.close();
    }
    
}
