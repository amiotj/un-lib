
package science.unlicense.api.model.tree;

import science.unlicense.api.collection.AbstractSequence;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.WeakEventListener;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.predicate.Predicate;

/**
 * Read only sequence of the nodes in a node tree.
 *
 * @author Johann Sorel
 */
public class NodeSequence extends AbstractSequence implements EventListener {

    private final Node root;
    private final boolean includeThisNode;
    private final Predicate filter;

    private Sequence cache = null;

    public NodeSequence(Node root, boolean includeThisNode) {
        this(root,includeThisNode,Predicate.TRUE);
    }

    public NodeSequence(Node root, boolean includeThisNode, Predicate filter) {
        this.root = root;
        this.includeThisNode = includeThisNode;
        this.filter = filter;
        root.addEventListener(CollectionMessage.PREDICATE, new WeakEventListener(root, Predicate.TRUE, this));
    }

    private synchronized void clearCache(){
        cache = null;
    }

    private synchronized Sequence getCache(){
        if(cache==null){
            cache = Nodes.flatten(root,includeThisNode,filter);
        }
        return cache;
    }

    @Override
    public Object get(int index) {
        return getCache().get(index);
    }

    @Override
    public int getSize(){
        return getCache().getSize();
    }

    @Override
    public Iterator createIterator() {
        return getCache().createIterator();
    }

    @Override
    public Iterator createReverseIterator() {
        return getCache().createReverseIterator();
    }

    @Override
    public boolean add(int index, Object value) {
        throw new UnimplementedException("Node sequence is read-only.");
    }

    @Override
    public boolean remove(int index) {
        throw new UnimplementedException("Node sequence is read-only.");
    }

    @Override
    public void receiveEvent(Event event) {
        clearCache();
    }

}
