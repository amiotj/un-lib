package science.unlicense.api.model.tree;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class DefaultNamedNode extends DefaultNode implements NamedNode{

    public static final Chars PROPERTY_NAME = new Chars("Name");
    public static final Chars PROPERTY_VALUE = new Chars("Value");
    
    protected Chars name;
    protected Object value = null;

    public DefaultNamedNode(boolean allowChildren) {
        super(allowChildren);
    }

    public DefaultNamedNode(Node[] children) {
        super(children);
    }

    public DefaultNamedNode(Chars name, boolean allowChildren) {
        super(allowChildren);
        this.name = name;
    }

    public DefaultNamedNode(Chars name, Node[] children) {
        super(children);
        this.name = name;
    }

    public DefaultNamedNode(Chars name, Object value, boolean allowChildren) {
        super(allowChildren);
        this.name = name;
        this.value = value;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        if(CObjects.equals(this.name, name)) return;
        Chars oldValue = this.name;
        this.name = name;
        if(hasListeners()){
            sendPropertyEvent(this, PROPERTY_NAME, oldValue, name);
        }
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        if(CObjects.equals(this.value, value)) return;
        Object oldValue = this.value;
        this.value = value;
        if(hasListeners()){
            sendPropertyEvent(this, PROPERTY_VALUE, oldValue, value);
        }
    }

    public NamedNode getNamedChild(Chars name) {
        for(int i=0,n=children.getSize();i<n;i++){
            final Object candidate = children.get(i);
            if(candidate instanceof NamedNode && name.equals(((NamedNode)candidate).getName())){
                return (NamedNode) candidate;
            }
        }
        return null;
    }

    public Sequence getNamedChildren(Chars name) {
        final Sequence res = new ArraySequence();
        for(int i=0,n=children.getSize();i<n;i++){
            final Object candidate = children.get(i);
            if(candidate instanceof NamedNode && name.equals(((NamedNode)candidate).getName())){
                res.add(candidate);
            }
        }
        return res;
    }
    
    public Chars thisToChars() {
        if(getValue()==null){
            return getName();
        }else{
            return getName().concat(':').concat(CObjects.toChars(getValue()));
        }
    }

}
