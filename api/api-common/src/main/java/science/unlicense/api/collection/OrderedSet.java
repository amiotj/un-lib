
package science.unlicense.api.collection;

/**
 * Ordered set. inserted values are ordered base on values returned
 * by order method.
 * Objects passed in must be instances of Orderable.
 *
 * @author Johann Sorel
 */
public interface OrderedSet extends Set{

}
