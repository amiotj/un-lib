
package science.unlicense.api.predicate;

/**
 *
 * @author Johann Sorel
 */
public class VariableSyncException extends RuntimeException{

    public VariableSyncException() {
    }

    public VariableSyncException(String message) {
        super(message);
    }

    public VariableSyncException(Throwable cause) {
        super(cause);
    }

    public VariableSyncException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
