
package science.unlicense.api.predicate;

import science.unlicense.api.CObjects;
import science.unlicense.api.References;
import science.unlicense.api.Releasable;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;

/**
 * Variable utilities.
 * 
 * @author Johann Sorel
 */
public final class Variables {
    
    private Variables(){}
    
    /**
     * Synchronize variable values in read/write mode.
     * 
     * @param var1 base variable
     * @param var2 variable to read from and write to
     */
    public static void sync(Variable var1, Variable var2){
        sync(var1, var2, false);
    }
    
    /**
     * Synchronize variable values.
     * 
     * @param var1 base variable
     * @param var2 variable to read from and write to
     * @param readOnly true to synchronize reading only.
     */
    public static void sync(Variable var1, Variable var2, boolean readOnly){
        syncInternal(var1, var2, readOnly);
    }
    
    static VarSync syncInternal(Variable var1, Variable var2, boolean readOnly){
        return new VarSync(var1, var2, readOnly);
    }
    
    public static final class VarSync implements Releasable, EventListener{
        private final References.Weak source1;
        private final References.Weak source2;
        private final Variable var1;
        private final Variable var2;
        private final boolean readOnly;

        private VarSync(Variable source1, Variable source2, boolean readOnly) {
            this.source1 = new References.Weak(source1.getHolder(),this);
            this.source2 = new References.Weak(source2.getHolder(),this);
            this.readOnly = readOnly;
            
            //if the variable isn't the same as the holder we keep a hard reference to the variable
            var1 = (source1!=source1.getHolder()) ? source1 : null;
            var2 = (source2!=source2.getHolder()) ? source2 : null;

            source1.setValue(source2.getValue());

            //listen to sources
            source1.addListener(this);
            source2.addListener(this);
        }

        public Variable getVar1() {
            return var1;
        }

        public Variable getVar2() {
            return var2;
        }
        
        public EventSource getHolder1() {
            return (EventSource) source1.get();
        }

        public EventSource getHolder2() {
            return (EventSource) source2.get();
        }

        public void release(){
            final Variable src1 = var1==null ? (Variable) source1.get() : var1;
            final Variable src2 = var2==null ? (Variable) source2.get() : var2;
            if(src1!=null) src1.removeListener(this);
            if(src2!=null) src2.removeListener(this);
        }

        public void receiveEvent(Event event) {
            final Variable src1 = var1==null ? (Variable) source1.get() : var1;
            final Variable src2 = var2==null ? (Variable) source2.get() : var2;
            if(src1==null || src2==null){
                //object is going to be disposed
                return;
            }

            final VariableMessage pe = (VariableMessage) event.getMessage();
            final Object source = event.getSource();
            final Object newVal = pe.getNewValue();
            if(source == src1.getHolder()){
                if(readOnly){
                    //raise an error if the value isn't the same
                    Object val = src2.getValue();
                    if(!CObjects.equals(val, newVal)){
                        throw new VariableSyncException("Variable is synchronized in read only, value can not be set.");
                    }
                } else {
                    src2.setValue(newVal);
                }
            }else if(source == src2.getHolder()){
                src1.setValue(newVal);
            }
        }
    }
            
    
}
