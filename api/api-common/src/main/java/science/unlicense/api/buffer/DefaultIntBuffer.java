
package science.unlicense.api.buffer;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class DefaultIntBuffer extends AbstractBuffer{

    private static final int[] MASKS = {
        0xFF000000,
        0x00FF0000,
        0x0000FF00,
        0x000000FF};
    private static final int[] OFFSET = {
        24,16,8,0};
    
    private final int[] buffer;

    public DefaultIntBuffer(int[] buffer) {
        super(Primitive.TYPE_INT, NumberEncoding.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }
    
    public boolean isWritable() {
        return true;
    }

    public Object getBackEnd() {
        return buffer;
    }

    public long getBufferByteSize() {
        return buffer.length*4;
    }

    public byte readByte(long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        final int i = buffer[index];
        return (byte)( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    public int readInt(long offset) {
        if(offset%4==0){
            return buffer[(int)(offset/4)];
        }else{
            return super.readInt(offset);
        }
    }

    public void readInt(int[] array, int arrayOffset, int length, long offset) {
        if(offset%4==0){
            Arrays.copy(buffer, (int)(offset/4), length, array, arrayOffset);
        }else{
            super.readInt(array, arrayOffset, length, offset);
        }
    }
    
    public void writeByte(byte value, long offset) {
        final int index = (int) (offset/4);
        final int mod = (int) (offset%4);
        int i = buffer[index];
        i &= ~MASKS[mod];
        i |= (value&0xFF) << OFFSET[mod];
        buffer[index] = i;
    }

    public void writeInt(int value, long offset) {
        if(offset%4==0){
            buffer[(int)(offset/4)] = value;
        }else{
            super.writeInt(value, offset);
        }
    }

    public void writeInt(int[] array, int arrayOffset, int length, long offset) {
        if(offset%4==0){
            Arrays.copy(array, arrayOffset, length, buffer, (int)(offset/4));
        }else{
            super.writeInt(array, arrayOffset, length, offset);
        }
    }

    public Buffer copy() {
        return new DefaultIntBuffer(Arrays.copy(buffer));
    }
    
}
