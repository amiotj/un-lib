
package science.unlicense.api.collection;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.InvalidIndexException;
import science.unlicense.api.exception.UnmodifiableException;

/**
 * Unmodifiable collection.
 * 
 * @author Johann Sorel
 */
final class StaticCollection extends AbstractSequence implements Set {

    protected Object[] values;

    StaticCollection(Object[] array) {
        this.values = Arrays.copy(array);
    }

    public Object get(int index) {
        if(index<0 || index>=values.length) throw new InvalidIndexException(""+index);
        return values[index];
    }

    public boolean add(Object candidate) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean add(int index, Object value) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean addAll(int index, Collection candidate) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }
    
    public boolean addAll(int index, Object[] candidate) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public Object replace(int index, Object item) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public void replaceAll(Collection items) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }
    
    public void replaceAll(Object[] items) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean addAll(Collection candidate) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }
    
    public boolean addAll(Object[] candidates) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }
    
    public boolean remove(Object candidate) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean remove(int index) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean removeAll(Collection candidate) {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean removeAll() {
        throw new UnmodifiableException("Collection is unmodifiable");
    }

    public boolean contains(Object candidate) {
        return Arrays.contains(values,0,values.length,candidate);
    }

    public int search(Object item) {
        return Arrays.getFirstOccurence(values, 0, values.length, item);
    }

    public int searchIdentity(Object item) {
        return Arrays.getFirstOccurenceIdentity(values, 0, values.length, item);
    }

    public Iterator createIterator() {
        return new ArrayIterator();
    }

    public Iterator createReverseIterator() {
        return new ReverseArrayIterator();
    }

    public int getSize() {
        return values.length;
    }

    public boolean isEmpty(){
        return values.length == 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Sequence)) {
            return false;
        }
        final Sequence other = (Sequence) obj;
        if(this.getSize() != other.getSize()){
            return false;
        }
        for(int i=0;i<this.getSize();i++){
            if(!get(i).equals(other.get(i))){
                return false;
            }
        }
        return true;
    }

    public Object[] toArray() {
        return Arrays.copy(values,0,values.length);
    }

    private class ArrayIterator implements Iterator{

        private int index = 0;

        public boolean hasNext() {
            return index<values.length;
        }

        public Object next() {
            Object obj = values[index];
            index++;
            return obj;
        }

        public boolean remove() {
            throw new RuntimeException("Collection is unmodifiable");
        }

        public void close() {
        }

    }

    private final class ReverseArrayIterator<A> implements Iterator {

        private int index = values.length - 1;

        @Override
        public boolean hasNext() {
            return values.length > 0 && index >= 0;
        }

        @Override
        public A next() {
            if (index < 0) {
                throw new CollectionException("No more element");
            }
            return (A) values[index--];
        }

        @Override
        public boolean remove() {
            throw new CollectionException("Collection is unmodifiable");
        }

        @Override
        public void close() {
        }
    }

}
