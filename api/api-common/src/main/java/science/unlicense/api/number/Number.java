
package science.unlicense.api.number;

/**
 * Subset of Arithmetic objects which define a numeric value.
 *
 * @author Johann Sorel
 */
public interface Number extends Arithmetic {

    /**
     * Map this number to an integer.
     * When using this method, the user is aware that an integer may not be
     * enough to represent this number therefor the returned value is considered
     * the best possible approximation possible.
     * 
     * @return integer
     */
    int toInteger();

    /**
     * Map this number to a long.
     * When using this method, the user is aware that a long may not be
     * enough to represent this number therefor the returned value is considered
     * the best possible approximation possible.
     *
     * @return integer
     */
    long toLong();

    /**
     * Map this number to a float.
     * When using this method, the user is aware that a float may not be
     * enough to represent this number therefor the returned value is considered
     * the best possible approximation possible.
     *
     * @return integer
     */
    float toFloat();
    
    /**
     * Map this number to a double.
     * When using this method, the user is aware that a double may not be
     * enough to represent this number therefor the returned value is considered
     * the best possible approximation possible.
     * 
     * @return integer
     */
    double toDouble();


}
