
package science.unlicense.api.buffer;

/**
 * 
 * @author Johann Sorel
 */
public interface DataCursor extends Cursor {

    /**
     * @return current bit offset, between 0 and 8.
     */
    int getBitOffset();

    /**
     * Move cursor bit position.
     * @param position new cursor position
     */
    void setBitOffset(int position);

    /**
     * Move cursor to given position.
     * @param position new cursor position
     */
    void setByteOffset(long position);
    
    /**
     * @return true if more bytes are available
     */
    boolean hasNext();
    
    /**
     * Get the number of remaining bytes.
     * @return remaining bytes.
     */
    long getRemaining();

    void skipBits(long nb);

    void skipToByteEnd();

    void skipByte();

    void skipBytes(long nb);

    ////////////////////////////////////////////////////////////////////////////
    int readBit();

    int readBit(int nbBits);

    int readBit(int nbBits, int startOrder);

    int[] readBit(int[] buffer, int nbBits);

    int[] readBit(int[] buffer, final int offset, final int length, int nbBits);

    byte readByte();

    void readByte(byte[] array);

    void readByte(byte[] array, int arrayOffset, int length);

    int readUByte();

    void readUByte(int[] array);

    void readUByte(int[] array, int arrayOffset, int length);

    short readShort();

    void readShort(short[] array);

    void readShort(short[] array, int arrayOffset, int length);

    int readUShort();

    void readUShort(int[] array);

    void readUShort(int[] array, int arrayOffset, int length);

    int readInt24();

    void readInt24(int[] array);

    void readInt24(int[] array, int arrayOffset, int length);

    int readUInt24();

    void readUInt24(int[] array);

    void readUInt24(int[] array, int arrayOffset, int length);

    int readInt();

    void readInt(int[] array);

    void readInt(int[] array, int arrayOffset, int length);

    long readUInt();

    void readUInt(long[] array);

    void readUInt(long[] array, int arrayOffset, int length);

    long readLong();

    void readLong(long[] array);

    void readLong(long[] array, int arrayOffset, int length);

    float readFloat();

    void readFloat(float[] array);

    void readFloat(float[] array, int arrayOffset, int length);

    double readDouble();

    void readDouble(double[] array);

    void readDouble(double[] array, int arrayOffset, int length);

    ////////////////////////////////////////////////////////////////////////////
    DataCursor writeBit(int value, int nbBits);

    DataCursor writeByte(byte value);

    DataCursor writeByte(byte[] array);

    DataCursor writeByte(byte[] array, int arrayOffset, int length);

    DataCursor writeUByte(int value);

    DataCursor writeUByte(int[] array);

    DataCursor writeUByte(int[] array, int arrayOffset, int length);

    DataCursor writeShort(short value);

    DataCursor writeShort(short[] array);

    DataCursor writeShort(short[] array, int arrayOffset, int length);

    DataCursor writeUShort(int value);

    DataCursor writeUShort(int[] array);

    DataCursor writeUShort(int[] array, int arrayOffset, int length);

    DataCursor writeInt24(int value);

    DataCursor writeInt24(int[] array);

    DataCursor writeInt24(int[] array, int arrayOffset, int length);

    DataCursor writeUInt24(int value);

    DataCursor writeUInt24(int[] array);

    DataCursor writeUInt24(int[] array, int arrayOffset, int length);

    DataCursor writeInt(int value);

    DataCursor writeInt(int[] array);

    DataCursor writeInt(int[] array, int arrayOffset, int length);

    DataCursor writeUInt(long value);

    DataCursor writeUInt(long[] array);

    DataCursor writeUInt(long[] array, int arrayOffset, int length);

    DataCursor writeLong(long value);

    DataCursor writeLong(long[] array);

    DataCursor writeLong(long[] array, int arrayOffset, int length);

    DataCursor writeFloat(float value);

    DataCursor writeFloat(float[] array);

    DataCursor writeFloat(float[] array, int arrayOffset, int length);

    DataCursor writeDouble(double value);

    DataCursor writeDouble(double[] array);

    DataCursor writeDouble(double[] array, int arrayOffset, int length);
    
}
