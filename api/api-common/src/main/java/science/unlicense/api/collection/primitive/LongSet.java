
package science.unlicense.api.collection.primitive;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.AbstractCollection;
import science.unlicense.api.collection.AbstractIterator;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedSet;

/**
 *
 * @author Johann Sorel
 */
public class LongSet extends AbstractCollection implements OrderedSet{

    private long[] buffer;
    private int size = 0;

    public LongSet() {
        buffer = new long[1000];
        size = 0;
    }
    
    private void growIfNecessary(int length){
        size += length;
        if(size >= buffer.length){
            int gs = Math.max(length, buffer.length*3);
            buffer = Arrays.resize(buffer, buffer.length+gs);
        }
    }
    
    public boolean add(long candidate){
        int index = Arrays.binarySearch(candidate, buffer,0,size-1);
        if(index<0){
            //new value
            index = -(index+1);
            growIfNecessary(1);
            Arrays.copy(buffer, index, size-1-index, buffer, index+1);
            buffer[index] = candidate;
            return true;
        }else{
            return false;
        }
    }
    
    public boolean remove(long candidate){
        final int index = Arrays.binarySearch(candidate, buffer,0,size-1);
        if(index>=0){
            Arrays.copy(buffer, index+1, size-index, buffer, index);
            size--;
            return true;
        }else{
            return false;
        }
    }
    
    public boolean contains(long candidate){
        return Arrays.binarySearch(candidate, buffer,0,size-1) >=0;
    }
    
    public boolean add(Object candidate) {
        return add((Long)candidate);
    }

    public boolean remove(Object candidate) {
        return remove((Long)candidate);
    }

    public boolean contains(Object candidate) {
        return contains((Long)candidate);
    }
    
    public Iterator createIterator() {
        return new Ite();
    }

    public boolean removeAll() {
        buffer = new long[0];
        return true;
    }

    /**
     * @return long array
     */
    public long[] toArrayLong() {
        return Arrays.copy(buffer, 0, size, new long[size], 0);
    }
    
    private final class Ite extends AbstractIterator{

        int index=0;
        
        @Override
        protected void findNext() {
            if(size>=index) return;
            nextValue = buffer[index];
            index++;
        }

    }
    
}
