
package science.unlicense.api.model.tree;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;

/**
 * Named nodes are node with a name and a value.
 * 
 * @author Johann Sorel
 */
public interface NamedNode extends Node{
   
    /**
     * Get node name.
     * 
     * @return CharSequence node name
     */
    Chars getName();
    
    /**
     * Set node name.
     * This may throw an exception if the name can not be changed.
     * 
     * @param name new node name
     */
    void setName(Chars name);
    
    /**
     * Get node value.
     * 
     * @return Object, can be null
     */
    Object getValue();
    
    /**
     * Set node value.
     * This may throw an exception if the value can not be changed.
     * 
     * @param value new node value
     */
    void setValue(Object value);
    
    /**
     * Get the first child node with given name.
     * 
     * @param name
     * @return NamedNode or null if not found
     */
    NamedNode getNamedChild(Chars name);
    
    /**
     * Get all child node with given name.
     * 
     * @param name
     * @return Sequence, never null but can be empty
     */
    Sequence getNamedChildren(Chars name);
    
}
