
package science.unlicense.api.number;

import science.unlicense.api.character.Chars;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.exception.UnimplementedException;

/**
 * 64bit decimal, also called Double.
 * Encoded in IEEE754
 * http://en.wikipedia.org/wiki/Double-precision_floating-point_format
 *
 * @author Johann Sorel
 */
public final class Float64 implements Number {

    public static final long MASK_FRACTION = 0x000fffffffffffffL;
    public static final long MASK_EXPONENT = 0x7ff0000000000000L;
    public static final long MASK_SIGN = 1l >> 63l;

    public static final int FRACTION_SIZE = 52;
    public static final int EXPONENT_SIZE = 11;
    public static final int EXP_BIAS = 1023;


    public static IEEE754 decompose(double value){
        return decompose(value, null);
    }

    public static IEEE754 decompose(double value, IEEE754 buffer){
        if(buffer == null) buffer = new IEEE754();
        final long bits = Double.doubleToLongBits(value);
        buffer.fraction = bits & MASK_FRACTION;
        buffer.exponent = (int) (((bits & MASK_EXPONENT) >> FRACTION_SIZE));
        buffer.sign = (int)(bits >> 63);

        return buffer;
    }


    /**
     * Algorithm from Bob Burger :
     * http://burgerrg.com/fp/index.html
     *
     *
     * @param candidate number to encode
     * @return Chars encoded number
     */
    public static Chars encode(double candidate) {

        //check special values : nan, inf
        if(Double.isNaN(candidate)){
            return new science.unlicense.api.character.Chars(new byte[]{'n','a','n'});
        }else if(Double.isInfinite(candidate)) {
            return (candidate<0)? new Chars(new byte[]{'-','i','n','f'})
                                : new Chars(new byte[]{'i','n','f'});
        }

        //TODO
        //throw new RuntimeException("TODO");
        return new Chars(Double.toString(candidate).replace(',', '.'));
    }


//    private static final int bias = 1023;
//    private static final int bitstoright = 52;
//    private static final int m1mask = 0xf;
//    private static final long hidden_bit = 0x100000;
//    private static final double float_radix = 2.147483648e9;
//
//    private static final int BIGSIZE  = 24;
//    private static final int MIN_E = -1074;
//    private static final int MAX_FIVE = 325;
//    private static final long B_P1 = (1l << 52);
//
//    private static int fixed(CharBuffer buf, double v, int prec) {
//        int f_n, i, d, n;
//
//        /* decompose float into sign, mantissa & exponent */
//        IEEE754 x = decompose(v);
//        boolean sign = x.sign == 0;
//        int e = x.exponent;
//        long f = x.fraction;
//        if (e != 0) {
//           e = e - EXP_BIAS - bitstoright;
//           f |= hidden_bit << 32;
//        } else if (f != 0) {
//           /* denormalized */
//           e = 1 - bias - bitstoright;
//        }
//
//        if (sign){
//            buf.append('-');
//        }
//        if (f == 0) {
//          for (i = prec; i > 0; i--){
//              buf.append('0');
//          }
//          buf.append('0');
//          return 0;
//        }
//
//        /* Compute the scaling factor estimate, k */
//        if (e > MIN_E)
//           k = estimate(e+52);
//        else {
//           Bigit y;
//
//           for (n = e+52, y = (Bigit)1 << 52; f < y; n--) y >>= 1;
//           k = estimate(n);
//        }
//
//        if (e >= 0)
//           f_n = e, s_n = 0;
//        else
//           f_n = 0, s_n = -e;
//
//        /* Scale it! */
//        if (k == 0) {
//           short_shift_left(f, f_n, &R);
//           one_shift_left(s_n, &S);
//           qr_shift = 1;
//        }
//        else if (k > 0) {
//           s_n += k;
//           if (f_n >= s_n)
//              f_n -= s_n, s_n = 0;
//           else
//              s_n -= f_n, f_n = 0;
//           short_shift_left(f, f_n, &R);
//           big_shift_left(&five[k-1], s_n, &S);
//           qr_shift = 0;
//        }
//        else {
//           s_n += k;
//           big_short_mul(&five[-k-1], f, &S);
//           big_shift_left(&S, f_n, &R);
//           one_shift_left(s_n, &S);
//           qr_shift = 1;
//        }
//
//        /* fixup */
//        if (big_comp(&R, &S) < 0) k--, mul10(&R);
//
//        if (qr_shift) {
//           sl = s_n / 64;
//           slr = s_n % 64;
//        }
//        else {
//           big_shift_left(&S, 1, &S2);
//           add_big(&S2, &S, &S3);
//           big_shift_left(&S2, 1, &S4);
//           add_big(&S4, &S, &S5);
//           add_big(&S4, &S2, &S6);
//           add_big(&S4, &S3, &S7);
//           big_shift_left(&S4, 1, &S8);
//           add_big(&S8, &S, &S9);
//        }
//
//        for (n = prec; ;) {
//
//           if (qr_shift) { /* Take advantage of the fact that S = (ash 1 s_n) */
//              if (R.l < sl)
//                 d = 0;
//              else if (R.l == sl) {
//                 Bigit *p;
//
//                 p = &R.d[sl];
//                 d = *p >> slr;
//                 *p &= ((Bigit)1 << slr) - 1;
//                 for (i = sl; (i > 0) && (*p == 0); i--) p--;
//                 R.l = i;
//              }
//              else {
//                 Bigit *p;
//
//                 p = &R.d[sl+1];
//                 d = *p << (64 - slr) | *(p-1) >> slr;
//                 p--;
//                 *p &= ((Bigit)1 << slr) - 1;
//                 for (i = sl; (i > 0) && (*p == 0); i--) p--;
//                 R.l = i;
//              }
//           }
//           else /* We need to do quotient-remainder */
//              d = qr();
//
//           *buf++ = d + '0';
//           if (--n == 0) break;
//           mul10(&R);
//        }
//
//        big_shift_left(&R, 1, &MM);
//        switch (big_comp(&MM, &S)) {
//         case -1: /* No rounding needed */
//           *buf = 0;
//           return k;
//         case 0: /* Exactly in the middle */
//           *buf++ = '5';
//           *buf = 0;
//           return k;
//         default:  /* Round up */
//           *buf-- = 0;
//           break;
//        }
//
//        for (n = prec; n > 0; n--) {
//           char c;
//           c = *buf;
//           if (c != '9') {
//              *buf = c+1;
//              return k;
//           }
//           *buf-- = '0';
//        }
//        *++buf = '1';
//        return k+1;
//     }




    public static double decode(Chars candidate) {
        return decode(candidate, 0, candidate.getCharLength());
    }

    /**
     *
     * @param candidate text de decode
     * @param fromIndex start offset
     * @param toIndex last character excluded,
     *          or use -1 to stop on sequence end or invalid character.
     * @return decoded value
     */
    public static double decode(Chars candidate, int fromIndex, int toIndex) {
        try{
            return decode(candidate.createIterator(fromIndex,toIndex),true);
        }catch(RuntimeException ex){
            throw new RuntimeException("Not an decimal : "+candidate);
        }
    }


    public static double decode(CharIterator ite, boolean strict) {
        if(!ite.hasNext()){
            throw new RuntimeException("Not an decimal");
        }

        boolean isNegative = false;
        boolean hasDecimals = false;
        boolean hasExpo = false;

        double number = 0;
        int dec = 1;
        int expo = 0;

        int c = ite.peekToUnicode();
        if(c == 43){ // +
            ite.skip();
        }else if(c == 45){ // -
            isNegative = true;
            ite.skip();
        }

        boolean requestDigit = true;
        while(ite.hasNext()){
            c = ite.peekToUnicode();
            if(requestDigit){
                //firt char must be a digit
                if(c > 47 && c < 58){ // digit
                    ite.skip();
                    c -= 48;
                    number = number*10 + c;
                    if (hasDecimals) {
                        dec *= 10;
                    }
                }else{
                    throw new RuntimeException("Not an decimal");
                }
                requestDigit = false;
                continue;
            }
            
            if(c == 46){ // .
                ite.skip();
                if(hasDecimals){
                    throw new RuntimeException("Not an decimal");
                }else{
                    hasDecimals = true;
                }
            }else if(c ==69 || c == 101){ // exponentiel E,e
                if(hasExpo){
                    throw new RuntimeException("Not an decimal");
                }else{
                    hasExpo = true;
                    ite.skip();
                    expo = Int32.decode(ite,false);
                    break;
                }
            }else if(c > 47 && c < 58){ // digit
                ite.skip();
                c -= 48;
                number = number*10 + c;
                if (hasDecimals) {
                    dec *= 10;
                }
            }else if(strict){
                throw new RuntimeException("Not an decimal");
            }else{
                //not valid, stop
                break;
            }
        }

        if(isNegative){
            number *= -1;
        }
        if(hasDecimals){
            number /= dec;
        }
        if(hasExpo){
            number *= Math.pow(10, expo);
        }

        return number;
    }

    public Arithmetic add(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic subtract(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic mult(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic divide(Arithmetic other) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic zero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isZero() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic one() {
        throw new UnimplementedException("Not supported yet.");
    }

    public boolean isOne() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Arithmetic pow(int n) {
        throw new UnimplementedException("Not supported yet.");
    }

    public int toInteger() {
        throw new UnimplementedException("Not supported yet.");
    }

    public long toLong() {
        throw new UnimplementedException("Not supported yet.");
    }

    public float toFloat() {
        throw new UnimplementedException("Not supported yet.");
    }

    public double toDouble() {
        throw new UnimplementedException("Not supported yet.");
    }

}