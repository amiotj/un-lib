package science.unlicense.api.model.tree;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.AbstractSequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.exception.InvalidArgumentException;


/**
 * Default node implementation.
 *
 * @author Johann Sorel
 */
public class DefaultNode extends AbstractNode implements EventListener{

    protected final Sequence children;

    private Object[] values;
    private int size;

    public DefaultNode(boolean allowChildren) {
        if(allowChildren){
            children = new TrackingSequence();
            values = new Object[1];
            size = 0;
        }else{
            children = Collections.emptySequence();
        }
    }

    public DefaultNode(Node[] children) {
        values = Arrays.copy(children, new Node[children.length]);
        size = children.length;
        this.children = new TrackingSequence();
    }

    public Sequence getChildren() {
        return children;
    }

    public boolean canHaveChildren() {
        return children instanceof TrackingSequence;
    }

    public Chars thisToChars(){
        return new Chars("DefaultNode");
    }

    public Class[] getEventClasses() {
        return new Class[]{
            PropertyMessage.class,
            NodeMessage.class
        };
    }

    /**
     * Receive child node events and transform them in TYPE_UPDATE events.
     */
    @Override
    public void receiveEvent(Event event) {
        final Object source = (Object) event.getSource();
        if(!(source instanceof Node)) return;
        final int index = Arrays.getFirstOccurenceIdentity(this.values, 0, size, source);
        if(index<0) return;
        sendNodeReplace(null,new Node[]{(Node)source}, index, event);
    }

    /**
     * Check if children collection contains the given node.
     * This is an made using identity test not equals.
     *
     * @param candidate
     * @return true if candidate is one of the children
     */
    protected final boolean containsChildIdentity(Object candidate){
        return Arrays.getFirstOccurenceIdentity(this.values, 0, size, candidate) >=0;
    }

    ////////////////////////////////////////////////////////////////////////////
    // collection methods //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    private void growIfNecessary(final int capacity) {
	if (capacity > values.length) {
	    int newCapacity = (values.length * 3)/2 + 1;
    	    if (newCapacity < capacity) {
                newCapacity = capacity;
            }
            values = Arrays.resize(values, newCapacity);
	}
    }

    protected Object getChildNode(int index) {
        if(index<0 || index>=size) throw new IndexOutOfBoundsException(""+index);
        return values[index];
    }

    protected boolean removeChildNode(int index) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        final Node removedValue = (Node) values[index];
        //remove listeners
        removedValue.removeEventListener(PropertyMessage.PREDICATE, this);
        removedValue.removeEventListener(NodeMessage.PREDICATE, this);

        if(index+1 < size){
            Arrays.copy(values, index+1, size-index-1, values, index);
        }
        size--;
        values[size] = null; //don't keep the reference
        if(hasListeners()) ((TrackingSequence)children).fireRemove(index, index, new Node[]{removedValue});
        return true;
    }

    protected void addChildren(int index, Object[] candidate) {
        if(!canHaveChildren()) throw new IllegalStateException("Node does not allow childrens.");
        growIfNecessary(size + candidate.length);

        for(int i=0;i<candidate.length;i++){
            if(candidate[i]==this){
                throw new InvalidArgumentException("Can not add self as children node.");
            }
            //add listeners
            ((Node)candidate[i]).addEventListener(PropertyMessage.PREDICATE, this);
            ((Node)candidate[i]).addEventListener(NodeMessage.PREDICATE, this);
        }

        for(int i=size-1;i>=index;i--){
            values[i+candidate.length] = values[i];
        }
        Arrays.copy(candidate, 0, candidate.length, values, index);
        size+=candidate.length;

        if(hasListeners()) ((TrackingSequence)children).fireAdd(index, index+candidate.length, Arrays.copy(candidate));
    }

    protected boolean removeChildren() {
        final Node[] childs = (Node[]) getChildren().toArray(Node.class);
        for(int i=0;i<childs.length;i++){
            childs[i].removeEventListener(PropertyMessage.PREDICATE, this);
            childs[i].removeEventListener(NodeMessage.PREDICATE, this);
        }
        values = new Object[1];
        size = 0;
        if(hasListeners()) ((TrackingSequence)children).fireRemove(0, childs.length, childs);
        return true;
    }

    protected void replaceChildren(Object[] items){
        Node[] old = (Node[]) getChildren().toArray(Node.class);
        if(Arrays.equals(old, items)) return;

        for(int i=0;i<old.length;i++){
            old[i].removeEventListener(PropertyMessage.PREDICATE, this);
            old[i].removeEventListener(NodeMessage.PREDICATE, this);
        }

        values = Arrays.copy(items);
        size = values.length;
        for(int i=0;i<values.length;i++){
            if(values[i]==this){
                throw new InvalidArgumentException("Can not add self as children node.");
            }
            //add listeners
            ((Node)values[i]).addEventListener(PropertyMessage.PREDICATE, this);
            ((Node)values[i]).addEventListener(NodeMessage.PREDICATE, this);
        }

        if(hasListeners()) ((TrackingSequence)this.children).
                fireReplaceAll(0, old.length,old, (Node[])items);
    }

    private class TrackingSequence extends AbstractSequence {

        public int getSize() {
            return size;
        }

        public Object get(int index) {
            return DefaultNode.this.getChildNode(index);
        }

        public boolean add(int index, Object value) {
            return addAll(index,new Node[]{(Node)value});
        }

        public boolean addAll(int index, Object[] values) {
            DefaultNode.this.addChildren(index,values);
            return true;
        }

        public boolean remove(int index) {
            return DefaultNode.this.removeChildNode(index);
        }

        public boolean removeAll() {
            return DefaultNode.this.removeChildren();
        }

        public void replaceAll(Object[] items) {
            DefaultNode.this.replaceChildren(items);
        }

        @Override
        public void replaceAll(Collection items) {
            final Node[] nodes = new Node[items.getSize()];
            Collections.copy(items, nodes, 0);
            DefaultNode.this.replaceChildren(nodes);
        }

        public Object[] toArray() {
            return Arrays.copy(values,0,size);
        }

        @Override
        protected void fireAdd(int startIndex, int endIndex, Object[] news) {
            super.fireAdd(startIndex, endIndex, news);
            sendNodeAdd((Node[]) news, startIndex);
        }

        @Override
        protected void fireRemove(int startIndex, int endIndex, Object[] olds) {
            super.fireRemove(startIndex, endIndex, olds);
            sendNodeRemove((Node[]) olds, startIndex);
        }

        @Override
        protected void fireReplace(int startIndex, int endIndex, Object[] olds, Object[] news) {
            super.fireReplace(startIndex, endIndex, olds, news);
            sendNodeReplace((Node[])olds, (Node[])news, startIndex, null);
        }

        @Override
        protected void fireReplaceAll(int startIndex, int endIndex, Object[] olds, Object[] news) {
            super.fireReplaceAll(startIndex, endIndex, olds, news);
            sendNodeReplaceAll(startIndex, endIndex, (Node[])olds, (Node[])news);
        }

        protected EventManager getEventManager(boolean create) {
            return DefaultNode.this.getEventManager(create);
        }

    }

}
