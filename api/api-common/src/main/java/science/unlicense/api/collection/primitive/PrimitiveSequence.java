
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public interface PrimitiveSequence extends Sequence {
    
    PrimitiveSequence moveTo(int position);
    
    boolean removeNext(int nbElement);
    
}
