
package science.unlicense.api.buffer;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDoubleBuffer extends AbstractBuffer{

    private static final long[] MASKS = {
        0xFF00000000000000l,
        0x00FF000000000000l,
        0x0000FF0000000000l,
        0x000000FF00000000l,
        0x00000000FF000000l,
        0x0000000000FF0000l,
        0x000000000000FF00l,
        0x00000000000000FFl};
    private static final int[] OFFSET = {
        56,48,40,32,24,16,8,0};
    
    private final double[] buffer;

    public DefaultDoubleBuffer(double[] buffer) {
        super(Primitive.TYPE_DOUBLE, NumberEncoding.BIG_ENDIAN, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }
    
    public boolean isWritable() {
        return true;
    }

    public Object getBackEnd() {
        return buffer;
    }

    public long getBufferByteSize() {
        return buffer.length*8;
    }

    public byte readByte(long offset) {
        final int index = (int) (offset/8);
        final int mod = (int) (offset%8);
        final double f = buffer[index];
        final long i = Double.doubleToRawLongBits(f);
        return (byte)( (i & MASKS[mod]) >> OFFSET[mod] );
    }

    public double readDouble(long offset) {
        if(offset%8==0){
            return buffer[(int)(offset/8)];
        }else{
            return super.readDouble(offset);
        }
    }

    public void readDouble(double[] array, int arrayOffset, int length, long offset) {
        if(offset%8==0){
            Arrays.copy(buffer, (int)(offset/8), length, array, arrayOffset);
        }else{
            super.readDouble(array, arrayOffset, length, offset);
        }
    }
    
    public void writeByte(byte value, long offset) {
        final int index = (int) (offset/8);
        final int mod = (int) (offset%8);
        final double f = buffer[index];
        long i = Double.doubleToRawLongBits(f);
        i &= ~MASKS[mod];
        i |= (long)(value&0xFF) << OFFSET[mod];
        buffer[index] = Double.longBitsToDouble(i);
    }

    public void writeDouble(double value, long offset) {
        if(offset%8==0){
            buffer[(int)(offset/8)] = value;
        }else{
            super.writeDouble(value, offset);
        }
    }

    public void writeDouble(double[] array, int arrayOffset, int length, long offset) {
        if(offset%8==0){
            Arrays.copy(array, arrayOffset, length, buffer, (int)(offset/8));
        }else{
            super.writeDouble(array, arrayOffset, length, offset);
        }
    }

    public Buffer copy() {
        return new DefaultDoubleBuffer(Arrays.copy(buffer));
    }

}
