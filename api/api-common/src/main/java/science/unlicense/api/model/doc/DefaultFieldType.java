
package science.unlicense.api.model.doc;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFieldType extends CObject implements FieldType {

    protected final Chars id;
    protected final CharArray title;
    protected final CharArray description;
    protected final int minOcc;
    protected final int maxOcc;
    protected final Class valueClass;
    protected final Object defaultValue;
    protected final DocumentType refType;
    protected final Dictionary attributes = new HashDictionary(Hasher.DEFAULT, 0);
    protected final Sequence constraints;

    public DefaultFieldType(Chars id, CharArray title, CharArray description, int minOcc, int maxOcc,
            Class valueClass, Object defaultValue, DocumentType refType, Dictionary attributes) {
        this(id,title,description,minOcc,maxOcc,valueClass,defaultValue,refType,attributes,null);
    }
    public DefaultFieldType(Chars id, CharArray title, CharArray description, int minOcc, int maxOcc, 
            Class valueClass, Object defaultValue, DocumentType refType, Dictionary attributes, Sequence constraints) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.minOcc = minOcc;
        this.maxOcc = maxOcc;
        this.valueClass = valueClass;
        this.defaultValue = defaultValue;
        this.refType = refType;
        if(attributes!=null) this.attributes.addAll(attributes);
        this.constraints = constraints==null ? Collections.emptySequence() : Collections.readOnlySequence(new ArraySequence(constraints));
        if(defaultValue!=null && !(valueClass.isInstance(defaultValue))){
            throw new InvalidArgumentException("Unvalid default value type, value ("+defaultValue.getClass()+")is not of compatible with value class ("+valueClass+").");
        }
    }
    
    public Chars getId() {
        return id;
    }

    public CharArray getTitle() {
        return title;
    }

    public CharArray getDescription() {
        return description;
    }

    public int getMinOccurences() {
        return minOcc;
    }

    public int getMaxOccurences() {
        return maxOcc;
    }
    
    public Class getValueClass() {
        return valueClass;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public DocumentType getReferenceType() {
        return refType;
    }

    public Dictionary getAttributes() {
        return attributes;
    }

    public Sequence getConstraints() {
        return constraints;
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        if(title!=null) cb.append(title);
        if(id!=null) cb.append('(').append(id).append(')');
        cb.append(" [").append(""+minOcc).append("...").append(""+maxOcc).append("] ");
        cb.append(valueClass.getSimpleName());
        if(description!=null) cb.append("  :  ").append(description);
        if(refType!=null) cb.append("  -->  ").append(refType.getId());
        return cb.toChars();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultFieldType other = (DefaultFieldType) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.title != other.title && (this.title == null || !this.title.equals(other.title))) {
            return false;
        }
        if (this.description != other.description && (this.description == null || !this.description.equals(other.description))) {
            return false;
        }
        if (this.minOcc != other.minOcc) {
            return false;
        }
        if (this.maxOcc != other.maxOcc) {
            return false;
        }
        if (this.valueClass != other.valueClass && (this.valueClass == null || !this.valueClass.equals(other.valueClass))) {
            return false;
        }
        if (this.defaultValue != other.defaultValue && (this.defaultValue == null || !this.defaultValue.equals(other.defaultValue))) {
            return false;
        }
        if (this.refType != other.refType && (this.refType == null || !this.refType.equals(other.refType))) {
            return false;
        }
        return attributes.equals(other.attributes);
    }

    public int getHash() {
        int hash = 3;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 89 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 89 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 89 * hash + this.minOcc;
        hash = 89 * hash + this.maxOcc;
        hash = 89 * hash + (this.valueClass != null ? this.valueClass.hashCode() : 0);
        hash = 89 * hash + (this.defaultValue != null ? this.defaultValue.hashCode() : 0);
        hash = 89 * hash + (this.refType != null ? this.refType.hashCode() : 0);
        return hash;
    }

}
