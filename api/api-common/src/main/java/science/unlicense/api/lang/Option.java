
package science.unlicense.api.lang;

/**
 * An optional value.
 *
 * @author Yann D'Isanto
 */
public final class Option {

    /**
     * The undefined optional value
     */
    public static final Option NONE = new Option(null);

    /**
     * the value.
     */
    private final Object value;

    /**
     * Create an optional value.
     * @param value the value
     */
    private Option(final Object value) {
        this.value = value;
    }

    public Object get() {
        return value;
    }

    /**
     * @return true if this value is defined.
     */
    public boolean isDefined() {
        return value != null;
    }

    /**
     * @return true is this value is empty (not defined).
     */
    public boolean isEmpty() {
        return value == null;
    }

    /**
     * @param defaultValue the default value to return if not defined.
     * @return this value if defined, the specified default value otherwise.
     */
    public Object getOrElse(Object defaultValue) {
        return isDefined() ? value : defaultValue;
    }

    /**
     * Creates an optional value.
     * @param value the value.
     * @return an optional value, NONE if value is null.
     */
    public static Option some(Object value) {
        return value != null ? new Option(value) : NONE;
    }

    /**
     * @return the NONE optional value (value is null).
     */
    public static Option none() {
        return NONE;
    }
}
