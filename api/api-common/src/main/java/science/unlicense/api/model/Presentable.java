
package science.unlicense.api.model;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface Presentable {

    Chars getId();
    
    CharArray getTitle();

    CharArray getDescription();
    
}
