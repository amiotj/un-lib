
package science.unlicense.api.collection;

/**
 *
 * @author Johann Sorel
 */
class OffsetIterator extends AbstractIterator {

    private final Iterator base;
    private int remaining;

    public OffsetIterator(Iterator base, int remaining) {
        this.base = base;
        this.remaining = remaining;
    }

    @Override
    protected void findNext() {
        for (; remaining > 0; remaining--) {
            if (base.hasNext()) {
                base.next();
            } else {
                remaining = 0;
            }
        }
        if (base.hasNext()) {
            nextValue = base.next();
        }
    }

}
