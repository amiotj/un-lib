

package science.unlicense.api.collection;

/**
 *
 * @author Johann Sorel
 */
public class ArrayStack extends ArraySequence implements Stack {

    public Object lookStart() {
        if(isEmpty()){
            return null;
        }else{
            return get(0);
        }
    }

    public Object lookEnd() {
        if(isEmpty()){
            return null;
        }else{
            return get(getSize()-1);
        }
    }

    public Object pickStart() {
        if(isEmpty()){
            return null;
        }else{
            Object o = get(0);
            remove(0);
            return o;
        }
    }

    public Object pickEnd() {
        if(isEmpty()){
            return null;
        }else{
            Object o = get(getSize()-1);
            remove(getSize()-1);
            return o;
        }
    }
    
}
