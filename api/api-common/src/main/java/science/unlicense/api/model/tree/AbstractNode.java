
package science.unlicense.api.model.tree;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;

/**
 * Abstract node class.
 * Provides :
 * - default implementations for add methods falling back on addChidlren.
 * - event manager and methods
 * - clean toChars
 *
 * @author Johann Sorel
 */
public abstract class AbstractNode extends AbstractEventSource implements Node{

    public Object accept(NodeVisitor visitor, Object context) {
        return visitor.visit(this, context);
    }

    public Chars thisToChars(){
        return new Chars("AbstractNode");
    }

    public Chars toChars() {
        return toCharsTree(3);
    }

    public Chars toCharsTree(int depth){
        return Nodes.toCharsTree(this, thisToChars(), depth);
    }
    
    protected void sendNodeAdd(Node[] added, int start){
        if(hasListeners()){
            getEventManager().sendEvent(new Event(this,new NodeMessage(
                NodeMessage.TYPE_ADD, start, start+added.length,
                    null, added)));
        }
    }

    protected void sendNodeRemove(Node[] removed, int start){
        if(hasListeners()){
            getEventManager().sendEvent(new Event(this,new NodeMessage(
                NodeMessage.TYPE_REMOVE, start, start+removed.length, removed, null)));
        }
    }

    protected void sendNodeReplace(Node[] olds, Node[] updated, int start, Event subEvent){
        if(hasListeners()){
            getEventManager().sendEvent(
                    new Event(this,
                            new NodeMessage(NodeMessage.TYPE_REPLACE, start, start+updated.length, olds, updated),
                            subEvent));
        }
    }

    /**
     * Send a node replace all event.
     * 
     * @param startIndex first index of modified element, -1 if unknowed
     * @param endIndex last index of modified element, -1 if unknowed
     * @param olds values before replacement
     * @param news values after replacement
     */
    protected void sendNodeReplaceAll(int startIndex, int endIndex, Node[] olds, Node[] news){
        if(hasListeners()){
            getEventManager().sendEvent(new Event(this,new NodeMessage(NodeMessage.TYPE_REPLACEALL, startIndex, endIndex,
                    olds, news)));
        }
    }
    
}
