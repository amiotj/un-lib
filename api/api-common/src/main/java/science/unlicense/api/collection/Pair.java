
package science.unlicense.api.collection;

import science.unlicense.api.CObjects;

/**
 * A pair is a group of 2 values.
 * 
 * @author Johann Sorel
 */
public class Pair {
   
    protected Object value1;
    protected Object value2;

    public Pair(Object value1, Object value2) {
        this.value1 = value1;
        this.value2 = value2;
    }
    
    public Pair(Pair pair) {
        this.value1 = pair.value1;
        this.value2 = pair.value2;
    }

    public Object getValue1() {
        return value1;
    }
    
    public Object getValue2() {
        return value2;
    }
    
    public boolean equals(final Object candidate) {
        if(!(candidate instanceof Pair)){
            return false;
        }
        final Pair other = (Pair) candidate;
        return CObjects.equals(value1, other.getValue1()) && 
               CObjects.equals(value2, other.getValue2());
    }

    public int hashCode() {
        return ((value1 == null) ? 0 : value1.hashCode())
             ^ ((value2 == null) ? 0 : value2.hashCode());
    }

}
