
package science.unlicense.api.model.doc;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Sequence;

/**
 * Builder for Document types.
 *
 * @author Johann Sorel
 */
public class DocumentTypeBuilder {

    protected Chars id;
    protected CharArray title;
    protected CharArray description;
    protected boolean strict = true;
    protected final Sequence fields = new ArraySequence();
    protected final Sequence attributes = new ArraySequence();
    protected final Sequence parents = new ArraySequence();

    public DocumentTypeBuilder(){
    }

    public DocumentTypeBuilder(Chars id){
        this.id = id;
    }

    public DocumentTypeBuilder id(Chars id) {
        this.id = id;
        return this;
    }

    public DocumentTypeBuilder title(CharArray title) {
        this.title = title;
        return this;
    }

    public DocumentTypeBuilder description(CharArray description) {
        this.description = description;
        return this;
    }

    public DocumentTypeBuilder strict(boolean strict) {
        this.strict = strict;
        return this;
    }

    public Sequence getFields() {
        return fields;
    }

    public Sequence getAttributes() {
        return attributes;
    }

    public Sequence getParents() {
        return parents;
    }

    public FieldTypeBuilder addField(Chars id){
        final FieldTypeBuilder ftb = new FieldTypeBuilder(id);
        fields.add(ftb);
        return ftb;
    }

    public FieldTypeBuilder addField(FieldTypeBuilder builder){
        fields.add(builder);
        return builder;
    }

    public FieldTypeBuilder addAttribute(Chars id, Object value){
        final FieldTypeBuilder ftb = new FieldTypeBuilder(id);
        attributes.add(ftb);
        return ftb;
    }

    public DocumentTypeBuilder addParent(DocumentType parent){
        parents.add(parent);
        return this;
    }

    public DocumentType build(){

        final FieldType[] fields = new FieldType[this.fields.getSize()];
        for (int i=0;i<fields.length;i++) {
            fields[i] = ((FieldTypeBuilder)this.fields.get(i)).build();
        }

        final Dictionary attributes = new HashDictionary();
        for (int i=0,n=this.attributes.getSize();i<n;i++) {
            final FieldType ft = ((FieldTypeBuilder)this.attributes.get(i)).build();
            attributes.add(ft.getId(), ft);
        }

        final DocumentType[] parents = new DocumentType[this.parents.getSize()];
        for (int i=0;i<parents.length;i++) {
            parents[i] = (DocumentType) this.parents.get(i);
        }

        return new DefaultDocumentType(id, title, description, strict, fields, attributes, parents);
    }

}
