
package science.unlicense.api.character;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class BacktrackCharIterator extends AbstractCharIterator {
    
    private final CharIterator in;
    private final Sequence buffer = new ArraySequence();
    private int mark = -1;

    public BacktrackCharIterator(CharIterator base) {
        super(base.getEncoding());
        this.in = base;
    }
    
    public void mark(){
        if(mark>= buffer.getSize()){
            buffer.removeAll();
        }else if(mark>0){
            //current position might not be the end
            for(int i=0;i<mark;i++){
                buffer.remove(0);
            }
        }
        mark = 0;
    }

    public void rewind(){
        mark = 0;
    }
    
    public void clear(){
        if(!buffer.isEmpty()){
            throw new RuntimeException("Buffer is not empty");
        }
        mark = -1;
    }
    
    private byte[] intNext(){
        //we have move backward, data is in the buffer
        if(mark<buffer.getSize()){
            return (byte[]) buffer.get(mark);
        }else if(!in.hasNext()){
            throw new RuntimeException("No more characters.");
        }else{
            final byte[] b = in.nextToBytes();
            buffer.add(b);
            return b;
        }
    }
    
    @Override
    public byte[] nextToBytes() {
        if(mark>=0){
            final byte[] b = intNext();
            mark++;
            return b;
        }
        return in.nextToBytes();
    }

    @Override
    public int[] nextBulk(ByteSequence bytebuffer) {
        if(mark>=0){
            final byte[] b = intNext();
            mark++;
            bytebuffer.put(b);
            return new int[]{encoding.toUnicode(b),b.length};
        }
        return in.nextBulk(bytebuffer);
    }

    @Override
    public void skip() {
        if(mark>=0){
            final byte[] b = intNext();
            mark++;
        }else{
            in.skip();
        }
    }

    @Override
    public byte[] peekToBytes() {
        if(mark>=0){
            final byte[] b = intNext();
            return b;
        }
        return in.peekToBytes();
    }

    @Override
    public void peekToBuffer(ByteSequence buffer) {
        if(mark>=0){
            final byte[] b = intNext();
            buffer.put(b);
        }
        in.peekToBuffer(buffer);
    }

    @Override
    public int peekToUnicode() {
        if(mark>=0){
            final byte[] b = intNext();
            return encoding.toUnicode(b);
        }
        return in.peekToUnicode();
    }

    @Override
    public int[] peekBulk(ByteSequence bytebuffer) {
        if(mark>=0){
            final byte[] b = intNext();
            bytebuffer.put(b);
            return new int[]{encoding.toUnicode(b),b.length};
        }
        return in.peekBulk(bytebuffer);
    }

    @Override
    public boolean nextEquals(byte[] cdata) {
        if(mark>=0){
            final byte[] b = intNext();
            return Arrays.equals(b, cdata);
        }
        return in.nextEquals(cdata);
    }

    @Override
    public boolean hasNext() {
        if(mark>=0 && mark<buffer.getSize()){
            return true;
        }
        return in.hasNext();
    }
    
}
