
package science.unlicense.api.model.doc;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class FieldTypeBuilder {

    protected Chars id;
    protected CharArray title;
    protected CharArray description;
    protected Class valueClass = Object.class;
    protected Object defaultValue = null;
    protected int minOcc = 1;
    protected int maxOcc = 1;
    protected DocumentType ref = null;
    protected Dictionary attributes = null;
    protected Sequence constraints = null;

    public FieldTypeBuilder(){
    }

    public FieldTypeBuilder(Chars id){
        this.id = id;
    }

    public FieldTypeBuilder id(Chars id) {
        this.id = id;
        return this;
    }

    public FieldTypeBuilder title(CharArray title) {
        this.title = title;
        return this;
    }

    public FieldTypeBuilder description(CharArray description) {
        this.description = description;
        return this;
    }

    public FieldTypeBuilder valueClass(Class valueClass) {
        this.valueClass = valueClass;
        return this;
    }

    public FieldTypeBuilder defaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public FieldTypeBuilder minOcc(int minOcc) {
        this.minOcc = minOcc;
        return this;
    }

    public FieldTypeBuilder maxOcc(int maxOcc) {
        this.maxOcc = maxOcc;
        return this;
    }

    public FieldTypeBuilder ref(DocumentType ref) {
        this.ref = ref;
        return this;
    }

    public FieldTypeBuilder attributes(Dictionary attributes) {
        this.attributes = attributes;
        return this;
    }

    public FieldTypeBuilder constraints(Sequence constraints) {
        this.constraints = constraints;
        return this;
    }
    
    public FieldType build() {
        return new DefaultFieldType(id, title, description, minOcc, maxOcc, 
                valueClass, defaultValue, ref, attributes, constraints);
    }


}
