
package science.unlicense.api.predicate;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 * A Constant expression.
 * Returning the same value whatever object or context.
 *
 * @author Johann Sorel
 */
public class Constant extends CObject implements Expression{

    private final Object value;

    /**
     * New constant expression.
     * @param value constant value
     */
    public Constant(Object value) {
        this.value = value;
    }

    /**
     * Get constant value.
     * @return Object, can be null
     */
    public Object getValue() {
        return value;
    }

    public Object evaluate(Object candidate) {
        return value;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Constant other = (Constant) obj;
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        return true;
    }

    public Chars toChars() {
        return new Chars("Constant : "+value);
    }

}
