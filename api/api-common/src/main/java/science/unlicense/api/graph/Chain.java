
package science.unlicense.api.graph;

import science.unlicense.api.collection.Sequence;

/**
 * A Chain is a sequence of edges.
 * 
 * @author Johann Sorel
 */
public interface Chain {
   
    /**
     * Get the sequence of edges in this chain.
     * @return Sequence
     */
    Sequence getEdges();
    
    /**
     * Length is the number of edges in this chain.
     * @return int
     */
    int getLength();
    
    /**
     * A Chain is elementary when it uses nodes only once.
     * @return true is chain is elementary.
     */
    boolean isElementary();
    
    /**
     * A Chain is simple when it uses edges only once.
     * @return true is chain is simple.
     */
    boolean isSimple();
    
    /**
     * A Chain is a path when vertex2 of all edges is equals to vertex1 of their next edge.
     * @return true is chain is a path.
     */
    boolean isPath();
    
    /**
     * A Chain is a circuit when it's a path and the starting and ending vertices are the same.
     * @return true is chain is a circuit.
     */
    boolean isCircuit();
}
