
package science.unlicense.api.model.tree;

import science.unlicense.api.Sorter;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class ViewNode extends AbstractNode {

    private final Node subNode;
    private final Predicate predicate;
    private final Sorter sorter;

    private boolean cacheChildren = false;
    private Sequence cache = null;

    public ViewNode(Node subNode, Predicate predicate) {
        this(subNode,predicate,null);
    }

    public ViewNode(Node subNode, Predicate predicate, Sorter sorter) {
        this.subNode = subNode;
        this.predicate = predicate;
        this.sorter = sorter;
    }

    /**
     * Indicate to cache or not children list after the first time it is requested.
     *
     * @param cacheChildren true to cache filtered childrens
     */
    public void setCacheChildren(boolean cacheChildren) {
        this.cacheChildren = cacheChildren;
    }

    public boolean isCacheChildren() {
        return cacheChildren;
    }

    public void clearCache(){
        cache = null;
    }

    public boolean canHaveChildren() {
        return subNode.canHaveChildren();
    }

    public Node getSubNode() {
        return subNode;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public Sequence getChildren() {
        if(cache == null){
            final Sequence filtered = new ArraySequence();
            final Iterator ite = Collections.filterCollection(subNode.getChildren(), predicate).createIterator();
            while(ite.hasNext()){
                final ViewNode vn = new ViewNode((Node)ite.next(),predicate, sorter);
                vn.setCacheChildren(cacheChildren);
                filtered.add(vn);
            }
            if(sorter!=null){
                Collections.sort(filtered, sorter);
            }

            if (cacheChildren) {
                cache = Collections.readOnlySequence(filtered);
                return cache;
            } else {
               return Collections.readOnlySequence(filtered);
            }
        }
        return cache;
    }

    public Class[] getEventClasses() {
        return subNode.getEventClasses();
    }

    public Chars toChars() {
        return subNode.toChars();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if(obj instanceof Node){
            //test the real node
            Node ref1 = unWrap(this);
            Node ref2 = unWrap((Node)obj);
            return ref1.equals(ref2);
        }
        return true;
    }

    /**
     * Remove all views from the original node.
     * @param node node to unwwrap
     * @return base wrapped node
     */
    public static Node unWrap(Node node){
        if(node instanceof ViewNode){
            return unWrap( ((ViewNode)node).getSubNode());
        }
        return node;
    }

}
