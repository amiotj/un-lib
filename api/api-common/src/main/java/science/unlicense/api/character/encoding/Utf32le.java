
package science.unlicense.api.character.encoding;

import science.unlicense.api.character.AbstractCharEncoding;

/**
 *
 * @author Johann Sorel
 */
public class Utf32le extends AbstractCharEncoding{
    
    public Utf32le() {
        super(new byte[]{'U','T','F','-','3','2','L','E'},true,4);
    }

    public int charlength(byte[] array, int offset) {
        return maxNbByte;
    }

    public int length(byte[] array) {
        return array.length/maxNbByte;
    }

    public void toUnicode(byte[] array, int[] codePoint, int offset) {
        codePoint[0] = (array[offset+3] & 0xff) << 24
                | (array[offset+2] & 0xff) << 16
                | (array[offset+1] & 0xff) <<  8
                | (array[offset+0] & 0xff) <<  0 ;
        codePoint[1] = maxNbByte;
    }

    public byte[] toBytes(int value) {
        final byte[] buffer = new byte[4];
        buffer[3] = (byte) ((value >> 24) & 0xff);
        buffer[2] = (byte) ((value >> 16) & 0xff);
        buffer[1] = (byte) ((value >>  8) & 0xff);
        buffer[0] = (byte) ((value >>  0) & 0xff);
        return buffer;
    }

    public void toBytes(int[] codePoint, byte[] array, int offset) {
        final int value = codePoint[0];
        array[offset+3] = (byte) ((value >> 24) & 0xff);
        array[offset+2] = (byte) ((value >> 16) & 0xff);
        array[offset+1] = (byte) ((value >>  8) & 0xff);
        array[offset+0] = (byte) ((value >>  0) & 0xff);
        codePoint[1] = maxNbByte;
    }

    public boolean isComplete(byte[] array, int offset, int length) {
        return length == 4;
    }

}
