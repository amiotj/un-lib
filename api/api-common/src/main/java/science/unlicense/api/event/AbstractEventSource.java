

package science.unlicense.api.event;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public class AbstractEventSource extends CObject implements EventSource {

    private final Object lock = new Object();
    private EventManager eventManager;
    
    public Class[] getEventClasses() {
        return new Class[0];
    }

    /**
     * Get property for given name.
     * 
     * @param name
     * @return Property
     */
    public Property getProperty(Chars name){
        return new DefaultProperty(this, name);
    }
    
    protected EventManager getEventManager() {
        return getEventManager(true);
    }
    
    protected EventManager getEventManager(boolean create) {
        synchronized (lock){
            if(eventManager==null && create) eventManager = new EventManager();
        }
        return eventManager;
    }
    
    protected boolean hasListeners(){
        final EventManager manager = getEventManager(false);
        return manager!=null && manager.hasListeners();
    }

    /**
     * Send a property event to listeners.
     *
     * @param source the event source
     * @param name name of the changed property
     * @param oldValue property old value
     * @param newValue property new value
     */
    protected void sendPropertyEvent(EventSource source, Chars name, Object oldValue, Object newValue){
        final EventManager manager = getEventManager(false);
        if(manager!=null){
            manager.sendPropertyEvent(source, name, oldValue, newValue);
        }
    }

    public EventListener[] getListeners(Predicate predicate) {
        final EventManager manager = getEventManager(false);
        if(manager==null) return EventManager.EMPTY_EVENTLISTENER_ARRAY;
        return manager.getListeners(predicate);
    }
    
    public void addEventListener(Predicate predicate, EventListener listener) {
        getEventManager().addEventListener(predicate, listener);
    }

    public void removeEventListener(Predicate predicate, EventListener listener) {
        final EventManager manager = getEventManager(false);
        if(manager==null) return;
        manager.removeEventListener(predicate, listener);
    }
    
}
