
package science.unlicense.api.graph;

import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Set;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractChain implements Chain{

    public int getLength() {
        return getEdges().getSize();
    }

    public boolean isElementary() {
        final Set passed = new HashSet();

        final Iterator ite = getEdges().createIterator();
        Edge previous = null;
        while(ite.hasNext()){
            final Edge edge = (Edge) ite.next();
            if(edge.isLoop()){
                //loop
                return false;
            }
            if(previous != null){
                int adj = previous.getAdjacentVertices(edge);
                if(adj==0){
                    throw new InvalidArgumentException("chain edges do not share a vertex");
                }else if(adj==1){
                    if(passed.contains(edge.getVertex2())){
                        //node already visited
                        return false;
                    }
                }else if(adj==2){
                    if(passed.contains(edge.getVertex1())){
                        //node already visited
                        return false;
                    }
                }else if(adj>2){
                    //loop
                    return false;
                }
            }
            passed.add(edge.getVertex1());
            passed.add(edge.getVertex2());
            previous = edge;
        }

        return true;
    }

    public boolean isSimple() {
        final Set passed = new HashSet();
        final Iterator ite = getEdges().createIterator();
        while(ite.hasNext()){
            final Edge edge = (Edge) ite.next();
            if(passed.contains(edge)){
                return false;
            }
            passed.add(edge);
        }
        return true;
    }

    public boolean isPath() {
        Vertex vertex = null;
        final Iterator ite = getEdges().createIterator();
        while(ite.hasNext()){
            final Edge edge = (Edge) ite.next();
            if(vertex == null){
                vertex = edge.getVertex2();
            }else if(!vertex.equals(edge.getVertex1())){
                return false;
            }
        }
        return true;
    }

    public boolean isCircuit() {
        if(!isPath()){
            return false;
        }
        final Vertex start = ((Edge)getEdges().get(0)).getVertex1();
        final Vertex end = ((Edge)getEdges().get(getEdges().getSize()-1)).getVertex2();
        return start.equals(end);
    }


}
