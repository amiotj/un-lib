package science.unlicense.api.character;

import science.unlicense.api.collection.primitive.ByteSequence;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractCharEncoding implements CharEncoding {

    protected final Chars name;
    protected final boolean isFixedSize;
    protected final int maxNbByte;

    public AbstractCharEncoding(byte[] bname, boolean isFixedSize, int maxNbByte) {
        this.name = new Chars(bname);
        this.isFixedSize = isFixedSize;
        this.maxNbByte = maxNbByte;
    }

    public Chars getName() {
        return name;
    }

    public int length(byte[] array) {
        int l = 0;
        int boffset = 0;
        while (boffset < array.length) {
            boffset += charlength(array, boffset);
            l++;
        }
        return l;
    }

    public int toUnicode(byte[] array) {
        final int[] buffer = new int[2];
        toUnicode(array, buffer, 0);
        return buffer[0];
    }

    public byte[] toBytes(int[] codePoints) {
        final ByteSequence buffer = new ByteSequence();
        for(int i=0;i<codePoints.length;i++){
            buffer.put(toBytes(codePoints[i]));
        }
        return buffer.toArrayByte();
    }
}
