
package science.unlicense.api.event;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.predicate.VariableSyncException;
import science.unlicense.api.predicate.Variables;

/**
 * Default property implementation based on java reflection capabilities.
 * 
 * @author Johann Sorel
 */
public class DefaultProperty extends CObject implements Property {
    
    private final WeakReference<EventSource> source;
    private final Chars name;
    private final Class valueClass;

    public DefaultProperty(EventSource source, Chars name) {
        this.source = new WeakReference<EventSource>(source);
        this.name = name;

        try {
            Class c = BeanUtils.getReadMethod(source.getClass(), name).getReturnType();
            //remove any autoboxing
            c = Properties.getBoxingClass(c);
            valueClass = c;

        } catch (InvalidArgumentException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    public EventSource getHolder() {
        return source.get();
    }
    
    EventSource getHolderError() {
        EventSource holder = source.get();
        if(holder==null) throw new IllegalStateException("Property source object has been garbage collected");
        return holder;
    }

    public Chars getName() {
        return name;
    }

    public Class getValueClass(){
        return valueClass;
    }
    
    public Object getValue(){
        final EventSource holder = getHolderError();
        try {
            return BeanUtils.getReadMethod(holder.getClass(), name).invoke(holder);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvalidArgumentException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }
    
    public void setValue(Object value){
        EventSource holder = source.get();
        if(holder==null) return;
        try {
            BeanUtils.getWriteMethod(holder.getClass(), name).invoke(holder, value);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvalidArgumentException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }
    
    public boolean isReadable(){
        final EventSource holder = getHolderError();
        return BeanUtils.getReadMethod(source.getClass(), name) != null;
    }
    
    public boolean isWritable(){
        final EventSource holder = getHolderError();
        return BeanUtils.getWriteMethod(holder.getClass(), name) != null;
    }
        
    public void sync(Variable var) {
        sync(var, false);
    }

    public void sync(Variable var, boolean readOnly) {
        final Variables.VarSync sync = getVarSync();
        if(sync!=null) throw new VariableSyncException("Variable is already synchronized");
        Variables.sync(this, var, readOnly);
    }
    
    public void unsync() {
        final Variables.VarSync sync = getVarSync();
        if(sync!=null) sync.release();
    }
    
    private Variables.VarSync getVarSync(){
        final EventSource holder = getHolderError();
        final EventListener[] listeners = holder.getListeners(new PropertyPredicate(name));
        if(listeners.length==0) return null;
        for(int i=0;i<listeners.length;i++){
            if(listeners[i] instanceof Variables.VarSync){
                final Variables.VarSync sync = ((Variables.VarSync)listeners[i]);
                if(sync.getVar1().equals(this)){
                    return sync;
                }
            }
        }
        return null;
    }
     
    public void addListener(EventListener listener){
        addEventListener(new PropertyPredicate(name), listener);
    }
    
    public void removeListener(EventListener listener){
        final EventSource holder = source.get();
        if(holder!=null) holder.removeEventListener(new PropertyPredicate(name), listener);
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    public void addEventListener(Predicate predicate, EventListener listener) {
        getHolderError().addEventListener(predicate, listener);
    }

    public void removeEventListener(Predicate predicate, EventListener listener) {
        final EventSource holder = source.get();
        if(holder!=null) holder.removeEventListener(predicate, listener);
    }

    public EventListener[] getListeners(Predicate predicate) {
        return getHolderError().getListeners(predicate);
    }

    public Chars toChars() {
        return new Chars("Property ").concat(name);
    }
    
}
