
package science.unlicense.api.buffer;

import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractCursor implements Cursor {

    protected NumberEncoding encoding;

    public AbstractCursor(NumberEncoding encoding) {
        this.encoding = encoding;
    }

    public NumberEncoding getEncoding() {
        return encoding;
    }
    
}