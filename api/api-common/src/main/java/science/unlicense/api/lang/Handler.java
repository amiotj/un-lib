
package science.unlicense.api.lang;


/**
 *
 * @author Johann Sorel
 */
public final class Handler {
    
    /**
     * In handler doing nothing.
     */
    public static final SafeIn NOOP_IN = new SafeIn() {
        public void perform(Object in) {}
    };

    /**
     * Void handler doing nothing.
     */
    public static final SafeVoid NOOP_VOID = new SafeVoid() {
        public void perform() {}
    };

    /**
     * Out handler returning a null value.
     */
    public static final SafeOut NULL_OUT = new SafeOut() {
        public Object perform() {
            return null;
        }
    };

    /**
     * InOut handler returning a null value.
     */
    public static final SafeInOut NULL_INOUT = new SafeInOut() {
        public Object perform(Object in) {
            return null;
        }
    };

    /**
     * Callback used to convert Exceptions to RuntimeExceptions.
     */
    private static final SafeInOut EXP_TO_RUNTIME = new SafeInOut() {
        @Override
        public Object perform(Object in) {
            final Exception ex = (Exception) in;
            throw new RuntimeException(ex.getMessage(), ex);
        }
    };

    public interface BinaryOut{
        Object perform(Object in1, Object in2) throws Exception;
    }

    public interface Binary{
        void perform(Object in1, Object in2) throws Exception;
    }

    public interface InOut{
        Object perform(Object in) throws Exception;
    }
    
    public interface In{
        void perform(Object in) throws Exception;
    }
    
    public interface Out{
        Object perform() throws Exception;
    }

    public interface Void{
        void perform() throws Exception;
    }

    public interface SafeBinaryOut extends BinaryOut{
        Object perform(Object in1, Object in2);
    }

    public interface SafeBinary extends Binary{
        void perform(Object in1, Object in2);
    }

    public interface SafeInOut extends InOut{
        Object perform(Object in);
    }

    public interface SafeIn extends In{
        void perform(Object in);
    }

    public interface SafeOut extends Out{
        Object perform();
    }

    public interface SafeVoid extends Void{
        void perform();
    }


    /**
     * Create a BinaryOut handler which return a constant value.
     *
     * @param value constant value
     * @return SafeBinaryOut
     */
    public static SafeBinaryOut constantBinaryOut(final Object value){
        return new SafeBinaryOut() {
            public Object perform(Object in1, Object in2) {
                return value;
            }
        };
    }

    /**
     * Create a InOut handler which return a constant value.
     *
     * @param value constant value
     * @return SafeInOut
     */
    public static SafeInOut constantInOut(final Object value){
        return new SafeInOut() {
            public Object perform(Object in) {
                return value;
            }
        };
    }

    /**
     * Create a Out handler which return a constant value.
     *
     * @param value constant value
     * @return SafeOut
     */
    public static SafeOut constantOut(final Object value){
        return new SafeOut() {
            public Object perform() {
                return value;
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static SafeBinaryOut safe(final BinaryOut handler){
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static SafeBinary safe(final Binary handler){
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static SafeInOut safe(final InOut handler){
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static SafeIn safe(final In handler){
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static SafeOut safe(final SafeOut handler){
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions throw are converted to runtime exceptions.
     *
     * @param handler
     * @return safe handler
     */
    public static SafeVoid safe(final Void handler){
        return safe(handler, EXP_TO_RUNTIME);
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static SafeBinaryOut safe(final BinaryOut handler, final SafeInOut exceptionCallback){
        return new SafeBinaryOut() {
            public Object perform(Object inValue1, Object inValue2) {
                try {
                    return handler.perform(inValue1,inValue2);
                } catch (Exception ex) {
                    return exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static SafeBinary safe(final Binary handler, final SafeInOut exceptionCallback){
        return new SafeBinary() {
            public void perform(Object inValue1, Object inValue2) {
                try {
                    handler.perform(inValue1,inValue2);
                } catch (Exception ex) {
                    exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static SafeInOut safe(final InOut handler, final SafeInOut exceptionCallback){
        return new SafeInOut() {
            public Object perform(Object in) {
                try {
                    return handler.perform(in);
                } catch (Exception ex) {
                    return exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static SafeIn safe(final In handler, final SafeInOut exceptionCallback){
        return new SafeIn() {
            public void perform(Object inValue) {
                try {
                    handler.perform(inValue);
                } catch (Exception ex) {
                    exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static SafeOut safe(final SafeOut handler, final SafeInOut exceptionCallback){
        return new SafeOut() {
            public Object perform() {
                try {
                    return handler.perform();
                } catch (Exception ex) {
                    return exceptionCallback.perform(ex);
                }
            }
        };
    }

    /**
     * Create a safe handler from an unsafe one.
     * The potential exceptions are processed by the callback which will
     * all provide the fallback returned value.
     *
     * @param handler
     * @param exceptionCallback exception fallback
     * @return safe handler
     */
    public static SafeVoid safe(final Void handler, final SafeInOut exceptionCallback){
        return new SafeVoid() {
            public void perform() {
                try {
                    handler.perform();
                } catch (Exception ex) {
                    exceptionCallback.perform(ex);
                }
            }
        };
    }

    public static InOut asInOut(final Void handler, final Object outValue){
        return new InOut() {
            public Object perform(Object in) throws Exception {
                handler.perform();
                return outValue;
            }
        };
    }

    public static InOut asInOut(final Out handler){
        return new InOut() {
            public Object perform(Object in) throws Exception {
                return handler.perform();
            }
        };
    }

    public static InOut asInOut(final In handler, final Object outValue){
        return new InOut() {
            public Object perform(Object in) throws Exception {
                handler.perform(in);
                return outValue;
            }
        };
    }

    public static In asIn(final Void handler){
        return new In() {
            public void perform(Object in) throws Exception {
                handler.perform();
            }
        };
    }

    public static In asIn(final Out handler){
        return new In() {
            public void perform(Object in) throws Exception {
                handler.perform();
            }
        };
    }

    public static In asIn(final InOut handler){
        return new In() {
            public void perform(Object in) throws Exception {
                handler.perform(in);
            }
        };
    }

    public static Out asOut(final Void handler, final Object outValue){
        return new Out() {
            public Object perform() throws Exception {
                handler.perform();
                return outValue;
            }
        };
    }

    public static Out asOut(final In handler, final Object inValue, final Object outValue){
        return new Out() {
            public Object perform() throws Exception {
                handler.perform(inValue);
                return outValue;
            }
        };
    }

    public static Out asOut(final InOut handler, final Object inValue){
        return new Out() {
            public Object perform() throws Exception {
                return handler.perform(inValue);
            }
        };
    }

    public static Void asVoid(final Out handler, final Object outValue){
        return new Void() {
            public void perform() throws Exception {
                handler.perform();
            }
        };
    }

    public static Void asVoid(final In handler, final Object inValue){
        return new Void() {
            public void perform() throws Exception {
                handler.perform(inValue);
            }
        };
    }

    public static Void asVoid(final InOut handler, final Object inValue){
        return new Void() {
            public void perform() throws Exception {
                handler.perform(inValue);
            }
        };
    }

    public static SafeInOut asInOut(final SafeVoid handler, final Object outValue){
        return new SafeInOut() {
            public Object perform(Object in) {
                handler.perform();
                return outValue;
            }
        };
    }

    public static SafeInOut asInOut(final SafeOut handler){
        return new SafeInOut() {
            public Object perform(Object in) {
                return handler.perform();
            }
        };
    }

    public static SafeInOut asInOut(final SafeIn handler, final Object outValue){
        return new SafeInOut() {
            public Object perform(Object in) {
                handler.perform(in);
                return outValue;
            }
        };
    }

    public static SafeIn asIn(final SafeVoid handler){
        return new SafeIn() {
            public void perform(Object in) {
                handler.perform();
            }
        };
    }

    public static SafeIn asIn(final SafeOut handler){
        return new SafeIn() {
            public void perform(Object in) {
                handler.perform();
            }
        };
    }

    public static SafeIn asIn(final SafeInOut handler){
        return new SafeIn() {
            public void perform(Object in) {
                handler.perform(in);
            }
        };
    }

    public static SafeOut asOut(final SafeVoid handler, final Object outValue){
        return new SafeOut() {
            public Object perform() {
                handler.perform();
                return outValue;
            }
        };
    }

    public static SafeOut asOut(final SafeIn handler, final Object inValue, final Object outValue){
        return new SafeOut() {
            public Object perform() {
                handler.perform(inValue);
                return outValue;
            }
        };
    }

    public static SafeOut asOut(final SafeInOut handler, final Object inValue){
        return new SafeOut() {
            public Object perform() {
                return handler.perform(inValue);
            }
        };
    }

    public static SafeVoid asVoid(final SafeOut handler, final Object outValue){
        return new SafeVoid() {
            public void perform() {
                handler.perform();
            }
        };
    }

    public static SafeVoid asVoid(final SafeIn handler, final Object inValue){
        return new SafeVoid() {
            public void perform() {
                handler.perform(inValue);
            }
        };
    }

    public static SafeVoid asVoid(final SafeInOut handler, final Object inValue){
        return new SafeVoid() {
            public void perform()  {
                handler.perform(inValue);
            }
        };
    }

}
