package science.unlicense.api.model.tree;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface TypedNode extends NamedNode{

    NodeCardinality getCardinality();

    NodeType getType();

    TypedNode addChild(NodeCardinality type, Object value);

    TypedNode getChild(Chars name);

    TypedNode getChild(NodeCardinality type);

    TypedNode getOrCreateChild(Chars name);

    TypedNode getOrCreateChild(NodeCardinality type);

    TypedNode[] getChildren(Chars name);

    TypedNode[] getChildren(NodeCardinality type);

}
