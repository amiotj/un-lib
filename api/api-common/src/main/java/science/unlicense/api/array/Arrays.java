
package science.unlicense.api.array;

import science.unlicense.api.CObjects;
import science.unlicense.api.Orderable;
import science.unlicense.api.Sorter;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.number.Int64;

/**
 * Utility methods for arrays.
 *
 * @author Johann Sorel
 * @author Yann D'Isanto
 * @author Bertrand Cote
 */
public final class Arrays {

    private static final Chars CHARS_NULL = new Chars(new byte[]{'n','u','l','l'});
    private static final Chars CHARS_TRUE = new Chars(new byte[]{'t','r','u','e'});
    private static final Chars CHARS_FALSE = new Chars(new byte[]{'f','a','l','s','e'});

    /** 0 length boolean array */
    public static final boolean[] ARRAY_BOOL_EMPTY = new boolean[0];
    /** 0 length byte array */
    public static final byte[] ARRAY_BYTE_EMPTY = new byte[0];
    /** 0 length short array */
    public static final short[] ARRAY_SHORT_EMPTY = new short[0];
    /** 0 length int array */
    public static final int[] ARRAY_INT_EMPTY = new int[0];
    /** 0 length long array */
    public static final long[] ARRAY_LONG_EMPTY = new long[0];
    /** 0 length float array */
    public static final float[] ARRAY_FLOAT_EMPTY = new float[0];
    /** 0 length double array */
    public static final double[] ARRAY_DOUBLE_EMPTY = new double[0];
    /** 0 length object array */
    public static final Object[] ARRAY_OBJECT_EMPTY = new Object[0];

    private Arrays(){}

    // =========================================================================
    
    public static int getSize(Object array) {
        if (array instanceof Object[]) {
            return ((Object[])array).length;
        } else if (array instanceof byte[]) {
            return ((byte[])array).length;
        } else if (array instanceof short[]) {
            return ((short[])array).length;
        } else if (array instanceof int[]) {
            return ((int[])array).length;
        } else if (array instanceof long[]) {
            return ((long[])array).length;
        } else if (array instanceof float[]) {
            return ((float[])array).length;
        } else if (array instanceof double[]) {
            return ((double[])array).length;
        }
        throw new InvalidArgumentException("Object is not an array");
    }
    
    public static Object getValue(Object array, int index) {
        if (array instanceof Object[]) {
            return ((Object[])array)[index];
        } else if (array instanceof byte[]) {
            return ((byte[])array)[index];
        } else if (array instanceof short[]) {
            return ((short[])array)[index];
        } else if (array instanceof int[]) {
            return ((int[])array)[index];
        } else if (array instanceof long[]) {
            return ((long[])array)[index];
        } else if (array instanceof float[]) {
            return ((float[])array)[index];
        } else if (array instanceof double[]) {
            return ((double[])array)[index];
        }
        throw new InvalidArgumentException("Object is not an array");
    }
    
    public static void setValue(Object array, int index, Object value) {
        if (array instanceof Object[]) {
            ((Object[])array)[index] = value;
        } else if (array instanceof byte[]) {
            ((byte[])array)[index] = (Byte) value;
        } else if (array instanceof short[]) {
            ((short[])array)[index] = (Short) value;
        } else if (array instanceof int[]) {
            ((int[])array)[index] = (Integer) value;
        } else if (array instanceof long[]) {
            ((long[])array)[index] = (Long) value;
        } else if (array instanceof float[]) {
            ((float[])array)[index] = (Float) value;
        } else if (array instanceof double[]) {
            ((double[])array)[index] = (Double) value;
        }
        throw new InvalidArgumentException("Object is not an array");
    }
    
    // =========================================================================
    
    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(boolean[] a1, boolean[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }
    
    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(byte[] a1, byte[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(short[] a1, short[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(int[] a1, int[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(long[] a1, long[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(float[] a1, float[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @param tolerance, tolerance
     * @return true if arrays are equal
     */
    public static boolean equals(float[] a1, float[] a2, double tolerance){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0,tolerance);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(double[] a1, double[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @param tolerance, tolerance
     * @return true if arrays are equal
     */
    public static boolean equals(double[] a1, double[] a2, double tolerance){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0,tolerance);
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order. Object equaltiy is made with equals method.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(Object[] a1, Object[] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        return equals(a1,0,a1.length,a2,0);
    }
    
    // =========================================================================

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(boolean[][] a1, boolean[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(byte[][] a1, byte[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(short[][] a1, short[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(int[][] a1, int[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(long[][] a1, long[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(float[][] a1, float[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @param tolerance, tolerance
     * @return true if arrays are equal
     */
    public static boolean equals(float[][] a1, float[][] a2, double tolerance){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i],tolerance)){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(double[][] a1, double[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @param tolerance, tolerance
     * @return true if arrays are equal
     */
    public static boolean equals(double[][] a1, double[][] a2, double tolerance){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i],tolerance)){
                return false;
            }
        }
        return true;
    }

    /**
     * Test if arrays are equals.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * @param a1
     * @param a2
     * @return true if arrays are equal
     */
    public static boolean equals(Object[][] a1, Object[][] a2){
        if (a1==a2) return true;
        if (a1==null || a2==null) return false;
        if(a1.length != a2.length) return false;
        for(int i=0;i<a1.length;i++){
            if(!equals(a1[i], a2[i])){
                return false;
            }
        }
        return true;
    }
    
    // =========================================================================
    
    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(boolean[] a1, int soffset, int length, boolean[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(byte[] a1, int soffset, int length, byte[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(short[] a1, int soffset, int length, short[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(int[] a1, int soffset, int length, int[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(long[] a1, int soffset, int length, long[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(float[] a1, int soffset, int length, float[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @param tolerance, tolerance
     * @return true if arrays are equal
     */
    public static boolean equals(float[] a1, int soffset, int length, float[] a2, int toffset, double tolerance){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            final double diff = a1[soffset+i] - a2[toffset+i];
            if( diff<-tolerance || diff>tolerance){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(double[] a1, int soffset, int length, double[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] != a2[toffset+i]){
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @param tolerance, tolerance
     * @return true if arrays are equal
     */
    public static boolean equals(double[] a1, int soffset, int length, double[] a2, int toffset, double tolerance){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            final double diff = a1[soffset+i] - a2[toffset+i];
            if (diff < -tolerance || diff > tolerance) {
                return false;
            }
        }

        return true;
    }

    /**
     * Test if arrays are equals from soffset and given length.
     * Will return true if array size are the same and all elements are equal
     * and in the same order. Object equaltiy is made with equals method.
     * Warning : offset+length lust not outpass array size
     * @param a1, not null
     * @param soffset, first array offset, must be positive
     * @param length, range length to test
     * @param a2, not null
     * @param toffset, second array offset, must be positive
     * @return true if arrays are equal
     */
    public static boolean equals(Object[] a1, int soffset, int length, Object[] a2, int toffset){
        if(soffset<0 || toffset<0){
            return false;
        }
        if(soffset+length>a1.length || toffset+length>a2.length){
            //size can't matched
            return false;
        }

        for(int i=0;i<length;i++){
            if(a1[soffset+i] == null){
                if(a2[toffset+i] != null){
                    return false;
                }
            }else if(!a1[soffset+i].equals(a2[toffset+i])){
                return false;
            }
        }

        return true;
    }
    
    // =========================================================================

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static boolean[] resize(boolean[] array, int newSize){
        return copy(array, 0, newSize);
    }
    
    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static byte[] resize(byte[] array, int newSize){
        return copy(array, 0, newSize);
    }

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static short[] resize(short[] array, int newSize){
        return copy(array, 0, newSize);
    }

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static int[] resize(int[] array, int newSize){
        return copy(array, 0, newSize);
    }

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static long[] resize(long[] array, int newSize){
        return copy(array, 0, newSize);
    }

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static float[] resize(float[] array, int newSize){
        return copy(array, 0, newSize);
    }

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static double[] resize(double[] array, int newSize){
        return copy(array, 0, newSize);
    }

    /**
     * Create a copy of given array.
     * if the new size is larger than original array, remaining cells will be filled
     * with default type value.
     * if new size is smaller the original array, values will be truncated.
     * @param array not null
     * @param newSize new size, must be 0 or positive
     * @return new array of given size
     */
    public static Object[] resize(Object[] array, int newSize){
        return copy(array, 0, newSize);
    }
    
    // =========================================================================
    
    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static boolean[] copy(boolean[] source, int offset, int length) {
        final boolean[] copy = new boolean[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : false;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static byte[] copy(byte[] source, int offset, int length) {
        final byte[] copy = new byte[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : 0;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static short[] copy(short[] source, int offset, int length) {
        final short[] copy = new short[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : 0;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static int[] copy(int[] source, int offset, int length) {
        final int[] copy = new int[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : 0;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static long[] copy(long[] source, int offset, int length) {
        final long[] copy = new long[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : 0;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static float[] copy(float[] source, int offset, int length) {
        final float[] copy = new float[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : 0;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static double[] copy(double[] source, int offset, int length) {
        final double[] copy = new double[length];
        for(int i = 0; i < length; i ++) {
            copy[i] = i + offset < source.length ? source[i + offset] : 0;
        }
        return copy;
    }

    /**
     * Copy a portion of given array.
     * If needed, the returned array is padded with nulls to obtain the required 
     * length.
     * @param source not null
     * @param offset offset to start copying
     * @param length number of elements to copy.
     * @return new array
     */
    public static Object[] copy(Object[] source, int offset, int length) {
        Object[] target = (Object[])java.lang.reflect.Array.newInstance(source.getClass().getComponentType(), length);
        for(int i = 0; i < length; i ++) {
            target[i] = i + offset < source.length ? source[i + offset] : null;
        }
        return target;
    }
    
    // =========================================================================
    
    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static boolean[] copy(boolean[] source) {
        return copy(source, 0, source.length,new boolean[source.length],0);
    }
    
    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static byte[] copy(byte[] source) {
        return copy(source, 0, source.length,new byte[source.length],0);
    }

    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static short[] copy(short[] source) {
        return copy(source, 0, source.length,new short[source.length],0);
    }

    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static int[] copy(int[] source) {
        return copy(source, 0, source.length,new int[source.length],0);
    }

    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static long[] copy(long[] source) {
        return copy(source, 0, source.length,new long[source.length],0);
    }

    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static float[] copy(float[] source) {
        return copy(source, 0, source.length,new float[source.length],0);
    }

    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static double[] copy(double[] source) {
        return copy(source, 0, source.length,new double[source.length],0);
    }

    /**
     * Copy given array.
     * @param source not null
     * @return new array
     */
    public static Object[] copy(Object[] source) {
        Object[] target = (Object[])java.lang.reflect.Array.newInstance(source.getClass().getComponentType(), source.length);
        return copy( source, 0, source.length, target, 0 );
    }
    
    // =========================================================================
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static boolean[] copy(boolean[] source, boolean[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static byte[] copy(byte[] source, byte[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static short[] copy(short[] source, short[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static int[] copy(int[] source, int[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static long[] copy(long[] source, long[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static float[] copy(float[] source, float[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static double[] copy(double[] source, double[] target){
        return copy(source,0,source.length,target,0);
    }
    
    /**
     * Copy given source array to the target array.
     * 
     * @param source not null
     * @param target array where to insert new elements
     * @return the target array with copied values.
     */
    public static <T extends Object> T[] copy(Object[] source, T[] target){
        return copy(source,0,source.length,target,0);
    }
    
    // =========================================================================
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static boolean[] copy(boolean[] source, int soffset, int length, boolean[] target, int toffset){
        for(int i=soffset; i<soffset+length; i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static byte[] copy(byte[] source, int soffset, int length, byte[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static short[] copy(short[] source, int soffset, int length, short[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static int[] copy(int[] source, int soffset, int length, int[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static long[] copy(long[] source, int soffset, int length, long[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static float[] copy(float[] source, int soffset, int length, float[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static double[] copy(double[] source, int soffset, int length, double[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = source[i];
        }
        return target;
    }
    
    /**
     * Copy a portion of given source array of length elements from soffset index, to 
     * the target array beginning to toffset index.
     * If needed, the returned array is padded with zeros to obtain the required 
     * length.
     * @param source not null
     * @param soffset offset to start copying
     * @param length number of elemnts to copy
     * @param target array where to insert new elements
     * @param toffset start insert index
     * @return the target array with copied values.
     */
    public static <T extends Object> T[] copy(Object[] source, int soffset, int length, T[] target, int toffset){
        for(int i=soffset;i<soffset+length;i++,toffset++){
            target[toffset] = (T) source[i];
        }
        return target;
    }
    
    // =========================================================================
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static boolean[][] copy(boolean[][] source) {
        boolean[][] target = new boolean[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static byte[][] copy(byte[][] source) {
        byte[][] target = new byte[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static short[][] copy(short[][] source) {
        short[][] target = new short[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static int[][] copy(int[][] source) {
        int[][] target = new int[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static long[][] copy(long[][] source) {
        long[][] target = new long[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static float[][] copy(float[][] source) {
        float[][] target = new float[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static double[][] copy(double[][] source) {
        double[][] target = new double[source.length][];
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return target;
    }
    
    /**
     *  Copy the given 2D source array.
     * @param source array to copy
     * @return array copy
     */
    public static Object[][] copy(Object[][] source) {
        Object[] target = (Object[])java.lang.reflect.Array.newInstance(source.getClass().getComponentType(), source.length);
        for (int i = 0; i < source.length; i++) {
            target[i] = copy(source[i]);
        }
        return (Object[][]) target;
    }
    
    // =========================================================================
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static boolean[][] copy(
            boolean[][] source, int sx, int sy, int w, int h,
            boolean[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static byte[][] copy(
            byte[][] source, int sx, int sy, int w, int h,
            byte[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static short[][] copy(
            short[][] source, int sx, int sy, int w, int h,
            short[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static int[][] copy(
            int[][] source, int sx, int sy, int w, int h,
            int[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static long[][] copy(
            long[][] source, int sx, int sy, int w, int h,
            long[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static float[][] copy(
            float[][] source, int sx, int sy, int w, int h,
            float[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static double[][] copy(
            double[][] source, int sx, int sy, int w, int h,
            double[][] target, int tx, int ty){
        for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    /**
     * Copy a portion of given 2D source array, from column sx and row sy of 
     * width w and height h, to the target array from column tx and row ty.
     * @param source 2D array
     * @param sx source column beginning
     * @param sy source row beginning
     * @param w width
     * @param h height
     * @param target 2D array
     * @param tx target column beginning
     * @param ty target row beginning
     * @return the target array with copied values.
     */
    public static Object[][] copy(
            Object[][] source, int sx, int sy, int w, int h,
            Object[][] target, int tx, int ty){
       for(int y=sy; y<sy+h; y++,ty++){
            for(int x=sx,btx=tx; x<sx+w; x++,btx++){
                target[ty][btx] = source[y][x];
            }
        }
        return target;
    }
    
    // =========================================================================

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static boolean[] insert(boolean[] values, int index, boolean value) {
        final boolean[] newArray = new boolean[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }
    
    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static byte[] insert(byte[] values, int index, byte value) {
        final byte[] newArray = new byte[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static short[] insert(short[] values, int index, short value) {
        final short[] newArray = new short[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static int[] insert(int[] values, int index, int value) {
        final int[] newArray = new int[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static long[] insert(long[] values, int index, long value) {
        final long[] newArray = new long[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static float[] insert(float[] values, int index, float value) {
        final float[] newArray = new float[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static double[] insert(double[] values, int index, double value) {
        final double[] newArray = new double[values.length+1];
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }

    /**
     * Insert given element at index.
     * @param values original array
     * @param index insert index
     * @param value inserted value
     * @return new array which size is source.size + 1
     */
    public static Object[] insert(Object[] values, int index, Object value) {
        Object[] newArray = (Object[])java.lang.reflect.Array.newInstance(values.getClass().getComponentType(), values.length+1);
        copy(values, 0, index, newArray, 0);
        newArray[index] = value;
        copy(values, index, values.length-index, newArray, index+1);
        return newArray;
    }
    
    // =========================================================================

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static boolean[] remove(boolean[] values, int index) {
        final boolean[] newArray = new boolean[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }
    
    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static byte[] remove(byte[] values, int index) {
        final byte[] newArray = new byte[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static short[] remove(short[] values, int index) {
        final short[] newArray = new short[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static int[] remove(int[] values, int index) {
        final int[] newArray = new int[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static long[] remove(long[] values, int index) {
        final long[] newArray = new long[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static float[] remove(float[] values, int index) {
        final float[] newArray = new float[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static double[] remove(double[] values, int index) {
        final double[] newArray = new double[values.length-1];
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }

    /**
     * Remove element at index.
     * @param values original array
     * @param index removed element index
     * @return new array which size is source.size - 1
     */
    public static Object[] remove(Object[] values, int index) {
        Object[] newArray = (Object[])java.lang.reflect.Array.newInstance(values.getClass().getComponentType(), values.length-1);
        copy(values, 0, index, newArray, 0);
        copy(values, index+1, values.length-(index+1), newArray, index);
        return newArray;
    }
    
    // =========================================================================

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static boolean removeWithin(boolean[] values, int index) {
        final boolean removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = false;
        return removedValue;
    }
    
    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static byte removeWithin(byte[] values, int index) {
        final byte removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = 0;
        return removedValue;
    }

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static short removeWithin(short[] values, int index) {
        final short removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = 0;
        return removedValue;
    }

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static int removeWithin(int[] values, int index) {
        final int removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = 0;
        return removedValue;
    }

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static long removeWithin(long[] values, int index) {
        final long removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = 0;
        return removedValue;
    }

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static float removeWithin(float[] values, int index) {
        final float removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = 0;
        return removedValue;
    }

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to zero
     *
     * @param values original array
     * @param index removed element index
     * @return removed value
     */
    public static double removeWithin(double[] values, int index) {
        final double removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = 0;
        return removedValue;
    }

    /**
     * Remove element at index.
     * Move elements after index by -1.
     * Last element is set to null
     *
     * @param values original array
     * @param index removed element index
     * @return removed object
     */
    public static Object removeWithin(Object[] values, int index) {
        final Object removedValue = values[index];
        if(index+1 < values.length){
            Arrays.copy(values, index+1, values.length-index-1, values, index);
        }
        values[values.length-1] = null; //remove reference
        return removedValue;
    }
    
    // =========================================================================
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(boolean[] buffer, boolean value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(byte[] buffer, byte value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(short[] buffer, short value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(int[] buffer, int value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(long[] buffer, long value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(float[] buffer, float value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(double[] buffer, double value){
        fill(buffer,0,buffer.length,value);
    }
    
    /**
     * Fill the given array with the given value.
     * @param buffer the array to fill in.
     * @param value fill value
     */
    public static void fill(Object[] buffer, Object value){
        fill(buffer,0,buffer.length,value);
    }
    
    // =========================================================================
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(boolean[] buffer, int offset, int length, boolean value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(byte[] buffer, int offset, int length, byte value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(short[] buffer, int offset, int length, short value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(int[] buffer, int offset, int length, int value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(long[] buffer, int offset, int length, long value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(float[] buffer, int offset, int length, float value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(double[] buffer, int offset, int length, double value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    /**
     * Fill the given array with the given value, from the given offset, on the 
     * given length.
     * @param buffer the array to fill in.
     * @param offset start index offset
     * @param length number of elements to fill
     * @param value fill value
     */
    public static void fill(Object[] buffer, int offset, int length, Object value){
        for(int i=0;i<length;i++){
            buffer[offset+i] = value;
        }
    }
    
    // =========================================================================

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(boolean[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final boolean buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }
    
    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(byte[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final byte buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(short[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final short buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(int[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final int buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(long[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final long buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(float[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final float buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(double[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final double buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }

    /**
     * Reverse array order, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     */
    public static void reverse(Object[] array, int offset, int length){
        for(int i=0,n=length/2 ;i<n; i++){
            final int findex = offset + i;
            final int bindex = offset + length - (i+1);
            final Object buffer = array[findex];
            array[findex] = array[bindex];
            array[bindex] = buffer;
        }
    }
    
    // =========================================================================
    
    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(boolean[] array, int offset, int length, int blockSize){
        final boolean[] buffer = new boolean[blockSize];

        for(int i=0,n=length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }
    
    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(byte[] array, int offset, int length, int blockSize){
        final byte[] buffer = new byte[blockSize];

        for(int i=0,n=length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }

    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(short[] array, int offset, int length, int blockSize){
        final short[] buffer = new short[blockSize];

        for(int i=0,n= length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }

    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(int[] array, int offset, int length, int blockSize){
        final int[] buffer = new int[blockSize];

        for(int i=0,n= length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }

    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(long[] array, int offset, int length, int blockSize){
        final long[] buffer = new long[blockSize];

        for(int i=0,n= length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }

    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(float[] array, int offset, int length, int blockSize){
        final float[] buffer = new float[blockSize];

        for(int i=0,n= length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }

    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(double[] array, int offset, int length, int blockSize){
        final double[] buffer = new double[blockSize];

        for(int i=0,n= length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }

    /**
     * Reverse array order by blocks, first one becomes last.
     * @param array array to reverse
     * @param offset start offset
     * @param length number of elements
     * @param blockSize number of element which compose an element
     */
    public static void reverse(Object[] array, int offset, int length, int blockSize){
        final Object[] buffer = new Object[blockSize];

        for(int i=0,n= length/(blockSize*2) ;i<n; i++){
            int findex = offset + i*blockSize;
            int bindex = offset + length - ((i+1)*blockSize);
            //copy forward block in buffer
            copy(array, findex, blockSize, buffer, 0);
            //copy backward block in forward block
            copy(array, bindex, blockSize, array, findex);
            //copy buffer block in backward block
            copy(buffer, 0, blockSize, array, bindex);
        }
    }
    
    // =========================================================================
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(boolean[] array, int index1, int index2) {
        final boolean temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(byte[] array, int index1, int index2) {
        final byte temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(short[] array, int index1, int index2) {
        final short temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(int[] array, int index1, int index2) {
        final int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(long[] array, int index1, int index2) {
        final long temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(float[] array, int index1, int index2) {
        final float temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(double[] array, int index1, int index2) {
        final double temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    /**
     * Swap values in array at given indexes.
     * @param array array to process
     * @param index1 index in array of first value
     * @param index2 index in array of second value 
     */
    public static void swap(Object[] array, int index1, int index2) {
        final Object temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
    
    // =========================================================================
    
    /**
     * Concatenate a array of boolean arrays.
     * @param arrays arrays to concatenate
     * @return boolean[] result concatenated array
     */
    public static boolean[] concatenate(boolean[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final boolean[] buffer = new boolean[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }
    
    /**
     * Concatenate a array of byte arrays.
     * @param arrays arrays to concatenate
     * @return byte[] result concatenated array
     */
    public static byte[] concatenate(byte[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final byte[] buffer = new byte[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }

    /**
     * Concatenate a array of short arrays.
     * @param arrays arrays to concatenate
     * @return short[] result concatenated array
     */
    public static short[] concatenate(short[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final short[] buffer = new short[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }

    /**
     * Concatenate a array of int arrays.
     * @param arrays arrays to concatenate
     * @return int[] result concatenated array
     */
    public static int[] concatenate(int[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final int[] buffer = new int[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }

    /**
     * Concatenate a array of long arrays.
     * @param arrays arrays to concatenate
     * @return long[] result concatenated array
     */
    public static long[] concatenate(long[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final long[] buffer = new long[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }

    /**
     * Concatenate a array of float arrays.
     * @param arrays arrays to concatenate
     * @return float[] result concatenated array
     */
    public static float[] concatenate(float[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final float[] buffer = new float[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }

    /**
     * Concatenate a array of double arrays.
     * @param arrays arrays to concatenate
     * @return double[] result concatenated array
     */
    public static double[] concatenate(double[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        final double[] buffer = new double[size];
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }
    
    /**
     * Concatenate a array of Object arrays.
     * @param arrays arrays to concatenate
     * @return Object[] result concatenated array
     */
    public static Object[] concatenate(Object[][] arrays){
        int size = 0;
        for(int i=0;i<arrays.length;i++){
            size += arrays[i].length;
        }
        Object[] buffer = (Object[])java.lang.reflect.Array.newInstance(arrays.getClass().getComponentType().getComponentType(), size);
        int offset = 0;
        for(int i=0;i<arrays.length;i++){
            copy(arrays[i], 0, arrays[i].length, buffer, offset);
            offset += arrays[i].length;
        }

        return buffer;
    }
    
    // =========================================================================
    
    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(boolean[] array, boolean candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(byte[] array, byte candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(short[] array, short candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(int[] array, int candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(long[] array, long candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(float[] array, float candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(double[] array, double candidate){
        return contains(array,0,array.length,candidate);
    }

    /**
     * Check if an element is contained is this array.
     * @param array search array
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean contains(Object[] array, Object candidate){
        return contains(array,0,array.length,candidate);
    }
    
    // =========================================================================
    
    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(boolean[] array, int offset, int length, boolean candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(byte[] array, int offset, int length, byte candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(short[] array, int offset, int length, short candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(int[] array, int offset, int length, int candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(long[] array, int offset, int length, long candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(float[] array, int offset, int length, float candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if eleme nt is in the array range, otherwise return false.
     */
    public static boolean contains(double[] array, int offset, int length, double candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate) return true;
        }
        return false;
    }

    /**
     * Check if an element is contained is this array range.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean contains(Object[] array, int offset, int length, Object candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(CObjects.equals(array[offset],candidate)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check if an element is contained is this array range.
     * Identity test is name using ==
     * 
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return true if element is in the array range, otherwise return false.
     */
    public static boolean containsIdentity(Object[] array, int offset, int length, Object candidate){
        if(length==0) return false;
        for(int n=offset+length;offset<n;offset++){
            if(array[offset] == candidate){
                return true;
            }
        }
        return false;
    }
    
    // =========================================================================

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(boolean[] array, boolean candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(byte[] array, byte candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(short[] array, short candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(int[] array, int candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(long[] array, long candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(float[] array, float candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(double[] array, double candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(Object[] array, Object candidate){
        for(int i=0;i<array.length;i++){
            if(CObjects.equals(array[i],candidate)){
                return i;
            }
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * Identity test is name using ==
     *
     * @param array search array
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurenceIdentity(Object[] array, Object candidate){
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate){
                return i;
            }
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(boolean[] array, int offset, int length, boolean candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(byte[] array, int offset, int length, byte candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(short[] array, int offset, int length, short candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(int[] array, int offset, int length, int candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset in dex where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(long[] array, int offset, int length, long candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(float[] array, int offset, int length, float candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(double[] array, int offset, int length, double candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate) return i;
        }
        return -1;
    }

    /**
     * Find the first occurrence of given value in the array.
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurence(Object[] array, int offset, int length, Object candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(CObjects.equals(array[i],candidate)){
                return i;
            }
        }
        return -1;
    }
    
    /**
     * Find the first occurrence of given value in the array.
     * Identity test is name using ==
     * 
     * @param array search array
     * @param offset index where to start searching
     * @param length number of elements to test
     * @param candidate searched value
     * @return candidate index in array, -1 if not found
     */
    public static int getFirstOccurenceIdentity(Object[] array, int offset, int length, Object candidate){
        for(int i=offset,n=offset+length;i<n;i++){
            if(array[i] == candidate){
                return i;
            }
        }
        return -1;
    }
    
    // =========================================================================

    public static Chars toChars(final Object array){
        return toChars(array,-1);
    }
    
    public static Chars toChars(final Object array, int maxlength){
        if(array instanceof boolean[])      return toChars((boolean[])array,maxlength);
        else if(array instanceof byte[])    return toChars((byte[])array,maxlength);
        else if(array instanceof short[])   return toChars((short[])array,maxlength);
        else if(array instanceof int[])     return toChars((int[])array,maxlength);
        else if(array instanceof long[])    return toChars((long[])array,maxlength);
        else if(array instanceof float[])   return toChars((float[])array,maxlength);
        else if(array instanceof double[])  return toChars((double[])array,maxlength);
        else if(array instanceof Object[])  return toChars((Object[])array,maxlength);
        else if(array instanceof boolean[][])return toChars((boolean[][])array);
        else if(array instanceof byte[][])  return toChars((byte[][])array);
        else if(array instanceof short[][]) return toChars((short[][])array);
        else if(array instanceof int[][])   return toChars((int[][])array);
        else if(array instanceof long[][])  return toChars((long[][])array);
        else if(array instanceof float[][]) return toChars((float[][])array);
        else if(array instanceof double[][])return toChars((double[][])array);
        else if(array instanceof Object[][])return toChars((Object[][])array);
        else{
            throw new RuntimeException("Object is not a 1D or 2D array : "+array);
        }
    }
    
    
    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final boolean[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(array[i]?CHARS_TRUE:CHARS_FALSE);
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }
    
    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final byte[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(Int32.encode(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final short[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(Int32.encode(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final int[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(Int32.encode(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final long[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(Int64.encode(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final float[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(Float64.encode(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final double[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(Float64.encode(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final Object[] array, int maxLength){
        if(array == null) return CHARS_NULL;

        final CharBuffer buffer = new CharBuffer();
        buffer.append('[');
        for(int i=0;i<array.length;i++){
            if(i>0) buffer.append(',');
            buffer.append(CObjects.toChars(array[i]));
            if(i==maxLength) {buffer.append(",...");break;}
        }
        buffer.append(']');
        return buffer.toChars();
    }
    
    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final boolean[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(array[i][j]?CHARS_TRUE:CHARS_FALSE);
            }
        }
        return buffer.toChars();
    }
    
    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final byte[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(Int32.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final short[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(Int32.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final int[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(Int32.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final long[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(Int64.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final float[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(Float64.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final double[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(Float64.encode(array[i][j]));
            }
        }
        return buffer.toChars();
    }

    /**
     * Chars representation of given array.
     * In a nice table structure.
     *
     * @param array, can be null
     * @return Chars, never null
     */
    public static Chars toChars(final Object[][] array){
        if(array == null) return CHARS_NULL;

        final TableWriter buffer = new TableWriter();
        for(int i=0;i<array.length;i++){
            buffer.newLine();
            for(int j=0;j<array[i].length;j++){
                buffer.appendCell(CObjects.toChars(array[i][j]));
            }
        }
        return buffer.toChars();
    }
    
    // =========================================================================
    
    /**
     * Check if an element is contained is this array.
     * Equality test is made with == .
     * 
     * @param array, can be null.
     * @param candidate searched value
     * @return true if element is in the array, otherwise return false.
     */
    public static boolean identityContains(Object[] array, Object candidate){
        if( array == null ) return false;
        for(int i=0;i<array.length;i++){
            if(array[i] == candidate) return true;
        }
        return false;
    }
    
    // =========================================================================
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(byte[] array, int leftIndex, int rightIndex, int pivotIndex) {
        byte pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (array[i] <= pivotValue) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(short[] array, int leftIndex, int rightIndex, int pivotIndex) {
        short pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (array[i] <= pivotValue) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(int[] array, int leftIndex, int rightIndex, int pivotIndex) {
        int pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (array[i] <= pivotValue) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(long[] array, int leftIndex, int rightIndex, int pivotIndex) {
        long pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (array[i] <= pivotValue) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(float[] array, int leftIndex, int rightIndex, int pivotIndex) {
        float pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (array[i] <= pivotValue) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(double[] array, int leftIndex, int rightIndex, int pivotIndex) {
        double pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (array[i] <= pivotValue) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( byte[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( short[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( int[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( long[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( float[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( double[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( byte[] array ) {
        quickSort( array, 0, array.length-1 );
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( short[] array ) {
        quickSort( array, 0, array.length-1 );
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( int[] array ) {
        quickSort( array, 0, array.length-1 );
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( long[] array ) {
        quickSort( array, 0, array.length-1 );
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( float[] array ) {
        quickSort( array, 0, array.length-1 );
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( double[] array ) {
        quickSort( array, 0, array.length-1 );
    }

    // ===== Object array sort =================================================
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(Object[] array, int leftIndex, int rightIndex, int pivotIndex) {
        Object pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (((Orderable)array[i]).order((Orderable)pivotValue) <= 0) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     */
    public static void quickSort( Object[] array, int leftIndex, int rightIndex ) {
        if (array == null || array.length < 2) {
            return;
        }
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex = partition(array, leftIndex, rightIndex, pivotIndex);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex);
        }
    }
    
    /**
     * Sort given array (QuickSort algorithm).
     * @param array the array to be sorted.
     */
    public static void sort( Object[] array ) {
        quickSort( array, 0, array.length-1 );
    }
    
    // ===== Object array sort with Sorter =====================================
    
    /**
     * Helper function for quickSort.
     * https://en.wikipedia.org/wiki/Quicksort
     */
    private static int partition(Object[] array, int leftIndex, int rightIndex, int pivotIndex, Sorter sorter) {
        Object pivotValue = array[pivotIndex];
        Arrays.swap(array, pivotIndex, rightIndex);  // Move pivot to end
        int storeIndex = leftIndex;
        for (int i = leftIndex; i < rightIndex; i++) {  // left ≤ i < right
            if (sorter.sort(array[i], pivotValue) <= 0) {
                Arrays.swap(array, i, storeIndex);
                storeIndex += 1;  // only increment storeIndex if swapped
            }
        }
        Arrays.swap(array, storeIndex, rightIndex);  // Move pivot to its final place
        return storeIndex;
    }
    
    /**
     * Sort given array using QuickSort in-place algorithm using Sorter.
     * Average case performance: O(n log n)
     * Worst case performance: O(n^2)
     * https://en.wikipedia.org/wiki/Quicksort
     * 
     * @param array the array to be sorted.
     * @param leftIndex the beginning index array (inclusive)
     * @param rightIndex the ending index array (inclusive)
     * @param sorter the Sorter
     */
    public static void quickSort( Object[] array, int leftIndex, int rightIndex, Sorter sorter ) {
        if( sorter == null ) {
            quickSort( (Orderable[])array, leftIndex, rightIndex );
            return;
        }
        if (array == null || array.length < 2) {
            return;
        }
        
        // If the list has 2 or more items
        if (leftIndex < rightIndex) {
            // choose any pivotIndex such that leftIndex ≤ pivotIndex ≤ rightIndex
            int pivotIndex = leftIndex + (rightIndex - leftIndex) / 2;
            // // Get lists of bigger and smaller items and final position of pivot
            int pivotNewIndex  = partition(array, leftIndex, rightIndex, pivotIndex, sorter);
            // Recursively sort elements smaller than the pivot (assume pivotNewIndex - 1 does not underflow)
            quickSort(array, leftIndex, pivotNewIndex - 1, sorter);
            // Recursively sort elements at least as big as the pivot (assume pivotNewIndex + 1 does not overflow)
            quickSort(array, pivotNewIndex + 1, rightIndex, sorter);
        }
    }
    
    /**
     * Sort given array using Sorter (QuickSort algorithm).
     * @param array the array to be sorted.
     * @param sorter the Sorter
     */
    public static void sort( Object[] array, Sorter sorter ) {
        quickSort( array, 0, array.length-1, sorter );
    }

    // =========================================================================
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value
     * @param values array to search in, must be sorted in ascending order
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(byte value, byte[] values) {
        if( values.length == 0 ) return -1;
        byte midVal;
        int mid;
        int left = 0;
        int right = values.length - 1;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            if (value < midVal) {
                right = mid - 1;
            } else if (value > midVal) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((value < midVal) ? -mid : -(mid + 1)) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value
     * @param values array to search in, must be sorted in ascending order
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(short value, short[] values) {
        if( values.length == 0 ) return -1;
        return binarySearch(value, values, 0, values.length - 1);
    }
    
    /**
     * TODO , find a way to define the right limit value.
     * 
     */
    public static int binarySearch(short value, short[] values, int left, int right) {
        if( values.length == 0 ) return -1;
        short midVal;
        int mid;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            if (value < midVal) {
                right = mid - 1;
            } else if (value > midVal) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((value < midVal) ? -mid : -(mid + 1)) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value
     * @param values array to search in, must be sorted in ascending order
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(int value, int[] values) {
        if( values.length == 0 ) return -1;
        return binarySearch(value, values, 0, values.length - 1);
    }
    
    /**
     * TODO , find a way to define the right limit value.
     * 
     */
    public static int binarySearch(int value, int[] values, int left, int right) {
        if( values.length == 0 ) return -1;
        int midVal;
        int mid;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            if (value < midVal) {
                right = mid - 1;
            } else if (value > midVal) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((value < midVal) ? -mid : -(mid + 1)) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value
     * @param values array to search in, must be sorted in ascending order
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(long value, long[] values) {
        if( values.length == 0 ) return -1;
        return binarySearch(value, values, 0, values.length - 1);
    }
    
    /**
     * TODO , find a way to define the right limit value.
     * 
     */
    public static int binarySearch(long value, long[] values, int left, int right) {
        if( values.length == 0 ) return -1;
        long midVal;
        int mid;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            if (value < midVal) {
                right = mid - 1;
            } else if (value > midVal) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((value < midVal) ? -mid : -(mid + 1)) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value
     * @param values array to search in, must be sorted in ascending order
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(float value, float[] values) {
        if( values.length == 0 ) return -1;
        return binarySearch(value, values, 0, values.length - 1);
    }
    
    /**
     * TODO , find a way to define the right limit value.
     * 
     */
    public static int binarySearch(float value, float[] values, int left, int right) {
        if( values.length == 0 ) return -1;
        float midVal;
        int mid;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            if (value < midVal) {
                right = mid - 1;
            } else if (value > midVal) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((value < midVal) ? -mid : -(mid + 1)) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value
     * @param values array to search in, must be sorted in ascending order
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(double value, double[] values) {
        if( values.length == 0 ) return -1;
        return binarySearch(value, values, 0, values.length - 1);
    }
    
    /**
     * TODO , find a way to define the right limit value.
     * 
     */
    public static int binarySearch(double value, double[] values, int left, int right) {
        if( values.length == 0 ) return -1;
        double midVal;
        int mid;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            if (value < midVal) {
                right = mid - 1;
            } else if (value > midVal) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((value < midVal) ? -mid : -(mid + 1)) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value (must implement Orderable)
     * @param values array to search in, must be sorted in ascending order and 
     * all elements must implement Orderable.
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(Object value, Object[] values) {
        if( values.length == 0 ) return -1;
        Orderable orderableValue = (Orderable)value;
        Object midVal;
        int mid, compareValueToMidVal;
        int left = 0;
        int right = values.length - 1;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            compareValueToMidVal = orderableValue.order(midVal);
            if ( compareValueToMidVal < 0 ) {
                right = mid - 1;
            } else if ( compareValueToMidVal > 0 ) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ( ( compareValueToMidVal < 0 ) ? -mid : -(mid + 1) ) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     * If sorter is null, binarySearch(Object value, Object[] values) is called.
     *
     * @param value searched value
     * @param values array to search in, must be sorted with the Sorter
     * @param sorter the Sorter
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(Object value, Object[] values, Sorter sorter) {
        if( sorter == null ) {
            return binarySearch( (Orderable)value, (Orderable[])values );
        }
        if( values.length == 0 ) return -1;
        Object midVal;
        int mid, compareValueToMidVal;
        int left = 0;
        int right = values.length - 1;

        do {
            mid = (left + right) / 2;
            midVal = values[mid];
            compareValueToMidVal = sorter.sort(value, midVal);
            if (compareValueToMidVal < 0) {
                right = mid - 1;
            } else if (compareValueToMidVal > 0) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ( ( compareValueToMidVal < 0 ) ? -mid : -(mid + 1) ) - 1;
    }
    
    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     *
     * @param value searched value (must implement Orderable)
     * @param values collection to search in, must be sorted in ascending order 
     * and all elements must implement Orderable.
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(Object value, Sequence values) {
        if( values.getSize() == 0 ) return -1;
        Orderable orderableValue = (Orderable)value;
        Object midVal;
        int mid, compareValueToMidVal;
        int left = 0;
        int right = values.getSize() - 1;

        do {
            mid = (left + right) / 2;
            midVal = values.get(mid);
            compareValueToMidVal = orderableValue.order(midVal);
            if (compareValueToMidVal < 0) {
                right = mid - 1;
            } else if (compareValueToMidVal > 0) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((compareValueToMidVal < 0) ? -mid : -(mid+1))-1;
    }

    /**
     * Binary search algorithm.
     * http://en.literateprograms.org/Binary_search_(Java)
     *
     * Changed from base algorithm to handle insert index before first element.
     * If sorter is null, binarySearch(Object value, Sequence values) is called.
     *
     * @param value searched value
     * @param values collection to search in, must be sorted with the Sorter
     * @param sorter the Sorter
     * @return if positive value index, if negative insert index = -(val+1)
     */
    public static int binarySearch(Object value, Sequence values, Sorter sorter) {
        if( sorter == null ) {
            return binarySearch( (Orderable)value, values );
        }
        if( values.getSize() == 0 ) return -1;
        Object midVal;
        int mid, compareValueToMidVal;
        int left = 0;
        int right = values.getSize() - 1;

        do {
            mid = (left + right) / 2;
            midVal = values.get(mid);
            compareValueToMidVal = sorter.sort(value, midVal);
            if (compareValueToMidVal < 0) {
                right = mid - 1;
            } else if (compareValueToMidVal > 0) {
                left = mid + 1;
            } else {
                return mid;
            }
        } while (left <= right);

        return ((compareValueToMidVal < 0) ? -mid : -(mid+1))-1;
    }
    
    // =========================================================================
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(byte[] source, byte[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(boolean[] source, boolean[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(short[] source, short[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(int[] source, int[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(long[] source, long[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @param tolerance decimal tolerance
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(float[] source, float[] candidate, int offset, int length, float tolerance ){
        return Arrays.equals(candidate, offset, length, source, 0, tolerance);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(float[] source, float[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @param tolerance decimal tolerance
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(double[] source, double[] candidate, int offset, int length, double tolerance){
        return Arrays.equals(candidate, offset, length, source, 0, tolerance);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(double[] source, double[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(Object[] source, Object[] candidate, int offset, int length ){
        return Arrays.equals(candidate, offset, length, source, 0);
    }
    
    // =========================================================================
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(byte[] source, byte[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(boolean[] source, boolean[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(short[] source, short[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(int[] source, int[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(long[] source, long[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param tolerance decimal tolerance
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(float[] source, float[] candidate, float tolerance){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(float[] source, float[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @param tolerance decimal tolerance
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(double[] source, double[] candidate, double tolerance){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(double[] source, double[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    /**
     * Test if the source array starts by the given candidate array.
     * @param source array to test the beginning.
     * @param candidate start array
     * @return true if the source array begins with the given candidate array.
     */
    public static boolean startsWith(Object[] source, Object[] candidate){
        return Arrays.equals(source, 0, candidate.length, candidate, 0);
    }
    
    // =========================================================================
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(byte[] source, byte[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(boolean[] source, boolean[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(short[] source, short[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(int[] source, int[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(long[] source, long[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @param tolerance decimal tolerance
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(float[] source, float[] candidate, int offset, int length, float tolerance){
        return Arrays.equals(candidate, offset, length, source, source.length-length, tolerance);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(float[] source, float[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @param tolerance decimal tolerance
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(double[] source, double[] candidate, int offset, int length, double tolerance){
        return Arrays.equals(candidate, offset, length, source, source.length-length, tolerance);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(double[] source, double[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param offset index where to start tests in the candidate array.
     * @param length of data to test in arrays.
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(Object[] source, Object[] candidate, int offset, int length){
        return Arrays.equals(candidate, offset, length, source, source.length-length);
    }
    
    // =========================================================================
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(byte[] source, byte[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(boolean[] source, boolean[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(short[] source, short[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(int[] source, int[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(long[] source, long[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param tolerance decimal tolerance
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(float[] source, float[] candidate, float tolerance){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length, tolerance);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(float[] source, float[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @param tolerance decimal tolerance
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(double[] source, double[] candidate, double tolerance){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length, tolerance);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(double[] source, double[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    /**
     * Test if the source array ends by the given candidate array.
     * @param source array to test the ending.
     * @param candidate end array
     * @return true if the source array ends with the given candidate array.
     */
    public static boolean endsWith(Object[] source, Object[] candidate){
        return Arrays.equals(candidate, 0, candidate.length, source, source.length-candidate.length);
    }
    
    // =========================================================================
    
    /**
     * Convert float array in a double array.
     * 
     * @param source
     * @return double array
     */
    public static double[] reformatDouble(float[] source){
        final double[] res = new double[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }
        
    /**
     * Convert int array in a double array.
     * 
     * @param source
     * @return double array
     */
    public static double[] reformatToDouble(int[] source){
        final double[] res = new double[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }
    
    /**
     * Convert float array in a double array.
     * 
     * @param source
     * @return double array
     */
    public static double[] reformatToDouble(float[] source){
        final double[] res = new double[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }
    
    
    /**
     * Convert int array in a float array.
     * 
     * @param source
     * @return float array
     */
    public static float[] reformatToFloat(int[] source){
        final float[] res = new float[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }
    
    /**
     * Convert double array in a float array.
     * Warning : loss of precision
     * 
     * @param source
     * @return float array
     */
    public static float[] reformatToFloat(double[] source){
        final float[] res = new float[source.length];
        for(int i=0;i<source.length;i++) res[i] = (float) source[i];
        return res;
    }

    /**
     * Convert int array in a long array.
     * Warning : loss of precision
     *
     * @param source
     * @return long array
     */
    public static long[] reformatToLong(int[] source){
        final long[] res = new long[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }

    /**
     * Convert double array in a int array.
     * Warning : loss of precision
     * 
     * @param source
     * @return int array
     */
    public static int[] reformatToInt(double[] source){
        final int[] res = new int[source.length];
        for(int i=0;i<source.length;i++) res[i] = (int) source[i];
        return res;
    }
    
    /**
     * Convert float array in a int array.
     * Warning : loss of precision
     * 
     * @param source
     * @return int array
     */
    public static int[] reformatToInt(float[] source){
        final int[] res = new int[source.length];
        for(int i=0;i<source.length;i++) res[i] = (int) source[i];
        return res;
    }

    /**
     * Convert long array in a int array.
     *
     * @param source
     * @return int array
     */
    public static int[] reformatToInt(long[] source){
        final int[] res = new int[source.length];
        for(int i=0;i<source.length;i++) res[i] = (int) source[i];
        return res;
    }

    /**
     * Convert short array in a int array.
     *
     * @param source
     * @return int array
     */
    public static int[] reformatToInt(short[] source){
        final int[] res = new int[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }

    /**
     * Convert int array in a double array.
     * 
     * @param source
     * @return double array
     */
    public static int[] reformatToInt(byte[] source){
        final int[] res = new int[source.length];
        for(int i=0;i<source.length;i++) res[i] = source[i];
        return res;
    }

    /**
     * Convert int array in a short array.
     *
     * @param source
     * @return short array
     */
    public static short[] reformatToShort(int[] source){
        final short[] res = new short[source.length];
        for(int i=0;i<source.length;i++) res[i] = (short) source[i];
        return res;
    }

    /**
     * Convert int array in a double array.
     * 
     * @param source
     * @return double array
     */
    public static byte[] reformatToByte(int[] source){
        final byte[] res = new byte[source.length];
        for(int i=0;i<source.length;i++) res[i] = (byte) source[i];
        return res;
    }
    
    // =========================================================================
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(byte[] array) {
//        if (array == null)
//            return 0;
//
//        int result = 1;
//        for (byte element : array)
//            result = 31 * result + element;
//
//        return result;
        
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + array[i];
            }
        }
        return hash;
    }
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(short[] array) {
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + array[i];
            }
        }
        return hash;
    }
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(int[] array) {
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + array[i];
            }
        }
        return hash;
    }
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(long[] array) {
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + array[i];
            }
        }
        return hash;
    }
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(float[] array) {
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + array[i];
            }
        }
        return hash;
    }
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(double[] array) {
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + array[i];
            }
        }
        return hash;
    }
    
    /**
     * Compute an hash value looping on all array values.
     * 
     * @param array Array
     * @return hash value
     */
    public static int computeHash(Object[] array) {
        int hash = 0;
        if (array != null) {
            for (int i=0;i<array.length;i++) {
                hash += 17 * hash + CObjects.getHash(array[i]);
            }
        }
        return hash;
    }
    
    // =========================================================================
    
    /**
     * XOR length elements of source from the index sourceIndex with elements of target from the index targetIndex.
     * The result is put in target from index targetIndex.
     * 
     * @param source source array.
     * @param sourceIndex source offset.
     * @param target target array
     * @param targetIndex target offset.
     * @param length number of elements to XOR.
     */
    public static void arrayXOR ( byte[] source, int sourceIndex, byte[] target, int targetIndex, int length ) {
        for( int is=sourceIndex; is<sourceIndex+length; is++ ) {
            target[targetIndex] = (byte)(target[targetIndex] ^ source[is]);
            targetIndex++;
        }
    }
    
}
