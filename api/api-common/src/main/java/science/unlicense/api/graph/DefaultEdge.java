
package science.unlicense.api.graph;

import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;

/**
 * Default edge implementation.
 *
 * @author Johann Sorel
 */
public class DefaultEdge extends AbstractEdge{

    private final Dictionary properties = new HashDictionary();
    private final Vertex v1;
    private final Vertex v2;

    public DefaultEdge(Vertex v1, Vertex v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public Vertex getVertex1() {
        return v1;
    }

    public Vertex getVertex2() {
        return v2;
    }

    public Dictionary getProperties() {
        return properties;
    }

}
