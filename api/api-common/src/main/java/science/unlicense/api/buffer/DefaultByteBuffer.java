
package science.unlicense.api.buffer;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 * 
 * @author Johann Sorel
 */
public class DefaultByteBuffer extends AbstractBuffer {

    private final byte[] buffer;

    public DefaultByteBuffer(byte[] buffer) {
        this(buffer,Primitive.TYPE_BYTE,NumberEncoding.BIG_ENDIAN);
    }
    
    public DefaultByteBuffer(byte[] buffer, int primitiveType, NumberEncoding encoding) {
        super(primitiveType, encoding, DefaultBufferFactory.INSTANCE);
        this.buffer = buffer;
    }
    
    public DefaultByteBuffer(int nbValue, int primitiveType, NumberEncoding encoding) {
        super(primitiveType, encoding, DefaultBufferFactory.INSTANCE);
        final double size = Primitive.getSizeInBytes(primitiveType)*nbValue;
        this.buffer = new byte[(int)Math.ceil(size)];
    }
    
    public Object getBackEnd() {
        return buffer;
    }

    public boolean isWritable() {
        return true;
    }

    public byte[] toByteArray() {
        return Arrays.copy(buffer);
    }

    public long getBufferByteSize() {
        return buffer.length;
    }

    public byte readByte(long offset) {
        return buffer[(int)offset];
    }

    public int readUByte(long offset) {
        return buffer[(int)offset] & 0xFF;
    }

    public short readShort(long offset) {
        return encoding.readShort(buffer, (int)offset);
    }

    public int readUShort(long offset) {
        return encoding.readUShort(buffer, (int)offset);
    }

    public int readInt24(long offset) {
        return encoding.readInt24(buffer, (int)offset);
    }

    public int readUInt24(long offset) {
        return encoding.readUInt24(buffer, (int)offset);
    }

    public int readInt(long offset) {
        return encoding.readInt(buffer, (int)offset);
    }

    public long readUInt(long offset) {
        return encoding.readUInt(buffer, (int)offset);
    }

    public long readLong(long offset) {
        return encoding.readLong(buffer, (int)offset);
    }

    public float readFloat(long offset) {
        return encoding.readFloat(buffer, (int)offset);
    }

    public double readDouble(long offset) {
        return encoding.readDouble(buffer, (int)offset);
    }

    public void writeByte(byte value, long offset) {
        buffer[(int)offset] = value;
    }
    
    public void writeByte(byte[] array, int arrayOffset, int length, long offset) {
        Arrays.copy(array, arrayOffset, length, buffer, (int)offset);
    }

    public void writeUByte(int value, long offset) {
        buffer[(int)offset] = (byte)value;
    }

    public void writeShort(short value, long offset) {
        encoding.writeShort(value, buffer, (int)offset);
    }

    public void writeUShort(int value, long offset) {
        encoding.writeUShort(value, buffer, (int)offset);
    }

    public void writeInt24(int value, long offset) {
        encoding.writeInt24(value, buffer, (int)offset);
    }

    public void writeUInt24(int value, long offset) {
        encoding.writeUInt24(value, buffer, (int)offset);
    }

    public void writeInt(int value, long offset) {
        encoding.writeInt(value, buffer, (int)offset);
    }

    public void writeUInt(long value, long offset) {
        encoding.writeUInt(value, buffer, (int)offset);
    }

    public void writeLong(long value, long offset) {
        encoding.writeLong(value, buffer, (int)offset);
    }

    public void writeFloat(float value, long offset) {
        encoding.writeFloat(value, buffer, (int)offset);
    }

    public void writeDouble(double value, long offset) {
        encoding.writeDouble(value, buffer, (int)offset);
    }

    public Buffer copy() {
        return new DefaultByteBuffer(Arrays.copy(buffer));
    }

}
