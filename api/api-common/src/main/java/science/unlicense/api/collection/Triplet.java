
package science.unlicense.api.collection;

import science.unlicense.api.CObjects;

/**
 * A triplet is a group of 3 values.
 * 
 * @author Johann Sorel
 */
public class Triplet {
   
    protected Object value1;
    protected Object value2;
    protected Object value3;

    public Triplet(Object value1, Object value2, Object value3) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }
    
    public Triplet(Triplet pair) {
        this.value1 = pair.value1;
        this.value2 = pair.value2;
        this.value3 = pair.value3;
    }

    public Object getValue1() {
        return value1;
    }
    
    public Object getValue2() {
        return value2;
    }
    
    public Object getValue3() {
        return value3;
    }
    
    public boolean equals(final Object candidate) {
        if(!(candidate instanceof Triplet)){
            return false;
        }
        final Triplet other = (Triplet) candidate;
        return CObjects.equals(value1, other.getValue1()) && 
               CObjects.equals(value2, other.getValue2()) && 
               CObjects.equals(value3, other.getValue3());
    }

    public int hashCode() {
        return ((value1 == null) ? 0 : value1.hashCode())
             ^ ((value2 == null) ? 0 : value2.hashCode())
             ^ ((value3 == null) ? 0 : value3.hashCode());
    }

}
