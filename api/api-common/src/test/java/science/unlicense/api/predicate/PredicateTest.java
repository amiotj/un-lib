
package science.unlicense.api.predicate;

import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.predicate.Or;
import science.unlicense.api.predicate.Not;
import science.unlicense.api.predicate.And;
import science.unlicense.api.predicate.Constant;
import org.junit.Test;
import org.junit.Assert;

/**
 * Test constant expressoin class.
 * 
 * @author Johann Sorel
 */
public class PredicateTest {
    
    /**
     * Test constant.
     */
    @Test
    public void constantTest() {
        final Constant constant = new Constant("test");
        
        Assert.assertEquals("test", constant.getValue());
        Assert.assertEquals("test", constant.evaluate(null));
        Assert.assertEquals("test", constant.evaluate("fdsfsd"));
        Assert.assertEquals("test", constant.evaluate("eeee"));
    }

    /**
     * Test TRUE predicate.
     */
    @Test
    public void trueTest() {
        Assert.assertTrue(Predicate.TRUE.evaluate(null));
    }
    
    /**
     * Test FALSE predicate.
     */
    @Test
    public void falseTest() {
        Assert.assertFalse(Predicate.FALSE.evaluate(null));
    }
    
    /**
     * Test Not predicate.
     */
    @Test
    public void notTest() {
        Assert.assertTrue(new Not(Predicate.FALSE).evaluate(null));
        Assert.assertFalse(new Not(Predicate.TRUE).evaluate(null));
    }
    
    /**
     * Test And predicate.
     */
    @Test
    public void andTest() {
        Assert.assertTrue(new And(new Predicate[]{Predicate.TRUE,Predicate.TRUE,Predicate.TRUE}).evaluate(null));
        Assert.assertFalse(new And(new Predicate[]{Predicate.TRUE,Predicate.FALSE,Predicate.TRUE}).evaluate(null));
        Assert.assertFalse(new And(new Predicate[]{Predicate.FALSE,Predicate.FALSE,Predicate.FALSE}).evaluate(null));
    }
    
    /**
     * Test Or predicate.
     */
    @Test
    public void orTest() {
        Assert.assertTrue(new Or(new Predicate[]{Predicate.TRUE,Predicate.TRUE,Predicate.TRUE}).evaluate(null));
        Assert.assertTrue(new Or(new Predicate[]{Predicate.FALSE,Predicate.TRUE,Predicate.FALSE}).evaluate(null));
        Assert.assertFalse(new Or(new Predicate[]{Predicate.FALSE,Predicate.FALSE,Predicate.FALSE}).evaluate(null));
    }

}