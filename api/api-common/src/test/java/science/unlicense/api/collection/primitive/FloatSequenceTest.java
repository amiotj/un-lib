
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class FloatSequenceTest extends PrimitiveSequenceTest {
        
    @Override
    protected PrimitiveSequence createNew() {
        return new FloatSequence();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            1.0f,
            2.0f,
            3.0f,
            4.0f,
            5.0f,
            6.0f,
            7.0f,
            8.0f,
            9.0f,
            10.0f,
            11.0f,
            12.0f,
            13.0f,
            14.0f,
            15.0f
        };
    }
   
}
