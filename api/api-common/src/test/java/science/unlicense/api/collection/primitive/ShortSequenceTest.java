
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.primitive.ShortSequence;
import science.unlicense.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class ShortSequenceTest extends PrimitiveSequenceTest {
        
    @Override
    protected PrimitiveSequence createNew() {
        return new ShortSequence();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            (short)1,
            (short)2,
            (short)3,
            (short)4,
            (short)5,
            (short)6,
            (short)7,
            (short)8,
            (short)9,
            (short)10,
            (short)11,
            (short)12,
            (short)13,
            (short)14,
            (short)15
        };
    }
   
}
