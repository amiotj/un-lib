
package science.unlicense.api.array;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultIntBuffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultIntBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultIntBuffer(new int[nbByte/4]);
    }

}
