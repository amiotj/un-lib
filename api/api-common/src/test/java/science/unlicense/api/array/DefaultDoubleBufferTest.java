
package science.unlicense.api.array;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultDoubleBuffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDoubleBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultDoubleBuffer(new double[nbByte/8]);
    }

}
