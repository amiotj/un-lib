
package science.unlicense.api.number;

import science.unlicense.api.number.Int32;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Int32Test {


    @Test
    public void testIntegerEncoding(){
        Chars cs;
        int i;

        i = 1234560789;
        cs = Int32.encode(i);
        Assert.assertEquals(CharEncodings.US_ASCII, cs.getEncoding());
        Assert.assertArrayEquals(new byte[]{'1','2','3','4','5','6','0','7','8','9'}, cs.toBytes());

        i = -1234560789;
        cs = Int32.encode(i);
        Assert.assertEquals(CharEncodings.US_ASCII, cs.getEncoding());
        Assert.assertArrayEquals(new byte[]{'-','1','2','3','4','5','6','0','7','8','9'}, cs.toBytes());

    }

    @Test
    public void testIntegerEncodingHexa(){
        Chars cs;
        int i;

        i = 115;
        cs = Int32.encodeHexa(i);
        Assert.assertEquals(CharEncodings.US_ASCII, cs.getEncoding());
        Assert.assertArrayEquals(new byte[]{'7','3'}, cs.toBytes());

        i = 255;
        cs = Int32.encodeHexa(i);
        Assert.assertEquals(CharEncodings.US_ASCII, cs.getEncoding());
        Assert.assertArrayEquals(new byte[]{'F','F'}, cs.toBytes());
    }

    @Test
    public void testIntegerDecoding(){
        byte[] array;
        Chars cs;
        int i;

        array = new byte[]{'1','2','3','4','5','6','0','7','8','9'};
        cs = new Chars(array);
        i = Int32.decode(cs);
        Assert.assertEquals(1234560789, i);

        array = new byte[]{'+','1','2','3','4','5','6','0','7','8','9'};
        cs = new Chars(array);
        i = Int32.decode(cs);
        Assert.assertEquals(1234560789, i);

        array = new byte[]{'-','1','2','3','4','5','6','0','7','8','9'};
        cs = new Chars(array);
        i = Int32.decode(cs);
        Assert.assertEquals(-1234560789, i);

    }

    @Test
    public void testIntegerDecodingHexa(){
        byte[] array;
        Chars cs;
        int i;

        array = new byte[]{'7','3'};
        cs = new Chars(array);
        i = Int32.decodeHexa(cs);
        Assert.assertEquals(115, i);

        array = new byte[]{'F','F'};
        cs = new Chars(array);
        i = Int32.decodeHexa(cs);
        Assert.assertEquals(255, i);
    }

    @Test
    public void testSignificantBits(){
        Assert.assertEquals(0, Int32.getSignificantNumberOfNBits(0));
        Assert.assertEquals(1, Int32.getSignificantNumberOfNBits(1));
        Assert.assertEquals(2, Int32.getSignificantNumberOfNBits(2));
        Assert.assertEquals(2, Int32.getSignificantNumberOfNBits(3));
        Assert.assertEquals(8, Int32.getSignificantNumberOfNBits(255));
    }

}
