
package science.unlicense.api.event;

import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.EventMessage;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.predicate.Predicate;

/**
 * Test default event class.
 * 
 * @author Johann Sorel
 */
public class DefaultEventTest {
    
    /**
     * Test creation and base properties.
     */
    @Test
    public void testCreation() {
        Event event = new Event(null, new DefaultEventMessage(true), null);
        Assert.assertEquals(null, event.getSource());
        Assert.assertEquals(true, event.getMessage().isConsumable());
        Assert.assertEquals(null, event.getTrigger());
        
        final EventSource dummy = new EventSource() {
            public Class[] getEventClasses() {
                throw new UnimplementedException("Not supported.");
            }
            public void addEventListener(Predicate predicate, EventListener listener) {
                throw new UnimplementedException("Not supported yet.");
            }
            public void removeEventListener(Predicate predicate, EventListener listener) {
                throw new UnimplementedException("Not supported yet.");
            }
            public EventListener[] getListeners(Predicate predicate) {
                throw new UnimplementedException("Not supported yet.");
            }
        };
        Event trigger = new Event(null, new DefaultEventMessage(true), null);
        event = new Event(dummy, new DefaultEventMessage(false), trigger);
        Assert.assertEquals(dummy, event.getSource());
        Assert.assertEquals(false, event.getMessage().isConsumable());
        Assert.assertEquals(trigger, event.getTrigger());
    }

    /**
     * Test consume.
     */
    @Test
    public void testIsConsumable() {
        EventMessage event = new DefaultEventMessage(true);
        Assert.assertFalse(event.isConsumed());
        event.consume();
        Assert.assertTrue(event.isConsumed());
        //recheck
        event.consume();
        Assert.assertTrue(event.isConsumed());
        
        event = new DefaultEventMessage(false);
        Assert.assertFalse(event.isConsumed());
        event.consume();
        Assert.assertFalse(event.isConsumed());
        
    }

}