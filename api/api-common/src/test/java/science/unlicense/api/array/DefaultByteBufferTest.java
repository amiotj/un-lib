
package science.unlicense.api.array;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultByteBuffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultByteBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultByteBuffer(new byte[nbByte]);
    }

}
