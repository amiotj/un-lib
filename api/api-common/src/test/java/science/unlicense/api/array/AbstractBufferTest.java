
package science.unlicense.api.array;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DataCursor;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBufferTest {

    private static final double DELTA = 0.000001;
    
    public abstract Buffer create(int nbByte);
    
    @Test
    public void rwByteTest(){
        final Buffer buffer = create(16);
        
        final byte[] expected = new byte[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        
        buffer.writeByte(expected,0);
        
        final byte[] res = new byte[16];
        buffer.readByte(res, 0);
        
        Assert.assertArrayEquals(expected, res);
    }
    
    @Test
    public void rwCursorTest(){
        final Buffer buffer = create(16);
        final DataCursor cursor = buffer.cursor();
        cursor.writeFloat(-1);
        cursor.writeFloat(12);
        cursor.writeFloat(21);
        cursor.writeFloat(-46);
        
        Assert.assertEquals(-1, buffer.readFloat(0), DELTA);
        Assert.assertEquals(12, buffer.readFloat(4), DELTA);
        Assert.assertEquals(21, buffer.readFloat(8), DELTA);
        Assert.assertEquals(-46, buffer.readFloat(12), DELTA);
        
    }
    
}
