
package science.unlicense.api.regex;

import science.unlicense.api.regex.Regex;
import science.unlicense.api.regex.RegexExec;
import org.junit.Test;
import org.junit.Assert;
import org.junit.Ignore;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class RegexReplaceTest {

    @Test
    public void replaceSingleChar(){
        
        final Chars text = new Chars("hello world !");
        
        final RegexExec exec = Regex.compile(new Chars(" "));
        final Chars res = exec.replace(text,new Chars("_"));
        Assert.assertEquals(new Chars("hello_world_!"), res);
        
    }

    @Test
    public void replaceGroup(){

        final Chars text = new Chars("aabbbabaabaaaabbbaa");

        final RegexExec exec = Regex.compile(new Chars("aa"));
        final Chars res = exec.replace(text,new Chars("y"));
        Assert.assertEquals(new Chars("ybbbabybyybbby"), res);

    }

    @Test
    public void replaceAmbigiousGroup(){

        Chars text = new Chars("bbbaaab");
        RegexExec exec = Regex.compile(new Chars("aa"));
        Chars res = exec.replace(text,new Chars("y"));
        Assert.assertEquals(new Chars("bbbyab"), res);

        //TODO fix this
        text = new Chars("PropertyClass {\n\n    /");
        exec = Regex.compile(new Chars("( |\n|\r|\t)+"));
        res = exec.replace(text,new Chars(" "));
        Assert.assertEquals(new Chars("PropertyClass { /"), res);
        
    }
    
}
