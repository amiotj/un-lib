
package science.unlicense.api.buffer;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultByteBuffer;
import science.unlicense.api.buffer.DefaultCursor;
import org.junit.Test;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class DefaultCursorTest {

    /**
     * Test cursor correctly end writing on the last buffer bits.
     * This test ensure the cursor to not try to read the next byte
     * when he is on the last one.
     */
    @Test
    public void testWriteLastBits() {

        final Buffer buffer = new DefaultByteBuffer(new byte[2]);
        final DefaultCursor cursor = new DefaultCursor(buffer, NumberEncoding.BIG_ENDIAN);
        cursor.writeBit(0, 4);
        cursor.writeBit(0, 4);
        cursor.writeBit(0, 4);
        cursor.writeBit(0, 4);
        

    }

}
