
package science.unlicense.api.event;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.EventManager;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;

/**
 * Test EventManager class.
 * 
 * @author Johann Sorel
 */
public class EventManagerTest {
    
    /**
     * Test listeners are properly registered and unregistered.
     * TODO API changed, test is not very usefull anymore : readapt it
     */
    @Test
    public void testListenerManagement(){
        final EventManager manager = new EventManager();
        Assert.assertFalse(manager.hasListeners());
        
        final EventListener l1 = new DummyListener();
        final EventListener l2 = new DummyListener();
        final EventListener l3 = new DummyListener();
        
        manager.addEventListener(PropertyMessage.PREDICATE, l1);
        manager.addEventListener(PropertyMessage.PREDICATE, l2);
        manager.addEventListener(DummyMessage.PREDICATE, l1);
        manager.addEventListener(DummyMessage.PREDICATE, l3);
        Assert.assertTrue(manager.hasListeners());
        
        manager.removeEventListener(PropertyMessage.PREDICATE, l1);
        manager.removeEventListener(PropertyMessage.PREDICATE, l2);
        manager.removeEventListener(DummyMessage.PREDICATE, l3);
        //we should still have 1 listeners registered
        Assert.assertTrue(manager.hasListeners());
        manager.removeEventListener(DummyMessage.PREDICATE, l1);
        Assert.assertFalse(manager.hasListeners());
        
        //adding mutliple time the same object as listener should count as one
        manager.addEventListener(DummyMessage.PREDICATE, l3);
        manager.addEventListener(DummyMessage.PREDICATE, l3);
        manager.addEventListener(DummyMessage.PREDICATE, l3);
        Assert.assertTrue(manager.hasListeners());
        manager.removeEventListener(DummyMessage.PREDICATE, l3);
        Assert.assertFalse(manager.hasListeners());
    }
    
    /**
     * Test sending events
     */
    @Test
    public void testSendEvent(){
        
        final EventManager manager = new EventManager();
        final DummyListener l1 = new DummyListener();
        final DummyListener l2 = new DummyListener();
        final DummyListener l3 = new DummyListener();
        
        manager.addEventListener(PropertyMessage.PREDICATE, l1);
        manager.addEventListener(PropertyMessage.PREDICATE, l2);
        manager.addEventListener(DummyMessage.PREDICATE, l1);
        manager.addEventListener(DummyMessage.PREDICATE, l3);
        
        //test property event --------------------------------------------------
        Event event = new Event(null, new PropertyMessage(new Chars("a"), 1, 2));
        manager.sendEvent(event);
        //l1 and l2 should have receive it
        Assert.assertEquals(1, l1.nbEvent);
        Assert.assertEquals(PropertyMessage.class, l1.messageClass);
        Assert.assertEquals(event, l1.event);
        Assert.assertEquals(1, l2.nbEvent);
        Assert.assertEquals(PropertyMessage.class, l2.messageClass);
        Assert.assertEquals(event, l2.event);
        Assert.assertEquals(0, l3.nbEvent);
        Assert.assertEquals(null, l3.messageClass);
        Assert.assertEquals(null, l3.event);
        l1.clear();
        l2.clear();
        l3.clear();
        
        // test dummy event ----------------------------------------------------
        event = new Event(null, new DummyMessage());
        manager.sendEvent(event);
        //l1 and l3 should have receive it
        Assert.assertEquals(1, l1.nbEvent);
        Assert.assertEquals(DummyMessage.class, l1.messageClass);
        Assert.assertEquals(event, l1.event);
        Assert.assertEquals(0, l2.nbEvent);
        Assert.assertEquals(null, l2.messageClass);
        Assert.assertEquals(null, l2.event);
        Assert.assertEquals(1, l3.nbEvent);
        Assert.assertEquals(DummyMessage.class, l3.messageClass);
        Assert.assertEquals(event, l3.event);
        l1.clear();
        l2.clear();
        l3.clear();
                
        //remove listener and test ---------------------------------------------
        manager.removeEventListener(DummyMessage.PREDICATE, l1);
        event = new Event(null, new DummyMessage());
        manager.sendEvent(event);
        //only l3 should have receive it
        Assert.assertEquals(0, l1.nbEvent);
        Assert.assertEquals(null, l1.messageClass);
        Assert.assertEquals(null, l1.event);
        Assert.assertEquals(0, l2.nbEvent);
        Assert.assertEquals(null, l2.messageClass);
        Assert.assertEquals(null, l2.event);
        Assert.assertEquals(1, l3.nbEvent);
        Assert.assertEquals(DummyMessage.class, l3.messageClass);
        Assert.assertEquals(event, l3.event);
        l1.clear();
        l2.clear();
        l3.clear();
        //check it is still registered as a property event
        event = new Event(null, new PropertyMessage(new Chars("a"), 1, 2));
        manager.sendEvent(event);
        //l1 and l2 should have receive it
        Assert.assertEquals(1, l1.nbEvent);
        Assert.assertEquals(PropertyMessage.class, l1.messageClass);
        Assert.assertEquals(event, l1.event);
        Assert.assertEquals(1, l2.nbEvent);
        Assert.assertEquals(PropertyMessage.class, l2.messageClass);
        Assert.assertEquals(event, l2.event);
        Assert.assertEquals(0, l3.nbEvent);
        Assert.assertEquals(null, l3.messageClass);
        Assert.assertEquals(null, l3.event);
        l1.clear();
        l2.clear();
        l3.clear();
    }
    
}
