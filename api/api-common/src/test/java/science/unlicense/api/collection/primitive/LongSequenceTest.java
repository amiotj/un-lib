
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.primitive.LongSequence;
import science.unlicense.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class LongSequenceTest extends PrimitiveSequenceTest {
        
    @Override
    protected PrimitiveSequence createNew() {
        return new LongSequence();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            1l,
            2l,
            3l,
            4l,
            5l,
            6l,
            7l,
            8l,
            9l,
            10l,
            11l,
            12l,
            13l,
            14l,
            15l
        };
    }
   
}
