
package science.unlicense.api.regex;

import science.unlicense.api.regex.Regex;
import science.unlicense.api.regex.RegexExec;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;

/**
 * Regular expression using standard syntax.
 * 
 * @author Johann Sorel
 */
public class RegexStandardTest {
    
    @Test
    public void singleCharTest(){
        
        final Chars exp = new Chars("a");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void escapeCharTest(){
        
        Chars exp = new Chars("\\t");
        RegexExec exec = Regex.compile(exp);
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("\t")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        
        exp = new Chars("\\r");
        exec = Regex.compile(exp);
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("\t")));
        Assert.assertTrue(exec.match(new Chars("\r")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        
        exp = new Chars("\\n");
        exec = Regex.compile(exp);
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("\t")));
        Assert.assertTrue(exec.match(new Chars("\n")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        
        exp = new Chars("\\u0061");
        exec = Regex.compile(exp);
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("\t")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void anyCharTest(){
        final Chars exp = new Chars(".");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("c")));
        Assert.assertFalse(exec.match(new Chars("ab")));
        
        
    }
    
    @Test
    public void listCharTest(){
        
        final Chars exp = new Chars("[ad$]");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("c")));
        Assert.assertTrue(exec.match(new Chars("d")));
        Assert.assertFalse(exec.match(new Chars("e")));
        Assert.assertFalse(exec.match(new Chars("f")));
        Assert.assertTrue(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void listNegateCharTest(){
        
        final Chars exp = new Chars("[^ad$]");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("A")));
        Assert.assertTrue(exec.match(new Chars("b")));
        Assert.assertTrue(exec.match(new Chars("c")));
        Assert.assertFalse(exec.match(new Chars("d")));
        Assert.assertTrue(exec.match(new Chars("e")));
        Assert.assertTrue(exec.match(new Chars("f")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void rangeCharTest(){
        
        final Chars exp = new Chars("[b-e]");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertTrue(exec.match(new Chars("b")));
        Assert.assertTrue(exec.match(new Chars("c")));
        Assert.assertTrue(exec.match(new Chars("d")));
        Assert.assertTrue(exec.match(new Chars("e")));
        Assert.assertFalse(exec.match(new Chars("f")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void rangeNegateCharTest(){
        
        final Chars exp = new Chars("[^b-e]");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("c")));
        Assert.assertFalse(exec.match(new Chars("d")));
        Assert.assertFalse(exec.match(new Chars("e")));
        Assert.assertTrue(exec.match(new Chars("f")));
        Assert.assertTrue(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void caseSensitiveWordTest(){
        
        final Chars exp = new Chars("'abc'");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertTrue(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("Abc")));
        Assert.assertFalse(exec.match(new Chars("ABC")));
        Assert.assertFalse(exec.match(new Chars("abC")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("Abcd")));
    }
    
    @Test
    public void caseInsensitiveWordTest(){
        
        final Chars exp = new Chars("\"abc\"");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertTrue(exec.match(new Chars("abc")));
        Assert.assertTrue(exec.match(new Chars("Abc")));
        Assert.assertTrue(exec.match(new Chars("ABC")));
        Assert.assertTrue(exec.match(new Chars("abC")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("Abcd")));
    }
    
    @Test
    public void zeroOneTest(){
        
        final Chars exp = new Chars("a?");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertTrue(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void zeroManyTest(){
        
        final Chars exp = new Chars("a*");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertTrue(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("aa")));
        Assert.assertTrue(exec.match(new Chars("aaaaaaa")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void oneManyTest(){
        
        final Chars exp = new Chars("a+");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("aa")));
        Assert.assertTrue(exec.match(new Chars("aaaaaaa")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void concatenationTest(){
        
        final Chars exp = new Chars("abcd");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void choiceTest(){
        
        final Chars exp = new Chars("a|b");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertTrue(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void choiceLongTest(){
        
        final Chars exp = new Chars("abcd|efgh");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
        Assert.assertTrue(exec.match(new Chars("efgh")));
    }
    
    @Test
    public void groupTest(){
        
        final Chars exp = new Chars("(((abcd)))");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void groupZeroOneTest(){
        
        final Chars exp = new Chars("(ab)?cd");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertTrue(exec.match(new Chars("cd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("ababcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void groupZeroManyTest(){
        
        final Chars exp = new Chars("(ab)*cd");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertTrue(exec.match(new Chars("cd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertTrue(exec.match(new Chars("ababcd")));
        Assert.assertTrue(exec.match(new Chars("abababcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void groupOneManyTest(){
        
        final Chars exp = new Chars("(ab)+cd");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("cd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertTrue(exec.match(new Chars("ababcd")));
        Assert.assertTrue(exec.match(new Chars("abababcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void numberTest(){
        final Chars exp = new Chars("(-|\\+)?[0-9]+(.[0-9]+)?((e|E)(-|\\+)?[0-9]+)?");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("cd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertTrue(exec.match(new Chars("5")));
        Assert.assertTrue(exec.match(new Chars("-5")));
        Assert.assertTrue(exec.match(new Chars("+5")));
        Assert.assertTrue(exec.match(new Chars("213")));
        Assert.assertTrue(exec.match(new Chars("-213")));
        Assert.assertTrue(exec.match(new Chars("+213")));
        Assert.assertTrue(exec.match(new Chars("14.45")));
        Assert.assertTrue(exec.match(new Chars("-14.45")));
        Assert.assertTrue(exec.match(new Chars("+14.45")));
        Assert.assertTrue(exec.match(new Chars("14e10")));
        Assert.assertTrue(exec.match(new Chars("14E10")));
        Assert.assertTrue(exec.match(new Chars("14E-10")));
        Assert.assertTrue(exec.match(new Chars("14.36E+10")));
        Assert.assertTrue(exec.match(new Chars("-14.36e-10")));
        
        
    }
    
    @Test
    public void escapeStringTest(){
        final Chars exp = new Chars("\\\"[^\\\"]*\\\"");
        final RegexExec exec = Regex.compile(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        
        
    }
    
}
