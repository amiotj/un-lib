
package science.unlicense.api.doc;

import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.DocMessage;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;

/**
 * Test Document events.
 * 
 * @author Johann Sorel
 */
public class DocumentEventTest {
    
    private static final Chars NAME = new Chars("testField");
    
    @Test
    public void singleValueTest(){
        
        final EventCollector collector = new EventCollector();
        
        final DefaultDocument doc = new DefaultDocument();
        doc.addEventListener(DocMessage.PREDICATE, collector);
        
        //event setting value
        doc.getField(NAME).setValue(15);
        Assert.assertEquals(1,collector.events.getSize());
        DocMessage event = (DocMessage)((Event)collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        DocMessage.FieldChange change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(null, change.getOldValue());
        Assert.assertEquals(15, change.getNewValue());
        collector.reset();
        
        //no event setting the same value again
        doc.getField(NAME).setValue(15);
        Assert.assertEquals(0,collector.events.getSize());
        collector.reset();
        
        //change value
        doc.getField(NAME).setValue(22);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage)((Event)collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(15, change.getOldValue());
        Assert.assertEquals(22, change.getNewValue());
        collector.reset();
        
        //set to null
        doc.getField(NAME).setValue(null);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage)((Event)collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(22, change.getOldValue());
        Assert.assertEquals(null, change.getNewValue());
        collector.reset();
    }
    
    @Test
    public void docValueTest(){
        
        final EventCollector collector = new EventCollector();
        
        final Document val1 = new DefaultDocument();
        val1.getField(NAME).setValue(10);
        final Document val2 = new DefaultDocument();
        val2.getField(NAME).setValue(10);
        final Document val3 = new DefaultDocument();
        val3.getField(NAME).setValue(20);
        
        final DefaultDocument doc = new DefaultDocument();
        doc.addEventListener(DocMessage.PREDICATE, collector);
        
        //event setting value
        doc.getField(NAME).setValue(val1);
        Assert.assertEquals(1,collector.events.getSize());
        DocMessage event = (DocMessage)((Event)collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        DocMessage.FieldChange change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(null, change.getOldValue());
        Assert.assertEquals(val1, change.getNewValue());
        collector.reset();
        
        //no event setting the equal value again
        doc.getField(NAME).setValue(val2);
        Assert.assertEquals(0,collector.events.getSize());
        collector.reset();
        
        //change value
        doc.getField(NAME).setValue(val3);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage)((Event)collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(val1, change.getOldValue());
        Assert.assertEquals(val3, change.getNewValue());
        collector.reset();
        
        //set to null
        doc.getField(NAME).setValue(null);
        Assert.assertEquals(1,collector.events.getSize());
        event = (DocMessage)((Event)collector.events.get(0)).getMessage();
        Assert.assertEquals(1,event.getChanges().getSize());
        change = (DocMessage.FieldChange) event.getChanges().createIterator().next();
        Assert.assertEquals(NAME, change.getId());
        Assert.assertEquals(val3, change.getOldValue());
        Assert.assertEquals(null, change.getNewValue());
        collector.reset();
    }
    
    private static class EventCollector implements EventListener{
        
        private final Sequence events = new ArraySequence();

        public void reset(){
            events.removeAll();
        }
        
        @Override
        public void receiveEvent(Event event) {
            events.add(event);
        }
        
    }
    
}
