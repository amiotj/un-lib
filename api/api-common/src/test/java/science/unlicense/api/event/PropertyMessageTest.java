
package science.unlicense.api.event;

import science.unlicense.api.event.PropertyMessage;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;

/**
 * Test property event class.
 * 
 * @author Johann Sorel
 */
public class PropertyMessageTest {
    
    /**
     * Test event properties
     */
    @Test
    public void testPropertyEvent() {
        final PropertyMessage event = new PropertyMessage(new Chars("name"), 14, 21);
        Assert.assertEquals(new Chars("name"), event.getPropertyName());
        Assert.assertEquals(14, event.getOldValue());
        Assert.assertEquals(21, event.getNewValue());
        
    }


}