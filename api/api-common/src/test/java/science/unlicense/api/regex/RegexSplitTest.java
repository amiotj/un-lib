
package science.unlicense.api.regex;

import science.unlicense.api.regex.Regex;
import science.unlicense.api.regex.RegexExec;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class RegexSplitTest {

    @Test
    public void splitSpaceTest(){
        
        final Chars text = new Chars("hello world !");
        
        final RegexExec exec = Regex.compile(new Chars(" "));
        final Chars[] split = exec.split(text);
        Assert.assertEquals(3, split.length);
        Assert.assertEquals(new Chars("hello"), split[0]);
        Assert.assertEquals(new Chars("world"), split[1]);
        Assert.assertEquals(new Chars("!"), split[2]);
        
    }
    
    @Test
    public void splitGroupeTest(){
        
        final Chars text = new Chars("yellow and blue or green and white");
        
        final RegexExec exec = Regex.compile(new Chars(" (and|or) "));
        final Chars[] split = exec.split(text);
        Assert.assertEquals(4, split.length);
        Assert.assertEquals(new Chars("yellow"), split[0]);
        Assert.assertEquals(new Chars("blue"), split[1]);
        Assert.assertEquals(new Chars("green"), split[2]);
        Assert.assertEquals(new Chars("white"), split[3]);
        
    }
    
    @Test
    public void replaceWhitespaces(){
        
        final Chars text = new Chars("tes  \n hello\n\t\t World   !");
        
        final RegexExec exec = Regex.compile(new Chars("( |\n|\r|\t)+"));
        final Chars[] split = exec.split(text);
        Assert.assertEquals(4, split.length);
        Assert.assertEquals(new Chars("tes"), split[0]);
        Assert.assertEquals(new Chars("hello"), split[1]);
        Assert.assertEquals(new Chars("World"), split[2]);
        Assert.assertEquals(new Chars("!"), split[3]);
        
        
    }
    
}
