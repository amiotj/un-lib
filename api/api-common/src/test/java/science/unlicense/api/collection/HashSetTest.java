
package science.unlicense.api.collection;

import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;

/**
 * Test sequence.
 * 
 * @author Johann Sorel
 */
public class HashSetTest extends CollectionTest {

    @Override
    protected Set createNew() {
        return new HashSet();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "1",
            "2",
            "3"
        };
    }
       
}
