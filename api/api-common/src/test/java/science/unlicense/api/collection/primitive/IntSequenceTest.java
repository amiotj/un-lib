
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class IntSequenceTest extends PrimitiveSequenceTest {
        
    @Override
    protected PrimitiveSequence createNew() {
        return new IntSequence();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        };
    }
   
}
