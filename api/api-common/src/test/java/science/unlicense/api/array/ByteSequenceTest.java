
package science.unlicense.api.array;

import science.unlicense.api.collection.primitive.ByteSequence;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Johann Sorel
 */
public class ByteSequenceTest {

    @Test
    public void growTest(){
        final ByteSequence buffer = new ByteSequence();
        Assert.assertEquals(0, buffer.getSize());

        buffer.put((byte)1);
        Assert.assertEquals(1, buffer.getSize());

        buffer.skip();
        Assert.assertEquals(2, buffer.getSize());

        buffer.put(new byte[50]);
        Assert.assertEquals(52, buffer.getSize());

        buffer.put(new byte[1200]);
        Assert.assertEquals(1252, buffer.getSize());

    }

}
