
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.primitive.DoubleSequence;
import science.unlicense.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class DoubleSequenceTest extends PrimitiveSequenceTest {
        
    @Override
    protected PrimitiveSequence createNew() {
        return new DoubleSequence();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            1.0,
            2.0,
            3.0,
            4.0,
            5.0,
            6.0,
            7.0,
            8.0,
            9.0,
            10.0,
            11.0,
            12.0,
            13.0,
            14.0,
            15.0
        };
    }
   
}
