
package science.unlicense.api.collection.primitive;

import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.collection.primitive.PrimitiveSequence;

/**
 *
 * @author Johann Sorel
 */
public class ByteSequenceTest extends PrimitiveSequenceTest {
        
    @Override
    protected PrimitiveSequence createNew() {
        return new ByteSequence();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            (byte)1,
            (byte)2,
            (byte)3,
            (byte)4,
            (byte)5,
            (byte)6,
            (byte)7,
            (byte)8,
            (byte)9,
            (byte)10,
            (byte)11,
            (byte)12,
            (byte)13,
            (byte)14,
            (byte)15
        };
    }
   
}
