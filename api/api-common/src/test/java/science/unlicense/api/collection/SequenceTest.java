
package science.unlicense.api.collection;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import org.junit.Test;
import org.junit.Assert;

/**
 * Test sequence.
 * 
 * @author Johann Sorel
 */
public abstract class SequenceTest extends CollectionTest{
   
    /**
     * Create a new sequence instance.
     * @return 
     */
    protected abstract Sequence createNew();
        
    /**
     * Manipulation tests.
     */
    @Test
    public void testModification(){
        Object[] samples = sampleValues();
        
        Sequence seq = createNew();
        seq.add(samples[0]);
        seq.add(samples[1]);
        seq.add(samples[2]);
        seq.add(samples[3]);
        seq.add(samples[4]);
        seq.add(samples[5]);
        seq.add(samples[6]);
        seq.add(samples[7]);
        seq.add(samples[8]);
        seq.add(samples[9]);
        seq.add(samples[10]);
        seq.add(samples[11]);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(12, seq.getSize());   
        
        seq.add(0,samples[12]);
        seq.add(2,samples[13]);
        seq.add(14,samples[14]);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(15, seq.getSize());  
        Assert.assertEquals(samples[12], seq.get(0));
        Assert.assertEquals(samples[0], seq.get(1));
        Assert.assertEquals(samples[13], seq.get(2));
        Assert.assertEquals(samples[1], seq.get(3));
        Assert.assertEquals(samples[2], seq.get(4));
        Assert.assertEquals(samples[3], seq.get(5));
        Assert.assertEquals(samples[4], seq.get(6));
        Assert.assertEquals(samples[5], seq.get(7));
        Assert.assertEquals(samples[6], seq.get(8));
        Assert.assertEquals(samples[7], seq.get(9));
        Assert.assertEquals(samples[8], seq.get(10));
        Assert.assertEquals(samples[9], seq.get(11));
        Assert.assertEquals(samples[10], seq.get(12));
        Assert.assertEquals(samples[11], seq.get(13));
        Assert.assertEquals(samples[14], seq.get(14));
        
        //test iterator
        Iterator ite = seq.createIterator();
        int cnt = 0;
        while(ite.hasNext()){
            Assert.assertEquals(seq.get(cnt), ite.next());
            cnt++;
        }
        Assert.assertEquals(15, cnt);
        
        //test reverse iterator
        ite = seq.createReverseIterator();
        cnt = 15;
        while(ite.hasNext()){
            Assert.assertEquals(seq.get(cnt-1), ite.next());
            cnt--;
        }
        Assert.assertEquals(0, cnt);
        
        
        //removing elements
        seq.remove(13);
        seq.remove(11);
        seq.remove(9);
        seq.remove(5);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(11, seq.getSize());  
        Assert.assertEquals(samples[12], seq.get(0));
        Assert.assertEquals(samples[0], seq.get(1));
        Assert.assertEquals(samples[13], seq.get(2));
        Assert.assertEquals(samples[1], seq.get(3));
        Assert.assertEquals(samples[2], seq.get(4));
        Assert.assertEquals(samples[4], seq.get(5));
        Assert.assertEquals(samples[5], seq.get(6));
        Assert.assertEquals(samples[6], seq.get(7));
        Assert.assertEquals(samples[8], seq.get(8));
        Assert.assertEquals(samples[10], seq.get(9));
        Assert.assertEquals(samples[14], seq.get(10));
        
        //cleaning
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
        Assert.assertEquals(0, seq.getSize());
        
    }
    
    /**
     * Manipulation tests.
     */
    @Test
    public void testModifyGroup(){
        Object[] samples = sampleValues();
        
        Sequence seq = createNew();
        seq.addAll(samples);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(samples.length, seq.getSize());           
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
        
        seq.addAll(0,samples);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(samples.length, seq.getSize());   
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
        
        seq.addAll(new ArraySequence(samples));
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(samples.length, seq.getSize());   
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
        
        seq.addAll(0,new ArraySequence(samples));
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(samples.length, seq.getSize());   
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
        
        
        //partial insert
        final Object[] parts = {samples[2],samples[3],samples[4]};
        seq.add(samples[0]);
        seq.add(samples[1]);
        seq.add(samples[5]);
        seq.addAll(2, parts);
        Assert.assertEquals(6, seq.getSize());   
        Assert.assertEquals(samples[0], seq.get(0));
        Assert.assertEquals(samples[1], seq.get(1));
        Assert.assertEquals(samples[2], seq.get(2));
        Assert.assertEquals(samples[3], seq.get(3));
        Assert.assertEquals(samples[4], seq.get(4));
        Assert.assertEquals(samples[5], seq.get(5));
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
     
        seq.add(samples[0]);
        seq.add(samples[1]);
        seq.add(samples[5]);
        seq.addAll(2, new ArraySequence(parts));
        Assert.assertEquals(6, seq.getSize());   
        Assert.assertEquals(samples[0], seq.get(0));
        Assert.assertEquals(samples[1], seq.get(1));
        Assert.assertEquals(samples[2], seq.get(2));
        Assert.assertEquals(samples[3], seq.get(3));
        Assert.assertEquals(samples[4], seq.get(4));
        Assert.assertEquals(samples[5], seq.get(5));
        seq.removeAll();
        Assert.assertTrue(seq.isEmpty());
        
    }
    
    
    @Test
    public void testNullIndex(){
        Sequence sequence = createNew();
        int index = sequence.search(null);
        Assert.assertTrue(index<0);
        
    }
    
    
}
