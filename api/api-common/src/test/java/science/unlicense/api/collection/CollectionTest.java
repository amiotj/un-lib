
package science.unlicense.api.collection;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.CollectionMessage;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;

/**
 * Test collection implementation.
 * TODO test all methods
 * 
 * @author Johann Sorel
 */
public abstract class CollectionTest {
   
    /**
     * Create a new sequence instance.
     * @return 
     */
    protected abstract Collection createNew();
    
    /**
     * Create test values, they must all be different and size = 15.
     * @return 
     */
    protected abstract Object[] sampleValues();
    
    /**
     * Manipulation tests.
     */
    @Test
    public void testAddAndRemove(){
        Object[] samples = sampleValues();
        
        Collection col = createNew();
        Assert.assertTrue(col.isEmpty());
        
        col.add(samples[0]);
        col.add(samples[1]);
        col.add(samples[2]);
        col.add(samples[3]);
        col.add(samples[4]);
        col.add(samples[5]);
        col.add(samples[6]);
        col.add(samples[7]);
        col.add(samples[8]);
        col.add(samples[9]);
        Assert.assertFalse(col.isEmpty());
        Assert.assertEquals(10, col.getSize());   
        
        col.remove(samples[1]);
        Assert.assertFalse(col.isEmpty());
        Assert.assertEquals(9, col.getSize());   
        Assert.assertTrue(col.contains(samples[0]));
        Assert.assertFalse(col.contains(samples[1]));
        Assert.assertTrue(col.contains(samples[2]));
        Assert.assertTrue(col.contains(samples[3]));
        Assert.assertTrue(col.contains(samples[4]));
        Assert.assertTrue(col.contains(samples[5]));
        Assert.assertTrue(col.contains(samples[6]));
        Assert.assertTrue(col.contains(samples[7]));
        Assert.assertTrue(col.contains(samples[8]));
        Assert.assertTrue(col.contains(samples[9]));
        
        
        //test iterator
        Iterator ite = col.createIterator();
        int cnt = 0;
        while(ite.hasNext()){
            Object next = ite.next();
            Assert.assertNotNull(next);
            cnt++;
        }
        Assert.assertEquals(9, cnt);
        
        //cleaning
        col.removeAll();
        Assert.assertTrue(col.isEmpty());
        Assert.assertEquals(0, col.getSize());
        
    }
        
    @Test
    public void testEvents(){
        final Object[] samples = sampleValues();
        final Collection col = createNew();
        final Collector listener = new Collector();
        
        col.addEventListener(CollectionMessage.PREDICATE, listener);
        
        //add one object
        col.add(samples[0]);
        Assert.assertEquals(1, listener.nbEvent);
        Assert.assertEquals(CollectionMessage.TYPE_ADD, listener.lastEvent.getType());
        Assert.assertEquals(0,listener.lastEvent.getOldElements().length);
        Assert.assertNotNull(null, listener.lastEvent.getNewElements());
        Assert.assertEquals(1, listener.lastEvent.getNewElements().length);
        Assert.assertEquals(samples[0], listener.lastEvent.getNewElements()[0]);
        listener.reset();
        
        //remove one object
        col.remove(samples[0]);
        Assert.assertEquals(1, listener.nbEvent);
        Assert.assertEquals(CollectionMessage.TYPE_REMOVE, listener.lastEvent.getType());
        Assert.assertEquals(0,listener.lastEvent.getNewElements().length);
        Assert.assertNotNull(null, listener.lastEvent.getOldElements());
        Assert.assertEquals(1, listener.lastEvent.getOldElements().length);
        Assert.assertEquals(samples[0], listener.lastEvent.getOldElements()[0]);
        listener.reset();
        
        //remove multiple objects
        col.add(samples[0]);
        col.add(samples[1]);
        listener.reset();
        col.removeAll();
        Assert.assertEquals(1, listener.nbEvent);
        Assert.assertEquals(CollectionMessage.TYPE_REMOVE, listener.lastEvent.getType());
        Assert.assertEquals(0,listener.lastEvent.getNewElements().length);
        Assert.assertNotNull(null, listener.lastEvent.getOldElements());
        Assert.assertEquals(2, listener.lastEvent.getOldElements().length);
        Assert.assertEquals(samples[0], listener.lastEvent.getOldElements()[0]);
        Assert.assertEquals(samples[1], listener.lastEvent.getOldElements()[1]);
        listener.reset();
        
    }
    
    private class Collector implements EventListener{

        int nbEvent = 0;
        CollectionMessage lastEvent;
        
        void reset(){
            nbEvent = 0;
            lastEvent = null;
        }
        
        @Override
        public void receiveEvent(Event event) {
            nbEvent++;
            this.lastEvent = (CollectionMessage) event.getMessage();
        }
        
    }
    
}
