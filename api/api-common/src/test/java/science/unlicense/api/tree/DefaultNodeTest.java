
package science.unlicense.api.tree;

import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.DummyListener;
import org.junit.Test;
import org.junit.Assert;

/**
 * Test default node class.
 *
 * @author Johann Sorel
 */
public class DefaultNodeTest {

    /**
     * Test creation and base properties.
     */
    @Test
    public void testCreation() {

        DefaultNode node = new DefaultNode(true);
        Assert.assertEquals(0, node.getChildren().getSize());

        final DefaultNode child1 = new DefaultNode(true);
        final DefaultNode child2 = new DefaultNode(true);
        final Node[] children = new Node[]{
            child1,child2
        };
        node = new DefaultNode(children);
        Assert.assertEquals(2, node.getChildren().getSize());
        Assert.assertEquals(child1, node.getChildren().toArray()[0]);
        Assert.assertEquals(child2, node.getChildren().toArray()[1]);
    }

    /**
     * Test adding and removing childrens.
     */
    @Test
    public void testChildrenManagement() {
        final DummyListener listener = new DummyListener();

        final DefaultNode node = new DefaultNode(true);
        node.addEventListener(NodeMessage.PREDICATE, listener);

        final Node child1 = new DefaultNode(true);
        final Node child2 = new DefaultNode(true);
        final Node child3 = new DefaultNode(true);
        final Node child4 = new DefaultNode(true);

        //test add single node--------------------------------------------------
        node.getChildren().add(child1);
        Assert.assertEquals(1, node.getChildren().getSize());
        Assert.assertEquals(child1, node.getChildren().toArray()[0]);
        //check listener
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,new NodeMessage(NodeMessage.TYPE_ADD, 0, 1, null, new Node[]{child1})), listener.event);
        listener.clear();

        //test removing by index -----------------------------------------------
        node.getChildren().remove(0);
        Assert.assertEquals(0, node.getChildren().getSize());
        //check listener
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,new NodeMessage(NodeMessage.TYPE_REMOVE, 0, 1, new Node[]{child1}, null)), listener.event);
        listener.clear();

        //test add multiple nodes ----------------------------------------------
        node.getChildren().addAll(new Node[]{child1,child2,child3});
        Assert.assertEquals(3, node.getChildren().getSize());
        Assert.assertEquals(child1, node.getChildren().toArray()[0]);
        Assert.assertEquals(child2, node.getChildren().toArray()[1]);
        Assert.assertEquals(child3, node.getChildren().toArray()[2]);
        //check listener
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,new NodeMessage(NodeMessage.TYPE_ADD, 0, 3, null, new Node[]{child1,child2,child3})), listener.event);
        listener.clear();

        //test add single node at index-----------------------------------------
        node.getChildren().add(2, child4);
        Assert.assertEquals(4, node.getChildren().getSize());
        Assert.assertEquals(child1, node.getChildren().toArray()[0]);
        Assert.assertEquals(child2, node.getChildren().toArray()[1]);
        Assert.assertEquals(child4, node.getChildren().toArray()[2]);
        Assert.assertEquals(child3, node.getChildren().toArray()[3]);
        //check listener
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,new NodeMessage(NodeMessage.TYPE_ADD, 2, 3, null, new Node[]{child4})), listener.event);
        listener.clear();

        //test removing by node ------------------------------------------------
        node.getChildren().remove(child2);
        Assert.assertEquals(3, node.getChildren().getSize());
        Assert.assertEquals(child1, node.getChildren().toArray()[0]);
        Assert.assertEquals(child4, node.getChildren().toArray()[1]);
        Assert.assertEquals(child3, node.getChildren().toArray()[2]);
        //check listener
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,new NodeMessage(NodeMessage.TYPE_REMOVE, 1, 2, new Node[]{child2}, null)), listener.event);
        listener.clear();
    }


    /**
     * Test child events fall throught.
     */
    @Test
    public void testFallEvent() {
        final DummyListener listener = new DummyListener();

        final DefaultNode node = new DefaultNode(true);
        final DefaultNode child = new DefaultNode(true);
        final DefaultNode subchild = new DefaultNode(true);

        node.getChildren().add(child);


        node.addEventListener(NodeMessage.PREDICATE, listener);

        //test child add single node--------------------------------------------
        child.getChildren().add(subchild);
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,
                    new NodeMessage(NodeMessage.TYPE_REPLACE,0, 1, null,new Node[]{child}),
                    new Event(child,new NodeMessage(NodeMessage.TYPE_ADD, 0, 1, null,new Node[]{subchild}))
                    ),
                listener.event);
        listener.clear();

        //test child removing by index -----------------------------------------
        child.getChildren().remove(0);
        //check listener
        Assert.assertEquals(1,listener.nbEvent);
        Assert.assertEquals(NodeMessage.class, listener.messageClass);
        Assert.assertEquals(new Event(node,
                    new NodeMessage(NodeMessage.TYPE_REPLACE,0, 1, null,new Node[]{child}),
                    new Event(child,new NodeMessage(NodeMessage.TYPE_REMOVE, 0, 1, new Node[]{subchild},null))),
                listener.event);
        listener.clear();


        //remove child, we should not receive anymore events -------------------
        node.getChildren().remove(child);
        listener.clear();

        child.getChildren().add(subchild);
        Assert.assertEquals(0,listener.nbEvent);
        Assert.assertEquals(null, listener.messageClass);
        Assert.assertEquals(null, listener.event);

    }

}