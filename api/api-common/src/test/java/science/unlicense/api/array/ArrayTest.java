package science.unlicense.api.array;

import science.unlicense.api.array.Arrays;
import org.junit.Test;

import org.junit.Assert;
import science.unlicense.api.Orderable;
import science.unlicense.api.Sorter;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;

// ========== Helper classes to test Object sort ===============================
/**
 * @author Bertrand COTE
 */
class OrderableObject implements Orderable {

    private final byte value;

    OrderableObject(byte value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    public byte getValue() {
        return value;
    }

    @Override
    public int order(Object other) {
        if (other instanceof OrderableObject) {
            if (this.value < ((OrderableObject) other).getValue()) {
                return -1;
            } else if (this.value == ((OrderableObject) other).getValue()) {
                return 0;
            } else {
                return 1;
            }
        } else {
            throw new InvalidArgumentException();
        }
    }

    @Override
    public String toString() {
        return "<" + this.value + ">";
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof OrderableObject)) {
            return false;
        }
        return this.value == ((OrderableObject) obj).getValue();
    }
}

/**
 * @author Bertrand COTE
 */
class OrderableObjectSorter implements Sorter {

    @Override
    public int sort(Object first, Object second) {
        return -(((OrderableObject) first).order(((OrderableObject) second)));
    }
}

// =============================================================================
/**
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class ArrayTest {

    @Test
    public void testRemove() {
        int[] values = new int[]{1, 2, 3, 4, 5};
        values = Arrays.remove(values, 2);
        Assert.assertArrayEquals(new int[]{1, 2, 4, 5}, values);
    }

    @Test
    public void testRemoveWithin() {
        int[] values = new int[]{1, 2, 3, 4, 5};
        int r = Arrays.removeWithin(values, 2);
        Assert.assertEquals(3, r);
        Assert.assertArrayEquals(new int[]{1, 2, 4, 5, 0}, values);
    }

//    @Test
// 	public void testStringSearch() {
// 		Assert.assertEquals(-1, Arrays.search("Billy", Arrays.asList("Anny","Emmy","Grammy")));
// 	}
    @Test
    public void testByteBinarySearch() {
        Assert.assertEquals(0, Arrays.binarySearch((byte) 1, new byte[]{1, 3, 4}));
        Assert.assertEquals(2, Arrays.binarySearch((byte) 4, new byte[]{1, 3, 4}));
        Assert.assertEquals(-1, Arrays.binarySearch((byte) -5, new byte[]{1, 3, 4}));
        Assert.assertEquals(-1, Arrays.binarySearch((byte) 0, new byte[]{1, 3, 4}));
        Assert.assertEquals(-2, Arrays.binarySearch((byte) 2, new byte[]{1, 3, 4}));
        Assert.assertEquals(-3, Arrays.binarySearch((byte) 4, new byte[]{1, 3, 5}));
        Assert.assertEquals(-4, Arrays.binarySearch((byte) 9, new byte[]{1, 3, 5}));
    }

    @Test
    public void testIntBinarySearch() {
        Assert.assertEquals(0, Arrays.binarySearch(1, new int[]{1, 3, 4}));
        Assert.assertEquals(2, Arrays.binarySearch(4, new int[]{1, 3, 4}));
        Assert.assertEquals(-1, Arrays.binarySearch(-5, new int[]{1, 3, 4}));
        Assert.assertEquals(-1, Arrays.binarySearch(0, new int[]{1, 3, 4}));
        Assert.assertEquals(-2, Arrays.binarySearch(2, new int[]{1, 3, 4}));
        Assert.assertEquals(-3, Arrays.binarySearch(4, new int[]{1, 3, 5}));
        Assert.assertEquals(-4, Arrays.binarySearch(9, new int[]{1, 3, 5}));
    }

    // =========================================================================
    // ========== Helper functions =============================================
    public static boolean[] byteArrayToBooleanArray(byte[] a) {
        boolean[] rep = new boolean[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = a[i] > 0;
        }
        return rep;
    }

    public static boolean[][] byteArrayArrayToBooleanArrayArray(byte[][] a) {
        boolean[][] rep = new boolean[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new boolean[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = a[i][j] > 0;
            }
        }
        return rep;
    }

    public static char[] byteArrayToCharArray(byte[] a) {
        char[] rep = new char[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (char) a[i];
        }
        return rep;
    }

    public static char[][] byteArrayArrayToCharArrayArray(byte[][] a) {
        char[][] rep = new char[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new char[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (char) a[i][j];
            }
        }
        return rep;
    }

    public static short[] byteArrayToShortArray(byte[] a) {
        short[] rep = new short[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (short) a[i];
        }
        return rep;
    }

    public static short[][] byteArrayArrayToShortArrayArray(byte[][] a) {
        short[][] rep = new short[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new short[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (short) a[i][j];
            }
        }
        return rep;
    }

    public static int[] byteArrayToIntArray(byte[] a) {
        int[] rep = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (int) a[i];
        }
        return rep;
    }

    public static int[][] byteArrayArrayToIntArrayArray(byte[][] a) {
        int[][] rep = new int[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new int[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (int) a[i][j];
            }
        }
        return rep;
    }

    public static long[] byteArrayToLongArray(byte[] a) {
        long[] rep = new long[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (long) a[i];
        }
        return rep;
    }

    public static long[][] byteArrayArrayToLongArrayArray(byte[][] a) {
        long[][] rep = new long[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new long[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (long) a[i][j];
            }
        }
        return rep;
    }

    public static float[] byteArrayToFloatArray(byte[] a) {
        float[] rep = new float[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (float) a[i];
        }
        return rep;
    }

    public static float[][] byteArrayArrayToFloatArrayArray(byte[][] a) {
        float[][] rep = new float[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new float[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (float) a[i][j];
            }
        }
        return rep;
    }

    public static double[] byteArrayToDoubleArray(byte[] a) {
        double[] rep = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (double) a[i];
        }
        return rep;
    }

    public static double[][] byteArrayArrayToDoubleArrayArray(byte[][] a) {
        double[][] rep = new double[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new double[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (double) a[i][j];
            }
        }
        return rep;
    }

    public static Byte[] byteArrayToByteArray(byte[] a) {
        Byte[] rep = new Byte[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = (Byte) a[i];
        }
        return rep;
    }

    public static Byte[][] byteArrayArrayToByteArrayArray(byte[][] a) {
        Byte[][] rep = new Byte[a.length][];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new Byte[a[i].length];
            for (int j = 0; j < a[i].length; j++) {
                rep[i][j] = (Byte) a[i][j];
            }
        }
        return rep;
    }

    public static OrderableObject[] byteArrayToOrderableObjectArray(byte[] a) {
        OrderableObject[] rep = new OrderableObject[a.length];
        for (int i = 0; i < a.length; i++) {
            rep[i] = new OrderableObject(a[i]);
        }
        return rep;
    }

    // =========================================================================
    // =========================================================================
    /**
     * data byte arrays.
     */
    static final byte[][] data = new byte[][]{
        {1, -1, 2},
        {-44, -94, -22, -34, 43, -95, -42, 22, 125, 82, 22, -10},
        {79, 74, -105, 95, -102, -68, 17, 59, -64, -1, 115, -117},
        {-127, -99, 115, 2, 42, -55, 1},
        {-107, -17, 34, 104, -48, -108, 97, -115, 28, 101, 87, 100, 114, 115, 103, -23, 56, 45},
        {5, 32, -6, -104, 102, -52, 88, -9, 26},};

    /**
     * Sorted arrays of given byte arrays in data.
     * Each test row is applied to the same row array in data.
     */
    static final byte[][] sortedDataTest = new byte[][]{
        {-1, 1, 2,},
        {-95, -94, -44, -42, -34, -22, -10, 22, 22, 43, 82, 125,},
        {-117, -105, -102, -68, -64, -1, 17, 59, 74, 79, 95, 115,},
        {-127, -99, -55, 1, 2, 42, 115,},
        {-115, -108, -107, -48, -23, -17, 28, 34, 45, 56, 87, 97, 100, 101, 103, 104, 114, 115,},
        {-104, -52, -9, -6, 5, 26, 32, 88, 102,}
    };

    /**
     * boolean contains(X[] array, X candidate) tests parameters and result for
     * the data byte arrays.
     * Each test row is applied to the same row array in data.
     */
    static final byte[][] containsDataTest = new byte[][]{
        {1, 1, -1, 1, 2, 1, -2, 0, 3, 0, -3, 0},
        {-10, 1, 43, 1, -36, 0, 36, 0}, // { value1, testResult1, value2, testResult2, ... } with testResult = 1->true 0->false
        {-102, 1, 74, 1, 27, 0, -99, 0},
        {-127, 1, 1, 1, 45, 0, -45, 0},
        {28, 1, -23, 1, -99, 0, 99, 0},
        {5, 1, -9, 1, -102, 0, 104, 0}
    };

    /**
     * boolean contains(X[] array, int offset, int length, X candidate) tests
     * parameters and result for the data byte arrays.
     * Each test row is applied to the same row array in data.
     */
    static final byte[][] containsDataTest2 = new byte[][]{
        {1, 2, -1, 1, 1, 2, 3, 0}, // { offset1, length1, candidate1, testResult1, offset2, length2, candidate2, testResult2, ... }
        {3, 5, -95, 1, 3, 5, 125, 0, 3, 5, -22, 0, 3, 5, -99, 0},
        {1, 8, -68, 1, 1, 8, 79, 0, 1, 8, -1, 0, 1, 8, 99, 0},
        {2, 2, 2, 1, 2, 2, -99, 0, 2, 2, 42, 0, 2, 2, -36, 0},
        {2, 3, 34, 1, 2, 3, -17, 0, 2, 3, -108, 0, 2, 3, 100, 0},
        {6, 3, 26, 1, 6, 3, -52, 0, 6, 3, 25, 0}
    };
    
    /**
     * Test getFirstOccurence with data array.
     * Each test row is applied to the same row array in data.
     */
    static final byte[][] getFirstOccurenceDataTest = new byte[][]{
        {0, 3, -1, 1, 1, 2, 2, 2, 1, 2, 1, -1}, // { offset1, length1, candidate1, result1, offset2, length2, candidate2, result2, ... }
        {5, 7, 22, 7, 8, 4, 22, 10, 11, 1, 22, -1},
        {},
        {0, 7, 115, 2, 0, 5, 1, -1,},
        {},
        {}
    };

    static final byte[][][] EqualsAIIAITest = new byte[][][]{
        {{}, // source
        {}, // target
        {0, 0, 0, 1}},// parameters: { soffset, length, toffset, expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, 115, 2, 42, -55, 1}, // target
        {0, 7, 0, 1}},// parameters: { soffset, length, toffset, expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {17, -127, -99, 115, 2, 42, -55, 1}, // target
        {0, 7, 1, 1}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, 115, 2, 42, -55, 1, 17}, // target
        {0, 7, 0, 1}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1, -58}, // source
        {-127, -99, 115, 2, 42, -55, 1, 17}, // target
        {0, 8, 0, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1, -58}, // source
        {-127, -99, 115, 2, 42, -55, 1, 17}, // target
        {5, 3, 5, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1, -58}, // source
        {17, -127, -99, 115, 2, 42, -55, 1, 17}, // target
        {5, 3, 6, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {5, 32, -6, -104, 102, -52, 88, -9, 26, -127, -99, 115, 2}, // target
        {0, 4, 9, 1}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, -15, 2, 42, -55, 1}, // target
        {0, 7, 0, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {17, -127, -99, 115, -20, 42, -55, 1}, // target
        {0, 6, 1, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {17, -127, -99, 115, 2, 42, -55, 1}, // target
        {0, 7, 2, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {115, 2, 42, -55, 1}, // target
        {2, 5, 0, 1}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-56, 2, 42, -55, 1}, // target
        {2, 5, 0, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {115, 2, 42, -55}, // target
        {2, 5, 0, 0}}, // parameters

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-99, 7, 115, 2, 42, -55}, // target
        {2, 5, 2, 0}}, // parameters

        {{-127, -99, 115, 2, 42}, // source
        {-99, 7, 115, 2, 42, -55, 1, 17}, // target
        {2, 5, 2, 0}}, // parameters
    };

    final static byte[][][][] copy2DArraysTest = new byte[][][][]{
        {
            { // source
                {},
                {}
            },
            { // target
                {},
                {}
            },
            {{0, 0, 0, 2, 0, 0}} // parameters: { sx, sy, w, h, tx, ty }
        },
        {
            { // source
                {10, -20},
                {30, -40, 50}
            },
            { // target
                {10, -20},
                {30, -40}
            },
            {{0, 0, 2, 2, 0, 0}} // parameters: { sx, sy, w, h, tx, ty }
        },
        { 
            {// source
                {10, -20},
                {30, -40, 50}
            },
            { // target
                {-40, 50}
            },
            {{1, 1, 2, 1, 0, 0}} // parameters: { sx, sy, w, h, tx, ty }
        },
        {
            { // source
                {10, -20},
                {30, -40, 50}
            },
            { // target
                {-40}
            },
            {{1, 1, 1, 1, 0, 0}} // parameters: { sx, sy, w, h, tx, ty }
        },
        {
            { // source
                {10, -20, 30},
                {-40, 50, -60},
                {70, -80, 90}
            },
            { // target
                {0, 0, 0},
                {0, 50, -60},
                {0, -80, 90}
            },
            {{1, 1, 2, 2, 1, 1}} // parameters: { sx, sy, w, h, tx, ty }
        },};

    static final byte[][][] EqualsAATest = new byte[][][]{
        {{}, // source
        {}, // target
        {1}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, 115, 2, 42, -55, 1}, // target
        {1}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-127, 115, 2, 42, -55, 1}, // source
        {-127, -99, 115, 2, 42, -55, 1}, // target
        {0}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, 115, 42, -55, 1}, // target
        {0}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, -15, 2, 42, -55, 1}, // target
        {0}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {-127, -99, 115, 2, 42, -55, -10}, // target
        {0}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-127, -99, 115, 2, 42, -55, 1}, // source
        {12, -99, 115, 2, 42, -55, 1}, // target
        {0}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{5, 32, -6, -104, 102, -52}, // source
        {5, 32, -6, -104, 102, -52}, // target
        {1}},// parameters: { expResult } expresult is 0 for false or 1 for true

        {{-44, -94, -22}, // source
        {-44, -94, -22}, // target
        {1}},// parameters: { expResult } expresult is 0 for false or 1 for true
    };

    static final byte[][][][] equals2DArraysTest = new byte[][][][]{
        {
            { // a1
                {},
                {}
            },
            { // a2
                {},
                {}
            },
            {{1}} // expected result: 0 for false, 1 for true
        },
        {
            { // a1
                {10, -20},
                {30, -40, 50}
            },
            { // a2
                {10, -20},
                {30, -40, 50}
            },
            {{1}} // expected result: 0 for false, 1 for true
        },
        {
            { // a1
                {10, -20},
                {30, -40, 50},
                {-60}
            },
            { // a2
                {10, -20},
                {30, -40, 50}
            },
            {{0}} // expected result: 0 for false, 1 for true
        },
        {
            { // a1
                {10, -20},
                {30, -40, 50}
            },
            { // a2
                {10, 20},
                {30, -40, 50}
            },
            {{0}} // expected result: 0 for false, 1 for true
        },
        {
            { // a1
                {10, -20},
                {30, -40, 50}
            },
            { // a2
                {10, -20},
                {-30, -40, 50}
            },
            {{0}} // expected result: 0 for false, 1 for true
        },};

    static final byte[][][][] concatenateTest = new byte[][][][]{
        {
            { // source array
                {10, -20, 30},
                {-40, 50},
                {-60, 70, -80, 90}
            },
            { // cancatenated source
                {10, -20, 30, -40, 50, -60, 70, -80, 90}
            }
        },
        {
            { // source array
                {-107, -17},
                {34, 104, -48, -108},
                {97, -115, 28, 101, 87},
                {100, 114},
                {},
                {115, 103, -23, 56, 45},},
            { // cancatenated source
                {-107, -17, 34, 104, -48, -108, 97, -115, 28, 101, 87, 100, 114, 115, 103, -23, 56, 45}
            }
        },
        {
            { // source array
                {},
                {-107, -17},
                {34, 104, -48, -108},
                {97, -115, 28, 101, 87},
                {100, 114},
                {},
                {115, 103, -23, 56, 45},},
            { // cancatenated source
                {-107, -17, 34, 104, -48, -108, 97, -115, 28, 101, 87, 100, 114, 115, 103, -23, 56, 45}
            }
        }
    };

    final static byte[][][] reverseAIITest = new byte[][][]{
        {
            {}, //input
            {}, //expResult
            {0, 0} // parameters: { offset, length }
        }, {
            {-2, -1, 0, 1, 2}, //input
            {2, 1, 0, -1, -2}, //expResult
            {0, 5} // parameters: { offset, length }
        }, {
            {-2, -1, 0, 1, 2},
            {2, 1, 0, -1, -2},
            {0, 5}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {1, -55, 42, 2, 115, -99, -127},
            {0, 7}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -99, 42, 2, 115, -55, 1},
            {2, 3}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-99, -127, 115, 2, 42, -55, 1},
            {0, 2}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -99, 115, 2, 42, 1, -55},
            {5, 2}
        },};

    static final byte[][][] reverseAIIBlockTest = new byte[][][]{
        {
            {}, // input
            {}, // expResult
            {0, 0, 1} // parameters: { offset, length, blockSize }
        }, {
            {-2, -1, 0, 1, 2}, // input
            {2, 1, 0, -1, -2}, // expResult
            {0, 5, 1} // parameters: { offset, length, blockSize }
        }, {
            {-2, -1, 0, 1, 2},
            {2, 1, 0, -1, -2},
            {0, 5, 1}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {1, -55, 42, 2, 115, -99, -127},
            {0, 7, 1}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -99, 42, 2, 115, -55, 1},
            {2, 3, 1}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-99, -127, 115, 2, 42, -55, 1},
            {0, 2, 1}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -99, 115, 2, 42, 1, -55},
            {5, 2, 1}
        }, {
            {-2, -1, 0, 1, 2},
            {1, 2, 0, -2, -1},
            {0, 5, 2}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -99, -55, 1, 42, 115, 2},
            {2, 5, 2}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -55, 1, 2, 42, -99, 115},
            {1, 6, 2}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, 42, -55, 1, -99, 115, 2},
            {1, 6, 3}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {115, 2, -127, -99, 42, -55, 1},
            {0, 4, 2}
        }, {
            {-127, -99, 115, 2, 42, -55, 1},
            {-127, -99, 115, -55, 1, 2, 42},
            {3, 4, 2}
        },};

    static final byte[][][] binarySearchTest = new byte[][][]{
        {
            {}, // sorted data array: values
            {10, -1, -10, -1},// search tests: { value1, expResult1, value2, expResult2, ... }
            // reverse order sample values binarySearch
            {10, -1, -10, -1}// search tests: { value1, expResult1, value2, expResult2, ... }
        },
        {
            {-1, 1, 2,}, // sorted data array: values
            {-1, 0, 1, 1, 2, 2, -2, -1, 3, -4},// search tests: { value1, expResult1, value2, expResult2, ... }
            // reverse order sample values binarySearch
            {-1, 2, 1, 1, 2, 0, -2, -4, 3, -1}// search tests: { value1, expResult1, value2, expResult2, ... }
        },
        {
            {-95, -94, -44, -42, -34, -22, -10, 22, 22, 43, 82, 125,}, // sorted data array: values
            {-94, 1, 43, 9, -96, -1, 126, -13}, // search tests: { value1, expResult1, value2, expResult2, ... }
            // reverse order sample values binarySearch
            {-94, 10, 43, 2, -96, -13, 126, -1}, // search tests: { value1, expResult1, value2, expResult2, ... }
        },};

    @Test
    public void testEquals_XArray_Int_Int_XArray_Int() {
        // prototype: boolean equals(X[] a1, int soffset, int length, X[] a2, int toffset)
        for (int i = 0; i < EqualsAIIAITest.length; i++) {
            byte[] source = EqualsAIIAITest[i][0];
            byte[] target = EqualsAIIAITest[i][1];
            int soffset = EqualsAIIAITest[i][2][0];
            int length = EqualsAIIAITest[i][2][1];
            int toffset = EqualsAIIAITest[i][2][2];
            boolean expResult = (EqualsAIIAITest[i][2][3] == 1);
            // test byte
            boolean result = science.unlicense.api.array.Arrays.equals(source, soffset, length, target, toffset);
            Assert.assertEquals(result, expResult);
            // test boolean
            result = science.unlicense.api.array.Arrays.equals(byteArrayToBooleanArray(source), soffset, length, byteArrayToBooleanArray(target), toffset);
            Assert.assertEquals(result, expResult);
            // test short
            result = science.unlicense.api.array.Arrays.equals(byteArrayToShortArray(source), soffset, length, byteArrayToShortArray(target), toffset);
            Assert.assertEquals(result, expResult);
            // test int
            result = science.unlicense.api.array.Arrays.equals(byteArrayToIntArray(source), soffset, length, byteArrayToIntArray(target), toffset);
            Assert.assertEquals(result, expResult);
            // test long
            result = science.unlicense.api.array.Arrays.equals(byteArrayToLongArray(source), soffset, length, byteArrayToLongArray(target), toffset);
            Assert.assertEquals(result, expResult);
            // test float
            result = science.unlicense.api.array.Arrays.equals(byteArrayToFloatArray(source), soffset, length, byteArrayToFloatArray(target), toffset);
            Assert.assertEquals(result, expResult);
            // test double
            result = science.unlicense.api.array.Arrays.equals(byteArrayToDoubleArray(source), soffset, length, byteArrayToDoubleArray(target), toffset);
            Assert.assertEquals(result, expResult);
            if (source.length > 0) {
                {   // test float tolerance
                    float[] targetReal = byteArrayToFloatArray(target);
                    float[] sourceReal = byteArrayToFloatArray(source);
                    float[] targetTolerance = new float[targetReal.length];
                    float tolerance = 1.f;
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 0.9f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 0.9f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 1.1f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, false);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 1.1f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, false);
                }
                { // test double tolerance
                    double[] targetReal = byteArrayToDoubleArray(target);
                    double[] sourceReal = byteArrayToDoubleArray(source);
                    double[] targetTolerance = new double[targetReal.length];
                    double tolerance = 1.f;
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 0.9;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 0.9;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 1.1;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, false);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 1.1;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, soffset, length, targetTolerance, toffset, tolerance);
                    Assert.assertEquals(result, false);
                }
            }
            // test Object
            result = science.unlicense.api.array.Arrays.equals(byteArrayToByteArray(source), soffset, length, byteArrayToByteArray(target), toffset);
            Assert.assertEquals(result, expResult);
        }
    }

    @Test
    public void testEquals_XArray_XArray() {
        // prototype: boolean equals(X[] a1, X[] a2)
        for (int i = 0; i < EqualsAATest.length; i++) {
            byte[] source = EqualsAATest[i][0];
            byte[] target = EqualsAATest[i][1];
            boolean expResult = (EqualsAATest[i][2][0] == 1);
            // test byte
            boolean result = science.unlicense.api.array.Arrays.equals(source, target);
            Assert.assertEquals(result, expResult);
            // test boolean
            result = science.unlicense.api.array.Arrays.equals(byteArrayToBooleanArray(source), byteArrayToBooleanArray(target));
            Assert.assertEquals(result, expResult);
            // test short
            result = science.unlicense.api.array.Arrays.equals(byteArrayToShortArray(source), byteArrayToShortArray(target));
            Assert.assertEquals(result, expResult);
            // test int
            result = science.unlicense.api.array.Arrays.equals(byteArrayToIntArray(source), byteArrayToIntArray(target));
            Assert.assertEquals(result, expResult);
            // test long
            result = science.unlicense.api.array.Arrays.equals(byteArrayToLongArray(source), byteArrayToLongArray(target));
            Assert.assertEquals(result, expResult);
            // test float
            result = science.unlicense.api.array.Arrays.equals(byteArrayToFloatArray(source), byteArrayToFloatArray(target));
            Assert.assertEquals(result, expResult);
            // test double
            result = science.unlicense.api.array.Arrays.equals(byteArrayToDoubleArray(source), byteArrayToDoubleArray(target));
            Assert.assertEquals(result, expResult);
            if (source.length > 0) {
                {   // test float tolerance
                    float[] targetReal = byteArrayToFloatArray(target);
                    float[] sourceReal = byteArrayToFloatArray(source);
                    float[] targetTolerance = new float[targetReal.length];
                    float tolerance = 1.f;
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 0.9f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 0.9f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 1.1f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, false);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 1.1f;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, false);
                }
                { // test double tolerance
                    double[] targetReal = byteArrayToDoubleArray(target);
                    double[] sourceReal = byteArrayToDoubleArray(source);
                    double[] targetTolerance = new double[targetReal.length];
                    double tolerance = 1.f;
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 0.9;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 0.9;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, expResult);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] + tolerance * 1.1;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, false);
                    for (int j = 0; j < target.length; j++) {
                        targetTolerance[j] = targetReal[j] - tolerance * 1.1;
                    }
                    result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                    Assert.assertEquals(result, false);
                }
            }
            // test Object
            result = science.unlicense.api.array.Arrays.equals(byteArrayToByteArray(source), byteArrayToByteArray(target));
            Assert.assertEquals(result, expResult);
        }
    }

    @Test
    public void testEqual_XArrayArray_XArrayArray() {
        // prototype: boolean equals(X[][] a1, X[][] a2)
        for (int i = 0; i < equals2DArraysTest.length; i++) {

            byte[][] source = equals2DArraysTest[i][0];
            byte[][] target = equals2DArraysTest[i][1];
            boolean expResult = (equals2DArraysTest[i][2][0][0] == 1);
            // test byte
            boolean result = science.unlicense.api.array.Arrays.equals(source, target);
            Assert.assertEquals(result, expResult);
            // test boolean
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToBooleanArrayArray(source), byteArrayArrayToBooleanArrayArray(target));
            Assert.assertEquals(result, expResult);
            // test short
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToShortArrayArray(source), byteArrayArrayToShortArrayArray(target));
            Assert.assertEquals(result, expResult);
            // test int
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToIntArrayArray(source), byteArrayArrayToIntArrayArray(target));
            Assert.assertEquals(result, expResult);
            // test long
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToLongArrayArray(source), byteArrayArrayToLongArrayArray(target));
            Assert.assertEquals(result, expResult);
            // test float
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToFloatArrayArray(source), byteArrayArrayToFloatArrayArray(target));
            Assert.assertEquals(result, expResult);
            // test double
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToDoubleArrayArray(source), byteArrayArrayToDoubleArrayArray(target));
            Assert.assertEquals(result, expResult);
            if (source.length > 0) {
                int totalElementsNumber = 0;
                for (int n = 0; n < target.length; n++) {
                    totalElementsNumber += target[n].length;
                }
                if (totalElementsNumber > 0) {
                    {   // test float tolerance
                        float[][] targetReal = byteArrayArrayToFloatArrayArray(target);
                        float[][] sourceReal = byteArrayArrayToFloatArrayArray(source);
                        float[][] targetTolerance = new float[targetReal.length][];
                        for (int j = 0; j < target.length; j++) {
                            targetTolerance[j] = new float[targetReal[j].length];
                        }
                        float tolerance = 1.f;

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] + tolerance * 0.9f;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, expResult);

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] - tolerance * 0.9f;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, expResult);

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] + tolerance * 1.1f;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, false);

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] - tolerance * 1.1f;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, false);

                    }
                    { // test double tolerance
                        double[][] targetReal = byteArrayArrayToDoubleArrayArray(target);
                        double[][] sourceReal = byteArrayArrayToDoubleArrayArray(source);
                        double[][] targetTolerance = new double[targetReal.length][];
                        for (int j = 0; j < target.length; j++) {
                            targetTolerance[j] = new double[targetReal[j].length];
                        }
                        double tolerance = 1.f;

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] + tolerance * 0.9;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, expResult);

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] - tolerance * 0.9;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, expResult);

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] + tolerance * 1.1;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, false);

                        for (int j = 0; j < target.length; j++) {
                            for (int k = 0; k < target[j].length; k++) {
                                targetTolerance[j][k] = targetReal[j][k] - tolerance * 1.1;
                            }
                        }
                        result = science.unlicense.api.array.Arrays.equals(sourceReal, targetTolerance, tolerance);
                        Assert.assertEquals(result, false);
                    }
                }
            }
            // test Object
            result = science.unlicense.api.array.Arrays.equals(byteArrayArrayToByteArrayArray(source), byteArrayArrayToByteArrayArray(target));
            Assert.assertEquals(result, expResult);
        }
    }

    @Test
    public void testContains_XArray_X() {
        // prototype: boolean contains(X[] array, X candidate)
        {   // test boolean
            boolean[][] boolSample = new boolean[][]{
                {true, true, false},
                {false, false, true},
                {true, true},
                {false, false}};
            Assert.assertEquals(Arrays.contains(boolSample[0], true), true);
            Assert.assertEquals(Arrays.contains(boolSample[0], false), true);
            Assert.assertEquals(Arrays.contains(boolSample[1], true), true);
            Assert.assertEquals(Arrays.contains(boolSample[1], false), true);
            Assert.assertEquals(Arrays.contains(boolSample[2], true), true);
            Assert.assertEquals(Arrays.contains(boolSample[2], false), false);
            Assert.assertEquals(Arrays.contains(boolSample[3], true), false);
            Assert.assertEquals(Arrays.contains(boolSample[3], false), true);
        }
        for (int i = 0; i < containsDataTest.length; i++) {
            byte[] dataSample = data[i];
            byte[] dataTest = containsDataTest[i];
            {   // test byte
                for (int j = 0; j < dataTest.length; j += 2) {
                    byte candidate = dataTest[j];
                    boolean expResult = (dataTest[j + 1] == 1);
                    boolean result = Arrays.contains(dataSample, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                short[] dataTestX = byteArrayToShortArray(dataTest);
                for (int j = 0; j < dataTestX.length; j += 2) {
                    short candidate = (short) dataTestX[j];
                    boolean expResult = (dataTestX[j + 1] == 1);
                    boolean result = Arrays.contains(dataSampleX, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                int[] dataTestX = byteArrayToIntArray(dataTest);
                for (int j = 0; j < dataTestX.length; j += 2) {
                    int candidate = (int) dataTestX[j];
                    boolean expResult = (dataTestX[j + 1] == 1);
                    boolean result = Arrays.contains(dataSampleX, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                long[] dataTestX = byteArrayToLongArray(dataTest);
                for (int j = 0; j < dataTestX.length; j += 2) {
                    long candidate = (long) dataTestX[j];
                    boolean expResult = (dataTestX[j + 1] == 1);
                    boolean result = Arrays.contains(dataSampleX, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                float[] dataTestX = byteArrayToFloatArray(dataTest);
                for (int j = 0; j < dataTestX.length; j += 2) {
                    float candidate = (float) dataTestX[j];
                    boolean expResult = (dataTestX[j + 1] == 1);
                    boolean result = Arrays.contains(dataSampleX, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                double[] dataTestX = byteArrayToDoubleArray(dataTest);
                for (int j = 0; j < dataTestX.length; j += 2) {
                    double candidate = (double) dataTestX[j];
                    boolean expResult = (dataTestX[j + 1] == 1);
                    boolean result = Arrays.contains(dataSampleX, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                Byte[] dataTestX = byteArrayToByteArray(dataTest);
                for (int j = 0; j < dataTestX.length; j += 2) {
                    Byte candidate = (Byte) dataTestX[j];
                    boolean expResult = (dataTestX[j + 1] == 1);
                    boolean result = Arrays.contains(dataSampleX, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
        }
    }

    @Test
    public void testIdentityContains_ObjectArray_Object() {
        // prototype: boolean identityContains(Object[] array, Object candidate)

        for (int i = 0; i < containsDataTest.length; i++) {
            byte[] dataSample = data[i];
            byte[] dataTest = containsDataTest[i];

            Byte[] dataSampleX = byteArrayToByteArray(dataSample);
            Byte[] dataTestX = byteArrayToByteArray(dataTest);

            for (int j = 0; j < dataTestX.length; j += 2) {
                Byte candidate = (Byte) dataTestX[j];
                boolean expResult = (dataTestX[j + 1] == 1);
                boolean result = Arrays.identityContains(dataSampleX, candidate);
                Assert.assertEquals(result, expResult);
            }
        }
    }

    @Test
    public void testContains_XArray_Int_Int_X() {
        // prototype: boolean contains(X[] array, int offset, int length, X candidate)
        {   // test boolean
            boolean[][] boolSample = new boolean[][]{
                {true, true, false},
                {false, false, true},
                {true, true},
                {false, false}};
            Assert.assertEquals(Arrays.contains(boolSample[0], 2, 1, true), false);
            Assert.assertEquals(Arrays.contains(boolSample[0], 2, 1, false), true);
            Assert.assertEquals(Arrays.contains(boolSample[1], 1, 1, true), false);
            Assert.assertEquals(Arrays.contains(boolSample[1], 1, 1, false), true);
            Assert.assertEquals(Arrays.contains(boolSample[2], 0, 1, true), true);
            Assert.assertEquals(Arrays.contains(boolSample[2], 0, 1, false), false);
            Assert.assertEquals(Arrays.contains(boolSample[3], 1, 1, false), true);
        }
        for (int i = 0; i < containsDataTest2.length; i++) {
            byte[] dataSample = data[i];
            byte[] dataTest = containsDataTest2[i];
            {   // test byte
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    byte candidate = dataTest[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(dataSample, offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test short
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    short candidate = byteArrayToShortArray(dataTest)[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(byteArrayToShortArray(dataSample), offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test int
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    int candidate = byteArrayToIntArray(dataTest)[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(byteArrayToIntArray(dataSample), offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test long
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    long candidate = byteArrayToLongArray(dataTest)[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(byteArrayToLongArray(dataSample), offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test float
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    float candidate = byteArrayToFloatArray(dataTest)[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(byteArrayToFloatArray(dataSample), offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // doubleType:
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    double candidate = byteArrayToDoubleArray(dataTest)[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(byteArrayToDoubleArray(dataSample), offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Object
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    Byte candidate = byteArrayToByteArray(dataTest)[j + 2];
                    boolean expResult = (dataTest[j + 3] == 1);
                    boolean result = Arrays.contains(byteArrayToByteArray(dataSample), offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
        }
    }

    @Test
    public void testCopy_XArray_Int_Int() {
        // prototype: X[] copy(X[] source, int offset, int length)
        for (byte[] dataSample : data) {
            {   // test byte
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSample, offset, length, Arrays.copy(dataSample, offset, length), 0));
                    }
                }
            }
            { // test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int offset = 0; offset < dataSample.length; offset++) {
                    for (int length = 0; length < dataSample.length - offset; length++) {
                        Assert.assertTrue(Arrays.equals(dataSampleX, offset, length, Arrays.copy(dataSampleX, offset, length), 0));
                    }
                }
            }
        }
    }

    @Test
    public void testCopy_XArray() {
        // prototype: X[] copy(X[] source)
        for (byte[] dataSample : data) {
            {   // test byte
                Assert.assertTrue(Arrays.equals(dataSample, Arrays.copy(dataSample)));
            }
            { // test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                Assert.assertTrue(Arrays.equals(dataSampleX, Arrays.copy(dataSampleX)));
            }
        }
    }

    @Test
    public void testCopy_XArray_Int_Int_XArray_Int() {
        // prototype: X[] copy(X[] source, int soffset, int length, X[] target, int toffset)
        final int delta = 2;
        for (byte[] dataSample : data) {
            {   //test byte
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            byte[] target = new byte[length + toffset + delta];
                            byte[] result = science.unlicense.api.array.Arrays.copy(dataSample, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new byte[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSample, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new byte[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            boolean[] target = new boolean[length + toffset + delta];
                            boolean[] result = science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new boolean[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new boolean[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            short[] target = new short[length + toffset + delta];
                            short[] result = science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new short[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new short[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            int[] target = new int[length + toffset + delta];
                            int[] result = science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new int[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new int[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            long[] target = new long[length + toffset + delta];
                            long[] result = science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new long[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new long[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            float[] target = new float[length + toffset + delta];
                            float[] result = science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new float[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new float[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            double[] target = new double[length + toffset + delta];
                            double[] result = science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new double[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new double[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
            { //test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int soffset = 0; soffset < dataSample.length; soffset++) {
                    for (int length = 0; length < dataSample.length - soffset; length++) {
                        for (int toffset = 0; toffset < delta; toffset++) {
                            Byte[] target = new Byte[length + toffset + delta];
                            Byte[] result = (Byte[]) science.unlicense.api.array.Arrays.copy(dataSampleX, soffset, length, target, toffset);
                            Assert.assertTrue(Arrays.equals(result, 0, toffset, new Byte[toffset], 0));
                            Assert.assertTrue(Arrays.equals(result, toffset, length, dataSampleX, soffset));
                            Assert.assertTrue(Arrays.equals(result, toffset + length, result.length - (toffset + length), new Byte[result.length - toffset - length], 0));
                            Assert.assertTrue(Arrays.equals(result, target));
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testCopy_XArrayArray_Int_Int_Int_Int_XArrayArray_Int_Int() {
        // prototype: X[][] copy( X[][] source, int sx, int sy, int w, int h, X[][] target, int tx, int ty)
        for (int i = 0; i < copy2DArraysTest.length; i++) {
            byte[][] source = copy2DArraysTest[i][0];
            byte[][] expResult = copy2DArraysTest[i][1];
            int sx = (int) copy2DArraysTest[i][2][0][0];
            int sy = (int) copy2DArraysTest[i][2][0][1];
            int w = (int) copy2DArraysTest[i][2][0][2];
            int h = (int) copy2DArraysTest[i][2][0][3];
            int tx = (int) copy2DArraysTest[i][2][0][4];
            int ty = (int) copy2DArraysTest[i][2][0][5];

            {   // test byte
                byte[][] target = new byte[ty + h][tx + w];
                byte[][] result = science.unlicense.api.array.Arrays.copy(source, sx, sy, w, h, target, tx, ty);
                Assert.assertTrue(Arrays.equals(result, expResult));
                Assert.assertTrue(Arrays.equals(target, expResult));
            }
            { // test boolean
                boolean[][] target = new boolean[ty + h][tx + w];
                boolean[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToBooleanArrayArray(source), sx, sy, w, h, target, tx, ty);
                boolean[][] expResultX = byteArrayArrayToBooleanArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
            { // test short
                short[][] target = new short[ty + h][tx + w];
                short[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToShortArrayArray(source), sx, sy, w, h, target, tx, ty);
                short[][] expResultX = byteArrayArrayToShortArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
            { // test int
                int[][] target = new int[ty + h][tx + w];
                int[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToIntArrayArray(source), sx, sy, w, h, target, tx, ty);
                int[][] expResultX = byteArrayArrayToIntArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
            { // test long
                long[][] target = new long[ty + h][tx + w];
                long[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToLongArrayArray(source), sx, sy, w, h, target, tx, ty);
                long[][] expResultX = byteArrayArrayToLongArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
            { // test float
                float[][] target = new float[ty + h][tx + w];
                float[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToFloatArrayArray(source), sx, sy, w, h, target, tx, ty);
                float[][] expResultX = byteArrayArrayToFloatArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
            { // test double
                double[][] target = new double[ty + h][tx + w];
                double[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToDoubleArrayArray(source), sx, sy, w, h, target, tx, ty);
                double[][] expResultX = byteArrayArrayToDoubleArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
            { // test Object
                Byte[][] target = new Byte[ty + h][tx + w];
                for (int j = 0; j < target.length; j++) {
                    for (int k = 0; k < target[j].length; k++) {
                        target[j][k] = 0;
                    }
                }
                Byte[][] result = (Byte[][]) science.unlicense.api.array.Arrays.copy(byteArrayArrayToByteArrayArray(source), sx, sy, w, h, target, tx, ty);
                Byte[][] expResultX = byteArrayArrayToByteArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
                Assert.assertTrue(Arrays.equals(target, expResultX));
            }
        }
    }
    
    @Test
    public void testCopy_XArrayArray() {
        // prototype: X[][] copy( X[][] source )
        for (int i = 0; i < copy2DArraysTest.length; i++) {
            byte[][] source = copy2DArraysTest[i][0];
            byte[][] expResult = source;
            {   // test byte
                byte[][] result = science.unlicense.api.array.Arrays.copy(source);
                Assert.assertTrue(Arrays.equals(expResult, result));
            }
            { // test boolean
                boolean[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToBooleanArrayArray(source));
                boolean[][] expResultX = byteArrayArrayToBooleanArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
            { // test short
                short[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToShortArrayArray(source));
                short[][] expResultX = byteArrayArrayToShortArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
            { // test int
                int[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToIntArrayArray(source));
                int[][] expResultX = byteArrayArrayToIntArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
            { // test long
                long[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToLongArrayArray(source));
                long[][] expResultX = byteArrayArrayToLongArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
            { // test float
                float[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToFloatArrayArray(source));
                float[][] expResultX = byteArrayArrayToFloatArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
            { // test double
                double[][] result = science.unlicense.api.array.Arrays.copy(byteArrayArrayToDoubleArrayArray(source));
                double[][] expResultX = byteArrayArrayToDoubleArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
            { // test Object
                Byte[][] result = (Byte[][]) science.unlicense.api.array.Arrays.copy(byteArrayArrayToByteArrayArray(source));
                Byte[][] expResultX = byteArrayArrayToByteArrayArray(expResult);
                Assert.assertTrue(Arrays.equals(result, expResultX));
            }
        }
    }

    @Test
    public void testInsert_XArray_Int_X() {
        // prototype: X[] insert(X[] values, int index, X value) {
        byte value = 25;
        for (byte[] dataSample : data) {
            {   //test byte
                for (int index = 0; index < dataSample.length; index++) {
                    byte[] result = Arrays.insert(dataSample, index, value);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSample, 0));
                    Assert.assertEquals(result[index], value);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSample, index));
                }
            }
            { //test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                boolean valueX = (value > 0);
                for (int index = 0; index < dataSample.length; index++) {
                    boolean[] result = Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
            { //test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                short valueX = (short) value;
                for (int index = 0; index < dataSample.length; index++) {
                    short[] result = Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
            { //test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                int valueX = (int) value;
                for (int index = 0; index < dataSample.length; index++) {
                    int[] result = Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
            { //test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                long valueX = (long) value;
                for (int index = 0; index < dataSample.length; index++) {
                    long[] result = Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
            { //test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                float valueX = (float) value;
                for (int index = 0; index < dataSample.length; index++) {
                    float[] result = Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX, 0.f);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
            { //test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                double valueX = (double) value;
                for (int index = 0; index < dataSample.length; index++) {
                    double[] result = Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX, 0.);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
            { //test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                Byte valueX = (Byte) value;
                for (int index = 0; index < dataSample.length; index++) {
                    Byte[] result = (Byte[]) Arrays.insert(dataSampleX, index, valueX);
                    Assert.assertTrue(Arrays.equals(result, 0, index, dataSampleX, 0));
                    Assert.assertEquals(result[index], valueX);
                    Assert.assertTrue(Arrays.equals(result, index + 1, dataSample.length - index, dataSampleX, index));
                }
            }
        }
    }

    @Test
    public void testRemove_XArray_Int() {
        // prototype: X[] remove(X[] values, int index)
        for (byte[] dataSample : data) {
            {   // test byte
                for (int index = 0; index < dataSample.length; index++) {
                    byte[] result = science.unlicense.api.array.Arrays.remove(dataSample, index);
                    Assert.assertTrue(Arrays.equals(dataSample, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSample, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    boolean[] result = science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    short[] result = science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    int[] result = science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    long[] result = science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    float[] result = science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    double[] result = science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    Byte[] result = (Byte[]) science.unlicense.api.array.Arrays.remove(dataSampleX, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                }
            }
        }
    }

    @Test
    public void testRemoveWithin_XArray_Int() {
        // prototype: X removeWithin(X[] values, int index)
        for (byte[] dataSample : data) {
            {   // test byte
                for (int index = 0; index < dataSample.length; index++) {
                    byte[] result = Arrays.copy(dataSample);
                    byte value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSample, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSample, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], (byte) 0);
                    Assert.assertEquals(dataSample[index], value);
                }
            }
            { // test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    boolean[] result = Arrays.copy(dataSampleX);
                    boolean value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], false);
                    Assert.assertEquals(dataSampleX[index], value);
                }
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    short[] result = Arrays.copy(dataSampleX);
                    short value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], (short) 0);
                    Assert.assertEquals(dataSampleX[index], value);
                }
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    int[] result = Arrays.copy(dataSampleX);
                    int value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], (int) 0);
                    Assert.assertEquals(dataSampleX[index], value);
                }
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    long[] result = Arrays.copy(dataSampleX);
                    long value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], (long) 0);
                    Assert.assertEquals(dataSampleX[index], value);
                }
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    float[] result = Arrays.copy(dataSampleX);
                    float value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], (float) 0, 0.f);
                    Assert.assertEquals(dataSampleX[index], value, 0.f);
                }
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    double[] result = Arrays.copy(dataSampleX);
                    double value = Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], (double) 0, 0.);
                    Assert.assertEquals(dataSampleX[index], value, 0.);
                }
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int index = 0; index < dataSample.length; index++) {
                    Byte[] result = (Byte[]) Arrays.copy(dataSampleX);
                    Byte value = (Byte) Arrays.removeWithin(result, index);
                    Assert.assertTrue(Arrays.equals(dataSampleX, 0, index, result, 0));
                    Assert.assertTrue(Arrays.equals(dataSampleX, index + 1, dataSample.length - index - 1, result, index));
                    Assert.assertEquals(result[dataSample.length - 1], null);
                    Assert.assertEquals(dataSampleX[index], value);
                }
            }
        }
    }

    @Test
    public void testFill_XArray_Int_Int_X() {
        // prototype: void fill(X[] buffer, int offset, int length, X value)
        final int bufferMaximumLength = 5;
        final byte emptyValue = -56;
        final byte fillValue = 17;

        for (int bufferLength = 1; bufferLength < bufferMaximumLength; bufferLength++) {
            for (int offset = 0; offset < bufferLength; offset++) {
                for (int length = 0; length < bufferLength - offset + 1; length++) {
                    {   // test boolean
                        boolean[] XArray = new boolean[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, emptyValue > 0);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, fillValue > 0);
                        boolean[] expXArray = new boolean[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? fillValue > 0 : emptyValue > 0;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test byte
                        byte[] XArray = new byte[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (byte) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (byte) fillValue);
                        byte[] expXArray = new byte[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (byte) fillValue : (byte) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test short
                        short[] XArray = new short[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (short) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (short) fillValue);
                        short[] expXArray = new short[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (short) fillValue : (short) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test int
                        int[] XArray = new int[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (int) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (int) fillValue);
                        int[] expXArray = new int[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (int) fillValue : (int) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test long
                        long[] XArray = new long[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (long) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (long) fillValue);
                        long[] expXArray = new long[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (long) fillValue : (long) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test float
                        float[] XArray = new float[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (float) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (float) fillValue);
                        float[] expXArray = new float[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (float) fillValue : (float) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test double
                        double[] XArray = new double[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (double) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (double) fillValue);
                        double[] expXArray = new double[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (double) fillValue : (double) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                    { // test Object
                        Byte[] XArray = new Byte[bufferLength];
                        science.unlicense.api.array.Arrays.fill(XArray, 0, bufferLength, (Byte) emptyValue);
                        science.unlicense.api.array.Arrays.fill(XArray, offset, length, (Byte) fillValue);
                        Byte[] expXArray = new Byte[bufferLength];
                        for (int i = 0; i < bufferLength; i++) {
                            expXArray[i] = ((i >= offset) && (i < (offset + length))) ? (Byte) fillValue : (Byte) emptyValue;
                        }
                        Assert.assertTrue(Arrays.equals(XArray, expXArray));
                    }
                }
            }
        }
    }

    @Test
    public void testFill_XArray_X() {
        // prototype: void fill(X[] buffer, X value)
        final int bufferMaximumLength = 5;
        final byte fillValue = 17;

        for (int bufferLength = 0; bufferLength < bufferMaximumLength; bufferLength++) {
            {   // test boolean
                boolean[] XArray = new boolean[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, fillValue > 0);
                boolean[] expXArray = new boolean[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = fillValue > 0;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test byte
                byte[] XArray = new byte[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (byte) fillValue);
                byte[] expXArray = new byte[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (byte) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test short
                short[] XArray = new short[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (short) fillValue);
                short[] expXArray = new short[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (short) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test int
                int[] XArray = new int[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (int) fillValue);
                int[] expXArray = new int[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (int) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test long
                long[] XArray = new long[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (long) fillValue);
                long[] expXArray = new long[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (long) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test float
                float[] XArray = new float[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (float) fillValue);
                float[] expXArray = new float[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (float) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test double
                double[] XArray = new double[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (double) fillValue);
                double[] expXArray = new double[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (double) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
            { // test Object
                Byte[] XArray = new Byte[bufferLength];
                science.unlicense.api.array.Arrays.fill(XArray, (Byte) fillValue);
                Byte[] expXArray = new Byte[bufferLength];
                for (int i = 0; i < bufferLength; i++) {
                    expXArray[i] = (Byte) fillValue;
                }
                Assert.assertTrue(Arrays.equals(XArray, expXArray));
            }
        }
    }

    @Test
    public void testGetFirstOccurence_XArray_Int_Int_X() {
        // prototype: int getFirstOccurence(X[] array, int offset, int length, X candidate)
        {   // test boolean
            boolean[][] boolSample = new boolean[][]{
                {true, true, false},
                {false, false, true},
                {true, true},
                {false, false}};
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[0], 0, 3, false), 2);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[0], 0, 3, true), 0);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[0], 1, 2, false), 2);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[0], 1, 2, true), 1);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[0], 0, 2, false), -1);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[1], 0, 3, false), 0);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[1], 0, 3, true), 2);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[1], 1, 2, false), 1);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[1], 1, 2, true), 2);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[1], 0, 2, true), -1);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[2], 0, 2, true), 0);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[2], 0, 2, false), -1);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[3], 0, 2, true), -1);
            Assert.assertEquals(Arrays.getFirstOccurence(boolSample[3], 0, 2, false), 0);
        }
        for (int i = 0; i < getFirstOccurenceDataTest.length; i++) {
            byte[] dataSample = data[i];
            byte[] dataTest = getFirstOccurenceDataTest[i];
            {   // test byte
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    byte candidate = dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSample, offset, length, candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    short candidateX = (short) dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSampleX, offset, length, candidateX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    int candidateX = (int) dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSampleX, offset, length, candidateX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    long candidateX = (long) dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSampleX, offset, length, candidateX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    float candidateX = (float) dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSampleX, offset, length, candidateX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    double candidateX = (double) dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSampleX, offset, length, candidateX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int j = 0; j < dataTest.length; j += 4) {
                    int offset = dataTest[j];
                    int length = dataTest[j + 1];
                    Byte candidateX = (Byte) dataTest[j + 2];
                    int expResult = dataTest[j + 3];
                    int result = Arrays.getFirstOccurence(dataSampleX, offset, length, candidateX);
                    Assert.assertEquals(result, expResult);
                }
            }
        }
    }

    @Test
    public void testResize_XArray_Int() {
        // prototype: X[] resize(X[] array, int newSize)
        for (int i = 0; i < data.length; i++) {
            byte[] dataSample = data[i];
            { // test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                boolean[] result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
            }
            { // test byte
                int newLowerSize = dataSample.length / 2;
                byte[] result = Arrays.resize(dataSample, newLowerSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSample, 0, newLowerSize)));
                int newLargerSize = dataSample.length * 2;
                result = Arrays.resize(dataSample, newLargerSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSample, 0, newLargerSize)));
            }
            { // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                short[] result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
            }
            { // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                int[] result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
            }
            { // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                long[] result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
            }
            { // test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                float[] result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
            }
            { // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                double[] result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
            }
            { // test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                int newSize = dataSample.length / 2; // lower size
                Byte[] result = (Byte[]) Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, Arrays.copy(dataSampleX, 0, newSize)));
                newSize = dataSample.length * 2; // larger size
                result = (Byte[]) Arrays.resize(dataSampleX, newSize);
                Assert.assertTrue(Arrays.equals(result, (Byte[]) Arrays.copy(dataSampleX, 0, newSize)));
            }
        }
    }

    @Test
    public void testConcatenate_XArray() {
        // prototype: X[] concatenate(X[][] arrays)
        for (int i = 0; i < concatenateTest.length; i++) {
            byte[][][] dataSample = concatenateTest[i];
            {   // test boolean
                boolean[][] source = byteArrayArrayToBooleanArrayArray(dataSample[0]);
                boolean[] expResult = byteArrayToBooleanArray(dataSample[1][0]);
                boolean[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test byte
                byte[][] source = dataSample[0];
                byte[] expResult = dataSample[1][0];
                byte[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test short
                short[][] source = byteArrayArrayToShortArrayArray(dataSample[0]);
                short[] expResult = byteArrayToShortArray(dataSample[1][0]);
                short[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test int
                int[][] source = byteArrayArrayToIntArrayArray(dataSample[0]);
                int[] expResult = byteArrayToIntArray(dataSample[1][0]);
                int[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test long
                long[][] source = byteArrayArrayToLongArrayArray(dataSample[0]);
                long[] expResult = byteArrayToLongArray(dataSample[1][0]);
                long[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test float
                float[][] source = byteArrayArrayToFloatArrayArray(dataSample[0]);
                float[] expResult = byteArrayToFloatArray(dataSample[1][0]);
                float[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test double
                double[][] source = byteArrayArrayToDoubleArrayArray(dataSample[0]);
                double[] expResult = byteArrayToDoubleArray(dataSample[1][0]);
                double[] result = Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
            {   // test Object
                Byte[][] source = byteArrayArrayToByteArrayArray(dataSample[0]);
                Byte[] expResult = byteArrayToByteArray(dataSample[1][0]);
                Byte[] result = (Byte[]) Arrays.concatenate(source);
                Assert.assertTrue(Arrays.equals(result, expResult));
            }
        }
    }

    @Test
    public void testReverse_XArray_Int_Int() {
        // prototype: void reverse(X[] array, int offset, int length)
        for (int i = 0; i < reverseAIITest.length; i++) {
            byte[] input = reverseAIITest[i][0];
            byte[] expResult = reverseAIITest[i][1];
            int offset = (int) reverseAIITest[i][2][0];
            int length = (int) reverseAIITest[i][2][1];
            {   // test byte
                byte[] source = Arrays.copy(input);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResult));
            }
            { // test boolean
                boolean[] source = byteArrayToBooleanArray(input);
                boolean[] expResultX = byteArrayToBooleanArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test short
                short[] source = byteArrayToShortArray(input);
                short[] expResultX = byteArrayToShortArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test int
                int[] source = byteArrayToIntArray(input);
                int[] expResultX = byteArrayToIntArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test long
                long[] source = byteArrayToLongArray(input);
                long[] expResultX = byteArrayToLongArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test float
                float[] source = byteArrayToFloatArray(input);
                float[] expResultX = byteArrayToFloatArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test double
                double[] source = byteArrayToDoubleArray(input);
                double[] expResultX = byteArrayToDoubleArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test Object
                Byte[] source = byteArrayToByteArray(input);
                Byte[] expResultX = byteArrayToByteArray(expResult);
                Arrays.reverse(source, offset, length);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
        }
    }

    @Test
    public void testReverse_XArray_Int_Int_Int() {
        // prototype: void reverse(X[] array, int offset, int length, int blockSize)
        for (int i = 0; i < reverseAIIBlockTest.length; i++) {
            byte[] input = reverseAIIBlockTest[i][0];
            byte[] expResult = reverseAIIBlockTest[i][1];
            int offset = (int) reverseAIIBlockTest[i][2][0];
            int length = (int) reverseAIIBlockTest[i][2][1];
            int blockSize = (int) reverseAIIBlockTest[i][2][2];
            {   // test byte
                byte[] source = Arrays.copy(input);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResult));
            }
            { // test boolean
                boolean[] source = byteArrayToBooleanArray(input);
                boolean[] expResultX = byteArrayToBooleanArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test short
                short[] source = byteArrayToShortArray(input);
                short[] expResultX = byteArrayToShortArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test int
                int[] source = byteArrayToIntArray(input);
                int[] expResultX = byteArrayToIntArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test long
                long[] source = byteArrayToLongArray(input);
                long[] expResultX = byteArrayToLongArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test float
                float[] source = byteArrayToFloatArray(input);
                float[] expResultX = byteArrayToFloatArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test double
                double[] source = byteArrayToDoubleArray(input);
                double[] expResultX = byteArrayToDoubleArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
            { // test Object
                Byte[] source = byteArrayToByteArray(input);
                Byte[] expResultX = byteArrayToByteArray(expResult);
                Arrays.reverse(source, offset, length, blockSize);
                Assert.assertTrue(Arrays.equals(source, expResultX));
            }
        }
    }

    @Test
    public void testSwap_XArray_Int_Int() {
        // prototype: void swap(X[] array, int index1, int index2)
        for (int i = 0; i < data.length; i++) {
            byte[] sampleData = data[i];

            { // test byte
                byte[] sampleDataX = Arrays.copy(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        byte val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k]);
                        Assert.assertEquals(val2, sampleDataX[j]);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test boolean
                boolean[] sampleDataX = byteArrayToBooleanArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        boolean val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k]);
                        Assert.assertEquals(val2, sampleDataX[j]);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test short
                short[] sampleDataX = byteArrayToShortArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        short val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k]);
                        Assert.assertEquals(val2, sampleDataX[j]);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test int
                int[] sampleDataX = byteArrayToIntArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        int val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k]);
                        Assert.assertEquals(val2, sampleDataX[j]);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test long
                long[] sampleDataX = byteArrayToLongArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        long val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k]);
                        Assert.assertEquals(val2, sampleDataX[j]);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test float
                float[] sampleDataX = byteArrayToFloatArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        float val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k], 0.f);
                        Assert.assertEquals(val2, sampleDataX[j], 0.f);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test double
                double[] sampleDataX = byteArrayToDoubleArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        double val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k], 0.);
                        Assert.assertEquals(val2, sampleDataX[j], 0.);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
            { // test Object
                Byte[] sampleDataX = byteArrayToByteArray(sampleData);
                for (int j = 0; j < sampleData.length - 1; j++) {
                    for (int k = j + 1; k < sampleData.length; k++) {
                        Byte val1 = sampleDataX[j], val2 = sampleDataX[k];
                        Arrays.swap(sampleDataX, j, k);
                        Assert.assertEquals(val1, sampleDataX[k]);
                        Assert.assertEquals(val2, sampleDataX[j]);
                        Arrays.swap(sampleDataX, j, k);
                    }
                }
            }
        }
    }

    @Test
    public void testQuickSort_XArray() {
        // prototype: void sort( X[] array )
        for (int i = 0; i < sortedDataTest.length; i++) {
            byte[] dataSample = data[i];
            byte[] expResult = sortedDataTest[i];
            {   // test byte
                byte[] input = Arrays.copy(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(expResult, input));
            }
            { // test short
                short[] input = byteArrayToShortArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(byteArrayToShortArray(expResult), input));
            }
            { // test int
                int[] input = byteArrayToIntArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(byteArrayToIntArray(expResult), input));
            }
            { // test long
                long[] input = byteArrayToLongArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(byteArrayToLongArray(expResult), input));
            }
            { // test float
                float[] input = byteArrayToFloatArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(byteArrayToFloatArray(expResult), input));
            }
            { // test double
                double[] input = byteArrayToDoubleArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(byteArrayToDoubleArray(expResult), input));
            }
            { // Test Orderable
                OrderableObject[] input = byteArrayToOrderableObjectArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1);
                Assert.assertTrue(Arrays.equals(byteArrayToOrderableObjectArray(expResult), input));
            }
            { // test Orderable with Sorter
                OrderableObjectSorter oos = new OrderableObjectSorter();
                OrderableObject[] input = byteArrayToOrderableObjectArray(dataSample);
                
                OrderableObject[] dataSampleOrderableObjectSorter = byteArrayToOrderableObjectArray(expResult);
                Arrays.reverse(dataSampleOrderableObjectSorter, 0, expResult.length);
                
                Arrays.quickSort(input, 0, input.length - 1, oos);
                Assert.assertTrue(Arrays.equals(dataSampleOrderableObjectSorter, input));
            }
            { // test Orderable with Sorter == null
                OrderableObject[] input = byteArrayToOrderableObjectArray(dataSample);
                Arrays.quickSort(input, 0, input.length - 1, null);
                Assert.assertTrue(Arrays.equals(byteArrayToOrderableObjectArray(expResult), input));
            }
        }
    }

    @Test
    public void testSort_XArray() {
        // prototype: void sort( X[] array )
        for (int i = 0; i < sortedDataTest.length; i++) {
            byte[] dataSample = data[i];
            byte[] expResult = sortedDataTest[i];
            {   // test byte
                byte[] input = Arrays.copy(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(expResult, input));
            }
            { // test short
                short[] input = byteArrayToShortArray(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(byteArrayToShortArray(expResult), input));
            }
            { // test int
                int[] input = byteArrayToIntArray(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(byteArrayToIntArray(expResult), input));
            }
            { // test long
                long[] input = byteArrayToLongArray(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(byteArrayToLongArray(expResult), input));
            }
            { // test float
                float[] input = byteArrayToFloatArray(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(byteArrayToFloatArray(expResult), input));
            }
            { // test double
                double[] input = byteArrayToDoubleArray(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(byteArrayToDoubleArray(expResult), input));
            }
            { // test Orderable
                OrderableObject[] input = byteArrayToOrderableObjectArray(dataSample);
                Arrays.sort(input);
                Assert.assertTrue(Arrays.equals(byteArrayToOrderableObjectArray(expResult), input));
            }
            { // test Orderable with Sorter
                OrderableObjectSorter oos = new OrderableObjectSorter();
                OrderableObject[] input = byteArrayToOrderableObjectArray(dataSample);
                
                OrderableObject[] dataSampleOrderableObjectSorter = byteArrayToOrderableObjectArray(expResult);
                Arrays.reverse(dataSampleOrderableObjectSorter, 0, expResult.length);
                
                Arrays.sort(input, oos);
                Assert.assertTrue(Arrays.equals(dataSampleOrderableObjectSorter, input));
            }
            { // test Orderable with Sorter == null
                OrderableObject[] input = byteArrayToOrderableObjectArray(dataSample);
                Arrays.sort(input, null);
                Assert.assertTrue(Arrays.equals(byteArrayToOrderableObjectArray(expResult), input));
            }
        }
    }

    @Test
    public void testBinarySearch_X_XArray() {
        // prototype: int binarySearch(X value, X[] values)
        for (int i = 0; i < binarySearchTest.length; i++) {

            byte[] dataSample = binarySearchTest[i][0];

            {   // test byte
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    byte value = binarySearchTest[i][1][j];
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSample);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   // test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    short value = (short) binarySearchTest[i][1][j];
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   // test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    int value = (int) binarySearchTest[i][1][j];
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   // test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    long value = (long) binarySearchTest[i][1][j];
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   // test short
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    float value = (float) binarySearchTest[i][1][j];
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   // test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    double value = (double) binarySearchTest[i][1][j];
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Orderable
                OrderableObject[] dataSampleX = byteArrayToOrderableObjectArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    OrderableObject value = new OrderableObject(binarySearchTest[i][1][j]);
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Orderable with Sorter
                OrderableObjectSorter oos = new OrderableObjectSorter();
                
                OrderableObject[] dataSampleX = byteArrayToOrderableObjectArray(dataSample);
                Arrays.reverse(dataSampleX, 0, dataSample.length);
                
                for (int j = 0; j < binarySearchTest[i][2].length; j += 2) {
                    OrderableObject value = new OrderableObject(binarySearchTest[i][2][j]);
                    int expResult = (int) binarySearchTest[i][2][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX, oos);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Orderable with Sorter == null
                OrderableObject[] dataSampleX = byteArrayToOrderableObjectArray(dataSample);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    OrderableObject value = new OrderableObject(binarySearchTest[i][1][j]);
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX, null);
                    Assert.assertEquals(result, expResult);
                }
            }
            
            // ===== test with Orderable Sequences =====
            {   // test Orderable
                OrderableObject[] dataSampleXArray = byteArrayToOrderableObjectArray(dataSample);
                Sequence dataSampleX = new ArraySequence(dataSampleXArray);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    OrderableObject value = new OrderableObject(binarySearchTest[i][1][j]);
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Orderable with Sorter
                OrderableObjectSorter oos = new OrderableObjectSorter();
                
                OrderableObject[] dataSampleXArray = byteArrayToOrderableObjectArray(dataSample);
                Arrays.reverse(dataSampleXArray, 0, dataSample.length);
                
                Sequence dataSampleX = new ArraySequence(dataSampleXArray);
                for (int j = 0; j < binarySearchTest[i][2].length; j += 2) {
                    OrderableObject value = new OrderableObject(binarySearchTest[i][2][j]);
                    int expResult = (int) binarySearchTest[i][2][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX, oos);
                    Assert.assertEquals(result, expResult);
                }
            }
            { // test Orderable with Sorter == null
                OrderableObject[] dataSampleXArray = byteArrayToOrderableObjectArray(dataSample);
                Sequence dataSampleX = new ArraySequence(dataSampleXArray);
                for (int j = 0; j < binarySearchTest[i][1].length; j += 2) {
                    OrderableObject value = new OrderableObject(binarySearchTest[i][1][j]);
                    int expResult = (int) binarySearchTest[i][1][j + 1];
                    int result = Arrays.binarySearch(value, dataSampleX, null);
                    Assert.assertEquals(result, expResult);
                }
            }
        }
    }

    @Test
    public void testStartsWith_XArray_XArray_Int_Int() {
        // prototype: boolean startsWith(X[] candidate, X[] source, int offset, int length )
        boolean result, expResult;
        for( int i=0; i<data.length; i++ ) {
            byte[] dataSample = data[i];
            {   //test byte
                for( int j=0; j<dataSample.length; j++ ) {
                    byte[] candidate = Arrays.copy(dataSample, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSample, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSample, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    boolean[] candidate = Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    short[] candidate = Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    int[] candidate = Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    long[] candidate = Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    float[] candidate = Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    double[] candidate = Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            if (dataSample.length > 0) {
                {   // test float tolerance
                    float[] dataSampleX = byteArrayToFloatArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        float[] candidate = byteArrayToFloatArray(Arrays.copy(dataSample, 0, j));
                        float[] candidateTolerance = new float[candidate.length];
                        float tolerance = .1f;
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy(dataSampleX, 0, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy(dataSampleX, 0, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                    }
                }
                {   // test double tolerance
                    double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        double[] candidate = byteArrayToDoubleArray(Arrays.copy(dataSample, 0, j));
                        double[] candidateTolerance = new double[candidate.length];
                        double tolerance = .1f;
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy(dataSampleX, 0, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy(dataSampleX, 0, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.startsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                    }
                }
            }
            {   //test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    Byte[] candidate = (Byte[])Arrays.copy(dataSampleX, 0, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.startsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, 0, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
        }
    }
    
    @Test
    public void testStartsWith_XArray_XArray() {
        // prototype: boolean startsWith(X[] candidate, X[] source )
        boolean result, expResult;
        for (int i = 0; i < data.length; i++) {
            byte[] dataSample = data[i];
            {   //test byte
                for (int j = 0; j < dataSample.length; j++) {
                    byte[] candidate = Arrays.copy(dataSample, 0, j);
                    result = Arrays.startsWith(dataSample, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSample, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    boolean[] candidate = Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    short[] candidate = Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    int[] candidate = Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    long[] candidate = Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    float[] candidate = Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    double[] candidate = Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                    Assert.assertEquals(result, expResult);
                }
            }
            if (dataSample.length > 0) {
                {   // test float tolerance
                    float[] dataSampleX = byteArrayToFloatArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        float[] candidate = byteArrayToFloatArray(Arrays.copy(dataSample, 0, j));
                        float[] candidateTolerance = new float[candidate.length];
                        float tolerance = .1f;

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9f;
                        }
                        result = Arrays.startsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy(dataSampleX, 0, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9f;
                        }
                        result = Arrays.startsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy(dataSampleX, 0, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);
                        if( j > 0 ) {
                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1f;
                            }
                            result = Arrays.startsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);

                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1f;
                            }
                            result = Arrays.startsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);
                        }
                    }
                }
                {   // test double tolerance
                    double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        double[] candidate = byteArrayToDoubleArray(Arrays.copy(dataSample, 0, j));
                        double[] candidateTolerance = new double[candidate.length];
                        double tolerance = .1f;

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9;
                        }
                        result = Arrays.startsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy(dataSampleX, 0, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9;
                        }
                        result = Arrays.startsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy(dataSampleX, 0, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);
                        
                        if( j > 0 ) {
                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1;
                            }
                            result = Arrays.startsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);

                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1;
                            }
                            result = Arrays.startsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);
                        }
                    }
                }
            }
            {   //test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    Byte[] candidate = (Byte[]) Arrays.copy(dataSampleX, 0, j);
                    result = Arrays.startsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, 0, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                    Assert.assertEquals(result, expResult);
                }
            }
        }
    }

    @Test
    public void testEndsWith_XArray_XArray_Int_Int() {
        // prototype: boolean endsWith(X[] candidate, X[] source, int offset, int length )
        boolean result, expResult;
        for( int i=0; i<data.length; i++ ) {
            byte[] dataSample = data[i];
            {   //test byte
                for( int j=0; j<dataSample.length; j++ ) {
                    byte[] candidate = Arrays.copy(dataSample, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSample, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSample, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    boolean[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    short[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    int[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    long[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    float[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            {   //test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    double[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
            if (dataSample.length > 0) {
                {   // test float tolerance
                    float[] dataSampleX = byteArrayToFloatArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        float[] candidate = byteArrayToFloatArray(Arrays.copy(dataSample, dataSample.length-j, j));
                        float[] candidateTolerance = new float[candidate.length];
                        float tolerance = .1f;
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1f;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                    }
                }
                {   // test double tolerance
                    double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        double[] candidate = byteArrayToDoubleArray(Arrays.copy(dataSample, dataSample.length-j, j));
                        double[] candidateTolerance = new double[candidate.length];
                        double tolerance = .1f;
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 0; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                expResult = Arrays.equals(
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy(candidate, offset, length), 
                                        tolerance);
                                Assert.assertEquals(result, expResult);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                        
                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1;
                        }
                        for (int offset = 0; offset < candidate.length; offset++) {
                            for (int length = 1; length < candidate.length - offset + 1; length++) {
                                result = Arrays.endsWith(dataSampleX, candidateTolerance, offset, length, tolerance);
                                Assert.assertEquals(result, false);
                            }
                        }
                    }
                }
            }
            {   //test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for( int j=0; j<dataSample.length; j++ ) {
                    Byte[] candidate = (Byte[])Arrays.copy(dataSampleX, dataSample.length-j, j);
                    for (int offset = 0; offset <candidate.length; offset++) {
                        for( int length=0; length<candidate.length-offset+1; length++ ) {
                            result = Arrays.endsWith(dataSampleX, candidate, offset, length);
                            expResult = Arrays.equals( 
                                        Arrays.copy( dataSampleX, dataSample.length-length, length),
                                        Arrays.copy( candidate, offset, length ) );
                            Assert.assertEquals( result, expResult );
                        }
                    }
                }
            }
        }
    }
    
    @Test
    public void testEndsWith_XArray_XArray() {
        // prototype: boolean endsWith(X[] candidate, X[] source )
        boolean result, expResult;
        for (int i = 0; i < data.length; i++) {
            byte[] dataSample = data[i];
            {   //test byte
                for (int j = 0; j < dataSample.length; j++) {
                    byte[] candidate = Arrays.copy(dataSample, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSample, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSample, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test boolean
                boolean[] dataSampleX = byteArrayToBooleanArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    boolean[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test short
                short[] dataSampleX = byteArrayToShortArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    short[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test int
                int[] dataSampleX = byteArrayToIntArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    int[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test long
                long[] dataSampleX = byteArrayToLongArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    long[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test float
                float[] dataSampleX = byteArrayToFloatArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    float[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                    Assert.assertEquals(result, expResult);
                }
            }
            {   //test double
                double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    double[] candidate = Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                    Assert.assertEquals(result, expResult);
                }
            }
            if (dataSample.length > 0) {
                {   // test float tolerance
                    float[] dataSampleX = byteArrayToFloatArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        float[] candidate = byteArrayToFloatArray(Arrays.copy(dataSample, dataSample.length-j, j));
                        float[] candidateTolerance = new float[candidate.length];
                        float tolerance = .1f;

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9f;
                        }
                        result = Arrays.endsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9f;
                        }
                        result = Arrays.endsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);
                        if( j > 0 ) {
                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1f;
                            }
                            result = Arrays.endsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);

                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1f;
                            }
                            result = Arrays.endsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);
                        }
                    }
                }
                {   // test double tolerance
                    double[] dataSampleX = byteArrayToDoubleArray(dataSample);
                    for (int j = 0; j < dataSample.length; j++) {
                        double[] candidate = byteArrayToDoubleArray(Arrays.copy(dataSample, dataSample.length-j, j));
                        double[] candidateTolerance = new double[candidate.length];
                        double tolerance = .1f;

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 0.9;
                        }
                        result = Arrays.endsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);

                        for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                            candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 0.9;
                        }
                        result = Arrays.endsWith(dataSampleX, candidate);
                        expResult = Arrays.equals(
                                Arrays.copy( dataSampleX, dataSample.length-candidate.length, candidate.length),
                                candidate);
                        Assert.assertEquals(result, expResult);
                        Assert.assertEquals(result, expResult);
                        
                        if( j > 0 ) {
                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] + tolerance * 1.1;
                            }
                            result = Arrays.endsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);

                            for (int crIndex = 0; crIndex < candidate.length; crIndex++) {
                                candidateTolerance[crIndex] = candidate[crIndex] - tolerance * 1.1;
                            }
                            result = Arrays.endsWith(dataSampleX, candidateTolerance, tolerance);
                            Assert.assertEquals(result, false);
                        }
                    }
                }
            }
            {   //test Object
                Byte[] dataSampleX = byteArrayToByteArray(dataSample);
                for (int j = 0; j < dataSample.length; j++) {
                    Byte[] candidate = (Byte[]) Arrays.copy(dataSampleX, dataSample.length-j, j);
                    result = Arrays.endsWith(dataSampleX, candidate);
                    expResult = Arrays.equals(
                            Arrays.copy(dataSampleX, dataSample.length-candidate.length, candidate.length),
                            candidate);
                    Assert.assertEquals(result, expResult);
                    Assert.assertEquals(result, expResult);
                }
            }
        }
    }
    
    @Test
    public void testToChars() {
        
        // ===== 1D arrays =====
        
        {   // test byte
            byte[] array = { 2, -1, 3 };
            Chars result = Arrays.toChars(array);
            byte[] expResult = { '[', '2', ',', '-', '1', ',', '3', ']' };
            Assert.assertTrue( Arrays.equals(expResult, result.toBytes() ) );
            
        }
        {   // test short
            short[] array = { 2, -1, 3 };
            Chars result = Arrays.toChars(array);
            byte[] expResult = { '[', '2', ',', '-', '1', ',', '3', ']' };
            Assert.assertTrue( Arrays.equals(expResult, result.toBytes() ) );
        }
        {   // test int
            int[] array = { 2, -1, 3 };
            Chars result = Arrays.toChars(array);
            byte[] expResult = { '[', '2', ',', '-', '1', ',', '3', ']' };
            Assert.assertTrue( Arrays.equals(expResult, result.toBytes() ) );
        }
        {   // test long
            long[] array = { 2, -1, 3 };
            Chars result = Arrays.toChars(array);
            byte[] expResult = { '[', '2', ',', '-', '1', ',', '3', ']' };
            Assert.assertTrue( Arrays.equals(expResult, result.toBytes() ) );
        }
        {   // test float
            float[] array = { 2, -1, 3 };
            Chars result = Arrays.toChars(array);
            byte[] expResult = { '[', '2', '.', '0', ',', '-', '1', '.', '0', ',', '3', '.', '0', ']' };
            Assert.assertTrue( Arrays.equals(expResult, result.toBytes() ) );
        }
        {   // test double
            double[] array = { 2, -1, 3 };
            Chars result = Arrays.toChars(array);
            byte[] expResult = { '[', '2', '.', '0', ',', '-', '1', '.', '0', ',', '3', '.', '0', ']' };
            Assert.assertTrue( Arrays.equals(expResult, result.toBytes() ) );
        }
    }
    
    /**
     * Test of xorArray method, of class HelperFunctions.
     */
    @Test
    public void testArrayXOR() {
        
        byte[][][] arrayXORDataTest = new byte[][][]{
            {
                { 0, 0, 4 }, // { sourceIndex, targetIndex, length
                { (byte)0x00, (byte)0xFF, (byte)0xF0, (byte)0x0F, }, // source
                { (byte)0x0F, (byte)0x0F, (byte)0xFF, (byte)0x0F, }, // target
                { (byte)0x0F, (byte)0xF0, (byte)0x0F, (byte)0x00, }, // expResult
            },{
                { 1, 0, 2 }, // { os, ot, length
                { (byte)0x00, (byte)0xFF, (byte)0xF0, (byte)0x0F, }, // source
                { (byte)0x0F, (byte)0x0F, (byte)0xFF, (byte)0x0F, }, // target
                { (byte)0xF0, (byte)0xFF, (byte)0xFF, (byte)0x0F, }, // expResult
            },{
                { 0, 1, 3 }, // { os, ot, length
                { (byte)0x00, (byte)0xFF, (byte)0xF0, (byte)0x0F, }, // source
                { (byte)0x0F, (byte)0x0F, (byte)0xFF, (byte)0x0F, }, // target
                { (byte)0x0F, (byte)0x0F, (byte)0x00, (byte)0xFF, }, // expResult
            },{
                { 3, 3, 1 }, // { os, ot, length
                { (byte)0x00, (byte)0xFF, (byte)0xF0, (byte)0x0F, }, // source
                { (byte)0x0F, (byte)0x0F, (byte)0xFF, (byte)0x0F, }, // target
                { (byte)0x0F, (byte)0x0F, (byte)0xFF, (byte)0x00, }, // expResult
            },
        };
        
        System.out.println("arrayXOR");
        
        for( byte[][] data : arrayXORDataTest ) {
            byte[] s = data[1];
            int os = data[0][0];
            byte[] t = data[2];
            int ot = data[0][1];
            int length = data[0][2];
            byte[] expResult = data[3];
            Arrays.arrayXOR(s, os, t, ot, length);
            
            Assert.assertArrayEquals( expResult, t );
        }
    }
}
