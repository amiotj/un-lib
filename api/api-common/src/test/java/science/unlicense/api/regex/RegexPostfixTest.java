
package science.unlicense.api.regex;

import science.unlicense.api.regex.Regex;
import science.unlicense.api.regex.RegexExec;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;

/**
 * Regular expression using postfix syntax.
 * 
 * @author Johann Sorel
 */
public class RegexPostfixTest {
    
    @Test
    public void singleCharTest(){
        
        final Chars exp = new Chars("a");
        final RegexExec exec = Regex.compilePostfix(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void zeroOneTest(){
        
        final Chars exp = new Chars("a?");
        final RegexExec exec = Regex.compilePostfix(exp);
        
        Assert.assertTrue(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
    }
    
    @Test
    public void zeroManyTest(){
        
        final Chars exp = new Chars("a*");
        final RegexExec exec = Regex.compilePostfix(exp);
        
        Assert.assertTrue(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("aa")));
        Assert.assertTrue(exec.match(new Chars("aaaaaaa")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void oneManyTest(){
        
        final Chars exp = new Chars("a+");
        final RegexExec exec = Regex.compilePostfix(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertTrue(exec.match(new Chars("a")));
        Assert.assertTrue(exec.match(new Chars("aa")));
        Assert.assertTrue(exec.match(new Chars("aaaaaaa")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertFalse(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
    @Test
    public void concatenationTest(){
        
        final Chars exp = new Chars("ab>c>d>");
        final RegexExec exec = Regex.compilePostfix(exp);
        
        Assert.assertFalse(exec.match(Chars.EMPTY));
        Assert.assertFalse(exec.match(new Chars("a")));
        Assert.assertFalse(exec.match(new Chars("abc")));
        Assert.assertFalse(exec.match(new Chars("bcd")));
        Assert.assertFalse(exec.match(new Chars("A")));
        Assert.assertFalse(exec.match(new Chars("b")));
        Assert.assertFalse(exec.match(new Chars("$")));
        Assert.assertTrue(exec.match(new Chars("abcd")));
        Assert.assertFalse(exec.match(new Chars("abcdz")));
        Assert.assertFalse(exec.match(new Chars("aaaaabcd")));
    }
    
}
