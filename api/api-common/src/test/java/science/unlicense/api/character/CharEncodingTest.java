
package science.unlicense.api.character;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.CharEncoding;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.array.Arrays;

/**
 *
 * @author Johann Sorel
 */
public class CharEncodingTest {

    public void ASCIItoLATIN1(){

//        final CharSequence in = new CharSequence(new byte[]{'h','e','l','l','o'}, CharEncodings.US_ASCII);
//        final CharSequence out = Characters.recode(in, CharEncodings.ISO_8859_16);
//
//        Assert.assertArrayEquals(in.toBytes(), out.toBytes());
    }

    @Test
    public void UTF8Test(){
        // A character
        testChar(CharEncodings.UTF_8, 65, new byte[]{65});
        // U+00B6 PILCROW SIGN
        testChar(CharEncodings.UTF_8, 182, new byte[]{-62, -74});
        // U+1D560 MATHEMATICAL DOUBLE-STRUCK SMALL O
        testChar(CharEncodings.UTF_8, 120160, new byte[]{-16, -99, -107, -96});
    }

    @Test
    public void UTF16BETest(){
        // A character
        testChar(CharEncodings.UTF_16BE, 65, new byte[]{0,65});
        // U+00B6 PILCROW SIGN
        testChar(CharEncodings.UTF_16BE, 182, new byte[]{0, -74});
        // U+1D560 MATHEMATICAL DOUBLE-STRUCK SMALL O
        testChar(CharEncodings.UTF_16BE, 120160, new byte[]{-40, 53, -35, 96});
    }

    @Test
    public void UTF16LETest(){
        // A character
        testChar(CharEncodings.UTF_16LE, 65, new byte[]{65,0});
        // U+00B6 PILCROW SIGN
        testChar(CharEncodings.UTF_16LE, 182, new byte[]{-74,0});
        // U+1D560 MATHEMATICAL DOUBLE-STRUCK SMALL O
        testChar(CharEncodings.UTF_16LE, 120160, new byte[]{53,-40, 96, -35});
    }

    @Test
    public void UTF32BETest(){
        // A character
        testChar(CharEncodings.UTF_32BE, 65, new byte[]{0,0,0,65});
        // U+00B6 PILCROW SIGN
        testChar(CharEncodings.UTF_32BE, 182, new byte[]{0,0,0,-74});
        // U+1D560 MATHEMATICAL DOUBLE-STRUCK SMALL O
        testChar(CharEncodings.UTF_32BE, 120160, new byte[]{0, 1, -43, 96});        
    }

    @Test
    public void UTF32LETest(){
        // A character
        testChar(CharEncodings.UTF_32LE, 65, new byte[]{65,0,0,0});
        // U+00B6 PILCROW SIGN
        testChar(CharEncodings.UTF_32LE, 182, new byte[]{-74,0,0,0});
        // U+1D560 MATHEMATICAL DOUBLE-STRUCK SMALL O
        testChar(CharEncodings.UTF_32LE, 120160, new byte[]{96, -43, 1, 0});
    }
    
    @Test
    public void ShiftJISTest(){
        // A character ASCII
        testChar(CharEncodings.SHIFT_JIS, 'A', new byte[]{'A'});
        // A character
        testChar(CharEncodings.SHIFT_JIS, 65, new byte[]{65});
        // 0xB6 0xFF76 # HALFWIDTH KATAKANA LETTER KA
        testChar(CharEncodings.SHIFT_JIS, 0xFF76, new byte[]{(byte)0xB6});
        // 0x8392 0x30F2 # KATAKANA LETTER WO
        testChar(CharEncodings.SHIFT_JIS, 0x30F2, new byte[]{(byte)0x83, (byte)0x92});
        // 0x95BD 0x5E73 # <CJK>
        testChar(CharEncodings.SHIFT_JIS, 0x5E73, new byte[]{(byte)0x95, (byte)0xBD});
    }
    
    @Test
    public void Windows1252Test(){
        // A character ASCII
        testChar(CharEncodings.WINDOWS_1252, 'A', new byte[]{'A'});
        // A character
        testChar(CharEncodings.WINDOWS_1252, 65, new byte[]{65});
        // 0x7F	0x007F	#DELETE
        testChar(CharEncodings.WINDOWS_1252, 0x007F, new byte[]{(byte)0x7F});
        // 0x80	0x20AC	#EURO SIGN
        testChar(CharEncodings.WINDOWS_1252, 0x20AC, new byte[]{(byte)0x80});
        // 0xA0	0x00A0	#NO-BREAK SPACE
        testChar(CharEncodings.WINDOWS_1252, 0x00A0, new byte[]{(byte)0xA0});
        // 0x9F	0x0178	#LATIN CAPITAL LETTER Y WITH DIAERESIS
        testChar(CharEncodings.WINDOWS_1252, 0x0178, new byte[]{(byte)0x9F});
    }
    
    private static void testChar(final CharEncoding encoding, final int charInUnicode, final byte[] encoded){
        
        final int[] container = new int[]{charInUnicode,0};
        final byte[] buffer = new byte[5]; //use offset of 1 for tests
        byte[] bytes;

        //test decoding
        Arrays.fill(buffer,(byte)0);
        bytes = encoding.toBytes(charInUnicode);
        Assert.assertArrayEquals(encoded, bytes);
        encoding.toBytes(container, buffer, 1);
        Assert.assertArrayEquals(new int[]{charInUnicode, encoded.length}, container);
        Assert.assertEquals(0, buffer[0]);
        Assert.assertArrayEquals(encoded, Arrays.copy(buffer, 1, encoded.length, new byte[encoded.length], 0));

        //test encoding
        Arrays.fill(container,0);
        encoding.toUnicode(buffer, container, 1);
        Assert.assertArrayEquals(new int[]{charInUnicode, encoded.length}, container);
        
        //test length
        Assert.assertEquals(1,encoding.length(encoded));        
        Assert.assertEquals(encoded.length, encoding.charlength(bytes, 0));
        Assert.assertEquals(encoded.length, encoding.charlength(
                Arrays.copy(bytes, 0, encoded.length, new byte[encoded.length+1], 1), 1));
        
    }
    
}
