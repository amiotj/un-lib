
package science.unlicense.api.event;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;

/**
 * Dummy listener, used for various tests.
 * 
 * @author Johann Sorel
 */
public class DummyListener implements EventListener {
    public int nbEvent = 0;
    public Class messageClass = null;
    public Event event = null;

    public void receiveEvent(Event event) {
        nbEvent++;
        this.messageClass = event.getMessage().getClass();
        this.event = event;
    }

    /**
     * Reset listener values
     */
    public void clear() {
        nbEvent = 0;
        messageClass = null;
        event = null;
    }
    
}
