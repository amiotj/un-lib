
package science.unlicense.api.collection;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.ArraySequenceSet;
import science.unlicense.api.collection.Sequence;
import org.junit.Test;
import org.junit.Assert;

/**
 * Test sequence.
 * 
 * @author Johann Sorel
 */
public class ArraySequenceSetTest extends SequenceTest {

    @Override
    protected Sequence createNew() {
        return new ArraySequenceSet();
    }

    @Override
    protected Object[] sampleValues() {
        return new Object[]{
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "1",
            "2",
            "3"
        };
    }
   
    /**
     * Constructors test.
     */
    @Test
    public void testConstructor(){
        
        Sequence seq = new ArraySequence();
        Assert.assertTrue(seq.isEmpty());
        Assert.assertEquals(0, seq.getSize());
        
        seq = new ArraySequence(new Object[0]);
        Assert.assertTrue(seq.isEmpty());
        Assert.assertEquals(0, seq.getSize());
        
        final Object[] array = new Object[]{"a","b","c"};
        seq = new ArraySequence(array);
        Assert.assertFalse(seq.isEmpty());
        Assert.assertEquals(3, seq.getSize());
        Assert.assertEquals("a", seq.get(0));
        Assert.assertEquals("b", seq.get(1));
        Assert.assertEquals("c", seq.get(2));
        
    }
    
}
