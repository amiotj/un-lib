
package science.unlicense.api.graph.operation;

import science.unlicense.api.graph.operation.Dijkstra;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.graph.DefaultGraph;
import science.unlicense.api.graph.Edge;
import science.unlicense.api.graph.Graph;
import science.unlicense.api.graph.Vertex;
import science.unlicense.api.predicate.Expression;

/**
 *
 * @author Johann Sorel
 */
public class DijkstraTest {

    @Test
    public void shortestPathTest(){

        //reproduce graph from
        // https://en.wikipedia.org/wiki/File:Dijkstra_Animation.gif
        final Graph graph = new DefaultGraph();
        final Vertex v1 = graph.createVertex();
        final Vertex v2 = graph.createVertex();
        final Vertex v3 = graph.createVertex();
        final Vertex v4 = graph.createVertex();
        final Vertex v5 = graph.createVertex();
        final Vertex v6 = graph.createVertex();
        graph.addVertex(v1);
        graph.addVertex(v2);
        graph.addVertex(v3);
        graph.addVertex(v4);
        graph.addVertex(v5);
        graph.addVertex(v6);

        final Edge e12 = graph.createEdge(v1, v2); e12.getProperties().add("dist", 7d);
        final Edge e13 = graph.createEdge(v1, v3); e13.getProperties().add("dist", 9d);
        final Edge e16 = graph.createEdge(v1, v6); e16.getProperties().add("dist", 14d);
        graph.addEdge(e12);
        graph.addEdge(e13);
        graph.addEdge(e16);

        final Edge e23 = graph.createEdge(v2, v3); e23.getProperties().add("dist", 10d);
        final Edge e24 = graph.createEdge(v2, v4); e24.getProperties().add("dist", 15d);
        graph.addEdge(e23);
        graph.addEdge(e24);

        final Edge e34 = graph.createEdge(v3, v4); e34.getProperties().add("dist", 11d);
        final Edge e36 = graph.createEdge(v3, v6); e36.getProperties().add("dist", 2d);
        graph.addEdge(e34);
        graph.addEdge(e36);

        final Edge e45 = graph.createEdge(v4, v5); e45.getProperties().add("dist", 6d);
        graph.addEdge(e45);

        final Edge e56 = graph.createEdge(v5, v6); e56.getProperties().add("dist", 9d);
        graph.addEdge(e56);

        final Sequence path = Dijkstra.getShortestPath(v1, v4, new Expression() {
            public Double evaluate(Object candidate) {
                return (Double)((Edge)candidate).getProperties().getValue("dist");
            }
        });

        Assert.assertNotNull(path);
        Assert.assertEquals(3, path.getSize());
        Assert.assertEquals(v1, path.get(0));
        Assert.assertEquals(v3, path.get(1));
        Assert.assertEquals(v4, path.get(2));
    }

}
