
package science.unlicense.api.array;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultFloatBuffer;

/**
 *
 * @author Johann Sorel
 */
public class DefaultFloatBufferTest extends AbstractBufferTest{

    @Override
    public Buffer create(int nbByte) {
        return new DefaultFloatBuffer(new float[nbByte/4]);
    }

}
