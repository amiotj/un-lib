
package science.unlicense.api.model3d;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * @author Johann Sorel
 */
public class AbstractModel3DFormat extends DefaultFormat implements Model3DFormat{

    public AbstractModel3DFormat(Chars identifier, Chars shortName, Chars longName, 
            Chars[] mimeTypes, Chars[] extensions, byte[][] signatures) {
        super(identifier,shortName,longName,mimeTypes,extensions, signatures);
    }

    public Model3DStore open(Object input) throws IOException {
        throw new IOException("Not supported.");
    }

}
