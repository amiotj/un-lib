
package science.unlicense.api.model3d;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.store.AbstractStore;
import science.unlicense.api.store.StoreException;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractModel3DStore extends AbstractStore implements Model3DStore{

    public AbstractModel3DStore(Model3DFormat format, Object input) {
        super(format,input);
    }

    public Model3DFormat getFormat() {
        return (Model3DFormat)super.getFormat();
    }

    public Collection searchElements(Class type) throws StoreException{
        final Collection col = new ArraySequence();
        final Collection ele = getElements();
        for(Iterator ite = ele.createIterator();ite.hasNext();){
            final Object candidate = ite.next();
            if(type.isInstance(candidate)){
                col.add(candidate);
            }
        }
        return col;
    }

    public ByteInputStream getSourceAsInputStream() throws IOException {
        return IOUtilities.toInputStream(input, new boolean[1]);
    }

    public ByteOutputStream getSourceAsOutputStream() throws IOException {
        return IOUtilities.toOutputStream(input, new boolean[1]);
    }

    public void writeElements(Collection elements) throws StoreException {
        throw new StoreException("Not supported.");
    }

}
