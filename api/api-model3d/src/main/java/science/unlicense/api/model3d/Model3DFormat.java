
package science.unlicense.api.model3d;

import science.unlicense.api.io.IOException;
import science.unlicense.api.store.Format;

/**
 * Define a 3d model format :
 * - Model3DStore
 *
 * @author Johann Sorel
 */
public interface Model3DFormat extends Format{

    Model3DStore open(Object input) throws IOException;

}
