
package science.unlicense.api.model3d;

import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.ViewNode;
import science.unlicense.system.util.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public class Model3Ds {


    /**
     * Lists available 3d model formats.
     * @return array of Model3DFormat, never null but can be empty.
     */
    public static Model3DFormat[] getFormats(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.api.model3d.Model3DFormat"), 
                Predicates.instanceOf(Model3DFormat.class));
        final Model3DFormat[] formats = new Model3DFormat[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }
    
    public static Model3DFormat getFormat(Chars name) throws InvalidArgumentException{
        final Model3DFormat[] formats = getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i].getIdentifier().equals(name)){
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static boolean canDecode(Object input) throws IOException {
        final Model3DFormat[] formats = getFormats();

        for(int i=0;i<formats.length;i++){
            try{
                if(formats[i].canDecode(input)){
                    return true;
                }
            }catch(IOException ex){
                //may happen
                Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
            }
        }

        return false;
    }

    /**
     * Convinient method to read a model source of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return Model3DStore, never null
     * @throws IOException if not format could support the source.
     */
    public static Model3DStore read(Object input) throws IOException {
        final Model3DFormat[] formats = getFormats();
        if(formats.length == 0){
            throw new IOException("No image formats available.");
        }

        for(int i=0;i<formats.length;i++){
            try{
                if(formats[i].canDecode(input)){
                    final Model3DStore store = formats[i].open(input);
                    return store;
                }
            }catch(IOException ex){
                //may happen
                Loggers.get().log(ex, Logger.LEVEL_WARNING);
            }
        }

        throw new IOException("No reader can read given input : "+input);
    }

    public static Predicate createPredicate(){
        return new Predicate() {
            @Override
            public Boolean evaluate(Object candidate) {
                try {
                    if(candidate instanceof Path && ((Path)candidate).isContainer()){
                        return true;
                    }

                    candidate = ViewNode.unWrap((Node)candidate);
                    return canDecode(candidate);
                } catch (IOException ex) {
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
                return false;
            }
        };
    }

}
