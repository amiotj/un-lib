
package science.unlicense.api.task;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;

/**
 * Common model for generic tasks.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTask extends AbstractEventSource implements Task{

    protected TaskDescriptor descriptor;
    protected boolean canceled = false;
    protected Document inputParameters;
    protected Document outputParameters;

    public AbstractTask(final TaskDescriptor descriptor) {
        this.descriptor = descriptor;
        this.inputParameters = new DefaultDocument(false, descriptor.getInputType());
        this.outputParameters = new DefaultDocument(false, descriptor.getOutputType());
    }

    public void setInput(Document input) {
        inputParameters = input;
    }

    public Document getInput() {
        return inputParameters;
    }

    public void cancel(){
        canceled = true;
    }

    public Class[] getEventClasses() {
        return new Class[]{TaskMessage.class};
    }

    protected void fireEvent(int state, float progress, Chars message, Exception error){
        if(hasListeners()){
            getEventManager().sendEvent(new Event(this, new TaskMessage(state, progress, message, outputParameters, error)));
        }
    }

}
