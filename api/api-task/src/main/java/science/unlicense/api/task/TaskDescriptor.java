
package science.unlicense.api.task;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DocumentType;

/**
 * Description of a Task.
 *
 * @author Johann Sorel
 */
public interface TaskDescriptor {

    /**
     * Get task id.
     * Should be unique on the system.
     * @return Chars, never null
     */
    Chars getId();

    /**
     * Get task name.
     * @return Chars, never null
     */
    CharArray getName();

    /**
     * Get task description.
     * @return Chars, never null
     */
    CharArray getDescription();

    /**
     * Input parameters of the task.
     * @return ParameterType , never null
     */
    DocumentType getInputType();

    /**
     * Output parameters of the task.
     * @return ParameterType , never null
     */
    DocumentType getOutputType();

    /**
     * Create a new instance of this task.
     * @return Task, never null.
     */
    Task create();

}
