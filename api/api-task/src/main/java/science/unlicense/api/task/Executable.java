
package science.unlicense.api.task;


/**
 * Minimal version of an executable task.
 * Contrary to Task, this interface do not provide any description,control
 * or listener capabilities.
 *
 * @author Johann Sorel
 */
public interface Executable {

    /**
     * Execute the task.
     * @return result parameter
     */
    Object execute();

}
