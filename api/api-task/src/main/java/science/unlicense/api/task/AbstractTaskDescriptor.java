
package science.unlicense.api.task;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DocumentType;

/**
 * Abstract task descriptor.
 *
 * @author Johann Sorel
 */
public abstract class AbstractTaskDescriptor extends CObject implements TaskDescriptor {

    public static final Chars INPUT = new Chars("input");
    public static final Chars OUTPUT = new Chars("output");
    
    protected final Chars id;
    protected final CharArray name;
    protected final CharArray description;
    protected final DocumentType inputType;
    protected final DocumentType outputType;

    public AbstractTaskDescriptor(Chars id, CharArray name, CharArray description,
            DocumentType inputType, DocumentType outputType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.inputType = inputType;
        this.outputType = outputType;
    }

    public Chars getId(){
        return id;
    }

    public CharArray getName(){
        return name;
    }

    public CharArray getDescription(){
        return description;
    }

    public DocumentType getInputType() {
        return inputType;
    }

    public DocumentType getOutputType() {
        return outputType;
    }

    public abstract Task create();

}
