
package science.unlicense.api.task;

import science.unlicense.api.event.EventSource;
import science.unlicense.api.model.doc.Document;

/**
 * Common model for generic tasks.
 *
 * @author Johann Sorel
 */
public interface Task extends EventSource, Executable{

    /**
     * Ask to stop the task if it is running.
     * Task have no guarantee to respect this request.
     * Best effort.
     */
    void cancel();

    /**
     * Set task input parameters.
     * No processing should be done here.
     * @param input
     */
    void setInput(Document input);

    /**
     * Get current input parameters.
     * @return Parameters
     */
    Document getInput();
    
    /**
     * Execute the task.
     * @return result parameters
     */
    Document execute();

}
