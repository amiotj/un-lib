
package science.unlicense.api.task;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;

/**
 * Convinient methods for tasks.
 *
 * @author Johann Sorel
 */
public final class Tasks {

    private Tasks(){}

    /**
     * Lists available image tasks
     * @return array of TaskDescriptor, never null but can be empty.
     */
    public static TaskDescriptor[] getDescriptors(){
        final NamedNode root = science.unlicense.system.System.get().getModuleManager().getMetadataRoot().search(
                new Chars("services/science.unlicense.api.task.TaskDescriptor"));
        if(root==null){
            return new TaskDescriptor[0];
        }
        return (TaskDescriptor[]) Nodes.getChildrenValues(root, null).toArray(TaskDescriptor.class);
    }

    /**
     * Get task descriptor for given id.
     * @param id task id
     * @return TaskDescriptor, null if not found
     */
    public static TaskDescriptor getDescriptor(Chars id){
        final TaskDescriptor[] descs = getDescriptors();
        for(int i=0;i<descs.length;i++){
            if(descs[i].getId().equals(id)) return descs[i];
        }
        return null;
    }

}
