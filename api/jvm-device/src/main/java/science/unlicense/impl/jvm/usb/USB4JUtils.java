

package science.unlicense.impl.jvm.usb;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 *
 * @author Johann Sorel
 */
public final class USB4JUtils {
    
    private static final Dictionary ERROR_MESSAGES = new HashDictionary();
    static {
        ERROR_MESSAGES.add(0, new Chars("Succes"));
        ERROR_MESSAGES.add(-1, new Chars("Input-Output error"));
        ERROR_MESSAGES.add(-2, new Chars("Invalid parameter"));
        ERROR_MESSAGES.add(-3, new Chars("Access error, may be permission/provileged denied"));
        ERROR_MESSAGES.add(-4, new Chars("Device not found"));
        ERROR_MESSAGES.add(-5, new Chars("Not found error"));
        ERROR_MESSAGES.add(-6, new Chars("Busy error, device is occupied"));
        ERROR_MESSAGES.add(-7, new Chars("Timeout error"));
        ERROR_MESSAGES.add(-8, new Chars("Overflow error"));
        ERROR_MESSAGES.add(-9, new Chars("Pipe error"));
        ERROR_MESSAGES.add(-10, new Chars("Interruption error"));
        ERROR_MESSAGES.add(-11, new Chars("Memory error"));
        ERROR_MESSAGES.add(-12, new Chars("Unsupported operation"));
        ERROR_MESSAGES.add(-99, new Chars("Unknowned error, other"));
    }
    
    private USB4JUtils(){}
    
    public static Chars getErrorMessage(int errorCode){
        Chars txt = (Chars) ERROR_MESSAGES.getValue(errorCode);
        if(txt==null) txt = (Chars) ERROR_MESSAGES.getValue(-99);
        return txt;
    }
    
}
