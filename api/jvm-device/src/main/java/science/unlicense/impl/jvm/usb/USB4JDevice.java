
package science.unlicense.impl.jvm.usb;

import org.usb4java.Context;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.LibUsb;
import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.device.DeviceException;
import science.unlicense.api.device.usb.USBControl;
import science.unlicense.api.device.usb.USBDevice;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.Int32;
import science.unlicense.api.model.tree.TypedNode;

/**
 *
 * @author Johann Sorel
 */
public class USB4JDevice extends CObject implements USBDevice{
    
    private final Context context;
    private final Device device;
    private final DeviceDescriptor desc;

    public USB4JDevice(Context context, Device device) throws DeviceException {
        this.context = context;
        this.device = device;
        this.desc = new DeviceDescriptor();
        int result = LibUsb.getDeviceDescriptor(device, desc);
        if (result != LibUsb.SUCCESS) {
            throw new DeviceException("Unable to read device descriptor. "+ USB4JUtils.getErrorMessage(result));
        }        
    }

    public Device getU4JDevice() {
        return device;
    }

    public Context getU4JContext() {
        return context;
    }
    
    public short getProductId() {
        return desc.idProduct();
    }

    public short getVendorId() {
        return desc.idVendor();
    }

    public USBControl createControl() throws DeviceException {
        return new USB4JControl(this);
    }
    
    public TypedNode getDescription() {
        throw new UnimplementedException("Not supported yet.");
    }

    public Chars toChars() {
        return Int32.encodeHexa(getVendorId()).concat(':').concat(Int32.encodeHexa(getProductId()));
    }

}
