

package science.unlicense.impl.jvm.usb;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.usb4java.DeviceHandle;
import org.usb4java.LibUsb;
import science.unlicense.api.device.DeviceException;
import science.unlicense.api.device.usb.USBControl;
import science.unlicense.api.device.usb.USBDevice;

/**
 *
 * @author Johann Sorel
 */
public class USB4JControl implements USBControl{

    private final USB4JDevice device;
    private final DeviceHandle handle;

    public USB4JControl(USB4JDevice device) throws DeviceException {
        this.device = device;
        
        this.handle = new DeviceHandle();
        int result = LibUsb.open(device.getU4JDevice(), handle);
        if (result != LibUsb.SUCCESS){
            throw new DeviceException("Unable to open USB device. "+ USB4JUtils.getErrorMessage(result));
        }
        
    }
    
    public USBDevice getDevice() {
        return device;
    }

    public void open() {
        LibUsb.setConfiguration(handle, 1);
        LibUsb.claimInterface(handle, 0);
    }

    public int writeSync(final byte endPoint, final ByteBuffer data, final long timeout) throws DeviceException {
        final IntBuffer nbByteTransfer = IntBuffer.allocate(1);
        final int status = LibUsb.bulkTransfer(handle, 
                (byte) (endPoint | LibUsb.ENDPOINT_OUT),
                data, nbByteTransfer, timeout);
        if(status!= LibUsb.SUCCESS){
            throw new DeviceException(""+USB4JUtils.getErrorMessage(status));
        }
        nbByteTransfer.position(0);
        return nbByteTransfer.get();
    }

    public int readSync(byte endPoint, final ByteBuffer data, final long timeout) throws DeviceException {
        final IntBuffer nbByteTransfer = IntBuffer.allocate(1);
        final int status = LibUsb.bulkTransfer(handle, 
                (byte) (endPoint | LibUsb.ENDPOINT_IN),
                data, nbByteTransfer, timeout);
        if(status!= LibUsb.SUCCESS){
            throw new DeviceException(""+USB4JUtils.getErrorMessage(status));
        }
        nbByteTransfer.position(0);
        return nbByteTransfer.get();
    }
    
    
    public void dispose() {
        LibUsb.close(handle);
    }

    @Override
    protected void finalize() throws Throwable {
        dispose();
        super.finalize();
    }
    
    
    
}
