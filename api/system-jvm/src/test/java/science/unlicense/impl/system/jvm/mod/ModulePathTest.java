
package science.unlicense.impl.system.jvm.mod;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ModulePathTest {

    /**
     * Check we can open a simple module path.
     */
    @Test
    public void testModuleSimplePath() throws IOException{
        final Chars str = new Chars("mod:/simple/path/test.txt");
        final Path path = Paths.resolve(str);
        final ByteInputStream stream = path.createInputStream();
        stream.close();

    }

    /**
     * Check we can open a complex module path.
     */
    @Test
    public void testModuleComplexPath() throws IOException{
        final Chars str = new Chars("mod:/simple/path/[@re  _ x3$   .t [xt");
        final Path path = Paths.resolve(str);
        final ByteInputStream stream = path.createInputStream();
        stream.close();

    }
    
    /**
     * Check we can navigate parent/child.
     */
    @Test
    public void testModuleParentChildSolving() throws IOException{
        final Chars str = new Chars("mod:/simple/path/[@re  _ x3$   .t [xt");
        final Path path = Paths.resolve(str);
        
        final Path parent = path.getParent();
        Assert.assertNotNull(parent);
        Assert.assertEquals(new Chars("mod:/simple/path/"),parent.toURI());
        
        final Path sibling = parent.resolve(new Chars("test.txt"));
        Assert.assertNotNull(sibling);
        Assert.assertEquals(new Chars("mod:/simple/path/test.txt"),sibling.toURI());
        

    }

}
