
package science.unlicense.impl.system.jvm.file;

import science.unlicense.impl.system.jvm.file.FilePath;
import science.unlicense.impl.system.jvm.file.FileResolver;
import java.io.File;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class FilePathTest {

    @Test
    public void relativePathTest() {

        final Path base = new FilePath(new FileResolver(), new File("test"));
        Assert.assertTrue(base.toURI().endsWith(new Chars("test")));

        final Path child1 = base.resolve(new Chars("child"));
        Assert.assertTrue(child1.toURI().endsWith(new Chars("test/child")));

        final Path child2 = base.resolve(new Chars(".child"));
        Assert.assertTrue(child2.toURI().endsWith(new Chars("test/.child")));

    }

}
