
package science.unlicense.impl.system.jvm;

import science.unlicense.system.path.Paths;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class PathsTest {
    
    @Test
    public void relativePathTest(){
        
        final Path p1 = Paths.resolve(new Chars("file:/home/user/"));
        final Path p2 = Paths.resolve(new Chars("file:/home/user/folder1/folder2/test.xml"));
        
        Chars relativePath = Paths.relativePath(p1, p2);
        Assert.assertEquals(new Chars("./folder1/folder2/test.xml"),relativePath);
        
    }
    
}
