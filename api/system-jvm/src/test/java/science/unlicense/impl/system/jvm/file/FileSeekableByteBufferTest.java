
package science.unlicense.impl.system.jvm.file;

import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.io.SeekableByteBuffer;

/**
 *
 * @author Johann Sorel
 */
public class FileSeekableByteBufferTest {
    
    @Test
    public void writeBytes() throws IOException, science.unlicense.api.io.IOException {
        
        File file = File.createTempFile("tbin", "bin");
        FilePath p = new FilePath(new FileResolver(), file);
        ByteOutputStream out = p.createOutputStream();
        out.write(new byte[]{0,1,2,3,4,5,6,7,8,9,});
        out.flush();
        out.close();
                
        //replace bytes
        SeekableByteBuffer sk = p.createSeekableBuffer(true, true, false);
        sk.setPosition(3);
        sk.write(new byte[]{13,14,15});
        sk.flush();
        assertEquals(6, sk.getPosition());
        assertEquals(10, sk.getSize());
        sk.dispose();
        
        ByteInputStream is = p.createInputStream();
        byte[] all = IOUtilities.readAll(is);
        is.close();
        assertArrayEquals(new byte[]{0,1,2,13,14,15,6,7,8,9}, all);
        
        //write at file end, must fail
        sk = p.createSeekableBuffer(true, true, false);
        sk.setPosition(10);
        try {
            sk.write(new byte[]{13,14,15});
            fail("Expanding file should fail");
        }catch (science.unlicense.api.io.IOException ex) {
            //ok
        }        
        
    }
    
}
