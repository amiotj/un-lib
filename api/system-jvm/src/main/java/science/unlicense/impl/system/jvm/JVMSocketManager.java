
package science.unlicense.impl.system.jvm;

import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.system.ClientSocket;
import science.unlicense.system.ServerSocket;
import science.unlicense.system.IPAddress;
import science.unlicense.system.SocketManager;

/**
 * Map JVM socket API to UN API.
 * @author Johann Sorel
 */
public class JVMSocketManager implements SocketManager{

    public IPAddress resolve(Chars name) throws IOException {
        final InetAddress ia;
        try {
            ia = InetAddress.getByName(name.toString());
        } catch (UnknownHostException ex) {
            throw new IOException(ex.getMessage());
        }
        return new IPAddress(ia.getAddress());
    }

    public ClientSocket createClientSocket(Chars host, int port) throws IOException {
        return createClientSocket(resolve(host), port);
    }

    public ClientSocket createClientSocket(IPAddress host, int port) throws IOException {
        final Socket s;
        try {
            s = new Socket(InetAddress.getByAddress(host.getValues()), port);
        } catch (UnknownHostException ex) {
            throw new IOException(ex);
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
        
        return new JVMClientSocket(s, host);
    }

    public ServerSocket createServerSocket(int port,ServerSocket.ClientHandler handler) throws IOException {
        try {
            final java.net.ServerSocket s = new java.net.ServerSocket(port);
            return new JVMServerSocket(s, handler);
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
        
    }
    
}
