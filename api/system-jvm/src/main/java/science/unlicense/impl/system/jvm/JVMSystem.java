
package science.unlicense.impl.system.jvm;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import science.unlicense.api.desktop.DefaultTransferBag;
import science.unlicense.api.desktop.TransferBag;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.system.GlobalProperties;
import science.unlicense.system.ModuleManager;
import science.unlicense.system.SocketManager;
import science.unlicense.system.System;

/**
 * JVM environment.
 *
 * @author Johann Sorel
 */
public final class JVMSystem extends System{

    private static final ModuleManager MODULE_MANAGER = new JVMModuleManager();
    private static final SocketManager SOCKET_MANAGER = new JVMSocketManager();
    private static final GlobalProperties PROPERTIES = new JVMGlobalProperties();
    private static final TransferBag CLIPBOARD = new DefaultTransferBag();
    private static final TransferBag DRAGANDDROP = new DefaultTransferBag();

    public ModuleManager getModuleManager() {
        return MODULE_MANAGER;
    }

    public SocketManager getSocketManager() {
        return SOCKET_MANAGER;
    }
    
    public GlobalProperties getProperties(){
        return PROPERTIES;
    }

    public String getType() {
        if(File.pathSeparator.equals("\\")){
            return TYPE_WINDOWS;
        }else{
            return TYPE_UNIX;
        }
    }

    public TransferBag getClipBoard() {
        return CLIPBOARD;
    }

    public TransferBag getDragAndDrapBag() {
        return DRAGANDDROP;
    }

    public static InputStream toInputStream(final ByteInputStream in) {
        if (in instanceof JVMInputStream) {
            return ((JVMInputStream)in).in;
        }
        
        return new InputStream() {
            @Override
            public int read() throws IOException {
                try {
                    return in.read();
                } catch (science.unlicense.api.io.IOException ex) {
                    throw new IOException(ex.getMessage(),ex);
                }
            }

            @Override
            public void close() throws IOException {
                try {
                    in.close();
                } catch (science.unlicense.api.io.IOException ex) {
                    throw new IOException(ex.getMessage(),ex);
                }
            }
        };
    }
    
}
