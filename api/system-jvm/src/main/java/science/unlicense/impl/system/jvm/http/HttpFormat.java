

package science.unlicense.impl.system.jvm.http;

import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 *
 * @author Johann Sorel
 */
public final class HttpFormat implements PathFormat{

    private final HttpResolver resolver;

    public HttpFormat() {
        this.resolver = new HttpResolver(this);
    }

    @Override
    public boolean isAbsolute() {
        return  true;
    }

    @Override
    public boolean canCreate(Path base) throws IOException {
        return false;
    }

    @Override
    public PathResolver createResolver(Path base) throws IOException {
        return resolver;
    }

}
