
package science.unlicense.impl.system.jvm.http;

import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 * Http path resolver
 *
 * @author Johann Sorel
 */
public final class HttpResolver implements PathResolver{

    private static final Chars PREFIX = new Chars("http:");
    private final PathFormat format;

    public HttpResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if(!path.startsWith(PREFIX)){
            return null;
        }

        return new HttpPath(this, path);
    }

}
