
package science.unlicense.impl.system.jvm.mod;

import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 * Module path resolver
 *
 * @author Johann Sorel
 */
public final class ModuleResolver implements PathResolver{

    private static final Chars PREFIX = new Chars("mod:");
    private final PathFormat format;

    public ModuleResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if(!path.startsWith(PREFIX)){
            return null;
        }
        return new ModulePath(this, path.truncate(4,-1));
    }

}
