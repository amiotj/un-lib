
package science.unlicense.impl.system.jvm;

import java.net.URL;
import java.util.Enumeration;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;
import science.unlicense.api.model.tree.DefaultNamedNode;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.system.AbstractModuleManager;
import science.unlicense.system.DefaultMetaTree;
import science.unlicense.system.MetaTree;

/**
 * Module manager for JVM.
 *
 * @author Johann Sorel
 */
public class JVMModuleManager extends AbstractModuleManager{

    private final Dictionary trees = new HashDictionary();

    public JVMModuleManager() {
    }

    public synchronized MetaTree getMetadataRoot(String name) {

        MetaTree meta = (MetaTree) trees.getValue(name);
        if(meta!= null) return meta;


        //load market.
        final NamedNode root = new DefaultNamedNode(Chars.EMPTY,true);
        meta = new DefaultMetaTree(root);

        try {
            final Enumeration<URL> urls = JVMModuleManager.class.getClassLoader().getResources("module/"+name+".tree");
            while (urls.hasMoreElements()) {
                final URL u = urls.nextElement();
                final NamedNode node = MetadataTreeIO.read(u);
                MetadataTreeIO.merge(root, node);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }

        trees.add(name, meta);
        return meta;
    }

    public Set getModules() {
        //TODO
        return new HashSet();
    }

}
