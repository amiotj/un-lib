
package science.unlicense.impl.system.jvm.mod;

import java.io.InputStream;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;
import science.unlicense.impl.system.jvm.JVMInputStream;
import science.unlicense.api.path.AbstractPath;

/**
 * Map a JVM jar resource to Path.
 *
 * @author Johann Sorel
 */
final class ModulePath extends AbstractPath {

    private final PathResolver resolver;
    private final Chars file;

    public ModulePath(final PathResolver resolver, Chars file) {
        this.resolver = resolver;
        this.file = file;
    }

    @Override
    public Chars getName() {
        return file.truncate(file.getLastOccurence('/')+1,-1);
    }

    @Override
    public Path getParent() {
        Chars parentPath = file;
        if(parentPath.endsWith('/')){
            parentPath = parentPath.truncate(0, parentPath.getCharLength()-1);
        }
        final int lastIndex = parentPath.getLastOccurence('/');
        if(lastIndex<=0)return null;
        parentPath = parentPath.truncate(0,lastIndex+1);
        return new ModulePath(resolver, parentPath);
    }

    @Override
    public boolean isContainer() throws IOException {
        return file.endsWith('/');
    }

    public boolean exists() throws IOException {
        return getClass().getResource(file.toString())!=null;
    }
    
    @Override
    public boolean createContainer() throws IOException {
        throw new IOException("Can not create module path");
    }

    @Override
    public boolean createLeaf() throws IOException {
        throw new IOException("Can not create path");
    }
    
    @Override
    public Path resolve(Chars address) {
        final Chars childPath;
        if(file.endsWith('/')){
            childPath = file.concat(address);
        }else{
            childPath = file.concat('/').concat(address);
        }
        return new ModulePath(resolver, childPath);
    }

    @Override
    public PathResolver getResolver() {
        return resolver;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        final InputStream stream = getClass().getResourceAsStream(file.toString());
        if(stream==null){
            throw new IOException("Module file do not exist for path : "+file);
        }
        return new JVMInputStream(stream);
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        throw new IOException("Can not write");
    }

    @Override
    public Collection getChildren() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Chars toURI() {
        return new Chars("mod:").concat(file);
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[0];
    }

    @Override
    public Chars toChars() {
        return new Chars("Module path : "+file);
    }

}