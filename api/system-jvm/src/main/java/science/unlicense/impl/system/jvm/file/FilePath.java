
package science.unlicense.impl.system.jvm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.AbstractCollection;
import science.unlicense.api.collection.AbstractIterator;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.BufferedInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathResolver;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.impl.system.jvm.JVMInputStream;
import science.unlicense.impl.system.jvm.JVMOutputStream;
import science.unlicense.api.path.AbstractPath;
import science.unlicense.api.regex.Regex;
import science.unlicense.api.regex.RegexExec;

/**
 * Map a JVM File to Path.
 *
 * @author Johann Sorel
 */
public final class FilePath extends AbstractPath{

    private static final int SEPARATOR = File.separatorChar;
    private static final boolean IS_WINDOWS = SEPARATOR == '\\';
    private static final RegexExec ABSOLUTE = Regex.compile(new Chars("./"+(IS_WINDOWS?"\\\\":"/")));

    private final PathResolver resolver;
    private final File file;
    private final Collection children = new AbstractCollection() {
        @Override
        public Iterator createIterator() {
            return new AbstractIterator() {
                final File[] array = file.listFiles();
                int i=0;
                @Override
                protected void findNext() {
                    if (array!=null && i<array.length) {
                        nextValue = new FilePath(resolver, array[i++]);
                    }
                }
            };
        }
    };

    public FilePath(final PathResolver resolver, File file) {
        this.resolver = resolver;
        this.file = file;
    }

    public File getFile() {
        return file;
    }
    
    @Override
    public Chars getName() {
        return new Chars(file.getName());
    }

    @Override
    public Path getParent() {
        final File f = file.getParentFile();
        if(f != null){
            return new FilePath(resolver,f);
        }
        return null;
    }

    @Override
    public Object getPathInfo(Chars key) {
        if(INFO_HIDDEN.equals(key)){
            return file.isHidden();
        }else if(INFO_OCTETSIZE.equals(key)){
            return file.length();
        }else if(INFO_LASTMODIFIED.equals(key)){
            return file.lastModified();
        }
        return super.getPathInfo(key);
    }

    @Override
    public boolean isContainer() throws IOException {
        return file.isDirectory();
    }

    public boolean exists() throws IOException {
        return file.exists();
    }
    @Override
    public boolean createContainer() throws IOException {
        return file.mkdirs();
    }

    public boolean createLeaf() throws IOException {
        try {
            return file.createNewFile();
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }
    
    @Override
    public void delete() throws IOException {
        if(file.isDirectory()){
            Iterator ite = children.createIterator();
            while(ite.hasNext()) {
                ((Path)ite.next()).delete();
            }
        }
        
        if(!file.delete()){
            throw new IOException("Failed to delete file : "+file);
        }
    }

    @Override
    public Path resolve(Chars address) {
        if(IS_WINDOWS && ABSOLUTE.match(address)){
            //windows absolute path
            return getResolver().resolve(address);
        }else if(address.startsWith(SEPARATOR)){
            //unix type absolute path
            return getResolver().resolve(address);
        }else{
            if(address.startsWith(SEPARATOR)) address = address.truncate(1, -1);
            final int composed = address.getFirstOccurence(SEPARATOR);
            if(composed < 0){
                //resolve now
                if (address.equals(new Chars("."))) {
                    return this;
                } else if (address.equals(new Chars(".."))) {
                    return getParent();
                } else {
                    final File child = new File(file, address.toString());
                    return new FilePath(resolver, child);
                }
            }else{
                //resolve by children
                final Chars childAddress = address.truncate(0,composed);
                final Chars subAddress = address.truncate(composed+1,-1);

                FilePath base;
                if (childAddress.equals(new Chars("."))) {
                    base = this;
                } else if (childAddress.equals(new Chars(".."))) {
                    base = (FilePath) getParent();
                } else {
                    final File child = new File(file, childAddress.toString());
                    base = new FilePath(resolver, child);
                }

                return base.resolve(subAddress);
            }
        }
    }

    @Override
    public PathResolver getResolver() {
        return resolver;
    }

    @Override
    public ByteInputStream createInputStream() throws IOException {
        try {
            final FileInputStream fs = new FileInputStream(file);
            return new BufferedInputStream(new JVMInputStream(fs));
        } catch (FileNotFoundException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public ByteOutputStream createOutputStream() throws IOException {
        try {
            return new JVMOutputStream(new FileOutputStream(file));
        } catch (FileNotFoundException ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        if(resize){
            throw new IOException("Resizing not supported");
        }
        try {
            return new FileSeekableByteBuffer(file, read, write, resize);
        } catch (FileNotFoundException ex) {
            throw new IOException(ex);
        }
    }
    
    @Override
    public Collection getChildren() {
        return children;
    }

    @Override
    public Chars toURI() {
        String uri = "file:"+file.getPath();
        if(IS_WINDOWS) uri = uri.replace('\\', '/');
        return new Chars(uri);
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[0];
    }

    @Override
    public Chars toChars() {
        return new Chars("File path : "+file.getPath());
    }

}
