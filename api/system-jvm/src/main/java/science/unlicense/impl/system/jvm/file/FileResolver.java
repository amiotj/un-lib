
package science.unlicense.impl.system.jvm.file;

import java.io.File;
import science.unlicense.api.character.Chars;
import science.unlicense.api.path.Path;
import science.unlicense.api.path.PathFormat;
import science.unlicense.api.path.PathResolver;

/**
 * File path resolver
 *
 * @author Johann Sorel
 */
public final class FileResolver implements PathResolver{

    private static final Chars PREFIX = new Chars("file:");
    private final PathFormat format;

    public FileResolver() {
        this.format = new FileFormat();
    }

    public FileResolver(PathFormat format) {
        this.format = format;
    }

    @Override
    public PathFormat getFormat() {
        return format;
    }

    @Override
    public Path resolve(Chars path) {
        if(!path.startsWith(PREFIX)){
            return null;
        }

        path = path.truncate(5,-1);
        File f = new File(path.toString());
        return new FilePath(this, f);
    }

}
