
package science.unlicense.impl.system.jvm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.model.tree.DefaultNamedNode;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;

/**
 * Utility class to read and write metadata trees from files.
 * 
 * @author Johann Sorel
 */
class MetadataTreeIO {
    
    private static final Chars DEPTH = new Chars("    ");
    private static final Chars INTEGER = new Chars("i:");
    private static final Chars DOUBLE = new Chars("d:");
    private static final Chars CHARS = new Chars("c:");
    
    public static NamedNode read(URL u) throws IOException, ClassNotFoundException, InstantiationException, NoSuchFieldException, InvalidArgumentException, IllegalAccessException{
        final String text = readFile(u);

        final NamedNode root = new DefaultNamedNode(true);
        
        //current path
        String[] currentPath = new String[0];
        for(String line : text.split("\n")){
            final int valueSeparator = line.indexOf('=');
            final String fullpath = (valueSeparator>0) ? line.substring(0, valueSeparator) : line;

            if(line.trim().isEmpty() || line.trim().startsWith("#")) continue;

            final String[] path = toPath(fullpath);
            currentPath = merge(currentPath,path);

            //create or found path
            NamedNode node = root;
            for (int i=0;i<currentPath.length;i++) {
                node = getNode(node, new Chars(currentPath[i]),true);
            }

            //set value if any
            if (valueSeparator<=0) {
                continue;
            }

            final String valueStr = line.substring(valueSeparator+1, line.length());
            final int splitIndex = valueStr.indexOf(':');
            final String type = valueStr.substring(0, splitIndex);
            final String value = valueStr.substring(splitIndex+1);
            if ("instance".equalsIgnoreCase(type)) {
                final Class c = Class.forName(value);
                final Object instance = c.newInstance();
                node.setValue(instance);
            } else if("constant".equalsIgnoreCase(type)) {
                final int s = value.lastIndexOf('.');
                final String className = value.substring(0,s);
                final String propName = value.substring(s+1);
                final Class c = Class.forName(className);
                final Field f = c.getDeclaredField(propName);
                final Object constant = f.get(null);
                node.setValue(constant);
            } else if("i".equalsIgnoreCase(type)) {
                node.setValue(Integer.valueOf(value));
            } else if("d".equalsIgnoreCase(type)) {
                node.setValue(Double.valueOf(value));
            } else if("c".equalsIgnoreCase(type)) {
                node.setValue(new Chars(value));
            } else {
                throw new RuntimeException("missing node value");
            }
        }
        
        return root;
    }
    
    public static void write(NamedNode node, URL url) throws URISyntaxException, FileNotFoundException, science.unlicense.api.io.IOException{
        
        File file = new File(url.toURI());
        final CharOutputStream cs = new CharOutputStream(new JVMOutputStream(new FileOutputStream(file)),CharEncodings.UTF_8);
        write(-1,node,cs); 
        cs.close();
    }
    
    private static void write(int depth, NamedNode node, CharOutputStream out) throws science.unlicense.api.io.IOException{
        
        for(int i=0;i<depth;i++){
            out.write(DEPTH);
        }
        if(depth>=0){
            out.write(node.getName());
            
            final Object value = node.getValue();
            if(value!=null){
                out.write('=');
                if(value instanceof Integer){
                    out.write(INTEGER).write(Int32.encode((Integer)value));
                }else if(value instanceof Double){
                    out.write(DOUBLE).write(Float64.encode((Integer)value));
                }else if(value instanceof Chars){
                    out.write(CHARS).write((Chars)value);
                }else{
                    throw new science.unlicense.api.io.IOException("Value class can not be stored : "+value.getClass());
                }
            }
            out.write('\n');
        }
        
        //store children nodes
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            write(depth+1, (NamedNode)ite.next(), out);
        }
    }
    
    /**
     * Merge the second tree in the first one.
     * 
     * @param base
     * @param other
     * @return 
     */
    public static void merge(NamedNode base, NamedNode other){
        //merge value
        if(other.getValue()!=null){
            if(base.getValue()!=null){
                if(!other.getValue().equals(base.getValue())){
                    throw new InvalidArgumentException("Node for same path have different values "+base);
                }
            }else{
                base.setValue(other.getValue());
            }
        }
        
        //merge children
        for (Iterator ite=other.getChildren().createIterator();ite.hasNext();) {
            final NamedNode n = (NamedNode)ite.next();
            NamedNode sibling = base.getNamedChild(n.getName());
            if(sibling==null){
                base.getChildren().add(n);
            }else{
                merge(sibling,n);
            }
        }
        
    }
    
    private static String[] toPath(String path){
        String[] stack = new String[0];
        while(path.startsWith("    ")){
            stack = Arrays.copyOf(stack, stack.length+1);
            path = path.substring(4);
        }
        stack = Arrays.copyOf(stack, stack.length+1);
        stack[stack.length-1] = path;
        return stack;
    }

    private static String[] merge(String[] currentPath, String[] path){
        final String[] result = path.clone();
        for(int i=0;i<result.length-1;i++){
            if(result[i]==null){
                result[i] = currentPath[i];
            }
        }
        return result;
    }

    private static NamedNode getNode(NamedNode parent, Chars name, boolean create){
        for (Iterator ite=parent.getChildren().createIterator();ite.hasNext();) {
            final Object n = ite.next();
            if(((NamedNode)n).getName().equals(name, true, true)){
                return (NamedNode) n;
            }
        }
        if(create){
            //create it
            NamedNode n = new DefaultNamedNode(name,true);
            parent.getChildren().add(n);
            return n;
        }else{
            return null;
        }

    }

    private static String readFile(URL url) throws IOException{
        final StringBuilder sb  = new StringBuilder();
        final InputStream stream = url.openStream();
        final BufferedReader br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        String line;
        try{
            while ((line = br.readLine()) != null) {
                sb.append(line).append('\n');
            }
        }finally{
            br.close();
            stream.close();
        }
        return sb.toString();
    }
    
}
