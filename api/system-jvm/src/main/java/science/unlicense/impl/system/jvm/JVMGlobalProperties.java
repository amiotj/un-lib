
package science.unlicense.impl.system.jvm;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.DefaultNamedNode;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.path.Path;
import science.unlicense.system.DefaultMetaTree;
import science.unlicense.system.GlobalProperties;
import science.unlicense.system.MetaTree;
import science.unlicense.impl.system.jvm.file.FilePath;
import science.unlicense.impl.system.jvm.file.FileResolver;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JVMGlobalProperties implements GlobalProperties {

    private final MetaTree systemTree;
    private final MetaTree persTree;
    private final MetaTree execTree;

    public JVMGlobalProperties() {
        final NamedNode systemRoot = new DefaultNamedNode(true);
        final NamedNode persRoot = new DefaultNamedNode(true);
        final NamedNode execRoot = new DefaultNamedNode(true);
        systemTree = new DefaultMetaTree(systemRoot);
        persTree = new DefaultMetaTree(persRoot);
        execTree = new DefaultMetaTree(execRoot);
        
        //system informations --------------------------------------------------
        final NamedNode system = new DefaultNamedNode(new Chars("system"),true);
        systemRoot.getChildren().add(system);

        //list root files
        final FileResolver fileResolver = new FileResolver();
        final NamedNode fileRoots = new DefaultNamedNode(new Chars("fileRoots"),true);
        system.getChildren().add(fileRoots);
        for(File fileroot : File.listRoots()){
            final NamedNode fr = new DefaultNamedNode(new Chars(fileroot.getName()), new FilePath(fileResolver, fileroot),true);
            fileRoots.getChildren().add(fr);
        }

         //user informations
        final NamedNode user = new DefaultNamedNode(new Chars("user"),true);
        system.getChildren().add(user);

        final NamedNode userHome = new DefaultNamedNode(new Chars("home"),
                new Chars("file:"+java.lang.System.getProperty("user.home")),true);
        final NamedNode userName = new DefaultNamedNode(new Chars("name"),
                new Chars(java.lang.System.getProperty("user.name")),true);
        user.getChildren().add(userHome);
        user.getChildren().add(userName);
        
        //load persisted informations ------------------------------------------
        URL persUrl = null;
        try{
            Path path = Paths.resolve((Chars)userHome.getValue());
            path = path.resolve(new Chars(".unlicense"));
            path.createContainer();
            path = path.resolve(new Chars("persistant.properties"));
            persUrl = new URL(path.toURI().toString());
            if(path.exists()){
                final NamedNode pers = MetadataTreeIO.read(persUrl);
                MetadataTreeIO.merge(persRoot, pers);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
           
        //save changes
        final URL p = persUrl;
        persRoot.addEventListener(NodeMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                try {
                    MetadataTreeIO.write(persRoot, p);
                } catch (URISyntaxException ex) {
                    ex.printStackTrace();
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        
    }
    
    public MetaTree getSystemTree() {
        return systemTree;
    }

    public MetaTree getPersistantTree() {
        return persTree;
    }

    public MetaTree getExecutionTree() {
        return execTree;
    }
    
}
