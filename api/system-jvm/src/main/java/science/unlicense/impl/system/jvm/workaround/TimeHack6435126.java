package science.unlicense.impl.system.jvm.workaround;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Enable the workaround for JDK-6435126
 *
 * Original project : https://io7m.github.io/timehack6435126/
 *
 * @see <a href="http://bugs.java.com/view_bug.do?bug_id=6435126">http://bugs.java.com/view_bug.do?bug_id=6435126</a>
 *
 */
public final class TimeHack6435126 {

    private TimeHack6435126() {
    }

    private static final AtomicBoolean ENABLED;

    static {
        ENABLED = new AtomicBoolean(false);
    }

    /**
     * Enable the operating system's high resolution timer.
     */
    public static void enableHighResolutionTimer() {
        if (TimeHack6435126.ENABLED.compareAndSet(false, true)) {
            final Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(Integer.MAX_VALUE);
                    } catch (final InterruptedException e) {
                        // Nothing
                    }
                }
            };
            t.setName("timehack6435126");
            t.setDaemon(true);
            t.start();
        }
    }
}
