

package science.unlicense.impl.system.jvm.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import science.unlicense.api.io.AbstractSeekableByteBuffer;
import science.unlicense.api.io.EOSException;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class FileSeekableByteBuffer extends AbstractSeekableByteBuffer {

    private final File file;
    private final RandomAccessFile raf;
    private final boolean resize;

    public FileSeekableByteBuffer(File file, boolean read, boolean write, boolean resize) throws FileNotFoundException {
        this.file = file;
        this.raf = new RandomAccessFile(file,write?"rw":"r");
        this.resize = resize;
    }
    
    public long getSize() throws IOException{
        try {
            return raf.length();
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }

    public void setPosition(long position) throws IOException {
        try {
            raf.seek(position);
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }

    public long getPosition() throws IOException {
        try {
            return raf.getFilePointer();
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }

    public long skip(long nb) throws IOException {
        try {
            return raf.skipBytes((int)nb);
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }

    public byte[] read(byte[] buffer, int offset, int length) throws IOException {
        while(length>0){
            try {
                int n = raf.read(buffer, offset, length);
                if(n==-1){
                    throw new EOSException("Not enough datas");
                }
                offset+=n;
                length-=n;
            } catch (java.io.EOFException ex) {
                throw new EOSException(ex.getMessage());
            } catch (java.io.IOException ex) {
                throw new IOException(ex);
            }
        }
        return buffer;
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        if (!resize) {
            if (getPosition()+length >= getSize()) {
                throw new IOException("seekable buffer can not be resized");
            }
        }
        try {
            raf.write(buffer, offset, length);
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }
    
    public void dispose() throws IOException {
        try {
            raf.close();
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }
    
}
