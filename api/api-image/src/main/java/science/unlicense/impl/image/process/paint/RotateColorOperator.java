
package science.unlicense.impl.image.process.paint;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractColorPointOperator;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.impl.color.colorspace.HSL;

/**
 * Rotate color.
 * 
 * @author Johann Sorel
 */
public class RotateColorOperator extends AbstractColorPointOperator {

    /**
     * Adjust color hue, default is 0.
     * This value will be added to the pixel hue and wrapped to 0 - 360
     */
    public static final FieldType INPUT_HUE = new FieldTypeBuilder(new Chars("Hue")).title(new Chars("Hue")).valueClass(Float.class).defaultValue(0f).build();

    /**
     * Adjust color saturation, default is 1.
     * This value will be multiplied to the pixel saturation and clipped to 0 - 1
     */
    public static final FieldType INPUT_SATURATION = new FieldTypeBuilder(new Chars("Saturation")).title(new Chars("Saturation")).valueClass(Float.class).defaultValue(1f).build();

    /**
     * Adjust color lightning, default is 1.
     * This value will be multiplied to the pixel lightning and clipped to 0 - 1
     */
    public static final FieldType INPUT_LIGHTNING = new FieldTypeBuilder(new Chars("Ligthning")).title(new Chars("Ligthning")).valueClass(Float.class).defaultValue(1f).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("rotatecolor"), new Chars("RotateColor"), Chars.EMPTY, RotateColorOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_HUE,INPUT_SATURATION,INPUT_LIGHTNING},
                new FieldType[]{OUTPUT_IMAGE});
    
    private final float[] hsl = new float[3];
    private final float[] bufferRGB = new float[4];
    private final float[] bufferHSL = new float[3];

    public RotateColorOperator() {
        super(DESCRIPTOR);
    }
    
    public Image execute(Image image, float h, float s, float l) {
        final Document params = new DefaultDocument(descriptor.getInputType());
        params.setFieldValue(INPUT_IMAGE.getId(),image);
        params.setFieldValue(INPUT_HUE.getId(),h);
        params.setFieldValue(INPUT_SATURATION.getId(),s);
        params.setFieldValue(INPUT_LIGHTNING.getId(),l);
        setInput(params);
        final Document result = execute();
        return (Image) result.getFieldValue(OUTPUT_IMAGE.getId());
    }
    
    protected void prepareInputs() {
        hsl[0] = (Float) inputParameters.getFieldValue(INPUT_HUE.getId());
        hsl[1] = (Float) inputParameters.getFieldValue(INPUT_SATURATION.getId());
        hsl[2] = (Float) inputParameters.getFieldValue(INPUT_LIGHTNING.getId());
    }

    protected Color evaluate(Color color) {
        color.toRGBA(bufferRGB, 0);
        final float alpha = bufferRGB[3];
        HSL.INSTANCE.toSpace(bufferRGB, bufferHSL);
        bufferHSL[0] += hsl[0];
        bufferHSL[1] *= hsl[1];
        bufferHSL[2] *= hsl[2];
        //ensure correct limits
        bufferHSL[0] = Maths.wrap(bufferHSL[0], 0, 360);
        bufferHSL[1] = Maths.clamp(bufferHSL[1], 0, 1);
        bufferHSL[2] = Maths.clamp(bufferHSL[2], 0, 1);
        HSL.INSTANCE.toRGBA(bufferHSL, bufferRGB);
        bufferRGB[3] = alpha;
        return new Color(bufferRGB);
    }

    public static Color rotate(Color color, double h, double s, double l){
        final float[] bufferRGB = new float[4];
        final float[] bufferHSL = new float[3];
        color.toRGBA(bufferRGB, 0);
        final float alpha = bufferRGB[3];
        HSL.INSTANCE.toSpace(bufferRGB, bufferHSL);
        bufferHSL[0] += h;
        bufferHSL[1] *= s;
        bufferHSL[2] *= l;
        //ensure correct limits
        bufferHSL[0] = Maths.wrap(bufferHSL[0], 0, 360);
        bufferHSL[1] = Maths.clamp(bufferHSL[1], 0, 1);
        bufferHSL[2] = Maths.clamp(bufferHSL[2], 0, 1);
        HSL.INSTANCE.toRGBA(bufferHSL, bufferRGB);
        bufferRGB[3] = alpha;
        return new Color(bufferRGB);
    }
}
