
package science.unlicense.impl.image.process.paint;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractColorPointOperator;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.impl.color.colorspace.HSL;

/**
 * Colorize an image using hue, saturation and lightning.
 * 
 * @author Johann Sorel
 */
public class ColorizeOperator extends AbstractColorPointOperator {

    public static final FieldType INPUT_HUE = new FieldTypeBuilder(new Chars("Hue")).title(new Chars("Hue")).valueClass(Float.class).build();
    public static final FieldType INPUT_SATURATION = new FieldTypeBuilder(new Chars("Saturation")).title(new Chars("Saturation")).valueClass(Float.class).build();
    public static final FieldType INPUT_LIGHTNING = new FieldTypeBuilder(new Chars("Ligthning")).title(new Chars("Ligthning")).valueClass(Float.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("colorize"), new Chars("Colorize"), Chars.EMPTY, ColorizeOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_HUE,INPUT_SATURATION,INPUT_LIGHTNING},
                new FieldType[]{OUTPUT_IMAGE});
    
    private final float[] hsl = new float[3];
    private final float[] bufferRGB = new float[4];
    private final float[] bufferHSL = new float[3];

    public ColorizeOperator() {
        super(DESCRIPTOR);
    }
    
    public Image execute(Image image, float h, float s, float l) {
        final Document params = new DefaultDocument(descriptor.getInputType());
        params.setFieldValue(INPUT_IMAGE.getId(),image);
        params.setFieldValue(INPUT_HUE.getId(),h);
        params.setFieldValue(INPUT_SATURATION.getId(),s);
        params.setFieldValue(INPUT_LIGHTNING.getId(),l);
        setInput(params);
        final Document result = execute();
        return (Image) result.getFieldValue(OUTPUT_IMAGE.getId());
    }
    
    protected void prepareInputs() {
        hsl[0] = (Float) inputParameters.getFieldValue(INPUT_HUE.getId());
        hsl[1] = (Float) inputParameters.getFieldValue(INPUT_SATURATION.getId());
        hsl[2] = (Float) inputParameters.getFieldValue(INPUT_LIGHTNING.getId());
    }

    protected Color evaluate(Color color) {
        color.toRGBA(bufferRGB, 0);
        final float alpha = bufferRGB[3];
        HSL.INSTANCE.toSpace(bufferRGB, bufferHSL);
        bufferHSL[0] = hsl[0];
        bufferHSL[1] = hsl[1];
        bufferHSL[2] *= hsl[2];
        HSL.INSTANCE.toRGBA(bufferHSL, bufferRGB);
        bufferRGB[3] = alpha;
        return new Color(bufferRGB);
    }

}
