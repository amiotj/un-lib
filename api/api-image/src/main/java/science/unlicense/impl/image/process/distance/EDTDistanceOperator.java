
package science.unlicense.impl.image.process.distance;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.InterpolatedColorModel;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.task.AbstractTask;

/**
 * 8SSEDT implementation of distance map.
 * 
 * This implementation produce an image with distances positive outside the mask
 * and negative distances inside the mask.
 * 
 * resources :
 * http://www.lems.brown.edu/vision/people/leymarie/Refs/CompVision/DT/DTpaper.pdf
 * http://www.ee.bgu.ac.il/~dinstein/stip2002/LeymarieLevineDistTrans_cvgip92.pdf
 * http://www.codersnotes.com/notes/signed-distance-fields/
 * 
 * @author Johann Sorel
 */
public class EDTDistanceOperator extends AbstractTask{
    
    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("edtdistance"), new Chars("edtdistance"), Chars.EMPTY, EDTDistanceOperator.class,
                new FieldType[]{INPUT_IMAGE},
                new FieldType[]{OUTPUT_IMAGE});

    //variable used for far pixel, used for uncalculated pixels.
    private final Cell far = new Cell();
    
    public EDTDistanceOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image){
        final Document param = new DefaultDocument(false,descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),image);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }
    
    @Override
    public Document execute() {
        final Image img = (Image) inputParameters.getFieldValue(INPUT_IMAGE.getId());
        
        final Extent.Long extent = img.getExtent();
                
        final int height = (int)extent.getL(1);
        final int width = (int)extent.getL(0);
        //a far coordinate, ensured to always be farther then any in image pixel
        far.x = height*2 + width*2;
        far.y = far.x;
        far.d2 = distSqrt(far.x, far.y);
        
        //this grid contains 0 for pixels inside geometry
        final Cell[][] outterDistance = new Cell[height][width];
        //this grid contains 0 for pixels outside geometry
        final Cell[][] innerDistance = new Cell[height][width];
        
        //fill grids with 0 or far if pixel is inside or outside
        final TupleIterator ite = img.getRawModel().asTupleBuffer(img).createIterator(null);        
        int[] coord = ite.getCoordinate();
        for(boolean[] tuple = ite.nextAsBoolean(null);tuple!=null;tuple=ite.nextAsBoolean(tuple)){
            outterDistance[coord[1]][coord[0]] = new Cell();
            innerDistance[coord[1]][coord[0]] = new Cell();
            if(tuple[0]){
                outterDistance[coord[1]][coord[0]].x = 0;
                outterDistance[coord[1]][coord[0]].y = 0;
                innerDistance[coord[1]][coord[0]].x = far.x;
                innerDistance[coord[1]][coord[0]].y = far.y;
                innerDistance[coord[1]][coord[0]].d2 = far.d2;
            }else{
                outterDistance[coord[1]][coord[0]].x = far.x;
                outterDistance[coord[1]][coord[0]].y = far.y;
                outterDistance[coord[1]][coord[0]].d2 = far.d2;
                innerDistance[coord[1]][coord[0]].x = 0;
                innerDistance[coord[1]][coord[0]].y = 0;
            }
        }
        
        // Calculate distances
        computeDistances(outterDistance);
        computeDistances(innerDistance);
        
        //Create distance result image
        final Image result = Images.createCustomBand(extent, 1, RawModel.TYPE_DOUBLE);
        final TupleBuffer tb = result.getRawModel().asTupleBuffer(result);
        double[] sample = new double[1];
        coord = new int[2];
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        double distIn, distOut;
        Cell c1,c2;
        for(coord[1]=0;coord[1]<height;coord[1]++){
            for(coord[0]=0;coord[0]<width;coord[0]++){
                c1 = outterDistance[coord[1]][coord[0]];
                c2 = innerDistance[coord[1]][coord[0]];
                distOut = Math.sqrt(distSqrt(c1.x,c1.y));
                distIn = -Math.sqrt(distSqrt(c2.x,c2.y));
                min = Maths.min(min, distOut);
                min = Maths.min(min, distIn);
                max = Maths.max(max, distOut);
                max = Maths.max(max, distIn);
                sample[0] = distIn<0 ? distIn : distOut;
                tb.setTuple(coord, sample);
            }
        }
        
        //create a grayscale palette between min and max
        final ColorModel cm = new InterpolatedColorModel(RawModel.TYPE_DOUBLE, 
                new Color[]{Color.BLACK,Color.WHITE}, new float[]{(float)min,(float)max});
        result.getModels().add(ColorModel.MODEL_COLOR, cm);
        
        outputParameters.setFieldValue(OUTPUT_IMAGE.getId(),result);
        return outputParameters;
    }
    
    private Cell get(Cell[][] g, int x, int y ){
        if(x>=0 && y>=0 && x<g[0].length && y<g.length ){
            return g[y][x];
        }else{
            return far;
        }
    }
        
    private void compare(Cell[][] g, int x, int y, int offsetx, int offsety){
        final Cell p = g[y][x];
        final Cell other = get(g, x+offsetx, y+offsety);
        int d2 = distSqrt(other.x+offsetx, other.y+offsety);
        if (d2 < p.d2){
            p.x = other.x+offsetx;
            p.y = other.y+offsety;
            p.d2 = d2;
        }
    }

    private void computeDistances(Cell[][] g){
        final int height = g.length;
        final int width = g[0].length;
        
        // Pass 0
        for (int y=0;y<height;y++){
            for (int x=0;x<width;x++){
                compare(g, x, y, -1,  0);
                compare(g, x, y,  0, -1);
                compare(g, x, y, -1, -1);
                compare(g, x, y,  1, -1);
            }
            for (int x=width-1;x>=0;x--){
                compare(g, x, y, 1, 0);
            }
        }

        // Pass 1
        for (int y=height-1;y>=0;y--){
            for (int x=width-1;x>=0;x--){
                compare(g, x, y,  1,  0);
                compare(g, x, y,  0,  1);
                compare(g, x, y, -1,  1);
                compare(g, x, y,  1,  1);
            }
            for (int x=0;x<width;x++){
                compare(g, x, y, -1, 0);
            }
        }
    }
    
    private static int distSqrt(int x, int y){
        return x*x + y*y;
    }
    
    private static class Cell{
        private int x,y;        
        private int d2;
    }
    
}
