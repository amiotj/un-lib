
package science.unlicense.impl.image;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import science.unlicense.api.image.color.BitPackColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.sample.BitPackRawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.io.BacktrackInputStream;

/**
 * The raw image reader is not associated to a format because such images
 * are often stored a binary files with metadata or specific extensions.
 * 
 * This reader support several uncompressed raw formats.
 * 
 * @author Johann Sorel
 */
public class RawImageReader extends AbstractImageReader {

    public static final int IMAGE_TYPE_RGB24    = 1;
    
    public static final int IMAGE_TYPE_ARGB32   = 2;
    public static final int IMAGE_TYPE_ARGB4444 = 3;    
    public static final int IMAGE_TYPE_BGRA32   = 4;
    public static final int IMAGE_TYPE_RGBA32   = 5;
    public static final int IMAGE_TYPE_RGBA4444 = 6;    
    public static final int IMAGE_TYPE_RGB565   = 7;
    
    public static final int IMAGE_TYPE_ALPHA8   = 8;
    
    
    
    private final Extent.Long size;
    private final int format;

    public RawImageReader(Extent.Long size, int format) {
        this.size = size;
        this.format = format;
    }
    
    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        return new HashDictionary();
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        
        final long nbPixel = size.getL(0)*size.getL(1);
        final BufferFactory bf = parameters.getBufferFactory();
        
        final Buffer buffer;
        final RawModel rm;
        final ColorModel cm;
        if(format==IMAGE_TYPE_RGB24){
            buffer = bf.createByte(nbPixel*3);
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
            cm = new DirectColorModel(rm, new int[]{0,1,2,-1}, false);
        }else if(format==IMAGE_TYPE_ARGB32){
            buffer = bf.createByte(nbPixel*4);
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
            cm = new DirectColorModel(rm, new int[]{3,0,1,2}, false);
        }else if(format==IMAGE_TYPE_ARGB4444){
            buffer = bf.createByte(nbPixel*2);
            rm = new BitPackRawModel(16, new int[]{0,4,8,12}, new int[]{4,4,4,4});
            cm = new BitPackColorModel(new int[]{4,4,4,4}, new int[]{1,2,3,0}, false);
        }else if(format==IMAGE_TYPE_BGRA32){
            buffer = bf.createByte(nbPixel*4);
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
            cm = new DirectColorModel(rm, new int[]{2,1,0,3}, false);
        }else if(format==IMAGE_TYPE_RGBA32){
            buffer = bf.createByte(nbPixel*4);
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
            cm = new DirectColorModel(rm, new int[]{0,1,2,3}, false);
        }else if(format==IMAGE_TYPE_RGBA4444){
            buffer = bf.createByte(nbPixel*2);
            rm = new BitPackRawModel(16, new int[]{0,4,8,12}, new int[]{4,4,4,4});
            cm = new BitPackColorModel(new int[]{4,4,4,4}, new int[]{0,1,2,3}, false);
        }else if(format==IMAGE_TYPE_RGB565){
            buffer = bf.createByte(nbPixel*2);
            rm = new BitPackRawModel(16, new int[]{0,5,11}, new int[]{5,6,5});
            cm = new BitPackColorModel(new int[]{5,6,5}, new int[]{0,0,0,-1}, false);
        }else if(format==IMAGE_TYPE_ALPHA8){
            buffer = bf.createByte(nbPixel);
            rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 1);
            cm = new DirectColorModel(rm, new int[]{-1,-1,-1,0}, false);
        }else{
            throw new IOException("Unsupported format");
        }
        
        final DataInputStream ds = new DataInputStream(stream);
        ds.readFully(buffer);
        
        return new DefaultImage(buffer, size, rm, cm);
    }
    
}
