package science.unlicense.impl.image.process.detection;

/**
 * A direction in the plane. As a convenience, directions provide unit vector
 * components (manhattan metric) for both the conventional plane and screen
 * coordinates (y axis reversed).
 *
 * Origin ;
 * http://www.tomgibara.com/computer-vision/Direction.java
 * 
 * @author Tom Gibara (Original author)
 * @author Johann Sorel (Adapted to Unlicense-Lib)
 */
public class Direction {

    public static final Direction E  = new Direction(1, 0); 
    public static final Direction NE = new Direction(1, 1);
    public static final Direction N  = new Direction(0, 1); 
    public static final Direction NW = new Direction(-1, 1);
    public static final Direction W  = new Direction(-1, 0); 
    public static final Direction SW = new Direction(-1, -1);
    public static final Direction S  = new Direction(0, -1);
    public static final Direction SE = new Direction(1, -1);

    /**
     * The horizontal distance moved in this direction within the plane.
     */
    public final int planeX;

    /**
     * The vertical distance moved in this direction within the plane.
     */
    public final int planeY;

    /**
     * The horizontal distance moved in this direction in screen coordinates.
     */
    public final int screenX;

    /**
     * The vertical distance moved in this direction in screen coordinates.
     */
    public final int screenY;

    /**
     * The euclidean length of this direction's vectors.
     */
    public final double length;

    private Direction(int x, int y) {
        planeX = x;
        planeY = y;
        screenX = x;
        screenY = -y;
        length = x != 0 && y != 0 ? Math.sqrt(2.0) / 2.0 : 1.0;
    }

}
