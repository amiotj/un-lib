
package science.unlicense.impl.image.process.paint;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.api.character.Chars;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.math.Maths;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import static science.unlicense.api.image.sample.RawModel.TYPE_1_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_2_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_4_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_INT;
import static science.unlicense.api.image.sample.RawModel.TYPE_LONG;
import static science.unlicense.api.image.sample.RawModel.TYPE_SHORT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_UINT;
import static science.unlicense.api.image.sample.RawModel.TYPE_USHORT;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;


/**
 * InPaint by isophote continuation.
 * http://www.developpez.net/forums/d947804/autres-langages/algorithmes/contribuez/java-patchmatch-inpainting-texture/
 *
 * @author Xavier Philippeau
 * @author Florent Humbert
 * @author Johann Sorel
 *
 */
public class InpaintOperator extends AbstractTask {

    public static final FieldType INPUT_MASK = new FieldTypeBuilder(new Chars("InputMask")).title(new Chars("Input mask, image  same size as source with 1bit data type.")).valueClass(Image.class).build();
    public static final FieldType INPUT_PRESERVATION = new FieldTypeBuilder(new Chars("Preservation")).title(new Chars("Input isophote preservation factor.")).valueClass(Integer.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("inpaint"), new Chars("InPaint"), Chars.EMPTY, InpaintOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_MASK,INPUT_PRESERVATION},
                new FieldType[]{OUTPUT_IMAGE});
    
    //in/out image and informations
    private Image input;
    private TupleBuffer inputsm;
    private ColorModel inputcm;
    private Image output;
    private TupleBuffer outputsm;
    private ColorModel outputcm;
    private Object sampleContainer;
    private double[] sampleContainerD;
    private int width;
    private int height;
    private int nbSample;
    private int sampleType;

    // neighbours offsets (for border spreading)
    private int[] dx4 = new int[] {-1, 0, 1, 0};
    private int[] dy4 = new int[] { 0,-1, 0, 1};

    // neighbours offsets (for sampling)
    private int[] dxs = null;
    private int[] dys = null;

    // distance Map to the unmasked part of the image
    private int[][] distmap = null;
    private int[][] intensity = null;

    // mask
    private boolean[][] mask = null;

    // isophote preservation factor
    private int preservation = 0;

    public InpaintOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Image mask, int preservation){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),image);
        param.setFieldValue(INPUT_MASK.getId(),mask);
        param.setFieldValue(INPUT_PRESERVATION.getId(),preservation);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(OUTPUT_IMAGE.getId());
    }

    public Document execute() {
        final Image maski = (Image) inputParameters.getFieldValue(INPUT_MASK.getId());
        preservation = (Integer)inputParameters.getFieldValue(INPUT_PRESERVATION.getId());

        input = (Image) inputParameters.getFieldValue(INPUT_IMAGE.getId());
        inputsm = input.getRawModel().asTupleBuffer(input);
        sampleContainer = inputsm.createTupleStore();
        nbSample = inputsm.getSampleCount();
        width = (int) input.getExtent().getL(0);
        height = (int) input.getExtent().getL(1);
        sampleType = inputsm.getPrimitiveType();
        sampleContainerD = new double[nbSample];

        // Initialize dxs[] and dys[] tables
        initSample();

        //build output image
        output = Images.create(input, input.getExtent());
        outputsm = output.getRawModel().asTupleBuffer(output);
        outputcm = output.getColorModel();
        output = Images.create(input, input.getExtent());

        // allocate memory
        this.mask = new boolean[this.width][this.height];
        this.intensity = new int[this.width][this.height];
        this.distmap = new int[this.width][this.height];

        // create mask
        final int[] coord = new int[2];
        final boolean[] sample = new boolean[1];
        final TupleBuffer msm = maski.getRawModel().asTupleBuffer(maski);
        for (coord[1] = 0; coord[1] < this.height; coord[1]++){
            for (coord[0] = 0; coord[0] < this.width; coord[0]++){
                msm.getTupleBoolean(coord, sample);
                mask[coord[0]][coord[1]] = sample[0];
            }
        }

        inpaintloop();

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),output);
        return outputParameters;
	}

	private void initSample() {
		// Initialize neighbours offsets for sampling
		dxs = new int[nbSample];
		dys = new int[nbSample];

		// **** build a spiral curve ****

		// directions: Left=(-1,0) Up=(0,-1) Right=(1,0) Down=(0,1)
		int[] dx = new int[] {-1, 0,1,0};
		int[] dy = new int[] { 0,-1,0,1};
		int dirIndex=0;
		int distance=0;
		int stepToDo=1;
		int x=0, y=0;
		while (true) {
			// move two times with the same StepCount
			for (int i = 0; i < 2; i++) {
				// move
				for (int j = 0; j < stepToDo; j++) {
					x += dx[dirIndex];
					y += dy[dirIndex];
					dxs[distance] = x;
					dys[distance] = y;
					distance++;
					if (distance >= nbSample) return;
				}
				// turn right
				dirIndex = (dirIndex + 1) % 4;
			}
			// increment StepCount
			stepToDo++;
		}
	}

	// ---------------------------------------------------------------------------------


	// Compute the initial borderline (unmasked pixels close to the mask)
	private List<int[]> computeBorderline() {

		List<int[]> borderline = new ArrayList<int[]>();

		for (int y=0; y<this.height; y++) {
			for (int x=0; x<this.width; x++) {
				// for each pixel NOT masked
				if (mask[x][y]) continue;

				// if a neighboor is masked
				// => put the pixel in the borderline list
				for (int k=0; k<4; k++) {
					int xk = x+dx4[k];
					int yk = y+dy4[k];
					if (xk<0 || xk>=this.width) continue;
					if (yk<0 || yk>=this.height) continue;
					if (mask[xk][yk]) {
						borderline.add(new int[] {x,y});
						break;
					}
				}
			}
		}
		return borderline;
	}

	// iteratively inpaint the image
	private void inpaintloop() {

		// initialize output image, distance map and intensity map
        final int[] coord = new int[2];
		for (int y=0; y<this.height; y++) {
			for (int x=0; x<this.width; x++) {
                 coord[0] = x; coord[1] = y;

				 // known value -> copy to ouput image
				if (!mask[x][y]){
                    outputsm.setTuple(coord, inputsm.getTuple(coord, sampleContainer));
                }

				// outside the mask -> distance = 0
				// inside the mask -> distance unknown
				if (!mask[x][y]){
					this.distmap[x][y]=0;
                }else{
					this.distmap[x][y]=Integer.MAX_VALUE;
                }

				// intensity
				intensity[x][y]=getIntensity(inputsm,coord);
			}
		}

		// outer borderline
		List<int[]> borderline = computeBorderline();

		// iteratively reduce the borderline
		while(!borderline.isEmpty()) {
			borderline = propagateBorderline(borderline);
		}
	}

	// inpaint pixels close to the borderline
	private List<int[]> propagateBorderline(List<int[]> boderline) {

		List<int[]> newBorderline = new ArrayList<int[]>();

        final int[] coord = new int[2];
		// for each pixel in the bordeline
		for (int[] pixel : boderline) {
			int x=pixel[0];
			int y=pixel[1];

			// distance from the image
			int dist = this.distmap[x][y];

			// explore neighbours, search for uncomputed pixels
			for (int k=0; k<4; k++) {
				int xk = x+dx4[k];
				int yk = y+dy4[k];
				if (xk<0 || xk>=this.width) continue;
				if (yk<0 || yk>=this.height) continue;

				if (!mask[xk][yk]) continue; // pixel value is already known.

				// compute distance to image
				this.distmap[xk][yk]=dist+1;

				// inpaint this pixel
				double[] result = inpaint(xk, yk);
				if (result==null) {
					// should not happen.
					System.err.println("inpaint for "+xk+","+yk+" returns "+result);
					continue;
				}

				// update output image, mask and intensity map
                adjustInOutSamples(result);
                coord[0] = xk;
                coord[1] = yk;
                outputsm.setTuple(coord, sampleContainer);
				intensity[xk][yk] = getIntensity(outputsm,coord);
				mask[xk][yk] = false;

				// add this pixel to the new borderline
				newBorderline.add( new int[]{xk,yk} );
			}
		}

		return newBorderline;
	}

	// inpaint one pixel
	private double[] inpaint(int x, int y) {

		final double[] vinpaint = new double[nbSample];
		final int dist = distmap[x][y];

		// sampling pixels in the region
		List<int[]> region = new ArrayList<int[]>();
		for (int k=0; k<dxs.length; k++) {
			int xk = x+dxs[k];
			int yk = y+dys[k];
			if (xk<0 ||xk>=this.width) continue;
			if (yk<0 ||yk>=this.height) continue;

			// take only pixels computed in previous loops
			int distk = distmap[xk][yk];
			if (distk>=dist) continue;

			region.add( new int[]{xk,yk} );
		}

		// mean isophote vector of the region
		double isox = 0, isoy = 0;
		int count=0;
		for (int[] pixel: region) {
			// isophote direction = normal to the gradient
			final double[] g = gradient(intensity, pixel[0], pixel[1], dist);
			if (g!=null){
				isox += -g[1] * g[2];
				isoy += g[0] * g[2];
				count++;
			}
		}
		if (count>0) {
			isox/=count; isoy/=count;
		}
		final double isolength = Math.sqrt( isox*isox + isoy*isoy );

		// contribution of each pixels in the region
		double wsum = 0;
		for (int[] pixel: region) {
			final int xk = pixel[0];
			final int yk = pixel[1];

			// propagation vector
			final int px = x-xk;
			final int py = y-yk;
			final double plength = Math.sqrt( px*px + py*py );

			// Weight of the propagation:

			// 1. isophote continuation: cos(isophote,propagation) = normalized dot product ( isophote , propagation )
			double wisophote = 0;
			if (isolength>0) {
				double cosangle = Math.abs(isox*px+isoy*py) / (isolength*plength);
				cosangle = Math.min(cosangle, 1.0);
				/*
				// linear weight version:
				double angle = Math.acos(cosangle);
				double alpha = 1-(angle/Math.PI);
				wisophote = Math.pow(alpha,this.preservation);
				*/
				wisophote = Math.pow(cosangle,this.preservation);
			}
			// 2. spread direction:
			// gradient length = O -> omnidirectionnal
			// gradient length = maxlength -> unidirectionnal
			final double unidir = Math.min(isolength/255,1);
			// 3. distance: distance to inpaint pixel
			final double wdist = 1.0 / (1.0 + plength*plength);
			// 4. probability: distance to image (unmasked pixel)
			final int distk = distmap[xk][yk];
			final double wproba = 1.0 / (1.0 + distk*distk);
			// global weight
			final double w = wdist * wproba * ( unidir*wisophote + (1-unidir)*1 );

            outputsm.getTupleDouble(pixel, sampleContainerD);
			for(int i=0;i<nbSample;i++){
				vinpaint[i] += w*sampleContainerD[i];
            }
			wsum+=w;
		}
		if (wsum<=0) return null;
		for(int i=0;i<nbSample;i++) {
			vinpaint[i]/=wsum;
			//if (vinpaint[i]<0)   vinpaint[i] = 0;
			//if (vinpaint[i]>255) vinpaint[i] = 255;
		}
		return vinpaint;
	}

	// 8 neightbours gradient
	private double[] gradient(int[][] array, int x, int y, int dist) {
		// Coordinates of 8 neighbours
		int px = x - 1;  // previous x
		int nx = x + 1;  // next x
		int py = y - 1;  // previous y
		int ny = y + 1;  // next y

		// limit to image dimension
		if (px < 0)	return null;
		if (nx >= this.width) return null;
		if (py < 0)	return null;
		if (ny >= this.height) return null;

		// availability of the 8 neighbours
		// (must be computed in previous loops)
		if (distmap[px][py]>=dist) return null;
		if (distmap[ x][py]>=dist) return null;
		if (distmap[nx][py]>=dist) return null;
		if (distmap[px][ y]>=dist) return null;
		if (distmap[nx][ y]>=dist) return null;
		if (distmap[px][ny]>=dist) return null;
		if (distmap[ x][ny]>=dist) return null;
		if (distmap[nx][ny]>=dist) return null;

		// Intensity of the 8 neighbours
		int Ipp = array[px][py];
		int Icp = array[ x][py];
		int Inp = array[nx][py];
		int Ipc = array[px][ y];
		int Inc = array[nx][ y];
		int Ipn = array[px][ny];
		int Icn = array[ x][ny];
		int Inn = array[nx][ny];

		// Local gradient
		double r2 = 2*Math.sqrt(2);
		double gradx = (Inc-Ipc)/2.0 + (Inn-Ipp)/r2 + (Inp-Ipn)/r2;
		double grady = (Icn-Icp)/2.0 + (Inn-Ipp)/r2 + (Ipn-Inp)/r2;
		double norme = Math.sqrt(gradx*gradx+grady*grady);

		return new double[] { gradx, grady, norme };
	}

	// get intensity of a pixel
    private int getIntensity(TupleBuffer sm, int[] coord) {
        final Color rgba = inputcm.toColor(sm.getTuple(coord, sampleContainer));
        //use grayscale value as intensity
        final float intensity = Colors.RGBtoGrayScalePAL(rgba.toRGBA());
        return (int) (intensity * 255);
	}


    private void adjustInOutSamples(double[] inSamples){
        switch(sampleType){
            case TYPE_1_BIT :
                for(int i=0;i<nbSample;i++) ((boolean[])sampleContainer)[i] = inSamples[i] != 0;
                break;
            case TYPE_2_BIT :
            case TYPE_4_BIT :
            case TYPE_BYTE :
                for(int i=0;i<nbSample;i++) ((byte[])sampleContainer)[i] = (byte)Maths.clamp(inSamples[i],-128,127);
                break;
            case TYPE_UBYTE :
                for(int i=0;i<nbSample;i++) ((short[])sampleContainer)[i] = (short)Maths.clamp(inSamples[i],0,255);
                break;
            case TYPE_SHORT :
                for(int i=0;i<nbSample;i++) ((short[])sampleContainer)[i] = (short)Maths.clamp(inSamples[i],Short.MIN_VALUE,Short.MAX_VALUE);
                break;
            case TYPE_USHORT :
                for(int i=0;i<nbSample;i++) ((int[])sampleContainer)[i] = (int)Maths.clamp(inSamples[i],0,65535);
                break;
            case TYPE_INT :
                for(int i=0;i<nbSample;i++) ((int[])sampleContainer)[i] = (int)inSamples[i];
                break;
            case TYPE_UINT :
                for(int i=0;i<nbSample;i++) ((long[])sampleContainer)[i] = (long)inSamples[i];
                break;
            case TYPE_LONG :
                for(int i=0;i<nbSample;i++) ((long[])sampleContainer)[i] = (long)inSamples[i];
                break;
            case TYPE_FLOAT :
                for(int i=0;i<nbSample;i++) ((float[])sampleContainer)[i] = (float)inSamples[i];
                break;
            case TYPE_DOUBLE :
                sampleContainer = inSamples;
                break;
            default: throw new UnimplementedException("Unusual data type.");
        }
    }

}
