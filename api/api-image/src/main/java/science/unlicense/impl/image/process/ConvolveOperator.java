
package science.unlicense.impl.image.process;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.process.AbstractAreaOperator;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_EXTENDER;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 * Convolution operator.
 * 
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class ConvolveOperator extends AbstractAreaOperator {

    public static final FieldType INPUT_CONV_MATRIX = new FieldTypeBuilder().id(new Chars("ConvolutionMatrix")).title(new Chars("Input convolution matrix")).valueClass(ConvolutionMatrix.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("convolve"), new Chars("Convolve"), Chars.EMPTY,ConvolveOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTENDER,INPUT_CONV_MATRIX},
                new FieldType[]{OUTPUT_IMAGE});
    
    private ConvolutionMatrix kernel;
    private double[][] matrix;
    private int height;
    private int width;

    public ConvolveOperator() {
        super(DESCRIPTOR);
    }

    protected int getTopPadding() {
        return kernel.getTopPadding();
    }

    protected int getBottomPadding() {
        return kernel.getBottomPadding();
    }

    protected int getLeftPadding() {
        return kernel.getLeftPadding();
    }

    protected int getRightPadding() {
        return kernel.getRightPadding();
    }

    protected void prepareInputs() {
        kernel = (ConvolutionMatrix) inputParameters.getFieldValue(INPUT_CONV_MATRIX.getId());
        matrix = kernel.getValuesCopy();
        height = matrix.length;
        width = matrix[0].length;
    }

    protected void evaluate(double[][][] workspace, double[] result) {
        double t;
        for(int k=0;k<result.length;k++){
            t=0;
            for (int kj = 0; kj < height; kj++){
                for (int ki = 0; ki < width; ki++) {
                    t += matrix[kj][ki] * workspace[kj][ki][k];
                }
            }
            result[k] = t;
        }
    }

}
