
package science.unlicense.impl.image.process.morphologic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_EXTENDER;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.image.process.Extrapolator;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;


/**
 * Morphological closure operator.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class CloseOperator extends AbstractTask {

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("close"), new Chars("Close"), Chars.EMPTY,CloseOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTENDER},
                new FieldType[]{OUTPUT_IMAGE});
    
    public CloseOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Extrapolator extender){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        param.setFieldValue(AbstractImageTaskDescriptor.INPUT_EXTENDER.getId(),extender);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(OUTPUT_IMAGE.getId());
    }

    public Document execute() {
        Image image = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Extrapolator extender = (Extrapolator) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_EXTENDER.getId());

        image = new DilateOperator().execute(image, null, extender);
        image = new ErodeOperator().execute(image, null, extender);

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),image);
        return outputParameters;
    }

}
