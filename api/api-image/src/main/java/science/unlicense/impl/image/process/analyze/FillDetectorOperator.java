
package science.unlicense.impl.image.process.analyze;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageModel;
import science.unlicense.api.task.AbstractTask;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.impl.image.MaskIterator;
import science.unlicense.impl.math.Statistics;

/**
 * This operator try to detect a possible paint in an area of an image.
 * 
 * Aiming to detect :
 * - plain value fill
 * - TODO gradient values fill
 * - TODO radial values fill
 * - TODO pattern repetition fill
 * 
 * @author Johann Sorel
 */
public class FillDetectorOperator extends AbstractTask {

    public static final FieldType INPUT_MASK = new FieldTypeBuilder(new Chars("Mask")).valueClass(Image.class).build();
    public static final FieldType INPUT_MODEL = new FieldTypeBuilder(new Chars("Model")).valueClass(Chars.class).defaultValue(new Chars("RawModel")).build();
    public static final FieldType OUTPUT_FILL = new FieldTypeBuilder(new Chars("Fill")).valueClass(Object.class).build();
    public static final FieldType OUTPUT_PROBABILITY = new FieldTypeBuilder(new Chars("Probability")).valueClass(Double.class).defaultValue(0.0).build();
    
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("contour"), new Chars("Contour"), Chars.EMPTY,ContourOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_MASK},
                new FieldType[]{OUTPUT_FILL,OUTPUT_PROBABILITY});
        
    public FillDetectorOperator() {
        super(DESCRIPTOR);
    }

    public Document execute() {
        final Image inputImage = (Image) inputParameters.getFieldValue(INPUT_IMAGE.getId());
        final Image inputMask = (Image) inputParameters.getFieldValue(INPUT_MASK.getId());
        final Chars model = (Chars) inputParameters.getFieldValue(INPUT_MODEL.getId());
        
        final ImageModel imgModel = (ImageModel) inputImage.getModels().getValue(model);
        TupleBuffer tupleBuffer = imgModel.asTupleBuffer(inputImage);
        TupleIterator tupleIte = tupleBuffer.createIterator(null);
        
        if(inputMask!=null){
            tupleIte = new MaskIterator(tupleIte, inputMask);
        }
    
        final Statistics[] stats = new Statistics[tupleBuffer.getSampleCount()];
        int i;
        for(double[] tuple=tupleIte.nextAsDouble(null);tuple!=null;tuple=tupleIte.nextAsDouble(tuple)){
            for(i=0;i<stats.length;i++){
                stats[i].add(tuple[i]);
            }
        }
        
        //average fill value
        final double[] fill = new double[stats.length];
        for(i=0;i<stats.length;i++){
            fill[i] = stats[i].getMean();
        }
        
        final Document params = new DefaultDocument(false,DESCRIPTOR.getOutputType());
        params.setFieldValue(OUTPUT_FILL.getId(),fill);
        return params;
    }
    
}
