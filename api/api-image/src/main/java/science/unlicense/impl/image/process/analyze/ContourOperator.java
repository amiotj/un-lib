
package science.unlicense.impl.image.process.analyze;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.primitive.DoubleSequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.MishandleException;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;

/**
 * This algorithm extract a contour geometry from given image.
 * 
 * This operator outputs one multipolygon per predicate.
 * 
 * @author Johann Sorel
 */
public class ContourOperator extends AbstractTask {

    public static final FieldType INPUT_PREDICATES = new FieldTypeBuilder(new Chars("Predicates")).title(new Chars("Predicates")).valueClass(Predicate[].class).build();
    public static final FieldType INPUT_BYCOLOR = new FieldTypeBuilder(new Chars("ByColor")).title(new Chars("By color")).valueClass(Boolean.class).defaultValue(Boolean.TRUE).build();
    public static final FieldType OUTPUT_GEOMETRIES = new FieldTypeBuilder(new Chars("Geometries")).title(new Chars("Geometries")).valueClass(MultiPolygon[].class).build();
    
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("contour"), new Chars("Contour"), Chars.EMPTY,ContourOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_PREDICATES,INPUT_BYCOLOR},
                new FieldType[]{OUTPUT_GEOMETRIES});
        
    public ContourOperator() {
        super(DESCRIPTOR);
    }

    public MultiPolygon execute(Image image, Predicate predicate, boolean byColor) {
        getInput().setFieldValue(INPUT_IMAGE.getId(), image);
        getInput().setFieldValue(INPUT_PREDICATES.getId(), new Predicate[]{predicate});
        getInput().setFieldValue(INPUT_BYCOLOR.getId(), byColor);
        final Document doc = execute();
        return ((MultiPolygon[]) doc.getFieldValue(OUTPUT_GEOMETRIES.getId()))[0];
    }
    
    public MultiPolygon[] execute(Image image, Predicate[] predicates, boolean byColor) {
        getInput().setFieldValue(INPUT_IMAGE.getId(), image);
        getInput().setFieldValue(INPUT_PREDICATES.getId(), predicates);
        getInput().setFieldValue(INPUT_BYCOLOR.getId(), byColor);
        final Document doc = execute();
        return (MultiPolygon[]) doc.getFieldValue(OUTPUT_GEOMETRIES.getId());
    }
    
    public Document execute() {
        final Image inputImage = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Predicate[] predicates = (Predicate[]) inputParameters.getFieldValue(INPUT_PREDICATES.getId());
        final boolean byColor = (Boolean) inputParameters.getFieldValue(INPUT_BYCOLOR.getId());

        final TupleBuffer tb;
        if(byColor){
            tb = inputImage.getColorModel().asTupleBuffer(inputImage);
        }else{
            tb = inputImage.getRawModel().asTupleBuffer(inputImage);
        }

        final int nbSample = tb.getSampleCount();
        final double[] sample = new double[nbSample];
        final long[] dimensions = inputImage.getExtent().toArray();
        
        final ContourBuilder builder[] = new ContourBuilder[predicates.length];
        for(int i=0;i<builder.length;i++){
            builder[i] = new ContourBuilder((int)dimensions[0], (int)dimensions[1]);
        }
        
        final int[] coord = new int[2];
        Object pixel;
        for(coord[1]=0;coord[1]<dimensions[1];coord[1]++){
            for(int i=0;i<builder.length;i++){
                builder[i].nextLine();
            }
            for(coord[0]=0;coord[0]<dimensions[0];coord[0]++){
                pixel = tb.getTupleDouble(coord, sample);
                
                for(int i=0;i<builder.length;i++){
                    builder[i].put(predicates[i].evaluate(pixel));
                }
            }
        }
        
        //get the result
        final MultiPolygon[] polygons = new MultiPolygon[predicates.length];
        for(int i=0;i<polygons.length;i++) polygons[i] = builder[i].build();
        outputParameters.setFieldValue(OUTPUT_GEOMETRIES.getId(),polygons);
        return outputParameters;
    }
    
    private static class ContourBuilder{
        
        private int x = 0;
        private int y = 0;
        private GeomRef[] previous;
        private GeomRef[] current;
        private final Sequence polygons = new ArraySequence();
        
        private ContourBuilder(int width, int height){
            previous = new GeomRef[width+1];
            current = new GeomRef[width+1];
        }
        
        private void nextLine(){
            
            final GeomRef[] temp = previous;
            previous = current;
            current = temp;
            Arrays.fill(current, null);
        }
        
        private void put(boolean state){
            
            final boolean top = y!=0 && previous[x]!=null; 
            final boolean left = x!=0 && current[x-1]!=null; 
            final boolean topleft = x!=0 && y!=0 && previous[x-1]!=null; 
            
            if (state) {
                if (topleft && top && left) {
                    // █ █
                    // 
                    // █ █
                    // do nothing
                    current[x] = current[x-1];
                } else if (topleft && top) {
                    // █ █
                    // ─┐
                    // o║█
                    current[x] = previous[x-1];
                    current[x].pushDown(x, y);
                } else if (topleft && left) {
                    // █│o
                    //  └═
                    // █ █
                    current[x] = previous[x-1];
                    current[x].pushRight(x, y);
                } else if (top && left) {
                    // o│█
                    // ─╝
                    // █ █
                    
                    //in this case, previous[x] and current[x] can be different
                    //geometries, we must merge them
                    current[x] = current[x-1];
                    current[x].merge(x, y, previous[x]);
                    current[x].geom.merge(x, y);
                } else if (topleft) {
                    // █│o
                    // ─┼═
                    // o║█
                    previous[x-1].geom.merge(x, y);
                    current[x] = new GeomRef(new TempGeometry(this));
                    current[x].newLoop(x, y);
                } else if (top) {
                    // o│█
                    //  │
                    // o║█
                    current[x] = previous[x];
                    current[x].pushDown(x, y);
                } else if (left) {
                    // o o
                    // ──═
                    // █ █
                    current[x] = current[x-1];
                    current[x].pushRight(x, y);
                } else {
                    // o o
                    //  ╔═
                    // o║█
                    current[x] = new GeomRef(new TempGeometry(this));
                    current[x].newLoop(x, y);
                }
            } else {
                if (topleft && top && left) {
                    // █ █
                    //  ╔═
                    // █║o
                    ensureSame(previous[x-1], previous[x], current[x-1]);                    
                    previous[x].newLoop(x, y);
                } else if (topleft && top) {
                    // █ █
                    // ──═
                    // o o
                    ensureSame(previous[x-1], previous[x]);
                    previous[x-1].pushRight(x, y);
                    
                } else if (topleft && left) {
                    // █│o
                    //  │
                    // █║o
                    ensureSame(previous[x-1], current[x-1]);
                    current[x-1].pushDown(x, y);
                } else if (top && left) {
                    // o│█
                    // ─┼═
                    // █║o
                    previous[x].pushRight(x, y);
                    current[x-1].pushDown(x, y);
                } else if (topleft) {
                    // █│o
                    // ═┘ 
                    // o o
                    previous[x-1].geom.merge(x, y);
                } else if (top) {
                    // o│█
                    //  └═
                    // o o
                    previous[x].pushRight(x, y);
                } else if (left) {
                    // o o
                    // ─┐
                    // █║o
                    current[x-1].pushDown(x, y);
                } else {
                    // o o
                    // 
                    // o o
                    // do nothing
                }
            }
            
            //prepare next iteration
            x++;
            if (x==previous.length-1) {
                //add a false last column
                put(false);
                x = 0;
                y++;
            }
        }
        
        private MultiPolygon build(){
            //add one false last line;
            nextLine();
            for(int i=0;i<current.length-1;i++) put(false);
            return new MultiPolygon(polygons);
        }
        
    }
    
    private static class GeomRef {
        
        private TempGeometry geom;

        private GeomRef(TempGeometry geom) {
            this.geom = geom;
        }
        
        private Loop newLoop(int x, int y) {
            return geom.newLoop(x,y);
        }
        
        private void pushRight(int x, int y) {
            geom.getEnd(x, y).pushRight();
        }
        
        private void pushDown(int x, int y) {
            geom.getEnd(x, y).pushDown();
        }
        
        private void merge(int x, int y, GeomRef other){
            if(geom == other.geom) return;
            geom.merge(x, y, other.geom);
            other.geom = this.geom;
        }
    }
    
    private static class TempGeometry {
        
        private final ContourBuilder builder;
        private final Sequence loops = new ArraySequence();

        public TempGeometry(ContourBuilder builder) {
            this.builder = builder;
        }
        
        Loop newLoop(int x, int y) {
            final Loop loop = new Loop(this,x,y);
            loops.add(loop);
            return loop;
        }
        
        Loop.End getEnd(int x, int y) {
            for (int i=0,n=loops.getSize();i<n;i++) {
                final Loop loop = (Loop) loops.get(i);
                if(loop.start.match(x, y)) return loop.start;
                if(loop.end.match(x, y)) return loop.end;
            }
            throw new MishandleException("Should not happen");
        }
        
        private void finish(){
            if (loops.getSize()>1) return;
            final Loop loop = (Loop) loops.get(0);
            final int size = loop.coords.getSize();
            if (loop.coords.read(0)==loop.coords.read(size-2) && loop.coords.read(1)==loop.coords.read(size-1)) {
                final Polyline outer = new Polyline(new DefaultTupleBuffer1D(loop.coords.toArrayDouble(), 2));
                loops.removeAll();
                builder.polygons.add(new Polygon(outer, null));
            }
        }
        
        private void merge(int x, int y, TempGeometry other){
            this.loops.addAll(other.loops);
            merge(x, y);
        }
        
        private void merge(int x, int y){
            if (loops.getSize()<2) return;
            Loop loop1 = null;
            Loop loop2 = null;
            int i=0;
            int n=loops.getSize();
            for (;i<n;i++) {
                final Loop loop = (Loop) loops.get(i);
                if(loop.start.match(x, y) || loop.end.match(x, y)) {
                    loop1 = loop;
                    break;
                } 
            }
            for (i++;i<n;i++) {
                final Loop loop = (Loop) loops.get(i);
                if(loop.start.match(x, y) || loop.end.match(x, y)) {
                    loop2 = loop;
                    break;
                } 
            }
            if (loop1==null || loop2==null) return;
            
            loops.remove(loop2);
            final double[] data = loop2.coords.toArrayDouble();
            if (loop1.end.match(loop2.start)) {
                loop1.coords.moveTo(loop1.coords.getSize());
                loop1.coords.put(data, 2, data.length-2);
            } else if (loop1.end.match(loop2.end)) {
                Arrays.reverse(data, 0, data.length, 2);
                loop1.coords.moveTo(loop1.coords.getSize());
                loop1.coords.put(data, 2, data.length-2);
            } else if (loop1.start.match(loop2.start)) {
                Arrays.reverse(data, 0, data.length, 2);
                loop1.coords.moveTo(0);
                loop1.coords.insert(data, 0, data.length-2);
            } else if (loop1.start.match(loop2.end)) {
                loop1.coords.moveTo(0);
                loop1.coords.insert(data, 0, data.length-2);
            } else {
                throw new MishandleException("Should not happen");
            }
            
            loop1.start.goingRight = null;
            loop1.end.goingRight = null;
            finish();
        }
        
    }
    
    private static class Loop {
        
        private final TempGeometry geom;
        private final DoubleSequence coords = new DoubleSequence();
        private final double[] buffer = new double[2];
        private final End start = new End() {
            int getPreviousIndex() { return 2; }
            int getIndex() { return 0; }
            int getNextIndex() { return 0; }
        };
        private final End end = new End() {
            int getPreviousIndex() { return coords.getSize()-4; }
            int getIndex() { return coords.getSize()-2; }
            int getNextIndex() { return coords.getSize(); }
        };

        public Loop(TempGeometry geom, int x, int y) {
            this.geom = geom;
            //add the 2 first segments
            coords.put(x);
            coords.put(y+1);
            coords.put(x);
            coords.put(y);
            coords.put(x+1);
            coords.put(y);
            start.goingRight = false;
            end.goingRight = true;
        }

        public String toString() {
            return new Polyline(new DefaultTupleBuffer1D(coords.toArrayDouble(), 2)).toString();
        }
        
        private abstract class End {
            Boolean goingRight=null;
            abstract int getPreviousIndex();
            abstract int getIndex();
            abstract int getNextIndex();
            private Loop getLoop() {
                return Loop.this;
            }
            
            void computeDirection(){
                if (goingRight!=null) return;
                final int pidx = getIndex();
                final int idx = getIndex();
                if (pidx<0 ||pidx>coords.getSize()) {
                   goingRight = null;
                } else {
                    goingRight = coords.read(idx) == coords.read(pidx)+1;
                }
            }
            
            void pushRight() {
                computeDirection();
                int index = getIndex();
                if (Boolean.TRUE.equals(goingRight)) {
                    //move coordinate
                    coords.moveTo(index);
                    coords.put(coords.read(index)+1);
                } else {
                    //add a new point
                    coords.read(index,buffer,0,2);
                    buffer[0]++;
                    coords.moveTo(getNextIndex());
                    coords.insert(buffer);
                }
                geom.finish();
                goingRight = true;
            }
            void pushDown() {
                computeDirection();
                int index = getIndex();
                if (Boolean.FALSE.equals(goingRight)) {
                    //move coordinate
                    coords.moveTo(index+1);
                    coords.put(coords.read(index+1)+1);
                } else {
                    //add a new point
                    coords.read(index,buffer,0,2);
                    buffer[1]++;
                    coords.moveTo(getNextIndex());
                    coords.insert(buffer);
                }
                geom.finish();
                goingRight = false;
            }
            boolean match(double x, double y) {
                coords.read(getIndex(),buffer,0,2);
                return buffer[0]==x && buffer[1]==y;
            }
            boolean match(End end) {
                final int index = end.getIndex();
                final double x = end.getLoop().coords.read(index);
                final double y = end.getLoop().coords.read(index+1);
                return match(x, y);
            }
        }
    }
    
    
    private static void ensureSame(GeomRef a, GeomRef b) throws InvalidArgumentException {
        if (a.geom != b.geom){
            throw new InvalidArgumentException("Objects are different");
        }
    }
    
    private static void ensureSame(GeomRef a, GeomRef b, GeomRef c) throws InvalidArgumentException {
        if (a.geom != b.geom || b.geom != c.geom){
            throw new InvalidArgumentException("Objects are different");
        }
    }
    
}
