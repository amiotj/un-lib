
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class YCbCr extends AbstractColorSpace{

    public static Chars NAME = new Chars("YCbCr");
    public static final ColorSpace INSTANCE = new YCbCr();

    public YCbCr() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("Cb"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("Cr"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }
    
    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] ycbcr) {
        ycbcr[0] = Maths.clamp(  0 + (int)(0.299*rgba[0])    + (int)(0.587*rgba[1])    + (int)(0.114*rgba[2]),    0f, 1f);
        ycbcr[1] = Maths.clamp(128 - (int)(0.168736*rgba[0]) - (int)(0.331264*rgba[1]) + (int)(0.5*rgba[2]),      0f, 1f);
        ycbcr[2] = Maths.clamp(128 + (int)(0.5*rgba[0])      - (int)(0.418688*rgba[1]) - (int)(0.081312*rgba[2]), 0f, 1f);
        return ycbcr;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] ycbcr, float[] rgba) {
        rgba[0] = Maths.clamp(ycbcr[0]                                 + (float)(1.402  *(ycbcr[2]-0.5)), 0f, 1f);
        rgba[1] = Maths.clamp(ycbcr[0] - (float)(0.34414*(ycbcr[1]-0.5)) - (float)(0.71414*(ycbcr[2]-0.5)), 0f, 1f);
        rgba[2] = Maths.clamp(ycbcr[0] + (float)(1.772  *(ycbcr[1]-0.5))                                , 0f, 1f);
        rgba[3] = 1f;
        return rgba;
    }

}
