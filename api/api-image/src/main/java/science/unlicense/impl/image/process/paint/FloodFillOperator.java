

package science.unlicense.impl.image.process.paint;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 *
 * @author Johann Sorel
 */
public class FloodFillOperator extends AbstractTask{

    public static final FieldType INPUT_POINT = new FieldTypeBuilder(new Chars("Point")).title(new Chars("Point")).valueClass(Tuple.class).build();
    public static final FieldType INPUT_PREDICATE = new FieldTypeBuilder(new Chars("Predicate")).title(new Chars("Predicate")).valueClass(Predicate.class).build();
    public static final FieldType INPUT_FILLSAMPLE = new FieldTypeBuilder(new Chars("FillSample")).title(new Chars("FillSample")).valueClass(Object.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("floodfill"), new Chars("FloodFill"), Chars.EMPTY, FloodFillOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_POINT,INPUT_PREDICATE,INPUT_FILLSAMPLE},
                new FieldType[]{OUTPUT_IMAGE});
    
    private final Sequence stack = new ArraySequence();
    private Predicate predicate;
    private TupleBuffer tb;
    private int width;
    private int height;
    private Object sample;
    private boolean topSet = false;
    private boolean bottomSet = false;
        
    public FloodFillOperator() {
        super(DESCRIPTOR);
    }
    
    public Image execute(Image base, Tuple point, Predicate predicate, Object fill){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),base);
        param.setFieldValue(INPUT_POINT.getId(),point);
        param.setFieldValue(INPUT_PREDICATE.getId(),predicate);
        param.setFieldValue(INPUT_FILLSAMPLE.getId(),fill);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }
     
    public Document execute() {
        final Image base = (Image) inputParameters.getFieldValue(INPUT_IMAGE.getId());
        final Tuple point = (Tuple) inputParameters.getFieldValue(INPUT_POINT.getId());
        predicate = (Predicate) inputParameters.getFieldValue(INPUT_PREDICATE.getId());
        final Object fillSample = (Object) inputParameters.getFieldValue(INPUT_FILLSAMPLE.getId());
               
        tb = base.getRawModel().asTupleBuffer(base);
        width = (int) base.getExtent().getL(0);
        height = (int) base.getExtent().getL(1);
        sample = tb.createTupleStore();
        
        int x = (int)point.getX();
        int y = (int)point.getY();
        
        stack.add(new int[]{x,y});
        
        while(!stack.isEmpty()){
            final int index = stack.getSize()-1;
            final int[] coord = (int[]) stack.get(index);
            stack.remove(index);
            
            bottomSet=false;
            topSet=false;
                        
            final int startX = coord[0];
            
            //search left
            for(;coord[0]>=0;coord[0]--){
                if(predicate.evaluate(tb.getTuple(coord, sample))){
                    tb.setTuple(coord, fillSample);
                    checkTopBottom(coord);
                }else{
                    break;
                }
            }            
            //search right
            for(coord[0]=startX+1;coord[0]<width;coord[0]++){
                if(predicate.evaluate(tb.getTuple(coord, sample))){
                    tb.setTuple(coord, fillSample);
                    checkTopBottom(coord);
                }else{
                    break;
                }
            }
        }
        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),base);
        return outputParameters;
    }
    
    private void checkTopBottom(int[] coord){
        //check under
        if(coord[1]>0){
            coord[1]--;
            if(predicate.evaluate(tb.getTuple(coord, sample))){
                if(!bottomSet){
                    stack.add(new int[]{coord[0],coord[1]});
                    bottomSet = true;
                }
            }else{
                bottomSet=false;
            }
            coord[1]++;
        }
        
        //check above
        if(coord[1]<height-1){
            coord[1]++;
            if(predicate.evaluate(tb.getTuple(coord, sample))){
                if(!topSet){
                    stack.add(new int[]{coord[0],coord[1]});
                    topSet = true;
                }
            }else{
                topSet=false;
            }
            coord[1]--;
        }
    }
    
}
