

package science.unlicense.impl.image.process.paint;

import science.unlicense.api.character.Chars;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.image.color.PixelIterator;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.impl.math.Affine2;

/**
 * Paint using a blend operator an image over another.
 * 
 * @author Johann Sorel
 */
public class BlendOperator extends AbstractTask{

    public static final FieldType INPUT_BLENDIMAGE = new FieldTypeBuilder(new Chars("BlendImage")).title(new Chars("Image to paint over")).valueClass(Image.class).build();
    public static final FieldType INPUT_BLENDALPHA = new FieldTypeBuilder(new Chars("AlphaBlending")).title(new Chars("AlphaBlending")).valueClass(AlphaBlending.class).build();
    public static final FieldType INPUT_TRANSFORM = new FieldTypeBuilder(new Chars("Transform")).title(new Chars("Transform")).valueClass(Affine2.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("blend"), new Chars("Blend"), Chars.EMPTY, BlendOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_BLENDIMAGE,INPUT_BLENDALPHA,INPUT_TRANSFORM},
                new FieldType[]{OUTPUT_IMAGE});
    
    public BlendOperator() {
        super(DESCRIPTOR);
    }
    
    public Image execute(Image base, Image over, AlphaBlending blend, Affine2 trs){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),base);
        param.setFieldValue(INPUT_BLENDIMAGE.getId(),over);
        param.setFieldValue(INPUT_BLENDALPHA.getId(),blend);
        param.setFieldValue(INPUT_TRANSFORM.getId(),trs);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }
    
    public Document execute() {
        final Image base = (Image) inputParameters.getFieldValue(INPUT_IMAGE.getId());
        final Image image = (Image) inputParameters.getFieldValue(INPUT_BLENDIMAGE.getId());
        final AlphaBlending alphaBlending = (AlphaBlending) inputParameters.getFieldValue(INPUT_BLENDALPHA.getId());
        final Affine2 transform = (Affine2) inputParameters.getFieldValue(INPUT_TRANSFORM.getId());

        final int width = (int) base.getExtent().getL(0);
        final int height = (int) base.getExtent().getL(1);

        final PixelBuffer baseTuples = base.getColorModel().asTupleBuffer(base);
        final PixelBuffer sourceTuples = image.getColorModel().asTupleBuffer(image);

        final int[] targetcoord = new int[image.getExtent().getDimension()];

        final double[] coord = new double[2];
        final PixelIterator iterator = sourceTuples.createIterator(null);
        for(Integer srcColor = iterator.nextAsARGB(); srcColor!=null; srcColor=iterator.nextAsARGB()){

            //convert in image coordinate
            final int[] gridCoord = iterator.getCoordinate();
            coord[0] = gridCoord[0];
            coord[1] = gridCoord[1];
            transform.transform(coord,coord);
            targetcoord[0] = (int)coord[0];
            targetcoord[1] = (int)coord[1];
            if(targetcoord[0]<0 || targetcoord[0]>width || targetcoord[1]<0 || targetcoord[1]>height){
                //out of image
                continue;
            }

            //blend color
            int dstColor = baseTuples.getARGB(targetcoord);
            final int resColor = alphaBlending.blend(srcColor,dstColor);

            //set value
            baseTuples.setARGB(targetcoord, resColor);
        }

        
        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),base);
        return outputParameters;
    }
    
}
