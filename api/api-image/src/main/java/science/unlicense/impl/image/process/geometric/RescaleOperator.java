

package science.unlicense.impl.image.process.geometric;

import science.unlicense.api.character.Chars;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class RescaleOperator extends AbstractTask {

    public static final FieldType INPUT_TARGET_EXTENT = new FieldTypeBuilder(new Chars("Extent")).title(new Chars("Input target image extent")).valueClass(Extent.class).build();
    
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("rescale"), new Chars("rescale"), Chars.EMPTY, RescaleOperator.class,
                new FieldType[]{INPUT_IMAGE, INPUT_TARGET_EXTENT},
                new FieldType[]{OUTPUT_IMAGE});
    
    public RescaleOperator() {
        super(DESCRIPTOR);
    }

    public Document execute() {
        final Image inputImage = (Image) inputParameters
                .getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Extent outputExtent = (Extent) inputParameters
                .getFieldValue(INPUT_TARGET_EXTENT.getId());
                
        final Image outputImage = execute(inputImage, outputExtent);

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }
    
    public static Image execute(Image inputImage, Extent outputExtent) {
        
        //TODO support Nd transforms
        final Affine2 trs = new Affine2();
        trs.setToIdentity();
        
        final double scalex = inputImage.getExtent().getL(0) / outputExtent.get(0);
        final double scaley = inputImage.getExtent().getL(1) / outputExtent.get(1);
        trs.set(0, 0, scalex);
        trs.set(1, 1, scaley);
        
        return TransformOperator.execute(inputImage, trs, outputExtent);

    }
        
}
