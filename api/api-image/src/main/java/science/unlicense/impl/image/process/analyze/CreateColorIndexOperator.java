
package science.unlicense.impl.image.process.analyze;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.ColorIndex;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.impl.geometry.algorithm.MedianCut;

/**
 * Operator which extract a color palette for an image.
 * This operator use the median cut algorithm.
 * 
 * @author Johann Sorel
 */
public class CreateColorIndexOperator extends AbstractTask {

    public static final FieldType INPUT_NBCOLOR = new FieldTypeBuilder(new Chars("NbColor")).title(new Chars("Number of colors")).valueClass(Integer.class).build();
    public static final FieldType OUTPUT_PALETTE = new FieldTypeBuilder(new Chars("Palette")).title(new Chars("Palette")).valueClass(ColorIndex.class).build();
    
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("extractpalette"), new Chars("ExtractPalette"), Chars.EMPTY,CreateColorIndexOperator.class,
                new FieldType[]{AbstractImageTaskDescriptor.INPUT_IMAGE,INPUT_NBCOLOR},
                new FieldType[]{OUTPUT_PALETTE});
        
    public CreateColorIndexOperator() {
        super(DESCRIPTOR);
    }

    public ColorIndex executeNow(Image image, int nbColor){
        inputParameters.setFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        inputParameters.setFieldValue(INPUT_NBCOLOR.getId(),nbColor);
        return (ColorIndex) execute().getFieldValue(OUTPUT_PALETTE.getId());
    }
    
    public Document execute() {
        final Image inputImage = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final int nbColor = (Integer) inputParameters.getFieldValue(INPUT_NBCOLOR.getId());

        final ColorModel colorModel = inputImage.getColorModel();
        final TupleBuffer tb = colorModel.asTupleBuffer(inputImage);
        final Sequence bboxes = MedianCut.medianCut(tb, nbColor);
        
        final Color[] palette = new Color[bboxes.getSize()];
        float[] rgba = new float[4];
        for(int i=0;i<palette.length;i++){
            final BBox bbox = (BBox) bboxes.get(i);
            colorModel.getColorSpace().toRGBA(bbox.getMiddle().toArrayFloat(), rgba);
            palette[i] = new Color(rgba);
        }
        
        outputParameters.setFieldValue(OUTPUT_PALETTE.getId(),new ColorIndex(palette));
        return outputParameters;
    }
    
}
