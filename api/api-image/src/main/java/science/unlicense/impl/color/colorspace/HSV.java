
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.math.Maths;

/**
 * Hue,Saturation,Value components.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HUE : 0-360
 * SATURATION : 0-1
 * VALUE : 0-1
 * 
 * @author Johann Sorel
 */
public class HSV extends AbstractColorSpace{

    public static Chars NAME = new Chars("HSV");
    public static final HSV INSTANCE = new HSV();

    private HSV() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("h"), null, Primitive.TYPE_INT,   0, +360),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("v"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }

    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] hsv) {
        final float red = rgba[0];
        final float green = rgba[1];
        final float blue = rgba[2];

        final float max = Maths.max(Maths.max(blue, red), green);
        final float min = Maths.min(Maths.min(blue, red), green);

        final float c = max - min;
        
        //compute H
        float h1 = 0;
        if (c == 0) {
            h1 = 0;
        } else if (max == red) {
            h1 = ((green - blue ) / c) % 6;
        } else if (max == green) {
            h1 = ((blue  - red  ) / c) + 2;
        } else if (max == blue) {
            h1 = ((red   - green) / c) + 4;
        }
        hsv[0] = Maths.wrap((int)(60 * h1), 0, 360);

        //compute L
        final float v = max;
        hsv[2] = v;
        
        //compute V
        float s;
        if (c == 0) {
            s = 0;
        } else {
            s = c / v;
        }
        hsv[1] = s;

        return hsv;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] hsv, float[] rgba) {

        final float h = hsv[0];
        final float s = hsv[1];
        final float v = hsv[2];

        final float c = v * s;
        final float h1 = h / 60;
        final float x = c * (1 - Math.abs((h1 % 2) - 1));        
        hue2rgb(h, h1, c, x, rgba);
        final float m = v - c;
        rgba[0] += m;
        rgba[1] += m;
        rgba[2] += m;
        rgba[3] = 1;
        return rgba;
    }

    static void hue2rgb(float h, float h1, float c, float x, float[] rgba) {
        //if(h==0)     { rgba[0] = 0; rgba[1]=0; rgba[2]=0; }
        if(h1<1)     { rgba[0] = c; rgba[1]=x; rgba[2]=0; }
        else if(h1<2){ rgba[0] = x; rgba[1]=c; rgba[2]=0; }
        else if(h1<3){ rgba[0] = 0; rgba[1]=c; rgba[2]=x; }
        else if(h1<4){ rgba[0] = 0; rgba[1]=x; rgba[2]=c; }
        else if(h1<5){ rgba[0] = x; rgba[1]=0; rgba[2]=c; }
        else         { rgba[0] = c; rgba[1]=0; rgba[2]=x; }
    }

}