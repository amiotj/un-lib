
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;

/**
 * Hue,Saturation,Intensity components.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HUE : 0-360
 * SATURATION : 0-1
 * INTENSITY : 0-1
 * 
 * TODO
 * 
 * @author Johann Sorel
 */
public class HSI extends AbstractColorSpace{

    public static Chars NAME = new Chars("HSI");
    public static final HSI INSTANCE = new HSI();

    private HSI() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("h"), null, Primitive.TYPE_INT,   0, +360),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("i"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }

    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] hsl) {
        //TODO
        return hsl;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] hsl, float[] rgba) {
        //TODO
        return rgba;
    }

}