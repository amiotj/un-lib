
package science.unlicense.impl.image.process.analyze;

import science.unlicense.api.character.Chars;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.AbstractPointOperator;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.impl.image.process.ConvolutionMatrix;

/**
 * Binary threshold of an image.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class ThresholdOperator extends AbstractPointOperator {

    public static final FieldType INPUT_THRESHOLD = new FieldTypeBuilder(new Chars("threshold")).title(new Chars("Input threshold")).valueClass(ConvolutionMatrix.class).build();

    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("threshold"), new Chars("Threshold"), Chars.EMPTY, ThresholdOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_THRESHOLD},
                new FieldType[]{OUTPUT_IMAGE});
    
    private double limit;

    public ThresholdOperator() {
        super(DESCRIPTOR);
    }

    protected void prepareInputs() {
        super.prepareInputs();
        limit = (Double) inputParameters.getFieldValue(INPUT_THRESHOLD.getId());
    }

    protected double evaluate(double value) {
        return (value<limit) ? 0 : 1;
    }

}
