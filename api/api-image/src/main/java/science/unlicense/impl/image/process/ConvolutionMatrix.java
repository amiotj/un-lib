
package science.unlicense.impl.image.process;

import science.unlicense.impl.math.DefaultMatrix;

/**
 * Convolution matrix, also called kernel is used in various image
 * processing operations.
 *
 * details at : http://en.wikipedia.org/wiki/Kernel_(image_processing)
 *
 * @author Johann Sorel
 * @author Florent Humbert
 */
public class ConvolutionMatrix extends DefaultMatrix {

    private int originX;
    private int originY;

    public ConvolutionMatrix(int originX, int originY, int nbrow, int nbcol) {
        super(nbrow, nbcol);
        this.originX = originX;
        this.originY = originY;
    }

    public ConvolutionMatrix(int originX, int originY, double[][] values) {
        super(values);
        this.originX = originX;
        this.originY = originY;
    }

    /**
     * Get convolution matrix origin on X.
     */
    public int getOriginX() {
        return originX;
    }

    /**
     * Set convolution matrix origin on X.
     */
    public void setOriginX(int originX) {
        this.originX = originX;
    }

    /**
     * Get convolution matrix origin on Y.
     */
    public int getOriginY() {
        return originY;
    }

    /**
     * Set convolution matrix origin on Y.
     */
    public void setOriginY(int originY) {
        this.originY = originY;
    }

    /**
     * Get matrix top padding.
     * Number of rows above the convolution center.
     */
    public int getTopPadding() {
        return originY;
    }

    /**
     * Get matrix bottom padding.
     * Number of rows under the convolution center.
     */
    public int getBottomPadding() {
        return nbRow - originY - 1;
    }

    /**
     * Get matrix left padding.
     * Number of columns on the left of the convolution center.
     */
    public int getLeftPadding() {
        return originX;
    }

    /**
     * Get matrix right padding.
     * Number of columns on the right of the convolution center.
     */
    public int getRightPadding() {
        return nbCol - originX - 1;
    }

}
