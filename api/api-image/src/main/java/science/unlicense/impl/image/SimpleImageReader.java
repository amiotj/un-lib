

package science.unlicense.impl.image;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.model.tree.DefaultTypedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.TypedNode;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.image.AbstractImageReader;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageReadParameters;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGESET;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION_EXTEND;
import static science.unlicense.api.image.ImageSetMetadata.MD_IMAGE_DIMENSION_ID;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.image.sample.PlanarRawModel2D;

/**
 *
 * @author Johann Sorel
 */
public class SimpleImageReader extends AbstractImageReader {

    private final int sampleType;
    private final NumberEncoding encoding;
    private final int[] dimensionSize;
    private final int nbSample;
    private final boolean interlaced;

    private DefaultTypedNode mdImage;


    public SimpleImageReader(int sampleType, NumberEncoding encoding, int nbSample, int[] dimensionSize, boolean interlaced) {
        this.sampleType = sampleType;
        this.encoding = encoding;
        this.nbSample = nbSample;
        this.dimensionSize = dimensionSize;
        this.interlaced = interlaced;
    }


    protected Dictionary readMetadatas(BacktrackInputStream stream) throws IOException {
        mdImage = new DefaultTypedNode(MD_IMAGE.getType());
        for(int i=0;i<dimensionSize.length;i++){
            final TypedNode tn = new DefaultTypedNode(MD_IMAGE_DIMENSION.getType(),new Node[]{
                new DefaultTypedNode(MD_IMAGE_DIMENSION_ID.getType(),"d"+i),
                new DefaultTypedNode(MD_IMAGE_DIMENSION_EXTEND.getType(),dimensionSize[i])});
            mdImage.getChildren().add(tn);
        }

        final TypedNode mdImageSet = new DefaultTypedNode(MD_IMAGESET,new Node[]{mdImage});
        final Dictionary metas = new HashDictionary();
        metas.add(mdImageSet.getType().getId(), mdImageSet);
        return metas;
    }

    protected Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException {
        readMetadatas(stream);

        if(dimensionSize.length!=2){
            throw new IOException("dimension size different from 2 not supported yet.");
        }

        final DataInputStream ds = new DataInputStream(stream,encoding);

        final int bitPerSample = Images.getBitsPerSample(sampleType);
        if(bitPerSample<8){
            throw new IOException("Bit per sample under 8 not supported yet.");
        }

        final Buffer bank = parameters.getBufferFactory().createByte((bitPerSample/8)*nbSample*dimensionSize[0]*dimensionSize[1]);
        ds.readFully(bank);
        final RawModel sm;
        if(interlaced){
            sm = new InterleavedRawModel(sampleType, nbSample);
        }else{
            sm = new PlanarRawModel2D(sampleType, nbSample);
        }

        final ColorModel cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);

        return new DefaultImage(bank, new Extent.Long(dimensionSize[0], dimensionSize[1]), sm, cm);
    }

}
