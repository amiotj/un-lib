
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.math.Maths;

/**
 * Doc ;
 * http://en.wikipedia.org/wiki/TSL_color_space
 * 
 * @author Johann Sorel
 */
public class TSL extends AbstractColorSpace{

    public static Chars NAME = new Chars("TSL");
    public static final ColorSpace INSTANCE = new TSL();
    
    public TSL() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("t"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("l"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }
    
    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] argb, float[] tsl) {
        final double sum = argb[1]+argb[2]+argb[3];
        final double r = argb[1] / sum;
        final double g = argb[2] / sum;
        final double r1 = r - 1.0/3.0;
        final double g1 = g - 1.0/3.0;
        
        if(g1>0){
            tsl[0] = (float) ((1.0/2*Maths.PI) * Math.atan(r1/g1) + 1.0/4.0);
        }else if(g1<0){
            tsl[0] = (float) ((1.0/2*Maths.PI) * Math.atan(r1/g1) + 3.0/4.0);
        }else{
            tsl[0] = 0.0f;
        }
        
        tsl[1] = (float)Math.sqrt(9.0/5.0 * (r1*r1 + g1*g1));
        tsl[2] = (float)(0.299*argb[1] + 0.587*argb[2] + 0.114*argb[3]);
        return tsl;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] tsl, float[] argb) {
        final double x = -Math.cos(2.0*Maths.PI*tsl[0]);
        double g;
        if(tsl[0]==0){
            g=0;
        }else{
            g = Math.sqrt(5.0/(9.0*(x*x+1))) * tsl[1];
            if(tsl[0]>1.0/2.0) g = -g;
        }
        final double r;
        if(tsl[0]==0){
            r = Math.sqrt(5)/3.0 * tsl[1];
        }else{
            r = x*g +1.0/3.0;
        }
        final double k = 1.0 / (0.185*r+0.473*g+0.114);
        
        argb[0] = 1.0f;
        argb[1] = (float)(k*r);
        argb[2] = (float)(k*g);
        argb[3] = (float)(k*(1-r-g));
        return argb;
    }

}
