

package science.unlicense.impl.image.process.geometric;

import science.unlicense.api.character.Chars;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 *
 * @author Johann Sorel
 */
public class TransformOperator extends AbstractTask {

    public static final FieldType INPUT_TRANSFORM = new FieldTypeBuilder(new Chars("Transformation")).title(new Chars("Input target to source transform")).valueClass(Transform.class).build();
    
    public static final FieldType INPUT_TARGET_EXTENT = new FieldTypeBuilder(new Chars("Extent")).title(new Chars("Input target image extent")).valueClass(Extent.class).build();
    
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("transform"), new Chars("transform"), Chars.EMPTY,TransformOperator.class,
                new FieldType[]{INPUT_IMAGE, INPUT_TRANSFORM, INPUT_TARGET_EXTENT},
                new FieldType[]{OUTPUT_IMAGE});
    
    public TransformOperator() {
        super(DESCRIPTOR);
    }

    public Document execute() {
        final Image inputImage = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final Transform transform = (Transform) inputParameters.getFieldValue(INPUT_TRANSFORM.getId());
        final Extent outputExtent = (Extent) inputParameters.getFieldValue(INPUT_TARGET_EXTENT.getId());
        
        final Image outputImage = execute(inputImage, transform, outputExtent);

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }
    
    public static Image execute(Image inputImage, Transform transform, Extent outputExtent){
        
        final TupleBuffer inputSM = inputImage.getRawModel().asTupleBuffer(inputImage);
        final Image outputImage = Images.create(inputImage, outputExtent);
        final TupleBuffer outputSM = outputImage.getRawModel().asTupleBuffer(outputImage);
        
        //TODO limited to 2D for now
        final Object sampleStore = outputSM.createTupleStore();
        final int width = (int) outputImage.getExtent().getL(0);
        final int height = (int) outputImage.getExtent().getL(1);
        final double[] coordOut = new double[2];
        final double[] coordIn = new double[2];
        final int[] iCoord = new int[2];
        for(coordOut[1]=0;coordOut[1]<height;coordOut[1]++){
            for(coordOut[0]=0;coordOut[0]<width;coordOut[0]++){
                transform.transform(coordOut, coordIn);
                iCoord[0]=(int)coordIn[0]; iCoord[1]=(int)coordIn[1];
                inputSM.getTuple(iCoord, sampleStore);
                iCoord[0]=(int)coordOut[0]; iCoord[1]=(int)coordOut[1];
                outputSM.setTuple(iCoord, sampleStore);
            }
        }
        
        return outputImage;
    }
    
}
