
package science.unlicense.impl.image.process.morphologic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractAreaOperator;
import science.unlicense.impl.image.process.ConvolutionMatrix;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_EXTENDER;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.image.process.Extrapolator;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 * Morphological dilation operator.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class DilateOperator extends AbstractAreaOperator {

    public static final FieldType INPUT_CONV_MATRIX = new FieldTypeBuilder(new Chars("ConvolutionMatrix")).title(new Chars("Input convolution matrix")).valueClass(ConvolutionMatrix.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("convolve"), new Chars("Convolve"), Chars.EMPTY, DilateOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTENDER,INPUT_CONV_MATRIX},
                new FieldType[]{OUTPUT_IMAGE});
    
    private static final ConvolutionMatrix DEFAULT_KERNEL = new ConvolutionMatrix(1, 1,
            new double[][]{
                {0,1,0},
                {1,1,1},
                {0,1,0},
            });

	private ConvolutionMatrix kernel;
    private double[][] matrix;
    private int height;
    private int width;
    private int originx;
    private int originy;
    private double[] buffer;

    public DilateOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, ConvolutionMatrix kernel, Extrapolator extender){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),image);
        param.setFieldValue(INPUT_EXTENDER.getId(),extender);
        param.setFieldValue(INPUT_CONV_MATRIX.getId(),kernel);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(OUTPUT_IMAGE.getId());
    }

    protected void prepareInputs() {
        kernel = (ConvolutionMatrix) inputParameters.getFieldValue(INPUT_CONV_MATRIX.getId());
        if(kernel==null){
            //default matrix
            kernel = DEFAULT_KERNEL;
        }
        matrix = kernel.getValuesCopy();
        height = matrix.length;
        width = matrix[0].length;
        buffer = new double[height * width];
        originx = kernel.getOriginX();
        originy = kernel.getOriginY();
    }

    protected int getTopPadding() {
        return kernel.getTopPadding();
    }

    protected int getBottomPadding() {
        return kernel.getBottomPadding();
    }

    protected int getLeftPadding() {
        return kernel.getLeftPadding();
    }

    protected int getRightPadding() {
        return kernel.getRightPadding();
    }

    protected void evaluate(double[][][] workspace, double[] result) {

        for (int canal = 0; canal < result.length; canal++) {

            for (int kj = 0; kj < height; kj++) {
                for (int ki = 0; ki < width; ki++) {
                    buffer[width * kj + ki] =
                            -workspace[originy][originx][canal]
                            + matrix[kj][ki] * workspace[kj][ki][canal];
                }
            }

            result[canal] = Maths.max(buffer) + workspace[originy][originx][canal];
        }
    }

}
