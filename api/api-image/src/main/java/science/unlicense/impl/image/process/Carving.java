package science.unlicense.impl.image.process;

import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;

/**
 * Reduce the height of an image, using the minimum energy path method.
 *
 * (cf. "Content Aware Image Resizing" by Avidan, S. & Shamir, A.)
 *
 * @author Xavier Philippeau
 * @author Johann Sorel (UN project adaptation)
 */
public class Carving {

    // image current dimension
    private int height = 0, width = 0;
    // rgb image
    private int[][] rgbimage = null;
    // energy of each pixel
    private int[][] energy = null;
    // all possible (horizontal) path
    private Path[] hpath = null;

    // Path class
    private class Path {
        // total energy

        int energy = 0;
        // direction of path (-1,0,+1) for each pixel
        int[] direction = null;
        // min, max relative offsets
        int lowestoffset = 0, highestoffset = 0;
    }

    /**
     * Constructor
     *
     * @param bimg the image to resize
     */
    public Carving(Image bimg) {
        // initialize arrays

        this.width = (int) bimg.getExtent().getL(0);
        this.height = (int) bimg.getExtent().getL(1);
        this.rgbimage = new int[width][height];
        this.energy = new int[width][height];
        this.hpath = new Path[height];

        // convert BufferedImage in a 2d array
        final PixelBuffer cm = bimg.getColorModel().asTupleBuffer(bimg);
        int[] coords = new int[2];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                coords[0] = x;
                coords[1] = y;
                this.rgbimage[x][y] = cm.getARGB(coords);
            }
        }
    }

    /**
     * @return the current image
     */
    public Image getImage() {
        final Image bimg = Images.create(new Extent.Long(width, height),Images.IMAGE_TYPE_RGBA);
        final PixelBuffer cm = bimg.getColorModel().asTupleBuffer(bimg);
        int[] coords = new int[2];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                coords[0] = x;
                coords[1] = y;
                cm.setARGB(coords, this.rgbimage[x][y]);
            }
        }
        return bimg;
    }

    /**
     * compute energy array
     *
     * @param ymin y range start
     * @param ymax y range end
     */
    private void computeEnergy(int ymin, int ymax) {
        for (int y = ymin; y < ymax; y++) {
            for (int x = 0; x < width; x++) {
                energy[x][y] = gradient(x, y);
            }
        }
    }

    /**
     * 8 neighbours gradient
     *
     * @param x x coord
     * @param y y coord
     * @return gradient norme
     */
    private int gradient(int x, int y) {
        // Coordinates of 8 neighbours
        int px = x - 1;  // previous x
        int nx = x + 1;  // next x
        int py = y - 1;  // previous y
        int ny = y + 1;  // next y

        // limit to image dimension (spheric)
        if (px < 0) {
            px = this.width - 1;
        }
        if (nx >= this.width) {
            nx = 0;
        }
        if (py < 0) {
            py = this.height - 1;
        }
        if (ny >= this.height) {
            ny = 0;
        }

        // Intensity of the 8 neighbours
        int Ipp = getLuminance(this.rgbimage[px][py]);
        int Icp = getLuminance(this.rgbimage[ x][py]);
        int Inp = getLuminance(this.rgbimage[nx][py]);
        int Ipc = getLuminance(this.rgbimage[px][ y]);
        int Inc = getLuminance(this.rgbimage[nx][ y]);
        int Ipn = getLuminance(this.rgbimage[px][ny]);
        int Icn = getLuminance(this.rgbimage[ x][ny]);
        int Inn = getLuminance(this.rgbimage[nx][ny]);

        // Local gradient (sobel)
        int gradx = (Inc - Ipc) * 2 + (Inp - Ipp) + (Inn - Ipn);
        int grady = (Icn - Icp) * 2 + (Ipn - Ipp) + (Inn - Inp);
        int norme = (int) Math.sqrt(gradx * gradx + grady * grady) / 4;

        return norme;
    }

    /**
     * return luminance of a rgb value
     *
     * @param rgb value in ARGB format
     * @return luminance
     */
    private int getLuminance(int rgb) {
        int r = (rgb >> 16) & 0xFF;
        int g = (rgb >> 8) & 0xFF;
        int b = rgb & 0xFF;
        int gray = (299 * r + 587 * g + 114 * b) / 1000;
        return gray;
    }

    /**
     * compute the horizontal path starting from left at y position = ystart
     *
     * @param ystart starting y position
     */
    private void computeHorizontalPath(int ystart) {
        // contruct a new path
        Path path = new Path();
        path.direction = new int[width];

        // add/replace in the global list
        this.hpath[ystart] = path;

        // go from letf to right
        int miny = Integer.MAX_VALUE, maxy = 0;
        int y = ystart;
        for (int x = 0; x < width - 1; x++) {

            // update min/max absolute value
            if (y < miny) {
                miny = y;
            }
            if (y > maxy) {
                maxy = y;
            }

            // update total energy of the path
            int v = this.energy[x][y];
            path.energy += v;

            // the 3 next possible positions for the current path
            int vk_up = Integer.MAX_VALUE;
            if (y > 0) {
                vk_up = this.energy[x + 1][y - 1];
            }
            int vk_center = Integer.MAX_VALUE;
            vk_center = this.energy[x + 1][y];
            int vk_down = Integer.MAX_VALUE;
            if (y < (height - 1)) {
                vk_down = this.energy[x + 1][y + 1];
            }

            // find the lowest energy for the 3 positions
            int min = Math.min(vk_up, Math.min(vk_center, vk_down));

            // follow the lowest energy direction
            int dir = 0;
            if (min == vk_up) {
                dir = -1;
            }
            if (min == vk_down) {
                dir = +1;
            }
            if (min == vk_center) {
                dir = 0;
            }
            y += dir;

            // update path
            path.direction[x] = dir;
        }
        path.lowestoffset = ystart - miny;
        path.highestoffset = maxy - ystart;
    }

    /**
     * @return the path index with minimum energy
     */
    private int findBestHorizontalPath() {
        int bestpathnumber = 0;
        int bestpathenergy = Integer.MAX_VALUE;
        for (int ystart = 0; ystart < height; ystart++) {
            if (this.hpath[ystart].energy < bestpathenergy) {
                bestpathenergy = this.hpath[ystart].energy;
                bestpathnumber = ystart;
                if (bestpathenergy == 0) {
                    break;
                }
            }
        }
        return bestpathnumber;
    }

    /**
     * Reduce the image height
     *
     * @param newheight new height of the image
     */
    public void reduceHeight(int newheight) {

        // compute the whole energy
        computeEnergy(0, this.height);

        // compute all path
        for (int y = 0; y < height; y++) {
            computeHorizontalPath(y);
        }

        // reduce image, line by line
        int hcount = this.height - newheight;
        for (int hloop = 0; hloop < hcount; hloop++) {

            // find best path
            int pathid = findBestHorizontalPath();

            // construct absolute path
            int[] bestpath = new int[hpath[pathid].direction.length];
            int yabs = pathid;
            for (int x = 0; x < hpath[pathid].direction.length; x++) {
                bestpath[x] = yabs;
                yabs += hpath[pathid].direction[x];
            }

            // construct the new image
            for (int x = 0; x < hpath[pathid].direction.length; x++) {

                int y = bestpath[x];

                // move up all elements of arrays below the path 
                int length = this.height - y - 1;
                System.arraycopy(this.energy[x], y + 1, this.energy[x], y, length);
                System.arraycopy(this.rgbimage[x], y + 1, this.rgbimage[x], y, length);

                // zeroing unused bottom part of arrays (not usefull)
                this.energy[x][this.height - 1] = 0;
            }
            // highlevel/downlevel watermark of changed region 
            int ymin_changed = pathid - hpath[pathid].lowestoffset;
            int ymax_changed = pathid + hpath[pathid].highestoffset;

            // resize hpath array
            int length = this.height - pathid - 1;
            System.arraycopy(this.hpath, pathid + 1, this.hpath, pathid, length);
            this.hpath[this.height - 1] = null;

            // resize the image
            this.height--;

            // optim: recompute only the energy for the changed region 
            int region_ymin = Math.max(ymin_changed - 1, 0);
            int region_ymax = Math.min(ymax_changed + 1, this.height);
            computeEnergy(region_ymin, region_ymax);

            // optim: recompute only the path that go through the changed region
            for (int y = 0; y < height; y++) {
                if ((y - this.hpath[y].lowestoffset) > ymax_changed) {
                    continue;
                }
                if ((y + this.hpath[y].highestoffset) < ymin_changed) {
                    continue;
                }
                computeHorizontalPath(y);
            }

        }
    }

}
