
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class CieLab extends AbstractColorSpace{

    public static Chars NAME = new Chars("CIE-LAB");

    public CieLab() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("L*"), null, Primitive.TYPE_INT,    0, +100),
            new ColorSpaceComponent(new Chars("a*"), null, Primitive.TYPE_INT, -300, +299),
            new ColorSpaceComponent(new Chars("b*"), null, Primitive.TYPE_INT, -300, +299)
        });
    }

    public float[] toSpace(float[] argb, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

    public float[] toRGBA(float[] values, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

}
