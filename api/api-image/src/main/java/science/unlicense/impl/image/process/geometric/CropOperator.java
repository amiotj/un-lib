

package science.unlicense.impl.image.process.geometric;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 *
 * @author Johann Sorel
 */
public class CropOperator extends AbstractTask {

    public static final FieldType INPUT_BBOX = new FieldTypeBuilder(new Chars("BBox")).title(new Chars("Input bbox to crop")).valueClass(BBox.class).build();
        
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("crop"), new Chars("crop"), Chars.EMPTY,CropOperator.class,
                new FieldType[]{INPUT_IMAGE, INPUT_BBOX},
                new FieldType[]{OUTPUT_IMAGE});
    
    public CropOperator() {
        super(DESCRIPTOR);
    }

    public Document execute() {
        final Image inputImage = (Image) inputParameters
                .getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        final BBox bbox = (BBox) inputParameters
                .getFieldValue(INPUT_BBOX.getId());
        
        final Image outputImage = execute(inputImage, bbox);

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }
    
    public static Image execute(Image inputImage, BBox bbox){
        
        final Extent ext = bbox.getExtent();
        
        final TupleBuffer inputTuples = inputImage.getRawModel().asTupleBuffer(inputImage);
        final Image outputImage = Images.create(inputImage, ext);
        final TupleBuffer outTuples = outputImage.getRawModel().asTupleBuffer(outputImage);
        
        TupleIterator ite = inputTuples.createIterator(bbox);
        for(Object samples=ite.next(); samples!=null; samples=ite.next() ){
            outTuples.setTuple(ite.getCoordinate(), samples);
        }
        
        return outputImage;
    }
    
}
