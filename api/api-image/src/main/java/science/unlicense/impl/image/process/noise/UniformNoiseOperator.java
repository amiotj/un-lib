
package science.unlicense.impl.image.process.noise;

import java.util.Random;
import science.unlicense.api.character.Chars;
import science.unlicense.api.image.process.AbstractPointOperator;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 * Add uniform noise to image.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public class UniformNoiseOperator extends AbstractPointOperator {

    public static final FieldType INPUT_DISTANCE = new FieldTypeBuilder(new Chars("Distance")).valueClass(Integer.class).build();

    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("convolve"), new Chars("Convolve"), Chars.EMPTY, UniformNoiseOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_DISTANCE},
                new FieldType[]{OUTPUT_IMAGE});
    
    private final Random randomizer = new Random();
    private int distance;
    private int distance2p1;

    public UniformNoiseOperator() {
        super(DESCRIPTOR);
    }

    protected void prepareInputs() {
        distance = (Integer) inputParameters.getFieldValue(INPUT_DISTANCE.getId());
        distance2p1  = distance*2 +1 ;
    }

    protected double evaluate(double value) {
        final int rand = randomizer.nextInt(distance2p1)  - distance;
        return value + rand;
    }

}
