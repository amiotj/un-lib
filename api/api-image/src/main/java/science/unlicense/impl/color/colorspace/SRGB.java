
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class SRGB extends AbstractColorSpace{

    public static Chars NAME = new Chars("sRGB");
    public static final ColorSpace INSTANCE = new SRGB();

    private SRGB() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("r"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("g"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("b"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }

    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] values, float[] rgba) {
        rgba[0] = values[0];
        rgba[1] = values[1];
        rgba[2] = values[2];
        rgba[3] = 1f;
        return rgba;
    }

}
