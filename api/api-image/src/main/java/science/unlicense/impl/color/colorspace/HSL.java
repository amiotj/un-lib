
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.math.Maths;

/**
 * Hue,Saturation,Lighting components.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HUE : 0-360
 * SATURATION : 0-1
 * LIGHTNING : 0-1
 * 
 * @author Johann Sorel
 */
public class HSL extends AbstractColorSpace{

    public static Chars NAME = new Chars("HSL");
    public static final HSL INSTANCE = new HSL();

    private HSL() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("h"), null, Primitive.TYPE_INT,   0, +360),
            new ColorSpaceComponent(new Chars("s"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("l"), null, Primitive.TYPE_FLOAT, 0, +1),
        });
    }

    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] hsl) {
        final float red = rgba[0];
        final float green = rgba[1];
        final float blue = rgba[2];

        final float max = Maths.max(Maths.max(blue, red), green);
        final float min = Maths.min(Maths.min(blue, red), green);

        final float c = max - min;
        
        //compute H
        float h1 = 0;
        if (c == 0) {
            h1 = 0;
        } else if (max == red) {
            h1 = ((green - blue ) / c) % 6;
        } else if (max == green) {
            h1 = ((blue  - red  ) / c) + 2;
        } else if (max == blue) {
            h1 = ((red   - green) / c) + 4;
        }
        hsl[0] = Maths.wrap((int)(60 * h1), 0, 360);

        //compute L
        final float l = 0.5f * (max + min);
        hsl[2] = l;
        
        //compute S
        float s;
        if (c == 0) {
            s = 0;
        } else {
            s = c / (1f - Math.abs(2*l-1f) );
        }
        hsl[1] = s;

        return hsl;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] hsl, float[] rgba) {

        final float h = hsl[0];
        final float s = hsl[1];
        final float l = hsl[2];

        final float c = (1- Math.abs(2*l-1)) * s;
        final float h1 = h / 60;
        final float x = c * (1 - Math.abs((h1 % 2) - 1));        
        HSV.hue2rgb(h, h1, c, x, rgba);
        final float m = l - c/2;
        rgba[0] += m;
        rgba[1] += m;
        rgba[2] += m;
        rgba[3] = 1;
        return rgba;
    }

}