
package science.unlicense.impl.image.process.geometric;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;


/**
 * Rotate 270° clockwise operator.
 *
 * @author Johann Sorel
 */
public class Rotate270Operator extends AbstractTask {

   public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
           new Chars("rotate-270"), new Chars("rotate-270"), Chars.EMPTY, Rotate270Operator.class,
                new FieldType[]{INPUT_IMAGE},
                new FieldType[]{OUTPUT_IMAGE});
    
    public Rotate270Operator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }

    public Document execute() {
        final Image image = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());

        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final TupleBuffer tb = image.getRawModel().asTupleBuffer(image);
        final Object storage = tb.createTupleStore();

        final Image result = Images.create(image, new Extent.Long(height, width));
        final TupleBuffer rsm = result.getRawModel().asTupleBuffer(result);
        
        final int[] coord1 = new int[2];
        final int[] coord2 = new int[2];
        for(coord1[1]=0;coord1[1]<height;coord1[1]++){
            for(coord1[0]=0;coord1[0]<width;coord1[0]++){
                coord2[0] = coord1[1];
                coord2[1] = width-coord1[0]-1;

                tb.getTuple(coord1, storage);
                rsm.setTuple(coord2, storage);
            }
        }

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),result);
        return outputParameters;
    }

}
