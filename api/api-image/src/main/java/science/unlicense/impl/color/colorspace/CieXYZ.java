
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class CieXYZ extends AbstractColorSpace{

    public static Chars NAME = new Chars("CIE-XYZ");

    public CieXYZ() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("X"), null, Primitive.TYPE_FLOAT, 0, 1),
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.TYPE_FLOAT, 0, 1),
            new ColorSpaceComponent(new Chars("Z"), null, Primitive.TYPE_FLOAT, 0, 1)
        });
    }

    public float[] toSpace(float[] argb, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

    public float[] toRGBA(float[] values, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

}
