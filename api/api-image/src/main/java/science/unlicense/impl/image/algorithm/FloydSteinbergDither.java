package science.unlicense.impl.image.algorithm;

/**
 * 
 * Origin :
 * http://en.literateprograms.org/index.php?title=Special:DownloadCode/Floyd-Steinberg_dithering_(Java)&oldid=12476
 * 
 * @author Spoon! (en.literateprograms.org)
 * @author Johann Sorel (adapted to unlicense-lib)
 */
public class FloydSteinbergDither{
    
    public static class RGBTriple {
        public final byte[] channels;
        public RGBTriple() { channels = new byte[3]; }
        public RGBTriple(int R, int G, int B)
        { channels = new byte[]{(byte)R, (byte)G, (byte)B}; }
    }

    private static byte plus_truncate_uchar(byte a, int b) {
        if ((a & 0xff) + b < 0)
            return 0;
        else if ((a & 0xff) + b > 255)
            return (byte)255;
        else
            return (byte)(a + b);
    }

    private static byte findNearestColor(RGBTriple color, RGBTriple[] palette) {
        int minDistanceSquared = 255*255 + 255*255 + 255*255 + 1;
        byte bestIndex = 0;
        for (byte i = 0; i < palette.length; i++) {
            int Rdiff = (color.channels[0] & 0xff) - (palette[i].channels[0] & 0xff);
            int Gdiff = (color.channels[1] & 0xff) - (palette[i].channels[1] & 0xff);
            int Bdiff = (color.channels[2] & 0xff) - (palette[i].channels[2] & 0xff);
            int distanceSquared = Rdiff*Rdiff + Gdiff*Gdiff + Bdiff*Bdiff;
            if (distanceSquared < minDistanceSquared) {
                minDistanceSquared = distanceSquared;
                bestIndex = i;
            }
        }
        return bestIndex;
    }

    public static byte[][] floydSteinbergDither(RGBTriple[][] image, RGBTriple[] palette) {
        
        byte[][] result = new byte[image.length][image[0].length];

        for (int y = 0; y < image.length; y++) {
	    for (int x = 0; x < image[y].length; x++) {
                final RGBTriple currentPixel = image[y][x];
                byte index = findNearestColor(currentPixel, palette);
                result[y][x] = index;

                for (int i = 0; i < 3; i++){
                    int error = (currentPixel.channels[i] & 0xff) - (palette[index].channels[i] & 0xff);
                    if (x + 1 < image[0].length) {
                        image[y+0][x+1].channels[i] = plus_truncate_uchar(image[y+0][x+1].channels[i], (error*7) >> 4);
                    }
                    if (y + 1 < image.length) {
                        if (x - 1 > 0) {
                            image[y+1][x-1].channels[i] = plus_truncate_uchar(image[y+1][x-1].channels[i], (error*3) >> 4);
                        }
                        image[y+1][x+0].channels[i] = plus_truncate_uchar(image[y+1][x+0].channels[i], (error*5) >> 4);
                        if (x + 1 < image[0].length) {
                            image[y+1][x+1].channels[i] = plus_truncate_uchar(image[y+1][x+1].channels[i], (error*1) >> 4);
                        }
                    }
                }
            }
	}
        return result;
    }

}