
package science.unlicense.impl.image;

import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.Image;

/**
 * Iterator with skips values based on a mask image.
 * 
 * @author Johann Sorel
 */
public class MaskIterator implements TupleIterator{

    private final TupleIterator ite;
    private final TupleBuffer maskBuffer;
    private final boolean[] maskTuple;

    public MaskIterator(TupleIterator ite, Image mask) {
        this.ite = ite;
        maskBuffer = mask.getRawModel().asTupleBuffer(mask);
        maskTuple = new boolean[maskBuffer.getSampleCount()];
    }
    
    @Override
    public int[] getCoordinate() {
        return ite.getCoordinate();
    }

    @Override
    public Object next() {
        Object v;
        for(v=ite.next();v!=null;v=ite.next()){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return v;
    }

    @Override
    public boolean[] nextAsBoolean(boolean[] buffer) {
        for(buffer=ite.nextAsBoolean(buffer);buffer!=null;buffer=ite.nextAsBoolean(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public byte[] nextAsByte(byte[] buffer) {
        for(buffer=ite.nextAsByte(buffer);buffer!=null;buffer=ite.nextAsByte(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public int[] nextAsUByte(int[] buffer) {
        for(buffer=ite.nextAsUByte(buffer);buffer!=null;buffer=ite.nextAsUByte(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public short[] nextAsShort(short[] buffer) {
        for(buffer=ite.nextAsShort(buffer);buffer!=null;buffer=ite.nextAsShort(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public int[] nextAsUShort(int[] buffer) {
        for(buffer=ite.nextAsUShort(buffer);buffer!=null;buffer=ite.nextAsUShort(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public int[] nextAsInt(int[] buffer) {
        for(buffer=ite.nextAsInt(buffer);buffer!=null;buffer=ite.nextAsInt(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public long[] nextAsUInt(long[] buffer) {
        for(buffer=ite.nextAsUInt(buffer);buffer!=null;buffer=ite.nextAsUInt(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public long[] nextAsLong(long[] buffer) {
        for(buffer=ite.nextAsLong(buffer);buffer!=null;buffer=ite.nextAsLong(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public float[] nextAsFloat(float[] buffer) {
        for(buffer=ite.nextAsFloat(buffer);buffer!=null;buffer=ite.nextAsFloat(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }

    @Override
    public double[] nextAsDouble(double[] buffer) {
        for(buffer=ite.nextAsDouble(buffer);buffer!=null;buffer=ite.nextAsDouble(buffer)){
            if(maskBuffer.getTupleBoolean(ite.getCoordinate(), maskTuple)[0]) break;
        }
        return buffer;
    }
    
}
