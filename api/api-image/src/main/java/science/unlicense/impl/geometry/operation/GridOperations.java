

package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.AbstractBinaryOperation;
import science.unlicense.impl.geometry.operation.AbstractBinaryOperationExecutor;
import science.unlicense.impl.geometry.operation.Intersects;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s3d.Grid;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.api.math.Tuple;

/**
 * Geometry operations for grid.
 * 
 * @author Johann Sorel
 */
public class GridOperations {
    
    private GridOperations(){}
    
    public static double[] toGridCoord(Grid grid, Tuple position){
        return grid.getGeomToGrid().transform(position.getValues(),null);
    }
    
    public static int[] toInts(double[] array){
        final int[] coord = new int[array.length];
        for(int i=0;i<coord.length;i++){
            coord[i] = (int)array[i];
        }
        return coord;
    }
        
    public static final AbstractBinaryOperationExecutor INTERSECTS_POINT =
            new AbstractBinaryOperationExecutor(Intersects.class, Point.class, Grid.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point)first;
            final Grid b = (Grid)second;
            final TupleBuffer tb = b.getImage().getRawModel().asTupleBuffer(b.getImage());
            final double[] coord = toGridCoord(b, a.getCoordinate());
            //check if point is outside grid
            for(int i=0;i<coord.length;i++){
                if(coord[i]<0 || coord[i]>=tb.getExtent().get(i)) return false;
            }
            
            //check if cell is filled at this coordinate
            final boolean[] sample = tb.getTupleBoolean(toInts(coord), null);
            return sample[0];
        }
    };
    
    public static final AbstractBinaryOperationExecutor INTERSECTS_SPHERE =
            new AbstractBinaryOperationExecutor(Intersects.class, Sphere.class, Grid.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Sphere a = (Sphere)first;
            final Grid b = (Grid)second;
            final double radius = a.getRadius();
            final TupleBuffer tb = b.getImage().getRawModel().asTupleBuffer(b.getImage());
            final double[] coord = toGridCoord(b, a.getCenter());
            //check if point is outside grid
            for(int i=0;i<coord.length;i++){
                if(coord[i]+radius<0 || coord[i]-radius>=tb.getExtent().get(i)) return false;
            }
            
            //check all intersecting blocks
            final double[] lower = new double[coord.length];
            final double[] upper = new double[coord.length];
            for(int i=0;i<coord.length;i++){
                lower[i] = (int)(coord[i]-radius);
                upper[i] = (int)(coord[i]+radius);
            }
            
            final TupleIterator ite = tb.createIterator(new BBox(lower, upper));
            final BBox voxel = new BBox(lower.length);
            boolean[] sample = new boolean[1];
            while(ite.nextAsBoolean(sample)!=null){
                if(sample[0]){
                    //check intersection with sphere
                    final int[] pixelCoord = ite.getCoordinate();
                    for(int i=0;i<pixelCoord.length;i++){
                        voxel.setRange(i, pixelCoord[i], pixelCoord[i]+1);
                    }
                    if(voxel.intersects(a)) return true;
                }
            }
            
            return false;
        }
    };
    
}
