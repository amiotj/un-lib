package science.unlicense.impl.image.process;

import science.unlicense.api.exception.InvalidArgumentException;

/**
 * Usual convolution matrices.
 *
 * @author Florent Humbert
 */
public final class ConvolutionMatrices {

    private ConvolutionMatrices() {
    }
    public static final ConvolutionMatrix SOBEL_VERTICAL =
            new ConvolutionMatrix(1, 1, new double[][]{
        {-1, -2, -1},
        {0, 0, 0},
        {1, 2, 1}
    });
    public static final ConvolutionMatrix SOBEL_HORIZONTAL =
            new ConvolutionMatrix(1, 1, new double[][]{
        {-1, 0, 1},
        {-2, 0, 2},
        {-1, 0, 1}
    });
    public static final ConvolutionMatrix PREWITT_VERTICAL =
            new ConvolutionMatrix(1, 1, new double[][]{
        {-1, -1, -1},
        {0, 0, 0},
        {1, 1, 1}
    });
    public static final ConvolutionMatrix PREWITT_HORIZONTAL =
            new ConvolutionMatrix(1, 1, new double[][]{
        {-1, 0, 1},
        {-1, 0, 1},
        {-1, 0, 1}
    });
    public static final ConvolutionMatrix LAPLACE =
            new ConvolutionMatrix(1, 1, new double[][]{
        {0, 1, 0},
        {1, -4, 1},
        {0, 1, 0}
    });
    public static final ConvolutionMatrix EMBOSS =
            new ConvolutionMatrix(1, 1, new double[][]{
        {2,  0, 0},
        {0, -1, 0},
        {0,  0, -1}
    });
    public static final ConvolutionMatrix MDIF =
            new ConvolutionMatrix(2, 2, new double[][]{
        {0, 1, 0, -1, 0},
        {1, 2, 0, -2, -1},
        {1, 3, 0, -3, -1},
        {1, 2, 0, -2, -1},
        {0, 1, 0, -1, 0}
    });
    public static final ConvolutionMatrix MEDIUM_SMOOTH =
            new ConvolutionMatrix(1, 1, new double[][]{
        {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0},
        {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0},
        {1.0 / 9.0, 1.0 / 9.0, 1.0 / 9.0}
    });
    public static final ConvolutionMatrix WEAK_BLUR =
            new ConvolutionMatrix(1, 1, new double[][]{
        {0.0, 0.05, 0.0},
        {0.05, 0.8, 0.05},
        {0.0, 0.05, 0.0}
    });
    public static final ConvolutionMatrix BALANCE_BLUR =
            new ConvolutionMatrix(1, 1, new double[][]{
        {1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0},
        {2.0 / 16.0, 4.0 / 16.0, 2.0 / 16.0},
        {1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0}
    });

    /**
     * Create a gaussian convolution matrix.
     *
     * @param radius
     * @param sigma
     * @return ConvolutionMatrix
     */
    public static ConvolutionMatrix createGaussian(int radius, double sigma) {
        if (sigma < 0) {
            throw new InvalidArgumentException("KernelGaussian : sigma negative");
        }

        double gaussianKernelFactor = 0;
        final int width = 2 * radius + 1;
        final int height = 2 * radius + 1;
        final double[][] matrix = new double[height][width];

        for (int ky = -radius; ky <= radius; ky++) {
            for (int kx = -radius; kx <= radius; kx++) {
                final double e = Math.exp(-(kx * kx + ky * ky) / (2 * sigma * sigma));
                gaussianKernelFactor += e;
                matrix[ky + radius][kx + radius] = e;
            }
        }

        for (int ky = 0; ky < height; ky++) {
            for (int kx = 0; kx < width; kx++) {
                matrix[ky][kx] /= gaussianKernelFactor;
            }
        }

        return new ConvolutionMatrix(radius, radius, matrix);
    }

    /**
     * Create a MDIF convolution matrix.
     *
     * @param rayon
     * @param sigma
     * @return ConvolutionMatrix
     */
    public static ConvolutionMatrix getMDIF(int rayon, double sigma) {
        final ConvolutionMatrix gauss = createGaussian(rayon, sigma);
        double total = 0.0;

        for (int ky = -rayon; ky <= rayon; ky++) {
            for (int kx = -rayon; kx <= rayon; kx++) {
                double value = gauss.get(kx + rayon, ky + rayon);
                value *= (ky + kx);
                if (value > 0) {
                    total += value;
                }
                gauss.set(kx + rayon, ky + rayon, value);
            }
        }
        for (int ky = -rayon; ky <= rayon; ky++) {
            for (int kx = -rayon; kx <= rayon; kx++) {
                double value = gauss.get(kx + rayon, ky + rayon);
                value *= 1.0f / total;
                gauss.set(kx + rayon, ky + rayon, value);
            }
        }

        return gauss;
    }
}
