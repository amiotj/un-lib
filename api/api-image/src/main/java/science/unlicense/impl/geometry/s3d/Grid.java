

package science.unlicense.impl.geometry.s3d;

import science.unlicense.impl.geometry.s3d.Geometry3D;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.number.Primitive;
import science.unlicense.impl.geometry.AbstractGeometry;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Affine3;


/**
 * A grid is an N dimension image used to fill space.
 * Values in the image should be interpreted as boolean values.
 * A value of true means the voxel is filled and can be used as if it was a cube.
 * 
 * TODO uncomplete class, just a draft for physics engine
 * 
 * @author Johann Sorel
 */
public class Grid extends AbstractGeometry implements Geometry3D{
    
    private final BBox sceneBbox;
    private final Image image;
    private final Affine3 geomToGrid;
    private final Affine3 gridToGeom;

    public Grid(BBox bbox, Extent.Long imageSize) {
        this.sceneBbox = bbox;
        this.image = Images.createCustomBand(imageSize, 1, Primitive.TYPE_1_BIT);
        final double scaleX = bbox.getSpan(0)/(imageSize.get(0));
        final double scaleY = bbox.getSpan(1)/(imageSize.get(1));
        final double scaleZ = bbox.getSpan(2)/(imageSize.get(2));
        this.gridToGeom = new Affine3(
                scaleX, 0, 0, sceneBbox.getMin(0), 
                0, scaleY, 0, sceneBbox.getMin(1), 
                0, 0, scaleZ, sceneBbox.getMin(2));
        this.geomToGrid = gridToGeom.invert();
    }
    
    /**
     * 
     * @param image
     * @param geomToGrid transform at voxel corner
     */
    public Grid(Image image, Affine3 geomToGrid) {
        this.image = image;
        this.geomToGrid = geomToGrid;
        this.gridToGeom = geomToGrid.invert();
        //TODO
        this.sceneBbox = null;
    }

    public Image getImage() {
        return image;
    }

    public Affine3 getGeomToGrid() {
        return geomToGrid;
    }

    public Affine3 getGridToGeom() {
        return gridToGeom;
    }
    
    public int getDimension() {
        return image.getExtent().getDimension();
    }

    public BBox getBoundingBox() {
        return new BBox(sceneBbox);
    }

    public double getSurface() {
        throw new UnimplementedException("Not supported yet.");
    }

    public double getVolume() {
        throw new UnimplementedException("Not supported yet.");
    }

}
