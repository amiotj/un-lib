
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;

/**
 *
 * @author Johann Sorel
 */
public class CMY extends AbstractColorSpace{

    public static Chars NAME = new Chars("CMY");
    public static final ColorSpace INSTANCE = new CMY();

    public CMY() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("C"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("M"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }
    
    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] cmy) {
        cmy[0] = 1f-rgba[0];
        cmy[1] = 1f-rgba[1];
        cmy[2] = 1f-rgba[2];
        return cmy;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] cmy, float[] rgba) {
        rgba[0] = 1f-cmy[0];
        rgba[1] = 1f-cmy[1];
        rgba[2] = 1f-cmy[2];
        rgba[3] = 1f;
        return rgba;
    }

}
