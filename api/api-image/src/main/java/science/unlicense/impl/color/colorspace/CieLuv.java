
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class CieLuv extends AbstractColorSpace{

    public static Chars NAME = new Chars("CIE-LUV");

    public CieLuv() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("L*"), null, Primitive.TYPE_INT,    0, +100),
            new ColorSpaceComponent(new Chars("u*"), null, Primitive.TYPE_INT, -100, +100),
            new ColorSpaceComponent(new Chars("v*"), null, Primitive.TYPE_INT, -100, +100)
        });
    }

    public float[] toSpace(float[] argb, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

    public float[] toRGBA(float[] values, float[] buffer) {
        throw new UnimplementedException("TODO");
    }

}
