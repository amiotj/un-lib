
package science.unlicense.impl.image.process.distance;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.task.AbstractTask;

/**
 * Brute force implementation of distance map.
 * 
 * @author Johann Sorel
 */
public class DistanceOperator extends AbstractTask{
    
    public static final FieldType INPUT_MAXDISTANCE = new FieldTypeBuilder(new Chars("dist")).valueClass(Integer.class).defaultValue(1f).build();
    
    public static DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("distance"), new Chars("distance"), Chars.EMPTY, DistanceOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_MAXDISTANCE},
                new FieldType[]{OUTPUT_IMAGE});

    public DistanceOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, int maxDistance){
        final Document param = new DefaultDocument(false,descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),image);
        param.setFieldValue(INPUT_MAXDISTANCE.getId(),maxDistance);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
    }
    
    @Override
    public Document execute() {
        final Image img = (Image) inputParameters.getFieldValue(INPUT_IMAGE.getId());
        final int maxDist = (Integer) inputParameters.getFieldValue(INPUT_MAXDISTANCE.getId());
        
        final Extent.Long extent = img.getExtent();
        
        //create distance image
        final Image distanceMap = Images.createCustomBand(extent, 1, RawModel.TYPE_DOUBLE);
        final double[] data = (double[]) distanceMap.getDataBuffer().getBackEnd();
        Arrays.fill(data, maxDist);
        
        //TODO
        throw new RuntimeException("TODO");
        
    }
    
}
