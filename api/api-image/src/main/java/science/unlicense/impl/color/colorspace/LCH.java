
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import static science.unlicense.impl.color.colorspace.HSV.hue2rgb;

/**
 * Luma(Y601),Chroma,Hue components
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * LUMA : 0-1
 * CHROMA : 0-1
 * HUE : 0-360
 * 
 * @author Johann Sorel
 */
public class LCH extends AbstractColorSpace{

    public static Chars NAME = new Chars("LCH");
    public static final LCH INSTANCE = new LCH();

    private LCH() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("l"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("c"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("h"), null, Primitive.TYPE_INT,   0, +360)
        });
    }

    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] lch) {
        final float red = rgba[0];
        final float green = rgba[1];
        final float blue = rgba[2];
        //TODO
        return lch;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] lch, float[] rgba) {

        final float l = lch[0];
        final float c = lch[1];
        final float h = lch[2];

        final float h1 = h / 60;
        final float x = c * (1 - Math.abs((h1 % 2) - 1));        
        hue2rgb(h, h1, c, x, rgba);
        final float m =  l - (0.3f*rgba[0] + 0.59f*rgba[1] + 0.11f*rgba[2]);
        rgba[0] += m;
        rgba[1] += m;
        rgba[2] += m;
        rgba[3] = 1;
        return rgba;
    }

}