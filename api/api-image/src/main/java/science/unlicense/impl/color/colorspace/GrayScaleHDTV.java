
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;

/**
 * Grayscale using HDTV luma components.
 * http://en.wikipedia.org/wiki/Grayscale
 *
 * @author Johann Sorel
 */
public class GrayScaleHDTV extends AbstractColorSpace{

    public static Chars NAME = new Chars("Grayscale-HDTV");

    public GrayScaleHDTV() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("G"), null, Primitive.TYPE_FLOAT, 0, 1)
        });
    }

    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] argb, float[] buffer) {
        buffer[0] = argb[1] * 0.2126f + argb[2] * 0.7152f + argb[3] * 0.0722f;
        return buffer;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] values, float[] buffer) {
        buffer[0] = 1f;
        buffer[1] = values[0];
        buffer[2] = values[0];
        buffer[3] = values[0];
        return buffer;
    }

}
