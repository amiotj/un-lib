
package science.unlicense.impl.image.process.morphologic;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.process.AbstractAreaOperator;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_EXTENDER;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.INPUT_IMAGE;
import static science.unlicense.api.image.process.AbstractImageTaskDescriptor.OUTPUT_IMAGE;
import science.unlicense.api.image.process.DefaultImageTaskDescriptor;
import science.unlicense.api.image.process.Extrapolator;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;


/**
 * Oil paint operator.
 *
 * @author Johann Sorel
 */
public class OilOperator extends AbstractAreaOperator{

    public static final FieldType INPUT_RADIUS = new FieldTypeBuilder(new Chars("Radius")).title(new Chars("Input radius")).valueClass(Integer.class).defaultValue(5).build();
    public static final FieldType INPUT_NBLEVEL = new FieldTypeBuilder(new Chars("NbLevel")).title(new Chars("Input number of intensity levels")).valueClass(Integer.class).defaultValue(20).build();
    
    public static final DefaultImageTaskDescriptor DESCRIPTOR = new DefaultImageTaskDescriptor(
            new Chars("oil"), new Chars("Oil"), Chars.EMPTY, OilOperator.class,
                new FieldType[]{INPUT_IMAGE,INPUT_EXTENDER,INPUT_RADIUS,INPUT_NBLEVEL},
                new FieldType[]{OUTPUT_IMAGE});
    
    private int radius;
    private int nbLevel;
    private int size;
    private double[][] average;
    private int[] intensityCount;
    
    public OilOperator() {
        super(DESCRIPTOR);
    }

    public Image execute(Image image, Extrapolator extender, int radius, int nbLevel){
        final Document param = new DefaultDocument(descriptor.getInputType());
        param.setFieldValue(INPUT_IMAGE.getId(),image);
        param.setFieldValue(INPUT_EXTENDER.getId(),extender);
        param.setFieldValue(INPUT_RADIUS.getId(),radius);
        param.setFieldValue(INPUT_NBLEVEL.getId(),nbLevel);
        setInput(param);
        final Document result = execute();
        return(Image) result.getFieldValue(OUTPUT_IMAGE.getId());
    }

    protected void prepareInputs() {
        super.prepareInputs();
        radius = (Integer)inputParameters.getFieldValue(INPUT_RADIUS.getId());
        nbLevel = (Integer)inputParameters.getFieldValue(INPUT_NBLEVEL.getId());
        average = new double[nbLevel][nbSample];
        intensityCount = new int[nbLevel];
        size = 1 + radius + radius;
    }
    
    protected int getTopPadding() {
        return radius;
    }

    protected int getBottomPadding() {
        return radius;
    }

    protected int getLeftPadding() {
        return radius;
    }

    protected int getRightPadding() {
        return radius;
    }

    protected void evaluate(double[][][] workspace, double[] result) {
        Arrays.fill(intensityCount, 0);
        for(int i=0;i<average.length;i++){
            Arrays.fill(average[i], 0.0);
        }
        
        int maxIntensity = 0;
        int maxCount = -1;
        
        for (int kj = 0; kj < size; kj++){
            for (int ki = 0; ki < size; ki++) {
                final double avg = Maths.sum(workspace[kj][ki])/nbSample;
                final int curIntensity = (int)(avg*nbLevel)/256;
                intensityCount[curIntensity]++;
                if(intensityCount[curIntensity]> maxCount){
                    maxCount = intensityCount[curIntensity];
                    maxIntensity = curIntensity;
                }
                
                for(int k=0;k<nbSample;k++){
                    average[curIntensity][k] += workspace[kj][ki][k];
                }
            }
        }
        
        //calculate final color
        for(int i=0;i<nbSample;i++){
            result[i] = average[maxIntensity][i] / maxCount;
        }
        
    }

}
