
package science.unlicense.impl.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.colorspace.AbstractColorSpace;
import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.color.colorspace.ColorSpaceComponent;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class CMYK extends AbstractColorSpace{

    public static Chars NAME = new Chars("CMYK");
    public static final ColorSpace INSTANCE = new CMYK();

    public CMYK() {
        super(NAME, new ColorSpaceComponent[]{
            new ColorSpaceComponent(new Chars("C"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("M"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("Y"), null, Primitive.TYPE_FLOAT, 0, +1),
            new ColorSpaceComponent(new Chars("K"), null, Primitive.TYPE_FLOAT, 0, +1)
        });
    }
    
    /**
     * {@inheritDoc }
     */
    public float[] toSpace(float[] rgba, float[] cmyk) {
        cmyk[3] = 1f - Maths.max(Maths.max(rgba[0],rgba[1]), rgba[2]);
        final float ik = (1f-cmyk[3]);
        cmyk[0] = (1f-rgba[0]-cmyk[3]) / ik;
        cmyk[1] = (1f-rgba[1]-cmyk[3]) / ik;
        cmyk[2] = (1f-rgba[2]-cmyk[3]) / ik;
        return cmyk;
    }

    /**
     * {@inheritDoc }
     */
    public float[] toRGBA(float[] cmyk, float[] rgba) {
        final float ik = (1f-cmyk[3]);
        rgba[0] = (1f-cmyk[0]) * ik;
        rgba[1] = (1f-cmyk[1]) * ik;
        rgba[2] = (1f-cmyk[2]) * ik;
        rgba[3] = 1f;
        return rgba;
    }

}
