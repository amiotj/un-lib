
package science.unlicense.api.image;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.array.TableWriter;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.system.path.Paths;
import science.unlicense.system.util.ModuleSeeker;

/**
 * Convinient methods for image manipulation.
 *
 * @author Johann Sorel
 */
public final class Images {

    public static final int IMAGE_TYPE_RGB                  = 0;
    public static final int IMAGE_TYPE_RGBA                 = 1;
    public static final int IMAGE_TYPE_RGBA_PREMULTIPLIED   = 2;
    
    private Images(){}

    /**
     * Returns the number of bits used for the given sample type.
     *
     * @param sampleType one of ImageSampleModel.TYPE_X
     * @return number of bits persample
     */
    public static int getBitsPerSample(int sampleType){
             if(sampleType == RawModel.TYPE_1_BIT){ return 1; }
        else if(sampleType == RawModel.TYPE_2_BIT){ return 2; }
        else if(sampleType == RawModel.TYPE_4_BIT){ return 4; }
        else if(sampleType == RawModel.TYPE_BYTE){ return 8; }
        else if(sampleType == RawModel.TYPE_UBYTE){ return 8; }
        else if(sampleType == RawModel.TYPE_SHORT){ return 16; }
        else if(sampleType == RawModel.TYPE_USHORT){ return 16; }
        else if(sampleType == RawModel.TYPE_INT){ return 32; }
        else if(sampleType == RawModel.TYPE_UINT){ return 32; }
        else if(sampleType == RawModel.TYPE_LONG){ return 64; }
        else if(sampleType == RawModel.TYPE_FLOAT){ return 32; }
        else if(sampleType == RawModel.TYPE_DOUBLE){ return 64; }
        else{ throw new RuntimeException("can not match number of bit per sample for type "+sampleType); }
    }

    /**
     * Create a ND image with an arbitrary number of bands.
     *
     * @param extent image size, must be positive
     * @param nbSample number of bands/samples in the image
     * @param sampleType samples type.
     * @return Image, never null
     */
    public static Image createCustomBand(Extent.Long extent, int nbSample, int sampleType){
        return createCustomBand(extent, nbSample, sampleType, null);
    }
    
    /**
     * Create a ND image with an arbitrary number of bands.
     *
     * @param extent image size, must be positive
     * @param nbSample number of bands/samples in the image
     * @param sampleType samples type.
     * @param bufferFactory image buffer factory, can be null
     * @return Image, never null
     */
    public static Image createCustomBand(Extent.Long extent, int nbSample, int sampleType, BufferFactory bufferFactory){
        if(bufferFactory==null) bufferFactory = DefaultBufferFactory.INSTANCE;
        final RawModel rm = new InterleavedRawModel(sampleType, nbSample);
        final Buffer buffer = rm.createBuffer(extent,bufferFactory);
        final ColorModel cm = new DirectColorModel(rm, new int[]{0,0,0,-1}, false);
        return new DefaultImage(buffer, extent, rm, cm);
    }
    
    public static Image create(Extent.Long extent, int imageType){
        return create(extent,imageType,DefaultBufferFactory.INSTANCE);
    }
    
    public static Image create(Extent.Long extent, int imageType, BufferFactory factory){
        if(factory==null) factory = DefaultBufferFactory.INSTANCE;
        
        final RawModel rm;
        final ColorModel cm;
        switch(imageType){
            case IMAGE_TYPE_RGB : 
                rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 3);
                cm = new DirectColorModel(rm, new int[]{0,1,2,-1}, false);
                break;
            case IMAGE_TYPE_RGBA : 
                rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
                cm = new DirectColorModel(rm, new int[]{0,1,2,3}, false);
                break;
            case IMAGE_TYPE_RGBA_PREMULTIPLIED : 
                rm = new InterleavedRawModel(RawModel.TYPE_UBYTE, 4);
                cm = new DirectColorModel(rm, new int[]{0,1,2,3}, true);
                break;
            default: throw new InvalidArgumentException("Unknowned image type : "+imageType);
        }
        final Buffer buffer = rm.createBuffer(extent,factory);
        return new DefaultImage(buffer, extent, rm, cm);
    }
    
    /**
     * Copy an image.
     * @return Image
     */
    public static Image copy(Image image){
        final Extent.Long extent = image.getExtent().copy();
        final Buffer buffer = image.getDataBuffer().copy();
        return new DefaultImage(buffer, extent, image.getModels(), image.getMetadatas());
    }
    
    /**
     * Create a new image with the same properties as given image but with a different
     * size.
     * @param image
     * @param dimensions
     * @return Image
     */
    public static Image create(Image image, Extent dimensions){
        final long[] ext = new long[dimensions.getDimension()];
        for(int i=0;i<ext.length;i++) ext[i] = (int) dimensions.get(i);
        return create(image, ext);
    }
    /**
     * Create a new image with the same properties as given image but with a different
     * size.
     * @param image
     * @param dimensions
     * @return Image
     */
    public static Image create(Image image, long[] dimensions){
        final Extent.Long extent = new Extent.Long(dimensions);
        final RawModel rm = image.getRawModel();
        final Buffer buffer = rm.createBuffer(extent);
        return new DefaultImage(buffer, extent, image.getModels(), image.getMetadatas());
    }

    /**
     * Lists available image formats.
     * @return array of ImageFormat, never null but can be empty.
     */
    public static ImageFormat[] getFormats(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.api.image.ImageFormat"), 
                Predicates.instanceOf(ImageFormat.class));
        final ImageFormat[] formats = new ImageFormat[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }
    
    public static ImageFormat getFormat(Chars name) throws InvalidArgumentException{
        final ImageFormat[] formats = getFormats();
        for(int i=0;i<formats.length;i++){
            if(formats[i].getIdentifier().equals(name)){
                return formats[i];
            }
        }
        throw new InvalidArgumentException("Format "+name+" not found.");
    }

    public static ImageFormat getFormatForExtension(Chars ext) throws InvalidArgumentException{
        final ImageFormat[] formats = getFormats();
        for(int i=0;i<formats.length;i++){
            for(Chars e : formats[i].getExtensions()){
                if(e.equals(ext, true, true)){
                    return formats[i];
                }
            }
        }
        throw new InvalidArgumentException("Format "+ext+" not found.");
    }
    static BacktrackInputStream toInputStream(Object input, boolean[] mustClose) throws IOException{
        BacktrackInputStream stream;
        if(input instanceof ByteInputStream){
            stream = new BacktrackInputStream((ByteInputStream)input);
            //we did not create the stream, we do not close it.
            mustClose[0] = false;
            stream.mark();
            return stream;
        }else if(input instanceof Path){
            stream = new BacktrackInputStream(((Path)input).createInputStream());
            mustClose[0] = true;
            stream.mark();
            return stream;
        }else{
            throw new IOException("Unsupported input : "+input);
        }
    }

    static ByteOutputStream toOutputStream(Object output, boolean[] mustClose) throws IOException{
        ByteOutputStream stream;
        if(output instanceof ByteOutputStream){
            stream = (ByteOutputStream)output;
            //we did not create the stream, we do not close it.
            mustClose[0] = false;
            return stream;
        }else if(output instanceof Path){
            stream = ((Path)output).createOutputStream();
            mustClose[0] = true;
            return stream;
        }else{
            throw new IOException("Unsupported output : "+output);
        }
    }

    /**
     * Convinient method to obtain an image reader.
     * The method will loop on available formats until one can decode the input.
     *
     * @param input
     * @return ImageReader, never null
     * @throws IOException if not format could support the source.
     */
    public static ImageReader createReader(Object input) throws IOException {
        final ImageFormat[] formats = getFormats();
        if(formats.length == 0){
            throw new IOException("No image formats available.");
        }

        final Sequence candidates = new ArraySequence();
        
        final boolean[] mustclose = new boolean[1];
        final BacktrackInputStream stream = toInputStream(input, mustclose);
        stream.mark();
        for(int i=0;i<formats.length;i++){
            if(formats[i].supportReading()){
                boolean canDecode = false;
                try{
                    canDecode = formats[i].canDecode(stream);
                }catch(IOException ex){
                    //may happen
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }

                if(!canDecode && formats[i].getSignature().length==0 && input instanceof Path){
                    //this format has no fixed signature, test the input file directly
                    canDecode = formats[i].canDecode(input);
                }
                
                if(canDecode){
                    candidates.add(formats[i]);
                }
            }
            stream.rewind();
        }
        
        if(candidates.getSize()>1 && input instanceof Path){
            final Chars ext = Paths.getExtension(new Chars(((Path)input).getName()));
            //multiple choice, try to filter by file extension
            for(int i=candidates.getSize()-1;i>=0 && candidates.getSize()>1;i--){
                final ImageFormat imf = (ImageFormat) candidates.get(i);
                if(!Arrays.contains(imf.getExtensions(),ext)){
                    candidates.remove(i);
                }
            }
        }
        
        stream.close();
        
        if(!candidates.isEmpty()){
            final ImageFormat imf = (ImageFormat) candidates.get(0);
            //we have found a reader, use it
            final ImageReader reader = imf.createReader();
            reader.setInput(input);
            return reader;
        }
        

        throw new IOException("No reader can read given input : "+input);
    }

    /**
     * Convinient method to read an image source of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return Image, never null
     * @throws IOException if not format could support the source.
     */
    public static Image read(Object input) throws IOException {
        final ImageReader reader =  createReader(input);
        final Image image = reader.read(null);
//        final BacktrackInputStream bs = (BacktrackInputStream) reader.getInput();
//        bs.close();
        return image;
    }

    /**
     * Convenient method to write an image.
     * This method will search for the format with given name and trye to write
     * the image.
     *
     * @param image
     * @param format
     * @param output
     * @throws IOException
     */
    public static void write(Image image, Chars format, Object output) throws IOException {
        final ImageFormat[] formats = getFormats();
        if(formats.length == 0){
            throw new IOException("No image formats available.");
        }

        final boolean[] mustclose = new boolean[1];
        final ByteOutputStream stream = toOutputStream(output, mustclose);
        try{
            for(int i=0;i<formats.length;i++){
                if(formats[i].supportWriting() && formats[i].getIdentifier().equals(format, true, true)){
                    final ImageWriter writer = formats[i].createWriter();
                    final ImageWriteParameters params = writer.createParameters();
                    writer.setOutput(stream);
                    writer.write(image, params);
                    return;
                }
            }
        }finally{
            if(mustclose[0]){
                stream.close();
            }
        }

        throw new IOException("No writer available for given format.");
    }
    
    /**
     * Create a printable grid of the image data in bounding box.
     * Ensure the region isn't to large.
     * 
     * @param image 2D image only
     * @param ext printed area
     * @return 
     */
    public static Chars toChars(Image image, Rectangle ext){
        
        if(ext==null){
            ext = new Rectangle(0, 0, image.getExtent().get(0), image.getExtent().get(1));
        }
        
        final TableWriter writer = new TableWriter();
        
        final RawModel sm = image.getRawModel();
        final TupleBuffer tb = sm.asTupleBuffer(image);
        final Object buffer = tb.createTupleStore();
        for(int x=0,xn=(int) ext.getWidth();x<xn;x++){
            writer.newLine();
            for(int y=0,yn=(int) ext.getHeight();y<yn;y++){
                tb.getTuple(new int[]{x,y}, buffer);
                writer.appendCell(Arrays.toChars(buffer));
            }
        }
        
        return writer.toChars();
    }
    
}
