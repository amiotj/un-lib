
package science.unlicense.api.image.process;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.task.AbstractTaskDescriptor;
import science.unlicense.api.image.Image;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 * Description of an operator.
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageTaskDescriptor extends AbstractTaskDescriptor {
    
    public static final FieldType INPUT_IMAGE   = new FieldTypeBuilder().id(new Chars("Image")).title(new Chars("Input image")).valueClass(Image.class).build();
    public static final FieldType INPUT_IMAGE2  = new FieldTypeBuilder().id(new Chars("Image2")).title(new Chars("Input image 2")).valueClass(Image.class).build();
    public static final FieldType INPUT_EXTENDER= new FieldTypeBuilder().id(new Chars("Extender")).title(new Chars("Input border extender")).valueClass(Extrapolator.class).build();
    public static final FieldType OUTPUT_IMAGE  = new FieldTypeBuilder().id(new Chars("Image")).title(new Chars("Input image")).valueClass(Image.class).build();

    public AbstractImageTaskDescriptor(Chars id, CharArray name, CharArray description,
            DocumentType inputType, DocumentType outputType) {
        super(id, name, description, inputType, outputType);
    }
    
    public AbstractImageTaskDescriptor(Chars id, CharArray name, CharArray description,
            FieldType[] inputs, FieldType[] outputs) {
        super(id, name, description, 
                new DefaultDocumentType(INPUT,null,null,true,inputs,null),
                new DefaultDocumentType(OUTPUT,null,null,true,outputs,null));
    }

}
