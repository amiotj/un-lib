
package science.unlicense.api.image;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.impl.io.BacktrackInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.Metadata;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageReader extends AbstractReader implements ImageReader{

    protected Dictionary metadatas;

    public ImageReadParameters createParameters() {
        return new DefaultImageReadParameters();
    }

    public final Chars[] getMetadataNames() throws IOException {
        final BacktrackInputStream stream = getInputAsBacktrackStream();
        stream.rewind();
        if(metadatas == null){
            metadatas = readMetadatas(stream);
        }

        final Collection names = metadatas.getKeys();
        final Chars[] array = new Chars[names.getSize()];
        Collections.copy(names, array, 0);
        return array;
    }

    public final Metadata getMetadata(Chars name) throws IOException {
        final BacktrackInputStream stream = getInputAsBacktrackStream();
        stream.rewind();
        if(metadatas == null){
            metadatas = readMetadatas(stream);
        }

        final Metadata meta = (Metadata) metadatas.getValue(name);
        if(meta!=null){
            return meta;
        }else{
            throw new IOException("No metadata for name "+name+".");
        }
    }


    protected abstract Dictionary readMetadatas(BacktrackInputStream stream) throws IOException;

    public final Image read(ImageReadParameters parameters) throws IOException {
        final BacktrackInputStream stream = getInputAsBacktrackStream();
        stream.rewind();
        if(parameters==null) parameters = createParameters();
        return read(parameters, stream);
    }

    protected abstract Image read(ImageReadParameters parameters, BacktrackInputStream stream) throws IOException;

}
