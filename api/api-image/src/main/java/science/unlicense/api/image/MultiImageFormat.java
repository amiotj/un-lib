
package science.unlicense.api.image;

import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface MultiImageFormat extends ImageFormat{
    
    /**
     * Create a new image store.
     * 
     * @param input
     * @return ImageStore
     * @throws science.unlicense.api.io.IOException if input is not supported
     */
    ImageStore createStore(Object input) throws IOException;
    
}
