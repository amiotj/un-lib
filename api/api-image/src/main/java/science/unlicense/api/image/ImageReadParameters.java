
package science.unlicense.api.image;

import science.unlicense.api.buffer.BufferFactory;

/**
 * Describe the parameters given to an image reader.
 * Each reader implementation may extend and add specific parameters.
 * 
 * @author Johann Sorel
 */
public interface ImageReadParameters {
    
    /**
     * Set the image index from the file to read.
     * Default is 0.
     */
    void setImageIndex(int imageIndex);
    
    /**
     * @return image index.
     */
    int getImageIndex();
    
    /**
     * Indicate if the image should be decompressed at reading.
     * This ensure standard raw and colors models will be returned at the expense
     * of a possible larger memory usage.
     */
    void setDecompress(boolean decompress);
    
    /**
     * @return true if image should be decompressed while reading.
     */
    boolean isDecompress();
    
    /**
     * Set prefered buffer factory.
     * @param factory, not null
     */
    void setBufferFactory(BufferFactory factory);
    
    /**
     * 
     * @return BufferFactory, never null
     */
    BufferFactory getBufferFactory();
    
}
