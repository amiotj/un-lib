
package science.unlicense.api.image.sample;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.geometry.AbstractTupleBuffer;
import science.unlicense.api.geometry.DefaultTupleBuffer;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;

/**
 * Usual two dimension images mostly share a common image structure once
 * uncompressed. This class should suffice for image with interleaved samples.
 *
 * ImageBank is expected to be ordered line by line, each sample next to another :
 * line 0 : [s1][s2][sN][s1][s2][sN] ...
 * line 1 : [s1][s2][sN][s1][s2][sN] ...
 * ...
 *
 * If image has more then 2 dimensions, datas are expected to be ordered by successive
 * slices of the two first dimensions starting from the first dimension. 4D example :
 * Slice : [0,0,0,0]
 * Slice : [0,0,1,0]
 * Slice : [0,0,Z,0]
 * Slice : [0,0,0,1]
 * Slice : [0,0,1,1]
 * Slice : [0,0,Z,1]
 * Slice : [0,0,0,W]
 * Slice : [0,0,1,W]
 * Slice : [0,0,Z,W]
 *
 * @author Johann Sorel
 */
public class InterleavedRawModel extends AbstractRawModel{

    private final int sampleType;
    private final int nbSample;
    
    public InterleavedRawModel(int sampleType, int nbSample) {
        this.sampleType = sampleType;
        this.nbSample = nbSample;
    }

    public int getPrimitiveType(){
        return sampleType;
    }

    public int getSampleCount() {
        return nbSample;
    }

    public TupleBuffer asTupleBuffer(Image image) {
        return new RawTupleBuffer(image.getDataBuffer(),image.getExtent(),sampleType,nbSample);
    }

    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        return AbstractTupleBuffer.createPrimitiveBuffer(dimensions, sampleType, nbSample,factory);
    }

    private static final class RawTupleBuffer extends DefaultTupleBuffer{

        public RawTupleBuffer(Buffer bank, Extent box, int sampleType, int nbSample) {
            super(bank, sampleType, nbSample, box);
        }
        
        public RawTupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
            return new RawTupleBuffer(createPrimitiveBuffer(dimensions,factory), dimensions, sampleType, nbSample);
        }

        public RawTupleBuffer copy(Buffer bank) {
            return new RawTupleBuffer(bank, dimensions, sampleType, nbSample);
        }
        
    }
    
}
