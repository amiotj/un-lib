
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.DefaultTupleIterator;

/**
 *
 * @author Johann Sorel
 */
public class DefaultPixelIterator extends DefaultTupleIterator implements PixelIterator {

    public DefaultPixelIterator(PixelBuffer sm, BBox bbox) {
        super(sm, bbox);
    }

    public Color nextAsColor() {
        moveNext();
        if(finished) return null;
        return ((PixelBuffer)sm).getColor(coordinate);
    }

    public float[] nextAsRGBA(float[] buffer) {
        moveNext();
        if(finished) return null;
        return ((PixelBuffer)sm).getRGBA(coordinate,buffer);
    }

    public Integer nextAsARGB() {
        moveNext();
        if(finished) return null;
        return ((PixelBuffer)sm).getARGB(coordinate);
    }

}
