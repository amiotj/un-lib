
package science.unlicense.api.color;

import science.unlicense.api.math.Maths;

/**
 * Other kinds of blending, not Porter-Duff.
 * 
 * @author Johann Sorel
 */
public abstract class VariousBlending {

    public static class Additive extends AbstractBlending{

        @Override
        public float[] blend(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for(int i=0,off=0;i<nbElement;i++,off+=4){
                buffer[bufOffset+off+0] = Maths.min(1f,src[srcOffset+off+0]+dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = Maths.min(1f,src[srcOffset+off+1]+dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = Maths.min(1f,src[srcOffset+off+2]+dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = Maths.min(1f,src[srcOffset+off+3]+dst[dstOffset+off+3]);
            }
            return buffer;
        }

        @Override
        public float[] blendPreMul(float[] src, int srcOffset, float[] dst, int dstOffset, float[] buffer, int bufOffset, int nbElement) {
            for(int i=0,off=0;i<nbElement;i++,off+=4){
                buffer[bufOffset+off+0] = Maths.min(1f,src[srcOffset+off+0]+dst[dstOffset+off+0]);
                buffer[bufOffset+off+1] = Maths.min(1f,src[srcOffset+off+1]+dst[dstOffset+off+1]);
                buffer[bufOffset+off+2] = Maths.min(1f,src[srcOffset+off+2]+dst[dstOffset+off+2]);
                buffer[bufOffset+off+3] = Maths.min(1f,src[srcOffset+off+3]+dst[dstOffset+off+3]);
            }
            return buffer;
        }

    }

}
