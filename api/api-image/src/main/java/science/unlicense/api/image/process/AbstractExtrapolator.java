
package science.unlicense.api.image.process;

import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;

/**
 * Abstract Extrapolator.
 * Redirects double[] coordinates to nearest int[] coordinate.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractExtrapolator implements Extrapolator{

    protected Image image;
    protected TupleBuffer imageTuples;
    protected int[] dims;

    public Sampler getParent() {
        return null;
    }

    public void setParentSampling(Image image) {
        this.image = image;
        if(image!=null){
            imageTuples = image.getRawModel().asTupleBuffer(image);
            final Extent.Long extent = image.getExtent();
            dims = new int[extent.getDimension()];
            for(int i=0;i<dims.length;i++){
                dims[i] = (int) extent.getL(i);
            }
        }
    }
    
    public final void evaluate(double[] coordinate, double[] buffer) {
        if(isOutsideImage(coordinate)){
            final int[] icoord = new int[coordinate.length];
            for(int i=0;i<icoord.length;i++) icoord[i] = (int)(coordinate[i]+0.5);
            evaluateOutside(dims, buffer);
        }else{
            final int[] icoord = new int[coordinate.length];
            for(int i=0;i<icoord.length;i++) icoord[i] = (int)(coordinate[i]+0.5);
            imageTuples.getTupleDouble(icoord, buffer);
        }
    }
    
    public final void evaluate(int[] coordinate, double[] buffer) {
        if(isOutsideImage(coordinate)){
            evaluateOutside(coordinate, buffer);
        }else{
            imageTuples.getTupleDouble(coordinate, buffer);
        }
    }

    protected abstract void evaluateOutside(int[] coordinate, double[] buffer);
    
    protected boolean isOutsideImage(double[] coord){
        for(int i=0;i<coord.length;i++){
            if(coord[i]<0 || coord[i]>dims[i]-1) return true;
        }
        return false;
    }
    
    protected boolean isOutsideImage(int[] coord){
        for(int i=0;i<coord.length;i++){
            if(coord[i]<0 || coord[i]>dims[i]-1) return true;
        }
        return false;
    }

}
