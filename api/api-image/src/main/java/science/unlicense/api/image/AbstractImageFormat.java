
package science.unlicense.api.image;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractImageFormat extends DefaultFormat implements ImageFormat {

    public AbstractImageFormat(Chars identifier, Chars shortName, Chars longName, 
            Chars[] mimeTypes, Chars[] extensions, byte[][] signatures) {
        super(identifier,shortName,longName,mimeTypes,extensions, signatures);
    }

}
