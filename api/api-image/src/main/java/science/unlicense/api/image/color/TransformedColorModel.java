
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 * Color model for images of samples with special encoding, like encoding on 4bits, 
 * floats or offset and scale transform values.
 *
 * @author Johann Sorel
 */
public class TransformedColorModel extends AbstractColorModel {

    private final int sampleType;
    private final int nbSample;
    private final Transform transform;
    private final boolean alphaPremultiplied;

    /**
     *
     */
    public TransformedColorModel(RawModel rawModel, Transform trs, boolean alphaPremultiplied) {
        super(SRGB.INSTANCE);
        this.sampleType = rawModel.getPrimitiveType();
        this.nbSample = rawModel.getSampleCount();
        this.transform = trs;
        this.alphaPremultiplied = alphaPremultiplied;
    }

    public boolean isAlphaPreMultiplied() {
        return alphaPremultiplied;
    }

    public Color toColor(Object sample) {
        final float[] rgba = new float[]{0,0,0,1};
                
        if(TYPE_FLOAT == sampleType){
            final float[] samples = (float[]) sample;
            float[] res = transform.transform(samples, null);
            for(int i=0;i<res.length;i++){
                rgba[i] = res[i];
            }
        }else{
            throw new UnimplementedException("Not supported yet.");
        }
        
        return new Color(rgba,alphaPremultiplied);
    }

    public int toColorARGB(Object sample) {
        final Color color = toColor(sample);
        return color.toARGB();
    }

    public Object toSample(Color color) {
        throw new UnimplementedException("Not supported yet.");
    }

    public Object toSample(int color) {
        return toSample(new Color(color));
    }

    
}
