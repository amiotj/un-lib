package science.unlicense.api.image;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;

/**
 *
 * @author Johann Sorel
 */
public class DefaultImage implements Image{

    private final Dictionary metadatas = new HashDictionary();
    private final Dictionary models = new HashDictionary();
    private final Buffer dataBuffer;
    private final Extent.Long extent;

    public DefaultImage(Buffer dataBuffer, Extent.Long extent,
            RawModel rm) {
        models.add(ImageModel.MODEL_RAW, rm);
        this.dataBuffer = dataBuffer;
        this.extent = extent;
    }
    
    public DefaultImage(Buffer dataBuffer, Extent.Long extent,
            RawModel rm, ColorModel cm) {
        models.add(ImageModel.MODEL_RAW, rm);
        models.add(ImageModel.MODEL_COLOR, cm);
        this.dataBuffer = dataBuffer;
        this.extent = extent;
    }

    public DefaultImage(Buffer dataBuffer, Extent.Long extent, 
            Dictionary models, Dictionary metadatas) {
        if(metadatas!=null) this.metadatas.addAll(metadatas);
        if(models!=null) this.models.addAll(models);
        this.dataBuffer = dataBuffer;
        this.extent = extent;
    }

    public Extent.Long getExtent() {
        return extent;
    }

    public Buffer getDataBuffer() {
        return dataBuffer;
    }

    public Dictionary getMetadatas() {
        return metadatas;
    }

    public Dictionary getModels() {
        return models;
    }

    public RawModel getRawModel(){
        return (RawModel) models.getValue(ImageModel.MODEL_RAW);
    }

    public ColorModel getColorModel(){
        return (ColorModel) models.getValue(ImageModel.MODEL_COLOR);
    }

}
