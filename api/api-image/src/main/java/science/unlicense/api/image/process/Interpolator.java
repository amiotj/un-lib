
package science.unlicense.api.image.process;

/**
 * Interpolator are used to average samples between pixels.
 * When scaling images, this result in smoother images.
 * 
 * @author Johann Sorel
 */
public interface Interpolator extends Sampler {
    
    void setParentSampling(Sampler sampling);
    
}
