
package science.unlicense.api.image.sample;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DataCursor;
import science.unlicense.api.geometry.AbstractTupleBuffer;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import static science.unlicense.api.image.sample.RawModel.TYPE_UINT;

/**
 * Raw model where samples do not use regular sizes, such as RGB565 with model
 * sample 0 : 5bits
 * sample 1 : 6bits
 * sample 2 : 5bits
 *
 * The sample type is mapped to UINT.
 *
 * @author Johann Sorel
 */
public class BitPackRawModel extends AbstractRawModel {

    private final int tupleBitSize;
    private final int[] sampleOffsets;
    private final int[] sampleNbBits;

    public BitPackRawModel(int tupleBitSize, int[] sampleOffsets, int[] sampleNbBits) {
        this.tupleBitSize = tupleBitSize;
        this.sampleOffsets = sampleOffsets;
        this.sampleNbBits = sampleNbBits;
    }

    @Override
    public int getSampleCount() {
        return sampleOffsets.length;
    }

    @Override
    public int getPrimitiveType() {
        return TYPE_INT;
    }

    @Override
    public Buffer createBuffer(Extent.Long dimensions, BufferFactory factory) {
        return createBuffer(dimensions, tupleBitSize, factory);
    }

    @Override
    public TupleBuffer asTupleBuffer(Image image) {
        return new BitPackTupleBuffer(image.getDataBuffer(), image.getExtent(), tupleBitSize, sampleOffsets, sampleNbBits);
    }

    private static long getSize(Extent.Long dimensions, int tupleBitSize){
        long size = dimensions.getL(0);
        for(int i=1;i<dimensions.getDimension();i++){
            size *= dimensions.getL(i);
        }
        size *= tupleBitSize;
        return (size+7)/8;
    }
    
    private static Buffer createBuffer(Extent.Long dimensions, int tupleBitSize, BufferFactory factory){
        return factory.createByte(getSize(dimensions, tupleBitSize));
    }

    private static final class BitPackTupleBuffer extends AbstractTupleBuffer{

        private final Buffer buffer;
        private final Extent.Long extent;
        private final int tupleBitSize;
        private final int[] sampleOffsets;
        private final int[] sampleNbBits;
        private final DataCursor cursor;

        public BitPackTupleBuffer(Buffer buffer, Extent.Long extent, int tupleBitSize, int[] sampleOffsets, int[] sampleNbBits) {
            super(extent, TYPE_UINT, sampleOffsets.length);
            this.buffer = buffer;
            this.extent = extent;
            this.tupleBitSize = tupleBitSize;
            this.sampleOffsets = sampleOffsets;
            this.sampleNbBits = sampleNbBits;
            this.cursor = buffer.cursor();
        }

        private long getOffsetInBits(int[] coordinate){
            long offset = coordinate[1]*extent.getL(0) + coordinate[0];
            return offset * tupleBitSize;
        }

        public Buffer getPrimitiveBuffer() {
            return buffer;
        }

        public TupleBuffer create(Extent.Long dimensions,BufferFactory factory) {
            final Buffer buffer = createBuffer(dimensions, tupleBitSize,factory);
            return new BitPackTupleBuffer(buffer, extent, tupleBitSize, sampleOffsets, sampleNbBits);
        }

        public TupleBuffer copy() {
            return copy(getPrimitiveBuffer().copy());
        }

        public TupleBuffer copy(Buffer buffer) {
            return new BitPackTupleBuffer(buffer, extent, tupleBitSize, sampleOffsets, sampleNbBits);
        }

        public Object createTupleStore() {
            return new long[nbSample];
        }

        public long[] getTupleUInt(int[] coordinate, long[] buffer) {
            if(buffer==null) buffer = new long[nbSample];
            long offsetBits = getOffsetInBits(coordinate);
            long sampleOffset;
            for(int i=0;i<nbSample;i++){
                sampleOffset = offsetBits+sampleOffsets[i];
                cursor.setByteOffset(sampleOffset/8);
                cursor.setBitOffset((int)(sampleOffset%8));
                buffer[i] = cursor.readBit(sampleNbBits[i]);
            }
            return buffer;
        }

        public void setTuple(int[] coordinate, Object sample) {
            final long[] buffer = (long[]) sample;
            long offsetBits = getOffsetInBits(coordinate);
            long sampleOffset;
            for(int i=0;i<nbSample;i++){
                sampleOffset = offsetBits+sampleOffsets[i];
                cursor.setByteOffset(sampleOffset/8);
                cursor.setBitOffset((int)(sampleOffset%8));
                cursor.writeBit((int)buffer[i], sampleNbBits[i]);
            }
        }

    }

}
