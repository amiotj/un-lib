
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 * Color model where samples do not use regular sizes, such as RGB565 with model
 * sample 0 : 5bits
 * sample 1 : 6bits
 * sample 2 : 5bits
 *
 *
 * @author Johann Sorel
 */
public class BitPackColorModel extends AbstractColorModel {

    private final int[] sampleNbBits;
    private final float[] sampleMaxs;
    private final int[] sampleMapping;
    private final boolean alphaPremultiplied;

    public BitPackColorModel(int[] sampleNbBits, int[] sampleMapping, boolean alphaPremultiplied) {
        super(SRGB.INSTANCE);
        this.sampleNbBits = sampleNbBits;
        this.sampleMapping = sampleMapping;
        this.alphaPremultiplied = alphaPremultiplied;
        
        this.sampleMaxs = new float[sampleNbBits.length];
        for(int i=0;i<sampleNbBits.length;i++){
            sampleMaxs[i] = (float) Math.pow(2,sampleNbBits[i])-1f;
        }
    }

    public int[] getSampleMapping() {
        return sampleMapping;
    }

    public int[] getSampleNbBits() {
        return sampleNbBits;
    }
    
    public Color toColor(Object sample) {
        final float[] rgba = new float[]{0,0,0,1};
        final long[] samples = (long[]) sample;
        for(int i=0;i<4;i++){
            if(sampleMapping[i] < 0) continue;
            rgba[i] = samples[sampleMapping[i]]/sampleMaxs[i];
        }
        return new Color(rgba,alphaPremultiplied);
    }

    public int toColorARGB(Object sample) {
        final Color color = toColor(sample);
        return color.toARGB();
    }

    public Object toSample(Color color) {
        final float[] rgba;
        if(alphaPremultiplied){
            rgba = color.toRGBAPreMul();
        }else{
            rgba = color.toRGBA();
        }
        //flip alpha to match

        final long[] samples = new long[sampleMapping.length];
        for(int i=0;i<sampleMapping.length;i++){
            if(sampleMapping[i]<0)continue;
            samples[sampleMapping[i]] = (long) (rgba[i] * sampleMaxs[i]);
        }
        return samples;
    }

    public Object toSample(int color) {
        return toSample(new Color(color));
    }
    
}
