
package science.unlicense.api.image.process;

import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.model.doc.Document;

/**
 * Image operator where output sample value can be calculated with
 * only the input sample value.
 *
 * Override evaluate(double[] sample, double[] result) to work by pixel
 * Override evaluate(double sample) to work by sample
 * 
 * @author Florent Humbert
 * @author Johann Sorel
 */
public abstract class AbstractPointOperator extends AbstractPreservingOperator{

    public AbstractPointOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    /// when iterating
    private double[] inSamples;
    private double[] inSamplesProcessed;
    private Object outSamples;

    public Document execute() {
        super.execute();
        prepareInputs();

        //loop on all pixels
        int k;
        inSamples = new double[nbSample];
        inSamplesProcessed = new double[nbSample];
        outSamples = inputTuples.createTupleStore();
        final TupleIterator ite = inputTuples.createIterator(null);
        for(inSamples = ite.nextAsDouble(inSamples); inSamples!=null; inSamples=ite.nextAsDouble(inSamples)){
            final int[] coord = ite.getCoordinate();
            evaluate(inSamples, inSamplesProcessed);

            //clip values to image sample type
            adjustInOutSamples(inSamplesProcessed,outSamples);

            //fill out image pixel
            outputTuples.setTuple(coord, outSamples);
        }

        return outputParameters;
    }

    protected void prepareInputs(){}

    protected void evaluate(double[] sample, double[] result){
        for(int k=0;k<nbSample;k++){
            inSamplesProcessed[k] = evaluate(inSamples[k]);
        }
    }
    
    protected double evaluate(double value){
        return value;
    }

}
