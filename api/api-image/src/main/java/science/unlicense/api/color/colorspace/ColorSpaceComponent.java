
package science.unlicense.api.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.unit.Unit;

/**
 * Define a colorspace component.
 * 
 * @author Johann Sorel
 */
public class ColorSpaceComponent {
    
    private final Chars name;
    private final Unit unit;
    private final int dataType;
    private final double minValue;
    private final double maxValue;

    public ColorSpaceComponent(Chars name, Unit unit, int dataType, double minValue, double maxValue) {
        this.name = name;
        this.unit = unit;
        this.dataType = dataType;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public Chars getName() {
        return name;
    }

    public Unit getUnit() {
        return unit;
    }

    public int getDataType() {
        return dataType;
    }
    
    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

}
