
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleIterator;

/**
 *
 * @author Johann Sorel
 */
public interface PixelIterator extends TupleIterator {

    Color nextAsColor();

    float[] nextAsRGBA(float[] buffer);

    Integer nextAsARGB();

}
