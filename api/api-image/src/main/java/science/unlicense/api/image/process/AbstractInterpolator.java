
package science.unlicense.api.image.process;

/**
 * Abstract Interpolator.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractInterpolator implements Interpolator{

    protected Sampler parent;
    protected int[] dims;

    public Sampler getParent() {
        return parent;
    }

    public void setParentSampling(Sampler parent) {
        this.parent = parent;
    }
    
}
