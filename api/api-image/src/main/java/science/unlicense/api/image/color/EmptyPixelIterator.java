
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.EmptyTupleIterator;

/**
 *
 * @author Johann Sorel
 */
public class EmptyPixelIterator extends EmptyTupleIterator implements PixelIterator {

    public static final PixelIterator INSTANCE = new EmptyPixelIterator();

    public Color nextAsColor() {
        return null;
    }

    public float[] nextAsRGBA(float[] buffer) {
        return null;
    }

    public Integer nextAsARGB() {
        return null;
    }

}
