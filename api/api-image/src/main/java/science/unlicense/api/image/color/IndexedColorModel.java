
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import static science.unlicense.api.image.sample.RawModel.TYPE_1_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_2_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_4_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_INT;
import static science.unlicense.api.image.sample.RawModel.TYPE_LONG;
import static science.unlicense.api.image.sample.RawModel.TYPE_SHORT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_UINT;
import static science.unlicense.api.image.sample.RawModel.TYPE_USHORT;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 * Indexed color model.
 * Each possible value in the image is mapped to a color.
 *
 * @author Johann Sorel
 */
public class IndexedColorModel extends AbstractColorModel{

    private final int sampleType;
    private final ColorIndex index;

    public IndexedColorModel(int sampleType, ColorIndex index) {
        super(SRGB.INSTANCE);
        this.sampleType = sampleType;
        this.index = index;
    }

    /**
     * Colors palette.
     * @return array never null, copy of the palette.
     */
    public ColorIndex getPalette() {
        return index;
    }

    public Color toColor(Object sample) {
        int index;
        if(   TYPE_1_BIT == sampleType || TYPE_2_BIT == sampleType
           || TYPE_4_BIT == sampleType || TYPE_BYTE == sampleType){
            final byte[] samples = (byte[]) sample;
            index = samples[0] & 0xFF;
        }else if(TYPE_UBYTE == sampleType ){
            final int[] samples = (int[]) sample;
            index = samples[0] & 0xFF;
        }else if(TYPE_SHORT == sampleType){
            final short[] samples = (short[]) sample;
            index = samples[0] & 0xFF;
        }else if(TYPE_USHORT == sampleType ||TYPE_INT == sampleType){
            final int[] samples = (int[]) sample;
            index = samples[0];
        }else if(TYPE_UINT == sampleType ||TYPE_LONG == sampleType){
            final long[] samples = (long[]) sample;
            index = (int) samples[0];
        }else if(TYPE_FLOAT == sampleType){
            final float[] samples = (float[]) sample;
            index = (int)samples[0];
        }else if(TYPE_DOUBLE == sampleType){
            final double[] samples = (double[]) sample;
            index = (int)samples[0];
        }else{
            throw new RuntimeException("Unsupported sample type "+sampleType);
        }

        return this.index.palette[index];
    }

    public int toColorARGB(Object sample) {
        return toColor(sample).toARGB();
    }

    public Object toSample(Color color) {
        for(int i=0;i<this.index.palette.length;i++){
            if(this.index.palette[i].equals(color)){
                return i;
            }
        }
        throw new RuntimeException("Sample value is not in available palette");
    }

    public Object toSample(int color) {
        return toSample(new Color(color));
    }
    
}
