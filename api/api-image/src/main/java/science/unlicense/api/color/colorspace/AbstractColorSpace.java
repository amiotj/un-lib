
package science.unlicense.api.color.colorspace;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.math.transform.Transform;

/**
 * Abstract ColorSpace implementation, will fall back on single tuple
 * conversion methods.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractColorSpace implements ColorSpace {
    
    protected final Chars name;
    protected final ColorSpaceComponent[] components;
    protected final int dimension;

    /**
     * 
     * 
     * @param name colorspace name
     * @param components colorspace components
     */
    public AbstractColorSpace(Chars name, ColorSpaceComponent[] components) {
        this.name = name;
        this.components = components;
        this.dimension = components.length;
    }

    public Chars getName() {
       return name;
    }

    public int getDimension() {
        return dimension;
    }

    public ColorSpaceComponent[] getComponents() {
        return components;
    }

    public int[] toRGBA(int[] values, int[] buffer, int precision) {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public int[] toSpace(int[] rgba, int[] buffer, int precision) {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public void toRGBA(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        final float[] in = new float[dimension];
        final float[] out = new float[4];
        for(int i=0;i<nbTuple;i++){
            for(int k=0;k<dimension;k++) in[k] = (float) source[sourceOffset+k];
            toRGBA(in, out);
            for(int k=0;k<4;k++) dest[destOffset+k] = out[k];
            sourceOffset += dimension;
            destOffset += 4;
        }
    }

    public void toRGBA(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        final float[] in = new float[dimension];
        final float[] out = new float[4];
        for(int i=0;i<nbTuple;i++){
            Arrays.copy(source, sourceOffset, dimension, in, 0);
            toRGBA(in, out);
            Arrays.copy(out, 0, 4, dest, destOffset);
            sourceOffset += dimension;
            destOffset += 4;
        }
    }

    public void toRGBA(int[] source, int sourceOffset, int[] dest, int destOffset, int nbTuple, int precision) {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public void toSpace(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        final float[] in = new float[4];
        final float[] out = new float[dimension];
        for(int i=0;i<nbTuple;i++){
            for(int k=0;k<4;k++) in[k] = (float) source[sourceOffset+k];
            toSpace(in, out);
            for(int k=0;k<dimension;k++) dest[destOffset+k] = out[k];
            sourceOffset += 4;
            destOffset += dimension;
        }
    }

    public void toSpace(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        final float[] in = new float[4];
        final float[] out = new float[dimension];
        for(int i=0;i<nbTuple;i++){
            Arrays.copy(source, sourceOffset, 4, in, 0);
            toSpace(in, out);
            Arrays.copy(out, 0, dimension, dest, destOffset);
            sourceOffset += 4;
            destOffset += dimension;
        }
    }

    public void toSpace(int[] source, int sourceOffset, int[] dest, int destOffset, int nbTuple, int precision) {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public Transform getTranform(ColorSpace cs) {
        return new FallBackCsTransformation(this, cs);
    }

}
