
package science.unlicense.api.image.color;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.AbstractTupleBuffer;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPixelBuffer extends AbstractTupleBuffer implements PixelBuffer {

    public AbstractPixelBuffer(Extent.Long dimensions, int sampleType, int nbSample) {
        super(dimensions, sampleType, nbSample);
    }

    @Override
    public Buffer getPrimitiveBuffer() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public TupleBuffer create(Extent.Long dimensions) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public TupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
        throw new UnimplementedException("Not supported yet.");
    }
    
    @Override
    public TupleBuffer copy(Buffer buffer) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public abstract PixelIterator createIterator(BBox bbox);

    @Override
    public int getARGB(int[] coord) {
        return getColor(coord).toARGB();
    }

    @Override
    public float[] getRGBA(int[] coord, float[] buffer) {
        if(buffer!=null){
            return getColor(coord).toRGBA(buffer,0);
        }else{
            return getColor(coord).toRGBA();
        }
    }

    @Override
    public void setARGB(int[] coord, int argb) {
        setColor(coord, new Color(argb));
    }

    @Override
    public void setRGBA(int[] coord, float[] rgba) {
        setColor(coord, new Color(rgba));
    }

    @Override
    public void setARGB(BBox box, int argb) {
        box = clipToBufferBox(box);
        if(box==null) return;

        final int nbDim = box.getDimension();
        final int[] coordinate = new int[nbDim];

        for(int i=0;i<nbDim;i++){
            coordinate[i] = (int)box.getMin(i);
        }

        combinaison:
        for(;;){
            setARGB(coordinate, argb);
            //prepare next iteration
            for(int i=nbDim-1;i>=0;i--){
                coordinate[i]++;
                if(coordinate[i]>=box.getMax(i)){
                    if(i==0){
                        //we have finish
                        break combinaison;
                    }
                    //continue loop increment previous dimension
                    coordinate[i] = (int)box.getMin(i);
                }else{
                    break;
                }
            }
        }
    }

    @Override
    public void setRGBA(BBox box, float[] rgba) {
        box = clipToBufferBox(box);
        if(box==null) return;

        final int nbDim = box.getDimension();
        final int[] coordinate = new int[nbDim];

        for(int i=0;i<nbDim;i++){
            coordinate[i] = (int)box.getMin(i);
        }

        combinaison:
        for(;;){
            setRGBA(coordinate, rgba);
            //prepare next iteration
            for(int i=nbDim-1;i>=0;i--){
                coordinate[i]++;
                if(coordinate[i]>=box.getMax(i)){
                    if(i==0){
                        //we have finish
                        break combinaison;
                    }
                    //continue loop increment previous dimension
                    coordinate[i] = (int)box.getMin(i);
                }else{
                    break;
                }
            }
        }
    }

    @Override
    public void setColor(BBox box, Color color) {
        box = clipToBufferBox(box);
        if(box==null) return;

        final int nbDim = box.getDimension();
        final int[] coordinate = new int[nbDim];

        for(int i=0;i<nbDim;i++){
            coordinate[i] = (int)box.getMin(i);
        }

        combinaison:
        for(;;){
            setColor(coordinate, color);
            //prepare next iteration
            for(int i=nbDim-1;i>=0;i--){
                coordinate[i]++;
                if(coordinate[i]>=box.getMax(i)){
                    if(i==0){
                        //we have finish
                        break combinaison;
                    }
                    //continue loop increment previous dimension
                    coordinate[i] = (int)box.getMin(i);
                }else{
                    break;
                }
            }
        }
    }

}
