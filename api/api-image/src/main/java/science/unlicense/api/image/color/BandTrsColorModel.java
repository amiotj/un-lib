
package science.unlicense.api.image.color;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.color.Color;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 *
 * @author Johann Sorel
 */
public class BandTrsColorModel extends AbstractColorModel {

    private final int sampleType;
    private final int nbSample;
    private final int[] mapping;
    private final float[] scale;
    private final float[] offset;
    private final boolean alphaPremultiplied;

    public BandTrsColorModel(RawModel rawModel, int[] mapping, float[] scale, float[] offset, boolean alphaPremultiplied) {
        super(SRGB.INSTANCE);
        this.sampleType = rawModel.getPrimitiveType();
        this.nbSample = rawModel.getSampleCount();
        this.mapping = mapping;
        this.scale = scale;
        this.offset = offset;
        this.alphaPremultiplied = alphaPremultiplied;
    }

    public int[] getMapping() {
        return Arrays.copy(mapping);
    }

    public boolean isAlphaPreMultiplied() {
        return alphaPremultiplied;
    }

    public Color toColor(Object sample) {
        final float[] rgba = new float[]{0,0,0,1};
        if(TYPE_BYTE == sampleType){
            final byte[] samples = (byte[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = samples[mapping[i]]*scale[i] + offset[i];
            }
        }else if(TYPE_UBYTE == sampleType){
            final int[] samples = (int[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = samples[mapping[i]]*scale[i] + offset[i];
            }
        }else if(TYPE_FLOAT == sampleType){
            final float[] samples = (float[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = samples[mapping[i]]*scale[i] + offset[i];
            }
        }else if(TYPE_DOUBLE == sampleType){
            final double[] samples = (double[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = (float)samples[mapping[i]]*scale[i] + offset[i];
            }
        }
        return new Color(rgba,alphaPremultiplied);
    }

    public int toColorARGB(Object sample) {
        final Color color = toColor(sample);
        return color.toARGB();
    }

    public Object toSample(Color color) {
        final float[] rgba;
        if(alphaPremultiplied){
            rgba = color.toRGBAPreMul();
        }else{
            rgba = color.toRGBA();
        }
        //flip alpha to match

        if(TYPE_BYTE == sampleType){
            final byte[] samples = new byte[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = (byte)(rgba[i] * 255);
            }
            return samples;
        }else if(TYPE_UBYTE == sampleType){
            final int[] samples = new int[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = (int)(rgba[i] * 255);
            }
            return samples;
        }else if(TYPE_FLOAT == sampleType){
            final float[] samples = new float[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = rgba[i];
            }
            return samples;
        }else if(TYPE_DOUBLE == sampleType){
            final double[] samples = new double[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = rgba[i];
            }
            return samples;
        }else{
            throw new RuntimeException("Sample can not be defined for given color.");
        }
    }

    public Object toSample(int color) {
        return toSample(new Color(color));
    }

    
}
