
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer;

/**
 * Tuple buffer with extra color accessors.
 *
 * @author Johann Sorel
 */
public interface PixelBuffer extends TupleBuffer {

    int getARGB(int[] coord);

    float[] getRGBA(int[] coord, float[] buffer);

    Color getColor(int[] coord);

    void setARGB(int[] coord, int argb);

    void setRGBA(int[] coord, float[] rgba);

    void setColor(int[] coord, Color color);

    void setARGB(BBox box, int argb);

    void setRGBA(BBox box, float[] rgba);

    void setColor(BBox box, Color color);

    PixelIterator createIterator(BBox bbox);
    
}
