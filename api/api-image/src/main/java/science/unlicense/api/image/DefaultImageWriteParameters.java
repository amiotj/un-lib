
package science.unlicense.api.image;

/**
 * Default image write parameters.
 * 
 * @author Johann Sorel
 */
public class DefaultImageWriteParameters implements ImageWriteParameters {
    
}
