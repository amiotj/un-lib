
package science.unlicense.api.image.process;


/**
 * Interpolator are used to average samples between pixels.
 * When scaling images, this result in smoother images.
 * 
 * @author Johann Sorel
 */
public class InterpolatorNearest extends AbstractInterpolator {

    public void evaluate(int[] coordinate, double[] buffer) {
        parent.evaluate(coordinate, buffer);
    }

    public void evaluate(double[] coordinate, double[] buffer) {
        final int[] icoord = new int[coordinate.length];
        for(int i=0;i<icoord.length;i++) icoord[i] = (int)(coordinate[i]+0.5);
        parent.evaluate(icoord, buffer);
    }

}
