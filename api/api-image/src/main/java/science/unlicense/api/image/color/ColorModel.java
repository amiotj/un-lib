package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.ImageModel;

/**
 * A color model convert an image sample value to a color.
 * All images do not store necesarly rgb values, there can be a indexed color palette
 * or any other kind of computation to obtain a color.
 * The color model takes care to convert sample to color and color to sample.
 *
 * @author Johann Sorel
 */
public interface ColorModel extends ImageModel {

    /**
     * Colorspace provided by the tuple buffer method.
     * 
     * @return ColorSpace, never null.
     */
    ColorSpace getColorSpace();
    
    /**
     * Convert given sample in a color.
     * @param sample
     * @return ColorRGBA
     */
    Color toColor(Object sample);

    /**
     * Convert given sample in a ARGB integer color.
     * 8bits per color components : A R G B
     * @param sample
     * @return ARGB color as integer
     */
    int toColorARGB(Object sample);

    /**
     * Convert given color in a sample value.
     * Caution 1 : there might exist multiple sample values for a color
     *           There is no guarantee on which sample value will be choosen
     * Caution 2 : given color might not have an exact match in this color model
     *           The color model will choose arbitrarly a close value.
     *
     * @param color
     * @return Object most appropriate sample structure
     */
    Object toSample(Color color);

    /**
     * Convert given color in a sample value.
     * Caution 1 : there might exist multiple sample values for a color
     *           There is no guarantee on which sample value will be choosen
     * Caution 2 : given color might not have an exact match in this color model
     *           The color model will choose arbitrarly a close value.
     *
     *
     * @param color
     * @return Object most appropriate sample structure
     */
    Object toSample(int color);


    /**
     * Provide a tuple buffer view as a one sample int tuple.
     * The PixelBuffer provide additional accessors for color types.
     * 
     * @param image
     * @return PixelBuffer
     */
    PixelBuffer asTupleBuffer(Image image);
    
}
