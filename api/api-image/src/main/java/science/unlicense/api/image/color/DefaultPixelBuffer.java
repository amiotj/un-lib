
package science.unlicense.api.image.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class DefaultPixelBuffer extends AbstractPixelBuffer {

    private final ColorModel cm;
    private final TupleBuffer rawTuples;

    public DefaultPixelBuffer(Image image, ColorModel cm) {
        super(image.getExtent(), Primitive.TYPE_FLOAT, cm.getColorSpace().getDimension());
        this.rawTuples = image.getRawModel().asTupleBuffer(image);
        this.cm = cm;
    }

    public float[] getTupleFloat(int[] coordinate, float[] buffer) {
        final Color color = getColor(coordinate);
        if(buffer==null){
            return color.toRGBA();
        }else{
            return color.toRGBA(buffer, 0);
        }
    }

    public Color getColor(int[] coord) {
        return cm.toColor(rawTuples.getTuple(coord, null));
    }

    public void setARGB(int[] coord, int color) {
        rawTuples.setTuple(coord,cm.toSample(color));
    }

    public void setColor(int[] coord, Color color) {
        rawTuples.setTuple(coord,cm.toSample(color));
    }

    public PixelIterator createIterator(BBox bbox) {
        bbox = clipToBufferBox(bbox);
        if(bbox==null) return EmptyPixelIterator.INSTANCE;
        return new DefaultPixelIterator(this, bbox);
    }
    
}
