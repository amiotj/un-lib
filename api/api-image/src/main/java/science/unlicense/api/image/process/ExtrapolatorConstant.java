
package science.unlicense.api.image.process;

/**
 * Extend image filled pixels with a constant value.
 * For example if we add value 5 to a one channel image we obtain ;
 * <pre>
 *              5 5 5 5 5
 * 3 3 3        5 3 3 3 5
 * 3 3 3  ===>  5 3 3 3 5
 * 3 3 3        5 3 3 3 5
 *              5 5 5 5 5
 * </pre>
 *
 * @author Humbert Florent
 * @author Johann Sorel
 */
public class ExtrapolatorConstant extends AbstractExtrapolator implements Extrapolator{

    private final double value;

    public ExtrapolatorConstant(double value) {
        this.value = value;
    }

    public void evaluateOutside(int[] coordinate, double[] buffer) {
        for(int i=0;i<buffer.length;i++)buffer[i] = value;
    }

}
