
package science.unlicense.api.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;

/**
 * Utilities methodes for colorspaces.
 *
 * @author Johann Sorel
 */
public final class ColorSpaces {

    private ColorSpaces(){}

    /**
     * Lists available color spaces.
     * @return array of ColorSpace, never null but can be empty.
     */
    public static ColorSpace[] getColorSpaces(){
        final NamedNode root = science.unlicense.system.System.get().getModuleManager().getMetadataRoot().search(new Chars("services/science.unlicense.api.color.colorspace.ColorSpace"));
        if(root==null){
            return new ColorSpace[0];
        }
        return (ColorSpace[]) Nodes.getChildrenValues(root, null).toArray(ColorSpace.class);
    }

}
