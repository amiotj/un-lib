
package science.unlicense.api.color.colorspace;

import science.unlicense.api.math.transform.AbstractTransform;


/**
 * Transformation between 2 colorspaces passing by intermediate sRGB
 * color space.
 *
 * @author Johann Sorel
 */
public class FallBackCsTransformation extends AbstractTransform {

    private final ColorSpace in;
    private final ColorSpace out;

    public FallBackCsTransformation(ColorSpace in, ColorSpace out) {
        this.in = in;
        this.out = out;
    }

    public int getInputDimensions() {
        return in.getDimension();
    }

    public int getOutputDimensions() {
        return out.getDimension();
    }

    public double[] transform(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple) {
        final double[] temp = new double[getOutputDimensions()*nbTuple];
        in.toRGBA(source, sourceOffset, temp, 0, nbTuple);
        out.toSpace(temp, 0, dest, destOffset, nbTuple);
        return dest;
    }

    public float[] transform(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple) {
        final float[] temp = new float[getOutputDimensions()*nbTuple];
        in.toRGBA(source, sourceOffset, temp, 0, nbTuple);
        out.toSpace(temp, 0, dest, destOffset, nbTuple);
        return dest;
    }

}
