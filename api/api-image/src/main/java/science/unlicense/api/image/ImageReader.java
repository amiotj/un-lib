package science.unlicense.api.image;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.Reader;
import science.unlicense.api.model.Metadata;

/**
 *
 * @author Johann Sorel
 */
public interface ImageReader extends Reader {

    /**
     * Create a default read parameters.
     *
     * @return ImageReadParameters
     */
    ImageReadParameters createParameters();

    /**
     * Image files store different metadata models whitin them.
     * Each often being specific to it's own storage design.
     *
     * @return array of avaiable metadata model names.
     */
    Chars[] getMetadataNames() throws IOException;

    /**
     * Get metadata of the given metamodel name.
     *
     * @param name metamodel name.
     * @return Node
     */
    Metadata getMetadata(Chars name) throws IOException;

    /**
     * Read a single image in the file.
     *
     * @param params , can be null.
     * @return
     * @throws IOException
     */
    Image read(ImageReadParameters params) throws IOException;

}
