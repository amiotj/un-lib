package science.unlicense.api.color;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Matrices;
import science.unlicense.api.number.Int32;

/**
 * Conversion methods for different colorspaces.
 *
 * @author Johann Sorel
 * @author Florent Humbert
 */
public final class Colors {

    public static final int MASK_ARGB = 0xFFFFFFFF;
    public static final int MASK_RGB  = 0x00FFFFFF;
    public static final int MASK_A    = 0xFF000000;
    public static final int MASK_R    = 0x00FF000;
    public static final int MASK_G    = 0x0000FF00;
    public static final int MASK_B    = 0x000000FF;

    private static final double[][] RGB_TO_YUV = new double[][]{
        { 0.299,      0.587,      0.114},
        {-0.14713,   -0.28886,    0.436},
        { 0.615,     -0.51498,   -0.10001}
    };

    private static final double[][] YUV_TO_RGB = new double[][]{
        {1,   0,          1.13983},
        {1,  -0.39465,   -0.58060},
        {1,   2.03211,    0      }
    };

    private static final double[][] YCbCr_TO_RGB = new double[][]{
        {1.164,    0,      1.569},
        {1.164,   -0.392, -0.813},
        {1.164,    2.017,  0    }
    };

    private static final double[][] RGB_TO_YCbCr = new double[][]{
        { 0.257,    0.504,   0.098},
        {-0.148,   -0.291,   0.439},
        { 0.439,   -0.368,  -0.071}
    };

    private Colors(){}

    public static int[] toRGB(int rgb, int[] buffer) {
        if(buffer==null) buffer = new int[3];
        buffer[0] = (rgb>>16) & 0xFF;
        buffer[1] = (rgb>>8) & 0xFF;
        buffer[2] = (rgb>>0) & 0xFF;
        return buffer;
    }

    public static int[] toRGBA(int rgb, int[] buffer) {
        if(buffer==null) buffer = new int[4];
        buffer[0] = (rgb>>16) & 0xFF;
        buffer[1] = (rgb>>8) & 0xFF;
        buffer[2] = (rgb>>0) & 0xFF;
        buffer[3] = (rgb>>24) & 0xFF;
        return buffer;
    }

    public static int[] toARGB(int rgb, int[] buffer) {
        if(buffer==null) buffer = new int[4];
        buffer[0] = (rgb>>24) & 0xFF;
        buffer[1] = (rgb>>16) & 0xFF;
        buffer[2] = (rgb>>8) & 0xFF;
        buffer[3] = (rgb>>0) & 0xFF;
        return buffer;
    }

    public static int toARGB(int r, int g, int b) {
        return toARGB(255, r, g, b);
    }

    public static int toARGB(int[] argb) {
        return toARGB(argb[0], argb[1], argb[2], argb[3]);
    }

    public static int toARGB(int a, int r, int g, int b) {
        return a << 24 | r << 16 | g << 8 | b ;
    }

    public static int toARGB(float r, float g, float b, float a) {
        return toARGB((int)(a*255), (int)(r*255), (int)(g*255), (int)(b*255));
    }

    /**
     * @param rgba in order [R,G,B,A]
     * @return int ARGB
     */
    public static int toARGB(float[] rgba) {
        return toARGB((int)(rgba[3]*255), (int)(rgba[0]*255), (int)(rgba[1]*255), (int)(rgba[2]*255));
    }

    /**
     * @param argb int ARGB
     * @return float[] [R,G,B,A]
     */
    public static float[] toRGBA(int argb, float[] buffer) {
        if(buffer==null) buffer = new float[4];
        buffer[0] = (float)((argb>>16) & 0xFF) / 255f;
        buffer[1] = (float)((argb>>8) & 0xFF) / 255f;
        buffer[2] = (float)((argb>>0) & 0xFF) / 255f;
        buffer[3] = (float)((argb>>24) & 0xFF) / 255f;
        return buffer;
    }


    public static int GrayScaletoRGB(int gray) {
        gray&=0xFF;
        return ((gray<<16)+(gray<<8)+gray);
    }

    public static int invertRGB(int rgb) {
        final int alpha = (rgb >> 24) & 0xFF;
        final int red = 255 - (rgb >> 16) & 0xFF;
        final int green = 255 - (rgb >> 8) & 0xFF;
        final int blue = 255 - (rgb) & 0xFF;
        return (alpha << 24) + (red << 16) + (green << 8) + blue;
    }

    public static float[] RGBtoYUV(float[] rgb){
        return Matrices.transform(RGB_TO_YUV, rgb, null);
    }

    public static float[] YUVtoRGB(float[] yuv){
        return Matrices.transform(YUV_TO_RGB, yuv, null);
    }

    public static float[] RGBtoYCbCr(float[] rgb) {
        final float[] ycbcr = Matrices.transform(YCbCr_TO_RGB, rgb, null);
        ycbcr[0] += 0.0625f;
        ycbcr[1] += 0.5f;
        ycbcr[2] += 0.5f;
        return ycbcr;
    }

    public static float[] YCbCrtoRGB(float[] ycbcr) {
        ycbcr[0] -= 0.0625f;
        ycbcr[1] -= 0.5f;
        ycbcr[2] -= 0.5f;
        return Matrices.transform(RGB_TO_YCbCr, ycbcr, null);
    }

    /**
     * RGB to grayscale using PAL luma components.
     * http://en.wikipedia.org/wiki/Grayscale
     * @param rgb
     * @return grayscale
     */
    public static float RGBtoGrayScalePAL(float[] rgb){
        return rgb[0] * 0.299f + rgb[1] * 0.587f + rgb[2] * 0.114f;
    }

    /**
     * RGB to grayscale using HDTV luma components.
     * http://en.wikipedia.org/wiki/Grayscale
     * @param rgb
     * @return grayscale
     */
    public static float RGBtoGrayScaleHDTV(float[] rgb){
        return rgb[0] * 0.2126f + rgb[1] * 0.7152f + rgb[2] * 0.0722f;
    }

    /**
     * http://en.wikipedia.org/wiki/YCbCr
     */
    public static int[] YCbCrtoRGB(int[] ycbcr) {
        return new int[]{
            Maths.clamp(ycbcr[0]                                 + (int)(1.402  *(ycbcr[2]-128)), 0, 255),
            Maths.clamp(ycbcr[0] - (int)(0.34414*(ycbcr[1]-128)) - (int)(0.71414*(ycbcr[2]-128)), 0, 255),
            Maths.clamp(ycbcr[0] + (int)(1.772  *(ycbcr[1]-128))                                , 0, 255)

        };
    }
    
    public static void YCbCrtoRGB(int[] ycbcr, int[] rgb) {
        rgb[0] = Maths.clamp(ycbcr[0]                                 + (int)(1.402  *(ycbcr[2]-128)), 0, 255);
        rgb[1] = Maths.clamp(ycbcr[0] - (int)(0.34414*(ycbcr[1]-128)) - (int)(0.71414*(ycbcr[2]-128)), 0, 255);
        rgb[2] = Maths.clamp(ycbcr[0] + (int)(1.772  *(ycbcr[1]-128))                                , 0, 255);
    }

    /**
     * http://en.wikipedia.org/wiki/YCbCr
     */
    public static int[] RGBtoYCbCr(int[] rgb) {
        return new int[]{
            Maths.clamp(  0 + (int)(0.299*rgb[0])    + (int)(0.587*rgb[1])    + (int)(0.114*rgb[2]),    0, 255),
            Maths.clamp(128 - (int)(0.168736*rgb[0]) - (int)(0.331264*rgb[1]) + (int)(0.5*rgb[2]),      0, 255),
            Maths.clamp(128 + (int)(0.5*rgb[0])      - (int)(0.418688*rgb[1]) - (int)(0.081312*rgb[2]), 0, 255)
        };
    }

    public static void RGBToHSL(double r, double g, double b, double[] hsl) {
        if(hsl.length!=3){
            throw new InvalidArgumentException("HSL.lenght!=3");
        }
        final double red = (r)/255.0;
        final double green = ( g)/255.0;
        final double blue = (b)/255.0;

        final double max = Maths.max(Maths.max(blue, red), green);
        final double min = Maths.min(Maths.min(blue, red), green);

        //compute H
        double h = 0;
        if(max==min){
            h=0;
        }else if(max==red){
            h = ((int) (60.0 * (green-blue)/(max-min))) % 360;
        }else if(max==green){
            h = ((int)((60.0 * (blue-red)/(max-min) + 120))) % 360;
        }else if(max==blue){
            h = ((int)((60.0 * (red-green)/(max-min) + 240)))% 360;
        }
        hsl[0] = h;

        //compute S
        double s = 0;
        if(max==min){
            s=0;
        }else if(max+min<=1.0){
            s = (max-min)/(max+min);
        }else{
            s = (max-min)/(2.0-(max+min));
        }
        hsl[1] = s;

        //compute L
        double l = 0.5*(max+min);
        hsl[2]=l;
    }

    public static void RGBtoHSL(int rgb, double[] hsl) {
        RGBToHSL((rgb >> 16) & 0xFF,(rgb >> 8) & 0xFF,rgb & 0xFF, hsl);
    }

    public static int HSLtoRGB(double[] hsl) {
        final double q;
        final double h = hsl[0];
        final double s = hsl[1];
        final double l = hsl[2];

        final int r, g, b;
        if (s==0) {
            r = g = b = (int) (255.0*l);
        } else {

            if(l<0.5){
                q = l * (1+s);
            }else{
                q = l+s-(l*s);
            }

            final double p = 2*l-q;
            final double hk = h/360.0;
            final double dR = hue2rgb(p, q, hk+1.0/3.0);
            final double dG = hue2rgb(p, q, hk);
            final double dB = hue2rgb(p, q, hk -1.0/3.0);

            r = (int) (dR*255.0);
            g = (int) (dG*255.0);
            b = (int) (dB*255.0);
        }

        return (r<<16) + (g<<8) + b;
    }

    private static double hue2rgb(double p, double q, double tc) {
        if(tc<0) tc+=1;
        if(tc>1) tc-=1;
        if(tc<1.0/6.0)
            return p+((q-p)*6.0*tc);
        if(tc<1.0/2.0)
            return q;
        if(tc<2.0/3.0)
            return p+((q-p)*6*(2.0/3.0-tc));
        return p;
    }

    /**
     * Convert from XYZ CIE to sRGB colorspace.
     *
     * @param xyz size 3, values between 0 and 1
     * @param rgb size 3 or 4, values between 0 and 255
     */
    public static void XYZtoRGB(double[] xyz, int[] rgb) {
        double r = 3.1338561*xyz[0]+ -1.6168667*xyz[1]+ -0.4906146*xyz[2];
        double g = -0.9787684*xyz[0]+  1.9161415*xyz[1]+  0.0334540*xyz[2];
        double b =  0.0719453*xyz[0]+ -0.2289914*xyz[1]+  1.4052427*xyz[2];

        r=XYZtoRGBTemp(r);
        g=XYZtoRGBTemp(g);
        b=XYZtoRGBTemp(b);

        r*=255.0f;
        g*=255.0f;
        b*=255.0f;
        rgb[0] = (int) r;
        rgb[1] = (int) g;
        rgb[2] = (int) b;
        for(int c = 0;c<3;c++) {
            if(rgb[c]<0) rgb[c] = 0;
            if(rgb[c]>255) rgb[c] = 255;
        }
    }

    /**
     * Convert from sRGB to XYZ CIE colorspace.
     *
     * @param xyz size 3, values between 0 and 1
     * @param rgb size 3 or 4, values between 0 and 255
     */
    public static void RGBtoXYZ(double[] xyz, int[] rgb) {
        double r = (rgb[0]/255.0) ;
        double g = (rgb[1]/255.0) ;
        double b = (rgb[2]/255.0) ;

        r = RGBtoXYZTemp(r);
        g = RGBtoXYZTemp(g);
        b = RGBtoXYZTemp(b);

        xyz[0] =  0.4360747*r + 0.3850649 *g+ 0.1430804*b;
        xyz[1] =  0.2225045*r + 0.7168786 *g+ 0.0606169*b;
        xyz[2] =  0.0139322*r + 0.0971045 *g+ 0.7141733*b;
    }

    private static double RGBtoXYZTemp(double c) {
        if (c <= 0.04045){
            c /= 12.92;
        }else{
            c = Math.pow((c+0.055)/1.055,2.4);
        }
        return c;
    }

    private static double XYZtoRGBTemp(double c) {
        if(c<0.0031308){
            return 12.92*c;
        }else{
            return 1.055*Math.pow(c, 1.0/2.4)-0.055;
        }
    }

    /**
     * Convert from XYZ CIE to sRGB colorspace using only conversion matrix.
     *
     * @param xyz size 3, values between 0 and 1
     * @param rgb size 3 or 4, values between 0 and 255
     */
    public static void XYZtoRGBMatrix(double[] xyz, int[] rgb) {
        double r = 3.1338561*xyz[0]+ -1.6168667*xyz[1]+ -0.4906146*xyz[2];
        double g = -0.9787684*xyz[0]+  1.9161415*xyz[1]+  0.0334540*xyz[2];
        double b =  0.0719453*xyz[0]+ -0.2289914*xyz[1]+  1.4052427*xyz[2];

        r*=255.0f;
        g*=255.0f;
        b*=255.0f;
        rgb[0] = (int) r;
        rgb[1] = (int) g;
        rgb[2] = (int) b;
        for(int c = 0;c<3;c++) {
            if(rgb[c]<0) rgb[c] = 0;
            if(rgb[c]>255) rgb[c] = 255;
        }
    }

    /**
     * Convert from sRGB to XYZ CIE colorspace using only conversion matrix.
     *
     * @param xyz size 3, values between 0 and 1
     * @param rgb size 3 or 4, values between 0 and 255
     */
    public static void RGBtoXYZMatrix(double[] xyz, int[] rgb) {
        final double r = (rgb[0]/255.0) ;
        final double g = (rgb[1]/255.0) ;
        final double b = (rgb[2]/255.0) ;

        xyz[0] = 0.4360747*r + 0.3850649*g + 0.1430804*b;
        xyz[1] = 0.2225045*r + 0.7168786*g + 0.0606169*b;
        xyz[2] = 0.0139322*r + 0.0971045*g + 0.7141733*b;
    }

    public static int RGB555toARGB(int rgb555){
        return 255 << 24
             | (C5toC8((rgb555 & 0x7C00) >> 10) << 16)
             | (C5toC8((rgb555 & 0x03E0) >>  5) <<  8)
             | (C5toC8((rgb555 & 0x001F)      )      );
    }

    public static int[] RGB555toRGBA(int rgb555){
        return new int[]{
            C5toC8((rgb555 & 0x7c00) >> 10),
            C5toC8((rgb555 & 0x03e0) >>  5),
            C5toC8((rgb555 & 0x001f)      ),
            255};
    }

    public static int RGB565toARGB(int rgb565){
        return 255 << 24
             | (C5toC8((rgb565 & 0xf800) >> 11) << 16)
             | (C6toC8((rgb565 & 0x07e0) >>  5) <<  8)
             | (C5toC8((rgb565 & 0x001f)      )      );
    }

    public static int[] RGB565toRGBA(int rgb565){
        return new int[]{
            C5toC8((rgb565 & 0xf800) >> 11),
            C6toC8((rgb565 & 0x07e0) >>  5),
            C5toC8((rgb565 & 0x001f)      ),
            255};
    }

    /**
     * Convert 4 bits [0-16] to 8 bits [0-255] value.
     */
    public static int C4toC8(int c){
        return c*17;
    }

    /**
     * Convert 5 bits [0-31] to 8 bits [0-255] value.
     * origin : http://stackoverflow.com/questions/2442576/how-does-one-convert-16-bit-rgb565-to-24-bit-rgb888
     */
    public static int C5toC8(int c){
        return (c*527+23) >> 6;
    }

    /**
     * Convert 6 bits [0-63] to 8 bits [0-255] value.
     * origin : http://stackoverflow.com/questions/2442576/how-does-one-convert-16-bit-rgb565-to-24-bit-rgb888
     */
    public static int C6toC8(int c){
        return (c*259+33) >> 6;
    }

    /**
     * Convert 8 bits [0-255] to 4 bits [0-16] value.
     */
    public static int C8toC4(int c){
        return c/17;
    }

    /**
     * Convert 8 bits [0-255] to 5 bits [0-31] value.
     * origin : http://stackoverflow.com/questions/2442576/how-does-one-convert-16-bit-rgb565-to-24-bit-rgb888
     */
    public static int C8toC5(int c){
        return (c*249+1014) >> 11;
    }

    /**
     * Convert 8 bits [0-255] to 6 bits [0-63] value.
     * origin : http://stackoverflow.com/questions/2442576/how-does-one-convert-16-bit-rgb565-to-24-bit-rgb888
     */
    public static int C8toC6(int c){
        return (c*253+505) >> 10;
    }

    /**
     * Interpolate between 2 colors.
     * @param start
     * @param end
     * @param ratio 0 with return start color, 1 will return end color
     * @return
     */
    public static Color interpolate(Color start, Color end, float ratio){
        final float[] startARGB = start.toRGBA();
        final float[] endARGB = end.toRGBA();
        return new Color(
                startARGB[0] + ratio*(endARGB[0] - startARGB[0]),
                startARGB[1] + ratio*(endARGB[1] - startARGB[1]),
                startARGB[2] + ratio*(endARGB[2] - startARGB[2]),
                startARGB[3] + ratio*(endARGB[3] - startARGB[3])
                );
    }

    /**
     * Interpolate between 2 colors.
     * @param start
     * @param end
     * @param ratio 0 with return start color, 1 will return end color
     * @return
     */
    public static int interpolate(int start, int end, float ratio){
        final int sr = ((start>>16) & 0xFF);
        final int sg = ((start>> 8) & 0xFF);
        final int sb = ((start>> 0) & 0xFF);
        final int sa = ((start>>24) & 0xFF);
        final int er = ((  end>>16) & 0xFF);
        final int eg = ((  end>> 8) & 0xFF);
        final int eb = ((  end>> 0) & 0xFF);
        final int ea = ((  end>>24) & 0xFF);
        return toARGB(
                sa + (int)(ratio*(ea-sa)),
                sr + (int)(ratio*(er-sr)),
                sg + (int)(ratio*(eg-sg)),
                sb + (int)(ratio*(eb-sb)));
    }

    /**
     * Parse a color in format #FFFFFF
     * @param text
     * @return
     */
    public static Color fromHexa(Chars text){

        if(text.getUnicode(0) != '#'){
            throw new RuntimeException("Unvalid format : "+text);
        }

        final int r,g,b,a;
        final int length = text.getCharLength();
        if(length==4){
            r = Int32.decodeHexa(text, 1, 2);
            g = Int32.decodeHexa(text, 2, 3);
            b = Int32.decodeHexa(text, 3, 4);
            a = 255;
        }else if(length==5){
            r = Int32.decodeHexa(text, 1, 2);
            g = Int32.decodeHexa(text, 2, 3);
            b = Int32.decodeHexa(text, 3, 4);
            a = Int32.decodeHexa(text, 4, 5);
        }else if(length==7){
            r = Int32.decodeHexa(text, 1, 3);
            g = Int32.decodeHexa(text, 3, 5);
            b = Int32.decodeHexa(text, 5, 7);
            a = 255;
        }else if(length==9){
            r = Int32.decodeHexa(text, 1, 3);
            g = Int32.decodeHexa(text, 3, 5);
            b = Int32.decodeHexa(text, 5, 7);
            a = Int32.decodeHexa(text, 7, 9);
        }else{
            throw new RuntimeException("Unvalid format : "+text);
        }

        return new Color(r, g, b, a);
    }

    public static Chars toHexa(Color color){
        final int ir = (int) (color.getRed() * 255);
        final int ig = (int) (color.getGreen()* 255);
        final int ib = (int) (color.getBlue() * 255);
        final int ia = (int) (color.getAlpha() * 255);

        final CharBuffer cb = new CharBuffer();
        cb.append('#');
        appendHexa(cb, ir);
        appendHexa(cb, ig);
        appendHexa(cb, ib);
        appendHexa(cb, ia);
        return cb.toChars();
    }

    private static void appendHexa(final CharBuffer cb,final int value){
        final Chars c = Int32.encodeHexa(value);
        if(c.getCharLength()==0){
            cb.append('0');
            cb.append('0');
        }else if(c.getCharLength()==1){
            cb.append('0');
            cb.append(c);
        }else{
            cb.append(c);
        }
    }

}