
package science.unlicense.api.color.colorspace;

import science.unlicense.api.character.Chars;
import science.unlicense.api.math.transform.Transform;

/**
 * Defines a 'range' of colors.
 * has fallback conversions methods to sRGB color space.
 *
 * https://en.wikipedia.org/wiki/Color_science
 * https://en.wikipedia.org/wiki/Color_space
 * https://en.wikipedia.org/wiki/Absolute_color_space
 * https://en.wikipedia.org/wiki/CIE_1931_color_space
 * https://en.wikipedia.org/wiki/SRGB
 * https://en.wikipedia.org/wiki/Lab_color_space
 * https://en.wikipedia.org/wiki/YCbCr
 *
 * @author Johann Sorel
 */
public interface ColorSpace {

    /**
     * Color space name.
     *
     * @return Chars, never null
     */
    Chars getName();

    /**
     * Get number of samples requiered by this colorspace.
     * @return int
     */
    int getDimension();

    /**
     * Get colorspace components.
     * @return Arrayn never null
     */
    ColorSpaceComponent[] getComponents();
    
    /**
     * Convert a single color from RGBA to space.
     *
     * @param rgba size = 4 (RGBA)
     * @return size = dimension
     */
    float[] toSpace(float[] rgba, float[] buffer);

    /**
     * Convert a single color from RGBA to space.
     *
     * @param rgba size = 4 (RGBA)
     * @param precision number of significant bits, common values are 8,12,16
     * @param buffer
     * @return size = dimension
     */
    int[] toSpace(int[] rgba, int[] buffer, int precision);

    /**
     * Convert a single color from space to RGBA.
     *
     * @param values size = dimension
     * @return size = 4 (RGBA)
     */
    float[] toRGBA(float[] values, float[] buffer);
    
    /**
     * Convert a single color from space to RGBA.
     *
     * @param values size = dimension
     * @param buffer
     * @param precision number of significant bits, common values are 8,12,16
     * @return size = 4 (RGBA)
     */
    int[] toRGBA(int[] values, int[] buffer, int precision);
    
    /**
     * Convert from this color space to ARGB.
     *
     * @param source
     * @param sourceOffset
     * @param dest
     * @param destOffset
     * @param nbTuple
     */
    void toRGBA(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple);

    /**
     * Convert from this color space to ARGB.
     *
     * @param source
     * @param sourceOffset
     * @param dest
     * @param destOffset
     * @param nbTuple
     */
    void toRGBA(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple);

    /**
     * Convert from this color space to ARGB.
     *
     * @param source
     * @param sourceOffset
     * @param dest
     * @param destOffset
     * @param nbTuple
     */
    void toRGBA(int[] source, int sourceOffset, int[] dest, int destOffset, int nbTuple, int precision);
    
    /**
     * Convert from ARGB to this color space.
     *
     * @param source
     * @param sourceOffset
     * @param dest
     * @param destOffset
     * @param nbTuple
     */
    void toSpace(double[] source, int sourceOffset, double[] dest, int destOffset, int nbTuple);

    /**
     * Convert from ARGB to this color space.
     *
     * @param source
     * @param sourceOffset
     * @param dest
     * @param destOffset
     * @param nbTuple
     */
    void toSpace(float[] source, int sourceOffset, float[] dest, int destOffset, int nbTuple);

    /**
     * Convert from ARGB to this color space.
     *
     * @param source
     * @param sourceOffset
     * @param dest
     * @param destOffset
     * @param nbTuple
     */
    void toSpace(int[] source, int sourceOffset, int[] dest, int destOffset, int nbTuple, int precision);
    
    /**
     * Create transform from this color space to given one.
     * In the worst case a fallback transformation using sRGB as pivot colorspace
     * will be used. (FallBackCsTransformation)
     *
     * @param cs
     * @return Transformation never null.
     */
    Transform getTranform(ColorSpace cs);

}
