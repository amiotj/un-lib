
package science.unlicense.api.image.sample;

import science.unlicense.api.geometry.AbstractTupleBuffer;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;

/**
 * Usual two dimension images mostly share a common image structure once
 * uncompressed. This class should suffice for image with planar samples.
 *
 * ImageBank is expected to be ordered sample blocks next to another, and line by line within each block :
 * Samples 1
 * line 0 : [s1][s1][s1][s1][s1][s1] ...
 * line 1 : [s1][s1][s1][s1][s1][s1] ...
 * ...
 * Samples 2
 * line 0 : [s2][s2][s2][s2][s2][s2] ...
 * line 1 : [s2][s2][s2][s2][s2][s2] ...
 * ...
 *
 * @author Johann Sorel
 */
public class PlanarRawModel2D extends AbstractRawModel{

    private final int sampleType;
    private final int nbSample;
    
    public PlanarRawModel2D(int sampleType, int nbSample) {
        this.sampleType = sampleType;
        this.nbSample = nbSample;
    }

    public int getPrimitiveType(){
        return sampleType;
    }

    public int getSampleCount() {
        return nbSample;
    }

    public Buffer createBuffer(Extent.Long dimensions,BufferFactory factory) {
        return AbstractTupleBuffer.createPrimitiveBuffer(dimensions, sampleType, nbSample,factory);
    }

    public TupleBuffer create(Extent.Long dimensions) {
        return new RawTupleBuffer(createBuffer(dimensions),dimensions,sampleType, nbSample);
    }
    
    @Override
    public TupleBuffer asTupleBuffer(Image image) {
        return new RawTupleBuffer(image.getDataBuffer(),image.getExtent(),sampleType,nbSample);
    }


    private static class RawTupleBuffer extends AbstractTupleBuffer{

        private final Buffer bank;
        private final int imageHeight;
        private final int imageWidth;

        private RawTupleBuffer(Buffer bank, Extent.Long extent, int sampleType, int nbSample){
            super(extent,sampleType,nbSample);
            this.imageWidth = (int) extent.getL(0);
            this.imageHeight = (int) extent.getL(1);
            this.bank = bank;
        }

        public TupleBuffer copy(Buffer bank) {
            return new RawTupleBuffer(bank, dimensions, sampleType, nbSample);
        }

        public Buffer getPrimitiveBuffer() {
            return bank;
        }

        public TupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
            return new RawTupleBuffer(createPrimitiveBuffer(dimensions,factory), dimensions,
                    sampleType, nbSample);
        }

        private void checkCoordinate(int[] coordinate, int sampleIndex){
            if(sampleIndex>=nbSample){
                throw new RuntimeException("Sample "+sampleIndex +" do not exist.");
            }
            if(coordinate.length != 2){
                throw new RuntimeException("Invalid coordinate dimension , expected 2 but was" +coordinate.length);
            }
            if(coordinate[0] >= imageWidth || coordinate[1] >= imageHeight){
                throw new RuntimeException("Invalid coordinate, out of image range");
            }
        }

        private int getByteOffsetInByte(int[] coordinate, int sampleIndex){
            checkCoordinate(coordinate, sampleIndex);

            final int bytePerSample = bitsPerSample/8;
            return sampleIndex*(imageWidth*imageHeight*bytePerSample) //block offset
                 + coordinate[1]*imageWidth*bytePerSample + coordinate[0]*bytePerSample; //sample offset
        }

        private int getByteOffsetInBits(int[] coordinate, int sampleIndex){
            checkCoordinate(coordinate, sampleIndex);

            return sampleIndex*(imageWidth*imageHeight*bitsPerSample) //block offset
                 + coordinate[1]*imageWidth*bitsPerSample + coordinate[0]*bitsPerSample; //sample offset
        }

        @Override
        public boolean[] getTupleBoolean(int[] coordinate, boolean[] buffer) {
            if(sampleType == TYPE_1_BIT){
                if(buffer == null) buffer = new boolean[nbSample];
                for(int i=0;i<nbSample;i++){
                    final int start = getByteOffsetInBits(coordinate,i);
                    final int byteOffset = start/8;
                    final int bitoffset = start%8;
                    final byte b = bank.readByte(byteOffset);
                    buffer[i] = (b & (1 << (7-bitoffset))) > 0;
                }
                return buffer;
            }
            return super.getTupleBoolean(coordinate, buffer);
        }

        @Override
        public byte[] getTupleByte(int[] coordinate, byte[] buffer) {
            if(sampleType == TYPE_BYTE){
                if(buffer == null) buffer = new byte[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readByte(getByteOffsetInByte(coordinate, i));
                return buffer;
            }else if(sampleType == TYPE_2_BIT){
                throw new RuntimeException("Not implemented yet");
            }else if(sampleType == TYPE_4_BIT){
                throw new RuntimeException("Not implemented yet");
            }
            return super.getTupleByte(coordinate, buffer);
        }

        @Override
        public int[] getTupleUByte(int[] coordinate, int[] buffer) {
            if(sampleType == TYPE_UBYTE){
                if(buffer == null) buffer = new int[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = (short)bank.readUByte(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleUByte(coordinate, buffer);
        }

        @Override
        public short[] getTupleShort(int[] coordinate, short[] buffer) {
            if(sampleType == TYPE_SHORT){
                if(buffer == null) buffer = new short[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readShort(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleShort(coordinate, buffer);
        }

        @Override
        public int[] getTupleUShort(int[] coordinate, int[] buffer) {
            if(sampleType == TYPE_USHORT){
                if(buffer == null) buffer = new int[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readUShort(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleUShort(coordinate, buffer);
        }

        @Override
        public int[] getTupleInt(int[] coordinate, int[] buffer) {
            if(sampleType == TYPE_INT){
                if(buffer == null) buffer = new int[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readInt(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleInt(coordinate, buffer);
        }

        @Override
        public long[] getTupleUInt(int[] coordinate, long[] buffer) {
            if(sampleType == TYPE_UINT){
                if(buffer == null) buffer = new long[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readUInt(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleUInt(coordinate, buffer);
        }

        @Override
        public long[] getTupleLong(int[] coordinate, long[] buffer) {
            if(sampleType == TYPE_LONG){
                if(buffer == null) buffer = new long[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readLong(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleLong(coordinate, buffer);
        }

        @Override
        public float[] getTupleFloat(int[] coordinate, float[] buffer) {
            if(sampleType == TYPE_FLOAT){
                if(buffer == null) buffer = new float[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readFloat(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleFloat(coordinate, buffer);
        }

        @Override
        public double[] getTupleDouble(int[] coordinate, double[] buffer) {
            if(sampleType == TYPE_DOUBLE){
                if(buffer == null) buffer = new double[nbSample];
                for(int i=0;i<nbSample;i++)buffer[i] = bank.readDouble(getByteOffsetInByte(coordinate, i));
                return buffer;
            }
            return super.getTupleDouble(coordinate, buffer);
        }

        @Override
        public void setTuple(int[] coordinate, Object sample) {
            if(sampleType == TYPE_BYTE){
                final byte[] buffer = (byte[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeByte(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_UBYTE){
                final short[] buffer = (short[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeUByte(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_SHORT){
                final short[] buffer = (short[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeShort(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_USHORT){
                final int[] buffer = (int[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeUShort(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_INT){
                final int[] buffer = (int[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeInt(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_UINT){
                final long[] buffer = (long[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeUInt(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_LONG){
                final long[] buffer = (long[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeLong(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_FLOAT){
                final float[] buffer = (float[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeFloat(buffer[i], getByteOffsetInByte(coordinate, i));
            }else if(sampleType == TYPE_DOUBLE){
                final double[] buffer = (double[]) sample;
                for(int i=0;i<nbSample;i++) bank.writeDouble(buffer[i], getByteOffsetInByte(coordinate, i));
            }else{
                throw new RuntimeException("Unexpected type "+ sampleType);
            }
        }

    }
    
}
    
