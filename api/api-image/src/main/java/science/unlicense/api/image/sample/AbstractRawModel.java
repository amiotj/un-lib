
package science.unlicense.api.image.sample;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.geometry.Extent;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractRawModel implements RawModel{

    public Buffer createBuffer(Extent.Long dimensions) {
        return createBuffer(dimensions, DefaultBufferFactory.INSTANCE);
    }
    
}
