
package science.unlicense.api.image.process;

/**
 * An Evaluator access image pixels and may alterate the values using extrapolation
 * or interpolation.
 * 
 * @author Johann Sorel
 */
public interface Sampler {
    
    /**
     * Sampling are often stacked.
     * For example : image + extrapolator + interpolator
     * 
     * @return parent sampling, can be null
     */
    Sampler getParent();
    
    /**
     * Evaluate value at given exact pixel coordinate.
     * 
     * @param coordinate : coordinate of the pixel
     * @param buffer : to store each sample value
     */
    void evaluate(int[] coordinate, double[] buffer);
    
    /**
     * Evaluate value at given coordinate.
     * 
     * @param coordinate : coordinate of the pixel 
     * @param buffer : to store each sample value
     */
    void evaluate(double[] coordinate, double[] buffer);
}
