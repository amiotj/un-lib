
package science.unlicense.api.image.process;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.model.doc.Document;

/**
 * Image operator where output color can be calculated with the input color.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractColorPointOperator extends AbstractPreservingOperator{

    public AbstractColorPointOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    /// when iterating
    private Object inSamples;
    private PixelBuffer inputBuffer;

    public Document execute() {
        super.execute();
        prepareInputs();

        //loop on all pixels
        final TupleIterator ite = inputTuples.createIterator(null);
        for(inSamples = ite.next(); inSamples!=null; inSamples=ite.next()){
            final int[] coord = ite.getCoordinate();
            Color color = inputColorModel.toColor(inSamples);
            color = evaluate(color);

            //fill out image pixel
            outputTuples.setTuple(coord, inputColorModel.toSample(color));
        }

        return outputParameters;
    }

    protected void prepareInputs(){
    }

    protected abstract Color evaluate(Color color);
    
}
