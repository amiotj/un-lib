package science.unlicense.api.image.sample;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.image.ImageModel;

/**
 *
 * @author Johann Sorel
 */
public interface RawModel extends ImageModel {

    public static final int TYPE_UNKNOWNED = Primitive.TYPE_UNKNOWNED;
    public static final int TYPE_1_BIT = Primitive.TYPE_1_BIT;
    public static final int TYPE_2_BIT = Primitive.TYPE_2_BIT;
    public static final int TYPE_4_BIT = Primitive.TYPE_4_BIT;
    public static final int TYPE_BYTE = Primitive.TYPE_BYTE;
    public static final int TYPE_UBYTE = Primitive.TYPE_UBYTE;
    public static final int TYPE_SHORT = Primitive.TYPE_SHORT;
    public static final int TYPE_USHORT = Primitive.TYPE_USHORT;
    public static final int TYPE_INT = Primitive.TYPE_INT;
    public static final int TYPE_UINT = Primitive.TYPE_UINT;
    public static final int TYPE_LONG = Primitive.TYPE_LONG;
    public static final int TYPE_ULONG = Primitive.TYPE_ULONG;
    public static final int TYPE_FLOAT = Primitive.TYPE_FLOAT;
    public static final int TYPE_DOUBLE = Primitive.TYPE_DOUBLE;

    /**
     * @return number of sample values in the tuples.
     */
    int getSampleCount();

    /**
     * @return type of samples, see Primitive.TYPE_X
     */
    int getPrimitiveType();
    
    /**
     * Create a primitive buffer.
     * 
     * @param dimensions
     * @return 
     */
    Buffer createBuffer(Extent.Long dimensions);
    
    /**
     * Create a primitive buffer.
     * 
     * @param dimensions
     * @param factory
     * @return 
     */
    Buffer createBuffer(Extent.Long dimensions, BufferFactory factory);
    
}
