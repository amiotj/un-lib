
package science.unlicense.api.image.process;

import science.unlicense.api.image.Image;

/**
 * Extrapolator are used in image operator to extrapolate pixel values
 * outside of the image area. This allows to reduce artifacts in image operators
 * like convolution.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public interface Extrapolator extends Sampler {

    Sampler getParent();
    
    void setParentSampling(Image image);
    
}
