
package science.unlicense.api.image.process;

import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.model.doc.Document;

/**
 * Image operator where output sample value can be calculated with
 * only the input sample value.
 *
 * Override evaluate(double[] sample, double[] result) to work by pixel
 * Override evaluate(double sample) to work by sample
 * 
 * @author Florent Humbert
 * @author Johann Sorel
 */
public abstract class AbstractPairPointOperator extends AbstractPairPreservingOperator{

    public AbstractPairPointOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    /// when iterating
    private double[] inSamples1;
    private double[] inSamples2;
    private double[] inSamplesProcessed;
    private Object outSamples;

    public Document execute() {
        super.execute();
        prepareInputs();

        //loop on all pixels
        int k;
        inSamples1 = new double[inputTuples1.getSampleCount()];
        inSamples2 = new double[inputTuples2.getSampleCount()];
        inSamplesProcessed = new double[outNbSample];
        outSamples = outputTuples.createTupleStore();
        final TupleIterator ite = inputTuples1.createIterator(null);
        for(inSamples1 = ite.nextAsDouble(inSamples1); inSamples1!=null; inSamples1=ite.nextAsDouble(inSamples1)){
            final int[] coord = ite.getCoordinate();
            
            inputTuples2.getTupleDouble(coord, inSamples2);
            evaluate(inSamples1, inSamples2, inSamplesProcessed);

            //clip values to image sample type
            adjustInOutSamples(inSamplesProcessed,outSamples);

            //fill out image pixel
            outputTuples.setTuple(coord, outSamples);
        }

        return outputParameters;
    }

    protected void prepareInputs(){}

    protected void evaluate(double[] tuple1, double[] tuple2, double[] result){
        if(result.length==tuple1.length && result.length == tuple2.length){
            for(int k=0;k<result.length;k++){
                result[k] = evaluate(tuple1[k], tuple2[k]);
            }
        }
    }
    
    protected abstract double evaluate(double value1, double value2);

}
