
package science.unlicense.api.image.process;

import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.model.doc.Document;

/**
 * Image operator where output sample value can be calculated with
 * a given range around the pixel.
 *
 * @author Johann Sorel
 * @author Florent Humbert
 */
public abstract class AbstractAreaOperator extends AbstractPreservingOperator{

    private static final Extrapolator ZERO_EXTENDER = new ExtrapolatorConstant(0);

    /// when iterating
    private double[] inSamples;
    private Object outSamples;

    public AbstractAreaOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    protected abstract int getTopPadding();

    protected abstract int getBottomPadding();

    protected abstract int getLeftPadding();

    protected abstract int getRightPadding();

    public Document execute() {
        super.execute();
        prepareInputs();

        Extrapolator extrapolator = (Extrapolator) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_EXTENDER.getId());
        if(extrapolator==null) extrapolator = ZERO_EXTENDER;
        extrapolator.setParentSampling(inputImage);
        
        //prepare the work space
        final int width     = (int) extent.get(0);
        final int height    = (int) extent.get(1);
        final int toppad    = getTopPadding();
        final int bottompad = getBottomPadding();
        final int leftpad   = getLeftPadding();
        final int rightpad  = getRightPadding();
        final int wsHeight  = toppad+bottompad+1;
        final int wsWidth   = leftpad+rightpad+1;
        final double[][][] workspaces = new double[toppad+bottompad+1][leftpad+rightpad+1][nbSample];

        //cache the current x, this way we know when we move only a single pixel aside
        //we can avoid getting the all matrix values, only grab the last column
        int lastX = -10;

        //loop on all pixels
        int x,y,k,maxx,maxy;
        final int[] xy = new int[2];
        inSamples = new double[nbSample];
        outSamples = inputTuples.createTupleStore();
        final TupleIterator ite = inputTuples.createIterator(null);
        for(inSamples = ite.nextAsDouble(inSamples); inSamples!=null; inSamples=ite.nextAsDouble(inSamples)){
            final int[] coord = ite.getCoordinate();
            maxx = coord[0] + rightpad+1;
            maxy = coord[1] + bottompad+1;
            if(coord[0]==lastX+1){
                //moved one pixel on x, we can grab only the last column values
                for(x=1;x<wsWidth;x++){
                    for(y=0;y<wsHeight;y++){
                        for(k=0;k<nbSample;k++){
                            workspaces[y][x-1][k] = workspaces[y][x][k];
                        }
                    }
                }
                //grave the last column values
                xy[0] = maxx-1;
                x=wsWidth-1;
                y=0;
                for(xy[1]=-toppad+coord[1];xy[1]<maxy;xy[1]++,y++){
                    //check if we are outside image
                    if(xy[0]<0 || xy[0]>=width || xy[1]<0 || xy[1]>=height){
                        extrapolator.evaluate(coord, workspaces[y][x]);
                    }else{
                        inputTuples.getTupleDouble(xy, workspaces[y][x]);
                    }
                }

            }else{
                //fill matrices
                x=0;
                for(xy[0]=-leftpad+coord[0];xy[0]<maxx;xy[0]++,x++){
                    y=0;
                    for(xy[1]=-toppad+coord[1];xy[1]<maxy;xy[1]++,y++){
                        //check if we are outside image
                        if(xy[0]<0 || xy[0]>=width || xy[1]<0 || xy[1]>=height){
                            extrapolator.evaluate(coord, workspaces[y][x]);
                        }else{
                            inputTuples.getTupleDouble(xy, workspaces[y][x]);
                        }
                    }
                }
            }
            lastX = coord[0];

            evaluate(workspaces,inSamples);

            //clip values to image sample type
            adjustInOutSamples(inSamples,outSamples);

            //fill out image pixel
            outputTuples.setTuple(coord, outSamples);
        }

        return outputParameters;
    };

    protected void prepareInputs(){}

    //TODO we could give the displacement information here, it might avoid recalculation some cell values
    // would greatly improve oil painting operator for example
    protected abstract void evaluate(double[][][] workspace, double[] result);

}
