
package science.unlicense.api.image;

import science.unlicense.api.character.Chars;

/**
 * An image pointer reference a single image in an ImageStore.
 * 
 * @author Johann Sorel
 */
public interface ImagePointer {
    
    /**
     * Image unique id in the store.
     * @return Chars, never null
     */
    Chars getId();
    
    /**
     * @return new Image Reader, can be null if reading is not supported
     */
    ImageReader createReader();

    /**
     * @return new Image Writer, can be null if writing is not supported
     */
    ImageWriter createWriter();
    
    
}
