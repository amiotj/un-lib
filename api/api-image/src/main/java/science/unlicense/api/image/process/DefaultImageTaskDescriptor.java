
package science.unlicense.api.image.process;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.task.Task;

/**
 *
 * @author Johann Sorel
 */
public class DefaultImageTaskDescriptor extends AbstractImageTaskDescriptor{

    private final Class operatorClass;
    
    public DefaultImageTaskDescriptor(Chars id, CharArray name, CharArray description, 
            Class operatorClass, DocumentType inputType, DocumentType outputType) {
        super(id, name, description, inputType, outputType);
        this.operatorClass = operatorClass;
    }
    
    public DefaultImageTaskDescriptor(Chars id, CharArray name, CharArray description, 
            Class operatorClass, FieldType[] inputs, FieldType[] outputs) {
        super(id, name, description, inputs, outputs);
        this.operatorClass = operatorClass;
    }

    public Task create() {
        try {
            return (Task) operatorClass.newInstance();
        } catch (InstantiationException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }
    
}
