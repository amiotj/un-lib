
package science.unlicense.api.image;

/**
 * Describe the parameters given to an image writer.
 * Each writer implementation may extend and add specific parameters.
 * 
 * @author Johann Sorel
 */
public interface ImageWriteParameters {
        
}
