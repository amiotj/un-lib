

package science.unlicense.api.image;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.Store;

/**
 * The image reader/writer model is inappropriate for more rich images format
 * like grib, netcdf, jpeg-2000, dicom. there are to much ambiguity in how to read or
 * write only parts of multidimensional images, acess overviews or add slices.
 * 
 * A store approach will provide a better control of the image store content and
 * metadatas.
 *  
 * @author Johann Sorel
 */
public interface ImageStore extends Store {

    /**
     * Get the original image format definition.
     * 
     * @return Format, never null
     */
    ImageFormat getFormat();
    
    /**
     * @return true if reading is supported.
     */
    boolean supportReading();

    /**
     * @return true if writing is supported.
     */
    boolean supportWriting();
    
    /**
     * List all images ids.
     * 
     * @return ids, never null, can be empty
     */
    Chars[] getImageIds();
    
    /**
     * Get image pointer for given id.
     * 
     * @param id image id
     * @return ImagePointer, never null
     * @throws science.unlicense.api.io.IOException if ID doesn't exist
     */
    ImagePointer getImage(Chars id) throws IOException;
    
    /**
     * Delete an image in the store.
     * 
     * @param id image id
     */
    void deleteImage(Chars id) throws IOException;
    
    /**
     * Create a new image in this store.
     * The created image might not exist in the input until a write operation has been made.
     * 
     * @param preferedId can be null, one will be automaticaly created
     * @return new but empty image pointer
     * @throws science.unlicense.api.io.IOException  if id is 
     */
    ImagePointer createImage(Chars preferedId) throws IOException;
    
} 
