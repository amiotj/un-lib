
package science.unlicense.api.image;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.TupleBuffer;

/**
 * Images are N dimension raw byte buffers.
 * ImageModel produce views of the image for a given purpose.
 * They add meaning to the image by providing an interpretation of the byte buffer
 * content as tuples with units.
 * additionally they may transform the buffer values or ignore some parts of the byte buffer.
 *
 * There are typically 3 types of image models :
 * - Raw model : access to the datas with no transformation
 * - Color model : interpretation of the datas as colors
 * - Physic model : interpretation of the datas as their real values
 *
 * The Raw model is mandatory.
 * Image may not necessarily have a color or physic model.
 * Other types of image models may be available.
 *
 * @author Johann Sorel
 */
public interface ImageModel {

    public static final Chars MODEL_RAW = new Chars("RAW");
    public static final Chars MODEL_COLOR = new Chars("COLOR");
    public static final Chars MODEL_PHYSIC = new Chars("PHYSIC");

    TupleBuffer asTupleBuffer(Image image);

}
