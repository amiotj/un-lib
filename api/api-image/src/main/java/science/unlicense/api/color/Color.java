
package science.unlicense.api.color;

import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 * Color object, composed of separate red, green, blue and alpha components.
 *
 * @author Johann Sorel
 */
public final class Color extends CObject {

    public static final Color WHITE         = new Color(1f, 1f, 1f, 1f);
    public static final Color GRAY_LIGHT    = new Color(0.75f, 0.75f, 0.75f, 1f);
    public static final Color GRAY_NORMAL   = new Color(0.5f, 0.5f, 0.5f, 1f);
    public static final Color GRAY_DARK     = new Color(0.25f, 0.25f, 0.25f, 1f);
    public static final Color BLACK         = new Color(0f, 0f, 0f, 1f);
    public static final Color RED           = new Color(1f, 0f, 0f, 1f);
    public static final Color GREEN         = new Color(0f, 1f, 0f, 1f);
    public static final Color BLUE          = new Color(0f, 0f, 1f, 1f);
    public static final Color TRS_WHITE     = new Color(1f, 1f, 1f, 0f);
    public static final Color TRS_BLACK     = new Color(0f, 0f, 0f, 0f);

    //color samples in colorspace
    private final float[] samples;
    private final ColorSpace colorSpace;

    //cache RBB values for the color, whatever it's colorspace
    //non premultiplied colors
    private final float red;
    private final float green;
    private final float blue;
    //premultiplied colors value.
    private final float redMul;
    private final float greenMul;
    private final float blueMul;

    private final float alpha;

    /**
     * Create color from int value.
     * Expect alpha not to be premultiplied.
     *
     * @param argb
     */
    public Color(int argb) {
        this(argb,false);
    }

    /**
     * Create color from int value.
     *
     * @param argb color
     * @param preMul if alpha is premultiplied
     */
    public Color(int argb, boolean preMul) {
        this(((argb >>> 16) & 0xFF)/255f,
             ((argb >>>  8) & 0xFF)/255f,
             ((argb >>>  0) & 0xFF)/255f,
             ((argb >>> 24) & 0xFF)/255f,
             preMul);
    }

    /**
     * Create color from R,G,B components.
     *
     * @param red compoent [0...255]
     * @param green compoent [0...255]
     * @param blue compoent [0...255]
     */
    public Color(int red, int green, int blue) {
        this(red/255f,green/255f,blue/255f,1f,false);
    }

    /**
     * Create color from R,G,B,A components.
     * Expect alpha not to be premultiplied.
     *
     * @param red compoent [0...255]
     * @param green compoent [0...255]
     * @param blue compoent [0...255]
     * @param alpha compoent [0...255]
     */
    public Color(int red, int green, int blue, int alpha) {
        this(red/255f,green/255f,blue/255f,alpha/255f,false);
    }

    /**
     * Create color from R,G,B,A components.
     *
     * @param red compoent [0...255]
     * @param green compoent [0...255]
     * @param blue compoent [0...255]
     * @param alpha compoent [0...255]
     * @param preMul if alpha is premultiplied
     */
    public Color(int red, int green, int blue, int alpha, boolean preMul) {
        this(red/255f,green/255f,blue/255f,alpha/255f,preMul);
    }

    /**
     * Create color from R,G,B components.
     *
     * @param red compoent [0...1]
     * @param green compoent [0...1]
     * @param blue compoent [0...1]
     */
    public Color(float red, float green, float blue) {
        this(red,green,blue,1f,false);
    }

    /**
     * Create color from R,G,B,A components.
     * Expect alpha not to be premultiplied.
     *
     * @param red compoent [0...1]
     * @param green compoent [0...1]
     * @param blue compoent [0...1]
     * @param alpha compoent [0...1]
     */
    public Color(float red, float green, float blue, float alpha) {
        this(red,green,blue,alpha,false);
    }

    /**
     * Create color from float array, [R,G,B,A?]
     * Aplha is optional
     * Expect alpha not to be premultiplied.
     *
     * @param rgba 3 or 4 dimension array
     */
    public Color(float[] rgba) {
        this(rgba[0],rgba[1],rgba[2],(rgba.length>3) ? rgba[3] : 1f, false);
    }

    /**
     * Create color from float array, [R,G,B,A]
     *
     * @param rgba 4 dimension array
     * @param preMul if alpha is premultiplied
     */
    public Color(float[] rgba, boolean preMul) {
        this(rgba[0],rgba[1],rgba[2],rgba[3], preMul);
    }

    /**
     * Create color from R,G,B,A components.
     *
     * @param red compoent [0...1]
     * @param green compoent [0...1]
     * @param blue compoent [0...1]
     * @param alpha compoent [0...1]
     * @param preMul if alpha is premultiplied
     */
    public Color(float red, float green, float blue, float alpha, boolean preMul) {

        if(preMul){
            if(alpha != 0f){
                this.red        = red/alpha;
                this.green      = green/alpha;
                this.blue       = blue/alpha;
            }else{
                //everything to zero
                this.red        = 0f;
                this.green      = 0f;
                this.blue       = 0f;
            }
            this.alpha      = alpha;
            this.redMul     = red;
            this.greenMul   = green;
            this.blueMul    = blue;
        }else{
            this.red        = red;
            this.green      = green;
            this.blue       = blue;
            this.alpha      = alpha;
            this.redMul     = this.red * alpha;
            this.greenMul   = this.green * alpha;
            this.blueMul    = this.blue * alpha;
        }

        this.colorSpace = SRGB.INSTANCE;
        this.samples = new float[]{red,green,blue};
    }

    public Color(float[] samples, float alpha, boolean preMul, ColorSpace cs){
        this.colorSpace = cs;
        this.samples = Arrays.copy(samples);

        float red;
        float green;
        float blue;
        if(cs instanceof SRGB){
            red = samples[0];
            green = samples[0];
            blue = samples[0];
        }else{
            final float[] rgb = new float[3];
            cs.toRGBA(samples, 0, rgb, 0, 1);
            red = rgb[0];
            green = rgb[1];
            blue = rgb[2];
        }

        if(preMul){
            if(alpha != 0f){
                this.red        = red/alpha;
                this.green      = green/alpha;
                this.blue       = blue/alpha;
            }else{
                //everything to zero
                this.red        = 0f;
                this.green      = 0f;
                this.blue       = 0f;
            }
            this.alpha      = alpha;
            this.redMul     = red;
            this.greenMul   = green;
            this.blueMul    = blue;
        }else{
            this.red        = red;
            this.green      = green;
            this.blue       = blue;
            this.alpha      = alpha;
            this.redMul     = this.red * alpha;
            this.greenMul   = this.green * alpha;
            this.blueMul    = this.blue * alpha;
        }
    }

    /**
     * Color color space.
     * @return ColorSpace never null
     */
    public ColorSpace getColorSpace() {
        return colorSpace;
    }

    /**
     * Color samples in color space units.
     * @param buffer if null a new array will be created
     * @return float[]
     */
    public float[] getSamples(float[] buffer){
        if(buffer==null){
            buffer = Arrays.copy(samples);
        }else{
            Arrays.copy(samples, 0, samples.length,buffer,0);
        }
        return buffer;
    }

    /**
     * Get red component.
     * @return red component [0...1]
     */
    public float getRed() {
        return red;
    }

    /**
     * Get green component.
     * @return green component [0...1]
     */
    public float getGreen() {
        return green;
    }

    /**
     * Get blue component.
     * @return blue component [0...1]
     */
    public float getBlue() {
        return blue;
    }

    /**
     * Get alpha component.
     * @return alpha component [0...1]
     */
    public float getAlpha() {
        return alpha;
    }

    /**
     * Get RGB components as float array.
     * Alpha value is ignored.
     *
     * @return float array RGB
     */
    public float[] toRGB(){
        return new float[]{red,green,blue};
    }

    /**
     * Get RGB components as float array.
     * Alpha value is ignored.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    public float[] toRGB(float[] buffer, int offset){
        buffer[offset] = red;
        buffer[offset+1] = green;
        buffer[offset+2] = blue;
        return buffer;
    }

    /**
     * Get RGBA components as float array.
     * Alpha not premultiplied.
     *
     * @return float array RGBA
     */
    public float[] toRGBA(){
        return new float[]{red,green,blue,alpha};
    }

    /**
     * Get RGBA components as float array.
     * Alpha not premultiplied.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    public float[] toRGBA(float[] buffer, int offset){
        buffer[offset] = red;
        buffer[offset+1] = green;
        buffer[offset+2] = blue;
        buffer[offset+3] = alpha;
        return buffer;
    }

    /**
     * Get RGBA components as float array.
     * Alpha premultiplied.
     *
     * @return float array RGBA
     */
    public float[] toRGBAPreMul(){
        return new float[]{redMul,greenMul,blueMul,alpha};
    }

    /**
     * Get RGBA components as float array.
     * Alpha premultiplied.
     *
     * @param buffer to write into, not null
     * @param offset start index where to write color
     * @return float array RGB
     */
    public float[] toRGBAPreMul(float[] buffer, int offset){
        buffer[offset] = redMul;
        buffer[offset+1] = greenMul;
        buffer[offset+2] = blueMul;
        buffer[offset+3] = alpha;
        return buffer;
    }

    /**
     * Encode color on an int, 8 bits per component in order ARGB.
     * Alpha not premultiplied.
     *
     * @return int ARGB
     */
    public int toARGB() {
        return ((int)(alpha*255)) << 24
             | ((int)(red*255))   << 16
             | ((int)(green*255)) <<  8
             | ((int)(blue*255))  <<  0;
    }

    /**
     * Encode color on an int, 8 bits per component in order ARGB.
     * Alpha premultiplied.
     *
     * @return int ARGB
     */
    public int toARGBPreMul() {
        return ((int)(alpha*255))    << 24
             | ((int)(redMul*255))   << 16
             | ((int)(greenMul*255)) <<  8
             | ((int)(blueMul*255))  <<  0;
    }

    /**
     * {@inheritDoc }
     */
    public Chars toChars() {
        return new Chars("["+(int)(red*255)+","+(int)(green*255)+","+(int)(blue*255)+","+(int)(alpha*255)+"]");
    }

    /**
     * {@inheritDoc }
     */
    public int getHash() {
        int hash = 5;
        hash = 31 * hash + Float.floatToIntBits(this.red);
        hash = 31 * hash + Float.floatToIntBits(this.green);
        hash = 31 * hash + Float.floatToIntBits(this.blue);
        hash = 31 * hash + Float.floatToIntBits(this.alpha);
        return hash;
    }

    /**
     * {@inheritDoc }
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Color other = (Color) obj;
        if (Float.floatToIntBits(this.red) != Float.floatToIntBits(other.red)) {
            return false;
        }
        if (Float.floatToIntBits(this.green) != Float.floatToIntBits(other.green)) {
            return false;
        }
        if (Float.floatToIntBits(this.blue) != Float.floatToIntBits(other.blue)) {
            return false;
        }
        if (Float.floatToIntBits(this.alpha) != Float.floatToIntBits(other.alpha)) {
            return false;
        }
        return true;
    }


}
