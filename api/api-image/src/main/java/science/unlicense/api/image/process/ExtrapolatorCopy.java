
package science.unlicense.api.image.process;

/**
 * Extend image filled pixels with closest pixel value.
 *
 * For example if on a one channel image we obtain ;
 * <pre>
 *              a a a b c c c
 *              a a a b c c c
 *   a b c      a a a b c c c
 *   d e f  ===>d d d e f f f
 *   g h i      g g g h i i i
 *              g g g h i i i
 *              g g g h i i i
 * </pre>
 *
 * @author Humbert Florent
 * @author Johann Sorel
 */
public class ExtrapolatorCopy extends AbstractExtrapolator implements Extrapolator{

    public ExtrapolatorCopy() {
    }

    public void evaluateOutside(int[] coordinate, double[] buffer) {

        //we need to restore old values after, better avoid modifying them
        final int oldx = coordinate[0];
        final int oldy = coordinate[1];
        if(oldx<0) coordinate[0] = 0;
        else if(oldx>=dims[0]) coordinate[0] = dims[0]-1;
        if(oldy<0) coordinate[1] = 0;
        else if(oldy>=dims[1]) coordinate[1] = dims[1]-1;

        imageTuples.getTupleDouble(coordinate, buffer);

        //restore old value
        coordinate[0] = oldx;
        coordinate[1] = oldy;
    }

}
