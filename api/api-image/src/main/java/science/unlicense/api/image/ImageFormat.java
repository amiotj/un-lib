
package science.unlicense.api.image;

import science.unlicense.api.store.Format;

/**
 * Define an image format.
 * - ImageReader
 * - ImageWriter
 *
 * @author Johann Sorel
 */
public interface ImageFormat extends Format {

    /**
     * Indicate if this format can support reading.
     * 
     * @return true if this format has reading capabilities.
     */
    boolean supportReading();

    /**
     * Indicate if this format can support writing.
     * 
     * This does not ensure all created ImageStore will support writing.
     * Writing support is also based on the input type and various other conditions.
     * Use the supportWriting method on ImageStore for a more accurate information.
     * 
     * @return true if this format has writing capabilities.
     */
    boolean supportWriting();
    
    /**
     * @return new Image Reader, can be null if reading is not supported
     */
    ImageReader createReader();

    /**
     * @return new Image Writer, can be null if writing is not supported
     */
    ImageWriter createWriter();

}
