
package science.unlicense.api.image.process;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.math.Maths;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.image.sample.RawModel.TYPE_1_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_2_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_4_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_INT;
import static science.unlicense.api.image.sample.RawModel.TYPE_LONG;
import static science.unlicense.api.image.sample.RawModel.TYPE_SHORT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_UINT;
import static science.unlicense.api.image.sample.RawModel.TYPE_USHORT;
import science.unlicense.api.model.doc.Document;

/**
 * Image operator which preserve the image size and models.
 *
 * @author Florent Humbert
 * @author Johann Sorel
 */
public abstract class AbstractPreservingOperator extends AbstractTask{

    public AbstractPreservingOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    protected Extent.Long extent;
    protected Image inputImage;
    protected RawModel inputSampleModel;
    protected TupleBuffer inputTuples;
    protected ColorModel inputColorModel;

    protected Image outputImage;
    protected RawModel outputSampleModel;
    protected TupleBuffer outputTuples;
    protected ColorModel outputColorModel;

    protected int nbSample;
    protected int sampleType;

    public Document execute() {
        inputImage = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        extent = inputImage.getExtent();
        inputSampleModel = inputImage.getRawModel();
        inputTuples = inputSampleModel.asTupleBuffer(inputImage);
        inputColorModel = inputImage.getColorModel();
        nbSample = inputTuples.getSampleCount();
        sampleType = inputTuples.getPrimitiveType();

        //calculate the result image
        final Buffer buffer = inputSampleModel.createBuffer(extent);
        outputColorModel = inputColorModel;
        outputSampleModel = inputSampleModel;
        outputImage = new DefaultImage(buffer, extent, outputSampleModel, outputColorModel);
        outputTuples = outputSampleModel.asTupleBuffer(outputImage);

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }

    protected void adjustInOutSamples(double[] inSamples, Object outSamples){
        switch(sampleType){
            case TYPE_1_BIT :
                for(int i=0;i<nbSample;i++) ((boolean[])outSamples)[i] = inSamples[i] != 0;
                break;
            case TYPE_2_BIT :
            case TYPE_4_BIT :
            case TYPE_BYTE :
                for(int i=0;i<nbSample;i++) ((byte[])outSamples)[i] = (byte)Maths.clamp(inSamples[i],-128,127);
                break;
            case TYPE_UBYTE :
                for(int i=0;i<nbSample;i++) ((int[])outSamples)[i] = (int)Maths.clamp(inSamples[i],0,255);
                break;
            case TYPE_SHORT :
                for(int i=0;i<nbSample;i++) ((short[])outSamples)[i] = (short)Maths.clamp(inSamples[i],Short.MIN_VALUE,Short.MAX_VALUE);
                break;
            case TYPE_USHORT :
                for(int i=0;i<nbSample;i++) ((int[])outSamples)[i] = (int)Maths.clamp(inSamples[i],0,65535);
                break;
            case TYPE_INT :
                for(int i=0;i<nbSample;i++) ((int[])outSamples)[i] = (int)inSamples[i];
                break;
            case TYPE_UINT :
                for(int i=0;i<nbSample;i++) ((long[])outSamples)[i] = (long)inSamples[i];
                break;
            case TYPE_LONG :
                for(int i=0;i<nbSample;i++) ((long[])outSamples)[i] = (long)inSamples[i];
                break;
            case TYPE_FLOAT :
                for(int i=0;i<nbSample;i++) ((float[])outSamples)[i] = (float)inSamples[i];
                break;
            case TYPE_DOUBLE :
                outSamples = inSamples;
                break;
            default: throw new UnimplementedException("Unusual data type.");
        }
    }

}
