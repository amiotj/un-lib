
package science.unlicense.api.image.color;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import static science.unlicense.api.image.sample.RawModel.TYPE_1_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_2_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_4_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_INT;
import static science.unlicense.api.image.sample.RawModel.TYPE_LONG;
import static science.unlicense.api.image.sample.RawModel.TYPE_SHORT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_UINT;
import static science.unlicense.api.image.sample.RawModel.TYPE_USHORT;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 * Interpolated color model.
 * Interpolate colors from a palette similar to a linear gradiant.
 *
 * @author Johann Sorel
 */
public class InterpolatedColorModel extends AbstractColorModel{

    private final int sampleType;
    private final Color[] palette;
    private final float[] steps;

    public InterpolatedColorModel(int sampleType, Color[] palette, float[] steps) {
        super(SRGB.INSTANCE);
        this.sampleType = sampleType;
        this.palette = palette;
        this.steps = steps;
    }

    /**
     * Colors palette.
     * @return array never null, copy of the palette.
     */
    public Color[] getPalette() {
        return Arrays.copy(palette, new Color[palette.length]);
    }

    /**
     * Colors steps.
     * @return array never null, copy of the steps.
     */
    public float[] getSteps() {
        return Arrays.copy(steps, new float[steps.length]);
    }

    public Color toColor(Object sample) {

        float d;
        if(   TYPE_1_BIT == sampleType || TYPE_2_BIT == sampleType
           || TYPE_4_BIT == sampleType || TYPE_BYTE == sampleType){
            final byte[] samples = (byte[]) sample;
            d = samples[0] & 0xFF;
        }else if(TYPE_UBYTE == sampleType || TYPE_SHORT == sampleType){
            final short[] samples = (short[]) sample;
            d = samples[0] & 0xFF;
        }else if(TYPE_USHORT == sampleType ||TYPE_INT == sampleType){
            final int[] samples = (int[]) sample;
            d = samples[0];
        }else if(TYPE_UINT == sampleType ||TYPE_LONG == sampleType){
            final long[] samples = (long[]) sample;
            d = samples[0];
        }else if(TYPE_FLOAT == sampleType){
            final float[] samples = (float[]) sample;
            d = samples[0];
        }else if(TYPE_DOUBLE == sampleType){
            final double[] samples = (double[]) sample;
            d = (float)samples[0];
        }else{
            throw new RuntimeException("Unsupported sample type "+sampleType);
        }

        //find interval
         //avoid under/above out of range
        d = Maths.clamp(d, steps[0], steps[steps.length-1]);
        int index=0;
        for(;index<steps.length-1;index++){
            if(d>= steps[index] && d<=steps[index+1]){
                break;
            }
        }
        d = (d-steps[index]) / (steps[index+1]-steps[index]);

        return Colors.interpolate(palette[index], palette[index+1], (float)d);
    }

    public int toColorARGB(Object sample) {
        return toColor(sample).toARGB();
    }

    public Object toSample(Color color) {
        throw new RuntimeException("TODO");
    }

    public Object toSample(int color) {
        return toSample(new Color(color));
    }
    
}
