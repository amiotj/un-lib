
package science.unlicense.api.image.process;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.math.Maths;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.image.sample.RawModel.TYPE_1_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_2_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_4_BIT;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_INT;
import static science.unlicense.api.image.sample.RawModel.TYPE_LONG;
import static science.unlicense.api.image.sample.RawModel.TYPE_SHORT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_UINT;
import static science.unlicense.api.image.sample.RawModel.TYPE_USHORT;
import science.unlicense.api.model.doc.Document;

/**
 * Image operator which preserve the image size and models.
 * 
 * This ensure both input images have the same dimensions.
 *
 * @author Johann Sorel
 */
public abstract class AbstractPairPreservingOperator extends AbstractTask{

    public AbstractPairPreservingOperator(AbstractImageTaskDescriptor descriptor) {
        super(descriptor);
    }

    protected Extent.Long extent;
    protected Image inputImage1;
    protected RawModel inputRawModel1;
    protected TupleBuffer inputTuples1;
    protected ColorModel inputColorModel1;

    protected Image inputImage2;
    protected RawModel inputRawModel2;
    protected TupleBuffer inputTuples2;
    protected ColorModel inputColorModel2;
    
    protected Image outputImage;
    protected RawModel outputRawModel;
    protected TupleBuffer outputTuples;
    protected ColorModel outputColorModel;

    protected int outNbSample;
    protected int outSampleType;

    public Document execute() {
        inputImage1 = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId());
        extent = inputImage1.getExtent();
        inputRawModel1 = inputImage1.getRawModel();
        inputTuples1 = inputRawModel1.asTupleBuffer(inputImage1);
        inputColorModel1 = inputImage1.getColorModel();
        
        inputImage2 = (Image) inputParameters.getFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE2.getId());
        inputRawModel2 = inputImage2.getRawModel();
        inputTuples2 = inputRawModel2.asTupleBuffer(inputImage2);
        inputColorModel2 = inputImage2.getColorModel();
        
        if(!inputImage1.getExtent().equals(inputImage2.getExtent())){
            throw new RuntimeException("Image extent mismatch");
        }
        

        //calculate the result image
        outputColorModel = getOutputColorModel();
        outputRawModel = getOutputSampleModel();
        final Buffer buffer = outputRawModel.createBuffer(extent);
        outputImage = new DefaultImage(buffer, extent, outputRawModel, outputColorModel);
        outNbSample = outputTuples.getSampleCount();
        outSampleType = outputTuples.getPrimitiveType();

        outputParameters.setFieldValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId(),outputImage);
        return outputParameters;
    }
    
    /**
     * 
     * @return same image sample model as first image
     */
    protected RawModel getOutputSampleModel(){
        return inputRawModel1;
    }
    
    /**
     * 
     * @return same image color model as first image
     */
    protected ColorModel getOutputColorModel(){
        return inputColorModel1;
    }

    protected void adjustInOutSamples(double[] inSamples, Object outSamples){
        switch(outSampleType){
            case TYPE_1_BIT :
                for(int i=0;i<outNbSample;i++) ((boolean[])outSamples)[i] = inSamples[i] != 0;
                break;
            case TYPE_2_BIT :
            case TYPE_4_BIT :
            case TYPE_BYTE :
                for(int i=0;i<outNbSample;i++) ((byte[])outSamples)[i] = (byte)Maths.clamp(inSamples[i],-128,127);
                break;
            case TYPE_UBYTE :
                for(int i=0;i<outNbSample;i++) ((int[])outSamples)[i] = (int)Maths.clamp(inSamples[i],0,255);
                break;
            case TYPE_SHORT :
                for(int i=0;i<outNbSample;i++) ((short[])outSamples)[i] = (short)Maths.clamp(inSamples[i],Short.MIN_VALUE,Short.MAX_VALUE);
                break;
            case TYPE_USHORT :
                for(int i=0;i<outNbSample;i++) ((int[])outSamples)[i] = (int)Maths.clamp(inSamples[i],0,65535);
                break;
            case TYPE_INT :
                for(int i=0;i<outNbSample;i++) ((int[])outSamples)[i] = (int)inSamples[i];
                break;
            case TYPE_UINT :
                for(int i=0;i<outNbSample;i++) ((long[])outSamples)[i] = (long)inSamples[i];
                break;
            case TYPE_LONG :
                for(int i=0;i<outNbSample;i++) ((long[])outSamples)[i] = (long)inSamples[i];
                break;
            case TYPE_FLOAT :
                for(int i=0;i<outNbSample;i++) ((float[])outSamples)[i] = (float)inSamples[i];
                break;
            case TYPE_DOUBLE :
                outSamples = inSamples;
                break;
            default: throw new UnimplementedException("Unusual data type.");
        }
    }

}
