
package science.unlicense.api.image;

import science.unlicense.api.CObjects;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DefaultBufferFactory;

/**
 * Default image read parameters, only store image index.
 * 
 * @author Johann Sorel
 */
public class DefaultImageReadParameters implements ImageReadParameters{

    protected int imageIndex;
    protected BufferFactory factory = DefaultBufferFactory.INSTANCE;
    protected boolean decompress = false;
    
    public void setImageIndex(int indexIndex) {
        imageIndex = indexIndex;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public boolean isDecompress() {
        return decompress;
    }

    public void setDecompress(boolean decompress) {
        this.decompress = decompress;
    }

    public BufferFactory getBufferFactory() {
        return factory;
    }

    public void setBufferFactory(BufferFactory factory) {
        CObjects.ensureNotNull(factory);
        this.factory = factory;
    }
    
}
