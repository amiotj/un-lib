
package science.unlicense.api.image.process;

import science.unlicense.api.math.InterpolationMethods;


/**
 * Interpolator are used to average samples between pixels.
 * When scaling images, this result in smoother images.
 * 
 * @author Johann Sorel
 */
public class InterpolatorLinear extends AbstractInterpolator {

    public void evaluate(int[] coordinate, double[] buffer) {
        parent.evaluate(coordinate, buffer);
    }

    public void evaluate(double[] coordinate, double[] buffer) {
        //get the 4 corners
        final double[] buffer1 = new double[buffer.length];
        final double[] buffer2 = new double[buffer.length];
        final double[] buffer3 = new double[buffer.length];
        final double[] buffer4 = new double[buffer.length];
        
        final double x = coordinate[0];
        final double y = coordinate[1];
        final double minx = Math.floor(coordinate[0]);
        final double maxx = Math.ceil(coordinate[0]);
        final double miny = Math.floor(coordinate[1]);
        final double maxy = Math.ceil(coordinate[1]);
        final double ratiox = maxx-minx;
        final double ratioy = maxy-miny;
        
        coordinate[0] = minx; coordinate[1] = miny;
        parent.evaluate(coordinate, buffer1);
        coordinate[0] = maxx; coordinate[1] = miny;
        parent.evaluate(coordinate, buffer2);
        coordinate[0] = minx; coordinate[1] = maxy;
        parent.evaluate(coordinate, buffer3);
        coordinate[0] = maxx; coordinate[1] = maxy;
        parent.evaluate(coordinate, buffer4);
        
        for(int i=0;i<buffer.length;i++){
            final double interMiny = InterpolationMethods.linear(ratiox, buffer1[i], buffer2[i]);
            final double interMaxY = InterpolationMethods.linear(ratiox, buffer3[i], buffer4[i]);
            buffer[i] = InterpolationMethods.linear(ratioy, interMiny, interMaxY);
        }
        
        //reset values, maybe they are used by the caller
        coordinate[0] = x; coordinate[1] = y;
    }

}
