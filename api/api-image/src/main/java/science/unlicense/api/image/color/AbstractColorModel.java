
package science.unlicense.api.image.color;

import science.unlicense.api.color.colorspace.ColorSpace;
import science.unlicense.api.image.Image;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractColorModel implements ColorModel {

    protected final ColorSpace colorSpace;

    /**
     * 
     * TODO : this should not be a color space.
     * @param colorSpace 
     */
    public AbstractColorModel(ColorSpace colorSpace) {
        this.colorSpace = colorSpace;
    }

    public ColorSpace getColorSpace() {
        return colorSpace;
    }

    public PixelBuffer asTupleBuffer(Image image) {
        return new DefaultPixelBuffer(image, this);
    }
    
}
