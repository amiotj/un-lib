
package science.unlicense.api.image.process;

/**
 * Extend image filled pixels with a repetition of the image.
 *
 * For example if on a one channel image we obtain ;
 * <pre>
 *              e f d e f d e
 *              h i g h i g h
 *   a b c      b c a b c a b
 *   d e f  ===>e f d e f d e
 *   g h i      h i g h i g h
 *              b c a b c a b
 *              e f d e f d e
 * </pre>
 *
 * @author Humbert Florent
 * @author Johann Sorel
 */
public class ExtrapolatorWrap extends AbstractExtrapolator implements Extrapolator{

    public ExtrapolatorWrap() {
    }

    public void evaluateOutside(int[] coordinate, double[] buffer) {

        //we need to restore old values after, better avoid modifying them
        final int oldx = coordinate[0];
        final int oldy = coordinate[1];
        if(oldx<0) coordinate[0] =  dims[0] - ( Math.abs(oldx) % dims[0] );
        else if(oldx>=dims[0]) coordinate[0] = (oldx % dims[0]) -1;
        if(oldy<0) coordinate[1] = dims[1] - ( Math.abs(oldy) % dims[1] );
        else if(oldy>=dims[1]) coordinate[1] = (oldy % dims[1]) -1;

        imageTuples.getTupleDouble(coordinate, buffer);

        //restore old value
        coordinate[0] = oldx;
        coordinate[1] = oldy;
    }

}
