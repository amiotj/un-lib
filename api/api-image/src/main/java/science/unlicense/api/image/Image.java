package science.unlicense.api.image;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.sample.RawModel;

/**
 * An image is a regular multidimentional array of values.
 * Usual images are in 2 dimensions and have 3 or 4 bands mapping RGBA colors.
 *
 * @author Johann Sorel
 */
public interface Image {

    /**
     * Get image extent.
     * An image is a N dimension grid.
     *
     * @return number and size of the image
     */
    Extent.Long getExtent();

    /**
     * Images store different metadata models whitin them.
     * Each often being specific to it's own storage design.
     *
     * @return Dictionary, Chars -> TypedNode
     */
    Dictionary getMetadatas();
    
    /**
     * Images are N dimension raw byte buffers.
     * ImageModel produce views of the image for a given purpose.
     * See {@link ImageModel}
     *
     * @return Dictionary, Chars -> Metadata
     */
    Dictionary getModels();

    /**
     * Get mandatory raw model.
     *
     * @return RawModel, never null.
     */
    RawModel getRawModel();

    /**
     * Get optional color model.
     *
     * @return ColorModel, can be null.
     */
    ColorModel getColorModel();

    /**
     * Raw image data buffer.
     * 
     * @return Buffer
     */
    Buffer getDataBuffer();

}
