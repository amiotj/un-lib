package science.unlicense.api.image;

import science.unlicense.api.io.IOException;
import science.unlicense.api.io.Writer;

/**
 *
 * @author Johann Sorel
 */
public interface ImageWriter extends Writer {

    /**
     * Create a default write parameters.
     *
     * @return ImageReadParameters
     */
    ImageWriteParameters createParameters();

    /**
     * Write an image in the file.
     *
     * @param params, can be null.
     * @throws IOException
     */
    void write(Image image, ImageWriteParameters params) throws IOException;

}
