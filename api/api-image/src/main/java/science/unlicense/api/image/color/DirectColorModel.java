
package science.unlicense.api.image.color;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.color.Color;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.image.sample.RawModel.TYPE_BYTE;
import static science.unlicense.api.image.sample.RawModel.TYPE_DOUBLE;
import static science.unlicense.api.image.sample.RawModel.TYPE_FLOAT;
import static science.unlicense.api.image.sample.RawModel.TYPE_UBYTE;
import science.unlicense.impl.color.colorspace.SRGB;

/**
 * Usual image storage formats are made to store colors, threfor once
 * the sample value is readed, minimum adjustment are necesseray to obtain a color.
 *
 * Supported sample types are :
 * BYTE UBYTE -> expected to be range 0-255
 * FLOAT DOUBLE -> expected to be range 0-1
 *
 * @author Johann Sorel
 */
public class DirectColorModel extends AbstractColorModel {

    private final int sampleType;
    private final int nbSample;
    private final int[] mapping;
    private final boolean alphaPremultiplied;

    /**
     *
     * @param rawModel
     * @param mapping sample indexes to RGBA mapping. use -1 for not provided.
     *         must be of size 4, in order R G B A
     * @param alphaPremultiplied indicate the sample values are already alpha premultiplied.
     *         value is interesting only if an alpha band exist.
     */
    public DirectColorModel(RawModel rawModel, int[] mapping, boolean alphaPremultiplied) {
        super(SRGB.INSTANCE);
        this.sampleType = rawModel.getPrimitiveType();
        this.nbSample = rawModel.getSampleCount();
        this.mapping = mapping;
        this.alphaPremultiplied = alphaPremultiplied;
    }

    public int[] getMapping() {
        return Arrays.copy(mapping);
    }

    public boolean isAlphaPreMultiplied() {
        return alphaPremultiplied;
    }

    public Color toColor(Object sample) {
        final float[] rgba = new float[]{0,0,0,1};
        if(TYPE_BYTE == sampleType){
            final byte[] samples = (byte[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = (samples[mapping[i]] &0xff)/255f;
            }
        }else if(TYPE_UBYTE == sampleType){
            final int[] samples = (int[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = samples[mapping[i]]/255f;
            }
        }else if(TYPE_FLOAT == sampleType){
            final float[] samples = (float[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = samples[mapping[i]];
            }
        }else if(TYPE_DOUBLE == sampleType){
            final double[] samples = (double[]) sample;
            for(int i=0;i<4;i++){
                if(mapping[i] < 0) continue;
                rgba[i] = (float)samples[mapping[i]];
            }
        }
        return new Color(rgba,alphaPremultiplied);
    }

    public int toColorARGB(Object sample) {
        final Color color = toColor(sample);
        return color.toARGB();
    }

    public Object toSample(Color color) {
        final float[] rgba;
        if(alphaPremultiplied){
            rgba = color.toRGBAPreMul();
        }else{
            rgba = color.toRGBA();
        }
        //flip alpha to match

        if(TYPE_BYTE == sampleType){
            final byte[] samples = new byte[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = (byte)(rgba[i] * 255);
            }
            return samples;
        }else if(TYPE_UBYTE == sampleType){
            final int[] samples = new int[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = (int)(rgba[i] * 255);
            }
            return samples;
        }else if(TYPE_FLOAT == sampleType){
            final float[] samples = new float[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = rgba[i];
            }
            return samples;
        }else if(TYPE_DOUBLE == sampleType){
            final double[] samples = new double[nbSample];
            for(int i=0;i<mapping.length;i++){
                if(mapping[i]<0)continue;
                samples[mapping[i]] = rgba[i];
            }
            return samples;
        }else{
            throw new RuntimeException("Sample can not be defined for given color.");
        }
    }

    public Object toSample(int color) {
        return toSample(new Color(color));
    }

    
}
