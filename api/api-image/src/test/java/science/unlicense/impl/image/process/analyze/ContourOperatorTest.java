
package science.unlicense.impl.image.process.analyze;

import static org.junit.Assert.*;
import org.junit.Test;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;

/**
 *
 * @author Johann Sorel
 */
public class ContourOperatorTest {

    private static final double DELTA = 0;
    
    private static final Predicate MATCHER = new Predicate() {
        @Override
        public Boolean evaluate(Object candidate) {
            return ((double[]) candidate)[0] == 7;
        }
    };
    private static final int[] VALID = new int[]{7};
    private static final int[] INVALID = new int[]{5};

    /**
     * .
     */
    @Test
    public void singleFalsePixel() {
        
        final Image img = Images.createCustomBand(new Extent.Long(1, 1), 1, RawModel.TYPE_INT);
        img.getRawModel().asTupleBuffer(img).setTuple(new int[]{0,0}, INVALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(0, geom.getGeometries().getSize());
    }
    
    /**
     * █
     */
    @Test
    public void singleTruePixel() {
        
        final Image img = Images.createCustomBand(new Extent.Long(1, 1), 1, RawModel.TYPE_INT);
        img.getRawModel().asTupleBuffer(img).setTuple(new int[]{0,0}, VALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getHoles());
        
        final TupleBuffer1D coordinates = exterior.getCoordinates();
        assertEquals(5l, coordinates.getExtent().getL(0));
        
        assertArrayEquals(new double[]{1,1}, coordinates.getTupleDouble(0, null),DELTA);
        assertArrayEquals(new double[]{0,1}, coordinates.getTupleDouble(1, null),DELTA);
        assertArrayEquals(new double[]{0,0}, coordinates.getTupleDouble(2, null),DELTA);
        assertArrayEquals(new double[]{1,0}, coordinates.getTupleDouble(3, null),DELTA);
        assertArrayEquals(new double[]{1,1}, coordinates.getTupleDouble(4, null),DELTA);
    }
    
    /**
     * ██
     * ██
     */
    @Test
    public void square() {
        
        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, RawModel.TYPE_INT);
        final TupleBuffer tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new int[]{0,0}, VALID);
        tb.setTuple(new int[]{1,0}, VALID);
        tb.setTuple(new int[]{1,1}, VALID);
        tb.setTuple(new int[]{0,1}, VALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getHoles());
        
        final TupleBuffer1D coordinates = exterior.getCoordinates();
        assertEquals(5l, coordinates.getExtent().getL(0));
        
        assertArrayEquals(new double[]{2,2}, coordinates.getTupleDouble(0, null),DELTA);
        assertArrayEquals(new double[]{0,2}, coordinates.getTupleDouble(1, null),DELTA);
        assertArrayEquals(new double[]{0,0}, coordinates.getTupleDouble(2, null),DELTA);
        assertArrayEquals(new double[]{2,0}, coordinates.getTupleDouble(3, null),DELTA);
        assertArrayEquals(new double[]{2,2}, coordinates.getTupleDouble(4, null),DELTA);
    }
    
    /**
     * ██
     * █.
     */
    @Test
    public void angle1() {
        
        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, RawModel.TYPE_INT);
        final TupleBuffer tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new int[]{0,0}, VALID);
        tb.setTuple(new int[]{1,0}, VALID);
        tb.setTuple(new int[]{1,1}, INVALID);
        tb.setTuple(new int[]{0,1}, VALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getHoles());
        
        final TupleBuffer1D coordinates = exterior.getCoordinates();
        assertEquals(7l, coordinates.getExtent().getL(0));
        final double[] coords = coordinates.getPrimitiveBuffer().toDoubleArray();
        
        final double[] expected = {
            1,2,
            0,2,
            0,0,
            2,0,
            2,1,
            1,1,
            1,2};
        
        assertArrayEquals(expected, coords, DELTA);
    }
    
    /**
     * .█
     * ██
     */
    @Test
    public void angle2() {
        
        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, RawModel.TYPE_INT);
        final TupleBuffer tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new int[]{0,0}, INVALID);
        tb.setTuple(new int[]{1,0}, VALID);
        tb.setTuple(new int[]{1,1}, VALID);
        tb.setTuple(new int[]{0,1}, VALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(1, geom.getGeometries().getSize());
        final Polygon polygon = (Polygon) geom.getGeometries().get(0);
        final Polyline exterior = polygon.getExterior();
        assertNotNull(exterior);
        assertNull(polygon.getHoles());
        
        final TupleBuffer1D coordinates = exterior.getCoordinates();
        assertEquals(7l, coordinates.getExtent().getL(0));
        final double[] coords = coordinates.getPrimitiveBuffer().toDoubleArray();
        
        final double[] expected = {
            2,2,
            0,2,
            0,1,
            1,1,
            1,0,
            2,0,
            2,2};
        
        assertArrayEquals(expected, coords, DELTA);
    }
    
    /**
     * .█
     * █.
     */
    @Test
    public void median1() {
        
        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, RawModel.TYPE_INT);
        final TupleBuffer tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new int[]{0,0}, INVALID);
        tb.setTuple(new int[]{1,0}, VALID);
        tb.setTuple(new int[]{1,1}, INVALID);
        tb.setTuple(new int[]{0,1}, VALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(2, geom.getGeometries().getSize());
        final Polygon poly1 = (Polygon) geom.getGeometries().get(0);
        final Polygon poly2 = (Polygon) geom.getGeometries().get(1);
        assertNull(poly1.getHoles());
        assertNull(poly2.getHoles());
        
        final double[] coords1 = poly1.getExterior().getCoordinates().getPrimitiveBuffer().toDoubleArray();
        final double[] coords2 = poly2.getExterior().getCoordinates().getPrimitiveBuffer().toDoubleArray();
        
        final double[] exp1 = {
            2,1,
            1,1,
            1,0,
            2,0,
            2,1};
        assertArrayEquals(exp1, coords1, DELTA);
        
        final double[] exp2 = {
            1,2,
            0,2,
            0,1,
            1,1,
            1,2};
        assertArrayEquals(exp2, coords2, DELTA);
    }
    
    /**
     * █.
     * .█
     */
    @Test
    public void median2() {
        
        final Image img = Images.createCustomBand(new Extent.Long(2, 2), 1, RawModel.TYPE_INT);
        final TupleBuffer tb = img.getRawModel().asTupleBuffer(img);
        tb.setTuple(new int[]{0,0}, VALID);
        tb.setTuple(new int[]{1,0}, INVALID);
        tb.setTuple(new int[]{1,1}, VALID);
        tb.setTuple(new int[]{0,1}, INVALID);
        
        final ContourOperator op = new ContourOperator();
        final MultiPolygon geom = op.execute(img, MATCHER, false);
        assertNotNull(geom);
        assertEquals(2, geom.getGeometries().getSize());
        final Polygon poly1 = (Polygon) geom.getGeometries().get(0);
        final Polygon poly2 = (Polygon) geom.getGeometries().get(1);
        assertNull(poly1.getHoles());
        assertNull(poly2.getHoles());
        
        final double[] coords1 = poly1.getExterior().getCoordinates().getPrimitiveBuffer().toDoubleArray();
        final double[] coords2 = poly2.getExterior().getCoordinates().getPrimitiveBuffer().toDoubleArray();
        
        final double[] exp1 = {
            1,1,
            0,1,
            0,0,
            1,0,
            1,1};
        assertArrayEquals(exp1, coords1, DELTA);
        
        final double[] exp2 = {
            2,2,
            1,2,
            1,1,
            2,1,
            2,2};
        assertArrayEquals(exp2, coords2, DELTA);
    }
    
}
