
package science.unlicense.api.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.impl.color.colorspace.HSL;
import science.unlicense.impl.color.colorspace.HSV;

/**
 * Test color methods.
 *
 * @author Johann Sorel
 */
public class ColorTest {

    private static final float DELTA = 0.0000001f;
    private static final HSV hsv = HSV.INSTANCE;

    /**
     * Check float array to ARGB conversion.
     */
    @Test
    public void floatToARGB(){
        final float[] rgba = new float[]{1f,0f,0f,1f};
        final Color color = new Color(rgba);

        int argb = color.toARGB();
        int a = (argb >>> 24) & 0xFF;
        int r = (argb >>> 16) & 0xFF;
        int g = (argb >>>  8) & 0xFF;
        int b = (argb >>>  0) & 0xFF;
        Assert.assertEquals(255, a);
        Assert.assertEquals(255, r);
        Assert.assertEquals(0, g);
        Assert.assertEquals(0, b);
    }

    /**
     * Check ARGB to float array conversion.
     */
    @Test
    public void ARGBToFloat(){
        int argb = -65536; //red
        final Color color = new Color(argb);
        Assert.assertEquals(1f, color.getAlpha(), DELTA);
        Assert.assertEquals(1f, color.getRed(), DELTA);
        Assert.assertEquals(0f, color.getGreen(), DELTA);
        Assert.assertEquals(0f, color.getBlue(), DELTA);
    }

    /**
     * Check YCbCr to RGB conversion.
     * TODO BUG
     */
    @Ignore
    @Test
    public void YCbCrToRGB(){
        final float[] ycc = new float[]{1f,0.5f,0.5f};
        final float[] rgb = Colors.YCbCrtoRGB(ycc);
        Assert.assertEquals(1f, rgb[0], DELTA);
        Assert.assertEquals(1f, rgb[1], DELTA);
        Assert.assertEquals(1f, rgb[2], DELTA);
    }

    @Test
    public void YCbCrToRGBInt(){
        final int[] ycc = new int[]{255,128,128};
        final int[] rgb = Colors.YCbCrtoRGB(ycc);
        Assert.assertEquals(255f, rgb[0], DELTA);
        Assert.assertEquals(255f, rgb[1], DELTA);
        Assert.assertEquals(255f, rgb[2], DELTA);
    }

    @Test
    public void HSVtoRGB(){
        final float[] bufferhsv = new float[3];
        final float[] bufferrgba = new float[4];
        Assert.assertArrayEquals(new float[]{  0,0,0}, hsv.toSpace(new float[]{0,0,0,1}, bufferhsv), DELTA);
        Assert.assertArrayEquals(new float[]{  0,1,1}, hsv.toSpace(new float[]{1,0,0,1}, bufferhsv), DELTA);
        Assert.assertArrayEquals(new float[]{120,1,1}, hsv.toSpace(new float[]{0,1,0,1}, bufferhsv), DELTA);
        Assert.assertArrayEquals(new float[]{240,1,1}, hsv.toSpace(new float[]{0,0,1,1}, bufferhsv), DELTA);
        Assert.assertArrayEquals(new float[]{  0,0,1}, hsv.toSpace(new float[]{1,1,1,1}, bufferhsv), DELTA);
        
        Assert.assertArrayEquals(new float[]{0,0,0,1}, hsv.toRGBA(new float[]{  0,0,0}, bufferrgba), DELTA);
        Assert.assertArrayEquals(new float[]{1,0,0,1}, hsv.toRGBA(new float[]{  0,1,1}, bufferrgba), DELTA);
        Assert.assertArrayEquals(new float[]{0,1,0,1}, hsv.toRGBA(new float[]{120,1,1}, bufferrgba), DELTA);
        Assert.assertArrayEquals(new float[]{0,0,1,1}, hsv.toRGBA(new float[]{240,1,1}, bufferrgba), DELTA);
        Assert.assertArrayEquals(new float[]{1,1,1,1}, hsv.toRGBA(new float[]{  0,0,1}, bufferrgba), DELTA);
                
    }
    
    @Test
    public void RGBtoHSL(){
        
    }
    
    @Test
    public void C4toC8(){
        Assert.assertEquals(0,     Colors.C4toC8(0));
        Assert.assertEquals(255,   Colors.C4toC8(15));
    }

    @Test
    public void C5toC8(){
        Assert.assertEquals(0,     Colors.C5toC8(0));
        Assert.assertEquals(255,   Colors.C5toC8(31));
    }

    @Test
    public void C6toC8(){
        Assert.assertEquals(0,     Colors.C6toC8(0));
        Assert.assertEquals(255,   Colors.C6toC8(63));
    }

}
