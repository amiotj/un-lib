
package science.unlicense.api.image.sample;

import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultByteBuffer;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;

/**
 * Test interleaved image sampel model.
 * 
 * @author Johann Sorel
 */
public class InterleavedTest {
    
    @Test
    public void test1Bit1Band(){
        
        final byte[] data = new byte[]{(byte)0xAA,0x55};
        final Buffer bank = new DefaultByteBuffer(data);
        final InterleavedRawModel sm = new InterleavedRawModel(RawModel.TYPE_1_BIT, 1);
        final Image image = new DefaultImage(bank, new Extent.Long(8, 2), sm);
        final TupleBuffer tb = sm.asTupleBuffer(image);
        
        final byte[] sample = new byte[1];
        Assert.assertEquals(1, tb.getTupleByte(new int[]{0,0}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{1,0}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{2,0}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{3,0}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{4,0}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{5,0}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{6,0}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{7,0}, sample)[0]);
        
        Assert.assertEquals(0, tb.getTupleByte(new int[]{0,1}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{1,1}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{2,1}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{3,1}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{4,1}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{5,1}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{6,1}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{7,1}, sample)[0]);
    }
    
    @Test
    public void test2Bit1Band(){
        
        final byte[] data = new byte[]{(byte)0x1B,(byte)0xE4};
        final Buffer bank = new DefaultByteBuffer(data);
        final InterleavedRawModel sm = new InterleavedRawModel(RawModel.TYPE_2_BIT, 1);
        final Image image = new DefaultImage(bank, new Extent.Long(4, 2), sm);
        final TupleBuffer tb = sm.asTupleBuffer(image);
        
        final byte[] sample = new byte[1];
        Assert.assertEquals(0, tb.getTupleByte(new int[]{0,0}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{1,0}, sample)[0]);
        Assert.assertEquals(2, tb.getTupleByte(new int[]{2,0}, sample)[0]);
        Assert.assertEquals(3, tb.getTupleByte(new int[]{3,0}, sample)[0]);
        
        Assert.assertEquals(3, tb.getTupleByte(new int[]{0,1}, sample)[0]);
        Assert.assertEquals(2, tb.getTupleByte(new int[]{1,1}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{2,1}, sample)[0]);
        Assert.assertEquals(0, tb.getTupleByte(new int[]{3,1}, sample)[0]);
    }
    
    @Test
    public void test4Bit1Band(){
        
        final byte[] data = new byte[]{(byte)0x52,(byte)0xC1};
        final Buffer bank = new DefaultByteBuffer(data);
        final InterleavedRawModel sm = new InterleavedRawModel(RawModel.TYPE_4_BIT, 1);
        final Image image = new DefaultImage(bank, new Extent.Long(2, 2), sm);
        final TupleBuffer tb = sm.asTupleBuffer(image);
        
        final byte[] sample = new byte[1];
        Assert.assertEquals(5, tb.getTupleByte(new int[]{0,0}, sample)[0]);
        Assert.assertEquals(2, tb.getTupleByte(new int[]{1,0}, sample)[0]);
        
        Assert.assertEquals(12,tb.getTupleByte(new int[]{0,1}, sample)[0]);
        Assert.assertEquals(1, tb.getTupleByte(new int[]{1,1}, sample)[0]);
    }
    
}
