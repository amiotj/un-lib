
package science.unlicense.api.image;

import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import org.junit.Test;
import science.unlicense.api.image.sample.RawModel;
import org.junit.Assert;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;

/**
 *
 * @author Johann Sorel
 */
public class ImageTest {

    @Test
    public void BitMaskImage(){

        final Image image = Images.createCustomBand(new Extent.Long(100, 40), 1, RawModel.TYPE_1_BIT);
        Assert.assertNotNull(image);
        Assert.assertEquals(2, image.getExtent().getDimension());
        Assert.assertEquals(100, image.getExtent().getL(0));
        Assert.assertEquals(40, image.getExtent().getL(1));

        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final int[] coordinate = new int[2];
        final boolean[] storage = new boolean[1];

        coordinate[0] = 0; coordinate[1] = 0;
        sm.getTuple(coordinate, storage);
        Assert.assertFalse(storage[0]);

        coordinate[0] = 10; coordinate[1] = 20;
        sm.getTuple(coordinate, storage);
        Assert.assertFalse(storage[0]);

        storage[0] = true;
        sm.setTuple(coordinate, storage);
        sm.getTuple(coordinate, storage);
        Assert.assertTrue(storage[0]);

    }

}
