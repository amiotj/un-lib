
package science.unlicense.api.color;

import science.unlicense.api.color.Color;
import science.unlicense.api.color.AlphaBlending;
import org.junit.Test;
import org.junit.Assert;
import static science.unlicense.api.color.AlphaBlending.CLEAR;
import static science.unlicense.api.color.AlphaBlending.DST;
import static science.unlicense.api.color.AlphaBlending.DST_ATOP;
import static science.unlicense.api.color.AlphaBlending.DST_IN;
import static science.unlicense.api.color.AlphaBlending.DST_OUT;
import static science.unlicense.api.color.AlphaBlending.DST_OVER;
import static science.unlicense.api.color.AlphaBlending.SRC;
import static science.unlicense.api.color.AlphaBlending.SRC_ATOP;
import static science.unlicense.api.color.AlphaBlending.SRC_IN;
import static science.unlicense.api.color.AlphaBlending.SRC_OUT;
import static science.unlicense.api.color.AlphaBlending.SRC_OVER;
import static science.unlicense.api.color.AlphaBlending.XOR;
import static science.unlicense.api.color.Color.BLUE;
import static science.unlicense.api.color.Color.RED;

/**
 * Test color blending.
 *
 * @author Johann Sorel
 */
public class AlphaBlendingTest {

    private static final float DELTA = 0.0000001f;
    private static final Color TRS = new Color(0f, 0f, 0f, 0f);

    @Test
    public void clear(){
        testCase(RED, TRS,  CLEAR, 1f, TRS);
        testCase(TRS, BLUE, CLEAR, 1f, TRS);
        testCase(RED, BLUE, CLEAR, 1f, TRS);
        testCase(TRS, TRS,  CLEAR, 1f, TRS);
    }

    @Test
    public void src(){
        testCase(RED, TRS,  SRC, 1f, RED);
        testCase(TRS, BLUE, SRC, 1f, TRS);
        testCase(RED, BLUE, SRC, 1f, RED);
        testCase(TRS, TRS,  SRC, 1f, TRS);

        testCase(RED, TRS,  SRC, 0.5f, new Color(1f,0f,0f,0.5f));
        testCase(TRS, BLUE, SRC, 0.5f, TRS);
        testCase(RED, BLUE, SRC, 0.5f, new Color(1f,0f,0f,0.5f));
        testCase(TRS, TRS,  SRC, 0.5f, TRS);
    }

    @Test
    public void dst(){
        testCase(RED, TRS,  DST, 1f, TRS);
        testCase(TRS, BLUE, DST, 1f, BLUE);
        testCase(RED, BLUE, DST, 1f, BLUE);
        testCase(TRS, TRS,  DST, 1f, TRS);

        //alpha has no effect in DST mode since alphe is applied on source only
        testCase(RED, TRS,  DST, 0.5f, TRS);
        testCase(TRS, BLUE, DST, 0.5f, BLUE);
        testCase(RED, BLUE, DST, 0.5f, BLUE);
        testCase(TRS, TRS,  DST, 0.5f, TRS);
    }

    @Test
    public void srcOver(){
        testCase(RED, TRS,  SRC_OVER, 1f, RED);
        testCase(TRS, BLUE, SRC_OVER, 1f, BLUE);
        testCase(RED, BLUE, SRC_OVER, 1f, RED);
        testCase(TRS, TRS,  SRC_OVER, 1f, TRS);

        testCase(RED, TRS,  SRC_OVER, 0.5f, new Color(1.0f, 0.0f, 0.0f, 0.5f));
        testCase(TRS, BLUE, SRC_OVER, 0.5f, BLUE);
        testCase(RED, BLUE, SRC_OVER, 0.5f, new Color(0.5f, 0.0f, 0.5f, 1.0f));
        testCase(TRS, TRS,  SRC_OVER, 0.5f, TRS);
    }

    @Test
    public void dstOver(){
        testCase(RED, TRS,  DST_OVER, 1f, RED);
        testCase(TRS, BLUE, DST_OVER, 1f, BLUE);
        testCase(RED, BLUE, DST_OVER, 1f, BLUE);
        testCase(TRS, TRS,  DST_OVER, 1f, TRS);

        testCase(RED, TRS,  DST_OVER, 0.5f, new Color(1.0f, 0.0f, 0.0f, 0.5f));
        testCase(TRS, BLUE, DST_OVER, 0.5f, BLUE);
        testCase(RED, BLUE, DST_OVER, 0.5f, BLUE);
        testCase(TRS, TRS,  DST_OVER, 0.5f, TRS);
    }

    @Test
    public void srcIn(){
        testCase(RED, TRS,  SRC_IN, 1f, TRS);
        testCase(TRS, BLUE, SRC_IN, 1f, TRS);
        testCase(RED, BLUE, SRC_IN, 1f, RED);
        testCase(TRS, TRS,  SRC_IN, 1f, TRS);

        testCase(RED, TRS,  SRC_IN, 0.5f, TRS);
        testCase(TRS, BLUE, SRC_IN, 0.5f, TRS);
        testCase(RED, BLUE, SRC_IN, 0.5f, new Color(1.0f, 0.0f, 0.0f, 0.5f));
        testCase(TRS, TRS,  SRC_IN, 0.5f, TRS);
    }

    @Test
    public void dstIn(){
        testCase(RED, TRS,  DST_IN, 1f, TRS);
        testCase(TRS, BLUE, DST_IN, 1f, TRS);
        testCase(RED, BLUE, DST_IN, 1f, BLUE);
        testCase(TRS, TRS,  DST_IN, 1f, TRS);

        testCase(RED, TRS,  DST_IN, 0.5f, TRS);
        testCase(TRS, BLUE, DST_IN, 0.5f, TRS);
        testCase(RED, BLUE, DST_IN, 0.5f, new Color(0.0f, 0.0f, 1.0f, 0.5f));
        testCase(TRS, TRS,  DST_IN, 0.5f, TRS);
    }

    @Test
    public void srcOut(){
        testCase(RED, TRS,  SRC_OUT, 1f, RED);
        testCase(TRS, BLUE, SRC_OUT, 1f, TRS);
        testCase(RED, BLUE, SRC_OUT, 1f, TRS);
        testCase(TRS, TRS,  SRC_OUT, 1f, TRS);

        testCase(RED, TRS,  SRC_OUT, 0.5f, new Color(1.0f, 0.0f, 0.0f, 0.5f));
        testCase(TRS, BLUE, SRC_OUT, 0.5f, TRS);
        testCase(RED, BLUE, SRC_OUT, 0.5f, TRS);
        testCase(TRS, TRS,  SRC_OUT, 0.5f, TRS);
    }

    @Test
    public void dstOut(){
        testCase(RED, TRS,  DST_OUT, 1f, TRS);
        testCase(TRS, BLUE, DST_OUT, 1f, BLUE);
        testCase(RED, BLUE, DST_OUT, 1f, TRS);
        testCase(TRS, TRS,  DST_OUT, 1f, TRS);

        testCase(RED, TRS,  DST_OUT, 0.5f, TRS);
        testCase(TRS, BLUE, DST_OUT, 0.5f, BLUE);
        testCase(RED, BLUE, DST_OUT, 0.5f, new Color(0.0f, 0.0f, 1.0f, 0.5f));
        testCase(TRS, TRS,  DST_OUT, 0.5f, TRS);
    }

    @Test
    public void srcAtop(){
        testCase(RED, TRS,  SRC_ATOP, 1f, TRS);
        testCase(TRS, BLUE, SRC_ATOP, 1f, BLUE);
        testCase(RED, BLUE, SRC_ATOP, 1f, RED);
        testCase(TRS, TRS,  SRC_ATOP, 1f, TRS);

        testCase(RED, TRS,  SRC_ATOP, 0.5f, TRS);
        testCase(TRS, BLUE, SRC_ATOP, 0.5f, BLUE);
        testCase(RED, BLUE, SRC_ATOP, 0.5f, new Color(0.5f, 0.0f, 0.5f, 1.0f));
        testCase(TRS, TRS,  SRC_ATOP, 0.5f, TRS);
    }

    @Test
    public void dstAtop(){
        testCase(RED, TRS,  DST_ATOP, 1f, RED);
        testCase(TRS, BLUE, DST_ATOP, 1f, TRS);
        testCase(RED, BLUE, DST_ATOP, 1f, BLUE);
        testCase(TRS, TRS,  DST_ATOP, 1f, TRS);

        testCase(RED, TRS,  DST_ATOP, 0.5f, new Color(1.0f, 0.0f, 0.0f, 0.5f));
        testCase(TRS, BLUE, DST_ATOP, 0.5f, TRS);
        testCase(RED, BLUE, DST_ATOP, 0.5f, new Color(0.0f, 0.0f, 1.0f, 0.5f));
        testCase(TRS, TRS,  DST_ATOP, 0.5f, TRS);
    }

    @Test
    public void xor(){
        testCase(RED, TRS,  XOR, 1f, RED);
        testCase(TRS, BLUE, XOR, 1f, BLUE);
        testCase(RED, BLUE, XOR, 1f, TRS);
        testCase(TRS, TRS,  XOR, 1f, TRS);

        testCase(RED, TRS,  XOR, 0.5f, new Color(1.0f, 0.0f, 0.0f, 0.5f));
        testCase(TRS, BLUE, XOR, 0.5f, BLUE);
        testCase(RED, BLUE, XOR, 0.5f, new Color(0.0f, 0.0f, 1.0f, 0.5f));
        testCase(TRS, TRS,  XOR, 0.5f, TRS);
    }


    private void testCase(Color src, Color dst, int type, float alpha, Color result){
        final AlphaBlending blend = AlphaBlending.create(type, alpha);
        final float[] buffer = new float[4];

        //not premultipled cases
        Assert.assertEquals(result, blend.blend(src, dst));
        Assert.assertEquals(result.toARGB(), blend.blend(src.toARGB(), dst.toARGB()));
        Assert.assertArrayEquals(result.toRGBA(), blend.blend(src.toRGBA(),dst.toRGBA(),buffer),DELTA);
        Assert.assertArrayEquals(result.toRGBA(), blend.blend(src.toRGBA(),0,dst.toRGBA(),0,buffer,0,1),DELTA);

        //premultiplied cases
        Assert.assertEquals(result.toARGBPreMul(), blend.blendPreMul(src.toARGBPreMul(), dst.toARGBPreMul()));
        Assert.assertArrayEquals(result.toRGBAPreMul(), blend.blendPreMul(src.toRGBAPreMul(),dst.toRGBAPreMul(),buffer),DELTA);
        Assert.assertArrayEquals(result.toRGBAPreMul(), blend.blendPreMul(src.toRGBAPreMul(),0,dst.toRGBAPreMul(),0,buffer,0,1),DELTA);

//        //static methods
//        switch(type){
//            case AlphaBlending.CLEAR : break;
//            case AlphaBlending.SRC : Assert.assertEquals(result, AlphaBlending.src(src,dst)); break;
//            case AlphaBlending.DST : Assert.assertEquals(result, AlphaBlending.dst(src,dst)); break;
//            case AlphaBlending.XOR : Assert.assertEquals(result, AlphaBlending.xor(src,dst)); break;
//            case AlphaBlending.LIGHTER : break;
//            case AlphaBlending.SRC_OVER : Assert.assertEquals(result, AlphaBlending.srcOver(src,dst)); break;
//            case AlphaBlending.SRC_IN : Assert.assertEquals(result, AlphaBlending.srcIn(src,dst)); break;
//            case AlphaBlending.SRC_OUT : Assert.assertEquals(result, AlphaBlending.srcOut(src,dst)); break;
//            case AlphaBlending.SRC_ATOP : Assert.assertEquals(result, AlphaBlending.srcAtop(src,dst)); break;
//            case AlphaBlending.DST_OVER : Assert.assertEquals(result, AlphaBlending.dstOver(src,dst)); break;
//            case AlphaBlending.DST_IN : Assert.assertEquals(result, AlphaBlending.dstIn(src,dst)); break;
//            case AlphaBlending.DST_OUT : Assert.assertEquals(result, AlphaBlending.dstOut(src,dst)); break;
//            case AlphaBlending.DST_ATOP : Assert.assertEquals(result, AlphaBlending.dstAtop(src,dst)); break;
//        }

    }

}