
package science.unlicense.api.color;

import science.unlicense.api.color.Color;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.BitPackColorModel;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.image.sample.BitPackRawModel;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class BitPackColorModelTest {
    
    @Test
    public void RGB565Test(){
        
        final Extent.Long ext = new Extent.Long(1, 4);
        final Buffer buffer = DefaultBufferFactory.create(8, Primitive.TYPE_UBYTE, NumberEncoding.BIG_ENDIAN);
        buffer.writeUShort(0, 0); //0,0,0
        buffer.writeUShort(0xFFFF, 2);//1,1,1
        buffer.writeUShort(0x07E0, 4);//0,1,0
        buffer.writeUShort(0xF81F, 6);//1,0,1
        
        final BitPackColorModel cm = new BitPackColorModel(new int[]{5,6,5}, new int[]{0,1,2,-1}, false);
        final RawModel rm = new BitPackRawModel(16, new int[]{0,5,11}, new int[]{5,6,5});
        final Image image = new DefaultImage(buffer, ext, rm, cm);
        
        final PixelBuffer pb = image.getColorModel().asTupleBuffer(image);
        Assert.assertEquals(new Color(0, 0, 0),pb.getColor(new int[]{0,0}));
        Assert.assertEquals(new Color(1f, 1f, 1f),pb.getColor(new int[]{0,1}));
        Assert.assertEquals(new Color(0f, 1f, 0f),pb.getColor(new int[]{0,2}));
        Assert.assertEquals(new Color(1f, 0f, 1f),pb.getColor(new int[]{0,3}));
        
    }
    
}
