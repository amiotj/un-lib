

package science.unlicense.api.image.process.geometric;

import science.unlicense.impl.image.process.geometric.Rotate270Operator;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;

/**
 * Rotate 270° operator test
 * 
 * @author Johann Sorel
 */
public class Rotate270Test {
    
    @Test
    public void test2x2(){
        
        Image image = Images.create(new Extent.Long(2, 2),Images.IMAGE_TYPE_RGB);
        TupleBuffer intb = image.getRawModel().asTupleBuffer(image);
        intb.setTuple(new int[]{0,0}, new byte[]{0,0,0});
        intb.setTuple(new int[]{1,0}, new byte[]{1,1,1});
        intb.setTuple(new int[]{0,1}, new byte[]{2,2,2});
        intb.setTuple(new int[]{1,1}, new byte[]{3,3,3});
        
        image = new Rotate270Operator().execute(image);
        intb = image.getRawModel().asTupleBuffer(image);
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(2, image.getExtent().getL(1));
        Assert.assertArrayEquals(new int[]{1,1,1}, (int[])intb.getTuple(new int[]{0,0}, null));
        Assert.assertArrayEquals(new int[]{3,3,3}, (int[])intb.getTuple(new int[]{1,0}, null));
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])intb.getTuple(new int[]{0,1}, null));
        Assert.assertArrayEquals(new int[]{2,2,2}, (int[])intb.getTuple(new int[]{1,1}, null));
    }
    
    @Test
    public void test3x2(){
        
        Image image = Images.create(new Extent.Long(3, 2),Images.IMAGE_TYPE_RGB);
        TupleBuffer intb = image.getRawModel().asTupleBuffer(image);
        intb.setTuple(new int[]{0,0}, new byte[]{0,0,0});
        intb.setTuple(new int[]{1,0}, new byte[]{1,1,1});
        intb.setTuple(new int[]{2,0}, new byte[]{2,2,2});
        intb.setTuple(new int[]{0,1}, new byte[]{3,3,3});
        intb.setTuple(new int[]{1,1}, new byte[]{4,4,4});
        intb.setTuple(new int[]{2,1}, new byte[]{5,5,5});
        
        image = new Rotate270Operator().execute(image);
        intb = image.getRawModel().asTupleBuffer(image);
        Assert.assertEquals(2, image.getExtent().getL(0));
        Assert.assertEquals(3, image.getExtent().getL(1));
        Assert.assertArrayEquals(new int[]{2,2,2}, (int[])intb.getTuple(new int[]{0,0}, null));
        Assert.assertArrayEquals(new int[]{1,1,1}, (int[])intb.getTuple(new int[]{0,1}, null));
        Assert.assertArrayEquals(new int[]{0,0,0}, (int[])intb.getTuple(new int[]{0,2}, null));
        Assert.assertArrayEquals(new int[]{5,5,5}, (int[])intb.getTuple(new int[]{1,0}, null));
        Assert.assertArrayEquals(new int[]{4,4,4}, (int[])intb.getTuple(new int[]{1,1}, null));
        Assert.assertArrayEquals(new int[]{3,3,3}, (int[])intb.getTuple(new int[]{1,2}, null));
    }
    
}
