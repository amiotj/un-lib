
package science.unlicense.api.image.process.paint;

import science.unlicense.impl.image.process.paint.FloodFillOperator;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.predicate.AbstractPredicate;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class FloodFillTest {
    
    /**
     * Check a full flood fill.
     */
    @Test
    public void allMatch(){
        
        final Image image = Images.createCustomBand(new Extent.Long(10, 10), 1, Primitive.TYPE_INT);
        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        sm.setTuple(new BBox(new double[]{0,0}, new double[]{10,10}), new int[]{16});
        
        final FloodFillOperator op = new FloodFillOperator();
        op.execute(image, new Vector(5, 5), new AbstractPredicate() {
            public Boolean evaluate(Object candidate) {
                return ((int[])candidate)[0] == 16;
            }
        }, new int[]{21});
        
        final int[] sample = new int[1];
        for(int y=0;y<10;y++){
            for(int x=0;x<10;x++){
                sm.getTuple(new int[]{x,y}, sample);
                Assert.assertEquals(x+","+y, 21,sample[0]);
            }
        }
        
    }
    
    /**
     * Check a square flood fill.
     */
    @Test
    public void squareMatch(){
        
        final Image image = Images.createCustomBand(new Extent.Long(10, 10), 1, Primitive.TYPE_INT);
        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        sm.setTuple(new BBox(new double[]{2,3}, new double[]{9,7}), new int[]{16});
        
        final FloodFillOperator op = new FloodFillOperator();
        op.execute(image, new Vector(5, 5), new AbstractPredicate() {
            public Boolean evaluate(Object candidate) {
                return ((int[])candidate)[0] == 16;
            }
        }, new int[]{21});
        
        final int[] sample = new int[1];
        for(int y=0;y<10;y++){
            for(int x=0;x<10;x++){
                sm.getTuple(new int[]{x,y}, sample);
                if(x<2||x>=9||y<3||y>=7){
                    Assert.assertEquals(x+","+y, 0,sample[0]);
                }else{
                    Assert.assertEquals(x+","+y, 21,sample[0]);
                }
            }
        }
        
    }
    
    
}
