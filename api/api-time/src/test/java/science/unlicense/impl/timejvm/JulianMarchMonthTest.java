package science.unlicense.impl.timejvm;

import science.unlicense.impl.time.JulianMonth;
import org.junit.Assert;
import org.junit.Test;
import static science.unlicense.impl.time.JulianMonth.APRIL;
import static science.unlicense.impl.time.JulianMonth.AUGUST;
import static science.unlicense.impl.time.JulianMonth.DECEMBER;
import static science.unlicense.impl.time.JulianMonth.FEBRUARY;
import static science.unlicense.impl.time.JulianMonth.JANUARY;
import static science.unlicense.impl.time.JulianMonth.JULY;
import static science.unlicense.impl.time.JulianMonth.JUNE;
import static science.unlicense.impl.time.JulianMonth.MARCH;
import static science.unlicense.impl.time.JulianMonth.MAY;
import static science.unlicense.impl.time.JulianMonth.NOVEMBER;
import static science.unlicense.impl.time.JulianMonth.OCTOBER;
import static science.unlicense.impl.time.JulianMonth.SEPTEMBER;

/**
 *
 * @author Samuel Andrés
 */
public class JulianMarchMonthTest {

    /**
     * Test of values method, of class JulianMarchMonth.
     */
    @Test
    public void testValues() {
        System.out.println("values");
        final JulianMonth[] expResult = {MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER, JANUARY, FEBRUARY};
        final JulianMonth[] result = JulianMonth.values();
        Assert.assertArrayEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class JulianMarchMonth.
     */
    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        Assert.assertEquals(MARCH, JulianMonth.valueOf("MARCH"));
        Assert.assertEquals(APRIL, JulianMonth.valueOf("APRIL"));
        Assert.assertEquals(MAY, JulianMonth.valueOf("MAY"));
        Assert.assertEquals(JUNE, JulianMonth.valueOf("JUNE"));
        Assert.assertEquals(JULY, JulianMonth.valueOf("JULY"));
        Assert.assertEquals(AUGUST, JulianMonth.valueOf("AUGUST"));
        Assert.assertEquals(SEPTEMBER, JulianMonth.valueOf("SEPTEMBER"));
        Assert.assertEquals(OCTOBER, JulianMonth.valueOf("OCTOBER"));
        Assert.assertEquals(NOVEMBER, JulianMonth.valueOf("NOVEMBER"));
        Assert.assertEquals(DECEMBER, JulianMonth.valueOf("DECEMBER"));
        Assert.assertEquals(JANUARY, JulianMonth.valueOf("JANUARY"));
        Assert.assertEquals(FEBRUARY, JulianMonth.valueOf("FEBRUARY"));
    }

    /**
     * Test of of method, of class JulianMarchMonth.
     */
    @Test
    public void testOf() {
        System.out.println("of");
        Assert.assertEquals(MARCH, JulianMonth.of(1));
        Assert.assertEquals(APRIL, JulianMonth.of(2));
        Assert.assertEquals(MAY, JulianMonth.of(3));
        Assert.assertEquals(JUNE, JulianMonth.of(4));
        Assert.assertEquals(JULY, JulianMonth.of(5));
        Assert.assertEquals(AUGUST, JulianMonth.of(6));
        Assert.assertEquals(SEPTEMBER, JulianMonth.of(7));
        Assert.assertEquals(OCTOBER, JulianMonth.of(8));
        Assert.assertEquals(NOVEMBER, JulianMonth.of(9));
        Assert.assertEquals(DECEMBER, JulianMonth.of(10));
        Assert.assertEquals(JANUARY, JulianMonth.of(11));
        Assert.assertEquals(FEBRUARY, JulianMonth.of(12));
    }

//    /**
//     * Test of from method, of class JulianMarchMonth.
//     */
//    @Test @Ignore
//    public void testFrom() {
//        System.out.println("from");
//        TemporalAccessor temporal = null;
//        JulianMarchMonth expResult = null;
//        JulianMarchMonth result = JulianMarchMonth.from(temporal);
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }

    /**
     * Test of getValue method, of class JulianMarchMonth.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        Assert.assertEquals(1, MARCH.getValue());
        Assert.assertEquals(2, APRIL.getValue());
        Assert.assertEquals(3, MAY.getValue());
        Assert.assertEquals(4, JUNE.getValue());
        Assert.assertEquals(5, JULY.getValue());
        Assert.assertEquals(6, AUGUST.getValue());
        Assert.assertEquals(7, SEPTEMBER.getValue());
        Assert.assertEquals(8, OCTOBER.getValue());
        Assert.assertEquals(9, NOVEMBER.getValue());
        Assert.assertEquals(10, DECEMBER.getValue());
        Assert.assertEquals(11, JANUARY.getValue());
        Assert.assertEquals(12, FEBRUARY.getValue());
    }

//    /**
//     * Test of getDisplayName method, of class JulianMarchMonth.
//     */
//    @Test @Ignore
//    public void testGetDisplayName() {
//        System.out.println("getDisplayName");
//        TextStyle style = TextStyle.FULL;
//        Locale locale = null;
//        JulianMarchMonth instance = null;
//        String expResult = "";
//        String result = MARCH.getDisplayName(style, Locale.ENGLISH);
//        Assert.assertEquals("MARCH", result);
//    }

//    /**
//     * Test of range method, of class JulianMarchMonth.
//     */
//    @Test @Ignore
//    public void testRange() {
//        System.out.println("range");
//        Assert.assertEquals(ValueRange.of(1, 31), MARCH.range(MONTH_OF_YEAR));
//    }

    /**
     * Test of plus method, of class JulianMarchMonth.
     */
    @Test
    public void testPlus() {
        System.out.println("plus");
        Assert.assertEquals(APRIL, MARCH.plus(1));
        Assert.assertEquals(MAY, APRIL.plus(1));
        Assert.assertEquals(JUNE, MAY.plus(1));
        Assert.assertEquals(JULY, JUNE.plus(1));
        Assert.assertEquals(AUGUST, JULY.plus(1));
        Assert.assertEquals(SEPTEMBER, AUGUST.plus(1));
        Assert.assertEquals(OCTOBER, SEPTEMBER.plus(1));
        Assert.assertEquals(NOVEMBER, OCTOBER.plus(1));
        Assert.assertEquals(DECEMBER, NOVEMBER.plus(1));
        Assert.assertEquals(JANUARY, DECEMBER.plus(1));
        Assert.assertEquals(FEBRUARY, JANUARY.plus(1));
        Assert.assertEquals(MARCH, FEBRUARY.plus(1));
    }

    /**
     * Test of minus method, of class JulianMarchMonth.
     */
    @Test
    public void testMinus() {
        System.out.println("minus");
        Assert.assertEquals(FEBRUARY, MARCH.minus(1));
        Assert.assertEquals(MARCH, APRIL.minus(1));
        Assert.assertEquals(APRIL, MAY.minus(1));
        Assert.assertEquals(MAY, JUNE.minus(1));
        Assert.assertEquals(JUNE, JULY.minus(1));
        Assert.assertEquals(JULY, AUGUST.minus(1));
        Assert.assertEquals(AUGUST, SEPTEMBER.minus(1));
        Assert.assertEquals(SEPTEMBER, OCTOBER.minus(1));
        Assert.assertEquals(OCTOBER, NOVEMBER.minus(1));
        Assert.assertEquals(NOVEMBER, DECEMBER.minus(1));
        Assert.assertEquals(DECEMBER, JANUARY.minus(1));
        Assert.assertEquals(JANUARY, FEBRUARY.minus(1));
    }

    /**
     * Test of length method, of class JulianMarchMonth.
     */
    @Test
    public void testLength() {
        System.out.println("length");
        Assert.assertEquals(31, MARCH.length(true));
        Assert.assertEquals(30, APRIL.length(true));
        Assert.assertEquals(31, MAY.length(true));
        Assert.assertEquals(30, JUNE.length(true));
        Assert.assertEquals(31, JULY.length(true));
        Assert.assertEquals(31, AUGUST.length(true));
        Assert.assertEquals(30, SEPTEMBER.length(true));
        Assert.assertEquals(31, OCTOBER.length(true));
        Assert.assertEquals(30, NOVEMBER.length(true));
        Assert.assertEquals(31, DECEMBER.length(true));
        Assert.assertEquals(31, JANUARY.length(true));
        Assert.assertEquals(29, FEBRUARY.length(true));

        Assert.assertEquals(31, MARCH.length(false));
        Assert.assertEquals(30, APRIL.length(false));
        Assert.assertEquals(31, MAY.length(false));
        Assert.assertEquals(30, JUNE.length(false));
        Assert.assertEquals(31, JULY.length(false));
        Assert.assertEquals(31, AUGUST.length(false));
        Assert.assertEquals(30, SEPTEMBER.length(false));
        Assert.assertEquals(31, OCTOBER.length(false));
        Assert.assertEquals(30, NOVEMBER.length(false));
        Assert.assertEquals(31, DECEMBER.length(false));
        Assert.assertEquals(31, JANUARY.length(false));
        Assert.assertEquals(28, FEBRUARY.length(false));
    }

    /**
     * Test of minLength method, of class JulianMarchMonth.
     */
    @Test
    public void testMinLength() {
        System.out.println("minLength");
        Assert.assertEquals(31, MARCH.minLength());
        Assert.assertEquals(30, APRIL.minLength());
        Assert.assertEquals(31, MAY.minLength());
        Assert.assertEquals(30, JUNE.minLength());
        Assert.assertEquals(31, JULY.minLength());
        Assert.assertEquals(31, AUGUST.minLength());
        Assert.assertEquals(30, SEPTEMBER.minLength());
        Assert.assertEquals(31, OCTOBER.minLength());
        Assert.assertEquals(30, NOVEMBER.minLength());
        Assert.assertEquals(31, DECEMBER.minLength());
        Assert.assertEquals(31, JANUARY.minLength());
        Assert.assertEquals(28, FEBRUARY.minLength());
    }

    /**
     * Test of maxLength method, of class JulianMarchMonth.
     */
    @Test
    public void testMaxLength() {
        System.out.println("maxLength");
        Assert.assertEquals(31, MARCH.maxLength());
        Assert.assertEquals(30, APRIL.maxLength());
        Assert.assertEquals(31, MAY.maxLength());
        Assert.assertEquals(30, JUNE.maxLength());
        Assert.assertEquals(31, JULY.maxLength());
        Assert.assertEquals(31, AUGUST.maxLength());
        Assert.assertEquals(30, SEPTEMBER.maxLength());
        Assert.assertEquals(31, OCTOBER.maxLength());
        Assert.assertEquals(30, NOVEMBER.maxLength());
        Assert.assertEquals(31, DECEMBER.maxLength());
        Assert.assertEquals(31, JANUARY.maxLength());
        Assert.assertEquals(29, FEBRUARY.maxLength());
    }

    /**
     * Test of firstDayOfYear method, of class JulianMarchMonth.
     */
    @Test
    public void testFirstDayOfYear() {
        System.out.println("firstDayOfYear");
        Assert.assertEquals(1, MARCH.firstDayOfYear(true));
    }

    /**
     * Test of firstMonthOfQuarter method, of class JulianMarchMonth.
     */
    @Test
    public void testFirstMonthOfQuarter() {
        System.out.println("firstMonthOfQuarter");
        Assert.assertEquals(MARCH, MARCH.firstMonthOfQuarter());
        Assert.assertEquals(MARCH, APRIL.firstMonthOfQuarter());
        Assert.assertEquals(MARCH, MAY.firstMonthOfQuarter());
        Assert.assertEquals(JUNE, JUNE.firstMonthOfQuarter());
        Assert.assertEquals(JUNE, JULY.firstMonthOfQuarter());
        Assert.assertEquals(JUNE, AUGUST.firstMonthOfQuarter());
        Assert.assertEquals(SEPTEMBER, SEPTEMBER.firstMonthOfQuarter());
        Assert.assertEquals(SEPTEMBER, OCTOBER.firstMonthOfQuarter());
        Assert.assertEquals(SEPTEMBER, NOVEMBER.firstMonthOfQuarter());
        Assert.assertEquals(DECEMBER, DECEMBER.firstMonthOfQuarter());
        Assert.assertEquals(DECEMBER, JANUARY.firstMonthOfQuarter());
        Assert.assertEquals(DECEMBER, FEBRUARY.firstMonthOfQuarter());
    }
}
