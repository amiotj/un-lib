package science.unlicense.impl.timejvm;

import science.unlicense.impl.time.JulianUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Samuel Andrés
 */
public class JulianMarchUtilsTest {

    /**
     *
     */
    @Test
    public void testGetMarchMonth0() {
        for(int i = 0; i<=365; i++){
            final int marchMonth0 = JulianUtils.getMarchMonth0(i);
            if(i<31) // mars
                Assert.assertEquals(0, marchMonth0);
            else if(i<61) // avril
                Assert.assertEquals(1, marchMonth0);
            else if(i<92) // mai
                Assert.assertEquals(2, marchMonth0);
            else if(i<122) // juin
                Assert.assertEquals(3, marchMonth0);
            else if(i<153) // juillet
                Assert.assertEquals(4, marchMonth0);
            else if(i<184) // août
                Assert.assertEquals(5, marchMonth0);
            else if(i<214) // septembre
                Assert.assertEquals(6, marchMonth0);
            else if(i<245) // octobre
                Assert.assertEquals(7, marchMonth0);
            else if(i<275) // novembre
                Assert.assertEquals(8, marchMonth0);
            else if(i<306) // décembre
                Assert.assertEquals(9, marchMonth0);
            else if(i<337) // janvier
                Assert.assertEquals(10, marchMonth0);
            else if(i<366) // février
                Assert.assertEquals(11, marchMonth0);
        }
    }
}
