
package science.unlicense.impl.time.format;

import science.unlicense.impl.time.format.ISO8601Parser;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.ParseException;

/**
 *
 * @author Johann Sorel
 */
public class ISO8601ParserTest {
    
    @Test
    public void yyyy() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
        
    @Test
    public void yyyyMM() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("154011"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals((Integer)11,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyy_MM() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540-11"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals((Integer)11,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyyMMdd() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("15401130"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals((Integer)11,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals((Integer)30,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyy_MM_dd() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540-11-30"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals((Integer)11,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals((Integer)30,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    
    @Test
    public void yyyyjjj() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540235"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals((Integer)235,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyy_jjj() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540-235"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals((Integer)235,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyyWww() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540W12"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals((Integer)12,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyy_Www() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540-W12"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals((Integer)12,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyyWwwj() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540W125"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals((Integer)12,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals((Integer)5,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void yyyy_Www_j() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("1540-W12-5"));
        Assert.assertEquals((Integer)1540,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals((Integer)12,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals((Integer)5,parser.dayOfWeek);
        Assert.assertEquals(null,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THH() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THHmm() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T1459"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THH_mm() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14:59"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THHmmSS() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T145907"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals((Integer)07,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THH_mm_SS() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14:59:07"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals((Integer)07,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THHmmSSsssss() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14590712345"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals((Integer)07,parser.second);
        Assert.assertEquals((Double)0.12345,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THH_mm_SS_sssss() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14:59:07,12345"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals((Integer)07,parser.second);
        Assert.assertEquals((Double)0.12345,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
        
        parser.parse(new Chars("T14:59:07.98765"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals((Integer)59,parser.minute);
        Assert.assertEquals((Integer)07,parser.second);
        Assert.assertEquals((Double)0.98765,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THHZ() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14Z"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(true,parser.timeZone0);
        Assert.assertEquals(null,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THHzzz() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14+07"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals((Integer)07,parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
        
        parser.parse(new Chars("T14-07"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(Integer.valueOf(-7),parser.timeZoneHour);
        Assert.assertEquals(null,parser.timeZoneMinute);
    }
    
    @Test
    public void THHzzzzz() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14+0746"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals((Integer)07,parser.timeZoneHour);
        Assert.assertEquals((Integer)46,parser.timeZoneMinute);
        
        parser.parse(new Chars("T14-0746"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(Integer.valueOf(-7),parser.timeZoneHour);
        Assert.assertEquals((Integer)46,parser.timeZoneMinute);
    }
    
    @Test
    public void THHzzzzzz() throws ParseException {
        final ISO8601Parser parser = new ISO8601Parser();
        parser.parse(new Chars("T14+07:46"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals((Integer)07,parser.timeZoneHour);
        Assert.assertEquals((Integer)46,parser.timeZoneMinute);
        
        parser.parse(new Chars("T14-07:46"));
        Assert.assertEquals(null,parser.year);
        Assert.assertEquals(null,parser.month);
        Assert.assertEquals(null,parser.week);
        Assert.assertEquals(null,parser.dayOfYear);
        Assert.assertEquals(null,parser.dayOfMonth);
        Assert.assertEquals(null,parser.dayOfWeek);
        Assert.assertEquals((Integer)14,parser.hour);
        Assert.assertEquals(null,parser.minute);
        Assert.assertEquals(null,parser.second);
        Assert.assertEquals(null,parser.secondFraction);
        Assert.assertEquals(null,parser.timeZone0);
        Assert.assertEquals(Integer.valueOf(-7),parser.timeZoneHour);
        Assert.assertEquals((Integer)46,parser.timeZoneMinute);
    }
    
}
