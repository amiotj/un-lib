package science.unlicense.impl.timejvm;

import science.unlicense.impl.time.GregorianDate;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.time.GregorianUtils;

/**
 *
 * @author Samuel Andrés
 */
public class GregorianDateTest {

//    @Test
//    public void testOf0() {
//        final LocalDate local = LocalDate.of(2016, Month.JANUARY, 16);
//        final GregorianDate gregorian = GregorianDate.ofEpochDay(local.toEpochDay());
//        Assert.assertEquals(16, gregorian.getDayOfMonth());
//        Assert.assertEquals(Month.JANUARY, gregorian.getMonth());
//        Assert.assertEquals(2016, gregorian.getYear());
//    }
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testOfEpochDay() {
//        for(long i = -1000000l; i<=1000000l; i++){
//            final LocalDate local = LocalDate.ofEpochDay(i);
//            final GregorianDate gregorian = GregorianDate.ofEpochDay(i);
//            Assert.assertEquals(local.getDayOfMonth(), gregorian.getDayOfMonth());
//            Assert.assertEquals(local.getMonth(), gregorian.getMonth());
//            Assert.assertEquals(local.getYear(), gregorian.getYear());
//        }
//    }
//
//    @Test
//    public void testToEpochDay() {
//        Assert.assertEquals(0, GregorianDate.of(1970, 1, 1).toEpochDay());
//        Assert.assertEquals(-GregorianUtils.DAYS_0000_TO_1970, GregorianDate.of(0000, 1, 1).toEpochDay());
//        Assert.assertEquals(-GregorianUtils.DAYS_0000_TO_1970-365, GregorianDate.of(-1, 1, 1).toEpochDay());
//    }
//
//    @Test
//    public void testDayOfYear() {
//        Assert.assertEquals(1, GregorianDate.of(2016, 1, 1).getDayOfYear());
//        Assert.assertEquals(61, GregorianDate.of(2016, 3, 1).getDayOfYear());
//        Assert.assertEquals(1, GregorianDate.of(2015, 1, 1).getDayOfYear());
//        Assert.assertEquals(60, GregorianDate.of(2015, 3, 1).getDayOfYear());
//
//        for(long i = -1000000l; i<=1000000l; i++){
//            Assert.assertEquals(LocalDate.ofEpochDay(i).getDayOfYear(), GregorianDate.ofEpochDay(i).getDayOfYear());
//        }
//    }
//
//    @Test
//    public void testDayOfYear2() {
//        Assert.assertEquals(1, GregorianDate.of(2016, 1, 1).getDayOfYear2());
//        Assert.assertEquals(61, GregorianDate.of(2016, 3, 1).getDayOfYear2());
//        Assert.assertEquals(1, GregorianDate.of(2015, 1, 1).getDayOfYear2());
//        Assert.assertEquals(60, GregorianDate.of(2015, 3, 1).getDayOfYear2());
//
//        for(long i = -1000000l; i<=1000000l; i++){
//            Assert.assertEquals(LocalDate.ofEpochDay(i).getDayOfYear(), GregorianDate.ofEpochDay(i).getDayOfYear2());
//        }
//    }
//
//    @Test
//    public void testDayOfWeek() {
//        Assert.assertEquals(DayOfWeek.THURSDAY, GregorianDate.of(1970, 1, 1).getDayOfWeek());
//        Assert.assertEquals(LocalDate.now().getDayOfWeek(), GregorianDate.from(LocalDate.now()).getDayOfWeek());
//    }
//
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testLengthOfMonth() {
//        Assert.assertEquals(31, GregorianDate.of(2016, 1, 1).lengthOfMonth());
//
//        Assert.assertEquals(29, GregorianDate.of(2016, 2, 1).lengthOfMonth());
//        Assert.assertEquals(28, GregorianDate.of(2015, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, GregorianDate.of(2000, 2, 1).lengthOfMonth());
//        Assert.assertEquals(28, GregorianDate.of(1900, 2, 1).lengthOfMonth());
//        Assert.assertEquals(28, GregorianDate.of(1700, 2, 1).lengthOfMonth());
//        Assert.assertEquals(29, GregorianDate.of(1600, 2, 1).lengthOfMonth());
//        Assert.assertEquals(28, GregorianDate.of(1500, 2, 1).lengthOfMonth());
//
//        Assert.assertEquals(31, GregorianDate.of(2016, 3, 1).lengthOfMonth());
//        Assert.assertEquals(30, GregorianDate.of(2016, 4, 1).lengthOfMonth());
//        Assert.assertEquals(31, GregorianDate.of(2016, 5, 1).lengthOfMonth());
//        Assert.assertEquals(30, GregorianDate.of(2016, 6, 1).lengthOfMonth());
//        Assert.assertEquals(31, GregorianDate.of(2016, 7, 1).lengthOfMonth());
//        Assert.assertEquals(31, GregorianDate.of(2016, 8, 1).lengthOfMonth());
//        Assert.assertEquals(30, GregorianDate.of(2016, 9, 1).lengthOfMonth());
//        Assert.assertEquals(31, GregorianDate.of(2016, 10, 1).lengthOfMonth());
//        Assert.assertEquals(30, GregorianDate.of(2016, 11, 1).lengthOfMonth());
//        Assert.assertEquals(31, GregorianDate.of(2016, 12, 1).lengthOfMonth());
//    }
//
//    /**
//     * Test of of method, of class GregorianDate.
//     */
//    @Test
//    public void testLengthOfYear() {
//        Assert.assertEquals(366, GregorianDate.of(2000, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, GregorianDate.of(2000, 1, 1).lengthOfYear());
//        Assert.assertEquals(365, GregorianDate.of(1900, 12, 31).lengthOfYear());
//        Assert.assertEquals(365, GregorianDate.of(1900, 1, 1).lengthOfYear());
//        Assert.assertEquals(366, GregorianDate.of(2016, 12, 31).lengthOfYear());
//        Assert.assertEquals(366, GregorianDate.of(2016, 1, 1).lengthOfYear());
//        Assert.assertEquals(365, GregorianDate.of(2015, 12, 31).lengthOfYear());
//        Assert.assertEquals(365, GregorianDate.of(2015, 1, 1).lengthOfYear());
//    }

}
