
package science.unlicense.impl.time;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.time.Duration;
import science.unlicense.api.unit.Unit;
import science.unlicense.api.unit.Units;
import science.unlicense.impl.number.LargeDecimal;
import science.unlicense.impl.number.LargeInteger;

/**
 *
 * @author Johann Sorel
 */
public class UnitDuration implements Duration{

    private final Unit unit;
    private final LargeInteger length;

    public UnitDuration(Unit unit, LargeInteger length) {
        this.unit = unit;
        this.length = length;
    }

    public Unit getUnit() {
        return unit;
    }

    public LargeInteger getLength() {
        return length;
    }
    
    public LargeDecimal getLength(Unit unit) {
        if(unit==this.unit){
            throw new UnimplementedException("TODO");
        }
        Transform transform = Units.getTransform(this.unit, unit);
        throw new UnimplementedException("TODO");
    }

}
