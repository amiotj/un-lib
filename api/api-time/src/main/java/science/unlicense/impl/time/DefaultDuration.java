
package science.unlicense.impl.time;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.time.Calendar;
import science.unlicense.api.time.Duration;
import science.unlicense.api.unit.Unit;
import science.unlicense.impl.number.LargeDecimal;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDuration implements Duration{

    private final Calendar calendar;
    private final Dictionary components;

    public DefaultDuration(Calendar calendar, Dictionary components) {
        this.calendar = calendar;
        this.components = components;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public Dictionary getComponents() {
        return components;
    }

    public LargeDecimal getLength(Unit unit) {
        throw new UnimplementedException("Not supported yet.");
    }

}
