package science.unlicense.impl.time;

import science.unlicense.api.time.TemporalException;

/**
 *
 * @author Samuel Andrés
 */
public final class TemporalUtils {

    /**
     *
     * @param start
     * @param end
     * @return
     */
    public static long daysBetween(final YearMonthDayDate start, final YearMonthDayDate end) {
        return end.toEpochDay() - start.toEpochDay();
    }

    /**
     *
     * @param start
     * @param end
     * @return
     */
    public static long monthsBetween(final YearMonthDayDate start, final YearMonthDayDate end) {
        final long packed1 = JulianUtils.getProlepticMonth(start.getYear(), start.getMonthValue()) * 32L + start.getDayOfMonth();
        final long packed2 = JulianUtils.getProlepticMonth(end.getYear(), end.getMonthValue()) * 32L + end.getDayOfMonth();
        return (packed2 - packed1) / 32;
    }

    /**
     *
     * @param <S>
     * @param <E>
     * @param startInclusive
     * @param endExclusive
     * @param unit
     * @return
     */
    public static long between(YearMonthDayDate startInclusive, YearMonthDayDate endExclusive, int unit) {
        switch (unit) {
            case RangeUnit.DAYS: return daysBetween(startInclusive, endExclusive);
            case RangeUnit.WEEKS: return daysBetween(startInclusive, endExclusive) / 7;
            case RangeUnit.MONTHS: return monthsBetween(startInclusive, endExclusive);
            case RangeUnit.YEARS: return monthsBetween(startInclusive, endExclusive) / 12;
        }
        throw new TemporalException("Unsupported unit: " + unit);
    }

    /**
     *
     * @param firstDate
     * @param otherDate
     * @return
     */
    public static int compare(final YearMonthDayDate firstDate, final YearMonthDayDate otherDate) {
        int cmp = (firstDate.getYear() - otherDate.getYear());
        if (cmp == 0) {
            cmp = (firstDate.getMonthValue() - otherDate.getMonthValue());
            if (cmp == 0) {
                cmp = (firstDate.getDayOfMonth() - otherDate.getDayOfMonth());
            }
        }
        return cmp;
    }

}
