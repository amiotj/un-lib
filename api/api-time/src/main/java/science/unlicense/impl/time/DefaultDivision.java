
package science.unlicense.impl.time;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.time.Division;

/**
 * Default division implementation.
 * 
 * @author Johann Sorel
 */
public class DefaultDivision implements Division {

    private final CharArray name;

    public DefaultDivision(CharArray name) {
        this.name = name;
    }

    public CharArray getName() {
        return name;
    }

}
