
package science.unlicense.impl.time;

/**
 *
 * @author Johann Sorel
 */
public class PetrinianUtils {
    
    
    public static boolean isLeapYear(long prolepticYear) {
        return (prolepticYear & 3) == 0;
    }
    
}
