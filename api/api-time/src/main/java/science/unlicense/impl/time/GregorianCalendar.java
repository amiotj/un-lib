
package science.unlicense.impl.time;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.time.Calendar;
import science.unlicense.api.time.ChronologicEvent;
import science.unlicense.api.time.Date;
import science.unlicense.api.time.Division;
import science.unlicense.api.time.TimeConstants;

/**
 *
 * @author Johann Sorel
 */
public class GregorianCalendar implements Calendar {

    public static final GregorianCalendar INSTANCE = new GregorianCalendar();

    public static final Division YEAR        = new DefaultDivision(new Chars("year"));
    public static final Division MONTH       = new DefaultDivision(new Chars("month"));
    public static final Division DAY         = new DefaultDivision(new Chars("day"));
    public static final Division HOUR        = new DefaultDivision(new Chars("hour"));
    public static final Division MINUTE      = new DefaultDivision(new Chars("minute"));
    public static final Division SECOND      = new DefaultDivision(new Chars("second"));
    public static final Division MILLISECOND = new DefaultDivision(new Chars("millisecond"));
    public static final Division NANOSECOND  = new DefaultDivision(new Chars("nanosecond"));
    public static final Division TIMEZONE    = new DefaultDivision(new Chars("timezone"));

    private final Chars name = new Chars("Gregorian");

    @Override
    public CharArray getName() {
        return name;
    }

    @Override
    public ChronologicEvent getAnchor() {
        return TimeConstants.COMMON_ERA;
    }

    @Override
    public Division[] getDivisions() {
        return new Division[]{
            YEAR,
            MONTH,
            DAY,
            HOUR,
            MINUTE,
            SECOND,
            MILLISECOND,
            NANOSECOND,
            TIMEZONE
        };
    }

    @Override
    public Date toDate(ChronologicEvent event) {
        throw new UnimplementedException("Not supported yet.");
    }

    /**
     * Calculate if a year is a leap year.
     *
     * @param year
     * @return true if it is a leap year
     */
    public static boolean isLeapYear(long year){
        //NOTE what about 1582 ? do we start the calendar at this date ?
        if (year%4!=0) return false;
        else if (year%400!=0) return false;
        else if (year%100!=0) return true;
        else return true;
    }


}
