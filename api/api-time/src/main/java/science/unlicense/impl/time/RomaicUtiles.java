
package science.unlicense.impl.time;

/**
 *
 * @author Johann Sorel
 */
public class RomaicUtiles {
    
    public static boolean isLeapYear(long prolepticYear) {
        return (prolepticYear & 3) == 0;
    }
    
}
