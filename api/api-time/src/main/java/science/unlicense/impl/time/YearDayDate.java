package science.unlicense.impl.time;

/**
 * Classe conteneur d'un jour dans une année donnée.
 *
 * @author Samuel Andrés
 */
public class YearDayDate {

    final int dayOfYear;
    final long year;

    public YearDayDate(int dayOfYear, long year) {
        this.dayOfYear = dayOfYear;
        this.year = year;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public long getYear() {
        return year;
    }
}
