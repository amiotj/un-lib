
package science.unlicense.impl.time;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.time.ChronologicEvent;
import science.unlicense.api.time.Duration;

/**
 *
 * @author Johann Sorel
 */
public class DefaultChronologicEvent implements ChronologicEvent {

    private final ChronologicEvent anchor;
    private final Duration duration;

    public DefaultChronologicEvent(ChronologicEvent anchor, Duration duration) {
        this.anchor = anchor;
        this.duration = duration;
    }

    @Override
    public ChronologicEvent getAnchor() {
        return anchor;
    }

    @Override
    public Duration getAnchorDistance() {
        return duration;
    }

    @Override
    public DefaultChronologicEvent toAbsolute(ChronologicEvent anchor) {
        if(anchor==this.anchor) return this;
        throw new UnimplementedException("Not supported yet.");
    }

}
