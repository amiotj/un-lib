package science.unlicense.impl.time;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.time.TemporalException;
import science.unlicense.impl.time.GregorianUtils;
import science.unlicense.impl.time.YearMonthDayDate;
import science.unlicense.impl.time.YearDayDate;
import science.unlicense.impl.time.JulianMonth;
import science.unlicense.impl.time.JulianUtils;

public final class GregorianDate extends YearMonthDayDate {


    private GregorianDate(int year, int month, int dayOfMonth) {
        super(year, month, dayOfMonth);
    }

//    private int get0(TemporalField field) {
//        switch ((ChronoField) field) {
//            case DAY_OF_WEEK: return getDayOfWeek().getValue();
//            case ALIGNED_DAY_OF_WEEK_IN_MONTH: return ((day - 1) % 7) + 1;
//            case ALIGNED_DAY_OF_WEEK_IN_YEAR: return ((getDayOfYear() - 1) % 7) + 1;
//            case DAY_OF_MONTH: return day;
//            case DAY_OF_YEAR: return getDayOfYear();
//            case EPOCH_DAY: throw new UnsupportedTemporalTypeException("Invalid field 'EpochDay' for get() method, use getLong() instead");
//            case ALIGNED_WEEK_OF_MONTH: return ((day - 1) / 7) + 1;
//            case ALIGNED_WEEK_OF_YEAR: return ((getDayOfYear() - 1) / 7) + 1;
//            case MONTH_OF_YEAR: return month;
//            case PROLEPTIC_MONTH: throw new UnsupportedTemporalTypeException("Invalid field 'ProlepticMonth' for get() method, use getLong() instead");
//            case YEAR_OF_ERA: return (year >= 1 ? year : 1 - year);
//            case YEAR: return year;
//            case ERA: return (year >= 1 ? 1 : 0);
//        }
//        throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
//    }

    @Override
    public long toEpochDay() {
        final long y = year;

        // Nombre de jours dans les années écoulées depuis l'année 0000 en comptant 365 jours par an.
        long total = 365 * y;

        // Si on est après l'an 0000
        if (y >= 0) {
            // On ajoute un jour par année bissextile (1 année sur 4) en tenant compte de l'année en cours (d'où le +3)
            // …sauf les années séculaires (1 année sur 100) en tenant compte de l'année en cours (d'où le +100)
            // …mais en maintenant une année séculaire bissextile tous les 4 siècles (1 année sur 400) en tenant compte e l'année en cours (d'où le +400)
            total += (y + 3) / 4 - (y + 99) / 100 + (y + 399) / 400;
        }
        // Sinon
        else {
            total -= y / -4 - y / -100 + y / -400;
        }

        // On ajoute le nombre de jours écoulés dans l'année jusqu'au mois en cours.
        total += getDayOfYear() - 1; // Pourquoi (-1) ?

        // On se ramène à l'époque Java
        return total - GregorianUtils.DAYS_0000_TO_1970;
    }

    public int getDayOfWeek() {
        // Le 1-1-1970 grégorien était un jeudi.
        int dow0 = (int) (toEpochDay() + 3) % 7;
        return dow0 + 1;
    }

    public boolean isLeapYear() {
        return GregorianUtils.isLeapYear(year);
    }

//    public int getDayOfYear() {
//        return getMonth().firstDayOfYear(isLeapYear()) + day - 1;
//    }

    /**
     * Autre altorithme pour calculer le jour de l'année.
     * @return
     */
    public int getDayOfYear2() {
        // On ajoute le nombre de jours écoulés dans l'année jusqu'au mois en cours.
        int total = ((367 * month - 362) / 12);


        // On ajoute le nombre de jours écoulés dans le mois courant.
        total += day;

        if (month > 2) {
            total--;
            if (isLeapYear() == false) {
                total--;
            }
        }

        return total;
    }

//    @Override
//    public GregorianDate with(TemporalField field, long newValue) {
//        if (field instanceof ChronoField) {
//            ChronoField f = (ChronoField) field;
//            f.checkValidValue(newValue);
//            switch (f) {
//                case DAY_OF_WEEK: return plusDays(newValue - getDayOfWeek().getValue());
//                case ALIGNED_DAY_OF_WEEK_IN_MONTH: return plusDays(newValue - getLong(ALIGNED_DAY_OF_WEEK_IN_MONTH));
//                case ALIGNED_DAY_OF_WEEK_IN_YEAR: return plusDays(newValue - getLong(ALIGNED_DAY_OF_WEEK_IN_YEAR));
//                case DAY_OF_MONTH: return withDayOfMonth((int) newValue);
//                case DAY_OF_YEAR: return withDayOfYear((int) newValue);
//                case EPOCH_DAY: return GregorianDate.ofEpochDay(newValue);
//                case ALIGNED_WEEK_OF_MONTH: return plusWeeks(newValue - getLong(ALIGNED_WEEK_OF_MONTH));
//                case ALIGNED_WEEK_OF_YEAR: return plusWeeks(newValue - getLong(ALIGNED_WEEK_OF_YEAR));
//                case MONTH_OF_YEAR: return withMonth((int) newValue);
//                case PROLEPTIC_MONTH: return plusMonths(newValue - JulianUtils.getProlepticMonth(year, month));
//                case YEAR_OF_ERA: return withYear((int) (year >= 1 ? newValue : 1 - newValue));
//                case YEAR: return withYear((int) newValue);
//                case ERA: return (getLong(ERA) == newValue ? this : withYear(1 - year));
//            }
//            throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
//        }
//        return field.adjustInto(this, newValue);
//    }

    public GregorianDate withYear(int year) {
        if (this.year == year) {
            return this;
        }
        return resolvePreviousValid(year, month, day);
    }

    public GregorianDate withMonth(int month) {
        if (this.month == month) {
            return this;
        }
        return resolvePreviousValid(year, month, day);
    }

    public GregorianDate withDayOfMonth(int dayOfMonth) {
        if (this.day == dayOfMonth) {
            return this;
        }
        return create(year, month, dayOfMonth);
    }

    public GregorianDate withDayOfYear(int dayOfYear) {
        if (this.getDayOfYear() == dayOfYear) {
            return this;
        }
        return ofYearDay(year, dayOfYear);
    }

    private static GregorianDate resolvePreviousValid(int year, int month, int day) {
        switch (month) {
            case 2:
                day = Math.min(day, GregorianUtils.isLeapYear(year) ? 29 : 28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = Math.min(day, 30);
                break;
        }
        return new GregorianDate(year, month, day);
    }

    public GregorianDate plusYears(long yearsToAdd) {
        if (yearsToAdd == 0) {
            return this;
        }
        int newYear = (int) (year + yearsToAdd);  // safe overflow
        return resolvePreviousValid(newYear, month, day);
    }

    public GregorianDate plusMonths(long monthsToAdd) {
        if (monthsToAdd == 0) {
            return this;
        }
        long monthCount = year * 12L + (month - 1);
        long calcMonths = monthCount + monthsToAdd;  // safe overflow
        int newYear = (int) calcMonths / 12;
        int newMonth = (int) (calcMonths % 12) + 1;
        return resolvePreviousValid(newYear, newMonth, day);
    }

    public GregorianDate plusWeeks(long weeksToAdd) {
        return plusDays(weeksToAdd * 7);
    }

    public GregorianDate plusDays(long daysToAdd) {
        if (daysToAdd == 0) {
            return this;
        }
        long mjDay = toEpochDay() + daysToAdd;
        return GregorianDate.ofEpochDay(mjDay);
    }

    public GregorianDate minusYears(long yearsToSubtract) {
        return (yearsToSubtract == Long.MIN_VALUE ? plusYears(Long.MAX_VALUE).plusYears(1) : plusYears(-yearsToSubtract));
    }

    public GregorianDate minusMonths(long monthsToSubtract) {
        return (monthsToSubtract == Long.MIN_VALUE ? plusMonths(Long.MAX_VALUE).plusMonths(1) : plusMonths(-monthsToSubtract));
    }

    public GregorianDate minusWeeks(long weeksToSubtract) {
        return (weeksToSubtract == Long.MIN_VALUE ? plusWeeks(Long.MAX_VALUE).plusWeeks(1) : plusWeeks(-weeksToSubtract));
    }

    public GregorianDate minusDays(long daysToSubtract) {
        return (daysToSubtract == Long.MIN_VALUE ? plusDays(Long.MAX_VALUE).plusDays(1) : plusDays(-daysToSubtract));
    }


    //==========================================================================
    // METHODES STATIQUES
    //==========================================================================

    public static GregorianDate of(int year, int month, int dayOfMonth) {
        return create(year, month, dayOfMonth);
    }

    public static GregorianDate ofYearDay(int year, int dayOfYear) {
        boolean leap = GregorianUtils.isLeapYear(year);
        if (dayOfYear == 366 && leap == false) {
            throw new TemporalException("Invalid date 'DayOfYear 366' as '" + year + "' is not a leap year");
        }
        throw new UnimplementedException();
//        Month moy = Month.of((dayOfYear - 1) / 31 + 1);
//        int monthEnd = moy.firstDayOfYear(leap) + moy.length(leap) - 1;
//        if (dayOfYear > monthEnd) {
//            moy = moy.plus(1);
//        }
//        int dom = dayOfYear - moy.firstDayOfYear(leap) + 1;
//        return new GregorianDate(year, moy.getValue(), dom);
    }

    public static GregorianDate ofEpochDay(long epochDay) {
        // Nombre de jours écoulés depuis l'époque 01/01/0000 grégorien
        long zeroDay = epochDay + GregorianUtils.DAYS_0000_TO_1970;

        /*
        Numéro de jour dans l'année commençant le 1er mars.
        Ce calcul nécessite de décaler le décompte des jours début mars.
        On enlève donc 60 jours correspondant aux 31 jours de janvier et 29
        jours de février 0000 qui est bisextile.
        */
        final YearDayDate marchDoy = GregorianUtils.toDayOfYear(zeroDay-60);

        //Retour au calendrier débutant en janvier.
        final YearMonthDayDate date = JulianUtils.translate(marchDoy, JulianMonth.JANUARY);

        // check year now we are certain it is correct
        int year = date.getYear();
        return new GregorianDate(year, date.getMonthValue(), date.getDayOfMonth());
    }

    /**
     *
     * @param year
     * @param month
     * @param dayOfMonth
     * @return
     */
    private static GregorianDate create(int year, int month, int dayOfMonth) {
        if (dayOfMonth > 28) {
            int dom = 31;
            switch (month) {
                case 2:
                    dom = (GregorianUtils.isLeapYear(year) ? 29 : 28);
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    dom = 30;
                    break;
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 29) {
                    throw new TemporalException("Invalid date 'February 29' as '" + year + "' is not a leap year");
                } else {
                    throw new TemporalException("Invalid date '" + month + " " + dayOfMonth + "'");
                }
            }
        }
        return new GregorianDate(year, month, dayOfMonth);
    }

}
