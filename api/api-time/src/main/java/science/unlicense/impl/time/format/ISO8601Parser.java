
package science.unlicense.impl.time.format;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.ParseException;

/**
 *
 * @author Johann Sorel
 */
public class ISO8601Parser {
    
    private CharIterator ite;

    public Integer year;
    public Integer month;
    public Integer week;
    public Integer dayOfYear;
    public Integer dayOfMonth;
    public Integer dayOfWeek;
    public Integer hour;
    public Integer minute;
    public Integer second;
    public Double secondFraction;
    public Boolean timeZone0;
    public Integer timeZoneHour;
    public Integer timeZoneMinute;
    
    public void parse(Chars text) throws ParseException {        
        ite = text.createIterator();
        
        if (!ite.hasNext()) throw new ParseException();
            
        int c = ite.peekToUnicode();
        if (c=='T') {
            //skip T
            ite.skip();
            parseTime();
        } else if (isDigit(c)) {
            parseDate();
        } else {
            throw new ParseException();
        }
                
    }
    
    /**
     * Default notations
     * yyyy
     * yyyy-MM
     * yyyy-MM
     * yyyy-MM-dd
     * yyyyMM
     * yyyyMMdd
     * 
     * Week notations
     * yyyy-Www-j
     * yyyy-Www
     * yyyyWwwj
     * yyyyWww
     * 
     * Day of year notations
     * yyyy-jjj
     * yyyyjjj
     * 
     * TODO Reduced notations, where missing elements are replaced by a - or nothing when not confusing
     * --MM-dd
     * -MM-dd
     * yyyy---dd
     * yyyy--dd
     * 
     * 
     * Old notations (removed in spec 2004)
     * yyMMdd
     * yy-MM-dd
     * 
     * Note (TODO confirm those, still valid after spec 2004 ?):
     * yyyyMM is an illegal notation, conflicts with yymmdd
     * 
     */
    private void parseDate() throws ParseException {
        
        // yyyy
        int v;
        v = nextAsDigit();
        v = v*10 + nextAsDigit();
        v = v*10 + nextAsDigit();
        v = v*10 + nextAsDigit();
        year = v;
        
        if (!ite.hasNext()) return;
        
        v = ite.nextToUnicode();
        if (v=='-') v = ite.nextToUnicode();
        else if (v=='T') { parseTime();return;}
            
        if (v=='W') {
            //Www
            v = nextAsDigit();
            v = v*10 + nextAsDigit();
            week = v;
            
            if (!ite.hasNext()) return;
            
            v = ite.nextToUnicode();
            if (v=='-') v = ite.nextToUnicode();
            else if (v=='T') { parseTime();return;}
            
            //j
            if (isDigit(v)) {
                dayOfWeek = v-48;
            } else {
                throw new ParseException();
            }
            
            if (!ite.hasNext()) return;
            
            v = ite.nextToUnicode();
            if (v=='T') {
                parseTime();
            } else {
                throw new ParseException();
            }
        
        } else if (isDigit(v)) {
            v = (v-48)*10 + nextAsDigit();
            //could be mm or jjj or mmdd
            if (!ite.hasNext()) {
                month = v;
                return;
            }
            
            int v2 = ite.peekToUnicode();
            if (v2=='T') {
                month = v;
                ite.skip();
                parseTime();
                return;
            } else if (v2=='-') {
                //it's MM-dd
                ite.skip();
                month = v;
                dayOfMonth = nextAsDigit()*10 + nextAsDigit();
                
                if (!ite.hasNext()) return;
                
                v = ite.nextToUnicode();
                if (v=='T') {
                    parseTime();
                } else {
                    throw new ParseException();
                }
                
            } else if (isDigit(v2)) {
                v2 = nextAsDigit();
                
                if (!ite.hasNext()) {
                    //it's jjj
                    dayOfYear = v*10 + v2;
                    return;
                }
                
                int v3 = ite.nextToUnicode();
                if (v3=='T') {
                    parseTime();
                    return;
                } else if (isDigit(v3)) {
                    //it's MMdd
                    month = v;
                    dayOfMonth = v2*10 + (v3-48);
                    
                    if (!ite.hasNext()) return;
                    
                    v = ite.nextToUnicode();
                    if (v=='T') {
                        parseTime();
                    } else {
                        throw new ParseException();
                    }
                    
                } else {
                    throw new ParseException();
                }
                
            } else {
                throw new ParseException();
            }
            
        } else {
            throw new ParseException();
        }
        
    }
    
    /**
     * Notations
     * THH:mm:SS,sss
     * THH:mm:SS
     * THH:mm
     * THH
     * THHmmSSsss
     * THHmmSS
     * THHmm
     * THH
     * 
     * Possibly followed by a time zone
     * 
     */
    private void parseTime() throws ParseException {
        //HH
        int v;
        hour = nextAsDigit()*10 + nextAsDigit();
        
        if (!ite.hasNext()) return;
        
        v = ite.peekToUnicode();
        if (v==':'){
            ite.skip();
            v = ite.peekToUnicode();
        }
        
        if (v=='Z' || v=='+' || v=='-') {
            parseTimeZone();
            return;
        }
        
        //mm
        minute = nextAsDigit()*10 + nextAsDigit();
        
        if (!ite.hasNext()) return;
        
        v = ite.peekToUnicode();
        if (v==':'){
            ite.skip();
            v = ite.peekToUnicode();
        }
        
        if (v=='Z' || v=='+' || v=='-') {
            parseTimeZone();
            return;
        }   
        
        //SS
        second = nextAsDigit()*10 + nextAsDigit();
        
        if (!ite.hasNext()) return;
        
        v = ite.peekToUnicode();
        if (v==',' || v=='.') {
            ite.skip();
            v = ite.peekToUnicode();
        }
        
        if (v=='Z' || v=='+' || v=='-') {
            parseTimeZone();
            return;
        }   
        
        //sss
        double div = 10.0;
        secondFraction = (double)nextAsDigit() / div;
        while (ite.hasNext()) {
            v = ite.peekToUnicode();
            
            if (v=='Z' || v=='+' || v=='-') {
                secondFraction = (double)v / div;
                parseTimeZone();
                return;
            }
            
            div *= 10.0;
            secondFraction += (double)nextAsDigit()/div;
        }
        
        
    }
    
    /**
     * Notations
     * Z
     * +HH
     * +HHmm
     * +HH:mm
     * -HH
     * -HHmm
     * -HH:mm
     * 
     */
    private void parseTimeZone() throws ParseException {
        int v;
        v = ite.nextToUnicode();
        if (v=='Z') {
            timeZone0 = true;
            if (ite.hasNext()) throw new ParseException();
        } else if (v=='+') {
            timeZoneHour = nextAsDigit()*10 + nextAsDigit();
        } else if (v=='-') {
            timeZoneHour = -(nextAsDigit()*10 + nextAsDigit());
        } else {
            throw new ParseException();
        }
        
        if (!ite.hasNext()) return;
        
        v = ite.nextToUnicode();
        if (v==':') v = ite.nextToUnicode();
        
        timeZoneMinute = (v-48)*10 + nextAsDigit();
        
        if (ite.hasNext()) throw new ParseException();
    }
    
    private int nextAsDigit(){
        final int c = ite.nextToUnicode();
        if (isDigit(c)) return c - 48;
        throw new InvalidArgumentException("Unexpected character "+new Char(c));
    }
    
    private static boolean isDigit(int c){
        return !(c < 48 || c > 57);
    }
    
}
