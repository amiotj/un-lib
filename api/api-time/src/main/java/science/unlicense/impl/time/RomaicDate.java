package science.unlicense.impl.time;

import science.unlicense.api.time.TemporalException;
import static science.unlicense.impl.time.JulianUtils.DAYS_PER_CENTURY;

/**
 *
 * @author Samuel Andrés
 */
public class RomaicDate extends YearMonthDayDate {

    public static final int[] MIN = {Integer.MIN_VALUE, 1, 1};
    public static final int[] MAX = {Integer.MAX_VALUE, 12, 31};

    /**
     * Nombre de jours entre le début de l'ère chrétienne (1er janvier 0 pétrinien)
     * et le début de l'ère Java (1er janvier 1970 grégorien).
     *
     * Du 1/1/0 (1er septembre 0000 romaïque) au 1/5/0 (1er janvier 0000
     * romaïque ~ 1er janvier -5509 pétrinien) : on ajoute 122 jours
     * Du 1/1/-5509 pétrinien au 1/1/-5500 pétrinien : on ajoute 8 années de
     * 365 jours et encore 2 jours supplémentaires correspondant aux jours des
     * deux années bissextiles entre -5509 et -5500 (-5508 et -5504).
     * Du 1/1/-5500 au 1/1/0000 pétrinien : 55 siècles
     * Du 1/1/0 (pétrinien) au 1/1/2000 (pétrinien) : 20 siècles
     * Du 1/1/2000 (pétrinien) au 1/1/1970 (pétrinien) : on retire 30 années de
     * 365 jours et encore 7 jours supplémentaires correspondants aux jours des
     * 7 années bissextiles entre 1970 et 2000.
     * Du 1/1/1970 (pétrinien) au 1/1/1970 (grégorien) : on retire 13 jours de
     * décalage entre les deux calendriers à cette date.
     *
     * Vaut ? jours.
     */
    static final long DAYS_0000_ROMAIC_TO_1970_GREGORIAN = (DAYS_PER_CENTURY * 75L) + 122L + (8L * 365L + 2L)- (30L * 365L + 7L) - 13L;

    private RomaicDate(int year, int month, int dayOfMonth) {
        super(year, month, dayOfMonth);
    }

    public RomaicMonth getMonth() {
        return RomaicMonth.of(month);
    }

//    private int get0(TemporalField field) {
//        switch ((ChronoField) field) {
//            case DAY_OF_WEEK: return getDayOfWeek().getValue();
//            case ALIGNED_DAY_OF_WEEK_IN_MONTH: return ((day - 1) % 7) + 1;
//            case ALIGNED_DAY_OF_WEEK_IN_YEAR: return ((getDayOfYear() - 1) % 7) + 1;
//            case DAY_OF_MONTH: return day;
//            case DAY_OF_YEAR: return getDayOfYear();
//            case EPOCH_DAY: throw new UnsupportedTemporalTypeException("Invalid field 'EpochDay' for get() method, use getLong() instead");
//            case ALIGNED_WEEK_OF_MONTH: return ((day - 1) / 7) + 1;
//            case ALIGNED_WEEK_OF_YEAR: return ((getDayOfYear() - 1) / 7) + 1;
//            case MONTH_OF_YEAR: return month;
//            case PROLEPTIC_MONTH: throw new UnsupportedTemporalTypeException("Invalid field 'ProlepticMonth' for get() method, use getLong() instead");
//            case YEAR_OF_ERA: return (year >= 1 ? year : 1 - year);
//            case YEAR: return year;
//            case ERA: return (year >= 1 ? 1 : 0);
//        }
//        throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
//    }

    
    public int lengthOfMonth() {
        return RomaicMonth.of(month).length(isLeapYear());
    }

    public long toEpochDay() {
        final long y = year;

        // Nombre de jours dans les années écoulées depuis l'année 0000 en comptant 365 jours par an.
        long total = 365 * y;

        // Si on est après l'an 0000
        if (y >= 0) {
            // On ajoute un jour par année bissextile (1 année sur 4) en tenant compte de l'année en cours (d'où le +3)
            total += (y + 3) / 4;
        }
        // Sinon
        else {
            total -= y / -4;
        }

        // On ajoute le nombre de jours écoulés dans l'année jusqu'au mois en cours.
        total += getDayOfYear() - 1; // Pourquoi (-1) ?

        // On se ramène à l'époque Java
        return total - DAYS_0000_ROMAIC_TO_1970_GREGORIAN;
    }

    /**
     *
     * @return
     */
    public int getDayOfWeek() {
        // Le 1-1-1970 grégorien était un jeudi.
        int dow0 = (int) (toEpochDay() + 3) % 7;
        return dow0 + 1;
    }

    public boolean isLeapYear() {
        return RomaicUtiles.isLeapYear(year);
    }

    public int getDayOfYear() {
        return getMonth().firstDayOfYear(isLeapYear()) + day - 1;
    }

    public RomaicDate withYear(int year) {
        if (this.year == year) {
            return this;
        }
        return resolvePreviousValid(year, month, day);
    }

    public RomaicDate withMonth(int month) {
        if (this.month == month) {
            return this;
        }
        return resolvePreviousValid(year, month, day);
    }

    public RomaicDate withDayOfMonth(int dayOfMonth) {
        if (this.day == dayOfMonth) {
            return this;
        }
        return of(year, month, dayOfMonth);
    }

    public RomaicDate withDayOfYear(int dayOfYear) {
        if (this.getDayOfYear() == dayOfYear) {
            return this;
        }
        return ofYearDay(year, dayOfYear);
    }

    private static RomaicDate resolvePreviousValid(int year, int month, int day) {
        switch (month) {
            case 2:
                day = Math.min(day, RomaicUtiles.isLeapYear(year) ? 29 : 28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = Math.min(day, 30);
                break;
        }
        return new RomaicDate(year, month, day);
    }

    public RomaicDate plusYears(long yearsToAdd) {
        if (yearsToAdd == 0) {
            return this;
        }
        int newYear = (int) (year + yearsToAdd);
        return resolvePreviousValid(newYear, month, day);
    }

    public RomaicDate plusMonths(long monthsToAdd) {
        if (monthsToAdd == 0) {
            return this;
        }
        long monthCount = year * 12L + (month - 1);
        long calcMonths = monthCount + monthsToAdd;  // safe overflow
        int newYear = (int) calcMonths / 12;
        int newMonth = (int) (calcMonths % 12) + 1;
        return resolvePreviousValid(newYear, newMonth, day);
    }

    public RomaicDate plusWeeks(long weeksToAdd) {
        return plusDays(weeksToAdd*7);
    }

    public RomaicDate plusDays(long daysToAdd) {
        if (daysToAdd == 0) {
            return this;
        }
        long mjDay = toEpochDay() + daysToAdd;
        return RomaicDate.ofEpochDay(mjDay);
    }

    public RomaicDate minusYears(long yearsToSubtract) {
        return (yearsToSubtract == Long.MIN_VALUE ? plusYears(Long.MAX_VALUE).plusYears(1) : plusYears(-yearsToSubtract));
    }

    public RomaicDate minusMonths(long monthsToSubtract) {
        return (monthsToSubtract == Long.MIN_VALUE ? plusMonths(Long.MAX_VALUE).plusMonths(1) : plusMonths(-monthsToSubtract));
    }

    public RomaicDate minusWeeks(long weeksToSubtract) {
        return (weeksToSubtract == Long.MIN_VALUE ? plusWeeks(Long.MAX_VALUE).plusWeeks(1) : plusWeeks(-weeksToSubtract));
    }

    public RomaicDate minusDays(long daysToSubtract) {
        return (daysToSubtract == Long.MIN_VALUE ? plusDays(Long.MAX_VALUE).plusDays(1) : plusDays(-daysToSubtract));
    }

    //==========================================================================
    // MÉTHODES STATIQUES
    //==========================================================================

    /**
     *
     * @param year
     * @param month
     * @param dayOfMonth
     * @return
     */
    public static RomaicDate of(int year, int month, int dayOfMonth) {
        return create(year, month, dayOfMonth);
    }

    /**
     *
     * @param year
     * @param month
     * @param dayOfMonth
     * @return
     */
    public static RomaicDate of(int year, RomaicMonth month, int dayOfMonth) {
        return create(year, month.getValue(), dayOfMonth);
    }

    /**
     *
     * @param year
     * @param dayOfYear
     * @return
     */
    public static RomaicDate ofYearDay(int year, int dayOfYear) {
        boolean leap = RomaicUtiles.isLeapYear(year);
        if (dayOfYear == 366 && leap == false) {
            throw new TemporalException("Invalid date 'DayOfYear 366' as '" + year + "' is not a leap year");
        }
        RomaicMonth moy = RomaicMonth.of((dayOfYear - 1) / 31 + 1);
        int monthEnd = moy.firstDayOfYear(leap) + moy.length(leap) - 1;
        if (dayOfYear > monthEnd) {
            moy = moy.plus(1);
        }
        int dom = dayOfYear - moy.firstDayOfYear(leap) + 1;
        return new RomaicDate(year, moy.getValue(), dom);
    }

    /**
     *
     * @param epochDay
     * @return
     */
    public static RomaicDate ofEpochDay(long epochDay) {

        // Conversion en jours depuis le 1/1/0 pétrinien
        long zeroDay = epochDay + DAYS_0000_ROMAIC_TO_1970_GREGORIAN;

        /*
        Numéro de jour dans l'année commençant le 1er mars.
        Ce calcul nécessite de décaler le décompte des jours début mars.
        On enlève donc 182 jours correspondant : 30 jours de semptembre,
        31 d'octobre, 30 de novembre, 31 de décembre, 31 de janvier et 29 de
        février 0000 qui est bisextile.
        */
        final YearDayDate dy = JulianUtils.toDayOfYear(zeroDay-182);

        // convert march-based values back to september-based
        final YearMonthDayDate date = JulianUtils.translate(dy, JulianMonth.SEPTEMBER);

        // check year now we are certain it is correct
        int year = date.getYear();
        return new RomaicDate(year, date.getMonthValue(), date.getDayOfMonth());
    }

    /**
     *
     * @param year
     * @param month
     * @param dayOfMonth
     * @return
     */
    private static RomaicDate create(int year, int month, int dayOfMonth) {
        if (dayOfMonth > 28) {
            int dom = 31;
            switch (month) {
                case 2:
                    dom = (RomaicUtiles.isLeapYear(year) ? 29 : 28);
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    dom = 30;
                    break;
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 29) {
                    throw new TemporalException("Invalid date 'February 29' as '" + year + "' is not a leap year");
                } else {
                    throw new TemporalException("Invalid date '" + month + " " + dayOfMonth + "'");
                }
            }
        }
        return new RomaicDate(year, month, dayOfMonth);
    }

}
