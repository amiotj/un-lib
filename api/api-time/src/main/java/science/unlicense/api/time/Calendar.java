
package science.unlicense.api.time;

import science.unlicense.api.character.CharArray;

/**
 *
 * @author Johann Sorel
 */
public interface Calendar {

    /**
     * Calendar name.
     *
     * @return name, not null, should be unique
     */
    CharArray getName();

    /**
     *
     * @return original event of the calendar
     */
    ChronologicEvent getAnchor();

    /**
     * Calendar divisions which compose dates.
     *
     * @return array, never null, should contain at least one value.
     */
    Division[] getDivisions();

    /**
     * Convert an event to a calendar date.
     * 
     * @param event
     * @return
     */
    Date toDate(ChronologicEvent event);
        
}
