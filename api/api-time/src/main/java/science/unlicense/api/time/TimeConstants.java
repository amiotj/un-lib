
package science.unlicense.api.time;

import science.unlicense.api.unit.Units;
import science.unlicense.impl.number.LargeInteger;
import science.unlicense.impl.time.DefaultChronologicEvent;
import science.unlicense.impl.time.UnitDuration;

/**
 * Various constants.
 * 
 * @author Johann Sorel
 */
public final class TimeConstants {

    public static final long YEAR_IN_MS    = 31536000000l;
    public static final long MONHT_IN_MS   = 2628000000l;
    public static final long WEEK_IN_MS    = 604800000l;
    public static final long DAY_IN_MS     = 86400000l;
    public static final long HOUR_IN_MS    = 3600000l;
    public static final long MINUTE_IN_MS  = 60000l;
    public static final long SECOND_IN_MS  = 1000l;

    public static final ChronologicEvent BIGBANG = null;
    /**
     * Common Era beginning event.
     * https://en.wikipedia.org/wiki/Common_Era
     *
     * Used as origin for modern calendars.
     * Approximated to 13.8 billion years after the BigBang.
     */
    public static final ChronologicEvent COMMON_ERA = new DefaultChronologicEvent(BIGBANG,
            new UnitDuration(Units.MILLISECOND, new LargeInteger(138 * 100000000L * YEAR_IN_MS)));
    
    /**
     * Posix timestamp beginning.
     * https://en.wikipedia.org/wiki/Unix_time
     * 
     * From date 1970-01-01.
     */
    public static final ChronologicEvent POSIX = new DefaultChronologicEvent(COMMON_ERA,
            new UnitDuration(Units.MILLISECOND, new LargeInteger(1970*YEAR_IN_MS)));//todo find exact value

    private TimeConstants(){}

}
