
package science.unlicense.api.time;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import static science.unlicense.api.time.TimeConstants.DAY_IN_MS;
import static science.unlicense.api.time.TimeConstants.HOUR_IN_MS;
import static science.unlicense.api.time.TimeConstants.MINUTE_IN_MS;
import static science.unlicense.api.time.TimeConstants.MONHT_IN_MS;
import static science.unlicense.api.time.TimeConstants.POSIX;
import static science.unlicense.api.time.TimeConstants.SECOND_IN_MS;
import static science.unlicense.api.time.TimeConstants.WEEK_IN_MS;
import static science.unlicense.api.time.TimeConstants.YEAR_IN_MS;
import science.unlicense.api.unit.Units;
import science.unlicense.impl.number.LargeInteger;
import science.unlicense.impl.time.DefaultChronologicEvent;
import science.unlicense.impl.time.UnitDuration;

/**
 * Time utils.
 * 
 * @author Johann Sorel
 */
public class Times {


    private static final long[] STEPS = new long[]{
        YEAR_IN_MS,
        MONHT_IN_MS,
        WEEK_IN_MS,
        DAY_IN_MS,
        HOUR_IN_MS,
        MINUTE_IN_MS,
        SECOND_IN_MS,
        1
    };
    private static final Chars[] SUFFIX = new Chars[]{
        new Chars("y"),
        new Chars("m"),
        new Chars("w"),
        new Chars("d"),
        new Chars("h"),
        new Chars("m"),
        new Chars("s"),
        new Chars("ms")
    };

    /**
     * Convert a duration in a human readable format.
     * TODO make something like a format class.
     *
     * @param duration
     * @return
     */
    public static Chars toChars(long duration) {

        if(duration == 0){
            return new Chars("0ms");
        }

        final CharBuffer cb = new CharBuffer();
        for(int i=0;i<STEPS.length;i++){
            final long nb = duration / STEPS[i];
            if(nb > 0) cb.append(" "+nb).append(SUFFIX[i]);
            duration = duration % STEPS[i];
        }

        return cb.toChars().trim();
    }

    /**
     * Convert a duration in a human readable format.
     * TODO make something like a format class.
     *
     * @param duration
     * @return
     */
    public static Chars toCharsSeconds(long duration) {

        final long nb = duration / SECOND_IN_MS;
        duration = duration % SECOND_IN_MS;

        final CharBuffer cb = new CharBuffer();
        cb.append(""+nb).append('.');
        if(duration<100) cb.append('0');
        if(duration<10)  cb.append('0');
        cb.append(""+duration).append("s");
        return cb.toChars().trim();
    }

    public static ChronologicEvent msFromEpoch(long milliseconds){
        return new DefaultChronologicEvent(POSIX, new UnitDuration(Units.MILLISECOND, new LargeInteger(milliseconds)));
    }

}
