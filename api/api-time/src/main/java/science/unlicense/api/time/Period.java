
package science.unlicense.api.time;

/**
 * A period is elapse of time between fixed chronologic events.
 *
 * @author Johann Sorel
 */
public interface Period {

    /**
     * Get period elapse of time.
     *
     * @return duration period duration, never null
     */
    Duration getDuration();

    /**
     * Starting chronologic event of the period.
     * @return ChronologicEvent, never null
     */
    ChronologicEvent getStart();

    /**
     * Ending chronologic event of the period.
     *
     * @return ChronologicEvent, never null.
     */
    ChronologicEvent getEnd();

}
