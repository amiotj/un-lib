
package science.unlicense.api.time;

import science.unlicense.api.collection.Collection;

/**
 * A repetition is a incomplete date or chronologic event definition.
 * As example : '16 June' is a repetition since it occures each year.
 * 
 * @author Johann Sorel
 */
public interface Repetition {

    /**
     * Returns the periods which match the repetition.
     * 
     * @param start not null
     * @param end not null
     * @param overlaps return periods which overlaps start and end boundaries
     * @return collection of all repeated periods between start and end events, not null, can be empty
     */
    Collection getRepetitions(ChronologicEvent start, ChronologicEvent end, boolean overlaps);

}
