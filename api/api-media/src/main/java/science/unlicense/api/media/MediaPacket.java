
package science.unlicense.api.media;

/**
 *
 * @author Johann Sorel
 */
public interface MediaPacket {

    /**
     * 
     * @return metadatas of the stream
     */
    MediaStreamMeta getMeta();
    
    /**
     * 
     * @return packet start time or -1 if unknown
     */
    long getStartTime();
    
    /**
     * 
     * @return packet time length or -1 if unknown
     */
    long getTimeLength();
    
}
