
package science.unlicense.api.media;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;

/**
 *
 * @author Johann Sorel
 */
public class MediaWriteParameters extends DefaultDocument{

    public static final DocumentType TYPE = new DefaultDocumentType(new Chars("mediaWriteParameters"),null,null,false,new FieldType[0],null);

    public MediaWriteParameters() {
        super(TYPE);
    }

}
