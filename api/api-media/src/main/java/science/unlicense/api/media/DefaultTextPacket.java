
package science.unlicense.api.media;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTextPacket extends AbstractMediaPacket implements TextPacket {

    protected final Chars text;
    
    public DefaultTextPacket(TextStreamMeta meta, long startTime, long timeLength, Chars text) {
        super(meta, startTime, timeLength);
        this.text = text;
    }

    public TextStreamMeta getMeta() {
        return (TextStreamMeta) super.getMeta();
    }

    public Chars getText() {
        return text;
    }

}
