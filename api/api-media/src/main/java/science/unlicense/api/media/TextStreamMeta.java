
package science.unlicense.api.media;

/**
 * Stream definition for text like streams.
 * Used for subtitles.
 * 
 * @author Johann Sorel
 */
public interface TextStreamMeta extends MediaStreamMeta{

}
