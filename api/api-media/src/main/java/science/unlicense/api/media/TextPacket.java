
package science.unlicense.api.media;

import science.unlicense.api.character.Chars;

/**
 * Text packet.
 * 
 * @author Johann Sorel
 */
public interface TextPacket extends MediaPacket {

    /**
     * 
     * @return text stream metadata.
     */
    TextStreamMeta getMeta();
    
    /**
     * @return packet text.
     */
    Chars getText();
    
}
