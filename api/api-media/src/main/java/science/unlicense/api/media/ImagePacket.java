
package science.unlicense.api.media;

import science.unlicense.api.image.Image;

/**
 *
 * @author Johann Sorel
 */
public interface ImagePacket extends MediaPacket{

    /**
     * 
     * @return image stream metadata.
     */
    ImageStreamMeta getMeta();
    
    /**
     * @return image
     */
    Image getImage();
    
}
