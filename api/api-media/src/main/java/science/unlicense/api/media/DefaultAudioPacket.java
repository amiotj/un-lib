
package science.unlicense.api.media;

/**
 *
 * @author Johann Sorel
 */
public class DefaultAudioPacket extends AbstractMediaPacket implements AudioPacket {

    protected final AudioSamples samples;
    
    public DefaultAudioPacket(AudioStreamMeta meta, long startTime, long timeLength, AudioSamples samples) {
        super(meta, startTime, timeLength);
        this.samples = samples;
    }

    public AudioStreamMeta getMeta() {
        return (AudioStreamMeta) super.getMeta();
    }

    public AudioSamples getSamples() {
        return samples;
    }

}
