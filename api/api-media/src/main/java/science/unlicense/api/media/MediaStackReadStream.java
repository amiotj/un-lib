
package science.unlicense.api.media;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.IOException;

/**
 * Stack multiple media readers as one.
 *
 *
 * @author Johann Sorel
 */
public class MediaStackReadStream implements MediaReadStream{

    private final MediaReadStream[] readers;
    private final MediaPacket[] packets;

    public MediaStackReadStream(MediaReadStream[] readers) {
        this.readers = readers;
        this.packets = new MediaPacket[readers.length];
    }

    public void moveTo(long time) throws IOException {
        Arrays.fill(packets, null);
        for(int i=0;i<packets.length;i++){
            readers[i].moveTo(time);
        }
    }

    public MediaPacket next() throws IOException {
        refill();
        
        int index = 0;
        for(int i=1;i<packets.length;i++){
            if(packets[i]!=null && (packets[index]==null || 
               packets[i].getStartTime()<packets[index].getStartTime())){
                index = i;
            }
        }
        
        final MediaPacket next = packets[index];
        packets[index] = null;
        return next;
    }
    
    private void refill() throws IOException{
        for(int i=0;i<packets.length;i++){
            if(packets[i]==null){
                packets[i] = readers[i].next();
            }
        }
    }
    
}
