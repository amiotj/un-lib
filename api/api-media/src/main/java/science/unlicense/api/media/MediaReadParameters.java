
package science.unlicense.api.media;

import science.unlicense.api.CObjects;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.DocumentType;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;

/**
 *
 * @author Johann Sorel
 */
public class MediaReadParameters extends DefaultDocument{

    protected BufferFactory factory = DefaultBufferFactory.INSTANCE;

    public static final FieldType PARAM_STREAM_INDEXES = new FieldTypeBuilder(new Chars("streamIndexes")).valueClass(int[].class).build();

    public static final DocumentType TYPE = new DefaultDocumentType(
            new Chars("mediaReadParameters"), null,null,true,new FieldType[]{PARAM_STREAM_INDEXES},null);

    public MediaReadParameters() {
        super(TYPE);
    }

    public void setStreamIndexes(int[] indexes){
        setFieldValue(PARAM_STREAM_INDEXES.getId(),indexes);
    }

    public int[] getStreamIndexes(){
        return (int[])getFieldValue(PARAM_STREAM_INDEXES.getId());
    }

    /**
     * Set prefered buffer factory.
     * @param factory, not null
     */
    public void setBufferFactory(BufferFactory factory) {
        CObjects.ensureNotNull(factory);
        this.factory = factory;
    }

    /**
     *
     * @return BufferFactory, never null
     */
    public BufferFactory getBufferFactory() {
        return factory;
    }

}
