
package science.unlicense.api.media;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.api.io.IOException;
import science.unlicense.system.util.ModuleSeeker;

/**
 * Convinient methods for media manipulation.
 *
 * @author Johann Sorel
 */
public final class Medias {

    private Medias(){}

    /**
     * Lists available media formats.
     * @return array of MediaFormat, never null but can be empty.
     */
    public static MediaFormat[] getFormats(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/format/science.unlicense.api.media.MediaFormat"), 
                Predicates.instanceOf(MediaFormat.class));
        final MediaFormat[] formats = new MediaFormat[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }

    public static boolean canDecode(Object input) throws IOException {
        final MediaFormat[] formats = getFormats();

        for(int i=0;i<formats.length;i++){
            try{
                if(formats[i].canDecode(input)){
                    return true;
                }
            }catch(IOException ex){
                //may happen
                Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
            }
        }

        return false;
    }
    
    /**
     * Convinient method to open a media of unknowned format.
     * The method will loop on available formats until one can decode the source.
     *
     * @param input
     * @return MediaStore, never null
     * @throws IOException if not format could support the source.
     */
    public static MediaStore open(Object input) throws IOException {
        final MediaFormat[] formats = getFormats();
        if(formats.length == 0){
            throw new IOException("No media formats available.");
        }

        final boolean[] mustclose = new boolean[1];
        for(int i=0;i<formats.length;i++){
            if(formats[i].supportReading()){
                try{
                    if(formats[i].canDecode(input)){
                        final MediaStore storage = formats[i].createStore(input);
                        return storage;
                    }
                }catch(IOException ex){
                    //may happen
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
            }
        }

        throw new IOException("No format can read given input : "+input);
    }

}
