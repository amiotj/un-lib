

package science.unlicense.api.media;

import science.unlicense.api.math.Maths;

/**
 * Utility method to manipulate PCM encoding.
 *
 * @author Johann Sorel
 */
public final class PCMUtils {

    private static final long[] MAXS = new long[65];
    private static final long[] HALFS = new long[65];
    static {
        for(int i=1;i<65;i++){
            MAXS[i] = MAXS[i-1] | (1<<(i-1));
            HALFS[i] = (long)(((double)MAXS[i] / 2.0) +0.5);
        }
    }

    private PCMUtils(){}

    public static long PCMSignedToPCM(long value, int nbBits){
        return value + HALFS[nbBits];
    }

    public static long PCMToPCMSigned(long value, int nbBits){
        return value - HALFS[nbBits];
    }

    public static float PCMToFloat(long value, int nbBits){
        return Maths.clamp( (2f*value/MAXS[nbBits])-1, -1, +1);
    }

    public static float PCMSignedToFloat(long value, int nbBits){
        return PCMToFloat(value+HALFS[nbBits], nbBits);
    }

    public static long floatToPCM(float value, int nbBits){
        return Maths.clamp( (long)(value*MAXS[nbBits]), 0, MAXS[nbBits]);
    }

    public static long floatToSignedPCM(float value, int nbBits){
        return floatToPCM(value, nbBits) - HALFS[nbBits];
    }

}
