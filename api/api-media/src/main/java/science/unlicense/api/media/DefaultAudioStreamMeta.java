

package science.unlicense.api.media;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class DefaultAudioStreamMeta implements AudioStreamMeta {

    private final Chars name;
    private final int[] channels;
    private final int[] bitsPerSample;
    private final double sampleRate;

    public DefaultAudioStreamMeta(Chars name, int[] channels, int[] bitsPerSample, double sampleRate) {
        this.name = name;
        this.channels = channels;
        this.sampleRate = sampleRate;
        this.bitsPerSample = bitsPerSample;
    }

    public Chars getName() {
        return name;
    }

    public int[] getChannels() {
        return Arrays.copy(channels, new int[channels.length]);
    }

    public double getSampleRate() {
        return sampleRate;
    }

    public int[] getBitsPerSample() {
        return Arrays.copy(bitsPerSample, new int[bitsPerSample.length]);
    }

}
