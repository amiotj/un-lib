
package science.unlicense.api.media;

import science.unlicense.api.model.tree.TypedNode;

/**
 *
 * @author Johann Sorel
 */
public interface ImageStreamMeta extends MediaStreamMeta {

    /**
     * Get the video image type definition.
     * Expected to follow the image metamodel.
     *
     * @return TypedNode defition of the video images.
     */
    TypedNode getImageMetadata();

    /**
     * Get number of frame in the stream.
     *
     * @return total number of frames, -1 for unknowned.
     */
    int getNbFrames();

    /**
     * Get the video frame rate.
     * In milliseconds between frames.
     *
     * @return  video frame rate.
     */
    int getFrameRate();

}
