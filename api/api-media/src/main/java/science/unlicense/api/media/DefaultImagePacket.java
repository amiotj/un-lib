
package science.unlicense.api.media;

import science.unlicense.api.image.Image;

/**
 *
 * @author Johann Sorel
 */
public class DefaultImagePacket extends AbstractMediaPacket implements ImagePacket {

    protected final Image image;
    
    public DefaultImagePacket(ImageStreamMeta meta, long startTime, long timeLength, Image image) {
        super(meta, startTime, timeLength);
        this.image = image;
    }

    public ImageStreamMeta getMeta() {
        return (ImageStreamMeta) super.getMeta();
    }

    public Image getImage() {
        return image;
    }

}
