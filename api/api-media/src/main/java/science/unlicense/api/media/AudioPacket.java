
package science.unlicense.api.media;

/**
 * Audio packet.
 * 
 * @author Johann Sorel
 */
public interface AudioPacket extends MediaPacket {

    /**
     * 
     * @return audio stream metadata.
     */
    AudioStreamMeta getMeta();
    
    /**
     * @return packet audio samples.
     */
    AudioSamples getSamples();
    
}
