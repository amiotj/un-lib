

package science.unlicense.api.media;

import science.unlicense.api.store.AbstractStore;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractMediaStore extends AbstractStore implements MediaStore {

    public AbstractMediaStore(MediaFormat format, Object input) {
        super(format, input);
    }

    public MediaFormat getFormat() {
        return (MediaFormat)super.getFormat();
    }
    
}
