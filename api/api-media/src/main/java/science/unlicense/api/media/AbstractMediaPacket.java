
package science.unlicense.api.media;

/**
 *
 * @author Johann Sorel
 */
public class AbstractMediaPacket implements MediaPacket{

    protected final MediaStreamMeta meta;
    protected final long startTime;
    protected final long timeLength;

    public AbstractMediaPacket(MediaStreamMeta meta, long startTime, long timeLength) {
        this.meta = meta;
        this.startTime = startTime;
        this.timeLength = timeLength;
    }
    
    public MediaStreamMeta getMeta() {
        return meta;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getTimeLength() {
        return timeLength;
    }

}
