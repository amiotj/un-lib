
package science.unlicense.api.media;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface MediaStreamMeta {

    /**
     * Get name of this stream.
     * 
     * @return Chars, never null
     */
    Chars getName();

}
