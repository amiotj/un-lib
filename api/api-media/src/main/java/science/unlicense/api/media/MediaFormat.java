
package science.unlicense.api.media;

import science.unlicense.api.io.IOException;
import science.unlicense.api.store.Format;

/**
 *
 * @author Johann Sorel
 */
public interface MediaFormat extends Format {

    /**
     * Encoding capabilities of the format.
     * @return MediaCapabilities
     */
    MediaCapabilities getCapabilities();

    /**
     * @return true if reading is supported.
     */
    boolean supportReading();

    /**
     * @return true if writing is supported.
     */
    boolean supportWriting();

    /**
     * @param input
     * @return new media store
     */
    MediaStore createStore(Object input) throws IOException;


}
