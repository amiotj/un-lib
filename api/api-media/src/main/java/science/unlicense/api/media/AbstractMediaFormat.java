
package science.unlicense.api.media;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.store.DefaultFormat;

/**
 * Abstract media format.
 *
 * @author Johann Sorel
 */
public abstract class AbstractMediaFormat extends DefaultFormat implements MediaFormat {

    public AbstractMediaFormat(Chars identifier, Chars shortName, Chars longName, 
            Chars[] mimeTypes, Chars[] extensions, byte[][] signatures) {
        super(identifier,shortName,longName,mimeTypes,extensions,signatures);
    }

    public MediaCapabilities getCapabilities() {
        return new AbstractMediaCapabilities();
    }

    public boolean supportReading() {
        return false;
    }

    public boolean supportWriting() {
        return false;
    }

    public MediaStore createStore(Object input) throws IOException {
        throw new IOException("Not supported.");
    }

}
