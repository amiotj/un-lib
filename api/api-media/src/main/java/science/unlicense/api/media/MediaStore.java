
package science.unlicense.api.media;

import science.unlicense.api.io.IOException;
import science.unlicense.api.store.Store;

/**
 *
 * @author Johann Sorel
 */
public interface MediaStore extends Store{

    /**
     * Media format.
     *
     * @return MediaFormat
     */
    MediaFormat getFormat();

    /**
     * List available streams in this storage.
     *
     * @return MediaStreamMeta array, never null
     */
    MediaStreamMeta[] getStreamsMeta() throws IOException;

    MediaReadStream createReader(MediaReadParameters params) throws IOException;

    MediaWriteStream createWriter(MediaWriteParameters params) throws IOException;

}
