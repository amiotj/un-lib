
package science.unlicense.impl.media.inmemory;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.DefaultImagePacket;
import science.unlicense.api.media.ImagePacket;
import science.unlicense.api.media.MediaReadStream;

/**
 *
 * @author Johann Sorel
 */
class MemoryImageReadStream implements MediaReadStream {

    private final MemoryImageSerie mv;
    private int currentImageIndex = -1;

    public MemoryImageReadStream(MemoryImageSerie mv) {
        this.mv = mv;
    }

    public ImagePacket next() {
        currentImageIndex++;
        if(currentImageIndex < mv.getImages().getSize()-1){
            final Image img = (Image) mv.getImages().get(currentImageIndex);
            final long step = mv.getTimeSpacing();
            final long time = currentImageIndex*step;
            final DefaultImagePacket packet = new DefaultImagePacket(null, time, step, img);
        }
        return null;
    }

    public void moveTo(long time) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
