
package science.unlicense.impl.media.inmemory;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.AbstractMediaFormat;

/**
 *
 * @author Johann Sorel
 */
public class MemoryImageVideoFormat extends AbstractMediaFormat {
    public static final MemoryImageVideoFormat INSTANCE = new MemoryImageVideoFormat();

    private MemoryImageVideoFormat() {
        super(new Chars("memory"),
              new Chars("Memory"),
              new Chars("In memory video format"),
              new Chars[]{
              },
              new Chars[]{
              },
              new byte[0][0]);
    }

}
