
package science.unlicense.impl.media.inmemory;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.ImageStreamMeta;
import science.unlicense.api.model.tree.TypedNode;

/**
 * An in memory video based on a sequence of images.
 *
 * @author Johann Sorel
 */
public class MemoryImageSerie {

    private final Sequence images = new ArraySequence();
    private int timeSpacing = 1;

    public MemoryImageSerie() {
    }

    /**
     * @return time between each image, in milliseconds.
     */
    public int getTimeSpacing() {
        return timeSpacing;
    }

    public void setTimeSpacing(int timeSpacing) {
        this.timeSpacing = timeSpacing;
    }

    /**
     * Get images stack in this video.
     * @return Sequence, never null, can be empty.
     */
    public Sequence getImages() {
        return images;
    }

    public MediaStreamMeta getStreamsMeta() {
        final ImageStreamMeta meta = new ImageStreamMeta() {
            public TypedNode getImageMetadata() {
                throw new UnimplementedException("Not supported yet.");
            }
            public int getNbFrames() {
                return images.getSize();
            }
            public int getFrameRate() {
                return timeSpacing;
            }
            public Chars getName() {
                return Chars.EMPTY;
            }
        };
        return meta;
    }

    public MediaReadStream getReader(){
        return new MemoryImageReadStream(this);
    }

}
