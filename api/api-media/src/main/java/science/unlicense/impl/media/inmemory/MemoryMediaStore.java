
package science.unlicense.impl.media.inmemory;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.media.AbstractMediaStore;
import science.unlicense.api.media.MediaStackReadStream;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStreamMeta;
import science.unlicense.api.media.MediaWriteParameters;
import science.unlicense.api.media.MediaWriteStream;

/**
 * An in memory video composed of a succesion of images.
 *
 * @author Johann Sorel
 */
public class MemoryMediaStore extends AbstractMediaStore{

    private final Sequence streams = new ArraySequence();

    public MemoryMediaStore() {
        super(MemoryImageVideoFormat.INSTANCE,null);
    }

    /**
     * Modifiable sequence of all streams in this storage.
     *
     * @return Sequence, never null. can be empty.
     */
    public Sequence getStreams() {
        return streams;
    }

    public MediaStreamMeta[] getStreamsMeta() {
        final MediaStreamMeta[] metas = new MediaStreamMeta[streams.getSize()];
        for(int i=0;i<metas.length;i++){
            metas[i] = ((MemoryImageSerie)streams.get(i)).getStreamsMeta();
        }

        return metas;
    }

    public MediaReadStream createReader(MediaReadParameters params) {
        final int[] indexes = params.getStreamIndexes();
        final MediaReadStream[] readers = new MediaReadStream[indexes.length];
        for(int i=0;i<indexes.length;i++){
            readers[i] = ((MemoryImageSerie)streams.get(i)).getReader();
        }
        return new MediaStackReadStream(readers);
    }

    public MediaWriteStream createWriter(MediaWriteParameters params) {
        throw new UnimplementedException("Not supported yet.");
    }

}
