
package science.unlicense.api.media;

import science.unlicense.api.media.PCMUtils;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author Johann Sorel
 */
public class PCMUtilsTest {

    @Test
    public void pcmFloatTest(){
        Assert.assertEquals(+1f, PCMUtils.PCMSignedToFloat(+32767,16));
        Assert.assertEquals(-1f, PCMUtils.PCMSignedToFloat(-32768,16));
    }

    @Test
    public void floatToPcmTest(){
        Assert.assertEquals(+32767, PCMUtils.floatToSignedPCM(+1,16));
        Assert.assertEquals(-32768, PCMUtils.floatToSignedPCM(-1,16));
    }

}
