
package science.unlicense.api.physic.skeleton;

import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.physic.skeleton.Joint;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class SkeletonTest {

    private static final float DELTA = 0.000001f;
    
    @Test
    public void bindPoseTest(){

        final Skeleton skeleton = new Skeleton();

        final Joint jt1 = new Joint(3);
        jt1.getNodeTransform().getTranslation().setX(1);
        jt1.getNodeTransform().notifyChanged();

        final Joint jt2 = new Joint(3);
        jt2.getNodeTransform().getTranslation().setY(1);
        jt2.getNodeTransform().notifyChanged();

        final Joint jt3 = new Joint(3);
        jt3.getNodeTransform().getTranslation().setZ(1);
        jt3.getNodeTransform().notifyChanged();

        jt1.getChildren().add(jt2);
        jt2.getChildren().add(jt3);

        skeleton.getChildren().add(jt1);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                jt1.getBindPose().toMatrix());

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1),
                jt2.getBindPose().toMatrix());

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 1,
                0, 0, 1, 1,
                0, 0, 0, 1),
                jt3.getBindPose().toMatrix());


        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                jt1.getInvertBindPose().toMatrix());

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, -1,
                0, 0, 1, 0,
                0, 0, 0, 1),
                jt2.getInvertBindPose().toMatrix());

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, -1,
                0, 0, 1, -1,
                0, 0, 0, 1),
                jt3.getInvertBindPose().toMatrix());

    }

    @Test
    public void restoreBaseTest(){

        //create a skeleton
        final Skeleton skeleton = new Skeleton();
        final Joint jt1 = new Joint(3);
        jt1.getNodeTransform().getTranslation().setX(1);
        jt1.getNodeTransform().notifyChanged();
        final Joint jt2 = new Joint(3);
        jt2.getNodeTransform().getTranslation().setY(1);
        jt2.getNodeTransform().notifyChanged();
        final Joint jt3 = new Joint(3);
        jt3.getNodeTransform().getTranslation().setZ(1);
        jt3.getNodeTransform().notifyChanged();
        jt1.getChildren().add(jt2);
        jt2.getChildren().add(jt3);
        skeleton.getChildren().add(jt1);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        //change the bind bose
        jt1.getNodeTransform().getTranslation().setZ(5);
        jt2.getNodeTransform().getTranslation().setX(8);
        jt3.getNodeTransform().getTranslation().setY(20);
        jt1.getNodeTransform().notifyChanged();
        jt2.getNodeTransform().notifyChanged();
        jt3.getNodeTransform().notifyChanged();
        skeleton.updateBindPose();

        //restore the base pose
        skeleton.resetToBase();
        Assert.assertEquals(new Vector(1, 0, 0), jt1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector(0, 1, 0), jt2.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 1), jt3.getNodeTransform().getTranslation());

    }

    @Test
    public void restoreBase2Test(){

        //create a skeleton
        final Skeleton skeleton = new Skeleton();
        final Joint jt1 = new Joint(3);
        jt1.getNodeTransform().getRotation().set(new Matrix3x3(
                1, 0, 0, 
                0, 0, -1, 
                0, 1, 0));
        jt1.getNodeTransform().notifyChanged();
        final Joint jt2 = new Joint(3);
        jt2.getNodeTransform().getTranslation().setY(1);
        jt2.getNodeTransform().notifyChanged();
        final Joint jt3 = new Joint(3);
        jt3.getNodeTransform().getTranslation().setY(1);
        jt3.getNodeTransform().notifyChanged();
        jt1.getChildren().add(jt2);
        jt2.getChildren().add(jt3);
        skeleton.getChildren().add(jt1);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
        
        Assert.assertEquals(1, jt1.getChildren().getSize());
        Assert.assertEquals(1, jt2.getChildren().getSize());
        Assert.assertEquals(0, jt3.getChildren().getSize());
        Assert.assertEquals(jt2, jt1.getChildren().get(0));
        Assert.assertEquals(jt3, jt2.getChildren().get(0));

        //check parent to node transform
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            jt1.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            jt2.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            jt3.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        
        //check bind pose
        Assert.assertArrayEquals(new Matrix4x4(
                  1,   0,   0, 0,
                  0,   0,  -1, 0,
                  0,   1,   0, 0,
                  0,   0,   0, 1).toArrayDouble(), 
            jt1.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(jt1.getBindPose().invert().toMatrix().toArrayDouble(),
            jt1.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                  1,   0,   0,    0,
                  0,   0,  -1,    0,
                  0,   1,   0,    1,
                  0,   0,   0,    1).toArrayDouble(), 
            jt2.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(jt2.getBindPose().invert().toMatrix().toArrayDouble(),
            jt2.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        
        
        //change the bind bose
        jt1.getNodeTransform().getTranslation().setZ(5);
        jt2.getNodeTransform().getTranslation().setX(8);
        jt1.getNodeTransform().notifyChanged();
        jt2.getNodeTransform().notifyChanged();
        skeleton.updateBindPose();

        //restore the base pose
        skeleton.resetToBase();
        //check parent to node transform
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 0,-1, 0,
                0, 1, 0, 0,
                0, 0, 0, 1).toArrayDouble(), 
            jt1.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 1,
                0, 0, 1, 0,
                0, 0, 0, 1).toArrayDouble(), 
            jt2.getNodeTransform().asMatrix().toArrayDouble(), DELTA);
        //check bind pose
        Assert.assertArrayEquals(new Matrix4x4(
                  1,   0,   0,  0,
                  0,   0,  -1,  0,
                  0,   1,   0,  0,
                  0,   0,   0,  1).toArrayDouble(), 
            jt1.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(jt1.getBindPose().invert().toMatrix().toArrayDouble(),
            jt1.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(new Matrix4x4(
                  1,   0,   0,  0,
                  0,   0,  -1,  0,
                  0,   1,   0,  1,
                  0,   0,   0,  1).toArrayDouble(), 
            jt2.getBindPose().toMatrix().toArrayDouble(), DELTA);
        Assert.assertArrayEquals(jt2.getBindPose().invert().toMatrix().toArrayDouble(),
            jt2.getInvertBindPose().toMatrix().toArrayDouble(), DELTA);

    }
    
    @Test
    public void resetToWorldTest(){
        
        final Joint jt1 = new Joint(3);
        final Joint jt2 = new Joint(3);
        final Joint jt3 = new Joint(3);
        jt1.getChildren().add(jt2);
        jt2.getChildren().add(jt3);
        
        jt1.getNodeTransform().getTranslation().setXYZ(1, 0, 0);
        jt2.getNodeTransform().getTranslation().setXYZ(0, 1, 0);
        jt3.getNodeTransform().getTranslation().setXYZ(0, 0, 1);
        jt1.getNodeTransform().notifyChanged();
        jt2.getNodeTransform().notifyChanged();
        jt3.getNodeTransform().notifyChanged();
        
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(jt1);
        
        //convert to world state
        skeleton.resetToWorldPose();
        Assert.assertEquals(new Vector(1, 0, 0), jt1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector(1, 1, 0), jt2.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector(1, 1, 1), jt3.getNodeTransform().getTranslation());
        
        //convert to prent state
        skeleton.reverseWorldPose();
        Assert.assertEquals(new Vector(1, 0, 0), jt1.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector(0, 1, 0), jt2.getNodeTransform().getTranslation());
        Assert.assertEquals(new Vector(0, 0, 1), jt3.getNodeTransform().getTranslation());
        
    }
    
}
