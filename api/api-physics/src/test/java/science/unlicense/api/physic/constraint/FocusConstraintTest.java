

package science.unlicense.api.physic.constraint;

import science.unlicense.api.physic.constraint.FocusConstraint;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.DefaultMatrix;

/**
 *
 * @author Johann Sorel
 */
public class FocusConstraintTest {
    
    private static final double EPSILON = 1e-12;
    
    @Test
    public void focusBackwardTest(){
        
        final SceneNode scene = new DefaultSceneNode(3);
        final SceneNode node = new DefaultSceneNode(3);
        final SceneNode target = new DefaultSceneNode(3);
        
        scene.getChildren().add(node);
        scene.getChildren().add(target);
        
        target.getNodeTransform().getTranslation().setXYZ(10, 0, 0);
        target.getNodeTransform().notifyChanged();
        
        final FocusConstraint constraint = new FocusConstraint(node, target);
        constraint.apply();
        
        final MatrixRW m = node.getNodeTransform().getRotation();
        Assert.assertArrayEquals(new double[]{
            0,0,1,
            0,1,0,
            1,0,0
        }, m.toArrayDouble(), EPSILON);
        
    }
    
    @Test
    public void hierarchyTest(){
        
        final SceneNode scene = new DefaultSceneNode(3);
        final SceneNode node = new DefaultSceneNode(3);
        final SceneNode target = new DefaultSceneNode(3);
        
        scene.getChildren().add(node);
        scene.getChildren().add(target);
        
        target.getNodeTransform().getTranslation().setXYZ(-2, 5, 7);
        target.getNodeTransform().notifyChanged();
        
        node.getNodeTransform().getTranslation().setXYZ(-2, 5, 3);
        node.getNodeTransform().notifyChanged();
        
        final FocusConstraint constraint = new FocusConstraint(node, target);
        constraint.apply();
        
        final MatrixRW m = node.getNodeTransform().getRotation();
        Assert.assertArrayEquals(new double[]{
           -1,0,0,
            0,1,0,
            0,0,1
        }, m.toArrayDouble(), EPSILON);
        
    }
    
}
