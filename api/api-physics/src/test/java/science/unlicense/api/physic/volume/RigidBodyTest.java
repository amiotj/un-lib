
package science.unlicense.api.physic.volume;

import science.unlicense.api.physic.body.RigidBody;
import org.junit.Test;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Vector;
import org.junit.Assert;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class RigidBodyTest {

    /**
     * Basic test, ensure the base maths are ok.
     */
    @Test
    public void testSanity(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_LOCAL);
        body.getNodeTransform().getTranslation().localAdd(new Vector(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 2,
                0, 0, 1, 3,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, -2,
                0, 0, 1, -3,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

    }

    @Test
    public void testLocalModeNoParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_LOCAL);
        body.getNodeTransform().getTranslation().localAdd(new Vector(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //the local transform should have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 3,
                0, 1, 0, 4,
                0, 0, 1, 5,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());

    }


    @Test
    public void testLocalModeParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_LOCAL);
        body.getNodeTransform().getTranslation().localAdd(new Vector(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        //create a parent
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getChildren().add(body);

        Assert.assertEquals(parent, body.getParent());

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //the local transform should have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 3,
                0, 1, 0, 4,
                0, 0, 1, 5,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());

        //check that the parent was not updated
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                parent.getNodeTransform().asMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                parent.getRootToNodeSpace().toMatrix());

    }

    @Test
    public void testParentModeNoParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        body.getNodeTransform().getTranslation().localAdd(new Vector(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //the local transform should have changed
        //there is no parent so this body transform should have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 3,
                0, 1, 0, 4,
                0, 0, 1, 5,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());

    }


    @Test
    public void testParentModeParent(){

        final Sphere sphere = new Sphere(1);
        final RigidBody body = new RigidBody(sphere, 1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        body.getNodeTransform().getTranslation().localAdd(new Vector(1, 2, 3));
        body.getNodeTransform().notifyChanged();

        //create a parent
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getChildren().add(body);

        Assert.assertEquals(parent, body.getParent());

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5));

        //the local transform should not have changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0, 2,
                0, 0, 1, 3,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());

        //world transform should be as expected
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -3,
                0, 1, 0, -4,
                0, 0, 1, -5,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        //check that the parent was updated
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 2,
                0, 1, 0, 2,
                0, 0, 1, 2,
                0, 0, 0, 1),
                parent.getNodeTransform().asMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -2,
                0, 1, 0, -2,
                0, 0, 1, -2,
                0, 0, 0, 1),
                parent.getRootToNodeSpace().toMatrix());

    }


}
