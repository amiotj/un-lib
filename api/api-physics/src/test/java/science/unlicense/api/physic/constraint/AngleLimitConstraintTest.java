

package science.unlicense.api.physic.constraint;

import science.unlicense.api.physic.constraint.AngleLimitConstraint;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Angles;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 * Test angle limit constraint.
 * 
 * @author Johann Sorel
 */
public class AngleLimitConstraintTest {
   
    private static final double DELTA = 0.0000000001;
    
    @Test
    public void testZero(){
        final SceneNode node = new DefaultSceneNode(3);
        final AngleLimitConstraint cst = new AngleLimitConstraint(
                node, new BBox(
                        new Vector(Angles.degreeToRadian(-40), Angles.degreeToRadian(-30), Angles.degreeToRadian(-20)), 
                        new Vector(Angles.degreeToRadian(+40), Angles.degreeToRadian(+30), Angles.degreeToRadian(+20))
                ));
        
        cst.apply();
        testAngles(node, 0, 0, 0);        
    }
    
    //TODO fix it
    @Ignore
    @Test
    public void testOutside(){
        final SceneNode node = new DefaultSceneNode(3);
        setToAngles(node, -50, -60, -30);
        
        final AngleLimitConstraint cst = new AngleLimitConstraint(
                node, new BBox(
                        new Vector(Angles.degreeToRadian(-40), Angles.degreeToRadian(-30), Angles.degreeToRadian(-20)), 
                        new Vector(Angles.degreeToRadian(+40), Angles.degreeToRadian(+30), Angles.degreeToRadian(+20))
                ));
        cst.apply();
        testAngles(node, -40, -30, -20);        
    }
    
    /**
     * 
     * @param node
     * @param ax in degrees
     * @param ay in degrees
     * @param az in degrees
     */
    private static void testAngles(SceneNode node, double ax, double ay, double az){
        // check the transform
        MatrixRW m = node.getNodeTransform().getRotation();
        double[] euler = Matrices.toEuler(m.getValuesCopy(), null);
        Assert.assertEquals(Angles.degreeToRadian(ax), euler[2], DELTA);
        Assert.assertEquals(Angles.degreeToRadian(ay), euler[1], DELTA);
        Assert.assertEquals(Angles.degreeToRadian(az), euler[0], DELTA);
        
    }
    
    private static void setToAngles(SceneNode node, double x, double y, double z){
        final Matrix3x3 m = Matrix3x3.createRotationEuler(new Vector(z, y, x));
        node.getNodeTransform().getRotation().set(m);
        node.getNodeTransform().notifyChanged();
    }
    
}
