
package science.unlicense.api.physic.integration;

import science.unlicense.api.physic.integration.Integrator;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.physic.DefaultWorld;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractIntegratorTest {

    public static final double EPSILON = 0.00000001;

    protected abstract Integrator createIntegrator();

    /**
     * Test fixed rigid bodies are not moved by the physics process.
     */
    @Test
    public void FixedTest(){

        final RigidBody sphere1 = new RigidBody(new Sphere(1), 1);
        sphere1.setMass(1);
        sphere1.setFixed(true);
        final NodeTransform trs1 = sphere1.getNodeTransform();

        final RigidBody sphere2 = new RigidBody(new Sphere(1), 1);
        sphere2.setMass(1);
        final NodeTransform trs2 = sphere2.getNodeTransform();

        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(createIntegrator());
        world.setDrag(10);
        world.getBodies().add(sphere1);
        world.getBodies().add(sphere2);

        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs1.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs1.getRotation());
        assertVNotEquals(new Vector(0, 0, 0), trs2.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs2.getRotation());

    }

    protected static void assertVEquals(Vector v1, Vector v2){
        Assert.assertArrayEquals(v1.toArrayDouble(), v2.toArrayDouble(), EPSILON);
    }

    protected static void assertVNotEquals(Vector v1, Vector v2){
        Assert.assertFalse(v1.equals(v2,EPSILON));
    }

    protected static void assertMEquals(Matrix m1, Matrix m2){
        Assert.assertArrayEquals(m1.toArrayDouble(), m2.toArrayDouble(), EPSILON);
    }

    protected static void assertMNotEquals(Matrix m1, Matrix m2){
        Assert.assertArrayEquals(m1.toArrayDouble(), m2.toArrayDouble(), EPSILON);
    }
}
