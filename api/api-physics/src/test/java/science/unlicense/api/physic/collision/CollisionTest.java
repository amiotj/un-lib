
package science.unlicense.api.physic.collision;

import junit.framework.TestCase;
import science.unlicense.api.physic.operation.Collision;
import org.junit.Test;
import org.junit.Assert;
import org.junit.Ignore;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class CollisionTest extends TestCase {

    private static final double DELTA = 0.000000001;

    ////////////////////////////////////////////////////////////////////////////
    // 2D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    @Test
    public void testRectanglePoint() throws OperationException {
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Point(105, 208), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0, 1), collision.getNormal());
        Assert.assertEquals(0d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testRectangleRectangle() throws OperationException{
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Rectangle(100, 208, 10, 10), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(-0, 1), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    @Test
    public void testRectangleCircle() throws OperationException{
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10), 1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Circle(new Vector(105, 202),3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
    }
    
    @Test
    public void testPolygonPoint() throws OperationException {
        RigidBody geom1 = new RigidBody(new Rectangle(100, 200, 10, 10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Point(105, 208), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0, 1), collision.getNormal());
        Assert.assertEquals(0d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testCirclePoint() throws OperationException{
        RigidBody geom1 = new RigidBody(new Circle(new Vector(100, 200),10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Point(100, 208), 1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(-0, 1), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }

    @Test
    public void testCircleCircle() throws OperationException{
        RigidBody geom1 = new RigidBody(new Circle(new Vector(100, 200),2),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Circle(new Vector(100, 203),3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(-0, 1), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // 3D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    @Test
    public void testSpherePlan() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Plane(new Vector(0,1,0),new Vector(0, 6, 0)),1);
        RigidBody geom2 = new RigidBody(new Sphere(new Vector(0,5,0),2),1);
        geom2.getMotion().getVelocity().setXYZ(0, -0.2, 0);
        //vertical penetration of 1 unit in Y

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,1,0), collision.getNormal());
        Assert.assertEquals(-3d, collision.getDistance(),DELTA);
        //with a velocity of -0.2 on Y, the sphere was 3y above at the first contact : 15seconds
        Assert.assertEquals(-3*(1/0.2), collision.getRollbackTime(), DELTA);

    }
    
    @Test
    public void testSpherePlan2() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Sphere(2),1);
        geom1.getNodeTransform().getTranslation().setXYZ(0, 5, 0);
        geom1.getNodeTransform().notifyChanged();
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Plane(new Vector(0,-1,0),new Vector(0, 0, 0)),1);
        geom2.getNodeTransform().getTranslation().setXYZ(0, 6, 0);
        geom2.getNodeTransform().notifyChanged();

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testSphereSphere() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Sphere(new Vector(0,20,0),10),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Sphere(new Vector(0,9,0),3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,-1,0), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testSphereSphere2() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Sphere(1),1);
        geom1.getNodeTransform().getTranslation().setXYZ(0,12.1,0);
        geom1.getNodeTransform().notifyChanged();
        
        //vertical penetration of 0.1 unit
        RigidBody geom2 = new RigidBody(new Sphere(3),1);
        geom2.getNodeTransform().getTranslation().setXYZ(0,16,0);
        geom2.getNodeTransform().notifyChanged();

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,1,0), collision.getNormal());
        Assert.assertEquals(-0.1d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testSphereCapsule() throws OperationException{
        RigidBody geom1 = new RigidBody(new Sphere(new Vector(0,9,0),3),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Capsule(10,3),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,-1,0), collision.getNormal());
        Assert.assertEquals(-2d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testSphereBBox() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Sphere(new Vector(0,5,0),2),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new BBox(new Vector(-2,1,-2),new Vector(2, 4, 2)),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,-1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }
    
    
    @Ignore
    @Test
    public void testSphereObbox() throws OperationException{
    }
    
    @Ignore
    @Test
    public void testCapsuleObbox() throws OperationException{
    }

    @Test
    public void testCapsuleCapsule() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Capsule(new Vector(0, 4, 0), new Vector(0, 5, 0),2),1);
        //vertical penetration of 1 unit
        RigidBody geom2 = new RigidBody(new Capsule(new Vector(0, 8, 0), new Vector(0, 9, 0),2),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }
    
    @Ignore
    @Test
    public void testObboxObbox() throws OperationException{
    }
    
    @Test
    public void testPlaneSphere() throws OperationException{
        RigidBody geom1 = new RigidBody(new Sphere(new Vector(0,9,0),3),1);
        //vertical penetration of 1 unit
        RigidBody geom2 = new RigidBody(new Plane(new Vector(0, 1, 0),new Vector(0, 7, 0)),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,-1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }
    
    @Test
    public void testPlaneCapsule() throws OperationException{
        
        RigidBody geom1 = new RigidBody(new Capsule(new Vector(0, 4, 0), new Vector(0,5,0),2),1);
        //vertical penetration of 2 unit
        RigidBody geom2 = new RigidBody(new Plane(new Vector(0,-1,0),new Vector(0, 6, 0)),1);

        Collision collision = (Collision) Operations.execute(new Collision(geom1, geom2));
        Assert.assertNotNull(collision);
        Assert.assertTrue(collision.isCollide());
        Assert.assertEquals(new Vector(0,1,0), collision.getNormal());
        Assert.assertEquals(-1d, collision.getDistance(),DELTA);
    }
    
    @Ignore
    @Test
    public void testPlaneObbox() throws OperationException{
    }
}
