

package science.unlicense.api.physic.constraint;

import science.unlicense.api.physic.constraint.TranslationConstraint;
import org.junit.Test;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.geometry.BBox;

import org.junit.Assert;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class TranslationConstraintTest {
    
    private static final double DELTA = 0.0000000001;
    
    /**
     * Test a node contraint relative to parent position.
     */
    @Test
    public void centeredTest(){
        
        final SceneNode node = new DefaultSceneNode(3);
        final BBox limits = new BBox(
                new double[]{-10,-20,-30}, 
                new double[]{+10,+20,+30});
        
        final TranslationConstraint tc = new TranslationConstraint(node, limits, true);
        
        //within tests
        node.getNodeTransform().getTranslation().setXYZ(-9, -18, -27);
        tc.apply();
        Assert.assertEquals(new Vector(-9, -18, -27), node.getNodeTransform().getTranslation());
        
        node.getNodeTransform().getTranslation().setXYZ(+9, +18, +27);
        tc.apply();
        Assert.assertEquals(new Vector(+9, +18, +27), node.getNodeTransform().getTranslation());
        
        //outside tests
        node.getNodeTransform().getTranslation().setXYZ(-15, -21, -89);
        tc.apply();
        Assert.assertEquals(new Vector(-10, -20, -30), node.getNodeTransform().getTranslation());
        
        node.getNodeTransform().getTranslation().setXYZ(+15, +21, +89);
        tc.apply();
        Assert.assertEquals(new Vector(+10, +20, +30), node.getNodeTransform().getTranslation());
    }
    
}
