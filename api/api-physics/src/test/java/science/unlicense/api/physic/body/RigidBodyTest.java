
package science.unlicense.api.physic.body;

import science.unlicense.api.physic.body.RigidBody;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class RigidBodyTest {

    /**
     * Single body set world position test.
     */
    @Test
    public void setWorldPositionTest(){
        final RigidBody body = new RigidBody(new Circle(2),1);
        body.setWorldPosition(new Vector(100, 200));
        
        Assert.assertEquals(new Matrix3x3(
                1, 0, -100,
                0, 1, -200,
                0, 0,  1),
                body.getRootToNodeSpace().toMatrix());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 100,
                0, 1, 200,
                0, 0,  1),
                body.getNodeTransform().asMatrix());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 100,
                0, 1, 200,
                0, 0,  1),
                body.getNodeToRootSpace().toMatrix());
        
        Assert.assertEquals(new Vector(100,200), body.getWorldPosition(null));
    }

    @Test
    public void setRootToNodeSpace_SingleTest(){
        final RigidBody body = new RigidBody(new Sphere(2),1);
        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2));

        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());

        Assert.assertEquals(new Vector(-9,3,-2), body.getWorldPosition(null));
    }

    @Test
    public void setRootToNodeSpace_ParentNoUpdateTest(){
        final DefaultSceneNode parent = new DefaultSceneNode(3);
        final RigidBody body = new RigidBody(new Sphere(2),1);
        parent.getChildren().add(body);

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2));

        //body should have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0,-9,
                0, 1, 0, 3,
                0, 0, 1,-2,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());
        //parent should not have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1),
                parent.getNodeTransform().asMatrix());

        Assert.assertEquals(new Vector(-9,3,-2), body.getWorldPosition(null));
    }

    /**
     * Test the update mode modifies the parent transform and not the body.
     */
    @Test
    public void setRootToNodeSpace_ParentUpdateTest(){
        final DefaultSceneNode parent = new DefaultSceneNode(3);
        final RigidBody body = new RigidBody(new Sphere(2),1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        body.getNodeTransform().set(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0,-2,
                0, 0, 1, 3,
                0, 0, 0, 1));
        parent.getChildren().add(body);

        body.setRootToNodeSpace(new Affine3(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2));

        //body should have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 9,
                0, 1, 0,-3,
                0, 0, 1, 2,
                0, 0, 0, 1),
                body.getRootToNodeSpace().toMatrix());
        //but not it's trandform
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 1,
                0, 1, 0,-2,
                0, 0, 1, 3,
                0, 0, 0, 1),
                body.getNodeTransform().asMatrix());
        //parent should have moved
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0,-10,
                0, 1, 0, 5,
                0, 0, 1,-5,
                0, 0, 0, 1),
                parent.getNodeTransform().asMatrix());

        Assert.assertEquals(new Vector(-9,3,-2), body.getWorldPosition(null));
    }
    
}
