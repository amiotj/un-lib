

package science.unlicense.api.physic.integration;

import science.unlicense.api.physic.integration.EulerIntegrator;
import science.unlicense.api.physic.integration.Integrator;
import org.junit.Test;
import science.unlicense.api.physic.DefaultWorld;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public class EulerIntegrationTest extends AbstractIntegratorTest{
    
    @Override
    protected Integrator createIntegrator() {
        return new EulerIntegrator(3);
    }

    @Test
    public void translationIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(1, 2, 3), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(2, 4, 6), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(2.5, 5, 7.5), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());
                
    }
    
    @Test
    public void translationIntermetiateIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(1, 2, 3), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(2, 4, 6), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(2.5, 5, 7.5), trs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), trs.getRotation());
                
    }
    
    @Test
    public void translationParentUpdateIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.getNodeTransform().getTranslation().setXYZ(0.1, 0.2, 0.3);
        rb.getNodeTransform().notifyChanged();
        rb.setMass(1);
        final NodeTransform rbtrs = rb.getNodeTransform();
        
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        final NodeTransform parenttrs = parent.getNodeTransform();
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(0.1, 0.2, 0.3), rbtrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), rbtrs.getRotation());
        assertVEquals(new Vector(5, 6, 7), parenttrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), parenttrs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(0.1, 0.2, 0.3), rbtrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), rbtrs.getRotation());
        assertVEquals(new Vector(6, 8, 10), parenttrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), parenttrs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(0.1, 0.2, 0.3), rbtrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), rbtrs.getRotation());
        assertVEquals(new Vector(6.5, 9, 11.5), parenttrs.getTranslation());
        assertMEquals(new Matrix3x3(1, 0, 0, 0, 1, 0, 0, 0, 1), parenttrs.getRotation());
                
    }
    
    @Test
    public void rotationIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector(0, 1, 0)), trs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector(0, 1, 0)), trs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector(0, 1, 0)), trs.getRotation());
        
    }
    
    @Test
    public void rotationIntermediateIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector(0, 1, 0)), trs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector(0, 1, 0)), trs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector(0, 1, 0)), trs.getRotation());
        
    }
    
    @Test
    public void rotationParentUpdateIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        final NodeTransform parenttrs = parent.getNodeTransform();
        
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(new Matrix3x3().setToIdentity(), trs.getRotation());
        assertVEquals(new Vector(4, 4, 4), parenttrs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector(0, 1, 0)), parenttrs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(new Matrix3x3().setToIdentity(), trs.getRotation());
        assertVEquals(new Vector(4, 4, 4), parenttrs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector(0, 1, 0)), parenttrs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(0, 0, 0), trs.getTranslation());
        assertMEquals(new Matrix3x3().setToIdentity(), trs.getRotation());
        assertVEquals(new Vector(4, 4, 4), parenttrs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector(0, 1, 0)), parenttrs.getRotation());
        
    }
    
    @Test
    public void translationAndRotationIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(1, 2, 3), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector(0, 1, 0)), trs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(2, 4, 6), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector(0, 1, 0)), trs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(2.5, 5, 7.5), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector(0, 1, 0)), trs.getRotation());
        
    }
    
    /**
     * Intermediate node in the scene
     */
    @Test
    public void translationAndRotationIntermetiateIntegrationTest(){
        
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMotion().getVelocity().setXYZ(1, 2, 3);
        rb.getMotion().getAngularVelocity().setXYZ(0, Maths.QUATER_PI, 0);
        rb.setMass(1);
        final NodeTransform trs = rb.getNodeTransform();
        
        final SceneNode parent = new DefaultSceneNode(3);
        parent.getNodeTransform().getTranslation().setXYZ(4, 4, 4);
        parent.getNodeTransform().notifyChanged();
        parent.getChildren().add(rb);
        
        
        final DefaultWorld world = new DefaultWorld(3);
        world.setIntegrationTimeStep(1);
        world.setIntegrator(new EulerIntegrator(3));
        world.setDrag(0);
        world.getBodies().add(rb);
        
        world.update(1);
        assertVEquals(new Vector(1, 2, 3), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI, new Vector(0, 1, 0)), trs.getRotation());
        
        world.update(1);
        assertVEquals(new Vector(2, 4, 6), trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2, new Vector(0, 1, 0)), trs.getRotation());
        
        world.setIntegrationTimeStep(0.5);
        world.update(0.5);
        assertVEquals(new Vector(2.5,5,7.5),trs.getTranslation());
        assertMEquals(Matrix3x3.createRotation3(Maths.QUATER_PI*2.5, new Vector(0, 1, 0)), trs.getRotation());
        
    }

    
    
}
