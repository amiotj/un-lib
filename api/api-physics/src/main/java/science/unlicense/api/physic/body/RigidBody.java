
package science.unlicense.api.physic.body;

import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.DefaultAffine;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;

/**
 * Physic body, mix from various 3d format (collada,pmx,...)
 * Reference : Collada 1.5.0 (Chapter 6: Physics Reference,p.200)
 *
 * Body with a regular shape and material.
 *
 * @author Johann Sorel
 */
public class RigidBody extends AbstractBody {

    public static final int UPDATE_LOCAL = 0;
    public static final int UPDATE_PARENT = 1;

    
    protected final Motion motion;
    protected Material material = new Material();
    protected Geometry geometry;
    protected int updateMode = UPDATE_LOCAL;
    protected boolean fixed = false;
    
    /**
     * 
     * @param geometry this body geometry, it will not be modified by physics
     * @param mass body mass
     */
    public RigidBody(Geometry geometry, double mass) {
        super(geometry.getDimension());
        this.geometry = geometry;
        this.motion = new Motion(geometry.getDimension());
        setMass(mass);
    }
    
    /**
     * Get rigid body material.
     * @return Material
     */
    public Material getMaterial() {
        return material;
    }

    /**
     * Set rigid body material.
     * @param material
     */
    public void setMaterial(Material material) {
        this.material = material;
    }

    /**
     * Get the body geometry.
     * @return Geometry
     */
    public Geometry getGeometry(){
        return geometry;
    }

    /**
     * Get the update mode for the setWorldToNodeTransform.
     * @return int , one of UPDATE_X
     */
    public int getUpdateMode() {
        return updateMode;
    }

    /**
     * Set the update mode for the setWorldToNodeTransform.
     * @param mode , one of UPDATE_X
     */
    public void setUpdateMode(int updateMode) {
        this.updateMode = updateMode;
    }

    public Motion getMotion() {
        return motion;
    }

    /**
     * Calculate acceleration.
     * force * (1/mass)
     * 
     * @param buffer
     * @return 
     */
    public Vector getAcceleration(Vector buffer){
        if(mass<=0){
            //negative check just in case
            buffer.setToZero();
        }else{
            motion.force.scale(1.0/mass,buffer);
        }
        return buffer;
    }
    
    public void setFixed(boolean fixed) {
        this.fixed = fixed;
        if(fixed) motion.velocity.setToZero();
    }

    public boolean isFixed() {
        return fixed;
    }

    public boolean isFree(){
        return !fixed;
    }
    
    public Vector getWorldPosition(Vector buffer){
        if(buffer==null){
            buffer = new Vector(getDimension());
        }else{
            buffer.setToZero();
        }
        final Affine m = getNodeToRootSpace();
        m.transform(buffer,buffer);
        return buffer;
    }
    
    /**
     * Translate body by given translation.
     * 
     * @param translation 
     */
    public void worldTranslate(Vector translation) {
        final AffineRW vm = DefaultAffine.create(translation.getSize());
        vm.setCol(translation.getSize(), translation.getValues());
        final AffineRW m = vm.multiply(getNodeToRootSpace()).localInvert();
        setRootToNodeSpace(m);
    }
    
    /**
     * Rotate body using given angular velocity.
     * 
     * @param rotation angular
     */
    public void worldRotate(Vector rotation) {
        final double angleRadians = rotation.length();
        if(angleRadians==0) return;
        
        final Vector axis = rotation.normalize();
        //force 3d
        final Vector rotAxis = new Vector(3);
        rotAxis.set(axis);
        
        final AffineRW m = DefaultAffine.create(getRootToNodeSpace());
        final Matrix3x3 vm = Matrix3x3.createRotation3(angleRadians, rotAxis);
        final AffineRW vms = DefaultAffine.create(m.getInputDimensions());
        final Matrix4x4 r4 = new Matrix4x4();
        r4.set(vm);
        vms.fromMatrix(r4);
        vms.localInvert();
        vms.localMultiply(m);
        setRootToNodeSpace(vms);
    }
        
    /**
     * transform body using given translation and angular velocity.
     * 
     * @param translation
     * @param rotation angular
     */
    public void transform(Vector translation, Vector rotation) {
        worldTranslate(translation);
        worldRotate(rotation);
    }
    
    public void setWorldPosition(Vector ve){
        worldTranslate(ve.subtract(getWorldPosition(null)));
    }

    public void setRootToNodeSpace(final Affine wtn) {
        if(parent==null){
            //no parent, update local
            getNodeTransform().set(wtn.invert());
        }else{
            if(updateMode==UPDATE_LOCAL){
                //move this node
                //calculate difference from parent
                AffineRW m = DefaultAffine.create(parent.getNodeToRootSpace());
                m = wtn.multiply(m);
                getNodeTransform().set(m.localInvert());
                
            }else{
                //update the parent transform
                AffineRW localtrs = DefaultAffine.create(getNodeTransform());
                localtrs = wtn.multiply(localtrs);
                parent.setRootToNodeSpace(localtrs);
            }            
        }
        
    }

}