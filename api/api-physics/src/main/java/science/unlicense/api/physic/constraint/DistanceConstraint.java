
package science.unlicense.api.physic.constraint;

import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.force.AbstractForce;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;

/**
 * Constraint distance between 2 bodies.
 *
 * @author Johann Sorel
 */
public class DistanceConstraint extends AbstractForce implements Constraint{

    private final RigidBody target1;
    private final RigidBody target2;
    private final double distance;

    public DistanceConstraint(RigidBody target1, RigidBody target2, double distance) {
        this.target1 = target1;
        this.target2 = target2;
        this.distance = distance;
    }

    public void apply() {
        final int dim = target1.getDimension();
        double[] center1 = new double[dim];
        double[] center2 = new double[dim];
        center1 = target1.getNodeToRootSpace().transform(center1,center1);
        center2 = target2.getNodeToRootSpace().transform(center2,center2);
        
        double[] direction = Vectors.subtract(center2, center1);
        double dist = Vectors.length(direction);
        
        if(this.distance==dist){
            return;
        }else if(dist==0){
            //bodies overlaps, push it upward
            direction[0] = 0;
            direction[1] = 1;
            direction[0] = 0;
        }
        
        dist -= distance;
        
        Vectors.normalize(direction, direction);
        Vectors.scale(direction, dist, direction);
        
        //TODO should we push back both ? 
        //make another constraint with rigid bodies to calculate ratio based on mass ?
        
        //push back second node
        direction = target2.getRootToNodeSpace().transform(direction, null);
        target2.getNodeTransform().getTranslation().localAdd(new Vector(direction).negate());
        target2.getNodeTransform().notifyChanged();
        
    }

}