
package science.unlicense.api.physic.law;

import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;

/**
 * Source : http://en.wikipedia.org/wiki/Damping
 * 
 * F = -cv
 * where :
 * c is the viscous damping coefficient
 * v is the velocity
 * 
 * @author Johann Sorel
 */
public final class Damping {
    
    private Damping(){}
    
    
    /**
     * Calculate force.
     * F = -cv
     * 
     * @param viscousCoeff
     * @param velocity
     * @return Vector force
     */
    public static Vector force(double viscousCoeff, Vector velocity){
        return velocity.scale(viscousCoeff).localNegate();
    }
    
    /**
     * Calculate viscous damping coefficient.
     * c = -(F/v)
     * 
     * @param force
     * @param velocity
     * @return double viscous damping coefficient
     */
    public static double viscousCoeff(Vector force, Vector velocity){
        final Vector t = force.divide(velocity).localNegate();
        return Maths.mean(t.getValues());
    }
    
    /**
     * Calculate velocity.
     * v = F/-c
     * 
     * @param force
     * @param viscousCoeff
     * @return double stiffness
     */
    public static Vector velocity(Vector force, double viscousCoeff){
        return force.scale(1/-(viscousCoeff));
    }
    
}
