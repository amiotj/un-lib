
package science.unlicense.api.physic.force;

import science.unlicense.api.physic.body.Particle;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class Spring extends AbstractForce {

    private double springStrength;
    private double damping;
    private double restLength;
    private final RigidBody a;
    private final RigidBody b;
    
    //caches for calculation
    private final Vector a2b;
    private final Vector va2b;

    /**
     * 
     * @param A first rigid body
     * @param B second rigid body
     * @param springStrength spring strength
     * @param damping damping value
     * @param restLength rest length, optimal size of the spring when no forces apply
     */
    public Spring(RigidBody A, RigidBody B, double springStrength, double damping, double restLength) {
        this.springStrength = springStrength;
        this.damping = damping;
        this.restLength = restLength;
        a = A;
        b = B;
        a2b = new Vector(A.getDimension());
        va2b = new Vector(A.getDimension());
    }

    public RigidBody getFirstEnd() {
        return a;
    }

    public RigidBody getSecondEnd() {
        return b;
    }

    /**
     * Get current spring length.
     * Distance between each end points.
     * @return length
     */
    public double getCurrentLength() {
        return Distance.distance(a.getWorldPosition(null), b.getWorldPosition(null));
    }

    public double getRestLength() {
        return restLength;
    }

    public void setRestLength(double l) {
        restLength = l;
    }

    public double getStrength() {
        return springStrength;
    }

    public void setStrength(double ks) {
        springStrength = ks;
    }

    public double getDamping() {
        return damping;
    }

    public void setDamping(double d) {
        damping = d;
    }

    public void apply(){
        if(!enable) return;
        
        if( (a instanceof Particle && !((Particle)a).isFree()) &&
            (b instanceof Particle && !((Particle)b).isFree())){
            return;
        }
        

        final Vector posA = a.getWorldPosition(null);
        final Vector posB = b.getWorldPosition(null);
        posA.subtract(posB, a2b);

        final double a2bDistance = a2b.length();

        if(a2bDistance == 0){
            a2b.setToZero();
        }else{
            a2b.localScale(1.0/a2bDistance);
        }

        // spring force is proportional to how much it stretched
        final double springForce = -( a2bDistance - restLength ) * springStrength;

        // want velocity along line b/w a & b, damping force is proportional to this
        final Vector velA = a.getMotion().getVelocity();
        final Vector velB = b.getMotion().getVelocity();
        velA.subtract(velB, va2b);

        final double dampingForce = -damping * a2b.dot(va2b);

        // forceB is same as forceA in opposite direction
        final double r = springForce + dampingForce;
        a2b.localScale(r);

        if(a instanceof Particle){
            if(((Particle)a).isFree()) a.getMotion().getForce().localAdd(a2b);
        }else{
            a.getMotion().getForce().localAdd(a2b);
        }
        if(b instanceof Particle){
            if(((Particle)b).isFree()) b.getMotion().getForce().localSubtract(a2b);
        }else{
            b.getMotion().getForce().localSubtract(a2b);
        }
        
    }

}