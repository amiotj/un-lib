
package science.unlicense.api.physic.constraint;

import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class CopyTranslationConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    public CopyTranslationConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        applyConstraint(target, toCopy, factor);
    }

    static void applyConstraint(SceneNode target, SceneNode toCopy, float factor) {
        final Vector trs = toCopy.getNodeTransform().getTranslation();
        final Vector baseTrs = target.getNodeTransform().getTranslation();
        trs.scale(factor, baseTrs);
        target.getNodeTransform().notifyChanged();
    }

}