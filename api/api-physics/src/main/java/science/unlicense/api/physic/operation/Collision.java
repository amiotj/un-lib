
package science.unlicense.api.physic.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.operation.AbstractBinaryOperation;
import science.unlicense.impl.math.Vector;

/**
 * Collision operation.
 * 
 * @author Johann Sorel
 */
public class Collision extends AbstractBinaryOperation {
    
    public static final Chars NAME = new Chars(new byte[]{'C','O','L','L','I','S','I','O','N'});
        
    private boolean collide = false;
    private Vector normal;
    private double distance;
    private Geometry[] impactGeometries;

    //rollback in time to find first contact
    private double timeRollback;

    public Collision(Body first, Body second) {
        super(first,second);
    }

    public Chars getName() {
        return NAME;
    }
    
    public void reset(Body first, Body second){
        this.first = first;
        this.second = second;
        this.collide = false;
        this.normal = null;
        this.distance = 0;
        impactGeometries = null;
        timeRollback = 0.0;
    }
    
    public Body getFirst() {
        return (Body)super.getFirst();
    }

    public Body getSecond() {
        return (Body)super.getSecond();
    }

    public void setFirst(Body first) {
        this.first = first;
    }

    public void setSecond(Body second) {
        this.second = second;
    }

    public boolean isCollide() {
        return collide;
    }

    public void setCollide(boolean collide) {
        this.collide = collide;
    }
    
    /**
     * Direction to push back second body.
     * @return Vector in world space
     */
    public Vector getNormal() {
        return normal;
    }

    /**
     * Set direction to push back second body.
     * 
     * @param normal in world space
     */
    public void setNormal(Vector normal) {
        this.normal = normal;
    }

    /**
     * Get distance between object.
     * Position value when objects do not collide
     * Negative value is a the penetration depth.
     * @return distance or penetration if negative
     */
    public double getDistance() {
        return distance;
    }

    /**
     * 
     * @param distance 
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Geometry[] getImpactGeometries() {
        return impactGeometries;
    }

    public void setImpactGeometries(Geometry[] impactGeometries) {
        this.impactGeometries = impactGeometries;
    }

    public void setRollbackTime(double time){
        this.timeRollback = time;
    }

    /**
     * Returns the time backward to the first contact.
     * @return time in seconds
     */
    public double getRollbackTime() {
        return timeRollback;
    }

    
}