
package science.unlicense.api.electronic;

/**
 * Ohm law.
 *
 * @author Johann Sorel
 */
public final class OhmLaw{

    private OhmLaw(){}

    /**
     * Calculate voltage.
     * V = R*I
     *
     * @param r resistance in ohms
     * @param i current in amperes
     * @return volts
     */
    public static double voltage(double r, double i){
        return r*i;
    }

    /**
     * Calculate resistance.
     * R = V/I
     *
     * @param v voltage in volts
     * @param i current in amperes
     * @return resistance in ohms
     */
    public static double resistance(double v, double i){
        return v/i;
    }

    /**
     * Calculate resistance.
     * I = V/R
     *
     * @param v voltage in volts
     * @param r resistance in ohms
     * @return current in amperes
     */
    public static double current(double v, double r){
        return v/r;
    }

}