
package science.unlicense.api.physic.operation;

import java.util.Random;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.operation.AbstractBinaryOperation;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.operation.GridOperations;
import science.unlicense.impl.geometry.operation.Nearest;
import science.unlicense.api.geometry.operation.Operation;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.operation.OperationExecutor;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Grid;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class CollisionExecutors {

    private CollisionExecutors(){}

    /**
     * Collision for cercle and sphere.
     */
    private static Collision radiusCollision(Collision collision,
                            final Tuple c1, final double r1,
                            final Tuple c2, final double r2){

        //we could use distance and intersect operations but this would duplicate
        //multiple calculation, so we do it here.
        final double[] diff = Vectors.subtract(c2.getValues(),c1.getValues());
        final double r = r1+r2;
        final double rSquare = r*r;
        double length = Vectors.lengthSquare(diff);

        if(length > rSquare){
            //no collision
            collision.setCollide(false);
            return collision;
        }

        length = Math.sqrt(length);
        collision.setCollide(true);

        if(length==0){
            //bodies overlap, push it aside.
            collision.setDistance(-r1);
            final Vector normal = new Vector(diff.length);
            normal.set(1, 1);
            collision.setNormal(normal);
        }else{
            collision.setDistance(-(r-length));
            collision.setNormal(new Vector(Vectors.scale(diff, 1/length)));
        }
        return collision;
    }

    /**
     * Collision for rectangle and bbox.
     */
    private static Collision boxCollision(Collision collision,
                                    final double[] min1, double[] max1,
                                    final double[] min2, double[] max2){

        //find if it intersects
        final double[] overlap = new double[min1.length];
        for(int i=0;i<min1.length;i++){
            if(max1[i] < min2[i] || max2[i] < min1[i]){
                //no overlap
                collision.setCollide(false);
                return collision;
            }
            overlap[i] = Maths.min(max1[i], max2[i]) - Maths.max(min1[i], min2[i]);
        }

        //find normal direction
        final double[] c1 = Vectors.add(min1, max1);
        Vectors.scale(c1, 0.5, c1);
        final double[] c2 = Vectors.add(min2, max2);
        Vectors.scale(c2, 0.5, c2);
        Vectors.subtract(c2, c1, c2);
        Vectors.normalize(c2, c2);

        collision.setCollide(true);
        collision.setNormal(new Vector(c2));
        collision.setDistance(-Maths.min(overlap));

        return collision;
    }
    
    private static int intersectSegment(double[] a, double[] b, double[] i, double[] p) {
        final double[] d = {b[0] - a[0], b[1] - a[1]};
        final double[] e = {p[0] - i[0], p[1] - i[1]};
        double denom = d[0]*e[1] - d[1]*e[0];
        if (denom == 0) {
            return -1;
        }
        double t = - (a[0]*e[1] - i[0]*e[1] - e[0]*a[1] + e[0]*i[1]) / denom;
        if (t < 0 || t > 1) {
            return 0;
        }
        t = - (- d[0]*a[1] + d[0]*i[1] + a[0]*d[1] - i[0]*d[1]) / denom;
        if (t < 0 || t > 1) {
            return 0;
        }
        return 1;
    }
    
    private static boolean collision(TupleBuffer1D points, Tuple point) {
        // TODO use random API when it exists
        // we choose a point which is far from the polygon
        Random r = new Random();
        double x = 10000 + r.nextDouble();
        double y = 10000 + r.nextDouble();
        final double[] i = {x, y};
        
        // we count nb of intersections, if it's even the point is outside
        // if it's odd the point is inside
        int nbIntersections = 0;
        int last = points.getDimension()-1;
        final double[] a = new double[points.getSampleCount()];
        final double[] b = new double[a.length];
        for (int cpt = 0; cpt < last; cpt++) {
            points.getTupleDouble(cpt,a);
            points.getTupleDouble(cpt+1,b);
            int iseg = intersectSegment(a,
                b, i, new double[]{point.getX(), point.getY()});
            if ((iseg == -1)) {
                return collision(points, point);
            }
            nbIntersections += iseg;
        }
        if (nbIntersections % 2 == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    private static double polygonArea(TupleBuffer1D points) {
        double area = 0;
        int last = points.getDimension()-1;
        final double[] a = new double[points.getSampleCount()];
        final double[] b = new double[a.length];
        for (int cpt = 0; cpt < last; cpt++) {
            points.getTupleDouble(cpt,a);
            points.getTupleDouble(cpt+1,b);
            area += a[0]*b[1] - b[0]*a[1];
        }
        return area / 2;
    }
    
    private static double[] polygonCenter(TupleBuffer1D points, double area) {
        double[] center = new double[2];
        int last = points.getDimension()-1;
        final double[] a = new double[points.getSampleCount()];
        final double[] b = new double[a.length];
        for (int cpt = 0; cpt < last; cpt++) {
            points.getTupleDouble(cpt,a);
            points.getTupleDouble(cpt+1,b);
            center[0] += (a[0] + b[0]) * (a[0]*b[1] - b[0]*a[1]);
            center[1] += (a[1] + b[1]) * (a[0]*b[1] - b[0]*a[1]);
        }
        center[0] /= 6*area;
        center[1] /= 6*area;
        return center;
    }
    
    private static Collision pointPolygonCollision(Collision collision, TupleBuffer1D outer, Sequence inners, Tuple point) {
        boolean pointInside = collision(outer, point);
        if (pointInside && inners != null) {
            for(int i=0,n=inners.getSize();i<n;i++){
                final Polyline hole = (Polyline) inners.get(i);
                pointInside = !collision(hole.getCoordinates(), point);
            }
            
        }
        if (!pointInside) {
            collision.setCollide(false);
            return collision;
        }
        
        //find normal direction
        final double[] c1 = polygonCenter(outer, polygonArea(outer));
        final double[] c2 = {point.getX(), point.getY()};
        Vectors.subtract(c2, c1, c2);
        Vectors.normalize(c2, c2);

        collision.setCollide(true);
        collision.setNormal(new Vector(c2));
        collision.setDistance(0d);
        
        return collision;
    }
    
    private static Collision polygonPolygonCollision(Collision collision, TupleBuffer1D coord1, Sequence holes1, TupleBuffer1D coord2, Sequence holes2) {
        // TODO : un polygone est une suite de points, on peut donc réutiliser l'algo de la méthode pointPolygonCollision de façon itérative
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static abstract class CollisionExecutor implements OperationExecutor{

        protected final Class firstGeomClass;
        protected final Class secondGeomClass;
        protected final boolean canInvert;

        public CollisionExecutor(Class firstGeomClass, Class secondGeomClass, boolean canInvert) {
            this.firstGeomClass = firstGeomClass;
            this.secondGeomClass = secondGeomClass;
            this.canInvert = canInvert;
        }

        public Class getOperationClass() {
            return Collision.class;
        }
        
        @Override
        public Object execute(Operation operation) throws OperationException {
            final Collision op = (Collision) operation;
            RigidBody firstRB = ((RigidBody)op.getFirst());
            RigidBody secondRB = ((RigidBody)op.getSecond());
            Geometry first = firstRB.getGeometry();
            Geometry second = secondRB.getGeometry();
            
            //convert geometries to world space for collision test
            first = (Geometry) Operations.execute(new Transform(first, firstRB.getNodeToRootSpace()));
            second = (Geometry) Operations.execute(new Transform(second, secondRB.getNodeToRootSpace()));
            
            
            boolean inverted = false;
            if(canInvert && !(firstGeomClass.isInstance(first) && secondGeomClass.isInstance(second))){
                //set geometries in expected order
                inverted = true;
                //invert geometries
                Geometry temp = first;
                first = second;
                second = temp;
                //invert rb
                RigidBody tempRB = firstRB;
                firstRB = secondRB;
                secondRB = tempRB;
            }
            try{
                executeInternal(op, firstRB, first, secondRB, second);
            }catch(Exception ex){
                return op;
            }
            if(op.isCollide()){
                //convert impact position back to geometry space TODO
//                final Geometry[] impacts = op.getImpactGeometries();
//                impacts[0] = (Geometry) Operations.execute(new Transform(impacts[0], firstRB.getRootToNodeSpace()));
//                impacts[1] = (Geometry) Operations.execute(new Transform(impacts[1], secondRB.getRootToNodeSpace()));
                
                if(inverted){
                    //flip the normal if first<>second have been inverted
                    op.getNormal().localNegate();
//                    Geometry imp = impacts[0];
//                    impacts[0] = impacts[1];
//                    impacts[1] = imp;
                }
                
                
            }

            //validate collision values
            if(op.isCollide() && !op.getNormal().isFinite()){
                throw new RuntimeException("Collision normal is not finite");
            }

            return op;
        }

        protected abstract Object executeInternal(Collision op,
                RigidBody firstRB, Object first,
                RigidBody secondRB, Object second) throws OperationException;

        @Override
        public boolean canHandle(Operation operation) {
            if(!(operation instanceof Collision))return false;
            final AbstractBinaryOperation op = (AbstractBinaryOperation) operation;
            if(!(op.getFirst() instanceof RigidBody)) return false;
            if(!(op.getSecond()instanceof RigidBody)) return false;
            final Object first = ((RigidBody)op.getFirst()).getGeometry();
            final Object second = ((RigidBody)op.getSecond()).getGeometry();
            if(canInvert){
                return (firstGeomClass.isInstance(first)  && secondGeomClass.isInstance(second))
                    || (firstGeomClass.isInstance(second) && secondGeomClass.isInstance(first));
            }else{
                return (firstGeomClass.isInstance(first)  && secondGeomClass.isInstance(second));
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // 2D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    public static final CollisionExecutor RECTANGLE_POINT =
            new CollisionExecutor(Rectangle.class, Point.class, true) {
        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Rectangle rect = (Rectangle) first;
            final Point point = (Point) second;
            return boxCollision(collision, new double[]{rect.getX()                , rect.getY()},
                                           new double[]{rect.getX()+rect.getWidth(), rect.getY()+rect.getHeight()},
                                           new double[]{point.getX()               , point.getY()},
                                           new double[]{point.getX()               , point.getY()});
        }
    };

    public static final CollisionExecutor RECTANGLE_RECTANGLE =
            new CollisionExecutor(Rectangle.class, Rectangle.class, false){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Rectangle rect1 = (Rectangle) first;
            final Rectangle rect2 = (Rectangle) second;
            return boxCollision(collision, new double[]{rect1.getX()                 ,rect1.getY()},
                                           new double[]{rect1.getX()+rect1.getWidth(),rect1.getY()+rect1.getHeight()},
                                           new double[]{rect2.getX()                 ,rect2.getY()},
                                           new double[]{rect2.getX()+rect2.getWidth(),rect2.getY()+rect2.getHeight()});
        }
    };

    public static final CollisionExecutor RECTANGLE_CIRCLE =
            new CollisionExecutor(Rectangle.class, Circle.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Rectangle rect = (Rectangle)first;
            final Circle circle  = (Circle)second;

            return collision;
        }
    };
    
    public static final CollisionExecutor POLYGON_POINT =
            new CollisionExecutor(Polygon.class, Point.class, true) {
        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB, Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Polygon pol = (Polygon) first;
            final Point point = (Point) second;
            return pointPolygonCollision(collision, pol.getExterior().getCoordinates(), pol.getHoles(), point.getCoordinate());
        }
    };
    
    public static final CollisionExecutor POLYGON_POLYGON =
            new CollisionExecutor(Polygon.class, Point.class, true) {
        @Override
        protected Object executeInternal(Collision op, RigidBody firstRB, Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Polygon pol1 = (Polygon) first;
            final Polygon pol2 = (Polygon) second;
            return polygonPolygonCollision(collision, pol1.getExterior().getCoordinates(), pol1.getHoles(), pol2.getExterior().getCoordinates(), pol2.getHoles());
        }
    };
    
    public static final CollisionExecutor CIRCLE_POINT =
            new CollisionExecutor(Circle.class, Point.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Circle circle = (Circle)first;
            final Point point = (Point)second;

            return radiusCollision(collision,
                    circle.getCenter(), circle.getRadius(),
                    point.getCoordinate(), 0);
        }
    };

    public static final CollisionExecutor CIRCLE_CIRCLE =
            new CollisionExecutor(Circle.class, Circle.class, false){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Circle circle1 = (Circle)first;
            final Circle circle2 = (Circle)second;

            return radiusCollision(collision,
                    circle1.getCenter(), circle1.getRadius(),
                    circle2.getCenter(), circle2.getRadius());
        }
    };

    ////////////////////////////////////////////////////////////////////////////
    // 3D //////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public static final CollisionExecutor PLANE_SPHERE =
            new CollisionExecutor(Plane.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Plane plane = (Plane)first;
            final Sphere sphere = (Sphere)second;
            final double radius = sphere.getRadius();            
            final double realDistance = Distance.distance(sphere.getCenter(), plane);
            final double penetration;
            final boolean collide;
            if(realDistance<=0){
                //sphere center is under the plan
                collide = true;
                penetration = realDistance - radius;
            }else{
                collide = realDistance < radius;
                penetration = (collide) ? -(radius-realDistance) : (radius-realDistance);
            }
            collision.setCollide(collide);
            collision.setNormal(plane.getNormal().copy());
            collision.setDistance(penetration);

            if(collide){
                //calculate collision points
                final double[] impactPoint = Geometries.projectPointOnPlan(sphere.getCenter().getValues(), plane.getNormal().getValues(), plane.getD());
                collision.setImpactGeometries(new Geometry[]{
                    new Point(impactPoint),
                    new Point(impactPoint)
                });

                //calculate rollback to find first contact
                final Vector velocityA = firstRB.getMotion().getVelocity();
                final Vector velocityB = secondRB.getMotion().getVelocity();
                final Vector relativeVelocity = velocityB.subtract(velocityA);

                final double angle = plane.calculateAngle(relativeVelocity);
                final double rollbackDist = penetration / Math.sin(angle);
                final double rollbackTime = rollbackDist / relativeVelocity.length();
                collision.setRollbackTime(rollbackTime);
                
            }
            return collision;
        }
    };


    public static final CollisionExecutor SEGMENT_SPHERE =
            new CollisionExecutor(Segment.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Segment segment = (Segment)first;
            final Sphere sphere = (Sphere)second;
            final double radius = sphere.getRadius();

            final double[] nearest = new double[segment.getStart().getSize()];
            final double distanceSquare = Distance.distanceSquare(segment.getStart().getValues(), segment.getEnd().getValues(), nearest, sphere.getCenter().getValues(), new double[1], op.getEpsilon());
            final double realDistance = Math.sqrt(distanceSquare);


            final boolean collide = realDistance < radius;
            final double penetration = (collide) ? -(radius-realDistance) : (radius-realDistance);
            final Vector normal = new Vector(Arrays.copy(nearest, new double[nearest.length]));
            normal.localSubtract(sphere.getCenter());
            normal.localNormalize();
            
            collision.setCollide(collide);
            collision.setNormal(normal);
            collision.setDistance(penetration);

            if(collide){
                //calculate collision points
                collision.setImpactGeometries(new Geometry[]{
                    new Point(Arrays.copy(nearest,new double[nearest.length])),
                    new Point(Arrays.copy(nearest,new double[nearest.length]))
                });

                //calculate rollback to find first contact
                final Vector velocityA = firstRB.getMotion().getVelocity();
                final Vector velocityB = secondRB.getMotion().getVelocity();
                final Vector relativeVelocity = velocityB.subtract(velocityA);

                final Plane p = new Plane(normal, new Vector(Arrays.copy(nearest,new double[nearest.length])));
                final double angle = p.calculateAngle(relativeVelocity);
                final double rollbackDist = penetration / Math.sin(angle);
                final double rollbackTime = rollbackDist / relativeVelocity.length();
                collision.setRollbackTime(rollbackTime);

            }
            return collision;
        }
    };

    public static final CollisionExecutor BBOX_BBOX =
            new CollisionExecutor(BBox.class, BBox.class, false){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final BBox bbox1 = (BBox)first;
            final BBox bbox2 = (BBox)second;
            return boxCollision(collision, bbox1.getLower().getValues(),
                                           bbox1.getUpper().getValues(),
                                           bbox2.getLower().getValues(),
                                           bbox2.getUpper().getValues());
        }
    };

    public static final CollisionExecutor BBOX_SPHERE =
            new CollisionExecutor(BBox.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final BBox bbox = (BBox)first;
            final Sphere sphere = (Sphere)second;
            final Tuple center = sphere.getCenter();
            final double radius = sphere.getRadius();

            //find nearest plan
            int index = -1;
            double d = Double.POSITIVE_INFINITY;
            for(int i=0;i<3;i++){
                double c = center.get(i);
                double d1 = Math.abs(c-bbox.getMax(i));
                double d2 = Math.abs(c-bbox.getMin(i));
                if(d1<d){
                    d = d1;
                    index = i*2;
                }
                if(d2<d){
                    d = d2;
                    index = i*2 + 1;
                }
            }
            
            //check if point projects on the plan
            final Plane plane;
            switch(index){
                case 0 : plane = new Plane(new Vector(+1, 0, 0), new Vector(bbox.getMax(0),0,0)); break;
                case 1 : plane = new Plane(new Vector(-1, 0, 0), new Vector(bbox.getMin(0),0,0)); break;
                case 2 : plane = new Plane(new Vector( 0,+1, 0), new Vector(0,bbox.getMax(1),0)); break;
                case 3 : plane = new Plane(new Vector( 0,-1, 0), new Vector(0,bbox.getMin(1),0)); break;
                case 4 : plane = new Plane(new Vector( 0, 0,+1), new Vector(0,0,bbox.getMax(2))); break;
                case 5 : plane = new Plane(new Vector( 0, 0,-1), new Vector(0,0,bbox.getMin(2))); break;
                default : throw new RuntimeException("Invalid index");
            }
            final double[] proj = Geometries.projectPointOnPlan(center.getValues(), plane.getNormal().getValues(), plane.getD());
            if(bbox.intersects(new Vector(proj))){
                //point is on or inside the face
                return PLANE_SPHERE.executeInternal(op, firstRB, plane, secondRB, second);
            }else{
                //point is outside the face
                //check if it's not too far
                double dist = Distance.distance(center, plane);
                if(dist>radius) return collision;

                //check this face edges
                final double min0 = bbox.getMin(0);
                final double max0 = bbox.getMax(0);
                final double min1 = bbox.getMin(1);
                final double max1 = bbox.getMax(1);
                final double min2 = bbox.getMin(2);
                final double max2 = bbox.getMax(2);
                final Segment[] edges;
                switch(index){
                    case 0 :
                    case 1 : {
                             final double k = index==0 ? max0 : min0;
                             edges = new Segment[]{
                                new Segment(new Vector(k,min1,min2),new Vector(k,min1,max2)),
                                new Segment(new Vector(k,min1,max2),new Vector(k,max1,max2)),
                                new Segment(new Vector(k,max1,max2),new Vector(k,max1,min2)),
                                new Segment(new Vector(k,max1,min2),new Vector(k,min1,min2))
                             };
                             }break;
                    case 2 :
                    case 3 : {
                             final double k = index==2 ? max1 : min1;
                             edges = new Segment[]{
                                new Segment(new Vector(min0,k,min2),new Vector(min0,k,max2)),
                                new Segment(new Vector(min0,k,max2),new Vector(max0,k,max2)),
                                new Segment(new Vector(max0,k,max2),new Vector(max0,k,min2)),
                                new Segment(new Vector(max0,k,min2),new Vector(min0,k,min2))
                             };
                             }break;
                    case 4 :
                    case 5 : {
                             final double k = index==4 ? max2 : min2;
                             edges = new Segment[]{
                                new Segment(new Vector(min0,min1,k),new Vector(min0,max1,k)),
                                new Segment(new Vector(min0,max1,k),new Vector(max0,max1,k)),
                                new Segment(new Vector(max0,max1,k),new Vector(max0,min1,k)),
                                new Segment(new Vector(max0,min1,k),new Vector(min0,min1,k))
                             };
                             }break;

                    default : throw new RuntimeException("Invalid index");
                }

                //find nearest edge
                d = Double.POSITIVE_INFINITY;
                Segment edge = null;
                for(int i=0;i<4;i++){
                    dist = Distance.distance(edges[i].getStart(), edges[i].getEnd(), center);
                    if(dist < radius && dist<d){
                        edge = edges[i];
                        d = dist;
                    }
                }

                if(edge!=null){
                    return SEGMENT_SPHERE.executeInternal(op, firstRB, edge, secondRB, second);
                }
            }

            return collision;
        }
    };

    public static final CollisionExecutor BBOX_CAPSULE =
            new CollisionExecutor(BBox.class, Capsule.class, true){

        protected Object executeInternal(Collision collision, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final BBox bbox = (BBox)first;
            final Capsule capsule = (Capsule)second;

            //TODO
            collision.setCollide(false);
            if(true) return collision;
            
            //find the nearest point on the bbox
            final Geometry[] nearest = bbox.nearest(new Segment(capsule.getFirst(),capsule.getSecond()));
            final Vector direction = new Vector(nearest[0].getCentroid().getCoordinate());
            direction.localSubtract(nearest[1].getCentroid().getCoordinate());
            final double distance = direction.length();
            boolean collide = distance <= capsule.getRadius();
            
            collision.setCollide(collide);
            
            //calculate the push back direction
            if(distance>0){
                //center of the sphere is outside the bbox
                collision.setNormal(direction.negate().localNormalize());
                collision.setDistance(-(capsule.getRadius()-distance));
            }else{
                //center of the sphere is inside the bbox
                //find the closest border
                //TODO
//                final Vector bboxCenter = new Vector(bbox.getCentroid().getCoordinate());
//                bboxCenter.localSubtract(sphere.getCenter());
//                final double[] diff = bboxCenter.getValues();
//                final int axis;
//                if(diff[0]<diff[1] && diff[0]<diff[2]) axis=0;
//                else if(diff[1]<diff[0] && diff[1]<diff[2]) axis=1;
//                else axis = 2;
//                
//                collision.setDistance(sphere.getRadius());
                
                //push it back up for now
                collision.setNormal(new Vector(0, 1, 0));
                collision.setDistance(capsule.getRadius());
                
            }
            
            return collision;
        }
    };

    public static final CollisionExecutor SPHERE_SPHERE =
            new CollisionExecutor(Sphere.class, Sphere.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            final Sphere sphere1 = (Sphere)first;
            final Sphere sphere2 = (Sphere)second;

            return radiusCollision(collision,
                    sphere1.getCenter(), sphere1.getRadius(),
                    sphere2.getCenter(), sphere2.getRadius());
        }
    };

    public static final CollisionExecutor SPHERE_CAPSULE =
            new CollisionExecutor(Sphere.class, Capsule.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            
            final Sphere sphere = (Sphere)first;
            final double[] sphereFirst = sphere.getCenter().getValues();
            final double[] sphereSecond = sphereFirst;
            final double[] sphereClosest = new double[3];
            
            final Capsule capsule = (Capsule)second;
            final double[] capsuleFirst = capsule.getFirst().getValues();
            final double[] capsuleSecond = capsule.getSecond().getValues();
            final double[] capsuleClosest = new double[3];
            
            final double[] ratio = new double[2];
            
            double distance = Distance.distance(
                    capsuleFirst, capsuleSecond, capsuleClosest, 
                    sphereFirst, sphereSecond, sphereClosest, 
                    ratio, op.getEpsilon());
            
            //remove radius
            distance -= sphere.getRadius();
            distance -= capsule.getRadius();
            
            collision.setImpactGeometries(new Geometry[]{new Point(sphereClosest),new Point(capsuleClosest)});

            Vector normal;
            if(distance<=0){
                //capsule segment and sphere center intersect
                normal = new Vector(Vectors.subtract(capsule.getCentroid().getCoordinate().getValues(),sphereClosest)).localNormalize();
            }else{
                normal = new Vector(Vectors.subtract(capsuleClosest,sphereClosest)).localNormalize();
            }

            if(!normal.isFinite()){
                normal.setXYZ(0, 1, 0);
            }

            collision.setNormal(normal);
            collision.setCollide(distance<0);
            collision.setDistance(distance);
            
            return collision;
        }
    };
    
    public static final CollisionExecutor SPHERE_GRID =
            new CollisionExecutor(Sphere.class, Grid.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;            
            final Sphere a = (Sphere)first;            
            final Grid b = (Grid)second;
            
            final double radius = a.getRadius();
            final TupleBuffer sm = b.getImage().getRawModel().asTupleBuffer(b.getImage());
            final double[] coord = GridOperations.toGridCoord(b, a.getCenter());
            //check if point is outside grid
            for(int i=0;i<coord.length;i++){
                if(coord[i]+radius<0 || coord[i]-radius>=sm.getExtent().get(i)) return false;
            }
            
            //check all intersecting blocks
            final double[] lower = new double[coord.length];
            final double[] upper = new double[coord.length];
            for(int i=0;i<coord.length;i++){
                lower[i] = (int)Math.floor(coord[i]-radius);
                upper[i] = (int)Math.ceil(coord[i]+radius);
                //check we don't go out of image
                lower[i] = Maths.max(lower[i], 0);
                upper[i] = Maths.min(upper[i], sm.getExtent().get(i));
            }
            
            final TupleIterator ite = sm.createIterator(new BBox(lower, upper));
            final BBox voxel = new BBox(lower.length);
            boolean[] sample = new boolean[1];
            while(ite.nextAsBoolean(sample)!=null){
                if(sample[0]){
                    //check collision with sphere
                    final int[] pixelCoord = ite.getCoordinate();
                    double[] vlow = new double[pixelCoord.length];
                    double[] vup = new double[pixelCoord.length];
                    for(int i=0;i<pixelCoord.length;i++){
                        vlow[i] = pixelCoord[i];
                        vup[i] = pixelCoord[i]+1;
                    }
                    //convert coordinate in world space
                    vlow = b.getGridToGeom().transform(vlow, vlow);
                    vup = b.getGridToGeom().transform(vup, vup);
                    voxel.getLower().set(vlow);
                    voxel.getUpper().set(vup);
                    
                    final RigidBody rb1 = new RigidBody(a, firstRB.getMass());
                    final RigidBody rb2 = new RigidBody(voxel, secondRB.getMass());
                    final Collision col = new Collision(rb1, rb2);
                    Operations.execute(col);
                    if(col.isCollide()){
                        collision.setCollide(true);
                        collision.setDistance(col.getDistance());
                        collision.setNormal(col.getNormal());
                        return collision;
                    }
                }
            }
            
            collision.setCollide(false);
            return collision;
            
        }
    };
    
    public static final CollisionExecutor CAPSULE_CAPSULE =
            new CollisionExecutor(Capsule.class, Capsule.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final Capsule capsule1 = (Capsule)first;
            final Capsule capsule2 = (Capsule)second;
            
            final Geometry[] nearest = (Geometry[])Operations.execute(new Nearest(
                    new Segment(capsule1.getFirst(),capsule1.getSecond()), 
                    new Segment(capsule2.getFirst(),capsule2.getSecond())
                    ));
            
            final Vector direction = new Vector(nearest[0].getCentroid().getCoordinate());
            direction.localSubtract(nearest[1].getCentroid().getCoordinate());
            final double distance = direction.length();
            final double sumRadius = capsule1.getRadius() + capsule2.getRadius();   
            final boolean collide = distance <= sumRadius;
            collision.setCollide(collide);
            collision.setDistance(distance-sumRadius);
            if(distance==0){
                //geometries overlap exactly
                collision.setNormal(new Vector(0, 1, 0));
            }else{
                collision.setNormal(direction.negate().localNormalize());
            }


            if(!collision.getNormal().isFinite()){
                System.out.println("la");
            }
            
            return collision;
        }
    };
    
    public static final CollisionExecutor PLANE_PLANE =
            new CollisionExecutor(Plane.class, Plane.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) {
            final Collision collision = (Collision) op;
            //planes are infinite, unless they are stictly parallel they always collide.
            //planes are part of a scene, but we ignore collision between them
            collision.setCollide(false);
            return collision;
        }
    };

    public static final CollisionExecutor PLANE_CAPSULE =
            new CollisionExecutor(Plane.class, Capsule.class, true){

        protected Object executeInternal(Collision op, RigidBody firstRB,
                Object first, RigidBody secondRB, Object second) throws OperationException {
            final Collision collision = (Collision) op;
            final Plane plane = (Plane)first;
            final Capsule capsule = (Capsule)second;
            final double radius = capsule.getRadius();       
            
            final Geometry[] nearest = plane.nearest(new Segment(capsule.getFirst(),capsule.getSecond()));
            final Vector nearestCapsule = new Vector(nearest[0].getCentroid().getCoordinate());
            final Vector nearestPlane = new Vector(nearest[1].getCentroid().getCoordinate());
            final Vector direction = nearestCapsule.subtract(nearestPlane);
            final double distance = direction.length();
            final boolean collide = distance <= radius;
            
            collision.setCollide(collide);
            if(!collide) return collision;
            
            //calculate collision points
            final Vector impactCapsule = nearestCapsule.add(direction.scale(distance));
            final Vector impactPlane = nearestPlane;
            collision.setImpactGeometries(new Geometry[]{
                new Point(impactPlane),
                new Point(impactCapsule)
            });
            
            //calculate the push back direction
            if(distance>0){
                //center of the sphere is outside the bbox
                collision.setNormal(direction.negate().localNormalize());
                collision.setDistance(-(radius-distance));
            }else{
                //center of the sphere is inside the bbox
                //find the closest border
                //TODO
//                final Vector bboxCenter = new Vector(bbox.getCentroid().getCoordinate());
//                bboxCenter.localSubtract(sphere.getCenter());
//                final double[] diff = bboxCenter.getValues();
//                final int axis;
//                if(diff[0]<diff[1] && diff[0]<diff[2]) axis=0;
//                else if(diff[1]<diff[0] && diff[1]<diff[2]) axis=1;
//                else axis = 2;
//                
//                collision.setDistance(sphere.getRadius());
                
                //push it back up for now
                collision.setNormal(new Vector(0, 1, 0));
                collision.setDistance(radius);
                
            }
            
            return collision;
        }
    };


    
}
