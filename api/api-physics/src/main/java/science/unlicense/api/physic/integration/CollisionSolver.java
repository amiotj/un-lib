
package science.unlicense.api.physic.integration;

import science.unlicense.api.physic.World;

/**
 * Solver in charge of updating a physique world.
 * @author Johann Sorel
 */
public interface CollisionSolver {

    /**
     * Update the given world.
     * @param world
     * @param timeellapsed
     */
    void update(World world, double timeellapsed);

}
