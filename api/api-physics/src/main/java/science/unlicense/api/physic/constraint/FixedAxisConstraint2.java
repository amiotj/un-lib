
package science.unlicense.api.physic.constraint;

import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 * Joint constraints.
 * Allow joint rotation only around given axis.
 *
 * @author Johann Sorel
 */
public class FixedAxisConstraint2 implements Constraint{

    private final SceneNode target;
    private final Vector axis;
    private final Vector up;
    private final Vector right;

    public FixedAxisConstraint2(SceneNode target, Vector axis, Vector up, Vector right) {
        this.target = target;
        this.axis = axis;
        this.up = up;
        this.right = right;
    }

    public void apply() {

        final MatrixRW nodeRotation = target.getNodeTransform().getRotation();

        final Vector forward = right.cross(up);
        
        //calculate rotation from axis to forward
        // we will make the calculation in this space
        final MatrixRW axisToForward = Matrix3x3.createRotation(axis, forward);
        
        //calculate reference direction
        final Vector ref = (Vector) nodeRotation.transform(forward);
        axisToForward.transform(ref, ref);
        
        //calculate angle on the 2 other planes
        double horizontalAngle = forward.shortestAngle(new Vector(ref.getX(),0,ref.getZ()).localNormalize());
        if(ref.getX()<0) horizontalAngle = -horizontalAngle;  

        double verticalAngle = ref.shortestAngle(new Vector(ref.getX(),0,ref.getZ()).localNormalize());
        if(ref.getY()<0) verticalAngle = -verticalAngle;

        //rebuild matrix
        final MatrixRW temp = new Matrix3x3().setToIdentity();
        if(!Double.isNaN(horizontalAngle)){
            final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, up);
            temp.localMultiply(mh);
        }
        if(!Double.isNaN(verticalAngle)){
            final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, right);
            temp.localMultiply(mv);
        }
        
        //invert axis transform
        temp.localMultiply(axisToForward.localInvert());
        
        //set transform
        nodeRotation.set(temp);
        target.getNodeTransform().notifyChanged();

    }
}