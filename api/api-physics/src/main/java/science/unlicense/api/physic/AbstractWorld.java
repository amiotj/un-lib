package science.unlicense.api.physic;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.physic.integration.EulerIntegrator;
import science.unlicense.api.physic.integration.Integrator;

/**
 * Abstract world.
 *
 * @author Johann Sorel
 */
public abstract class AbstractWorld extends AbstractEventSource implements World{

    protected static final float DEFAULT_DRAG = 0.001f;
    
    protected final Sequence groups = new ArraySequence();

    protected final CollisionsState colState = new CollisionsState(this);
    protected double integrationTimeStep = 0.016; //16ms
    protected Integrator integrator;
    protected double drag = DEFAULT_DRAG;

    /**
     * Time step remaining from previous  update
     */
    private double previousRemaining = 0.0;

    public AbstractWorld(int dimension) {
        integrator = new EulerIntegrator(dimension);
    }
    
    public double getDrag() {
        return drag;
    }

    public void setDrag(double drag) {
        this.drag = drag;
    }

    public double getIntegrationTimeStep() {
        return integrationTimeStep;
    }

    public void setIntegrationTimeStep(double timestep) {
        integrationTimeStep = timestep;
    }

    public Integrator getIntegrator() {
        return integrator;
    }

    public void setIntegrator(Integrator integrator) {
        this.integrator = integrator;
    }

    public CollisionsState getCollisionsState() {
        return colState;
    }

    public Sequence getGroups() {
        return groups;
    }

    public void update(double timeellapsed){
        //add previous remaining time
        timeellapsed += previousRemaining;
        //calculate number of iterators
        final int nbIte = (int)Math.floor(timeellapsed/integrationTimeStep);
        previousRemaining = timeellapsed - (nbIte*integrationTimeStep);

        //apply integration
        if(integrator!=null){
            for(int i=0;i<nbIte;i++){
                integrator.update(this, integrationTimeStep);
            }
        }
        
        //notify listener world has been updated
        sendPropertyEvent(this, PROPERTY_UPDATE, false, true);
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

}