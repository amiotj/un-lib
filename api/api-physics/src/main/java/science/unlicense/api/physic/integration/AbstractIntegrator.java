
package science.unlicense.api.physic.integration;


/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractIntegrator implements Integrator{
        
    protected CollisionSolver collisionSolver = new DefaultCollisionSolver();
    
    public CollisionSolver getCollisionSolver() {
        return collisionSolver;
    }

    public void setCollisionSolver(CollisionSolver solver) {
        this.collisionSolver = solver;
    }
    
}
