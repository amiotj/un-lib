
package science.unlicense.api.physic;

import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.api.event.MessagePredicate;
import science.unlicense.api.physic.operation.Collision;
import science.unlicense.api.predicate.Predicate;

/**
 * Collision event send when 2 body collide or separate.
 * 
 * @author Johann Sorel
 */
public class CollisionMessage extends DefaultEventMessage{

    public static final Predicate PREDICATE = new MessagePredicate(CollisionMessage.class);

    public static final int TYPE_COLLIDE = 0;
    public static final int TYPE_SEPARATE = 1;
    
    private final Collision collision;
    private final int type;
    
    public CollisionMessage(Collision collision, int type) {
        super(true);
        this.collision = collision;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public Collision getCollision() {
        return collision;
    }

}

