
package science.unlicense.api.physic.integration;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.WorldUtilities;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.impl.math.Vector;

/**
 *
 * Rotation/torque from : http://www.enchantedage.com/node/68
 * Thanks to Jon Watte for the public domain resources.
 * 
 * 
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class EulerIntegrator extends AbstractIntegrator {

    //linear buffers
    private final Vector translation;
    private final Vector acceleration;
    //angular buffers
    private final Vector rotation;
    private final Vector scaledTorque;
            
    public EulerIntegrator(int worldDimension) {
        this.translation = new Vector(worldDimension);
        this.acceleration = new Vector(worldDimension);
        this.rotation = new Vector(worldDimension);
        this.scaledTorque = new Vector(worldDimension);
    }

    public void update(final World world, final double timeellapsed){
        WorldUtilities.clearBodyForces(world);
        WorldUtilities.applyForces(world);
                
        if(collisionSolver!=null){
            collisionSolver.update(world, timeellapsed);
        }
        
        final Sequence bodies = world.getBodies();
        for(int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body)bodies.get(i);
                        
            if(body instanceof RigidBody){
                final RigidBody rb = (RigidBody) body;
                if(rb.isFixed()) continue;
                
                final double mass = rb.getMass();
                
                //linear integration
                final Vector linearVelocity = rb.getMotion().getVelocity();
                final Vector force = rb.getMotion().getForce();
                
                if(mass<=0){
                    //negative check just in case
                    acceleration.setToZero();
                }else{
                    force.scale(1.0/mass, acceleration);
                }
                
                acceleration.localScale(timeellapsed);
                linearVelocity.localAdd(acceleration);
                linearVelocity.scale(timeellapsed,translation);
                rb.worldTranslate(translation);
                
                //angular integration
                final Vector angularVelocity = rb.getMotion().getAngularVelocity();
                final Vector torque = rb.getMotion().getTorque();
                
                if(mass<=0){
                    //negative check just in case
                    scaledTorque.setToZero();
                }else{
                    torque.scale(1.0/mass, scaledTorque);
                }
                scaledTorque.localScale(timeellapsed);      
                angularVelocity.localAdd(scaledTorque);
                angularVelocity.scale(timeellapsed,rotation);
                rb.worldRotate(rotation);
                
            }
        }
    }

}
