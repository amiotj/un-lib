
package science.unlicense.api.physic.constraint;

import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 * Joint constraints.
 * Allow joint rotation only around given axis.
 *
 * @author Johann Sorel
 */
public class FixedAxisConstraint implements Constraint{

    private final SceneNode target;
    private final Vector axis;

    public FixedAxisConstraint(SceneNode target, Vector axis) {
        this.target = target;
        this.axis = axis;
    }

    public void apply() {

        final MatrixRW rotation = target.getNodeTransform().getRotation();

        //transform the contraints axis on X
        final Matrix3x3 temp = Matrix3x3.createRotation(axis,new Vector(1, 0, 0));
        temp.multiply(rotation, rotation);

        //convert the rotation in Euler angles
        //this way can simply revome the 2 axis rotations we don't want
        final double[] euler = Matrices.toEuler(rotation.getValuesCopy(), null);
        //rmore the rotation which are not allowed
        euler[0] = 0;
        euler[1] = 0;

        //change our euler angle back in the rotation
        rotation.set(Matrices.fromEuler(euler, rotation.getValuesCopy()));

        //transform the contraints back on the fixed axis
        temp.localInvert();
        temp.multiply(rotation, rotation);
        target.getNodeTransform().notifyChanged();

    }

}
