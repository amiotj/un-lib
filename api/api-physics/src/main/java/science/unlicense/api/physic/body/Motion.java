
package science.unlicense.api.physic.body;

import science.unlicense.impl.math.Vector;

/**
 * Current motion state of a rigid body.
 * 
 * @author Johann Sorel
 */
public class Motion {
    
    protected final Vector velocity;
    protected final Vector force;
    protected final Vector angularVelocity;
    protected final Vector torque;

    public Motion(int dim) {
        this.velocity = new Vector(dim);
        this.force = new Vector(dim);
        this.angularVelocity = new Vector(dim);
        this.torque = new Vector(dim);
    }

    /**
     * Get body velocity.
     * @return Vector velocity in unit/second
     */
    public Vector getVelocity() {
        return velocity;
    }

    /**
     * Get body currently applied forces.
     * @return Vector
     */
    public Vector getForce() {
        return force;
    }

    /**
     * Rotation angle x rotation speed.
     * @return Vector
     */
    public Vector getAngularVelocity() {
        return angularVelocity;
    }

    /**
     * Similar to Force but for body rotation.
     * @return Vector
     */
    public Vector getTorque() {
        return torque;
    }
    
}
