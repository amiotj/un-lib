
package science.unlicense.api.physic.operation;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.OrientedGeometry;
import science.unlicense.impl.geometry.Point;
import science.unlicense.api.geometry.operation.Operation;
import science.unlicense.api.geometry.operation.OperationExecutor;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Grid;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.DefaultAffine;
import science.unlicense.impl.math.Vector;

/**
 * Transform executors.
 *
 * @author Johann Sorel
 */
public class TransformExecutors {

    private TransformExecutors(){}

    private static abstract class TransformExecutor implements OperationExecutor{

        private final Class clazz;

        public TransformExecutor(Class geometryClass) {
            this.clazz = geometryClass;
        }

        public Class getOperationClass() {
            return Transform.class;
        }
        
        public Geometry execute(Operation operation) {
            final Transform transform = (Transform) operation;
            final science.unlicense.api.math.transform.Transform matrix = transform.getTransform();
            final Geometry geom = transform.getGeometry();
            return execute(geom, matrix);
        }

        protected abstract Geometry execute(Geometry geom, science.unlicense.api.math.transform.Transform transform);

        public boolean canHandle(Operation operation) {
            return operation instanceof Transform &&
                    clazz.isInstance( ((Transform)operation).getGeometry());
        }

    }

    public static final OperationExecutor POINT =
            new TransformExecutor(Point.class){

        protected Point execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Point candidate = (Point) geom;
            return new Point(transform.transform(candidate.getCoordinate(),null));
        }
    };
    
    public static final OperationExecutor RECTANGLE =
            new TransformExecutor(Rectangle.class){

        protected Rectangle execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Rectangle candidate = (Rectangle) geom;
            double[] ul = new double[]{
                candidate.getX(),
                candidate.getY()
                };
            ul = transform.transform(ul, ul);
            return new Rectangle(ul[0], ul[1], candidate.getWidth(), candidate.getHeight());
        }
    };

    public static final OperationExecutor CIRCLE =
            new TransformExecutor(Circle.class){

        protected Circle execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Circle candidate = (Circle) geom;
            return new Circle(
                    transform.transform(candidate.getCenter(),null),
                    candidate.getRadius());
        }
    };

    public static final OperationExecutor BBOX =
            new TransformExecutor(BBox.class){

        protected BBox execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final BBox bbox = (BBox) geom;
            return new BBox(
                transform.transform(bbox.getLower(),null),
                transform.transform(bbox.getUpper(),null)
                );
        }
    };

    public static final OperationExecutor SPHERE =
            new TransformExecutor(Sphere.class){

        protected Sphere execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Sphere sphere = (Sphere) geom;
            final Sphere copy = new Sphere();
            copy.getCenter().set(sphere.getCenter());
            copy.setRadius(sphere.getRadius());
            
            transform.transform(copy.getCenter(),copy.getCenter());
            //make a transform on x axis, TODO this is not right, just a quick simplification
            //real result should be an ellipsoid
            final Vector v = new Vector(copy.getRadius(), 0, 0,0);
            ((Affine)transform).toMatrix().transform(v, v);
            copy.setRadius(v.length());
            
            return copy;
        }
    };
    
    public static final OperationExecutor CAPSULE =
            new TransformExecutor(Capsule.class){

        protected Capsule execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Capsule capsule = (Capsule) geom;
            final Capsule copy = new Capsule(capsule.getHeight(),capsule.getRadius());
            
            final Affine m = capsule.getTransform();
            copy.getTransform().set(m.multiply((Affine) transform));
            
            //make a transform on x axis, TODO this is not right, just a quick simplification
            final Vector v = new Vector(copy.getRadius(), 0, 0);
            transform.transform(v, v);
            copy.setRadius(v.length());
            
            return copy;
        }
    };
    
    public static final OperationExecutor PLANE =
            new TransformExecutor(Plane.class){

        protected Plane execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Plane plane = (Plane) geom;
            final Vector cpNormal = new Vector(plane.getNormal().getSize()+1);
            cpNormal.set(plane.getNormal());
            final Vector cpPoint = plane.getPoint().copy();
            transform.transform(cpPoint,cpPoint);
            ((Affine)transform).toMatrix().transform(cpNormal,cpNormal);
            return new Plane(cpNormal.getXYZ(),cpPoint);
        }
    };
    
    public static final OperationExecutor GRID =
            new TransformExecutor(Grid.class){

        protected Grid execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final Grid grid = (Grid) geom;
            final AffineRW trs = DefaultAffine.create(grid.getGeomToGrid());
            trs.localMultiply((Affine)transform);
            return new Grid(grid.getImage(), (Affine3)trs);
        }
    };

    public static final OperationExecutor ORIENTED =
            new TransformExecutor(OrientedGeometry.class){

        protected Capsule execute(Geometry geom, science.unlicense.api.math.transform.Transform transform) {
            final OrientedGeometry oriented = (OrientedGeometry) geom;
            //TODO
            throw new RuntimeException("TODO");
        }
    };

}
