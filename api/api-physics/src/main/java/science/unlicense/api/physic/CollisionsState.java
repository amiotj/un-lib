
package science.unlicense.api.physic;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.operation.Collision;

/**
 * Contains the world list of collisions.
 * This class can generate events on start and end collisions.
 * 
 * @author Johann Sorel
 */
public class CollisionsState extends AbstractEventSource{

    private final World world;
    private final Sequence oldState = new ArraySequence();
    private final Sequence newState = new ArraySequence();
    private final Sequence viewState = Collections.readOnlySequence(newState);

    private final Sequence newCol = new ArraySequence();
    
    public CollisionsState(World world) {
        this.world = world;
    }

    public Sequence getCollisions() {
        return viewState;
    }
    
    public void update(){
        oldState.replaceAll(newState);
        newState.removeAll();
        newCol.removeAll();
        
        final Sequence bodies = world.getBodies();

        //test collisions
        Collision collision = new Collision(null, null);
        for(int i=0,n=bodies.getSize();i<n;i++){
            final Body bodyA = (Body) bodies.get(i);
            for(int k=i+1;k<n;k++){
                final Body bodyB = (Body) bodies.get(k);
                if(!bodyA.canGroupCollide(bodyB)){
                    //no collision
                    continue;
                }else{
                    //possible collision
                    collision.reset(bodyA, bodyB);
                    try {
                        Operations.execute(collision);
                    } catch (OperationException ex) {
                        throw new RuntimeException(ex.getMessage(),ex);
                    }
                    if(!collision.isCollide()){
                        //no collision
                        continue;
                    }
                    
                    //collision found
                    newState.add(collision);
                    
                    if(hasListeners() && !contains(oldState,collision)){
                        //new collision
                        newCol.add(collision);
                    }
                    collision = new Collision(null, null);
                    
                }
            }
        }
        
        if(hasListeners()){
            final Sequence obs = findObsolete();
            
            final EventManager manager = getEventManager();
            //send separate events
            for(int i=0;i<obs.getSize();i++){
                manager.sendEvent(new Event(this, new CollisionMessage((Collision) obs.get(i), CollisionMessage.TYPE_SEPARATE)));
            }
            //send collide events
            for(int i=0;i<newCol.getSize();i++){
                manager.sendEvent(new Event(this, new CollisionMessage((Collision) newCol.get(i), CollisionMessage.TYPE_COLLIDE)));
            }
        }
        
    }
    
    private Sequence findObsolete(){
        //TODO we could reduce time by knowing which bodies events are listened
        final Sequence ncp = new ArraySequence();
        
        //search obsolete collisions
        for(int o=0,on=oldState.getSize();o<on;o++){
            final Object oldCol = oldState.get(o);
            if(!contains(newState, (Collision) oldCol)){
                ncp.add(oldCol);
            }
        }
        return ncp;
    }
    
    private static boolean contains(Sequence s, Collision c){
        for(int o=0,on=s.getSize();o<on;o++){
            final Collision t = (Collision) s.get(o);
            if( (c.getFirst()==t.getFirst() && c.getSecond()==t.getSecond()) ||
                (c.getFirst()==t.getSecond() && c.getSecond()==t.getFirst())){
                return true;
            }
        }
        return false;
    }
    
}
