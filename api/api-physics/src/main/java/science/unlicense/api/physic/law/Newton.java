
package science.unlicense.api.physic.law;

import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class Newton {
    
    /**
     * Calculate velocity.
     * acceleration = distance / time.
     * @param force
     * @param mass
     * @return 
     */
    public static Vector velocity(Vector distance, double timeellapsed){
        return distance.scale(1/timeellapsed);
    }
    
}
