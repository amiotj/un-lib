
package science.unlicense.api.physic.force;

/**
 * Abstract Force.
 *
 * @author Johann Sorel
 */
public abstract class AbstractForce implements Force{

    protected boolean constraint = false;
    protected boolean enable = true;

    public AbstractForce() {
    }

    public AbstractForce(boolean isConstraint) {
        constraint = isConstraint;
    }
    
    public boolean isConstraint() {
        return constraint;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

}
