
package science.unlicense.api.physic.body;

import science.unlicense.impl.geometry.Point;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class Particle extends RigidBody {

    private double age = 0;

    public Particle(int dimension, double m) {
        super(new Point(dimension),m);
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

}
