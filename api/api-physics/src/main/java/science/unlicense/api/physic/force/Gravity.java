
package science.unlicense.api.physic.force;

import science.unlicense.api.physic.body.Body;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class Gravity implements Singularity{

    private final Vector gravity;

    public Gravity(Vector gravity) {
        this.gravity = gravity;
    }

    public Gravity(double g) {
        gravity = new Vector(0, g, 0);
    }

    public void apply(Body body) {
        body.getMotion().getForce().localAdd(gravity);
    }

}