
package science.unlicense.api.physic.skeleton;

/**
 * An ik solver tries to solve an inverse kinematic chain.
 * There are multiple solutions to achieve or get close to the solution.
 * Some might be cpu consuming while other might achieve faster but less accurate 
 * result yet sufficiant for 3d animation.
 * 
 * @author Johann Sorel
 */
public interface IKSolver {
    
    void solve(InverseKinematic ik);
    
}
