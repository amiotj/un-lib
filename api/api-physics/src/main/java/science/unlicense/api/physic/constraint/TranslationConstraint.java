

package science.unlicense.api.physic.constraint;

import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * Constraints a node in a given bounding box.
 *
 * @author Johann Sorel
 */
public class TranslationConstraint implements Constraint {

    private final SceneNode target;
    private final BBox limits;
    private final boolean relative;

    /**
     * 
     * @param target node to constraint
     * @param limits bounding box use to restrict the node translation
     * @param relativeToParent :
     *       true if bbox is in node space
     *       false if bbox is in world space                   
     */
    public TranslationConstraint(SceneNode target, BBox limits, boolean relativeToParent) {
        this.target = target;
        this.limits = limits;
        this.relative = relativeToParent;
    }

    /**
     * Node on which this constraint is applied.
     * @return SceneNode
     */
    public SceneNode getTarget() {
        return target;
    }

    /**
     * Bounding box use to restrict the node translation.
     * @return BBox
     */
    public BBox getLimits() {
        return limits;
    }

    /**
     * Indicate how the bbox limit must be interpreted.
     * @return true if bbox is in node space
     *          false if bbox is in world space    
     */
    public boolean isRelativeToParent() {
        return relative;
    }

    public void apply() {
        //TODO do we consider rotation and scale values for the limit ? or just the translation ?
        Tuple lower = limits.getLower();
        Tuple upper = limits.getUpper();
        if(!relative){
            final Affine wtn = target.getRootToNodeSpace();
            lower = wtn.transform(lower,null);
            upper = wtn.transform(upper,null);
        }

        final Vector translation = target.getNodeTransform().getTranslation();
        for(int i=0,n=limits.getDimension();i<n;i++){
            translation.set(i, Maths.clamp(translation.get(i), lower.get(i), upper.get(i)));
        }
        target.getNodeTransform().notifyChanged();
    }

}
