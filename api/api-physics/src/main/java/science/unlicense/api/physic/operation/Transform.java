
package science.unlicense.api.physic.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.operation.AbstractOperation;

/**
 * Geometry transform operation.
 * 
 * @author Johann Sorel
 */
public class Transform extends AbstractOperation {

    public static final Chars NAME = new Chars(new byte[]{'T','R','A','N','S','F','O','R','M'});

    private final Geometry geometry;
    private final science.unlicense.api.math.transform.Transform trs;

    public Transform(Geometry geometry, science.unlicense.api.math.transform.Transform trs) {
        this.geometry = geometry;
        this.trs = trs;
    }
    
    public Chars getName() {
        return NAME;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public science.unlicense.api.math.transform.Transform getTransform() {
        return trs;
    }

}