package science.unlicense.api.physic.integration;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.WorldUtilities;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class ModifiedEulerIntegrator extends AbstractIntegrator{

    private final Vector translation;
    private final Vector scaledForce;
    private final Vector scaledForce2;
            
    public ModifiedEulerIntegrator(int worldDimension) {
        this.translation = new Vector(worldDimension);
        this.scaledForce = new Vector(worldDimension);
        this.scaledForce2 = new Vector(worldDimension);
    }
    
    public void update(final World world, final double timeellapsed){
        WorldUtilities.clearBodyForces(world);
        WorldUtilities.applyForces(world);

        if(collisionSolver!=null){
            collisionSolver.update(world, timeellapsed);
        }
        
        final double halftt = 0.5f*timeellapsed*timeellapsed;

        final Sequence bodies = world.getBodies();
        for(int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body)bodies.get(i);
            
            if(body instanceof RigidBody){
                if(!((RigidBody)body).isFree()) continue;
            }
            
            if(body instanceof RigidBody){
                final RigidBody rb = (RigidBody) body;
               
                final Vector force = rb.getMotion().getForce();
                final Vector velocity = rb.getMotion().getVelocity();
                final double mass = rb.getMass();
                
                if(mass<=0){
                    //negative check just in case
                    scaledForce.setToZero();
                }else{
                    force.scale(1.0/mass, scaledForce);
                }

                velocity.scale(timeellapsed, translation);
                scaledForce.scale(halftt,scaledForce2);
                translation.localAdd(scaledForce2);
                scaledForce.scale(timeellapsed,scaledForce2);
                velocity.localAdd(scaledForce2);

                rb.worldTranslate(translation);
            }
        }
    }

}
