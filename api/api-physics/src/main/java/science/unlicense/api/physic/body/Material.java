
package science.unlicense.api.physic.body;

/**
 * Material attributes.
 * Physical properties used by the physics engine.
 * 
 * @author Johann Sorel
 */
public class Material {
    
    protected double density = 1;
    protected double restitution = 1;
    protected double linearAttenuation = 0;
    protected double angularAttenuation = 0;
    protected double staticFriction = 0;
    protected double dynamicFriction = 0;

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    /**
     * Get material restitution.
     * @return restitution
     */
    public double getRestitution() {
        return restitution;
    }

    /**
     * Set body restitution.
     * @param restitution 
     */
    public void setRestitution(double restitution) {
        this.restitution = restitution;
    }

    public double getLinearAttenuation() {
        return linearAttenuation;
    }

    public void setLinearAttenuation(double linearAttenuation) {
        this.linearAttenuation = linearAttenuation;
    }

    public double getAngularAttenuation() {
        return angularAttenuation;
    }

    public void setAngularAttenuation(double angularAttenuation) {
        this.angularAttenuation = angularAttenuation;
    }

    public double getStaticFriction() {
        return staticFriction;
    }

    public void setStaticFriction(double staticFriction) {
        this.staticFriction = staticFriction;
    }

    public double getDynamicFriction() {
        return dynamicFriction;
    }

    public void setDynamicFriction(double dynamicFriction) {
        this.dynamicFriction = dynamicFriction;
    }
    
}
