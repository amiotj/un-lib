package science.unlicense.api.physic.integration;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.WorldUtilities;
import science.unlicense.api.physic.body.Particle;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class RungeKuttaIntegrator extends AbstractIntegrator{

    private final Sequence originalPositions   = new ArraySequence();
    private final Sequence originalVelocities  = new ArraySequence();
    private final Sequence k1Forces            = new ArraySequence();
    private final Sequence k1Velocities        = new ArraySequence();
    private final Sequence k2Forces            = new ArraySequence();
    private final Sequence k2Velocities        = new ArraySequence();
    private final Sequence k3Forces            = new ArraySequence();
    private final Sequence k3Velocities        = new ArraySequence();
    private final Sequence k4Forces            = new ArraySequence();
    private final Sequence k4Velocities        = new ArraySequence();

    private int worldDimension;
    
    public RungeKuttaIntegrator(int worldDimension) {
        this.worldDimension = worldDimension;
    }

    private void allocateParticles(final World world) {
        final Sequence bodies = world.getBodies();
        while(bodies.getSize()> originalPositions.getSize()){
            originalPositions.add(new Vector(worldDimension));
            originalVelocities.add(new Vector(worldDimension));
            k1Forces.add(new Vector(worldDimension));
            k1Velocities.add(new Vector(worldDimension));
            k2Forces.add(new Vector(worldDimension));
            k2Velocities.add(new Vector(worldDimension));
            k3Forces.add(new Vector(worldDimension));
            k3Velocities.add(new Vector(worldDimension));
            k4Forces.add(new Vector(worldDimension));
            k4Velocities.add(new Vector(worldDimension));
        }
    }

    public void update(final World world, final double timeellapsed){
        allocateParticles(world);
        final Sequence bodies = world.getBodies();

        /////////////////////////////////////////////////////////
        // save original position and velocities

        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody)bodies.get(i);
            if(rb.isFree()){
                ((Vector)originalPositions.get(i)).set(rb.getWorldPosition(null));
                ((Vector)originalVelocities.get(i)).set(rb.getMotion().getVelocity());
            }
            rb.getMotion().getForce().setToZero();// and clear the forces
        }

        ////////////////////////////////////////////////////////
        // get all the k1 values

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody)bodies.get(i);
            if (p.isFree()){
                ((Vector)k1Forces.get(i)).set(p.getMotion().getForce());
                ((Vector)k1Velocities.get(i)).set(p.getMotion().getVelocity());
            }
            p.getMotion().getForce().setToZero();
        }

        ////////////////////////////////////////////////////////////////
        // get k2 values

        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody)bodies.get(i);

            if(rb.isFree()){
                final Vector originalPosition = (Vector)originalPositions.get(i);
                final Vector k1Velocity = (Vector)k1Velocities.get(i);

                final Vector translation = new Vector(k1Velocity);
                translation.localScale(0.5 * timeellapsed);
                translation.localAdd(originalPosition);

                final Vector originalVelocity = (Vector)originalVelocities.get(i);
                final Vector k1Force = (Vector)k1Forces.get(i);

                rb.getMotion().getVelocity().set(k1Force);
                rb.getMotion().getVelocity().localScale(0.5 * timeellapsed / rb.getMass());
                rb.getMotion().getVelocity().localAdd(originalVelocity);                
                
                rb.setWorldPosition(translation);
            }
        }

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody)bodies.get(i);
            if(p.isFree()){
                ((Vector)k2Forces.get(i)).set(p.getMotion().getForce());
                ((Vector)k2Velocities.get(i)).set(p.getMotion().getVelocity());
            }

            p.getMotion().getForce().setToZero(); // and clear the forces now that we are done with them
        }


        /////////////////////////////////////////////////////
        // get k3 values

        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody)bodies.get(i);
            if(rb.isFree()){
                final Vector originalPosition = (Vector)originalPositions.get(i);
                final Vector k2Velocity = (Vector)k2Velocities.get(i);

                final Vector translation = new Vector(k2Velocity);
                translation.localScale(0.5 * timeellapsed);
                translation.localAdd(originalPosition);
                
                final Vector originalVelocity = (Vector)originalVelocities.get(i);
                final Vector k2Force = (Vector)k2Forces.get(i);

                rb.getMotion().getVelocity().set(k2Force);
                rb.getMotion().getVelocity().localScale(0.5 * timeellapsed / rb.getMass());
                rb.getMotion().getVelocity().localAdd(originalVelocity);
                
                rb.setWorldPosition(translation);
            }
        }

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody)bodies.get(i);
            if(p.isFree()){
                ((Vector)k3Forces.get(i)).set(p.getMotion().getForce());
                ((Vector)k3Velocities.get(i)).set(p.getMotion().getVelocity());
            }

            p.getMotion().getForce().setToZero(); // and clear the forces now that we are done with them
        }


        //////////////////////////////////////////////////
        // get k4 values

        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody)bodies.get(i);
            if(rb.isFree()){
                final Vector originalPosition = (Vector)originalPositions.get(i);
                final Vector k3Velocity = (Vector)k3Velocities.get(i);

                final Vector translation = new Vector(k3Velocity);
                translation.localScale(timeellapsed);
                translation.localAdd(originalPosition);
                
                final Vector originalVelocity = (Vector)originalVelocities.get(i);
                final Vector k3Force = (Vector)k3Forces.get(i);

                rb.getMotion().getVelocity().set(k3Force);
                rb.getMotion().getVelocity().localScale(timeellapsed / rb.getMass());
                rb.getMotion().getVelocity().localAdd(originalVelocity);
                
                rb.setWorldPosition(translation);
            }
        }

        WorldUtilities.applyForces(world);

        // save the intermediate forces
        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody p = (RigidBody)bodies.get(i);
            if(p.isFree()){
                ((Vector)k4Forces.get(i)).set(p.getMotion().getForce());
                ((Vector)k4Velocities.get(i)).set(p.getMotion().getVelocity());
            }
        }

        /////////////////////////////////////////////////////////////
        // put them all together and what do you get?

        for(int i=0; i<bodies.getSize(); ++i){
            final RigidBody rb = (RigidBody)bodies.get(i);
            if(rb instanceof Particle){
                ((Particle)rb).setAge(((Particle)rb).getAge() + timeellapsed);
            }
            if(rb.isFree()){
                
                // update position
                final Vector originalPosition = (Vector)originalPositions.get(i);
                final Vector k1Velocity = (Vector)k1Velocities.get(i);
                final Vector k2Velocity = (Vector)k2Velocities.get(i);
                final Vector k3Velocity = (Vector)k3Velocities.get(i);
                final Vector k4Velocity = (Vector)k4Velocities.get(i);

                final Vector translation = new Vector(worldDimension);
                k2Velocity.localScale(2.0);
                k3Velocity.localScale(2.0);
                translation.localAdd(k1Velocity);
                translation.localAdd(k2Velocity);
                translation.localAdd(k3Velocity);
                translation.localAdd(k4Velocity);
                translation.localScale(timeellapsed / 6.0);
                translation.localAdd(originalPosition);
                                
                // update velocity
                final Vector originalVelocity = (Vector)originalVelocities.get(i);
                final Vector k1Force = (Vector)k1Forces.get(i);
                final Vector k2Force = (Vector)k2Forces.get(i);
                final Vector k3Force = (Vector)k3Forces.get(i);
                final Vector k4Force = (Vector)k4Forces.get(i);

                rb.getMotion().getVelocity().setToZero();
                k2Force.localScale(2.0);
                k3Force.localScale(2.0);
                rb.getMotion().getVelocity().localAdd(k1Force);
                rb.getMotion().getVelocity().localAdd(k2Force);
                rb.getMotion().getVelocity().localAdd(k3Force);
                rb.getMotion().getVelocity().localAdd(k4Force);
                rb.getMotion().getVelocity().localScale(timeellapsed / (6.0f*rb.getMass()));
                rb.getMotion().getVelocity().localAdd(originalVelocity);
                
                rb.setWorldPosition(translation);
            }
        }
    }
}
