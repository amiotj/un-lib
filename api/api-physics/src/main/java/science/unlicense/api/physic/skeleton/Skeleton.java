
package science.unlicense.api.physic.skeleton;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeSequence;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.predicate.ClassPredicate;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.scenegraph.SceneUtils;

/**
 * Skeleton composed of joints.
 *
 * @author Johann Sorel
 */
public class Skeleton extends DefaultSceneNode{

    private static final NodeVisitor BINDPOSE_SOLVER = new DefaultNodeVisitor(){
            public Object visit(Node node, Object context) {
                if(node instanceof Joint){
                    final Joint jt = (Joint) node;
                    jt.updateBindBose();
                    return super.visit(node, context);
                }
                //something else attached to the joint, skip it
                return null;
            }
        };

    private static final NodeVisitor INVBINDPOSE_SOLVER = new DefaultNodeVisitor(){
            public Object visit(Node node, Object context) {
                if(node instanceof Joint){
                    final Joint jt = (Joint) node;
                    jt.getInvertBindPose().set(jt.getBindPose());
                    jt.getInvertBindPose().localInvert();
                    return super.visit(node, context);
                }
                //something else attached to the joint, skip it
                return null;
            }
        };

    private static final NodeVisitor RESTORE_BASE = new DefaultNodeVisitor(){
            public Object visit(Node node, Object context) {
                if(node instanceof Joint){
                    final Joint jt = (Joint) node;
                    final AffineRW bindPose = jt.getBindPose();
                    bindPose.set(jt.getInvertBindPose());
                    bindPose.localInvert();
                    //at this stage, the bind pose contains the root to joint matrix

                    final SceneNode parent = jt.getParent();
                    if(parent instanceof Joint){
                        final Joint jpa = (Joint) parent;
                        //we remove the root to parent transform
                        final AffineRW parentToRoot = jpa.getBindPose().invert();
                        final AffineRW parentToJoint = parentToRoot.multiply(bindPose);
                        //convert the matrix back to a NodeTransform
                        jt.getNodeTransform().set(parentToJoint);
                    }else{
                        //no parent, the bind pose is the same as parent to joint matrix
                        jt.getNodeTransform().set(bindPose);
                    }

                    return super.visit(node, context);
                }
                //something else attached to the joint, skip it
                return null;
            }
        };

    private final Sequence iks = new ArraySequence();

    //view of all joints in the skeleton
    private final Sequence joints;

    public Skeleton() {
        this(3);
    }

    public Skeleton(int dimension) {
        super(dimension);
        joints = new NodeSequence(this, false, new ClassPredicate(Joint.class));
    }

    public Skeleton(CoordinateSystem cs) {
        super(cs);
        joints = new NodeSequence(this, false, new ClassPredicate(Joint.class));
    }

    public Sequence getAllJoints() {
        return joints;
    }


    /**
     * Get the joint for given joint identifier.
     *
     * @param identifier if identifier is a CharArray the joint name will be used
     *                   if identifier is a Number the joint id will be used
     * @return Joint may be null
     */
    public Joint getJoint(final Object identifier){
        if(identifier instanceof CharArray){
            return (Joint) Nodes.SEARCHER.visit(this, new Predicate() {
                public Boolean evaluate(Object candidate) {
                    return candidate instanceof Joint && identifier.equals(((Joint)candidate).getName());
                }
            });
        }else if(identifier instanceof Number){
            final int jointId = ((Number)identifier).intValue();
            return (Joint) Nodes.SEARCHER.visit(this, new Predicate() {
                public Boolean evaluate(Object candidate) {
                    return candidate instanceof Joint && jointId == ((Joint)candidate).getId();
                }
            });
        }else{
            throw new InvalidArgumentException("Unexpected identifier type "+identifier+", expecting a CharArray or Number.");
        }
    }


    /**
     * @return Sequence of inverse kinematics.
     */
    public Sequence getIks() {
        return iks;
    }

    /**
     * Calculate root to joint matrix and invert matrix for each joint.
     */
    public void updateBindPose(){
        for(int i=0,n=children.getSize();i<n;i++){
            ((Joint)children.get(i)).accept(BINDPOSE_SOLVER, null);
        }
    }

    /**
     * Calculate root to joint matrix and invert matrix for each joint.
     */
    public void updateInvBindPose(){
        for(int i=0,n=children.getSize();i<n;i++){
            ((Joint)children.get(i)).accept(INVBINDPOSE_SOLVER, null);
        }
    }

    /**
     * When all joints transforms are world to joint transforms.
     * This method will rebuild the parent to joint transforms.
     */
    public void reverseWorldPose(){
        for(int i=0,n=children.getSize();i<n;i++){
            SceneUtils.reverseWorldToNodeTrsRecursive((Joint)children.get(i));
        }
    }

    /**
     * When all joints transforms are parent to joint transforms.
     * This method will rebuild the world to joint transforms.
     */
    public void resetToWorldPose(){
        for(int i=0,n=children.getSize();i<n;i++){
            SceneUtils.reverseParentToNodeTrsRecursive((Joint)children.get(i));
        }
    }

    /**
     * Use the inverse bind pose matrix to restore the original joint poses.
     * Bind pose matrix and NodeTransform are restored.
     */
    public void resetToBase(){
        for(int i=0,n=children.getSize();i<n;i++){
            ((Joint)children.get(i)).accept(RESTORE_BASE, null);
        }
    }

    /**
     * Loop on all inverse kinematics and call solve method.
     */
    public void solveKinematics(){
        final Iterator ikite = iks.createIterator();
        while(ikite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ikite.next();
            ik.solve();
        }
    }

    /**
     * Deep cop, copies joint nodes.
     *
     * @return
     */
    public Skeleton copy(){
        final Skeleton copy = new Skeleton();
        for(int i=0,n=children.getSize();i<n;i++){
            copy.getChildren().add( ((Joint)children.get(i)).deepCopy());
        }
        return copy;
    }

    public Chars toChars() {
        return new Chars("Skeleton");
    }
    
    public Chars toChars(int maxDepth) {
        final CharBuffer sb = new CharBuffer();
        sb.append("Skeleton :\n");
        for(int i=0,n=children.getSize();i<n;i++){
            sb.append( ((Joint)children.get(i)).toCharsTree(maxDepth).toString());
            sb.append("\n");
        }
        return sb.toChars();
    }

}
