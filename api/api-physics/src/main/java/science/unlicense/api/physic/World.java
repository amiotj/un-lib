
package science.unlicense.api.physic;

import science.unlicense.api.physic.body.Body;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.physic.force.Force;
import science.unlicense.api.physic.force.Singularity;
import science.unlicense.api.physic.integration.Integrator;

/**
 * Physic world, adapted from collada.
 * Reference : Collada 1.5.0 (Chapter 6: Physics Reference,p.228)
 *
 * @author Johann Sorel
 */
public interface World extends EventSource{

    public static final Chars PROPERTY_UPDATE = new Chars("update");

    /**
     * Get World global viscosity/drag.
     * to simulate air friction.
     * 
     * @return drag [0-1]
     */
    double getDrag();
    
    /**
     * Set World global viscosity/drag.
     * 
     * @param drag [0-1]
     */
    void setDrag(double d);
    
    /**
     * Get integrator used to update the physique world.
     * @return Integrator
     */
    Integrator getIntegrator();

    /**
     * Set integrator used to update the physique world.
     * @param solver
     */
    void setIntegrator(Integrator integrator);

    /**
     * Contain object holding all collisions and managing events.
     * 
     * @return CollisionsState
     */
    CollisionsState getCollisionsState();
    
    /**
     * List the world singularities.
     * The list is a view, use addSingularity/removeSingularity methods to modify it.
     *
     * @return Sequence, never null, can be empty.
     */
    Sequence getSingularities();

    /**
     * Add singularity in this world.
     * @param singularity
     */
    void addSingularity(Singularity singularity);

    /**
     * Remove singularity from this world.
     * @param singularity
     */
    void removeSingularity(Singularity singularity);

    /**
     * List the world bodies.
     * The list is a view, use addBody/removeBody methods to modify it.
     *
     * @return Sequence, never null, can be empty.
     */
    Sequence getBodies();

    /**
     * Add body in this world.
     * @param body
     */
    void addBody(Body body);

    /**
     * Remove body from this world.
     * @param body
     */
    void removeBody(Body body);

    /**
     * List the present forces.
     * The list is a view, use addForce/removeForce methods to modify it.
     *
     * @return Sequence, never null, can be empty.
     */
    Sequence getForces();

    /**
     * Add force in this world.
     * @param force
     */
    void addForce(Force force);

    /**
     * Remove force from this world.
     * @param force
     */
    void removeForce(Force force);

    /**
     * Remove all elements in this world.
     */
    void removeAll();
    
    /**
     * List the world bodies group.
     * Groups are used to define collision exclusions.
     * Bodies is the same group are not tested for collision.
     *
     * @return Sequence, never null, can be empty.
     */
    Sequence getGroups();

    /**
     * Integration time step, used by solver.
     * @return double in seconds
     */
    double getIntegrationTimeStep();

    /**
     * Update the world using current solver.
     * @param timeellapsed in seconds
     */
    void update(final double timeellapsed);

}