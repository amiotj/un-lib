
package science.unlicense.api.physic.force;

import science.unlicense.api.physic.body.Body;

/**
 *
 * @author Johann Sorel
 */
public interface Singularity {
    
    /**
     * Apply singularity on given body.
     * @param body 
     */
    void apply(Body body);
    
}