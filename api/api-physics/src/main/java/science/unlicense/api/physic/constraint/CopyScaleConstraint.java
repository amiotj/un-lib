
package science.unlicense.api.physic.constraint;

import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class CopyScaleConstraint implements Constraint{

    private final SceneNode target;
    private final SceneNode toCopy;
    private final float factor;

    public CopyScaleConstraint(SceneNode target, SceneNode toCopy, float factor) {
        this.target = target;
        this.toCopy = toCopy;
        this.factor = factor;
    }

    public void apply() {
        applyConstraint(target, toCopy, factor);
    }

    static void applyConstraint(SceneNode target, SceneNode toCopy, float factor){
        final Vector scl = toCopy.getNodeTransform().getScale();
        final Vector baseScl = target.getNodeTransform().getScale();
        scl.scale(factor, baseScl);
        target.getNodeTransform().notifyChanged();
    }

}