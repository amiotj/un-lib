
package science.unlicense.api.physic.constraint;

/**
 *
 * @author Johann Sorel
 */
public interface Constraint {

    void apply();

}
