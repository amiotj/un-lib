
package science.unlicense.api.physic.skeleton;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.physic.constraint.Constraint;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 * CCD (Cyclic Coordinate Descent) method to solve IK.
 * Works by succesive pass moving the nodes rotation until it is close enough.
 * This method is used in PMD models.
 *
 * @author Johann Sorel
 */
public class IKSolverCCD implements IKSolver{

    /**
     * Value used to break, when distance or angle is small enough.
     * Angle is small, skip updating this node.
     */
    private static final double DELTA = 1e-8;

    private final int maxIteration;
    private final double maxAngle;
    
    //caches
    private final Vector effectorPosition;
    private final Vector targetPosition;
    private final Vector vec;
    private final Matrix3x3 rotation;
    
    

    /**
     *
     * @param maxIteration maximum number of iteration until break out.
     *         the solution might be found sooner.
     * @param maxAngle maximum angle applied to a joint at each iteration
     *         this avoid having to shark changes between each iteration which
     *         would not produce a good final result.
     */
    public IKSolverCCD(int maxIteration, double maxAngle, int dimension) {
        this.maxIteration = maxIteration;
        this.maxAngle = maxAngle;
        
        this.effectorPosition = new Vector(dimension);
        this.targetPosition = new Vector(dimension);
        this.vec = new Vector(dimension);
        this.rotation = new Matrix3x3().setToIdentity();
    }

    public int getMaxIteration() {
        return maxIteration;
    }

    public double getMaxAngle() {
        return maxAngle;
    }

    public void solve(final InverseKinematic ik) {
        final Joint[] chainJoints = ik.getChain();
        final Joint effector = ik.getEffector();
        final Joint target = ik.getTarget();
        final AffineRW effectorBindPose = effector.getBindPose();
        final AffineRW targetBindPose = target.getBindPose();

        
        for(int i=0;i<maxIteration;i++){

            //loop on the chain joints, they should be is the effector > base order
            for(int k=0;k<chainJoints.length;k++){
                final Joint joint = chainJoints[k];

                //calculate effector and target position in root joint space
                final AffineRW matInvBone = joint.getBindPose().invert();
                getTranslation(effectorBindPose,effectorPosition);
                getTranslation(targetBindPose,targetPosition);
                matInvBone.transform(effectorPosition, effectorPosition);
                matInvBone.transform(targetPosition, targetPosition);

                // check if the effector is close to the target
                effectorPosition.subtract(targetPosition, vec);
                if(vec.dot(vec) < DELTA) return;

                // calculate rotation angle
                effectorPosition.localNormalize();
                targetPosition.localNormalize();
                double rotationAngle = effectorPosition.shortestAngle(targetPosition);
                if(Math.abs(rotationAngle) < DELTA) continue;
                //avoid too much rotation by iteration
                rotationAngle = Maths.clamp(rotationAngle, -maxAngle, maxAngle);

                // calculate rotation axis
                effectorPosition.cross(targetPosition,vec);
                if (vec.dot(vec) < DELTA) continue;
                vec.localNormalize();

                //buil rotation matrix
                rotation.fromAngle(rotationAngle, vec);              
                final MatrixRW baserot = joint.getNodeTransform().getRotation();
                baserot.localMultiply(rotation);

                //apply constraints
                final Iterator ite = joint.getConstraints().createIterator();
                while(ite.hasNext()){
                    final Constraint cst = (Constraint) ite.next();
                    cst.apply();
                }
                joint.getNodeTransform().notifyChanged();

                //update the previous joints position
                for(int p=k; p>=0; p--){
                    chainJoints[p].updateBindBose();
                }
                effector.updateBindBose();
            }
        }
    }

    private static void getTranslation(Affine m, Vector buffer){
        final double[] t = buffer.getValues();
        for(int i=0;i<t.length;i++) t[i] = m.get(i,t.length);
    }
    
}