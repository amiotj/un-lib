
package science.unlicense.api.physic.law;

import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public final class Euler {
    
    /**
     * Calculate acceleration.
     * acceleration = force / mass.
     * @param force
     * @param mass
     * @return 
     */
    public static Vector acceleration(Vector force, double mass){
        return force.scale(1/mass);
    }
    
    /**
     * Calculate velocity.
     * acceleration * time_step
     * 
     * @param acceleration
     * @param timeellapsed
     * @return 
     */
    public static Vector velocity(Vector acceleration, double timeellapsed){
        return acceleration.scale(timeellapsed);
    }
    
}
