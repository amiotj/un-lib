
package science.unlicense.api.physic.constraint;

import science.unlicense.api.math.Affine;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;

/**
 * Force target node to look at given node.
 *
 * @author Johann Sorel
 */
public class FocusConstraint implements Constraint{

    private final SceneNode node;
    private SceneNode target;

    public FocusConstraint(SceneNode node, SceneNode target) {
        this.node = node;
        this.target = target;
    }

    public SceneNode getTarget() {
        return target;
    }

    public void setTarget(SceneNode target) {
        this.target = target;
    }

    public void apply() {
        if(target==null) return;

        final Affine rootToNode = node.getRootToNodeSpace();
        final Affine targetToRoot = target.getNodeToRootSpace();
        final Affine targetToNode = targetToRoot.multiply(rootToNode);
        
        final Vector targetPosition = (Vector)(targetToNode.transform(new Vector(0,0,0,1), new Vector(4))).getXYZ();

        if(targetPosition.length()<=0){
            //node overlaps
            return;
        }
        
        final Vector up = new Vector(0, 1, 0, 0);
        targetToNode.transform(up, up);
        up.localNormalize();

        final Matrix4x4 newTransform = new Matrix4x4(Matrices.lookAt(new Vector(0, 0, 0), targetPosition, up.getXYZ(), null));

//        node.setRootToNodeSpace(newTransform);
        node.getNodeTransform().getRotation().set(newTransform.getMatrix(0, 2, 0, 2));
        node.getNodeTransform().notifyChanged();
    }

}
