
package science.unlicense.api.physic.body;

import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;
import science.unlicense.api.scenegraph.DefaultSceneNode;

/**
 * Physic body, mix from various 3d format (collada,pmx,...)
 * Reference : Collada 1.5.0 (Chapter 6: Physics Reference,p.200)

 * @author Johann Sorel
 */
public abstract class AbstractBody extends DefaultSceneNode implements Body {

    //groups
    protected BodyGroup group;
    protected final Set noCollisionGroups = new HashSet();

    // force state
    protected double mass;
    protected double inverseMass;
    
    protected boolean phantom = false;

    public AbstractBody(int dimension) {
        super(dimension, false);
    }

    public boolean isPhantom() {
        return phantom;
    }

    public void setPhantom(boolean phantom) {
        this.phantom = phantom;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Material informations ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get body mass.
     * value 0 is used for infinite mass.
     * @return body mass
     */
    public double getMass(){
        return mass;
    }

    /**
     * Set body mass.
     * value 0 is used for infinite mass.
     * @param mass
     */
    public void setMass(double mass) {
        this.mass = mass;
        if(mass == 0){
            this.inverseMass = 0;
        }else{
            this.inverseMass = 1/mass;
        }
    }

    /**
     * Get body inverse mass.
     * @return inverse body mass
     */
    public double getInverseMass(){
        return inverseMass;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Groups informations /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Group in which this body is.
     * @return sequence of body groups
     */
    public BodyGroup getGroup(){
        return group;
    }

    public void setGroup(BodyGroup group) {
        if(this.group!=null){
            this.group.remove(this);
        }
        this.group = group;
        if(this.group!=null){
            this.group.add(this);
        }
    }

    /**
     * Test if this body and given body are in groups which can collide.
     * @param candidate
     * @return true if group are not exclusive.
     */
    public boolean canGroupCollide(Body candidate){
        if(group != null && group == candidate.getGroup()){
            //same group, no collision
            return false;
        }

        return !(noCollisionGroups.contains(candidate.getGroup()) ||
                candidate.getNoCollisionGroups().contains(group) );
    }

    public void addNoCollisionGroup(BodyGroup group){
        noCollisionGroups.add(group);
    }

    public void removeNoCollisionGroup(BodyGroup group){
        noCollisionGroups.remove(group);
    }

    public Set getNoCollisionGroups() {
        return noCollisionGroups;
    }

}