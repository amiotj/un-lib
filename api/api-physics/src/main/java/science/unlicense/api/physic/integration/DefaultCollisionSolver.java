
package science.unlicense.api.physic.integration;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.operation.Collision;
import science.unlicense.api.physic.body.Material;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class DefaultCollisionSolver implements CollisionSolver{

    public void update(final World world, final double timeellapsed){
        world.getCollisionsState().update();
        final Sequence cols = world.getCollisionsState().getCollisions();
        
        for(int i=0,n=cols.getSize();i<n;i++){
            final Collision col = (Collision) cols.get(i);
            solveCollision(col);
        }
    }

    /**
     * Adapted from tutorial :
     * http://gamedev.tutsplus.com/tutorials/implementation/create-custom-2d-physics-engine-aabb-circle-impulse-resolution/
     *
     * @param collision
     * @param bodyA
     * @param bodyB
     */
    private void solveCollision(Collision collision){

        final RigidBody bodyA = (RigidBody) collision.getFirst();
        final RigidBody bodyB = (RigidBody) collision.getSecond();
        if(bodyA.isPhantom() || bodyB.isPhantom()){
            //phantom object, no collision resolution
            return;
        }
                
        final Vector normal = collision.getNormal();
        // Calculate relative velocity
        final Vector velocityA = bodyA.getMotion().getVelocity();
        final Vector velocityB = bodyB.getMotion().getVelocity();
        final Vector rv = velocityB.subtract(velocityA);

        // Calculate relative velocity in terms of the normal direction
        final double velAlongNormal = normal.dot(rv);

        // Do not resolve if velocities are separating
        if(velAlongNormal > 0){
          return;
        }

        final Material materialA = bodyA.getMaterial();
        final Material materialB = bodyB.getMaterial();

        // Calculate restitution
        final double restitution = Maths.min(materialA.getRestitution(), materialB.getRestitution());

        // Calculate impulse
        double j = -(1 + restitution) * velAlongNormal;
        j /= bodyA.getInverseMass() + bodyB.getInverseMass();
        final Vector impulse = normal.scale(j);

        // Calculate ratio based on mass of each body
        final double massA = bodyA.getMass();
        final double massB = bodyB.getMass();
        final double sumMass = massA + massB;
        final double ratioA;
        final double ratioB;
        if(sumMass==0){
            //undefined masses, we can't solve it
            return;
        }else{
            ratioA = massA / sumMass;
            ratioB = massB / sumMass;
        }

        //rollback in time to first contact
        double rollbackTime = collision.getRollbackTime();
        if(rollbackTime!=0){
            if(bodyA.isFree() && !velocityA.isZero()){
                Vector correction = new Vector(normal.getSize());
                bodyA.getWorldPosition(correction);
                correction.localAdd(velocityA.scale(rollbackTime));
                bodyA.setWorldPosition(correction);
            }
            if(bodyB.isFree() && !velocityB.isZero()){
                Vector correction = new Vector(normal.getSize());
                bodyB.getWorldPosition(correction);
                correction.localAdd(velocityB.scale(rollbackTime));
                bodyB.setWorldPosition(correction);
            }

        }else{
            //todo remove this code when rollback time is implemented everywhere

        //check rest state
//        if(Math.abs(velAlongNormal) < 0.00001){
            //object collide but they have no velocity
            //that may be the case if the world started in this state
            //or because of cumulative arithmetic calculation errors
            // solution : we push them aside
            final double distance = collision.getDistance();
            Vector pushA = normal.scale(distance*ratioA);
            Vector pushB = normal.scale(-distance*ratioB);
            if(bodyA.isFree()) bodyA.worldTranslate(pushA);
            if(bodyB.isFree()) bodyB.worldTranslate(pushB);
//        }
        }
        
        // Update impulse
        if(ratioA>0){
            //linear
            final Vector impulseA = impulse.scale(-bodyA.getInverseMass()*ratioA);
            bodyA.getMotion().getVelocity().localAdd(impulseA);
            //angular TODO BUGS
//            Vector impactA = new Vector(collision.getImpactGeometries()[0].getCentroid().getCoordinate());
//            final Matrix rootToNodeSpace = bodyA.getRootToNodeSpace();
//            final Tuple com = bodyA.getGeometry().getCentroid().getCoordinate();
//            impactA = (Vector) rootToNodeSpace.transform(impactA, 1);
//            Vector normalA = (Vector) rootToNodeSpace.transform(normal,0);
//            Vector impactDistA = impactA.subtract(com);
//            bodyA.getMotion().getAngularVelocity().localAdd(normalA.cross(impactDistA).scale(-j*bodyA.getInverseMass()));
        }
        
        if(ratioB>0){
            //linear
            final Vector impulseB = impulse.scale(bodyB.getInverseMass()*ratioB);
            bodyB.getMotion().getVelocity().localAdd(impulseB);
            //angular
//            final Vector impactB = new Vector(collision.getImpactGeometries()[1].getCentroid().getCoordinate());
//            Vector impactDistB = impactB.subtract(bodyB.getGeometry().getCentroid().getCoordinate());
//            bodyB.getMotion().getAngularVelocity().localAdd(normal.cross(impactDistB).scale(j*bodyA.getInverseMass()));
            //bodyB.getAngularVelocity().localAdd(normal.cross(impactDistB));
        }
        
    }

}
