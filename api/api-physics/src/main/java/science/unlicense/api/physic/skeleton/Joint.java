
package science.unlicense.api.physic.skeleton;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.DefaultAffine;

/**
 *
 * @author Johann Sorel
 */
public class Joint extends DefaultSceneNode {

    private final AffineRW bindpose;
    private final AffineRW invbindpose;

    private CharArray name = new Chars(Arrays.ARRAY_BYTE_EMPTY);
    private int id = -1;

    private final Sequence constraints = new ArraySequence();

    public Joint(int dimension) {
        this(dimension,null);
    }

    public Joint(int dimension,CharArray name) {
        super(dimension, true);
        bindpose = DefaultAffine.create(dimension);
        invbindpose = DefaultAffine.create(dimension);
        setName(name);
    }

    /**
     * Constraints applied to this joint.
     */
    public Sequence getConstraints() {
        return constraints;
    }

    /**
     * Get node id.
     * @return integer
     */
    public int getId() {
        return id;
    }

    /**
     * Set node id.
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get node name.
     * @return Chars, can be null.
     */
    public CharArray getName() {
        return name;
    }

    /**
     * Set node name.
     * @param name , can be null.
     */
    public void setName(CharArray name) {
        this.name = name;
    }

    public AffineRW getBindPose() {
        return bindpose;
    }

    public AffineRW getInvertBindPose() {
        return invbindpose;
    }

    /**
     * Update bind pose, relative to the parent bind pose.
     */
    public void updateBindBose(){
        final SceneNode parent = getParent();
        if(parent instanceof Joint){
            final Joint jpa = (Joint) parent;
            getBindPose().set(jpa.getBindPose().multiply(getNodeTransform()));
        }else{
            getBindPose().set(getNodeTransform());
        }
    }

    /**
     * @return copy of this joint without children
     */
    public Joint copy(){
        final Joint t = new Joint(bindpose.getInputDimensions());
        t.setName(getName());
        t.getNodeTransform().set(getNodeTransform());
        t.bindpose.set(bindpose);
        t.invbindpose.set(invbindpose);
        return t;
    }

    public Joint deepCopy(){
        final Joint copy = copy();
        for(int i=0,n=children.getSize();i<n;i++){
            copy.children.add(((Joint)children.get(i)).deepCopy() );
        }
        return copy;
    }

    /**
     * @return true if node is an element in a kinematic chain.
     */
    public boolean isKinematic(Skeleton skeleton){
        final Iterator ite = skeleton.getIks().createIterator();
        while(ite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ite.next();
            if(Arrays.contains(ik.getChain(), this)){
                return true;
            }
        }
        return false;
    }

    /**
     * @return true if node is a kinematic effector.
     */
    public boolean isKinematicEffector(Skeleton skeleton){
        final Iterator ite = skeleton.getIks().createIterator();
        while(ite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ite.next();
            if(ik.getEffector() == this){
                return true;
            }
        }
        return false;
    }

    /**
     * @return true if node is a kinematic target.
     */
    public boolean isKinematicTarget(Skeleton skeleton){
        final Iterator ite = skeleton.getIks().createIterator();
        while(ite.hasNext()){
            final InverseKinematic ik = (InverseKinematic) ite.next();
            if(ik.getTarget() == this){
                return true;
            }
        }
        return false;
    }

    public Chars thisToChars() {
        if(getName() instanceof LChars){
            return ((LChars)getName()).toCharsAll();
        }
        return CObjects.toChars(getName());
    }

}