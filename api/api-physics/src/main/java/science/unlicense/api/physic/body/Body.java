
package science.unlicense.api.physic.body;

import science.unlicense.api.collection.Set;
import science.unlicense.api.scenegraph.SceneNode;

/**
 * Physic body, mix from various 3d format (collada,pmx,...)
 * Reference : Collada 1.5.0 (Chapter 6: Physics Reference,p.200)

 * @author Johann Sorel
 */
public interface Body extends SceneNode {

    /**
     * Phantom bodies exist only to trigger events.
     * Could be used to determinate if an object enter an area.
     * 
     * No collision resolution should be made on such objects
     * 
     * @return true if body is a phantom;
     */
    boolean isPhantom();
    
    /**
     * Set body phantom state.
     * 
     * @param phantom 
     */
    void setPhantom(boolean phantom);
    
    /**
     * Get body mass.
     * value 0 is used for infinite mass.
     * @return body mass
     */
    double getMass();

    /**
     * Set body mass.
     * value 0 is used for infinite mass.
     * @param mass
     */
    void setMass(double mass);

    /**
     * Get body inverse mass.
     * @return inverse body mass
     */
    double getInverseMass();

    ////////////////////////////////////////////////////////////////////////////
    // State informations //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Motion state of the body.
     * @return Motion
     */
    Motion getMotion();
    
    ////////////////////////////////////////////////////////////////////////////
    // Groups informations /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Group in which this body is.
     * @return sequence of body groups
     */
    BodyGroup getGroup();

    void setGroup(BodyGroup group);

    /**
     * Test if this body and given body are in groups which can collide.
     * @param candidate
     * @return true if group are not exclusive.
     */
    boolean canGroupCollide(Body candidate);

    void addNoCollisionGroup(BodyGroup group);

    void removeNoCollisionGroup(BodyGroup group);

    Set getNoCollisionGroups();

}