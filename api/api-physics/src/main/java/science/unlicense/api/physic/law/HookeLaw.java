
package science.unlicense.api.physic.law;

import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;

/**
 * Law used for linear type spring objects.
 * Reference : http://en.wikipedia.org/wiki/Hooke's_law
 * 
 * F = kx
 * where :
 * k is the material stiffness
 * x is a distance
 * 
 * @author Johann Sorel
 */
public final class HookeLaw {
    
    private HookeLaw(){}
    
    /**
     * Calculate force.
     * F = kx
     * 
     * @param stiffness
     * @param distance
     * @return Vector force
     */
    public static Vector force(double stiffness, Vector distance){
        return distance.scale(stiffness);
    }
    
    /**
     * Calculate stiffness.
     * k = F/x
     * 
     * @param force
     * @param distance
     * @return double stiffness
     */
    public static double stiffness(Vector force, Vector distance){
        final Vector t = force.divide(distance);
        return Maths.mean(t.getValues());
    }
    
    /**
     * Calculate distance.
     * x = F/k
     * 
     * @param force
     * @param stiffness
     * @return Vector distance
     */
    public static Vector distance(Vector force, double stiffness){
        return force.scale(1/stiffness);
    }
    
}
