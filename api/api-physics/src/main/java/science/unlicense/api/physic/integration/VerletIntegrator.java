
package science.unlicense.api.physic.integration;

import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.WorldUtilities;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.impl.math.Vector;

/**
 * 
 * Source : 
 * http://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet
 * 
 * tutorials :
 * http://gamedevelopment.tutsplus.com/tutorials/simulate-tearable-cloth-and-ragdolls-with-simple-verlet-integration--gamedev-519
 * http://www.gamedev.net/page/resources/_/technical/math-and-physics/a-verlet-based-approach-for-2d-game-physics-r2714
 * http://graphics.cs.cmu.edu/nsp/course/15-869/2006/papers/jakobsen.htm
 * 
 * @author Johann Sorel
 */
public final class VerletIntegrator extends AbstractIntegrator{

    private static final class VerletState{
        public final Vector lastPosition;
        public final Vector acceleration;

        public VerletState(int wd) {
            this.lastPosition = new Vector(wd);
            this.acceleration = new Vector(wd);
        }
    }

    private final HashDictionary states = new HashDictionary(Hasher.IDENTITY);
    private final int worldDimension;
    
    public VerletIntegrator(int worldDimension) {
        this.worldDimension = worldDimension;
    }

    @Override
    public void update(World world, double timeellapsed) {

        final Sequence bodies = world.getBodies();

        //apply singularity and forces
        WorldUtilities.applyForces(world);

        //calculate acceleration
        for(int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body)bodies.get(i);
            if(!(body instanceof RigidBody)) continue;
            final RigidBody rb = (RigidBody) body;
            if(!rb.isFree()) continue;

            VerletState state = (VerletState) states.getValue(rb);
            if(state==null){
                state = new VerletState(worldDimension);
                state.lastPosition.set(rb.getWorldPosition(null));
                states.add(rb, state);
            }

            rb.getAcceleration(state.acceleration);
        }

        //solve constraints
        for(int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body)bodies.get(i);
            if(!(body instanceof RigidBody)) continue;
            final RigidBody rb = (RigidBody) body;
            if(!rb.isFree()) continue;
         
            //TODO
            
        }
        
        //calculate positions base on acceleration
        final double time2 = timeellapsed * timeellapsed;
        for(int i=0,n=bodies.getSize(); i<n; i++){
            final Body body = (Body)bodies.get(i);
            if(!(body instanceof RigidBody)) continue;
            final RigidBody rb = (RigidBody) body;
            if(!rb.isFree()) continue;
            
            final VerletState state = (VerletState) states.getValue(rb);
            final Vector pos = rb.getWorldPosition(null);
            final Vector vel = pos.subtract(state.lastPosition);
            final Vector newPos = vel.add(state.acceleration.scale(time2));
            state.lastPosition.set(pos);
            rb.setWorldPosition(newPos);
        }

        //clear forces
        WorldUtilities.clearBodyForces(world);

    }

}
