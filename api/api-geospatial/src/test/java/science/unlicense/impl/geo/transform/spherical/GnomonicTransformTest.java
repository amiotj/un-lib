package science.unlicense.impl.geo.transform.spherical;

import science.unlicense.impl.geo.transform.spherical.GnomonicTransform;
import static java.lang.Math.toRadians;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.geo.transform.AbstractTransformTest;

/**
 *
 * @author Samuel Andrés
 */
public class GnomonicTransformTest extends AbstractTransformTest{

    private static final double RADIUS = 1.;
    final double[] expectedXTab = new double[]{0.0000, 0.1763, 0.3640, 0.5774, 0.8391, 1.1918, 1.7321, 2.7475, 5.6713};
    
    /**
     * Resources: Map Projections - A Working Manual.
     * John P. Snyder U.S. Geological Survey Professional Paper 1395
     * Table 26, p. 168
     */
    @Test
    public void test_equatorial(){
        
        final GnomonicTransform projection = new GnomonicTransform(RADIUS, 0., 0.);
        
        equatorialTest(80., new double[]{5.6713, 5.7588, 6.0353, 6.5486, 7.4033, 8.8229, 11.3426, 16.5817, 32.6596}, projection);
        equatorialTest(70., new double[]{2.7475, 2.7899, 2.9238, 3.1725, 3.5866, 4.2743, 5.4950, 8.0331, 15.8221}, projection);
        equatorialTest(60., new double[]{1.7321, 1.7588, 1.8432, 2.0000, 2.2610, 2.6946, 3.4641, 5.0642, 9.9745}, projection);
        equatorialTest(50., new double[]{1.1918, 1.2101, 1.2682, 1.3761, 1.5557, 1.8540, 2.3835, 3.4845, 6.8630}, projection);
        equatorialTest(40., new double[]{.8391, .8520, .8930, .9689, 1.0954, 1.3054, 1.6782, 2.4534, 4.8322}, projection);
        equatorialTest(30., new double[]{.5774, .5863, .6144, .6667, .7537, .8982, 1.1547, 1.6881, 3.3248}, projection);
        equatorialTest(20., new double[]{.3640, .3696, .3873, .4203, .4751, .5662, .7279, 1.0642, 2.0960}, projection);
        equatorialTest(10., new double[]{.1763, .1790, .1876, .2036, .2302, .2743, .3527, .5155, 1.0154}, projection);
        equatorialTest(0., new double[]{.0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000}, projection);
    }   
    
    private void equatorialTest(final double latitude, final double[] expectedYTab, final GnomonicTransform projection){
        
        for(int i=0; i<=8; i++){
            Assert.assertArrayEquals(new double[]{expectedXTab[i], expectedYTab[i]}, 
                    projection.transform(new double[]{toRadians(latitude), toRadians(i*10.)},null), DELTA);
        }
    }
}
