package science.unlicense.impl.geo.transform.spherical;

import science.unlicense.impl.geo.transform.spherical.OrthographicTransform;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.toRadians;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.geo.transform.AbstractTransformTest;

/**
 *
 * @author Samuel Andrés
 */
public class OrthographicTransformTest extends AbstractTransformTest{

    private static final double RADIUS = 1.;
    
    @Test
    public void test_1(){
        
        final OrthographicTransform projection = new OrthographicTransform(RADIUS, PI/2, 0);
        
        Assert.assertArrayEquals(new double[]{0., 0.}, projection.transform(new double[]{PI/2, 0.},null), 0.);
        
        // Nord
        Assert.assertArrayEquals(new double[]{0.5, -0.5}, projection.transform(new double[]{PI/4, PI/4},null), 1e-10);
        Assert.assertArrayEquals(new double[]{-0.5, -0.5}, projection.transform(new double[]{PI/4, -PI/4},null), 1e-10);
        Assert.assertArrayEquals(new double[]{0.5, 0.5}, projection.transform(new double[]{PI/4, 3*PI/4},null), 1e-10);
        Assert.assertArrayEquals(new double[]{-0.5, 0.5}, projection.transform(new double[]{PI/4, -3*PI/4},null), 1e-10);
        
        // Sud
        Assert.assertArrayEquals(new double[]{-0.5, -0.5}, projection.transform(new double[]{-PI/4, -PI/4},null), 1e-10);
        Assert.assertArrayEquals(new double[]{0.5, -0.5}, projection.transform(new double[]{-PI/4, PI/4},null), 1e-10);
        
        Assert.assertArrayEquals(new double[]{cos(PI/4), 0.}, projection.transform(new double[]{PI/4, PI/2},null), 1e-30);
    }    
    
    @Test
    public void test_2(){
        
        final OrthographicTransform projection = new OrthographicTransform(RADIUS, PI/4, 0);
        
        Assert.assertArrayEquals(new double[]{0., 0.}, projection.transform(new double[]{PI/4, 0.},null), 0.);
        
        Assert.assertArrayEquals(new double[]{0., -1.}, projection.transform(new double[]{-PI/4., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -1.}, projection.transform(new double[]{-10.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.9969173337331281}, projection.transform(new double[]{-11.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.9876883405951378}, projection.transform(new double[]{-12.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.9723699203976766}, projection.transform(new double[]{-13.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.9510565162951535}, projection.transform(new double[]{-14.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.9238795325112867}, projection.transform(new double[]{-15.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.8910065241883679}, projection.transform(new double[]{-16.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.8526401643540923}, projection.transform(new double[]{-17.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.8090169943749476}, projection.transform(new double[]{-18.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.760405965600031}, projection.transform(new double[]{-19.*PI/40., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., -0.7071067811865476}, projection.transform(new double[]{-PI/2., 0.},null), 0.);
        Assert.assertArrayEquals(new double[]{0., 0.}, projection.transform(new double[]{-3*PI/4., 0.},null), 1e-15);
        
    }    
    
    /**
     * Resources: Map Projections - A Working Manual.
     * John P. Snyder U.S. Geological Survey Professional Paper 1395
     * Table 22, p. 151
     */
    @Test
    public void test_equatorial(){
        
        final OrthographicTransform projection = new OrthographicTransform(RADIUS, 0., 0.);
        
        equatorialTest(90., 1., new double[]{.0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000}, projection);
        equatorialTest(80., .9848, new double[]{.0000, .0302, .0594, .0868, .1116, .1330, .1504, .1632, .1710, .1736}, projection);
        equatorialTest(70., .9397, new double[]{.0000, .0594, .1170, .1710, .2198, .2620, .2962, .3214, .3368, .3420}, projection);
        equatorialTest(60., .8660, new double[]{.0000, .0868, .1710, .2500, .3214, .3830, .4330, .4698, .4924, .5000}, projection);
        equatorialTest(50., .7660, new double[]{.0000, .1116, .2198, .3214, .4132, .4924, .5567, .6040, .6330, .6428}, projection);
        equatorialTest(40., .6428, new double[]{.0000, .1330, .2620, .3830, .4924, .5868, .6634, .7198, .7544, .7660}, projection);
        equatorialTest(30., .5000, new double[]{.0000, .1504, .2962, .4330, .5567, .6634, .7500, .8138, .8529, .8660}, projection);
        equatorialTest(20., .3420, new double[]{.0000, .1632, .3214, .4698, .6040, .7198, .8138, .8830, .9254, .9397}, projection);
        equatorialTest(10., .1736, new double[]{.0000, .1710, .3368, .4924, .6330, .7544, .8529, .9254, .9698, .9848}, projection);
        equatorialTest(0., .0000, new double[]{.0000, .1736, .3420, .5000, .6428, .7660, .8660, .9397, .9848, 1.}, projection);
    }   
    
    private void equatorialTest(final double latitude, final double expectedY, final double[] expectedXTab, final OrthographicTransform projection){
        
        Assert.assertArrayEquals(new double[]{expectedXTab[0], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(0.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[1], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(10.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[2], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(20.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[3], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(30.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[4], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(40.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[5], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(50.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[6], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(60.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[7], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(70.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[8], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(80.)},null), DELTA);
        Assert.assertArrayEquals(new double[]{expectedXTab[9], expectedY}, projection.transform(new double[]{toRadians(latitude), toRadians(90.)},null), DELTA);
    }
    
    /**
     * Resources: Map Projections - A Working Manual.
     * John P. Snyder U.S. Geological Survey Professional Paper 1395
     * Table 23, pp. 152-153
     */
    @Test
    public void test_oblique(){
        
        final OrthographicTransform projection = new OrthographicTransform(RADIUS, toRadians(40.), 0.);
        
        obliqueTest(90., 
            new double[]{.0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000, .0000}, 
            new double[]{.7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660, .7660}, projection);
        obliqueTest(80., 
            new double[]{.0000, .0302, .0594, .0868, .1116, .1330, .1504, .1632, .1710, .1736, .1710, .1632, .1504, .1330, .1116, .0868, .0594, .0302, .0000}, 
            new double[]{.6428, .6445, .6495, .6577, .6689, .6827, .6986, .7162, .7350, .7544, .7738, .7926, .8102, .8262, .8399, .8511, .8593, .8643, .8660}, projection);
        obliqueTest(70., 
            new double[]{.0000, .0594, .1170, .1710, .2198, .2620, .2962, .3214, .3368, .3420, .3368, .3214, .2962, .2620, .2198, .1710, .1170, .0594, .0000}, 
            new double[]{.5000, .5033, .5133, .5295, .5514, .5785, .6099, .6447, .6817, .7198, .7580, .7950, .8298, .8612, .8883, .9102, .9264, .9364, .9397}, projection);
        obliqueTest(60., 
            new double[]{.0000, .0868, .1710, .2500, .3214, .3830, .4330, .4698, .4924, .5000, .4924, .4698, .4330, .3830, .3214, .2500, .1710, .0868, .0000}, 
            new double[]{.3420, .3469, .3614, .3851, .4172, .4568, .5027, .5535, .6076, .6634, .7192, .7733, .8241, .8700, .9096, .9417, .9654, .9799, .9848}, projection);
        obliqueTest(50., 
            new double[]{.0000, .1116, .2198, .3214, .4132, .4924, .5567, .6040, .6330, .6428, .6330, .6040, .5567, .4924, .4132, .3214, .2198, .1116, .0000}, 
            new double[]{.1736, .1799, .1986, .2290, .2703, .3212, .3802, .4455, .5151, .5868, .6586, .7281, .7934, .8524, .9033, .9446, .9751, .9937, 1.}, projection);
        obliqueTest(40., 
            new double[]{.0000, .1330, .2620, .3830, .4924, .5868, .6634, .7198, .7544, .7660, .7544, .7198, .6634, .5868}, 
            new double[]{.0000, .0075, .0297, .0660, .1152, .1759, .2462, .3240, .4069, .4924, .5779, .6608, .7386, .8089}, projection);
        obliqueTest(30., 
            new double[]{ .0000, .1504, .2962, .4330, .5567, .6634, .7500, .8138, .8529, .8660, .8529, .8138}, 
            new double[]{-.1736,-.1652,-.1401,-.0991,-.0434, .0252, .1047, .1926, .2864, .3830, .4797, .5734}, projection);
        obliqueTest(20., 
            new double[]{ .0000, .1632, .3214, .4698, .6040, .7198, .8138, .8830, .9254, .9397, .9254}, 
            new double[]{-.3420,-.3328,-.3056,-.2611,-.2007,-.1263,-.0400, .0554, .1571, .2620, .3669}, projection);
        obliqueTest(10., 
            new double[]{ .0000, .1710, .3368, .4924, .6330, .7544, .8529, .9254, .9698, .9848}, 
            new double[]{-.5000,-.4904,-.4618,-.4152,-.3519,-.2739,-.1835,-.0835, .0231, .1330}, projection);
        obliqueTest(0., 
            new double[]{ .0000, .1736, .3420, .5000, .6428, .7660, .8660, .9397, .9848, 1.}, 
            new double[]{-.6428,-.6330,-.6040,-.5567,-.4924,-.4132,-.3214,-.2198,-.1116, .0000}, projection);
        obliqueTest(-10., 
            new double[]{ .0000, .1710, .3368, .4924, .6330, .7544, .8529, .9254, .9698}, 
            new double[]{-.7660,-.7564,-.7279,-.6812,-.6179,-.5399,-.4495,-.3495,-.2429}, projection);
        obliqueTest(-20., 
            new double[]{ .0000, .1632, .3214, .4698, .6040, .7198, .8138, .8830}, 
            new double[]{-.8660,-.8568,-.8296,-.7851,-.7247,-.6503,-.5640,-.4686}, projection);
        obliqueTest(-30., 
            new double[]{ .0000, .1504, .2962, .4330, .5567, .6634, .7500}, 
            new double[]{-.9397,-.9312,-.9061,-.8651,-.8095,-.7408,-.6614}, projection);
        obliqueTest(-40., 
            new double[]{ .0000, .1330, .2620, .3830, .4924}, 
            new double[]{-.9848,-.9773,-.9551,-.9188,-.8696}, projection);
        obliqueTest(-50., 
            new double[]{ .0000}, 
            new double[]{-1.}, projection);
    }   
    
    private void obliqueTest(final double latitude, final double[] expectedXTab, final double[] expectedYTab, final OrthographicTransform projection){
        Assert.assertEquals(expectedYTab.length, expectedXTab.length);
        int longitude=0;
        for(int i=0; i<expectedXTab.length; i++){
            Assert.assertArrayEquals(new double[]{expectedXTab[i], expectedYTab[i]}, projection.transform(new double[]{toRadians(latitude), toRadians(longitude)},null), DELTA);
            longitude+=10;
        }
    }
    
    @Test
    public void test_inverse(){
        final OrthographicTransform projection = new OrthographicTransform(RADIUS, PI/2, 0);
        
        // Nord
        Assert.assertArrayEquals(new double[]{PI/4, PI/4}, projection.inverseTransform(new double[]{0.5, -0.5}), 1e-10);
        Assert.assertArrayEquals(new double[]{PI/4, 3*PI/4}, projection.inverseTransform(new double[]{0.5, 0.5}), 1e-10);
        
        // Sud
        Assert.assertArrayEquals(new double[]{PI/4, -PI/4}, projection.inverseTransform(new double[]{-0.5, -0.5}), 1e-10);
        Assert.assertArrayEquals(new double[]{PI/4, -3*PI/4}, projection.inverseTransform(new double[]{-0.5, 0.5}), 1e-10);
    } 
}
