
package science.unlicense.impl.geo.transform.spherical;

import science.unlicense.impl.geo.transform.spherical.MercatorTransform;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Angles;
import science.unlicense.impl.geo.transform.AbstractTransformTest;

/**
 *
 * @author Johann Sorel
 */
public class MercatorTransformTest extends AbstractTransformTest{

    @Test
    public void transformTest(){

        MercatorTransform trs = new MercatorTransform(Angles.degreeToRadian(40));

        double[] coord = new double[]{Angles.degreeToRadian(10),Angles.degreeToRadian(10)};
        coord = trs.transform(coord,null);
        coord = trs.inverseTransform(coord);
        Assert.assertEquals(10.0,Angles.radianToDegree(coord[0]),DELTA);
        Assert.assertEquals(10.0,Angles.radianToDegree(coord[1]),DELTA);

    }
}
