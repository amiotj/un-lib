

package science.unlicense.api.parser;

import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.api.parser.NFARuleState;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.NFATokenState;
import science.unlicense.api.lexer.TokenGroup;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.regex.NFAState;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.RegexTokenType;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.regex.NFAState.Fork;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ParserTest {
    
    @Test
    public void singleRuleTest() throws IOException{
        final Chars tokenName = new Chars("tokentype1");
        final TokenType tokenType = new RegexTokenType(tokenName, new Chars(".*"));
        final TokenGroup group = new TokenGroup(new Chars("DEFAULT"));
        group.add(tokenName, tokenType);
        final Rule rule = new Rule(new Chars("rule1"),group).setSubState(tokenType);
        
        //check sub states
        final NFAState tempState = rule.getState();
        Assert.assertTrue(tempState instanceof NFATokenState);
        final NFATokenState tokenState = (NFATokenState) tempState;
        Assert.assertEquals(tokenType, tokenState.getTokenType());
        
        
        //check rule states
        final NFARuleState start = new NFARuleState(rule);
        Assert.assertEquals(rule, start.getRule());
        Assert.assertTrue(start.getNextState() instanceof NFATokenState);
        final NFATokenState tokState = (NFATokenState)start.getNextState();
        Assert.assertEquals(tokenType, tokState.getTokenType());
        Assert.assertNull(tokState.getNextState());
        
    }
    
    @Test
    public void parseTwoRule() throws IOException{
        //read the grammar
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/twoRule.gr")));
        final OrderedHashDictionary tokens = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokens, rules);
        Assert.assertEquals(3, rules.getSize());
        
        //rule 1 ---------------------------------------------------------------
        final Rule rule1 = (Rule) rules.getValue(new Chars("rula"));     
        final NFAState rule1State = rule1.getState();
        Assert.assertTrue(rule1State instanceof NFATokenState);
        final NFATokenState token1State = (NFATokenState) rule1State;
        Assert.assertEquals(new Chars("TOKA"), token1State.getTokenType().getName());
        
        //rule 2 ---------------------------------------------------------------
        final Rule rule2 = (Rule) rules.getValue(new Chars("rulb"));    
        final NFAState rule2State = rule2.getState();
        Assert.assertTrue(rule2State instanceof NFATokenState);
        final NFATokenState token2State = (NFATokenState) rule2State;
        Assert.assertEquals(new Chars("TOKB"), token2State.getTokenType().getName());
        
        //rule 3 ---------------------------------------------------------------
        final Rule rule3 = (Rule) rules.getValue(new Chars("rulc"));     
        final NFAState rule3State = rule3.getState();
        Assert.assertTrue(rule3State instanceof NFAState.Fork);
        final NFAState.Fork fork = (NFAState.Fork) rule3State;
        Assert.assertTrue(fork.getNext1State() instanceof NFARuleState);
        Assert.assertTrue(fork.getNext2State() instanceof NFARuleState);
        final NFARuleState fork1 = (NFARuleState) fork.getNext1State();
        final NFARuleState fork2 = (NFARuleState) fork.getNext2State();
        Assert.assertEquals(rule1,fork1.getRule());
        Assert.assertEquals(rule2,fork2.getRule());
        
    }
    
    @Test
    public void parseTest() throws IOException{
        //read the grammar
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/sample.gr")));
        final OrderedHashDictionary tokenGroups = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokenGroups, rules);
        // grammar parser is already tested        
        final Rule rule = (Rule) rules.getValue(new Chars("program"));
                
        //prepare lexer
        final Lexer lexer = new Lexer();
        lexer.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/program.ex")));
        
        //prepare parser
        final Parser parser = new Parser(rule);
        parser.setInput(lexer);
        final SyntaxNode node = parser.parse();
        
        Assert.assertEquals(
            "program\n" +
            "├─ import\n" +
            "│  ├─ IMPORT[0:0,0:6]='import'\n" +
            "│  ├─ WS[0:6,0:7]=' '\n" +
            "│  ├─ WORD[0:7,0:16]='unlicense'\n" +
            "│  ├─ WS[0:16,0:17]=' '\n" +
            "│  └─ COM[0:17,0:18]=';'\n" +
            "├─ WS[0:18,1:1]='\\n '\n" +
            "├─ import\n" +
            "│  ├─ IMPORT[1:1,1:7]='import'\n" +
            "│  ├─ WS[1:7,1:8]=' '\n" +
            "│  ├─ WORD[1:8,1:15]='project'\n" +
            "│  └─ COM[1:15,1:16]=';'\n" +
            "├─ WS[1:16,2:0]='\\n'\n" +
            "├─ import\n" +
            "│  ├─ IMPORT[2:0,2:6]='import'\n" +
            "│  ├─ WS[2:6,2:9]='   '\n" +
            "│  ├─ WORD[2:9,2:14]='other'\n" +
            "│  ├─ WS[2:14,2:17]='   '\n" +
            "│  └─ COM[2:17,2:18]=';'\n" +
            "├─ WS[2:18,4:0]='\\n\\n'\n" +
            "├─ statement\n" +
            "│  ├─ NUMBER[4:0,4:4]='3.14'\n" +
            "│  └─ COM[4:4,4:5]=';'\n" +
            "├─ WS[4:5,5:4]='\\n    '\n" +
            "├─ statement\n" +
            "│  ├─ NUMBER[5:4,5:7]='+12'\n" +
            "│  ├─ WS[5:7,5:9]='  '\n" +
            "│  └─ COM[5:9,5:10]=';'\n" +
            "├─ WS[5:10,6:0]='\\n'\n" +
            "├─ statement\n" +
            "│  ├─ NUMBER[6:0,6:9]='-789.5e+6'\n" +
            "│  ├─ WS[6:9,6:10]=' '\n" +
            "│  └─ COM[6:10,6:11]=';'\n" +
            "└─ WS[6:11,7:0]='\\n'"
            , node.toString());
        
    }

    @Test
    public void parseTwoGroup() throws IOException{
        //read the grammar : test is made in GrammarReaderTest class
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/twoTokenGroup.gr")));
        final OrderedHashDictionary tokenGroups = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokenGroups, rules);

        //parse
        final Rule progRule = (Rule) rules.getValue(new Chars("prog"));

        final Chars text = new Chars("?a¿c?b¿d");

        final Parser parser = new Parser(tokenGroups,progRule,null);
        parser.setInput(text.toBytes());

        final SyntaxNode ast = parser.parse();

        Assert.assertEquals(
        "prog\n" +
        "├─ rule1\n" +
        "│  ├─ TYPE1[0:0,0:1]='?'\n" +
        "│  └─ group1\n" +
        "│     └─ TOKA[0:1,0:2]='a'\n" +
        "├─ rule2\n" +
        "│  ├─ TYPE2[0:2,0:3]='¿'\n" +
        "│  └─ group2\n" +
        "│     └─ TOKC[0:3,0:4]='c'\n" +
        "├─ rule1\n" +
        "│  ├─ TYPE1[0:4,0:5]='?'\n" +
        "│  └─ group1\n" +
        "│     └─ TOKB[0:5,0:6]='b'\n" +
        "└─ rule2\n" +
        "   ├─ TYPE2[0:6,0:7]='¿'\n" +
        "   └─ group2\n" +
        "      └─ TOKD[0:7,0:8]='d'"
        , ast.toString());
    }

}
