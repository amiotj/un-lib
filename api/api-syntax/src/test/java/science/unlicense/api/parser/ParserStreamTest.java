

package science.unlicense.api.parser;

import science.unlicense.api.parser.ParserStream;
import science.unlicense.api.parser.NFARuleState;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.NFATokenState;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.regex.NFAStep;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ParserStreamTest {
            
    private static final Chars RULE_PROGRAM = new Chars("program");
    private static final Chars RULE_IMPORT = new Chars("import");
    private static final Chars RULE_STATEMENT = new Chars("statement");
    
    private static final Chars TOKEN_WS = new Chars("WS");
    private static final Chars TOKEN_WORD = new Chars("WORD");
    private static final Chars TOKEN_COM = new Chars("COM");
    private static final Chars TOKEN_IMPORT = new Chars("IMPORT");
    private static final Chars TOKEN_NUMBER = new Chars("NUMBER");
    
    @Test
    public void parseTest() throws IOException{
        //read the grammar
        final UNGrammarReader reader = new UNGrammarReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/sample.gr")));
        final OrderedHashDictionary tokenGroups = new OrderedHashDictionary();
        final OrderedHashDictionary rules = new OrderedHashDictionary();
        reader.read(tokenGroups, rules);
        // grammar parser is already tested        
        final Rule rule = (Rule) rules.getValue(new Chars("program"));
                
        //prepare lexer
        final Lexer lexer = new Lexer();
        lexer.setInput(Paths.resolve(new Chars("mod:/un/impl/grammar/io/program.ex")));
        
        //prepare parser
        final ParserStream stream = new ParserStream(lexer,rule);
        
        NFAStep step = null;
        
        nextIsRuleStart(stream, RULE_PROGRAM);
        
        nextIsRuleStart(stream, RULE_IMPORT);
        nextIsToken(stream, TOKEN_IMPORT, new Chars("import"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_WORD, new Chars("unlicense"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_COM, new Chars(";"));
        nextIsRuleEnd(stream, RULE_IMPORT);
        
        nextIsWS(stream);
        
        nextIsRuleStart(stream, RULE_IMPORT);
        nextIsToken(stream, TOKEN_IMPORT, new Chars("import"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_WORD, new Chars("project"));
        nextIsToken(stream, TOKEN_COM, new Chars(";"));
        nextIsRuleEnd(stream, RULE_IMPORT);
        
        nextIsWS(stream);
        
        nextIsRuleStart(stream, RULE_IMPORT);
        nextIsToken(stream, TOKEN_IMPORT, new Chars("import"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_WORD, new Chars("other"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_COM, new Chars(";"));
        nextIsRuleEnd(stream, RULE_IMPORT);
        
        nextIsWS(stream);
        
        nextIsRuleStart(stream, RULE_STATEMENT);
        nextIsToken(stream, TOKEN_NUMBER, new Chars("3.14"));
        nextIsToken(stream, TOKEN_COM, new Chars(";"));
        nextIsRuleEnd(stream, RULE_STATEMENT);
        
        nextIsWS(stream);
        
        nextIsRuleStart(stream, RULE_STATEMENT);
        nextIsToken(stream, TOKEN_NUMBER, new Chars("+12"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_COM, new Chars(";"));
        nextIsRuleEnd(stream, RULE_STATEMENT);
        
        nextIsWS(stream);
        
        nextIsRuleStart(stream, RULE_STATEMENT);
        nextIsToken(stream, TOKEN_NUMBER, new Chars("-789.5e+6"));
        nextIsWS(stream);
        nextIsToken(stream, TOKEN_COM, new Chars(";"));
        nextIsRuleEnd(stream, RULE_STATEMENT);
        
        //TODO something odd here
//        nextIsWS(stream);
//        
//        nextIsRuleEnd(stream, RULE_PROGRAM);
//        
//        step = stream.next();
//        Assert.assertNull(step);
        
//"├─ WS='\\n\\n'\n" +
//"├─ statement\n" +
//"│  ├─ NUMBER='3.14'\n" +
//"│  └─ COM=';'\n" +
//"├─ WS='\\n    '\n" +
//"├─ statement\n" +
//"│  ├─ NUMBER='+12'\n" +
//"│  ├─ WS='  '\n" +
//"│  └─ COM=';'\n" +
//"├─ WS='\\n'\n" +
//"├─ statement\n" +
//"│  ├─ NUMBER='-789.5e+6'\n" +
//"│  ├─ WS=' '\n" +
//"│  └─ COM=';'\n" +
//"└─ WS='\\n'", node.toString());
        
    }
    
    private static void nextIsRuleStart(ParserStream stream, Chars type) throws IOException{
        NFAStep step = stream.next();
        Assert.assertTrue(NFARuleState.isRuleStart(step.state, type));
    }
    
    private static void nextIsRuleEnd(ParserStream stream, Chars type) throws IOException{
        NFAStep step = stream.next();
        Assert.assertTrue(NFARuleState.isRuleEnd(step.state, type));
    }
    
    private static void nextIsWS(ParserStream stream) throws IOException{
        NFAStep step = stream.next();
        Assert.assertTrue(NFATokenState.isToken(step.state, TOKEN_WS));
    }
    
    private static void nextIsToken(ParserStream stream, Chars type, Chars text) throws IOException{
        NFAStep step = stream.next();
        Assert.assertTrue(NFATokenState.isToken(step.state, type));
        Assert.assertEquals(text, ((Token)step.value).value);
    }
    
}
