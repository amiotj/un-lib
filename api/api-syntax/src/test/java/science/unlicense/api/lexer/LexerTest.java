

package science.unlicense.api.lexer;

import science.unlicense.api.lexer.RegexTokenType;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.lexer.Lexer;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class LexerTest {

    @Test
    public void wordTest() throws IOException{

        final Chars text = new Chars("Hello public domain lexer");
        final ArrayInputStream in = new ArrayInputStream(text.toBytes());

        final Lexer lexer = new Lexer();
        
        Token token;
        final TokenType wordType = RegexTokenType.word();
        final TokenType spaceType = RegexTokenType.space();
        lexer.setInput(in);
        lexer.getTokenGroup().add(wordType);
        lexer.getTokenGroup().add(spaceType);
        lexer.init();

        token = lexer.next();
        Assert.assertEquals(wordType, token.type);
        Assert.assertEquals(new Chars("Hello"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" "), token.value);

        token = lexer.next();
        Assert.assertEquals(wordType, token.type);
        Assert.assertEquals(new Chars("public"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" "), token.value);

        token = lexer.next();
        Assert.assertEquals(wordType, token.type);
        Assert.assertEquals(new Chars("domain"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" "), token.value);

        token = lexer.next();
        Assert.assertEquals(wordType, token.type);
        Assert.assertEquals(new Chars("lexer"), token.value);

        token = lexer.next();
        Assert.assertNull(token);

    }

    @Test
    public void baseTokenTest() throws IOException{

        final Chars text = new Chars("14.56 \t \r \n kEy KEY value ");
        final ArrayInputStream in = new ArrayInputStream(text.toBytes());

        final Lexer lexer = new Lexer();
        Token token;
        final TokenType keywordType = RegexTokenType.keyword(new Chars("KEY"), new Chars("KEY"));
        final TokenType numberType = RegexTokenType.decimal();
        final TokenType wordType = RegexTokenType.word();
        final TokenType spaceType = RegexTokenType.space();
        lexer.setInput(in);
        lexer.getTokenGroup().add(keywordType);
        lexer.getTokenGroup().add(numberType);
        lexer.getTokenGroup().add(wordType);
        lexer.getTokenGroup().add(spaceType);
        lexer.init();

        token = lexer.next();
        Assert.assertEquals(numberType, token.type);
        Assert.assertEquals(new Chars("14.56"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" \t \r \n "), token.value);

        token = lexer.next();
        Assert.assertEquals(wordType, token.type);
        Assert.assertEquals(new Chars("kEy"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" "), token.value);

        token = lexer.next();
        Assert.assertEquals(keywordType, token.type);
        Assert.assertEquals(new Chars("KEY"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" "), token.value);

        token = lexer.next();
        Assert.assertEquals(wordType, token.type);
        Assert.assertEquals(new Chars("value"), token.value);

        token = lexer.next();
        Assert.assertEquals(spaceType, token.type);
        Assert.assertEquals(new Chars(" "), token.value);

        token = lexer.next();
        Assert.assertNull(token);

    }

    /**
     * Check behavior when 2 token type match.
     * TokenType priority order must be preserved.
     */
    @Test
    public void twoTokenTest() throws IOException{
        
        final Chars text = new Chars("template");

        Lexer lexer = new Lexer();
        Token token;
        final TokenType type1 = RegexTokenType.keyword(new Chars("test1"),new Chars("template"));
        final TokenType type2 = RegexTokenType.keyword(new Chars("test2"),new Chars("[a-z]*"));
        
        //test token type 1 before 2
        lexer.setInput(new ArrayInputStream(text.toBytes()));
        lexer.getTokenGroup().removeAll();
        lexer.getTokenGroup().add(type1);
        lexer.getTokenGroup().add(type2);
        lexer.init();        
        
        token = lexer.next();
        Assert.assertEquals(type1, token.type);
        Assert.assertEquals(new Chars("template"), token.value);        
        token = lexer.next();
        Assert.assertNull(token);
        
        //test token type 2 before 1
        lexer = new Lexer();
        lexer.setInput(new ArrayInputStream(text.toBytes()));
        lexer.getTokenGroup().removeAll();
        lexer.getTokenGroup().add(type2);
        lexer.getTokenGroup().add(type1);
        lexer.init();         
        token = lexer.next();
        Assert.assertEquals(type2, token.type);
        Assert.assertEquals(new Chars("template"), token.value);        
        token = lexer.next();
        Assert.assertNull(token);
        
        
    }
        
}
