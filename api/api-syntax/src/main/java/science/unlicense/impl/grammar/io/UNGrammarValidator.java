
package science.unlicense.impl.grammar.io;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.regex.NFAState;

/**
 * Helper class to find and fix possible incoherencies in grammars.
 * TODO 
 * 
 * @author Johann Sorel
 */
public final class UNGrammarValidator {
    
    private Rule rule;
    
    private UNGrammarValidator(Rule rule){
        this.rule = rule;
    }
    
    private void validate(){
        
    }
    
    /**
     * produce a nice graph like char view.
     * 
     * @param rule
     * @return 
     */
    public static Chars toChars(Rule rule){
        final Dictionary states = new HashDictionary(Hasher.IDENTITY);
        
        final NFAState state = rule.getState();

        if(state instanceof NFAState.Fork){
            final NFAState.Fork fork = (NFAState.Fork) state;
            final NFAState next1 = fork.getNext1State();
            final NFAState next2 = fork.getNext2State();

            if(states.getKeys().contains(next1)){
                
            }


        }else if(state instanceof NFAState.Forward){
            
        }


        return null;
    }

    private static final class Row {

        

    }

}
