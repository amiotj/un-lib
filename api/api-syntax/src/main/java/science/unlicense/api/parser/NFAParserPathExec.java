
package science.unlicense.api.parser;

import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.regex.NFAPathExec;
import science.unlicense.api.regex.NFAPath;
import science.unlicense.api.regex.NFAState;

/**
 *
 * @author Johann Sorel
 */
public class NFAParserPathExec extends NFAPathExec {

    private final Predicate exclude;
    
    public NFAParserPathExec(NFAState state, Predicate exclude) {
        super(state);
        this.exclude = exclude;
    }

    protected NFAPath createPath(NFAState state) {
        return new NFAParserPath(state,exclude);
    }

}
