
package science.unlicense.api.parser;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.regex.NFAState;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public class NFATokenState extends NFAState.AbstractForward implements NFAState.Evaluator{

    private final TokenType tokenType;

    public NFATokenState(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public boolean evaluate(Object candidate) {
        return ((Token)candidate).type == tokenType;
    }

    @Override
    public Chars toChars() {
        return CObjects.toChars(tokenType);
    }
        
    public static boolean isToken(NFAState state, Chars name){
        return state instanceof NFATokenState && 
                name.equals( ((NFATokenState)state).getTokenType().getName());
    }
    
}
