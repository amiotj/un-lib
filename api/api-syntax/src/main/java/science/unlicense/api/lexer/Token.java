

package science.unlicense.api.lexer;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Token extends CObject{

    public TokenType type;

    public int lineStart;
    public int lineEnd;

    public int lineCharStart;
    public int lineCharEnd;

    public int charStart;
    public int charEnd;
    
    public Chars value;

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(type.getName());
        cb.append("["+lineStart+":"+lineCharStart+","+lineEnd+":"+lineCharEnd+"]");
        cb.append("='");
        cb.append(value.replaceAll(new Chars("\n"), new Chars("\\n")));
        cb.append('\'');
        
        return cb.toChars();
    }

}
