
package science.unlicense.api.parser;

import science.unlicense.api.lexer.TokenGroup;
import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.regex.NFAState;
import science.unlicense.api.regex.NFATemp;
import science.unlicense.api.lexer.TokenType;

/**
 *
 * @author Johann Sorel
 */
public final class Rule extends CObject{
    
    private final Chars name;
    private TokenGroup tokens;
    private NFATemp subStates;

    public Rule(Chars name, TokenGroup tokens) {
        CObjects.ensureNotNull(name);
        this.name = name;
        this.tokens = tokens;
    }
        
    public Rule setSubState(TokenType tt){
        final NFATokenState ts = new NFATokenState(tt);
        return setSubState(new NFATemp(ts, ts.getNextRef()));
    }
    
    public Rule setSubState(NFATemp temp){
        this.subStates = temp;
        return this;
    }
    
    public Chars getName() {
        return name;
    }

    public TokenGroup getTokens() {
        return tokens;
    }

    public void setTokens(TokenGroup tokens) {
        this.tokens = tokens;
    }
    
    public NFAState getState() {
        return subStates.state;
    }

    public Chars toChars() {
        return name;
    }
    
    /**
     * Unroll this rule.
     * This solves definitions like :
     * B = (B + B) | A ;
     * solving it to :
     * B = A (+ B)?
     * 
     * TODO : is this a right solution ?
     * How do we rebuild the AST as expected after ?
     * 
     */
    public void unroll(){
        if(subStates.state instanceof NFAState.Fork){
            final NFAState.Fork fork = (NFAState.Fork) subStates.state;
            final NFAState state1 = fork.getNext1State();
            final NFAState state2 = fork.getNext1State();
        }
    }
    
}
