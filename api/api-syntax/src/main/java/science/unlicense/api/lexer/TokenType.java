

package science.unlicense.api.lexer;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.regex.NFAState;

/**
 * Token type with it's defined executor.
 * 
 * @author Johann Sorel
 */
public interface TokenType {

    /**
     * Token tyoe which indicate the stream doesn't not match any token.
     */
    public static final TokenType NO_MATCH = new TokenType() {
        public Chars getName() {
            return new Chars("NO MATCH");
        }
        public NFAState createNFAState() {
            throw new UnimplementedException("Not supported.");
        }
    };
    
    /**
     * Token type name.
     * @return name
     */
    Chars getName();

    /**
     * 
     * @return NFAState never null.
     */
    NFAState createNFAState();
    
}
