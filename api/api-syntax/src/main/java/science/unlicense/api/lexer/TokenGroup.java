
package science.unlicense.api.lexer;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 * A token group is a set is a map of tokens.
 * Rules may define custom tokens, this allows to modify parsing on the fly.
 * This is used to parse aberrant syntaxes which to not follow a strict token set.
 * 
 * @author Johann Sorel
 */
public class TokenGroup extends OrderedHashDictionary{
    
    private Chars name;

    public TokenGroup(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public void add(TokenType tokenType){
        if(containsKey(tokenType.getName())) {
            throw new InvalidArgumentException("Token for name "+tokenType.getName()+" already exist.");
        }
        add(tokenType.getName(),tokenType);
    }

    public Chars toChars() {
        return name;
    }

}
