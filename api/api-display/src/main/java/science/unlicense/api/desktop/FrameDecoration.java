
package science.unlicense.api.desktop;

import science.unlicense.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public interface FrameDecoration {
    
    /**
     * Frame margins.
     * 
     * @return frame border margins
     */
    Margin getMargin();
        
    /**
     * Get frame having this decoration.
     * 
     * @return Frame, can be null.
     */
    Frame getFrame();
    
    /**
     * Set frame using this decoration.
     * The frame itself should call this method.
     * 
     * @param frame 
     */
    void setFrame(Frame frame);
    
}
