
package science.unlicense.api.painter2d;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.api.image.Image;
import science.unlicense.api.task.Task;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.scenegraph.s2d.Graphic2DFactory;
import science.unlicense.impl.math.Affine2;

/**
 * A Painter provides convenient methods to render text and geometries.
 * The general output is an image but implementations may differ and provide
 * other kind of results, like PDF, PS, SVG or OpenGL rendering.
 *
 * @author Johann Sorel
 */
public interface Painter2D {

    /**
     * General transform of the painter.
     * Applied to all drawing operations.
     * @return Matrix4 copy of the current transform
     */
    Affine2 getTransform();
    
    void setTransform(Affine2 trs);

    /**
     * Access informations on the available fonts.
     *
     * @return FontStore, never null.
     */
    FontStore getFontStore();

    /**
     * Set clipping geometry,
     * Any fragment outside of the clip with not be rendered.
     * The clip geometry is not transformed by the painter transform affine.
     *
     * @param geom Geometry2D , null to remove clip
     */
    void setClip(Geometry2D geom);

    /**
     * Get the current clip
     * @return Geometry2D, can be null
     */
    Geometry2D getClip();

    /**
     * Get color blending method.
     * 
     * @return AlphaBlending, never null
     */
    AlphaBlending getAlphaBlending();

    /**
     * Set color blending method.
     * 
     * @param alphaBlending can not be null
     */
    void setAlphaBlending(AlphaBlending alphaBlending);
    
    /**
     * Set paint used for painting the interior or outline of geometries
     * and texts.
     *
     * @param paint, can be null
     */
    void setPaint(Paint paint);

    /**
     * Get current paint.
     *
     * @return Paint or null
     */
    Paint getPaint();

    /**
     * Set brush used for painting outline of geometries.
     *
     * @param brush, can be null
     */
    void setBrush(Brush brush);

    /**
     * Get current brush.
     *
     * @return Brush or null
     */
    Brush getBrush();

    /**
     * Set font used for text rendering.
     *
     * @param font, can be null
     */
    void setFont(Font font);

    /**
     * Get current font.
     *
     * @return Font or null
     */
    Font getFont();

    /**
     * The operation of painting the interior of the geometry.
     *
     * @param geom Geometry2D
     */
    void fill(Geometry2D geom);

    /**
     * The operation of painting the interior of a text glyphs.
     *
     * @param text , text to render
     * @param x, x coordinate of the first letter baseline
     * @param y, y coordinate of the first letter baseline
     */
    void fill(CharArray text, float x, float y);

    /**
     * The operation of painting the outline of the geometry.
     *
     * @param geom Geometry2D
     */
    void stroke(Geometry2D geom);

    /**
     * The operation of painting the outline of a text glyphs.
     *
     * @param text , text to render
     * @param x, x coordinate of the first letter baseline
     * @param y, y coordinate of the first letter baseline
     */
    void stroke(CharArray text, float x, float y);

    /**
     * Paint given image applying given transform operation.
     *
     * @param image
     * @param transform
     */
    void paint(Image image, Affine2 transform);

    /**
     * Ask the painter to execute the given task on an area.
     * The task is expected to be configured and should produce an image result.
     * 
     * @param task to execute
     * @param area on which the task should be executed
     */
    void execute(Task task, Geometry2D area);

    /**
     * Save current rendering parameters.
     *
     * @return state, never null
     */
    PainterState saveState();

    /**
     * Restore to given rendering parameters.
     *
     * @param state, not null
     */
    void restoreState(PainterState state);

    /**
     * Create derivate painter applying the given transform.
     * Aside from the transform all other informations are shared.
     * 
     * @param transform affine not null
     * @return derivate Painter2D
     */
    Painter2D derivate(Affine2 transform);
    
    /**
     * Render a scene node.
     * Scene nodes which have been created by the graphic factory
     * will be more efficient.
     * Other kind of nodes might be ignored based on the implementation.
     * 
     * @param node 
     */
    void render(SceneNode node);
    
    /**
     * Get optimized graphic node factory for this painter.
     * 
     * @return factory
     */
    Graphic2DFactory getGraphicFactory();
    
    /**
     * Process any remaining instructions.
     * Implementations of Painters may not apply stroke or fill operation
     * right away.
     * To ensure everything is processed, use this method.
     */
    void flush();

    /**
     * Release all resources used.
     * The painter should not be used after this call.
     */
    void dispose();

}
