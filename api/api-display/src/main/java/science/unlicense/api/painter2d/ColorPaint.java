
package science.unlicense.api.painter2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.impl.math.Affine2;

/**
 * Plain color paint.
 *
 * @author Johann Sorel
 */
public class ColorPaint implements Paint{

    private final Color color;
    private final boolean isOpaque;

    public ColorPaint(Color color) {
        CObjects.ensureNotNull(color);
        this.color = color;
        isOpaque = color.getAlpha() == 1f;
    }

    public Color getColor() {
        return color;
    }

    public void fill(Image image, Image flagImage, BBox bbox, Affine2 trs, AlphaBlending blending) {
        //base image
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        final TupleIterator fite = flagImage.getRawModel().asTupleBuffer(flagImage).createIterator(bbox);
        final float[] sampleStore = new float[4];
        final float[] buffer = new float[4];

        for(byte[] flagSample = (byte[])fite.next();
            flagSample!=null;
            flagSample=(byte[])fite.next()){

            if(flagSample[0] == -1 && isOpaque){
                //opaque replacement
                cm.setColor(fite.getCoordinate(), color);
            }else if(flagSample[0]!=0){
                //alpha blending
                final float alpha = (flagSample[0] & 0xff) / 255f;
                final float[] srcrgba = new float[]{
                    color.getRed(),
                    color.getGreen(),
                    color.getBlue(),
                    color.getAlpha()*alpha
                };
                final int[] coordinate = fite.getCoordinate();
                cm.getRGBA(coordinate, sampleStore);
                blending.blend(srcrgba, sampleStore,buffer);
                cm.setRGBA(coordinate, buffer);
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ColorPaint other = (ColorPaint) obj;
        if (this.color != other.color && (this.color == null || !this.color.equals(other.color))) {
            return false;
        }
        return true;
    }
    
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.color != null ? this.color.hashCode() : 0);
        return hash;
    }

}
