
package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.Property;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Angles;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class CircularLayout extends AbstractLayout {

    public static final Chars PROPERTY_START_ANGLE = new Chars("StartAngle");
    public static final Chars PROPERTY_END_ANGLE = new Chars("EndAngle");
    public static final Chars PROPERTY_RADIUS = new Chars("Radius");
    
    private Expression centerX = null;
    private Expression centerY = null;
    private double radius = Double.NaN;
    private double startAngle = Double.NaN;
    private double endAngle = Double.NaN;

    public CircularLayout() {
        super(null);
    }

    public CircularLayout(Expression centerX, Expression centerY, double radius, double startAngle, double endAngle){
        super(null);
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.startAngle = startAngle;
        this.endAngle = endAngle;
    }
    
    /**
     * Get layout circle center X.
     * Null for automatic.
     * 
     * @return layout circle center or null
     */
    public Expression getCenterX() {
        return centerX;
    }

    public void setCenterX(Expression centerX) {
        this.centerX = centerX;
        setDirty();
    }
    
    /**
     * Get layout circle center Y.
     * Null for automatic.
     * 
     * @return layout circle center or null
     */
    public Expression getCenterY() {
        return centerY;
    }

    public void setCenterY(Expression centerY) {
        this.centerY = centerY;
        setDirty();
    }

    /**
     * Get layout circle radius.
     * NaN for automatic.
     * 
     * @return layout circle center or NaN
     */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        setDirty();
    }

    public Property varRadius(){
        return getProperty(PROPERTY_RADIUS);
    }
    
    /**
     * 
     * @return start angle in radians
     */
    public double getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(double startAngle) {
        this.startAngle = startAngle;
        setDirty();
    }

    public Property varStartAngle(){
        return getProperty(PROPERTY_START_ANGLE);
    }
    
    /**
     * 
     * @return end angle in radians
     */
    public double getEndAngle() {
        return endAngle;
    }

    public void setEndAngle(double endAngle) {
        this.endAngle = endAngle;
        setDirty();
    }
    
    public Property varEndAngle(){
        return getProperty(PROPERTY_END_ANGLE);
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_RESERVE_SPACE.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {

        //find elements biggest size and radius
        final double[] res = calculateRadiusAndSize();
        final double radius = res[0];
        final double sizeX = res[1];
        final double sizeY = res[2];

        extents.bestX = radius*2 + sizeX;
        extents.bestY = radius*2 + sizeY;
        extents.minX = extents.bestX;
        extents.minX = extents.bestY;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
        
    }

    public void update() {
        final Positionable[] nodes = getPositionableArray();
        
        //find circle center
        final Vector center = calculateCenter();
        
        //find start and end angles
        final double startADegree = Double.isNaN(startAngle) ? 0.0 : startAngle;
        final double endADegree = Double.isNaN(endAngle) ? 360.0+startADegree : endAngle;
        final double startA = Angles.degreeToRadian(startADegree);
        final double endA = Angles.degreeToRadian(endADegree);
        
        
        //find elements biggest size and radius
        final double[] res = calculateRadiusAndSize();
        final double radius = res[0];
                
        //calculate angle between each element
        final double stepA = (endA-startA) / nodes.length;
        
        //place each elements
        final Extents buffer = new Extents();
        for(int i=0;i<nodes.length;i++){
            final Positionable w = nodes[i];
            w.getExtents(buffer,null);
            final Extent ext = buffer.getBest(null);
            final Vector pos = new Vector(
                    radius * Math.cos(startA+stepA*i),
                    radius * Math.sin(startA+stepA*i));
            pos.localAdd(center);
            
            w.setEffectiveExtent(ext);
            w.getNodeTransform().setToTranslation(pos.getValues());
        }
        
    }

    protected Vector calculateCenter(){
        
        //find circle center
        final BBox inner = getView();
        final Vector center = new Vector(inner.getCentroid().getCoordinate());
        
        if(centerX!=null){
            center.setX(((Number)centerX.evaluate(this)).doubleValue());
        }
        if(centerY!=null){
            center.setY(((Number)centerY.evaluate(this)).doubleValue());
        }
        return center;
    }
    
    protected double[] calculateRadiusAndSize(){
        final Positionable[] nodes = getPositionableArray();
        
        //find elements biggest size
        final Extents buffer = new Extents();
        final Extent bigExt = new Extent.Double(0, 0);
        for(int i=0;i<nodes.length;i++){
            nodes[i].getExtents(buffer,null);
            bigExt.expand(buffer.getBest(null));
        }
        final double size = Angles.hypot(bigExt.get(0), bigExt.get(1));
        
        //calculate radius
        final double radius = Double.isNaN(this.radius) ? ((size*nodes.length)/Maths.TWO_PI) : this.radius;
        
        return new double[]{radius,bigExt.get(0),bigExt.get(1)};
    }
    
}
