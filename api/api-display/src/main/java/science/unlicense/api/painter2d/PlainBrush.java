
package science.unlicense.api.painter2d;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.PathStep2D;
import science.unlicense.impl.geometry.path.FlattenPathIterator;
import science.unlicense.impl.geometry.s2d.MultiPolygon;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.image.Image;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.Geometries;

/**
 * A plain brush is the definition of a standard brush as defined in SVG, CSS,
 * Postscript or PDF.
 * 
 * @author Johann Sorel
 */
public class PlainBrush implements Brush {

    private static final byte[] OPAQUE = new byte[]{-1};
    
    public static final int LINECAP_BUTT = 1;
    public static final int LINECAP_ROUND = 2;
    public static final int LINECAP_SQUARE = 3;
    
    public static final int LINEJOIN_MITER = 4;
    public static final int LINEJOIN_ROUND = 5;
    public static final int LINEJOIN_BEVEL = 6;
    
    private final float width;
    private final float offset;
    private final int linecap;
    private final int linejoin;
    private final float miterLimit;
    private final float[] dasharray;

    public PlainBrush(float width, int linecap) {
        this(width,linecap,LINEJOIN_BEVEL,0,0,null);
    }
    
    public PlainBrush(float width, int linecap, int linejoin, float miterLimit, float offset, float[] dasharray) {
        this.width = width;
        this.linecap = linecap;
        this.linejoin = linejoin;
        this.miterLimit = miterLimit;
        this.offset = offset;
        this.dasharray = dasharray;
    }

    public float getWidth() {
        return width;
    }

    public int getLineCap() {
        return linecap;
    }

    public int getLineJoin() {
        return linejoin;
    }

    public float getMiterLimit() {
        return miterLimit;
    }

    public float getOffset() {
        return offset;
    }

    public float[] getDashes() {
        return dasharray;
    }
    
    public double getMaxWidth() {
        return width;
    }
    
    /**
     * TODO uncomplete.
     * 
     * @param geom
     * @param resolution
     * @return 
     */
    public Geometry2D outline(Geometry2D geom, double[] resolution) {
        final PathIterator pathIterator = new FlattenPathIterator(geom.createPathIterator(), resolution);
        
        PathStep2D currentStep=null;
        PathStep2D previousStep=null;
        final Tuple segmentStart = new DefaultTuple(2);
        final Tuple segmentEnd = new DefaultTuple(2);
        final Tuple pathStart = new DefaultTuple(2);
        
        final Sequence outlines = new ArraySequence();
        final Sequence points = new ArraySequence();
                
        final double halfwidth = width/2;
        while(pathIterator.next()){
            currentStep = new PathStep2D(pathIterator);
            final int stepType = currentStep.getType();
            
            if(stepType == PathIterator.TYPE_MOVE_TO){
                if(previousStep!=null && previousStep.getType() != PathIterator.TYPE_MOVE_TO){
                    //finish previous outline
                    outlines.add(toPath(points));
                    points.removeAll();
                }
            }else if(stepType == PathIterator.TYPE_LINE_TO || stepType == PathIterator.TYPE_CLOSE){
                
                final boolean buildFirst = (previousStep!=null && previousStep.getType() == PathIterator.TYPE_MOVE_TO);
                double minx;
                double maxx;
                double miny;
                double maxy;
                if(segmentStart.getY() < segmentEnd.getY()){
                    miny = segmentStart.getY();
                    maxy = segmentEnd.getY();
                }else{
                    miny = segmentEnd.getY();
                    maxy = segmentStart.getY();
                }
                if(segmentStart.getX() < segmentEnd.getX()){
                    minx = segmentStart.getX();
                    maxx = segmentEnd.getX();
                }else{
                    minx = segmentEnd.getX();
                    maxx = segmentStart.getX();
                }
                
                if(minx == maxx){
                    //vertical line
                    if(buildFirst){
                        points.add(new DefaultTuple(minx, miny-halfwidth));
                        points.add(new DefaultTuple(minx, miny+halfwidth));
                    }
                    points.add(0,new DefaultTuple(minx, maxy-halfwidth));
                    points.add(new DefaultTuple(minx, maxy+halfwidth));
                }else if(miny == maxy){
                    //horizontal line
                    if(buildFirst){
                        points.add(new DefaultTuple(minx-halfwidth, miny));
                        points.add(new DefaultTuple(minx+halfwidth, miny));
                    }
                    points.add(0,new DefaultTuple(maxx-halfwidth, miny));
                    points.add(new DefaultTuple(maxx+halfwidth, miny));
                }else{
                    //diagonal line
                    final double dx = segmentEnd.getX()-segmentStart.getX();
                    final double dy = segmentEnd.getY()-segmentStart.getY();
                    final double a = dy / dx;
                    final double b = segmentEnd.getY() - (a * segmentEnd.getX());
//                    Vector v = new Vector
                    
                    
                }
            }
                        
            //prepare next loop
            previousStep = new PathStep2D(pathIterator);
        }
        
        if(!points.isEmpty()){
            //close the last outline
            outlines.add(toPath(points));
        }
        
        return new MultiPolygon(outlines);
    }
    
    private Polygon toPath(Sequence points){
        final Polyline outter = new Polyline(Geometries.toTupleBuffer(points));
        return new Polygon(outter, new ArraySequence());
    }

    public void bitMask(final Geometry2D geom, final double[] resolution, final Image flagImage) {

        final TupleRW segmentStart = new DefaultTuple(2);
        final TupleRW segmentEnd = new DefaultTuple(2);
        final TupleRW pathStart = new DefaultTuple(2);
        final BBox bbox = new BBox(2);
        final int maskWidth = (int) flagImage.getExtent().getL(0);
        final int maskHeight = (int) flagImage.getExtent().getL(1);
        
        //simplify iterator, we only want line segments
        final PathIterator pathIterator = new FlattenPathIterator(geom.createPathIterator(), resolution);
        final TupleBuffer fsm = flagImage.getRawModel().asTupleBuffer(flagImage);

        final double halfwidth = width/2;
        while(pathIterator.next()){
            final int stepType = pathIterator.getType();
            draw:
            if(PathIterator.TYPE_MOVE_TO == stepType){
                pathIterator.getPosition(segmentEnd);
                //Do nothing, just update coordinates
                pathIterator.getPosition(pathStart);
            }else if(PathIterator.TYPE_LINE_TO == stepType || PathIterator.TYPE_CLOSE == stepType){
                if(PathIterator.TYPE_CLOSE == stepType){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                }else{
                    pathIterator.getPosition(segmentEnd);
                }

                double minx;
                double maxx;
                double miny;
                double maxy;
                if(segmentStart.getY() < segmentEnd.getY()){
                    miny = segmentStart.getY();
                    maxy = segmentEnd.getY();
                }else{
                    miny = segmentEnd.getY();
                    maxy = segmentStart.getY();
                }
                if(segmentStart.getX() < segmentEnd.getX()){
                    minx = segmentStart.getX();
                    maxx = segmentEnd.getX();
                }else{
                    minx = segmentEnd.getX();
                    maxx = segmentStart.getX();
                }

                if(miny == maxy){
                    //horizontal line
                    bbox.setRange(0, minx, maxx);
                    bbox.setRange(1, miny-halfwidth, maxy+halfwidth);
                    clip(bbox, maskWidth, maskHeight);
                    fsm.setTuple(bbox, OPAQUE);
                }else if(minx == maxx){
                    //vertical line
                    bbox.setRange(0, minx-halfwidth, maxx+halfwidth);
                    bbox.setRange(1, miny, maxy);
                    clip(bbox, maskWidth, maskHeight);
                    fsm.setTuple(bbox, OPAQUE);
                }else{
                    //diagonale line
                    final double dx = segmentEnd.getX()-segmentStart.getX();
                    final double dy = segmentEnd.getY()-segmentStart.getY();
                    final double a = dy / dx;
                    final double b = segmentEnd.getY() - (a * segmentEnd.getX());
                    //TODO not done right, still better than nothing
                    int y = (miny%1 >= 0.5d)? (int)Math.ceil(miny) : (int)miny;
                    final int imaxy = (int) Math.round(maxy);
                    for(; y<imaxy; y++){
                        final double x = ((0.5+y)-b)/a;//+0.5 for pixel center
                        int ix = (int)Math.round(x);
                        bbox.setRange(0, x-halfwidth, x+halfwidth);
                        bbox.setRange(1, y, y+1);
                        clip(bbox, maskWidth, maskHeight);
                        fsm.setTuple(bbox, OPAQUE);
                    }
                }
                
            }else{
                throw new RuntimeException("Unexpected step type : "+stepType);
            }
            segmentStart.set(segmentEnd);
        }
                
    }
    
    private void clip(BBox bbox, int width, int height){
        if(bbox.getMin(0)<0)bbox.setRange(0, 0, bbox.getMax(0));
        if(bbox.getMin(1)<0)bbox.setRange(1, 0, bbox.getMax(1));
        if(bbox.getMin(0)>width)bbox.setRange(0, width, bbox.getMax(0));
        if(bbox.getMin(1)>height)bbox.setRange(1, height, bbox.getMax(1));
        
        if(bbox.getMax(0)<0)bbox.setRange(0, bbox.getMin(0),0);
        if(bbox.getMax(1)<0)bbox.setRange(1, bbox.getMin(1),0);
        if(bbox.getMax(0)>width)bbox.setRange(0, bbox.getMin(0),width);
        if(bbox.getMax(1)>height)bbox.setRange(1, bbox.getMin(1),height);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlainBrush other = (PlainBrush) obj;
        if (Float.floatToIntBits(this.width) != Float.floatToIntBits(other.width)) {
            return false;
        }
        if (this.linecap != other.linecap) {
            return false;
        }
        if (!Arrays.equals(this.dasharray, other.dasharray)) {
            return false;
        }
        return true;
    }
    
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Float.floatToIntBits(this.width);
        hash = 43 * hash + this.linecap;
        return hash;
    }
    
}
