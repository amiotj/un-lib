
package science.unlicense.api.scenegraph;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.math.Affine;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.api.model.tree.Node;

/**
 * A scene node is a node located in space and in the tree structure.
 * Scene nodes have a reference to their parent and a transform with
 * rotation, translation and scale informations.
 * 
 * @author Johann Sorel
 */
public interface SceneNode extends Node{

    /**
     * Property indicating the widget parent.
     */
    public static final Chars PROPERTY_PARENT = new Chars("Parent");

    /**
     * Scene node children.
     * 
     * @return Sequence
     */
    Sequence getChildren();
    
    /**
     * Get the number of dimensions used in this node.
     * Usualy 2D or 3D.
     * The scene node matrix size will be dimension+1.
     *
     * @return number of dimension.
     */
    int getDimension();

    /**
     * Get parent node.
     * @return parent node, can be null
     */
    SceneNode getParent();

    /**
     * Recursively explore parents until the root node is found.
     * @return root node, never null.
     *   This node is returned if it has no parent
     */
    SceneNode getRoot();
    
    /**
     * Get the coordinate system in which the values are declared.
     * If this node coordinate system is null and has a parent, then parent
     * coordinate system will be returned.
     * 
     * @return CoordinateSystem can be null
     */
    CoordinateSystem getCoordinateSystem();
    
    /**
     * Get the coordinate system in which the values are declared.
     * 
     * @return CoordinateSystem can be null
     */
    CoordinateSystem getLocalCoordinateSystem();

    /**
     * Set the coordinate system in which the values are declared.
     * 
     * @param coordinateSystem can be null
     * @throws InvalidArgumentException is coordinate system dimension does not match node dimension
     */
    void setLocalCoordinateSystem(CoordinateSystem coordinateSystem) throws InvalidArgumentException;
    
    /**
     * Returns the parent to node transform.
     * This should be understood as the node to parent space transform.
     * If there is no parent, then getNodeTransform().asMatrix() and getNodeToRootSpace()
     * should be equal.
     * 
     * @return this node 'Parent to Node' transform
     */
    NodeTransform getNodeTransform();

    /**
     * Get the World to Node coordinate space.
     * Example :
     * If the node is 1 unit on X above the root, then this transform
     * will have -1 on the x axis of the translation to move from world coordinate
     * to Node coordinate.
     *
     * @return an immutable matrix calculate from the node chain.
     */
    Affine getRootToNodeSpace();
    
    /**
     * Inverse of RootToNode.
     * 
     * @return an immutable matrix calculate from the node chain.
     */
    Affine getNodeToRootSpace();

    /**
     * Update the node world to node transform.
     * Implementation may update any transform in the world to node chain
     * as long as the world to node transform is correct at the end.
     * @param wtn must be orthogonal of size dimension+1
     */
    void setRootToNodeSpace(Affine wtn);

    /**
     * Update the node world to node transform.
     * Implementation may update any transform in the world to node chain
     * as long as the world to node transform is correct at the end.
     * @param wtn
     */
    void setRootToNodeSpace(NodeTransform wtn);

}
