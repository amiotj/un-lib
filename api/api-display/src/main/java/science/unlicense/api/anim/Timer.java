
package science.unlicense.api.anim;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.predicate.Variable;

/**
 * A timer is responsible for updating animation at defined intervals.
 * This allows to obtain various refresh behaviors.
 * The timer generates a 'pulse' which is visible when listening to the updatetime property.
 *
 * @author Johann Sorel
 */
public interface Timer extends EventSource {

    public static final Chars PROPERTY_UPDATETIME = new Chars("UpdateTime");

    /**
     * Current timer time in nanoseconds.
     *
     * @return current timer time in nanoseconds.
     */
    long getTime();
    
    /**
     * Last timer update time in nanoseconds.
     * 
     * @return last update time in nanoseconds.
     */
    long getUpdateTime();

    /**
     * Update time variable.
     * 
     * @return Update time variable.
     */
    Variable varUpdateTime();

}
