
package science.unlicense.api.painter2d;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.Image;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.task.Task;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.scenegraph.s2d.GeometryNode2D;
import science.unlicense.api.scenegraph.s2d.GraphicNode2D;
import science.unlicense.api.scenegraph.s2d.ImageNode2D;
import science.unlicense.api.scenegraph.s2d.TextNode2D;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
abstract class SimplifiedPainter2D implements Painter2D {

    private final DefaultPainterVisitor visitor = new DefaultPainterVisitor();


    public void fill(CharArray text, float x, float y) {
        text(text,x,y,true);
    }

    public void stroke(CharArray text, float x, float y) {
        text(text,x,y,false);
    }

    private void text(CharArray text, float x, float y, boolean fill){
        final Paint paint = getPaint();
        final Font font = getFont();
        final Brush brush = getBrush();
        if(text == null || paint == null || font == null) return;
        if(!fill && brush==null) return;

        final FontMetadata metadata = font.getMetadata();
        final CharIterator ite = text.createIterator();
        final Affine2 trs = new Affine2(1, 0, x,
                                        0, -1, y);

        final double spacing = metadata.getAdvanceWidth(' ');

        double offset = 0.0;
        while(ite.hasNext()){
            final int c = ite.nextToUnicode();

            if(c == ' '){
                //TODO handle control caracters
                offset += spacing;
            }else{
                character(c, trs, fill);
                offset += metadata.getAdvanceWidth(c);
            }

            trs.set(0, 2, x+offset);
        }
    }

    protected void character(int c, Affine2 trs, boolean fill) {
        Geometry2D geom = getFont().getGlyph(c);
        if(geom==null) return;

        geom = new TransformedGeometry2D(geom, trs);

        if(fill){
            fill(geom);
        }else{
            stroke(geom);
        }
    }

    public void execute(Task task, Geometry2D area) {
        if(!(this instanceof ImagePainter2D)){
            throw new UnimplementedException("Not supported yet.");
        }

        //TODO special kind of image task
        this.flush();
        final Image inImage = ((ImagePainter2D)this).getImage();
        task.getInput().setFieldValue(new Chars("Image"),inImage);
        final Document execute = task.execute();
        final Image outImage = (Image) execute.getFieldValue(new Chars("Image"));
        if(outImage!=null){
            paint(outImage, new Affine2());
        }
    }

    public void render(SceneNode node) {
        node.accept(visitor, null);
    }


    public PainterState saveState() {
        return new DefaultPainterState(
                getTransform(),
                getClip(),
                getAlphaBlending(),
                getPaint(),
                getBrush(),
                getFont());
    }

    public void restoreState(PainterState state) {
        setTransform(state.getTransform());
        setClip(state.getClip());
        setAlphaBlending(state.getAlphaBlending());
        setPaint(state.getPaint());
        setBrush(state.getBrush());
        setFont(state.getFont());
    }

    @Override
    public Painter2D derivate(Affine2 transform) {
        return new DefaultDerivatePainter2D(this, transform);
    }
    
    protected class DefaultPainterVisitor extends DefaultNodeVisitor{
        public Object visit(Node node, Object context) {
            if(node instanceof GraphicNode2D){
                final Affine2 cp = getTransform();

                final GraphicNode2D gn = (GraphicNode2D) node;
                final AlphaBlending blending = gn.getBlending();
                if(blending!=null){
                    setAlphaBlending(blending);
                }

                Affine2 deriv = (Affine2) cp.multiply(gn.getNodeToRootSpace());
                setTransform(deriv);

                if(node instanceof GeometryNode2D){

                    final GeometryNode2D cdt = (GeometryNode2D) node;
                    //fills
                    final Paint[] fills = cdt.getFills();
                    for(int i=0;i<fills.length;i++){
                        setPaint(fills[i]);
                        fill(cdt.getGeometry());
                    }

                    //contours
                    final Contour[] contours = cdt.getContours();
                    for(int i=0;i<contours.length;i++){
                        setBrush(contours[i].getValue1());
                        setPaint(contours[i].getValue2());
                        stroke(cdt.getGeometry());
                    }

                }else if(node instanceof ImageNode2D){
                    final ImageNode2D gln = (ImageNode2D) node;
                    paint(gln.getImage(), null);


                }else if(node instanceof TextNode2D){

                    final TextNode2D cdt = (TextNode2D) node;
                    Vector anchor = cdt.getAnchor();

                    setFont(cdt.getFont());
                    //fills
                    final Paint[] fills = cdt.getFills();
                    for(int i=0;i<fills.length;i++){
                        setPaint(fills[i]);
                        fill(cdt.getText(),(float)anchor.getX(),(float)anchor.getY());
                    }
                }

                setTransform(cp);
            }

            return super.visit(node, context);
        }
    }

}
