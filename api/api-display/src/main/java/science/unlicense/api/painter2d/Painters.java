
package science.unlicense.api.painter2d;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;

/**
 * Convinient methods to manipulate Painters.
 *
 * @author Johann Sorel
 */
public final class Painters {

    private Painters(){}

    /**
     * Get the first painter manager.
     * @return PainterManager
     */
    public static PainterManager getPainterManager(){
        return getManagers()[0];
    }

    /**
     * Lists available painter managers.
     * @return array of PainterManager, never null but can be empty.
     */
    public static PainterManager[] getManagers(){
        final NamedNode root = science.unlicense.system.System.get().getModuleManager().getMetadataRoot().search(
                new Chars("services/science.unlicense.api.painter2d.PainterManager"));
        if(root==null){
            return new PainterManager[0];
        }
        return (PainterManager[]) Nodes.getChildrenValues(root, null).toArray(PainterManager.class);
    }

}
