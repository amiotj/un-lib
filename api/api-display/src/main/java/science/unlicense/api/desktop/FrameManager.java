
package science.unlicense.api.desktop;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Extent;

/**
 * A frame manager provides a general interface to create frames
 * independent of the underlying API.
 *
 * @author Johann Sorel
 */
public interface FrameManager {

    /**
     * Common name of this frame manager,
     * @return frame manager name
     */
    Chars getName();

    /**
     * The first cursor name must be the default cursor.
     * At least 2 cursor must be supported : default and null.
     * 
     * @return all named cursors available, never null, contain at least 2 names.
     */
    Chars[] getCursorNames();
    
    /**
     * Create a new frame.
     * @param translucent, ask for the frame to be translucent if possible.
     * @return Frame
     */
    Frame createFrame(boolean translucent);

    /**
     * Create a new child frame.
     * Child frames are automaticaly disposed when parent frame is disposed.
     *
     * @param parent parent frame
     * @param modale true to lock the parent frame events until this frame is closed
     * @param translucent, ask for the frame to be translucent if possible.
     * @return Frame child frame
     */
    Frame createFrame(Frame parent, boolean modale, boolean translucent);

    /**
     * TODO improve API, there might be several monitor.
     * also missing frequency, colormodel and other stuffs.
     * @return main display resolution.
     */
    Extent getDisplaySize();

    /**
     * Set frames icon.
     * TODO, it should take an image and frame by frame.
     * 
     * @param imagePath path of png files inside the jar
     */
    void setIcon(Chars imagePath);
    
}
