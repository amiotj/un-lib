

package science.unlicense.api.anim;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.math.Maths;

/**
 * Abstract animation.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractAnimation extends AbstractEventSource implements Animation{

    private final EventListener timerListener = new EventListener() {
        @Override
        public void receiveEvent(Event event) {
            final PropertyMessage pe = (PropertyMessage) event.getMessage();
            final long time = (Long)pe.getNewValue();
            pulse(time);
        }
    };
    private Chars name = Chars.EMPTY;
    private Timer timer = null;
    private float speed = 1f;
    private boolean play = false;

    /** current play state */
    private long lastTimeUpdate = -1;
    private double currentTime = 0;
    private int currentRepeat = 0;
    private int nbRepeat = 1;

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer){
        if(this.timer != null){
            this.timer.removeEventListener(PropertyMessage.PREDICATE, timerListener);
        }
        this.timer = timer;
        if(this.timer != null){
            this.timer.addEventListener(PropertyMessage.PREDICATE, timerListener);
        }
    }

    public void pulse(long time) {
        if(!play) return;

        //update current position
        if(lastTimeUpdate==-1){
            //starting or resuming animation
            lastTimeUpdate = time;
        }

        double animationTime = currentTime + ((double)(time-lastTimeUpdate) / 1000000.0) * speed;
        lastTimeUpdate = time;

        final double length = getLength();
        if(animationTime>length && speed>0){
            //reached the end
            if(nbRepeat==0){
                //infinite loop
                animationTime = animationTime-length;
            }else if(currentRepeat>0){
                //decrement
                animationTime = animationTime-length;
                currentRepeat--;
            }else{
                //end
                stop();
            }
        }else if(animationTime<0 && speed<0){
            //reached the end
            if(nbRepeat==0){
                //infinite loop
                animationTime = length-animationTime;
            }else if(currentRepeat>0){
                //decrement
                animationTime = length-animationTime;
                currentRepeat--;
            }else{
                //end
                stop();
            }
        }

        //ensure we are never outside the valid animation range
        animationTime = Maths.clamp(0, animationTime, length);
        setTime(animationTime);
        update();
    }

    public void setTime(double time) {
        if(currentTime==time) return;
        
        if(hasListeners()){
            final double old = currentTime;
            currentTime = time;
            sendPropertyEvent(this, PROPERTY_TIME, old, time);
        }else{
            currentTime = time;
        }
    }

    public double getTime() {
        return currentTime;
    }
    
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }

    public void start() {
        currentRepeat = 0;
        play = true;
    }
    
    public void stop() {
        play = false;
        lastTimeUpdate = -1;
    }
    
    public void reset() {
        stop();
        currentTime = 0;
        lastTimeUpdate = -1;
    }

    public void setRepeatCount(int nbRepeat) {
        this.nbRepeat = nbRepeat;
    }

    public int getNbRepeat() {
        return nbRepeat;
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

}
