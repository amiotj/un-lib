
package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;

/**
 * Layout for 2 components.
 * 
 * @author Johann Sorel
 */
public class PairLayout extends AbstractLayout {

    public static final int RELATIVE_TOP = 0;
    public static final int RELATIVE_RIGHT = 1;
    public static final int RELATIVE_BOTTOM = 2;
    public static final int RELATIVE_LEFT = 3;
    
    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;
    
    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;
    
    private int relativePos = RELATIVE_TOP;
    private int halignement = HALIGN_CENTER;
    private int valignement = VALIGN_CENTER;
    private int distance = 4;

    public PairLayout() {
        super(null);
    }

    public int getRelativePosition() {
        return relativePos;
    }

    public void setRelativePosition(int relativePos) {
        if(this.relativePos==relativePos) return;
        this.relativePos = relativePos;
        setDirty();
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        if(this.distance==distance) return;
        this.distance = distance;
        setDirty();
    }

    public int getVerticalAlignement() {
        return valignement;
    }

    public void setVerticalAlignement(int valignement) {
        if(this.valignement==valignement) return;
        this.valignement = valignement;
        setDirty();
    }

    public int getHorizontalAlignement() {
        return halignement;
    }

    public void setHorizontalAlignement(int halignement) {
        if(this.halignement==halignement) return;
        this.halignement = halignement;
        setDirty();
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_LAYOUT_CONSTRAINT.equals(propertyName)
               || Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_RESERVE_SPACE.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }

    protected void calculateExtents(Extents extents, Extent constraint) {
        final Extents empties = new Extents();
        
        final Positionable[] children = getPositionableArray();
        final Positionable mainPos;
        final Positionable secondPos;
        final Extents mainExts;
        final Extents secondExts;
        final double distance;
                
        if(children.length>1){
            mainPos = children[0];
            secondPos = children[1];
            mainExts = mainPos.getExtents(null,null);
            secondExts = secondPos.getExtents(null,null);
            distance = this.distance;
        }else if(children.length>0){
            mainPos = children[0];
            secondPos = null;
            mainExts = mainPos.getExtents(null,null);
            secondExts = empties;
            distance = 0;
        }else{
            mainPos = null;
            secondPos = null;
            mainExts = empties;
            secondExts = empties;
            distance = 0;
        }
        
        final Extent buffer1 = new Extent.Double(2);
        final Extent buffer2 = new Extent.Double(2);
        final Extent buffer3 = new Extent.Double(2);
        calculateExt(mainExts.getMin(buffer1), secondExts.getMin(buffer2), buffer3, distance);
        extents.setMin(buffer3);
        calculateExt(mainExts.getBest(buffer1), secondExts.getBest(buffer2), buffer3, distance);
        extents.setBest(buffer3);
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    private void calculateExt(Extent main, Extent second, Extent buffer, double distance){
        if(relativePos==RELATIVE_BOTTOM || relativePos==RELATIVE_TOP){
            buffer.set(0, Math.max(main.get(0), second.get(0)));
            buffer.set(1, main.get(1) + second.get(1) + distance);
        }else{
            buffer.set(0, main.get(0) + second.get(0) + distance);
            buffer.set(1, Math.max(main.get(1), second.get(1)));
        }
    }
    
    public void update() {
        
        final Extents empties = new Extents();
        
        final Positionable[] children = getPositionableArray();
        final Positionable mainPos;
        final Positionable secondPos;
        final Extents mainExts;
        final Extents secondExts;
        final double distance;
                
        if(children.length>1){
            mainPos = children[0];
            secondPos = children[1];
            mainExts = mainPos.getExtents(null,null);
            secondExts = secondPos.getExtents(null,null);
            distance = this.distance;
        }else if(children.length>0){
            mainPos = children[0];
            secondPos = null;
            mainExts = mainPos.getExtents(null,null);
            secondExts = empties;
            distance = 0;
        }else{
            mainPos = null;
            secondPos = null;
            mainExts = empties;
            secondExts = empties;
            distance = 0;
        }
        
        final BBox viewbbox = getView();
        double viewWidth = viewbbox.getSpan(0);
        double viewHeight = viewbbox.getSpan(1);
                
        final Extent empty = new Extent.Double(2);
        
        //prepare the informations we need
        final Extent mainMinExtent      = mainExts.getMin(null);
        final Extent secondMinExtent    = secondExts.getMin(null);
        final Extent mainExtent         = mainExts.getBest(null);
        final Extent secondExtent       = secondExts.getBest(null);
        final Extent mainMaxExtent      = mainExts.getMax(null);
        final Extent secondMaxExtent    = secondExts.getMax(null);

        //adjust sizes based on min/max extends
        if(relativePos == RELATIVE_BOTTOM || relativePos == RELATIVE_TOP){

            final double[] matchs = findBestMatch(viewHeight-distance,
                    mainMinExtent.get(1), mainExtent.get(1), mainMaxExtent.get(1),
                    secondMinExtent.get(1), secondExtent.get(1), secondMaxExtent.get(1));
            mainExtent.set(0, findBestMatch(viewWidth, mainMinExtent.get(0), mainExtent.get(0), mainMaxExtent.get(0)));
            mainExtent.set(1, matchs[0]);
            secondExtent.set(0, findBestMatch(viewWidth, secondMinExtent.get(0), secondExtent.get(0), secondMaxExtent.get(0)));
            secondExtent.set(1, matchs[1]);
            
        }else if(relativePos == RELATIVE_LEFT || relativePos == RELATIVE_RIGHT){

            final double[] matchs = findBestMatch(viewWidth-distance,
                    mainMinExtent.get(0), mainExtent.get(0), mainMaxExtent.get(0),
                    secondMinExtent.get(0), secondExtent.get(0), secondMaxExtent.get(0));
            mainExtent.set(0, matchs[0]);
            mainExtent.set(1, findBestMatch(viewHeight, mainMinExtent.get(1), mainExtent.get(1), mainMaxExtent.get(1)));
            secondExtent.set(0, matchs[1]);
            secondExtent.set(1, findBestMatch(viewHeight, secondMinExtent.get(1), secondExtent.get(1), secondMaxExtent.get(1)));
        }else{
            throw new RuntimeException("Invalid image placement "+relativePos);
        }


                
        //calculate relative positions
        final double[] secondTrs = new double[2];
        final double[] mainTrs = new double[2];
        final double totalWidth;
        final double totalHeight;
        if(relativePos == RELATIVE_BOTTOM || relativePos == RELATIVE_TOP){
            double mainV = mainExtent.get(1);
            double secondV = secondExtent.get(1);
            totalHeight = mainV + secondV + distance;
            totalWidth = Maths.max(mainExtent.get(0), secondExtent.get(0));
            double half = totalHeight/2.0;
            
            if(relativePos == RELATIVE_TOP){
                mainTrs[1] += half - mainV/2.0 ;
                secondTrs[1] -= half - secondV/2.0 ;
            }else{
                mainTrs[1] -= half - mainV/2.0 ;
                secondTrs[1] += half - secondV/2.0 ;
            }
            
        }else if(relativePos == RELATIVE_LEFT || relativePos == RELATIVE_RIGHT){              
            double mainV = mainExtent.get(0);
            double secondV = secondExtent.get(0);
            totalHeight = Maths.max(mainExtent.get(1), secondExtent.get(1));
            totalWidth = mainV + secondV + distance;
            double half = totalWidth/2.0;
            
            if(relativePos == RELATIVE_LEFT){
                mainTrs[0] += half - mainV/2.0 ;
                secondTrs[0] -= half - secondV/2.0 ;
            }else{
                mainTrs[0] -= half - mainV/2.0 ;
                secondTrs[0] += half - secondV/2.0 ;
            }
        }else{
            throw new RuntimeException("Invalid image placement "+relativePos);
        }
        
        //append global horizontal and vertical alignment
        if(valignement == VALIGN_TOP){
            final double offsetY = (viewbbox.getSpan(1) - totalHeight) / 2;
            secondTrs[1] -= offsetY;
            mainTrs[1] -= offsetY;
            
        }else if(valignement == VALIGN_BOTTOM){
            final double offsetY = (viewbbox.getSpan(1) - totalHeight) / 2;
            secondTrs[1] += offsetY;
            mainTrs[1] += offsetY;
        }
        
        if(halignement == HALIGN_LEFT){
            final double offsetX = (viewbbox.getSpan(0) - totalWidth) / 2;
            secondTrs[0] -= offsetX;
            mainTrs[0] -= offsetX;
        }else if(halignement == HALIGN_RIGHT){
            final double offsetX = (viewbbox.getSpan(0) - totalWidth) / 2;
            secondTrs[0] += offsetX;
            mainTrs[0] += offsetX;
        }
        
        //append inner bbox offsets
        secondTrs[0] += viewbbox.getMiddle(0);
        mainTrs[0] += viewbbox.getMiddle(0);
        secondTrs[1] += viewbbox.getMiddle(1);
        mainTrs[1] += viewbbox.getMiddle(1);
        
        if(mainPos!=null){
            mainPos.setEffectiveExtent(mainExtent);
            mainPos.getNodeTransform().setToTranslation(mainTrs);
        }
        if(secondPos!=null){
            secondPos.setEffectiveExtent(secondExtent);
            secondPos.getNodeTransform().setToTranslation(secondTrs);
        }
        
    }


    private static double findBestMatch(double size, double min, double best, double max){
        if(size>best){
            return Maths.min(size, max);
        }else if(size<best){
            return Maths.max(size, min);
        }else{
            return best;
        }
    }

    private static double[] findBestMatch(double size, double min0, double best0, double max0, double min1, double best1, double max1){
        double sum = best0 + best1;
        if(sum>size){
            //shrink values
            double remaining = sum-size;
            double hremaining = remaining / 2.0;
            double spare0 = Maths.max(0,best0-min0);
            double spare1 = Maths.max(0,best1-min1);
            if(spare0<hremaining){
                best0 -= spare0;
                best1 -= Maths.min(spare1, remaining-spare0);
            }else if(spare1<hremaining){
                best0 -= Maths.min(spare0, remaining-spare1);
                best1 -= spare1;
            }else{
                best0 -= hremaining;
                best1 -= hremaining;
            }
        }else if(sum<size){
            //expand values
            double remaining = size-sum;
            double hremaining = remaining / 2.0;
            double spare0 = Maths.max(0,max0-best0);
            double spare1 = Maths.max(0,max1-best1);
            if(spare0<hremaining){
                best0 += spare0;
                best1 += Maths.min(spare1, remaining-spare0);
            }else if(spare1<hremaining){
                best1 += spare1;
                best0 += Maths.min(spare0, remaining-spare1);
            }else{
                best0 += hremaining;
                best1 += hremaining;
            }
        }

        return new double[]{best0,best1};

    }

}
