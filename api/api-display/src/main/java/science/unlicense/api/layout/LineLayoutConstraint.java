
package science.unlicense.api.layout;

/**
 * Line layout constraint.
 * 
 * @author Johann Sorel
 */
public class LineLayoutConstraint implements LayoutConstraint {

    /**
     * Force moving to next line after the element which has this constraint.
     */
    public static final LineLayoutConstraint TO_NEXT_LINE = new LineLayoutConstraint();
    
    private LineLayoutConstraint() {
        super();
    }

}
