
package science.unlicense.api.anim;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventSource;

/**
 * Describes an animation.
 *
 * @author Johann Sorel
 */
public interface Animation extends EventSource {

    public static final Chars PROPERTY_TIME = new Chars("Time");

    /**
     * Get animation name.
     *
     * @return animation name
     */
    Chars getName();

    /**
     * Set animation name.
     *
     * @param name animation name
     */
    void setName(Chars name);

    /**
     * Timer used to schedules the animation refresh rate.
     * 
     * @return current animation timer
     */
    Timer getTimer();

    /**
     * Set timer used to schedules the animation refresh rate.
     * 
     * @param timer
     */
    void setTimer(Timer timer);
    
    /**
     * If you do not use a timer this method allows to simulate an timer event.
     * if the animations isn't playing this method wont have any effect.
     * 
     * @param timeNano unrelated time may not be relative to the animation
     */
    void pulse(long timeNano);

    /**
     * Animation speed correction.
     * Value :
     * - 2 twice speed
     * - 1 for normal speed
     * - 0.5 for half speed
     *
     * @param speed
     */
    void setSpeed(float speed);

    /**
     * @return animation speed
     */
    float getSpeed();

    /**
     * Move animation to given time.
     *
     * @param time in milliseconds
     */
    void setTime(double time);

    /**
     * Get current animation time from beginning.
     * @return time in milliseconds, -1 if not started.
     */
    double getTime();

    /**
     * Get animation total length in milliseconds.
     *
     * @return animation lenght, -1 if unknowned
     */
    double getLength();

    /**
     * Start the animation.
     */
    void start();
    
    /**
     * Stop animation.
     */
    void stop();

    /**
     * Stop and reset animation to the begining.
     */
    void reset();
    
    /**
     * Make the animation loop when it reach the end.
     * 0 : infinite repeat
     * N : n times
     * Default is 1.
     * 
     * @param nbRepeat
     */
    void setRepeatCount(int nbRepeat);

    /**
     *
     * @return number of repeat
     */
    int getNbRepeat();
    
    /**
     * Update the animation.
     * CAUTION : you should not call this method yourself, 
     * the timer should generate a time event wish then calls this method.
     */
    void update();

}
