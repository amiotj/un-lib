

package science.unlicense.api.painter2d;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 *
 * @author Johann Sorel
 */
public class CachedFontMetadata extends AbstractFontMetadata {

    private final FontMetadata base;
    //TODO replace it by an optimized int map
    private final Dictionary advances = new HashDictionary();

    public CachedFontMetadata(FontMetadata base) {
        super(base);
        this.base = base;
    }
    
    public double getAdvanceWidth(int unicode) {
        Double d = (Double) advances.getValue(unicode);
        if(d==null){
            d = base.getAdvanceWidth(unicode);
            advances.add(unicode, d);
        }
        return d;
    }
    
}
