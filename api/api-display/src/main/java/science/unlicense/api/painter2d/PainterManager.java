

package science.unlicense.api.painter2d;

/**
 *
 * @author Johann Sorel
 */
public interface PainterManager {
    
    ImagePainter2D createPainter(int width, int height);
    
}
