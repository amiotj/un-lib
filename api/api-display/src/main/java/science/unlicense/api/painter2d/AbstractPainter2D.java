
package science.unlicense.api.painter2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.scenegraph.s2d.DefaultGraphic2DFactory;
import science.unlicense.api.scenegraph.s2d.Graphic2DFactory;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Affine2;

/**
 * Abstract painter 2d.
 *
 * @author Johann Sorel
 */
public abstract class AbstractPainter2D extends SimplifiedPainter2D{

    protected Paint paint;
    protected Brush brush;
    protected Font font;
    protected Geometry2D clip;
    protected boolean antialiazing = false;
    protected AlphaBlending alphaBlending = AlphaBlending.DEFAULT;
    private final Affine2 baseMatrix = new Affine2();
    private final Affine2 matrix = new Affine2();

    public AbstractPainter2D() {
    }
    
    public AbstractPainter2D(Affine2 base) {
        baseMatrix.set(base);
    }

    public final Affine2 getTransform() {
        return matrix;
    }
    
    public final void setTransform(Affine2 trs){
        matrix.set(trs);
    }
    
    protected Affine2 getFinalTransform(){
        return (Affine2) baseMatrix.multiply(matrix);
    }
    
    public void setClip(Geometry2D geom){
        this.clip = geom;
    }

    public Geometry2D getClip() {
        return clip;
    }

    public void setAntialiazing(boolean antialiazing) {
        this.antialiazing = antialiazing;
    }

    public boolean isAntialiazing() {
        return antialiazing;
    }

    public AlphaBlending getAlphaBlending() {
        return alphaBlending;
    }

    public void setAlphaBlending(AlphaBlending alphaBlending) {
        CObjects.ensureNotNull(alphaBlending);
        this.alphaBlending = alphaBlending;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Font getFont() {
        return font;
    }

    public void setPaint(final Paint paint) {
        this.paint = paint;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setBrush(final Brush brush) {
        this.brush = brush;
    }

    public Brush getBrush() {
        return brush;
    }

    public Graphic2DFactory getGraphicFactory() {
        return DefaultGraphic2DFactory.INSTANCE;
    }

    /**
     * Default implementation, does nothing.
     */
    public void flush() {
    }

}
