
package science.unlicense.api.scenegraph.s2d;

import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;

/**
 *
 * @author Johann Sorel
 */
public class DefaultGraphic2DFactory implements Graphic2DFactory{

    public static final DefaultGraphic2DFactory INSTANCE = new DefaultGraphic2DFactory();

    @Override
    public SceneNode createNode() {
        return new DefaultSceneNode(2);
    }

    @Override
    public GeometryNode2D createGeometryNode() {
        return new GeometryNode2D();
    }

    @Override
    public ImageNode2D createImageNode() {
        return new ImageNode2D();
    }

    @Override
    public TextNode2D createTextNode() {
        return new TextNode2D();
    }

}
