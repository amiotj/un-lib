
package science.unlicense.api.anim;

import java.util.TimerTask;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Property;

/**
 * Timer using java TimerTask backend.
 *
 * TODO : make a system api to access timer.
 * TODO : write a default timer on a thread.
 *
 * @author Johann Sorel
 */
public class DefaultTimer extends AbstractEventSource implements Timer{


    private final java.util.Timer javaTimer = new java.util.Timer();
    private final int rateMs;
    private long updateTime = 0;

    public DefaultTimer(int rateMs) {
        this.rateMs = rateMs;

        javaTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                setUpdateTime(getTime());
            }
        }, rateMs, rateMs);
    }

    /**
     *
     * @return refresh rate in milliseconds.
     */
    public int getRefreshRateMs() {
        return rateMs;
    }

    public long getTime() {
        return System.nanoTime();
    }

    public long getUpdateTime() {
        return updateTime;
    }

    /**
     *
     * @param time in nanoseconds
     */
    public void setUpdateTime(long time) {
        final long oldTime = this.updateTime;
        this.updateTime = time;
        sendPropertyEvent(this, PROPERTY_UPDATETIME, oldTime, time);
    }

    public Property varUpdateTime() {
        return getProperty(PROPERTY_UPDATETIME);
    }

    public void stop(){
        javaTimer.cancel();
    }

}
