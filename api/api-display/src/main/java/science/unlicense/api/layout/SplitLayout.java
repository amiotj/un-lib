

package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class SplitLayout extends AbstractLayout{

    private boolean vertical = false;
    private double splitGap = 3;
    private double splitPosition = 300;

    public SplitLayout() {
        super(SplitConstraint.class);
    }

    public void setVerticalSplit(boolean vertical) {
        if(vertical==this.vertical) return;
        this.vertical = vertical;
        setDirty();
    }

    public boolean isVerticalSplit() {
        return vertical;
    }

    public double getSplitGap() {
        return splitGap;
    }

    public void setSplitGap(double splitGap) {
        if(splitGap==this.splitGap) return;
        this.splitGap = splitGap;
        setDirty();
    }

    public double getSplitPosition() {
        return splitPosition;
    }

    public void setSplitPosition(double splitPosition) {
        if(splitPosition==this.splitPosition) return;
        this.splitPosition = splitPosition;
        setDirty();
    }

    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_LAYOUT_CONSTRAINT.equals(propertyName)
               || Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_RESERVE_SPACE.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    public void update() {

        final BBox extent = getView();

        //find all located widgets
        Positionable first = null;
        Positionable second = null;
        Positionable split = null;
        final Positionable[] children = getPositionableArray();
        for(int i=0;i<children.length;i++){
            final Positionable widget = children[i];
            final LayoutConstraint lcst = widget.getLayoutConstraint();
            if(lcst == SplitConstraint.TOP) first = widget;
            else if(lcst == SplitConstraint.BOTTOM) second = widget;
            else if(lcst == SplitConstraint.SPLITTER) split = widget;
        }

        Extent firstExt = new Extent.Double(2);
        Extent secondExt = new Extent.Double(2);
        Extent splitExt = new Extent.Double(2);
        final double[] firstTrs = new double[2];
        final double[] secondTrs = new double[2];
        final double[] splitTrs = new double[2];
        if(first!=null) first.getExtents(null,null).getMin(firstExt);
        if(second!=null) second.getExtents(null,null).getMin(secondExt);
        if(split!=null) split.getExtents(null,null).getMin(splitExt);


        if(vertical){
            final double trsx = extent.getMiddle(0);
            
            firstExt.set(0, extent.getSpan(0));
            firstExt.set(1, splitPosition);
            firstTrs[0] = trsx;
            firstTrs[1] = extent.getMin(1)+firstExt.get(1)/2.0;
                        
            splitExt.set(0, extent.getSpan(0));
            splitExt.set(1, splitGap);
            splitTrs[0] = trsx;
            splitTrs[1] = extent.getMin(1) + splitPosition + splitExt.get(1)/2.0;

            secondExt.set(0, extent.getSpan(0));
            secondExt.set(1, extent.getSpan(1)-splitPosition-splitGap);
            secondTrs[0] = trsx;
            secondTrs[1] = extent.getMax(1) - secondExt.get(1)/2.0;
        }else{
            final double trsy = extent.getMiddle(1);
            
            firstExt.set(0, splitPosition);
            firstExt.set(1, extent.getSpan(1));
            firstTrs[0] = extent.getMin(0)+firstExt.get(0)/2.0;
            firstTrs[1] = trsy;

            splitExt.set(0, splitGap);
            splitExt.set(1, extent.getSpan(1));
            splitTrs[0] = extent.getMin(0)+ splitPosition + splitExt.get(0)/2.0;
            splitTrs[1] = trsy;
            
            secondExt.set(0, extent.getSpan(0)-splitPosition-splitGap);
            secondExt.set(1, extent.getSpan(1));
            secondTrs[0] = extent.getMax(0)- secondExt.get(0)/2.0;
            secondTrs[1] = trsy;
        }

        if(first!=null){
            first.setEffectiveExtent(firstExt);
            first.getNodeTransform().setToTranslation(firstTrs);
        }
        if(second!=null){
            second.setEffectiveExtent(secondExt);
            second.getNodeTransform().setToTranslation(secondTrs);
        }
        if(split!=null){
            split.setEffectiveExtent(splitExt);
            split.getNodeTransform().setToTranslation(splitTrs);
        }
    }

    protected void calculateExtents(Extents extents, Extent constraint) {
        final Extent ext = new Extent.Double(2);

        //find all located elements
        Positionable first = null;
        Positionable second = null;
        final Positionable[] children = getPositionableArray();
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(lcst == SplitConstraint.TOP) first = element;
            else if(lcst == SplitConstraint.BOTTOM) second = element;
        }

        Extent firstExt = EMPTY;
        Extent secondExt = EMPTY;
        if(first!=null) firstExt = first.getExtents(null,null).getBest(null);
        if(second!=null) secondExt = second.getExtents(null,null).getBest(null);

        if(vertical){
            ext.set(0, Maths.max(firstExt.get(0), secondExt.get(0)));
            ext.set(1, firstExt.get(1)+splitGap+secondExt.get(1));
        }else{
            ext.set(0, firstExt.get(0)+splitGap+secondExt.get(0));
            ext.set(1, Maths.max(firstExt.get(1), secondExt.get(1)));
        }

        extents.minX = ext.get(0);
        extents.minY = ext.get(1);
        extents.bestX = extents.minX;
        extents.bestY = extents.minY;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

}
