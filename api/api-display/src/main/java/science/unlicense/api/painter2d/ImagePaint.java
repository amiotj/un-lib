
package science.unlicense.api.painter2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.image.Image;
import science.unlicense.api.math.Affine;
import science.unlicense.impl.math.Affine2;

/**
 * Image pattern paint.
 * 
 * @author Johann Sorel
 */
public class ImagePaint implements Paint {

    //TODO take possible values from opengl
    public static final int WARP_NONE = 0;
    public static final int WARP_REPEAT = 1;
    public static final int WARP_MIRROR_REPEAT = 2;
    
    private final Image image;
    private final Affine transform;
    private final int wrapx;
    private final int wrapy;

    public ImagePaint(Image image, Affine transform, int wrapx, int wrapy) {
        this.image = image;
        this.transform = transform;
        this.wrapx = wrapx;
        this.wrapy = wrapy;
    }

    public Image getImage() {
        return image;
    }

    public Affine getTransform() {
        return transform;
    }

    public int getWrapX() {
        return wrapx;
    }
    
    public int getWrapY() {
        return wrapy;
    }
    
    @Override
    public void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending) {
        throw new UnimplementedException("Not supported yet.");
    }

}
