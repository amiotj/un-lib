
package science.unlicense.api.layout;

import science.unlicense.api.layout.LayoutConstraint;

/**
 * Split layout constraint.
 *
 * @author Johann Sorel
 */
public class SplitConstraint implements LayoutConstraint{

    public static final SplitConstraint TOP    = new SplitConstraint();
    public static final SplitConstraint BOTTOM = new SplitConstraint();
    public static final SplitConstraint LEFT   = TOP;
    public static final SplitConstraint RIGHT  = BOTTOM;
    public static final SplitConstraint SPLITTER = new SplitConstraint();

    private SplitConstraint() {
    }

}
