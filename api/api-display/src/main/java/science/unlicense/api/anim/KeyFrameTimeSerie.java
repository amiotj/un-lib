

package science.unlicense.api.anim;

import science.unlicense.api.collection.ArrayOrderedSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedSet;

/**
 * Serie of key frames.
 * Time serie are often associated to a single object in the scene.
 *
 * @author Johann Sorel
 */
public abstract class KeyFrameTimeSerie implements TimeSerie{
    
    protected final OrderedSet frames = new ArrayOrderedSet();
 
    /**
     * Modifiable ordered set of key frames.
     * @return frames, never null.
     */
    public OrderedSet getFrames() {
        return frames;
    }

    /**
     * Get frame at exact given time.
     * 
     * @param time
     * @return
     */
    public KeyFrame getFrame(double time){
        final Iterator ite = frames.createIterator();
        while(ite.hasNext()){
            final KeyFrame frame = (KeyFrame) ite.next();
            if(frame.getTime()==time) return frame;
        }
        return null;
    }
        
    /**
     * Find nearest above and under frames.
     * if time matches exactly a frame then this frame is used both as under and above frames.
     * 
     * @param time
     * @param buffer
     * @return 
     */
    public KeyFrame[] getNearest(double time, KeyFrame[] buffer){
        
        //find the under and above frames
        if(buffer==null){
            buffer = new KeyFrame[2];
        }else{
            buffer[0] = null;
            buffer[1] = null;
        }
        final Iterator ite = frames.createIterator();
        while(ite.hasNext()){
            final KeyFrame f = (KeyFrame)ite.next();
            final double frameTime = f.getTime();
            if(frameTime<time){
                buffer[0] = f;
                buffer[1] = f;
            }else if(frameTime == time){
                //exact match
                buffer[0] = f;
                buffer[1] = f;
                break;
            }else if(frameTime > time){
                buffer[1] = f;
                if(buffer[0]==null){
                    buffer[0] = f;
                }
                break;
            }
        }
        
        return buffer;
    }

    /**
     * Get a keyframe at the given time or if it doesn't exist create a key frame
     * result of the interpolation.
     * 
     * @param time
     * @param buffer
     * @return
     */
    public abstract KeyFrame interpolate(double time, KeyFrame buffer);


    /**
     * Find the last frame time end.
     * Time in milliseconds.
     *
     * @return
     */
    public double getLength(){
        if(frames.isEmpty()){
            return 0;
        }
        final KeyFrame f = (KeyFrame) frames.toArray()[frames.getSize()-1];
        return f.getTime();
    }
    
}
