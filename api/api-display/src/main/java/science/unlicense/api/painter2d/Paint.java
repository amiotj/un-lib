
package science.unlicense.api.painter2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.image.Image;
import science.unlicense.impl.math.Affine2;

/**
 * A Paint defines the inner filling color.
 *
 * @author Johann Sorel
 */
public interface Paint {

    void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending);

}
