
package science.unlicense.api.desktop;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.api.event.MessagePredicate;
import science.unlicense.api.predicate.Predicate;

/**
 * A frame event message.
 * 
 * @author Johann Sorel
 */
public class FrameMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(FrameMessage.class);

    public static final int TYPE_PREDISPOSE = 1;
    public static final int TYPE_DISPOSED = 2;
    
    private final int type;

    /**
     * Create a new frame message.
     * 
     * @param type one of FrameMessage.TYPE_X
     */
    public FrameMessage(int type) {
        super(true);
        this.type = type;
    }

    /**
     * Mouse event type.
     * 
     * @return int, one of FrameMessage.TYPE_X
     */
    public int getType() {
        return type;
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("FE(");
        switch(type){
            case TYPE_PREDISPOSE : cb.append("PRE-DISPOSE");break;
            case TYPE_DISPOSED : cb.append("DISPOSED");break;
        }
        cb.append(')');
        
        return cb.toChars();
    }
    
    
}
