
package science.unlicense.api.scenegraph.s2d;

import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.image.Image;

/**
 * 
 * @author Johann Sorel
 */
public class ImageNode2D extends GraphicNode2D{

    protected Image image;
    
    public ImageNode2D() {
        super();
    }

    public ImageNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    
}
