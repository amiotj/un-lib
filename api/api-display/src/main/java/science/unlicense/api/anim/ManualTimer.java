
package science.unlicense.api.anim;

import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Property;

/**
 * Manually controlled Timer.
 * 
 * @author Johann Sorel
 */
public class ManualTimer extends AbstractEventSource implements Timer{

    private long updateTime = 0;

    public ManualTimer() {
    }

    public long getTime() {
        return updateTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    /**
     *
     * @param time in nanoseconds
     */
    public void setUpdateTime(long time) {
        final long oldTime = this.updateTime;
        this.updateTime = time;
        sendPropertyEvent(this, PROPERTY_UPDATETIME, oldTime, time);
    }

    public Property varUpdateTime() {
        return getProperty(PROPERTY_UPDATETIME);
    }

    public void stop(){
    }

}
