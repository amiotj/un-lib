
package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;
import science.unlicense.api.geometry.BBox;

/**
 * Place elements on a regular grid.
 *
 * @author Johann Sorel
 */
public class GridLayout extends AbstractLayout{

    private int nbRow;
    private int nbCol;
    private double horizontalSpacing = 0;
    private double verticalSpacing = 0;

    /**
     * Default grid layout configuration with two columns and an infinite
     * number of rows.
     */
    public GridLayout() {
        this(0,2);
    }

    /**
     * New grid layout.
     *
     * @param nbRow : number of rows, <=0 for undefined
     * @param nbCol : number of columns, <=0 for undefined
     */
    public GridLayout(int nbRow, int nbCol) {
        this(nbRow,nbCol,0,0);
    }

    /**
     * New grid layout.
     *
     * @param nbRow : number of rows, <=0 for undefined
     * @param nbCol : number of columns, <=0 for undefined
     */
    public GridLayout(int nbRow, int nbCol, double spacex, double spacey) {
        super(null);
        if(nbRow<0)nbRow=0;
        if(nbCol<0)nbCol=0;
        this.nbRow = nbRow;
        this.nbCol = nbCol;
        this.horizontalSpacing = spacex;
        this.verticalSpacing = spacey;
    }

    public int getNbRow() {
        return nbRow;
    }

    public void setNbRow(int nbRow) {
        if(nbRow<0)nbRow=0;
        if(this.nbRow == nbRow) return;
        this.nbRow = nbRow;
        setDirty();
    }

    public int getNbCol() {
        return nbCol;
    }

    public void setNbCol(int nbCol) {
        if(nbCol<0)nbCol=0;
        if(this.nbCol == nbCol) return;
        this.nbCol = nbCol;
        setDirty();
    }

    public double getHorizontalSpacing() {
        return horizontalSpacing;
    }

    public void setHorizontalSpacing(double horizontalSpacing) {
        if(this.horizontalSpacing == horizontalSpacing) return;
        this.horizontalSpacing = horizontalSpacing;
        setDirty();
    }

    public double getVerticalSpacing() {
        return verticalSpacing;
    }

    public void setVerticalSpacing(double verticalSpacing) {
        if(this.verticalSpacing == verticalSpacing) return;
        this.verticalSpacing = verticalSpacing;
        setDirty();
    }

    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_RESERVE_SPACE.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {

        final Extent ext = new Extent.Double(2);
        final Extents buffer = new Extents();

        //find the widest and highest

        final Positionable[] children = getPositionableArray();
        for(Positionable child : children){
            if(!child.isVisible()) continue;
            child.getExtents(buffer,null);
            ext.set(0, Maths.max(ext.get(0), buffer.bestX));
            ext.set(1, Maths.max(ext.get(1), buffer.bestY));
        }


        final int nbx;
        final int nby;
        if(nbRow==0){
            //infinite number of row
            nbx = nbCol;
            nby = children.length/nbx;
        }else if(nbCol==0){
            //infinite number of columns
            nby = nbRow;
            nbx = children.length/nby;
        }else{
            //fixed size, fill line by line
            nbx = nbCol;
            nby = nbRow;
        }

        ext.set(0, nbx*ext.get(0));
        ext.set(1, nby*ext.get(1));

        //add spacing
        ext.set(0, ext.get(0) + Maths.max(0, nbx-1)*horizontalSpacing );
        ext.set(1, ext.get(1) + Maths.max(0, nby-1)*verticalSpacing );

        extents.minX = ext.get(0);
        extents.minY = ext.get(1);
        extents.bestX = extents.minX;
        extents.bestY = extents.minY;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    public void update() {

        final BBox inner = getView();
        final Positionable[] children = getPositionableArray();

        //calculate the cell sizes
        final double offsetX = inner.getMin(0);
        final double offsetY = inner.getMin(1);
        final double availableWidth = inner.getSpan(0);
        final double availableHeight = inner.getSpan(1);
        final int nbx;
        final int nby;
        if(nbRow==0){
            //infinite number of row
            nbx = nbCol<=0 ? 1 : nbCol;
            nby = children.length/nbx;
        }else if(nbCol==0){
            //infinite number of columns
            nby = nbRow<=0 ? 1 : nbRow;
            nbx = children.length/nby;
        }else{
            //fixed size, fill line by line
            nbx = nbCol;
            nby = nbRow;
        }

        final double spacingTotalx = Math.max(0, nbx-1)*horizontalSpacing;
        final double spacingTotaly = Math.max(0, nby-1)*verticalSpacing;
        final double cellx = (availableWidth-spacingTotalx)/nbx;
        final double celly = (availableHeight-spacingTotaly)/nby;
        final double spacex = cellx + horizontalSpacing;
        final double spacey = celly + verticalSpacing;

        int x=0;
        int y=0;
        final Extent ext = new Extent.Double(cellx,celly);
        final double[] trs = new double[2];
        for(Positionable child : children){
            if(!child.isVisible()) continue;
            trs[0] = offsetX+x*spacex + cellx/2.0;
            trs[1] = offsetY+y*spacey + celly/2.0;
            child.setEffectiveExtent(ext);
            child.getNodeTransform().setToTranslation(trs);

            //prepare next loop
            x++;
            if(x>=nbx){
                x=0;
                y++;
            }
        }

    }

}
