
package science.unlicense.api.layout;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.s2d.Rectangle;

/**
 * Form layout places elements in a dynamic column/row grid
 * elements may span across multiple columns or rows.
 * Space between columns or rows can be configured as a whole or one by one.
 *
 * @author Johann Sorel
 */
public class FormLayout extends AbstractLayout{
    
    /**
     * Use for column and row sizes.
     * This value indicate the layout that the size should try to fit the best
     * width on the elements it contains.
     */
    public static final int SIZE_AUTO = -1;
    /**
     * Use for column and row sizes.
     * This value indicate the layout can expand the col/row size when there is
     * empty space left.
     */
    public static final int SIZE_EXPAND = -2;
    
    private double defaultColumnSize    = -1; // dynamic
    private double defaultRowSize       = -1; // dynamic
    private double defaultColumnSpace   = 0;
    private double defaultRowSpace      = 0;
    private double[] columnSize         = new double[0];
    private double[] rowSize            = new double[0];
    private double[] columnSpace        = new double[0];
    private double[] rowSpace           = new double[0];

    public FormLayout() {
        super(FillConstraint.class);
    }
    
    public FormLayout(double defaultColumnSpace, double defaultRowSpace) {
        super(FillConstraint.class);
        this.defaultColumnSpace = defaultColumnSpace;
        this.defaultRowSpace = defaultRowSpace;
    }
    
    public FormLayout(Sequence positionable) {
        super(FillConstraint.class);
        setPositionables(positionable);
    }

    public double getDefaultColumnSize() {
        return defaultColumnSize;
    }

    public void setDefaultColumnSize(double defaultColumnSize) {
        if(this.defaultColumnSize==defaultColumnSize) return;
        this.defaultColumnSize = defaultColumnSize;
        setDirty();
    }

    public double getDefaultColumnSpace() {
        return defaultColumnSpace;
    }

    public void setDefaultColumnSpace(double defaultColumnSpace) {
        if(this.defaultColumnSpace==defaultColumnSpace) return;
        this.defaultColumnSpace = defaultColumnSpace;
        setDirty();
    }

    public double getDefaultRowSize() {
        return defaultRowSize;
    }

    public void setDefaultRowSize(double defaultRowSize) {
        if(this.defaultRowSize==defaultRowSize) return;
        this.defaultRowSize = defaultRowSize;
        setDirty();
    }

    public double getDefaultRowSpace() {
        return defaultRowSpace;
    }

    public void setDefaultRowSpace(double defaultRowSpace) {
        if(this.defaultRowSpace==defaultRowSpace) return;
        this.defaultRowSpace = defaultRowSpace;
        setDirty();
    }

    public void setColumnSize(int index, double size){
        if(columnSize.length<=index){
            final int i = columnSize.length;
            columnSize = Arrays.resize(columnSize, index+1);
            Arrays.fill(columnSize, i, columnSize.length-i,-1);
        }
        if(columnSize[index]==size) return;
        columnSize[index] = size;
        setDirty();
    }

    /**
     * Reset all column sizes set.
     */
    public void resetColumnSizes(){
        columnSize = new double[0];
        setDirty();
    }
    
    public double getColumnSize(int i){
        if(columnSize.length>i){
            return (columnSize[i]<0) ? defaultColumnSize : columnSize[i];
        }
        return defaultColumnSize;
    }
    
    public void setRowSize(int index, double size){
        if(rowSize.length<=index){
            final int i = rowSize.length;
            rowSize = Arrays.resize(rowSize, index+1);
            Arrays.fill(rowSize, i, rowSize.length-i,-1);
        }
        if(rowSize[index]==size) return;
        rowSize[index] = size;
        setDirty();
    }
    
    public double getRowSize(int i){
        if(rowSize.length>i){
            return (rowSize[i]<0) ? defaultRowSize : rowSize[i];
        }
        return defaultRowSize;
    }


    /**
     * Reset all column sizes set.
     */
    public void resetRowSizes(){
        rowSize = new double[0];
        setDirty();
    }

    public void setColumnSpace(int index, double size){
        if(columnSpace.length<=index){
            final int i = columnSpace.length;
            columnSpace = Arrays.resize(rowSize, index+1);
            Arrays.fill(columnSpace, i, columnSpace.length-i,-1);
        }
        columnSpace[index] = size;
        setDirty();
    }
    
    public double getColumnSpace(int i){
        if(columnSpace.length>i){
            return (columnSpace[i]<0) ? defaultColumnSpace : columnSpace[i];
        }
        return defaultColumnSpace;
    }
    
    public void setRowSpace(int index, double size){
        if(rowSpace.length<=index){
            final int i = rowSpace.length;
            rowSpace = Arrays.resize(rowSize, index+1);
            Arrays.fill(rowSpace, i, rowSpace.length-i,-1);
        }
        rowSpace[index] = size;
        setDirty();
    }
    
    public double getRowSpace(int i){
        if(rowSpace.length>i){
            return (rowSpace[i]<0) ? defaultRowSpace : rowSpace[i];
        }
        return defaultRowSpace;
    }
    
    private int[] getResizableColumns(){
        int[] resizable = new int[0];
        for(int i=0;i<columnSize.length;i++){
            if(columnSize[i]==SIZE_EXPAND){
                resizable = Arrays.insert(resizable, resizable.length, i);
            }
        }
        return resizable;
    }
    
    private int[] getResizableRows(){
        int[] resizable = new int[0];
        for(int i=0;i<rowSize.length;i++){
            if(rowSize[i]==SIZE_EXPAND){
                resizable = Arrays.insert(resizable, resizable.length, i);
            }
        }
        return resizable;
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_LAYOUT_CONSTRAINT.equals(propertyName)
               || Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_RESERVE_SPACE.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    /**
     * Calculate the best column sizes with respect to elements constraints.
     * 
     * @return res[0] = column size,  res[1] = row sizes
     */
    private Extents1D[][] calculateColRowSizes(){
        final Positionable[] children = getPositionableArray();
        final Positionable[] fchildren = new Positionable[children.length];
        int nbChild = 0;

        //find number of lines and columns
        int nbColumns = 0;
        int nbRows = 0;
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(!(lcst instanceof FillConstraint)) continue;
            final FillConstraint cst = (FillConstraint)lcst;
            if(!element.isReserveSpace() && !element.isVisible()) continue;
            nbColumns = Maths.max(nbColumns, cst.getX()+cst.getSpanX());
            nbRows = Maths.max(nbRows, cst.getY()+cst.getSpanY());
            fchildren[nbChild] = element;
            nbChild++;
        }

        //find all lines and columns size
        final Extents1D[] bestColSizes = new Extents1D[nbColumns];
        final Extents1D[] bestRowSizes = new Extents1D[nbRows];
        
        //fill knowned sizes
        for(int i=0;i<bestColSizes.length;i++){
            bestColSizes[i] = new Extents1D();
            bestColSizes[i].best = getColumnSize(i);
        }
        for(int i=0;i<bestRowSizes.length;i++){
            bestRowSizes[i] = new Extents1D();
            bestRowSizes[i].best = getRowSize(i);
        }
        
        
        //fill sizes from components size, only if all elements have a span of 1
        final Extents buffer = new Extents();
        final double[] minColSizes = new double[nbColumns];
        final double[] minRowSizes = new double[nbRows];
        for(int i=0;i<bestColSizes.length;i++){
            if(bestColSizes[i].best>=0)continue;
            boolean fixed = true;
            for(int k=0;k<nbChild;k++){
                final Positionable w = fchildren[k];
                final FillConstraint cst = (FillConstraint) w.getLayoutConstraint();
                if(cst.getX()<=i && i<cst.getX()+cst.getSpanX()){
                    if(cst.getSpanX()==1){
                        w.getExtents(buffer,null);
                        bestColSizes[i].best = Maths.max(bestColSizes[i].best, buffer.bestX);
                        minColSizes[i] = bestColSizes[i].best;
                    }else{
                        //this element span on multiple col/row, size will be dediced in next loop
                        fixed=false;
                    }
                }
            }
            if(!fixed) bestColSizes[i].best=-1;
        }
        //find minimum size of remaining columns and rows when there are spanning elements
        for(int i=0;i<bestColSizes.length;i++){
            if(bestColSizes[i].best>=0)continue;
            for(int k=0;k<nbChild;k++){
                final Positionable w = fchildren[k];
                final FillConstraint cst = (FillConstraint) w.getLayoutConstraint();
                if(cst.getX()<=i && cst.getSpanX()>1 && i<cst.getX()+cst.getSpanX()){
                    w.getExtents(buffer,null);
                    double width = buffer.bestX;
                    //remove the minimum known size and spacing
                    for(int c=cst.getX(),d=c+cst.getSpanX();c<d;c++){
                        width -= minColSizes[c];
                        if(c<d-1) width -= getColumnSpace(c);
                    }
                    if(width<0){
                        //no need to expend the sizes
                        continue;
                    }
                    //split the size among columns
                    width /= cst.getSpanX();
                    for(int c=i,d=i+cst.getSpanX();c<d && c<minColSizes.length;c++){
                        minColSizes[c] += width;
                    }
                }
            }
        }
        //Set the sizes of col/row with spans
        for(int i=0;i<bestColSizes.length;i++){
            if(bestColSizes[i].best<0)bestColSizes[i].best = minColSizes[i];
        }
        
        
        //fill sizes from components size, only if all elements have a span of 1
        final Extent colConstraint = new Extent.Double(0, Double.POSITIVE_INFINITY);        
        final Extents extents = new Extents();
        for(int i=0;i<bestRowSizes.length;i++){
            if(bestRowSizes[i].best>=0)continue;
            boolean fixed = true;
            for(int k=0;k<nbChild;k++){
                final Positionable w = fchildren[k];
                final FillConstraint cst = (FillConstraint) w.getLayoutConstraint();
                if(cst.getY()<=i && i<cst.getY()+cst.getSpanY()){
                    if(cst.getSpanY()==1){
                        colConstraint.set(0, bestColSizes[cst.getX()].best); //TODO take spanX in consideration
                        w.getExtents(extents, colConstraint);
                        bestRowSizes[i].best = Maths.max(bestRowSizes[i].best, extents.bestY);
                        minRowSizes[i] = bestRowSizes[i].best;
                    }else{
                        //this element span on multiple col/row, size will be dediced in next loop
                        fixed=false;
                    }
                }
            }
            if(!fixed) bestRowSizes[i].best=-1;
        }        
        //find minimum size of remaining columns and rows when there are spanning elements        
        for(int i=0;i<bestRowSizes.length;i++){
            if(bestRowSizes[i].best>=0)continue;
            for(int k=0;k<nbChild;k++){
                final Positionable w = fchildren[k];
                final FillConstraint cst = (FillConstraint) w.getLayoutConstraint();
                if(cst.getY()==i && cst.getSpanY()>1){
                    colConstraint.set(0, bestColSizes[cst.getX()].best); //TODO take spanX in consideration
                    w.getExtents(extents, colConstraint);
                    double height = extents.bestY;
                    //remove the minimum known size and spacing
                    for(int c=i,d=i+cst.getSpanY();c<d;c++){
                        height -= minRowSizes[c];
                        if(c<d-1) height -= getRowSpace(c);
                    }
                    if(height<0){
                        //no need to expend the sizes
                        continue;
                    }
                    //split the size among rows
                    height /= cst.getSpanY();
                    for(int c=i,d=i+cst.getSpanY();c<d;c++){
                        minRowSizes[c] += height;
                    }
                }
            }
        }        
        //Set the sizes of col/row with spans
        for(int i=0;i<bestRowSizes.length;i++){
            if(bestRowSizes[i].best<0)bestRowSizes[i].best = minRowSizes[i];
        }
                
        return new Extents1D[][]{bestColSizes,bestRowSizes};
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {

        final Extents1D[][] sizes = calculateColRowSizes();
        double spanX = 0;
        for(int i=0;i<sizes[0].length;i++){
            spanX += sizes[0][i].best;
            if(i<sizes[0].length-1){
                spanX += getColumnSpace(i);
            }
        }
        
        double spanY = 0;
        for(int i=0;i<sizes[1].length;i++){
            spanY += sizes[1][i].best;
            if(i<sizes[1].length-1){
                spanY += getRowSpace(i);
            }
        }
        
        extents.minX = spanX;
        extents.minY = spanY;
        extents.bestX = spanX;
        extents.bestY = spanY;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    public void update() {
        final BBox inner = getView();
        final Positionable[] children = getPositionableArray();
        final Extents1D[][] sizes = calculateColRowSizes();

        if(sizes[0].length==0){
            //no elements
            return;
        }
        
        //expand row and cols with expandable sizes.
        //find how much we have left available
        final Extents1D[] bestColSizes = sizes[0];
        final Extents1D[] bestRowSizes = sizes[1];
        final int[] resizableCols = getResizableColumns();
        final int[] resizableRows = getResizableRows();
        double currentWidth = 0.0;
        double currentHeight = 0.0;
        
        //remove row and col spaces
        for(int i=0;i<bestColSizes.length;i++){
            currentWidth += bestColSizes[i].best;
            if(i<bestColSizes.length-1){
                currentWidth += getColumnSpace(i);
            }
        }
        for(int i=0;i<bestRowSizes.length;i++){
            currentHeight += bestRowSizes[i].best;
            if(i<bestRowSizes.length-1){
                currentHeight += getRowSpace(i);
            }
        }
        
        final double remainingWidthPerCol = (inner.getSpan(0)-currentWidth)/resizableCols.length;
        final double remainingHeightPerRow = (inner.getSpan(1)-currentHeight)/resizableRows.length;
        if(remainingWidthPerCol>0){
            //split remaining space on resizable columns
            for(int i=0;i<resizableCols.length;i++){
                if(resizableCols[i]<bestColSizes.length){
                    bestColSizes[resizableCols[i]].best += remainingWidthPerCol;
                }
            }
        }else{
            //reduce missing space on resizable columns
            //TODO improve algo, current one do not give a propertional or complete reduction
            for(int i=0;i<resizableCols.length;i++){
                if(resizableCols[i]<bestColSizes.length){
                    bestColSizes[resizableCols[i]].best += remainingWidthPerCol;
                    if(bestColSizes[resizableCols[i]].best<0) bestColSizes[resizableCols[i]].best = 0;
                }
            }
        }
        if(remainingHeightPerRow>0){
            //split remaining space on resizable rows
            for(int i=0;i<resizableRows.length;i++){
                if(resizableRows[i]<bestRowSizes.length){
                    bestRowSizes[resizableRows[i]].best += remainingHeightPerRow;
                }
            }
        }else{
            //reduce missing space on resizable columns
            //TODO improve algo, current one do not give a propertional or complete reduction
            for(int i=0;i<resizableRows.length;i++){
                if(resizableRows[i]<bestRowSizes.length){
                    bestRowSizes[resizableRows[i]].best += remainingHeightPerRow;
                    if(bestRowSizes[resizableRows[i]].best<0) bestRowSizes[resizableRows[i]].best = 0;
                }
            }
        }
        
        
        //precalculate offsets of each col/row
        final double[] colOffsets = new double[bestColSizes.length];
        final double[] rowOffsets = new double[bestRowSizes.length];
        
        
        //start at margin offset
        colOffsets[0] = inner.getMin(0);
        rowOffsets[0] = inner.getMin(1);
        for(int i=1;i<colOffsets.length;i++){
            colOffsets[i] = colOffsets[i-1] + bestColSizes[i-1].best + getColumnSpace(i-1);
        }
        for(int i=1;i<rowOffsets.length;i++){
            rowOffsets[i] = rowOffsets[i-1] + bestRowSizes[i-1].best + getRowSpace(i-1);
        }
        
        final BBox ext = new BBox(2);
        double width;
        double height;
        double offx;
        double offy;
            
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(!(lcst instanceof FillConstraint) || (!element.isReserveSpace() && !element.isVisible())){
                element.setEffectiveExtent(EMPTY);
                continue;
            }
            
            final FillConstraint cst = (FillConstraint)lcst;
            
            //calculate element size
            offx = colOffsets[cst.getX()];
            offy = rowOffsets[cst.getY()];
            width = 0;
            height = 0;
            for(int c=cst.getX(),cn=cst.getX()+cst.getSpanX();c<cn;c++){
                width += bestColSizes[c].best;
                if(c<cn-1) width += getColumnSpace(c);
            }
            for(int r=cst.getY(),rn=cst.getY()+cst.getSpanY();r<rn;r++){
                height += bestRowSizes[r].best;
                if(r<rn-1) height += getRowSpace(r);
            }
            
            ext.setRange(0, offx, offx+width);
            ext.setRange(1, offy, offy+height);
            cst.applyOn(element, ext);
        }
        
    }

    private static class Extents1D{
        public double min;
        public double best;
        public double max;
    }

}
