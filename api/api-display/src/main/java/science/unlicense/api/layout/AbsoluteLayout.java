
package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;

/**
 * Absolute layout places elements according to specified bounds.
 *
 * @author Johann Sorel
 */
public class AbsoluteLayout extends AbstractLayout {

    public AbsoluteLayout() {
        super(null);
    }

    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {
        //do nothing, positionable have they own transforms
        extents.minX = 0.0;
        extents.minX = 0.0;
        extents.bestX = 100;
        extents.bestY = 100;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    public void update() {
        //do nothing, positionable have they own transforms
        //just set the extent
        final Positionable[] children = getPositionableArray();
        final Extents buffer = new Extents();
        for(int i=0;i<children.length;i++){
            final Positionable element = (Positionable) children[i];
            element.getExtents(buffer, null);
            element.setEffectiveExtent(buffer.getBest(null));
        }
    }

}
