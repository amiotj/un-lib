

package science.unlicense.api.layout;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.scenegraph.DefaultSceneNode;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPositionableNode extends DefaultSceneNode implements Positionable{

    // layout informations
    protected final Extent effExtent = new Extent.Double(-1,-1);
    private final Extents overrides = new Extents();
    private Extents currentExts = null;
    
    protected LayoutConstraint layoutConstraint = null;
    protected boolean visible = true;
    protected boolean reserveSpace = true;

    public AbstractPositionableNode(boolean allowChildren) {
        super(2,allowChildren);
        overrides.minX = Double.NaN;
        overrides.minY = Double.NaN;
        overrides.bestX = Double.NaN;
        overrides.bestY = Double.NaN;
        overrides.maxX = Double.NaN;
        overrides.maxY = Double.NaN;
    }
    
    /**
     * {@inheritDoc }
     */
    public final Extents getOverrideExtents(){
        return overrides.copy();
    }

    /**
     * {@inheritDoc }
     */
    public final void setOverrideExtents(Extents extent){
        if(overrides.equals(extent)) return;
        final Extents old = this.overrides.copy();
        overrides.set(extent);
        sendPropertyEvent(this,PROPERTY_OVERRIDE_EXTENTS, old, extent.copy());
        updateExtents();
    }

    /**
     * {@inheritDoc }
     */
    public Extent getEffectiveExtent() {
         return effExtent.copy();
    }

    /**
     * {@inheritDoc }
     */
    public void setEffectiveExtent(Extent extent) {
        if(effExtent!=null && effExtent.equals(extent)) return;
        final Extent old = this.effExtent.copy();
        this.effExtent.set(extent);
        sendPropertyEvent(this,PROPERTY_EFFECTIVE_EXTENT, old, extent);
    }
    
    /**
     * {@inheritDoc }
     */
    public final BBox getBoundingBox() {
        return centeredBBox(effExtent, null);
    }
    
    public static BBox centeredBBox(Extent extent, BBox buffer) {
        if(buffer==null) buffer = new BBox(2);
        final double hx = extent.get(0) / 2.0;
        final double hy = extent.get(1) / 2.0;
        buffer.setRange(0, -hx, hx);
        buffer.setRange(1, -hy, hy);
        return buffer;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isReserveSpace() {
        return reserveSpace;
    }

    /**
     * {@inheritDoc }
     */
    public void setReserveSpace(boolean reserve) {
        if(this.reserveSpace != reserve){
            this.reserveSpace = reserve;
            sendPropertyEvent(this,PROPERTY_RESERVE_SPACE, !reserve, reserve);
        }
    }

    /**
     * {@inheritDoc }
     */
    public void setLayoutConstraint(LayoutConstraint layoutConstraint) {
        if(this.layoutConstraint != layoutConstraint){
            final LayoutConstraint oldValue = this.layoutConstraint;
            this.layoutConstraint = layoutConstraint;
            sendPropertyEvent(this,PROPERTY_LAYOUT_CONSTRAINT, oldValue, layoutConstraint);
        }
    }

    /**
     * {@inheritDoc }
     */
    public LayoutConstraint getLayoutConstraint() {
        return layoutConstraint;
    }
    
    /**
     * {@inheritDoc }
     */
    public void setVisible(boolean visible) {
        if(this.visible != visible){
            this.visible = visible;
            sendPropertyEvent(this,PROPERTY_VISIBLE, !visible, visible);
        }
    }

    /**
     * {@inheritDoc }
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * {@inheritDoc }
     */
    public final Extents getExtents(){
        return getExtents(null);
    }

    /**
     * {@inheritDoc }
     */
    public final Extents getExtents(Extents buffer) {
        if(currentExts==null){
            currentExts = new Extents();
            getExtents(currentExts, effExtent);
        }
        if(buffer==null){
            return currentExts.copy();
        }else{
            buffer.set(currentExts);
            return buffer;
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public final Extents getExtents(Extents buffer, Extent constraint) {
        if(buffer==null) buffer = new Extents();
        getExtentsInternal(buffer, constraint);
        //apply overrides
        if(!Double.isNaN(overrides.minX)) buffer.minX = overrides.minX;
        if(!Double.isNaN(overrides.minY)) buffer.minY = overrides.minY;
        if(!Double.isNaN(overrides.bestX)) buffer.bestX = overrides.bestX;
        if(!Double.isNaN(overrides.bestY)) buffer.bestY = overrides.bestY;
        if(!Double.isNaN(overrides.maxX)) buffer.maxX = overrides.maxX;
        if(!Double.isNaN(overrides.maxY)) buffer.maxY = overrides.maxY;
        return buffer;
    }
    
    /**
     * Calculate extents, ignore overrides, this is handle in AbstractPositionableNode
     */
    protected abstract void getExtentsInternal(Extents buffer, Extent constraint);
    
    /**
     * Recalculate widget min/max/best extent.
     * Sends event if extent changed.
     * 
     * @return true if an extent has been updated
     */
    protected boolean updateExtents(){
        Extents old = currentExts;
        currentExts = null;
        getExtents();
        if(old!=null && old.equals(currentExts)){
            return false;
        }else{
            sendPropertyEvent(this,PROPERTY_EXTENTS, old, currentExts.copy());
            return true;
        }
    }
    
}
