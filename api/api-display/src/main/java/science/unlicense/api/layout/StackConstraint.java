

package science.unlicense.api.layout;

/**
 *
 * @author Johann Sorel
 */
public class StackConstraint implements LayoutConstraint {
    
    private final float priority;

    public StackConstraint(int priority) {
        this.priority = priority;
    }

    /**
     * Widgets with higher priority will be placed above others.
     * 
     * @return constaint priority
     */
    public float getPriority() {
        return priority;
    }
    
}
