
package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;
import science.unlicense.api.geometry.BBox;

/**
 * Table layout places elements in a dynamic column/row grid
 * elements may span across multiple columns or rows.
 * Space between columns or rows can be configured as a whole or one by one.
 *
 * @author Johann Sorel
 */
public class TableLayout extends AbstractLayout{
    
    private double[] columnSize         = new double[0];
    private double[] rowSize            = new double[0];
    private double[] columnSpace        = new double[0];
    private double[] rowSpace           = new double[0];

    public TableLayout() {
        this(0,0);
    }
    
    public TableLayout(int nbRow, int nbCol) {
        super(FillConstraint.class);
        columnSize = new double[nbCol];
        rowSize = new double[nbRow];
        columnSpace = new double[nbCol];
        rowSpace = new double[nbRow];
    }
    
    public void setColumnSize(int index, double size){
        if(columnSize[index]==size) return;
        columnSize[index] = size;
        setDirty();
    }

    
    public double getColumnSize(int i){
        return columnSize[i];
    }
    
    public void setRowSize(int index, double size){
        if(rowSize[index]==size) return;
        rowSize[index] = size;
        setDirty();
    }
    
    public double getRowSize(int i){
        return rowSize[i];
    }

    public void setColumnSpace(int index, double size){
        if(columnSpace[index]==size) return;
        columnSpace[index] = size;
        setDirty();
    }
    
    public double getColumnSpace(int i){
        return columnSpace[i];
    }
    
    public void setRowSpace(int index, double size){
        if(rowSpace[index]==size) return;
        rowSpace[index] = size;
        setDirty();
    }
    
    public double getRowSpace(int i){
        return rowSpace[i];
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_LAYOUT_CONSTRAINT.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {

        extents.setAll(Maths.sum(columnSize)+Maths.sum(columnSpace), Maths.sum(rowSize)+Maths.sum(rowSpace));
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    public void update() {
        final BBox inner = getView();
        final Positionable[] children = getPositionableArray();
                
        
        //precalculate offsets of each col/row
        final double[] colOffsets = new double[columnSize.length];
        final double[] rowOffsets = new double[rowSize.length];
        
        //start at margin offset
        colOffsets[0] = inner.getMin(0);
        rowOffsets[0] = inner.getMin(1);
        for(int i=1;i<colOffsets.length;i++){
            colOffsets[i] = colOffsets[i-1] + columnSize[i-1] + getColumnSpace(i-1);
        }
        for(int i=1;i<rowOffsets.length;i++){
            rowOffsets[i] = rowOffsets[i-1] + rowSize[i-1] + getRowSpace(i-1);
        }

        final BBox ext = new BBox(2);
        double width;
        double height;
        double offx;
        double offy;
            
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(!(lcst instanceof FillConstraint) || !element.isVisible()){
                element.setEffectiveExtent(EMPTY);
                continue;
            }
            
            final FillConstraint cst = (FillConstraint)lcst;
            
            //calculate element size
            offx = colOffsets[cst.getX()];
            offy = rowOffsets[cst.getY()];
            width = 0;
            height = 0;
            for(int c=cst.getX(),cn=cst.getX()+cst.getSpanX();c<cn;c++){
                width += columnSize[c];
                if(c<cn-1) width += getColumnSpace(c);
            }
            for(int r=cst.getY(),rn=cst.getY()+cst.getSpanY();r<rn;r++){
                height += rowSize[r];
                if(r<rn-1) height += getRowSpace(r);
            }

            ext.setRange(0, offx, offx+width);
            ext.setRange(1, offy, offy+height);
            cst.applyOn(element, ext);
        }
        
    }

}
