
package science.unlicense.api.painter2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.image.Image;
import science.unlicense.api.scenegraph.s2d.Graphic2DFactory;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class DefaultDerivatePainter2D extends SimplifiedPainter2D {

    private final Painter2D parent;
    private final Affine2 baseMatrix;
    private final Affine2 matrix = new Affine2();

    public DefaultDerivatePainter2D(Painter2D parent, Affine2 baseMatrix) {
        this.parent = parent;
        this.baseMatrix = baseMatrix;
    }

    protected Affine2 getFinalTransform(){
        return (Affine2) baseMatrix.multiply(matrix);
    }

    @Override
    public Affine2 getTransform() {
        return matrix;
    }

    @Override
    public void setTransform(Affine2 trs) {
        matrix.set(trs);
    }

    @Override
    public FontStore getFontStore() {
        return parent.getFontStore();
    }

    @Override
    public AlphaBlending getAlphaBlending() {
        return parent.getAlphaBlending();
    }

    @Override
    public void setAlphaBlending(AlphaBlending alphaBlending) {
        parent.setAlphaBlending(alphaBlending);
    }

    @Override
    public void setPaint(Paint paint) {
        parent.setPaint(paint);
    }

    @Override
    public Paint getPaint() {
        return parent.getPaint();
    }

    @Override
    public void setBrush(Brush brush) {
        parent.setBrush(brush);
    }

    @Override
    public Brush getBrush() {
        return parent.getBrush();
    }

    @Override
    public void setFont(Font font) {
        parent.setFont(font);
    }

    @Override
    public Font getFont() {
        return parent.getFont();
    }

    @Override
    public void setClip(Geometry2D geom) {
        if(geom==null) parent.setClip(null);
        else parent.setClip(new TransformedGeometry2D(geom, baseMatrix));
    }

    @Override
    public Geometry2D getClip() {
        Geometry2D clip = parent.getClip();
        if(clip==null) return null;
        return new TransformedGeometry2D(clip,baseMatrix.invert());
    }
    
    @Override
    public void fill(Geometry2D geom) {
        parent.fill(new TransformedGeometry2D(geom, getFinalTransform()));
    }

    @Override
    public void stroke(Geometry2D geom) {
        parent.stroke(new TransformedGeometry2D(geom, getFinalTransform()));
    }

    @Override
    public void paint(Image image, Affine2 transform) {
        if (transform==null) {
            parent.paint(image, getFinalTransform());
        } else {
            parent.paint(image, (Affine2) getFinalTransform().multiply(transform));
        }
    }

    @Override
    public Graphic2DFactory getGraphicFactory() {
        return parent.getGraphicFactory();
    }

    @Override
    public void flush() {
        parent.flush();
    }

    @Override
    public void dispose() {
        parent.dispose();
    }

}
