
package science.unlicense.api.scenegraph.s2d;

import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.painter2d.Contour;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 * 
 * @author Johann Sorel
 */
public class GeometryNode2D extends GraphicNode2D{

    protected Geometry2D geometry;
    protected Contour[] contours = null;
    protected Paint[] fills = null;
    
    public GeometryNode2D() {
        super();
    }

    public GeometryNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public Geometry2D getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry2D geometry) {
        this.geometry = geometry;
    }

    public Contour[] getContours() {
        return contours;
    }

    public void setContours(Contour[] contours) {
        this.contours = contours;
    }

    public Paint[] getFills() {
        return fills;
    }

    public void setFills(Paint[] fills) {
        this.fills = fills;
    }
    
}
