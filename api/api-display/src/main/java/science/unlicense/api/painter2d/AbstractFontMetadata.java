
package science.unlicense.api.painter2d;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;

/**
 * Default font metadata.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractFontMetadata implements FontMetadata{

    protected BBox glyphBox;
    protected double ascent;
    protected double descent;
    protected double lineGap;
    protected double advanceWidthMax;
    protected double minLeftSideBearing;
    protected double minRightSideBearing;
    protected double xMaxExtent;

    public AbstractFontMetadata() {
    }
    
    public AbstractFontMetadata(FontMetadata meta) {
        this.glyphBox = meta.getGlyphBox();
        this.ascent = meta.getAscent();
        this.descent = meta.getDescent();
        this.lineGap = meta.getLineGap();
        this.advanceWidthMax = meta.getAdvanceWidthMax();
        this.minLeftSideBearing = meta.getMinLeftSideBearing();
        this.minRightSideBearing = meta.getMinRightSideBearing();
        this.xMaxExtent = meta.getXMaxExtent();
    }
    
    public BBox getGlyphBox() {
        return glyphBox;
    }

    public void setGlyphBox(BBox glyphBox) {
        this.glyphBox = glyphBox;
    }

    public double getAscent() {
        return ascent;
    }

    public void setAscent(double ascent) {
        this.ascent = ascent;
    }

    public double getDescent() {
        return descent;
    }

    public void setDescent(double descent) {
        this.descent = descent;
    }

    public double getLineGap() {
        return lineGap;
    }

    public void setLineGap(double lineGap) {
        this.lineGap = lineGap;
    }

    public double getAdvanceWidthMax() {
        return advanceWidthMax;
    }

    public void setAdvanceWidthMax(double advanceWidthMax) {
        this.advanceWidthMax = advanceWidthMax;
    }

    public double getMinLeftSideBearing() {
        return minLeftSideBearing;
    }

    public void setMinLeftSideBearing(double minLeftSideBearing) {
        this.minLeftSideBearing = minLeftSideBearing;
    }

    public double getMinRightSideBearing() {
        return minRightSideBearing;
    }

    public void setMinRightSideBearing(double minRightSideBearing) {
        this.minRightSideBearing = minRightSideBearing;
    }

    public double getXMaxExtent() {
        return xMaxExtent;
    }

    public void setxMaxExtent(double xMaxExtent) {
        this.xMaxExtent = xMaxExtent;
    }

    public double getAdvanceWidth(Char c) {
        return getAdvanceWidth(c.toUnicode());
    }
    
    public BBox getCharBox(Char c) {
        return glyphBox;
    }

    public BBox getCharsBox(CharArray text) {
        final BBox box = new BBox(2);
        if(text!=null && !text.isEmpty()){
            box.set(glyphBox);
            final CharIterator ite = text.createIterator();
            double width=0;
            while(ite.hasNext()){
                final int c = ite.nextToUnicode();
                width += getAdvanceWidth(c);
            }
            box.setRange(0, box.getMin(0), width);
        }        
        return box;
    }
    
    public FontMetadata derivate(double fontSize){
        return new DerivateFontMetadata(this, fontSize);
    }
    
}
