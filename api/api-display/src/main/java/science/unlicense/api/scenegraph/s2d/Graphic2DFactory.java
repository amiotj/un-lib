
package science.unlicense.api.scenegraph.s2d;

import science.unlicense.api.scenegraph.SceneNode;

/**
 *
 * @author Johann Sorel
 */
public interface Graphic2DFactory {

    SceneNode createNode();
    
    GeometryNode2D createGeometryNode();
    
    ImageNode2D createImageNode();
    
    TextNode2D createTextNode();
    
}
