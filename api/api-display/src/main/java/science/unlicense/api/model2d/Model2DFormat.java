

package science.unlicense.api.model2d;

import science.unlicense.api.io.IOException;
import science.unlicense.api.store.Format;

/**
 *
 * @author Johann Sorel
 */
public interface Model2DFormat extends Format{

    Model2DStore open(Object input) throws IOException;
    
}
