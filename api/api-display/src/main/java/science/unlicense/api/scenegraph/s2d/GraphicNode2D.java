
package science.unlicense.api.scenegraph.s2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 * A Graphic is geometry with style rendering informations.
 * 
 * @author Johann Sorel
 */
public class GraphicNode2D extends DefaultSceneNode{

    private Geometry2D clip;
    private AlphaBlending blending = null;
    
    public GraphicNode2D() {
        super(2);
    }

    public GraphicNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public Geometry2D getClip() {
        return clip;
    }

    public void setClip(Geometry2D clip) {
        this.clip = clip;
    }

    public AlphaBlending getBlending() {
        return blending;
    }

    public void setBlending(AlphaBlending blending) {
        this.blending = blending;
    }
    
    
    
}
