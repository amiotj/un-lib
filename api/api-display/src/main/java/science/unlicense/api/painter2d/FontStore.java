
package science.unlicense.api.painter2d;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Set;
import science.unlicense.api.collection.primitive.IntSet;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 * A font store list the available families it supports.
 * 
 * @author Johann Sorel
 */
public interface FontStore {
    
    /**
     * List available family names.
     */
    Set getFamilies();
    
    /**
     * Test if this resolver has this font family.
     * @param name 
     */
    boolean containsFamily(Chars name);
    
    /**
     * Get font family metadatas.
     * @param family
     * @return FontMetadata
     */
    FontMetadata getMetadata(Chars family);
    
    /**
     * Get a single slyph.
     * @param character
     * @param family
     * @return 
     */
    Geometry2D getGlyph(int unicode, Chars family);
    
    /**
     * Get a single slyph.
     * @param character
     * @param family
     * @return 
     */
    Geometry2D getGlyph(Char character, Chars family);
 
    /**
     * List available glyphs for a font family.
     * 
     * @param family font family name
     * @return unicode sequence, never null
     */
    IntSet listCharacters(Chars family);

    /**
     * Read and store the given font.
     * 
     * @param path Path to font file.
     */
    void registerFont(Path path) throws IOException;
    
}
