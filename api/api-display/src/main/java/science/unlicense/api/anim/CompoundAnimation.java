
package science.unlicense.api.anim;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.math.Maths;
import science.unlicense.api.predicate.ClassPredicate;

/**
 * Group of animations acting like a single one.
 *
 * @author Johann Sorel
 */
public class CompoundAnimation extends AbstractAnimation {

    private final Sequence elements = new ArraySequence();

    public CompoundAnimation() {
        Collections.constraint(elements, new ClassPredicate(Animation.class));
    }

    public Sequence getElements() {
        return elements;
    }

    public void setSpeed(float speed) {
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).setSpeed(speed);
        }
        super.setSpeed(speed);
    }

    public void setTime(double time) {
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).setTime(time);
        }
        super.setTime(time);
    }

    /**
     * Return the length of the longest sub animation.
     * @return speed
     */
    public double getLength() {
        double length = 0;
        for(int i=0,n=elements.getSize();i<n;i++){
            length = Maths.max( ((Animation)elements.get(i)).getLength(), length);
        }
        return length;
    }

    public void start() {
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).start();
        }
        super.start();
    }

    public void stop() {
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).stop();
        }
        super.stop();
    }
    
    public void reset() {
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).reset();
        }
        super.reset();
    }

    public void setRepeatCount(int repeat) {
        super.setRepeatCount(repeat);
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).setRepeatCount(repeat);
        }
    }

    public void update() {
        for(int i=0,n=elements.getSize();i<n;i++){
            ((Animation)elements.get(i)).update();
        }
    }
    
}
