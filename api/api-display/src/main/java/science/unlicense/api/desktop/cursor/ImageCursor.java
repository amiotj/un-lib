
package science.unlicense.api.desktop.cursor;

import science.unlicense.api.image.Image;
import science.unlicense.api.math.Tuple;

/**
 * 
 * @author Johann Sorel
 */
public class ImageCursor implements Cursor{

    private final Image image;
    private final Tuple pick;

    public ImageCursor(Image image, Tuple pick) {
        this.image = image;
        this.pick = pick;
    }

    public Image getImage() {
        return image;
    }

    public Tuple getPick() {
        return pick;
    }
    
}
