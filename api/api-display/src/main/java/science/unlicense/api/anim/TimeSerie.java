

package science.unlicense.api.anim;

/**
 * Time serie.
 *
 * @author Johann Sorel
 */
public interface TimeSerie {
    
    /**
     * Get a keyframe at the given time.
     * The key frame might be the result of the an interpolation.
     * 
     * @param time
     * @param buffer
     * @return
     */
    KeyFrame interpolate(double time, KeyFrame buffer);


    /**
     * Find the length of this time serie.
     *
     * @return Time length in milliseconds.
     */
    double getLength();
    
}
