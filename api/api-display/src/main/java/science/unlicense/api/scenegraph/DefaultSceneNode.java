
package science.unlicense.api.scenegraph;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.CoordinateSystems;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.impl.math.DefaultAffine;

/**
 * Default scene node implementation.
 * 
 * @author Johann Sorel
 */
public class DefaultSceneNode extends DefaultNode implements SceneNode{
    
    /**
     * This node 'Parent to Node' Matrix.
     */
    protected final NodeTransform modelTransform;
    protected CoordinateSystem coordinateSystem;
    /**
     * Additional properties.
     * created only when needed.
     */
    private Dictionary properties = null;
    protected SceneNode parent = null;

    /**
     * Cache root to node transforms.
     */
    private final AffineRW rootToNode;
    private boolean rootToNodeDirty = true;
    private final AffineRW nodeToRoot;
    private boolean nodeToRootDirty = true;
    
    
    /**
     * Create a scene node with a defined world dimension.
     * The dimension is use by the node transform.
     * 
     * @param dimension 
     */
    public DefaultSceneNode(int dimension) {
        this(dimension,true);
    }
    
    /**
     * Create a scene node with a defined coordinate system.
     * The dimension is use by the node transform.
     * 
     * @param cs
     */
    public DefaultSceneNode(CoordinateSystem cs) {
        this(cs,true);
    }
    
    /**
     * Create a scene node with a defined coordinate system.
     * The dimension is use by the node transform.
     * 
     * @param cs
     * @param allowChildren
     */
    public DefaultSceneNode(CoordinateSystem cs, boolean allowChildren) {
        this(cs.getDimension(),allowChildren);
        this.coordinateSystem = cs;
    }

    /**
     * Create a scene node with a defined world dimension.
     * The dimension is use by the node transform.
     * 
     * @param dimension
     * @param allowChildren true to allow children, false otherwise
     */
    public DefaultSceneNode(int dimension, boolean allowChildren) {
        super(allowChildren);
        rootToNode = DefaultAffine.create(dimension);
        nodeToRoot = DefaultAffine.create(dimension);
        modelTransform = new NotifiedNodeTransform(dimension);
    }

    /**
     * {@inheritDoc }
     * @return 
     */
    public int getDimension() {
        return modelTransform.getDimension();
    }

    /**
     * {@inheritDoc }
     */
    public CoordinateSystem getCoordinateSystem() {
        if(coordinateSystem!=null) return coordinateSystem;
        if(parent!=null) parent.getCoordinateSystem();
        return null;
    }

    /**
     * {@inheritDoc }
     */
    public CoordinateSystem getLocalCoordinateSystem() {
        return coordinateSystem;
    }

    /**
     * {@inheritDoc }
     */
    public synchronized void setLocalCoordinateSystem(CoordinateSystem coordinateSystem) {
        if(this.coordinateSystem==coordinateSystem) return;
        if(coordinateSystem!=null && coordinateSystem.getDimension()!=getDimension()){
            throw new InvalidArgumentException("Coordinate system dimension ("+coordinateSystem.getDimension()
                                                +") differ from node dimension ("+getDimension()+").");
        }
        this.coordinateSystem = coordinateSystem;
        rootToNodeDirty = true;
    }

    /**
     * {@inheritDoc }
     * @return 
     */
    public NodeTransform getNodeTransform() {
        return modelTransform;
    }

    /**
     * User defined properties.
     * Can be used to store additional informations on a node without
     * creating a new sub class.
     * 
     * @return Dictionary, never null
     */
    public synchronized Dictionary getProperties() {
        if(properties==null)properties = new HashDictionary();
        return properties;
    }

    /**
     * {@inheritDoc }
     */
    public synchronized Affine getRootToNodeSpace(){
        if(rootToNodeDirty){
            rootToNodeDirty = false;
            rootToNode.set(getNodeTransform().invert());
            SceneNode parent = getParent();
            if(parent != null){
                
                //adjust transform if coordinate system are different
                if(coordinateSystem!=null){
                    CoordinateSystem parentCs = parent.getCoordinateSystem();
                    if(parentCs!=null && !coordinateSystem.equals(parentCs)){
                        final Affine trs = (Affine) CoordinateSystems.createTransform(parentCs, coordinateSystem);
                        rootToNode.localMultiply(trs);
                    }
                }
                
                final Affine pm = parent.getRootToNodeSpace();
                rootToNode.localMultiply(pm);
            }
        }
        return rootToNode;
    }
    
    /**
     * {@inheritDoc }
     */
    public synchronized Affine getNodeToRootSpace(){
        if(nodeToRootDirty){
            nodeToRootDirty = false;
            getRootToNodeSpace().invert(nodeToRoot);
        }
        return nodeToRoot;
    }
    
    private synchronized void setCacheTransformsDirty(){
        if(rootToNodeDirty && nodeToRootDirty) return;
        rootToNodeDirty=true;
        nodeToRootDirty=true;
        for(Object n : getChildren().toArray()){
            if(n instanceof DefaultSceneNode){
                ((DefaultSceneNode)n).setCacheTransformsDirty();
            }
        }
    }

    /**
     * {@inheritDoc }
     * @param wtn 
     */
    public void setRootToNodeSpace(NodeTransform wtn) {
        setRootToNodeSpace((Affine)wtn);
    }

    /**
     * {@inheritDoc }
     * @param wtn 
     */
    public void setRootToNodeSpace(Affine wtn) {
        if(parent==null){
            //update the local transform
            getNodeTransform().set(wtn.invert());
        }else{
            //calculate difference from parent
            Affine m = parent.getNodeToRootSpace();
            m = wtn.multiply(m);
            getNodeTransform().set(m.invert());
        }
        setCacheTransformsDirty();
    }

    /**
     * {@inheritDoc }
     * @return 
     */
    public SceneNode getParent() {
        return (SceneNode) parent;
    }

    /**
     *  {@inheritDoc }
     * @param parent 
     */
    private void setParent(SceneNode parent) {
        final boolean changed = !CObjects.equals(this.parent, parent);
        if (changed) {
            final Object oldVal = this.parent;
            this.parent = parent;

            if (parent!=null && parent.getDimension()!=getDimension() ) {
                throw new InvalidArgumentException("Parent dimension differ.");
            }

            setCacheTransformsDirty();
            sendPropertyEvent(this, PROPERTY_PARENT, oldVal, parent);
        }
    }

    /**
     * {@inheritDoc }
     * @return root node
     */
    public SceneNode getRoot(){
        if(parent != null){
            return parent.getRoot();
        }
        //this is the root
        return this;
    }
    
    /**
     * {@inheritDoc }
     * Set parent reference on new children nodes.
     * 
     * @param array
     * @param index 
     */
    protected void addChildren(int index, Object[] array) {
        for(Object n : array){
            //search in the parents if we did not create a loop
            SceneNode p = getParent();
            while(p!=null){
                if(p==n){
                    throw new RuntimeException("Loop in scene node hierarchy");
                }
                p = p.getParent();
            }
            //check the node is not already in the list
            if(getChildren().contains(n)){
                throw new InvalidArgumentException("Object is already a children of this node : "+n);
            }

            childAdded((Node) n);
        }
        super.addChildren(index, array);
    }

    protected void replaceChildren(Object[] children){
        final Object[] oldChildren = getChildren().toArray();
        for(Object child : oldChildren){
            if(!Arrays.containsIdentity(children, 0, children.length, child)){
                childRemoved((Node) child);
            }
        }
        for(Object n : children){
            //search in the parents if we did not create a loop
            SceneNode p = getParent();
            while(p!=null){
                if(p==n){
                    throw new RuntimeException("Loop in scene node hierarchy");
                }
                p = p.getParent();
            }
            if(!Arrays.containsIdentity(oldChildren, 0, oldChildren.length, n)){
                //new children
                childAdded((Node) n);
            }
        }
        super.replaceChildren(children);
    }

    /**
     * {@inheritDoc }
     * Removes parent reference on children node.
     * 
     * @param index 
     */
    protected boolean removeChildNode(int index){
        childRemoved((Node) children.get(index));
        return super.removeChildNode(index);
    }

    protected boolean removeChildren() {
        for(Object child : getChildren().toArray()){
            childRemoved((Node) child);
        }
        return super.removeChildren();
    }
    
    /**
     * Removes parent reference.
     * 
     * @param child 
     */
    private void childRemoved(Node child){
        if(child instanceof DefaultSceneNode){
            ((DefaultSceneNode)child).setParent(null);
        }
    }
    
    /**
     * Sets parent reference.
     * 
     * @param child 
     */
    private void childAdded(Node child){
        if(child instanceof SceneNode){
            final SceneNode previous = ((SceneNode)child).getParent();
            if(previous!=null) previous.getChildren().remove(child);
            ((DefaultSceneNode)child).setParent(this);
        }
    }
    
    /**
     * Catch transform change to update world transform.
     */
    private final class NotifiedNodeTransform extends NodeTransform{

        public NotifiedNodeTransform(int dimension) {
            super(dimension);
        }

        public NotifiedNodeTransform(DefaultMatrix rotation, Vector scale, Vector translation) {
            super(rotation, scale, translation);
        }

        public void notifyChanged() {
            setCacheTransformsDirty();
            super.notifyChanged();
        }

    }

}
