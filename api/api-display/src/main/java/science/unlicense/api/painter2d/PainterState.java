
package science.unlicense.api.painter2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public interface PainterState {

    Affine2 getTransform();

    Geometry2D getClip();

    AlphaBlending getAlphaBlending();

    Paint getPaint();

    Brush getBrush();

    Font getFont();

}
