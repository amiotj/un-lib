
package science.unlicense.api.layout;

/**
 * Defines a widget constraint in the layout.
 * 
 * @author Johann Sorel
 */
public interface LayoutConstraint {
    
}
