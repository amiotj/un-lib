
package science.unlicense.api.scenegraph.s2d;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.impl.math.Vector;

/**
 * 
 * @author Johann Sorel
 */
public class TextNode2D extends GraphicNode2D{

    private CharArray text;
    private Font font;
    protected Paint[] fills = null;
    private Vector anchor;
    
    public TextNode2D() {
        super();
    }

    public TextNode2D(CoordinateSystem cs) {
        super(cs);
    }

    public CharArray getText() {
        return text;
    }

    public void setText(CharArray text) {
        this.text = text;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Paint[] getFills() {
        return fills;
    }

    public void setFills(Paint[] fills) {
        this.fills = fills;
    }

    public Vector getAnchor() {
        return anchor;
    }

    public void setAnchor(Vector anchor) {
        this.anchor = anchor;
    }
    
}
