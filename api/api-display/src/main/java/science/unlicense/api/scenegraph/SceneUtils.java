

package science.unlicense.api.scenegraph;

import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.impl.math.DefaultAffine;

/**
 * Utilities for scene and scene nodes.
 *
 * @author Johann Sorel
 */
public final class SceneUtils {

    private static final NodeVisitor INVERT_WORLD_TO_NODE = new DefaultNodeVisitor(){
            /**
             * @param node
             * @param root world root node
             */
            public Object visit(Node node, Object root) {
                if(node instanceof SceneNode){
                    //visit child before this node
                    super.visit(node, root);
                    if(node != root){
                        reverseWorldToNodeTrs((SceneNode) node, true);
                    }
                }
                return null;
            }
        };

    private static final NodeVisitor INVERT_PARENT_TO_NODE = new DefaultNodeVisitor(){
            /**
             * @param node
             * @param root world root node
             */
            public Object visit(Node node, Object root) {
                if(node instanceof SceneNode){
                    if(node != root){
                        reverseParentToNodeTrs((SceneNode) node);
                    }
                    super.visit(node, root);
                }
                return null;
            }
        };

    private SceneUtils() {}

    /**
     * When all scene node transforms are world root to node transforms.
     * This method will rebuild the parent to node transforms.
     */
    public static void reverseWorldToNodeTrsRecursive(SceneNode worldRoot){
        worldRoot.accept(INVERT_WORLD_TO_NODE, worldRoot);
    }

    /**
     * reverse a single node world to node transform.
     * @param candidate
     * @param parentInWorldTrs true if the parent is in world space
     */
    public static void reverseWorldToNodeTrs(SceneNode candidate, boolean parentInWorldTrs){
        final SceneNode parent = candidate.getParent();
        if(parent != null){
            final Affine prmatrix;
            if(parentInWorldTrs){
                prmatrix = parent.getNodeTransform();
            }else{
                prmatrix = parent.getNodeToRootSpace();
            }
            final Affine jtmatrix = candidate.getNodeTransform();
            final AffineRW m = prmatrix.invert();
            m.localMultiply(jtmatrix);
            candidate.getNodeTransform().set(m);
        }
    }

    /**
     * When all joints transforms are parent to node transforms.
     * This method will rebuild the world to node transforms.
     */
    public static void reverseParentToNodeTrsRecursive(SceneNode worldRoot){
        worldRoot.accept(INVERT_PARENT_TO_NODE, worldRoot);
    }

    public static void reverseParentToNodeTrs(SceneNode candidate){
        final SceneNode parent = candidate.getParent();
        if(parent != null){
            final Affine prmatrix = parent.getNodeTransform();
            final Affine jtmatrix = candidate.getNodeTransform();
            final AffineRW m = DefaultAffine.create(prmatrix);
            m.localMultiply(jtmatrix);
            candidate.getNodeTransform().set(m);
        }
    }

}
