
package science.unlicense.api.painter2d;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.math.Affine2;

/**
 * A Font holds a description of a text glyphs including
 * size, weight and family.
 * 
 * @author Johann Sorel
 */
public class Font {
    
    public static int WEIGHT_NONE = 0;
    public static int WEIGHT_BOLD = 1;
    
    //from SVG : 10.12 Text decoration
    public static int DECORATION_NON = 0;
    public static int DECORATION_UNDERLINE = 1;
    public static int DECORATION_OVERLINE = 2;
    public static int DECORATION_LINE_THROUGHT = 3;
    public static int DECORATION_BLINK = 4;
    
    private Chars[] families;
    private float size = 8;
    private int weight;
    private final Affine2 sizeMatrix;
    
    private FontMetadata baseMetadata = null;
    private FontMetadata adjustedMetadata = null;

    /**
     * 
     * @param families, font family by preference order.
     * @param size font size
     * @param weight font weight
     */
    public Font(Chars[] families, float size, int weight) {
        this.families = families;
        this.size = size;
        this.weight = weight;
        
        getMetadata();
        final double scale = size/baseMetadata.getAscent();
        sizeMatrix = new Affine2();
        sizeMatrix.set(0, 0, scale);
        sizeMatrix.set(1, 1, scale);
    }

    public Chars[] getFamilies() {
        return families;
    }

    public float getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }
    
    public FontMetadata getMetadata(){
        if(adjustedMetadata == null){
            baseMetadata = FontContext.INSTANCE.getMetadata(getFamilies()[0]);
            adjustedMetadata = FontContext.INSTANCE.getMetadata(this);
        }
        return adjustedMetadata;
    }
    
    public Geometry2D getGlyph(int unicode){
        final Geometry2D geom = FontContext.INSTANCE.getGlyph(this, unicode);
        if(geom==null) return null;
        return new TransformedGeometry2D(geom, sizeMatrix);
    }
    
    public Geometry2D getGlyph(Char character){
        final Geometry2D geom = FontContext.INSTANCE.getGlyph(this, character);
        if(geom==null) return null;
        return new TransformedGeometry2D(geom, sizeMatrix);
    }
        
    /**
     * Affine2 used to scale font glyph to expected size.
     * 
     * @return Affine2
     */
    public Affine2 getGlyphToSizeMatrix(){
        return sizeMatrix;
    }
    
    /**
     * Calculate the character X offset in given char array.
     * 
     * @param font
     * @param text
     * @param index
     * @return 
     */
    public static double calculateOffset(Font font, CharArray text, int index){
        final FontMetadata metadata = font.getMetadata();
        final CharIterator ite = text.createIterator();
        final double spacing = metadata.getAdvanceWidth(' ');
        
        double offset = 0.0;
        while(ite.hasNext() && index>0){
            index--;
            final int c = ite.nextToUnicode();
            if(c == ' '){
                //TODO handle control caracters
                offset += spacing;
            }else{
                //TODO there is an odd advance value for the 's' character in tuffy font
                offset += metadata.getAdvanceWidth(c);
            }
        }
        return offset;
    }
    
}
