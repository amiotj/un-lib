
package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;
import science.unlicense.api.geometry.BBox;

/**
 * Layout placing component at positions : TOP,BOTTOM,LEFt,RIGHT,CENTER
 *
 * @author Johann Sorel
 */
public class BorderLayout extends AbstractLayout{

    public BorderLayout() {
        super(BorderConstraint.class);
    }

    public BorderLayout(Sequence positionable) {
        super(BorderConstraint.class);
        setPositionables(positionable);
    }

    public Positionable getTop(){
        return getElement(BorderConstraint.TOP);
    }
    
    public Positionable getBottom(){
        return getElement(BorderConstraint.BOTTOM);
    }
    
    public Positionable getLeft(){
        return getElement(BorderConstraint.LEFT);
    }
    
    public Positionable getRight(){
        return getElement(BorderConstraint.RIGHT);
    }
    
    private Positionable getElement(BorderConstraint cstt){
        final Positionable[] children = getPositionableArray();
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(!(lcst instanceof BorderConstraint)) continue;
            final BorderConstraint cst = (BorderConstraint)lcst;
            if(cst == cstt) return element;
        }
        return null;
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_LAYOUT_CONSTRAINT.equals(propertyName)
               || Positionable.PROPERTY_EXTENTS.equals(propertyName)
               || Positionable.PROPERTY_RESERVE_SPACE.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {
        
        //find all located elements
        Positionable top = null;
        Positionable bottom = null;
        Positionable left = null;
        Positionable right = null;
        Positionable center = null;

        final Positionable[] children = getPositionableArray();
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(!(lcst instanceof BorderConstraint)) continue;
            final BorderConstraint cst = (BorderConstraint)lcst;
            if(!element.isReserveSpace() && !element.isVisible()) continue;

            if(cst == BorderConstraint.TOP) top = element;
            else if(cst == BorderConstraint.BOTTOM) bottom = element;
            else if(cst == BorderConstraint.LEFT) left = element;
            else if(cst == BorderConstraint.RIGHT) right = element;
            else if(cst == BorderConstraint.CENTER) center = element;
        }

        Extents topExt = EMPTIES;
        Extents bottomExt = EMPTIES;
        Extents leftExt = EMPTIES;
        Extents rightExt = EMPTIES;
        Extents centerExt = EMPTIES;
        
        //start by top and bottom elements
        final Extent cst = new Extent.Double(constraint.get(0), Double.POSITIVE_INFINITY);
        if(top!=null) topExt = top.getExtents(null, cst);
        if(bottom!=null) bottomExt = bottom.getExtents(null, cst);
        
        //adjust constraint height for left and right elements
        cst.set(0, Double.POSITIVE_INFINITY);
        cst.set(1, constraint.get(1)-topExt.bestY-bottomExt.bestY);
        
        if(left!=null) leftExt = left.getExtents(null, cst);
        if(right!=null) rightExt = right.getExtents(null, cst);
        if(center!=null) centerExt = center.getExtents(null, null);

        extents.minX = Maths.max(
                Maths.max(topExt.minX, bottomExt.minX),
                leftExt.minX+centerExt.minX+rightExt.minX)
                ;
        extents.minY = 
                Maths.max(Maths.max(leftExt.minY,rightExt.minY), centerExt.minY)
                + topExt.minY
                + bottomExt.minY
                ;
        
        extents.bestX = Maths.max(
                Maths.max(topExt.bestX, bottomExt.bestX),
                leftExt.bestX+centerExt.bestX+rightExt.bestX)
                ;
        extents.bestY = 
                Maths.max(Maths.max(leftExt.bestY,rightExt.bestY), centerExt.bestY)
                + topExt.bestY
                + bottomExt.bestY
                ;

        extents.maxX = Maths.max(
                Maths.max(topExt.maxX, bottomExt.maxX),
                leftExt.maxX+centerExt.maxX+rightExt.maxX)
                ;
        extents.maxY = 
                Maths.max(Maths.max(leftExt.maxY,rightExt.maxY), centerExt.maxY)
                + topExt.maxY
                + bottomExt.maxY
                ;
        
    }
    
    public void update() {

        final BBox inner = getView();

        //find all located elements
        Positionable top = null;
        Positionable bottom = null;
        Positionable left = null;
        Positionable right = null;
        Positionable center = null;

        final Positionable[] children = getPositionableArray();
        for(int i=0;i<children.length;i++){
            final Positionable element = children[i];
            final LayoutConstraint lcst = element.getLayoutConstraint();
            if(!(lcst instanceof BorderConstraint) || (!element.isReserveSpace() && !element.isVisible())){
                element.setEffectiveExtent(EMPTY);
                continue;
            }

            final BorderConstraint cst = (BorderConstraint)lcst;
            
            if(cst == BorderConstraint.TOP) top = element;
            else if(cst == BorderConstraint.BOTTOM) bottom = element;
            else if(cst == BorderConstraint.LEFT) left = element;
            else if(cst == BorderConstraint.RIGHT) right = element;
            else if(cst == BorderConstraint.CENTER) center = element;
        }

        final Extent constraint = inner.getExtent();
        Extents topExts = EMPTIES;
        Extents bottomExts = EMPTIES;
        Extents leftExts = EMPTIES;
        Extents rightExts = EMPTIES;
        Extents centerExts = EMPTIES;
        
        //start by top and bottom elements
        final Extent cst = new Extent.Double(constraint.get(0), Double.POSITIVE_INFINITY);
        if(top!=null) topExts = top.getExtents(null, cst);
        if(bottom!=null) bottomExts = bottom.getExtents(null, cst);
        
        //adjust constraint height for left and right elements
        cst.set(0, Double.POSITIVE_INFINITY);
        cst.set(1, constraint.get(1)-topExts.bestY-bottomExts.bestY);
        
        if(left!=null) leftExts = left.getExtents(null, cst);
        if(right!=null) rightExts = right.getExtents(null, cst);
        if(center!=null) centerExts = center.getExtents(null, null);

        
        Extent topExt = new Extent.Double(2);
        Extent bottomExt = new Extent.Double(2);
        Extent leftExt = new Extent.Double(2);
        Extent rightExt = new Extent.Double(2);
        Extent centerExt = new Extent.Double(2);
        if(top!=null) topExts.getBest(topExt);
        if(bottom!=null) bottomExts.getBest(bottomExt);
        if(left!=null) leftExts.getBest(leftExt);
        if(right!=null) rightExts.getBest(rightExt);
        if(center!=null) centerExts.getBest(centerExt);
        
        topExt.set(0, inner.getSpan(0));
        bottomExt.set(0, inner.getSpan(0));
        leftExt.set(1, inner.getSpan(1)-(topExt.get(1)+bottomExt.get(1)));
        rightExt.set(1, inner.getSpan(1)-(topExt.get(1)+bottomExt.get(1)));
        centerExt.set(0, inner.getSpan(0)-(leftExt.get(0)+rightExt.get(0)));
        centerExt.set(1, inner.getSpan(1)-(topExt.get(1)+bottomExt.get(1)));

        final double x = inner.getMin(0);
        final double y = inner.getMin(1);
        final double[] trs = new double[2];
        
        if(top!=null){
            trs[0] = x;
            trs[1] = y;
            trs[0] += topExt.get(0)/ 2.0;
            trs[1] += topExt.get(1)/ 2.0;
            top.setEffectiveExtent(topExt);
            top.getNodeTransform().setToTranslation(trs);
        }
        if(bottom!=null){
            trs[0] = x;
            trs[1] = y + leftExt.get(1)+topExt.get(1);
            trs[0] += bottomExt.get(0)/ 2.0;
            trs[1] += bottomExt.get(1)/ 2.0;
            bottom.setEffectiveExtent(bottomExt);
            bottom.getNodeTransform().setToTranslation(trs);
        }
        if(left!=null){
            trs[0] = x;
            trs[1] = y + topExt.get(1);
            trs[0] += leftExt.get(0)/ 2.0;
            trs[1] += leftExt.get(1)/ 2.0;
            left.setEffectiveExtent(leftExt);
            left.getNodeTransform().setToTranslation(trs);
        }
        if(right!=null){
            trs[0] = x + leftExt.get(0)+centerExt.get(0);
            trs[1] = y + topExt.get(1);
            trs[0] += rightExt.get(0)/ 2.0;
            trs[1] += rightExt.get(1)/ 2.0;
            right.setEffectiveExtent(rightExt);
            right.getNodeTransform().setToTranslation(trs);
        }
        if(center!=null){
            trs[0] = x + leftExt.get(0);
            trs[1] = y + topExt.get(1);
            trs[0] += centerExt.get(0)/ 2.0;
            trs[1] += centerExt.get(1)/ 2.0;
            center.setEffectiveExtent(centerExt);
            center.getNodeTransform().setToTranslation(trs);
        }

    }

}