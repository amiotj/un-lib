

package science.unlicense.api.layout;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.AbstractIterator;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Triplet;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 * Place each element next to other or on the next line is there is no space left.
 * Each element will be affected the same space.
 * The number of element per line is decided by the parent width.
 * 
 * @author Johann Sorel
 */
public class BlockLayout extends AbstractLayout{

    private final Extent blockSize = new Extent.Double(100, 100);

    public BlockLayout() {
        super(null);
    }

    public BlockLayout(Extent ext) {
        super(null);
        blockSize.set(ext);
    }
    
    public Extent getBlockSize() {
        return blockSize.copy();
    }
    
    public void setBlockSize(Extent ext){
        blockSize.set(ext);
        setDirty();
    }
    
    /**
     * Size of the current grid.
     * 
     * @return int[]{nbPerLine,nbLine}
     */
    public int[] getGridSize(){
        final Positionable[] children = getPositionableArray();
        
        final BBox ext = getView();
        final int nbchild = children.length;
        int nbPerLine = (int) (ext.getSpan(0) / blockSize.get(0));
        //we need at least one element per line even if it will be crop
        if(nbPerLine<1) nbPerLine = 1;
        
        final int nbLine = (int) (((double)nbchild / (double)nbPerLine) +0.5);
        return new int[]{nbPerLine,nbLine};
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if( Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {
        final int[] gridSize = getGridSize();
        
        extents.minX = gridSize[0]*blockSize.get(0);
        extents.minY = gridSize[1]*blockSize.get(1);
        extents.bestX = extents.minX;
        extents.bestY = extents.minY;
        extents.maxX = Double.POSITIVE_INFINITY;
        extents.maxY = Double.POSITIVE_INFINITY;
    }

    public void update() {
        
        final BBox ext = getView();
        final Positionable[] children = getPositionableArray();
        int nbPerLine = (int) (ext.getSpan(0) / blockSize.get(0));
        //we need at least one element per line even if it will be crop
        if(nbPerLine<1) nbPerLine = 1;
                
        final double cellx = blockSize.get(0);
        final double celly = blockSize.get(1);
        final double trsx = ext.getMin(0) + cellx/2.0 ;
        final double trsy = ext.getMin(1) + celly/2.0 ;
        int x=0;
        int y=0;
        double[] trs = new double[2];
        for(int i=0;i<children.length;i++){
            final Positionable w = children[i];
                        
            trs[0] = trsx + x*cellx;
            trs[1] = trsy + y*celly;
            w.setEffectiveExtent(blockSize);
            w.getNodeTransform().setToTranslation(trs);
                        
            //prepare next iteration
            x++;
            if(x>=nbPerLine){
                x=0;
                y++;
            }
        }
        
    }

    /**
     * Optimize children search.
     * 
     * @param dirtyBBox
     * @return 
     */
    public Iterator getChildren(BBox dirtyBBox) {
        final Positionable[] children = getPositionableArray();
        
        final BBox ext = getView();
        final int nbchild = children.length;
        int nbPerLine = (int) (ext.getSpan(0) / blockSize.get(0));
        //we need at least one element per line even if it will be crop
        if(nbPerLine<1) nbPerLine = 1;
        final int nbLine = (int) (((double)nbchild / (double)nbPerLine) +0.5);
        
        //calculate first and last intersecting lines
        int firstLine = Maths.clamp((int)Math.floor(dirtyBBox.getMin(1) / blockSize.get(1)),0,nbLine);
        int lastLine  = Maths.clamp((int)Math.ceil (dirtyBBox.getMax(1) / blockSize.get(1)),0,nbLine);
        final int firstIndex = firstLine*nbPerLine;
        final int lastIndex = Math.min( (lastLine+1)*nbPerLine, nbchild);
        
        double minX = dirtyBBox.getMin(0);
        double minY = dirtyBBox.getMin(1);
        double maxX = dirtyBBox.getMax(0);
        double maxY = dirtyBBox.getMax(1);
        final double[] dirtyCoords = new double[]{
            minX,minY,
            minX,maxY,
            maxX,maxY,
            maxX,minY
        };
        final double[] childDirty = new double[8];
        final BBox childDirtyArea = new BBox(2);
        
        return new AbstractIterator() {
            private int i=firstIndex;
            protected void findNext() {
                for(;nextValue==null && i<lastIndex;i++){
                    final Positionable child = children[i];
                    if(!child.getEffectiveExtent().isEmpty()){
                        final NodeTransform trs = child.getNodeTransform();
                        trs.inverseTransform(dirtyCoords, 0, childDirty, 0, 4);
                        childDirtyArea.setRange(0, childDirty[0], childDirty[0]);
                        childDirtyArea.setRange(1, childDirty[1], childDirty[1]);
                        childDirtyArea.expand(childDirty[2], childDirty[3]);
                        childDirtyArea.expand(childDirty[4], childDirty[5]);
                        childDirtyArea.expand(childDirty[6], childDirty[7]);
                        nextValue = new Triplet(child,trs,childDirtyArea);
                    }
                }
            }
        };
        
    }
    
}
