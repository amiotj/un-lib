
package science.unlicense.api.layout;

import science.unlicense.api.collection.AbstractIterator;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Triplet;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 * Common structure for layout instances.
 *
 * @author Johann Sorel
 */
public abstract class AbstractLayout extends AbstractEventSource implements Layout{

    protected static final Extent EMPTY = new Extent.Double(0, 0);
    protected static final Extents EMPTIES = new Extents();
    
    private final EventListener childListener = new EventListener() {
        public void receiveEvent(Event event) {
            receiveChildEvent(event);
        }
    };
    
    private final EventListener sequenceListener = new EventListener() {
        public void receiveEvent(Event event) {
            CollectionMessage ce = (CollectionMessage) event.getMessage();
            final int type = ce.getType();
            if(type == NodeMessage.TYPE_ADD){
                //attached event listener on new elements
                final Object[] elements = ce.getNewElements();
                for(int i=0;i<elements.length;i++){
                    ((Positionable)elements[i]).addEventListener(PropertyMessage.PREDICATE, childListener);
                }
                setDirty();
            }else if(type == NodeMessage.TYPE_REMOVE){
                //remove event listener on new elements
                final Object[] elements = ce.getOldElements();
                for(int i=0;i<elements.length;i++){
                    ((Positionable)elements[i]).removeEventListener(PropertyMessage.PREDICATE, childListener);
                }
                setDirty();
            }else if(type == NodeMessage.TYPE_REPLACEALL){
                //remove event listener on new elements
                final Object[] olds = ce.getOldElements();
                for(int i=0;i<olds.length;i++){
                    ((Positionable)olds[i]).removeEventListener(PropertyMessage.PREDICATE, childListener);
                }
                //attached event listener on new elements
                final Object[] news = ce.getNewElements();
                for(int i=0;i<news.length;i++){
                    ((Positionable)news[i]).addEventListener(PropertyMessage.PREDICATE, childListener);
                }
                setDirty();
            }
        }
    };
    
    private Sequence positionables;
    private boolean extentDirty = true;
    
    private final BBox workingExtent = new BBox(2);
    //extents for current working extent
    private final Extents extents = new Extents();

    private final Class layoutConstraintClass;

    public AbstractLayout(Class layoutConstraintClass) {
        this.layoutConstraintClass = layoutConstraintClass;
    }

    public Class getLayoutConstraintClass() {
        return layoutConstraintClass;
    }

    public final BBox getView() {
        synchronized (workingExtent){
            return new BBox(workingExtent);
        }
    }

    public final void setView(BBox bbox) {
        if (!bbox.isValid()) {
            throw new InvalidArgumentException("Unvalid bbox "+bbox);
        }
        final BBox old;
        synchronized (workingExtent){
            if(this.workingExtent.equals(bbox)) return;
            old = new BBox(workingExtent);
            this.workingExtent.set(bbox);
        }
        sendPropertyEvent(this, PROPERTY_VIEW, old, new BBox(bbox));
        setDirty();
    }
    
    public final Extents getExtents() {
        updateExtents();
        return extents.copy();
    }

    public Extents getExtents(Extents buffer, Extent constraint) {
        if(buffer==null) buffer = new Extents();
        calculateExtents(buffer, constraint);
        return buffer;
    }
    
    public void setPositionables(Sequence positionables) {
        if(this.positionables==positionables) return;
        
        //remove listener on previous collection
        if(this.positionables!=null){
            this.positionables.removeEventListener(CollectionMessage.PREDICATE, sequenceListener);
            for(int i=0,n=this.positionables.getSize();i<n;i++){
                ((Positionable)this.positionables.get(i)).removeEventListener(PropertyMessage.PREDICATE, childListener);
            }
        }
        this.positionables = positionables;
        
        //listen to positionable list changes
        if(this.positionables!=null){
            this.positionables.addEventListener(CollectionMessage.PREDICATE, sequenceListener);
            for(int i=0,n=this.positionables.getSize();i<n;i++){
                ((Positionable)this.positionables.get(i)).addEventListener(PropertyMessage.PREDICATE, childListener);
            }
        }
        setDirty();
    }
    
    public Positionable[] getPositionableArray(){
        if(positionables==null) return new Positionable[0];
        final Positionable[] array = new Positionable[positionables.getSize()];
        Collections.copy(positionables, array, 0);
        return array;
    }
    
    protected final void setDirty() {
        synchronized(this){
            this.extentDirty = true;
        }
        sendPropertyEvent(this, PROPERTY_DIRTY, false, true);
    }

    private synchronized void updateExtents(){
        if(extentDirty){
            extentDirty = false;
            calculateExtents(extents, workingExtent.getExtent());
        }
    }

    /**
     * Calculate extend, without border.
     */
    protected abstract void calculateExtents(Extents extents, Extent constraint);

    protected abstract void receiveChildEvent(Event event);
    
    public final Iterator getPositionables(final BBox searchArea){
        final Positionable[] children = getPositionableArray();
        if(children.length==0){
            return Collections.emptyIterator();
        }
              
        return new AbstractIterator() {
            private final BBox childSearchArea = new BBox(2);  
            private int i=0;
            protected void findNext() {
                for(;nextValue==null && i<children.length;i++){
                    final Positionable child = children[i];
                    final Extent extent = child.getEffectiveExtent();
                    if(extent.isEmpty()) continue;
                    final NodeTransform trs = child.getNodeTransform();
                    
                    //check intersection with the dirty box
                    Geometries.transform(searchArea, trs.inverseAsMatrix(), childSearchArea);

                    //test intersection
                    if(childSearchArea.intersects(child.getBoundingBox(), false)){
                        nextValue = new Triplet(child,trs,childSearchArea);
                    }
                }
            }
        };
        
    }
    
    public final Class[] getEventClasses() {
        return new Class[]{
            PropertyMessage.class
        };
    }

}