
package science.unlicense.api.desktop;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.Property;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Tuple;

/**
 * A Frame is rectangle shape displayed on the desktop.
 *
 * @author Johann Sorel
 */
public interface Frame extends EventSource{

    public static final Chars PROP_TITLE = new Chars("Title");
    public static final Chars PROP_ON_SCREEN_LOCATION = new Chars("OnScreenLocation");
    public static final Chars PROP_SIZE = new Chars("Size");
    public static final Chars PROP_VISIBLE = new Chars("Visible");
    public static final Chars PROP_MAXIMIZABLE = new Chars("Maximizable");
    public static final Chars PROP_MINIMIZABLE = new Chars("Minimizable");
    public static final Chars PROP_CLOSABLE = new Chars("Closable");
    public static final Chars PROP_ALWAYSONTOP = new Chars("AlwaysOnTop");
    public static final Chars PROP_CURSOR = new Chars("Cursor");
    public static final Chars PROP_STATE = new Chars("State");
    public static final Chars PROP_DISPOSED = new Chars("Disposed");
    
    public static final int STATE_NORMAL = 0;
    public static final int STATE_MINIMIZED = 1;
    public static final int STATE_MAXIMIZED = 2;
    public static final int STATE_MAXIMIZED_VERTICAL = 3;
    public static final int STATE_MAXIMIZED_HORIZONTAL = 4;
    public static final int STATE_FULLSCREEN = 5;

    /**
     * Get parent frame.
     *
     * @return parent frame, can be null.
     */
    Frame getParentFrame();

    /**
     * Get children frames if any.
     * 
     * @return children frames, never null, can be empty.
     */
    Sequence getChildrenFrames();

    /**
     * Get frame manager.
     * 
     * @return FrameManager who created this frame.
     */
    FrameManager getManager();

    /**
     * Get frame title.
     * 
     * @return frame title, can be null
     */
    CharArray getTitle();

    /**
     * Set frame title.
     * 
     * @param title , can be null
     */
    void setTitle(CharArray title);
    
    /**
     * Title property.
     */
    Property varTitle();
    
    /**
     * Get frame base cursor.
     * 
     * @return Cursor, can be null
     */
    Cursor getCursor();
    
    /**
     * 
     * @param cursor, null to hide cursor.
     */
    void setCursor(Cursor cursor);

    /**
     * Set frame location on screen.
     * 
     * @param location 
     */
    void setOnScreenLocation(Tuple location);

    /**
     * Get frame location on screen.
     * 
     * @return frame location on screen.
     */
    Tuple getOnScreenLocation();

    /**
     * Set frame size.
     * 
     * @param width
     * @param height 
     */
    void setSize(int width, int height);

    /**
     * Get frame size.
     * 
     * @return size
     */
    Extent getSize();

    /**
     * Set frame visible.
     * 
     * @param visible 
     */
    void setVisible(boolean visible);

    /**
     * Get frame visibility.
     * 
     * @return true if frame is visible.
     */
    boolean isVisible();

    /**
     * Visible property.
     */
    Property varVisible();
    
    /**
     * Get frame modale state.
     *
     * @return true if frame is modale
     */
    boolean isModale();

    /**
     * Create a system dependant frame decoration.
     * 
     * @return FrameDecoration
     */
    FrameDecoration getSystemDecoration();
    
    /**
     * 
     * @param decoration frame decoration, null to make frame undecorated
     */
    void setDecoration(FrameDecoration decoration);
    
    /**
     * 
     * @return frame decoration, can be null.
     */
    FrameDecoration getDecoration();
    
    /**
     * A frame can be in only one state at the time.
     * States include :
     * - normal
     * - minimized
     * - maximized (horizontal,vertical,both)
     * - fullscreen
     * 
     * @return current frame state
     */
    int getState();
    
    /**
     * Set frame state.
     * 
     * @param state 
     */
    void setState(int state);
    
    /**
     * Set frame maximizable state.
     * 
     * @param maximizable 
     */
    void setMaximizable(boolean maximizable);

    /**
     * Get frame maximizable state.
     * 
     * @return true if frame is maximizable.
     */
    boolean isMaximizable();
    
    /**
     * Maximizable property.
     */
    Property varMaximizable();
    
    /**
     * Set frame minimizable state.
     * 
     * @param minimizable 
     */
    void setMinimizable(boolean minimizable);

    /**
     * Get frame minimizable state.
     * 
     * @return true if frame is minimizable.
     */
    boolean isMinimizable();
    
    /**
     * Minimizable property.
     */
    Property varMinimizable();
    
    /**
     * Set frame closable state.
     * 
     * @param closable 
     */
    void setClosable(boolean closable);

    /**
     * Get frame closable state.
     * 
     * @return true if frame is closable.
     */
    boolean isClosable();
        
    /**
     * Closable property.
     */
    Property varClosable();
    
    /**
     * Ask to always put the frame on top of any other.
     * 
     * @param ontop 
     */
    void setAlwaysonTop(boolean ontop);

    /**
     * Get frame on-top state.
     * 
     * @return true if frame is always on top.
     */
    boolean isAlwaysOnTop();

    /**
     * Get frame translucent state.
     * 
     * @return true if frame has a translucent background
     */
    boolean isTranslucent();

    /**
     * Release all resources used by the frame.
     * It should not be used after this call.
     */
    void dispose();

    /**
     * Indicate if frame has been disposed.
     */
    boolean isDisposed();

    /**
     * Block calling thread until this frame has been disposed.
     * 
     * TODO : seems strange, find a better way to implement this need
     */
    void waitForDisposal();

}
