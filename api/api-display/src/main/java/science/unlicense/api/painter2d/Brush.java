
package science.unlicense.api.painter2d;

import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.api.image.Image;

/**
 * A Brush is used to render the outline of a geometry.
 * It is responsible for giving the shape (area which will be filled)
 * of the geometry outline.
 *
 * @author Johann Sorel
 */
public interface Brush {

    Geometry2D outline(Geometry2D geom, double[] resolution);

    void bitMask(Geometry2D geom, double[] resolution,Image flagImage);
    
    /**
     * Get the brush max width.
     * This does not have to be accurate, but mostly correct for other classes
     * to properly estimate the space used by the stroke.
     * @return double 0 or positive
     */
    double getMaxWidth();
    
}
