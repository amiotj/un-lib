

package science.unlicense.api.layout;

import science.unlicense.api.Sorter;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;

/**
 * A stack layout puts widgets above each other in a stack.
 * This allows to create overlay effects.
 * 
 * @author Johann Sorel
 */
public class StackLayout extends AbstractLayout {

    private static final Sorter STACK_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            final StackConstraint cst1 = (StackConstraint) ((Positionable)first).getLayoutConstraint();
            final StackConstraint cst2 = (StackConstraint) ((Positionable)second).getLayoutConstraint();
            return Float.compare(cst1.getPriority(), cst2.getPriority());
        }
    };

    public StackLayout() {
        super(StackConstraint.class);
    }
    
    protected void receiveChildEvent(Event event) {
        if(event.getMessage() instanceof PropertyMessage){
            final Chars propertyName =  ((PropertyMessage)event.getMessage()).getPropertyName();
            if(   Positionable.PROPERTY_LAYOUT_CONSTRAINT.equals(propertyName)
               || Positionable.PROPERTY_VISIBLE.equals(propertyName)){
                //a layout related property has changed
                setDirty();
            }
        }
    }
    
    protected void calculateExtents(Extents extents, Extent constraint) {
        final Sequence seq = getValidChildren();
        extents.setAll(0,0);
        
        final Extents buffer = new Extents();
        for(int i=0,n=seq.getSize();i<n;i++){
            final Positionable child = (Positionable) seq.get(i);
            child.getExtents(buffer, null);
            extents.minX  = Maths.max(extents.minX,buffer.minX);
            extents.minY  = Maths.max(extents.minY,buffer.minY);
            extents.bestX = Maths.max(extents.bestX,buffer.bestX);
            extents.bestY = Maths.max(extents.bestY,buffer.bestY);
            extents.maxX  = Maths.max(extents.maxX,buffer.maxX);
            extents.maxY  = Maths.max(extents.maxY,buffer.maxY);
        }
    }

    public void update() {
        
        final BBox bbox = getView();
        final Extent ext = bbox.getExtent();
        final double[] trs = bbox.getMiddle().getValues();
        final Sequence seq = getValidChildren();
        for(int i=0,n=seq.getSize();i<n;i++){
            final Positionable child = (Positionable) seq.get(i);
            child.setEffectiveExtent(ext);
            child.getNodeTransform().setToTranslation(trs);
        }
    }
    
    private Sequence getValidChildren(){
        final Sequence seq = new ArraySequence();
        
        final Positionable[] children = getPositionableArray();
        for(int i=0;i<children.length;i++){
            final Positionable w = children[i];
            final LayoutConstraint constraint = w.getLayoutConstraint();
            if(constraint instanceof StackConstraint){
                seq.add(w);
            }
        }
        Collections.sort(seq, STACK_SORTER);
        return seq;
    }
    
}
