
package science.unlicense.api.desktop;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;

/**
 * Convenient methods to manipulate frame managers.
 *
 * @author Johann Sorel
 */
public final class FrameManagers {

    private FrameManagers(){}

    /**
     * Indicate if current system has a framing system.
     * @return true if at least one frame manager is available.
     */
    public static boolean isFrameless(){
        return getManagers().length == 0;
    }

    /**
     * Get the first frame manager.
     * @return FrameManager
     */
    public static FrameManager getFrameManager(){
        return getManagers()[0];
    }

    /**
     * Lists available frame managers.
     * @return array of ImageFormat, never null but can be empty.
     */
    public static FrameManager[] getManagers(){
        final NamedNode root = science.unlicense.system.System.get().getModuleManager().getMetadataRoot().search(
                new Chars("services/science.unlicense.api.desktop.FrameManager"));
        if(root==null){
            return new FrameManager[0];
        }
        return (FrameManager[]) Nodes.getChildrenValues(root, null).toArray(FrameManager.class);
    }

}
