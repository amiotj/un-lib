
package science.unlicense.api.painter2d;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.system.System;

/**
 * Provides informations on the available fonts, glyphs and metrics.
 *
 * @author Johann Sorel
 */
public final class FontContext {

    public static final FontContext INSTANCE = new FontContext();
    private static final Chars PATH = new Chars("services/science.unlicense.api.painter2d.FontStore");

    private FontContext(){}

    /**
     * Lists available font resolvers.
     * @return array of FontResolver, never null but can be empty.
     */
    public static FontStore[] getResolvers(){
        final NamedNode root = System.get().getModuleManager().getMetadataRoot().search(PATH);
        if(root==null){
            return new FontStore[0];
        }
        return (FontStore[]) Nodes.getChildrenValues(root, null).toArray(FontStore.class);
    }


    /**
     * Lists font resolver for given font family.
     *
     * @return FontResolver, can be null
     */
    public static FontStore getResolver(Chars family){
        final FontStore[] resolvers = getResolvers();
        for(int i=0;i<resolvers.length;i++){
            FontMetadata fm = resolvers[i].getMetadata(family);
            if(fm!=null) return resolvers[i];
        }
        return null;
    }

    /**
     * List all available font families.
     *
     * @return Sequence of CharSequences
     */
    public Sequence getFamilies() {
        final Sequence families = new ArraySequence();
        final FontStore[] resolvers = getResolvers();
        for(int i=0;i<resolvers.length;i++){
            families.addAll(resolvers[i].getFamilies());
        }
        return families;
    }

    /**
     * Get font metadatas.
     * @param font
     * @return
     */
    public FontMetadata getMetadata(Font font) {
        final Chars[] families = font.getFamilies();
        for(int i=0;i<families.length;i++){
            FontMetadata fm = getMetadata(families[i]);
            if(fm!=null){
                //derivate for given font size
                return fm.derivate(font.getSize());
            }
        }
        return null;
    }

    public FontMetadata getMetadata(Chars family) {
        final FontStore[] resolvers = getResolvers();
        for(int i=0;i<resolvers.length;i++){
            FontMetadata fm = resolvers[i].getMetadata(family);
            if(fm!=null) return fm;
        }
        return null;
    }

    public Geometry2D getGlyph(Font font, int unicode) {
        final FontStore[] resolvers = getResolvers();
        final Chars[] families = font.getFamilies();
        for(int k=0;k<families.length;k++){
            for(int i=0;i<resolvers.length;i++){
                final Geometry2D glyph = resolvers[i].getGlyph(unicode, families[k]);
                if(glyph!=null) return glyph;
            }
        }
        return null;
    }
    
    public Geometry2D getGlyph(Font font, Char character) {
        final FontStore[] resolvers = getResolvers();
        final Chars[] families = font.getFamilies();
        for(int k=0;k<families.length;k++){
            for(int i=0;i<resolvers.length;i++){
                final Geometry2D glyph = resolvers[i].getGlyph(character, families[k]);
                if(glyph!=null) return glyph;
            }
        }
        return null;
    }
}
