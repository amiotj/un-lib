

package science.unlicense.api.anim;

import science.unlicense.api.Orderable;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.PropertyMessage;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrame extends AbstractEventSource implements Orderable{
    
    public static final Chars PROPERTY_TIME = new Chars("Time");
    public static final Chars PROPERTY_VALUE = new Chars("Value");
    
    protected double time;
    protected Object value;

    public KeyFrame() {
        this(0.0);
    }
    
    public KeyFrame(double time) {
        this.time = time;
    }
    
    public KeyFrame(double time, Object value) {
        this.time = time;
        this.value = value;
    }

    /**
     * Get time position.
     * @return frame time in milliseconds
     */
    public double getTime() {
        return time;
    }

    /**
     * Set time position.
     * @param time in milliseconds
     */
    public void setTime(double time) {
        if(this.time == time) return;
        final double old = this.time;
        this.time = time;
        sendPropertyEvent(this, PROPERTY_TIME, old, this.time);
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        if(this.value == value) return;
        final Object old = this.value;
        this.value = value;
        sendPropertyEvent(this, PROPERTY_VALUE, old, this.value);
    }
    
    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    public int order(Object other) {
        final double otime = ((KeyFrame)other).time;
        final double r = time - otime;
        if(r>0) return 1;
        if(r<0) return -1;
        return 0;
    }
    
}
