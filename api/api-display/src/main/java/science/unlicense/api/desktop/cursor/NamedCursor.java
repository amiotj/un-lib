
package science.unlicense.api.desktop.cursor;

import science.unlicense.api.character.Chars;

/**
 * Named cursor are operating system dependant cursors.
 * The possible names should be retrieved from the FrameManager.
 * 
 * @author Johann Sorel
 */
public class NamedCursor implements Cursor {

    public static final NamedCursor DEFAULT = new NamedCursor(new Chars("default"));
    public static final NamedCursor NULL = new NamedCursor(new Chars("null"));
    
    private final Chars name;

    public NamedCursor(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }
    
}
