

package science.unlicense.api.painter2d;

import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;

/**
 * Painter2D which purpose is to produce an image.
 * 
 * @author Johann Sorel
 */
public interface ImagePainter2D extends Painter2D{
    
    void setSize(Extent.Long size);
    
    Extent.Long getSize();
    
    Image getImage();
    
}
