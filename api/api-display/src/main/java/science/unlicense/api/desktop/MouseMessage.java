
package science.unlicense.api.desktop;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.api.event.MessagePredicate;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.predicate.Predicate;

/**
 * A mouse event message.
 * 
 * @author Johann Sorel
 */
public class MouseMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(MouseMessage.class);

    public static final int BUTTON_1 = 1;
    public static final int BUTTON_2 = 2;
    public static final int BUTTON_3 = 3;
    
    public static final int TYPE_PRESS = 1;
    public static final int TYPE_RELEASE = 2;
    public static final int TYPE_TYPED = 3;
    public static final int TYPE_WHEEL = 4;
    public static final int TYPE_MOVE = 5;
    public static final int TYPE_ENTER = 6;
    public static final int TYPE_EXIT = 7;
    
    private final int type;
    private final int button;
    private final int nbClick;
    private final Tuple mousePosition;
    private final Tuple screenPosition;
    private final double wheelOffset;
    private final boolean dragging;

    /**
     * Create a new mouse event.
     * 
     * @param type one of MouseMessage.TYPE_X
     * @param button clicked button, -1 if none
     * @param nbClick number of mouse button clicks
     * @param mousePosition mouse position
     * @param screenPosition mouse screen position
     * @param wheelOffset wheel movement, 0 for none
     * @param dragging dragging must be true when the current event is a mouse move while a button is pressed
     */
    public MouseMessage(int type, int button, int nbClick, Tuple mousePosition,
            Tuple screenPosition, double wheelOffset, boolean dragging) {
        super(true);
        this.type = type;
        this.button = button;
        this.nbClick = nbClick;
        this.mousePosition = mousePosition;
        this.screenPosition = screenPosition;
        this.wheelOffset = wheelOffset;
        this.dragging = dragging;
    }

    /**
     * Mouse event type.
     * 
     * @return int, one of MouseMessage.TYPE_X
     */
    public int getType() {
        return type;
    }

    /**
     * Get the clicked button,
     * 
     * @return int, -1 if not a click event.
     */
    public int getButton() {
        return button;
    }

    /**
     * Get mouse button number of clicks.
     *
     * @return int, -1 if not a click event.
     */
    public int getNbClick() {
        return nbClick;
    }

    /**
     * Get the mouse position at this event.
     * 
     * @return Tuple, never null
     */
    public Tuple getMousePosition() {
        return mousePosition;
    }
    
    /**
     * Get the mouse screen position at this event.
     * 
     * @return Tuple, never null
     */
    public Tuple getMouseScreenPosition() {
        return screenPosition;
    }

    /**
     * Get the number of wheel movements.
     * 
     * @return double, wheel rotation
     */
    public double getWheelOffset() {
        return wheelOffset;
    }

    /**
     * Indicate if the current operation is a mouse drag.
     * 
     * @return true if drag
     */
    public boolean isDragging() {
        return dragging;
    }


    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("ME(");
        switch(type){
            case TYPE_ENTER : cb.append("ENTER");break;
            case TYPE_EXIT : cb.append("EXIT");break;
            case TYPE_MOVE : cb.append("MOVE");break;
            case TYPE_PRESS : cb.append("PRESS");break;
            case TYPE_RELEASE : cb.append("RELEASE");break;
            case TYPE_TYPED : cb.append("TYPED");break;
            case TYPE_WHEEL : cb.append("WHEEL");break;
        }
        cb.append(",Button-"+button);
        cb.append(')');
        
        return cb.toChars();
    }
    
    
}
