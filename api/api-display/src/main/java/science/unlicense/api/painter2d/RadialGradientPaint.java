
package science.unlicense.api.painter2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.impl.math.Affine2;

/**
 * LinearGradiant.
 * Similar to SVG.
 *
 * @author Johann Sorel
 */
public class RadialGradientPaint implements Paint {

    private final double centerX;
    private final double centerY;
    private final double radius;
    private final double focusX;
    private final double focusY;
    private final double[] positions;
    private final Color[] colors;

    public RadialGradientPaint(double centerX, double centerY,
            double radius, double focusX, double focusY, double[] positions, Color[] colors) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.focusX = focusX;
        this.focusY = focusY;
        this.positions = positions;
        this.colors = colors;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getRadius() {
        return radius;
    }

    public double getFocusX() {
        return focusX;
    }

    public double getFocusY() {
        return focusY;
    }

    public double[] getPositions() {
        return positions;
    }

    public Color[] getColors() {
        return colors;
    }

    public void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending) {

        //base image
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        final float[] sampleStore = new float[1];
        final float[] buffer = new float[4];

        //flag image
        final TupleIterator fite = flagImage.getRawModel().asTupleBuffer(flagImage).createIterator(flagBbox);

        final double[] tc = new double[2];

        for(byte[] flagSample = (byte[])fite.next();flagSample!=null;flagSample=(byte[])fite.next()){
            if(flagSample[0]!=0){
                //calculate and set color
                final int[] coord = fite.getCoordinate();
                tc[0] = coord[0];
                tc[1] = coord[1];
                trs.transform(tc, tc);
                double d = distance(centerX,centerY,tc[0],tc[1]) / radius;
                if(d<0) d=0;
                if(d>1) d=1;

                //find interval
                int index=0;
                for(;index<positions.length-1;index++){
                    if(d>= positions[index] && d<=positions[index+1]){
                        break;
                    }
                }
                d = (d-positions[index]) / (positions[index+1]-positions[index]);

                final Color color = Colors.interpolate(colors[index], colors[index+1], (float)d);

                final float alpha = (flagSample[0] & 0xff) / 255f;
                final float[] srcrgba = new float[]{
                    color.getRed(),
                    color.getGreen(),
                    color.getBlue(),
                    color.getAlpha()*alpha
                };
                final int[] coordinate = fite.getCoordinate();
                cm.getRGBA(coordinate, sampleStore);
                blending.blend(srcrgba,sampleStore,buffer);
                cm.setRGBA(coordinate, buffer);

            }
        }

    }

    static double distance(double startX, double startY, double endX, double endY){
        final double adj = endX-startX;
        final double opp = endY-startY;
        return Math.sqrt(adj*adj + opp*opp);
    }

}
