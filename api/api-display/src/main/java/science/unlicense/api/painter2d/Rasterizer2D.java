
package science.unlicense.api.painter2d;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 * A Rasterizer transforms vector geometries in gridd information datas.
 * 
 * @author Johann Sorel
 */
public interface Rasterizer2D {
    
    /**
     * 
     * @return 
     */
    Extent.Long getExtent();
    
    Image getMask();
    
    /**
     * Reset image on given area.
     * 
     * @param area 
     */
    void resetBBox(BBox area);
    
    /**
     * Rasterize given geometry.
     * The produced image contains a byte for each pixel.
     * 0 is for a pixel outside the geometry
     * 255 is for a pixel completely inside the geometry
     * 1-254 may be used to indicate partial inclusion
     * 
     * @param geom
     * @return Image, this image is a 1 band byte image
     */
    Image rasterize(Geometry2D geom);
    
    
}
