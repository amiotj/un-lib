
package science.unlicense.api.painter2d;

import science.unlicense.api.color.AlphaBlending;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class DefaultPainterState implements PainterState {

    private final Affine2 transform;
    private final Geometry2D clip;
    private final AlphaBlending blending;
    private final Paint paint;
    private final Brush brush;
    private final Font font;

    public DefaultPainterState(Affine2 transform, Geometry2D clip, AlphaBlending blending, Paint paint, Brush brush, Font font) {
        this.transform = transform;
        this.clip = clip;
        this.blending = blending;
        this.paint = paint;
        this.brush = brush;
        this.font = font;
    }

    @Override
    public Affine2 getTransform() {
        return transform;
    }

    @Override
    public Geometry2D getClip() {
        return clip;
    }

    @Override
    public AlphaBlending getAlphaBlending() {
        return blending;
    }

    @Override
    public Paint getPaint() {
        return paint;
    }

    @Override
    public Brush getBrush() {
        return brush;
    }

    @Override
    public Font getFont() {
        return font;
    }

}
