
package science.unlicense.api.layout;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Margin extends CObject {

    public final double top;
    public final double bottom;
    public final double left;
    public final double right;

    public Margin() {
        this.top = 0;
        this.bottom = 0;
        this.left = 0;
        this.right = 0;
    }

    public Margin(double top, double right, double bottom, double left) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }
    
    public Margin(double v) {
        this.top = v;
        this.bottom = v;
        this.left = v;
        this.right = v;
    }

    public boolean isZero(){
        return top==0.0 && bottom==0.0 && left==0.0 && right==0.0;
    }
    
    public Margin expand(Margin margin){
        if(margin.isZero()) return this;
        return new Margin(
                top+margin.top, 
                right+margin.right, 
                bottom+margin.bottom, 
                left+margin.left);
    }
    
    public Chars toChars() {
        return new Chars("Margin["+top+","+right+","+bottom+","+left+"]");
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Margin other = (Margin) obj;
        if (Double.doubleToLongBits(this.top) != Double.doubleToLongBits(other.top)) {
            return false;
        }
        if (Double.doubleToLongBits(this.bottom) != Double.doubleToLongBits(other.bottom)) {
            return false;
        }
        if (Double.doubleToLongBits(this.left) != Double.doubleToLongBits(other.left)) {
            return false;
        }
        if (Double.doubleToLongBits(this.right) != Double.doubleToLongBits(other.right)) {
            return false;
        }
        return true;
    }

    public int getHash() {
        int hash = 7;
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.top) ^ (Double.doubleToLongBits(this.top) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.bottom) ^ (Double.doubleToLongBits(this.bottom) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.left) ^ (Double.doubleToLongBits(this.left) >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.right) ^ (Double.doubleToLongBits(this.right) >>> 32));
        return hash;
    }
    
}
