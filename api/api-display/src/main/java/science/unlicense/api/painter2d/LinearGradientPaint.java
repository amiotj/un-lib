
package science.unlicense.api.painter2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.impl.math.Affine2;

/**
 * LinearGradiant.
 * Similar to SVG.
 *
 * @author Johann Sorel
 */
public class LinearGradientPaint implements Paint {

    private final double startX;
    private final double startY;
    private final double endX;
    private final double endY;
    private final double[] positions;
    private final Color[] colors;

    public LinearGradientPaint(double startX, double startY,
            double endX, double endY, double[] positions, Color[] colors) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.positions = positions;
        this.colors = colors;
        //check colors are not null
        CObjects.ensureNotNull(colors);
        for(int i=0;i<colors.length;i++){
            CObjects.ensureNotNull(colors[i]);
        }
    }

    public double getStartX() {
        return startX;
    }

    public double getStartY() {
        return startY;
    }

    public double getEndX() {
        return endX;
    }

    public double getEndY() {
        return endY;
    }

    public double[] getPositions() {
        return positions;
    }

    public Color[] getColors() {
        return colors;
    }

    public void fill(Image image, Image flagImage, BBox flagBbox, Affine2 trs, AlphaBlending blending) {

        //base image
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        final float[] sampleStore = new float[4];
        final float[] buffer = new float[4];

        //flag image
        final TupleIterator fite = flagImage.getRawModel().asTupleBuffer(flagImage).createIterator(flagBbox);

        final double[] tc = new double[2];

        for(byte[] flagSample = (byte[])fite.next();flagSample!=null;flagSample=(byte[])fite.next()){
            if(flagSample[0]!=0){
                //calculate and set color
                final int[] coord = fite.getCoordinate();
                tc[0] = coord[0];
                tc[1] = coord[1];
                trs.transform(tc, tc);
                double d = distance(tc[0],tc[1],startX,startY,endX,endY);
                if(d<0) d=0;
                if(d>1) d=1;

                //find interval
                int index=0;
                for(;index<positions.length-1;index++){
                    if(d>= positions[index] && d<=positions[index+1]){
                        break;
                    }
                }
                d = (d-positions[index]) / (positions[index+1]-positions[index]);

                final Color color = Colors.interpolate(colors[index], colors[index+1], (float)d);

                final float alpha = (flagSample[0] & 0xff) / 255f;
                final float[] srcrgba = new float[]{
                    color.getRed(),
                    color.getGreen(),
                    color.getBlue(),
                    color.getAlpha()*alpha
                };
                final int[] coordinate = fite.getCoordinate();
                cm.getRGBA(coordinate, sampleStore);
                blending.blend(srcrgba, sampleStore,buffer);
                cm.setRGBA(coordinate, buffer);
            }
        }

    }

    static double distance(double cX, double cY, double startX, double startY, double endX, double endY){
        final double adj = endX-startX;
        final double opp = endY-startY;
        final double hyp = Math.sqrt(adj*adj + opp*opp);
        return ( (startY-cY)*(startY-endY) -(startX-cX)*(endX-startX) ) / (hyp*hyp) ;
    }


}
