
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.KeyFrameTimeSerie;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrameTupleTimeSerie extends KeyFrameTimeSerie implements TupleTimeSerie{

    public int getDimension() {
        if(getFrames().isEmpty()){
            return 0;
        }else{
            TupleKeyFrame frame = (TupleKeyFrame) getFrames().createIterator().next();
            return frame.getValue().getSize();
        }
    }
    
    public TupleKeyFrame interpolate(double time, KeyFrame buffer) {
        
        final KeyFrame[] nearest = getNearest(time, null);
        final TupleKeyFrame underFrame = (TupleKeyFrame) nearest[0];
        final TupleKeyFrame aboveFrame = (TupleKeyFrame) nearest[1];
        
        final TupleKeyFrame frame = buffer==null ? new TupleKeyFrame() : (TupleKeyFrame) buffer;
        if(frame.getValue()==null) frame.setValue(new Vector(underFrame.getValue().getSize()));
        
        if(underFrame == aboveFrame){
            //we are at the begin, the end, or exactly on a frame
            frame.setValue(underFrame.getValue().copy());
        }else{
            //interpolate        
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Tuple ub = underFrame.getValue();
            final Tuple ab = aboveFrame.getValue();
            Vectors.lerp(ub.getValues(), ab.getValues(), ratio, frame.getValue().getValues());
        }
        
        frame.setTime(time);
        return frame;
    }
    
}
