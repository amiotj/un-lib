
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface TransformTimeSerie extends TimeSerie {

    int getDimension();

    TransformKeyFrame interpolate(double time, KeyFrame buffer);
    
}
