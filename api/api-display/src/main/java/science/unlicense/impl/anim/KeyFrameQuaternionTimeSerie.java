
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.KeyFrameTimeSerie;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Quaternions;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrameQuaternionTimeSerie extends KeyFrameTimeSerie implements TupleTimeSerie,RotationTimeSerie{

    public int getDimension() {
        if(getFrames().isEmpty()){
            return 0;
        }else{
            return 3;
        }
    }
    
    public TupleKeyFrame interpolate(double time, KeyFrame buffer) {
        
        final KeyFrame[] nearest = getNearest(time, null);
        final TupleKeyFrame underFrame = (TupleKeyFrame) nearest[0];
        final TupleKeyFrame aboveFrame = (TupleKeyFrame) nearest[1];
        
        final TupleKeyFrame frame = buffer==null ? new TupleKeyFrame() : (TupleKeyFrame) buffer;
        if(frame.getValue()==null) frame.setValue(new Quaternion());
        
        if(underFrame == aboveFrame){
            //we are at the begin, the end, or exactly on a frame
            frame.setValue(underFrame.getValue().copy());
        }else{
            //interpolate        
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Tuple ub = underFrame.getValue();
            final Tuple ab = aboveFrame.getValue();
            
            Quaternions.slerp(ub.getValues(), ab.getValues(), ratio, frame.getValue().getValues());
        }
        
        frame.setTime(time);
        return frame;
    }

    @Override
    public MatrixKeyFrame interpolateAsMatrix(double time, MatrixKeyFrame buffer) {
        final KeyFrame[] nearest = getNearest(time, null);
        final TupleKeyFrame underFrame = (TupleKeyFrame) nearest[0];
        final TupleKeyFrame aboveFrame = (TupleKeyFrame) nearest[1];
        
        final MatrixKeyFrame frame = buffer==null ? new MatrixKeyFrame() : (MatrixKeyFrame) buffer;
        if(frame.getValue()==null) frame.setValue(new Matrix3x3());
        
        final DefaultMatrix matrix = frame.getValue();
        if(underFrame == aboveFrame){
            //we are at the begin, the end, or exactly on a frame
            Quaternions.toMatrix(underFrame.getValue().getValues(),matrix.getValues());
        }else{
            //interpolate        
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            final double ratio = (time-underFrame.getTime()) / extent;

            final Tuple ub = underFrame.getValue();
            final Tuple ab = aboveFrame.getValue();
            
            double[] rot = Quaternions.slerp(ub.getValues(), ab.getValues(), ratio, null);
            Quaternions.toMatrix(rot, matrix.getValues());
        }
        
        frame.setTime(time);
        return frame;
    }
    
}
