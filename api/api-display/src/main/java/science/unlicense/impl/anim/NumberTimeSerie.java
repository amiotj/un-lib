
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface NumberTimeSerie extends TimeSerie{
    
    double interpolate(double time);

    NumberKeyFrame interpolate(double time, KeyFrame buffer);
    
}
