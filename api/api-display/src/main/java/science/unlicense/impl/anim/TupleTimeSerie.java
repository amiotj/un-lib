
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface TupleTimeSerie extends TimeSerie{

    int getDimension();
    
    TupleKeyFrame interpolate(double time, KeyFrame buffer);

}
