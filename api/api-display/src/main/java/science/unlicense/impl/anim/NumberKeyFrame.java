
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;

/**
 *
 * @author Johann Sorel
 */
public class NumberKeyFrame extends KeyFrame{
    

    public NumberKeyFrame() {
    }

    public NumberKeyFrame(double time, double value) {
        super(time,value);
    }

    public Double getValue() {
        return (Double) value;
    }

    public void setValue(double value) {
        setValue((Double)value);
    }
    
}
