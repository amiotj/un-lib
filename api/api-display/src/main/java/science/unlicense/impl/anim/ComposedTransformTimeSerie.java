
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.TimeSerie;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public class ComposedTransformTimeSerie implements TransformTimeSerie{

    public static final int ROTATION_QUATERNION = 1;
    public static final int ROTATION_EULER = 2;



    private TupleTimeSerie scaleTimeSerie;
    private TupleTimeSerie translationTimeSerie;
    private TimeSerie rotationTimeSerie;
    //buffers
    private TupleKeyFrame scaleFrame;
    private TupleKeyFrame trsFrame;
    private TupleKeyFrame rotateTFrame;
    private MatrixKeyFrame rotateMFrame;

    public TupleTimeSerie getScaleTimeSerie() {
        return scaleTimeSerie;
    }

    public void setScaleTimeSerie(TupleTimeSerie scaleTimeSerie) {
        this.scaleTimeSerie = scaleTimeSerie;
    }

    public TupleTimeSerie getTranslationTimeSerie() {
        return translationTimeSerie;
    }

    public void setTranslationTimeSerie(TupleTimeSerie translationTimeSerie) {
        this.translationTimeSerie = translationTimeSerie;
    }

    public TimeSerie getRotationTimeSerie() {
        return rotationTimeSerie;
    }

    /**
     * Accept MatrixTimeSerie or RotationTimeSerie.
     * 
     * @param rotationTimeSerie 
     */
    public void setRotationTimeSerie(TimeSerie rotationTimeSerie) {
        this.rotationTimeSerie = rotationTimeSerie;
    }

    public int getDimension(){
        if(scaleTimeSerie!=null) return scaleTimeSerie.getDimension();
        if(translationTimeSerie!=null) return translationTimeSerie.getDimension();
        if(rotationTimeSerie instanceof TupleTimeSerie) return ((TupleTimeSerie)rotationTimeSerie).getDimension();
        if(rotationTimeSerie instanceof MatrixTimeSerie) return ((MatrixTimeSerie)rotationTimeSerie).getDimension();
        return 0;
    }

    @Override
    public TransformKeyFrame interpolate(double time, KeyFrame buffer) {

        final TransformKeyFrame frame = buffer!=null ? (TransformKeyFrame)buffer :
                new TransformKeyFrame(time, new NodeTransform(getDimension()));
        final NodeTransform trs = frame.getValue();


        // set scale
        if(scaleTimeSerie!=null){
            scaleFrame = scaleTimeSerie.interpolate(time, scaleFrame);
            trs.getScale().set(scaleFrame.getValue());
        }else{
            trs.getScale().setAll(1.0);
        }

        // set translation
        if(translationTimeSerie!=null){
            trsFrame = translationTimeSerie.interpolate(time, trsFrame);
            trs.getTranslation().set(trsFrame.getValue());
        }else{
            trs.getTranslation().setAll(1.0);
        }

        // set rotation
        if(rotationTimeSerie instanceof RotationTimeSerie){
            rotateMFrame = ((RotationTimeSerie)rotationTimeSerie).interpolateAsMatrix(time, rotateMFrame);
            trs.getRotation().set(rotateMFrame.getValue());
        }else if(rotationTimeSerie instanceof MatrixTimeSerie){
            rotateMFrame = ((MatrixTimeSerie)rotationTimeSerie).interpolate(time, rotateMFrame);
            trs.getRotation().set(rotateMFrame.getValue());
        }else{
            trs.getRotation().setToIdentity();
        }

        return frame;
    }

    public double getLength() {
        double length = 0;
        if(scaleTimeSerie!=null)        length = Math.max(length, scaleTimeSerie.getLength());
        if(translationTimeSerie!=null)  length = Math.max(length, translationTimeSerie.getLength());
        if(rotationTimeSerie!=null)     length = Math.max(length, rotationTimeSerie.getLength());
        return length;
    }
    
}
