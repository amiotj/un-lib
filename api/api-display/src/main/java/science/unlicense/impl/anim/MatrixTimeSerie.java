
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface MatrixTimeSerie extends TimeSerie{

    int getDimension();
    
    MatrixKeyFrame interpolate(double time, KeyFrame buffer);
    
}
