
package science.unlicense.impl.anim;

import science.unlicense.api.anim.Timer;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.Matrix2x2;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 * 
 * @author Johann Sorel
 */
public class TransformTimeSerieHelper {

    private TransformAnimation animation = new TransformAnimation();
    private KeyFrameTransformTimeSerie timeSerie;
    private TransformKeyFrame keyFrame = null;

    public final int dimension;

    public TransformTimeSerieHelper() {
        this(2);
    }

    public TransformTimeSerieHelper(int dimension) {
        this.dimension = dimension;
        timeSerie = new KeyFrameTransformTimeSerie();
    }
    
    public TransformTimeSerieHelper(TransformAnimation animation) {
        dimension = 2;
        this.animation = animation;
        this.timeSerie = (KeyFrameTransformTimeSerie) animation.getTimeSerie();
    }

    public TransformTimeSerieHelper reset(){
        animation.setRepeatCount(0);
        animation.setSpeed(1.0f);
        animation.setTime(0.0f);
        animation.setTimer(null);
        timeSerie.getFrames().removeAll();
        animation.setNode(null);
        keyFrame = null;
        return this;
    }

    /**
     * Set animation target node.
     *
     * @param node
     * @return TrsBuilder this
     */
    public TransformTimeSerieHelper on(SceneNode node){
        animation.setNode(node);
        return this;
    }

    /**
     * Set animation timer.
     *
     * @param timer
     * @return TrsBuilder this
     */
    public TransformTimeSerieHelper timer(Timer timer){
        animation.setTimer(timer);
        return this;
    }
    
    /**
     * Set animation repeat.
     *
     * @param nbReapeat
     * @return TrsBuilder this
     */
    public TransformTimeSerieHelper repeat(int nbReapeat){
        animation.setRepeatCount(nbReapeat);
        return this;
    }
    
    /**
     * Move to give time, reusing or creating a new keyframe.
     *
     * @param timeMs
     * @return TrsBuilder this
     */
    public TransformTimeSerieHelper at(long timeMs){
        keyFrame = (TransformKeyFrame) timeSerie.getFrame(timeMs);
        if(keyFrame==null){
            keyFrame = new TransformKeyFrame();
            keyFrame.setTime(timeMs);
            keyFrame.setValue(new NodeTransform(dimension));
            timeSerie.getFrames().add(keyFrame);
        }
        return this;
    }

    public TransformTimeSerieHelper translation(double x, double y){
        return translation(new double[]{x,y});
    }

    public TransformTimeSerieHelper translation(double x, double y, double z){
        return translation(new double[]{x,y,z});
    }

    public TransformTimeSerieHelper translation(double[] trs){
        if(keyFrame==null) throw new InvalidArgumentException("No active keyframe, use at(time) method before setting transform properties.");
        keyFrame.getValue().getTranslation().set(trs);
        keyFrame.getValue().notifyChanged();
        return this;
    }

    public TransformTimeSerieHelper rotation(double angle){
        return rotation(Matrix2x2.fromAngle(angle));
    }

    public TransformTimeSerieHelper rotation(Quaternion qt){
        return rotation(qt.toMatrix3());
    }

    public TransformTimeSerieHelper rotation(Matrix rotation){
        if(keyFrame==null) throw new InvalidArgumentException("No active keyframe, use at(time) method before setting transform properties.");
        keyFrame.getValue().getRotation().set(rotation);
        keyFrame.getValue().notifyChanged();
        return this;
    }

    public TransformTimeSerieHelper scale(double uniform){
        if(keyFrame==null) throw new InvalidArgumentException("No active keyframe, use at(time) method before setting transform properties.");
        keyFrame.getValue().getScale().setAll(uniform);
        keyFrame.getValue().notifyChanged();
        return this;
    }

    public TransformTimeSerieHelper scale(double x, double y){
        return scale(new double[]{x,y});
    }

    public TransformTimeSerieHelper scale(double x, double y, double z){
        return scale(new double[]{x,y,z});
    }

    public TransformTimeSerieHelper scale(double[] trs){
        if(keyFrame==null) throw new InvalidArgumentException("No active keyframe, use at(time) method before setting transform properties.");
        keyFrame.getValue().getScale().set(trs);
        keyFrame.getValue().notifyChanged();
        return this;
    }

    public TransformAnimation build(){

        final TransformAnimation anim = new TransformAnimation();
        anim.setNode(animation.getNode());
        anim.setRepeatCount(animation.getNbRepeat());
        anim.setSpeed(animation.getSpeed());
        anim.setTimer(animation.getTimer());

        final KeyFrameTransformTimeSerie cp = new KeyFrameTransformTimeSerie();
        final Iterator ite = timeSerie.getFrames().createIterator();
        while(ite.hasNext()){
            final TransformKeyFrame kf = (TransformKeyFrame) ite.next();
            final TransformKeyFrame cpkf = new TransformKeyFrame();
            cpkf.setTime(kf.getTime());
            final NodeTransform trs = new NodeTransform(dimension);
            trs.set(kf.getValue());
            cpkf.setValue(trs);
            cp.getFrames().add(cpkf);
        }
        
        anim.setTimeSerie(timeSerie);
        return anim;
    }

}
