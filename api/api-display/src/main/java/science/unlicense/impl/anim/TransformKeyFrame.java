
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public class TransformKeyFrame extends KeyFrame {
    
    public TransformKeyFrame() {
    }
    
    public TransformKeyFrame(double time, NodeTransform transform) {
        super(time,transform);
    }
    
    public TransformKeyFrame(double time, Vector position, Quaternion rotation, Vector scale){
        super(time);
        NodeTransform transform = new NodeTransform(position.getSize());
        transform.getTranslation().set(position);
        transform.getRotation().set(rotation.toMatrix3());
        transform.getScale().set(scale);
        transform.notifyChanged();
        setValue(transform);
    }


    public NodeTransform getValue() {
        return (NodeTransform) value;
    }

    public void setValue(NodeTransform transform) {
        setValue((Object)transform);
    }
    
}
