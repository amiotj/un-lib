
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.math.Tuple;

/**
 *
 * @author Johann Sorel
 */
public class TupleKeyFrame extends KeyFrame{

    public TupleKeyFrame() {
    }

    public TupleKeyFrame(double time, Tuple value) {
        super(time,value);
    }

    public Tuple getValue() {
        return (Tuple) value;
    }

    public void setValue(Tuple value) {
        this.value = value;
    }
    
}
