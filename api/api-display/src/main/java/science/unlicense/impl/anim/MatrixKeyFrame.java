
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.impl.math.DefaultMatrix;

/**
 *
 * @author Johann Sorel
 */
public class MatrixKeyFrame extends KeyFrame{


    public MatrixKeyFrame() {
    }

    public MatrixKeyFrame(double time, DefaultMatrix value) {
        super(time,value);
    }

    public DefaultMatrix getValue() {
        return (DefaultMatrix) value;
    }

    public void setValue(DefaultMatrix value) {
        setValue((Object)value);
    }

}
