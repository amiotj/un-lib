
package science.unlicense.impl.anim;

import science.unlicense.api.anim.TimeSerie;

/**
 *
 * @author Johann Sorel
 */
public interface RotationTimeSerie extends TimeSerie{

    int getDimension();
    
    MatrixKeyFrame interpolateAsMatrix(double time, MatrixKeyFrame buffer);

}
