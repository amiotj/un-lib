
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.KeyFrameTimeSerie;
import science.unlicense.api.math.InterpolationMethod;

/**
 *
 * @author Johann Sorel
 */
public class KeyFrameNumberTimeSerie extends KeyFrameTimeSerie implements NumberTimeSerie{

    private InterpolationMethod method = InterpolationMethod.create(InterpolationMethod.METHOD_LINEAR);

    public InterpolationMethod getMethod() {
        return method;
    }

    public void setMethod(InterpolationMethod method) {
        this.method = method;
    }
    
    public double interpolate(double time) {
        final KeyFrame[] nearest = getNearest(time, null);
        final NumberKeyFrame underFrame = (NumberKeyFrame) nearest[0];
        final NumberKeyFrame aboveFrame = (NumberKeyFrame) nearest[1];

        if(underFrame == aboveFrame){
            //we are at the begin, the end, or exactly on a frame
            return underFrame.getTime();
        }else{
            //interpolate
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            double ratio = (time-underFrame.getTime()) / extent;
            double ub = underFrame.getValue();
            double ab = aboveFrame.getValue();
            if(ab<ub){
                double temp = ub;
                ub = ab;
                ab = temp;
                ratio = 1.0-ratio;
            }
            return method.interpolate(ratio, ub, ab);
        }
    }

    public NumberKeyFrame interpolate(double time, KeyFrame buffer) {
        
        final KeyFrame[] nearest = getNearest(time, null);
        final NumberKeyFrame underFrame = (NumberKeyFrame) nearest[0];
        final NumberKeyFrame aboveFrame = (NumberKeyFrame) nearest[1];
        
        final NumberKeyFrame frame = (NumberKeyFrame) (buffer==null ? new NumberKeyFrame() : buffer);
        
        if(underFrame == aboveFrame){
            //we are at the begin, the end, or exactly on a frame
            frame.setValue(underFrame.getValue());
        }else{
            //interpolate
            final double extent = aboveFrame.getTime() - underFrame.getTime();
            double ratio = (time-underFrame.getTime()) / extent;
            double ub = underFrame.getValue();
            double ab = aboveFrame.getValue();
            if(ab<ub){
                double temp = ub;
                ub = ab;
                ab = temp;
                ratio = 1.0-ratio;
            }
            frame.setValue(method.interpolate(ratio, ub, ab));
        }
        
        frame.setTime(time);
        return frame;
    }
    
}
