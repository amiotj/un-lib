
package science.unlicense.impl.anim;

import science.unlicense.api.anim.KeyFrame;
import science.unlicense.api.anim.TimeSerie;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class ComposedTupleTimeSerie implements TupleTimeSerie {

    private final Sequence series = new ArraySequence();

    public Sequence getSeries() {
        return series;
    }

    public int getDimension() {
        return series.getSize();
    }
    
    public TupleKeyFrame interpolate(double time, KeyFrame buffer) {
        final TupleKeyFrame frame = buffer!=null ?  (TupleKeyFrame)buffer : new TupleKeyFrame(time, new Vector(series.getSize()));
        final Tuple tuple = frame.getValue();
        for(int i=0,n=series.getSize();i<n;i++){
            final NumberTimeSerie nts = ((NumberTimeSerie)series.get(i));
            ((TupleRW)tuple).set(i, nts.interpolate(time));
        }
        return frame;
    }

    public double getLength() {
        double length = 0;
        for(int i=0,n=series.getSize();i<n;i++){
            double l = ((TimeSerie)series.get(i)).getLength();
            if(l>length) length = l;
        }
        return length;
    }

}
