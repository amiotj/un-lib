
package science.unlicense.api.painter2d;

import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.ColorPaint;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.math.Affine2;

/**
 * General tests for Painter2D.
 * 
 * @author Johann Sorel
 */
public abstract class ImagePainter2DTest {

    private static final Color TRS = new Color(0, 0, 0, 0);

    /**
     * Painter disposed after each test method
     */
    private ImagePainter2D painter;

    @After
    public void afterMethod(){
        if(painter !=null){
            painter.dispose();
        }
    }

    protected abstract ImagePainter2D createPainter(int width,int height);
    
    @Test
    public void testFillRectangle(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 3
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 4
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 5
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 6
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 7
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //10
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //11
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //12
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //13
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        
        painter = createPainter(20, 20);
        final Rectangle rect = new Rectangle(2, 3, 10, 12);
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(rect);
        painter.flush();
        
        final Image image = painter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        
        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    @Ignore //this has changed, bbox of geometry area clips the fill
    @Test
    public void testFillLine(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 2
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 3
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 4
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 5
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 6
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 7
            {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, // 9
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //10
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //11
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //12
            {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1}, //13
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1}, //14
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1}, //15
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        final Segment line = new Segment(9, 17, 6, 2);
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(line);
        painter.flush();

        final Image image = painter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    @Test
    @Ignore
    public void testFillPolyline(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //10
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //11
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //12
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        final Polyline poly = new Polyline(Geometries.toTupleBuffer(new Tuple[]{
            new DefaultTuple(10, 5),
            new DefaultTuple(15, 15),
            new DefaultTuple(5, 15),
            new DefaultTuple(10, 5)}));
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(poly);
        painter.flush();
                
        final Image image = painter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    /**
     * Test the close element of path iterator is well interpreted.
     */
    @Test
    @Ignore
    public void testFillPath1(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //10
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //11
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //12
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        final Path path = new Path();
        path.appendMoveTo(10, 5);
        path.appendLineTo(15, 15);
        path.appendLineTo(5, 15);
        path.appendClose();
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(path);

        final Image image = painter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    /**
     * Test for bug : uncorrectly closed fill area
     */
    @Test
    @Ignore
    public void testFillPath2(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 3
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 4
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 5
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 6
            {0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0}, // 7
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //10
            {0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0}, //11
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //12
            {0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);

        final Path path = new Path();
        path.appendMoveTo(10, 5);
        path.appendLineTo(15, 15);
        path.appendLineTo(5, 15);
        path.appendLineTo(10, 5);
        path.appendClose();
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(path);
        painter.flush();

        final Image image = painter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    @Test
    public void testOverlapFill(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 3
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 4
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 5
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 6
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 7
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 8
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 9
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //10
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //11
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //12
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        painter = createPainter(20, 20);
        Rectangle rect = new Rectangle(0, 0, 20, 20);
        painter.setPaint(new ColorPaint(Color.BLUE));
        painter.fill(rect);
        painter.flush();
        
        Image image = painter.getImage();
        PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                Assert.assertEquals(" at coord "+y+" "+x,Color.BLUE, c);
            }
        }

        rect = new Rectangle(5, 3, 10, 12);
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(rect);
        painter.flush();
        
        image = painter.getImage();
        cm = image.getColorModel().asTupleBuffer(image);

        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? Color.BLUE : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    @Test
    public void testImagePaint(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 3
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 4
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 5
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 6
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 7
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 8
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, // 9
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //10
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //11
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //12
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //13
            {0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        final Image subImage = Images.create(new Extent.Long(10, 12), Images.IMAGE_TYPE_RGBA);
        final BBox all = new BBox(2);
        all.setRange(0, 0, 10);
        all.setRange(1, 0, 12);
        subImage.getColorModel().asTupleBuffer(subImage).setColor(all, Color.RED);
        final Affine2 trs = new Affine2(
                1, 0, 5,
                0, 1, 3);

        
        painter = createPainter(20, 20);
        painter.paint(subImage, trs);
        painter.flush();
        
        final Image image = painter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);

        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }

    @Test
    public void testImageTransformPaint(){

        final int[][] expected = new int[][]{
          // 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 0
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 1
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // 2
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 3
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 4
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 5
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 6
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 7
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 8
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, // 9
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //10
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //11
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //12
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //13
            {0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0}, //14
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //15
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //16
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //17
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //18
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  //19
        };

        final Image subImage = Images.create(new Extent.Long(10, 12), Images.IMAGE_TYPE_RGBA);
        final BBox all = new BBox(2);
        all.setRange(0, 0, 10);
        all.setRange(1, 0, 12);
        subImage.getColorModel().asTupleBuffer(subImage).setColor(all, Color.RED);
        final Affine2 trs = new Affine2(
                1, 0, 5,
                0, 1, 3);



        final ImagePainter2D imgPainter = createPainter(20, 20);
        Painter2D painter = imgPainter;
        final Affine2 steptrs = new Affine2(
                1, 0, -3,
                0, 1,  0);
        painter = painter.derivate(steptrs);
        painter.paint(subImage, trs);
        painter.flush();
        
        final Image image = imgPainter.getImage();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);

        for(int y=0;y<20;y++){
            for(int x=0;x<20;x++){
                final Color c = cm.getColor(new int[]{x,y});
                final Color exp = (expected[y][x] == 0) ? TRS : Color.RED;
                Assert.assertEquals(" at coord "+y+" "+x,exp, c);
            }
        }
        
    }


}
