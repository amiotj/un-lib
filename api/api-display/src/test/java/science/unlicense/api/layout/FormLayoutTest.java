
package science.unlicense.api.layout;

import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import org.junit.Test;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import org.junit.Assert;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public class FormLayoutTest {

    @Test
    public void emptyTest(){

        final FormLayout layout = new FormLayout();

        final NodeTransform transform = new NodeTransform(2);
        final Extent size = new Extent.Double(2);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));

        //check best size
        Assert.assertEquals(new Extent.Double(0d, 0d),layout.getExtents().getBest(null));

        //change size ----------------------------------------------------------
        //should have no effect
        layout.setView(new BBox(new Vector(0, 0), new Vector(200, 200)));

        //check best size
        Assert.assertEquals(new Extent.Double(0d, 0d),layout.getExtents().getBest(null));

    }
    
    @Test
    public void oneWidgetTest(){

        final Space space1 = new Space(new Extent.Double(30, 10),FillConstraint.builder().build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1}));

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(30, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 15,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(30d, 10d),layout.getExtents().getBest(null));

        //change size ----------------------------------------------------------
        //should have no effect
        layout.setView(new BBox(new Vector(0, 0), new Vector(200, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(30, 10), space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 15,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(30d, 10d),layout.getExtents().getBest(null));

    }

    @Test
    public void treeWidgetOneRowTest(){

        final Space space1 = new Space(new Extent.Double(5, 10),FillConstraint.builder().coord(0, 0).fill(true, true).build());
        final Space space2 = new Space(new Extent.Double(15, 5),FillConstraint.builder().coord(1, 0).fill(true, true).build());
        final Space space3 = new Space(new Extent.Double(30, 2),FillConstraint.builder().coord(2, 0).fill(true, true).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1,space2,space3}));
        layout.setDefaultColumnSpace(3);
        layout.setDefaultRowSpace(3);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(5, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 2.5,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(15, 10),space2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 8+7.5, // 5 +3
                0, 1, 5,
                0, 0, 1),
                space2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(30, 10),space3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 26+15, // 5 + 3 + 15 + 3
                0, 1, 5,
                0, 0, 1),
                space3.getNodeTransform().asMatrix());

        //check best size
        // 5 + 3 + 15 + 3 + 30
        Assert.assertEquals(new Extent.Double(56, 10),layout.getExtents().getBest(null));

    }
    
    @Test
    public void verticalExpandTest(){

        final Space space1 = new Space(new Extent.Double(10, 10), FillConstraint.builder().coord(0, 0).build());
        final Space space3 = new Space(new Extent.Double(10, 10), FillConstraint.builder().coord(0, 2).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1,space3}));
        layout.setRowSize(1, FormLayout.SIZE_EXPAND);
        layout.setDefaultColumnSpace(3);
        layout.setDefaultRowSpace(3);
        

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(10, 10),space3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 95,
                0, 0, 1),
                space3.getNodeTransform().asMatrix());

        //check best size
        // 10 + 10 + 2*3 (row spacing)
        Assert.assertEquals(new Extent.Double(10, 26),layout.getExtents().getBest(null));
    }
    
    @Test
    public void spanYTest(){

        final Space space0 = new Space(new Extent.Double(20, 100), FillConstraint.builder().coord(0, 0).span(1, 2).fill(true, true).build());
        final Space space1 = new Space(new Extent.Double(10, 10), FillConstraint.builder().coord(1, 0).fill(true, true).build());
        final Space space3 = new Space(new Extent.Double(10, 10), FillConstraint.builder().coord(1, 1).fill(true, true).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space0,space1,space3}));     
        layout.setDefaultColumnSpace(0);   
        layout.setDefaultRowSpace(0);

        Assert.assertEquals(new Extent.Double(30, 100),layout.getExtents().getBest(null));
        
        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 50),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10+15,
                0, 1, 25,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(10, 50),space3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 20+5,
                0, 1, 50+25,
                0, 0, 1),
                space3.getNodeTransform().asMatrix());

    }


    @Test
    public void treeReserveSpaceTest(){

        final Space space1 = new Space(new Extent.Double(5, 10),FillConstraint.builder().coord(0, 0).fill(true, true).build());
        final Space space2 = new Space(new Extent.Double(15, 5),FillConstraint.builder().coord(1, 0).fill(true, true).build());

        final FormLayout layout = new FormLayout(new ArraySequence(new Object[]{space1,space2}));
        layout.setColumnSize(0, FormLayout.SIZE_EXPAND);

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(85, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 42.5,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(15, 10),space2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 92.5,
                0, 1, 5,
                0, 0, 1),
                space2.getNodeTransform().asMatrix());

        Assert.assertEquals(new Extent.Double(20, 10),layout.getExtents().getBest(null));

        //set reserve space and visibility to false
        space2.setVisible(false);
        space2.setReserveSpace(false);
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 10),space1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 5,
                0, 0, 1),
                space1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(0, 0),space2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 92.5,
                0, 1, 5,
                0, 0, 1),
                space2.getNodeTransform().asMatrix());

        Assert.assertEquals(new Extent.Double(5, 10),layout.getExtents().getBest(null));


    }

}
