
package science.unlicense.api.layout;

import science.unlicense.api.layout.DisplacementLayout;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class DisplacementLayoutTest {
    
    @Test
    public void testNoOffset(){
        
        final Space but1 = new Space(new Extent.Double(40, 20));

        final DisplacementLayout layout = new DisplacementLayout();
        layout.setHorizontalAlignement(DisplacementLayout.HALIGN_LEFT);
        layout.setVerticalAlignement(DisplacementLayout.VALIGN_TOP);
        layout.setFillWidth(true);
        layout.setPositionables(new ArraySequence(new Object[]{but1}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 20),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, -40,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(40, 20),layout.getExtents().getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector(-40, -40), new Vector(20,40)));
        layout.update();
        Assert.assertEquals(new Extent.Double(60, 20), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 0,
                0, 1, -30,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(40, 20),layout.getExtents().getBest(null));

    }
    
}
