
package science.unlicense.api.layout;

import science.unlicense.api.layout.BlockLayout;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class BlockLayoutTest {
    
    @Test
    public void test2x2(){
        
        final Space but1 = new Space(new Extent.Double(10, 20));
        final Space but2 = new Space(new Extent.Double(10, 20));
        final Space but3 = new Space(new Extent.Double(10, 20));
        final Space but4 = new Space(new Extent.Double(10, 20));

        final BlockLayout layout = new BlockLayout(new Extent.Double(50, 20));
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,but3,but4}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(50, 20),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 10,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 75,
                0, 1, 10,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 30,
                0, 0, 1),
                but3.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but4.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 75,
                0, 1, 30,
                0, 0, 1),
                but4.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(100d, 40d),layout.getExtents().getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(80, 500)));
        layout.update();
        Assert.assertEquals(new Extent.Double(50, 20), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 10,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 30,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 50,
                0, 0, 1),
                but3.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(50, 20),but4.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 70,
                0, 0, 1),
                but4.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(50d, 80d),layout.getExtents().getBest(null));

    }
    
}
