
package science.unlicense.api.desktop;

import static java.lang.Thread.sleep;
import java.util.concurrent.atomic.AtomicBoolean;
import static org.junit.Assert.*;
import org.junit.Test;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.PropertyPredicate;

/**
 * Frame manager implementation must inherit this test case.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractFrameTest {
    
    protected abstract Frame createFrame();
    
    @Test
    public void waitForDisposalTest() {
        
        Frame frame = createFrame();
        frame.setSize(10, 10);
        
        final AtomicBoolean disposed = new AtomicBoolean();
        
        frame.addEventListener(new PropertyPredicate(Frame.PROP_DISPOSED), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                PropertyMessage message = (PropertyMessage) event.getMessage();
                disposed.set( (Boolean)message.getNewValue());
            }
        });
        
        frame.setVisible(true);
        
        //trigger frame disposal
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                frame.setVisible(false);
                frame.dispose();
            }
        }.start();
        
        frame.waitForDisposal();
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        
        assertEquals(true, disposed.get());
        
    }
    
}
