

package science.unlicense.api.layout;

import science.unlicense.api.layout.AbstractPositionableNode;
import science.unlicense.api.layout.LayoutConstraint;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.geometry.Extent;

/**
 *
 * @author Johann Sorel
 */
public class Space extends AbstractPositionableNode{
    
    private final Extent ext;
    
    public Space() {
        this(new Extent.Double(1, 1));
    }
    
    public Space(Extent extent) {
        this(extent,null);
    }
    
    public Space(Extent extent, LayoutConstraint constraint) {
        super(false);
        this.ext = extent;
        setLayoutConstraint(constraint);
    }

    @Override
    protected void getExtentsInternal(Extents buffer, Extent constraint) {
        buffer.setAll(ext);
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }
    
}
