
package science.unlicense.api.layout;

import science.unlicense.api.layout.GridLayout;
import org.junit.Test;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import org.junit.Assert;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class GridLayoutTest {

    @Test
    public void verticalTest(){

        final Space but1 = new Space(new Extent.Double(2, 2));
        final Space but2 = new Space(new Extent.Double(2, 2));
        final Space but3 = new Space(new Extent.Double(2, 2));

        final GridLayout layout = new GridLayout(-1, 1);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,but3}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 300)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100d, 100d),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 150,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 250,
                0, 0, 1),
                but3.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(2d, 6d),layout.getExtents().getBest(null));

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(20, 30)));
        layout.update();
        Assert.assertEquals(new Extent.Double(20d, 10d), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 5,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(20d, 10d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 15,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(20d, 10d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 25,
                0, 0, 1),
                but3.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(2d, 6d),layout.getExtents().getBest(null));

    }


    @Test
    public void horizontalTest(){

        final Space but1 = new Space(new Extent.Double(2, 2));
        final Space but2 = new Space(new Extent.Double(2, 2));
        final Space but3 = new Space(new Extent.Double(2, 2));

        final GridLayout layout = new GridLayout(1, -1);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,but3}));

        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(300, 100)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100d, 100d),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 50,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 150,
                0, 1, 50,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(100d, 100d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 250,
                0, 1, 50,
                0, 0, 1),
                but3.getNodeTransform().asMatrix());

        //change size ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(60, 5)));
        layout.update();
        Assert.assertEquals(new Extent.Double(20d, 5d), but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 10,
                0, 1, 2.5,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(20d, 5d),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 30,
                0, 1, 2.5,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(20d, 5d),but3.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 2.5,
                0, 0, 1),
                but3.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(6d, 2d),layout.getExtents().getBest(null));

    }

    @Test
    public void spacing1x1Text(){

        final Space ele1 = new Space(new Extent.Double(10, 10));

        final GridLayout layout = new GridLayout(1, 1, 2, 2);
        layout.setPositionables(new ArraySequence(new Object[]{ele1}));

        Assert.assertEquals(new Extent.Double(10, 10), layout.getExtents().getBest(null));

        layout.setView(new BBox(new Vector(0, 0), new Vector(10, 10)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10), ele1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                ele1.getNodeTransform().asMatrix());
    }

    @Test
    public void spacing2x2Text(){

        final Space ele00 = new Space(new Extent.Double(10, 10));
        final Space ele01 = new Space(new Extent.Double(10, 10));
        final Space ele10 = new Space(new Extent.Double(10, 10));
        final Space ele11 = new Space(new Extent.Double(10, 10));

        final GridLayout layout = new GridLayout(2, 2, 2, 2);
        layout.setPositionables(new ArraySequence(new Object[]{ele00,ele01,ele10,ele11}));

        Assert.assertEquals(new Extent.Double(22, 22), layout.getExtents().getBest(null));

        layout.setView(new BBox(new Vector(0, 0), new Vector(22, 22)));
        layout.update();
        Assert.assertEquals(new Extent.Double(10, 10), ele00.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 5,
                0, 0, 1),
                ele00.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(10, 10), ele01.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 17,
                0, 1, 5,
                0, 0, 1),
                ele01.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(10, 10), ele10.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 5,
                0, 1, 17,
                0, 0, 1),
                ele10.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(10, 10), ele11.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 17,
                0, 1, 17,
                0, 0, 1),
                ele11.getNodeTransform().asMatrix());
    }

}
