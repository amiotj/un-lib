
package science.unlicense.api.scenegraph;

import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.math.transform.NodeTransform;
import org.junit.Test;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;
import org.junit.Assert;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Affine;

/**
 *
 * @author Johann Sorel
 */
public class SceneNodeTest {

    private static final double DELTA = 0.0000001;

    /**
     * Check raised exception between node dimension sizes.
     */
    @Test
    public void testNodeDimension(){

        final SceneNode parent = new DefaultSceneNode(3);
        final SceneNode child = new DefaultSceneNode(2);

        try {
            parent.getChildren().add(child);
            Assert.fail("Different scene node diemnsions should raise an exception.");
        } catch(InvalidArgumentException ex) {
            //ok
        }

    }

    /**
     * When there is no parent, the node transform should be equal to NodeToRootSpace matrix.
     */
    @Test
    public void testNodeToRootTransform(){
        
        final SceneNode node = new DefaultSceneNode(3);
        node.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        node.getNodeTransform().notifyChanged();
        
        Assert.assertEquals(node.getNodeTransform().asMatrix(), node.getNodeToRootSpace().toMatrix());
        
    }
    
    @Test
    public void testRootToNodeTransform(){

        final SceneNode scene = new DefaultSceneNode(3);
        final SceneNode n1 = new DefaultSceneNode(3);
        n1.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        n1.getNodeTransform().notifyChanged();
        final SceneNode n2 = new DefaultSceneNode(3);
        n2.getNodeTransform().getTranslation().setXYZ(3, -5, 7);
        n2.getNodeTransform().notifyChanged();
        scene.getChildren().add(n1);
        n1.getChildren().add(n2);

        //calculate our reference vector
        final Vector reference = new Vector(0, 0, 0, 1);
        n1.getNodeTransform().transform(reference,reference);
        n2.getNodeTransform().transform(reference,reference);

        Assert.assertEquals(5, reference.get(0), DELTA);
        Assert.assertEquals(-4, reference.get(1), DELTA);
        Assert.assertEquals(10, reference.get(2), DELTA);

        final Affine worldToNode = n2.getRootToNodeSpace();
        final Vector candidate = new Vector(0, 0, 0, 1);
        worldToNode.transform(candidate,candidate);

        //we should be back to 0
        Assert.assertEquals(new Vector(-5, 4, -10, 1), candidate);

    }

    @Test
    public void testUpdateRootToNodeTransformNoParent(){

        final SceneNode n1 = new DefaultSceneNode(3);
        n1.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        n1.getNodeTransform().notifyChanged();
        
        n1.setRootToNodeSpace(new NodeTransform(
                new Matrix3x3().setToIdentity(),
                new Vector(1,1,1),
                new Vector(-2,-4,-6)));

        //check the n1 transform has changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 2,
                0, 1, 0, 4,
                0, 0, 1, 6,
                0, 0, 0, 1),
                n1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -2,
                0, 1, 0, -4,
                0, 0, 1, -6,
                0, 0, 0, 1),
                n1.getRootToNodeSpace().toMatrix());

    }


    @Test
    public void testUpdateRootToNodeTransform(){

        final SceneNode scene = new DefaultSceneNode(3);
        scene.getNodeTransform().getTranslation().setXYZ(50, 60, 70);
        scene.getNodeTransform().notifyChanged();
        final SceneNode n1 = new DefaultSceneNode(3);
        n1.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        n1.getNodeTransform().notifyChanged();
        scene.getChildren().add(n1);
        
        n1.setRootToNodeSpace(new NodeTransform(
                new Matrix3x3().setToIdentity(),
                new Vector(1,1,1),
                new Vector(-52,-64,-76)));

        //check parent has not changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 50,
                0, 1, 0, 60,
                0, 0, 1, 70,
                0, 0, 0, 1),
                scene.getNodeTransform().asMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -50,
                0, 1, 0, -60,
                0, 0, 1, -70,
                0, 0, 0, 1),
                scene.getRootToNodeSpace().toMatrix());

        //check the n1 transform has changed
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, 2,
                0, 1, 0, 4,
                0, 0, 1, 6,
                0, 0, 0, 1),
                n1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Matrix4x4(
                1, 0, 0, -52,
                0, 1, 0, -64,
                0, 0, 1, -76,
                0, 0, 0, 1),
                n1.getRootToNodeSpace().toMatrix());

    }

}
