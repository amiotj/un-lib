
package science.unlicense.api.layout;

import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.LineLayout;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class LineLayoutTest {

    @Test
    public void placementTest(){

        final Space butTop = new Space(new Extent.Double(2, 2),BorderConstraint.TOP);
        final Space butBottom = new Space(new Extent.Double(2, 2),BorderConstraint.BOTTOM);
        final Space butLeft = new Space(new Extent.Double(2, 2),BorderConstraint.LEFT);
        final Space butRight = new Space(new Extent.Double(2, 2),BorderConstraint.RIGHT);
        final Space butCenter = new Space(new Extent.Double(2, 2),BorderConstraint.CENTER);
        
        final Sequence elements = new ArraySequence(new Object[]{butTop,butBottom,butLeft,butRight,butCenter});
        final LineLayout layout = new LineLayout();
        layout.setPositionables(elements);

        //placement element with enough size available
        layout.setView(new BBox(new Vector(0, 0), new Vector(500, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(2,2),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 1,
                0, 0, 1),
                butTop.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 7,
                0, 1, 1,
                0, 0, 1),
                butBottom.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 13,
                0, 1, 1,
                0, 0, 1),
                butLeft.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 19,
                0, 1, 1,
                0, 0, 1),
                butRight.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 25,
                0, 1, 1,
                0, 0, 1),
                butCenter.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(26d, 2d),layout.getExtents().getBest(null));


        //change size , not enough space on x for all elements
        layout.setView(new BBox(new Vector(0, 0), new Vector(17, 200)));
        layout.update();
        Assert.assertEquals(new Extent.Double(2,2),butTop.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 1,
                0, 0, 1),
                butTop.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butBottom.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 7,
                0, 1, 1,
                0, 0, 1),
                butBottom.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butLeft.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 13,
                0, 1, 1,
                0, 0, 1),
                butLeft.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butRight.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 1,
                0, 1, 7,
                0, 0, 1),
                butRight.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(2,2),butCenter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 7,
                0, 1, 7,
                0, 0, 1),
                butCenter.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(14d, 8d),layout.getExtents().getBest(null));
    }
    
}
