
package science.unlicense.api.layout;

import science.unlicense.api.layout.SplitConstraint;
import science.unlicense.api.layout.SplitLayout;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class SplitLayoutTest {
    
    @Test
    public void test2(){
        
        final Space but1 = new Space(new Extent.Double(10, 20));
        final Space but2 = new Space(new Extent.Double(20, 10));
        final Space splitter = new Space(new Extent.Double(3, 3));
        but1.setLayoutConstraint(SplitConstraint.TOP);
        but2.setLayoutConstraint(SplitConstraint.BOTTOM);
        splitter.setLayoutConstraint(SplitConstraint.SPLITTER);

        final SplitLayout layout = new SplitLayout();
        layout.setSplitGap(3);
        layout.setSplitPosition(30);
        layout.setVerticalSplit(true);
        layout.setPositionables(new ArraySequence(new Object[]{but1,but2,splitter}));


        //first check ----------------------------------------------------------
        layout.setView(new BBox(new Vector(0, 0), new Vector(100, 63)));
        layout.update();
        Assert.assertEquals(new Extent.Double(100, 30),but1.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 15,
                0, 0, 1),
                but1.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(100, 30),but2.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 30+3+15,
                0, 0, 1),
                but2.getNodeTransform().asMatrix());
        Assert.assertEquals(new Extent.Double(100, 3),splitter.getEffectiveExtent());
        Assert.assertEquals(new Matrix3x3(
                1, 0, 50,
                0, 1, 30+1.5,
                0, 0, 1),
                splitter.getNodeTransform().asMatrix());

        //check best size
        Assert.assertEquals(new Extent.Double(20, 33),layout.getExtents().getBest(null));

    }
    
}
