package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.index.quadtrees.QuadTreePruneLimit;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeTraversalType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeMemberType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeType;
import science.unlicense.api.geometry.index.quadtrees.QuadrantType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeLimit;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeRaycastResult;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;

import science.unlicense.api.geometry.index.Dimensions;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.geometry.index.Rectangle;
import science.unlicense.api.geometry.index.utilities.TestUtilities;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class QuadTreePruneLimitTest extends QuadTreeCommonTests {
    
    private static final double DELTA = 0.0000001;
    
  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad128()
  {
    try {
      final Vector size = new Vector(128, 128);
      final Vector position = new Vector(0,0);
      final Vector size_minimum = new Vector(2, 2);
      return QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad16()
  {
    try {
      final Vector size = new Vector(16, 16);
      final Vector position = new Vector(0,0);
      final Vector size_minimum = new Vector(2, 2);
      return QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad2()
  {
    try {
      final Vector size = new Vector(2, 2);
      final Vector position = new Vector(0,0);
      final Vector size_minimum = new Vector(2, 2);
      return QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad32()
  {
    try {
      final Vector size = new Vector(32, 32);
      final Vector position = new Vector(0,0);
      final Vector size_minimum = new Vector(2, 2);
      return QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Override <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad512()
  {
    try {
      final Vector size = new Vector(512, 512);
      final Vector position = new Vector(0,0);
      final Vector size_minimum = new Vector(2, 2);
      return QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
    } catch (final Exception e) {
      Assert.fail(e.getMessage());
    }

    throw new RuntimeException("Unreachable code");
  }

  @Test public
  void
  testCreateLimitXOdd()
  {
    final Vector size = new Vector(4, 4);
    final Vector position = new Vector(0,0);
    final Vector size_minimum = new Vector(3, 2);
    try {
        QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public
  void
  testCreateLimitXTooBig()
  {
    final Vector size = new Vector(2, 2);
    final Vector position = new Vector(0,0);
    final Vector size_minimum = new Vector(4, 2);
    try {
        QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public
  void
  testCreateLimitXTooSmall()
  {
    final Vector size = new Vector(2, 2);
    final Vector position = new Vector(0,0);
    final Vector size_minimum = new Vector(1, 2);
    try {
        QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public
  void
  testCreateLimitYOdd()
  {
    final Vector size = new Vector(4, 4);
    final Vector position = new Vector(0,0);
    final Vector size_minimum = new Vector(2, 3);
    try {
        QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public
  void
  testCreateLimitYTooBig()
  {
    final Vector size = new Vector(2, 2);
    final Vector position = new Vector(0,0);
    final Vector size_minimum = new Vector(2, 4);
    try {
        QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public
  void
  testCreateLimitYTooSmall()
  {
    final Vector size = new Vector(2, 2);
    final Vector position = new Vector(0,0);
    final Vector size_minimum = new Vector(2, 1);
    try {
        QuadTreePruneLimit.newQuadTree(size, position, size_minimum);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public void testInsertLimit()
    throws Exception
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePruneLimit.newQuadTree(
        new Vector(16, 16),
        new Vector(0,0),
        new Vector(8, 8));

    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(0, 0));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    q.quadTreeTraverse(new QuadTreeTraversalType<Exception>() {
      @Override public void visit(
        final int depth,
        final Tuple lower,
        final Tuple upper)
          throws Exception
      {
        Assert.assertTrue(Dimensions.getSpanSizeX(lower, upper) >= 8);
        Assert.assertTrue(Dimensions.getSpanSizeX(lower, upper) >= 8);
      }
    });
  }

  @Test public void testInsertSplitNotX()
    throws Exception
  {
    final QuadTreeType<Rectangle> q =
      QuadTreeLimit.newQuadTree(
        new Vector(128, 128),
        new Vector(0,0),
        new Vector(128, 2));

    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(0, 0));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    final Counter counter = new Counter();
    q.quadTreeTraverse(counter);
    Assert.assertEquals(1, counter.count);
  }

  @Test public void testInsertSplitNotY()
    throws Exception
  {
    final QuadTreeType<Rectangle> q =
      QuadTreeLimit.newQuadTree(
        new Vector(128, 128),
        new Vector(0,0),
        new Vector(2, 128));

    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(0, 0));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    final Counter counter = new Counter();
    q.quadTreeTraverse(counter);
    Assert.assertEquals(1, counter.count);
  }

  @Test public void testRaycastQuadrants()
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePruneLimit.newQuadTree(
        new Vector(512, 512),
        new Vector(0,0),
        new Vector(2, 2));

    q.quadTreeInsert(new Rectangle(0, new Vector(32, 32), new Vector(
      80,
      80)));
    q.quadTreeInsert(new Rectangle(1, new Vector(400, 400), new Vector(
      480,
      480)));

    final Ray ray =
      new Ray(new Vector(0,0), new Vector(511, 511).normalize());
    final SortedSet<QuadTreeRaycastResult<QuadrantType>> items =
      new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
    q.quadTreeQueryRaycastQuadrants(ray, items);

    Assert.assertEquals(6, items.size());

    final Iterator<QuadTreeRaycastResult<QuadrantType>> iter
                = items.iterator();

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(0, quad.getLower().getX(),DELTA);
            Assert.assertEquals(0, quad.getLower().getY(),DELTA);
            Assert.assertEquals(63.875, quad.getUpper().getX(),DELTA);
            Assert.assertEquals(63.875, quad.getUpper().getY(),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(63.875, quad.getLower().getX(),DELTA);
            Assert.assertEquals(63.875, quad.getLower().getY(),DELTA);
            Assert.assertEquals(127.75, quad.getUpper().getX(),DELTA);
            Assert.assertEquals(127.75, quad.getUpper().getY(),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(127.75, quad.getLower().getX(),DELTA);
            Assert.assertEquals(127.75, quad.getLower().getY(),DELTA);
            Assert.assertEquals(255.5, quad.getUpper().getX(),DELTA);
            Assert.assertEquals(255.5, quad.getUpper().getY(),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(255.5, quad.getLower().getX(),DELTA);
            Assert.assertEquals(255.5, quad.getLower().getY(),DELTA);
            Assert.assertEquals(383.25, quad.getUpper().getX(),DELTA);
            Assert.assertEquals(383.25, quad.getUpper().getY(),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(383.25, quad.getLower().getX(),DELTA);
            Assert.assertEquals(383.25, quad.getLower().getY(),DELTA);
            Assert.assertEquals(447.125, quad.getUpper().getX(),DELTA);
            Assert.assertEquals(447.125, quad.getUpper().getY(),DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(447.125, quad.getLower().getX(),DELTA);
            Assert.assertEquals(447.125, quad.getLower().getY(),DELTA);
            Assert.assertEquals(511, quad.getUpper().getX(),DELTA);
            Assert.assertEquals(511, quad.getUpper().getY(),DELTA);
        }

    Assert.assertFalse(iter.hasNext());
  }

  @Test public void testRaycastQuadrantsNegativeRay()
  {
    final QuadTreeType<Rectangle> q =
      QuadTreePruneLimit.newQuadTree(
        new Vector(512, 512),
        new Vector(0,0),
        new Vector(2, 2));

    final Ray ray =
      new Ray(new Vector(512, 512), new Vector(
        -0.5,
        -0.5).normalize());
    final SortedSet<QuadTreeRaycastResult<QuadrantType>> items =
      new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
    q.quadTreeQueryRaycastQuadrants(ray, items);

    Assert.assertEquals(1, items.size());
  }

  @Test public void testRemovePrune()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle[] rectangles =
      TestUtilities.makeRectangles(0, q.quadTreeGetSizeX());

    for (final Rectangle r : rectangles) {
      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);
      }

      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertFalse(in);
      }
    }

    {
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(21, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[0]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(17, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[1]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(13, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[2]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(9, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(rectangles[3]);
      Assert.assertTrue(removed);

      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(1, counter.count);
    }

    {
      final IterationCounter counter = new IterationCounter();
      q.quadTreeIterateObjects(counter);
      Assert.assertEquals(0, counter.count);
    }

    for (final Rectangle r : rectangles) {
      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);
      }

      {
        final boolean in = q.quadTreeInsert(r);
        Assert.assertFalse(in);
      }
    }

    {
      final IterationCounter counter = new IterationCounter();
      q.quadTreeIterateObjects(counter);
      Assert.assertEquals(4, counter.count);
    }

    {
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(21, counter.count);
    }
  }

  @Test public void testRemovePruneNotLeafNotEmpty()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();

    /**
     * The rectangles are larger than the smallest quadrant size, and as a
     * result, will not be inserted into leaf nodes. Removing one of them will
     * trigger an attempt to prune nodes, which will fail due to a non-empty
     * non-leaf node.
     */

    final Rectangle r0 =
      new Rectangle(0, new Vector(1, 1), new Vector(7, 7));
    final Rectangle r1 =
      new Rectangle(1, new Vector(1, 1), new Vector(7, 7));

    {
      final boolean in = q.quadTreeInsert(r0);
      Assert.assertTrue(in);
    }

    {
      final boolean in = q.quadTreeInsert(r1);
      Assert.assertTrue(in);
    }

    {
      // (4 ^ 0) + (4 ^ 1) + 4
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(9, counter.count);
    }

    {
      final boolean removed = q.quadTreeRemove(r0);
      Assert.assertTrue(removed);
    }

    {
      // (4 ^ 0) + (4 ^ 1)
      final Counter counter = new Counter();
      q.quadTreeTraverse(counter);
      Assert.assertEquals(5, counter.count);
    }
  }

}
