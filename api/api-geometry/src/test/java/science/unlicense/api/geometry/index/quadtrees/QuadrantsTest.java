
package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.index.quadtrees.Quadrants;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.math.Vector;


/**
 * @author Mark Raynsford
 */
public final class QuadrantsTest{
    
    private static final double DELTA = 0.0000001;
    
  @Test public void testQuadrantsSimple()
  {
    final Vector lower = new Vector(8, 8);
    final Vector upper = new Vector(15, 15);
    final Quadrants q = Quadrants.split(lower, upper);

    Assert.assertEquals(8, q.getX0Y0Lower().getX(),DELTA);
    Assert.assertEquals(8, q.getX0Y0Lower().getY(),DELTA);
    Assert.assertEquals(11.5, q.getX0Y0Upper().getX(),DELTA);
    Assert.assertEquals(11.5, q.getX0Y0Upper().getY(),DELTA);

    Assert.assertEquals(11.5, q.getX1Y0Lower().getX(),DELTA);
    Assert.assertEquals(8, q.getX1Y0Lower().getY(),DELTA);
    Assert.assertEquals(15, q.getX1Y0Upper().getX(),DELTA);
    Assert.assertEquals(11.5, q.getX1Y0Upper().getY(),DELTA);

    Assert.assertEquals(8, q.getX0Y1Lower().getX(),DELTA);
    Assert.assertEquals(11.5, q.getX0Y1Lower().getY(),DELTA);
    Assert.assertEquals(11.5, q.getX0Y1Upper().getX(),DELTA);
    Assert.assertEquals(15, q.getX0Y1Upper().getY(),DELTA);

    Assert.assertEquals(11.5, q.getX1Y1Lower().getX(),DELTA);
    Assert.assertEquals(11.5, q.getX1Y1Lower().getY(),DELTA);
    Assert.assertEquals(15, q.getX1Y1Upper().getX(),DELTA);
    Assert.assertEquals(15, q.getX1Y1Upper().getY(),DELTA);
  }
}
