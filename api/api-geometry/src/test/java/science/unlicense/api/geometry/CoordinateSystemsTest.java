
package science.unlicense.api.geometry;

import science.unlicense.api.geometry.coordsys.Direction;
import science.unlicense.api.geometry.coordsys.CoordinateSystems;
import science.unlicense.api.geometry.coordsys.Axis;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.unit.Units;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class CoordinateSystemsTest {
    
    private static final double DELTA = 0.0000001;
    
    @Test
    public void testTransform(){
        
        final CoordinateSystem source = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.BACKWARD, Units.CENTIMETER),
                new Axis(Direction.LEFT, Units.CENTIMETER),
                new Axis(Direction.UP, Units.CENTIMETER),
            }
        );
        final CoordinateSystem target = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.FORWARD, Units.METER)
            }
        );
        
        final Transform trs = CoordinateSystems.createTransform(source, target);
        
        Tuple tuple = new DefaultTuple(1, 2, 3);        
        tuple = trs.transform(tuple, null);
        
        Assert.assertEquals(-0.02, tuple.getX(), DELTA);
        Assert.assertEquals(+0.03, tuple.getY(), DELTA);
        Assert.assertEquals(-0.01, tuple.getZ(), DELTA);
        
    }
    
    @Test
    public void testReduceTransform(){
        
        final CoordinateSystem source = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.RIGHT, Units.METER)
            }
        );
        final CoordinateSystem target = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.FORWARD, Units.METER),
                new Axis(Direction.RIGHT, Units.METER)
            }
        );
        
        final Transform trs = CoordinateSystems.createTransform(source, target);
        Assert.assertEquals(3, trs.getInputDimensions());
        Assert.assertEquals(2, trs.getOutputDimensions());
        
        Tuple tuple = new DefaultTuple(1, 2, 3);        
        tuple = trs.transform(tuple, null);
        Assert.assertEquals(2, tuple.getSize());
        
        Assert.assertEquals(1, tuple.getX(), DELTA);
        Assert.assertEquals(3, tuple.getY(), DELTA);
    }
    
}
