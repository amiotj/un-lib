package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.geometry.index.octtrees.OctTreeType;
import science.unlicense.api.geometry.index.octtrees.OctTreeLimit;
import science.unlicense.api.geometry.index.octtrees.OctTreeMemberType;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;

import science.unlicense.api.geometry.index.Cuboid;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class OctTreeLimitTest extends OctTreeCommonTests {

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(128, 128, 128),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128Offset64() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(128, 128, 128),
                    new Vector(64, 64, 64),
                    new Vector(128, 128, 128));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128OffsetM64() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(128, 128, 128),
                    new Vector(-64, -64, -64),
                    new Vector(128, 128, 128));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct16() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(16, 16, 16),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(2, 2, 2),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_2_4() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(2, 2, 4),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_2() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(2, 4, 2),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_4() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(2, 4, 4),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_2() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(4, 2, 2),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_4() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(4, 2, 4),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_4_2() {
        try {
            return OctTreeLimit.newOctTree(
                    new Vector(4, 4, 2),
                    new Vector(0,0,0),
                    new Vector(2, 2, 2));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testCreateLimitOddX() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(3, 4, 4));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitOddY() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(4, 3, 4));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitOddZ() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(4, 4, 3));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooLargeX() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(6, 4, 4));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooLargeY() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(4, 6, 4));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooLargeZ() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(4, 4, 6));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooSmallX() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(1, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooSmallY() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(2, 1, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateLimitTooSmallZ() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(4, 4, 4),
                new Vector(0,0,0),
                new Vector(2, 2, 1));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddX() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(3, 2, 2),
                new Vector(0,0,0),
                new Vector(2, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddY() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(2, 3, 2),
                new Vector(0,0,0),
                new Vector(2, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateOddZ() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(2, 2, 3),
                new Vector(0,0,0),
                new Vector(2, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallX() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(1, 2, 2),
                new Vector(0,0,0),
                new Vector(2, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallY() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(2, 1, 2),
                new Vector(0,0,0),
                new Vector(2, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testCreateTooSmallZ() {
        try {
            OctTreeLimit.newOctTree(
                new Vector(2, 2, 1),
                new Vector(0,0,0),
                new Vector(2, 2, 2));
            Assert.fail("Exception expected");
        }catch (InvalidArgumentException ex) {}
    }

    @Test
    public final void testInsertLeafNoSplit() throws Exception {
        final OctTreeType<Cuboid> q
                = OctTreeLimit.newOctTree(
                        new Vector(128, 128, 128),
                        new Vector(0,0,0),
                        new Vector(128, 128, 128));

        final Counter counter = new Counter();
        final Cuboid r = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(counter);
        Assert.assertEquals(1, counter.count);
    }

    @Test
    public final void testInsertSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct128();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(49, c.count);
    }

    @Test
    public final void testInsertSplitNotX() throws Exception {
        final OctTreeType<Cuboid> q
                = OctTreeLimit.newOctTree(
                        new Vector(128, 128, 128),
                        new Vector(0,0,0),
                        new Vector(128, 2, 2));

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitNotY() throws Exception {
        final OctTreeType<Cuboid> q
                = OctTreeLimit.newOctTree(
                        new Vector(128, 128, 128),
                        new Vector(0,0,0),
                        new Vector(2, 128, 2));

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }

    @Test
    public final void testInsertSplitNotZ() throws Exception {
        final OctTreeType<Cuboid> q
                = OctTreeLimit.newOctTree(
                        new Vector(128, 128, 128),
                        new Vector(0,0,0),
                        new Vector(2, 2, 128));

        final Counter c = new Counter();
        final Cuboid r = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(1, c.count);
    }
}
