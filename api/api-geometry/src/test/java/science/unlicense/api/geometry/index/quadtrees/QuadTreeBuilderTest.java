package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.index.quadtrees.QuadTreePruneLimit;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeSDPruneLimit;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeBuilderType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeBuilder;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeSDPrune;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeSDBasic;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeLimit;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeBasic;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeSDLimit;
import science.unlicense.api.geometry.index.quadtrees.QuadTreePrune;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;

import science.unlicense.api.geometry.index.Rectangle;

/**
 * @author Mark Raynsford
 */
public final class QuadTreeBuilderTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testBasic() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.disableLimitedQuadrantSizes();
        b.disablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.build();
        Assert.assertTrue(r instanceof QuadTreeBasic);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testLimit() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.enableLimitedQuadrantSizes(32, 48);
        b.disablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.build();
        Assert.assertTrue(r instanceof QuadTreeLimit);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testPrune() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.disableLimitedQuadrantSizes();
        b.enablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.build();
        Assert.assertTrue(r instanceof QuadTreePrune);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testPruneLimit() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.enableLimitedQuadrantSizes(32, 48);
        b.enablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.build();
        Assert.assertTrue(r instanceof QuadTreePruneLimit);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testSDBasic() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.disableLimitedQuadrantSizes();
        b.disablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.buildWithSD();
        Assert.assertTrue(r instanceof QuadTreeSDBasic);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testSDLimit() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.enableLimitedQuadrantSizes(32, 48);
        b.disablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.buildWithSD();
        Assert.assertTrue(r instanceof QuadTreeSDLimit);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testSDPrune() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.disableLimitedQuadrantSizes();
        b.enablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.buildWithSD();
        Assert.assertTrue(r instanceof QuadTreeSDPrune);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void testSDPruneLimit() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        b.enableLimitedQuadrantSizes(32, 48);
        b.enablePruning();
        b.setPosition2i(0, 4);
        b.setSize2i(128, 64);

        final QuadTreeType<Rectangle> r = b.buildWithSD();
        Assert.assertTrue(r instanceof QuadTreeSDPruneLimit);
        Assert.assertEquals(128, r.quadTreeGetSizeX(), DELTA);
        Assert.assertEquals(64, r.quadTreeGetSizeY(), DELTA);
        Assert.assertEquals(0, r.quadTreeGetPositionX(), DELTA);
        Assert.assertEquals(4, r.quadTreeGetPositionY(), DELTA);
    }

    @Test
    public void
            testTooSmallLimitX() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        try {
            b.enableLimitedQuadrantSizes(0, 2);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void
            testTooSmallLimitY() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        try {
            b.enableLimitedQuadrantSizes(2, 0);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void
            testTooSmallX() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        try {
            b.setSize2i(0, 2);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }

    @Test
    public void
            testTooSmallY() {
        final QuadTreeBuilderType<Rectangle> b = QuadTreeBuilder.newBuilder();
        try {
            b.setSize2i(2, 0);
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {
        }
    }
}
