package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.index.quadtrees.QuadTreeSDType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeMemberType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeType;
import science.unlicense.api.geometry.index.quadtrees.QuadrantType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeSDBasic;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeRaycastResult;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;

import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.geometry.index.SDType;
import science.unlicense.api.geometry.index.Rectangle;
import science.unlicense.api.geometry.index.utilities.TestUtilities;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class QuadTreeSDBasicTest extends QuadTreeCommonTests {

    private static final double DELTA = 0.0000001;

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad128() {
        try {
            final Vector size = new Vector(128, 128);
            final Vector position = new Vector(0, 0);
            return QuadTreeSDBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad16() {
        try {
            final Vector size = new Vector(16, 16);
            final Vector position = new Vector(0, 0);
            return QuadTreeSDBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad2() {
        try {
            final Vector size = new Vector(2, 2);
            final Vector position = new Vector(0, 0);
            return QuadTreeSDBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad32() {
        try {
            final Vector size = new Vector(32, 32);
            final Vector position = new Vector(0, 0);
            return QuadTreeSDBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad512() {
        try {
            final Vector size = new Vector(512, 512);
            final Vector position = new Vector(0, 0);
            return QuadTreeSDBasic.newQuadTree(size, position);
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testClearDynamic()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(32, 32), new Vector(0, 0));
        final Rectangle[] dynamics = TestUtilities.makeRectangles(0, 32);
        final Rectangle[] statics = TestUtilities.makeRectangles(10, 32);

        for (final Rectangle r : dynamics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }
        for (final Rectangle r : statics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.quadTreeIterateObjects(counter);
            Assert.assertEquals(8, counter.count);
        }

        q.quadTreeSDClearDynamic();

        {
            final IterationCounter counter = new IterationCounter();
            q.quadTreeIterateObjects(counter);
            Assert.assertEquals(4, counter.count);
        }
    }

    @Test
    public void testCreateSDXOdd() {
        try {
            QuadTreeSDBasic.newQuadTree(new Vector(3, 2), new Vector(0, 0));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateSDXTooSmall() {
        try {
            QuadTreeSDBasic.newQuadTree(new Vector(1, 2), new Vector(0, 0));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateSDYOdd() {
        try {
            QuadTreeSDBasic.newQuadTree(new Vector(4, 3), new Vector(0, 0));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateSDYTooSmall() {
        try {
            QuadTreeSDBasic.newQuadTree(new Vector(2, 1), new Vector(0, 0));
            Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void testInsertSplitNotX()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(2, 4), new Vector(0, 0));

        final Rectangle r
                = new Rectangle(0, new Vector(0, 0), new Vector(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        final Counter counter = new Counter();
        q.quadTreeTraverse(counter);
        Assert.assertEquals(5, counter.count);
    }

    @Test
    public void testInsertSplitNotY()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(4, 2), new Vector(0, 0));

        final Rectangle r
                = new Rectangle(0, new Vector(0, 0), new Vector(0, 0));

        final boolean in = q.quadTreeInsert(r);
        Assert.assertTrue(in);

        final Counter counter = new Counter();
        q.quadTreeTraverse(counter);
        Assert.assertEquals(5, counter.count);
    }

    @Test
    public void testInsertTypeDynamicCollision()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(16, 16), new Vector(0, 0));

        final Rectangle r
                = new Rectangle(0, new Vector(0, 0), new Vector(12, 12));

        boolean in = false;
        in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
        Assert.assertTrue(in);
        in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
        Assert.assertFalse(in);
    }

    @Test
    public void testInsertTypeStaticCollision()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(16, 16), new Vector(0, 0));

        final Rectangle r
                = new Rectangle(0, new Vector(0, 0), new Vector(12, 12));

        boolean in = false;
        in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
        Assert.assertTrue(in);
        in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
        Assert.assertFalse(in);
    }

    @Test
    public void testIterateStopEarlyDynamic()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(16, 16), new Vector(0, 0));

        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 16);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        final IterationChecker1 counter = new IterationChecker1() {
            @Override
            public Boolean evaluate(final Object candidate) {
                ++this.count;
                if (this.count >= 2) {
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }
        };

        q.quadTreeIterateObjects(counter);

        Assert.assertEquals(2, counter.count);
    }

    @Test
    public void testIterateStopEarlyStatic()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(16, 16), new Vector(0, 0));

        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 16);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        final IterationChecker1 counter = new IterationChecker1() {
            @Override
            public Boolean evaluate(final Object candidate) {
                ++this.count;
                if (this.count >= 2) {
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }
        };

        q.quadTreeIterateObjects(counter);

        Assert.assertEquals(2, counter.count);
    }

    @Test
    public void testQueryContainingStatic() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(128, 128), new Vector(0, 0));

        final boolean in
                = q.quadTreeInsertSD(new Rectangle(
                        0,
                        new Vector(66, 66),
                        new Vector(127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaContaining(new Rectangle(
                    0,
                    new Vector(66, 66),
                    new Vector(127, 127)), items);

            Assert.assertEquals(1, items.size());
        }
    }

    @Test
    public void testQueryContainingStaticNot() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(128, 128), new Vector(0, 0));

        final boolean in
                = q.quadTreeInsertSD(new Rectangle(
                        0,
                        new Vector(66, 66),
                        new Vector(127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaContaining(new Rectangle(
                    0,
                    new Vector(0, 0),
                    new Vector(65, 65)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testQueryOverlappingStatic() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(128, 128), new Vector(0, 0));

        final Rectangle[] dynamics = TestUtilities.makeRectangles(0, 128);
        final Rectangle[] statics = TestUtilities.makeRectangles(10, 128);

        for (final Rectangle r : dynamics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }
        for (final Rectangle r : statics) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaOverlapping(new Rectangle(
                    0,
                    new Vector(16, 16),
                    new Vector(80, 80)), items);

            Assert.assertEquals(8, items.size());
        }
    }

    @Test
    public void testQueryOverlappingStaticNot() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(128, 128), new Vector(0, 0));

        final boolean in
                = q.quadTreeInsertSD(new Rectangle(
                        0,
                        new Vector(66, 66),
                        new Vector(127, 127)), SDType.SD_STATIC);
        Assert.assertTrue(in);

        {
            final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
            q.quadTreeQueryAreaOverlapping(new Rectangle(
                    0,
                    new Vector(0, 0),
                    new Vector(65, 65)), items);

            Assert.assertEquals(0, items.size());
        }
    }

    @Test
    public void testRaycastQuadrants() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(512, 512), new Vector(0, 0));

        q.quadTreeInsert(new Rectangle(0, new Vector(32, 32), new Vector(
                80,
                80)));
        q.quadTreeInsert(new Rectangle(1, new Vector(400, 400), new Vector(
                480,
                480)));

        final Ray ray
                = new Ray(new Vector(0, 0), new Vector(511, 511).normalize());
        final SortedSet<QuadTreeRaycastResult<QuadrantType>> items
                = new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
        q.quadTreeQueryRaycastQuadrants(ray, items);

        Assert.assertEquals(6, items.size());

        final Iterator<QuadTreeRaycastResult<QuadrantType>> iter
                = items.iterator();

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(0, quad.getLower().getX(), DELTA);
            Assert.assertEquals(0, quad.getLower().getY(), DELTA);
            Assert.assertEquals(63.875, quad.getUpper().getX(), DELTA);
            Assert.assertEquals(63.875, quad.getUpper().getY(), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(63.875, quad.getLower().getX(), DELTA);
            Assert.assertEquals(63.875, quad.getLower().getY(), DELTA);
            Assert.assertEquals(127.75, quad.getUpper().getX(), DELTA);
            Assert.assertEquals(127.75, quad.getUpper().getY(), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(127.75, quad.getLower().getX(), DELTA);
            Assert.assertEquals(127.75, quad.getLower().getY(), DELTA);
            Assert.assertEquals(255.5, quad.getUpper().getX(), DELTA);
            Assert.assertEquals(255.5, quad.getUpper().getY(), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(255.5, quad.getLower().getX(), DELTA);
            Assert.assertEquals(255.5, quad.getLower().getY(), DELTA);
            Assert.assertEquals(383.25, quad.getUpper().getX(), DELTA);
            Assert.assertEquals(383.25, quad.getUpper().getY(), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(383.25, quad.getLower().getX(), DELTA);
            Assert.assertEquals(383.25, quad.getLower().getY(), DELTA);
            Assert.assertEquals(447.125, quad.getUpper().getX(), DELTA);
            Assert.assertEquals(447.125, quad.getUpper().getY(), DELTA);
        }

        {
            final QuadTreeRaycastResult<QuadrantType> rq = iter.next();
            final QuadrantType quad = rq.getObject();
            Assert.assertEquals(447.125, quad.getLower().getX(), DELTA);
            Assert.assertEquals(447.125, quad.getLower().getY(), DELTA);
            Assert.assertEquals(511, quad.getUpper().getX(), DELTA);
            Assert.assertEquals(511, quad.getUpper().getY(), DELTA);
        }

        Assert.assertFalse(iter.hasNext());
    }

    @Test
    public void testRaycastQuadrantsNegativeRay() {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(512, 512), new Vector(0, 0));

        final Ray ray
                = new Ray(new Vector(512, 512), new Vector(
                        -0.5,
                        -0.5).normalize());
        final SortedSet<QuadTreeRaycastResult<QuadrantType>> items
                = new TreeSet<QuadTreeRaycastResult<QuadrantType>>();
        q.quadTreeQueryRaycastQuadrants(ray, items);

        Assert.assertEquals(1, items.size());
    }

    @Test
    public void testRemoveSubDynamic()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(32, 32), new Vector(0, 0));
        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 32);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_DYNAMIC);
            Assert.assertTrue(in);
        }

        for (final Rectangle r : rectangles) {
            final boolean removed = q.quadTreeRemove(r);
            Assert.assertTrue(removed);
        }
    }

    @Test
    public void testRemoveSubStatic()
            throws Exception {
        final QuadTreeSDType<Rectangle> q
                = QuadTreeSDBasic.newQuadTree(new Vector(32, 32), new Vector(0, 0));
        final Rectangle[] rectangles = TestUtilities.makeRectangles(0, 32);

        for (final Rectangle r : rectangles) {
            final boolean in = q.quadTreeInsertSD(r, SDType.SD_STATIC);
            Assert.assertTrue(in);
        }

        for (final Rectangle r : rectangles) {
            final boolean removed = q.quadTreeRemove(r);
            Assert.assertTrue(removed);
        }
    }
}
