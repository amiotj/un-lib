package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.geometry.index.octtrees.OctTreeType;
import science.unlicense.api.geometry.index.octtrees.OctTreePrune;
import science.unlicense.api.geometry.index.octtrees.OctTreeMemberType;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;

import science.unlicense.api.geometry.index.Cuboid;
import science.unlicense.api.geometry.index.utilities.TestUtilities;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class OctTreePruneTest extends OctTreeCommonTests {

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct128() {
        try {
            return OctTreePrune.newOctTree(
                    new Vector(128, 128, 128),
                    new Vector(0, 0, 0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>>
            OctTreeType<T>
            makeOct128Offset64() {
        try {
            return OctTreePrune.newOctTree(
                    new Vector(128, 128, 128),
                    new Vector(64, 64, 64));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>>
            OctTreeType<T>
            makeOct128OffsetM64() {
        try {
            return OctTreePrune.newOctTree(
                    new Vector(128, 128, 128),
                    new Vector(-64, -64, -64));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct16() {
        try {
            return OctTreePrune.newOctTree(
                    new Vector(16, 16, 16),
                    new Vector(0, 0, 0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2() {
        try {
            return OctTreePrune.newOctTree(new Vector(2, 2, 2), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_2_4() {
        try {
            return OctTreePrune.newOctTree(new Vector(2, 2, 4), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_2() {
        try {
            return OctTreePrune.newOctTree(new Vector(2, 4, 2), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct2_4_4() {
        try {
            return OctTreePrune.newOctTree(new Vector(2, 4, 4), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_2() {
        try {
            return OctTreePrune.newOctTree(new Vector(4, 2, 2), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_2_4() {
        try {
            return OctTreePrune.newOctTree(new Vector(4, 2, 4), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Override
    <T extends OctTreeMemberType<T>> OctTreeType<T> makeOct4_4_2() {
        try {
            return OctTreePrune.newOctTree(new Vector(4, 4, 2), new Vector(
                    0,
                    0,
                    0));
        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }

        throw new RuntimeException("Unreachable code");
    }

    @Test
    public void testCreateOddX() {
        try {
            OctTreePrune.newOctTree(new Vector(3, 2, 2), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateOddY() {
        try {
            OctTreePrune.newOctTree(new Vector(2, 3, 2), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateOddZ() {
        try {
            OctTreePrune.newOctTree(new Vector(2, 2, 3), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreatePruneOddX() {
        try {
            OctTreePrune.newOctTree(new Vector(3, 4, 4), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreatePruneOddY() {
        try {
            OctTreePrune.newOctTree(new Vector(4, 3, 4), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreatePruneOddZ() {
        try {
            OctTreePrune.newOctTree(new Vector(4, 4, 3), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateTooSmallX() {
       try {
           OctTreePrune.newOctTree(new Vector(1, 2, 2), new Vector(0, 0, 0));
           Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateTooSmallY() {
        try {
            OctTreePrune.newOctTree(new Vector(2, 1, 2), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public void
            testCreateTooSmallZ() {
        try {
            OctTreePrune.newOctTree(new Vector(2, 2, 1), new Vector(0, 0, 0));
            Assert.fail("Exception expected");
    } catch (InvalidArgumentException ex) {}
    }

    @Test
    public final void testInsertLeafNoSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplit() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct128();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(57, c.count);
    }

    @Test
    public final void testInsertSplitNot() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitXNotYNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_2_2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitXYNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_4_2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitXZNotY() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct4_2_4();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitYNotZNotZ() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_4_2();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitYZNotX() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_4_4();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public final void testInsertSplitZNotXNotY() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct2_2_4();

        final Counter c = new Counter();
        final Cuboid r
                = new Cuboid(0, new Vector(0, 0, 0), new Vector(0, 0, 0));

        final boolean in = q.octTreeInsert(r);
        Assert.assertTrue(in);

        q.octTreeTraverse(c);
        Assert.assertEquals(9, c.count);
    }

    @Test
    public void testRemovePrune() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct128();
        final Cuboid[] cuboids = TestUtilities.makeCuboids(0, 128);

        for (final Cuboid c : cuboids) {
            {
                final boolean in = q.octTreeInsert(c);
                Assert.assertTrue(in);
            }

            {
                final boolean in = q.octTreeInsert(c);
                Assert.assertFalse(in);
            }
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2)
            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(73, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8
            final boolean removed = q.octTreeRemove(cuboids[0]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(65, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8 - 8
            final boolean removed = q.octTreeRemove(cuboids[1]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(57, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8 - 8 - 8
            final boolean removed = q.octTreeRemove(cuboids[2]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(49, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8 - 8 - 8 - 8
            final boolean removed = q.octTreeRemove(cuboids[3]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(41, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8 - 8 - 8 - 8 - 8
            final boolean removed = q.octTreeRemove(cuboids[4]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(33, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8 - 8 - 8 - 8 - 8 - 8
            final boolean removed = q.octTreeRemove(cuboids[5]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(25, counter.count);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + (8 ^ 2) - 8 - 8 - 8 - 8 - 8 - 8 - 8
            final boolean removed = q.octTreeRemove(cuboids[6]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(17, counter.count);
        }

        {
            // (8 ^ 0)
            final boolean removed = q.octTreeRemove(cuboids[7]);
            Assert.assertTrue(removed);

            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(1, counter.count);
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.octTreeIterateObjects(counter);
            Assert.assertEquals(0, counter.count);
        }

        for (final Cuboid c : cuboids) {
            {
                final boolean in = q.octTreeInsert(c);
                Assert.assertTrue(in);
            }

            {
                final boolean in = q.octTreeInsert(c);
                Assert.assertFalse(in);
            }
        }

        {
            final IterationCounter counter = new IterationCounter();
            q.octTreeIterateObjects(counter);
            Assert.assertEquals(8, counter.count);
        }

        {
            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(73, counter.count);
        }
    }

    @Test
    public void testRemovePruneNotLeafNotEmpty() throws Exception {
        final OctTreeType<Cuboid> q = this.makeOct16();

        /**
         * The cuboids are larger than the smallest octant size, and as a
         * result, will not be inserted into leaf nodes. Removing one of them
         * will trigger an attempt to prune nodes, which will fail due to a
         * non-empty non-leaf node.
         */
        final Cuboid c0
                = new Cuboid(0, new Vector(1, 1, 1), new Vector(7, 7, 7));
        final Cuboid c1
                = new Cuboid(1, new Vector(1, 1, 1), new Vector(7, 7, 7));

        {
            final boolean in = q.octTreeInsert(c0);
            Assert.assertTrue(in);
        }

        {
            final boolean in = q.octTreeInsert(c1);
            Assert.assertTrue(in);
        }

        {
            // (8 ^ 0) + (8 ^ 1) + 8
            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(17, counter.count);
        }

        {
            final boolean removed = q.octTreeRemove(c0);
            Assert.assertTrue(removed);
        }

        {
            // (8 ^ 0) + (8 ^ 1)
            final Counter counter = new Counter();
            q.octTreeTraverse(counter);
            Assert.assertEquals(9, counter.count);
        }
    }
}
