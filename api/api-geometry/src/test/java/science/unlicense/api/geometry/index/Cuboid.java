package science.unlicense.api.geometry.index;

import science.unlicense.api.CObjects;
import science.unlicense.api.geometry.index.octtrees.OctTreeMemberType;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class Cuboid extends OctTreeMemberType<Cuboid> {

    private final long id;

    public Cuboid(
            final long in_id,
            final Vector in_lower,
            final Vector in_upper) {
        this.id = in_id;
        this.lower.set(in_lower);
        this.upper.set(in_upper);
    }

    @Override
    public int compareTo(
            final Cuboid other) {
        CObjects.ensureNotNull(other, "Other");
        final Cuboid o = other;

        if (o.id < this.id) {
            return -1;
        }
        if (o.id > this.id) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(
            final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Cuboid other = (Cuboid) obj;
        if (!this.lower.equals(other.lower)) {
            return false;
        }
        if (!this.upper.equals(other.upper)) {
            return false;
        }
        return true;
    }

    public long getId() {
        return this.id;
    }

//  @Override public int hashCode()
//  {
//    final int prime = 31;
//    int result = 1;
//    result = (prime * result) + this.lower.hashCode();
//    result = (prime * result) + this.upper.hashCode();
//    return result;
//  }
//
//  @Override public String toString()
//  {
//    final StringBuilder b = new StringBuilder();
//    b.append("[Cuboid lower=");
//    b.append(this.lower);
//    b.append(", upper=");
//    b.append(this.upper);
//    b.append("]");
//    final String r = b.toString();
//    assert r != null;
//    return r;
//  }
}
