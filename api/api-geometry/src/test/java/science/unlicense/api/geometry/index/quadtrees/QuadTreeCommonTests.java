package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.index.quadtrees.QuadTreeMemberType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeTraversalType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeRaycastResult;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.geometry.index.Dimensions;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.geometry.index.Rectangle;
import science.unlicense.api.geometry.index.utilities.TestUtilities;
import science.unlicense.api.exception.NullArgumentException;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public abstract class QuadTreeCommonTests
{
    private static final double DELTA = 0.0000001;
    
  protected static final class Counter implements
  QuadTreeTraversalType<Exception>
  {
    int count = 0;

    Counter()
    {

    }

    @Override public void visit(
      final int depth,
      final Tuple lower,
      final Tuple upper)
        throws Exception
    {
      ++this.count;
    }
  }

  protected static abstract class IterationChecker0 implements Predicate {
      
    boolean found_r0 = false;
    boolean found_r1 = false;
    boolean found_r2 = false;
    boolean found_r3 = false;

    IterationChecker0()
    {

    }
  }

  protected static abstract class IterationChecker1 implements Predicate {
    int count = 0;

    IterationChecker1()
    {

    }
  }

  protected static final class IterationCounter implements Predicate {
    int count = 0;

    IterationCounter()
    {

    }

    @Override public Boolean evaluate(
        final Object candidate)
    {
      ++this.count;
      return Boolean.TRUE;
    }
  }

  abstract <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad128();

  abstract <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad16();

  abstract <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad2();

  abstract <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad32();

  abstract <T extends QuadTreeMemberType<T>> QuadTreeType<T> makeQuad512();

  @Test public void testClear()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle r0 =
      new Rectangle(0, new Vector(8, 8), new Vector(48, 48));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 80), new Vector(48, 120));
    final Rectangle r2 =
      new Rectangle(2, new Vector(80, 8), new Vector(120, 48));
    final Rectangle r3 =
      new Rectangle(3, new Vector(80, 80), new Vector(120, 120));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    {
      final IterationCounter counter = new IterationCounter();
      q.quadTreeIterateObjects(counter);
      Assert.assertEquals(4, counter.count);
    }

    {
      final IterationCounter counter = new IterationCounter();
      q.quadTreeClear();
      q.quadTreeIterateObjects(counter);
      Assert.assertEquals(0, counter.count);
    }
  }

  @Test public void testCountInitial()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Counter c = new Counter();

    q.quadTreeTraverse(c);
    Assert.assertEquals(1, c.count);
  }

  @Test public void testCreate()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    Assert.assertEquals(128, q.quadTreeGetSizeX(),DELTA);
    Assert.assertEquals(128, q.quadTreeGetSizeY(),DELTA);
    Assert.assertEquals(0, q.quadTreeGetPositionX(),DELTA);
    Assert.assertEquals(0, q.quadTreeGetPositionY(),DELTA);
  }

  @Test public void testInsertAtImmediateRootChildren()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Counter c = new Counter();
    final Rectangle r0 =
      new Rectangle(0, new Vector(0, 0), new Vector(7, 7));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 0), new Vector(15, 7));
    final Rectangle r2 =
      new Rectangle(2, new Vector(0, 8), new Vector(7, 15));
    final Rectangle r3 =
      new Rectangle(3, new Vector(8, 8), new Vector(15, 15));

    {
      final Tuple bal = r0.getLower();
      final Tuple bau = r0.getUpper();
      Assert.assertEquals(8, Dimensions.getSpanSizeX(bal, bau),DELTA);
      Assert.assertEquals(8, Dimensions.getSpanSizeY(bal, bau),DELTA);
    }

    {
      final Tuple bal = r1.getLower();
      final Tuple bau = r1.getUpper();
      Assert.assertEquals(8, Dimensions.getSpanSizeX(bal, bau),DELTA);
      Assert.assertEquals(8, Dimensions.getSpanSizeY(bal, bau),DELTA);
    }

    {
      final Tuple bal = r2.getLower();
      final Tuple bau = r2.getUpper();
      Assert.assertEquals(8, Dimensions.getSpanSizeX(bal, bau),DELTA);
      Assert.assertEquals(8, Dimensions.getSpanSizeY(bal, bau),DELTA);
    }

    {
      final Tuple bal = r3.getLower();
      final Tuple bau = r3.getUpper();
      Assert.assertEquals(8, Dimensions.getSpanSizeX(bal, bau),DELTA);
      Assert.assertEquals(8, Dimensions.getSpanSizeY(bal, bau),DELTA);
    }

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    q.quadTreeTraverse(c);
    Assert.assertEquals(21, c.count);
  }

  @Test public void testInsertAtRoot()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Counter c = new Counter();
    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(12, 12));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    q.quadTreeTraverse(c);
    Assert.assertEquals(5, c.count);
  }

  @Test public void testInsertDuplicate()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(12, 12));

    boolean in = false;
    in = q.quadTreeInsert(r);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r);
    Assert.assertFalse(in);
  }

  @Test public
  void
  testInsertIllFormed()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r =
      new Rectangle(0, new Vector(12, 12), new Vector(0, 0));

    try {
        q.quadTreeInsert(r);
        Assert.fail("Exception expected");
        } catch (InvalidArgumentException ex) {}
  }

  @Test public void testInsertOutside()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r =
      new Rectangle(0, new Vector(18, 18), new Vector(28, 28));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertFalse(in);
  }

  @Test public void testInsertTiny()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(0, 0));

    final boolean in = q.quadTreeInsert(r);
    Assert.assertTrue(in);

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaContaining(new Rectangle(
        0,
        new Vector(0, 0),
        new Vector(15, 15)), items);
      Assert.assertEquals(1, items.size());
    }
  }

  @Test public void testIterate()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r0 =
      new Rectangle(0, new Vector(0, 0), new Vector(7, 7));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 0), new Vector(15, 7));
    final Rectangle r2 =
      new Rectangle(2, new Vector(0, 8), new Vector(7, 15));
    final Rectangle r3 =
      new Rectangle(3, new Vector(8, 8), new Vector(15, 15));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    final IterationChecker0 counter = new IterationChecker0() {
      @Override 
      public Boolean evaluate(final Object candidate){
        if (candidate == r0) {
          this.found_r0 = true;
        }
        if (candidate == r1) {
          this.found_r1 = true;
        }
        if (candidate == r2) {
          this.found_r2 = true;
        }
        if (candidate == r3) {
          this.found_r3 = true;
        }

        return Boolean.TRUE;
      }
    };

    q.quadTreeIterateObjects(counter);

    Assert.assertTrue(counter.found_r0);
    Assert.assertTrue(counter.found_r1);
    Assert.assertTrue(counter.found_r2);
    Assert.assertTrue(counter.found_r3);
  }

  @Test
  public void testIterateNull()
    throws Throwable
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Predicate f = TestUtilities.actuallyNull();
    try {
        q.quadTreeIterateObjects(f);
        Assert.fail("Exception expected");
        } catch (NullArgumentException ex) {}
  }

  @Test 
  public void testIterateStopEarly()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r0 =
      new Rectangle(0, new Vector(0, 0), new Vector(7, 7));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 0), new Vector(15, 7));
    final Rectangle r2 =
      new Rectangle(2, new Vector(0, 8), new Vector(7, 15));
    final Rectangle r3 =
      new Rectangle(3, new Vector(8, 8), new Vector(15, 15));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    final IterationChecker1 counter = new IterationChecker1() {
      @Override 
      public Boolean evaluate(final Object candidate){
        ++this.count;
        if (this.count >= 2) {
          return Boolean.FALSE;
        }
        return Boolean.TRUE;
      }
    };

    q.quadTreeIterateObjects(counter);

    Assert.assertEquals(2, counter.count);
  }

  @Test public void testQueryContaining()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle r0 =
      new Rectangle(0, new Vector(8, 8), new Vector(48, 48));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 80), new Vector(48, 120));
    final Rectangle r2 =
      new Rectangle(2, new Vector(80, 8), new Vector(120, 48));
    final Rectangle r3 =
      new Rectangle(3, new Vector(80, 80), new Vector(120, 120));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaContaining(new Rectangle(0, new Vector(
        8 - 4,
        8 - 4), new Vector(48 + 4, 48 + 4)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r0));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaContaining(new Rectangle(0, new Vector(
        8 - 4,
        80 - 4), new Vector(48 + 2, 120 + 4)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r1));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaContaining(new Rectangle(0, new Vector(
        80 - 4,
        8 - 4), new Vector(120 + 4, 48 + 4)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r2));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaContaining(new Rectangle(0, new Vector(
        80 - 4,
        80 - 4), new Vector(120 + 4, 120 + 4)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r3));
    }
  }

  @Test public void testQueryContainingExact()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle r0 =
      new Rectangle(0, new Vector(8, 8), new Vector(48, 48));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 80), new Vector(48, 120));
    final Rectangle r2 =
      new Rectangle(2, new Vector(80, 8), new Vector(120, 48));
    final Rectangle r3 =
      new Rectangle(3, new Vector(80, 80), new Vector(120, 120));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaContaining(new Rectangle(
        0,
        new Vector(0, 0),
        new Vector(127, 127)), items);
      Assert.assertEquals(4, items.size());
      Assert.assertTrue(items.contains(r0));
      Assert.assertTrue(items.contains(r1));
      Assert.assertTrue(items.contains(r2));
      Assert.assertTrue(items.contains(r3));
    }
  }

  @Test public void testQueryOverlapping()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle r0 =
      new Rectangle(0, new Vector(8, 8), new Vector(48, 48));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 80), new Vector(48, 120));
    final Rectangle r2 =
      new Rectangle(2, new Vector(80, 8), new Vector(120, 48));
    final Rectangle r3 =
      new Rectangle(3, new Vector(80, 80), new Vector(120, 120));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(0, new Vector(
        8 - 2,
        8 - 2), new Vector(8 + 2, 8 + 2)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r0));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(0, new Vector(
        8 - 2,
        80 - 2), new Vector(8 + 2, 80 + 2)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r1));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(0, new Vector(
        80 - 2,
        8 - 2), new Vector(80 + 2, 8 + 2)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r2));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(0, new Vector(
        80 - 2,
        80 - 2), new Vector(80 + 2, 80 + 2)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r3));
    }
  }

  @Test public void testQueryOverlappingExact()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle r0 =
      new Rectangle(0, new Vector(8, 8), new Vector(48, 48));
    final Rectangle r1 =
      new Rectangle(1, new Vector(8, 80), new Vector(48, 120));
    final Rectangle r2 =
      new Rectangle(2, new Vector(80, 8), new Vector(120, 48));
    final Rectangle r3 =
      new Rectangle(3, new Vector(80, 80), new Vector(120, 120));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(0, new Vector(
        8 + 2,
        8 + 2), new Vector(120 - 2, 120 - 2)), items);
      Assert.assertEquals(4, items.size());
      Assert.assertTrue(items.contains(r0));
      Assert.assertTrue(items.contains(r1));
      Assert.assertTrue(items.contains(r2));
      Assert.assertTrue(items.contains(r3));
    }
  }

  @Test public void testQueryOverlappingNotAll()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    final Rectangle r0 =
      new Rectangle(0, new Vector(0, 0), new Vector(127, 127));
    final Rectangle r1 =
      new Rectangle(1, new Vector(4, 4), new Vector(127, 127));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(
        0,
        new Vector(0, 0),
        new Vector(2, 2)), items);
      Assert.assertEquals(1, items.size());
      Assert.assertTrue(items.contains(r0));
    }

    {
      final SortedSet<Rectangle> items = new TreeSet<Rectangle>();
      q.quadTreeQueryAreaOverlapping(new Rectangle(0, new Vector(
        127 - 4,
        127 - 4), new Vector(127, 127)), items);
      Assert.assertEquals(2, items.size());
      Assert.assertTrue(items.contains(r0));
      Assert.assertTrue(items.contains(r1));
    }
  }

  @Test public void testRaycast()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad512();

    q.quadTreeInsert(new Rectangle(0, new Vector(32, 32), new Vector(
      80,
      80)));
    q.quadTreeInsert(new Rectangle(1, new Vector(400, 32), new Vector(
      400 + 32,
      80)));
    q.quadTreeInsert(new Rectangle(2, new Vector(400, 400), new Vector(
      480,
      480)));

    final Ray ray =
      new Ray(new Vector(0,0), new Vector(511, 511).normalize());
    final SortedSet<QuadTreeRaycastResult<Rectangle>> items =
      new TreeSet<QuadTreeRaycastResult<Rectangle>>();
    q.quadTreeQueryRaycast(ray, items);

    Assert.assertEquals(2, items.size());

    final Iterator<QuadTreeRaycastResult<Rectangle>> iter = items.iterator();

    {
      final QuadTreeRaycastResult<Rectangle> rr = iter.next();
      final Rectangle r = rr.getObject();
      Assert.assertEquals(32, r.getLower().getX(),DELTA);
      Assert.assertEquals(32, r.getLower().getY(),DELTA);
    }

    {
      final QuadTreeRaycastResult<Rectangle> rr = iter.next();
      final Rectangle r = rr.getObject();
      Assert.assertEquals(400, r.getLower().getX(),DELTA);
      Assert.assertEquals(400, r.getLower().getY(),DELTA);
    }

    Assert.assertFalse(iter.hasNext());
  }

  @Test public void testRemove()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad16();
    final Rectangle r =
      new Rectangle(0, new Vector(0, 0), new Vector(12, 12));

    boolean in = false;
    in = q.quadTreeInsert(r);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r);
    Assert.assertFalse(in);

    boolean removed = false;
    removed = q.quadTreeRemove(r);
    Assert.assertTrue(removed);
    removed = q.quadTreeRemove(r);
    Assert.assertFalse(removed);
  }

  @Test public void testRemoveSub()
    throws Exception
  {
    final QuadTreeType<Rectangle> q = this.makeQuad32();

    final Rectangle r0 =
      new Rectangle(0, new Vector(2, 2), new Vector(4, 4));
    final Rectangle r1 =
      new Rectangle(1, new Vector(18, 2), new Vector(20, 4));
    final Rectangle r2 =
      new Rectangle(2, new Vector(2, 18), new Vector(4, 20));
    final Rectangle r3 =
      new Rectangle(3, new Vector(18, 18), new Vector(20, 20));

    boolean in = false;
    in = q.quadTreeInsert(r0);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r1);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r2);
    Assert.assertTrue(in);
    in = q.quadTreeInsert(r3);
    Assert.assertTrue(in);

    boolean removed = false;
    removed = q.quadTreeRemove(r0);
    Assert.assertTrue(removed);
    removed = q.quadTreeRemove(r1);
    Assert.assertTrue(removed);
    removed = q.quadTreeRemove(r2);
    Assert.assertTrue(removed);
    removed = q.quadTreeRemove(r3);
    Assert.assertTrue(removed);
  }

  @Test public void testToString()
  {
    final QuadTreeType<Rectangle> q = this.makeQuad128();
    System.err.println(q.toString());
  }
}
