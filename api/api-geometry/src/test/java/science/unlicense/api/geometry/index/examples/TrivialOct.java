package science.unlicense.api.geometry.index.examples;

import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.geometry.index.octtrees.OctTreeBasic;
import science.unlicense.api.geometry.index.octtrees.OctTreeMemberType;
import science.unlicense.api.geometry.index.octtrees.OctTreeRaycastResult;
import science.unlicense.api.geometry.index.octtrees.OctTreeType;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class TrivialOct {

    /**
     * An extremely simple class that implements {@link OctTreeMemberType}.
     */
    static class Something extends OctTreeMemberType<Something> {

        /**
         * A "pool" of unique identifiers, shared between all objects of type <code>Something</code>.
         */
        private static final AtomicLong pool = new AtomicLong(0);
        /**
         * The unique identifier of this object.
         */
        private final long id;

        Something(final Vector in_lower,final Vector in_upper) {
            this.id = Something.pool.incrementAndGet();
            this.lower.set(in_lower);
            this.upper.set(in_upper);
        }

        @Override
        public int compareTo(final Something other) {
            if (this.id > other.id) {
                return 1;
            }
            if (this.id < other.id) {
                return -1;
            }
            return 0;
        }

    }

    @Test
    public void example() {
        /**
         * Create a octtree of width, height, and depth 128, using the simplest implementation the package provides.
         */
        final OctTreeType<Something> tree = OctTreeBasic.newOctTree(new Vector(128, 128, 128), new Vector(0, 0, 0));

        /**
         * Insert eight objects into the tree. The sizes and positions of the object will place one in each corner of the volume described by the tree.
         */
        Something s0;
        Something s1;
        Something s2;
        Something s3;
        Something s4;
        Something s5;
        Something s6;
        Something s7;

        {
            final Vector lower = new Vector(0, 0, 0);
            final Vector upper = new Vector(31, 31, 31);
            s0 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(64, 0, 0);
            final Vector upper = new Vector(64 + 31, 31, 31);
            s1 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(0, 64, 0);
            final Vector upper = new Vector(31, 64 + 31, 31);
            s2 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(64, 64, 0);
            final Vector upper = new Vector(64 + 31, 64 + 31, 31);
            s3 = new Something(lower, upper);
        }

        /**
         * Upper Z...
         */
        {
            final Vector lower = new Vector(0, 0, 64);
            final Vector upper = new Vector(31, 31, 64 + 31);
            s4 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(64, 0, 64);
            final Vector upper = new Vector(64 + 31, 31, 64 + 31);
            s5 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(0, 64, 64);
            final Vector upper = new Vector(31, 64 + 31, 64 + 31);
            s6 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(64, 64, 64);
            final Vector upper = new Vector(64 + 31, 64 + 31, 64 + 31);
            s7 = new Something(lower, upper);
        }

        boolean inserted = true;
        inserted &= tree.octTreeInsert(s0);
        inserted &= tree.octTreeInsert(s1);
        inserted &= tree.octTreeInsert(s2);
        inserted &= tree.octTreeInsert(s3);
        inserted &= tree.octTreeInsert(s4);
        inserted &= tree.octTreeInsert(s5);
        inserted &= tree.octTreeInsert(s6);
        inserted &= tree.octTreeInsert(s7);

        Assert.assertTrue(inserted);

        /**
         * Now, select a volume of the tree and check that the expected objects were contained within the volume.
         */
        {
            final BBox volume = new BBox(3);
            volume.getLower().setXYZ(0, 0, 0);
            volume.getUpper().setXYZ(40, 128, 40);

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.octTreeQueryVolumeContaining(volume, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertTrue(objects.contains(s0));
            Assert.assertFalse(objects.contains(s1));
            Assert.assertTrue(objects.contains(s2));
            Assert.assertFalse(objects.contains(s3));
        }

        /**
         * Now, select another volume of the tree and check that the expected objects were overlapped by the volume.
         */
        {
            final BBox volume = new BBox(3);
            volume.getLower().setXYZ(0, 0, 80);
            volume.getUpper().setXYZ(80, 80, 128);

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.octTreeQueryVolumeOverlapping(volume, objects);

            Assert.assertEquals(4, objects.size());
            Assert.assertTrue(objects.contains(s4));
            Assert.assertTrue(objects.contains(s5));
            Assert.assertTrue(objects.contains(s6));
            Assert.assertTrue(objects.contains(s7));
        }

        /**
         * Now, cast a ray from (16,16,16) towards (128,128,16), and check that the expected objects were intersected by the ray.
         *
         * Note that objects are returned in order of increasing distance: The nearest object intersected by the ray will be the first in the returned set.
         */
        {

            final Vector origin = new Vector(16, 16, 16);
            final Vector direction = new Vector(128, 128, 16).normalize();
            final Ray ray = new Ray(origin, direction);

            final TreeSet<OctTreeRaycastResult<Something>> objects = new TreeSet<OctTreeRaycastResult<Something>>();
            tree.octTreeQueryRaycast(ray, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertSame(objects.first().getObject(), s0);
            Assert.assertSame(objects.last().getObject(), s3);
        }
    }
}
