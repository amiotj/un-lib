package science.unlicense.api.geometry.index.examples;

import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeBasic;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeMemberType;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeRaycastResult;
import science.unlicense.api.geometry.index.quadtrees.QuadTreeType;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public final class TrivialQuad {

    /**
     * An extremely simple class that implements {@link QuadTreeMemberType}.
     */
    static class Something extends QuadTreeMemberType<Something> {

        /**
         * A "pool" of unique identifiers, shared between all objects of type <code>Something</code>.
         */

        private static final AtomicLong pool = new AtomicLong(0);
        /**
         * The unique identifier of this object.
         */

        private final long id;

        Something(final Vector in_lower,final Vector in_upper) {
            this.id = Something.pool.incrementAndGet();
            this.lower.set(in_lower);
            this.upper.set(in_upper);
        }

        @Override
        public int compareTo(
                final Something other) {
            if (this.id > other.id) {
                return 1;
            }
            if (this.id < other.id) {
                return -1;
            }
            return 0;
        }

    }

    @Test
    public void example() {
        /**
         * Create a quadtree of width and height 128, using the simplest implementation the package provides.
         */
        final QuadTreeType<Something> tree
                = QuadTreeBasic.newQuadTree(new Vector(128, 128), new Vector(0, 0));

        /**
         * Insert four objects into the tree. The sizes and positions of the object will place one in each corner of the area described by the tree.
         */
        Something s0;
        Something s1;
        Something s2;
        Something s3;

        {
            final Vector lower = new Vector(0, 0);
            final Vector upper = new Vector(31, 31);
            s0 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(64, 0);
            final Vector upper = new Vector(64 + 31, 31);
            s1 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(0, 64);
            final Vector upper = new Vector(0 + 31, 64 + 31);
            s2 = new Something(lower, upper);
        }

        {
            final Vector lower = new Vector(64, 64);
            final Vector upper = new Vector(64 + 31, 64 + 31);
            s3 = new Something(lower, upper);
        }

        boolean inserted = true;
        inserted &= tree.quadTreeInsert(s0);
        inserted &= tree.quadTreeInsert(s1);
        inserted &= tree.quadTreeInsert(s2);
        inserted &= tree.quadTreeInsert(s3);

        Assert.assertTrue(inserted);

        /**
         * Now, select an area of the tree and check that the expected objects were contained within the area.
         */
        {
            final BBox area = new BBox(2);
            area.getLower().setXY(0, 0);
            area.getUpper().setXY(40, 128);

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.quadTreeQueryAreaContaining(area, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertTrue(objects.contains(s0));
            Assert.assertFalse(objects.contains(s1));
            Assert.assertTrue(objects.contains(s2));
            Assert.assertFalse(objects.contains(s3));
        }

        /**
         * Now, select another area of the tree and check that the expected objects were overlapped by the area.
         */
        {
            final BBox area = new BBox(2);
            area.getLower().setXY(0, 0);
            area.getUpper().setXY(80, 80);

            final TreeSet<Something> objects = new TreeSet<Something>();
            tree.quadTreeQueryAreaOverlapping(area, objects);

            Assert.assertEquals(4, objects.size());
            Assert.assertTrue(objects.contains(s0));
            Assert.assertTrue(objects.contains(s1));
            Assert.assertTrue(objects.contains(s2));
            Assert.assertTrue(objects.contains(s3));
        }

        /**
         * Now, cast a ray from (16,16) towards (128,128), and check that the expected objects were intersected by the ray.
         *
         * Note that objects are returned in order of increasing distance: The nearest object intersected by the ray will be the first in the returned set.
         */
        {
            final Vector origin = new Vector(16, 16);
            final Vector direction = new Vector(128, 128).normalize();
            final Ray ray = new Ray(origin, direction);

            final TreeSet<QuadTreeRaycastResult<Something>> objects
                    = new TreeSet<QuadTreeRaycastResult<Something>>();
            tree.quadTreeQueryRaycast(ray, objects);

            Assert.assertEquals(2, objects.size());
            Assert.assertSame(objects.first().getObject(), s0);
            Assert.assertSame(objects.last().getObject(), s3);
        }
    }
}
