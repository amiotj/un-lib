package science.unlicense.api.geometry.index;

import org.junit.Assert;
import org.junit.Test;

import org.junit.Ignore;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.Vector;

/**
 * @author Mark Raynsford
 */
public class RayI3DTest {

    public static final double DELTA = 0.0000001;
    
    @Test
    public void testRayEqualsNotCase0() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertFalse(ray0.equals(null));
    }

    @Test
    public void testRayEqualsNotCase1() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertFalse(ray0.equals(Integer.valueOf(23)));
    }

    @Test
    public void testRayEqualsNotCase2() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(1, 2, 3), new Vector(0, 0, 0));
        Assert.assertFalse(ray0.equals(ray1));
    }

    @Test
    public void testRayEqualsNotCase3() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(0, 0, 0), new Vector(1, 2, 3));
        Assert.assertFalse(ray0.equals(ray1));
    }

    @Test
    public void testRayEqualsReflexive() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertEquals(ray0, ray0);
    }

    @Test
    public void testRayEqualsSymmetric() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertEquals(ray0, ray1);
        Assert.assertEquals(ray1, ray0);
    }

    @Test
    public void testRayEqualsTransitive() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray2 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertEquals(ray0, ray1);
        Assert.assertEquals(ray1, ray2);
        Assert.assertEquals(ray0, ray2);
    }

    //TODO remove this test ? Un Vectors are modifiable
    @Ignore
    @Test
    public void skipRayHashCodeEquals() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertTrue(ray0.hashCode() == ray1.hashCode());
    }

    @Test
    public void testRayToStringEquals() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        Assert.assertTrue(ray0.toString().equals(ray1.toString()));
    }

    @Test
    public void testRayToStringNotEquals() {
        final Ray ray0 = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));
        final Ray ray1 = new Ray(new Vector(0, 0, 0), new Vector(1, 2, 3));
        Assert.assertFalse(ray0.toString().equals(ray1.toString()));
    }

    @Test
    public void testRayZero() {
        final Ray ray = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 0));

        final Tuple ray_origin = ray.getPosition();
        final Vector ray_direction = ray.getDirection();
//    final Vector ray_direction_inv = ray.getDirectionInverse();

        Assert.assertEquals(ray_origin.getX(),ray_origin.getY(),DELTA);
        Assert.assertEquals(ray_direction.getX(),ray_direction.getY(),DELTA);
        Assert.assertEquals(ray_origin.getX(),ray_origin.getZ(),DELTA);
        Assert.assertEquals(ray_direction.getX(),ray_direction.getZ(),DELTA);
//    Assert.assertTrue(ray_direction_inv.getX() == Double.POSITIVE_INFINITY);
//    Assert.assertTrue(ray_direction_inv.getY() == Double.POSITIVE_INFINITY);
//    Assert.assertTrue(ray_direction_inv.getZ() == Double.POSITIVE_INFINITY);
    }
}
