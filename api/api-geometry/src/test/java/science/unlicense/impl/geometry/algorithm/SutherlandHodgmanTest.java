

package science.unlicense.impl.geometry.algorithm;

import science.unlicense.impl.geometry.algorithm.SutherlandHodgman;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class SutherlandHodgmanTest {
    
    
    @Test
    public void testRectangleClip(){
        
        final Sequence subject = new ArraySequence();
        subject.add(new Vector(0,0));
        subject.add(new Vector(10,0));
        subject.add(new Vector(10,20));
        subject.add(new Vector(0,20));
        
        final Sequence clip = new ArraySequence();
        clip.add(new Segment( 5, 8, 5, 24));
        clip.add(new Segment( 5,24,16, 24));
        clip.add(new Segment(16,24,16,  8));
        clip.add(new Segment(15, 8, 5,  8));
        
        final Sequence result = SutherlandHodgman.clip(subject, clip);
        Assert.assertEquals(4,result.getSize());
        Assert.assertEquals(new DefaultTuple( 5,  8),result.get(0));
        Assert.assertEquals(new DefaultTuple(10,  8),result.get(1));
        Assert.assertEquals(new DefaultTuple(10, 20),result.get(2));
        Assert.assertEquals(new DefaultTuple( 5, 20),result.get(3));
        
    }
    
}
