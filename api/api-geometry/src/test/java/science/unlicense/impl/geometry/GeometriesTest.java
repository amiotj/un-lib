
package science.unlicense.impl.geometry;

import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.Geometries;
import org.junit.Test;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.math.Vector;

import org.junit.Assert;

/**
 * @author Johann Sorel
 * @author Izyumov Konstantin
 */
public class GeometriesTest {
    private static final double DELTA = 0.000000001;
    private static final double[] lineStart = new double[]{2, 4};
    private static final double[] lineEnd = new double[]{6, 1};
    private static final double[] pointOnLine = new double[]{4, 2.5};
    private static final double[] pointLeft = new double[]{3, 5};
    private static final double[] pointRight = new double[]{5, 0.1};

    @Test
    public void lineSideTest() {

        double side;

        side = Geometries.lineSide(lineStart, lineEnd, pointOnLine);
        Assert.assertTrue(side == 0);

        side = Geometries.lineSide(lineStart, lineEnd, pointLeft);
        Assert.assertTrue(side > 0);

        side = Geometries.lineSide(lineStart, lineEnd, pointRight);
        Assert.assertTrue(side < 0);

    }

    @Test
    public void isOnLineTest() {
        Assert.assertTrue(Geometries.isOnLine(new Point(lineStart), new Point(lineEnd), new Point(pointOnLine)));
        Assert.assertFalse(Geometries.isOnLine(new Point(lineStart), new Point(lineEnd), new Point(pointLeft)));
        Assert.assertFalse(Geometries.isOnLine(new Point(lineStart), new Point(lineEnd), new Point(pointRight)));
    }

    @Test
    public void isCounterClockwiseTest() {
        //on line is false
        Assert.assertFalse(Geometries.isCounterClockwise(new Point(lineStart), new Point(lineEnd), new Point(pointOnLine)));
        Assert.assertTrue(Geometries.isCounterClockwise(new Point(lineStart), new Point(lineEnd), new Point(pointLeft)));
        Assert.assertFalse(Geometries.isCounterClockwise(new Point(lineStart), new Point(lineEnd), new Point(pointRight)));
        //on line is false
        Assert.assertFalse(Geometries.isCounterClockwise(lineStart, lineEnd, pointOnLine));
        Assert.assertTrue(Geometries.isCounterClockwise(lineStart, lineEnd, pointLeft));
        Assert.assertFalse(Geometries.isCounterClockwise(lineStart, lineEnd, pointRight));
    }

    @Test
    public void isAtRightOfTest() {
        //on line is false
        Assert.assertFalse(Geometries.isAtRightOf(new Point(lineStart), new Point(lineEnd), new Point(pointOnLine)));
        Assert.assertFalse(Geometries.isAtRightOf(new Point(lineStart), new Point(lineEnd), new Point(pointLeft)));
        Assert.assertTrue(Geometries.isAtRightOf(new Point(lineStart), new Point(lineEnd), new Point(pointRight)));
        //on line is false
        Assert.assertFalse(Geometries.isAtRightOf(lineStart, lineEnd, pointOnLine));
        Assert.assertFalse(Geometries.isAtRightOf(lineStart, lineEnd, pointLeft));
        Assert.assertTrue(Geometries.isAtRightOf(lineStart, lineEnd, pointRight));
    }

    @Test
    public void testProjectPointOnPlan() {
        final Plane plane = new Plane(new Vector(0, 1, 0), new Vector(0, 4, 0));
        final Vector center = new Vector(0, 5, 0);
        double[] proj = Geometries.projectPointOnPlan(center.getValues(), plane.getNormal().getValues(), plane.getD());

        Assert.assertEquals(0, proj[0], DELTA);
        Assert.assertEquals(4, proj[1], DELTA);
        Assert.assertEquals(0, proj[2], DELTA);
    }

    @Test
    public void testInCircle1() {
        final Point a = new Point(-1, 0);
        final Point b = new Point(0, 1);
        final Point c = new Point(1, 0);
        final Point d = new Point(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle2() {
        final Point a = new Point(1, 0);
        final Point b = new Point(0, 1);
        final Point c = new Point(-1, 0);
        final Point d = new Point(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle3() {
        final Point a = new Point(1, 0);
        final Point b = new Point(0, -1);
        final Point c = new Point(0, 1);
        final Point d = new Point(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle4() {
        final Point a = new Point(1, 0);
        final Point b = new Point(0, 1);
        final Point c = new Point(0, -1);
        final Point d = new Point(0, 0);

        Assert.assertTrue(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle5() {
        final Point a = new Point(-1, 0);
        final Point b = new Point(0, 1);
        final Point c = new Point(1, 0);
        final Point d = new Point(10, 10);

        Assert.assertFalse(Geometries.inCircle(a, b, c, d));
    }

    @Test
    public void testInCircle6() {
        final Point a = new Point(-1, 0);
        final Point b = new Point(0, 1);
        final Point c = new Point(1, 0);
        final Point d = new Point(0, 1+DELTA);

        Assert.assertFalse(Geometries.inCircle(a, b, c, d));
    }

}
