
package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.Intersects;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.impl.geometry.operation.IntersectsExecutors;
import org.junit.Test;
import org.junit.Assert;
import org.junit.Ignore;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class IntersectsTest {

    @Test
    public void point_point() throws OperationException{
        Point geom1 = new Point(10, 20);
        Point geom2 = new Point(10, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Point(11, 20);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void point_line() throws OperationException{
        Point geom1 = new Point(10, 20);
        Segment geom2 = new Segment(10, 20, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(11, 20, 12, 20);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(5, 20, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(5, 22, 15, 22);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void point_rectangle() throws OperationException{
        Point geom1 = new Point(new DefaultTuple(10, 20));
        Rectangle geom2 = new Rectangle(8, 18, 4, 4);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Rectangle(16, 18, 4, 4);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void point_circle() throws OperationException{
        Point geom1 = new Point(10, 20);
        Circle geom2 = new Circle(10, 2, 19);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Circle(3, -1, 5);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void point_plane() throws OperationException{
        Point geom1 = new Point(0, 10, 0);
        Plane geom2 = new Plane(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Point(35, 8, -12);
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Point(35, -6, -12);
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Point(35, 0, -12);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void line_line() throws OperationException{
        Segment geom1 = new Segment(10, 20, 15, 20);
        Segment geom2 = new Segment(10, 20, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(-5, -2, 15, 20);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(10, 21, 15, 21);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(16, 20, 17, 20);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Segment(19, 5, 19, 50);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void rectangle_rectangle() throws OperationException{
        Rectangle geom1 = new Rectangle(8, 18, 4, 4);
        Rectangle geom2 = new Rectangle(8, 18, 4, 4);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Rectangle(2, 20, 7, 1);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Rectangle(1, 4, 1, 3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void bbox_bbox() throws OperationException{
        BBox geom1 = new BBox(new double[]{8, 18}, new double[]{12, 22});
        BBox geom2 = new BBox(new double[]{8, 18}, new double[]{12, 22});
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new BBox(new double[]{2, 20}, new double[]{9, 21});
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new BBox(new double[]{1, 4}, new double[]{2, 7});
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void sphere_sphere() throws OperationException{
        Sphere geom1 = new Sphere(1); geom1.getTransform().getTranslation().setXYZ(2, -3, 5);
        Sphere geom2 = new Sphere(2.2); geom2.getTransform().getTranslation().setXYZ(2, -1, 5);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Sphere(1.5); geom2.getTransform().getTranslation().setXYZ(5, -3, 5);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void sphere_capsule() throws OperationException{
        final Capsule geom1 = new Capsule(new DefaultTuple(1, 1, 5), new DefaultTuple(1, 1, 10), 1);

        Sphere geom2 = new Sphere(new DefaultTuple(1, 1, 5), 2);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Sphere(new DefaultTuple(1, 2, 8), 1.5);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Sphere(new DefaultTuple(5, -3, 5), 2);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Sphere(new DefaultTuple(1, 1, 12.9), 2);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Sphere(new DefaultTuple(1, 1, 13.1), 2);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void sphere_plane() throws OperationException{
        Sphere geom1 = new Sphere(new Vector(0, 2, 0),1);
        Plane geom2 = new Plane(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Sphere(new Vector(35, 8, -12),1);
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Sphere(new Vector(35, -6, -12),1);
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Sphere(new Vector(35, 0.7, -12),1);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Sphere(new Vector(35, -0.3, -12),1);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Sphere(new Vector(35, -5.0000001, -12),5);
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        geom1 = new Sphere(new Vector(35, -4.999999, -12),5);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void sphere_bbox() throws OperationException{
        BBox geom2 = new BBox(new Vector(6,6,6), new Vector(10,10,10));
        
        //inside bbox
        Sphere geom1 = new Sphere(new Vector(8, 8, 8),1);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
        //englobe bbox
        geom1 = new Sphere(new Vector(8, 8, 8),20);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
        //above no intersect
        geom1 = new Sphere(new Vector(8, 12.5, 8),2);
        Assert.assertEquals(false, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(false, Operations.execute(new Intersects(geom2, geom1)));
        //above intersect
        geom1 = new Sphere(new Vector(8, 11.5, 8),2);
        Assert.assertEquals(true, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(true, Operations.execute(new Intersects(geom2, geom1)));
    }
    
    @Test
    public void circle_circle() throws OperationException{
        Circle geom1 = new Circle(10, 5, 2);
        Circle geom2 = new Circle(10, 2, 2);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Circle(10, 2, 0.99);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
        geom2 = new Circle(3, -1, 2);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    @Test
    public void capsule_capsule() throws OperationException{
        Capsule geom1 = new Capsule(new DefaultTuple(10, 20, 5), new DefaultTuple(15, 20, 5), 1);

        Capsule geom2 = new Capsule(new DefaultTuple(10, 20, 5), new DefaultTuple(15, 20, 5), 1);
        Assert.assertEquals(Boolean.TRUE,  Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));

        geom2 = new Capsule(new DefaultTuple(-5, -2, 5), new DefaultTuple(15, 20, 5), 1);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Intersects(geom1, geom2)));

        geom1 = new Capsule(new DefaultTuple(10, 20, 5), new DefaultTuple(15, 20, 5), 0.2);

        geom2 = new Capsule(new DefaultTuple(10, 21, 5), new DefaultTuple(15, 21, 5), 0.3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Capsule(new DefaultTuple(16, 20, 5), new DefaultTuple(17, 20, 5), 0.3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));

        geom2 = new Capsule(new DefaultTuple(19, 5, 5), new DefaultTuple(19, 50, 5), 0.3);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom1, geom2)));
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Intersects(geom2, geom1)));
    }

    /**
     * Test againt a [0,0,0,1,1,1] bbox.
     * 
     * @throws OperationException 
     */
    @Test
    public void triangle_bbox() throws OperationException{
        
        final BBox bbox = new BBox(3);
        bbox.setRange(0, 0, 1);
        bbox.setRange(1, 0, 1);
        bbox.setRange(2, 0, 1);
        
        final Vector v0 = new Vector(3);
        final Vector v1 = new Vector(3);
        final Vector v2 = new Vector(3);
        final Triangle triangle = new Triangle(v0, v1, v2);
        
        final Intersects intersects = new Intersects(triangle, bbox);
                
        //triangle cross bbox on x plan
        v0.setXYZ(0.5, +10, -10);
        v1.setXYZ(0.5, -10, -10);
        v2.setXYZ(0.5, -10, +10);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle cross bbox on y plan
        v0.setXYZ(+10, 0.5, -10);
        v1.setXYZ(-10, 0.5, -10);
        v2.setXYZ(-10, 0.5, +10);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle cross bbox on z plan
        v0.setXYZ(+10, -10, 0.5);
        v1.setXYZ(-10, -10, 0.5);
        v2.setXYZ(-10, +10, 0.5);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle within bbox
        v0.setXYZ(0.2,0.2,0.2);
        v1.setXYZ(0.2,0.4,0.2);
        v2.setXYZ(0.2,0.2,0.4);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle one corner in bbox
        v0.setXYZ(0.5,0.5,0.5);
        v1.setXYZ(+10,+10,+10);
        v2.setXYZ(-10,-10,-10);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
    }
    
    /**
     * Test against a [10,20,30,11,22,33] bbox.
     * 
     * @throws OperationException 
     */
    @Test
    public void triangle_bbox2() throws OperationException{
        
        final BBox bbox = new BBox(3);
        bbox.setRange(0, 10, 11);
        bbox.setRange(1, 20, 22);
        bbox.setRange(2, 30, 33);
        
        final Vector v0 = new Vector(3);
        final Vector v1 = new Vector(3);
        final Vector v2 = new Vector(3);
        final Triangle triangle = new Triangle(v0, v1, v2);
        
        final Intersects intersects = new Intersects(triangle, bbox);
                
        //triangle cross bbox on x plan
        v0.setXYZ(10.5, +100, 0);
        v1.setXYZ(10.5, 0, 0);
        v2.setXYZ(10.5, 0, +100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle cross bbox on y plan
        v0.setXYZ(+100, 21, 0);
        v1.setXYZ(0, 21, 0);
        v2.setXYZ(0, 21, +100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle cross bbox on z plan
        v0.setXYZ(+100, 0, 31.5);
        v1.setXYZ(0, 0, 31.5);
        v2.setXYZ(0, +100, 31.5);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle within bbox
        v0.setXYZ(10.2,21.2,32.2);
        v1.setXYZ(10.2,21.4,32.2);
        v2.setXYZ(10.2,21.2,32.4);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle one corner in bbox 1
        v0.setXYZ(10.5,21.5,32.5);
        v1.setXYZ(10.5,+100,+100);
        v2.setXYZ(10.5,-100,-100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
        
        //triangle one corner in bbox 2
        v0.setXYZ(10.5,21.5,32.5);
        v1.setXYZ(+100,+100,+100);
        v2.setXYZ(-100,-100,-100);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
                
        //from a real case ----------
        //one corner outside bbox
        bbox.setRange(0, 25, 26);
        bbox.setRange(1, 14, 15);
        bbox.setRange(2, 48, 49);
        v0.setXYZ(24.793482276873835,14.662215379777855,48.69177254509293);
        v1.setXYZ(25.419224087367553,14.907598565732481,48.710409708649166);
        v2.setXYZ(25.404734553018685,14.97108488632958,48.052672859715464);
        Assert.assertEquals(Boolean.TRUE, IntersectsExecutors.TRIANGLE_BBOX.execute(intersects));
    }
    
}