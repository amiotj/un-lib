
package science.unlicense.impl.geometry.triangulate;

import science.unlicense.impl.geometry.triangulate.EarClipping;
import org.junit.Test;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.math.Vector;
import org.junit.Assert;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.geometry.Geometries;

/**
 *
 * @author Johann Sorel
 */
public class EarClippingTest {

    /**
     * Test a triangle geometry.
     */
    @Test
    public void triangleTest() throws OperationException{

        final Vector v1 = new Vector(1,2);
        final Vector v2 = new Vector(10,20);
        final Vector v3 = new Vector(5,5);

        //test correct rotation : clockwise
        Triangle geometry = new Triangle(v1, v2, v3);
        EarClipping ec = new EarClipping();
        Sequence triangles = ec.triangulate(geometry);
        Assert.assertEquals(1, triangles.getSize());
        Tuple[] t = (Tuple[])triangles.get(0);
        Assert.assertArrayEquals(new Tuple[]{v3,v1,v2}, t);

        //test reverse rotation : counter-clockwise
        geometry = new Triangle(v1, v3, v2);
        ec = new EarClipping();
        triangles = ec.triangulate(geometry);
        Assert.assertEquals(1, triangles.getSize());
        t = (Tuple[])triangles.get(0);
        Assert.assertArrayEquals(new Tuple[]{v3,v1,v2}, t);

    }

    /**
     * Test a square geometry.
     */
    @Test
    public void quadTest() throws OperationException{

        final Vector v1 = new Vector(0,0);
        final Vector v2 = new Vector(0,10);
        final Vector v3 = new Vector(10,10);
        final Vector v4 = new Vector(10,0);

        //test correct rotation : clockwise
        Polygon geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v2,v3,v4,v1})), null);
        EarClipping ec = new EarClipping();
        Sequence triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        Tuple[] t1 = (Tuple[])triangles.get(0);
        Tuple[] t2 = (Tuple[])triangles.get(1);
        Assert.assertArrayEquals(new Tuple[]{v4,v1,v2}, t1);
        Assert.assertArrayEquals(new Tuple[]{v4,v2,v3}, t2);

        //test reverse rotation : counter-clockwise
        geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v4,v3,v2,v1})), null);
        ec = new EarClipping();
        triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        t1 = (Tuple[])triangles.get(0);
        t2 = (Tuple[])triangles.get(1);
        Assert.assertArrayEquals(new Tuple[]{v4,v1,v2}, t1);
        Assert.assertArrayEquals(new Tuple[]{v4,v2,v3}, t2);

    }

    /**
     * Test a V shape geometry.
     */
    @Test
    public void vTest() throws OperationException{

        final Vector v1 = new Vector(5,0);
        final Vector v2 = new Vector(0,10);
        final Vector v3 = new Vector(5,5);
        final Vector v4 = new Vector(10,10);

        //test correct rotation : clockwise
        Polygon geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v2,v3,v4,v1})), null);
        EarClipping ec = new EarClipping();
        Sequence triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        Tuple[] t1 = (Tuple[])triangles.get(0);
        Tuple[] t2 = (Tuple[])triangles.get(1);
        Assert.assertArrayEquals(new Tuple[]{v1,v2,v3}, t1);
        Assert.assertArrayEquals(new Tuple[]{v1,v3,v4}, t2);

        //test reverse rotation : counter-clockwise
        geometry = new Polygon(new Polyline(Geometries.toTupleBuffer(new Tuple[]{v1,v4,v3,v2,v1})), null);
        ec = new EarClipping();
        triangles = ec.triangulate(geometry);
        Assert.assertEquals(2, triangles.getSize());
        t1 = (Tuple[])triangles.get(0);
        t2 = (Tuple[])triangles.get(1);
        Assert.assertArrayEquals(new Tuple[]{v1,v2,v3}, t1);
        Assert.assertArrayEquals(new Tuple[]{v1,v3,v4}, t2);

    }

}
