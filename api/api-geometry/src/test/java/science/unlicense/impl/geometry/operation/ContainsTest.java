
package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.Contains;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.api.geometry.operation.OperationException;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class ContainsTest {

    @Test
    public void rectangle_point() throws OperationException{
        final Rectangle geom1 = new Rectangle(5, 10, 4, 4);
        Point geom2 = new Point(6, 13);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(10, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(6, 8);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
    }

    @Test
    public void bbox_point() throws OperationException{
        final BBox geom1 = new BBox(new double[]{5,10}, new double[]{9,14});
        Point geom2 = new Point(6, 13);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(10, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(6, 8);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
    }

    @Test
    public void triangle_point() throws OperationException{
        Triangle geom1 = new Triangle(new DefaultTuple(5, 10), new DefaultTuple(10, 10), new DefaultTuple(10,15));
        Point geom2 = new Point(9, 12);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(6, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(2, 4);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
    }

    @Test
    public void circle_point() throws OperationException{
        Circle geom1 = new Circle(8, 11, 2);
        Point geom2 = new Point(9, 12);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(6, 13);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(2, 4);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
    }

    @Test
    public void curvepolygon_point() throws OperationException{

        final Polyline exterior = new Polyline(
            new DefaultTupleBuffer1D(new double[]{
                0,0,
                5,0,
                5,3,
                0,3,
                0,0
            }, 2));
        final Polygon geom1 = new Polygon(exterior, new ArraySequence());

        Point geom2 = new Point(1.3, 2.8);
        Assert.assertEquals(Boolean.TRUE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(-2, 2.8);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
        geom2 = new Point(1.3, 3.1);
        Assert.assertEquals(Boolean.FALSE, Operations.execute(new Contains(geom1, geom2)));
    }
}
