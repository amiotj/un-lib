

package science.unlicense.impl.geometry.operation;

import science.unlicense.api.geometry.operation.OperationException;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class IntersectionTest {
    
    private static final double DELTA = 0.00000001;
    
    @Test
    public void ray_plane() throws OperationException{
        Ray ray;
        Plane plane;
        
        //simple test
        ray   = new Ray(new Vector(-10, 0, 0), new Vector(1, 0, 0));
        plane = new Plane(new Vector(-1,0,0), new Vector(0,0,0));
        Assert.assertEquals(new Point(0, 0, 0), ray.intersection(plane));
        
        //offset
        ray   = new Ray(new Vector(-10, 0, 0), new Vector(1, 0, 0));
        plane = new Plane(new Vector(-1,0,0), new Vector(15,-8,7));
        Assert.assertEquals(new Point(15, 0, 0), ray.intersection(plane));
        
        //ray pointing opposite direction 
        ray   = new Ray(new Vector(-10, 0, 0), new Vector(-1, 0, 0));
        plane = new Plane(new Vector(-1,0,0), new Vector(0,0,0));
        Assert.assertEquals(null, ray.intersection(plane));
        
        //ray offset and parallal to plane
        ray   = new Ray(new Vector(-10, 0, 0), new Vector(0, 1, 0));
        plane = new Plane(new Vector(-1,0,0), new Vector(0,0,0));
        Assert.assertEquals(null, ray.intersection(plane));
        
        //ray parallal and on the plane 
        ray   = new Ray(new Vector(0, 0, 0), new Vector(0, 1, 0));
        plane = new Plane(new Vector(-1,0,0), new Vector(0,0,0));
        Assert.assertEquals(new Ray(new Vector(0, 0, 0), new Vector(0, 1, 0)), ray.intersection(plane));
        
    }
    @Test
    public void rectangle_rectangle() throws OperationException{
        final Rectangle r1 = new Rectangle(2, 2, 4, 4);
        final Rectangle r2 = new Rectangle(3, 4, 4, 4);
        
        final Rectangle r3 = (Rectangle) r1.intersection(r2);
        Assert.assertEquals(3, r3.getX(),DELTA);
        Assert.assertEquals(4, r3.getY(),DELTA);
        Assert.assertEquals(3, r3.getWidth(),DELTA);
        Assert.assertEquals(2, r3.getHeight(),DELTA);
        
    }
    
}
