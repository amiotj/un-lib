
package science.unlicense.impl.geometry.s2d;

import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Circle;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;

/**
 *
 * @author bertrand
 */
public class LineTest {
    
    private static final int startIndex = 0;
    private static final int endIndex = 1;
    private static final int areaLengthIndex = 2;
    private static final int bboxMinIndex = 3;
    private static final int bboxMaxIndex = 4;
    private static final int boundingCircleIndex = 5;
    private static final int centroidIndex = 6;
    
    private static final double[][][] lineParametersTest = new double[][][] {
        // { { startX, startY }, { endX, endY }, { area, length }, { bboxXMin, bboxYMin }, { boxXMax, bboxYMax }, { boundingCircleCx, boundingCircleCy, radius }, { centroidX, centroidY } },
        { {  1, 2 }, { -3, -4 }, { 0, Math.sqrt(52.) }, { -3, -4 }, { 1, 2 }, { -1,  -1, Math.sqrt(52.)/2. }, { -1,  -1 } },
        { { -3, 5 }, {  2, -1 }, { 0, Math.sqrt(61.) }, { -3, -1 }, { 2, 5 }, { -0.5, 2, Math.sqrt(61.)/2. }, { -0.5, 2 } },
    };
    
    private static final Segment[] lineTest;
    
    static {
        lineTest = new Segment[lineParametersTest.length];
        for( int i=0; i<lineParametersTest.length; i++ ) {
            lineTest[i] = new Segment( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1], lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1]);
        }
    }
    
    public LineTest() {
    }

    /**
     * Test of getStart method, of class Line.
     */
    @Test
    public void testGetStart() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            Tuple expResult = new DefaultTuple( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1] );
            Tuple result = instance.getStart();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of setStart method, of class Line.
     */
    @Test
    public void testSetStart() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            instance.setStart( new DefaultTuple( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1] ) );
            Tuple expResult = new DefaultTuple( lineParametersTest[i][startIndex][0], lineParametersTest[i][startIndex][1] );
            Tuple result = instance.getStart();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getStartX method, of class Line.
     */
    @Test
    public void testGetStartX() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            double expResult = lineParametersTest[i][startIndex][0];
            double result = instance.getStartX();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setStartX method, of class Line.
     */
    @Test
    public void testSetStartX() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            instance.setStartX(lineParametersTest[i][startIndex][0]);
            double expResult = lineParametersTest[i][startIndex][0];
            double result = instance.getStartX();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getStartY method, of class Line.
     */
    @Test
    public void testGetStartY() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            double expResult = lineParametersTest[i][startIndex][1];
            double result = instance.getStartY();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setStartY method, of class Line.
     */
    @Test
    public void testSetStartY() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            instance.setStartY(lineParametersTest[i][startIndex][1]);
            double expResult = lineParametersTest[i][startIndex][1];
            double result = instance.getStartY();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getEnd method, of class Line.
     */
    @Test
    public void testGetEnd() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            Tuple expResult = new DefaultTuple( lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1] );
            Tuple result = instance.getEnd();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of setEnd method, of class Line.
     */
    @Test
    public void testSetEnd() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            instance.setEnd( new DefaultTuple( lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1] ) );
            Tuple expResult = new DefaultTuple( lineParametersTest[i][endIndex][0], lineParametersTest[i][endIndex][1] );
            Tuple result = instance.getEnd();
            Assert.assertEquals(expResult, result);
        }
    }

    /**
     * Test of getEndX method, of class Line.
     */
    @Test
    public void testGetEndX() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            double expResult = lineParametersTest[i][endIndex][0];
            double result = instance.getEndX();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setEndX method, of class Line.
     */
    @Test
    public void testSetEndX() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            instance.setEndX(lineParametersTest[i][endIndex][0]);
            double expResult = lineParametersTest[i][endIndex][0];
            double result = instance.getEndX();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getEndY method, of class Line.
     */
    @Test
    public void testGetEndY() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            double expResult = lineParametersTest[i][endIndex][1];
            double result = instance.getEndY();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setEndY method, of class Line.
     */
    @Test
    public void testSetEndY() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            instance.setEndY(lineParametersTest[i][startIndex][1]);
            double expResult = lineParametersTest[i][startIndex][1];
            double result = instance.getEndY();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getArea method, of class Line.
     */
    @Test
    public void testGetArea() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = new Segment();
            double expResult = lineParametersTest[i][areaLengthIndex][0];
            double result = instance.getArea();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getLength method, of class Line.
     */
    @Test
    public void testGetLength() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            double expResult = lineParametersTest[i][areaLengthIndex][1];
            double result = instance.getLength();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getBoundingBox method, of class Line.
     */
    @Test
    public void testGetBoundingBox() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            BBox expResult = new BBox( lineParametersTest[i][bboxMinIndex], lineParametersTest[i][bboxMaxIndex]);
            BBox result = instance.getBoundingBox();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getBoundingCircle method, of class Line.
     */
    @Test
    public void testGetBoundingCircle() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            Circle expResult = new Circle( lineParametersTest[i][boundingCircleIndex][0], lineParametersTest[i][boundingCircleIndex][1], lineParametersTest[i][boundingCircleIndex][2]);
            Circle result = instance.getBoundingCircle();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getCentroid method, of class Line.
     */
    @Test
    public void testGetCentroid() {
        for( int i=0; i<lineParametersTest.length; i++ ) {
            Segment instance = lineTest[i];
            Point expResult = new Point( lineParametersTest[i][centroidIndex][0], lineParametersTest[i][boundingCircleIndex][1]);
            Point result = instance.getCentroid();
            Assert.assertTrue(expResult.equals(result));
        }
    }
    
}
