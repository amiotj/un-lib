
package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.Distance;
import org.junit.Test;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.impl.geometry.OBBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Sheet;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Vector;

import org.junit.Assert;

/**
 *
 * @author Johann Sorel
 */
public class DistanceTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void point_point() throws OperationException{
        Point geom1 = new Point(10, 20);
        Point geom2 = new Point(10, 20);
        Assert.assertEquals(0d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Point(11, 20);
        Assert.assertEquals(1d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void point_line() throws OperationException{
        Point geom1 = new Point(10, 20);
        Segment geom2 = new Segment(10, 20, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(11, 20, 12, 20);
        Assert.assertEquals(1d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(5, 20, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(5, 22, 15, 22);
        Assert.assertEquals(2d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(2d, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void point_circle() throws OperationException{
        Point geom1 = new Point(10, 20);
        Circle geom2 = new Circle(10, 2, 19);
        Assert.assertEquals(-1d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(-1d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Circle(10, -1, 5);
        Assert.assertEquals(16d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(16d, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void point_plane() throws OperationException{
        Point geom1 = new Point(0, 10, 0);
        Plane geom2 = new Plane(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Assert.assertEquals(10d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(10d, Operations.execute(new Distance(geom2, geom1)));
        geom1 = new Point(35, 8, -12);
        Assert.assertEquals(8d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(8d, Operations.execute(new Distance(geom2, geom1)));

        //point is in the plan
        geom1 = new Point(35, -6, -12);
        Assert.assertEquals(0.0, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0.0, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void point_sheet() throws OperationException{
        Point geom1 = new Point(0, 10, 0);
        Sheet geom2 = new Sheet(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Assert.assertEquals(10d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(10d, Operations.execute(new Distance(geom2, geom1)));
        geom1 = new Point(35, 8, -12);
        Assert.assertEquals(8d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(8d, Operations.execute(new Distance(geom2, geom1)));
        geom1 = new Point(35, -6, -12);
        Assert.assertEquals(6d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(6d, Operations.execute(new Distance(geom2, geom1)));
    }
    
    @Test
    public void point_rectangle() throws OperationException{
        Point geom1 = new Point(0, 0);
        Rectangle geom2 = new Rectangle(5,10,20,5);
        double result = (Double) Operations.execute(new Distance(geom1, geom2));
        double expected = Math.sqrt(5*5 + 10*10);
        Assert.assertEquals(expected, result, DELTA);

        //point inside
        geom1 = new Point(10,12);
        result = (Double) Operations.execute(new Distance(geom1, geom2));
        Assert.assertEquals(0.0, result, DELTA);
    }
    
    @Test
    public void point_bbox() throws OperationException{
        Point geom1 = new Point(0, 0);
        BBox geom2 = new BBox(new double[]{5,10},new double[]{25,15});
        double result = (Double) Operations.execute(new Distance(geom1, geom2));
        double expected = Math.sqrt(5*5 + 10*10);
        Assert.assertEquals(expected, result, DELTA);

        //point inside
        geom1 = new Point(10,12);
        result = (Double) Operations.execute(new Distance(geom1, geom2));
        Assert.assertEquals(0.0, result, DELTA);
    }
    
    @Test
    public void point_obbox() throws OperationException{
        Point geom1 = new Point(0, 0);
        OBBox geom2 = new OBBox(new double[]{0,0},new double[]{20,5});
        geom2.getTransform().getTranslation().setXY(5, 10);
        geom2.getTransform().notifyChanged();
        double result = (Double) Operations.execute(new Distance(geom1, geom2));
        double expected = Math.sqrt(5*5 + 10*10);
        Assert.assertEquals(expected, result, DELTA);

        //point inside
        geom1 = new Point(10,12);
        result = (Double) Operations.execute(new Distance(geom1, geom2));
        Assert.assertEquals(0.0, result, DELTA);
    }

    @Test
    public void line_line() throws OperationException{
        Segment geom1 = new Segment(10, 20, 15, 20);
        Segment geom2 = new Segment(10, 20, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(-5, -2, 15, 20);
        Assert.assertEquals(0d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(10, 21, 15, 21);
        Assert.assertEquals(1d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(16, 20, 17, 20);
        Assert.assertEquals(1d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(1d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Segment(19, 5, 19, 50);
        Assert.assertEquals(4d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(4d, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void circle_circle() throws OperationException{
        Circle geom1 = new Circle(10, 5, 2);
        Circle geom2 = new Circle(10, 2, 2);
        Assert.assertEquals(-1d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(-1d, Operations.execute(new Distance(geom2, geom1)));
        geom2 = new Circle(10, 2, 0.99);
        Assert.assertEquals(0.01d, (Double)Operations.execute(new Distance(geom1, geom2)),DELTA);
        Assert.assertEquals(0.01d, (Double)Operations.execute(new Distance(geom2, geom1)),DELTA);
        geom2 = new Circle(3, 5, 2);
        Assert.assertEquals(3d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(3d, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void sphere_sphere() throws OperationException{
        Sphere geom1 = new Sphere(1); geom1.getTransform().getTranslation().setXYZ(2, -3, 5);
        Sphere geom2 = new Sphere(2.2); geom2.getTransform().getTranslation().setXYZ(2, -1, 5);
        Assert.assertTrue((Double)Operations.execute(new Distance(geom1, geom2))<=0);
        Assert.assertTrue((Double)Operations.execute(new Distance(geom1, geom2))<=0);
        geom2 = new Sphere(1.5); geom2.getTransform().getTranslation().setXYZ(5, -3, 5);
        Assert.assertEquals(0.5, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(0.5, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void sphere_plane() throws OperationException{
        Sphere geom1 = new Sphere(3); geom1.getTransform().getTranslation().setXYZ(0, 10, 0);
        Plane geom2 = new Plane(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Assert.assertEquals(7d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(7d, Operations.execute(new Distance(geom2, geom1)));
        geom1 = new Sphere(3); geom1.getTransform().getTranslation().setXYZ(35, 8, -12);
        Assert.assertEquals(5d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(5d, Operations.execute(new Distance(geom2, geom1)));
        geom1 = new Sphere(3); geom1.getTransform().getTranslation().setXYZ(35, -6, -12);
        Assert.assertEquals(-9d, Operations.execute(new Distance(geom1, geom2)));
        Assert.assertEquals(-9d, Operations.execute(new Distance(geom2, geom1)));
    }

    @Test
    public void capsule_capsule() throws OperationException{
        Capsule geom1 = new Capsule(new DefaultTuple(10, 20, 5), new DefaultTuple(15, 20, 5), 1);
        Capsule geom2 = new Capsule(new DefaultTuple(10, 20, 5), new DefaultTuple(15, 20, 5), 1);
        Assert.assertTrue((Double)Operations.execute(new Distance(geom1, geom2))<=0);
        Assert.assertTrue((Double)Operations.execute(new Distance(geom1, geom2))<=0);

        geom2 = new Capsule(new DefaultTuple(-5, -2, 5), new DefaultTuple(15, 20, 5), 1);
        Assert.assertTrue((Double)Operations.execute(new Distance(geom1, geom2))<=0);
        Assert.assertTrue((Double)Operations.execute(new Distance(geom1, geom2))<=0);

        geom1 = new Capsule(new DefaultTuple(10, 20, 5), new DefaultTuple(15, 20, 5), 0.2);
        geom2 = new Capsule(new DefaultTuple(10, 21, 5), new DefaultTuple(15, 21, 5), 0.3);
        Assert.assertEquals(0.5d, (Double)Operations.execute(new Distance(geom1, geom2)), DELTA);
        Assert.assertEquals(0.5d, (Double)Operations.execute(new Distance(geom2, geom1)), DELTA);

        geom2 = new Capsule(new DefaultTuple(16, 20, 5), new DefaultTuple(17, 20, 5), 0.3);
        Assert.assertEquals(0.5d, (Double)Operations.execute(new Distance(geom1, geom2)), DELTA);
        Assert.assertEquals(0.5d, (Double)Operations.execute(new Distance(geom2, geom1)), DELTA);

        geom2 = new Capsule(new DefaultTuple(19, 5, 5), new DefaultTuple(19, 50, 5), 0.3);
        //19 - 15 - 0.2 - 0.3 = 3.5
        Assert.assertEquals(3.5d, (Double)Operations.execute(new Distance(geom1, geom2)), DELTA);
        Assert.assertEquals(3.5d, (Double)Operations.execute(new Distance(geom2, geom1)), DELTA);
    }

    @Test
    public void testDistanceLineAndPoint() {
        {
            final Point a = new Point(0, 0);
            final Point b = new Point(0, 1);
            final Point c = new Point(1, 0);
            Assert.assertTrue(Distance.distanceLineAndPoint(a, b, c) - 1 < 1e-8);
        }
        {
            final Point a = new Point(0, 0);
            final Point b = new Point(1, 0);
            final Point c = new Point(0, 1);
            Assert.assertTrue(Distance.distanceLineAndPoint(a, b, c) - 1 < 1e-8);
        }
        {
            final Point a = new Point(0, 0);
            final Point b = new Point(1, 1);
            final Point c = new Point(0, 1);
            Assert.assertTrue(Distance.distanceLineAndPoint(a, b, c) > 0);
        }
    }
}