
package science.unlicense.impl.geometry.s2d;

import science.unlicense.impl.geometry.s2d.Circle;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Bertrand COTE
 */
public class CircleTest {
    
    private static final int circleIndex = 0;
    
    private static final double[][][] circleParametersTest = new double[][][] {
        // { { cx, cy, radius } },
        { { 2, -3, 4 } },
        { { -5, 2, 2 } },
        { { -5, -8, 1.5 } },
        { { 9, 4, 3.5 } },
    };
    
    private static final Circle[] circleTest;
    
    static {
        circleTest = new Circle[circleParametersTest.length];
        for( int i=0; i<circleParametersTest.length; i++ ) {
            TupleRW center = new DefaultTuple( circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1] );
            double radius = circleParametersTest[i][circleIndex][2];
            circleTest[i] = new Circle( center, radius );
        }
    }
    
    public CircleTest() {
    }

    /**
     * Test of getCenter method, of class Circle.
     */
    @Test
    public void testGetCenter() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            Tuple expResult = new DefaultTuple( circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1] );
            Tuple result = instance.getCenter();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getCenterX method, of class Circle.
     */
    @Test
    public void testGetCenterX() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            double expResult = circleParametersTest[i][circleIndex][0];
            double result = instance.getCenterX();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getCenterY method, of class Circle.
     */
    @Test
    public void testGetCenterY() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            double expResult = circleParametersTest[i][circleIndex][1];
            double result = instance.getCenterY();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setCenter method, of class Circle.
     */
    @Test
    public void testSetCenter_Tuple() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = new Circle();
            TupleRW center = new DefaultTuple( circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1] );
            Tuple expResult = new DefaultTuple( circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1] );
            instance.setCenter(center);
            Tuple result = instance.getCenter();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of setCenter method, of class Circle.
     */
    @Test
    public void testSetCenter_double_double() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = new Circle();
            double cx = circleParametersTest[i][circleIndex][0];
            double cy = circleParametersTest[i][circleIndex][1];
            instance.setCenter(cx, cy);
            Assert.assertEquals(cx, instance.getCenterX(), 0.);
            Assert.assertEquals(cy, instance.getCenterY(), 0.);
        }
    }

    /**
     * Test of setCenterX method, of class Circle.
     */
    @Test
    public void testSetCenterX() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = new Circle();
            double cx = circleParametersTest[i][circleIndex][0];
            instance.setCenterX(cx);
            Assert.assertEquals(cx, instance.getCenterX(), 0.);
        }
    }

    /**
     * Test of setCenterY method, of class Circle.
     */
    @Test
    public void testSetCenterY() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = new Circle();
            double cy = circleParametersTest[i][circleIndex][1];
            instance.setCenterY(cy);
            Assert.assertEquals(cy, instance.getCenterY(), 0.);
        }
    }

    /**
     * Test of getRadius method, of class Circle.
     */
    @Test
    public void testGetRadius() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            double expResult = circleParametersTest[i][circleIndex][2];
            double result = instance.getRadius();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setRadius method, of class Circle.
     */
    @Test
    public void testSetRadius() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = new Circle();
            double r = circleParametersTest[i][circleIndex][2];
            instance.setRadius(r);
            Assert.assertEquals(r, instance.getRadius(), 0.);
        }
    }

    /**
     * Test of getDiameter method, of class Circle.
     */
    @Test
    public void testGetDiameter() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            double expResult = 2.*circleParametersTest[i][circleIndex][2];
            double result = instance.getDiameter();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getArea method, of class Circle.
     */
    @Test
    public void testGetArea() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            double expResult = Maths.PI*circleParametersTest[i][circleIndex][2]*circleParametersTest[i][circleIndex][2];
            double result = instance.getArea();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getLength method, of class Circle.
     */
    @Test
    public void testGetLength() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            double expResult = Maths.TWO_PI*circleParametersTest[i][circleIndex][2];
            double result = instance.getLength();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getBoundingBox method, of class Circle.
     */
    @Test
    public void testGetBoundingBox() {
        for( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            BBox expResult = new BBox( 
                    new DefaultTuple( new double[]{
                        circleParametersTest[i][circleIndex][0]-circleParametersTest[i][circleIndex][2],
                        circleParametersTest[i][circleIndex][1]-circleParametersTest[i][circleIndex][2]}),
                    new DefaultTuple( new double[]{
                        circleParametersTest[i][circleIndex][0]+circleParametersTest[i][circleIndex][2],
                        circleParametersTest[i][circleIndex][1]+circleParametersTest[i][circleIndex][2]}) );
            BBox result = instance.getBoundingBox();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getBoundingCircle method, of class Circle.
     */
    @Test
    public void testGetBoundingCircle() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            Circle expResult = new Circle();
            expResult.setCenter(circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1]);
            expResult.setRadius(circleParametersTest[i][circleIndex][2]);
            Circle result = instance.getBoundingCircle();
            Assert.assertTrue(expResult.getCenter().equals(result.getCenter()));
            Assert.assertEquals( expResult.getRadius(), result.getRadius(), 0. );
        }
    }

    /**
     * Test of getCentroid method, of class Circle.
     */
    @Test
    public void testGetCentroid() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            Point expResult = new Point( circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1] ); //Maths.TWO_PI*circleParametersTest[i][circleIndex][2];
            Point result = instance.getCentroid();
            Assert.assertTrue(expResult.equals(result));
        }
    }

//    /**
//     * Test of createPathIterator method, of class Circle.
//     */
//    @Test
//    public void testCreatePathIterator() {
//        System.out.println("createPathIterator");
//        Circle instance = new Circle();
//        PathIterator expResult = null;
//        PathIterator result = instance.createPathIterator();
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }

    /**
     * Test of equals method, of class Circle.
     */
    @Test
    public void testEquals() {
        for ( int i=0; i<circleParametersTest.length; i++ ) {
            Circle instance = circleTest[i];
            
            Circle other = new Circle();
            other.setCenter(circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1]);
            other.setRadius(circleParametersTest[i][circleIndex][2]);
            Object obj = other;
            Assert.assertTrue( instance.equals(obj) );
            
            other = new Circle();
            other.setCenter(1.+circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1]);
            other.setRadius(circleParametersTest[i][circleIndex][2]);
            obj = other;
            Assert.assertTrue( !instance.equals(obj) );
            
            other = new Circle();
            other.setCenter(circleParametersTest[i][circleIndex][0], 1.+circleParametersTest[i][circleIndex][1]);
            other.setRadius(circleParametersTest[i][circleIndex][2]);
            obj = other;
            Assert.assertTrue( !instance.equals(obj) );
            
            other = new Circle();
            other.setCenter(circleParametersTest[i][circleIndex][0], circleParametersTest[i][circleIndex][1]);
            other.setRadius(1.+circleParametersTest[i][circleIndex][2]);
            obj = other;
            Assert.assertTrue( !instance.equals(obj) );
        }
    }

//    /**
//     * Test of toChars method, of class Circle.
//     */
//    @Test
//    public void testToChars() {
//        System.out.println("toChars");
//        Circle instance = new Circle();
//        Chars expResult = null;
//        Chars result = instance.toChars();
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
}
