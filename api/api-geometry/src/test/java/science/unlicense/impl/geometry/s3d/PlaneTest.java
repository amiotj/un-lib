
package science.unlicense.impl.geometry.s3d;

import science.unlicense.impl.geometry.s3d.Plane;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class PlaneTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void computePlaneFromPoints(){
        
        final Plane plane = Plane.computeBestFittingPlan(new Tuple[]{
            new Vector(-1, 0, 0),
            new Vector(0, -1, 0),
            new Vector(0, +1, 0),
            new Vector(+1, 0, 0)
        });
        
        Assert.assertEquals(new Vector(0, 0, -1), plane.getNormal());
        Assert.assertEquals(new Vector(0, 0, 0), plane.getPoint());
    }
    
    @Test
    public void calculateAngle(){

        final Plane plane = new Plane(new Vector(0, 1, 0), new Vector(0, 0, 0));

        Assert.assertEquals(Maths.HALF_PI, plane.calculateAngle(new Vector(0, 15, 0)), DELTA);
        Assert.assertEquals(0.0, plane.calculateAngle(new Vector(0.5, 0, 0)), DELTA);
        Assert.assertEquals(0.0, plane.calculateAngle(new Vector(0, 0, 10)), DELTA);
        Assert.assertEquals(Maths.QUATER_PI, plane.calculateAngle(new Vector(1, 1, 0)), DELTA);
    }

}
