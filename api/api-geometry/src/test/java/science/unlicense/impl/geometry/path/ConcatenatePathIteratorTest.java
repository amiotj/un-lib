
package science.unlicense.impl.geometry.path;

import science.unlicense.impl.geometry.path.CoordinateTuplePathIterator;
import science.unlicense.impl.geometry.path.ConcatenatePathIterator;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.CoordinatePathIterator;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public class ConcatenatePathIteratorTest {
    
    @Test
    public void concatenateTest(){
        
        final Tuple i1 = new DefaultTuple(5, 10);
        final Tuple i2 = new DefaultTuple(10, 15);
        final Tuple i3 = new DefaultTuple(15, 20);
        final Tuple i4 = new DefaultTuple(20, 25);
        final Tuple i5 = new DefaultTuple(25, 30);
        final Tuple i6 = new DefaultTuple(30, 35);
        
        final CoordinateTuplePathIterator ite1 = new CoordinateTuplePathIterator(new Tuple[]{i1,i2,i3});
        final CoordinateTuplePathIterator ite2 = new CoordinateTuplePathIterator(new Tuple[]{i4,i5,i6});
        final Sequence seq = new ArraySequence();
        seq.add(ite1);
        seq.add(ite2);
        final ConcatenatePathIterator ite = new ConcatenatePathIterator(seq);
        
        
        TupleRW c = new DefaultTuple(2);
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO,ite.getType());
        Assert.assertEquals(i1, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i2, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i3, ite.getPosition(c));
        
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_MOVE_TO,ite.getType());
        Assert.assertEquals(i4, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i5, ite.getPosition(c));
        Assert.assertTrue(ite.next());
        Assert.assertEquals(PathIterator.TYPE_LINE_TO,ite.getType());
        Assert.assertEquals(i6, ite.getPosition(c));
        
        Assert.assertFalse(ite.next());
        
        
    }
    
}
