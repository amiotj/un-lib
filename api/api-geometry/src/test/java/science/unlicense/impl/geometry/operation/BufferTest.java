
package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.Buffer;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.api.geometry.operation.OperationException;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.Point;

/**
 *
 * @author Johann Sorel
 */
public class BufferTest {

    @Test
    public void point() throws OperationException{
        final Point point = new Point(5, 10);

        Object result = Operations.execute(new Buffer(point, 3));
        Assert.assertEquals(new Circle(5, 10, 3), result);

        result = Operations.execute(new Buffer(point, -2));
        Assert.assertEquals(new Point(5, 10), result);

    }

    @Test
    public void circle() throws OperationException{
        final Circle circle = new Circle(5, 10, 6);

        Object result = Operations.execute(new Buffer(circle, 3));
        Assert.assertEquals(new Circle(5, 10, 9), result);

        result = Operations.execute(new Buffer(circle, -6));
        Assert.assertEquals(new Point(5, 10), result);

        result = Operations.execute(new Buffer(circle, -9));
        Assert.assertEquals(new Point(5, 10), result);

    }

}
