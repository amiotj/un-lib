package science.unlicense.impl.geometry.triangulate;

import science.unlicense.impl.geometry.triangulate.Delaunay;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.operation.ConvexHull;

import java.util.Random;

import org.junit.Assert;
import org.junit.Ignore;

/**
 * TODO more complete tests.
 *
 * @author Johann Sorel
 * @author Izyumov Konstantin
 */
public class DelaunayTest {

    /**
     * Test a single triangle case.
     */
    @Test(timeout = 2000)
    public void singleTest() {

        final Delaunay delaunay = new Delaunay();

        delaunay.insertPoint(new Point(0, 0));
        delaunay.insertPoint(new Point(0, 1));
        delaunay.insertPoint(new Point(1, 0));

        final Sequence edges = delaunay.computeEdges();
        Assert.assertEquals(3, edges.getSize());
        ArraySequence arraySequence = new ArraySequence();
        arraySequence.add(new Point[]{new Point(0, 0), new Point(0, 1)});
        arraySequence.add(new Point[]{new Point(0, 1), new Point(1, 0)});
        arraySequence.add(new Point[]{new Point(0, 0), new Point(1, 0)});

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < arraySequence.getSize(); j++) {
                Point[] in = (Point[]) arraySequence.get(j);
                Point[] out = (Point[]) edges.get(i);
                if (in[0].equals(out[0]) && in[1].equals(out[1])) {
                    arraySequence.remove(j);
                    break;
                }
            }
        }
        Assert.assertTrue(arraySequence.getSize() == 0);


        final Sequence triangles = delaunay.computeTriangles();
        Assert.assertEquals(1, triangles.getSize());

        final Sequence voronoi = delaunay.computeVoronoi();
        Assert.assertEquals(3, voronoi.getSize());

        convexHullTest(delaunay);
    }

    @Test(timeout = 20000)
    public void testRandomTriangulation() {
        int AMOUNT_RANDOM_TESTS = 10000;
        int AMOUNT_RANDOM_POINTS = 5;
        int SIZE_TRIANGULATION_BOX = 100;
        Random random = new Random();
        for (int i = 0; i < AMOUNT_RANDOM_TESTS; i++) {
            Delaunay delaunay = new Delaunay();
            Point[] points = new Point[AMOUNT_RANDOM_POINTS];
            for (int j = 0; j < points.length; j++) {
                points[j] = new Point(
                        random.nextInt(SIZE_TRIANGULATION_BOX),
                        random.nextInt(SIZE_TRIANGULATION_BOX)
                );
            }
            for (int j = 0; j < AMOUNT_RANDOM_POINTS; j++) {
                delaunay.insertPoint(points[j]);
            }
            if (delaunay.computeTriangles().getSize() == 0) {
                System.err.println("Error in triangulation. Save points for analyze:");
                for (int j = 0; j < points.length; j++) {
                    System.err.println(points[j]);
                }
            }
            Assert.assertTrue(delaunay.computeTriangles().getSize() > 0);
            convexHullTest(delaunay);
        }
    }

    @Ignore //TODO test failing
    @Test(timeout = 1000)
    public void skipSmallTriangles1() {
        double shortDistance = 1.0D;
        for (int i = 0; i < 6; i++) {
            shortDistance /= 10;
            Delaunay delaunay = new Delaunay();
            delaunay.insertPoint(new Point(0.0D, 0.0D));
            delaunay.insertPoint(new Point(0.0D, 1.0D));
            delaunay.insertPoint(new Point(shortDistance, 0.5D));

            final String msg = String.format("shortDistance = %.2e", shortDistance);

            final Sequence edges = delaunay.computeEdges();
            Assert.assertEquals(msg, 3, edges.getSize());
            Assert.assertArrayEquals(msg, new Point[]{new Point(0, 0), new Point(0, 1)}, (Point[]) edges.get(0));
            Assert.assertArrayEquals(msg, new Point[]{new Point(0, 1), new Point(shortDistance, 0.5)}, (Point[]) edges.get(1));
            Assert.assertArrayEquals(msg, new Point[]{new Point(0, 0), new Point(shortDistance, 0.5)}, (Point[]) edges.get(2));

            final Sequence triangles = delaunay.computeTriangles();
            Assert.assertEquals(msg, 1, triangles.getSize());
            Assert.assertArrayEquals(msg, new Point[]{
                    new Point(0, 1),
                    new Point(0, 0),
                    new Point(shortDistance, 0.5)}, (Point[]) triangles.get(0));

            convexHullTest(delaunay);
        }
    }

    @Test(timeout = 1000)
    public void smallTriangles2() {
        double shortDistance = 1.0D;
        for (int i = 0; i < 6; i++) {
            shortDistance /= 10;
            Delaunay delaunay = new Delaunay();
            delaunay.insertPoint(new Point(0.0D, 0.0D));
            delaunay.insertPoint(new Point(1.0D, 0.0D));
            delaunay.insertPoint(new Point(0.5D, shortDistance));

            final String msg = String.format("shortDistance = %.2e", shortDistance);

            final Sequence edges = delaunay.computeEdges();
            Assert.assertEquals(msg, 3, edges.getSize());

            final Sequence triangles = delaunay.computeTriangles();
            Assert.assertEquals(msg, 1, triangles.getSize());

            convexHullTest(delaunay);
        }
    }


    @Test(timeout = 1000)
    public void triangulate4points() {
        Point[] points = new Point[]{
                new Point(58, 66),
                new Point(34, 76),
                new Point(68, 73),
                new Point(44, 71)
        };
        Delaunay delaunay = new Delaunay();
        for (int i = 0; i < points.length; i++) {
            delaunay.insertPoint(points[i]);
        }
        Assert.assertTrue(delaunay.computeTriangles().getSize() > 0);
        convexHullTest(delaunay);
    }

    @Test//(timeout = 1000)
    public void triangulate5points() {
        Point[] points = new Point[]{
                new Point(72, 13),
                new Point(50, 44),
                new Point(72, 13),
                new Point(45, 61),
                new Point(27, 59)
        };
        Delaunay delaunay = new Delaunay();
        for (int i = 0; i < points.length; i++) {
            delaunay.insertPoint(points[i]);
        }
        Assert.assertTrue(delaunay.computeTriangles().getSize() > 0);
        convexHullTest(delaunay);
    }

    @Test(timeout = 1000)
    public void needleTriangle1() {
        Delaunay delaunay = new Delaunay();
        delaunay.insertPoint(new Point(+0.0D, +0.0D));
        delaunay.insertPoint(new Point(+1.0D, +0.0D));
        Point point = new Point(+0.5D, 1.0e-6);
        delaunay.insertPoint(point);
        Assert.assertTrue(delaunay.computeTriangles().getSize() > 0);
        convexHullTest(delaunay);
    }

    @Test(timeout = 1000)
    public void needleTriangle2() {
        Delaunay delaunay = new Delaunay();
        delaunay.insertPoint(new Point(+0.0D, +0.0D));
        delaunay.insertPoint(new Point(+1.0D, +0.0D));
        delaunay.insertPoint(new Point(+0.0D, 1.0e-8));
        Assert.assertTrue(delaunay.computeTriangles().getSize() > 0);
        convexHullTest(delaunay);
    }

    public static void convexHullTest(Delaunay delaunay) {
        Sequence edges = delaunay.computeEdges();
        Sequence delaunayPoints = delaunay.getPoints();
        Point[] points = new Point[delaunayPoints.getSize()];

        String msg = "\nPoints of triangulation:\n";
        for (int i = 0; i < points.length; i++) {
            points[i] = (Point) delaunayPoints.get(i);
            msg += points[i] + "\n";
        }

        msg += "\nEdges\n";
        for (int i = 0; i < edges.getSize(); i++) {
            for (int j = 0; j < 2; j++) {
                msg +=  ((Point[])edges.get(i))[j] + " ";
            }
            msg += "\n";
        }

        Point[] convexHullPoints = ConvexHull.convexHull(points);
        msg += "\nConvexHull points:\n";
        for (int i = 0; i < convexHullPoints.length; i++) {
            msg += convexHullPoints[i] + "\n";
        }

        for (int i = 0; i < convexHullPoints.length; i++) {
            Point p1 = convexHullPoints[i];
            Point p2;
            if (i == 0) {
                p2 = convexHullPoints[convexHullPoints.length - 1];
            } else {
                p2 = convexHullPoints[i - 1];
            }
            boolean isConvexInEdges = false;
            for (int j = 0; j < edges.getSize(); j++) {
                Point[] edgePoint = (Point[]) edges.get(j);
                if ((edgePoint[0].equals(p1) && edgePoint[1].equals(p2)) ||
                        (edgePoint[1].equals(p1) && edgePoint[0].equals(p2))) {
                    isConvexInEdges = true;
                    j = edges.getSize();
                }
            }
            Assert.assertTrue(msg + "\n Checking convex point : " + i + "\n", isConvexInEdges);
        }
    }
}
