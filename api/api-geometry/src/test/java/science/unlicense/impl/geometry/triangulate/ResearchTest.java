package science.unlicense.impl.geometry.triangulate;

import science.unlicense.impl.geometry.triangulate.Delaunay;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import science.unlicense.impl.geometry.Point;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

/**
 * class ResearchTest created for analyze triangulation on
 * different type of input data:
 * - more points in lines
 * - more points on border
 * - more points inside border
 * - random points
 *
 * @author Izyumov Konstantin
 */
@RunWith(Parameterized.class)
public class ResearchTest {

    private static final boolean PRINTABLE = false;

    enum TYPE_TEST {
        RANDOM,
        CIRCLE,
        LINE_IN_LINE,
        IN_TRIANGLE;
    }

    @Parameters
    public static Collection<Object[]> data() {
        int[] amountPoints = new int[]{
                3, 5,
                10, 20, 50,
                100, 200, 500,
               // 1000, 2000
        };
        Object[][] tests = new Object[TYPE_TEST.values().length*amountPoints.length][2];
        int position = 0;
        for (int i = 0; i < TYPE_TEST.values().length; i++) {
            for (int j = 0; j < amountPoints.length; j++) {
                tests[position++] = new Object[]{TYPE_TEST.values()[i], amountPoints[j]};
            }
        }
        return Arrays.asList(tests);
    }

    private static Random random = new Random();

    public TYPE_TEST type;
    public int amount;

    public ResearchTest(TYPE_TEST type, int amount) {
        this.type = type;
        this.amount = amount;
    }


    @Test(timeout = 20000)
    public void testTriangulationDifferentType() {
        if(PRINTABLE)System.out.print("TYPE OF TEST: " + type.toString());
        Assert.assertTrue(internalTest(amount, type) > 0);
    }

    private static int internalTest(int size, TYPE_TEST type_test) {
        int amountTest = 20;
        long start[] = new long[amountTest];
        long finish[] = new long[amountTest];
        float averageTime = 0;
        int sizeTriangles = 0;
        Point[] mav = null;
        switch (type_test) {
            case RANDOM:
                mav = getRandomPoints(size);
                break;
            case CIRCLE:
                mav = getCirclePoints(size);
                break;
            case LINE_IN_LINE:
                mav = getLineOnLine(size);
                break;
            case IN_TRIANGLE:
                mav = getInTriangles(size);
                break;
        }

        for (int i = 0; i < amountTest; i++) {
            start[i] = System.currentTimeMillis();
            Delaunay delaunay = new Delaunay();
            for (int j = 0; j < mav.length; j++) {
                delaunay.insertPoint(mav[j]);
            }
            finish[i] = System.currentTimeMillis();
            if(i==0) {
                sizeTriangles = delaunay.computeTriangles().getSize();
                DelaunayTest.convexHullTest(delaunay);
            }
        }
        for (int i = 0; i < amountTest; i++) {
            float time = (float) (finish[i] - start[i]);
            averageTime += time;
        }

        averageTime /= amountTest;
        if(PRINTABLE){
            System.out.print(": amount points: " + String.format("%6d", size)
                    + " point. "
                    + "Time is "
                    + String.format("%12.2f", averageTime) + " ms. "
                    + "Triangles is "
                    + String.format("%6d", sizeTriangles) + " triangles. \n"
            );
        }
        return sizeTriangles;
    }


    public static Point[] getRandomPoints(final int size) {
        Point[] coordinates = new Point[size];
        for (int j = 0; j < coordinates.length; j++) {
            coordinates[j] = new Point(
                    (random.nextFloat()) * 600,
                    (random.nextFloat()) * 600
            );
        }
        return coordinates;
    }

    public static Point[] getCirclePoints(final int size) {
        Point[] coordinates = new Point[size];
        for (int j = 0; j < size - 1; j++) {
            coordinates[j] = new Point(
                    300 * Math.sin(2 * 3.1415 / size * j) + 300,
                    300 * Math.cos(2 * 3.1415 / size * j) + 300
            );
        }
        coordinates[coordinates.length - 1] = new Point(300, 300);
        return coordinates;
    }

    public static Point[] getLineOnLine(final int size) {
        Point[] coordinates = new Point[3 + size];
        coordinates[0] = new Point(0, 0);
        coordinates[1] = new Point(600, 0);
        coordinates[2] = new Point(600, 600);
        for (int j = 3; j < coordinates.length; j++) {
            coordinates[j] = new Point(
                    0.8D * j / coordinates.length * 600D,
                    0.8D * j / coordinates.length * 600D
            );
        }
        return coordinates;
    }

    public static Point[] getInTriangles(final int size) {
        Point[] coordinates = new Point[4 + size];
        coordinates[0] = new Point(0, 0);
        coordinates[1] = new Point(600, 0);
        coordinates[2] = new Point(0, 600);
        coordinates[3] = new Point(600, 600);
        for (int j = 4; j < coordinates.length; j++) {
            coordinates[j] = new Point(
                    0.1 + 600 * 0.8 * (double) j / (double) size,
                    300
            );
        }
        return coordinates;
    }
}
