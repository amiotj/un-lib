
package science.unlicense.impl.geometry.path;

import science.unlicense.impl.geometry.path.TransformedPathIterator;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class TransformedPathIteratorTest {

    @Test
    public void mismatchDimensionTest() {

        final Rectangle rect = new Rectangle();
        final Matrix4x4 m = new Matrix4x4();

        try {
            new TransformedPathIterator(rect.createPathIterator(), m);
            Assert.fail("Creating a transformed geometry with a different size transform should have failed");
        } catch(InvalidArgumentException ex) {
            //ok
        }
    }

}
