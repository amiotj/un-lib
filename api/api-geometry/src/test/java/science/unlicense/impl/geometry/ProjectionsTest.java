
package science.unlicense.impl.geometry;

import science.unlicense.impl.geometry.Projections;
import org.junit.Test;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Vector;
import org.junit.Assert;
import science.unlicense.api.math.Affine;

/**
 *
 * @author Johann Sorel
 */
public class ProjectionsTest {

    @Test
    public void centeredMatrixTest(){
        final BBox source = new BBox(new Vector(10, 10), new Vector(30, 50));
        final BBox target = new BBox(new Vector(100, 200), new Vector(200, 400));

        final Affine trs = Projections.centered(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector(140, 280), new Vector(160, 320)), result);
    }

    @Test
    public void scaledMatrixTest(){
        final BBox source = new BBox(new Vector(10, 10), new Vector(30, 50));
        final BBox target = new BBox(new Vector(60, 20), new Vector(110, 40));

        final Affine trs = Projections.scaled(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector(80, 20), new Vector(90, 40)), result);
    }

    @Test
    public void stretchedMatrixTest(){
        final BBox source = new BBox(new Vector(10, 10), new Vector(30, 50));
        final BBox target = new BBox(new Vector(100, 200), new Vector(200, 400));

        final Affine trs = Projections.stretched(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(target, result);
    }

    @Test
    public void zoomedMatrixTest(){
        final BBox source = new BBox(new Vector(10, 10), new Vector(30, 50));
        final BBox target = new BBox(new Vector(60, 20), new Vector(110, 40));

        final Affine trs = Projections.zoomed(source, target);
        final BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector(60, -20), new Vector(110, 80)), result);
    }

    @Test
    public void fitMatrixTest(){
        final BBox source = new BBox(new Vector(10, 10), new Vector(30, 50));
        final BBox target = new BBox(new Vector(60, 20), new Vector(110, 40));

        Affine trs = Projections.fit(source, target, 0);
        BBox result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector(60, -20), new Vector(110, 80)), result);

        trs = Projections.fit(source, target, 1);
        result = new BBox(trs.transform(source.getLower(),null),trs.transform(source.getUpper(),null));
        Assert.assertEquals(new BBox(new Vector(80, 20), new Vector(90, 40)), result);
    }

}
