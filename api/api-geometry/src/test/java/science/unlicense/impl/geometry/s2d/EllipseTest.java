
package science.unlicense.impl.geometry.s2d;

import science.unlicense.impl.geometry.s2d.Ellipse;
import science.unlicense.impl.geometry.s2d.Circle;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Bertrand COTE
 */
public class EllipseTest {
    
    private static final int ellipseIndex = 0;
    
    private static final double[][][] ellipseParametersTest = new double[][][] {
        // { { cx, cy, rx, ry } },
        { { 2, -3, 4, 3 } },
        { { -5, 2, 2, 1 } },
        { { -5, -8, 1.5, 2.25 } },
        { { 9, 4, 3.5, 5.75 } },
    };
    
    private static final Ellipse[] ellipseTest;
    
    static {
        ellipseTest = new Ellipse[ellipseParametersTest.length];
        for (int i = 0; i < ellipseParametersTest.length; i++) {
            ellipseTest[i] = new Ellipse(
                    ellipseParametersTest[i][ellipseIndex][0],
                    ellipseParametersTest[i][ellipseIndex][1],
                    ellipseParametersTest[i][ellipseIndex][2],
                    ellipseParametersTest[i][ellipseIndex][3]);
        }
    }
    
    public EllipseTest() {
    }

    /**
     * Test of getCenter method, of class Ellipse.
     */
    @Test
    public void testGetCenter() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            Tuple expResult = new DefaultTuple( ellipseParametersTest[i][ellipseIndex][0], ellipseParametersTest[i][ellipseIndex][1] );
            Tuple result = instance.getCenter();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getCenterX method, of class Ellipse.
     */
    @Test
    public void testGetCenterX() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            double expResult = ellipseParametersTest[i][ellipseIndex][0];
            double result = instance.getCenterX();
            Assert.assertEquals(expResult, result, 0);
        }
    }

    /**
     * Test of getCenterY method, of class Ellipse.
     */
    @Test
    public void testGetCenterY() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            double expResult = ellipseParametersTest[i][ellipseIndex][1];
            double result = instance.getCenterY();
            Assert.assertEquals(expResult, result, 0);
        }
    }

    /**
     * Test of setCenter method, of class Ellipse.
     */
    @Test
    public void testSetCenter_Tuple() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = new Ellipse();
            TupleRW center = new DefaultTuple( ellipseParametersTest[i][ellipseIndex][0], ellipseParametersTest[i][ellipseIndex][1] );
            Tuple expResult = new DefaultTuple( ellipseParametersTest[i][ellipseIndex][0], ellipseParametersTest[i][ellipseIndex][1] );
            instance.setCenter(center);
            Tuple result = instance.getCenter();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of setCenter method, of class Ellipse.
     */
    @Test
    public void testSetCenter_double_double() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = new Ellipse();
            double cx = ellipseParametersTest[i][ellipseIndex][0];
            double cy = ellipseParametersTest[i][ellipseIndex][1];
            instance.setCenter(cx, cy);
            Tuple expResult = new DefaultTuple( ellipseParametersTest[i][ellipseIndex][0], ellipseParametersTest[i][ellipseIndex][1] );
            Tuple result = instance.getCenter();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of setCenterX method, of class Ellipse.
     */
    @Test
    public void testSetCenterX() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = new Ellipse();
            double cx = ellipseParametersTest[i][ellipseIndex][0];
            instance.setCenterX(cx);
            Assert.assertEquals(cx, instance.getCenterX(), 0.);
        }
    }

    /**
     * Test of setCenterY method, of class Ellipse.
     */
    @Test
    public void testSetCenterY() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = new Ellipse();
            double cy = ellipseParametersTest[i][ellipseIndex][1];
            instance.setCenterY(cy);
            Assert.assertEquals(cy, instance.getCenterY(), 0.);
        }
    }

    /**
     * Test of getRadiusX method, of class Ellipse.
     */
    @Test
    public void testGetRadiusX() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            double expResult = ellipseParametersTest[i][ellipseIndex][2];
            double result = instance.getRadiusX();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setRadiusX method, of class Ellipse.
     */
    @Test
    public void testSetRadiusX() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = new Ellipse();
            double r = ellipseParametersTest[i][ellipseIndex][2];
            instance.setRadiusX(r);
            Assert.assertEquals(r, instance.getRadiusX(), 0.);
        }
    }

    /**
     * Test of getRadiusY method, of class Ellipse.
     */
    @Test
    public void testGetRadiusY() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            double expResult = ellipseParametersTest[i][ellipseIndex][3];
            double result = instance.getRadiusY();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of setRadiusY method, of class Ellipse.
     */
    @Test
    public void testSetRadiusY() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = new Ellipse();
            double r = ellipseParametersTest[i][ellipseIndex][3];
            instance.setRadiusY(r);
            Assert.assertEquals(r, instance.getRadiusY(), 0.);
        }
    }

    /**
     * Test of getArea method, of class Ellipse.
     */
    @Test
    public void testGetArea() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            double expResult = Maths.PI*ellipseParametersTest[i][ellipseIndex][2]*ellipseParametersTest[i][ellipseIndex][3];
            double result = instance.getArea();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getLength method, of class Ellipse.
     */
    @Test
    public void testGetLength() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            double expResult = Maths.PI*(ellipseParametersTest[i][ellipseIndex][2]+ellipseParametersTest[i][ellipseIndex][3]);
            double result = instance.getLength();
            Assert.assertEquals(expResult, result, 0.);
        }
    }

    /**
     * Test of getBoundingBox method, of class Ellipse.
     */
    @Test
    public void testGetBoundingBox() {
        for( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            BBox expResult = new BBox( 
                    new DefaultTuple( new double[]{
                        ellipseParametersTest[i][ellipseIndex][0]-ellipseParametersTest[i][ellipseIndex][2],
                        ellipseParametersTest[i][ellipseIndex][1]-ellipseParametersTest[i][ellipseIndex][3]}),
                    new DefaultTuple( new double[]{
                        ellipseParametersTest[i][ellipseIndex][0]+ellipseParametersTest[i][ellipseIndex][2],
                        ellipseParametersTest[i][ellipseIndex][1]+ellipseParametersTest[i][ellipseIndex][3]}) );
            BBox result = instance.getBoundingBox();
            Assert.assertTrue(expResult.equals(result));
        }
    }

    /**
     * Test of getBoundingCircle method, of class Ellipse.
     */
    @Test
    public void testGetBoundingCircle() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            Circle expResult = new Circle();
            expResult.setCenter(ellipseParametersTest[i][ellipseIndex][0], ellipseParametersTest[i][ellipseIndex][1]);
            expResult.setRadius(Maths.max(ellipseParametersTest[i][ellipseIndex][2], ellipseParametersTest[i][ellipseIndex][3]));
            Circle result = instance.getBoundingCircle();
            Assert.assertTrue(expResult.getCenter().equals(result.getCenter()));
            Assert.assertEquals( expResult.getRadius(), result.getRadius(), 0. );
        }
    }

    /**
     * Test of getCentroid method, of class Ellipse.
     */
    @Test
    public void testGetCentroid() {
        for ( int i=0; i<ellipseParametersTest.length; i++ ) {
            Ellipse instance = ellipseTest[i];
            Point expResult = new Point( ellipseParametersTest[i][ellipseIndex][0], ellipseParametersTest[i][ellipseIndex][1] );
            Point result = instance.getCentroid();
            Assert.assertTrue(expResult.equals(result));
        }
    }

//    /**
//     * Test of createPathIterator method, of class Ellipse.
//     */
//    @Test
//    public void testCreatePathIterator() {
//        System.out.println("createPathIterator");
//        Ellipse instance = new Ellipse();
//        PathIterator expResult = null;
//        PathIterator result = instance.createPathIterator();
//        Assert.assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        Assert.fail("The test case is a prototype.");
//    }
    
}
