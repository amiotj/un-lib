package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.ConvexHull;
import org.junit.Test;
import science.unlicense.impl.geometry.Point;
import org.junit.Assert;

public class ConvexHullTest {

    // Simple test of triangle
    // all elements in triangle
    @Test
    public void convexHullTest1() throws Exception {
        Point[] points = new Point[]{
                new Point(0, 0),
                new Point(0, 1),
                new Point(1, 0)
        };
        Point[] convexPoints = ConvexHull.convexHull(points);
        Assert.assertTrue(convexPoints.length == 3);
        Assert.assertTrue(points[0].equals(convexPoints[0]));
        Assert.assertTrue(points[1].equals(convexPoints[1]));
        Assert.assertTrue(points[2].equals(convexPoints[2]));
    }

    // ConvexHull test of rectangle
    // all elements is convexHull
    @Test
    public void convexHullTest2() throws Exception {
        Point[] points = new Point[]{
                new Point(0, 0),
                new Point(0, 1),
                new Point(1, 0),
                new Point(1, 1),
        };
        Point[] convexPoints = ConvexHull.convexHull(points);
        Assert.assertTrue(convexPoints.length == 4);
        Assert.assertTrue(points[0].equals(convexPoints[0]));
        Assert.assertTrue(points[1].equals(convexPoints[1]));
        Assert.assertTrue(points[3].equals(convexPoints[2]));
        Assert.assertTrue(points[2].equals(convexPoints[3]));
    }

    @Test
    public void convexHullTest3() throws Exception {
        Point[] points = new Point[]{
                new Point(0, 0),
                new Point(0, 1),
                new Point(1, 0),
                new Point(1, 1),
                new Point(0.5,0.5)
        };
        Point[] convexPoints = ConvexHull.convexHull(points);
        Assert.assertTrue(convexPoints.length == 4);
        Assert.assertTrue(points[0].equals(convexPoints[0]));
        Assert.assertTrue(points[1].equals(convexPoints[1]));
        Assert.assertTrue(points[2].equals(convexPoints[3]));
        Assert.assertTrue(points[3].equals(convexPoints[2]));
    }


    @Test
    public void convexHullTest4() {
        Point[] points = new Point[]{
                new Point(34, 90),
                new Point(99, 47),
                new Point(34, 53),
                new Point(88, 31),
                new Point(1, 92)
        };
        Point[] convexPoints = ConvexHull.convexHull(points);
        Assert.assertTrue(convexPoints.length == 5);
        Assert.assertTrue(points[4].equals(convexPoints[0]));
        Assert.assertTrue(points[0].equals(convexPoints[1]));
        Assert.assertTrue(points[1].equals(convexPoints[2]));
        Assert.assertTrue(points[3].equals(convexPoints[3]));
        Assert.assertTrue(points[2].equals(convexPoints[4]));
    }

    @Test
    public void convexHullTest5() {
        Point[] points = new Point[]{
                new Point(51, 34),
                new Point(0, 31),
                new Point(51, 26),
                new Point(51, 67),
                new Point(5, 93)
        };
        Point[] convexPoints = ConvexHull.convexHull(points);
        Assert.assertTrue(convexPoints.length == 5);
        Assert.assertTrue(points[1].equals(convexPoints[0]));
        Assert.assertTrue(points[4].equals(convexPoints[1]));
        Assert.assertTrue(points[3].equals(convexPoints[2]));
        Assert.assertTrue(points[0].equals(convexPoints[3]));
        Assert.assertTrue(points[2].equals(convexPoints[4]));
    }
}