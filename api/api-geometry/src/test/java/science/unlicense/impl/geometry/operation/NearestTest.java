
package science.unlicense.impl.geometry.operation;

import science.unlicense.impl.geometry.operation.Nearest;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.api.geometry.operation.OperationException;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.OBBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class NearestTest {

    private static final double DELTA = 0.00000001;

    @Test
    public void point_point() throws OperationException{
        Point geom1 = new Point(10, 20);
        Point geom2 = new Point(10, 25);
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(geom2,result[1]);
    }

    @Test
    public void point_plane() throws OperationException{
        Point geom1 = new Point(0, 10, 0);
        Plane geom2 = new Plane(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(0, 0, 0),result[1]);

        geom1 = new Point(35, -8, -12);
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(35, 0, -12),result[1]);
    }
    
    @Test
    public void point_triangle() throws OperationException{
        Point geom1 = new Point(0, 10, 0);
        Triangle geom2 = new Triangle(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(0, 0, 0),result[1]);

        geom1 = new Point(20, -3, 0);
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(1, 0, 0),result[1]);
    }
    
    @Test
    public void point_rectangle() throws OperationException{
        Point geom1 = new Point(0, 0);
        Rectangle geom2 = new Rectangle(5,10,20,5);
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(5, 10),result[1]);

        //point inside
        geom1 = new Point(10,12);
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(10, 12),result[1]);
    }
    
    @Test
    public void point_bbox() throws OperationException{
        Point geom1 = new Point(0, 0);
        BBox geom2 = new BBox(new double[]{5,10},new double[]{25,15});
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(5, 10),result[1]);

        //point inside
        geom1 = new Point(10,12);
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(10, 12),result[1]);
    }
    
    @Test
    public void point_obbox() throws OperationException{
        Point geom1 = new Point(0, 0);
        OBBox geom2 = new OBBox(new double[]{0,0},new double[]{20,5});
        geom2.getTransform().getTranslation().setXY(5, 10);
        geom2.getTransform().notifyChanged();
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(5, 10),result[1]);

        //point inside
        geom1 = new Point(10,12);
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(geom1,result[0]);
        Assert.assertEquals(new Point(10, 12),result[1]);
    }

    @Test
    public void segment_triangle() throws OperationException{
        //segment straight over the triangle, both point project on it
        Segment geom1 = new Segment(new Vector(0.1, 12, 0.1), new Vector(0.3, 10, 0.3));
        Triangle geom2 = new Triangle(new Vector(1,0,0), new Vector(0,0,0), new Vector(0,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(new Point(0.3, 10, 0.3),result[0]);
        Assert.assertEquals(new Point(0.3,  0, 0.3),result[1]);

        //segment do not project on triangle
        geom1 = new Segment(new Vector(14, -5, 3), new Vector(17, -8, 6));
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(new Point(14, -5, 3),result[0]);
        Assert.assertEquals(new Point(1, 0, 0),result[1]);
    }
    
    @Test
    public void triangle_triangle() throws OperationException{
        //parallal triangles
        Triangle geom1 = new Triangle(new Vector(1,1,0), new Vector(1,0,0), new Vector(1,0,1));
        Triangle geom2 = new Triangle(new Vector(2,1,0), new Vector(2,0,0), new Vector(2,0,1));
        Geometry[] result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(new Point(1, 1, 0),result[0]);
        Assert.assertEquals(new Point(2, 1, 0),result[1]);

        //perpendicular triangles
        geom1 = new Triangle(new Vector(10,3,0), new Vector(0,3,0), new Vector(0,3,10));
        result = (Geometry[]) Operations.execute(new Nearest(geom1, geom2));
        Assert.assertEquals(new Point(2, 3, 0),result[0]);
        Assert.assertEquals(new Point(2, 1, 0),result[1]);
    }
    
}
