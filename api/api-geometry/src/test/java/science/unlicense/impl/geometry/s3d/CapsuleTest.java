

package science.unlicense.impl.geometry.s3d;

import science.unlicense.impl.geometry.s3d.Capsule;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.impl.math.Vector;

/**
 * capsule class tests.
 *
 * @author Johann Sorel
 */
public class CapsuleTest {

    private static final double DELTA = 0.0000000001;

    @Test
    public void testPoints(){

        Capsule capsule = new Capsule(10, 1);
        Assert.assertArrayEquals(new double[]{0, -5, 0}, capsule.getFirst().getValues(), DELTA);
        Assert.assertArrayEquals(new double[]{0, +5, 0}, capsule.getSecond().getValues(), DELTA);

        capsule = new Capsule(new Vector(-5, 3, 7), new Vector(8, -9, 2), 1);
        Assert.assertArrayEquals(new double[]{-5, 3, 7}, capsule.getFirst().getValues(), DELTA);
        Assert.assertArrayEquals(new double[]{8, -9, 2}, capsule.getSecond().getValues(), DELTA);

    }

}
