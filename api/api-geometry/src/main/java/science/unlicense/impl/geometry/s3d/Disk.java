

package science.unlicense.impl.geometry.s3d;

import science.unlicense.impl.geometry.AbstractOrientedGeometry;
import science.unlicense.api.geometry.BBox;

/**
 * A filled disk defined by a radius.
 * The disk rotates around the Y axis.
 *
 * @author Johann Sorel
 */
public class Disk extends AbstractOrientedGeometry implements Geometry3D {

    private double radius;

    public Disk(double radius) {
        super(3);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public BBox getUnorientedBounds() {
        final BBox bbox = new BBox(3);
        bbox.getLower().setXYZ(-radius, 0, -radius);
        bbox.getUpper().setXYZ(radius, 0, radius);
        return bbox;
    }

    /**
     * Calculate disk surface area.
     * Since a disk has 2 sides :
     * 2 * (Math.PI*radius*radius)
     * 
     * @return surface
     */
    public double getSurface() {
        return 2 * (Math.PI*radius*radius);
    }

    /**
     * A Disk has no volume.
     * 
     * @return always 0
     */
    public double getVolume() {
        return 0;
    }
    
}
