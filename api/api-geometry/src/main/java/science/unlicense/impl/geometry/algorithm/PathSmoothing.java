
package science.unlicense.impl.geometry.algorithm;

import java.util.ArrayList;

/**
 * This class converts a sequence of coordinates in a smoother path with
 * longer line segments and quadratic curves.
 * 
 * Origin :
 * https://github.com/jankovicsandras/imagetracerjava
 * 
 * @author jankovics andras (andras@jankovics.net)
 * @author Johann Sorel (Adapted to unlicense-lib)
 */
public class PathSmoothing {
    
    
    /**
     * 5.2. - 5.6. recursively fitting a straight or quadratic line segment on this sequence of path nodes.
     * 
     * @param path
     * @param ltreshold
     * @param qtreshold
     * @param seqstart
     * @param seqend
     * @return 
     */
    public static ArrayList<double[]> fitseq(final ArrayList<double[]> path, 
            final double ltreshold, final double qtreshold, 
            final int seqstart, final int seqend){
        
        ArrayList<double[]> segment = new ArrayList<double[]>();
        int pathlength = path.size();
        
        // return if invalid seqend
        if((seqend>pathlength)||(seqend<0)){
            return segment;
        }
        
        final double[] startCoord = path.get(seqstart);
        final double[] endCoord = path.get(seqend);
        
        int errorpoint=seqstart;
        boolean curvepass=true;
        double px, py, dist2, errorval=0;
        double tl = (seqend-seqstart); 
        if(tl<0){ tl += pathlength; }
        final double vx = (double)(endCoord[0]-startCoord[0]) / tl;
        final double vy = (double)(endCoord[1]-startCoord[1]) / tl;
        
        // 5.2. Fit a straight line on the sequence
        int pcnt = (seqstart+1)%pathlength;
        double pl;
        while(pcnt != seqend){
            pl = pcnt-seqstart; 
            if(pl<0){ pl += pathlength; }
            px = startCoord[0] + vx * pl; 
            py = startCoord[1] + vy * pl;
            dist2 = (path.get(pcnt)[0]-px)*(path.get(pcnt)[0]-px) + (path.get(pcnt)[1]-py)*(path.get(pcnt)[1]-py);
            if(dist2>ltreshold){curvepass=false;}
            if(dist2>errorval){ errorpoint=pcnt; errorval=dist2; }
            pcnt = (pcnt+1)%pathlength;
        }
        
        // return straight line if fits
        double[] thissegment;
        if(curvepass){
            segment.add(new double[7]);
            thissegment = segment.get(segment.size()-1);
            thissegment[0] = 1.0;
            thissegment[1] = startCoord[0];
            thissegment[2] = startCoord[1];
            thissegment[3] = endCoord[0];
            thissegment[4] = endCoord[1];
            thissegment[5] = 0.0;
            thissegment[6] = 0.0;
            return segment;
        }
        
        // 5.3. If the straight line fails (an error>ltreshold), find the point with the biggest error
        int fitpoint = errorpoint; 
        curvepass = true; 
        errorval = 0;
        
        // 5.4. Fit a quadratic spline through this point, measure errors on every point in the sequence
        // helpers and projecting to get control point
        double t=(double)(fitpoint-seqstart)/tl, t1=(1.0-t)*(1.0-t), t2=2.0*(1.0-t)*t, t3=t*t;
        double cpx = (t1*startCoord[0] + t3*endCoord[0] - path.get(fitpoint)[0])/-t2;
        double cpy = (t1*startCoord[1] + t3*endCoord[1] - path.get(fitpoint)[1])/-t2;
        
        // Check every point
        pcnt = seqstart+1;
        while(pcnt != seqend){
            
            t=(double)(pcnt-seqstart)/tl; t1=(1.0-t)*(1.0-t); t2=2.0*(1.0-t)*t; t3=t*t;
            px = t1 * startCoord[0] + t2 * cpx + t3 * endCoord[0]; 
            py = t1 * startCoord[1] + t2 * cpy + t3 * endCoord[1];
            
            dist2 = (path.get(pcnt)[0]-px)*(path.get(pcnt)[0]-px) + (path.get(pcnt)[1]-py)*(path.get(pcnt)[1]-py);
            
            if(dist2>qtreshold){curvepass=false;}
            if(dist2>errorval){ errorpoint=pcnt; errorval=dist2; }
            pcnt = (pcnt+1)%pathlength;
        }
        
        // return spline if fits
        if(curvepass){
            segment.add(new double[7]);
            thissegment = segment.get(segment.size()-1);
            thissegment[0] = 2.0;
            thissegment[1] = startCoord[0];
            thissegment[2] = startCoord[1];
            thissegment[3] = cpx;
            thissegment[4] = cpy;
            thissegment[5] = endCoord[0];
            thissegment[6] = endCoord[1];
            return segment; 
        }
        
        // 5.5. If the spline fails (an error>qtreshold), find the point with the biggest error,
        // set splitpoint = (fitting point + errorpoint)/2
        int splitpoint = (int)((fitpoint + errorpoint)/2);
        
        // 5.6. Split sequence and recursively apply 5.2. - 5.6. to startpoint-splitpoint and splitpoint-endpoint sequences
        segment = fitseq(path,ltreshold,qtreshold,seqstart,splitpoint);
        segment.addAll(fitseq(path,ltreshold,qtreshold,splitpoint,seqend));
        return segment;
    }
    
}
