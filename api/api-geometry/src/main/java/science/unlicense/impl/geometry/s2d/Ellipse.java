
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;

/**
 * A Ellipse.
 *
 * Specification :
 * - SVG v1.1:9.4 :
 *   The ‘ellipse’ element defines an ellipse which is axis-aligned with the
 *   current user coordinate system based on a center point and two radii.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class Ellipse extends AbstractGeometry2D {

    private TupleRW center;
    private double rx;
    private double ry;

    public Ellipse() {
        this.center = new DefaultTuple(0, 0);
    }

    public Ellipse(final double cx, final double cy,
                     final double rx, final double ry) {
        this.center = new DefaultTuple(cx, cy);
        this.rx = rx;
        this.ry = ry;
    }

    /**
     * The ellipse center.
     */
    public Tuple getCenter() {
        return center;
    }

    public double getCenterX() {
        return center.get(0);
    }

    public double getCenterY() {
        return center.get(1);
    }

    public void setCenter(final TupleRW center) {
        this.center = center;
    }

    public void setCenter(final double cx, final double cy) {
        this.center = new DefaultTuple(cx, cy);
    }

    public void setCenterX(double cx) {
        this.center.set(0, cx);
    }

    public void setCenterY(double cy) {
        this.center.set(1, cy);
    }

    /**
     * The x-axis radius of the ellipse.
     */
    public double getRadiusX() {
        return rx;
    }

    public void setRadiusX(double rx) {
        this.rx = rx;
    }

    /**
     * The y-axis radius of the ellipse.
     */
    public double getRadiusY() {
        return ry;
    }

    public void setRadiusY(double ry) {
        this.ry = ry;
    }
    
    // ===== Commun ============================================================
    
    /**
     * Calculate the ellipse surface area.
     *
     * @return ellipse area.
     */
    public double getArea() {
        return Maths.PI*this.rx*this.ry;
    }
    
    /**
     * Calculate circle circumference.
     *
     * @return circle circumference
     */
    public double getLength() {
        return Maths.PI*(this.rx+this.ry);
    }
    
    /**
     * Ellipse bounding box is a box centered on ellipse center with a width of 
     * twice the radiusX on x axis, and twice the radiusY on y axis.
     *
     * @return BoundingBox
     */
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(2);
        bbox.setRange(0, center.getX()-this.rx, center.getX()+this.rx);
        bbox.setRange(1, center.getY()-this.ry, center.getY()+this.ry);
        return bbox;
    }

    /**
     * Bounding circle is a copy of this circle.
     *
     * @return Circle
     */
    public Circle getBoundingCircle() {
        return new Circle(center.copy(),Maths.max(this.rx, this.ry));
    }
    
    // Polygon getConvexhull() method defined in AbstractGeometry2D abstract class
    
    // Geometry buffer(double distance) method  method defined in AbstractGeometry abstract class

    /**
     * Centroid is ellipse center.
     *
     * @return Point
     */
    public Point getCentroid() {
        return new Point(center);
    }
    
    // =========================================================================

    public PathIterator createPathIterator() {
        final Path path = new Path();
        final double cx = center.getX();
        final double cy = center.getY();
        final double maxX = cx+rx;
        final double maxY = cy+ry;

        //calculate control point positions
        final double k = 4d * (Math.sqrt(2)-1d) / 3d;
        final double offsetX = rx*k;
        final double offsetY = ry*k;

        path.appendMoveTo(cx-rx, cy);
        path.appendCubicTo(cx-rx,      cy-offsetY, cx-offsetX, cy-ry,      cx,    cy-ry);
        path.appendCubicTo(cx+offsetX, cy-ry,      maxX,       cy-offsetY, maxX,  cy   );
        path.appendCubicTo(maxX,       cy+offsetY, cx+offsetX, maxY,       cx,    maxY );
        path.appendCubicTo(cx-offsetX, maxY,       cx-rx,      cy+offsetY, cx-rx, cy   );
        return path.createPathIterator();
    }

}
