
package science.unlicense.impl.geometry.path;

import science.unlicense.api.math.Tuple;

/**
 * Path iterator over a coordinate array.
 * @author Johann Sorel
 */
public class CoordinateTuplePathIterator extends AbstractStepPathIterator{

    private final Tuple[] coords;
    private final boolean close;
    private int index = 0;
    
    public CoordinateTuplePathIterator(Tuple[] tuples) {
        this(tuples,false);
    }
       
    public CoordinateTuplePathIterator(Tuple[] coords,boolean close) {
        this.coords = coords;
        this.close = close;
    }
        
    public void reset() {
        index = 0;
    }

    public boolean next() {
        if(index==coords.length && close){
            currentStep = new PathStep2D(TYPE_CLOSE);
            index++;
            return true;
        }
        if(index>=coords.length){
            return false;
        }
        
        final int type = (index==0) ? PathIterator.TYPE_MOVE_TO : PathIterator.TYPE_LINE_TO;
        currentStep = new PathStep2D(type, coords[index].get(0), coords[index].get(1));
        
        index++;
        return true;
    }
    
}
