
package science.unlicense.impl.geometry;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.AbstractGeometry2D;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.path.CoordinateTuplePathIterator;

/**
 * A Point.
 *
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Point
 *   An ST_Point value is a 0-dimensional geometry and represents a single location.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class Point extends AbstractGeometry2D {

    private TupleRW coord;

    public Point() {
        this(2);
    }

    public Point(int dimension) {
        this(new DefaultTuple(dimension));
    }

    public Point(double[] coords){
        this(new DefaultTuple(coords));
    }

    public Point(double x, double y){
        this(new DefaultTuple(x, y));
    }

    public Point(double x, double y, double z){
        this(new DefaultTuple(x, y, z));
    }

    public Point(double x, double y, double z, double w){
        this(new DefaultTuple(x, y, z, w));
    }

    public Point(TupleRW coord) {
        this.coord = coord;
    }

    public int getDimension() {
        return coord.getSize();
    }

    public TupleRW getCoordinate() {
        return coord;
    }

    public double getX() {
        return coord.get(0);
    }

    public double getY() {
        return coord.get(1);
    }

    public double getZ() {
        return coord.get(2);
    }

    public double getW() {
        return coord.get(3);
    }

    public void setCoordinate(final TupleRW center) {
        this.coord = center;
    }

    public void setCoordinate(final double cx, final double cy) {
        this.coord = new DefaultTuple(cx, cy);
    }

    public void setX(double cx) {
        this.coord.set(0, cx);
    }

    public void setY(double cy) {
        this.coord.set(1, cy);
    }

    public void setZ(double cz) {
        this.coord.set(2, cz);
    }

    public void setW(double cw) {
        this.coord.set(3, cw);
    }
    
    // ===== Commun ============================================================
    
    /**
     * Calculate the line surface area.
     *
     * @return circle area.
     */
    public double getArea() {
        return 0.;
    }

    /**
     * Calculate line length.
     *
     * @return circle circumference
     */
    public double getLength() {
        return 0.;
    }
    
    /**
     * Line bounding box is a box which has as corners both start and end tuples
     * with a width of twice the radius.
     *
     * @return BoundingBox
     */
    public BBox getBoundingBox() {
        final int dimension = this.getDimension();
        final BBox bbox = new BBox( dimension );
        if( dimension > 0 ) bbox.setRange( 0, this.getX(), this.getX() );
        if( dimension > 1 ) bbox.setRange( 1, this.getY(), this.getY() );
        if( dimension > 2 ) bbox.setRange( 2, this.getZ(), this.getZ() );
        if( dimension > 3 ) bbox.setRange( 3, this.getW(), this.getW() );
        return bbox;
    }

    /**
     * Bounding circle.
     * half-length of the line.
     *
     * @return Circle
     */
    public Circle getBoundingCircle() {
        TupleRW center = new DefaultTuple( this.getX(), this.getY() );
        return new Circle(center, 0.);
    }
    
    // Polygon getConvexhull() method defined in AbstractGeometry2D abstract class
    
    // Geometry buffer(double distance) method  method defined in AbstractGeometry abstract class
    
    /**
     * Centroid is circle center.
     *
     * @return Point
     */
    public Point getCentroid() {
        return new Point( this.coord );
    }
    
    // =========================================================================
    
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point other = (Point) obj;
        if (this.coord != other.coord && (this.coord == null || !this.coord.equals(other.coord))) {
            return false;
        }
        return true;
    }
    
    public PathIterator createPathIterator() {
        return new CoordinateTuplePathIterator(new Tuple[]{coord});
    }

}
