
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.Sequence;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_MultiSurface
 *   The elements of an ST_MultiSurface value are restricted to ST_Surface values.
 *
 * @author Johann Sorel
 */
public class MultiSurface extends MultiGeometry{
    
    public MultiSurface() {
    }

    public MultiSurface(Sequence geometries) {
        super(geometries);
    }
}
