
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.path.CoordinateTuplePathIterator;
import science.unlicense.impl.math.Vectors;

/**
 * A Line segment .
 * 
 * Specification :
 * - SVG v1.1:9.5 :
 *   The ‘line’ element defines a line segment that starts at one point and ends at another.
 *
 * @author Johann Sorel
 * @author Bertrand COTE
 */
public class Segment extends AbstractGeometry2D {

    private TupleRW start;
    private TupleRW end;

    public Segment() {
        this(2);
    }
    
    public Segment(int dimension) {
        this.start = new DefaultTuple(dimension);
        this.end = new DefaultTuple(dimension);
    }
    
    public Segment(TupleRW start, TupleRW end) {
        this.start = start;
        this.end = end;
    }

    public Segment(final double x1, final double y1,
                  final double x2, final double y2) {
        this.start = new DefaultTuple(x1,y1);
        this.end = new DefaultTuple(x2,y2);
    }

    public int getDimension() {
        return start.getSize();
    }
    
    public TupleRW getStart() {
        return start;
    }

    public void setStart(TupleRW start) {
        this.start = start;
    }

    /**
     * The x-axis coordinate of the start of the line.
     */
    public double getStartX() {
        return start.get(0);
    }

    public void setStartX(double x1) {
        start.set(0, x1);
    }

    /**
     * The y-axis coordinate of the start of the line.
     */
    public double getStartY() {
        return start.get(1);
    }

    public void setStartY(double y1) {
        start.set(1, y1);
    }

    public TupleRW getEnd() {
        return end;
    }

    public void setEnd(TupleRW end) {
        this.end = end;
    }

    /**
     * The x-axis coordinate of the end of the line.
     */
    public double getEndX() {
        return end.get(0);
    }

    public void setEndX(double x2) {
        end.set(0, x2);
    }

    /**
     * The y-axis coordinate of the end of the line.
     */
    public double getEndY() {
        return end.get(1);
    }

    public void setEndY(double y2) {
        end.set(1, y2);
    }
    
    // ===== Commun ============================================================
    
    /**
     * Calculate the line surface area.
     *
     * @return 0.
     */
    public double getArea() {
        return 0.;
    }

    /**
     * Calculate line length.
     *
     * @return the line's length.
     */
    public double getLength() {
        return Vectors.length(Vectors.subtract(this.end.getValues(), this.start.getValues()));
    }

    /**
     * Line bounding box is a box which has as corners both start and end tuples
     * with a width of twice the radius.
     *
     * @return BoundingBox
     */
    public BBox getBoundingBox() {
        final BBox bbox = new BBox(2);
        bbox.setRange( 0, 
                Maths.min(this.start.getX(), this.end.getX()), 
                Maths.max(this.start.getX(), this.end.getX()));
        bbox.setRange( 1, 
                Maths.min(this.start.getY(), this.end.getY()), 
                Maths.max(this.start.getY(), this.end.getY()));
        return bbox;
    }

    /**
     * Bounding circle is centered on the middle of the line and has as radius 
     * half-length of the line.
     *
     * @return
     */
    public Circle getBoundingCircle() {
        Tuple lineVector = new DefaultTuple( Vectors.subtract(this.end.getValues(), this.start.getValues()));
        TupleRW center = new DefaultTuple( Vectors.add( this.start.getValues(), Vectors.scale(lineVector.getValues(), 0.5)));
        double radius = Vectors.length(lineVector.getValues())/2.;
        return new Circle(center,radius);
    }
    
    // Polygon getConvexhull() method defined in AbstractGeometry2D abstract class
    
    // Geometry buffer(double distance) method  method defined in AbstractGeometry abstract class

    /**
     * Centroid is line's center.
     *
     * @return Point
     */
    public Point getCentroid() {
        Tuple lineVector = new DefaultTuple( Vectors.subtract(this.end.getValues(), this.start.getValues()));
        TupleRW center = new DefaultTuple( Vectors.add( this.start.getValues(), Vectors.scale(lineVector.getValues(), 0.5)));
        return new Point(center);
    }
    
    // =========================================================================

    @Override
    public PathIterator createPathIterator() {
        return new CoordinateTuplePathIterator(new Tuple[]{start,end});
    }

}
