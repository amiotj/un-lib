
package science.unlicense.impl.geometry.s2d;

import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.PathStep2D;
import science.unlicense.impl.geometry.path.AbstractStepPathIterator;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * Geometry defined by a custom path.
 * Specification :
 * - SVG : Path
 * - PostScript : Path
 * - WKT/WKB ISO 13249-3 : ST_CompoundCurve
 *   The general notion of a compound curve is a sequence of contiguous curves 
 *   such that adjacent curves are joined at their end points
 *
 * @author Johann Sorel
 */
public class Path extends AbstractGeometry2D implements Curve{

    private final Sequence steps = new ArraySequence();

    public Path() {
    }

    /**
     * Get path steps.
     * @return Sequence of PathStep2D
     */
    public Sequence getSteps() {
        return steps;
    }

    /**
     * Copy path iteration. 
     * @param ite 
     */
    public void append(PathIterator ite){
        while(ite.next()){
            steps.add(new PathStep2D(ite));
        }
    }
    
    public void append(PathStep2D step){
        steps.add(step);
    }
    
    public void append(int type, double rx, double ry, double xrot, boolean largeArcFlag, boolean sweepFlag, double x, double y){
        steps.add(new PathStep2D(type, rx, ry, xrot, largeArcFlag, sweepFlag, x, y));
    }
    
    public void appendMoveTo(double x, double y){
        steps.add(new PathStep2D(PathIterator.TYPE_MOVE_TO,x,y));
    }
    
    /**
     * Append 'move to' step relative to previous position.
     * If there is no previous coordinate, dx,dy values are considered as absolute.
     * 
     * @param dx delta X relative to previous coordinate.
     * @param dy delta Y relative to previous coordinate.
     */
    public void appendMoveToRelative(double dx, double dy){
        if(steps.isEmpty()){
            appendMoveTo(dx, dy);
        }else{
            final PathStep2D lastStep = (PathStep2D) steps.get(steps.getSize()-1);
            appendMoveTo(lastStep.values[0] + dx, lastStep.values[1] + dy);
        }
    }
    
    public void appendLineTo(double x, double y){
        steps.add(new PathStep2D(PathIterator.TYPE_LINE_TO,x,y));
    }
    
    /**
     * Append 'line to' step relative to previous position.
     * If there is no previous coordinate, dx,dy values are considered as absolute.
     * 
     * @param dx delta X relative to previous coordinate.
     * @param dy delta Y relative to previous coordinate.
     */
    public void appendLineToRelative(double dx, double dy){
        if(steps.isEmpty()){
            appendLineTo(dx, dy);
        }else{
            final PathStep2D lastStep = (PathStep2D) steps.get(steps.getSize()-1);
            appendLineTo(lastStep.values[0] + dx, lastStep.values[1] + dy);
        }
    }

    public void appendCubicTo(double cx1, double cy1, double cx2, double cy2, double x, double y){
        steps.add(new PathStep2D(PathIterator.TYPE_CUBIC,cx1,cy1,cx2,cy2,x,y));
    }

    /**
     * Append 'cubic to' step relative to previous position.
     * If there is no previous coordinate, dcx1,dcy1 values are considered as absolute.
     * 
     * @param dcx1 first control point X relative to previous coordinate.
     * @param dcy1 first control point Y relative to previous coordinate.
     * @param dcx2 second control point X relative to dcx1.
     * @param dcy2 second control point Y relative to dcx1.
     * @param dx delta X relative to dcx2.
     * @param dy delta Y relative to dcy2.
     */
    public void appendCubicToRelative(double dcx1, double dcy1, double dcx2, double dcy2, double dx, double dy){
        double lastX,lastY;
        if(steps.isEmpty()){
            lastX = lastY = 0;
        }else{
            final PathStep2D lastStep = (PathStep2D) steps.get(steps.getSize()-1);
            lastX = lastStep.values[0];
            lastY = lastStep.values[1];
        }
        
        appendCubicTo(
                lastX + dcx1, 
                lastY + dcy1,
                lastX + dcx1 + dcx2, 
                lastY + dcy1 + dcy2,
                lastX + dcx1 + dcx2 + dx, 
                lastY + dcy1 + dcy2 + dy
        );
    }
    
    public void appendQuadTo(double cx, double cy, double x, double y){
        steps.add(new PathStep2D(PathIterator.TYPE_QUADRATIC,cx,cy,x,y));
    }
    
    /**
     * Append 'quad to' step relative to previous position.
     * If there is no previous coordinate, dcx,dcy values are considered as absolute.
     * 
     * @param dcx control point X relative to previous coordinate.
     * @param dcy control point Y relative to previous coordinate.
     * @param dx delta X relative to dcx.
     * @param dy delta Y relative to dcy.
     */
    public void appendQuadToRelative(double dcx, double dcy, double dx, double dy){
        double lastX,lastY;
        if(steps.isEmpty()){
            lastX = lastY = 0;
        }else{
            final PathStep2D lastStep = (PathStep2D) steps.get(steps.getSize()-1);
            lastX = lastStep.values[0];
            lastY = lastStep.values[1];
        }
        appendQuadTo(
                lastX + dcx, 
                lastY + dcy,
                lastX + dcx + dx, 
                lastY + dcy + dy);
    }

    public void appendArcTo(double rx, double ry, double xrot,
            boolean largeArcFlag, boolean sweepFlag, double x, double y){
        steps.add(new PathStep2D(PathIterator.TYPE_ARC,rx,ry,xrot,largeArcFlag,sweepFlag,x,y));
    }

    public void appendClose(){
        steps.add(new PathStep2D(PathIterator.TYPE_CLOSE));
    }
    
    public Path copy(){
        final Path path = new Path();
        path.steps.addAll(steps);
        return path;
    }
    
    public PathIterator createPathIterator() {
        return new Path2DIterator();
    }

    private class Path2DIterator extends AbstractStepPathIterator{

        private int index = -1;
        
        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            boolean hasNext = index < steps.getSize() ;
            if(hasNext){
                currentStep = (PathStep2D)steps.get(index);
            }else{
                currentStep = null;
            }
            return hasNext;
        }

    }
    
}
