
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.geometry.operation.Operation;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Circle;

/**
 *
 * @author Johann Sorel
 */
public class BufferExecutors {

    private BufferExecutors(){}

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(Buffer.class, Point.class){

        public Object execute(Operation operation) {
            final Buffer op = (Buffer) operation;
            final Point p = (Point) op.getGeometry();
            final double distance = op.getDistance();
            if(distance <=0){
                return new Point(p.getCoordinate().copy());
            }else{
                return new Circle(p.getCoordinate().copy(), distance);
            }
        }
    };

    public static final AbstractSingleOperationExecutor CIRCLE =
            new AbstractSingleOperationExecutor(Buffer.class, Circle.class){

        public Object execute(Operation operation) {
            final Buffer op = (Buffer) operation;
            final Circle p = (Circle) op.getGeometry();
            double distance = op.getDistance();
            distance = p.getRadius()+distance;
            if(distance <=0){
                return new Point(p.getCenter().copy());
            }else{
                return new Circle(p.getCenter().copy(), distance);
            }
        }
    };

}
