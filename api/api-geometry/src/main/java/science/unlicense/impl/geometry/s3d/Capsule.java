
package science.unlicense.impl.geometry.s3d;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.AbstractOrientedGeometry;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * A Capsule is a cylinder with half spheres at each end.
 * It can be described as a buffer around a line segment.
 * Also called sphere-swept line.
 * The capsule rotates around the Y axis.
 *
 * @author Johann Sorel
 */
public class Capsule extends AbstractOrientedGeometry implements Geometry3D {

    private double height;
    private double radius;

    public Capsule(double height, double radius) {
        super(3);
        this.height = height;
        this.radius = radius;
    }

    public Capsule(Tuple first, Tuple second, double radius) {
        super(3);
        final Vector temp = new Vector(second);
        temp.localSubtract(first);

        this.height = temp.length();
        this.radius = radius;

        //calculate rotation
        transform.getRotation().set(Matrix3x3.createRotation(new Vector(0, 1, 0), temp));
        //calculate translation
        temp.set(second);
        temp.localAdd(first);
        temp.localScale(0.5);
        transform.getTranslation().set(temp);
        transform.notifyChanged();

    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Calculate the bottom point of the capsule axis.
     * Including orientation
     * @return capsule axis bottom point
     */
    public Vector getFirst(){
        final Vector v = new Vector(0, -height/2, 0);
        return (Vector) transform.transform(v, v);
    }

    /**
     * Calculate the top point of the capsule axis.
     * Including orientation
     * @return capsule axis top point
     */
    public Vector getSecond(){
        final Vector v = new Vector(0, +height/2, 0);
        return (Vector) transform.transform(v, v);
    }

    /**
     * Calculate the capsule volume.
     *
     * @return volume.
     */
    public double getVolume(){
        return Math.PI * (radius*radius) * ((4.0/3.0)*radius + height);
    }

    /**
     * Calculate the capsule surface area.
     *
     * @return area.
     */
    public double getSurface() {
        return 2 * Math.PI * radius * (2 * radius + height);
    }

    /**
     * Calculate the capsule circumference.
     *
     * @return circumference.
     */
    public double getCircumference(){
        return 2 * Math.PI * radius;
    }

    public Point getCentroid() {
        final Vector v = new Vector(0, 0, 0);
        return new Point(transform.transform(v, v));
    }
    
    public BBox getUnorientedBounds() {
        final BBox bbox = new BBox(3);
        bbox.getLower().setXYZ(-radius, -height/2 - radius, -radius);
        bbox.getUpper().setXYZ(radius, height/2 + radius, radius);
        return bbox;
    }

}
