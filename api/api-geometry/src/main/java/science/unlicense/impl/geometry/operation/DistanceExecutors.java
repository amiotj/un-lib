
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.OBBox;
import science.unlicense.impl.geometry.Point;
import static science.unlicense.impl.geometry.operation.Distance.distance;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Sheet;
import science.unlicense.impl.geometry.s3d.Sphere;

/**
 *
 * @author Johann Sorel
 */
public class DistanceExecutors {

    private DistanceExecutors(){}

    public static final AbstractBinaryOperationExecutor POINT_POINT =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point)first;
            final Point b = (Point)second;
            return distance(a.getCoordinate(), b.getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_CIRCLE =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, Circle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point)first;
            final Circle b = (Circle)second;
            return distance(a.getCoordinate(), b.getCenter()) - b.getRadius();
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_LINE =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, Segment.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point)first;
            final Segment b = (Segment)second;
            return distance(b.getStart(), b.getEnd(), a.getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_RECTANGLE =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, Rectangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Geometry[] nearest = (Geometry[]) NearestExecutors.POINT_RECTANGLE.execute(op, first, second);
            return distance(((Point)nearest[0]).getCoordinate(), ((Point)nearest[1]).getCoordinate());
        }
    };
    
    public static final AbstractBinaryOperationExecutor POINT_BBOX =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, BBox.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Geometry[] nearest = (Geometry[]) NearestExecutors.POINT_BBOX.execute(op, first, second);
            return distance(((Point)nearest[0]).getCoordinate(), ((Point)nearest[1]).getCoordinate());
        }
    };
    
    public static final AbstractBinaryOperationExecutor POINT_OBBOX =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, OBBox.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Geometry[] nearest = (Geometry[]) NearestExecutors.POINT_OBBOX.execute(op, first, second);
            return distance(((Point)nearest[0]).getCoordinate(), ((Point)nearest[1]).getCoordinate());
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_PLANE =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point)first;
            final Plane b = (Plane)second;
            final double d = Distance.distance(a.getCoordinate(), b);
            //negative ditances are within the plan
            return (d<0) ? 0 : d;
        }
    };

    public static final AbstractBinaryOperationExecutor POINT_SHEET =
            new AbstractBinaryOperationExecutor(Distance.class, Point.class, Sheet.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point)first;
            final Sheet b = (Sheet)second;
            return Math.abs(Distance.distance(a.getCoordinate(), b));
        }
    };

    public static final AbstractBinaryOperationExecutor LINE_LINE =
            new AbstractBinaryOperationExecutor(Distance.class, Segment.class, Segment.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Segment line1 = (Segment)first;
            final Segment line2 = (Segment)second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[line1.getStart().getSize()];
            final double[] c2 = new double[line1.getStart().getSize()];
            final double dist = distance(
                                    line1.getStart().getValues(), line1.getEnd().getValues(), c1,
                                    line2.getStart().getValues(), line2.getEnd().getValues(), c2,
                                    ratio,op.epsilon);
            return dist;
        }
    };

    public static final AbstractBinaryOperationExecutor CIRCLE_CIRCLE =
            new AbstractBinaryOperationExecutor(Distance.class, Circle.class, Circle.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Circle a = (Circle)first;
            final Circle b = (Circle)second;
            return distance(a.getCenter(), b.getCenter()) - a.getRadius() - b.getRadius();
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_SPHERE =
            new AbstractBinaryOperationExecutor(Distance.class, Sphere.class, Sphere.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere s1 = (Sphere)first;
            final Sphere s2 = (Sphere)second;
            // distance between sphere center
            final double dist = distance(s1.getCenter(), s2.getCenter());

            // sum of radius
            final double rs = s1.getRadius() + s2.getRadius();
            return dist - rs;
        }
    };

    public static final AbstractBinaryOperationExecutor SPHERE_PLANE =
            new AbstractBinaryOperationExecutor(Distance.class, Sphere.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Sphere a = (Sphere)first;
            final Plane b = (Plane)second;
            final double d = Distance.distance(a.getCenter(), b);
            final double radius = a.getRadius();
            //TODO how should we consider distance when sphere under ?
            //total penetration ? from closest border to plan ?
            return d - radius;
        }
    };

    public static final AbstractBinaryOperationExecutor CAPSULE_CAPSULE =
            new AbstractBinaryOperationExecutor(Distance.class, Capsule.class, Capsule.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Capsule capsule1 = (Capsule)first;
            final Capsule capsule2 = (Capsule)second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[capsule1.getFirst().getSize()];
            final double[] c2 = new double[capsule1.getFirst().getSize()];
            final double dist = distance(
                                    capsule1.getFirst().getValues(), capsule1.getSecond().getValues(), c1,
                                    capsule2.getFirst().getValues(), capsule2.getSecond().getValues(), c2,
                                    ratio,op.epsilon);
            return dist - capsule1.getRadius() - capsule2.getRadius();
        }
    };

}