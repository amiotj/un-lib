
package science.unlicense.impl.geometry;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 * Abstract oriented geometry.
 *
 * @author Johann Sorel
 */
public abstract class AbstractOrientedGeometry extends AbstractGeometry implements OrientedGeometry{

    private final int dimension;
    protected final NodeTransform transform;

    public AbstractOrientedGeometry(int dimension) {
        this.dimension = dimension;
        this.transform = new NodeTransform(dimension);
    }

    public NodeTransform getTransform() {
        return transform;
    }

    public int getDimension() {
        return dimension;
    }

    public BBox getBoundingBox() {
        final BBox bbox = getUnorientedBounds();
        //TODO
        throw new RuntimeException("Not implemented yet.");
    }

}
