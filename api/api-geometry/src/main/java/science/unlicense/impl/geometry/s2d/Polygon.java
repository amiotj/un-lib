
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 * A Polygon.
 *
 * Specification :
 *  - SVG v1.1:9.7 :
 *   The ‘polygon’ element defines a closed shape consisting
 *   of a set of connected straight line segments.
 * - WKT/WKB ISO 13249-3 : ST_Polygon
 *   The ST_Polygon type is a subtype of ST_CurvePolygon whose boundary
 *   is defined by linear rings
 *
 * @author Johann Sorel
 */
public class Polygon extends CurvePolygon {

    public Polygon(Polyline outer, Sequence inners) {
        super(outer, inners);
        if(!outer.isClosed()){
            throw new InvalidArgumentException("Outter polyline must be closed");
        }
    }

    public Polyline getExterior() {
        return (Polyline) outer;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Polygon other = (Polygon) obj;
        if (this.outer != other.outer && (this.outer == null || !this.outer.equals(other.outer))) {
            return false;
        }
        if (!CObjects.equals(this.inners,other.inners)) {
            return false;
        }
        return true;
    }

}
