package science.unlicense.impl.geometry.triangulate;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.operation.Distance;

/**
 * Incremental Delaunay Triangulation
 * <p>
 * http://www.developpez.net/forums/d485702/autres-langages/algorithmes/contribuez/java-triangulation-delaunay-incrementale/
 *
 * @author Pseudo-code by Guibas and Stolfi
 * @author Java-code by X.Philippeau
 * @author Johann sorel (adapted to Unlicense project)
 * @author Izyumov Konstantin (code fix)
 * @see Primitives for the Manipulation of General Subdivisions and the
 * Computation of Voronoi Diagrams (Leonidas Guibas,Jorge Stolfi)
 * @see "Technical Report. Number 728". The University of Cambridge.
 * "A Robust Efficient Algorithm for Point Location in Triangulations"
 * February 1997
 * Peter J. C. Brown, Peter J. C. Brown
 * <p>
 * <p>
 * Picture bouding box:
 * d---------c
 * |         |
 * |  *****  |
 * |  *****  |
 * |  *****  |
 * |         |
 * a---------b
 * <p>
 * <p>
 * ____________*
 * e.Onext___/  \ e.Dprev
 * _________/    \
 * ________*======*
 * e.Org__________e.Dest
 */
public class Delaunay {

    // starting edge for walk (see locate() method)
    private QuadEdge startingEdge = null;
    // list of quadEdge belonging to Delaunay triangulation
    private final Sequence quadEdge = new ArraySequence();
    // list of points  to Delaunay triangulation
    private Sequence points = new ArraySequence();

    //    // Bounding box of the triangulation
    private class TriangulationBox {
        Point a = new Point(); // lower left
        Point b = new Point(); // lower right
        Point c = new Point(); // upper right
        Point d = new Point(); // upper left

        public void setBox(BBox bbox) {
            // extend the bounding-box to surround min/max
            final double factor = 1e5;
            final double centerx = bbox.getMiddle(0);
            final double centery = bbox.getMiddle(1);
            final double x_min = ((bbox.getMin(0) - centerx - 1) * factor + centerx);
            final double x_max = ((bbox.getMax(0) - centerx + 1) * factor + centerx);
            final double y_min = ((bbox.getMin(1) - centery - 1) * factor + centery);
            final double y_max = ((bbox.getMax(1) - centery + 1) * factor + centery);

            // set new positions
            a.setX(x_min);
            a.setY(y_min);
            b.setX(x_max);
            b.setY(y_min);
            c.setX(x_max);
            c.setY(y_max);
            d.setX(x_min);
            d.setY(y_max);
        }
    }

    private final TriangulationBox tBox = new TriangulationBox();
    private final BBox bbox = new BBox(2);

    public Delaunay() {
        bbox.setToReverseMax();
        // create the QuadEdge graph of the bounding box
        final QuadEdge ab = QuadEdge.makeEdge(tBox.a, tBox.b);
        final QuadEdge bc = QuadEdge.makeEdge(tBox.b, tBox.c);
        final QuadEdge cd = QuadEdge.makeEdge(tBox.c, tBox.d);
        final QuadEdge da = QuadEdge.makeEdge(tBox.d, tBox.a);
        QuadEdge.splice(ab.sym(), bc);
        QuadEdge.splice(bc.sym(), cd);
        QuadEdge.splice(cd.sym(), da);
        QuadEdge.splice(da.sym(), ab);

        this.startingEdge = ab;
    }

    /**
     * Returns an edge e of the triangle containing the point p (Guibas and Stolfi)
     *
     * @param p the point to localte
     * @return the edge of the triangle
     */
    private QuadEdge locate(Point p) {

        /* outside the bounding box ? */
        if (!bbox.intersects(p.getCoordinate(), false)) {
            // update the size of the bounding box (cf locate() method)
            bbox.expand(p.getCoordinate());
            tBox.setBox(bbox);
        }

        QuadEdge e = startingEdge;
        while (true) {
            /* duplicate point ? */
            if (p.getX() == e.orig().getX()
                    && p.getY() == e.orig().getY()) {
                return e;
            }
            if (p.getX() == e.dest().getX()
                    && p.getY() == e.dest().getY()) {
                return e;
            }

            /* walk  - A Robust Point Location Algorithm*/
            if (Geometries.isAtRightOf(e.orig(), e.dest(), p)) {
                e = e.sym();
            } else {
                int whichOp = 0;
                if (!Geometries.isAtRightOf(e.onext().orig(), e.onext().dest(), p)) {
                    whichOp += 1;
                }
                if (!Geometries.isAtRightOf(e.dprev().orig(), e.dprev().dest(), p)) {
                    whichOp += 2;
                }
                if (whichOp == 0) {
                    return e;
                } else if (whichOp == 1) {
                    e = e.onext();
                } else if (whichOp == 2) {
                    e = e.dprev();
                } else {
                    if (Distance.distanceLineAndPoint(e.onext().orig(), e.onext().dest(), p) <
                            Distance.distanceLineAndPoint(e.dprev().orig(), e.dprev().dest(), p)) {
                        e = e.onext();
                    } else {
                        e = e.dprev();
                    }
                }
            }
        }
    }

    /**
     * Inserts a new point into a Delaunay triangulation (Guibas and Stolfi)
     *
     * @param p the point to insert
     */
    public void insertPoint(Point p) {
        points.add(p);
        QuadEdge e = locate(p);

        // point is a duplicate -> nothing to do
        if (p.getX() == e.orig().getX()
                && p.getY() == e.orig().getY()) {
            return;
        }
        if (p.getX() == e.dest().getX()
                && p.getY() == e.dest().getY()) {
            return;
        }

        // point is on an existing edge -> remove the edge
        if (Geometries.isOnLine(e.orig(), e.dest(), p)) {
            e = e.oprev();
            this.quadEdge.remove(e.onext().sym());
            this.quadEdge.remove(e.onext());
            QuadEdge.deleteEdge(e.onext());
        }

        // Connect the new point to the vertices of the containing triangle
        // (or quadrilateral in case of the point is on an existing edge)
        QuadEdge base = QuadEdge.makeEdge(e.orig(), p);
        this.quadEdge.add(base);

        QuadEdge.splice(base, e);
        this.startingEdge = base;
        do {
            base = QuadEdge.connect(e, base.sym());
            this.quadEdge.add(base);
            e = base.oprev();
        } while (e.lnext() != startingEdge);

        // Examine suspect edges to ensure that the Delaunay condition is satisfied.
        do {
            QuadEdge t = e.oprev();

            if (Geometries.isAtRightOf(e.orig(), e.dest(), t.dest())
                    && Geometries.inCircle(e.orig(), t.dest(), e.dest(), p)) {
                // flip triangles
                QuadEdge.swapEdge(e);
                e = e.oprev();
            } else if (e.onext() == startingEdge) {
                return; // no more suspect edges
            } else {
                e = e.onext().lprev();  // next suspect edge
            }
        } while (true);
    }

    /**
     * compute and return the list of edges
     */
    public Sequence computeEdges() {
        final Sequence edges = new ArraySequence();
        // do not return edges pointing to/from surrouding triangle
        for (int i = 0, n = quadEdge.getSize(); i < n; i++) {
            final QuadEdge q = (QuadEdge) quadEdge.get(i);
            if (q.orig() == tBox.a || q.orig() == tBox.b || q.orig() == tBox.c || q.orig() == tBox.d) {
                continue;
            }
            if (q.dest() == tBox.a || q.dest() == tBox.b || q.dest() == tBox.c || q.dest() == tBox.d) {
                continue;
            }
            edges.add(new Point[]{q.orig(), q.dest()});
        }
        return edges;
    }

    /**
     * compute and return the list of triangles
     */
    public Sequence computeTriangles() {
        final Sequence triangles = new ArraySequence();

        // do not process edges pointing to/from surrouding triangle
        // --> mark them as already computed
        for (int i = 0, n = quadEdge.getSize(); i < n; i++) {
            final QuadEdge q = (QuadEdge) quadEdge.get(i);
            q.mark = false;
            q.sym().mark = false;
            if (q.orig() == tBox.a || q.orig() == tBox.b || q.orig() == tBox.c || q.orig() == tBox.d) {
                q.mark = true;
            }
            if (q.dest() == tBox.a || q.dest() == tBox.b || q.dest() == tBox.c || q.dest() == tBox.d) {
                q.sym().mark = true;
            }
        }

        // compute the 2 triangles associated to each quadEdge
        for (int i = 0, n = quadEdge.getSize(); i < n; i++) {
            final QuadEdge qe = (QuadEdge) quadEdge.get(i);
            // first triangle
            final QuadEdge q1 = qe;
            final QuadEdge q2 = q1.lnext();
            final QuadEdge q3 = q2.lnext();
            if (!q1.mark && !q2.mark && !q3.mark) {
                triangles.add(new Point[]{q1.orig(), q2.orig(), q3.orig()});
            }

            // second triangle
            final QuadEdge qsym1 = qe.sym();
            final QuadEdge qsym2 = qsym1.lnext();
            final QuadEdge qsym3 = qsym2.lnext();
            if (!qsym1.mark && !qsym2.mark && !qsym3.mark) {
                triangles.add(new Point[]{qsym1.orig(), qsym2.orig(), qsym3.orig()});
            }

            // mark as used
            qe.mark = true;
            qe.sym().mark = true;
        }

        return triangles;
    }

    public Sequence computeVoronoi() {
        final Sequence voronoi = new ArraySequence();

        // do not process edges pointing to/from surrouding triangle
        // --> mark them as already computed
        for (int i = 0, n = quadEdge.getSize(); i < n; i++) {
            final QuadEdge q = (QuadEdge) quadEdge.get(i);
            q.mark = false;
            q.sym().mark = false;
            if (q.orig() == tBox.a || q.orig() == tBox.b || q.orig() == tBox.c || q.orig() == tBox.d) {
                q.mark = true;
            }
            if (q.dest() == tBox.a || q.dest() == tBox.b || q.dest() == tBox.c || q.dest() == tBox.d) {
                q.sym().mark = true;
            }
        }

        for (int i = 0, n = quadEdge.getSize(); i < n; i++) {
            final QuadEdge qe = (QuadEdge) quadEdge.get(i);

            // walk throught left and right region
            for (int b = 0; b <= 1; b++) {
                QuadEdge qstart = (b == 0) ? qe : qe.sym();
                if (qstart.mark) {
                    continue;
                }

                // new region start
                final Sequence poly = new ArraySequence();

                // walk around region
                QuadEdge qregion = qstart;
                while (true) {
                    qregion.mark = true;

                    // compute CircumCenter if needed
                    if (qregion.rot().orig() == null) {
                        final QuadEdge q1 = qregion;
                        final Point p0 = q1.orig();
                        final QuadEdge q2 = q1.lnext();
                        final Point p1 = q2.orig();
                        final QuadEdge q3 = q2.lnext();
                        final Point p2 = q3.orig();

                        final double ex = p1.getX() - p0.getX(), ey = p1.getY() - p0.getY();
                        final double nx = p2.getY() - p1.getY(), ny = p1.getX() - p2.getX();
                        final double dx = (p0.getX() - p2.getX()) * 0.5, dy = (p0.getY() - p2.getY()) * 0.5;
                        final double s = (ex * dx + ey * dy) / (ex * nx + ey * ny);
                        final double cx = (p1.getX() + p2.getX()) * 0.5 + s * nx;
                        final double cy = (p1.getY() + p2.getY()) * 0.5 + s * ny;

                        final Point p = new Point(cx, cy);
                        qregion.rot().setOrig(p);
                    }

                    poly.add(qregion.rot().orig());

                    qregion = qregion.onext();
                    if (qregion == qstart) {
                        break;
                    }
                }

                // add region to output list
                Point[] v = new Point[poly.getSize()];
                Collections.copy(poly, new Point[poly.getSize()], 0);
                voronoi.add(v);
            }
        }
        return voronoi;
    }

    public Sequence getPoints() {
        return points;
    }
}