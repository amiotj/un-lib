

package science.unlicense.impl.geometry;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.math.Vector;

/**
 * Oriented BoundingBox.
 * 
 * @author Johann Sorel
 */
public class OBBox extends AbstractOrientedGeometry {

    private final TupleRW upper;
    private final TupleRW lower;
    
    public OBBox(int dimension) {
        super(dimension);
        lower = new Vector(dimension);
        upper = new Vector(dimension);
    }
    
    public OBBox(TupleRW min, TupleRW max) {
        super(min.getSize());
        this.lower = min;
        this.upper = max;
    }
    
    public OBBox(double[] min, double[] max) {
        super(min.length);
        this.lower = new Vector(min);
        this.upper = new Vector(max);
    }
    
    public TupleRW getUpper() {
        return upper;
    }

    public TupleRW getLower() {
        return lower;
    }
    
    public Tuple getMiddle() {
        return new Vector(upper).localAdd(lower).localScale(0.5);
    }

    public double getMin(int ordinate){
        return lower.get(ordinate);
    }

    public double getMiddle(int ordinate){
        return (upper.get(ordinate) + lower.get(ordinate)) / 2;
    }

    public double getMax(int ordinate){
        return upper.get(ordinate);
    }

    public double getSpan(int ordinate){
        return getMax(ordinate)-getMin(ordinate);
    }
    
    public BBox getUnorientedBounds() {
        return new BBox(lower.copy(), upper.copy());
    }
    
}
