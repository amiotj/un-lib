
package science.unlicense.impl.geometry.s2d;

import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.PathStep2D;
import science.unlicense.impl.geometry.path.AbstractStepPathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.TupleRW;

/**
 * Single segment cubic curve.
 * 
 * @author Johann Sorel
 */
public class CubicCurve extends AbstractGeometry2D {

    private TupleRW start;
    private TupleRW control1;
    private TupleRW control2;
    private TupleRW end;

    public CubicCurve(double startx, double starty,
               double cx1, double cy1,
               double cx2, double cy2,
               double endx, double endy) {
        this.start = new DefaultTuple(startx, starty);
        this.control1 = new DefaultTuple(cx1, cy1);
        this.control2 = new DefaultTuple(cx2, cy2);
        this.end = new DefaultTuple(endx, endy);
    }


    public TupleRW getStart() {
        return start;
    }

    public void setStart(TupleRW start) {
        this.start = start;
    }

    /**
     * The x-axis coordinate of the start of the curve.
     */
    public double getStartX() {
        return start.get(0);
    }

    public void setStartX(double x1) {
        start.set(0, x1);
    }

    /**
     * The y-axis coordinate of the start of the curve.
     */
    public double getStartY() {
        return start.get(1);
    }

    public void setStartY(double y1) {
        start.set(1, y1);
    }

    public TupleRW getControl1() {
        return control1;
    }

    public void setControl1(TupleRW control) {
        this.control1 = control;
    }

    /**
     * The x-axis coordinate of the first control point of the curve.
     */
    public double getControl1X() {
        return control1.get(0);
    }

    public void setControl1X(double x2) {
        control1.set(0, x2);
    }

    /**
     * The y-axis coordinate of the first control point of the line.
     */
    public double getControl1Y() {
        return control1.get(1);
    }

    public void setControl1Y(double y2) {
        control1.set(1, y2);
    }

    public TupleRW getControl2() {
        return control2;
    }

    public void setControl2(TupleRW control) {
        this.control2 = control;
    }

    /**
     * The x-axis coordinate of the second control point of the curve.
     */
    public double getControl2X() {
        return control2.get(0);
    }

    public void setControl2X(double x2) {
        control2.set(0, x2);
    }

    /**
     * The y-axis coordinate of the second control point of the line.
     */
    public double getControl2Y() {
        return control2.get(1);
    }

    public void setControl2Y(double y2) {
        control2.set(1, y2);
    }

    public TupleRW getEnd() {
        return end;
    }

    public void setEnd(TupleRW end) {
        this.end = end;
    }

    /**
     * The x-axis coordinate of the end of the curve.
     */
    public double getEndX() {
        return end.get(0);
    }

    public void setEndX(double x2) {
        end.set(0, x2);
    }

    /**
     * The y-axis coordinate of the end of the curve.
     */
    public double getEndY() {
        return end.get(1);
    }

    public void setEndY(double y2) {
        end.set(1, y2);
    }

    @Override
    public PathIterator createPathIterator() {
        return new CubicCurve2DIterator();
    }

    private final class CubicCurve2DIterator extends AbstractStepPathIterator{

        private int index = -1;

        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            if(index == 0){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,start.get(0),start.get(1));
            }else if(index == 1){
                currentStep = new PathStep2D(PathIterator.TYPE_CUBIC,
                        control1.get(0),control1.get(1),
                        control2.get(0),control2.get(1),
                        end.get(0),end.get(1));
            }else{
                currentStep = null;
            }

            return currentStep != null;
        }

    }

}
