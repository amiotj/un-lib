
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.path.ConcatenatePathIterator;
import science.unlicense.impl.geometry.path.PathIterator;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_GeomCollection
 *   An ST_GeomCollection is a collection of zero or more ST_Geometry values
 * 
 * @author Johann Sorel
 */
public class MultiGeometry extends AbstractGeometry2D{
    
    private Sequence geometries;

    public MultiGeometry() {
    }

    public MultiGeometry(Sequence geometries) {
        this.geometries = geometries;
    }

    public Sequence getGeometries() {
        return geometries;
    }
    
    public PathIterator createPathIterator() {
        final Sequence s = new ArraySequence();
        if(geometries!=null){
            for(int i=0,n=geometries.getSize();i<n;i++){
                s.add( ((Geometry2D)geometries.get(i)).createPathIterator() );
            }
        }
        return new ConcatenatePathIterator(s);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MultiGeometry other = (MultiGeometry) obj;
        if (this.geometries != other.geometries && (this.geometries == null || !this.geometries.equals(other.geometries))) {
            return false;
        }
        return true;
    }
    
    
    
}
