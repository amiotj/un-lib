
package science.unlicense.impl.geometry.s2d;

/**
 * Specification :
 *  - WKT/WKB ISO 13249-3 : ST_PolyhdrlSurface
 *    The ST_TIN type is a subtype of ST_PolyhdrlSurface composed only 
 *    of triangles (ST_Triangle) that uses the Delaunay algorithm, or a 
 *    similar implementation-defined algorithm, complemented with 
 *    consideration for breaklines, soft breaks, control contours, 
 *    break voids, drape voids, voids, holes, stop lines and maximum 
 *    length of triangle sides
 * 
 * @author Johann Sorel
 */
public class TIN {

    private Triangle[] triangles;
        
    public TIN(Triangle[] triangles) {
        this.triangles = triangles;
    }
        
}
