
package science.unlicense.impl.geometry.path;

import science.unlicense.api.math.TupleRW;

/**
 *
 * @author Johann Sorel
 */
public interface PathIterator {

    //Path type defined by SVG specification
    public static final int TYPE_MOVE_TO = 1;
    public static final int TYPE_LINE_TO = 2;
    public static final int TYPE_CLOSE = 3;
    public static final int TYPE_CUBIC = 4;
    public static final int TYPE_QUADRATIC = 5;
    public static final int TYPE_ARC = 6;

    /**
     * Get the geometry number of dimensions.
     * @return number of dimension
     */
    int getDimension();
    
    /**
     * Return to iteration beginning.
     */
    void reset();

    /**
     * @return true if next step exist.
     */
    boolean next();

    /**
     * @return current step type
     */
    int getType();

    /**
     * This step position.
     *
     * @param buffer Coordinate : must not be null
     * @return Coordinate
     */
    TupleRW getPosition(TupleRW buffer);

    /**
     * If step is Cubic, get the first control point.
     * If step is Quadratic, get the control point.
     *
     * @param buffer Coordinate : must not be null
     * @return Coordinate
     */
    TupleRW getFirstControl(TupleRW buffer);

    /**
     * If step is Cubic, get the second control point.
     *
     * @param buffer Coordinate : must not be null
     * @return Coordinate
     */
    TupleRW getSecondControl(TupleRW buffer);

    /**
     * If step is Arc, get the arc x , y radius and x rotation.
     *
     * @param buffer Coordinate : must not be null
     * @return Coordinate
     */
    TupleRW getArcParameters(TupleRW buffer);

    /**
     * If step is Arc, get large arc flag.
     *
     * @return boolean
     */
    boolean getLargeArcFlag();

    /**
     * If step is Arc, get sweep flag.
     *
     * @return boolean
     */
    boolean getSweepFlag();

}
