
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Contains extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'C','O','N','T','A','I','N','S'});
        
    public Contains(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }
    
}
