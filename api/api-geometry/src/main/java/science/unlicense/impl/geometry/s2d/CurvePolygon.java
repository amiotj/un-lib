
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.path.ConcatenatePathIterator;
import science.unlicense.impl.geometry.path.PathIterator;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_CurvePolygon
 *   An ST_CurvePolygon value is a planar surface consisting of a single patch, 
 *   defined by one exterior boundary and zero or more interior boundaries
 * 
 * @author Johann Sorel
 */
public class CurvePolygon extends AbstractGeometry2D implements Surface {
    
    protected final Curve outer;
    protected final Sequence inners;

    public CurvePolygon(Curve outer, Sequence inners) {
        this.outer = outer;
        this.inners = inners;
    }

    public Curve getExterior() {
        return outer;
    }

    public Sequence getHoles() {
        return inners;
    }

    public PathIterator createPathIterator() {
        final Sequence s = new ArraySequence();
        if(outer!=null)s.add(outer.createPathIterator());
        if(inners!=null){
            for(int i=0,n=inners.getSize();i<n;i++){
                s.add( ((Geometry2D)inners.get(i)).createPathIterator() );
            }
        }
        return new ConcatenatePathIterator(s);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CurvePolygon other = (CurvePolygon) obj;
        if (this.outer != other.outer && (this.outer == null || !this.outer.equals(other.outer))) {
            return false;
        }
        if (!this.inners.equals(other.inners)) {
            return false;
        }
        return true;
    }
    
}
