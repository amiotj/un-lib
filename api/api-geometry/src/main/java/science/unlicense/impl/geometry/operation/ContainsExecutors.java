
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.api.math.Tuple;
import static science.unlicense.impl.geometry.operation.Distance.distance;
import science.unlicense.impl.geometry.s2d.Curve;
import science.unlicense.impl.geometry.s2d.CurvePolygon;
import science.unlicense.impl.geometry.s2d.Polyline;

/**
 *
 * @author Johann Sorel
 */
public class ContainsExecutors {

    private ContainsExecutors(){}

    /**
     * Test if point is within polygon using Winding Number algorithm.
     *
     * http://geomalgorithms.com/a03-_inclusion.html
     * http://en.wikipedia.org/wiki/Point_in_polygon
     *
     * @param pl expected closed polyline, start point == end point
     * @param b point to test
     * @return true if point is inside polygon
     */
    private static boolean isInside(Polyline pl, Point b){
        final TupleBuffer1D coords = pl.getCoordinates();
        final double[] point = b.getCoordinate().getValues();

        //check invalid polygons
        if(coords.getDimension()< 4) return false;

        int windingNumber=0;
        double[] current = new double[coords.getSampleCount()];
        double[] previous = new double[current.length];
        coords.getTupleDouble(0, current);
        for(int i=1,n=coords.getDimension();i<n;i++){
            Arrays.copy(current, 0, current.length, previous,0);
            coords.getTupleDouble(i, current);

            if(previous[1] <= point[1]){
                if(current[1] > point[1]){
                    if(Geometries.lineSide(previous, current, point) > 0){
                        windingNumber++;
                    }
                }
            }else{
                if(current[1] <= point[1]){
                    if(Geometries.lineSide(previous, current, point) < 0){
                        windingNumber--;
                    }
                }
            }
        }

        //if 0 point is outside
        return windingNumber != 0;
    }


    public static final AbstractBinaryOperationExecutor RECTANGLE_POINT =
            new AbstractBinaryOperationExecutor(Contains.class, Rectangle.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Rectangle rect = (Rectangle)first;
            final Point pt       = (Point)second;
            final double x = pt.getX();
            if(x<rect.getX() || x>rect.getX()+rect.getWidth()) return false;
            final double y = pt.getY();
            if(y<rect.getY() || y>rect.getY()+rect.getHeight()) return false;
            return true;
        }
    };

    public static final AbstractBinaryOperationExecutor BBOX_POINT =
            new AbstractBinaryOperationExecutor(Contains.class, BBox.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final BBox bbox = (BBox)first;
            final Point pt         = (Point)second;
            final int dim = bbox.getDimension();
            for(int i=0;i<dim;i++){
                final double v = pt.getCoordinate().get(i);
                if(v<bbox.getMin(i) || v>bbox.getMax(i)) return false;
            }
            return true;
        }
    };

    public static final AbstractBinaryOperationExecutor TRIANGLE_POINT =
            new AbstractBinaryOperationExecutor(Contains.class, Triangle.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Triangle a = (Triangle)first;
            final Point b = (Point)second;
            final Tuple bary = a.getBarycentricValue(b.getCoordinate());
            return bary.getY() >= 0.0 && bary.getZ() >= 0.0 && (bary.getY() + bary.getZ()) <= 1.0;
        }
    };

    public static final AbstractBinaryOperationExecutor CIRCLE_POINT =
            new AbstractBinaryOperationExecutor(Contains.class, Circle.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Circle a  = (Circle)first;
            final Point b = (Point)second;
            final double d = distance(a.getCenter(), b.getCoordinate());
            return d-a.getRadius() < 0;
        }
    };

    public static final AbstractBinaryOperationExecutor CURVEPOLYGON_POINT =
            new AbstractBinaryOperationExecutor(Contains.class, CurvePolygon.class, Point.class, false){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final CurvePolygon a  = (CurvePolygon)first;
            final Point b = (Point)second;

            final Curve c = a.getExterior();
            if(!(c instanceof Polyline)){
                throw new RuntimeException("Curve types not supported yet.");
            }

            final Polyline pl = (Polyline) c;
            boolean inside = isInside(pl, b);

            if(inside){
                //check the holes
                final Sequence holes = a.getHoles();
                for(int i=0,n=holes.getSize();i<n;i++){
                    final Polyline hole = (Polyline) holes.get(i);
                    if(isInside(hole, b)){
                        //point in a hole
                        return false;
                    }
                }
            }

            return inside;
        }
    };

}
