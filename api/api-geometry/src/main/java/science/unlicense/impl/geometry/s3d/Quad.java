

package science.unlicense.impl.geometry.s3d;

import science.unlicense.impl.geometry.AbstractOrientedGeometry;
import science.unlicense.api.geometry.BBox;

/**
 *
 * @author Johann Sorel
 */
public class Quad extends AbstractOrientedGeometry implements Geometry3D {

    private double width;
    private double height;

    /**
     * 
     * @param width along X axis
     * @param height along Y axis
     */
    public Quad(double width, double height) {
        super(3);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    
    public BBox getUnorientedBounds() {
        final BBox bbox = new BBox(3);
        bbox.getLower().setXYZ(-width/2.0, 0, -height/2.00);
        bbox.getUpper().setXYZ(width/2.0, 0, height/2.0);
        return bbox;
    }

    /**
     * Calculate quad surface area.
     * width * height
     * 
     * @return surface
     */
    public double getSurface() {
        return width * height;
    }

    /**
     * A Quad has no volume.
     * 
     * @return always 0
     */
    public double getVolume() {
        return 0;
    }
    
}
