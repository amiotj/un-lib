
package science.unlicense.impl.geometry.topo;

import science.unlicense.api.graph.DefaultVertex;
import science.unlicense.api.graph.Graph;

/**
 * Topologic Node, extends the Graph API.
 *
 * @author Johann Sorel
 */
public class Node extends DefaultVertex{

    public Node(Graph graph) {
        super(graph);
    }

}
