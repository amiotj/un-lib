
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Touches extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'T','O','U','C','H','E','S'});

    public Touches(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
