
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Overlaps extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'O','V','E','R','L','A','P','S'});

    public Overlaps(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
