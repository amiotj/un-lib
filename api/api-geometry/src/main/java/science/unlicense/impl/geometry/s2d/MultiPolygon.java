
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.Sequence;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_MultiSurface
 *   The elements of an ST_MultiPolygon value are restricted to ST_Polygon values
 * 
 * @author Johann Sorel
 */
public class MultiPolygon extends MultiSurface{

    public MultiPolygon(Sequence geometries) {
        super(geometries);
    }
    
}
