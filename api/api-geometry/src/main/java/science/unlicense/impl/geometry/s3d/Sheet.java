
package science.unlicense.impl.geometry.s3d;

import science.unlicense.impl.geometry.AbstractGeometry;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.math.Vector;

/**
 * Similar to a plane but like a paper sheet.
 *
 * @author Johann Sorel
 */
public class Sheet extends AbstractGeometry implements Geometry3D {

    // Plane normal. Points x on the plane satisfy Dot(n,x) = d
    private Vector normal;
    // a point on the plane
    private Vector point;
    // d = dot(n,p) for a given point p on the plane
    private double d;

    public Sheet(Vector normal, Vector point) {
        this.normal = normal;
        this.point = point;
        this.d = normal.dot(point);
    }

    /**
     * Build plan from 3 points.
     * @param a Tuple, not null
     * @param b Tuple, not null
     * @param c Tuple, not null
     */
    public Sheet(Vector a, Vector b, Vector c) {
        normal = b.subtract(a).cross(c.subtract(a)).localNormalize();
        point = a;
        d = normal.dot(a);
    }

    public Vector getNormal() {
        return normal;
    }

    /**
     * A point on the plane.
     */
    public Vector getPoint(){
        return point;
    }

    public double getD() {
        return d;
    }

    public int getDimension() {
        return normal.getSize();
    }

    public BBox getBoundingBox() {
        //TODO not right, if normal is perfectly on an axis then the bbox is infnite
        //on another axi.
        final BBox bbox = new BBox(3);
        for(int i=0,n=getDimension();i<n;i++){
            bbox.setRange(i, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        }
        return bbox;
    }

    /**
     * Sheet surface is infinite.
     * @return infinite
     */
    public double getSurface() {
        return Double.POSITIVE_INFINITY;
    }

    /**
     * Sheet volume is 0.
     * @return infinite
     */
    public double getVolume() {
        return 0;
    }
    
}
