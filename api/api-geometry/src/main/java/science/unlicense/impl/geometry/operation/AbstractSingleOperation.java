
package science.unlicense.impl.geometry.operation;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractSingleOperation extends AbstractOperation{

    protected Object geom;

    public AbstractSingleOperation(Object geom) {
        this.geom = geom;
    }

    public AbstractSingleOperation(Object geom, double epsilon) {
        super(epsilon);
        this.geom = geom;
    }

    public Object getGeometry() {
        return geom;
    }

}
