
package science.unlicense.impl.geometry.path;

import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.TupleRW;

/**
 * Simplify a path iterator removing points  to close to each other.
 *
 * @author Johann Sorel
 */
public class RadialDistanceSimplifyPathIterator implements PathIterator{

    private PathIterator iterator;
    private double radius;

    private boolean valid = false;
    private DefaultTuple buffer1;
    private DefaultTuple buffer2;

    public RadialDistanceSimplifyPathIterator(PathIterator iterator, double radius) {
        this.iterator = iterator;
        this.radius = radius;
        if (iterator!=null) {
            buffer1 = new DefaultTuple(iterator.getDimension());
            buffer2 = new DefaultTuple(iterator.getDimension());
        }
    }
    
    @Override
    public int getDimension() {
        return iterator.getDimension();
    }
    
    public void reset(PathIterator iterator, double radius){
        this.iterator = iterator;
        this.radius = radius;
        buffer1 = new DefaultTuple(iterator.getDimension());
        buffer2 = new DefaultTuple(iterator.getDimension());
    }

    public void reset() {
        iterator.reset();
    }

    public boolean next() {
        valid = false;
        while(!valid && iterator.next()){
            simplifyStep();
        }

        return valid;
    }

    private void simplifyStep(){
        final int stepType = iterator.getType();
        if(PathIterator.TYPE_MOVE_TO == stepType){
            //nothing to simplify
            valid = true;
        }else if(PathIterator.TYPE_LINE_TO == stepType){
            iterator.getPosition(buffer2);
            final double distance = Distance.distance(buffer1, buffer2);
            if(distance < radius){
                //skip this point
                valid = false;
                return;
            }else{
                valid = true;
            }
        }else if(PathIterator.TYPE_CLOSE == stepType){
            //nothing to simplify
            valid = true;
        }else{
            throw new RuntimeException("Simplifier expects a flatten geometries, "
                    + "without curves but found step type : "+stepType);
        }

        if(PathIterator.TYPE_CLOSE != stepType){
            //store coordinate for next iteration
            iterator.getPosition(buffer1);
        }
    }

    public int getType() {
        return iterator.getType();
    }

    public TupleRW getPosition(TupleRW buffer) {
        return iterator.getPosition(buffer);
    }

    public TupleRW getFirstControl(TupleRW buffer) {
        return iterator.getFirstControl(buffer);
    }

    public TupleRW getSecondControl(TupleRW buffer) {
        return iterator.getSecondControl(buffer);
    }

    public TupleRW getArcParameters(TupleRW buffer) {
        return iterator.getArcParameters(buffer);
    }

    public boolean getLargeArcFlag() {
        return iterator.getLargeArcFlag();
    }

    public boolean getSweepFlag() {
        return iterator.getSweepFlag();
    }

}
