
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Crosses extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'C','R','O','S','S','E','S'});

    public Crosses(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
