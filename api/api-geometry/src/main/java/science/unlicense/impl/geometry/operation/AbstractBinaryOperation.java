
package science.unlicense.impl.geometry.operation;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractBinaryOperation extends AbstractOperation{
 
    protected Object first;
    protected Object second;
    
    public AbstractBinaryOperation(Object first, Object second) {
        this.first = first;
        this.second = second;
    }

    public Object getFirst() {
        return first;
    }

    public Object getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+" ("+first.getClass().getSimpleName()+","+second.getClass().getSimpleName()+")";
    }

}
