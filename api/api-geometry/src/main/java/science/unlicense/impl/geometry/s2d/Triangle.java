
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.path.AbstractStepPathIterator;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.PathStep2D;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;

/**
 * Specification :
 *  - WKT/WKB ISO 13249-3 : ST_Triangle
 *   The ST_Triangle type is a subtype of ST_Polygon with an exterior
 *   boundary having exactly four points (the last point being the same
 *   as the first point) and no interior boundaries
 *
 * @author Johann Sorel
 */
public class Triangle extends SingularGeometry2D {

    private final TupleRW first;
    private final TupleRW second;
    private final TupleRW third;

    public Triangle(TupleRW first, TupleRW second, TupleRW third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public TupleRW getFirstCoord(){
        return first;
    }

    public TupleRW getSecondCoord(){
        return second;
    }

    public TupleRW getThirdCoord(){
        return third;
    }

    /**
     * Calculate the barycentric value in triangle for given point.
     * @param point
     * @return Vector barycentric values
     */
    public Tuple getBarycentricValue(Tuple p){
        return new Vector(getBarycentricValue(
                first.getValues(), second.getValues(), third.getValues(), p.getValues()));
    }

    /**
     * Calculate triangle normal.
     * 
     * @return normal vector
     */
    public Vector getNormal(){
        return Geometries.calculateNormal(first, second, third);
    }
    
    /**
     * Calculate the barycentric value in triangle for given point.
     * @param p
     * @return Vector barycentric values
     */
    public static double[] getBarycentricValue(final double[] a, final double[] b, final double[] c, final double[] p){
        final double[] v0 = Vectors.subtract(b, a);
        final double[] v1 = Vectors.subtract(c, a);
        final double[] v2 = Vectors.subtract(p, a);
        final double d00 = Vectors.dot(v0, v0);
        final double d01 = Vectors.dot(v0,v1);
        final double d11 = Vectors.dot(v1,v1);
        final double d20 = Vectors.dot(v2,v0);
        final double d21 = Vectors.dot(v2,v1);
        final double denom = d00 * d11 - d01 * d01;
        final double v = (d11 * d20 - d01 * d21) / denom;
        final double w = (d00 * d21 - d01 * d20) / denom;
        final double u = 1.0f - v - w;
        return new double[]{u, v, w};
    }

    public Polygon toPolygon() {
        final Polyline poly = new Polyline(
            Geometries.toTupleBuffer(new Tuple[]{
                first.copy(),
                second.copy(),
                third.copy(),
                first.copy()
            }));
        return new Polygon(poly, null);
    }

    public BBox getBoundingBox() {
        final BBox bbox = new BBox(first.getSize());
        bbox.getLower().set(first);
        bbox.getUpper().set(first);
        bbox.expand(second);
        bbox.expand(third);
        return bbox;
    }

    @Override
    public PathIterator createPathIterator() {
        return new TriangleIterator();
    }
    
    private class TriangleIterator extends AbstractStepPathIterator{

        private int index = -1;

        public void reset() {
            index = -1;
        }

        public boolean next() {
            index++;
            if(index == 0){
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO,first.getX(), first.getY());
            }else if(index == 1){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,second.getX(), second.getY());
            }else if(index == 2){
                currentStep = new PathStep2D(PathIterator.TYPE_LINE_TO,third.getX(), third.getY());
            }else if(index == 3){
                currentStep = new PathStep2D(PathIterator.TYPE_CLOSE);
            }else{
                currentStep = null;
            }

            return currentStep != null;
        }

    }


}
