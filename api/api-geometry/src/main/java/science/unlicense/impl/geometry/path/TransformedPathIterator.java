
package science.unlicense.impl.geometry.path;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.math.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public class TransformedPathIterator implements PathIterator{

    private final PathIterator sub;
    private final Transform trs;
    private final TupleRW temp;

    public TransformedPathIterator(PathIterator ite, Transform trs) {
        if (ite.getDimension() != trs.getInputDimensions()) {
            throw new InvalidArgumentException("PathIterator and Transform do not have the same dimension.");
        }
        if (trs.getInputDimensions() != trs.getOutputDimensions()) {
            throw new InvalidArgumentException("Transform input and output dimensions are different.");
        }
        
        this.sub = ite;
        this.trs = trs;
        this.temp = new DefaultTuple(trs.getInputDimensions());
    }

    @Override
    public int getDimension() {
        return trs.getOutputDimensions();
    }

    public void reset() {
        sub.reset();
    }

    public boolean next() {
        return sub.next();
    }

    public int getType() {
        return sub.getType();
    }

    public TupleRW getPosition(final TupleRW buffer) {
        temp.setToZero();
        sub.getPosition(temp);
        trs.transform(temp, temp);
        buffer.set(temp);
        return buffer;
    }

    public TupleRW getFirstControl(TupleRW buffer) {
        temp.setToZero();
        sub.getFirstControl(temp);
        trs.transform(temp, temp);
        buffer.set(temp);
        return buffer;
    }

    public TupleRW getSecondControl(TupleRW buffer) {
        temp.setToZero();
        sub.getSecondControl(temp);
        trs.transform(temp, temp);
        buffer.set(temp);
        return buffer;
    }

    public TupleRW getArcParameters(TupleRW buffer) {
        sub.getArcParameters(buffer);
        buffer = trs.transform(buffer, buffer.copy());
        return buffer;
    }

    public boolean getLargeArcFlag() {
        return sub.getLargeArcFlag();
    }

    public boolean getSweepFlag() {
        return sub.getSweepFlag();
    }
    
}
