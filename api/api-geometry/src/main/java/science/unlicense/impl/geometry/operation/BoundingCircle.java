
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 * Compute the bounding circle or sphere of a geometry.
 *
 * @author Johann Sorel
 */
public class BoundingCircle extends AbstractSingleOperation{

    private static final Chars NAME = new Chars(new byte[]{'B','O','U','N','D','I','N','G','C','I','R','C','L','E'});

    public BoundingCircle(Geometry first) {
        super(first);
    }
    
    public Chars getName() {
        return NAME;
    }

    public static double[] calculateCircleCenter(double[] a, double[] b, double[] c) {
        final double as = (b[1]-a[1]) / (b[0]-a[0]);
        final double bs = (c[1]-b[1]) / (c[0]-b[0]);
        final double[] center = new double[2];
        center[0] = (as * bs * (a[1]-c[1]) + bs * (a[0]+b[0]) - as * (b[0]+c[0])) / (2 * (bs-as));
        center[1] = -1.0 * (center[0] - (a[0]+b[0])/2.0) / as + (a[1]+b[1])/2.0;
        return center;
    }

}
