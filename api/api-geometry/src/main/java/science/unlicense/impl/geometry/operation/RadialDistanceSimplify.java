
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class RadialDistanceSimplify extends AbstractSingleOperation{

    private static final Chars NAME = new Chars(new byte[]
    {'R','A','D','I','A','L','D','I','S','T','A','N','C','E','S','I','M','P','L','I','F','Y'});

    private final double radius;

    public RadialDistanceSimplify(Geometry geometry, double radius) {
        super(geometry);
        this.radius = radius;
    }
    
    public Chars getName() {
        return NAME;
    }

    public double getDistance() {
        return radius;
    }




}
