
package science.unlicense.impl.geometry.path;

import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.math.Tuple;

/**
 * Path iterator over a coordinate array.
 * @author Johann Sorel
 */
public class CoordinatePathIterator extends AbstractStepPathIterator{

    private final TupleBuffer1D coords;
    private final boolean close;
    private final int length;
    private int index = 0;
        
    public CoordinatePathIterator(TupleBuffer1D coords) {
        this(coords,false);
    }
    
    public CoordinatePathIterator(TupleBuffer1D coords,boolean close) {
        this.coords = coords;
        this.close = close;
        this.length = coords.getDimension();
    }
        
    public void reset() {
        index = 0;
    }

    public boolean next() {
        if(index==length && close){
            currentStep = new PathStep2D(TYPE_CLOSE);
            index++;
            return true;
        }
        if(index>=length){
            return false;
        }
        
        final int type = (index==0) ? PathIterator.TYPE_MOVE_TO : PathIterator.TYPE_LINE_TO;
        double[] sample = coords.getTupleDouble(index, new double[2]);
        currentStep = new PathStep2D(type, sample[0], sample[1]);
        
        index++;
        return true;
    }
    
}
