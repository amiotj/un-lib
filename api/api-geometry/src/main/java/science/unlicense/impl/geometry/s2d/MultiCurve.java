
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.Sequence;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_MultiCurve
 *   The elements of an ST_MultiCurve value are restricted to ST_Curve values.
 * 
 * @author Johann Sorel
 */
public class MultiCurve extends MultiGeometry {
    
    public MultiCurve() {
    }

    public MultiCurve(Sequence geometries) {
        super(geometries);
    }
    
}
