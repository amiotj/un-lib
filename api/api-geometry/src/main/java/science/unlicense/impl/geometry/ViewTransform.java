
package science.unlicense.impl.geometry;

import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Affine2;

public class ViewTransform {

    private final Matrix3x3 matrix = new Matrix3x3().setToIdentity();
    private final Affine2 sourceToTarget = new Affine2();

    public Affine2 getSourceToTarget() {
        return sourceToTarget;
    }

    public void set(Matrix3x3 transform) {
        matrix.set(transform);
    }

    public void setToIdentity() {
        matrix.setToIdentity();
        sourceToTarget.fromMatrix(matrix);
    }

    public void scaleAt(double scale, double[] coords) {
        final Matrix3x3 m = new Matrix3x3().setToIdentity();
        m.set(0, 0, scale);
        m.set(1, 1, scale);
        m.set(0, 2, coords[0] - coords[0]*scale);
        m.set(1, 2, coords[1] - coords[1]*scale);
        m.multiply(matrix,matrix);
        sourceToTarget.fromMatrix(matrix);
    }

    public void rotateAt(double angle, double[] coords) {
        coords = new double[]{coords[0],coords[1],0};
        final double cos = Math.cos(angle);
        final double sin = Math.sin(angle);
        final Matrix3x3 m = new Matrix3x3().setToIdentity();
        m.set(0, 0,  cos);
        m.set(0, 1, -sin);
        m.set(1, 0,  sin);
        m.set(1, 1,  cos);
        m.set(0, 2, coords[0]);
        m.set(1, 2, coords[1]);
        coords = m.transform(coords, null);
        m.set(0, 2, m.get(0, 2) - coords[0]);
        m.set(1, 2, m.get(1, 2) - coords[1]);
        m.multiply(matrix,matrix);
        sourceToTarget.fromMatrix(matrix);
    }

    public void translate(double[] translation) {
        final Matrix3x3 m = new Matrix3x3().setToIdentity();
        m.set(0, 2, translation[0]);
        m.set(1, 2, translation[1]);
        m.multiply(matrix,matrix);
        sourceToTarget.fromMatrix(matrix);
    }

}