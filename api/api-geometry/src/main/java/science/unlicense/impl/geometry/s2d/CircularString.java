
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.number.Primitive;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.operation.BoundingCircle;
import science.unlicense.impl.geometry.path.AbstractStepPathIterator;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.PathStep2D;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_CircularString
 *   An ST_CircularString value has circular interpolation between ST_Point values
 *
 * @author Johann Sorel
 */
public class CircularString extends AbstractGeometry2D implements Curve {
    
    private final TupleBuffer1D coords;

    public CircularString() {
        this(new DefaultTupleBuffer1D(Primitive.TYPE_DOUBLE, 2, 0));
    }
    
    public CircularString(TupleBuffer1D coords) {
        this.coords = coords;
    }
    
    public TupleBuffer1D getCoordinates() {
        return coords;
    }

    /**
     * A CircularString isclosed if first coordinate equals last.
     * @return true if string is closed.
     */
    public boolean isClosed(){
        final double[] t0 = coords.getTupleDouble(0, new double[coords.getSampleCount()]);
        final double[] tn = coords.getTupleDouble(coords.getDimension()-1, new double[coords.getSampleCount()]);
        return Arrays.equals(t0, tn);
    }

    public PathIterator createPathIterator() {
        return new CircularPathIterator();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CircularString other = (CircularString) obj;
        if (!this.coords.equals(other.coords)) {
            return false;
        }
        return true;
    }
    
    private class CircularPathIterator extends AbstractStepPathIterator{

        private final double[] a = new double[2];
        private final double[] b = new double[2];
        private final double[] c = new double[2];
        private int index = -1;

        public void reset() {
            index = -1;
        }

        public boolean next() {

            //TODO not right but at least it has a close shape of a circle.
            index++;
            if(index>=coords.getDimension()){
                return false;
            }else if(index==0){
                coords.getTupleDouble(0, a);
                currentStep = new PathStep2D(PathIterator.TYPE_MOVE_TO, a[0], a[1]);
            }else if(index==1){
                coords.getTupleDouble(index-1, a);
                coords.getTupleDouble(index+0, b);
                coords.getTupleDouble(index+1, c);               
                final double[] center = BoundingCircle.calculateCircleCenter(a, b, c);
                final double[] sym = Geometries.calculateAxisSymmetry(a, b, center);                
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC, sym[0], sym[1], b[0], b[1]);
            }else{
                coords.getTupleDouble(index-2, a);
                coords.getTupleDouble(index-1, b);
                coords.getTupleDouble(index-0, c);
                final double[] center = BoundingCircle.calculateCircleCenter(a, b, c);
                final double[] sym = Geometries.calculateAxisSymmetry(b, c, center);                
                currentStep = new PathStep2D(PathIterator.TYPE_QUADRATIC, sym[0], sym[1], c[0], c[1]);
            }

            return true;
        }

    }
    
}
