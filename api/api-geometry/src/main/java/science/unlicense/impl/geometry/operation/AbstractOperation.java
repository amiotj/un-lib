
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.geometry.operation.Operation;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractOperation implements Operation{

    protected final double epsilon;

    public AbstractOperation() {
        this(0.000000001);
    }

    public AbstractOperation(double epsilon) {
        this.epsilon = epsilon;
    }
    
    public double getEpsilon() {
        return epsilon;
    }
    
}
