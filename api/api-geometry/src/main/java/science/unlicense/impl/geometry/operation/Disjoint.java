
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Disjoint extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'D','I','S','J','O','I','N','T'});

    public Disjoint(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
