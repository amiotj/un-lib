
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.operation.Operation;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.math.Affine;
import science.unlicense.impl.geometry.OBBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class NearestExecutors {

    private NearestExecutors(){}

    public static abstract class NearestExecutor extends AbstractBinaryOperationExecutor{

        public NearestExecutor() {
            super(null, null, null, false);
        }

        public NearestExecutor(Class firstGeomClass, Class secondGeomClass) {
            super(Nearest.class,firstGeomClass,secondGeomClass,true);
        }
        
        public Object execute(Operation operation) throws OperationException{
            final AbstractBinaryOperation op = (AbstractBinaryOperation) operation;
            Object first = op.getFirst();
            Object second = op.getSecond();
            
            if(canInvert && !(firstGeomClass.isInstance(first) && secondGeomClass.isInstance(second))){
                //set geometries in expected order
                Object temp = first;
                first = second;
                second = temp;
                //flip result
                Geometry[] res = (Geometry[]) execute(op, first, second);
                temp = res[0];
                res[0] = res[1];
                res[1] = (Geometry) temp;
                return res;
            }
            
            return execute(op, first, second);
        }
        
    }
    
    public static final NearestExecutor POINT_POINT =
            new NearestExecutor(Point.class, Point.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point)first;
            final Point b = (Point)second;
            return new Geometry[]{a,b};
        }
    };

    public static final NearestExecutor POINT_PLANE =
            new NearestExecutor(Point.class, Plane.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a  = (Point)first;
            final Plane b = (Plane)second;
            return new Geometry[]{a, new Point(Geometries.projectPointOnPlan(
                    a.getCoordinate().getValues(), 
                    b.getNormal().getValues(),
                    b.getD()))};
        }
    };

    public static final NearestExecutor POINT_TRIANGLE =
            new NearestExecutor(Point.class, Triangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Point a = (Point)first;
            final Triangle b = (Triangle)second;
            
            final double[] coord = a.getCoordinate().getValues();
            
            final double[] t0 = b.getFirstCoord().getValues();
            final double[] t1 = b.getSecondCoord().getValues();
            final double[] t2 = b.getThirdCoord().getValues();
            final double[] planNormal = Geometries.calculateNormal(t0,t1,t2);
            final double planD = Geometries.calculatePlanD(planNormal, t0);
            
            final double[] pointOnPlan = Geometries.projectPointOnPlan(coord, planNormal, planD);
            
            if(Geometries.inTriangle(t0, t1, t2, pointOnPlan)){
                //point projection is in in the triangle
                return new Geometry[]{a, new Point(pointOnPlan)};
            }else{
                //calculate distance to edges
                double distanceSqt;
                final double[] res = new double[coord.length];
                final double[] buffer = new double[coord.length];
                final double[] ratio = new double[1];
                
                //distance to first edge
                double distSqt = Distance.distanceSquare(t0, t1, buffer, coord, ratio, op.epsilon);                
                distanceSqt = distSqt;
                Arrays.copy(buffer,0,buffer.length,res,0);
                
                //distance to second edge
                distSqt = Distance.distanceSquare(t1, t2, buffer, coord, ratio, op.epsilon);                
                if(distSqt<distanceSqt){
                    distanceSqt = distSqt;
                    Arrays.copy(buffer,0,buffer.length,res,0);
                }
                
                //distance to third edge
                distSqt = Distance.distanceSquare(t2, t0, buffer, coord, ratio, op.epsilon);                
                if(distSqt<distanceSqt){
                    distanceSqt = distSqt;
                    Arrays.copy(buffer,0,buffer.length,res,0);
                }
                                
                return new Geometry[]{a,new Point(res)};
            }
        }
    };
    
    public static final NearestExecutor POINT_LINE =
            new NearestExecutor(Point.class, Segment.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Point a = (Point)first;
            final Segment b = (Segment)second;
            final Segment la = new Segment(a.getCoordinate(), a.getCoordinate());
            return (Geometry[])LINE_LINE.execute(op, la, b);
        }
    };
    
    public static final NearestExecutor POINT_RECTANGLE =
            new NearestExecutor(Point.class, Rectangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Rectangle b = (Rectangle)second;
            return (Geometry[])POINT_BBOX.execute(op, first, b.getBoundingBox());
        }
    };
    
    public static final NearestExecutor POINT_BBOX =
            new NearestExecutor(Point.class, BBox.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Point a = (Point)first;
            final BBox b = (BBox)second;            
            final int dim = a.getDimension();            
            final TupleRW nearest = new DefaultTuple(dim);
            for(int i=0;i<dim;i++){
                double val = a.getCoordinate().get(i);
                final double minx = b.getMin(i);
                final double maxx = b.getMax(i);
                if(val<minx) val = minx;
                if(val>maxx) val = maxx;
                nearest.set(i, val);
            }
            return new Geometry[]{a,new Point(nearest)};
        }
    };
    
    public static final NearestExecutor POINT_OBBOX =
            new NearestExecutor(Point.class, OBBox.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) throws OperationException {
            final Point a = (Point)first;
            final OBBox b = (OBBox)second;
            
            //we move the point in the OBBox space
            Affine rt = b.getTransform();
            Affine irt = rt.invert();
            Point pt = new Point(irt.transform(a.getCoordinate(),null));
            final BBox bbox = new BBox(b.getLower(),b.getUpper());
            final Geometry[] geoms = (Geometry[])POINT_BBOX.execute(op, pt, bbox);
            
            //convert point back in world space
            pt = (Point) geoms[1];
            rt.transform(pt.getCoordinate(), pt.getCoordinate());
            
            return new Geometry[]{a,pt};
        }
    };

    public static final NearestExecutor LINE_LINE =
            new NearestExecutor(Segment.class, Segment.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Segment line1 = (Segment)first;
            final Segment line2 = (Segment)second;
            final double[] ratio = new double[2];
            final double[] c1 = new double[line1.getStart().getSize()];
            final double[] c2 = new double[line1.getStart().getSize()];
            final double dist = Distance.distance(
                                    line1.getStart().getValues(), line1.getEnd().getValues(), c1,
                                    line2.getStart().getValues(), line2.getEnd().getValues(), c2,
                                    ratio,op.epsilon);
            return new Geometry[]{new Point(c1),new Point(c2)};
        }
    };

    public static final NearestExecutor LINE_TRIANGLE =
            new NearestExecutor(Segment.class, Triangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Segment segment = (Segment)first;
            final Triangle triangle  = (Triangle)second;
            final double[] segmentStart = segment.getStart().getValues();
            final double[] segmentEnd = segment.getEnd().getValues();
            
            final double[] t0 = triangle.getFirstCoord().getValues();
            final double[] t1 = triangle.getSecondCoord().getValues();
            final double[] t2 = triangle.getThirdCoord().getValues();
            
            final double[] p0 = new double[segmentStart.length];
            final double[] p1 = new double[segmentStart.length];
            final double distance = Distance.distanceSquareSegmentTriangle(segmentStart, segmentEnd, 
                    p0, t0, t1, t2, p1, op.epsilon);
            
            return new Geometry[]{new Point(p0), new Point(p1)};
        }
    };
    
    public static final NearestExecutor TRIANGLE_TRIANGLE =
            new NearestExecutor(Triangle.class, Triangle.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Triangle triangle1 = (Triangle)first;
            final Triangle triangle2  = (Triangle)second;
            
            final double[] t0 = triangle2.getFirstCoord().getValues();
            final double[] t1 = triangle2.getSecondCoord().getValues();
            final double[] t2 = triangle2.getThirdCoord().getValues();
            
            double distance;
            final double[] res0 = new double[t0.length];
            final double[] res1 = new double[t0.length];
            
            final double[] p0 = new double[t0.length];
            final double[] p1 = new double[t0.length];
            
            //fisrt edge test
            double[] segmentStart = triangle1.getFirstCoord().getValues();
            double[] segmentEnd = triangle1.getSecondCoord().getValues();
            double dist = Distance.distanceSquareSegmentTriangle(segmentStart, segmentEnd, 
                    p0, t0, t1, t2, p1, op.epsilon);
            
            distance = dist;
            Arrays.copy(p0, 0, p0.length, res0, 0);
            Arrays.copy(p1, 0, p1.length, res1, 0);
            
            //second edge test
            segmentStart = triangle1.getSecondCoord().getValues();
            segmentEnd = triangle1.getThirdCoord().getValues();
            dist = Distance.distanceSquareSegmentTriangle(segmentStart, segmentEnd, 
                    p0, t0, t1, t2, p1, op.epsilon);
            
            if(dist<distance){
                distance = dist;
                Arrays.copy(p0, 0, p0.length, res0, 0);
                Arrays.copy(p1, 0, p1.length, res1, 0);
            }
            
            //third edge test
            segmentStart = triangle1.getThirdCoord().getValues();
            segmentEnd = triangle1.getFirstCoord().getValues();
            dist = Distance.distanceSquareSegmentTriangle(segmentStart, segmentEnd, 
                    p0, t0, t1, t2, p1, op.epsilon);
            
            if(dist<distance){
                distance = dist;
                Arrays.copy(p0, 0, p0.length, res0, 0);
                Arrays.copy(p1, 0, p1.length, res1, 0);
            }
            
            
            return new Geometry[]{new Point(res0), new Point(res1)};
        }
    };
    
    
    public static final NearestExecutor POLYLINE_POLYLINE =
            new NearestExecutor(Polyline.class, Polyline.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Polyline line1 = (Polyline)first;
            final Polyline line2 = (Polyline)second;

            final double[] c1 = new double[line1.getDimension()];
            final double[] c2 = new double[line1.getDimension()];
            double distance = Double.MAX_VALUE;

            final double[] tempRatio = new double[2];
            final double[] tempC1 = new double[line1.getDimension()];
            final double[] tempC2 = new double[line1.getDimension()];

            final TupleBuffer1D line1Coords = line1.getCoordinates();
            final TupleBuffer1D line2Coords = line2.getCoordinates();
            final int nb1 = line1Coords.getDimension()-1;
            final int nb2 = line2Coords.getDimension()-1;

            for(int i=0;i<nb1;i++){
                final double[] s1 = line1Coords.getTupleDouble(i,null);
                final double[] e1 = line1Coords.getTupleDouble(i+1,null);
                for(int k=0;k<nb2;k++){
                    final double[] s2 = line2Coords.getTupleDouble(k,null);
                    final double[] e2 = line2Coords.getTupleDouble(k+1,null);

                    final double dist = Distance.distance(
                                    s1, e1, tempC1,
                                    s2, e2, tempC2,
                                    tempRatio,op.epsilon);
                    if(dist<distance){
                        //keep informations
                        distance = dist;
                        Arrays.copy(tempC1, 0, tempC1.length, c1, 0);
                        Arrays.copy(tempC2, 0, tempC2.length, c2, 0);
                    }
                }
            }

            return new Geometry[]{new Point(c1), new Point(c2)};
        }
    };
    
    public static final NearestExecutor PLANE_SEGMENT =
            new NearestExecutor(Plane.class, Segment.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final Plane plan  = (Plane)first;
            final Segment segment = (Segment)second;
            
            final Vector vb1 = new Vector(segment.getStart());
            double d1 = plan.getNormal().dot(vb1.subtract(plan.getNormal().scale(plan.getD())) );
            final Vector vb2 = new Vector(segment.getEnd());
            double d2 = plan.getNormal().dot(vb2.subtract(plan.getNormal().scale(plan.getD())) );
            
            final Vector pointOnPlan;
            final Vector segmentPoint;
            if(d1<d2){
                segmentPoint = vb1;
                pointOnPlan = vb1.subtract(plan.getNormal().scale(d1));
            }else{
                segmentPoint = vb2;
                pointOnPlan = vb2.subtract(plan.getNormal().scale(d2));
            }
            
            return new Geometry[]{new Point(pointOnPlan), new Point(segmentPoint)};
        }
    };

    public static final NearestExecutor BBOX_SEGMENT =
            new NearestExecutor(BBox.class, Segment.class){

        protected Geometry[] execute(AbstractBinaryOperation op, Object first, Object second) {
            final BBox bbox  = (BBox)first;
            final Segment segment = (Segment)second;
            final double[] segmentStart = segment.getStart().getValues();
            final double[] segmentEnd = segment.getEnd().getValues();

            final double[] l = bbox.getLower().getValues();
            final double[] u = bbox.getUpper().getValues();
            final double[][] edges = {
                {l[0], l[1], l[2]},
                {l[0], l[1], u[2]},
                {l[0], u[1], l[2]},
                {l[0], u[1], u[2]},
                {u[0], l[1], l[2]},
                {u[0], l[1], u[2]},
                {u[0], u[1], l[2]},
                {u[0], u[1], u[2]}
            };
            //12 edges
            //bottom face   :  0,1  1,3  3,2  2,0
            //top face      :  4,5  5,7  7,6  6,4
            //contour faces :  0,4  1,5  2,6  3,7
            final int[][] indexEdge = {
                {0,1},{1,3},{3,2},{2,0},
                {4,5},{5,7},{7,6},{6,4},
                {0,4},{1,5},{2,6},{3,7}
            };

            final double[] p0 = new double[segmentStart.length];
            final double[] p1 = new double[segmentStart.length];
            final double[] ratio = new double[2];
            final double[] nearestPointSegment = new double[segmentStart.length];
            final double[] nearestPointBBox = new double[segmentStart.length];

            //test each edge distance to segment
            double distance = Double.POSITIVE_INFINITY;
            double dist;
            for(int i=0;i<indexEdge.length;i++){
                dist = Distance.distanceSquare(segmentStart, segmentEnd, p0, edges[indexEdge[i][0]], edges[indexEdge[i][1]], p1, ratio, op.epsilon);
                if(dist<distance){
                    distance = dist;
                    Arrays.copy(p0,0,p0.length,nearestPointSegment,0);
                    Arrays.copy(p1,0,p1.length,nearestPointBBox,0);
                }
            }

            //test the two segment ends
            //TODO
            //test plan intersection
            //TODO

            return new Geometry[]{new Point(nearestPointBBox), new Point(nearestPointSegment)};
        }
    };
    
}