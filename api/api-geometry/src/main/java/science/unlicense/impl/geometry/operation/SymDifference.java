
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class SymDifference extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'S','Y','M','D','I','F','F','E','R','E','N','C','E'});

    public SymDifference(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
