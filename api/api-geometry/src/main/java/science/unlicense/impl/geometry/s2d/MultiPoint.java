
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.Sequence;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_MultiPoint
 *   The elements of an ST_MultiPoint value are restricted to ST_Point values.
 * 
 * @author Johann Sorel
 */
public class MultiPoint extends MultiGeometry{

    public MultiPoint(Sequence geometries) {
        super(geometries);
    }
    
}
