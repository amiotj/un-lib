
package science.unlicense.impl.geometry.topo;

import science.unlicense.api.graph.DefaultEdge;

/**
 * Topologic Edge, extends the graph API.
 *
 * @author Johann Sorel
 */
public class Edge extends DefaultEdge {

    public Edge(Node n1, Node n2) {
        super(n1, n2);
    }

}
