
package science.unlicense.impl.geometry;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.number.Primitive;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.path.FlattenPathIterator;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;
import static science.unlicense.impl.math.Vectors.cross;
import static science.unlicense.impl.math.Vectors.dot;
import static science.unlicense.impl.math.Vectors.subtract;

/**
 * Utilities for geometries.
 *
 * @author Johann Sorel
 */
public class Geometries {

    private Geometries() {
    }

    /**
     * Calculate normal of triangle made of given 3 points.
     *
     * @param a
     * @param b
     * @param c
     * @return normal
     */
    public static Vector calculateNormal(Tuple a, Tuple b, Tuple c) {
        return new Vector(calculateNormal(a.getValues(), b.getValues(), c.getValues()));
    }

    /**
     * Calculate normal of triangle made of given 3 points.
     *
     * @param a
     * @param b
     * @param c
     * @return normal
     */
    public static double[] calculateNormal(double[] a, double[] b, double[] c) {
        final double[] ab = Vectors.subtract(b, a);
        final double[] ac = Vectors.subtract(c, a);
        final double[] res = Vectors.cross(ab, ac);
        Vectors.normalize(res, res);
        return res;
    }

    /**
     * Calculate normal of triangle made of given 3 points.
     *
     * @param a
     * @param b
     * @param c
     * @param buffer
     * @return normal
     */
    public static float[] calculateNormal(float[] a, float[] b, float[] c, float[] buffer) {
        final float[] ab = Vectors.subtract(b, a);
        final float[] ac = Vectors.subtract(c, a);
        buffer = Vectors.cross(ab, ac, buffer);
        return Vectors.normalize(buffer, buffer);
    }

    /**
     * @param c symmetry center
     * @param p point to reflect
     * @return reflected point
     */
    public static double[] calculatePointSymmetry(double[] c, double[] p) {
        final double[] r = new double[c.length];
        for (int i = 0; i < r.length; i++) r[i] = (2 * c[i]) - p[i];
        return r;
    }

    /**
     * @param a symmetry axis a
     * @param b symmetry axis b
     * @param p point to reflect
     * @return reflected point
     */
    public static double[] calculateAxisSymmetry(double[] a, double[] b, double[] p) {
        final double[] ratio = new double[2];
        final double[] c1 = new double[a.length];
        final double[] c2 = new double[a.length];
        Distance.distance(a, b, c1,
                p, p, c2,
                ratio, 0.000000001);

        return calculatePointSymmetry(c1, p);
    }

    /**
     * Test if given a,b,c,d quad is convex.
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return true if convex, false otherwise.
     */
    public static boolean isConvex(Tuple a, Tuple b, Tuple c, Tuple d) {
        final double[] bda = cross(subtract(d.getValues(), b.getValues()), subtract(a.getValues(), b.getValues()));
        final double[] bdc = cross(subtract(d.getValues(), b.getValues()), subtract(c.getValues(), b.getValues()));
        if (dot(bda, bdc) >= 0.0) return false;
        final double[] acd = cross(subtract(c.getValues(), a.getValues()), subtract(d.getValues(), a.getValues()));
        final double[] acb = cross(subtract(c.getValues(), a.getValues()), subtract(b.getValues(), a.getValues()));
        return dot(acd, acb) < 0.0;
    }

    /**
     * Test if the Point p is on the line porting the edge
     *
     * @param a line origine
     * @param b line end
     * @param p Point to test
     * @return true/false
     */
    public static boolean isOnLine(Point a, Point b, Point p) {
        // test if the vector product is zero
        return lineSide(a.getCoordinate().getValues(),
                b.getCoordinate().getValues(),
                p.getCoordinate().getValues()
        ) == 0;
    }

    /**
     * Test if the Point P is at the right of the line A-B.
     *
     * @param a line origine
     * @param b line end
     * @param p Point to test
     * @return true/false
     */
    public static boolean isAtRightOf(Point a, Point b, Point p) {
        return isCounterClockwise(p, b, a);
    }

    /**
     * Test if the Point P is at the right of the line A-B.
     *
     * @param a line origine
     * @param b line end
     * @param p Point to test
     * @return true/false
     */
    public static boolean isAtRightOf(double[] a, double[] b, double[] p) {
        return isCounterClockwise(p, b, a);
    }

    /**
     * return true if a, b and c turn in Counter Clockwise direction
     *
     * @param a,b,c the 3 points to test
     * @return true if a, b and c turn in Counter Clockwise direction
     */
    public static boolean isCounterClockwise(Point a, Point b, Point c) {
        return isCounterClockwise(
                a.getCoordinate().getValues(),
                b.getCoordinate().getValues(),
                c.getCoordinate().getValues());
    }

    /**
     * return true if a, b and c turn in Counter Clockwise direction
     *
     * @param a,b,c the 3 points to test
     * @return true if a, b and c turn in Counter Clockwise direction
     */
    public static boolean isCounterClockwise(double[] a, double[] b, double[] c) {
        // test the sign of the determinant of ab x cb
        return lineSide(a, b, c) > 0;
    }

    /**
     * Test the side of a point compare to a line.
     *
     * @param a line start
     * @param b line end
     * @param c to test
     * @return > 0 if point is on the left side
     * = 0 if point is on the line
     * < 0 if point is on the right side
     */
    public static double lineSide(double[] a, double[] b, double[] c) {
        return (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1]);
    }

    /**
     * The Delaunay criteria:
     * <p>
     * test if the point d is inside the circumscribed circle of triangle p1,p2,p3
     *
     * @param p1,p2,p3 triangle points
     * @param pd       point to test
     * @return true if point p0 inside circle and false, if point p0 is outside of circle or on border
     */
    public static boolean inCircle(Point p1, Point p2, Point p3, Point pd) {
        double p1Square = p1.getX() * p1.getX() + p1.getY() * p1.getY();
        double p2Square = p2.getX() * p2.getX() + p2.getY() * p2.getY();
        double p3Square = p3.getX() * p3.getX() + p3.getY() * p3.getY();
        double pdSquare = pd.getX() * pd.getX() + pd.getY() * pd.getY();

        double a = det33(p1.getX(), p1.getY(), 1, p2.getX(), p2.getY(), 1, p3.getX(), p3.getY(), 1);
        double b = det33(p1Square, p1.getY(), 1, p2Square, p2.getY(), 1, p3Square, p3.getY(), 1);
        double c = det33(p1Square, p1.getX(), 1, p2Square, p2.getX(), 1, p3Square, p3.getX(), 1);
        double d = det33(p1Square, p1.getX(), p1.getY(), p2Square, p2.getX(), p2.getY(), p3Square, p3.getX(), p3.getY());

        double det44 = Math.signum(a) * (a * pdSquare - b * pd.getX() + c * pd.getY() - d);

        if (det44 < 0) {
            return true;
        }
        return false;
    }

    /**
     * Test if a point is inside given triangle.
     *
     * @param a
     * @param b
     * @param c
     * @param p
     * @return
     */
    public static boolean inTriangle(double[] a, double[] b, double[] c, double[] p) {
        final double[] bary = Triangle.getBarycentricValue(a, b, c, p);
        return bary[1] >= 0.0 && bary[2] >= 0.0 && (bary[1] + bary[2]) <= 1.0;
    }

    /**
     * Calculate constant D of a plan.
     * Same as normal.dot(point).
     *
     * @param normal
     * @param pointOnPlan
     * @return
     */
    public static double calculatePlanD(double[] normal, double[] pointOnPlan) {
        return Vectors.dot(normal, pointOnPlan);
    }

    /**
     * Calculate projection of a point on a plan.
     *
     * @param point      point to project
     * @param planNormal plan normal
     * @param planD
     * @return projected point
     */
    public static double[] projectPointOnPlan(double[] point, double[] planNormal, double planD) {
        double[] va = Vectors.subtract(point, Vectors.scale(planNormal, planD));
        double d = Vectors.dot(planNormal, va);
        return Vectors.subtract(point, Vectors.scale(planNormal, d));
    }

    /**
     * Test if given sequence of tuple is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param coordinates
     * @return
     */
    public static boolean isClockWise(Sequence coordinates) {
        final double area = calculateArea(coordinates);
        return area > 0;
    }

    public static boolean isClockWise(TupleBuffer1D coordinates) {
        final double area = calculateArea(coordinates);
        return area > 0;
    }

    /**
     * Test if given path iterator is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param pathIterator
     * @return true if clockwise
     */
    public static boolean isClockWise(PathIterator pathIterator) {
        final Sequence outter = new ArraySequence();
        TupleRW start = new DefaultTuple(2);
        while (pathIterator.next()) {
            final int type = pathIterator.getType();
            if (type == PathIterator.TYPE_MOVE_TO) {
                pathIterator.getPosition(start);
            } else if (type == PathIterator.TYPE_CLOSE) {
                outter.add(start);
                break;
            }
            outter.add(pathIterator.getPosition(new DefaultTuple(2)));
        }
        final double area = calculateArea(outter);
        return area > 0;
    }

    /**
     * Test if given sequence of tuple is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param coordinates
     * @return
     */
    public static double calculateArea(Sequence coordinates) {
        double area = 0;
        final int numPoints = coordinates.getSize();
        for (int i = 0; i < numPoints - 1; i++) {
            final Tuple start = (Tuple) coordinates.get(i);
            final Tuple end = (Tuple) coordinates.get(i + 1);
            area += (start.getX() + end.getX()) * (start.getY() - end.getY());
        }
        return area / 2.0;
    }

    /**
     * Test if given sequence of tuple is in clockwise direction.
     * This method expect the coordinates to be a closed line.
     *
     * @param coordinates
     * @return
     */
    public static double calculateArea(TupleBuffer1D coordinates) {
        double area = 0;
        final int numPoints = coordinates.getDimension();
        final double[] start = new double[coordinates.getSampleCount()];
        final double[] end = new double[start.length];
        for (int i = 0; i < numPoints - 1; i++) {
            coordinates.getTupleDouble(i, start);
            coordinates.getTupleDouble(i + 1, end);
            area += (start[0] + end[0]) * (start[1] - end[1]);
        }
        return area / 2.0;
    }

    private static double det33(double... m) {
        double det33 = 0;
        det33 += m[0] * (m[4] * m[8] - m[5] * m[7]);
        det33 -= m[1] * (m[3] * m[8] - m[5] * m[6]);
        det33 += m[2] * (m[3] * m[7] - m[4] * m[6]);
        return det33;
    }

    public static BBox transform(BBox bbox, Matrix trs, BBox buffer) {
        if (buffer == null) {
            buffer = new BBox(bbox.getDimension());
        }

        double minX = bbox.getMin(0);
        double minY = bbox.getMin(1);
        double maxX = bbox.getMax(0);
        double maxY = bbox.getMax(1);
        final double[] coords = new double[]{
                minX, minY, 1,
                minX, maxY, 1,
                maxX, maxY, 1,
                maxX, minY, 1
        };
        final double[] trsCoords = new double[12];

        trs.transform(coords, 0, trsCoords, 0, 4);
        buffer.setRange(0, trsCoords[0], trsCoords[0]);
        buffer.setRange(1, trsCoords[1], trsCoords[1]);
        buffer.expand(trsCoords[3], trsCoords[4]);
        buffer.expand(trsCoords[6], trsCoords[7]);
        buffer.expand(trsCoords[9], trsCoords[10]);

        return buffer;
    }

    public static Geometry2D flatten(Geometry2D geom, double[] resolution) {
        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), resolution);
        final Path path = new Path();
        path.append(ite);
        return path;
    }

    public static TupleBuffer1D toTupleBuffer(Tuple[] tuples) {
        final TupleBuffer1D buffer = new DefaultTupleBuffer1D(
                Primitive.TYPE_DOUBLE, tuples[0].getSize(), tuples.length);
        for (int i = 0; i < tuples.length; i++) {
            buffer.setTuple(i, tuples[i].getValues());
        }
        return buffer;
    }

    public static TupleBuffer1D toTupleBuffer(Sequence tuples) {
        final Tuple tuple = (Tuple) tuples.get(0);
        final TupleBuffer1D buffer = new DefaultTupleBuffer1D(
                Primitive.TYPE_DOUBLE, tuple.getSize(), tuples.getSize());
        for (int i = 0, n = tuples.getSize(); i < n; i++) {
            buffer.setTuple(i, ((Tuple) tuples.get(i)).getValues());
        }
        return buffer;
    }

    public static Tuple[] toTupleArray(TupleBuffer1D buffer) {
        final Tuple[] tuples = new Tuple[buffer.getDimension()];
        for (int i = 0; i < tuples.length; i++) {
            tuples[i] = new Vector(buffer.getTupleDouble(i, null));
        }
        return tuples;
    }

}
