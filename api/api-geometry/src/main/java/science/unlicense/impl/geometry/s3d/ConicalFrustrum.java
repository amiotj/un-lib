
package science.unlicense.impl.geometry.s3d;

import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.AbstractOrientedGeometry;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;

/**
 * A conical frustrum is a volume defined
 * by 2 circles aligned on the same axis
 *
 * @author Johann Sorel
 */
public class ConicalFrustrum extends AbstractOrientedGeometry implements Geometry3D {

    private double height;
    private double radius1;
    private double radius2;

    /**
     *
     * @param height
     * @param radius1 top radius
     * @param radius2 bottom radius
     */
    public ConicalFrustrum(double height, double radius1, double radius2) {
        super(3);
        this.height = height;
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getRadius1() {
        return radius1;
    }

    public void setRadius1(double radius) {
        this.radius1 = radius;
    }

    public double getRadius2() {
        return radius2;
    }

    public void setRadius2(double radius) {
        this.radius2 = radius;
    }

    public Vector getFirst(){
        return new Vector(0, -height/2, 0);
    }

    public Vector getSecond(){
        return new Vector(0, height/2, 0);
    }

    /**
     * Calculate the volume.
     *
     * @return volume.
     */
    public double getVolume(){
        return (1.0/3.0) * Math.PI * height * ( radius1*radius1 + radius2*radius2 + (radius1*radius2));
    }

    /**
     * Calculate the cone lateral height.
     *
     * @return cone lateral height
     */
    public double getLateralHeight(){
        return Math.sqrt( Math.pow(radius1-radius2,2) + (height*height));
    }

    /**
     * Calculate top circle surface area.
     *
     * @return base circle area.
     */
    public double getTopArea() {
        return Math.PI*radius1*radius1;
    }

    /**
     * Calculate the top circumference.
     *
     * @return circumference.
     */
    public double getTopCircumference(){
        return 2 * Math.PI * radius1;
    }

    /**
     * Calculate top circle surface area.
     *
     * @return base circle area.
     */
    public double getBottomArea() {
        return Math.PI*radius2*radius2;
    }

    /**
     * Calculate the bottom circumference.
     *
     * @return circumference.
     */
    public double getBottomCircumference(){
        return 2 * Math.PI * radius2;
    }

    /**
     * Calculate lateral surface of the frustrum.
     *
     * @return lateral surface.
     */
    public double getLateralSurfaceArea(){
        return Math.PI * (radius1+radius2) * getLateralHeight();
    }

    /**
     * Calculate the surface area.
     *
     * @return area.
     */
    public double getSurface() {
        return getTopArea() + getBottomArea() + getLateralSurfaceArea();
    }

    public BBox getUnorientedBounds() {
        final BBox bbox = new BBox(3);
        final double maxRadius = Maths.max(radius1, radius2);
        bbox.getLower().setXYZ(-maxRadius, -height/2 - maxRadius, -maxRadius);
        bbox.getUpper().setXYZ(maxRadius, height/2 + maxRadius, maxRadius);
        return bbox;
    }

}
