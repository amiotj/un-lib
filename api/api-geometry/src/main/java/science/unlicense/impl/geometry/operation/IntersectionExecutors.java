
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.algorithm.SutherlandHodgman;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class IntersectionExecutors {

    private IntersectionExecutors(){}

    public static final AbstractBinaryOperationExecutor RAY_TRIANGLE =
            new AbstractBinaryOperationExecutor(Intersection.class, Ray.class, Triangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {

            final Ray ray = (Ray) first;
            final Triangle triangle = (Triangle) second;
            final Tuple position = ray.getPosition();
            final Tuple direction = ray.getDirection();

            final Tuple t1 = triangle.getFirstCoord();
            final Tuple t2 = triangle.getSecondCoord();
            final Tuple t3 = triangle.getThirdCoord();
            final Vector n = Geometries.calculateNormal(t1, t2, t3);
            
            if(n.length()==0){
                //triangle is a point
                return null;
            }

            final Vector rel = new Vector(3);
            rel.set(position);
            rel.localSubtract(t1);
            final double a = -n.dot(rel);
            final double b =  n.dot(new Vector(direction.get(0),direction.get(1),direction.get(2)));

            if(Math.abs(b) < 0.000001){
                return null;
            }

            final double r = a / b;
    //        if(r<0){
    //            return null;
    //        }

            return new Point(new DefaultTuple(
                position.getX() + r * direction.get(0),
                position.getY() + r * direction.get(1),
                position.getZ() + r * direction.get(2)
                ));
            }
    };
    
    /**
     * http://en.wikipedia.org/wiki/Line–plane_intersection
     */
    public static final AbstractBinaryOperationExecutor RAY_PLANE =
            new AbstractBinaryOperationExecutor(Intersection.class, Ray.class, Plane.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {

            final Ray ray = (Ray) first;
            final Plane plane = (Plane) second;
            final TupleRW rayPosition = ray.getPosition();
            final Vector rayDirection = ray.getDirection();
            
            final Vector n = plane.getNormal();
            
            final Vector rel = new Vector(3);
            rel.set(rayPosition);
            rel.localSubtract(plane.getPoint());
            final double a = n.dot(rel);
            final double b =  n.dot(rayDirection);

            if(Math.abs(b) < 0.000001){
                //ray is parallal
                if(a==0){
                    //ray is on the plane
                    return new Ray(rayPosition.copy(), rayDirection.copy());
                }
                
                return null;
            }

            final double r = -a / b;
            if(r<0){
                //ray points the opposite direction
                return null;
            }

            return new Point(new DefaultTuple(
                rayPosition.getX() + r * rayDirection.get(0),
                rayPosition.getY() + r * rayDirection.get(1),
                rayPosition.getZ() + r * rayDirection.get(2)
                ));
        }            
    };

    public static final AbstractBinaryOperationExecutor TRIANGLE_TRIANGLE =
            new AbstractBinaryOperationExecutor(Intersection.class, Triangle.class, Triangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Triangle t1 = (Triangle) first;
            final Triangle t2 = (Triangle) second;
            
            final Sequence subject = new ArraySequence();
            subject.add(t1.getFirstCoord());
            subject.add(t1.getSecondCoord());
            subject.add(t1.getThirdCoord());
            
            final Sequence clip = new ArraySequence();
            clip.add(new Segment(t2.getFirstCoord(), t2.getSecondCoord()));
            clip.add(new Segment(t2.getSecondCoord(),t2.getThirdCoord()));
            clip.add(new Segment(t2.getThirdCoord(), t2.getFirstCoord()));
            
            final Sequence result = SutherlandHodgman.clip(subject, clip);
            final int nb = result.getSize();
            if(nb==0){
                return null;
            }else if(nb==3){
                return new Triangle((TupleRW)result.get(0), (TupleRW)result.get(1), (TupleRW)result.get(2));
            }else{
                result.add(new Vector((Tuple)result.get(0)));
                return new Polygon(new Polyline(Geometries.toTupleBuffer(result)), new ArraySequence());
            }
            
        }
    };
    
    public static final AbstractBinaryOperationExecutor RECTANGLE_RECTANGLE =
            new AbstractBinaryOperationExecutor(Intersection.class, Rectangle.class, Rectangle.class, true){

        protected Object execute(AbstractBinaryOperation op, Object first, Object second) {
            final Rectangle rect1 = (Rectangle) first;
            final Rectangle rect2 = (Rectangle) second;
            return rect1.intersection(rect2);
        }
    };
    
}
