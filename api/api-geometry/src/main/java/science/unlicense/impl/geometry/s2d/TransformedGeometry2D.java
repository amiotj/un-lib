
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.TransformedPathIterator;

/**
 *
 * @author Johann Sorel
 */
public class TransformedGeometry2D extends AbstractGeometry2D {

    private final Geometry2D sub;
    private final Transform trs;

    public TransformedGeometry2D(Geometry2D sub, Transform trs) {
        CObjects.ensureNotNull(sub);
        CObjects.ensureNotNull(trs);
        if (sub.getDimension() != trs.getInputDimensions()) {
            throw new InvalidArgumentException("PathIterator and Transform do not have the same dimension.");
        }
        if (trs.getInputDimensions() != trs.getOutputDimensions()) {
            throw new InvalidArgumentException("Transform input and output dimensions are different.");
        }
        this.sub = sub;
        this.trs = trs;
    }
    
    public PathIterator createPathIterator() {
        return new TransformedPathIterator(sub.createPathIterator(), trs);
    }
    
}
