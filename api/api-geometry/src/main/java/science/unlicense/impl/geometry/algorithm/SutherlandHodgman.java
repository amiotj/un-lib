

package science.unlicense.impl.geometry.algorithm;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.api.math.Tuple;

/**
 * Clipping algorithm.
 * 
 * doc :
 * https://en.wikipedia.org/wiki/Sutherland–Hodgman_algorithm
 * 
 * @author Johann Sorel
 */
public class SutherlandHodgman {
       
    /**
     * 
     * @param subjectPolygon : sequence of Tuples
     * @param clipPolygon : sequence of Segment
     * @return Sequence of tuple for the result polygon
     */
    public static Sequence clip(Sequence subjectPolygon, Sequence clipPolygon){
        final Sequence outputList = new ArraySequence(subjectPolygon);
        for(int i=0,n=clipPolygon.getSize();i<n;i++){
            final Segment clipEdge = (Segment) clipPolygon.get(i);
            final Sequence inputList = new ArraySequence(outputList);
            if(inputList.isEmpty()) break;            
            outputList.removeAll();
            
            Tuple start = (Tuple) inputList.get(inputList.getSize()-1);
            for(int k=0,kn=inputList.getSize();k<kn;k++){
                final Tuple end = (Tuple) inputList.get(k);
                
                if(isInside(clipEdge, end)){
                    if(!isInside(clipEdge, start)){
                        outputList.add(computeIntersection(clipEdge, start, end));
                    }
                    outputList.add(end.copy());
                }else if(isInside(clipEdge, start)){
                    outputList.add(computeIntersection(clipEdge, start, end));
                }
                start = end;
            }
        }
        
        return outputList;
    } 
    
    private static boolean isInside(Segment edge, Tuple point){
        return Geometries.isAtRightOf(
                edge.getStart().getValues(), 
                edge.getEnd().getValues(), 
                point.getValues());
    }
    
    private static Tuple computeIntersection(Segment edge, Tuple start, Tuple end){
        final Tuple buffer1 = start.copy();
        final Tuple buffer2 = start.copy();
        final double[] ratio = new double[2];
        Distance.distance(edge.getStart().getValues(), edge.getEnd().getValues(), buffer1.getValues(), 
                          start.getValues(), end.getValues(), buffer2.getValues(), ratio, 0.000000000000001);
        return buffer1;
    }
    
}
