
package science.unlicense.impl.geometry.s2d;

/**
 * Specification :
 *  - WKT/WKB ISO 13249-3 : ST_PolyhdrlSurface
 *    The ST_PolyhdrlSurface type is a subtype of ST_Surface composed 
 *    of contiguous polygon surfaces (ST_Polygon) connected along 
 *    their common boundary curves
 *
 * @author Johann Sorel
 */
public class PolyhedralSurface extends AbstractGeometry2D implements Surface {
    
    private Polygon[] polygons;

    public PolyhedralSurface() {
    }

    public PolyhedralSurface(Polygon[] polygons) {
        this.polygons = polygons;
    }

    public Polygon[] getPolygons() {
        return polygons;
    }
    
}
