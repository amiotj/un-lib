
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Difference extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'D','I','F','F','E','R','E','N','C','E'});

    public Difference(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
