
package science.unlicense.impl.geometry;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 * An oriented geometry is a geometry with specific properties
 * and additional rotation,translation properties.
 *
 * @author Johann Sorel
 */
public interface OrientedGeometry extends Geometry {

    /**
     * Get geometry transform.
     * @return NodeTransform, never null
     */
    NodeTransform getTransform();

    /**
     * Get the bounding box without the orientation rotation and translation
     * applied.
     * @return Bounding box
     */
    BBox getUnorientedBounds();

}
