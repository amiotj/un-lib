
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Relate extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'R','E','L','A','T','E'});

    public Relate(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
