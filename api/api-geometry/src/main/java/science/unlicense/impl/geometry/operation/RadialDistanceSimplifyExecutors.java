
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.geometry.operation.Operation;
import science.unlicense.impl.geometry.Point;

/**
 *
 * @author Johann Sorel
 */
public class RadialDistanceSimplifyExecutors {

    private RadialDistanceSimplifyExecutors(){}

    public static final AbstractSingleOperationExecutor POINT =
            new AbstractSingleOperationExecutor(RadialDistanceSimplify.class, Point.class){

        public Object execute(Operation operation) {
            final RadialDistanceSimplify op = (RadialDistanceSimplify) operation;
            final Point p = (Point) op.getGeometry();
            return new Point(p.getCoordinate().copy());
        }
    };

}
