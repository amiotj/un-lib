
package science.unlicense.impl.geometry.s2d;

import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.operation.OperationException;

/**
 * A 2D Geometry is a geometry which can be represented by a path.
 *
 * @author Johann Sorel
 */
public interface Geometry2D extends Geometry {

    /**
     * Calculate geometry area.
     *
     * @return area
     */
    double getArea();

    /**
     * Calculate geometry length.
     *
     * @return length
     */
    double getLength();

    /**
     * Calculate circle that encapsulate the geometry.
     *
     * @return Circle
     */
    Circle getBoundingCircle() throws OperationException;

    /**
     * Calculate convex polygon that encapsulate the geometry.
     *
     * @return Polygon
     */
    Polygon getConvexhull() throws OperationException;

    /**
     * Create a path iteraor.
     *
     * @return PathIterator
     */
    PathIterator createPathIterator();

}
