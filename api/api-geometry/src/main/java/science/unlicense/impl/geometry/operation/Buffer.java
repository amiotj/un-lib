
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 * Buffer operation, expands the geometry contour.
 *
 * @author Johann Sorel
 */
public class Buffer extends AbstractSingleOperation{

    private static final Chars NAME = new Chars(new byte[]{'B','U','F','F','E','R'});

    private final double distance;

    public Buffer(Geometry geometry, double distance) {
        super(geometry);
        this.distance = distance;
    }

    public Chars getName() {
        return NAME;
    }

    public double getDistance() {
        return distance;
    }

}
