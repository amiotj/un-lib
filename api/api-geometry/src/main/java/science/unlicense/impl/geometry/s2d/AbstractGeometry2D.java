
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.AbstractGeometry;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.impl.geometry.path.TransformedPathIterator;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.math.transform.Transform;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractGeometry2D extends AbstractGeometry implements Geometry2D {

    public int getDimension() {
        return 2;
    }

    public double getArea(){
        throw new UnimplementedException("Not supported yet.");
    }

    public double getLength(){
        throw new UnimplementedException("Not supported yet.");
    }

    public Circle getBoundingCircle(){
        throw new UnimplementedException("Not supported yet.");
    }

    public Polygon getConvexhull() throws OperationException{
        return (Polygon)super.getConvexhull();
    }

    public BBox getBoundingBox() {
        final BBox bbox = new BBox(2);
        final PathIterator ite = createPathIterator();
        final TupleRW coord = new DefaultTuple(2);
        boolean first = true;
        while(ite.next()){
            //TODO this is not accurate, it doesnt take in consideration the curve types.
            if(ite.getType() == PathIterator.TYPE_CLOSE) continue;
            ite.getPosition(coord);
            if(first){
                first = false;
                bbox.setRange(0, coord.getX(), coord.getX());
                bbox.setRange(1, coord.getY(), coord.getY());
            }else{
                if(bbox.getMin(0)>coord.get(0)) bbox.setRange(0, coord.get(0),  bbox.getMax(0));
                if(bbox.getMax(0)<coord.get(0)) bbox.setRange(0, bbox.getMin(0),coord.get(0));
                if(bbox.getMin(1)>coord.get(1)) bbox.setRange(1, coord.get(1),  bbox.getMax(1));
                if(bbox.getMax(1)<coord.get(1)) bbox.setRange(1, bbox.getMin(1),coord.get(1));
            }

        }
        return bbox;
    }

    public PathIterator createPathIterator() {
        throw new UnimplementedException("Not supported.");
    }

    public PathIterator createPathIterator(Transform transform) {
        return new TransformedPathIterator(createPathIterator(), transform);
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append(getClass().getSimpleName());
        sb.append(' ');

        final TupleRW c = new DefaultTuple(3);
        final PathIterator ite = createPathIterator();
        while(ite.next()){
            if(ite.getType() != PathIterator.TYPE_CLOSE){
                ite.getPosition(c);
                sb.append(CObjects.toChars(c));
                sb.append(',');
            }else{
                sb.append("[CLOSE]");
                sb.append(',');
            }
        }
        return sb.toChars();
    }

}
