
package science.unlicense.impl.geometry;

import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.CObject;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.impl.geometry.operation.Buffer;
import science.unlicense.impl.geometry.operation.Contains;
import science.unlicense.impl.geometry.operation.ConvexHull;
import science.unlicense.impl.geometry.operation.Crosses;
import science.unlicense.impl.geometry.operation.Difference;
import science.unlicense.impl.geometry.operation.Disjoint;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.operation.Intersection;
import science.unlicense.impl.geometry.operation.Intersects;
import science.unlicense.impl.geometry.operation.Nearest;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.geometry.operation.Operations;
import science.unlicense.impl.geometry.operation.Overlaps;
import science.unlicense.impl.geometry.operation.SymDifference;
import science.unlicense.impl.geometry.operation.Touches;
import science.unlicense.impl.geometry.operation.Union;
import science.unlicense.impl.geometry.operation.Within;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractGeometry extends CObject implements Geometry {

    private Dictionary userProperties = null;

    public synchronized Dictionary getUserProperties() {
        if(userProperties==null) userProperties = new HashDictionary();
        return userProperties;
    }

    public Point getCentroid() {
        throw new UnimplementedException("Not supported yet.");
    }

    public final Geometry buffer(double distance) throws OperationException{
        final Buffer op = new Buffer(this, distance);
        return (Geometry)Operations.execute(op);
    }

    public final boolean contains(Geometry geometry) throws OperationException {
        final Contains op = new Contains(this, geometry);
        return (Boolean)Operations.execute(op);
    }

    public Geometry getConvexhull() throws OperationException{
        final ConvexHull op = new ConvexHull(this);
        return (Geometry)Operations.execute(op);
    }

    public final boolean crosses(Geometry geometry) throws OperationException{
        final Crosses op = new Crosses(this, geometry);
        return (Boolean)Operations.execute(op);
    }

    public final Geometry difference(Geometry geometry) throws OperationException{
        final Difference op = new Difference(this, geometry);
        return (Geometry)Operations.execute(op);
    }

    public final boolean disjoint(Geometry geometry) throws OperationException{
        final Disjoint op = new Disjoint(this, geometry);
        return (Boolean)Operations.execute(op);
    }

    public final double distance(Geometry geometry) throws OperationException{
        final Distance op = new Distance(this, geometry);
        return (Double)Operations.execute(op);
    }

    public final Geometry intersection(Geometry geometry) throws OperationException{
        final Intersection op = new Intersection(this, geometry);
        return (Geometry)Operations.execute(op);
    }

    public final boolean intersects(Geometry geometry) throws OperationException{
        final Intersects op = new Intersects(this, geometry);
        return (Boolean)Operations.execute(op);
    }

    public final Geometry[] nearest(Geometry geometry) throws OperationException{
        final Nearest op = new Nearest(this, geometry);
        return (Geometry[])Operations.execute(op);
    }

    public final boolean overlaps(Geometry geometry) throws OperationException{
        final Overlaps op = new Overlaps(this, geometry);
        return (Boolean)Operations.execute(op);
    }

    public final Geometry symDifference(Geometry geometry) throws OperationException{
        final SymDifference op = new SymDifference(this, geometry);
        return (Geometry)Operations.execute(op);
    }

    public final boolean touches(Geometry geometry) throws OperationException{
        final Touches op = new Touches(this, geometry);
        return (Boolean)Operations.execute(op);
    }

    public final Geometry union(Geometry geometry) throws OperationException{
        final Union op = new Union(this, geometry);
        return (Geometry)Operations.execute(op);
    }

    public final boolean within(Geometry geometry) throws OperationException{
        final Within op = new Within(this, geometry);
        return (Boolean)Operations.execute(op);
    }

}
