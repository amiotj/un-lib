
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Intersection extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'I','N','T','E','R','S','E','C','T','I','O','N'});

    public Intersection(Geometry first, Geometry second) {
        super(first,second);
    }

    public Chars getName() {
        return NAME;
    }
    
}
