
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.Sorter;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.Point;

/**
 * @author Johann Sorel
 * @author Izyumov Konstantin
 * @see WikiPedia: https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
 */
public class ConvexHull extends AbstractSingleOperation {

    private static final Chars NAME = new Chars(new byte[]{'C', 'O', 'N', 'V', 'E', 'X', 'H', 'U', 'L', 'L'});

    public ConvexHull(Geometry first) {
        super(first);
    }

    public Chars getName() {
        return NAME;
    }

    public static Point[] convexHull(Point[] inputPoints) {
        if (inputPoints.length < 2)
            return inputPoints;

        HashSet uniquePoints = new HashSet();
        for (int i = 0; i < inputPoints.length; i++) {
            uniquePoints.add(inputPoints[i]);
        }


        ArraySequence arrayUnique = new ArraySequence(uniquePoints.toArray());

        Collections.sort(arrayUnique, new Sorter() {
            @Override
            public int sort(Object first, Object second) {
                if (((Point) first).getX() == ((Point) second).getX()){
                    if (((Point) first).getY() > ((Point) second).getY())
                        return 1;
                    if (((Point) first).getY() < ((Point) second).getY())
                        return -1;
                    return 0;
                }
                if (((Point) first).getX() > ((Point) second).getX())
                    return 1;
                if (((Point) first).getX() < ((Point) second).getX())
                    return -1;
                return 0;
            }
        });

        int n = arrayUnique.getSize();
        Point[] P = new Point[n];
        for (int i = 0; i < n; i++) {
            P[i] = (Point) arrayUnique.get(i);
        }

        Point[] H = new Point[2 * n];

        int k = 0;
        // Build lower hull
        for (int i = 0; i < n; ++i) {
            while (k >= 2 && Geometries.isCounterClockwise(H[k - 2], H[k - 1], P[i])) {
                k--;
            }
            H[k++] = P[i];
        }

        // Build upper hull
        for (int i = n - 2, t = k + 1; i >= 0; i--) {
            while (k >= t && Geometries.isCounterClockwise(H[k - 2], H[k - 1], P[i])) {
                k--;
            }
            H[k++] = P[i];
        }
        if (k > 1) {
            H = (Point[]) Arrays.copy(H, 0, k - 1); // remove non-hull vertices after k; remove k - 1 which is a duplicate
        }
        return H;
    }
}
