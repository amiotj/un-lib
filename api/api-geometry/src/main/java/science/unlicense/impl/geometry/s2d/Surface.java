
package science.unlicense.impl.geometry.s2d;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_Surface
 *   An ST_Surface value is a 2-dimensional geometry that consists of a 
 *   single connected interior that is associated with one exterior ring 
 *   and zero or more interior rings
 * 
 * @author Johann Sorel
 */
public interface Surface extends Geometry2D {
    
}
