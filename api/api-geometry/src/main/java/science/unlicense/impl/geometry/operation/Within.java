
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Within extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'W','I','T','H','I','N'});

    public Within(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
