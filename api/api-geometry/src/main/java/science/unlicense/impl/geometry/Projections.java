
package science.unlicense.impl.geometry;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.DefaultAffine;
import science.unlicense.impl.math.Vectors;

/**
 * Methods to create transforms for common transformation cases.
 * 
 * @author Johann Sorel
 */
public final class Projections {
    
    private Projections(){}
    
    /**
     * Create an Affine that transform the given bbox to be centerd into target bbox.
     * 
     * @param source
     * @param target
     * @return 
     */
    public static AffineRW centered(BBox source, BBox target){
        final Tuple sourceCenter = source.getMiddle();
        final Tuple targetCenter = target.getMiddle();
        final double[] trs = Vectors.subtract(targetCenter.getValues(), sourceCenter.getValues());
        final AffineRW m = DefaultAffine.create(trs.length).setToIdentity();
        m.setCol(trs.length, trs);
        return m;
    }

    /**
     * Create an Affine that transform the given bbox to be centerd into target bbox.
     * The source bbox is scaled with given scale.
     *
     * @param source
     * @param target
     * @param scale wanted scale.
     * @return
     */
    public static AffineRW centeredScaled(BBox source, BBox target, double scale){

        final double[] sourceCenter = source.getMiddle().getValues();
        final double[] targetCenter = target.getMiddle().getValues();
        Vectors.scale(sourceCenter, scale, sourceCenter);
        final double[] trs = Vectors.subtract(targetCenter, sourceCenter);

        final AffineRW mt = DefaultAffine.create(trs.length).setToIdentity();
        for(int i=0;i<trs.length;i++){
            mt.set(i, i, scale);
            mt.set(i, trs.length, trs[i]);
        }
        
        return mt;
    }

    /**
     * Create an Affine that transform the given bbox to fit into target bbox.
     * Dimensions ratio are preserved and will be centered in target bbox.
     * 
     * @param source
     * @param target
     * @return 
     */
    public static AffineRW scaled(BBox source, BBox target){
        //find min scale
        final int dim =source.getDimension();
        double scale = target.getSpan(0) / source.getSpan(0);
        for(int i=1;i<dim;i++){
            scale = Maths.min( target.getSpan(i) / source.getSpan(i) , scale);
        }
        return centeredScaled(source, target, scale);
    }
    
    /**
     * Create an Affine that transform the given bbox to fill the all space into target bbox.
     * Dimensions ratio are not preserved.
     * 
     * @param source
     * @param target
     * @return 
     */
    public static AffineRW stretched(BBox source, BBox target){
        final int dim = source.getDimension();
        final AffineRW m = DefaultAffine.create(dim).setToIdentity();

        for(int i=0;i<dim;i++){
            final double scale = target.getSpan(i) / source.getSpan(i);
            final double trs = target.getMin(i) - source.getMin(i) * scale;
            m.set(i, i, scale);
            m.set(i, dim, trs);
        }

        return m;
    }
    
    /**
     * Create an Affine that transform the given bbox to fill the all space into target bbox.
     * The source bbox is scaled preserving ratio and centering source bbox.
     * 
     * @param source
     * @param target
     * @return 
     */
    public static AffineRW zoomed(BBox source, BBox target){
        //find max scale
        final int dim =source.getDimension();
        double scale = target.getSpan(0) / source.getSpan(0);
        for(int i=1;i<dim;i++){
            scale = Maths.max( target.getSpan(i) / source.getSpan(i) , scale);
        }
        return centeredScaled(source, target, scale);
    }
    
    /**
     * Create an Affine that transform the given bbox to fit space into target bbox on one
     * specified dimension.
     * The source bbox is scaled preserving ratio and will be centered on other axis.
     * 
     * @param source
     * @param target
     * @param axis index to fit
     * @return 
     */
    public static AffineRW fit(BBox source, BBox target, int axis){
        final double scale = target.getSpan(axis) / source.getSpan(axis);
        return centeredScaled(source, target, scale);
    }

}
