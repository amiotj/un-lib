
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.collection.Sequence;

/**
 * Specification :
 * - WKT/WKB ISO 13249-3 : ST_MultiLineString
 *   The elements of an ST_MultiLineString value are restricted to ST_LineString values.
 * 
 * @author Johann Sorel
 */
public class MultiPolyline extends MultiCurve {

    public MultiPolyline(Sequence geometries) {
        super(geometries);
    }
    
}
