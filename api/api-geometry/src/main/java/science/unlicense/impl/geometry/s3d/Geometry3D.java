

package science.unlicense.impl.geometry.s3d;

import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public interface Geometry3D extends Geometry{

    /**
     * Calculate geometry surface area.
     *
     * @return surface
     */
    double getSurface();

    /**
     * Calculate geometry volume.
     *
     * @return volume
     */
    double getVolume();

}
