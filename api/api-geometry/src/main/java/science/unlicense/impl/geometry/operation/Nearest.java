
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.math.Tuple;

/**
 * Find the nearest points between 2 geometries.
 *
 * @author Johann Sorel
 */
public class Nearest extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'N','E','A','R','E','S','T'});

    public Nearest(Geometry first, Geometry second) {
        super(first,second);
    }

    public void setFirst(Geometry first) {
        this.first = first;
    }

    public void setSecond(Geometry second) {
        this.second = second;
    }
    
    public Chars getName() {
        return NAME;
    }

    /**
     *  See Distance.
     * @param line1Coords
     * @param buffer1
     * @param line2Coords
     * @param line2End
     * @param buffer2
     * @param ratio
     * @param offset : will store the segment offset of the nearest points
     * @param epsilon
     */
    public static void nearest(Sequence line1Coords, double[] buffer1,
                                Sequence line2Coords, double[] buffer2,
                                double[] ratio, int[] offset, double epsilon){

        double distance = Double.MAX_VALUE;

        final double[] tempRatio = new double[2];
        final double[] tempC1 = new double[buffer1.length];
        final double[] tempC2 = new double[buffer1.length];

        final int nb1 = line1Coords.getSize()-1;
        final int nb2 = line2Coords.getSize()-1;

        for(int i=0;i<nb1;i++){
            final Tuple s1 = (Tuple) line1Coords.get(i);
            final Tuple e1 = (Tuple) line1Coords.get(i+1);
            for(int k=0;k<nb2;k++){
                final Tuple s2 = (Tuple) line2Coords.get(k);
                final Tuple e2 = (Tuple) line2Coords.get(k+1);

                final double dist = Distance.distance(
                                s1.getValues(), e1.getValues(), tempC1,
                                s2.getValues(), e2.getValues(), tempC2,
                                tempRatio,epsilon);
                if(dist<distance){
                    //keep informations
                    distance = dist;
                    offset[0] = i;
                    offset[1] = k;
                    Arrays.copy(tempC1, 0, tempC1.length, buffer1, 0);
                    Arrays.copy(tempC2, 0, tempC2.length, buffer2, 0);
                }
            }
        }

    }
    
    public static void nearest(double[] line1Coords, int size1, double[] buffer1,
                               double[] line2Coords, int size2, double[] buffer2,
                                double[] ratio, int[] offset, double epsilon){

        double distance = Double.MAX_VALUE;

        final int dim = buffer1.length;
        final double[] tempRatio = new double[2];
        final double[] tempC1 = new double[dim];
        final double[] tempC2 = new double[dim];

        final int nb1 = size1/dim;
        final int nb2 = size2/dim;
        
        final double[] s1 = new double[dim];
        final double[] e1 = new double[dim];
        final double[] s2 = new double[dim];
        final double[] e2 = new double[dim];
        for(int i=0;i<nb1;i++){
            Arrays.copy(line1Coords, i*dim, dim, s1, 0);
            Arrays.copy(line1Coords, (i+1)*dim, dim, e1, 0);
            for(int k=0;k<nb2;k++){
                Arrays.copy(line2Coords, k*dim, dim, s2, 0);
                Arrays.copy(line2Coords, (k+1)*dim, dim, e2, 0);
                final double dist = Distance.distance(
                                s1, e1, tempC1,
                                s2, e2, tempC2,
                                tempRatio,epsilon);
                if(dist<distance){
                    //keep informations
                    distance = dist;
                    offset[0] = i;
                    offset[1] = k;
                    Arrays.copy(tempC1, 0, tempC1.length, buffer1, 0);
                    Arrays.copy(tempC2, 0, tempC2.length, buffer2, 0);
                }
            }
        }

    }

}
