
package science.unlicense.impl.geometry.s2d;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.CoordinatePathIterator;
import science.unlicense.api.geometry.DefaultTupleBuffer1D;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.number.Primitive;

/**
 * A LineString/Polyline.
 *
 * Specification :
 * - SVG v1.1:9.6 :
 *   The ‘polyline’ element defines a set of connected straight line segments.
 *   Typically, ‘polyline’ elements define open shapes.
 * - WKT/WKB ISO 13249-3 : LineString/LinearRing/ST_LineString 
 *   An ST_LineString value has linear interpolation between ST_Point values
 *   called LineString or LinearRing if closed
 *
 * @author Johann Sorel
 */
public class Polyline extends AbstractGeometry2D implements Curve {

    private TupleBuffer1D coords;

    public Polyline() {
        this(new DefaultTupleBuffer1D(Primitive.TYPE_DOUBLE, 2, 0));
    }
    
    public Polyline(TupleBuffer1D coords) {
        CObjects.ensureNotNull(coords);
        this.coords = coords;
    }
    
    public TupleBuffer1D getCoordinates() {
        return coords;
    }

    /**
     * A Polyline isclosed if first coordinate equals last.
     * @return true if polyline is closed.
     */
    public boolean isClosed(){
        final double[] t0 = coords.getTupleDouble(0, new double[coords.getSampleCount()]);
        final double[] tn = coords.getTupleDouble(coords.getDimension()-1, new double[coords.getSampleCount()]);
        return Arrays.equals(t0, tn);
    }

    public PathIterator createPathIterator() {
        return new CoordinatePathIterator(coords);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Polyline other = (Polyline) obj;
        if (!this.coords.equals(other.coords)) {
            return false;
        }
        return true;
    }
    
}
