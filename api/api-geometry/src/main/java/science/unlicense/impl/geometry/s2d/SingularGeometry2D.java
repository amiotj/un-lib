

package science.unlicense.impl.geometry.s2d;

/**
 * There are numerous named geometry types.
 * If all types extend Polygon the relative constraint of each type may be
 * altered after creation. For exemple a fourth point could be added to a triangle.
 * 
 * Therefor singular geometry even if they mathematicaly are polygons will not
 * extend the Polygon class to enforce each type constraint.
 * 
 * @author Johann Sorel
 */
public abstract class SingularGeometry2D extends AbstractGeometry2D {
   
    /**
     * Convert this singular geometry to it's polygon equivalent.
     */
    public abstract Polygon toPolygon();
    
}
