
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Intersects extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'I','N','T','E','R','S','E','C','T','S'});
        
    public Intersects(Geometry first, Geometry second) {
        super(first,second);
    }

    public Chars getName() {
        return NAME;
    }
    
    public void setFirst(Object first) {
        this.first = first;
    }

    public void setSecond(Object second) {
        this.second = second;
    }
    
}
