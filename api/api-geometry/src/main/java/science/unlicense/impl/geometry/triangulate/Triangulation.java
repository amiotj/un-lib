package science.unlicense.impl.geometry.triangulate;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.geometry.Point;

public class Triangulation {

    public static class Contour {
        Sequence lines;
        boolean isHole;

        public Contour(Sequence lines, boolean isHole) {
            this.lines = lines;
            this.isHole = isHole;
        }
    }

    public static Sequence getTriangles(Contour contourNotIntersect) {
        if (contourNotIntersect == null)
            return null;

        // checking contour
        // TODO: 15.08.2016

        // create first iteration of delaunay triangulation
        Delaunay delaunay = new Delaunay();
        for (int i = 0; i < contourNotIntersect.lines.getSize(); i++) {
            Point[] line = (Point[]) contourNotIntersect.lines.get(i);
            delaunay.insertPoint(line[0]);
            delaunay.insertPoint(line[1]);
        }

        // adapted edge to lines
        adaptedEdge(delaunay, contourNotIntersect.lines);

        // remove triangles
        // TODO: 15.08.2016

        return delaunay.computeTriangles();
    }

    private static void adaptedEdge(Delaunay delaunay, Sequence lines) {
        Sequence edge = delaunay.computeEdges();
        Sequence linePosition = new ArraySequence();
        for (int i = 0; i < lines.getSize(); i++) {
            boolean isFound = false;
            Point[] line = (Point[]) lines.get(i);
            for (int j = 0; j < edge.getSize(); j++) {
                if (sameLines(line, (Point[]) edge.get(j))) {
                    isFound = true;
                    j = edge.getSize();
                }
            }
            if (!isFound) {
                linePosition.add(i);
            }
        }
        if (linePosition.getSize() > 0) {
            science.unlicense.api.collection.Collections.sort(linePosition);
            for (int i = linePosition.getSize() - 1; i >= 0; i--) {
                Point[] line = (Point[]) lines.get((int) (Integer) linePosition.get(i));
                Point middle = new Point(
                        (line[0].getX() + line[1].getX()) / 2.,
                        (line[0].getY() + line[1].getY()) / 2.
                );
                lines.add(new Point[]{line[0], middle});
                lines.add(new Point[]{line[1], middle});
                lines.remove((int) (Integer) linePosition.get(i));
                delaunay.insertPoint(middle);
            }
            adaptedEdge(delaunay, lines);
        }
    }

    private static boolean sameLines(Point[] line1, Point[] line2) {
        if (!line1[0].equals(line2[0]) && !line1[0].equals(line2[1])) {
            return false;
        }
        if (line1[0].equals(line2[0]) && line1[1].equals(line2[1])) {
            return true;
        }
        if (line1[1].equals(line2[0]) && line1[0].equals(line2[1])) {
            return true;
        }
        return false;
    }

}
