
package science.unlicense.impl.geometry.algorithm;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.math.Vectors;

/**
 * K-means clustering.
 * 
 * resources :
 * https://en.wikipedia.org/wiki/K-means_clustering
 * 
 * @author Johann Sorel (Adapted to unlicense-lib)
 */
public class KMeans {
    
    /**
     * 
     * @param tuples
     * @param nbCluster
     * @param nbCycle
     * @return 
     */
    public static double[][] kmeans(TupleBuffer tuples, int nbCluster, int nbCycle){
        
        final double[][] means = randomMeans(tuples, nbCluster);
        final long[] count = new long[nbCluster];
        final double[][] cumul = new double[nbCluster][tuples.getSampleCount()];
        
        double[] tuple = null;
        int closestMean;
        double dist, d;
        
        
        for(int i=0;i<nbCycle;i++){
            //reset counters
            Arrays.fill(count, 0);
            for(int k=0;k<cumul.length;k++) Arrays.fill(cumul[k], 0);
            
            //iterate on tuples
            final TupleIterator ite = tuples.createIterator(null);
            for(tuple=ite.nextAsDouble(tuple);tuple!=null;tuple=ite.nextAsDouble(tuple)){
                closestMean = 0;
                dist = Distance.distance(means[0], tuple);
                for(int m=1;m<means.length;m++){
                    d = Distance.distance(means[m], tuple);
                    if(d<dist){
                        dist = d;
                        closestMean = m;
                    }
                }
                
                count[closestMean]++;
                Vectors.add(cumul[closestMean], tuple, cumul[closestMean]);
            }
            
            //calculate new means
            for(int m=0;m<means.length;m++){
                Vectors.scale(cumul[m], count[m], means[m]);
            }
        }
        
        return means;
    }
    
    /**
     * Generating random means.
     */
    private static double[][] randomMeans(TupleBuffer tuples, int nbClusters){
        
        //find min and max values
        final double[] tuple = new double[tuples.getSampleCount()];
        tuples.createIterator(null).nextAsDouble(tuple);
        final BBox bbox = new BBox(tuples.getSampleCount());
        bbox.getLower().set(tuple);
        bbox.getUpper().set(tuple);
        bbox.expand(tuples);
        
        //generate random means inside bbox extent
        final double[] min = bbox.getLower().getValues();
        final double[] max = bbox.getUpper().getValues();
        final double[][] means = new double[nbClusters][tuple.length];
        for(int i=0;i<nbClusters;i++){
            for(int k=0;k<tuple.length;k++){
                means[i][k] = Maths.random(min[k], max[k]);
            }
        }
        
        return means;
    }
    
}
