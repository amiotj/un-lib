package science.unlicense.impl.geometry.triangulate;

import science.unlicense.impl.geometry.Point;


/**
 * Quad-Edge data structure
 *
 * @author Structure by Guibas and Stolfi
 * @author X.Philippeau
 * @author Johann sorel (adapted to Unlicense project)
 *
 * @see Primitives for the Manipulation of General Subdivisions and the
 * Computation of Voronoi Diagrams (Leonidas Guibas,Jorge Stolfi)
 */
final class QuadEdge {

    // pointer to the next (direct order) QuadEdge
    private QuadEdge onext;
    // pointer to the dual QuadEdge (faces graph <-> edges graph)
    private QuadEdge rot;
    // origin point of the edge/face
    private Point orig;
    // marker for triangle generation
    public boolean mark = false;

    /**
     * (private) constructor. Use makeEdge() to create a new QuadEdge
     *
     * @param onext pointer to the next QuadEdge on the ring
     * @param rot pointer to the next (direct order) crossing edge
     * @param orig Origin point
     */
    private QuadEdge(QuadEdge Onext, QuadEdge rot, Point orig) {
        this.onext = Onext;
        this.rot = rot;
        this.orig = orig;
    }

    // ----------------------------------------------------------------
    //                             Getter/Setter
    // ----------------------------------------------------------------
    public QuadEdge onext() {
        return onext;
    }

    public QuadEdge rot() {
        return rot;
    }

    public Point orig() {
        return orig;
    }

    public void setOnext(QuadEdge next) {
        onext = next;
    }

    public void setRot(QuadEdge rot) {
        this.rot = rot;
    }

    public void setOrig(Point p) {
        this.orig = p;
    }

    // ----------------------------------------------------------------
    //                      QuadEdge Navigation
    // ----------------------------------------------------------------
    /**
     * @return the symetric (reverse) QuadEdge
     */
    public QuadEdge sym() {
        return rot.rot();
    }

    /**
     * @return the other extremity point
     */
    public Point dest() {
        return sym().orig();
    }

    /**
     * @return the symetric dual QuadEdge
     */
    public QuadEdge rotSym() {
        return rot.sym();
    }

    /**
     * @return the previous QuadEdge (pointing to this.orig)
     */
    public QuadEdge oprev() {
        return rot.onext().rot();
    }

    /**
     * @return the previous QuadEdge starting from dest()
     */
    public QuadEdge dprev() {
        return rotSym().onext().rotSym();
    }

    /**
     * @return the next QuadEdge on left Face
     */
    public QuadEdge lnext() {
        return rotSym().onext().rot();
    }

    /**
     * @return the previous QuadEdge on left Face
     */
    public QuadEdge lprev() {
        return onext().sym();
    }

    // ************************** STATIC ******************************
    /**
     * Create a new edge (i.e. a segment)
     *
     * @param orig origin of the segment
     * @param dest end of the segment
     * @return the QuadEdge of the origin point
     */
    public static QuadEdge makeEdge(Point orig, Point dest) {
        final QuadEdge q0 = new QuadEdge(null, null, orig);
        final QuadEdge q1 = new QuadEdge(null, null, null);
        final QuadEdge q2 = new QuadEdge(null, null, dest);
        final QuadEdge q3 = new QuadEdge(null, null, null);

        // create the segment
        q0.onext = q0;
        q2.onext = q2; // lonely segment: no "next" quadedge
        q1.onext = q3;
        q3.onext = q1; // in the dual: 2 communicating facets

        // dual switch
        q0.rot = q1;
        q1.rot = q2;
        q2.rot = q3;
        q3.rot = q0;

        return q0;
    }

    /**
     * attach/detach the two edges = combine/split the two rings in the dual space
     *
     * @param q1,q2 the 2 QuadEdge to attach/detach
     */
    public static void splice(QuadEdge a, QuadEdge b) {
        final QuadEdge alpha = a.onext().rot();
        final QuadEdge beta = b.onext().rot();

        final QuadEdge t1 = b.onext();
        final QuadEdge t2 = a.onext();
        final QuadEdge t3 = beta.onext();
        final QuadEdge t4 = alpha.onext();

        a.setOnext(t1);
        b.setOnext(t2);
        alpha.setOnext(t3);
        beta.setOnext(t4);
    }

    /**
     * Create a new QuadEdge by connecting 2 QuadEdges
     *
     * @param e1,e2 the 2 QuadEdges to connect
     * @return the new QuadEdge
     */
    public static QuadEdge connect(QuadEdge e1, QuadEdge e2) {
        final QuadEdge q = makeEdge(e1.dest(), e2.orig());
        splice(q, e1.lnext());
        splice(q.sym(), e2);
        return q;
    }

    public static void swapEdge(QuadEdge e) {
        final QuadEdge a = e.oprev();
        final QuadEdge b = e.sym().oprev();
        splice(e, a);
        splice(e.sym(), b);
        splice(e, a.lnext());
        splice(e.sym(), b.lnext());
        e.orig = a.dest();
        e.sym().orig = b.dest();
    }

    /**
     * Delete a QuadEdge
     *
     * @param q the QuadEdge to delete
     */
    public static void deleteEdge(QuadEdge q) {
        splice(q, q.oprev());
        splice(q.sym(), q.sym().oprev());
    }

}