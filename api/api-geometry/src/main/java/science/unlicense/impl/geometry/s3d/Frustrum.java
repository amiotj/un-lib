
package science.unlicense.impl.geometry.s3d;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.AbstractGeometry;

/**
 * 
 * @author Johann Sorel
 */
public class Frustrum extends AbstractGeometry implements Geometry3D{

    private final Plane[] planes;

    public Frustrum(Plane[] planes) {
        this.planes = planes;
    }

    public Plane[] getPlanes() {
        return planes;
    }

    @Override
    public int getDimension() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public BBox getBoundingBox() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public double getSurface() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public double getVolume() {
        throw new UnimplementedException("Not supported yet.");
    }

}
