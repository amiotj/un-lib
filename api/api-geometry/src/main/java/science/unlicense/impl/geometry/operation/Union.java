
package science.unlicense.impl.geometry.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Geometry;

/**
 *
 * @author Johann Sorel
 */
public class Union extends AbstractBinaryOperation{

    private static final Chars NAME = new Chars(new byte[]{'U','N','I','O','N'});

    public Union(Geometry first, Geometry second) {
        super(first,second);
    }
    
    public Chars getName() {
        return NAME;
    }

}
