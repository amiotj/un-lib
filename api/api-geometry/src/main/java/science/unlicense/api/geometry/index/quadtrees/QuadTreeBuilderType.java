
package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.exception.InvalidArgumentException;

/**
 * A mutable builder for instantiating quadtree implementations.
 *
 * @param <T>
 *          The precise type of members.
 * @author Mark Raynsford
 */
public interface QuadTreeBuilderType<T extends QuadTreeMemberType<T>>{
  /**
   * @return A new quadtree based on the parameters given so far.
   */

  QuadTreeType<T> build();

  /**
   * @return A new SD quadtree based on the parameters given so far.
   */

  QuadTreeSDType<T> buildWithSD();

  /**
   * Do not use specific minimum quadrant sizes.
   */

  void disableLimitedQuadrantSizes();

  /**
   * Disable pruning of empty quadtree nodes.
   */

  void disablePruning();

  /**
   * Use specific minimum quadrant sizes.
   *
   * @param x
   *          The minimum quadrant width - Must be even and greater than or
   *          equal to <code>2</code>.
   * @param y
   *          The minimum quadrant height - Must be even and greater than or
   *          equal to <code>2</code>.
   *
   * @throws InvalidArgumentException
   *           If the preconditions above are not satisfied.
   */

  void enableLimitedQuadrantSizes(
    int x,
    int y)
      throws InvalidArgumentException;

  /**
   * Enable pruning of empty quadtree nodes.
   */

  void enablePruning();

  /**
   * Set the position of the quadtree.
   *
   * @param x
   *          The X coordinate.
   * @param y
   *          The Y coordinate.
   */

  void setPosition2i(
    int x,
    int y);

  /**
   * <p>
   * Set the size of the quadtree.
   * </p>
   *
   * @param x
   *          The width - Must be even and greater than or equal to
   *          <code>2</code>.
   * @param y
   *          The height - Must be even and greater than or equal to
   *          <code>2</code>.
   *
   * @throws InvalidArgumentException
   *           If the preconditions above are not satisfied.
   */

  void setSize2i(
    int x,
    int y)
      throws InvalidArgumentException;
}
