
package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.exception.InvalidArgumentException;


/**
 * Checks for quadtree invariants.
 * 
 * @author Mark Raynsford
 */
final class QuadTreeChecks{
    
  static void checkSize(
    final String name,
    final double width,
    final double height)
  {
    if (width < 2) {
      final String s = String.format("%s: Width must be >= 2", name);
      throw new InvalidArgumentException(s);
    }
    if ((width % 2) != 0) {
      final String s = String.format("%s: Width must be even", name);
      throw new InvalidArgumentException(s);
    }

    if (height < 2) {
      final String s = String.format("%s: Height must be >= 2", name);
      throw new InvalidArgumentException(s);
    }
    if ((height % 2) != 0) {
      final String s = String.format("%s: Height must be even", name);
      throw new InvalidArgumentException(s);
    }
  }

  private QuadTreeChecks()
  {
    throw new RuntimeException("Unreachable code");
  }
}
