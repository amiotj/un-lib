
package science.unlicense.api.geometry;

/**
 * Default Tuple iterator.
 *
 * @author Johann Sorel
 */
public class DefaultTupleIterator implements TupleIterator{

    protected final TupleBuffer sm;
    private final long[] dimensions;
    protected final int[] coordinate;
    private final int[] dimStart;
    private final int[] dimEnd;
    private final Object container;
    private boolean start = true;;
    protected boolean finished = false;

    public DefaultTupleIterator(TupleBuffer sm, BBox bbox) {
        this.sm = sm;
        this.dimensions = sm.getExtent().toArray();
        this.coordinate = new int[this.dimensions.length];
        this.container = sm.createTupleStore();

        this.dimStart = new int[this.dimensions.length];
        this.dimEnd = new int[this.dimensions.length];
        for(int i=0;i<this.dimensions.length;i++){
            dimStart[i] = (int)bbox.getMin(i);
            dimEnd[i] = (int)bbox.getMax(i);
            coordinate[i] = dimStart[i];
        }
    }

    public int[] getCoordinate() {
        return coordinate;
    }

    /**
     * Move to the newt coordinate.
     * {@code finished} property is set to false when end is reached.
     *
     *
     */
    protected void moveNext(){
        if(finished) return;
        if(start){ start = false; return;}

        coordinate[0]++;
        for(int i=0;i<this.dimensions.length;i++){
            if(coordinate[i]>=dimEnd[i]){
                //get back to this dimension start
                coordinate[i]=dimStart[i];
                if(i<dimensions.length-1){
                    //iterator next dimension
                    coordinate[i+1]++;
                }else{
                    //we have finish
                    finished = true;
                }
            }
        }
    }

    public Object next() {
        moveNext();
        if(finished) return null;
        return sm.getTuple(coordinate, container);
    }

    public boolean[] nextAsBoolean(boolean[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleBoolean(coordinate, buffer);
    }

    public byte[] nextAsByte(byte[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleByte(coordinate, buffer);
    }

    public int[] nextAsUByte(int[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleUByte(coordinate, buffer);
    }

    public short[] nextAsShort(short[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleShort(coordinate, buffer);
    }

    public int[] nextAsUShort(int[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleUShort(coordinate, buffer);
    }

    public int[] nextAsInt(int[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleInt(coordinate, buffer);
    }

    public long[] nextAsUInt(long[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleUInt(coordinate, buffer);
    }

    public long[] nextAsLong(long[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleLong(coordinate, buffer);
    }

    public float[] nextAsFloat(float[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleFloat(coordinate, buffer);
    }

    public double[] nextAsDouble(double[] buffer) {
        moveNext();
        if(finished) return null;
        return sm.getTupleDouble(coordinate, buffer);
    }

}
