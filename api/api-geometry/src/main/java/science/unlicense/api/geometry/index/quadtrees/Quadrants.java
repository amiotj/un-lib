
package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.index.Dimensions;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * Functions to produce new quadrants from an existing pair of corners.
 * 
 * @author Mark Raynsford
 */
public final class Quadrants{
    
  /**
   * <p>
   * Split a quadrant defined by the two points <code>lower</code> and
   * <code>upper</code> into four quadrants.
   * </p>
   *
   * @param lower
   *          The lower corner
   * @param upper
   *          The upper corner
   * @return A set of newly split quadrants.
   */

  public static Quadrants split(
    final Tuple lower,
    final Tuple upper)
  {
    final double size_x = Dimensions.getSpanSizeX(lower, upper);
    final double size_y = Dimensions.getSpanSizeY(lower, upper);

    assert size_x >= 2;
    assert size_y >= 2;

    final double[] x_spans = new double[4];
    final double[] y_spans = new double[4];

    Dimensions.split1D(lower.getX(), upper.getX(), x_spans);
    Dimensions.split1D(lower.getY(), upper.getY(), y_spans);

    final Vector in_x0y0_lower = new Vector(x_spans[0], y_spans[0]);
    final Vector in_x0y1_lower = new Vector(x_spans[0], y_spans[2]);
    final Vector in_x1y0_lower = new Vector(x_spans[2], y_spans[0]);
    final Vector in_x1y1_lower = new Vector(x_spans[2], y_spans[2]);
    final Vector in_x0y0_upper = new Vector(x_spans[1], y_spans[1]);
    final Vector in_x0y1_upper = new Vector(x_spans[1], y_spans[3]);
    final Vector in_x1y0_upper = new Vector(x_spans[3], y_spans[1]);
    final Vector in_x1y1_upper = new Vector(x_spans[3], y_spans[3]);

    return new Quadrants(
      in_x0y0_lower,
      in_x0y0_upper,
      in_x0y1_lower,
      in_x0y1_upper,
      in_x1y0_lower,
      in_x1y0_upper,
      in_x1y1_lower,
      in_x1y1_upper);
  }

  private final Vector x0y0_lower;
  private final Vector x0y0_upper;
  private final Vector x0y1_lower;
  private final Vector x0y1_upper;
  private final Vector x1y0_lower;
  private final Vector x1y0_upper;
  private final Vector x1y1_lower;
  private final Vector x1y1_upper;

  private Quadrants(
    final Vector in_x0y0_lower,
    final Vector in_x0y0_upper,
    final Vector in_x0y1_lower,
    final Vector in_x0y1_upper,
    final Vector in_x1y0_lower,
    final Vector in_x1y0_upper,
    final Vector in_x1y1_lower,
    final Vector in_x1y1_upper)
  {
    this.x0y0_lower = in_x0y0_lower;
    this.x0y0_upper = in_x0y0_upper;
    this.x0y1_lower = in_x0y1_lower;
    this.x0y1_upper = in_x0y1_upper;
    this.x1y0_lower = in_x1y0_lower;
    this.x1y0_upper = in_x1y0_upper;
    this.x1y1_lower = in_x1y1_lower;
    this.x1y1_upper = in_x1y1_upper;
  }

  /**
   * @return The lower <code>(x0, y0)</code> corner
   */

  public Vector getX0Y0Lower()
  {
    return this.x0y0_lower;
  }

  /**
   * @return The upper <code>(x0, y0)</code> corner
   */

  public Vector getX0Y0Upper()
  {
    return this.x0y0_upper;
  }

  /**
   * @return The lower <code>(x0, y1)</code> corner
   */

  public Vector getX0Y1Lower()
  {
    return this.x0y1_lower;
  }

  /**
   * @return The upper <code>(x0, y1)</code> corner
   */

  public Vector getX0Y1Upper()
  {
    return this.x0y1_upper;
  }

  /**
   * @return The lower <code>(x1, y0)</code> corner
   */

  public Vector getX1Y0Lower()
  {
    return this.x1y0_lower;
  }

  /**
   * @return The upper <code>(x1, y0)</code> corner
   */

  public Vector getX1Y0Upper()
  {
    return this.x1y0_upper;
  }

  /**
   * @return The lower <code>(x1, y1)</code> corner
   */

  public Vector getX1Y1Lower()
  {
    return this.x1y1_lower;
  }

  /**
   * @return The upper <code>(x1, y1)</code> corner
   */

  public Vector getX1Y1Upper()
  {
    return this.x1y1_upper;
  }
}
