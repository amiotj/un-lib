
package science.unlicense.api.geometry.operation;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public interface Operation {
    
    Chars getName();
    
    /**
     * Tolerance for calculations.
     * @return double
     */
    double getEpsilon();
    
}
