package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.math.Tuple;

/**
 * <p>
 * The type of procedures used when iterating over octants in octtrees.
 * </p>
 *
 * @param <E> The type of raised exceptions.
 * @author Mark Raynsford
 */
public interface OctTreeTraversalType<E extends Throwable> {

    /**
     * Visit an octant.
     *
     * @param depth The depth of the octant.
     * @param lower The lower corner of the octant.
     * @param upper The upper corner of the octant.
     * @throws E If required.
     */
    void visit(final int depth,final Tuple lower,final Tuple upper) throws E;
    
}
