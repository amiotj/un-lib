package science.unlicense.api.geometry.index.octtrees;

import java.util.SortedSet;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.predicate.Predicate;

/**
 * The interface provided by octtree implementations.
 *
 * @param <T> The precise type of octtree members.
 * @author Mark Raynsford
 */
public interface OctTreeType<T extends OctTreeMemberType<T>> {

    /**
     * Delete all objects, if any, contained within the octtree.
     */

    void octTreeClear();

    /**
     * @return The X axis value of the lower corner of the octtree.
     */
    double octTreeGetPositionX();

    /**
     * @return The Y axis value of the lower corner of the octtree.
     */
    double octTreeGetPositionY();

    /**
     * @return The Z axis value of the lower corner of the octtree.
     */
    double octTreeGetPositionZ();

    /**
     * @return The maximum size of the octtree on the X axis.
     */
    double octTreeGetSizeX();

    /**
     * @return The maximum size of the octtree on the Y axis.
     */
    double octTreeGetSizeY();

    /**
     * @return The maximum size of the octtree on the Z axis.
     */
    double octTreeGetSizeZ();

    /**
     * Insert the object <code>item</code> into the octtree.
     * <p>
     * The function returns <code>false</code> if the object could not be inserted for any reason (perhaps due to being too large).
     * </p>
     *
     * @param item The object to insert
     *
     * @return <code>true</code> if the object was inserted
     * @throws InvalidArgumentException If the object's bounding volume is not <i>well-formed</i>.
     *
     * @see com.io7m.jspatial.BoundingVolumeCheck#isWellFormed(BBox)
     */
    boolean octTreeInsert(
            T item)
            throws InvalidArgumentException;

    /**
     * Pass each object in the octtree to <code>f.call()</code>, in no particular order. Iteration stops if <code>f.call()</code> returns <code>false</code>, or
     * raises an exception.
     *
     * @param f The function that will receive each object
     */
   void octTreeIterateObjects(Predicate f) throws Exception;

    /**
     * Returns the objects intersected by the ray <code>ray</code> in <code>items</code>.
     *
     * The objects are returned in order of increasing scalar distance from the origin of <code>ray</code>. That is, the nearest object to the origin of
     * <code>ray</code> will be the first item in <code>items</code>.
     *
     * @see com.io7m.jtensors.Vector#distance(com.io7m.jtensors.Vector, com.io7m.jtensors.Vector)
     *
     * @param ray The ray
     * @param items The returned objects
     * @throws InvalidArgumentException If the object's bounding volume is not <i>well-formed</i>.
     *
     * @see com.io7m.jspatial.BoundingVolumeCheck#isWellFormed(BBox)
     */
    void octTreeQueryRaycast(
            Ray ray,
            SortedSet<OctTreeRaycastResult<T>> items)
            throws InvalidArgumentException;

    /**
     * Returns all objects in the tree that are completely contained within <code>volume</code>, saving the results to <code>items</code>.
     *
     * @param volume The volume to examine
     * @param items The returned items
     * @throws InvalidArgumentException If the object's bounding volume is not <i>well-formed</i>.
     *
     * @see com.io7m.jspatial.BoundingVolumeCheck#isWellFormed(BBox)
     */
    void octTreeQueryVolumeContaining(
            BBox volume,
            SortedSet<T> items)
            throws InvalidArgumentException;

    /**
     * Returns all objects in the tree that are overlapped by <code>volume</code>, saving the results to <code>items</code>.
     *
     * @param volume The volume to examine
     * @param items The returned items
     * @throws InvalidArgumentException If the object's bounding volume is not <i>well-formed</i>.
     *
     * @see com.io7m.jspatial.BoundingVolumeCheck#isWellFormed(BBox)
     */
    void octTreeQueryVolumeOverlapping(
            BBox volume,
            SortedSet<T> items)
            throws InvalidArgumentException;

    /**
     * Remove the object <code>item</code> from the octtree.
     * <p>
     * The function returns <code>false</code> if the object could not be removed for any reason (perhaps due to not being in the tree in the first place).
     * </p>
     *
     * @param item The object to remove
     *
     * @return <code>true</code> if the object was removed
     * @throws InvalidArgumentException If the object's bounding volume is not <i>well-formed</i>.
     *
     * @see com.io7m.jspatial.BoundingVolumeCheck#isWellFormed(BBox)
     */
    boolean octTreeRemove(
            T item)
            throws InvalidArgumentException;

    /**
     * Pass each node of the given octtree to <code>traversal.visit()</code>, in depth-first order.
     *
     * @param traversal The traversal
     * @throws E Propagated from <code>traversal.visit()</code>
     * @param <E> The type of raised exceptions
     */
    <E extends Throwable> void octTreeTraverse(
            OctTreeTraversalType<E> traversal)
            throws E;

}
