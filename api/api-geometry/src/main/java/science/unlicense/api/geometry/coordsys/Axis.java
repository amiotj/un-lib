
package science.unlicense.api.geometry.coordsys;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.unit.Unit;

/**
 *
 * @author Johann Sorel
 */
public class Axis extends CObject {

    private final Direction direction;
    private final Unit unit;

    public Axis() {
        this.direction = null;
        this.unit = null;
    }

    public Axis(Direction direction, Unit unit) {
        this.direction = direction;
        this.unit = unit;
    }

    public Direction getDirection() {
        return direction;
    }

    public Unit getUnit() {
        return unit;
    }

    public Chars toChars() {
        return new Chars("Axi[").concat(direction.getName()).concat(',').concat(unit.toChars()).concat(']');
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 67 * hash + (this.direction != null ? this.direction.hashCode() : 0);
        hash = 67 * hash + (this.unit != null ? this.unit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Axis other = (Axis) obj;
        if (this.direction != other.direction && (this.direction == null || !this.direction.equals(other.direction))) {
            return false;
        }
        return !(this.unit != other.unit && (this.unit == null || !this.unit.equals(other.unit)));
    }
    
    
    
}
