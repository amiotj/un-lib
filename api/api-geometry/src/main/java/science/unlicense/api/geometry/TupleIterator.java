
package science.unlicense.api.geometry;

/**
 * Tuple iterator.
 *
 * @author Johann Sorel
 */
public interface TupleIterator {

    /**
     * Get the current tuple coodinate.
     * @return int[], returns always the same container
     */
    int[] getCoordinate();

    /**
     * Get the next tuple. null if no more tuples.
     * @return new tuple value, returns always the same container
     */
    Object next();

    /**
     * Get the next tuple as boolean array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    boolean[] nextAsBoolean(boolean[] buffer);

    /**
     * Get the next tuple as byte array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    byte[] nextAsByte(byte[] buffer);

    /**
     * Get the next tuple as UByte array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    int[] nextAsUByte(int[] buffer);

    /**
     * Get the next tuple as Short array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    short[] nextAsShort(short[] buffer);

    /**
     * Get the next tuple as UShort array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    int[] nextAsUShort(int[] buffer);

    /**
     * Get the next tuple as int array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    int[] nextAsInt(int[] buffer);

    /**
     * Get the next tuple as UInt array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    long[] nextAsUInt(long[] buffer);

    /**
     * Get the next tuple as long array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    long[] nextAsLong(long[] buffer);

    /**
     * Get the next tuple as float array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    float[] nextAsFloat(float[] buffer);

    /**
     * Get the next tuple as double array. null if no more tuples.
     * @param buffer not null
     * @return new tuple value, returns given array or null.
     */
    double[] nextAsDouble(double[] buffer);

}
