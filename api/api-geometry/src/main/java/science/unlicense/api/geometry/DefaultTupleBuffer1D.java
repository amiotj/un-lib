
package science.unlicense.api.geometry;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.DataCursor;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 * Default tuple buffer implementation where each tuple have only 1 dimension.
 *
 * @author Johann Sorel
 */
public class DefaultTupleBuffer1D extends DefaultTupleBuffer implements TupleBuffer1D {

    public DefaultTupleBuffer1D(byte[] buffer, int nbSample) {
        this(DefaultBufferFactory.wrap(buffer), Primitive.TYPE_UBYTE, nbSample, buffer.length/nbSample);
    }
    
    public DefaultTupleBuffer1D(int[] buffer, int nbSample) {
        this(DefaultBufferFactory.wrap(buffer), Primitive.TYPE_INT, nbSample, buffer.length/nbSample);
    }
    
    public DefaultTupleBuffer1D(float[] buffer, int nbSample) {
        this(DefaultBufferFactory.wrap(buffer), Primitive.TYPE_FLOAT, nbSample, buffer.length/nbSample);
    }
    
    public DefaultTupleBuffer1D(double[] buffer, int nbSample) {
        this(DefaultBufferFactory.wrap(buffer), Primitive.TYPE_DOUBLE, nbSample, buffer.length/nbSample);
    }
    
    /**
     * Constructor for 1D buffer sample.
     */
    public DefaultTupleBuffer1D(int sampleType, int nbSample, long size) {
        super(DefaultBufferFactory.create(size*nbSample, sampleType, NumberEncoding.BIG_ENDIAN),
                sampleType,nbSample,new Extent.Long(size));
    }
    
    /**
     * Constructor for 1D buffer sample.
     */
    public DefaultTupleBuffer1D(Buffer bank,int sampleType, int nbSample, long size) {
        super(bank,sampleType,nbSample,new Extent.Long(size));
    }

    public int getDimension() {
        //NOTE : should it be long or int ?
        return (int) dimensions.getL(0);
    }

    public DefaultTupleBuffer1D copy() {
        return (DefaultTupleBuffer1D) super.copy();
    }

    public DefaultTupleBuffer1D copy(Buffer bank) {
        return new DefaultTupleBuffer1D(bank, sampleType,
                nbSample, dimensions.getL(0));
    }

    public DefaultTupleBuffer create(Extent.Long dimensions,BufferFactory factory) {
        return new DefaultTupleBuffer1D(createPrimitiveBuffer(dimensions,factory), 
                sampleType, nbSample, dimensions.getL(0));
    }
    
    public Object getTuple(int coordinate, Object buffer) {
        switch(sampleType){
            case Primitive.TYPE_1_BIT : return getTupleBoolean(coordinate, (boolean[])buffer);
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE : return getTupleByte(coordinate, (byte[])buffer);
            case Primitive.TYPE_UBYTE : return getTupleUByte(coordinate, (int[])buffer);
            case Primitive.TYPE_SHORT : return getTupleShort(coordinate, (short[])buffer);
            case Primitive.TYPE_USHORT : return getTupleUShort(coordinate, (int[])buffer);
            case Primitive.TYPE_INT : return getTupleInt(coordinate, (int[])buffer);
            case Primitive.TYPE_UINT : return getTupleUInt(coordinate, (long[])buffer);
            case Primitive.TYPE_LONG : return getTupleLong(coordinate, (long[])buffer);
            case Primitive.TYPE_FLOAT : return getTupleFloat(coordinate, (float[])buffer);
            case Primitive.TYPE_DOUBLE : return getTupleDouble(coordinate, (double[])buffer);
        }

        throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
    }

    public boolean[] getTupleBoolean(int coordinate, boolean[] buffer) {
        if(sampleType == Primitive.TYPE_1_BIT){
            if(buffer == null) buffer = new boolean[nbSample];
            final int start = getStartIndexInBits(coordinate);
            int byteOffset = start/8;
            int bitoffset = start%8;
            for(int i=0;i<nbSample;i++){
                byte b = bank.readByte(byteOffset);
                buffer[i] = (b & (1 << (7-bitoffset))) > 0;

                //prepare next iteration
                if(bitoffset == 7){
                    byteOffset++;
                    bitoffset=0;
                }else{
                    bitoffset++;
                }
            }
            return buffer;
        }
        return super.getTupleBoolean(new int[]{coordinate}, buffer);
    }

    public byte[] getTupleByte(int coordinate, byte[] buffer) {
        if(sampleType == Primitive.TYPE_BYTE){
            if(buffer == null) buffer = new byte[nbSample];
            int k = getStartIndexInByte(coordinate);
            for(int i=0;i<nbSample;i++,k+=1)buffer[i] = bank.readByte(k);
            return buffer;
        }else if(sampleType == Primitive.TYPE_1_BIT){
            if(buffer == null) buffer = new byte[nbSample];
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;
            
            final DataCursor cursor = bank.cursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for(int i=0;i<nbSample;i++){
                buffer[i] = (byte)cursor.readBit(1);
            }
            
            return buffer;
        }else if(sampleType == Primitive.TYPE_2_BIT){
            if(buffer == null) buffer = new byte[nbSample];
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;
            
            final DataCursor cursor = bank.cursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for(int i=0;i<nbSample;i++){
                buffer[i] = (byte)cursor.readBit(2);
            }
            
            return buffer;
        }else if(sampleType == Primitive.TYPE_4_BIT){
            if(buffer == null) buffer = new byte[nbSample];
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;
            
            final DataCursor cursor = bank.cursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for(int i=0;i<nbSample;i++){
                buffer[i] = (byte)cursor.readBit(4);
            }
            
            return buffer;
        }
        return super.getTupleByte(new int[]{coordinate}, buffer);
    }

    public int[] getTupleUByte(int coordinate, int[] buffer) {
        if(sampleType == Primitive.TYPE_UBYTE){
            if(buffer == null) buffer = new int[nbSample];
            final int start = getStartIndexInByte(coordinate);
            for(int i=0,k=start;i<nbSample;i++,k+=1)buffer[i] = bank.readByte(k) & 0xff;
            return buffer;
        }
        return super.getTupleUByte(new int[]{coordinate}, buffer);
    }

    public short[] getTupleShort(int coordinate, short[] buffer) {
        if(sampleType == Primitive.TYPE_SHORT){
            if(buffer == null) buffer = new short[nbSample];
            final int start = getStartIndexInByte(coordinate);
            bank.readShort(buffer, start);
            return buffer;
        }
        return super.getTupleShort(new int[]{coordinate}, buffer);
    }

    public int[] getTupleUShort(int coordinate, int[] buffer) {
        if(sampleType == Primitive.TYPE_USHORT){
            if(buffer == null) buffer = new int[nbSample];
            bank.readUShort(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleUShort(new int[]{coordinate}, buffer);
    }

    public int[] getTupleInt(int coordinate, int[] buffer) {
        if(sampleType == Primitive.TYPE_INT){
            if(buffer == null) buffer = new int[nbSample];
            bank.readInt(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleInt(new int[]{coordinate}, buffer);
    }

    public long[] getTupleUInt(int coordinate, long[] buffer) {
        if(sampleType == Primitive.TYPE_UINT){
            if(buffer == null) buffer = new long[nbSample];
            bank.readUInt(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleUInt(new int[]{coordinate}, buffer);
    }

    public long[] getTupleLong(int coordinate, long[] buffer) {
        if(sampleType == Primitive.TYPE_LONG){
            if(buffer == null) buffer = new long[nbSample];
            bank.readLong(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleLong(new int[]{coordinate}, buffer);
    }

    public float[] getTupleFloat(int coordinate, float[] buffer) {
        if(sampleType == Primitive.TYPE_FLOAT){
            if(buffer == null) buffer = new float[nbSample];
            bank.readFloat(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleFloat(new int[]{coordinate}, buffer);
    }

    public double[] getTupleDouble(int coordinate, double[] buffer) {
        if(sampleType == Primitive.TYPE_UBYTE){
            if(buffer == null) buffer = new double[nbSample];
            final int start = getStartIndexInByte(coordinate);
            for(int i=0,k=start;i<nbSample;i++,k+=1)buffer[i] = (bank.readByte(k) & 0xff);
            return buffer;
        }else if(sampleType == Primitive.TYPE_DOUBLE){
            if(buffer == null) buffer = new double[nbSample];
            bank.readDouble(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleDouble(new int[]{coordinate}, buffer);
    }

    @Override
    public void setTuple(int coordinate, Object sample) {
        setTuple(new int[]{coordinate}, sample);
    }
}
