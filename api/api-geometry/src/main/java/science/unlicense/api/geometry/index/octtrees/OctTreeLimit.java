package science.unlicense.api.geometry.index.octtrees;

import java.util.SortedSet;
import java.util.TreeSet;

import science.unlicense.api.CObject;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.geometry.index.BoundingVolumeCheck;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.index.Dimensions;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.math.Vector;

/**
 * <p>
 * An octtree implementation based on {@link OctTreeBasic} but implementing a
 * minimum size limit on octants.
 * </p>
 *
 * @param <T> The precise type of octtree members.
 * @author Mark Raynsford
 */
public final class OctTreeLimit<T extends OctTreeMemberType<T>> extends CObject implements OctTreeType<T> {

    private final class Octant extends OctantType {

        private boolean leaf;
        private final SortedSet<T> octant_objects;
        private final double size_x;
        private final double size_y;
        private final double size_z;
        private Octant x0y0z0;
        private Octant x0y0z1;
        private Octant x0y1z0;
        private Octant x0y1z1;
        private Octant x1y0z0;
        private Octant x1y0z1;
        private Octant x1y1z0;
        private Octant x1y1z1;

        public Octant(
                final Vector in_lower,
                final Vector in_upper) {
            this.lower.set(in_lower);
            this.upper.set(in_upper);
            this.size_x = Dimensions.getSpanSizeX(this.lower, this.upper);
            this.size_y = Dimensions.getSpanSizeY(this.lower, this.upper);
            this.size_z = Dimensions.getSpanSizeZ(this.lower, this.upper);

            this.octant_objects = new TreeSet<T>();

            this.leaf = true;
            this.x0y0z0 = null;
            this.x1y0z0 = null;
            this.x0y1z0 = null;
            this.x1y1z0 = null;
            this.x0y0z1 = null;
            this.x1y0z1 = null;
            this.x0y1z1 = null;
            this.x1y1z1 = null;
        }

        private boolean canSplit() {
            final double mx = OctTreeLimit.this.minimum_size_x;
            final double my = OctTreeLimit.this.minimum_size_y;
            final double mz = OctTreeLimit.this.minimum_size_z;
            return this.size_x >= mx * 2
                    && this.size_y >= my * 2
                    && this.size_z >= mz * 2;
        }

        void clear() {
            this.octant_objects.clear();
            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.clear();
                assert this.x1y0z0 != null;
                this.x1y0z0.clear();
                assert this.x0y1z0 != null;
                this.x0y1z0.clear();
                assert this.x1y1z0 != null;
                this.x1y1z0.clear();

                assert this.x0y0z1 != null;
                this.x0y0z1.clear();
                assert this.x1y0z1 != null;
                this.x1y0z1.clear();
                assert this.x0y1z1 != null;
                this.x0y1z1.clear();
                assert this.x1y1z1 != null;
                this.x1y1z1.clear();
            }
        }

        private void collectRecursive(
                final SortedSet<T> items) {
            items.addAll(this.octant_objects);
            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.collectRecursive(items);
                assert this.x0y1z0 != null;
                this.x0y1z0.collectRecursive(items);
                assert this.x1y0z0 != null;
                this.x1y0z0.collectRecursive(items);
                assert this.x1y1z0 != null;
                this.x1y1z0.collectRecursive(items);

                assert this.x0y0z1 != null;
                this.x0y0z1.collectRecursive(items);
                assert this.x0y1z1 != null;
                this.x0y1z1.collectRecursive(items);
                assert this.x1y0z1 != null;
                this.x1y0z1.collectRecursive(items);
                assert this.x1y1z1 != null;
                this.x1y1z1.collectRecursive(items);
            }
        }

        /**
         * Attempt to insert <code>item</code> into this node, or the children
         * of this node.
         *
         * @return <code>true</code>, if the item was inserted and
         * <code>false</code> otherwise.
         */
        boolean insert(
                final T item) {
            return this.insertBase(item);
        }

        /**
         * Insertion base case: item may or may not fit within node.
         */
        protected boolean insertBase(
                final T item) {
            if (OctTreeLimit.this.objects_all.contains(item)) {
                return false;
            }
            if (BoundingVolumeCheck.containedWithin(this, item)) {
                return this.insertStep(item);
            }
            return false;
        }

        /**
         * Insert the given object into the current octant's object list, and
         * also inserted into the "global" object list.
         */
        protected boolean insertObject(
                final T item) {
            OctTreeLimit.this.objects_all.add(item);
            this.octant_objects.add(item);
            return true;
        }

        /**
         * Insertion inductive case: item fits within node, but may fit more
         * precisely within a child node.
         */
        // CHECKSTYLE:OFF
        private boolean insertStep(
                // CHECKSTYLE:ON
                final T item) {
            /**
             * The object can fit in this node, but perhaps it is possible to
             * fit it more precisely within one of the child nodes.
             */

            /**
             * If this node is a leaf, and is large enough to split, do so.
             */
            if (this.leaf == true) {
                if (this.canSplit()) {
                    this.split();
                } else {

                    /**
                     * The node is a leaf, but cannot be split further. Insert
                     * directly.
                     */
                    return this.insertObject(item);
                }
            }

            /**
             * See if the object will fit in any of the child nodes.
             */
            assert this.leaf == false;

            assert this.x0y0z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y0z0, item)) {
                assert this.x0y0z0 != null;
                return this.x0y0z0.insertStep(item);
            }
            assert this.x1y0z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y0z0, item)) {
                assert this.x1y0z0 != null;
                return this.x1y0z0.insertStep(item);
            }
            assert this.x0y1z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y1z0, item)) {
                assert this.x0y1z0 != null;
                return this.x0y1z0.insertStep(item);
            }
            assert this.x1y1z0 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y1z0, item)) {
                assert this.x1y1z0 != null;
                return this.x1y1z0.insertStep(item);
            }

            assert this.x0y0z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y0z1, item)) {
                assert this.x0y0z1 != null;
                return this.x0y0z1.insertStep(item);
            }
            assert this.x1y0z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y0z1, item)) {
                assert this.x1y0z1 != null;
                return this.x1y0z1.insertStep(item);
            }
            assert this.x0y1z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x0y1z1, item)) {
                assert this.x0y1z1 != null;
                return this.x0y1z1.insertStep(item);
            }
            assert this.x1y1z1 != null;
            if (BoundingVolumeCheck.containedWithin(this.x1y1z1, item)) {
                assert this.x1y1z1 != null;
                return this.x1y1z1.insertStep(item);
            }

            /**
             * Otherwise, insert the object into this node.
             */
            return this.insertObject(item);
        }

        void raycast(
                final Ray ray,
                final SortedSet<OctTreeRaycastResult<T>> items) {
            if (BoundingVolumeCheck.rayBoxIntersects(
                    ray,
                    this.lower.getX(),
                    this.lower.getY(),
                    this.lower.getZ(),
                    this.upper.getX(),
                    this.upper.getY(),
                    this.upper.getZ())) {

                for (final T object : this.octant_objects) {
                    final Tuple object_lower
                            = object.getLower();
                    final Tuple object_upper
                            = object.getUpper();

                    if (BoundingVolumeCheck.rayBoxIntersects(
                            ray,
                            object_lower.getX(),
                            object_lower.getY(),
                            object_lower.getZ(),
                            object_upper.getX(),
                            object_upper.getY(),
                            object_upper.getZ())) {

                        final OctTreeRaycastResult<T> r
                                = new OctTreeRaycastResult<T>(object, Distance.distance(
                                        new Vector(
                                                object_lower.getX(),
                                                object_lower.getY(),
                                                object_lower.getZ()),
                                        ray.getPosition()));
                        items.add(r);
                    }
                }

                if (this.leaf == false) {
                    assert this.x0y0z0 != null;
                    this.x0y0z0.raycast(ray, items);
                    assert this.x0y1z0 != null;
                    this.x0y1z0.raycast(ray, items);
                    assert this.x1y0z0 != null;
                    this.x1y0z0.raycast(ray, items);
                    assert this.x1y1z0 != null;
                    this.x1y1z0.raycast(ray, items);

                    assert this.x0y0z1 != null;
                    this.x0y0z1.raycast(ray, items);
                    assert this.x0y1z1 != null;
                    this.x0y1z1.raycast(ray, items);
                    assert this.x1y0z1 != null;
                    this.x1y0z1.raycast(ray, items);
                    assert this.x1y1z1 != null;
                    this.x1y1z1.raycast(ray, items);
                }
            }
        }

        boolean remove(
                final T item) {
            if (OctTreeLimit.this.objects_all.contains(item) == false) {
                return false;
            }

            /**
             * If an object is in objects_all, then it must be within the bounds
             * of the tree, according to insert().
             */
            assert BoundingVolumeCheck.containedWithin(this, item);
            return this.removeStep(item);
        }

        // CHECKSTYLE:OFF
        private boolean removeStep(
                // CHECKSTYLE:ON
                final T item) {
            if (this.octant_objects.contains(item)) {
                this.octant_objects.remove(item);
                OctTreeLimit.this.objects_all.remove(item);
                return true;
            }

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y0z0, item)) {
                    assert this.x0y0z0 != null;
                    return this.x0y0z0.removeStep(item);
                }
                assert this.x1y0z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y0z0, item)) {
                    assert this.x1y0z0 != null;
                    return this.x1y0z0.removeStep(item);
                }
                assert this.x0y1z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y1z0, item)) {
                    assert this.x0y1z0 != null;
                    return this.x0y1z0.removeStep(item);
                }
                assert this.x1y1z0 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y1z0, item)) {
                    assert this.x1y1z0 != null;
                    return this.x1y1z0.removeStep(item);
                }

                assert this.x0y0z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y0z1, item)) {
                    assert this.x0y0z1 != null;
                    return this.x0y0z1.removeStep(item);
                }
                assert this.x1y0z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y0z1, item)) {
                    assert this.x1y0z1 != null;
                    return this.x1y0z1.removeStep(item);
                }
                assert this.x0y1z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x0y1z1, item)) {
                    assert this.x0y1z1 != null;
                    return this.x0y1z1.removeStep(item);
                }
                assert this.x1y1z1 != null;
                if (BoundingVolumeCheck.containedWithin(this.x1y1z1, item)) {
                    assert this.x1y1z1 != null;
                    return this.x1y1z1.removeStep(item);
                }
            }

            /**
             * The object must be in the tree, according to objects_all.
             * Therefore it must be in this node, or one of the child octants.
             */
            throw new RuntimeException("Unreachable code");
        }

        /**
         * Split this node into four octants.
         */
        private void split() {
            assert this.canSplit();

            final Octants q = Octants.split(this.lower, this.upper);
            this.x0y0z0 = new Octant(q.getX0Y0Z0Lower(), q.getX0Y0Z0Upper());
            this.x0y1z0 = new Octant(q.getX0Y1Z0Lower(), q.getX0Y1Z0Upper());
            this.x1y0z0 = new Octant(q.getX1Y0Z0Lower(), q.getX1Y0Z0Upper());
            this.x1y1z0 = new Octant(q.getX1Y1Z0Lower(), q.getX1Y1Z0Upper());
            this.x0y0z1 = new Octant(q.getX0Y0Z1Lower(), q.getX0Y0Z1Upper());
            this.x0y1z1 = new Octant(q.getX0Y1Z1Lower(), q.getX0Y1Z1Upper());
            this.x1y0z1 = new Octant(q.getX1Y0Z1Lower(), q.getX1Y0Z1Upper());
            this.x1y1z1 = new Octant(q.getX1Y1Z1Lower(), q.getX1Y1Z1Upper());
            this.leaf = false;
        }

        <E extends Throwable> void traverse(
                final int depth,
                final OctTreeTraversalType<E> traversal)
                throws E {
            traversal.visit(depth, this.lower, this.upper);

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.traverse(depth + 1, traversal);
                assert this.x1y0z0 != null;
                this.x1y0z0.traverse(depth + 1, traversal);
                assert this.x0y1z0 != null;
                this.x0y1z0.traverse(depth + 1, traversal);
                assert this.x1y1z0 != null;
                this.x1y1z0.traverse(depth + 1, traversal);

                assert this.x0y0z1 != null;
                this.x0y0z1.traverse(depth + 1, traversal);
                assert this.x1y0z1 != null;
                this.x1y0z1.traverse(depth + 1, traversal);
                assert this.x0y1z1 != null;
                this.x0y1z1.traverse(depth + 1, traversal);
                assert this.x1y1z1 != null;
                this.x1y1z1.traverse(depth + 1, traversal);
            }
        }

        void volumeContaining(
                final BBox volume,
                final SortedSet<T> items) {
            /**
             * If <code>volume</code> completely contains this octant, collect
             * everything in this octant and all children of this octant.
             */

            if (BoundingVolumeCheck.containedWithin(volume, this)) {
                this.collectRecursive(items);
                return;
            }

            /**
             * Otherwise, <code>volume</code> may be overlapping this octant and
             * therefore some items may still be contained within
             * <code>volume</code>.
             */
            for (final T object : this.octant_objects) {
                assert object != null;
                if (BoundingVolumeCheck.containedWithin(volume, object)) {
                    items.add(object);
                }
            }

            if (this.leaf == false) {
                assert this.x0y0z0 != null;
                this.x0y0z0.volumeContaining(volume, items);
                assert this.x0y1z0 != null;
                this.x0y1z0.volumeContaining(volume, items);
                assert this.x1y0z0 != null;
                this.x1y0z0.volumeContaining(volume, items);
                assert this.x1y1z0 != null;
                this.x1y1z0.volumeContaining(volume, items);

                assert this.x0y0z1 != null;
                this.x0y0z1.volumeContaining(volume, items);
                assert this.x0y1z1 != null;
                this.x0y1z1.volumeContaining(volume, items);
                assert this.x1y0z1 != null;
                this.x1y0z1.volumeContaining(volume, items);
                assert this.x1y1z1 != null;
                this.x1y1z1.volumeContaining(volume, items);
            }
        }

        void volumeOverlapping(
                final BBox volume,
                final SortedSet<T> items) {
            /**
             * If <code>volume</code> overlaps this octant, test each object
             * against <code>volume</code>.
             */

            if (BoundingVolumeCheck.overlapsVolume(volume, this)) {
                for (final T object : this.octant_objects) {
                    assert object != null;
                    if (BoundingVolumeCheck.overlapsVolume(volume, object)) {
                        items.add(object);
                    }
                }

                if (this.leaf == false) {
                    assert this.x0y0z0 != null;
                    this.x0y0z0.volumeOverlapping(volume, items);
                    assert this.x1y0z0 != null;
                    this.x1y0z0.volumeOverlapping(volume, items);
                    assert this.x0y1z0 != null;
                    this.x0y1z0.volumeOverlapping(volume, items);
                    assert this.x1y1z0 != null;
                    this.x1y1z0.volumeOverlapping(volume, items);

                    assert this.x0y0z1 != null;
                    this.x0y0z1.volumeOverlapping(volume, items);
                    assert this.x1y0z1 != null;
                    this.x1y0z1.volumeOverlapping(volume, items);
                    assert this.x0y1z1 != null;
                    this.x0y1z1.volumeOverlapping(volume, items);
                    assert this.x1y1z1 != null;
                    this.x1y1z1.volumeOverlapping(volume, items);
                }
            }
        }
    }

    /**
     * Construct a new octtree with the given size and position.
     *
     * @param size The size.
     * @param position The position.
     * @param limit_size The minimum octant size.
     *
     * @return A new octtree.
     * @param <T> The precise type of octtree members.
     */
    public static <T extends OctTreeMemberType<T>> OctTreeType<T> newOctTree(
            final Vector size,
            final Vector position,
            final Vector limit_size) {
        return new OctTreeLimit<T>(position, size, limit_size);
    }

    private final double minimum_size_x;
    private final double minimum_size_y;
    private final double minimum_size_z;
    private final SortedSet<T> objects_all;
    private final Octant root;

    private OctTreeLimit(
            final Vector position,
            final Vector size,
            final Vector size_minimum) {
        CObjects.ensureNotNull(position, "Position");
        CObjects.ensureNotNull(size, "Size");
        CObjects.ensureNotNull(size_minimum, "Minimum size");

        OctTreeChecks.checkSize(
                "Octtree size",
                size.getX(),
                size.getY(),
                size.getZ());
        OctTreeChecks.checkSize(
                "Minimum octant size",
                size_minimum.getX(),
                size_minimum.getY(),
                size_minimum.getZ());

        if (size_minimum.getX() > size.getX()) {
            final String s
                    = String.format(
                            "Minimum octant width (%f) is greater than the octtree width (%f)",
                            size_minimum.getX(),
                            size.getX());
            throw new InvalidArgumentException(s);
        }
        if (size_minimum.getY() > size.getY()) {
            final String s
                    = String
                            .format(
                                    "Minimum octant height (%f) is greater than the octtree height (%f)",
                                    size_minimum.getY(),
                                    size.getY());
            throw new InvalidArgumentException(s);
        }
        if (size_minimum.getZ() > size.getZ()) {
            final String s
                    = String.format(
                            "Minimum octant depth (%f) is greater than the octtree depth (%f)",
                            size_minimum.getZ(),
                            size.getZ());
            throw new InvalidArgumentException(s);
        }

        this.minimum_size_x = size_minimum.getX();
        this.minimum_size_y = size_minimum.getY();
        this.minimum_size_z = size_minimum.getZ();
        this.objects_all = new TreeSet<T>();

        final Vector lower = new Vector(position);
        final Vector upper
                = new Vector(
                        (position.getX() + size.getX()) - 1,
                        (position.getY() + size.getY()) - 1,
                        (position.getZ() + size.getZ()) - 1);
        this.root = new Octant(lower, upper);
    }

    @Override
    public void octTreeClear() {
        this.objects_all.clear();
        this.root.clear();
    }

    @Override
    public double octTreeGetPositionX() {
        return this.root.getLower().getX();
    }

    @Override
    public double octTreeGetPositionY() {
        return this.root.getLower().getY();
    }

    @Override
    public double octTreeGetPositionZ() {
        return this.root.getLower().getZ();
    }

    @Override
    public double octTreeGetSizeX() {
        return this.root.size_x;
    }

    @Override
    public double octTreeGetSizeY() {
        return this.root.size_y;
    }

    @Override
    public double octTreeGetSizeZ() {
        return this.root.size_z;
    }

    @Override
    public boolean octTreeInsert(
            final T item) {
        BoundingVolumeCheck.checkWellFormed(item);
        return this.root.insert(item);
    }

    @Override
    public void octTreeIterateObjects(final Predicate f) throws Exception {
        CObjects.ensureNotNull(f, "Function");

        for (final T object : this.objects_all) {
            assert object != null;
            final Boolean r = f.evaluate(object);
            if (r.booleanValue() == false) {
                break;
            }
        }
    }

    @Override
    public void octTreeQueryRaycast(
            final Ray ray,
            final SortedSet<OctTreeRaycastResult<T>> items) {
        CObjects.ensureNotNull(ray, "Ray");
        CObjects.ensureNotNull(items, "Items");
        this.root.raycast(ray, items);
    }

    @Override
    public void octTreeQueryVolumeContaining(
            final BBox volume,
            final SortedSet<T> items) {
        BoundingVolumeCheck.checkWellFormed(volume);
        CObjects.ensureNotNull(items, "Items");
        this.root.volumeContaining(volume, items);
    }

    @Override
    public void octTreeQueryVolumeOverlapping(
            final BBox volume,
            final SortedSet<T> items) {
        BoundingVolumeCheck.checkWellFormed(volume);
        CObjects.ensureNotNull(items, "Items");
        this.root.volumeOverlapping(volume, items);
    }

    @Override
    public boolean octTreeRemove(
            final T item) {
        BoundingVolumeCheck.checkWellFormed(item);
        return this.root.remove(item);
    }

    @Override
    public <E extends Throwable> void octTreeTraverse(
            final OctTreeTraversalType<E> traversal)
            throws E {
        CObjects.ensureNotNull(traversal, "Traversal");
        this.root.traverse(0, traversal);
    }

    @Override
    public Chars toChars() {
        final CharBuffer b = new CharBuffer();
        b.append("[OctTree ");
        b.append(this.root.getLower());
        b.append(" ");
        b.append(this.root.getUpper());
        b.append(" ");
        b.append(this.objects_all);
        b.append("]");
        return b.toChars();
    }
}
