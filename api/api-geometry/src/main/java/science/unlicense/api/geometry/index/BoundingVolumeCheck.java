
package science.unlicense.api.geometry.index;

import science.unlicense.api.CObjects;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.Vector;

/**
 * <p>
 * Overlap and containment checks between bounding volumes.
 * </p>
 *
 * @see BBox
 * @author Mark Raynsford
 */
public final class BoundingVolumeCheck{
    
  /**
   * The result of an overlap or containment check.
   */
  public static enum Result{
      
    /**
     * The object is contained within the query volume.
     */
    RESULT_CONTAINED_WITHIN,

    /**
     * The object does not overlap the query volume.
     */
    RESULT_NO_OVERLAP,

    /**
     * The object is overlapped by the query volume.
     */
    RESULT_OVERLAP,
  }

  /**
   * Determine whether <code>b</code> overlaps <code>a</code>, or is
   * completely contained by <code>a</code>.
   *
   * @param container
   *          The container
   * @param item
   *          The item that may be contained
   * @return The containment result
   */

  public static Result checkAgainst(
    final BBox container,
    final BBox item)
  {
    final double c_x0 = container.getLower().getX();
    final double c_x1 = container.getUpper().getX();
    final double c_y0 = container.getLower().getY();
    final double c_y1 = container.getUpper().getY();
    final double c_z0 = container.getLower().getZ();
    final double c_z1 = container.getUpper().getZ();

    final double i_x0 = item.getLower().getX();
    final double i_x1 = item.getUpper().getX();
    final double i_y0 = item.getLower().getY();
    final double i_y1 = item.getUpper().getY();
    final double i_z0 = item.getLower().getZ();
    final double i_z1 = item.getUpper().getZ();

    // Check for containment
    if (BoundingVolumeCheck.contains(
      c_x0,
      c_x1,
      c_y0,
      c_y1,
      c_z0,
      c_z1,
      i_x0,
      i_x1,
      i_y0,
      i_y1,
      i_z0,
      i_z1)) {
      return Result.RESULT_CONTAINED_WITHIN;
    }

    // Check for overlap.
    if (BoundingVolumeCheck.overlaps(
      c_x0,
      c_x1,
      c_y0,
      c_y1,
      c_z0,
      c_z1,
      i_x0,
      i_x1,
      i_y0,
      i_y1,
      i_z0,
      i_z1)) {
      return Result.RESULT_OVERLAP;
    }

    return Result.RESULT_NO_OVERLAP;
  }

  /**
   * Evaluates {@link #isWellFormed(BBox)} for the given item
   * and raises {@link InvalidArgumentException} if the result is
   * <code>false</code>.
   *
   * @param item
   *          The area.
   */

  public static void checkWellFormed(
    final BBox item)
  {
    if (BoundingVolumeCheck.isWellFormed(item) == false) {
      throw new InvalidArgumentException("Bounding volume is not well-formed");
    }
  }

  /**
   * @param container
   *          The container
   * @param item
   *          The item that may be contained
   * @return <code>true</code> iff <code>item</code> is completely contained
   *         within <code>container</code>.
   */

  public static boolean containedWithin(
    final BBox container,
    final BBox item)
  {
    final double c_x0 = container.getLower().getX();
    final double c_x1 = container.getUpper().getX();
    final double c_y0 = container.getLower().getY();
    final double c_y1 = container.getUpper().getY();
    final double c_z0 = container.getLower().getZ();
    final double c_z1 = container.getUpper().getZ();

    final double i_x0 = item.getLower().getX();
    final double i_x1 = item.getUpper().getX();
    final double i_y0 = item.getLower().getY();
    final double i_y1 = item.getUpper().getY();
    final double i_z0 = item.getLower().getZ();
    final double i_z1 = item.getUpper().getZ();

    return BoundingVolumeCheck.contains(
      c_x0,
      c_x1,
      c_y0,
      c_y1,
      c_z0,
      c_z1,
      i_x0,
      i_x1,
      i_y0,
      i_y1,
      i_z0,
      i_z1);
  }

  /**
   * The area <code>a</code> described by the given vertices contains
   * <code>b</code>.
   *
   * @param a_x0
   *          The X coordinate of the lower corner of <code>a</code>
   * @param a_x1
   *          The X coordinate of the upper corner of <code>a</code>
   * @param a_y0
   *          The Y coordinate of the lower corner of <code>a</code>
   * @param a_y1
   *          The Y coordinate of the upper corner of <code>a</code>
   * @param a_z0
   *          The Z coordinate of the lower corner of <code>a</code>
   * @param a_z1
   *          The Z coordinate of the upper corner of <code>a</code>
   * @param b_x0
   *          The X coordinate of the lower corner of <code>b</code>
   * @param b_x1
   *          The X coordinate of the upper corner of <code>b</code>
   * @param b_y0
   *          The Y coordinate of the lower corner of <code>b</code>
   * @param b_y1
   *          The Y coordinate of the upper corner of <code>b</code>
   * @param b_z0
   *          The Z coordinate of the lower corner of <code>b</code>
   * @param b_z1
   *          The Z coordinate of the upper corner of <code>b</code>
   * @return <code>true</code> if <code>a</code> contains <code>b</code>.
   */

  public static boolean contains(
    final double a_x0,
    final double a_x1,
    final double a_y0,
    final double a_y1,
    final double a_z0,
    final double a_z1,
    final double b_x0,
    final double b_x1,
    final double b_y0,
    final double b_y1,
    final double b_z0,
    final double b_z1)
  {
    final boolean c0 = b_x0 >= a_x0;
    final boolean c1 = b_x1 <= a_x1;
    final boolean c2 = b_y0 >= a_y0;
    final boolean c3 = b_y1 <= a_y1;
    final boolean c4 = b_z0 >= a_z0;
    final boolean c5 = b_z1 <= a_z1;

    return c0 && c1 && c2 && c3 && c4 && c5;
  }

  /**
   * @param container
   *          The volume
   * @return <code>true</code> iff the given bounding volume is well formed.
   *         That is, iff
   *         <code>container.getLower().getX() &lt;= container.getUpper().getX()</code>
   *         and
   *         <code>container.getLower().getY() &lt;= container.getUpper().getY()</code>
   *         and
   *         <code>container.getLower().getZ() &lt;= container.getUpper().getZ()</code>
   *         .
   */

  public static boolean isWellFormed(
    final BBox container)
  {
    CObjects.ensureNotNull(container, "Container");

    final Tuple lower = container.getLower();
    final Tuple upper = container.getUpper();
    if (lower.getX() > upper.getX()) {
      return false;
    }
    if (lower.getY() > upper.getY()) {
      return false;
    }
    if (lower.getZ() > upper.getZ()) {
      return false;
    }
    return true;
  }

  /**
   * The area <code>a</code> described by the given vertices overlaps
   * <code>b</code>.
   *
   * @param a_x0
   *          The X coordinate of the lower corner of <code>a</code>
   * @param a_x1
   *          The X coordinate of the upper corner of <code>a</code>
   * @param a_y0
   *          The Y coordinate of the lower corner of <code>a</code>
   * @param a_y1
   *          The Y coordinate of the upper corner of <code>a</code>
   * @param a_z0
   *          The Z coordinate of the lower corner of <code>a</code>
   * @param a_z1
   *          The Z coordinate of the upper corner of <code>a</code>
   * @param b_x0
   *          The X coordinate of the lower corner of <code>b</code>
   * @param b_x1
   *          The X coordinate of the upper corner of <code>b</code>
   * @param b_y0
   *          The Y coordinate of the lower corner of <code>b</code>
   * @param b_y1
   *          The Y coordinate of the upper corner of <code>b</code>
   * @param b_z0
   *          The Z coordinate of the lower corner of <code>b</code>
   * @param b_z1
   *          The Z coordinate of the upper corner of <code>b</code>
   * @return <code>true</code> if <code>a</code> overlaps <code>b</code>.
   */

  public static boolean overlaps(
    final double a_x0,
    final double a_x1,
    final double a_y0,
    final double a_y1,
    final double a_z0,
    final double a_z1,
    final double b_x0,
    final double b_x1,
    final double b_y0,
    final double b_y1,
    final double b_z0,
    final double b_z1)
  {
    final boolean c0 = a_x0 < b_x1;
    final boolean c1 = a_x1 > b_x0;
    final boolean c2 = a_y0 < b_y1;
    final boolean c3 = a_y1 > b_y0;
    final boolean c4 = a_z0 < b_z1;
    final boolean c5 = a_z1 > b_z0;

    return c0 && c1 && c2 && c3 && c4 && c5;
  }

  /**
   * @param container
   *          The container
   * @param item
   *          The item that may be overlapping
   * @return <code>true</code> iff <code>item</code> overlaps
   *         <code>container</code>.
   */

  public static boolean overlapsVolume(
    final BBox container,
    final BBox item)
  {
    final double c_x0 = container.getLower().getX();
    final double c_x1 = container.getUpper().getX();
    final double c_y0 = container.getLower().getY();
    final double c_y1 = container.getUpper().getY();
    final double c_z0 = container.getLower().getZ();
    final double c_z1 = container.getUpper().getZ();

    final double i_x0 = item.getLower().getX();
    final double i_x1 = item.getUpper().getX();
    final double i_y0 = item.getLower().getY();
    final double i_y1 = item.getUpper().getY();
    final double i_z0 = item.getLower().getZ();
    final double i_z1 = item.getUpper().getZ();

    return BoundingVolumeCheck.overlaps(
      c_x0,
      c_x1,
      c_y0,
      c_y1,
      c_z0,
      c_z1,
      i_x0,
      i_x1,
      i_y0,
      i_y1,
      i_z0,
      i_z1);
  }

  /**
   * <p>Branchless optimization of the Kay-Kajiya slab ray/AABB intersection test
   * by Tavian Barnes.</p>
   * <p>See <a href="http://tavianator.com/2011/05/fast-branchless-raybounding-box-intersections/">tavianator.com</a>.</p>

   * @param ray
   *          The ray
   * @param x0
   *          The lower X coordinate
   * @param y0
   *          The lower Y coordinate
   * @param z0
   *          The lower z coordinate
   * @param x1
   *          The upper X coordinate
   * @param y1
   *          The upper Y coordinate
   * @param z1
   *          The upper z coordinate
   * @return <code>true</code> if the ray intersects the box given by the
   *         corners.
   */

  public static boolean rayBoxIntersects(
    final Ray ray,
    final double x0,
    final double y0,
    final double z0,
    final double x1,
    final double y1,
    final double z1)
  {
    final Tuple ray_origin = ray.getPosition();
    final Vector ray_direction = ray.getDirection();

    final double tx0 = (x0 - ray_origin.getX()) / ray_direction.getX();
    final double tx1 = (x1 - ray_origin.getX()) / ray_direction.getX();

    double tmin = Math.min(tx0, tx1);
    double tmax = Math.max(tx0, tx1);

    final double ty0 = (y0 - ray_origin.getY()) / ray_direction.getY();
    final double ty1 = (y1 - ray_origin.getY()) / ray_direction.getY();

    tmin = Math.max(tmin, Math.min(ty0, ty1));
    tmax = Math.min(tmax, Math.max(ty0, ty1));

    final double tz0 = (z0 - ray_origin.getZ()) / ray_direction.getZ();
    final double tz1 = (z1 - ray_origin.getZ()) / ray_direction.getZ();

    tmin = Math.max(tmin, Math.min(tz0, tz1));
    tmax = Math.min(tmax, Math.max(tz0, tz1));

    return ((tmax >= Math.max(0, tmin)) && (tmin < Double.POSITIVE_INFINITY));
  }

  private BoundingVolumeCheck()
  {
    throw new RuntimeException("Unreachable code");
  }
}
