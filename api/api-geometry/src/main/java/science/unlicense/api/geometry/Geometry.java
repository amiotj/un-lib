
package science.unlicense.api.geometry;

import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.impl.geometry.Point;

/**
 * Top level interface for all geometry type.
 *
 * This package contain the merge result of several specifications :
 * - SVG
 * - OGC WKT/WKB
 * - ISO 13249-3
 * - Postscript
 * - PDF
 * - TTF
 *
 * @author Johann Sorel
 */
public interface Geometry {

    /**
     * Get the geometry number of dimensions.
     * @return number of dimension
     */
    int getDimension();

    /**
     * Get this geometry BoundingBox.
     *
     * @return BoundingBox
     */
    BBox getBoundingBox();

    /**
     * Calculate geometry centroid.
     * 
     * @return centroid point
     */
    Point getCentroid();

    /**
     * Index which may be used to store additional user informations.
     *
     * @return Index, never null
     */
    Dictionary getUserProperties();

    ////////////////////////////////////////////////////////////////////////////
    //common analyze operations ////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    Geometry buffer(double distance) throws OperationException;

     /**
     * Test if given geometry is contained in this one.
     *
     * @param geometry
     * @return true if contained, false otherwise
     * @throws science.unlicense.api.geometry.operation.OperationException
     */
    boolean contains(Geometry geometry) throws OperationException;

    Geometry getConvexhull() throws OperationException;

    boolean crosses(Geometry geometry) throws OperationException;

    Geometry difference(Geometry geometry) throws OperationException;

    boolean disjoint(Geometry geometry) throws OperationException;

    double distance(Geometry geometry) throws OperationException;

    Geometry intersection(Geometry geometry) throws OperationException;

    boolean intersects(Geometry geometry) throws OperationException;

    Geometry[] nearest(Geometry geometry) throws OperationException;

    boolean overlaps(Geometry geometry) throws OperationException;

    Geometry symDifference(Geometry geometry) throws OperationException;

    boolean touches(Geometry geometry) throws OperationException;

    Geometry union(Geometry geometry) throws OperationException;

    boolean within(Geometry geometry) throws OperationException;

}
