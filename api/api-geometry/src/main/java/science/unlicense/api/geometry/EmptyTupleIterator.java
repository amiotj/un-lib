
package science.unlicense.api.geometry;

import science.unlicense.api.exception.UnimplementedException;

/**
 * Empty sample iterator.
 *
 * @author Johann Sorel
 */
public class EmptyTupleIterator implements TupleIterator{

    public static final TupleIterator INSTANCE = new EmptyTupleIterator();

    public EmptyTupleIterator() {
    }

    public int[] getCoordinate() {
        throw new UnimplementedException("Not on any pixel at this time");
    }

    public Object next() {
        return null;
    }

    public boolean[] nextAsBoolean(boolean[] buffer) {
        return null;
    }

    public byte[] nextAsByte(byte[] buffer) {
        return null;
    }

    public int[] nextAsUByte(int[] buffer) {
        return null;
    }

    public short[] nextAsShort(short[] buffer) {
        return null;
    }

    public int[] nextAsUShort(int[] buffer) {
        return null;
    }

    public int[] nextAsInt(int[] buffer) {
        return null;
    }

    public long[] nextAsUInt(long[] buffer) {
        return null;
    }

    public long[] nextAsLong(long[] buffer) {
        return null;
    }

    public float[] nextAsFloat(float[] buffer) {
        return null;
    }

    public double[] nextAsDouble(double[] buffer) {
        return null;
    }

}
