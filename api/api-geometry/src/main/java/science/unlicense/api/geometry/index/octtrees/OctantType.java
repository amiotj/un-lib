
package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.geometry.BBox;

/**
 * The type of octants in octtrees.
 * 
 * @author Mark Raynsford
 */
public class OctantType extends BBox{

    public OctantType() {
        super(3);
    }
  
}
