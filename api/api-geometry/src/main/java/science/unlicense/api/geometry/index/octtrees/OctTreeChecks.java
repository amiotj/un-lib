
package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.exception.InvalidArgumentException;


/**
 * Checks for octtree invariants.
 * 
 * @author Mark Raynsford
 */
final class OctTreeChecks
{
  static void checkSize(
    final String name,
    final double width,
    final double height,
    final double depth)
  {
    if (width < 2) {
      final String s = String.format("%s: Width must be >= 2", name);
      throw new InvalidArgumentException(s);
    }
    if ((width % 2) != 0) {
      final String s = String.format("%s: Width must be even", name);
      throw new InvalidArgumentException(s);
    }

    if (height < 2) {
      final String s = String.format("%s: Height must be >= 2", name);
      throw new InvalidArgumentException(s);
    }
    if ((height % 2) != 0) {
      final String s = String.format("%s: Height must be even", name);
      throw new InvalidArgumentException(s);
    }

    if (depth < 2) {
      final String s = String.format("%s: Depth must be >= 2", name);
      throw new InvalidArgumentException(s);
    }
    if ((depth % 2) != 0) {
      final String s = String.format("%s: Depth must be even", name);
      throw new InvalidArgumentException(s);
    }
  }

  private OctTreeChecks()
  {
    throw new RuntimeException("Unreachable code");
  }
}
