
package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.BBox;

/**
 * <p>
 * The type of objects that can be placed into a quadtree. Objects are
 * required to:
 * </p>
 * <ul>
 * <li>Be comparable and have a total order, in order to allow for "set"
 * semantics within the tree.</li>
 * <li>Have axis-aligned bounding boxes, for spatial queries.</li>
 * </ul>
 *
 * @param <T>
 *          The precise type of quadtree members.
 */
public abstract class QuadTreeMemberType<T> extends BBox implements Comparable<T>{

    public QuadTreeMemberType() {
        super(2);
    }
}
