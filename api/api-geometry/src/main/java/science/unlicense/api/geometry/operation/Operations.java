
package science.unlicense.api.geometry.operation;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.system.System;

/**
 *
 * @author Johann Sorel
 */
public final class Operations {

    private static final Chars OPERATION_PATH = new Chars("services/science.unlicense.api.geometry.operation.OperationExecutor");

    private static Dictionary CACHE;
    
    private Operations(){}

    /**
     * Lists available image formats.
     * @return array of ImageFormat, never null but can be empty.
     */
    public static OperationExecutor[] getExecutors(){
        final NamedNode root = System.get().getModuleManager().getMetadataRoot().search(OPERATION_PATH);
        if(root==null){
            return new OperationExecutor[0];
        }
        return (OperationExecutor[]) Nodes.getChildrenValues(root, null).toArray(OperationExecutor.class);
    }
    
    private static OperationExecutor[] getExecutors(Class opClass){
        synchronized(OPERATION_PATH){
            if(CACHE==null){
                CACHE = new HashDictionary();
                final OperationExecutor[] execs = getExecutors();
                for(int i=0;i<execs.length;i++){
                    final Class oc = execs[i].getOperationClass();
                    OperationExecutor[] array = (OperationExecutor[]) CACHE.getValue(oc);
                    if(array==null){
                        array = new OperationExecutor[]{execs[i]};
                    }else{
                        array = (OperationExecutor[]) Arrays.insert(array, array.length, execs[i]);
                    }
                    CACHE.add(oc, array);
                }
            }
        }
        return (OperationExecutor[]) CACHE.getValue(opClass);
    }
    

    public static Object execute(Operation operation) throws OperationException{
        final OperationExecutor[] executors = getExecutors(operation.getClass());
        for(int i=0,n=executors.length;i<n;i++){
            if(executors[i].canHandle(operation)){
                return executors[i].execute(operation);
            }
        }
        throw new OperationException("No executor can handle the operation : "+operation);
    }

}
