
package science.unlicense.api.geometry.index;

import science.unlicense.api.collection.Iterator;

/**
 *
 * @author Johann Sorel
 */
public interface Index {

    void put(Object key, Object value);
    
    Iterator search(Object condition);
    
}
