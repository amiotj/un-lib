
package science.unlicense.api.geometry;

import science.unlicense.api.buffer.Buffer;

/**
 *
 * @author Johann Sorel
 */
public interface TupleBuffer1D extends TupleBuffer {

    int getDimension();
    /**
     * Make a copy of this tuple buffer.
     * 
     * @return
     */
    TupleBuffer1D copy();
    
    /**
     * Make a copy of this tuple buffer using given primitive buffer.
     * @param buffer
     * @return
     */
    TupleBuffer1D copy(Buffer buffer);
    
    Object getTuple(int coordinate, Object buffer);

    boolean[] getTupleBoolean(int coordinate, boolean[] buffer);

    byte[] getTupleByte(int coordinate, byte[] buffer);

    int[] getTupleUByte(int coordinate, int[] buffer);

    short[] getTupleShort(int coordinate, short[] buffer);

    int[] getTupleUShort(int coordinate, int[] buffer);

    int[] getTupleInt(int coordinate, int[] buffer);

    long[] getTupleUInt(int coordinate, long[] buffer);

    long[] getTupleLong(int coordinate, long[] buffer);

    float[] getTupleFloat(int coordinate, float[] buffer);

    double[] getTupleDouble(int coordinate, double[] buffer);

    /**
     * set the given coordinate with given samples.
     *
     * @param coordinate
     * @param sample
     */
    void setTuple(int coordinate, Object sample);

}
