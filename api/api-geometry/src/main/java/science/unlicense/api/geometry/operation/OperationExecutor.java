
package science.unlicense.api.geometry.operation;

/**
 * An operation executor is able to process a single operation type.
 * 
 * 
 * @author Johann Sorel
 */
public interface OperationExecutor {
    
    /**
     * 
     * @return Operation class supported by this executor.
     */
    Class getOperationClass();
    
    /**
     * All possible parameters of the operation may not be supported in such
     * can the canHandle method should return false.
     * @param operation
     * @return 
     */
    boolean canHandle(Operation operation);
    
    /**
     * Execute operation.
     * 
     * @param operation
     * @return
     * @throws OperationException 
     */
    Object execute(Operation operation) throws OperationException;
    
}
