
package science.unlicense.api.geometry;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.math.Maths;

/**
 * An extent defines sizes along axes.
 * Unlike BoundingBox, only a size is indicated, not a start/end pair.
 * 
 * @author Johann Sorel
 */
public abstract class Extent extends CObject {
    
    public abstract int getDimension();

    public abstract double get(int ordinate);

    public abstract void set(int ordinate, double size);
    
    /**
     * Copy given extent.
     * 
     * @param extent to copy, not null.
     */
    public abstract void set(final Extent extent);

    /**
     * Increase this extent dimension value replacing them
     * by given extent values if they are greater.
     * 
     * @param extent, not null
     */
    public abstract void expand(final Extent extent);
    
    /**
     * Decrease this extent dimension value replacing them
     * by given extent values if they are smaller.
     * 
     * @param extent, not null
     */
    public abstract void shrink(final Extent extent);
    
    /**
     * Set all dimension values to zero.
     */
    public void setToZero(){
        for(int i=0,n=getDimension();i<n;i++){
            set(i, 0);
        }
    }
        
    /**
     * Check if extent is empty, all dimension values set to zero.
     * 
     * @return true if extent is empty.
     */
    public boolean isEmpty(){
       for(int i=getDimension()-1;i>-1;i--){
           if(get(i)!=0.0) return false;
       } 
       return true;
    }
    
    /**
     * Make a copy of this extent.
     * @return Extent never null
     */
    public abstract Extent copy();


    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Extent other = (Extent) obj;
        for(int i=0,n=other.getDimension();i<n;i++){
            if(this.get(i) != other.get(i)){
                return false;
            }
        }
        return true;
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Extent ");
        for(int i=0,n=getDimension();i<n;i++){
            cb.append(""+get(i));
            cb.append(' ');
        }
        return cb.toChars();
    }

    public static class Long extends Extent {

        private final long[] values;

        public Long(int dimension) {
            values = new long[dimension];
        }

        public Long(long[] values) {
            this.values = values;
        }

        public Long(long size1){
            values = new long[]{size1};
        }

        public Long(long size1, long size2){
            values = new long[]{size1,size2};
        }

        public Long(long size1, long size2, long size3){
            values = new long[]{size1,size2,size3};
        }

        public Long(long size1, long size2, long size3, long size4){
            values = new long[]{size1,size2,size3,size4};
        }

        public int getDimension() {
            return values.length;
        }

        public double get(int ordinate){
            return values[ordinate];
        }
        
        public long getL(int ordinate){
            return values[ordinate];
        }

        public void set(int ordinate, double size){
            values[ordinate] = (long) size;
        }
        
        public void setL(int ordinate, long size){
            values[ordinate] = size;
        }

        /**
         * Copy given extent.
         *
         * @param extent to copy, not null.
         */
        public void set(final Extent extent){
            for(int i=0,n=extent.getDimension();i<n;i++){
                values[i] = (long) extent.get(i);
            }
        }

        /**
         * Increase this extent dimension value replacing them
         * by given extent values if they are greater.
         *
         * @param extent, not null
         */
        public void expand(final Extent extent){
            for(int i=0;i<values.length;i++){
                values[i] = (long) Maths.max(values[i], extent.get(i));
            }
        }

        /**
         * Decrease this extent dimension value replacing them
         * by given extent values if they are smaller.
         *
         * @param extent, not null
         */
        public void shrink(final Extent extent){
            for(int i=0;i<values.length;i++){
                values[i] = (long) Maths.min(values[i], extent.get(i));
            }
        }

        /**
         * Make a copy of this extent.
         * @return Extent never null
         */
        public Extent.Long copy() {
            return new Extent.Long(Arrays.copy(values));
        }
        
        public long[] toArray(){
            return Arrays.copy(values);
        }

    }


    public static class Double extends Extent {

        private final double[] values;

        public Double(int dimension) {
            values = new double[dimension];
        }

        public Double(double[] values) {
            this.values = values;
        }

        public Double(double size1){
            values = new double[]{size1};
        }

        public Double(double size1, double size2){
            values = new double[]{size1,size2};
        }

        public Double(double size1, double size2, double size3){
            values = new double[]{size1,size2,size3};
        }

        public Double(double size1, double size2, double size3, double size4){
            values = new double[]{size1,size2,size3,size4};
        }

        public int getDimension() {
            return values.length;
        }

        public double get(int ordinate){
            return values[ordinate];
        }

        public void set(int ordinate, double size){
            values[ordinate] = size;
        }

        /**
         * Copy given extent.
         *
         * @param extent to copy, not null.
         */
        public void set(final Extent extent){
            if(extent instanceof Double){
                Arrays.copy(((Double)extent).values, 0, ((Double)extent).values.length, values, 0);
            }else{
                for(int i=0,n=extent.getDimension();i<n;i++){
                    values[i] = extent.get(i);
                }
            }
        }

        /**
         * Increase this extent dimension value replacing them
         * by given extent values if they are greater.
         *
         * @param extent, not null
         */
        public void expand(final Extent extent){
            for(int i=0;i<values.length;i++){
                values[i] = Maths.max(values[i], extent.get(i));
            }
        }

        /**
         * Decrease this extent dimension value replacing them
         * by given extent values if they are smaller.
         *
         * @param extent, not null
         */
        public void shrink(final Extent extent){
            for(int i=0;i<values.length;i++){
                values[i] = Maths.min(values[i], extent.get(i));
            }
        }

        /**
         * Make a copy of this extent.
         * @return Extent never null
         */
        public Extent.Double copy() {
            return new Double(Arrays.copy(values));
        }

    }

}
