
package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.geometry.index.Dimensions;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;
/**
 * Functions to produce new octants from an existing pair of corners.
 * 
 * @author Mark Raynsford
 */
public final class Octants{
    
  /**
   * Split an octant defined by the two points <code>lower</code> and
   * <code>upper</code> into eight octants.
   *
   * @return Eight new octants
   * @param upper
   *          The upper corner
   * @param lower
   *          The lower corner
   */

  public static Octants split(
    final Tuple lower,
    final Tuple upper)
  {
    final double size_x = Dimensions.getSpanSizeX(lower, upper);
    final double size_y = Dimensions.getSpanSizeY(lower, upper);
    final double size_z = Dimensions.getSpanSizeZ(lower, upper);

    assert size_x >= 2;
    assert size_y >= 2;
    assert size_z >= 2;

    final double[] x_spans = new double[4];
    final double[] y_spans = new double[4];
    final double[] z_spans = new double[4];

    Dimensions.split1D(lower.getX(), upper.getX(), x_spans);
    Dimensions.split1D(lower.getY(), upper.getY(), y_spans);
    Dimensions.split1D(lower.getZ(), upper.getZ(), z_spans);

    final Vector in_x0y0z0_lower =
      new Vector(x_spans[0], y_spans[0], z_spans[0]);
    final Vector in_x0y0z0_upper =
      new Vector(x_spans[1], y_spans[1], z_spans[1]);

    final Vector in_x1y0z0_lower =
      new Vector(x_spans[2], y_spans[0], z_spans[0]);
    final Vector in_x1y0z0_upper =
      new Vector(x_spans[3], y_spans[1], z_spans[1]);

    final Vector in_x0y1z0_lower =
      new Vector(x_spans[0], y_spans[2], z_spans[0]);
    final Vector in_x0y1z0_upper =
      new Vector(x_spans[1], y_spans[3], z_spans[1]);

    final Vector in_x1y1z0_lower =
      new Vector(x_spans[2], y_spans[2], z_spans[0]);
    final Vector in_x1y1z0_upper =
      new Vector(x_spans[3], y_spans[3], z_spans[1]);

    final Vector in_x0y0z1_lower =
      new Vector(x_spans[0], y_spans[0], z_spans[2]);
    final Vector in_x0y0z1_upper =
      new Vector(x_spans[1], y_spans[1], z_spans[3]);

    final Vector in_x1y0z1_lower =
      new Vector(x_spans[2], y_spans[0], z_spans[2]);
    final Vector in_x1y0z1_upper =
      new Vector(x_spans[3], y_spans[1], z_spans[3]);

    final Vector in_x0y1z1_lower =
      new Vector(x_spans[0], y_spans[2], z_spans[2]);
    final Vector in_x0y1z1_upper =
      new Vector(x_spans[1], y_spans[3], z_spans[3]);

    final Vector in_x1y1z1_lower =
      new Vector(x_spans[2], y_spans[2], z_spans[2]);
    final Vector in_x1y1z1_upper =
      new Vector(x_spans[3], y_spans[3], z_spans[3]);

    return new Octants(
      in_x0y0z0_lower,
      in_x0y0z0_upper,
      in_x1y0z0_lower,
      in_x1y0z0_upper,
      in_x0y1z0_lower,
      in_x0y1z0_upper,
      in_x1y1z0_lower,
      in_x1y1z0_upper,
      in_x0y0z1_lower,
      in_x0y0z1_upper,
      in_x1y0z1_lower,
      in_x1y0z1_upper,
      in_x0y1z1_lower,
      in_x0y1z1_upper,
      in_x1y1z1_lower,
      in_x1y1z1_upper);
  }

  private final Vector x0y0z0_lower;
  private final Vector x0y0z0_upper;
  private final Vector x0y0z1_lower;
  private final Vector x0y0z1_upper;
  private final Vector x0y1z0_lower;
  private final Vector x0y1z0_upper;
  private final Vector x0y1z1_lower;
  private final Vector x0y1z1_upper;
  private final Vector x1y0z0_lower;
  private final Vector x1y0z0_upper;
  private final Vector x1y0z1_lower;
  private final Vector x1y0z1_upper;
  private final Vector x1y1z0_lower;
  private final Vector x1y1z0_upper;
  private final Vector x1y1z1_lower;

  private final Vector x1y1z1_upper;

  private Octants(
    final Vector in_x0y0z0_lower,
    final Vector in_x0y0z0_upper,
    final Vector in_x1y0z0_lower,
    final Vector in_x1y0z0_upper,
    final Vector in_x0y1z0_lower,
    final Vector in_x0y1z0_upper,
    final Vector in_x1y1z0_lower,
    final Vector in_x1y1z0_upper,
    final Vector in_x0y0z1_lower,
    final Vector in_x0y0z1_upper,
    final Vector in_x1y0z1_lower,
    final Vector in_x1y0z1_upper,
    final Vector in_x0y1z1_lower,
    final Vector in_x0y1z1_upper,
    final Vector in_x1y1z1_lower,
    final Vector in_x1y1z1_upper)
  {
    this.x0y0z0_lower = in_x0y0z0_lower;
    this.x0y0z0_upper = in_x0y0z0_upper;
    this.x1y0z0_lower = in_x1y0z0_lower;
    this.x1y0z0_upper = in_x1y0z0_upper;
    this.x0y1z0_lower = in_x0y1z0_lower;
    this.x0y1z0_upper = in_x0y1z0_upper;
    this.x1y1z0_lower = in_x1y1z0_lower;
    this.x1y1z0_upper = in_x1y1z0_upper;
    this.x0y0z1_lower = in_x0y0z1_lower;
    this.x0y0z1_upper = in_x0y0z1_upper;
    this.x1y0z1_lower = in_x1y0z1_lower;
    this.x1y0z1_upper = in_x1y0z1_upper;
    this.x0y1z1_lower = in_x0y1z1_lower;
    this.x0y1z1_upper = in_x0y1z1_upper;
    this.x1y1z1_lower = in_x1y1z1_lower;
    this.x1y1z1_upper = in_x1y1z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y0, z0)</code> octant.
   */

  public Vector getX0Y0Z0Lower()
  {
    return this.x0y0z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y0, z0)</code> octant.
   */

  public Vector getX0Y0Z0Upper()
  {
    return this.x0y0z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y0, z1)</code> octant.
   */

  public Vector getX0Y0Z1Lower()
  {
    return this.x0y0z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y0, z1)</code> octant.
   */

  public Vector getX0Y0Z1Upper()
  {
    return this.x0y0z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y1, z0)</code> octant.
   */

  public Vector getX0Y1Z0Lower()
  {
    return this.x0y1z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y1, z0)</code> octant.
   */

  public Vector getX0Y1Z0Upper()
  {
    return this.x0y1z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x0, y1, z1)</code> octant.
   */

  public Vector getX0Y1Z1Lower()
  {
    return this.x0y1z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x0, y1, z1)</code> octant.
   */

  public Vector getX0Y1Z1Upper()
  {
    return this.x0y1z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y0, z0)</code> octant.
   */

  public Vector getX1Y0Z0Lower()
  {
    return this.x1y0z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y0, z0)</code> octant.
   */

  public Vector getX1Y0Z0Upper()
  {
    return this.x1y0z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y0, z1)</code> octant.
   */

  public Vector getX1Y0Z1Lower()
  {
    return this.x1y0z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y0, z1)</code> octant.
   */

  public Vector getX1Y0Z1Upper()
  {
    return this.x1y0z1_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y1, z0)</code> octant.
   */

  public Vector getX1Y1Z0Lower()
  {
    return this.x1y1z0_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y1, z0)</code> octant.
   */

  public Vector getX1Y1Z0Upper()
  {
    return this.x1y1z0_upper;
  }

  /**
   * @return The lower corner of the <code>(x1, y1, z1)</code> octant.
   */

  public Vector getX1Y1Z1Lower()
  {
    return this.x1y1z1_lower;
  }

  /**
   * @return The upper corner of the <code>(x1, y1, z1)</code> octant.
   */

  public Vector getX1Y1Z1Upper()
  {
    return this.x1y1z1_upper;
  }
}
