
package science.unlicense.api.geometry;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.AbstractGeometry;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.math.Vector;

/**
 * N dimension bounding box.
 *
 * @author Johann Sorel
 */
public class BBox extends AbstractGeometry {

    protected final int dimension;
    protected final TupleRW upper;
    protected final TupleRW lower;

    public BBox(int dimension) {
        this(new double[dimension],new double[dimension]);
    }

    public BBox(double[] min, double[] max) {
        this.dimension = min.length;
        upper = new Vector(max);
        lower = new Vector(min);
    }

    public BBox(TupleRW lower,TupleRW upper) {
        this.upper = upper;
        this.lower = lower;
        this.dimension = lower.getSize();
    }

    public BBox(BBox other) {
        this.upper = other.upper.copy();
        this.lower = other.lower.copy();
        this.dimension = lower.getSize();
    }

    public BBox(Extent extent) {
        this.dimension = extent.getDimension();
        this.upper = new Vector(dimension);
        this.lower = new Vector(dimension);
        for(int i=0;i<this.dimension;i++){
            upper.set(i,extent.get(i));
        }
    }

    public int getDimension() {
        return dimension;
    }

    public TupleRW getUpper() {
        return upper;
    }

    public TupleRW getLower() {
        return lower;
    }

    public TupleRW getMiddle() {
        return new Vector(upper).localAdd(lower).localScale(0.5);
    }

    public double getMin(int ordinate){
        return lower.get(ordinate);
    }

    public double getMiddle(int ordinate){
        return (upper.get(ordinate) + lower.get(ordinate)) / 2;
    }

    public double getMax(int ordinate){
        return upper.get(ordinate);
    }

    public double getSpan(int ordinate){
        return getMax(ordinate)-getMin(ordinate);
    }

    public Extent getExtent(){
        final Extent ext = new Extent.Double(dimension);
        for(int i=0;i<dimension;i++){
            ext.set(i, getSpan(i));
        }
        return ext;
    }
    
    public TupleRW[] getCorners3D(TupleRW[] corners){
        if(corners==null){
            corners = new TupleRW[]{
                new Vector(3),
                new Vector(3),
                new Vector(3),
                new Vector(3),
                new Vector(3),
                new Vector(3),
                new Vector(3),
                new Vector(3)
            };
        }

        final double[] l = lower.getValues();
        final double[] u = upper.getValues();

        corners[0].setXYZ(l[0], l[1], l[2]);
        corners[1].setXYZ(l[0], l[1], u[2]);
        corners[2].setXYZ(l[0], u[1], l[2]);
        corners[3].setXYZ(l[0], u[1], u[2]);
        corners[4].setXYZ(u[0], l[1], l[2]);
        corners[5].setXYZ(u[0], l[1], u[2]);
        corners[6].setXYZ(u[0], u[1], l[2]);
        corners[7].setXYZ(u[0], u[1], u[2]);
        
        return corners;
    }

    /**
     * Copy values from given bounding box.
     * @param bbox not null
     */
    public void set(BBox bbox){
        upper.set(bbox.getUpper());
        lower.set(bbox.getLower());
    }

    public void setRange(int ordinate, double min, double max){
        lower.set(ordinate, min);
        upper.set(ordinate, max);
    }

    /**
     * Grow this bounding to contain the given one.
     * @param bbox
     */
    public void expand(BBox bbox){
        for(int i=0;i<dimension;i++){
            lower.set(i, Maths.min(lower.get(i), bbox.getMin(i)));
            upper.set(i, Maths.max(upper.get(i), bbox.getMax(i)));
        }
    }

    /**
     * Grow this bounding to contain the given point.
     * @param point
     */
    public void expand(Tuple point){
        for(int i=0;i<dimension;i++){
            lower.set(i, Maths.min(lower.get(i), point.get(i)));
            upper.set(i, Maths.max(upper.get(i), point.get(i)));
        }
    }
    
    /**
     * Grow this bounding to contain the given point.
     * @param point
     */
    public void expand(double[] point){
        for(int i=0;i<dimension;i++){
            lower.set(i, Maths.min(lower.get(i), point[i]));
            upper.set(i, Maths.max(upper.get(i), point[i]));
        }
    }
    
    /**
     * Grow this bounding to contain all tuples in buffer.
     * @param buffer
     */
    public void expand(TupleBuffer buffer){
        final TupleIterator ite = buffer.createIterator(null);
        double[] tuple = new double[dimension];
        for(tuple=ite.nextAsDouble(tuple);tuple!=null;tuple=ite.nextAsDouble(tuple)){
            for(int i=0;i<dimension;i++){
                lower.set(i, Maths.min(lower.get(i), tuple[i]));
                upper.set(i, Maths.max(upper.get(i), tuple[i]));
            }
        }
    }
    
    /**
     * Grow this bounding to contain the given point.
     * @param point
     */
    public void expand(float[] point){
        for(int i=0;i<dimension;i++){
            lower.set(i, Maths.min(lower.get(i), point[i]));
            upper.set(i, Maths.max(upper.get(i), point[i]));
        }
    }
    
    public void expand(double px){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
    }
    
    public void expand(double px, double py){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
        lower.set(1, Maths.min(lower.get(1), py));
        upper.set(1, Maths.max(upper.get(1), py));
    }
    
    public void expand(double px, double py, double pz){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
        lower.set(1, Maths.min(lower.get(1), py));
        upper.set(1, Maths.max(upper.get(1), py));
        lower.set(2, Maths.min(lower.get(2), pz));
        upper.set(2, Maths.max(upper.get(2), pz));
    }
    
    public void expand(double px, double py, double pz, double pw){
        lower.set(0, Maths.min(lower.get(0), px));
        upper.set(0, Maths.max(upper.get(0), px));
        lower.set(1, Maths.min(lower.get(1), py));
        upper.set(1, Maths.max(upper.get(1), py));
        lower.set(2, Maths.min(lower.get(2), pz));
        upper.set(2, Maths.max(upper.get(2), pz));
        lower.set(3, Maths.min(lower.get(3), pw));
        upper.set(3, Maths.max(upper.get(3), pw));
    }
    
    /**
     * Expand the bbox in all directions by given amount.
     */
    public void grow(double amount){
        for(int i=0;i<dimension;i++){
            lower.set(i, lower.get(i)-amount);
            upper.set(i, upper.get(i)+amount);
        }
    }
    
    /**
     *
     * @return true if lower corner equals upper corner.
     */
    public boolean isEmpty(){
        return lower.equals(upper);
    }

    /**
     * Set all values to zero
     */
    public void setToZero(){
        upper.setToZero();
        lower.setToZero();
    }
    
    /**
     * Set all values to NaN.
     */
    public void setToNaN(){
        upper.setToNaN();
        lower.setToNaN();
    }

    /**
     * Set lower values to max value and upper values to negative max value
     */
    public void setToReverseMax(){
        lower.setAll(Double.MAX_VALUE);
        upper.setAll(-Double.MAX_VALUE);
    }
    
    /**
     * Check that all values are not NaN or Infinites.
     * @return true if valid
     */
    public boolean isValid(){
        return lower.isValid() && upper.isValid();
    }
    
    /**
     * Reorder values, ensure min/max values are in
     * lower/upper corners.
     */
    public void reorder(){
        for(int i=0;i<dimension;i++){
            final double tl = lower.get(i);
            final double tu = upper.get(i);
            if(tl>tu){
                upper.set(i, tl);
                lower.set(i, tu);
            }
        }
    }
    
    public BBox getBoundingBox() {
        return this;
    }

    public Point getCentroid() {
        return new Point(getMiddle());
    }
    
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BBox other = (BBox) obj;
        if (this.upper != other.upper && (this.upper == null || !this.upper.equals(other.upper))) {
            return false;
        }
        if (this.lower != other.lower && (this.lower == null || !this.lower.equals(other.lower))) {
            return false;
        }
        return true;
    }

    public Chars toChars() {
        return new Chars("BBOX : ").concat(CObjects.toChars(lower)).concat(CObjects.toChars(upper));
    }

    public boolean intersects(Tuple coord){
        return intersects(coord, true);
    }
    
    public boolean intersects(Tuple coord, boolean includeBorder){
        final int dim = getDimension();
        if(includeBorder){
            for(int i=0;i<dim;i++){
                if(getMax(i) < coord.get(i) || getMin(i) > coord.get(i)) return false;
            }
        }else{
            for(int i=0;i<dim;i++){
                if(getMax(i) <= coord.get(i) || getMin(i) >= coord.get(i)) return false;
            }
        }
        
        return true;
    }
    
    public boolean intersects(BBox bbox){
        return intersects(bbox, true);
    }
    
    public boolean intersects(BBox bbox, boolean includeBorder){
        final int dim = getDimension();
        if(includeBorder){
            for(int i=0;i<dim;i++){
                if(getMax(i) < bbox.getMin(i) || getMin(i) > bbox.getMax(i)) return false;
            }
        }else{
            for(int i=0;i<dim;i++){
                if(getMax(i) <= bbox.getMin(i) || getMin(i) >= bbox.getMax(i)) return false;
            }
        }
        
        return true;
    }
    
    /**
     * Test intersection with an Extent.
     * The extent is interpreted as a BBox starting at coordinate zero on each dimension.
     * 
     * TODO : make extent a geometry ?
     * 
     * @param extent
     * @param includeBorder
     * @return 
     */
    public boolean intersects(Extent extent, boolean includeBorder){
        for(int i=getDimension()-1;i>-1;i--){
            if(includeBorder){
                if(getMin(i) > extent.get(i) || getMax(i) < 0.0) return false;
            }else{
                if(getMin(i) >= extent.get(i) || getMax(i) <= 0.0) return false;
            }
        }
        return true;
    }
    
    public boolean contains(BBox bbox){
        final int dim = getDimension();
        for(int i=0;i<dim;i++){
            if(getMax(i) < bbox.getMax(i) || getMin(i) > bbox.getMin(i)) return false;
        }
        return true;
    }

    /**
     * Intersect this bbox with given one, store the result in this bbox.
     * This method does not perform any verification.
     * 
     * @param bbox
     * @return
     */
    public void localIntersect(BBox bbox){
        final double[] lo1Vals = lower.getValues();
        final double[] up1Vals = upper.getValues();
        final double[] lo2Vals = bbox.getLower().getValues();
        final double[] up2Vals = bbox.getUpper().getValues();
        for(int i=0;i<lo1Vals.length;i++){
            lo1Vals[i] = Maths.max(lo1Vals[i], lo2Vals[i]);
            up1Vals[i] = Maths.min(up1Vals[i], up2Vals[i]);
        }
    }
    
}
