package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;

/**
 * <p>
 * An object returned by a successful raycast operation. Objects are ordered by
 * their scalar distance from the origin of the ray.
 * </p>
 *
 * <p>
 * Note that although the <code>OctTreeRaycastResult</code> class is immutable,
 * whether or not the object of type <code>T</code> returned by
 * {@link #getObject()} is mutable depends entirely on the user and therefore
 * thread-safety is also the responsibility of the user.
 * </p>
 *
 * @param <T> The precise type of octtree members.
 * @author Mark Raynsford
 */
public final class OctTreeRaycastResult<T extends BBox> extends CObject
        implements Comparable<OctTreeRaycastResult<T>> {

    private final double distance;
    private final T object;

    /**
     * Construct a new result.
     *
     * @param in_object The object.
     * @param in_distance The distance to the object.
     */
    public OctTreeRaycastResult(
            final T in_object,
            final double in_distance) {
        CObjects.ensureNotNull(in_object, "Object");
        this.object = in_object;
        this.distance = in_distance;
    }

    @Override
    public int compareTo(final OctTreeRaycastResult<T> other) {
        CObjects.ensureNotNull(other, "Other");
        return Double.compare(this.distance, other.distance);
    }

    @Override
    public boolean equals(
            final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final OctTreeRaycastResult<?> other = (OctTreeRaycastResult<?>) obj;
        if (Double.doubleToLongBits(this.distance) != Double
                .doubleToLongBits(other.distance)) {
            return false;
        }
        if (!this.object.equals(other.object)) {
            return false;
        }
        return true;
    }

    /**
     * @return The distance of this object from the origin of the ray.
     */
    public double getDistance() {
        return this.distance;
    }

    /**
     * @return The intersected object.
     */
    public T getObject() {
        return this.object;
    }

    @Override
    public int getHash() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(this.distance);
        result = (prime * result) + (int) (temp ^ (temp >>> 32));
        result = (prime * result) + this.object.hashCode();
        return result;
    }

    @Override
    public Chars toChars() {
        final CharBuffer b = new CharBuffer();
        b.append("[OctTreeRaycastResult ");
        b.append(this.distance);
        b.append(" ");
        b.append(this.object);
        b.append("]");
        return b.toChars();
    }
}
