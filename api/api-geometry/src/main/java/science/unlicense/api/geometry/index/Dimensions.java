
package science.unlicense.api.geometry.index;

import science.unlicense.api.math.Tuple;

/**
 * Functions to calculate dimensions and spans.
 * 
 * @author Mark Raynsford
 */
public final class Dimensions{
    
  /**
   * Return the span on the X axis of the inclusive range defined by the given
   * points.
   *
   * @param lower
   *          The lower point
   * @param upper
   *          The upper point
   * @return upper.getX() - lower.getX()) + 1
   */
  public static double getSpanSizeX(
    final Tuple lower,
    final Tuple upper)
  {
    return (upper.getX() - lower.getX()) + 1;
  }

  /**
   * Return the span on the Y axis of the inclusive range defined by the given
   * points.
   *
   * @param lower
   *          The lower point
   * @param upper
   *          The upper point
   * @return upper.getY() - lower.getY()) + 1
   */

  public static double getSpanSizeY(
    final Tuple lower,
    final Tuple upper)
  {
    return (upper.getY() - lower.getY()) + 1;
  }

  /**
   * Return the span on the Z axis of the inclusive range defined by the given
   * points.
   *
   * @param lower
   *          The lower point
   * @param upper
   *          The upper point
   * @return upper.getZ() - lower.getZ()) + 1
   */

  public static double getSpanSizeZ(
    final Tuple lower,
    final Tuple upper)
  {
    return (upper.getZ() - lower.getZ()) + 1;
  }

  /**
   * <p>
   * Given an inclusive range defined by <code>[low .. high]</code>, split the
   * range in the middle and produce two new inclusive ranges.
   * </p>
   * <p>
   * The lower and upper bounds of the lower range are stored in
   * <code>out[0]</code> and <code>out[1]</code>, respectively. The lower and
   * upper bounds of the upper range are stored in <code>out[2]</code> and
   * <code>out[3]</code>, respectively.
   * </p>
   *
   * @param low
   *          The lower bound
   * @param high
   *          The upper bound
   * @param out
   *          The output vector
   */

  public static void split1D(
    final double low,
    final double high,
    final double[] out)
  {
    assert out.length == 4;

    final double size = (high - low) + 1;
    out[0] = low;
    out[1] = (low+high) /2.0;
    out[2] = (low+high) /2.0;
    out[3] = high;
  }

  private Dimensions()
  {
    throw new RuntimeException("Unreachable code");
  }
}
