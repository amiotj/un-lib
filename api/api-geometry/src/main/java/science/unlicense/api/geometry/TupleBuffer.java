
package science.unlicense.api.geometry;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;

/**
 * A tuple buffer is a N dimension array of tuples.
 *
 *
 * @author Johann Sorel
 */
public interface TupleBuffer {

    /**
     * Get buffer dimensions.
     * 
     * @return int[]
     */
    Extent.Long getExtent();

    /**
     * @return number of sample values in the tuples.
     */
    int getSampleCount();

    /**
     * @return type of samples, see Primitive.TYPE_X
     */
    int getPrimitiveType();

    /**
     * @return Image data bank
     */
    Buffer getPrimitiveBuffer();

    /**
     * Create a byte buffer compatible with this tuple buffer.
     * @param dimensions
     * @param factory
     * @return Buffer
     */
    Buffer createPrimitiveBuffer(Extent.Long dimensions, BufferFactory factory);
    
    /**
     * Create a new tuple buffer with given dimension.
     *
     * @param dimensions
     * @return ImageSampleModel
     */
    TupleBuffer create(Extent.Long dimensions);
    
    /**
     * Create a new tuple buffer with given dimension.
     *
     * @param dimensions
     * @param factory
     * @return ImageSampleModel
     */
    TupleBuffer create(Extent.Long dimensions, BufferFactory factory);

    /**
     * Make a copy of this tuple buffer.
     * 
     * @return
     */
    TupleBuffer copy();
    
    /**
     * Make a copy of this tuple buffer using given primitive buffer.
     * @param buffer
     * @return
     */
    TupleBuffer copy(Buffer buffer);

    /**
     * @return create the most efficient tuple sample store array for this tuple buffer.
     */
    Object createTupleStore();

    Object getTuple(int[] coordinate, Object buffer);

    boolean[] getTupleBoolean(int[] coordinate, boolean[] buffer);

    byte[] getTupleByte(int[] coordinate, byte[] buffer);

    int[] getTupleUByte(int[] coordinate, int[] buffer);

    short[] getTupleShort(int[] coordinate, short[] buffer);

    int[] getTupleUShort(int[] coordinate, int[] buffer);

    int[] getTupleInt(int[] coordinate, int[] buffer);

    long[] getTupleUInt(int[] coordinate, long[] buffer);

    long[] getTupleLong(int[] coordinate, long[] buffer);

    float[] getTupleFloat(int[] coordinate, float[] buffer);

    double[] getTupleDouble(int[] coordinate, double[] buffer);

    /**
     * set the given coordinate with given sample.
     *
     * @param coordinate
     * @param sample
     */
    void setTuple(int[] coordinate, Object sample);

    /**
     * Fill the given bounding box with given sample.
     *
     * @param box
     * @param sample
     */
    void setTuple(BBox box, Object sample);

    /**
     * Create an iterator over buffer tuples.
     * @return TupleIterator
     */
    TupleIterator createIterator(BBox bbox);


}
