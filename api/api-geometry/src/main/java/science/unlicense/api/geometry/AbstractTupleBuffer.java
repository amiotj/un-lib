
package science.unlicense.api.geometry;

import java.lang.reflect.Array;
import science.unlicense.api.CObject;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleBuffer extends CObject implements TupleBuffer {

    protected int sampleType;
    protected int nbSample;
    protected int bitsPerSample;
    /**
     * = nbSample*bitsPerSample
     */
    protected int bitsPerDoxel;
    protected Extent.Long dimensions;

    public AbstractTupleBuffer() {
        updateInternal(new Extent.Long(0l),Primitive.TYPE_UBYTE,1);
    }
    
    /**
     *
     * @param dimensions
     * @param sampleType : one of Primitive.Primitive.TYPE_X
     * @param nbSample
     */
    public AbstractTupleBuffer(final Extent.Long dimensions, final int sampleType, final int nbSample) {
        updateInternal(dimensions,sampleType,nbSample);
    }

    /**
     * 
     * @param dimensions
     * @param sampleType
     * @param nbSample 
     */
    protected void updateModel(final Extent.Long dimensions, final int sampleType, final int nbSample){
        updateInternal(dimensions,sampleType,nbSample);
    }
    
    private void updateInternal(final Extent.Long dimensions, final int sampleType, final int nbSample){
        this.dimensions = dimensions;
        this.sampleType = sampleType;
        this.nbSample = nbSample;
        this.bitsPerSample = Primitive.getSizeInBits(sampleType);
        this.bitsPerDoxel = nbSample*bitsPerSample;
    }
        
    public Buffer createPrimitiveBuffer(Extent.Long dimensions, BufferFactory factory) {
        if(factory==null) factory = getPrimitiveBuffer().getFactory();
        long size = bitsPerDoxel;
        for(int i=0,n=dimensions.getDimension();i<n;i++){
            size *= dimensions.get(i);
        }
        return factory.createByte(size);
    }

    public TupleBuffer create(Extent.Long dimensions) {
        return create(dimensions, getPrimitiveBuffer().getFactory());
    }
    
    public Extent.Long getExtent() {
        return dimensions;
    }

    public int getBitsPerSample(){
        return bitsPerSample;
    }

    public final int getPrimitiveType() {
        return sampleType;
    }

    public int getSampleCount() {
        return nbSample;
    }

    public TupleBuffer copy() {
        final Buffer bufferBase = getPrimitiveBuffer();
        return copy(bufferBase.copy(bufferBase.getPrimitiveType(), bufferBase.getEncoding()));
    }

    public Object createTupleStore() {
        switch(sampleType){
            case Primitive.TYPE_1_BIT : return new boolean[nbSample];
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE : return new byte[nbSample];
            case Primitive.TYPE_UBYTE : return new int[nbSample];
            case Primitive.TYPE_SHORT : return new short[nbSample];
            case Primitive.TYPE_USHORT : return new int[nbSample];
            case Primitive.TYPE_INT : return new int[nbSample];
            case Primitive.TYPE_UINT : return new long[nbSample];
            case Primitive.TYPE_LONG : return new long[nbSample];
            case Primitive.TYPE_FLOAT : return new float[nbSample];
            case Primitive.TYPE_DOUBLE : return new double[nbSample];
            default: throw new UnimplementedException("Unusual data type, sample model should overide this method.");
        }
    }

    public Object getTuple(int[] coordinate, Object buffer) {
        switch(sampleType){
            case Primitive.TYPE_1_BIT : return getTupleBoolean(coordinate, (boolean[])buffer);
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE : return getTupleByte(coordinate, (byte[])buffer);
            case Primitive.TYPE_UBYTE : return getTupleUByte(coordinate, (int[])buffer);
            case Primitive.TYPE_SHORT : return getTupleShort(coordinate, (short[])buffer);
            case Primitive.TYPE_USHORT : return getTupleUShort(coordinate, (int[])buffer);
            case Primitive.TYPE_INT : return getTupleInt(coordinate, (int[])buffer);
            case Primitive.TYPE_UINT : return getTupleUInt(coordinate, (long[])buffer);
            case Primitive.TYPE_LONG : return getTupleLong(coordinate, (long[])buffer);
            case Primitive.TYPE_FLOAT : return getTupleFloat(coordinate, (float[])buffer);
            case Primitive.TYPE_DOUBLE : return getTupleDouble(coordinate, (double[])buffer);
        }

        throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
    }

    public boolean[] getTupleBoolean(int[] coordinate, boolean[] buffer) {
        if(buffer == null) buffer = new boolean[nbSample];
        switch(sampleType){
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i] != 0;
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i] != 0;
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i] != 0;
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = us[i] != 0;
                break;
            case Primitive.TYPE_INT :
                final int[] in = getTupleInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = in[i] != 0;
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ui[i] != 0;
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = l[i] != 0;
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = f[i] != 0;
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = d[i] != 0;
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public byte[] getTupleByte(int[] coordinate, byte[] buffer) {
        if(buffer == null) buffer = new byte[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
                final boolean[] b = getTupleBoolean(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i] ? (byte)0 : (byte)1;
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)us[i];
                break;
            case Primitive.TYPE_INT :
                final int[] in = getTupleInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (byte)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public int[] getTupleUByte(int[] coordinate, int[] buffer) {
        if(buffer == null) buffer = new int[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] ub = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)us[i];
                break;
            case Primitive.TYPE_INT :
                final int[] in = getTupleInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public short[] getTupleShort(int[] coordinate, short[] buffer) {
        if(buffer == null) buffer = new short[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short) ub[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)us[i];
                break;
            case Primitive.TYPE_INT :
                final int[] in = getTupleInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (short)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public int[] getTupleUShort(int[] coordinate, int[] buffer) {
        if(buffer == null) buffer = new int[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_INT :
                final int[] in = getTupleInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public int[] getTupleInt(int[] coordinate, int[] buffer) {
        if(buffer == null) buffer = new int[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = us[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (int)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public long[] getTupleUInt(int[] coordinate, long[] buffer) {
        if(buffer == null) buffer = new long[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = us[i];
                break;
            case Primitive.TYPE_INT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (long)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (long)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public long[] getTupleLong(int[] coordinate, long[] buffer) {
        if(buffer == null) buffer = new long[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = us[i];
                break;
            case Primitive.TYPE_INT :
                final long[] in = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ui[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (long)f[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (long)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public float[] getTupleFloat(int[] coordinate, float[] buffer) {
        if(buffer == null) buffer = new float[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = us[i];
                break;
            case Primitive.TYPE_INT :
                final long[] in = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = l[i];
                break;
            case Primitive.TYPE_DOUBLE :
                final double[] d = getTupleDouble(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = (float)d[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public double[] getTupleDouble(int[] coordinate, double[] buffer) {
        if(buffer == null) buffer = new double[nbSample];
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                final byte[] b = getTupleByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = b[i];
                break;
            case Primitive.TYPE_UBYTE :
                final int[] ub = getTupleUByte(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ub[i];
                break;
            case Primitive.TYPE_SHORT :
                final short[] s = getTupleShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = s[i];
                break;
            case Primitive.TYPE_USHORT :
                final int[] us = getTupleUShort(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = us[i];
                break;
            case Primitive.TYPE_INT :
                final int[] in = getTupleInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = in[i];
                break;
            case Primitive.TYPE_UINT :
                final long[] ui = getTupleUInt(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = ui[i];
                break;
            case Primitive.TYPE_LONG :
                final long[] l = getTupleLong(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = l[i];
                break;
            case Primitive.TYPE_FLOAT :
                final float[] f = getTupleFloat(coordinate, null);
                for(int i=0;i<buffer.length;i++) buffer[i] = f[i];
                break;
            default :
                throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
        }

        return buffer;
    }

    public void setTuple(int[] coordinate, Object sample) {
        throw new UnimplementedException("Writing not supported");
    }

    public void setTuple(BBox box, Object sample) {
        box = clipToBufferBox(box);
        if(box==null) return;

        final int nbDim = box.getDimension();
        final int[] coordinate = new int[nbDim];

        for(int i=0;i<nbDim;i++){
            coordinate[i] = (int)box.getMin(i);
        }

        combinaison:
        for(;;){
            setTuple(coordinate, sample);
            //prepare next iteration
            for(int i=nbDim-1;i>=0;i--){
                coordinate[i]++;
                if(coordinate[i]>=box.getMax(i)){
                    if(i==0){
                        //we have finish
                        break combinaison;
                    }
                    //continue loop increment previous dimension
                    coordinate[i] = (int)box.getMin(i);
                }else{
                    break;
                }
            }
        }
    }

    public TupleIterator createIterator(BBox bbox) {
        bbox = clipToBufferBox(bbox);
        if(bbox==null) return EmptyTupleIterator.INSTANCE;
        return new DefaultTupleIterator(this,bbox);
    }

    /**
     * Ensure given bbox do not exit buffer bounds.
     * @param box
     * @return BoundingBox or null if no intersection
     */
    protected BBox clipToBufferBox(BBox box){
        if(box==null){
            //make a full buffer bbox
            box = new BBox(2);
            box.setRange(0, 0, dimensions.getL(0));
            box.setRange(1, 0, dimensions.getL(1));
            return box;
        }

        //ensure we are in the buffer zone
        long minx = (int) box.getMin(0);
        if(minx<0) minx=0;
        if(minx>=dimensions.getL(0)) return null;

        long maxx = (int) (box.getMax(0)+0.5);
        if(maxx<0) return null;
        if(maxx>dimensions.getL(0)) maxx = dimensions.getL(0);


        long miny = (int) box.getMin(1);
        if(miny<0) miny=0;
        if(miny>=dimensions.getL(1)) return null;

        long maxy = (int) (box.getMax(1)+0.5);
        if(maxy<0) return null;
        if(maxy>dimensions.getL(1)) maxy = dimensions.getL(1);

        box.setRange(0, minx, maxx);
        box.setRange(1, miny, maxy);
        return box;
    }

    /**
     * Test and convert sample container to this buffer type.
     * @param sampleContainer
     * @return
     */
    protected Object ensureType(Object sampleContainer){
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
                if(sampleContainer instanceof boolean[]) return sampleContainer;
                final boolean[] bools = new boolean[nbSample];
                for(int i=0;i<bools.length;i++) bools[i] = ((Number)Array.get(sampleContainer, i)).intValue() != 0;
                return bools;
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
                if(sampleContainer instanceof byte[]) return sampleContainer;
                final byte[] bytes = new byte[nbSample];
                for(int i=0;i<bytes.length;i++) bytes[i] = ((Number)Array.get(sampleContainer, i)).byteValue();
                return bytes;
            case Primitive.TYPE_UBYTE :
                if(sampleContainer instanceof int[]) return sampleContainer;
                final int[] ubytes = new int[nbSample];
                for(int i=0;i<nbSample;i++) ubytes[i] = ((Number)Array.get(sampleContainer, i)).intValue();
                return ubytes;
            case Primitive.TYPE_SHORT :
                if(sampleContainer instanceof short[]) return sampleContainer;
                final short[] shorts = new short[nbSample];
                for(int i=0;i<shorts.length;i++) shorts[i] = ((Number)Array.get(sampleContainer, i)).shortValue();
                return shorts;
            case Primitive.TYPE_USHORT :
                if(sampleContainer instanceof int[]) return sampleContainer;
                final int[] ushorts = new int[nbSample];
                for(int i=0;i<ushorts.length;i++) ushorts[i] = ((Number)Array.get(sampleContainer, i)).intValue();
                return ushorts;
            case Primitive.TYPE_INT :
                if(sampleContainer instanceof int[]) return sampleContainer;
                final int[] ints = new int[nbSample];
                for(int i=0;i<ints.length;i++) ints[i] = ((Number)Array.get(sampleContainer, i)).intValue();
                return ints;
            case Primitive.TYPE_UINT :
                if(sampleContainer instanceof long[]) return sampleContainer;
                final long[] uints = new long[nbSample];
                for(int i=0;i<uints.length;i++) uints[i] = ((Number)Array.get(sampleContainer, i)).longValue();
                return uints;
            case Primitive.TYPE_LONG :
                if(sampleContainer instanceof long[]) return sampleContainer;
                final long[] longs = new long[nbSample];
                for(int i=0;i<longs.length;i++) longs[i] = ((Number)Array.get(sampleContainer, i)).longValue();
                return longs;
            case Primitive.TYPE_FLOAT :
                if(sampleContainer instanceof float[]) return sampleContainer;
                final float[] floats = new float[nbSample];
                for(int i=0;i<floats.length;i++) floats[i] = ((Number)Array.get(sampleContainer, i)).floatValue();
                return floats;
            case Primitive.TYPE_DOUBLE :
                if(sampleContainer instanceof double[]) return sampleContainer;
                final double[] doubles = new double[nbSample];
                for(int i=0;i<doubles.length;i++) doubles[i] = ((Number)Array.get(sampleContainer, i)).doubleValue();
                return doubles;
            default: throw new UnimplementedException("Unusual data type, sample model should overide this method.");
        }
    }

    public int getHash() {
        int hash = 7;
        hash = 47 * hash + getPrimitiveType();
        hash = 47 * hash + getSampleCount();
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TupleBuffer other = (TupleBuffer) obj;
        if (this.sampleType != other.getPrimitiveType()) {
            return false;
        }
        if (this.nbSample != other.getSampleCount()) {
            return false;
        }
        if (!this.dimensions.equals(other.getExtent())) {
            return false;
        }
        if(!getPrimitiveBuffer().equals(other.getPrimitiveBuffer())){
            return false;
        }
        
        return true;
    }

    public static Buffer createPrimitiveBuffer(Extent.Long dimensions, int sampleType, int nbSample, BufferFactory factory) {
        long size = nbSample;
        for(int i=0,n=dimensions.getDimension();i<n;i++){
            size *= dimensions.get(i);
        }
        switch(sampleType){
            case Primitive.TYPE_1_BIT :
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE :
            case Primitive.TYPE_UBYTE :
            case Primitive.TYPE_SHORT :
            case Primitive.TYPE_USHORT :
            case Primitive.TYPE_INT24 :
            case Primitive.TYPE_UINT24 :
            case Primitive.TYPE_LONG :
            case Primitive.TYPE_ULONG :
                size *= Primitive.getSizeInBits(sampleType);
                size = ((size+7) / 8) ;
                return factory.createByte(size);
            case Primitive.TYPE_INT :
            case Primitive.TYPE_UINT :
                return factory.createInt(size);
            case Primitive.TYPE_FLOAT :
                return factory.createFloat(size);
            case Primitive.TYPE_DOUBLE :
                return factory.createDouble(size);
            default :
                throw new RuntimeException("Unsupported sample type "+sampleType);
        }
        
    }

}
