
package science.unlicense.api.geometry;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DataCursor;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.Primitive;

/**
 * Default tuple buffer implementation where each tuple are not to each other.
 * 
 *
 * @author Johann Sorel
 */
public class DefaultTupleBuffer extends AbstractTupleBuffer{

    protected final Buffer bank;
    //contains the steps between each value of a dimension
    private final int[] dimSteps;

    private static Extent.Long toDims(Extent box){
        Extent.Long dimSizes = new Extent.Long(box.getDimension());
        for(int i=0,n=box.getDimension();i<n;i++){
            dimSizes.set(i, box.get(i));
        }
        return dimSizes;
    }

    /**
     * Constructor for 2D bufer sample.
     */
    public DefaultTupleBuffer(Buffer bank,int sampleType, int nbSample,
            int width, int height) {
        this(bank,sampleType,nbSample,new Extent.Double(width,height));
    }

    /**
     * Constructor for ND samples.
     */
    public DefaultTupleBuffer(Buffer bank,int sampleType, int nbSample,
            Extent box) {
        super(toDims(box), sampleType, nbSample);
        this.bank = bank;
        this.dimSteps = new int[this.dimensions.getDimension()];
        this.dimSteps[0] = 1;
        for(int i=1;i<dimSteps.length;i++){
            this.dimSteps[i] = this.dimSteps[i-1]* (int)dimensions.getL(i-1);
        }

    }

    protected DefaultTupleBuffer(Buffer bank,int sampleType, int nbSample,
            Extent.Long dims) {
        super(dims, sampleType, nbSample);
        this.bank = bank;
        this.dimSteps = new int[this.dimensions.getDimension()];
        this.dimSteps[0] = 1;
        for(int i=1;i<dimSteps.length;i++){
            this.dimSteps[i] = this.dimSteps[i-1]* (int)dimensions.get(i-1);
        }
    }

    public DefaultTupleBuffer copy(Buffer bank) {
        return new DefaultTupleBuffer(bank, sampleType,
                nbSample, dimensions);
    }

    public Buffer getPrimitiveBuffer() {
        return bank;
    }

    public DefaultTupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
        return new DefaultTupleBuffer(createPrimitiveBuffer(dimensions,factory), 
                sampleType, nbSample, dimensions);
    }
    
    protected int getStartIndexInByte(int[] coordinate){
        return getStartIndexInBits(coordinate)/8;
    }

    protected int getStartIndexInByte(int coordinate){
        return getStartIndexInBits(coordinate)/8;
    }
    
    protected int getStartIndexInBits(int coordinate){
        return dimSteps[0]*coordinate*bitsPerDoxel;
    }
    
    protected int getStartIndexInBits(int[] coordinate){
        //we do not check coordinate
        int position = 0;
        for(int i=0;i<coordinate.length;i++){
            if(coordinate[i]>=dimensions.getL(i)){
                throw new RuntimeException("Invalid coordinate at index "+i+" value "+coordinate[i]+" , out of buffer range");
            }
            position += dimSteps[i]*coordinate[i]*bitsPerDoxel;
        }
        return position;
    }

    public boolean[] getTupleBoolean(int[] coordinate, boolean[] buffer) {
        if(sampleType == Primitive.TYPE_1_BIT){
            if(buffer == null) buffer = new boolean[nbSample];
            final int start = getStartIndexInBits(coordinate);
            int byteOffset = start/8;
            int bitoffset = start%8;
            for(int i=0;i<nbSample;i++){
                byte b = bank.readByte(byteOffset);
                buffer[i] = (b & (1 << (7-bitoffset))) > 0;

                //prepare next iteration
                if(bitoffset == 7){
                    byteOffset++;
                    bitoffset=0;
                }else{
                    bitoffset++;
                }
            }
            return buffer;
        }
        return super.getTupleBoolean(coordinate, buffer);
    }

    public byte[] getTupleByte(int[] coordinate, byte[] buffer) {
        if(sampleType == Primitive.TYPE_BYTE){
            if(buffer == null) buffer = new byte[nbSample];
            int k = getStartIndexInByte(coordinate);
            for(int i=0;i<nbSample;i++,k+=1)buffer[i] = bank.readByte(k);
            return buffer;
        }else if(sampleType == Primitive.TYPE_1_BIT){
            if(buffer == null) buffer = new byte[nbSample];
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;
            
            final DataCursor cursor = bank.cursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for(int i=0;i<nbSample;i++){
                buffer[i] = (byte)cursor.readBit(1);
            }
            
            return buffer;
        }else if(sampleType == Primitive.TYPE_2_BIT){
            if(buffer == null) buffer = new byte[nbSample];
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;
            
            final DataCursor cursor = bank.cursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for(int i=0;i<nbSample;i++){
                buffer[i] = (byte)cursor.readBit(2);
            }
            
            return buffer;
        }else if(sampleType == Primitive.TYPE_4_BIT){
            if(buffer == null) buffer = new byte[nbSample];
            final int k = getStartIndexInBits(coordinate);
            final int kb = k/8;
            final int offset = k%8;
            
            final DataCursor cursor = bank.cursor();
            cursor.setByteOffset(kb);
            cursor.setBitOffset(offset);
            for(int i=0;i<nbSample;i++){
                buffer[i] = (byte)cursor.readBit(4);
            }
            
            return buffer;
        }
        return super.getTupleByte(coordinate, buffer);
    }

    public int[] getTupleUByte(int[] coordinate, int[] buffer) {
        if(sampleType == Primitive.TYPE_UBYTE){
            if(buffer == null) buffer = new int[nbSample];
            final int start = getStartIndexInByte(coordinate);
            for(int i=0,k=start;i<nbSample;i++,k+=1)buffer[i] = bank.readByte(k) & 0xff;
            return buffer;
        }
        return super.getTupleUByte(coordinate, buffer);
    }

    public short[] getTupleShort(int[] coordinate, short[] buffer) {
        if(sampleType == Primitive.TYPE_SHORT){
            if(buffer == null) buffer = new short[nbSample];
            final int start = getStartIndexInByte(coordinate);
            bank.readShort(buffer, start);
            return buffer;
        }
        return super.getTupleShort(coordinate, buffer);
    }

    public int[] getTupleUShort(int[] coordinate, int[] buffer) {
        if(sampleType == Primitive.TYPE_USHORT){
            if(buffer == null) buffer = new int[nbSample];
            bank.readUShort(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleUShort(coordinate, buffer);
    }

    public int[] getTupleInt(int[] coordinate, int[] buffer) {
        if(sampleType == Primitive.TYPE_INT){
            if(buffer == null) buffer = new int[nbSample];
            bank.readInt(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleInt(coordinate, buffer);
    }

    public long[] getTupleUInt(int[] coordinate, long[] buffer) {
        if(sampleType == Primitive.TYPE_UINT){
            if(buffer == null) buffer = new long[nbSample];
            bank.readUInt(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleUInt(coordinate, buffer);
    }

    public long[] getTupleLong(int[] coordinate, long[] buffer) {
        if(sampleType == Primitive.TYPE_LONG){
            if(buffer == null) buffer = new long[nbSample];
            bank.readLong(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleLong(coordinate, buffer);
    }

    public float[] getTupleFloat(int[] coordinate, float[] buffer) {
        if(sampleType == Primitive.TYPE_FLOAT){
            if(buffer == null) buffer = new float[nbSample];
            bank.readFloat(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleFloat(coordinate, buffer);
    }

    public double[] getTupleDouble(int[] coordinate, double[] buffer) {
        if(sampleType == Primitive.TYPE_UBYTE){
            if(buffer == null) buffer = new double[nbSample];
            final int start = getStartIndexInByte(coordinate);
            for(int i=0,k=start;i<nbSample;i++,k+=1)buffer[i] = (bank.readByte(k) & 0xff);
            return buffer;
        }else if(sampleType == Primitive.TYPE_DOUBLE){
            if(buffer == null) buffer = new double[nbSample];
            bank.readDouble(buffer, getStartIndexInByte(coordinate));
            return buffer;
        }
        return super.getTupleDouble(coordinate, buffer);
    }

    public void setTuple(int[] coordinate, Object sample) {
        sample = ensureType(sample);
        if(sampleType == Primitive.TYPE_1_BIT){
            final int start = getStartIndexInBits(coordinate);
            int byteOffset = start/8;
            int bitoffset = start%8;
            for(int i=0;i<nbSample;i++){
                byte b = bank.readByte(byteOffset);
                if(((boolean[])sample)[i]){
                    b |= 1 << (7-bitoffset);
                }else{
                    b &= ~(1 << (7-bitoffset));
                }

                bank.writeByte(b, byteOffset);
                //prepare next iteration
                if(bitoffset == 7){
                    byteOffset++;
                    bitoffset=0;
                }else{
                    bitoffset++;
                }
            }
        }else if(sampleType == Primitive.TYPE_2_BIT){
            throw new RuntimeException("Unexpected type "+ sampleType);
        }else if(sampleType == Primitive.TYPE_4_BIT){
            throw new RuntimeException("Unexpected type "+ sampleType);
        }else if(sampleType == Primitive.TYPE_BYTE){
            final int start = getStartIndexInByte(coordinate);
            final byte[] buffer = (byte[]) sample;
            bank.writeByte(buffer, start);
        }else if(sampleType == Primitive.TYPE_UBYTE){
            final int start = getStartIndexInByte(coordinate);
            final int[] buffer = (int[]) sample;
            bank.writeUByte(buffer, start);
        }else if(sampleType == Primitive.TYPE_SHORT){
            final int start = getStartIndexInByte(coordinate);
            final short[] buffer = (short[]) sample;
            bank.writeShort(buffer, start);
        }else if(sampleType == Primitive.TYPE_USHORT){
            final int start = getStartIndexInByte(coordinate);
            final int[] buffer = (int[]) sample;
            bank.writeUShort(buffer, start);
        }else if(sampleType == Primitive.TYPE_INT){
            final int start = getStartIndexInByte(coordinate);
            final int[] buffer = (int[]) sample;
            bank.writeInt(buffer, start);
        }else if(sampleType == Primitive.TYPE_UINT){
            final int start = getStartIndexInByte(coordinate);
            final long[] buffer = (long[]) sample;
            bank.writeUInt(buffer, start);
        }else if(sampleType == Primitive.TYPE_LONG){
            final int start = getStartIndexInByte(coordinate);
            final long[] buffer = (long[]) sample;
            bank.writeLong(buffer, start);
        }else if(sampleType == Primitive.TYPE_FLOAT){
            final int start = getStartIndexInByte(coordinate);
            final float[] buffer = (float[]) sample;
            bank.writeFloat(buffer, start);
        }else if(sampleType == Primitive.TYPE_DOUBLE){
            final int start = getStartIndexInByte(coordinate);
            final double[] buffer = (double[]) sample;
            bank.writeDouble(buffer, start);
        }else{
            throw new RuntimeException("Unexpected type "+ sampleType);
        }
    }

    public void setTuple(BBox box, Object sample) {
        if(sampleType == Primitive.TYPE_BYTE && nbSample == 1 && box.getDimension() == 2){
            //more effecient fill for casual 2d buffers
            final int bytePerLine = (int) dimensions.getL(0);

            box = clipToBufferBox(box);
            if(box==null)return;

            //ensure we are in the buffer zone
            int minx = (int) box.getMin(0);
            int maxx = (int) box.getMax(0);
            int miny = (int) box.getMin(1);
            int maxy = (int) box.getMax(1);
            final int spanx = maxx-minx;

            //TODO this is not guarante, backend can be anything
            final byte[] array = (byte[]) bank.getBackEnd();
            final byte b = ((byte[])sample)[0];

            int offset = miny*bytePerLine + minx;
            for(int i=miny;i<maxy;i++){
                Arrays.fill(array, offset, spanx, b);
                offset += bytePerLine;
            }

        }else{
            super.setTuple(box, sample);
        }
    }

    public TupleIterator createIterator(BBox bbox) {
        if(nbSample==1&&sampleType==Primitive.TYPE_BYTE&&dimensions.getDimension()==2){
            bbox = clipToBufferBox(bbox);
            if(bbox==null) return EmptyTupleIterator.INSTANCE;
            return new InterleavedIByteterator(bbox);
        }
        return super.createIterator(bbox);
    }

    private class InterleavedIByteterator implements TupleIterator{

        private final int[] coordinate = new int[dimensions.getDimension()];
        private final byte[] sample = new byte[1];
        //TODO this is not guarante, backend can be anything
        private final byte[] datas = (byte[]) bank.getBackEnd();
        private final int endIndex;
        private final int linestep;
        private final int linelength;
        private int index;
        private int lineIndex;
        private int pindex;

        public InterleavedIByteterator(BBox bbox) {
            final int[] dimStart = new int[dimensions.getDimension()];
            final int[] dimEnd = new int[dimStart.length];
            for(int i=0;i<dimStart.length;i++){
                dimStart[i] = (int)bbox.getMin(i);
                dimEnd[i] = (int)bbox.getMax(i);
                coordinate[i] = dimStart[i];
            }

//            for(int i=0;i<coordinate.length;i++){
//                if(dimStart[i]>=dimensions[i]){
//                    //bbox is out of buffer
//                    index=1;
//                    endIndex=0;
//                    linestep=0;
//                    linelength=0;
//                    return;
//                }
//            }

            //move to start position
            index = getStartIndexInByte(dimStart);
            //calculate end position
            endIndex = getStartIndexInByte(new int[]{dimEnd[0]-1,dimEnd[1]-1});
            //calculate length of a line clipped by the bbox
            linelength = dimEnd[0]-1-dimStart[0];
            //calculate step between end line x and next line start x
            linestep = (int) (dimensions.getL(0)-linelength);
        }

        public int[] getCoordinate() {
            coordinate[0] = pindex%(int)dimensions.getL(0);
            coordinate[1] = pindex/(int)dimensions.getL(0);
            return coordinate;
        }

        public Object next() {
            if(index>endIndex) return null;
            sample[0] = datas[index];
            pindex = index;
            lineIndex++;
            if(lineIndex>linelength){
                index += linestep;
                lineIndex=0;
            }else{
                index++;
            }
            return sample;
        }

        public boolean[] nextAsBoolean(boolean[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte[] nextAsByte(byte[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int[] nextAsUByte(int[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short[] nextAsShort(short[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int[] nextAsUShort(int[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int[] nextAsInt(int[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long[] nextAsUInt(long[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long[] nextAsLong(long[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public float[] nextAsFloat(float[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

        public double[] nextAsDouble(double[] buffer) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

}
