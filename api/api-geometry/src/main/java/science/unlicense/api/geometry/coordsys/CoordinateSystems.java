
package science.unlicense.api.geometry.coordsys;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.math.transform.ConcatenateTransform;
import science.unlicense.api.math.transform.DimStackTransform;
import science.unlicense.api.math.transform.RearrangeTransform;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.unit.Units;
import science.unlicense.impl.math.Affine1;

/**
 *
 * @author Johann Sorel
 */
public class CoordinateSystems {
    
    public static Transform createTransform(CoordinateSystem source, CoordinateSystem target){
        
        final Axis[] srcAxis = source.getAxis();
        final Axis[] trgAxis = target.getAxis();
        if(srcAxis.length < trgAxis.length){
            throw new InvalidArgumentException("Source dimension has less axis then target : "
                    +srcAxis.length+" "+trgAxis.length);
        }
        
        
        //find axis mapping
        final int dim = target.getDimension();
        final int[] mapping = new int[srcAxis.length];
        Arrays.fill(mapping, -1);
        final Transform[] trsArray = new Transform[dim];
        
        boolean needRearrange = srcAxis.length != trgAxis.length;
        
        loop:
        for(int i=0;i<trgAxis.length;i++){
            final Axis tgtAxis = trgAxis[i];
            for(int k=0;k<srcAxis.length;k++){
                final Axis srAxis = srcAxis[k];
                if(tgtAxis.getDirection().isCompatible(srAxis.getDirection())!=0){
                    trsArray[i] = createTransform(srAxis, tgtAxis);
                    mapping[k] = i;
                    if(i!=k) needRearrange = true;
                    continue loop;
                }
            }
            throw new InvalidArgumentException("No mapping for axis : "+tgtAxis);
        }
        
        //aggregate each axis transform
        final Transform stackTrs = DimStackTransform.create(trsArray);
        
        if(needRearrange){
            //rearrange values
            final Transform rearrange = RearrangeTransform.create(mapping);
            return ConcatenateTransform.create(rearrange,stackTrs);
        }else{
            return stackTrs;
        }
        
    }
    
    public static Transform createTransform(Axis source, Axis target){
        
        final Direction sourceDirection = source.getDirection();
        final Direction targetDirection = target.getDirection();
        //direction factor
        final int factor = sourceDirection.isCompatible(targetDirection);
        if(factor==0){
            throw new InvalidArgumentException("Axis are not compatible");
        }
        
        
        //transform for units
        final Transform unitTransform = Units.getTransform(source.getUnit(), target.getUnit());
        
        if(factor==1){
            return unitTransform;
        }else{
            //invert values
            final Transform scaleTransform = new Affine1(-1,0);
            return ConcatenateTransform.create(unitTransform, scaleTransform);
        }
        
    }
    
}
