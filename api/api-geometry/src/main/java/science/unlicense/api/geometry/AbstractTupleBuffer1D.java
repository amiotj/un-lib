
package science.unlicense.api.geometry;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleBuffer1D extends AbstractTupleBuffer implements TupleBuffer1D{

    @Override
    public TupleBuffer1D copy() {
        return copy(createPrimitiveBuffer(dimensions,null));
    }

    @Override
    public Object getTuple(int coordinate, Object buffer) {
        switch(sampleType){
            case Primitive.TYPE_1_BIT : return getTupleBoolean(coordinate, (boolean[])buffer);
            case Primitive.TYPE_2_BIT :
            case Primitive.TYPE_4_BIT :
            case Primitive.TYPE_BYTE : return getTupleByte(coordinate, (byte[])buffer);
            case Primitive.TYPE_UBYTE : return getTupleUByte(coordinate, (int[])buffer);
            case Primitive.TYPE_SHORT : return getTupleShort(coordinate, (short[])buffer);
            case Primitive.TYPE_USHORT : return getTupleUShort(coordinate, (int[])buffer);
            case Primitive.TYPE_INT : return getTupleInt(coordinate, (int[])buffer);
            case Primitive.TYPE_UINT : return getTupleUInt(coordinate, (long[])buffer);
            case Primitive.TYPE_LONG : return getTupleLong(coordinate, (long[])buffer);
            case Primitive.TYPE_FLOAT : return getTupleFloat(coordinate, (float[])buffer);
            case Primitive.TYPE_DOUBLE : return getTupleDouble(coordinate, (double[])buffer);
        }

        throw new UnimplementedException("Uncorrect data type, refer to primitive data type.");
    }

    @Override
    public boolean[] getTupleBoolean(int coordinate, boolean[] buffer) {
        return getTupleBoolean(new int[]{coordinate}, buffer);
    }

    @Override
    public byte[] getTupleByte(int coordinate, byte[] buffer) {
        return getTupleByte(new int[]{coordinate}, buffer);
    }

    @Override
    public int[] getTupleUByte(int coordinate, int[] buffer) {
        return getTupleUByte(new int[]{coordinate}, buffer);
    }

    @Override
    public short[] getTupleShort(int coordinate, short[] buffer) {
        return getTupleShort(new int[]{coordinate}, buffer);
    }

    @Override
    public int[] getTupleUShort(int coordinate, int[] buffer) {
        return getTupleUShort(new int[]{coordinate}, buffer);
    }

    @Override
    public int[] getTupleInt(int coordinate, int[] buffer) {
        return getTupleInt(new int[]{coordinate}, buffer);
    }

    @Override
    public long[] getTupleUInt(int coordinate, long[] buffer) {
        return getTupleUInt(new int[]{coordinate}, buffer);
    }

    @Override
    public long[] getTupleLong(int coordinate, long[] buffer) {
        return getTupleLong(new int[]{coordinate}, buffer);
    }

    @Override
    public float[] getTupleFloat(int coordinate, float[] buffer) {
        return getTupleFloat(new int[]{coordinate}, buffer);
    }

    @Override
    public double[] getTupleDouble(int coordinate, double[] buffer) {
        return getTupleDouble(new int[]{coordinate}, buffer);
    }
    
}
