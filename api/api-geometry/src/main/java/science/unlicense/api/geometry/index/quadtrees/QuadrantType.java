
package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.geometry.BBox;

/**
 * The type of quadrants in quadtrees.
 * 
 * @author Mark Raynsford
 */
public abstract class QuadrantType extends BBox{

    public QuadrantType() {
        super(2);
    }
    
}
