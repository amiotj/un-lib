
package science.unlicense.api.geometry.index;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;

/**
 * 
 * @author Johann Sorel
 */
public class QuadTree {

    private QuadNode root;
    private final int maxDepth;

    public QuadTree() {
        this(8);
    }

    public QuadTree(int maxDepth) {
        this(2,maxDepth,null);
    }
    
    public QuadTree(int dimension, int maxDepth, BBox bounds) {
        this.maxDepth = maxDepth;
        this.root = new QuadNode(dimension);
        
        if(bounds!=null){
            this.root.set(bounds);
        }else{
            root.setToNaN();
        }
    }
    
    public int getMaxDepth() {
        return maxDepth;
    }

    public BBox getBounds() {
        return new BBox(root);
    }

    public BBox getRecordBounds(){
        final BBox bbox = new BBox(root.getDimension());
        bbox.setToNaN();
        root.getRecordBounds(bbox);
        return bbox;
    }
    
    public QuadNode getRoot() {
        return root;
    }
    
    public Sequence search(BBox bbox){
        final Sequence results = new ArraySequence();
        search(bbox, root, results);
        return results;
    }
    
    private void search(BBox bbox, QuadNode node, Sequence results){
        if(bbox.contains(node)){
            //take all records, no need to test intersection
            node.getAllRecords(results);
        }else if(bbox.intersects(node)){
            //search intersecting records
            final Sequence records = node.getRecords();
            for(int i=0;i<records.getSize();i++){
                final IndexRecord rec = (IndexRecord) records.get(i);
                if(rec.intersects(bbox)) results.add(rec);
            }

            final QuadNode[] children = node.getChildren();
            if(children!=null){
                for(int i=0;i<children.length;i++){
                    search(bbox,children[i],results);
                }
            }
        }
    }
    
    public void add(IndexRecord rec){
        if(!root.isValid() || !root.contains(rec)){
            //root is empty or too small, resize the tree
            final BBox newSize = new BBox(rec);
            if(root.isValid()) newSize.expand(newSize);
            resize(newSize);
        }
        insert(root, rec, 0);
    }
        
    private void insert(QuadNode node, IndexRecord rec, int depth){
        if(depth>maxDepth){
            //max depth reach, insert in this node
            node.getRecords().add(rec);
        }else{
            
            //search if a children can contain it
            final QuadNode[] children = node.getChildrenCreate();
            for(int i=0;i<children.length;i++){
                if(children[i].contains(rec)){
                    insert(children[i], rec, depth+1);
                    return;
                }
            }
            
            //no children can fully contain it
            node.getRecords().add(rec);
        }
    }
    
    private void resize(BBox bbox){
        final Sequence allRecords = new ArraySequence();
        root.getAllRecords(allRecords);
        
        root = new QuadNode(bbox);
        for(int i=0;i<allRecords.getSize();i++){
            insert(root, (IndexRecord)allRecords.get(i), 0);
        }
    }
    
}
