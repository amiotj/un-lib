package science.unlicense.api.geometry.index.quadtrees;

import science.unlicense.api.math.Tuple;

/**
 * The type of procedures used when iterating over quadrants in quadtrees.
 *
 * @param <E> The type of raised exceptions.
 * @author Mark Raynsford
 */
public interface QuadTreeTraversalType<E extends Throwable> {

    /**
     * Visit a node.
     *
     * @param depth The node depth.
     * @param lower The lower corner.
     * @param upper The upper corner.
     * @throws E If required.
     */
    void visit(final int depth,final Tuple lower,final Tuple upper) throws E;
    
}
