
package science.unlicense.api.geometry.coordsys;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.unit.Units;

/**
 * A coordinate system define a ordered set of axis.
 * 
 * In 3D models and animations may be stored in different coordinate system conventions.
 * Those are know as LeftHand or RightHand coordinate systems.
 * 
 * TODO, find a proper way to merge with CSUtilities and to declare this information
 * on classes which need it.
 * 
 * @author Johann Sorel
 */
public class CoordinateSystem extends CObject{

    public static final CoordinateSystem CARTESIAN3D_METRIC_RIGH_HANDED = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.BACKWARD, Units.METER)
            }
    );
    
    public static final CoordinateSystem CARTESIAN3D_METRIC_LEFT_HANDED = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.METER),
                new Axis(Direction.UP, Units.METER),
                new Axis(Direction.FORWARD, Units.METER)
            }
    );
            
    private final Axis[] axis;

    public CoordinateSystem(Axis[] axis) {
        this.axis = axis;
    }
    
    public int getDimension(){
        return axis.length;
    }

    public Axis[] getAxis() {
        return Arrays.copy(axis, new Axis[axis.length]);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(new Chars("CS["));
        cb.append(axis[0].toChars());
        for(int i=1;i<axis.length;i++){
            cb.append(',').append(axis[i].toChars());
        }
        cb.append(new Chars("]"));
        return cb.toChars();
    }

    @Override
    public int getHash() {
        int hash = 5;
        hash = 41 * hash + Arrays.computeHash(this.axis);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CoordinateSystem other = (CoordinateSystem) obj;
        return Arrays.equals(this.axis, other.axis);
    }
    
}
