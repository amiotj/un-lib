
package science.unlicense.api.geometry.index.octtrees;

import science.unlicense.api.geometry.BBox;

/**
 * <p>
 * The type of objects that can be placed into an octtree. Objects are
 * required to:
 * </p>
 * <ul>
 * <li>Be comparable and have a total order, in order to allow for "set"
 * semantics within the tree.</li>
 * <li>Have axis-aligned bounding boxes, for spatial queries.</li>
 * </ul>
 *
 * @param <T>The precise type of octtree members.
 * @author Mark Raynsford
 */
public abstract class OctTreeMemberType<T> extends BBox implements Comparable<T>{

    public OctTreeMemberType() {
        super(3);
    }
    
}
