
package science.unlicense.api.io;

import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.path.Path;

/**
 * Base class for writer implementations.
 * Provides the common used functionnalities to properly open and close
 * the output.
 * 
 * @author Johann Sorel
 */
public class AbstractWriter implements Writer{

    private boolean closeStream = false;
    
    protected Object output;
    private ByteOutputStream byteStream;
    private DataOutputStream dataStream;
    private CharOutputStream charStream;
    
    public void setOutput(Object output) throws IOException {
        this.output = output;
    }

    public Object getOutput() {
        return output;
    }

    public Document getConfiguration() {
        return null;
    }

    public void setConfiguration(Document configuration) {
        //ignore it
    }
    
    protected final ByteOutputStream getOutputAsByteStream() throws IOException {
        if(byteStream!=null) return byteStream;

        Object output = this.output;
        if(output instanceof Chars){
            //try to resolve it
            //TODO cyclic reference
        }
        
        if(output instanceof ByteOutputStream){
            byteStream = (ByteOutputStream)output;
            //we did not create the stream, we do not close it.
            closeStream = false;
            return byteStream;
        }else if(output instanceof Path){
            byteStream = ((Path)output).createOutputStream();
            closeStream = true;
            return byteStream;
        }else{
            throw new IOException("Unsupported output : " + output);
        }
    }
    
    protected final DataOutputStream getOutputAsDataStream(NumberEncoding encoding) throws IOException {
        if(dataStream!=null) return dataStream;
        dataStream = new DataOutputStream(getOutputAsByteStream(), encoding);
        return dataStream;
    }
        
    protected final CharOutputStream getOutputAsCharStream(CharEncoding encoding) throws IOException {
        if(charStream!=null) return charStream;
        charStream = new CharOutputStream(getOutputAsByteStream(),encoding);
        return charStream;
    }
    
    public void dispose() throws IOException {
        if(closeStream && byteStream!=null){
            byteStream.close();
            byteStream = null;
            dataStream = null;
            charStream = null;
        }
    }
    
    private void checkOpen() throws IOException {
        if(byteStream!=null){
            throw new IOException("Input stream is already open.");
        }
    }
    
}
