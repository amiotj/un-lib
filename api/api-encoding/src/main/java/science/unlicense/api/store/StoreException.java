
package science.unlicense.api.store;

/**
 * Store exception.
 * 
 * @author Johann Sorel
 */
public class StoreException extends Exception{

    public StoreException() {
        super();
    }

    public StoreException(String message) {
        super(message);
    }
    
    public StoreException(Throwable throwable) {
        super(throwable);
    }
    
    public StoreException(Exception exception) {
        super(exception.getMessage(),exception);
    }
    
    public StoreException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
    
}
