
package science.unlicense.api.io;

import science.unlicense.api.model.doc.Document;

/**
 * Many API share a common approach of Reader and Writer.
 * This interface and the AbstractReader provides the general needs
 * to open, convert to binary or char stream and dispose them.
 * 
 * @author Johann Sorel
 */
public interface Reader {
    
    /**
     * Set the reader input.
     * The type of input supported may change based on the reader.
     * 
     * @param input source to read from,
     * @throws IOException if input type is not supported.
     */
    void setInput(Object input) throws IOException;

    /**
     * Configure this object which specific parameters.
     * To obtain the possible parameters call getConfiguration().getType();
     *
     * @param configuration
     */
    void setConfiguration(Document configuration);

    /**
     * Get configuration.
     *
     * @return configuration, never null
     */
    Document getConfiguration();
    
    /**
     * Get the currently set input.
     * 
     * @return Object can be null.
     */
    Object getInput();
    
    /**
     * Release this reader, freeing any additional resources opened
     * by the reader.
     * Implementation may be reusable other not.
     * 
     * @throws IOException if freeing all resources failed.
     */
    void dispose() throws IOException;

}
