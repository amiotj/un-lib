
package science.unlicense.api.io;

import science.unlicense.api.exception.InvalidArgumentException;

/**
 * A ByteOutputStream which wrap another.
 *
 * @author Johann Sorel
 */
public abstract class WrapOutputStream extends AbstractOutputStream{

    protected final ByteOutputStream out;

    public WrapOutputStream(ByteOutputStream out) {
        if(out == null){
            throw new InvalidArgumentException("Outputstream must not be null.");
        }
        this.out = out;
    }

    public void flush() throws IOException {
        out.flush();
    }

    public void close() throws IOException {
        out.close();
    }

}
