package science.unlicense.api.io;

import science.unlicense.api.buffer.Buffer;

/**
 * Basic byte stream, strict minimum required.
 * 
 * InputStreams are not concurrent.
 *
 * @author Johann Sorel
 */
public interface ByteInputStream {

    /**
     * Read next byte.
     *
     * @return byte as int or -1 if reached end of stream
     * @throws IOException
     */
    int read() throws IOException;

    /**
     * Read as much byte as can contain the buffer.
     *
     * @param buffer
     * @return number of bytes readed or -1 if reached end of stream
     * @throws IOException
     */
    int read(byte[] buffer) throws IOException;

    /**
     * Read 'length' byte and store them at 'offset' in 'buffer'.
     *
     * @param buffer
     * @param offset
     * @param length
     * @return number of bytes readed or -1 if reached end of stream
     * @throws IOException
     */
    int read(byte[] buffer, int offset, int length) throws IOException;
    
    /**
     * Read 'length' byte and store them at 'offset' in 'buffer'.
     *
     * @param buffer
     * @param offset
     * @param length
     * @return number of bytes readed or -1 if reached end of stream
     * @throws IOException
     */
    int read(Buffer buffer, int offset, int length) throws IOException;
    
    /**
     * Skip next n bytes.
     * returns number of byte skiped.
     *
     * @param n
     * @return
     * @throws IOException
     */
    long skip(long n) throws IOException;

    /**
     * Close the stream.
     *
     * @throws IOException
     */
    void close() throws IOException;

}
