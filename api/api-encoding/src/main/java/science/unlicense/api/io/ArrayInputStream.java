package science.unlicense.api.io;

/**
 * Byte input stream backed by a byte array.
 *
 * @author Johann Sorel
 */
public class ArrayInputStream extends AbstractInputStream{

    private final byte[] array;
    private int index = 0;

    public ArrayInputStream(byte[] array) {
        this.array = array;
    }

    /**
     * Current index in the array.
     * @return
     */
    public int getIndex() {
        return index;
    }

    public int read() throws IOException {
        if(index>=array.length){
            return -1;
        }
        final int b = array[index] & 0xff; //unsign it
        index++;
        return b;
    }

    public void close() throws IOException {
    }

}
