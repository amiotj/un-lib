
package science.unlicense.api.io;

import science.unlicense.api.collection.primitive.ByteSequence;

/**
 *
 * @author Johann Sorel
 */
public class ArrayOutputStream extends AbstractOutputStream{

    private final ByteSequence buffer;

    public ArrayOutputStream() {
        this(null);
    }

    public ArrayOutputStream(ByteSequence buffer) {
        if(buffer == null){
            buffer = new ByteSequence();
        }
        this.buffer = buffer;
    }

    public ByteSequence getBuffer() {
        return buffer;
    }

    @Override
    public void write(byte b) throws IOException {
        buffer.put(b);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        this.buffer.put(buffer, offset, length);
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public void flush() throws IOException {
    }

}
