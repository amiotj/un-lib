
package science.unlicense.api.path;

import science.unlicense.api.CObjects;
import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.AbstractNode;
import science.unlicense.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractPath extends AbstractNode implements Path {
    
    public AbstractPath() {
    }

    public boolean canHaveChildren() {
        try {
            return isContainer();
        }catch(IOException ex){
            return false;
        }
    }

    public Object getPathInfo(Chars key) {
        return null;
    }
    
    @Override
    public Collection getChildren(Predicate filter) {
        return Collections.filter(getChildren(), filter);
    }

    public void delete() throws IOException {
        throw new IOException("Delete not supported.");
    }
    
    public final int getHash() {
        return toURI().hashCode();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if(obj instanceof Path){
            return CObjects.equals( ((Path)obj).toURI(), this.toURI());
        }

        return false;
    }

    public SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException {
        throw new IOException("Not supported.");
    }

    public Chars thisToChars() {
        return new Chars(getName());
    }

}
