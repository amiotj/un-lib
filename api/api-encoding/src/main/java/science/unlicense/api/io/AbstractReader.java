
package science.unlicense.api.io;

import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.impl.io.BacktrackInputStream;

/**
 * Base class for reader implementations.
 * Provides the common used functionnalities to properly open and close
 * the input.
 * 
 * @author Johann Sorel
 */
public class AbstractReader implements Reader{

    private boolean closeStream = false;
    
    protected Object input;
    private ByteInputStream byteStream;
    private DataInputStream dataStream;
    private BacktrackInputStream backStream;
    private CharInputStream charStream;
    private Document config = new DefaultDocument();
    
    public void setInput(Object input) throws IOException {
        this.input = input;
        //remove all cached streams
        byteStream = null;
        dataStream = null;
        backStream = null;
        charStream = null;
    }

    public Object getInput() {
        return input;
    }

    public Document getConfiguration() {
        return null;
    }

    public void setConfiguration(Document configuration) {
        //ignore it
    }

    protected final ByteInputStream getInputAsByteStream() throws IOException {
        if(byteStream!=null) return byteStream;

        Object input = this.input;
        if(input instanceof Chars){
            //try to resolve it
            //TODO cyclic reference
        }
        
        final boolean[] res = new boolean[1];
        byteStream = IOUtilities.toInputStream(input, res);
        closeStream = res[0];
        
        return byteStream;
    }
    
    protected final DataInputStream getInputAsDataStream(NumberEncoding encoding) throws IOException {
        if(dataStream!=null) return dataStream;
        dataStream = new DataInputStream(getInputAsByteStream(), encoding);
        return dataStream;
    }
    
    protected final BacktrackInputStream getInputAsBacktrackStream() throws IOException {
        if(backStream!=null) return backStream;
        backStream = new BacktrackInputStream(getInputAsByteStream());
        backStream.mark();
        return backStream;
    }
    
    protected final CharInputStream getInputAsCharStream(CharEncoding encoding) throws IOException {
        if(charStream!=null) return charStream;
        charStream = new CharInputStream(getInputAsByteStream(),encoding);
        return charStream;
    }
    
    protected final CharInputStream getInputAsCharStream(CharEncoding encoding, Char endline) throws IOException {
        if(charStream!=null) return charStream;
        charStream = new CharInputStream(getInputAsByteStream(),encoding,endline);
        return charStream;
    }
    
    public void dispose() throws IOException {
        if(closeStream && byteStream!=null){
            byteStream.close();
            byteStream = null;
            dataStream = null;
            backStream = null;
            charStream = null;
        }
    }
    
    private void checkOpen() throws IOException {
        if(byteStream!=null){
            throw new IOException("Input stream is already open.");
        }
    }
    
}
