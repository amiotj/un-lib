
package science.unlicense.api.io;

import science.unlicense.api.buffer.Buffer;


/**
 * Basic byte stream, strict minimum required.
 *
 * @author Johann Sorel
 */
public interface ByteOutputStream {


    void write(byte b) throws IOException;

    void write(byte[] buffer) throws IOException;

    void write(byte[] buffer, int offset, int length) throws IOException;

    void write(Buffer buffer, int offset, int length) throws IOException;
    
    /**
     *
     * @throws IOException
     */
    void flush() throws IOException;

    /**
     * Close the stream.
     *
     * @throws IOException
     */
    void close() throws IOException;

}
