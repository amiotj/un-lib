

package science.unlicense.api.io;


/**
 * Seekable byte buffer, used to map large amount of datas.
 * 
 * @author Johann Sorel
 */
public interface SeekableByteBuffer {
    
    /**
     * Total size of this buffer.
     * 
     * @return size in bytes.
     */
    long getSize() throws IOException;
        
    /**
     * Indicate if reading operations are supported.
     * methods : read(...) and skip(...)
     * 
     * @return true if reading operations are supported
     */
    boolean isReadable();
    
    /**
     * Indicate if writing operations are supported.
     * methods : write(...)
     * 
     * @return true if writing operations are supported
     */
    boolean isWritable();
    
    /**
     * Indicate if resizing operations are supported.
     * methods : insert(...) and remove(...)
     * 
     * @return true if resizing operations are supported
     */
    boolean isResizable();
    
    /**
     * Move buffer to given position.
     * 
     * @param position new position
     */
    void setPosition(long position) throws IOException;
    
    /**
     * Get buffer current position.
     * 
     * @return position
     * @throws IOException 
     */
    long getPosition() throws IOException;
    
    /**
     * Skip given number of bytes.
     * This method may not skip all bytes in one time
     * 
     * @param nb, number of bytes to skip
     * @return skipped bytes
     * @throws IOException 
     */
    long skip(long nb) throws IOException;
    
    /**
     * Skip given number of bytes.
     * This method ensure all byes are skipped.
     * 
     * @param nb
     * @throws IOException 
     */
    void skipAll(long nb) throws IOException;
    
    /**
     * Get a single byte, moves position by 1.
     * 
     * @return
     * @throws IOException 
     */
    int read() throws IOException;
    
    /**
     * Get multiple bytes, moves position by number of bytes.
     * 
     * @param nb
     * @return
     * @throws IOException 
     */
    byte[] read(int nb) throws IOException;
    
    /**
     * Get multiple bytes, moves position by number of bytes.
     * 
     * @param buffer
     * @return
     * @throws IOException 
     */
    byte[] read(byte[] buffer) throws IOException;
    
    /**
     * Get multiple bytes, moves position by number of bytes.
     * 
     * @param buffer
     * @param offset
     * @param length
     * @return
     * @throws IOException 
     */
    byte[] read(byte[] buffer, int offset, int length) throws IOException;
    
    /**
     * Set a single byte, moves position by 1.
     * 
     * @param b
     * @throws IOException 
     */
    void write(byte b) throws IOException;
        
    /**
     * Set multiple bytes, moves position by number of bytes.
     * 
     * @param buffer
     * @throws IOException 
     */
    void write(byte[] buffer) throws IOException;
    
    /**
     * Set multiple bytes, moves position by number of bytes.
     * 
     * @param buffer
     * @param offset
     * @param length
     * @throws IOException 
     */
    void write(byte[] buffer, int offset, int length) throws IOException;
    
    /**
     * Insert a single byte, moves position by 1 and grow size.
     * 
     * @param b
     * @throws IOException 
     */
    void insert(byte b) throws IOException;
        
    /**
     * Insert multiple bytes, moves position by number of bytes and grow size.
     * 
     * @param buffer
     * @throws IOException 
     */
    void insert(byte[] buffer) throws IOException;
    
    /**
     * Insert multiple bytes, moves position by number of bytes and grow size.
     * 
     * @param buffer
     * @param offset
     * @param length
     * @throws IOException 
     */
    void insert(byte[] buffer, int offset, int length) throws IOException;
    
    /**
     * Remove number of bytes located after current position.
     * Position is unchanged
     * 
     * @param nb number of bytes to remove
     * @throws IOException 
     */
    void remove(int nb) throws IOException;
        
    /**
     * Flush any waiting operations.
     * 
     * @throws IOException 
     */
    void flush() throws IOException;
    
    /**
     * Release resources.
     * 
     * @throws IOException 
     */
    void dispose() throws IOException;
    
}
