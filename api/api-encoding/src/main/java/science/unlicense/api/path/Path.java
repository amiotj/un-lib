
package science.unlicense.api.path;

import science.unlicense.api.io.SeekableByteBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.predicate.Predicate;

/**
 * A Path is similar to a pointer toward some datas.
 * Similar to linux mount points, a path can be toward a hard disk file,
 * a distant file service like ftp or html or within archives.
 *
 * @author Johann Sorel
 */
public interface Path extends Node, EventSource{

    /**
     * In path resolving, this path is noted as '.'
     */
    public static final Chars PATH_SELF = new Chars(".");
    /**
     * In path resolving, this path parent is noted as '..'
     */
    public static final Chars PATH_PARENT = new Chars("..");

    /**
     * Path info for hidden property.
     */
    public static final Chars INFO_HIDDEN = new Chars("hidden");
    /**
     * Path size in bytes.
     */
    public static final Chars INFO_OCTETSIZE = new Chars("octetsize");
    /**
     * Path last modified date.
     */
    public static final Chars INFO_LASTMODIFIED = new Chars("lastmodified");
    
    /**
     * Get this path segment name.
     */
    Chars getName();
    
    /**
     * Path can hold multiple information, like :
     * - hidden
     * - date
     * - user
     * 
     * @return info value or null if unknowned.
     */
    Object getPathInfo(Chars key);
    
    /**
     * Parent Path if any.
     * @return Path or null
     */
    Path getParent();

    /**
     * Return path children.
     *
     * @return never null, can be empty
     */
    Collection getChildren(Predicate filter);

    /**
     * Test if this path is a container in the current resolver.
     * (only serve as a segment in the tree).
     *
     * @return true if this path exist and is a container
     * @throws IOException
     */
    boolean isContainer() throws IOException;

    /**
     * Test if this path target exists.
     * 
     * @return true if this path point toward something that exists
     * @throws IOException 
     */
    boolean exists() throws IOException;
    
    /**
     * Create the path container if it doesn't exist.
     * @return
     * @throws IOException
     */
    boolean createContainer() throws IOException;
    
    /**
     * Create the path leaf if it doesn't exist.
     * @return
     * @throws IOException
     */
    boolean createLeaf() throws IOException;
    
    /**
     * Delete this path file or folder.
     * If it is a folder, is it recursive.
     * 
     * @throws IOException if path could not be deleted
     */
    void delete() throws IOException;

    /**
     * Resolve a relative or absolute address starting at this path address.
     * @param address
     * @return Path
     */
    Path resolve(Chars address);

    /**
     * Resolver which created this path.
     * @return PathResolver
     */
    PathResolver getResolver();

    /**
     * Create an inputstream for this path.
     *
     * @return ByteInputStream
     * @throws IOException if stream can not be created
     */
    ByteInputStream createInputStream() throws IOException;

    /**
     * Create an outputstream for this path.
     *
     * @return ByteOutputStream
     * @throws IOException if stream can not be created
     */
    ByteOutputStream createOutputStream() throws IOException;

    /**
     * Create a seekable buffer on this path datas.
     * 
     * @param read set to true to request reading support
     * @param write set to true to request writing support
     * @param resize set to true to request resizing support
     * @return SeekableByteBuffer, never null
     * @throws IOException if buffer creation failed
     */
    SeekableByteBuffer createSeekableBuffer(boolean read, boolean write, boolean resize) throws IOException;
    
    /**
     * get URI representation.
     */
    Chars toURI();

    Chars toChars();
    
}
