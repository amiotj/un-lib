
package science.unlicense.api.path;

import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public interface PathFormat {

    /**
     * Indicate if this resolver need a base path to create a resolver.
     * Example :
     * - The operating system file resolver is absolute.
     * - A zip archive resolver is not absolute.
     *
     * @return true if absolute
     */
    boolean isAbsolute();

    /**
     * Indicate if this path can be used.
     * For non-absolute resolvers.
     * Absolute resolvers should always return false.
     * 
     * @param base
     * @return true if it can be used
     * @throws IOException
     */
    boolean canCreate(Path base) throws IOException;

    /**
     * Create path resolver.
     *
     * @param base, need only if resolver is not absolute.
     * @return PathResolver, never null
     * @throws IOException if something goes wrong
     */
    PathResolver createResolver(Path base) throws IOException;

}
