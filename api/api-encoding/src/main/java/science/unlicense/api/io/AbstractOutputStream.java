
package science.unlicense.api.io;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractOutputStream implements ByteOutputStream{

    private byte[] bb = null;
    
    public void write(byte[] buffer) throws IOException {
        write(buffer,0,buffer.length);
    }

    public void write(byte[] buffer, int offset, int length) throws IOException {
        for(int i=offset,n=length;i<n;i++){
            write(buffer[i]);
        }
    }
    
    public void write(Buffer buffer, int offset, int length) throws IOException {
        if(bb==null) bb = new byte[4096];
        
        int nbRead;
        while(length>0){
            nbRead = Maths.min(bb.length, length);
            buffer.readByte(bb, 0, nbRead, offset);
            write(bb, 0, nbRead);
            length -= nbRead;
        }
    }

}
