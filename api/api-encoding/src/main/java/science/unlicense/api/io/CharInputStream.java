
package science.unlicense.api.io;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.Chars;

/**
 * Character input stream.
 * Wraps a ByteInpustStream and provide methods for Characters reading.
 *
 * @author Johann Sorel
 */
public class CharInputStream extends AbstractInputStream{

    private static final int BUFFER_SIZE = 4096;

    private final ByteInputStream in;
    private final CharEncoding encoding;
    private final byte[] elb;

    //store several bytes ahead
    private final byte[] array = new byte[BUFFER_SIZE];
    private final ByteSequence byteBuffer = new ByteSequence();
    private boolean finished = false;
    private int offset = 0;
    private int remain = 0;

    //next char info
    private int charOffset = 0;
    private int charSize = 0;

    private final ByteSequence lineBuffer = new ByteSequence();

    /**
     * New character input stream with '\n' end of line character.
     *
     * @param in wrapped byte input stream, not null
     * @param encoding Character encoding to read in, not null
     */
    public CharInputStream(final ByteInputStream in, final CharEncoding encoding) {
        this(in,encoding,new Char('\n'));
    }

    /**
     * New character input stream.
     *
     * @param in wrapped byte input stream, not null
     * @param encoding Character encoding to read in, not null
     * @param endline End of line character, not null
     */
    public CharInputStream(ByteInputStream in, CharEncoding encoding, Char endline) {
        this.in = in;
        this.encoding = encoding;
        this.elb = endline.toBytes();
        if(elb.length != 1) throw new RuntimeException("End line size != 1, not supported yet.");
    }

    private void fill() throws IOException{
        while(!finished && remain < 64){
            final int nb = in.read(array);
            if(nb==-1){
                finished = true;
            }else{
                byteBuffer.put(array, 0, nb);
                remain += nb;
            }
        }

        //avoid having to much unused bytes
        if(offset>BUFFER_SIZE){
            byteBuffer.trimStart(offset);
            offset = 0;
        }
    }

    /**
     * Use with caution, this may break the char encoding offsets.
     *
     * @return causes an IOException
     * @throws IOException
     */
    public int read() throws IOException {
        fill();
        if(remain == 0){
            return -1;
        }
        final byte b = byteBuffer.getBackArray()[offset];
        offset++;
        return b;
    }

    private boolean internalReadChar() throws IOException{
        fill();
        if(remain == 0){
            return false;
        }
        charSize = encoding.charlength(byteBuffer.getBackArray(), offset);
        charOffset = offset;
        offset += charSize;
        remain -= charSize;
        return true;
    }

    /**
     * Read a single char.
     *
     * @return Char or null if nothing left.
     * @throws IOException
     */
    public Char readChar() throws IOException{
        if(!internalReadChar()) return null;
        return new Char(Arrays.copy(byteBuffer.getBackArray(),charOffset, charSize), encoding);
    }

    /**
     * Read a single char, write it in given buffer
     *
     * @param buffer byte buffer to copy character bytes, not null
     * @param offset offset in the byte buffer where to start writing character
     * @return number of bytes read, -1 if no more chars.
     * @throws IOException
     */
    public int readChar(final byte[] buffer, final int offset) throws IOException{
        if(!internalReadChar()) return -1;
        Arrays.copy(byteBuffer.getBackArray(),charOffset, charSize, buffer, offset);
        return charSize;
    }

    /**
     * Read a single char, write it in given buffer
     *
     * @return number of bytes read, -1 if no more chars.
     * @throws IOException
     */
    public int readChar(final ByteSequence buffer) throws IOException{
        if(!internalReadChar()) return -1;
        buffer.put(byteBuffer.getBackArray(), charOffset, charSize);
        return charSize;
    }

    /**
     * Read a character sequence until it reached a new line character.
     *
     * @return next line or null if nothing left.
     * @throws IOException
     */
    public Chars readLine() throws IOException {
        final byte[] data = readLineAsBytes();
        if(data != null){
            return new Chars(data, encoding);
        }
        return null;
    }

    /**
     * Read a character sequence as byte array
     * until it reached a new line character.
     *
     * @return next line or null when finished.
     * @throws IOException
     */
    public byte[] readLineAsBytes() throws IOException {
        lineBuffer.removeAll();
        while(readChar(lineBuffer) > 0){
            if(lineBuffer.endWidth(elb)){
                break;
            }
        }

        if(lineBuffer.getSize() == 0){
            return null;
        }else{
            return lineBuffer.toArrayByte();
        }
    }

    /**
     * Close underlying stream.
     * 
     * @throws IOException
     */
    public void close() throws IOException {
        in.close();
    }

}
