
package science.unlicense.api.io;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;

/**
 * Character output stream.
 * Wraps a ByteOutpustStream and provide methods for Characters writing.
 *
 * @author Johann Sorel
 */
public class CharOutputStream extends WrapOutputStream {

    private final CharEncoding encoding;
    private final byte[] endOfLine;

    /**
     * New character output stream with UTF-8 encoding
     * and '\n' end of line character.
     *
     * @param out wrapped byte output stream, not null
     */
    public CharOutputStream(final ByteOutputStream out) {
        this(out, CharEncodings.UTF_8);
    }

    /**
     * New character output stream with '\n' end of line character.
     *
     * @param out wrapped byte output stream, not null
     * @param encoding output character encoding, not null
     */
    public CharOutputStream(final ByteOutputStream out, final CharEncoding encoding) {
        this(out,encoding,new Char('\n'));
    }

    /**
     * New character output stream.
     *
     * @param out wrapped byte output stream, not null
     * @param encoding output character encoding, not null
     * @param endofline End of line character, not null
     */
    public CharOutputStream(final ByteOutputStream out, final CharEncoding encoding, final Char endofline) {
        super(out);
        this.encoding = encoding;
        endOfLine = endofline.toBytes(encoding);
    }

    /**
     * Write a single character from a unicode code point.
     *
     * @param unicode unicode code point of character to write
     * @return this char stream
     * @throws IOException
     */
    public CharOutputStream write(int unicode) throws IOException {
        out.write(encoding.toBytes(unicode));
        return this;
    }

    /**
     * Write a single character.
     *
     * @param c character to write, not null
     * @return this char stream
     * @throws IOException
     */
    public CharOutputStream write(Char c) throws IOException {
        out.write(c.toBytes(encoding));
        return this;
    }

    /**
     * Write a character sequence.
     *
     * @param s sequence to write, not null
     * @return this char stream
     * @throws IOException
     */
    public CharOutputStream write(Chars s) throws IOException {
        out.write(s.toBytes(encoding));
        return this;
    }

    /**
     * Write a single byte if the outputstream.
     * Caution when using this method, you may break the encoding.
     *
     * @param b byte to write.
     * @throws IOException
     */
    public void write(byte b) throws IOException {
        out.write(b);
    }

    /**
     * Write a line.
     * The end of line character will be added after the line.
     *
     * @param line, can be null
     * @return this char stream
     * @throws IOException
     */
    public CharOutputStream writeLine(final Chars line) throws IOException {
        if(line!=null)out.write(line.toBytes(encoding));
        return endLine();
    }

    /**
     * Write end of line character.
     *
     * @return this char stream
     * @throws IOException
     */
    public CharOutputStream endLine() throws IOException {
        out.write(endOfLine);
        return this;
    }

}
