
package science.unlicense.api.store;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.math.Maths;
import science.unlicense.api.path.Path;

/**
 * Default format.
 * 
 * @author Johann Sorel
 */
public abstract class DefaultFormat implements Format {

    private static final Chars SUF = new Chars(".");
    
    protected final Chars identifier;
    protected final Chars shortName;
    protected final Chars longName;
    protected final Chars[] mimeTypes;
    protected final Chars[] extensions;
    protected final byte[][] signatures;

    public DefaultFormat(Chars identifier, Chars shortName, Chars longName, 
            Chars[] mimeTypes, Chars[] extensions, byte[][] signatures) {
        this.identifier = identifier;
        this.shortName = shortName;
        this.longName = longName;
        this.mimeTypes = mimeTypes;
        this.extensions = extensions;
        this.signatures = signatures;
    }

    public Chars getIdentifier() {
        return identifier;
    }

    public Chars getShortName() {
        return shortName;
    }

    public Chars getLongName() {
        return longName;
    }

    public Chars[] getMimeTypes() {
        return mimeTypes;
    }

    public Chars[] getExtensions() {
        return extensions;
    }

    public byte[][] getSignature() {
        return signatures;
    }

    /**
     * Default implementation checks for the path extension or signatures.
     * 
     * @param input
     * @return
     * @throws IOException 
     */
    public boolean canDecode(Object input) throws IOException {
        
        //check by extension if we have a path
        final Chars[] exts = getExtensions();
        if(exts.length>0 && input instanceof Path){
            final Path path = (Path) input;
            final Chars name = new Chars(path.getName()).toLowerCase();
            for(int i=0;i<exts.length;i++){
                if(name.endsWith(SUF.concat(exts[i]))){
                    return true;
                }
            }
            return false;
        }
        
        //check the signatures if we can obtain a stream
        final byte[][] signatures = getSignature();
        int maxLength = 0;
        for(int i=0;i<signatures.length;i++){
            maxLength = Maths.max(maxLength, signatures[i].length);
        }
        
        if(maxLength!=0){
            final boolean[] mustClose = new boolean[]{false};
            ByteInputStream stream = null;
            try{
                stream = IOUtilities.toInputStream(input, mustClose);
                final DataInputStream ds = new DataInputStream(stream);
                final byte[] sign = ds.readFully(new byte[maxLength]);
                for(int i=0;i<signatures.length;i++){
                    if(Arrays.equals(signatures[i],0,signatures[i].length,sign,0)){
                        return true;
                    }
                }
            }finally{
                if(mustClose[0]){
                    stream.close();
                }
            }
        }
        
        return false;
    }

    public long searchEnd(ByteInputStream in, boolean fullscan) throws IOException {
        return -1;
    }

}
