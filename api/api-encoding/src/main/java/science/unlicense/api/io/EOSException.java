package science.unlicense.api.io;

import science.unlicense.api.io.IOException;

/**
 * End of Stream exception.
 *
 * @author Johann Sorel
 */
public class EOSException extends IOException{

    public EOSException() {
    }

    public EOSException(String message) {
        super(message);
    }

}
