
package science.unlicense.api.io;

/**
 * A ByteOutputStream which wrap another.
 *
 * @author Johann Sorel
 */
public abstract class WrapInputStream extends AbstractInputStream{

    protected final ByteInputStream in;

    public WrapInputStream(ByteInputStream in) {
        this.in = in;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
