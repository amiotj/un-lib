
package science.unlicense.api.store;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.predicate.AbstractPredicate;
import science.unlicense.api.path.Path;

/**
 * Predicate that filter Path objects.
 * This predicate checks the file extensions.
 * 
 * @author Johann Sorel
 */
public class FormatPredicate extends AbstractPredicate{

    private final Chars text;
    private final Format[] formats;
    private final Chars[] exts;

    public FormatPredicate(Format format) {
        this.formats = new Format[]{format};

        final CharBuffer cb = new CharBuffer();
        cb.append(format.getLongName()).append(' ').append('(');
        exts = format.getExtensions();
        for(int i=0;i<exts.length;i++){
            if(i>0) cb.append(',');
            cb.append("*.").append(exts[i]);
        }
        cb.append(')');
        text = cb.toChars();

    }

    public FormatPredicate(CharArray globalName, Format[] formats) {
        this.formats = formats;

        final Sequence seq = new ArraySequence();
        for(int i=0;i<this.formats.length;i++){
            seq.addAll(formats[i].getExtensions());
        }
        exts = new Chars[seq.getSize()];
        Collections.copy(seq, exts, 0);

        final CharBuffer cb = new CharBuffer();
        cb.append(globalName).append(' ').append('(');
        for(int i=0;i<exts.length;i++){
            if(i>0) cb.append(',');
            cb.append("*.").append(exts[i]);
        }
        cb.append(')');
        text = cb.toChars();
    }
    
    public Boolean evaluate(Object candidate) {
        if(candidate instanceof Path){
            final Path path = (Path) candidate;
            final Chars name = new Chars(path.getName()).toLowerCase();
            for(int i=0;i<exts.length;i++){
                if(name.endsWith(exts[i].toLowerCase())){
                    return true;
                }
            }
        }
        return false;
    }
    
    public Chars toChars() {
        return text;
    }

}
