

package science.unlicense.api.store;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;

/**
 *
 * @author Johann Sorel
 */
public class AbstractStore extends CObject implements Store {

    protected final Format format;
    protected final Object input;
    private Logger logger = Loggers.get();

    public AbstractStore(Format format, Object input) {
        this.format = format;
        this.input = input;
    }
    
    public Format getFormat() {
        return format;
    }

    public Object getInput() {
        return input;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        CObjects.ensureNotNull(logger);
        this.logger = logger;
    }
    
    public void dispose() {
    }
    
}
