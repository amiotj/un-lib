
package science.unlicense.api.logging;

import science.unlicense.api.logging.DefaultLogger;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 * Simple file logger.
 * 
 * TODO : find a proper way to release the stream.
 * TODO : should that be in API package ?
 * 
 *@author Johann Sorel
 */
public class FileLogger extends DefaultLogger{

    private final Path path;
    private final CharOutputStream stream;

    public FileLogger(Path path) throws IOException {
        this.path = path;
        this.stream = new CharOutputStream(path.createOutputStream(), CharEncodings.UTF_8);
    }

    public void log(Chars message, Throwable exception, float level) {
        super.log(message, exception, level);
        try {
            if(message!=null){
                stream.writeLine(message);
            }
            if(exception!=null){
                final Writer writer = new StringWriter();
                exception.printStackTrace(new PrintWriter(writer));
                stream.write(new Chars(writer.toString()));
            }
            stream.flush();
        } catch (IOException ex) {
            super.critical(exception);
        }
    }
    
    public void dispose() throws IOException{
        stream.close();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        stream.close();
    }
    
    
}
