
package science.unlicense.api.store;

import science.unlicense.api.logging.Logger;

/**
 * A Store is a data container.
 * This interface provides the basic common definition shared by all kind of stores.
 * Multiple implementations exist, for image,media,3d,documents,... 
 * 
 * @author Johann Sorel
 */
public interface Store {
    
    /**
     * Get the original format definition.
     * 
     * @return Format, never null
     */
    Format getFormat();
    
    /**
     * Get the store logger.
     * 
     * @return Logger, never null.
     */
    Logger getLogger();
    
    /**
     * Set store exception logger.
     * Exception should always be send back to the caller, yet minor, expected errors
     * or debugging informations can be logged.
     * 
     * @param logger, never null
     */
    void setLogger(Logger logger);
    
    /**
     * Get the store root object, often a Path or a Stream but could be anything.
     * 
     * @return input object, can be null.
     */
    Object getInput();
    
    /**
     * Release any resource used by this store.
     * The store should not be used anymore after this call.
     */
    void dispose();
    
}
