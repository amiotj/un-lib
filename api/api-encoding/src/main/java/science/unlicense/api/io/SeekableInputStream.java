
package science.unlicense.api.io;

/**
 *
 * @author Johann Sorel
 */
public class SeekableInputStream extends AbstractInputStream {

    private final SeekableByteBuffer buffer;

    public SeekableInputStream(SeekableByteBuffer buffer) {
        this.buffer = buffer;
    }
    
    public int read() throws IOException {
        return buffer.read();
    }

    public long skip(long n) throws IOException {
        return buffer.skip(n);
    }

    /**
     * Do nothing, the backend SeekableByteBuffer must be closed separately.
     * @throws IOException 
     */
    public void close() throws IOException {
        //do nothing
    }

}
