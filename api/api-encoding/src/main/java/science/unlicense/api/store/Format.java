
package science.unlicense.api.store;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;

/**
 * A format define a group of bytes organize in a defined structure.
 * Formats are often associated to mime types and file extensions.
 *
 * Several subtype exist such as :
 * - ImageFormat : PNG,BMP,TGA,...
 * - MediaFormat : WAV,MKV,MP4,...
 * - Model3DFormat : Collada,OBJ,3DS,...
 * - ArchiveFormat : ZIP,TAR,...
 *
 * @author Johann Sorel
 */
public interface Format {

    /**
     * Identifier inside the library.
     * Should be unique.
     *
     * @return Chars, never null
     */
    Chars getIdentifier();

    /**
     * Short name, most commun abbreviation used for this format.
     *
     * @return Chars, never null
     */
    CharArray getShortName();

    /**
     * Long name, full name of this format.
     *
     * @return Chars, never null
     */
    CharArray getLongName();

    /**
     * List of knowned mime-types.
     *
     * @return Chars array, never null, can be empty.
     */
    Chars[] getMimeTypes();

    /**
     * List of used file extensions.
     *
     * @return Chars array, never null, can be empty.
     */
    Chars[] getExtensions();

    /**
     * Get a list of possible file signatures used by this format.
     * A signature is a small number of bytes located at the begining of the file
     * which allows to quickly identify the file format.
     * Some format might have more then one signature.
     * 
     * Size 0 if there are no specific signature for this format.
     * 
     * @return array of possible signatures, never null
     */
    byte[][] getSignature();
    
    /**
     * Test to check if given input can be decoded.
     * This method is expected to be fast, only a minimal decoding should be performed.
     * Most format have a signature at the beginning of a file, it is
     * enough to test only this signature.
     * 
     * @param input
     * @return true if input can be decoded by this format.
     * @throws science.unlicense.api.io.IOException
     */
    boolean canDecode(Object input) throws IOException;

    /**
     * Finding the expected end of the current format can have multiple use.
     * 
     * If the file size is available near the begining, often in the header then
     * it should be returned whatever value has the fullscan flag.
     * 
     * If the size can not be determinated then method should return -1 unless
     * the fullscan flag is set to true it which case it should read the stream until it
     * finds the end.
     * 
     * Formats are allowed to return -1 in any case.
     * 
     * @param in
     * @param fullscan
     * @return
     * @throws IOException 
     */
    long searchEnd(ByteInputStream in, boolean fullscan) throws IOException;
    
}
