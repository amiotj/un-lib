
package science.unlicense.impl.io;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.api.io.IOException;

/**
 * Wrap a ByteStream, simulating an end of file value when needed.
 *
 * @author Johann Sorel
 */
public class ClipInputStream extends AbstractInputStream{

    private final ByteInputStream in;
    private final int end;
    private int index=0;

    public ClipInputStream(ByteInputStream in, int end) {
        this.in = in;
        this.end = end;
    }

    @Override
    public int read() throws IOException {
        if(index>=end) return -1;
        index++;
        return in.read();
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        length = Math.min(end-index, length);
        if(length==0) return -1;
        int nb = in.read(buffer, offset, length);
        if(nb==-1) return -1;
        index += nb;
        return nb;
    }

    @Override
    public int read(Buffer buffer, int offset, int length) throws IOException {
        length = Math.min(end-index, length);
        if(length==0) return -1;
        int nb = in.read(buffer, offset, length);
        if(nb==-1) return -1;
        index += nb;
        return nb;
    }

    @Override
    public long skip(long n) throws IOException {
        n = Math.min(end-index, n);
        if(n==0) return -1;
        long nb = in.skip(n);
        if(nb==-1) return -1;
        index += nb;
        return nb;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
    
}
