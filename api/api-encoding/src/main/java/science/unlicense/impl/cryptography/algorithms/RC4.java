package science.unlicense.impl.cryptography.algorithms;

/**
 * Implementation of RC4 based on pseudo-code found on RC4's wikipedia page:
 * https://en.wikipedia.org/wiki/RC4
 * 
 * @author Francois Berder
 *
 */
public final class RC4 implements CipherAlgorithm {

  
    private final byte[] s = new byte[256];
    private int i;
    private int j;
    
    public RC4(byte[] key) {
        i = 0;
        j = 0;
        keySchedule(key);
    }
    
    @Override
    public byte[] cipher(byte[] input) {
        byte[] output = new byte[input.length];
        
        for(int k = 0; k < input.length; ++k)
            output[k] = cipherByte(input[k]);
        
        return output;
    }

    @Override
    public byte[] invCipher(byte[] input) {
        return cipher(input);
    }

    private void keySchedule(byte[] key) {
        for(int k = 0; k < 256; ++k) {
            s[k] = (byte) (k & 0xff);
        }
        
        int l = 0;
        for(int k = 0; k < 256; ++k) {
            l = (l + s[k] + key[k % key.length]) & 0xff;
            byte tmp = s[l];
            s[l] = s[k];
            s[k] = tmp;
        }
    }
    
    private byte cipherByte(byte in) {
        i = (i + 1) & 0xff;
        j = (j + s[i]) & 0xff;
        byte tmp = s[i];
        s[i] = s[j];
        s[j] = tmp;
        byte c = s[(s[i]+s[j]) & 0xff];
        
        return (byte) ((in^c) & 0xff);
    }
}
