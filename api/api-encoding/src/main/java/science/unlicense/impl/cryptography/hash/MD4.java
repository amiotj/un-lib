package science.unlicense.impl.cryptography.hash;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.NumberEncoding;

/**
 * description: https://tools.ietf.org/html/rfc1320
 * 
 * @author Francois Berder
 *
 */
public final class MD4 implements HashFunction {

    public static final Chars NAME = new Chars("MD4");

    private final int[] state = new int[4];
    private final byte[] digestBits = new byte[16];
    private final int[] block = new int[16];
    private int blockIndex;
    private long count;
    
    
    public MD4() {
        reset();
    }
    
    public Chars getName() {
        return NAME;
    }

    public boolean isCryptographic() {
        return true;
    }

    public int getResultLength() {
        return 128;
    }

    public void reset() {
        state[0] = 0x67452301;
        state[1] = 0xefcdab89;
        state[2] = 0x98badcfe;
        state[3] = 0x10325476;
        
        blockIndex = 0;
        count = 0;
    }

    public void update(int b) {
        update((byte)b);
    }

    public void update(byte b) {
        final int mask = (8 * (blockIndex & 3));

        count += 8;
        block[blockIndex >> 2] &= ~(0xff << mask);
        block[blockIndex >> 2] |= (b & 0xff) << mask;
        blockIndex++;
        if (blockIndex == 64) {
            transform();
            blockIndex = 0;
        }
    }

    public void update(byte[] buffer) {
        update(buffer, 0, buffer.length);
    }

    public void update(byte input[], int offset, int len) {
        for (int i = 0; i < len; i++) {
            update(input[i + offset]);
        }
    }

    public byte[] getResultBytes() {
        finish();
        return Arrays.copy(digestBits, new byte[digestBits.length]);
    }

    public long getResultLong() {
        return NumberEncoding.BIG_ENDIAN.readLong(getResultBytes(), 0);
    }
    
//    #define F(X,Y,Z) 
//    #define G(X,Y,Z) ((X & Y) | (X & Z) | (Y & Z))
//    #define H(X,Y,Z) (X ^ Y ^ Z)
//
//
//    #define ROTL(W,N) (((W) << N) | ((W) >> (32-N)))
//
//    #define ROUND1(workingState,a,b,c,d,x,s) \
//        a = ROTL(a + F(b,c,d) + x,s); 
//
//    #define ROUND2(workingState,a,b,c,d,x,s) \
//        a = ROTL(a + G(b,c,d) + x + 0x5A827999,s); 
//
//    #define ROUND3(workingState,a,b,c,d,x,s) \
//        a = ROTL(a + H(b,c,d) + x + 0x6ED9EBA1,s); 

    private int F(int X, int Y, int Z) {
        return (X & Y) | (~X & Z);
    }
    
    private int G(int X, int Y, int Z) {
        return (X & Y) | (X & Z) | (Y & Z);
    }
    
    private int H(int X, int Y, int Z) {
        return X ^ Y ^ Z;
    }
    
    private int ROTL(int W, int N) {
        return (W << N) | (W >>> (32-N));
    }
    
    private void ROUND1(int[] workingState, int indexA, int indexB, int indexC, int indexD, int X, int S) {
        workingState[indexA] = ROTL(workingState[indexA] + F(workingState[indexB],workingState[indexC],workingState[indexD]) + X, S);
    }
    
    private void ROUND2(int[] workingState, int indexA, int indexB, int indexC, int indexD, int X, int S) {
        workingState[indexA] = ROTL(workingState[indexA] + G(workingState[indexB],workingState[indexC],workingState[indexD]) + X + 0x5A827999, S); 
    }
    
    private void ROUND3(int[] workingState, int indexA, int indexB, int indexC, int indexD, int X, int S) {
        workingState[indexA] = ROTL(workingState[indexA] + H(workingState[indexB],workingState[indexC],workingState[indexD]) + X + 0x6ED9EBA1,S); 
    }
    
    private void transform() {
        
        int[] workingState = new int[4];
        workingState[0] = state[0];
        workingState[1] = state[1];
        workingState[2] = state[2];
        workingState[3] = state[3];
        
        // Round 1
        ROUND1(workingState,0,1,2,3,block[0],3);     ROUND1(workingState,3,0,1,2,block[1],7);     ROUND1(workingState,2,3,0,1,block[2],11);    ROUND1(workingState,1,2,3,0,block[3],19);
        ROUND1(workingState,0,1,2,3,block[4],3);     ROUND1(workingState,3,0,1,2,block[5],7);     ROUND1(workingState,2,3,0,1,block[6],11);    ROUND1(workingState,1,2,3,0,block[7],19);
        ROUND1(workingState,0,1,2,3,block[8],3);     ROUND1(workingState,3,0,1,2,block[9],7);     ROUND1(workingState,2,3,0,1,block[10],11);   ROUND1(workingState,1,2,3,0,block[11],19);
        ROUND1(workingState,0,1,2,3,block[12],3);    ROUND1(workingState,3,0,1,2,block[13],7);    ROUND1(workingState,2,3,0,1,block[14],11);   ROUND1(workingState,1,2,3,0,block[15],19);
        
        // Roun3 2      
        ROUND2(workingState,0,1,2,3,block[0],3);     ROUND2(workingState,3,0,1,2,block[4],5);     ROUND2(workingState,2,3,0,1,block[8],9);     ROUND2(workingState,1,2,3,0,block[12],13);
        ROUND2(workingState,0,1,2,3,block[1],3);     ROUND2(workingState,3,0,1,2,block[5],5);     ROUND2(workingState,2,3,0,1,block[9],9);     ROUND2(workingState,1,2,3,0,block[13],13);
        ROUND2(workingState,0,1,2,3,block[2],3);     ROUND2(workingState,3,0,1,2,block[6],5);     ROUND2(workingState,2,3,0,1,block[10],9);    ROUND2(workingState,1,2,3,0,block[14],13);
        ROUND2(workingState,0,1,2,3,block[3],3);     ROUND2(workingState,3,0,1,2,block[7],5);     ROUND2(workingState,2,3,0,1,block[11],9);    ROUND2(workingState,1,2,3,0,block[15],13);
        
        // Roun3 3      
        ROUND3(workingState,0,1,2,3,block[0],3);     ROUND3(workingState,3,0,1,2,block[8],9);     ROUND3(workingState,2,3,0,1,block[4],11);    ROUND3(workingState,1,2,3,0,block[12],15);
        ROUND3(workingState,0,1,2,3,block[2],3);     ROUND3(workingState,3,0,1,2,block[10],9);    ROUND3(workingState,2,3,0,1,block[6],11);    ROUND3(workingState,1,2,3,0,block[14],15);
        ROUND3(workingState,0,1,2,3,block[1],3);     ROUND3(workingState,3,0,1,2,block[9],9);     ROUND3(workingState,2,3,0,1,block[5],11);    ROUND3(workingState,1,2,3,0,block[13],15);
        ROUND3(workingState,0,1,2,3,block[3],3);     ROUND3(workingState,3,0,1,2,block[11],9);    ROUND3(workingState,2,3,0,1,block[7],11);    ROUND3(workingState,1,2,3,0,block[15],15);
        
        state[0] += workingState[0];
        state[1] += workingState[1];
        state[2] += workingState[2];
        state[3] += workingState[3];
    }
    
    private void finish() {
        final byte bits[] = new byte[8];

        for (int i = 0; i < 8; i++) {
            bits[i] = (byte) ((count >>> ((i * 8))) & 0xff);
        }

        update((byte) 128);
        while (blockIndex != 56) {
            update((byte) 0);
        }
        // This should cause a transform to happen.
        update(bits);
        for (int i = 0; i < 16; i++) {
            digestBits[i] = (byte) ((state[i >> 2] >>> ((i & 3) * 8)) & 0xff);
        }
    }
}
