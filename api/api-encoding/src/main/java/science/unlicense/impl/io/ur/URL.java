
package science.unlicense.impl.io.ur;

import science.unlicense.api.character.Chars;

/**
 * Uniform Resource Locator
 *
 * @author Johann Sorel
 */
public class URL extends URI{

    public URL(Chars schema, Chars path) {
        super(schema, path);
    }

}
