package science.unlicense.impl.cryptography.hash;

import science.unlicense.api.character.Chars;

/**
 * Implementation of SHA-384.
 *
 * @author Francois Berder
 *
 */
public class SHA384 extends SHA2_64 {

    public static final Chars NAME = new Chars("SHA-384");

    private final static long resetState[] = { 
        0xcbbb9d5dc1059ed8L, 0x629a292a367cd507L, 0x9159015a3070dd17L, 0x152fecd8f70e5939L,
        0x67332667ffc00b31L, 0x8eb44a8768581511L, 0xdb0c2e0d64f98fa7L, 0x47b5481dbefa4fa4L,    
    };

    public SHA384() {
    	super(resetState);
    }
    
	@Override
	public Chars getName() {
		return NAME;
	}

	@Override
	public int getResultLength() {
		return 384;
	}
}
