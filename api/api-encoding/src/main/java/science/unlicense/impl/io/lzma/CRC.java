package science.unlicense.impl.io.lzma;

/**
 * From 7-ZIP SDK, placed under public domain. 
 * http://www.7-zip.org/sdk.html
 *
 * @author Igor Pavlov
 */
public class CRC {

    static public int[] Table = new int[256];

    static {
        for (int i = 0; i < 256; i++) {
            int r = i;
            for (int j = 0; j < 8; j++) {
                if ((r & 1) != 0) {
                    r = (r >>> 1) ^ 0xEDB88320;
                } else {
                    r >>>= 1;
                }
            }
            Table[i] = r;
        }
    }
    int _value = -1;

    public void init() {
        _value = -1;
    }

    public void update(byte[] data, int offset, int size) {
        for (int i = 0; i < size; i++) {
            _value = Table[(_value ^ data[offset + i]) & 0xFF] ^ (_value >>> 8);
        }
    }

    public void update(byte[] data) {
        int size = data.length;
        for (int i = 0; i < size; i++) {
            _value = Table[(_value ^ data[i]) & 0xFF] ^ (_value >>> 8);
        }
    }

    public void updateByte(int b) {
        _value = Table[(_value ^ b) & 0xFF] ^ (_value >>> 8);
    }

    public int getDigest() {
        return _value ^ (-1);
    }
}
