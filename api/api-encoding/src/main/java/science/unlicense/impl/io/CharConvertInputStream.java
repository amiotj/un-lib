
package science.unlicense.impl.io;

import science.unlicense.api.io.WrapInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.exception.UnimplementedException;

/**
 * On the fly character encoding change stream.
 *
 * @author Johann Sorel
 */
public class CharConvertInputStream extends WrapInputStream{

    public CharConvertInputStream(ByteInputStream in) {
        super(in);
    }

    @Override
    public int read() throws IOException {
        throw new UnimplementedException("Not supported yet.");
//        final int[] cp = new int[2];
//        final byte[] inarray = new byte[4];
//        final byte[] outarray = new byte[4];
//        int offset = 0;
//        while(fill(in,inarray,offset) > 0){
//
//            source.toUnicode(inarray, cp, offset);
//            offset = cp[1];
//            target.fromUnicode(cp, outarray);
//            out.write(outarray, 0, cp[1]);
//        }
    }


    private static int fill(final ByteInputStream in, byte[] array, int offset) throws IOException{

        //pack at the begining remaining bytes
        if(offset>0){
            Arrays.copy(array, offset, array.length-offset, array, 0);
        }

        while(offset<array.length){
            int b = in.read();
            if(b<0){
                //nothing left to read
                break;
            }
            array[offset] = (byte)b;
            offset++;
        }

        return offset;
    }

}
