
package science.unlicense.impl.io.gzip;

import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.io.WrapOutputStream;
import science.unlicense.impl.cryptography.hash.CRC32;
import science.unlicense.impl.cryptography.hash.HashOutputStream;
import science.unlicense.impl.io.deflate.DeflateOutputStream;

/**
 * Draft, just to enable it to be used by other classes which migth requiere GZip.
 *
 * @author Johann Sorel
 */
public class GZipOutputStream extends WrapOutputStream{

    private boolean header = false;
    private boolean finish = false;
    private HashOutputStream crcOut;
    private int len = 0;

    public GZipOutputStream(ByteOutputStream out) {
        super(out);
    }

    /**
     * Call to finish the gzip stream.
     * Calculates and write the CRC32 checksum and length.
     * @throws IOException
     */
    public void finish() throws IOException{
        if(finish) return;
        finish = true;

        crcOut.flush();
        final DataOutputStream ds = new DataOutputStream(out);
        ds.writeInt((int)crcOut.hashValue(), NumberEncoding.LITTLE_ENDIAN);
        ds.writeInt(len, NumberEncoding.LITTLE_ENDIAN);
    }

    public void write(byte b) throws IOException {
        writeHeader();
        crcOut.write(b);
        len++;
    }

    public void flush() throws IOException {
        crcOut.flush();
        super.flush();
    }

    public void close() throws IOException {
        finish();
        super.close();
    }

    private void writeHeader() throws IOException{
        if(header) return;
        header = true;

        final DataOutputStream ds = new DataOutputStream(out);

        ds.writeShort((short)GZipInputStream.SIGNATURE,NumberEncoding.LITTLE_ENDIAN);
        ds.writeByte((byte)8); //CM : deflate
        ds.writeByte((byte)0); //flags : none
        ds.writeInt(0,NumberEncoding.LITTLE_ENDIAN); // current time
        ds.writeByte((byte)0); //XFL : compression
        ds.writeByte((byte)GZipConstants.OS_UNKNOWN); //OS

        final DeflateOutputStream deflateOut = new DeflateOutputStream(out);
        crcOut = new HashOutputStream(new CRC32(), deflateOut);
    }

}
