package science.unlicense.impl.cryptography.paddings;

/**
 *
 * @author Bertrand COTE
 */
public class NoPadding implements Padding {

    @Override
    public byte[] pad(byte[] text) {
        return text;
    }

    @Override
    public byte[] unPad(byte[] text) {
        return text;
    }
    
}