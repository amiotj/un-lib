package science.unlicense.impl.cryptography.algorithms;

import java.util.Random;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.Int32;

/**
 * A word is an element of { 0, 1, ... , 2³²−1 }.
 * 
 * http://edipermadi.files.wordpress.com/2008/06/salsa20-spec.pdf
 *
 * @author Bertrand COTE
 */
public class Salsa20 implements CipherAlgorithm {
    
    // ==========================================================
    // ========== Constants used in expansion function ==========
    // ==========================================================
    
    // Define σ0 = (101, 120, 112, 97), σ1 = (110, 100, 32, 51), σ2 = (50, 45, 98, 121), and σ3 = (116, 101, 32, 107).
    private static final byte[] sigma0 = new byte[]{ (byte)101, (byte)120, (byte)112, (byte) 97 };
    private static final byte[] sigma1 = new byte[]{ (byte)110, (byte)100, (byte) 32, (byte) 51 };
    private static final byte[] sigma2 = new byte[]{ (byte) 50, (byte) 45, (byte) 98, (byte)121 };
    private static final byte[] sigma3 = new byte[]{ (byte)116, (byte)101, (byte) 32, (byte)107 };
    
    private static byte[] sigma = new byte[]{
        // sigma0
        (byte)101, (byte)120, (byte)112, (byte) 97,
        // k0
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        // sigma1
        (byte)110, (byte)100, (byte) 32, (byte) 51,
        // n
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        // sigma2
        (byte) 50, (byte) 45, (byte) 98, (byte)121,
        // k1
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        // sigma3
        (byte)116, (byte)101, (byte) 32, (byte)107
    };

    // Define τ0 = (101, 120, 112, 97), τ1 = (110, 100, 32, 49), τ2 = (54, 45, 98, 121), and τ3 = (116, 101, 32, 107).
    private static final byte[] tau0 = new byte[]{ (byte)101, (byte)120, (byte)112, (byte) 97 };
    private static final byte[] tau1 = new byte[]{ (byte)110, (byte)100, (byte) 32, (byte) 49 };
    private static final byte[] tau2 = new byte[]{ (byte) 54, (byte) 45, (byte) 98, (byte)121 };
    private static final byte[] tau3 = new byte[]{ (byte)116, (byte)101, (byte) 32, (byte)107 };
    
    private static byte[] tau = new byte[]{
        // sigma0
        (byte)101, (byte)120, (byte)112, (byte) 97,
        // k
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        // sigma1
        (byte)110, (byte)100, (byte) 32, (byte) 49,
        // n
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        // sigma2
        (byte) 54, (byte) 45, (byte) 98, (byte)121,
        // k
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0, (byte)  0,
        // sigma3
        (byte)116, (byte)101, (byte) 32, (byte)107
    };
    
    // ==========================================================
    // ==========================================================
    // ==========================================================

    
    // ========== CipherAlgorithm interface ====================================
            
    @Override
    public byte[] cipher(byte[] input) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public byte[] invCipher(byte[] input) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    // =========================================================================
    
    /**
     * Circular left shift of the register.
     * 
     * E.g:
     *      circularLeftShift(0xc0a8787e, 5) returns 0x150f0fd8
     * 
     * @param register
     * @param leftShift
     * @return 
     */
    public static int circularLeftShift( int register, int leftShift ) {
        
        final int integerBitsCount = 32;
        
        final int a = register<<leftShift;
        final int b = (register>>(integerBitsCount-leftShift)) & ((1<<leftShift)-1);
        
        return a | b;
    }
    
    /**
     * Computes zWords as:
     * <ul>
     * <li>z1 = y1 ⊕ ((y0 + y3 ) <<<  7)</li>
     * <li>z2 = y2 ⊕ ((z1 + y0 ) <<<  9)</li>
     * <li>z3 = y3 ⊕ ((z2 + z1 ) <<< 13)</li>
     * <li>z0 = y0 ⊕ ((z3 + z2 ) <<< 18)</li>
     * </ul>
     * (<<< means circular left shift)
     * 
     * @param y a 4-word sequence.
     * @return zWords a 4-word sequence if y is a 4-word sequence.
     */
    public static int[] quarterRound( int[] y ) {
        int[] z = new int[y.length];
        
        // z1 = y1 ⊕ ((y0 + y3 ) < < 7)
        z[1] = y[1] ^ ( circularLeftShift( y[0] + y[3], 7 ) );
        // z2 = y2 ⊕ ((z1 + y0 ) < < 9)
        z[2] = y[2] ^ ( circularLeftShift( z[1] + y[0],  9 ) );
        // z3 = y3 ⊕ ((z2 + z1 ) < < 13)
        z[3] = y[3] ^ ( circularLeftShift( z[2] + z[1], 13 ) );
        // z0 = y0 ⊕ ((z3 + z2 ) < < 18)
        z[0] = y[0] ^ ( circularLeftShift( z[3] + z[2], 18 ) );
        
        return z;
        
    }
    
    
    /**
     * <ul>
     * <li>(z0 , z1 , z2 , z3 ) = quarterRound(y0 , y1 , y2 , y3 )</li>
     * <li>(z5 , z6 , z7 , z4 ) = quarterRound(y5 , y6 , y7 , y4 )</li>
     * <li>(z10 , z11 , z8 , z9 ) = quarterRound(y10 , y11 , y8 , y9 )</li>
     * <li>(z15 , z12 , z13 , z14 ) = quarterRound(y15 , y12 , y13 , y14 )</li>
     * </ul>
     * @param y a 16-word sequence.
     * @return z a 16-word sequence if y is a 16-word sequence.
     */
    public static int[] rowRound( int[] y ) {
        
        int[] z = new int[y.length];
        
        int[] tempoIn = new int[4];
        int[] tempoOut;
        
        // (z0 , z1 , z2 , z3 ) = quarterRound(y0 , y1 , y2 , y3 )
        tempoIn[0] = y[0];
        tempoIn[1] = y[1];
        tempoIn[2] = y[2];
        tempoIn[3] = y[3];
        tempoOut = quarterRound(tempoIn); // (z0 , z1 , z2 , z3 )
        z[0] = tempoOut[0];
        z[1] = tempoOut[1];
        z[2] = tempoOut[2];
        z[3] = tempoOut[3];
        
        // (z5 , z6 , z7 , z4 ) = quarterRound(y5 , y6 , y7 , y4 )
        tempoIn[0] = y[5];
        tempoIn[1] = y[6];
        tempoIn[2] = y[7];
        tempoIn[3] = y[4];
        tempoOut = quarterRound(tempoIn); // (z5 , z6 , z7 , z4 )
        z[5] = tempoOut[0];
        z[6] = tempoOut[1];
        z[7] = tempoOut[2];
        z[4] = tempoOut[3];
        
        // (z10 , z11 , z8 , z9 ) = quarterRound(y10 , y11 , y8 , y9 )
        tempoIn[0] = y[10];
        tempoIn[1] = y[11];
        tempoIn[2] = y[8];
        tempoIn[3] = y[9];
        tempoOut = quarterRound(tempoIn); // (z10 , z11 , z8 , z9 )
        z[10] = tempoOut[0];
        z[11] = tempoOut[1];
        z[8] = tempoOut[2];
        z[9] = tempoOut[3];
        
        // (z15 , z12 , z13 , z14 ) = quarterRound(y15 , y12 , y13 , y14 )
        tempoIn[0] = y[15];
        tempoIn[1] = y[12];
        tempoIn[2] = y[13];
        tempoIn[3] = y[14];
        tempoOut = quarterRound(tempoIn); // (z15 , z12 , z13 , z14 )
        z[15] = tempoOut[0];
        z[12] = tempoOut[1];
        z[13] = tempoOut[2];
        z[14] = tempoOut[3];
        
        return z;
    }
    
    
    /**
     * <ul>
     * <li>(y0 , y4 , y8 , y12 ) = quarterRound(x0 , x4 , x8 , x12 )</li>
     * <li>(y5 , y9 , y13 , y1 ) = quarterRound(x5 , x9 , x13 , x1 )</li>
     * <li>(y10 , y14 , y2 , y6 ) = quarterRound(x10 , x14 , x2 , x6 )</li>
     * <li>(y15 , y3 , y7 , y11 ) = quarterRound(x15 , x3 , x7 , x11 )</li>
     * </ul>
     * 
     * @param x a 16-word sequence.
     * @return y a 16-word sequence.
     */
    public static int[] columnRound( int[] x ) {
        
        int[] y = new int[x.length];
        
        int[] tempoIn = new int[4];
        int[] tempoOut;
        
        // (y0 , y4 , y8 , y12 ) = quarterRound(x0 , x4 , x8 , x12 )
        tempoIn[0] = x[0];
        tempoIn[1] = x[4];
        tempoIn[2] = x[8];
        tempoIn[3] = x[12];
        tempoOut = quarterRound( tempoIn );
        y[0] = tempoOut[0];
        y[4] = tempoOut[1];
        y[8] = tempoOut[2];
        y[12] = tempoOut[3];
        
        // (y5 , y9 , y13 , y1 ) = quarterRound(x5 , x9 , x13 , x1 )
        tempoIn[0] = x[5];
        tempoIn[1] = x[9];
        tempoIn[2] = x[13];
        tempoIn[3] = x[1];
        tempoOut = quarterRound( tempoIn );
        y[5] = tempoOut[0];
        y[9] = tempoOut[1];
        y[13] = tempoOut[2];
        y[1] = tempoOut[3];
        
        // (y10 , y14 , y2 , y6 ) = quarterRound(x10 , x14 , x2 , x6 )
        tempoIn[0] = x[10];
        tempoIn[1] = x[14];
        tempoIn[2] = x[2];
        tempoIn[3] = x[6];
        tempoOut = quarterRound( tempoIn );
        y[10] = tempoOut[0];
        y[14] = tempoOut[1];
        y[2] = tempoOut[2];
        y[6] = tempoOut[3];
        
        // (y15 , y3 , y7 , y11 ) = quarterRound(x15 , x3 , x7 , x11 )
        tempoIn[0] = x[15];
        tempoIn[1] = x[3];
        tempoIn[2] = x[7];
        tempoIn[3] = x[11];
        tempoOut = quarterRound( tempoIn );
        y[15] = tempoOut[0];
        y[3] = tempoOut[1];
        y[7] = tempoOut[2];
        y[11] = tempoOut[3];
        
        return y;
    }
    
    
    /**
     * A double round is a column round followed by a row round: 
     * doubleRound(x) = rowRound(columnRound(x)).
     * One can visualize a double round as modifying the columns of the input in
     * parallel, and then modifying the rows in parallel. Each word is modified 
     * twice.
     * 
     * @param x a 16-word sequence.
     * @return a 16-word sequence.
     */
    public static int[] doubleRound( int[] x ) {
        return rowRound(columnRound(x));
    }
    
    
    /**
     * If b = (b0 , b1 , b2 , b3 ) then 
     * littleEndian(b) = b3*2²⁴ + b2*2¹⁶ + b1*2⁸ + b0.
     * 
     * Note that littleEndian is invertible.
     * 
     * @param b a 4-byte sequence.
     * @return a word.
     */
    public static int littleEndian( byte[] b ) {
        int out = b[3] & 0xff;
        
        out <<= 8;
        out |= b[2] & 0xff;
        
        out <<= 8;
        out |= b[1] & 0xff;
        
        out <<= 8;
        out |= b[0] & 0xff;
        
        return out;
    }
    
    public static byte[] invLittleEndian( int w ) {
        byte[] b = new byte[4];
        
        b[0] = (byte)( w & 0xff );
        b[1] = (byte)( (w>>8) & 0xff );
        b[2] = (byte)( (w>>16) & 0xff );
        b[3] = (byte)( (w>>24) & 0xff );
        
        return b;
    }
    
    /**
     * 
     * @param x a 64-byte sequence.
     * @return a 64-byte sequence.
     */
    public static byte[] hash( byte[] x ) {
        
        // In detail: Starting from x = (x[0], x[1], . . . , x[63]), define
        //  x0 = littleendian(x[0], x[1], x[2], x[3]),
        //  x1 = littleendian(x[4], x[5], x[6], x[7]),
        //  ...
        //  x15 = littleendian(x[60], x[61], x[62], x[63])
        int[] xWords = new int[16];
        for( int i=0; i<16; i++ ) {
            xWords[i] = littleEndian( Arrays.copy(x, 4*i, 4) );
        }
        
        // Define (z0 , z1 , . . . , z15 ) = doubleRound¹⁰ (x0 , x1 , . . . , x15 )
        int[] zWords = Arrays.copy(xWords);
        for( int i=0; i<10; i++ ) {
            zWords = doubleRound( zWords );
        }
        
        // Then Salsa20(x) is the concatenation of
        //  littleendian−1 (z0 + x0 )
        //  littleendian−1 (z1 + x1 )
        //  ...
        //  littleendian−1 (z15 + x15 )
        byte[] z = new byte[64];
        for( int i=0; i<16; i++ ){
            byte[] tempo = invLittleEndian( zWords[i] + xWords[i] );
            Arrays.copy(tempo, 0, 4, z, 4*i);
        }
        
        return z;
    }
    
    
    /**
     * “Expansion” refers to the expansion of (k, n) into Salsa20_k (n). 
     * It also refers to the expansion of k into a long stream of Salsa20k 
     * outputs for various n’s; see Section 10.
     * The constants σ0 σ1 σ2 σ3 and τ0 τ1 τ2 τ3 are “expand 32-byte k” and 
     * “expand 16-byte k” in ASCII.
     * 
     * @param k a 32-byte or 16-byte sequence.
     * @param n a 16-byte sequence.
     * @return a 64-byte sequence.
     */
    public static byte[] expansion( byte[] k, byte[] n) {
        
        byte[] result = new byte[64];
        
        if( k.length == 32 ) {
            // If k0 , k1 , n are 16-byte sequences then Salsa20k0 ,k1 (n) = Salsa20(σ0 , k0 , σ1 , n, σ2 , k1 , σ3 ).
            System.arraycopy(sigma0, 0, result, 0, 4);
            System.arraycopy(k, 0, result, 4, 16);
            System.arraycopy(sigma1, 0, result, 20, 4);
            System.arraycopy(n, 0, result, 24, 16);
            System.arraycopy(sigma2, 0, result, 40, 4);
            System.arraycopy(k, 16, result, 44, 16);
            System.arraycopy(sigma3, 0, result, 60, 4);
            
            return result;
            
        } else if( k.length == 16 ) {
            // If k, n are 16-byte sequences then Salsa20k (n) = Salsa20(τ0 , k, τ1 , n, τ2 , k, τ3 ).
            System.arraycopy(tau0, 0, result, 0, 4);
            System.arraycopy(k, 0, result, 4, 16);
            System.arraycopy(tau1, 0, result, 20, 4);
            System.arraycopy(n, 0, result, 24, 16);
            System.arraycopy(tau2, 0, result, 40, 4);
            System.arraycopy(k, 0, result, 44, 16);
            System.arraycopy(tau3, 0, result, 60, 4);
            
            return result;
            
        } else {
            return null; // throw exception <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        }
    }
    
    /**
     * Increment value coded in byte array (MSB=byteCounter[0] and LSB=byteCounter[byteCounter.length-1]).
     * 
     * @param byteCounter 
     */
    private static void incByteCounter( byte[] byteCounter ) {
        for( int i=byteCounter.length-1; i>-1; i-- ) {
            int val = byteCounter[i];
            val++;
            val = val & 0xFF;
            byteCounter[i] = (byte)val;
            
            if( val != 0x00 ) {
                break;
            }
        }
    }
    
    /**
     * The Salsa20 encryption of m with nonce v under key k, denoted 
     * Salsa20_k(v)⊕m, is an l-byte sequence.
     * 
     * @param k the secret key: a 32-byte or 16-byte sequence (preferably 32 bytes).
     * @param v the nonce: an 8-byte sequence (a unique message number).
     * @param m the plaintext message: an l-byte sequence for some l ∈ { 0, 1, ... , 2⁷⁰ }.
     * @return the ciphertext message: an l-byte sequence (Salsa20_k(v)⊕m).
     */
    public static byte[] encryption( byte[] k, byte[] v, byte[] m ) {
        
        // n = { v_0, v_1, ..., v_7, cpt_0, cpt_1, ..., cpt_7 } ( at the init, all cpt_i are 0)
        byte[] n = Arrays.copy(v, 0, 16);
        
        byte[] ct = Arrays.copy(m);
        
        for( int i=0; i<m.length; i+=64 ) {

            byte[] exp = expansion( k, n );
            byte[] h = hash( exp );
            
            int currentblockLength = Math.min(64, ct.length-64*i);
            
            Arrays.arrayXOR(h, 0, ct, i, currentblockLength);
            incByteCounter(n);
        }
        
        return ct;
    }
    
    
    public static void main( String[] args ) {
        
        Random rand = new Random();
        
        // =====================================================================
        
        byte[] secretKey = new byte[32];
        rand.nextBytes(secretKey);
        
        // =====================================================================
        
        byte[] nonce = new byte[8];
        rand.nextBytes(nonce);
        
        // =====================================================================
        
        String mString = 
                "Jules Gabriel Verne (French: [ʒyl vɛʁn]; 8 February 1828 – 24 " + 
                "March 1905) was a French novelist, poet, and playwright best " + 
                "known for his adventure novels and his profound influence on " + 
                "the literary genre of science fiction.\n" +
                "\n" +
                "Verne was born to bourgeois parents in the seaport of Nantes, " + 
                "where he was trained to follow in his father's footsteps as a " + 
                "lawyer, but quit the profession early in life to write for " + 
                "magazines and the stage. His collaboration with the publisher " + 
                "Pierre-Jules Hetzel led to the creation of the Voyages " + 
                "Extraordinaires, a widely popular series of scrupulously " + 
                "researched adventure novels including Journey to the Center " + 
                "of the Earth, Twenty Thousand Leagues Under the Sea, and " + 
                "Around the World in Eighty Days.\n" +
                "\n" +
                "Verne is generally considered a major literary author in " + 
                "France and most of Europe, where he has had a wide influence " + 
                "on the literary avant-garde and on surrealism. His " + 
                "reputation is markedly different in Anglophone regions, where " + 
                "he has often been labeled a writer of genre fiction or " + 
                "children's books, not least because of the highly abridged " + 
                "and altered translations in which his novels are often " + 
                "reprinted.\n" +
                "\n" +
                "Verne is the second most-translated author in the world since " + 
                "1979, between the English-language writers Agatha Christie " + 
                "and William Shakespeare, and probably was the " + 
                "most-translated during the 1960s and 1970s. He is one " + 
                "of the authors sometimes called \"The Father of Science " + 
                "Fiction\", as are H. G. Wells and Hugo Gernsback.";
        byte[] m = mString.getBytes();
        
        // =====================================================================
        // =====================================================================
        // =====================================================================
        
        System.out.println( "secretKey =\n" + Int32.encodeHexa(secretKey) + "\n" );
        
        System.out.println( "nonce =\n" + Int32.encodeHexa(nonce) + "\n" );
        
        System.out.println( "mString =\n" + mString );
        System.out.println( "m =\n" + Int32.encodeHexa(m) + "\n" );
        
        byte[] ct = encryption( secretKey, nonce, m );
        
        System.out.println( "ct =\n" + Int32.encodeHexa(ct) + "\n" );
        
        byte[] pt = encryption( secretKey, nonce, ct );
        
        System.out.println( "pt =\n" + Int32.encodeHexa(pt) + "\n" );
        System.out.println( "pt =\n" + new Chars(pt,CharEncodings.US_ASCII) + "\n" );
        
        System.out.println("result = " + Arrays.equals(m, pt));
    }
    
}
