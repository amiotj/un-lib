
package science.unlicense.impl.io.gzip;

/**
 * A GZip stream is composed of several members (basicaly files)
 * 
 * @author Johann Sorel
 */
public final class GZipMember {
    
    public boolean ftext;
    public boolean fhcrc;
    public boolean fextra;
    public String fname;
    public String fcomment;
    public long mtime;
    public int xfl;
    public int os;
    
}
