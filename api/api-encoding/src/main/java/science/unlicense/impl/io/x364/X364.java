
package science.unlicense.impl.io.x364;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

/**
 * Terminal/Console colors knowned as X3.64 - ECMA-48 - ISO/IEC 6429.
 * Write the following byte sequence in the output stream.
 *
 * @author Johann Sorel
 */
public final class X364 {

    public static final Chars MODE_RESET                 = toCode((byte) 0, (byte)'m');
    public static final Chars MODE_BRIGHT                = toCode((byte) 1, (byte)'m');
    public static final Chars MODE_BOLD                  = toCode((byte) 2, (byte)'m');
    public static final Chars MODE_ITALIC                = toCode((byte) 3, (byte)'m');
    public static final Chars MODE_UNDERSCORE            = toCode((byte) 4, (byte)'m');
    public static final Chars MODE_BLINK                 = toCode((byte) 5, (byte)'m');
    public static final Chars MODE_BLINK_FAST            = toCode((byte) 6, (byte)'m');
    public static final Chars MODE_REVERSE               = toCode((byte) 7, (byte)'m');
    public static final Chars MODE_HIDDEN                = toCode((byte) 8, (byte)'m');
    public static final Chars MODE_CROSSOUT              = toCode((byte) 9, (byte)'m');
    public static final Chars MODE_PRIME_FONT            = toCode((byte)10, (byte)'m');
    public static final Chars MODE_FRAKTUR               = toCode((byte)20, (byte)'m');
    public static final Chars MODE_DOUBLEUNDERLINE       = toCode((byte)21, (byte)'m');
    public static final Chars MODE_FAINT                 = toCode((byte)22, (byte)'m');
    public static final Chars MODE_NO_ITALIC             = toCode((byte)23, (byte)'m');
    public static final Chars MODE_NO_UNDERLINE          = toCode((byte)24, (byte)'m');
    public static final Chars MODE_NO_BLINK              = toCode((byte)25, (byte)'m');
    public static final Chars MODE_POSITIVE              = toCode((byte)27, (byte)'m');
    public static final Chars MODE_REVEAL                = toCode((byte)28, (byte)'m');
    public static final Chars MODE_NO_CROSSOUT           = toCode((byte)29, (byte)'m');
    public static final Chars MODE_FRAMED                = toCode((byte)51, (byte)'m');
    public static final Chars MODE_ENCIRCLED             = toCode((byte)52, (byte)'m');
    public static final Chars MODE_OVERLINED             = toCode((byte)53, (byte)'m');
    public static final Chars MODE_NOt_FRAMED_ORENCIRCLED= toCode((byte)54, (byte)'m');
    public static final Chars MODE_NOT_OVERLINED         = toCode((byte)55, (byte)'m');
    public static final Chars MODE_IDEO_UNDERLINE        = toCode((byte)60, (byte)'m');
    public static final Chars MODE_IDEO_DOUBLEUNDERLINE  = toCode((byte)61, (byte)'m');
    public static final Chars MODE_IDEO_OVERLINE         = toCode((byte)62, (byte)'m');
    public static final Chars MODE_IDEO_DOUBLEOVERLINE   = toCode((byte)63, (byte)'m');
    public static final Chars MODE_IDEO_STRESS           = toCode((byte)64, (byte)'m');
    public static final Chars MODE_IDEO_ATT_OFF          = toCode((byte)65, (byte)'m');
    

    public static final Chars FOREGROUND_BLACK    = foreground((byte)0);
    public static final Chars FOREGROUND_RED      = foreground((byte)1);
    public static final Chars FOREGROUND_GREEN    = foreground((byte)2);
    public static final Chars FOREGROUND_YELLOW   = foreground((byte)3);
    public static final Chars FOREGROUND_BLUE     = foreground((byte)4);
    public static final Chars FOREGROUND_MAGENTA  = foreground((byte)5);
    public static final Chars FOREGROUND_CYAN     = foreground((byte)6);
    public static final Chars FOREGROUND_WHITE    = foreground((byte)7);
    public static final Chars FOREGROUND_DEFAULT  = foreground((byte)8);

    public static final Chars BACKGROUND_BLACK    = background((byte)0);
    public static final Chars BACKGROUND_RED      = background((byte)1);
    public static final Chars BACKGROUND_GREEN    = background((byte)2);
    public static final Chars BACKGROUND_YELLOW   = background((byte)3);
    public static final Chars BACKGROUND_BLUE     = background((byte)4);
    public static final Chars BACKGROUND_MAGENTA  = background((byte)5);
    public static final Chars BACKGROUND_CYAN     = background((byte)6);
    public static final Chars BACKGROUND_WHITE    = background((byte)7);
    public static final Chars BACKGROUND_DEFAULT  = background((byte)8);

    public static final byte COLOR_BLACK    = 0;
    public static final byte COLOR_RED      = 1;
    public static final byte COLOR_GREEN    = 2;
    public static final byte COLOR_YELLOW   = 3;
    public static final byte COLOR_BLUE     = 4;
    public static final byte COLOR_MAGENTA  = 5;
    public static final byte COLOR_CYAN     = 6;
    public static final byte COLOR_WHITE    = 7;
    public static final byte COLOR_DEFAULT  = 8;

    /**
     * Erase line
     */
    public static final Chars ERASE_LINE = toCode((byte) '2', (byte) 'K');

    /**
     * Clear screen
     */
    public static final Chars CLEAR_SCREEN = toCode((byte) '2', (byte) 'J');

    
    private X364(){}
    
    /**
     * font mode.
     */
    public static Chars mode(int mode) {
        return toCode(Int32.encode(mode).toBytes(), (byte) 'm');
    }

    /**
     * Background color.
     */
    public static Chars foreground(int color) {
        return toCode(Int32.encode(30 + color).toBytes(), (byte) 'm');
    }

    /**
     * Background color.
     */
    public static Chars background(int color) {
        return toCode(Int32.encode(40 + color).toBytes(), (byte) 'm');
    }

    /**
     * Cursor up
     */
    public static Chars cursorUp(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'A');
    }

    /**
     * Cursor down
     */
    public static Chars cursorDown(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'B');
    }

    /**
     * Cursor forward
     */
    public static Chars cursorForward(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'C');
    }

    /**
     * Cursor backward
     */
    public static Chars cursorBackward(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'D');
    }

    /**
     * Cursor next line
     */
    public static Chars cursorNextLine(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'E');
    }

    /**
     * Cursor previous line
     */
    public static Chars cursorPreviousLine(int nbLine) {
        return toCode(Int32.encode(nbLine).toBytes(), (byte) 'F');
    }

    /**
     * Cursor horizontal absolute.
     * Move cursor to column
     */
    public static Chars cursorHAbsolute(int col) {
        return toCode(Int32.encode(col).toBytes(), (byte) 'G');
    }

    /**
     * Cursor position
     */
    public static Chars cursorPosition(int row, int col) {
        return toCode(Int32.encode(row).toBytes(), Int32.encode(col).toBytes(), (byte) 'H');
    }

    private static Chars toCode(byte b){
        return new Chars(new byte[]{27,91, b},CharEncodings.US_ASCII);
    }
    
    private static Chars toCode(byte b1, byte b2){
        return new Chars(new byte[]{27,91, b1, b2},CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte b1, byte b2, byte b3){
        return new Chars(new byte[]{27,91, b1, b2, b3},CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte[] bs, byte end){
        final byte[] table = new byte[3+bs.length];
        table[0] = 27;
        table[1] = 91;
        Arrays.copy(bs, 0, bs.length, table, 2);
        table[table.length-1] = end;
        return new Chars(table,CharEncodings.US_ASCII);
    }

    private static Chars toCode(byte[] bs1, byte[] bs2, byte end){
        final byte[] table = new byte[4+bs1.length+bs2.length];
        table[0] = 27;
        table[1] = 91;
        Arrays.copy(bs1, 0, bs1.length, table, 2);
        table[1+bs1.length] = ';';
        Arrays.copy(bs2, 0, bs2.length, table, 2+bs1.length);
        table[table.length-1] = end;
        return new Chars(table,CharEncodings.US_ASCII);
    }

    
}
