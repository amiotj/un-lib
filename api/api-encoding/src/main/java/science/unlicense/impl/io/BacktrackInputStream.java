
package science.unlicense.impl.io;

import science.unlicense.api.io.WrapInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.io.EOSException;

/**
 * ByteInputStream which allow to move forward and backward in the stream
 * by placing tags.
 *
 * @author Johann Sorel
 */
public class BacktrackInputStream extends WrapInputStream{

    private final ByteSequence buffer = new ByteSequence();
    private int mark = -1;

    public BacktrackInputStream(ByteInputStream in) {
        super(in);
    }

    public void mark(){
        if(mark>= buffer.getSize()){
            buffer.removeAll();
        }else if(mark>0){
            //current position might not be the end
            buffer.trimStart(mark);
        }
        mark = 0;
    }

    public void rewind(){
        mark = 0;
    }

    /**
     * Remove the marker and stop buffering datas.
     * If some bytes are still in the buffer they will be returned first
     * before using the underlying stream.
     */
    public void clearMark(){
        mark();
        if(buffer.isEmpty()){
            mark = -1;
        }else{
            mark = -2;
        }
    }

    /**
     * Current offset position from the mark.
     */
    public int position(){
        return mark;
    }

    @Override
    public int read() throws IOException {
        if(mark>=0){

            int b;
            //we have move backward, data is in the buffer
            if(mark<buffer.getSize()){
                b = buffer.read(mark) & 0xff;
            }else{
                b = in.read();
                if(b==-1) return b;
                buffer.put((byte)b);
            }
            mark++;

            return b;
        }else if(mark==-2){
            final int size = buffer.getSize();
            int b = buffer.read(0) & 0xFF;
            buffer.trimStart(1);
            if(size==1){
                //last byte in the buffer
                buffer.removeAll();
                mark = -1;
            }
            return b;
        }

        return in.read();
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        if(mark>=0){

            //we have move backward, data is in the buffer
            if(mark<this.buffer.getSize()){
                int nb = this.buffer.read(mark, buffer, offset, length);
                mark += nb;
                return nb;
            }

            int nb = in.read(buffer,offset,length);
            if(nb!=-1) {
                this.buffer.put(buffer,offset,nb);
                mark += nb;
            }
            return nb;
        }else if(mark==-2){
            final int size = this.buffer.getSize();
            buffer[offset] = this.buffer.read(0);
            this.buffer.trimStart(1);
            if(size==1){
                //last byte in the buffer
                this.buffer.removeAll();
                mark = -1;
            }
            return 1;
        }else{
            return in.read(buffer,offset,length);
        }
    }

    @Override
    public long skip(final long n) throws IOException {
        if(mark>=0){
            long readed = 0;
            long nb = n;
            //skip in similar to read since we must store each byte
            //we have move backward, data is in the buffer
            if(mark<this.buffer.getSize()){
                //we have some in the buffer we can skip
                long diff = this.buffer.getSize()-mark;
                if(diff>=nb){
                    //move mark in the buffer only
                    mark += nb;
                    return nb;
                }else{
                    //we will need to skip from the input stream
                    readed = diff;
                    nb -= diff;
                    mark += diff;
                }
            }

            //we need to keep the skiped datas in the buffer
            final byte[] temp = new byte[(int)nb];
            final int rnb = in.read(temp,0,temp.length);
            if (rnb==-1) {
                //nothing left
                return readed==0 ? -1 : readed;  
            } else {
                mark += rnb;
                this.buffer.put(temp,0,rnb);
                return readed+rnb;
            }
        }else if(mark==-2){
            final int size = buffer.getSize();
            if(size>0){
                int b = buffer.read(0) & 0xFF;
                buffer.trimStart(1);
                if(size==1){
                    //last byte in the buffer
                    buffer.removeAll();
                    mark = -1;
                }
                return b;
            }
        }

        return in.skip(n);
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

}
