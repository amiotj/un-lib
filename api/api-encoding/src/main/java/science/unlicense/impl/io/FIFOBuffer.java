
package science.unlicense.impl.io;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.array.Arrays;

/**
 * fixed size buffer.
 *
 * @author Johann Sorel
 */
public class FIFOBuffer {

    private final int size;
    public final byte[] buffer;
    public int used;

    public FIFOBuffer(int size) {
        this.size = size;
        buffer = new byte[size];
    }

    /**
     * Fill remaining space with bytes from given stream.
     * @param is source ByteInputStream
     * @return the number of byte in the buffer.
     */
    public void fill(ByteInputStream is) throws IOException {
        if(used>=size){
            //already full
            return;
        }
        final int nb = is.read(buffer, used, size-used);
        if(nb<0){
            //we reached the end of the stream
            return;
        }
        used += nb;
    }

    /**
     * remove the first bytes.
     * @param nbbyte
     */
    public void clip(int nbbyte){
        used -= nbbyte;
        Arrays.copy(buffer, nbbyte, used, buffer, 0);
    }

    /**
     * Peek one byte.
     * @return byte
     */
    public byte peek(){
        byte b = buffer[0];
        clip(1);
        return b;
    }

}
