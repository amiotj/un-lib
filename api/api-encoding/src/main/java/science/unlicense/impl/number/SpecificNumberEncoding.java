
package science.unlicense.impl.number;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.number.NumberEncoding;

/**
 * Unusual, obsolete or specialized number encodings.
 *
 * Documentation :
 * https://en.wikipedia.org/wiki/Signed_number_representations
 *
 * Some real case usage examples :
 * - leading signed magnitude : GRIB image format
 * - trailing signed magnitude : ProtoBuffer
 * - one's complement : ...
 * - excess-K : IEEE floating points standard
 * 
 * @author Johann Sorel
 */
public class SpecificNumberEncoding {

    public static final NumberEncoding LEADING_SIGNED_MAGNITUDE_BE = new LeadingSignedMagnitude_BE();
    public static final NumberEncoding LEADING_SIGNED_MAGNITUDE_LE = new LeadingSignedMagnitude_LE();
    public static final NumberEncoding TRAILING_SIGNED_MAGNITUDE_BE = new LeadingSignedMagnitude_BE();
    public static final NumberEncoding TRAILING_SIGNED_MAGNITUDE_LE = new LeadingSignedMagnitude_LE();
    public static final NumberEncoding ONE_COMPLEMENT_BE = new OneComplement_BE();
    public static final NumberEncoding ONE_COMPLEMENT_LE = new OneComplement_LE();
    public static final NumberEncoding EXCESS_K_BE = new ExcessK_BE();
    public static final NumberEncoding EXCESS_K_LE = new ExcessK_LE();

    static final class LeadingSignedMagnitude_BE extends NumberEncoding.BigEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class LeadingSignedMagnitude_LE extends NumberEncoding.LittleEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class TrailingSignedMagnitude_BE extends NumberEncoding.BigEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class TrailingSignedMagnitude_LE extends NumberEncoding.LittleEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class OneComplement_BE extends NumberEncoding.BigEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class OneComplement_LE extends NumberEncoding.LittleEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class ExcessK_BE extends NumberEncoding.BigEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

    static final class ExcessK_LE extends NumberEncoding.LittleEndian{

        public byte readByte(int b) {
            throw new UnimplementedException("Not supported yet.");
        }

        public byte readByte(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public short readShort(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt24(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public int readInt(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public long readLong(byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeByte(byte value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeShort(short value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt24(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeInt(int value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

        public void writeLong(long value, byte[] buffer, int offset) {
            throw new UnimplementedException("Not supported yet.");
        }

    }

}