package science.unlicense.impl.cryptography.paddings;

/**
 *
 * @author Bertrand COTE
 */
public interface Padding {
    
    /**
     * Add a pad to the given text.
     * 
     * @param text
     * @return the padded text.
     */
    public byte[] pad( byte[] text );
    
    /**
     * 
     * @param text
     * @return the unpadded text.
     * @throws InvalidPadException 
     */
    public byte[] unPad( byte[] text ) throws InvalidPadException;
}
