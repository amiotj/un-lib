package science.unlicense.impl.cryptography.hash;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.NumberEncoding;

/**
 * CRC32 checksum.
 * Java adaptation of PNG annexe D : http://www.w3.org/TR/PNG/#D-CRCAppendix
 *
 * @author Johann Sorel
 */
public class CRC32 implements HashFunction{

    public static final Chars NAME = new Chars("CRC-32");
    
    /**
     * Fast CRC table. store checksum for all 8 bits combinaison.
     */
    private static final int[] FAST_TABLE;

    static {
        FAST_TABLE = new int[256];
        for (int n = 0; n < 256; n++) {
            int c = n;
            for (int k = 0; k < 8; k++) {
                if ((c & 1) != 0) {
                    c = 0xedb88320 ^ (c >>> 1);
                } else {
                    c = c >>> 1;
                }
            }
            FAST_TABLE[n] = c;
        }
    }

    //current checksum value
    protected int crc = 0;

    
    public Chars getName() {
        return NAME;
    }

    public int getResultLength() {
        return 32;
    }

    public boolean isCryptographic() {
        return false;
    }
    
    public void reset(){
        crc = 0;
    }
    
    public void update(int b) {
        int c = ~crc;
        c = FAST_TABLE[(c ^ b) & 0xff] ^ (c >>> 8);
        crc = ~c;
    }

    public void update(final byte[] buffer) {
        update(buffer, 0, buffer.length);
    }
    
    public void update(final byte[] buffer, int offset, int length) {
        int c = ~crc;
        for (int n = 0; n < length; n++) {
            c = FAST_TABLE[(c ^ buffer[n]) & 0xff] ^ (c >>> 8);
        }
        crc = ~c;
    }
    
    public byte[] getResultBytes() {
        final byte[] buffer = new byte[4];
        NumberEncoding.BIG_ENDIAN.writeUInt(getResultLong(), buffer, 0);
        return buffer;
    }
    
    public long getResultLong() {
        return crc;
    }

}
