
package science.unlicense.impl.io;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.WrapOutputStream;
import science.unlicense.api.io.IOException;

/**
 * On the fly character encoding change stream.
 * 
 * @author Johann Sorel
 */
public class CharConvertOutputStream extends WrapOutputStream{

    public CharConvertOutputStream(ByteOutputStream out) {
        super(out);
    }

    @Override
    public void write(byte b) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
