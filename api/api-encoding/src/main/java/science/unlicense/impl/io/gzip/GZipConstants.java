
package science.unlicense.impl.io.gzip;

/**
 * GZip constants.
 * 
 * @author Johann Sorel
 */
public final class GZipConstants {
    
    /** FAT filesystem (MS-DOS, OS/2, NT/Win32) */
    public static final int OS_FAT = 0; 
    /** Amiga */
    public static final int OS_AMIGA = 1;
    /** VMS (or OpenVMS) */
    public static final int OS_VMS = 2;
    /** Unix */
    public static final int OS_UNIX = 3;
    /** VM/CMS */
    public static final int OS_VM = 4;
    /** Atari TOS */
    public static final int OS_ATARI = 5;
    /** HPFS filesystem (OS/2, NT) */
    public static final int OS_HPFS = 6;
    /** Macintosh */
    public static final int OS_MAC = 7;
    /** Z-System */
    public static final int OS_Z = 8;
    /** CP/M */
    public static final int OS_CPM = 9;
    /** TOPS-20 */
    public static final int OS_TOPS =10;
    /** NTFS filesystem (NT) */
    public static final int OS_NTFS =11;
    /** QDOS */
    public static final int OS_ODOS =12;
    /** Acorn RISCOS */
    public static final int OS_RISC =13;
    /** unknown */
    public static final int OS_UNKNOWN = 255;
    
    private GZipConstants(){}
    
}
