
package science.unlicense.impl.io.lzw;

import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * TODO
 * 
 * @author Johann Sorel
 */
public class LZWInputStream extends AbstractInputStream {

    private final DataInputStream in;
    private final int codeSize;

    public LZWInputStream(final ByteInputStream in, int codeSize) {
        if(in instanceof DataInputStream){
            this.in = (DataInputStream) in;
        }else{
            this.in = new DataInputStream(in);
        }
        this.codeSize = codeSize;
    }

    public int read() throws IOException {
        throw new IOException("Not supported yet.");
    }

    public void close() throws IOException {
        in.close();
    }

}
