/*
 * $Log: ECField.java,v $
 * Revision 1.3  2010/12/06 02:54:19  stuart
 * Compile E*.java in jdk 1.5
 *
 * Revision 1.2  2001/08/19 01:59:47  stuart
 * gfNew()
 *
 */
package science.unlicense.impl.cryptography.ec;
import java.util.Random;
import java.math.BigInteger;
import science.unlicense.api.exception.InvalidArgumentException;

/**
 * Algebraic operations on the finite field GF(2^m).
 *
 * This public domain software was written by Stuart D. Gathman,
 * based on C software written by Paulo S.L.M. Barreto
 * <pbarreto@nw.com.br> based on original C++ software written by
 * George Barwood <george.barwood@dial.pipex.com>
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * References
 * ==========
 *
 * 1.	Erik De Win <erik.dewin@esat.kuleuven.ac.be> et alii:
 *		"A Fast Software Implementation for Arithmetic Operations in GF(2^n)",
 *		presented at Asiacrypt96 (preprint).
 *
 * Change log
 * ==========
 *
 * 1997.09.15:
 *		-- Fixed a bug in gfTrace() which would give wrong values (and
 *		possibly also addressing error once in a while) for every field
 *		where where GF_TM0 != 1.  Thanks to Dave Dahm <Dave.Dahm@unisys.com>
 *		for pointing out this bug and providing debugging data.
 */

public class ECField implements ECParam255 {
  private static final int BITS_PER_LUNIT = 16;
  /** Maximum units in a field point. */
  private static final int GF_POINT_UNITS = 2*(GF_K+1);
  private static final int BASE = 1 << GF_L;
  static final char TOGGLE = (char)(BASE - 1);	// used by ECTest
  //private final int GF_POINT_UNITS = 2*(GF_K+1);
  //private final int BASE = 1 << GF_L;
  //private final int TOGGLE = BASE - 1;
  // index range is [0..(BASE-2)]
  private static final char[] expt = new char[BASE]; 
  // index range is [1..(BASE-1)], but logt[0] is set to (BASE-1)
  private static final char[] logt = new char[BASE];
  private static final char[] nzt = { 1, GF_NZT };

  /* Initialize the log and exponent tables. */
  static {
    int root, i, j;

    root = BASE | GF_RP;
    expt[0] = 1;
    for (i = 1; i < BASE; i++) { 
      j = expt[i-1] << 1;
      if ((j & BASE) != 0) {
	j ^= root;
      }
      expt[i] = (char)j;
    }
    for (i = 0; i < TOGGLE; i++) {
      logt[expt[i]] = (char)i;
    }
    logt[0] = TOGGLE; /* a trick used by gfMultiply, gfSquare, gfAddMul */
  }

  /** Allocate a new polynomial. */
  static char[] gfNew() {
    return new char[GF_POINT_UNITS];
  }

  static char[] gfNew(char[] q) {
    char[] p = new char[GF_POINT_UNITS];
    System.arraycopy(q,0,p,0,q[0] + 1);
    return p;
  }

  static boolean gfEqual(char[] p,char[] q) {
    int n = p[0] + 1;
    for (int i = 0; i < n; ++i)
      if (p[i] != q[i]) return false;
    return true;
  }

  /*
  public boolean equals(Object obj) {
    if (!(obj instanceof ECField)) return false;
    ECField q = (ECField)obj;
    int n = p[0] + 1;
    for (int i = 0; i <= n; ++i)
      if (p[i] != q.p[i]) return false;
    return true;
  }
  */

  static void gfClear(char[] p) {
    for (int i = 0; i < p.length; ++i)
      p[i] = 0;
  }

  static void gfCopy(char[] p, char[] q) {
    System.arraycopy(q,0,p,0,q[0] + 1);
  }

  /** Set p := q + r */
  static void gfAdd(char[] p, char[] q, char[] r) {
    int i;

    if (q[0] > r[0]) {
      /* xor the the common-degree coefficients: */
      for (i = 1; i <= r[0]; i++) {
	p[i] = (char)(q[i] ^ r[i]);
      }
      /* invariant: i == r[0] + 1 */
      System.arraycopy(q,i,p,i,q[0] - r[0]);
      /* deg(p) inherits the value of deg(q): */
      p[0] = q[0];
    } else if (q[0] < r[0]) {
      /* xor the the common-degree coefficients: */
      for (i = 1; i <= q[0]; i++) {
	p[i] = (char)(q[i] ^ r[i]);
      }
      /* invariant: i == q[0] + 1 */
      System.arraycopy(r,i,p,i,r[0] - q[0]);
      /* deg(p) inherits the value of deg(r): */
      p[0] = r[0];
    } else { /* deg(q) == deg(r) */
      /* scan to determine deg(p): */
      for (i = q[0]; i > 0; i--) {
	if ((q[i] ^ r[i]) != 0) break;
      }
      /* xor the the common-degree coefficients, if any is left: */
      for (p[0] = (char)i; i > 0; i--) {
	p[i] = (char)(q[i] ^ r[i]);
      }
    }
  }

  /** reduce p mod the irreducible trinomial x^GF_K + x^GF_T + 1 */
  private static void gfReduce (char[] p) {
    int i;

    for (i = p[0]; i > GF_K; i--) {
      p[i - GF_K] ^= p[i];
      p[i + GF_T - GF_K] ^= p[i];
      p[i] = 0;
    }
    if (p[0] > GF_K) {
      /* scan to update deg(p): */
      p[0] = GF_K;
      while (p[0] != 0 && p[p[0]] == 0) {
	p[0]--;
      }
    }
  }

  /** set r := p * q mod (x^GF_K + x^GF_T + 1) */
  static void gfMultiply(char[] r, char[] p, char[] q) {
    int x, log_pi, log_qj;
    char[] lg = new char[GF_K + 2];	// clear after use

    if (r == p || r == q)
      throw new InvalidArgumentException("destination cannot overlap source");
    if (p[0] != 0 && q[0] != 0) {
      /* precompute logt[q[j]] to reduce table lookups: */
      for (int j = q[0]; j != 0; j--) {
	lg[j] = logt[q[j]];
      }
      /* perform multiplication: */
      gfClear(r);
      for (int i = p[0]; i != 0; i--) {
	if ((log_pi = logt[p[i]]) != TOGGLE) { /* p[i] != 0 */
	  for (int j = q[0]; j != 0; j--) {
	    if ((log_qj = lg[j]) != TOGGLE) { /* q[j] != 0 */
	    /*	r[i+j-1] ^= expt[(logt[p[i]] + logt[q[j]]) % TOGGLE]; */
	      r[i+j-1] ^= expt[
	        (x = log_pi + log_qj) >= TOGGLE ? x - TOGGLE : x
	      ];
	    }
	  }
	}
      }
      r[0] = (char)(p[0] + q[0] - 1);
      /* reduce r mod (x^GF_K + x^GF_T + 1): */
      gfReduce(r);
    } else {
      /* set r to the null polynomial: */
      r[0] = 0;
    }
    /* destroy potentially sensitive data: */
    x = log_pi = log_qj = 0;
    gfClear(lg);
  }

  /** set r := p^2 mod (x^GF_K + x^GF_T + 1). */
  static void gfSquare(char[] r,char[] p) {

    if (p[0] != 0) {
      /* in what follows,
         note that (x != 0) => (x^2 = exp((2 * log(x)) % TOGGLE)): */
      int i = p[0];
      int x = logt[p[i]];
      if (x != TOGGLE) { /* p[i] != 0 */
	r[2*i - 1] = expt[(x += x) >= TOGGLE ? x - TOGGLE : x];
      } else {
	r[2*i - 1] = 0;
      }
      for (i = p[0] - 1; i != 0; i--) {
	r[2*i] = 0;
	x = logt[p[i]];
	if (x != TOGGLE) { /* p[i] != 0 */
	  r[2*i - 1] = expt[(x += x) >= TOGGLE ? x - TOGGLE : x];
	} else {
	  r[2*i - 1] = 0;
	}
      }
      r[0] = (char)(2*p[0] - 1);
      /* reduce r mod (x^GF_K + x^GF_T + 1): */
      gfReduce(r);
    } else {
      r[0] = 0;
    }
  }

  /** sets p := (b^(-1))*p mod (x^GF_K + x^GF_T + 1) */
  static void gfSmallDiv(char[] p, char b) {
    int x, lb = logt[b];

    assert(b != 0);
    for (int i = p[0]; i > 0; i--) {
      if ((x = logt[p[i]]) != TOGGLE) { /* p[i] != 0 */
	p[i] = expt[(x += TOGGLE - lb) >= TOGGLE ? x - TOGGLE : x];
      }
    }
  }

  private static void gfAddMul(
    final char[] a, final int alpha, final int j, final char[] b) {
    final int la = logt[alpha];
    int i = b[0];
    int x = j + i;
    int a0 = a[0];
    while (a0 < x)
      a[++a0] = 0;

    while (i > 0) {
      if ((x = logt[b[i]]) != TOGGLE) /* b[i] != 0 */
	//a[j+i] ^= expt[(x += la) % TOGGLE];	// 30ms/invert slower 
	a[j + i] ^= expt[(x += la) >= TOGGLE ? x - TOGGLE : x];
      --i;
    }

    while (a0 > 0 && a[a0]==0)
      --a0;
    a[0] = (char)a0;
  }

  /** sets b := a^(-1) mod (x^GF_K + x^GF_T + 1)
      warning: a and b must not overlap! 
      @return true on failure
   */
  static boolean gfInvert(char[] r, char[] a) {
    int x, j, alpha;

    if (r == a)
      throw new InvalidArgumentException("source and destination overlap");
    if (a[0] == 0) {
      /* a is not invertible */
      return true;
    }

    /* initialize b := 1; c := 0; f := p; g := x^GF_K + x^GF_T + 1: */
    char[] b = r;
    b[0] = 1; b[1] = 1;
    char[] c = gfNew();
    char[] f = gfNew(a);
    char[] g = gfNew();
    g[0] = (char)(GF_K + 1); g[1] = 1; g[GF_T + 1] = 1; g[GF_K + 1] = 1;

    try {
      for (;;) {
	if (f[0] == 1) {
	  assert (f[1] > 0);
	  gfSmallDiv (b, f[1]);
	  if (b != r)
	    gfCopy(r,c = b);
	  return false;
	}
	if (f[0] < g[0]) {
	  char[] t = f; f = g; g = t;
	  t = b; b = c; c = t;
	}
	j = f[0] - g[0];
	x = logt[f[f[0]]] - logt[g[g[0]]] + TOGGLE;
	alpha = expt[x >= TOGGLE ? x - TOGGLE : x];
	gfAddMul(f, alpha, j, g);
	gfAddMul(b, alpha, j, c);
      }
    }
    finally {
      /* destroy potentially sensitive data: */
      gfClear(c); gfClear(f); gfClear(g); x = j = alpha = 0;
    }
  }

  /** sets p := sqrt(b) = b^(2^(GF_M-1)) */
  static void gfSquareRoot(char[] p,char b) {

    /* only values from GF(2**GF_L) are allowed for b */
    //b &= TOGGLE;
    b &= ((1 << GF_L) - 1);
    final char[] q = (b != 0) ? new char[] { 1, b } : new char[] { 0 };
    /* square q GF_M - 1 times: */
    if (((GF_M - 1) & 1) != 0) // GF_M - 1 is odd 
      gfSquare(p, q);
    else // GF_M - 1 is even 
      gfCopy(p, q);

    for (int i = GF_M - 1; i > 1; i -= 2) {
      gfSquare(p, p);
      gfSquare(p, p);
    }
  }


  /** Quickly evaluates to the trace of p. */
  static boolean gfTrace(char[] p) {
/*
      Let GF(2^m) be considered as a space vector over GF(2).
      The trace function Tr: GF(2^m) -> GF(2) is linear:
      Tr(p + q) = Tr(p) + Tr(q) and Tr(k*p) = k*Tr(p) for k in GF(2).

      Hence, the trace of any field element can be efficiently computed
      if the trace is known for a basis of GF(2^m).

      In other terms, let p(x) = SUM {p_i * x^i} for i = 0...m-1;
      then Tr(p) = SUM {p_i * Tr(x^i)} for i = 0...m-1.

      Surprisingly enough (at least for me :-), it is often the case that
      Tr(p) is simply Tr(p_0) or else Tr(p_0) + Tr(p_(m-1)).

      These properties are exploited in this fast algorithm by George Barwood.
*/

    if (GF_TM0 == 1 && GF_TM1 == 1)
      /* unit trace mask */
      return (p[0] != 0) && (p[1] & 1) != 0;
    if (p[0] != 0) {
      int i;
      int w;

/*
      // the general algorithm would be:
      for (lunit n  = trace_mask[0]; n > 0; n--) {
	      w ^= p[n] & trace_mask[n];
      }
*/
      w = p[1] & GF_TM1;
      if (GF_TM0 != 1)
	if (p[0] >= GF_TM0) {
	  w ^= p[GF_TM0] & GF_TM2;
	}

      /* compute parity of w: */
      for (i = BITS_PER_LUNIT/2; i > 0; i >>= 1) {
	w ^= w >> i;
      }
      return (w & 1) != 0;
    }
    return false;
  } /* gfTrace */

  /** set p to a solution of p^2 + p = beta.
      @return true if there is no solution
    */
  static boolean gfQuadSolve (char[] p, char[] beta) {
    int i;
    assert (p != beta); /* note that this test is not complete */
    /* check if a solution exists: */
    if (gfTrace (beta))
      return true; /* no solution */
    if ((GF_M & 1) == 0) {
      char[] d = gfNew(beta), t = gfNew();
      p[0] = 0;
      assert (gfTrace(nzt));
      for (i = 1; i < GF_M; i++) {
	gfSquare(p, p);
	gfSquare(d, d);
	gfMultiply(t, d, nzt);
	gfAdd(p, p, t);
	gfAdd(d, d, beta);
      }
      /* destroy potentially sensitive information: */
      gfClear(d);
      gfClear(t);
    }
    else {
      gfCopy(p, beta);
      for (i = 0; i < GF_M/2; i++) {
	gfSquare(p, p);
	gfSquare(p, p);
	gfAdd(p, p, beta);
      }
    }
    return false; /* solution found */
  }


  /** Evaluates to the rightmost (least significant) bit of p */
  static boolean gfYbit (char[] p) {
    return (p[0] != 0) && (p[1] & 1) != 0;
  } /* gfYbit */


  /** Packs a field point into a BigInteger. */
  static BigInteger gfPack(char[] p) {
    BigInteger k = BigInteger.valueOf(0);
    for (int i = p[0]; i > 0; i--)
      k = k.shiftLeft(GF_L).add(BigInteger.valueOf(p[i]));
    return k;
  } /* gfPack */


  /** Unpacks a BigInteger into a field point. */
  static void gfUnpack (char[] p, BigInteger x) {
    char n;
    for (n = 0; x.signum() != 0; n++) {
      p[n+1] = (char) (x.intValue() & TOGGLE);
      x = x.shiftRight(GF_L);
    }
    p[0] = n;
  } /* gfUnpack */

  // self test code follows

  /** Formats the contents of p. */
  static String toString(char[] p) {
    boolean bNeedsPlus = false;
    final StringBuffer buf = new StringBuffer();

    for (int i = p[0]; i > 0; i--) {
      if (p[i] != 0) {
	if (bNeedsPlus)
	  buf.append(" + ");
	buf.append(Integer.toHexString(p[i]));
	if (i > 2) {
	  buf.append("x^");
	  buf.append(i-1);
	} else if (i == 2) {
	  buf.append('x');
	} 
	bNeedsPlus = true;
      }
    }
    if (!bNeedsPlus) {
      // null polynomial 
      return "0";
    }
    return buf.toString();
  } /* gfPrint */

  /** sets p := <random field element> */
  static void gfRandom (char[] p,Random rand) {
    p[0] = GF_K;
    for (int i = 1; i <= GF_K; i++) {
      p[i] = (char) (rand.nextInt() & TOGGLE);
    }
    while (p[0] != 0 && p[p[0]] == 0) {
      p[0]--;
    }
  } /* gfRandom */

}
