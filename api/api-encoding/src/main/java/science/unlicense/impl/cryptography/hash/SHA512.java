package science.unlicense.impl.cryptography.hash;

import science.unlicense.api.character.Chars;

/**
 * Implementation of SHA-512.
 *
 * @author Francois Berder
 *
 */
public class SHA512 extends SHA2_64 {

    public static final Chars NAME = new Chars("SHA-512");

    private final static long resetState[] = { 
        0x6a09e667f3bcc908L, 0xbb67ae8584caa73bL, 0x3c6ef372fe94f82bL, 0xa54ff53a5f1d36f1L,
        0x510e527fade682d1L, 0x9b05688c2b3e6c1fL, 0x1f83d9abfb41bd6bL, 0x5be0cd19137e2179L
    };

    public SHA512() {
    	super(resetState);
    }
    
	@Override
	public Chars getName() {
		return NAME;
	}

	@Override
	public int getResultLength() {
		return 512;
	}
}
