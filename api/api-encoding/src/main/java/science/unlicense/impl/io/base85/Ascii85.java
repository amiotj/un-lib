
package science.unlicense.impl.io.base85;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * Documentation :
 * https://en.wikipedia.org/wiki/Ascii85
 * http://www.adobe.com/products/postscript/pdfs/PLRM.pdf - ASCII85Encode
 * http://tenminutetutor.com/binary-encoding-ascii85-encoding
 * 
 * @author Johann Sorel
 */
public class Ascii85 {

    private static final int[] SCALES = new int[]{
        52200625,
        614125,
        7225,
        85,
        1
    };

    public static Chars encodeBytes(byte[] data) {
        final CharBuffer cb = new CharBuffer(CharEncodings.US_ASCII);
        cb.append('<');
        cb.append('~');

        int nb = 0;
        int x = 0;
        while(nb < data.length-1){
            x = (x << 8) | data[nb++];

            if(nb%4 == 0) {
                if(x==0){
                    cb.append('z');
                }else{
                    cb.append( 33 + ((x/52200625) % 85) );
                    cb.append( 33 + ((x/614125 ) % 85) );
                    cb.append( 33 + ((x/7225 ) % 85) );
                    cb.append( 33 + ((x/85) % 85) );
                    cb.append( 33 + (x % 85) );
                    x = 0;
                }
            }
        }

        //pad remaining
        x = (x << 8) | (data[nb++]&0xFF);
        final int padding = nb%4;
        switch (padding) {
            case 1:
                x = (x << 24);
                cb.append( 33 + ((x/52200625) % 85) );
                cb.append( 33 + ((x/614125 ) % 85) );
                break;
            case 2:
                x = (x << 16);
                cb.append( 33 + ((x/52200625) % 85) );
                cb.append( 33 + ((x/614125 ) % 85) );
                cb.append( 33 + ((x/7225 ) % 85) );
                break;
            case 3:
                x = (x << 8);
                cb.append( 33 + ((x/52200625) % 85) );
                cb.append( 33 + ((x/614125 ) % 85) );
                cb.append( 33 + ((x/7225 ) % 85) );
                cb.append( 33 + ((x/85) % 85) );
                break;
            case 0:
                cb.append( 33 + ((x/52200625) % 85) );
                cb.append( 33 + ((x/614125 ) % 85) );
                cb.append( 33 + ((x/7225 ) % 85) );
                cb.append( 33 + ((x/85) % 85) );
                cb.append( 33 + (x % 85) );
                break;
        }
        cb.append('~');
        cb.append('>');

        return cb.toChars();
    }

    public static byte[] decode(Chars data){
        final ByteSequence seq = new ByteSequence();
        final CharIterator ite = data.createIterator();

        if(ite.nextToUnicode()!='<') throw new RuntimeException("Missing start tag <~");
        if(ite.nextToUnicode()!='~') throw new RuntimeException("Missing start tag <~");

        int c;
        int x = 0;
        int nb = 0;
        final byte[] buffer = new byte[4];
        while(ite.hasNext()){
            c = ite.nextToUnicode();
            if(c==' '||c=='\n'||c=='\r'||c=='\t') continue;

            if(c=='z'){
                if(nb!=0) throw new RuntimeException("Unvalid 'z' position");
                seq.put((byte)0);
                seq.put((byte)0);
                seq.put((byte)0);
                seq.put((byte)0);
            }else if(c=='~'){
                x += (c-33)*SCALES[nb];
                //end of data
                break;
            }else{
                x += (c-33)*SCALES[nb++];
                if(nb==5){
                    NumberEncoding.BIG_ENDIAN.writeInt(x, buffer, 0);
                    seq.put(buffer);
                    nb=0;
                    x=0;
                }
            }
        }

        if(nb!=0){
            NumberEncoding.BIG_ENDIAN.writeInt(x, buffer, 0);
            switch(nb){
                case 1:
                    throw new RuntimeException("Unvalid number of remaining characters.");
                case 2:
                    seq.put(buffer[0]);
                    break;
                case 3:
                    seq.put(buffer[0]);
                    seq.put(buffer[1]);
                    break;
                case 4:
                    seq.put(buffer[0]);
                    seq.put(buffer[1]);
                    seq.put(buffer[2]);
                    break;
            }
        }

        return seq.toArrayByte();
    }

}
