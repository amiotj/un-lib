
package science.unlicense.impl.io.deflate;

import science.unlicense.impl.io.huffman.HuffmanTree;

/**
 *
 * @author Johann Sorel
 */
public class DeflateConstants {

    static final HuffmanTree FIX_HUFFMAN_LITERAL = HuffmanUtilities.buildFixedHuffmanLiteralTree();
    static final HuffmanTree FIX_HUFFMAN_DISTANCE = HuffmanUtilities.buildFixedHuffmanDistanceTree();

    public static final int NO_COMPRESSION = 0;
    public static final int FIXED_HUFFMAN = 1;
    public static final int DYNAMIC_HUFFMAN = 2;
    public static final int RESERVED = 3;

}
