package science.unlicense.impl.io.rle;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.AbstractInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 * Run-Length Encoding input stream.
 *
 * This stream wraps another stream and uncompress it's content using RLE.
 * http://en.wikipedia.org/wiki/Run-length_encoding
 *
 * A derivate version of RLE is also used in bitmaps, this class can not handle it.
 *
 * @author Johann Sorel
 */
public final class RLEInputStream extends  AbstractInputStream{

    private final DataInputStream in;
    private final int dataSize;
    private final byte[] buffer;
    private int index = 0;
    private int remaining = 0;

    public RLEInputStream(final ByteInputStream in) {
        this(in,1);
    }

    /**
     * @param in
     * @param dataSize , number of byte used for rawdata and rle packet
     *  Common value is 1 (for one byte), but some storage format can take advantage of
     *  largeur blocks (like 4bytes for a float/int or 3bytes for a color).
     */
    public RLEInputStream(final ByteInputStream in,final int dataSize) {
        this.in = (in instanceof DataInputStream) ? (DataInputStream)in : new DataInputStream(in);
        this.dataSize = dataSize;
        this.buffer = new byte[128*dataSize];
    }

    public int read() throws IOException {

        read:
        if(remaining<=0){
            final int flag = in.read();
            if(flag<0){
                //nothing left in the wrapped stream
                break read;
            }

            index=0;
            if((flag & 0x80) == 0){
                remaining = (flag + 1)*dataSize;
                in.readFully(buffer, 0, remaining);
            }else{
                final int nb = (flag & 0x7F)+1;
                remaining = nb*dataSize;
                in.readFully(buffer, 0, dataSize);
                for(int i=1;i<nb;i++){
                    Arrays.copy(buffer, 0, dataSize, buffer, dataSize*i);
                }
            }
        }

        if(remaining<=0){
            //nothing left to read
            return -1;
        }

        int b = buffer[index] & 0xff; //unsign it
        index++;
        remaining--;
        return b;
    }

    public void close() throws IOException {
        in.close();
    }

}
