package science.unlicense.impl.cryptography.hash;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.number.NumberEncoding;

/**
 * Code used by SHA384 and SHA512.
 * 
 * Description: https://tools.ietf.org/html/rfc4634
 * 
 * @author Francois Berder
 *
 */
public abstract class SHA2_64 implements HashFunction {

	private final long resetState[];
    private long state[] = new long[8];
    private long count;
    private byte[] digestBits;

    private long block[] = new long[16];
    private int blockIndex;

//	#define ROTL(W,N) (((W) << (N)) | ((W) >> (64-(N))))
//	#define ROTR(W,N) (((W) >> (N)) | ((W) << (64-(N))))
//	#define CH(X,Y,Z) (((X) & (Y)) ^ ((~(X)) & (Z)))
//	#define MAJ(X,Y,Z) (((X) & (Y)) ^ ((X) & (Z)) ^ ((Y) & (Z)))
//	#define BSIG0(X) (ROTR(X,28) ^ ROTR(X,34) ^ ROTR(X,39))
//	#define BSIG1(X) (ROTR(X,14) ^ ROTR(X,18) ^ ROTR(X,41))
//	#define SSIG0(X) (ROTR((X),1) ^ ROTR((X),8) ^ ((X) >> 7))
//	#define SSIG1(X) (ROTR((X),19) ^ ROTR((X),61) ^ ((X) >> 6))
//
//	#define R(A,B,C,D,E,F,G,H,K,T)  T1 = H + BSIG1(E) + CH(E,F,G) + K + w[T]; \
//	                              T2 = BSIG0(A) + MAJ(A,B,C); \
//	                              D += T1; \
//	                              H = T1 + T2;    
    private final long dd[] = new long[8];

    public SHA2_64(long resetState[]) {
    	this.resetState = resetState;
    	reset();
    }
    
	@Override
	public boolean isCryptographic() {
		return true;
	}

	@Override
	public void reset() {
		
        state[0] = resetState[0];
        state[1] = resetState[1];
        state[2] = resetState[2];
        state[3] = resetState[3];
        state[4] = resetState[4];
        state[5] = resetState[5];
        state[6] = resetState[6];
        state[7] = resetState[7];

        count = 0;
        digestBits = new byte[getResultLength()/8];
        Arrays.fill(digestBits, (byte)0);
		blockIndex = 0;
	}
	
	public void update(int b) {
	    update((byte)b);
	}
	
	public void update(byte b) {
	    final int mask = (8 * (blockIndex & 7));
	
	    count += 8;
	    block[blockIndex >> 3] &= (~((long)(0xff) << mask));
	    block[blockIndex >> 3] |= ((long)(b & 0xff) << mask);
	    blockIndex++;
	    if (blockIndex == 128) {
	        transform();
	        blockIndex = 0;
	    }
	}
	
	public byte[] getResultBytes() {
	    finish();
	    return Arrays.copy(digestBits, new byte[digestBits.length]);
	}
	
	public long getResultLong() {
	    return NumberEncoding.BIG_ENDIAN.readLong(getResultBytes(), 0);
	}
	
	public void update(byte input[]) {
	    update(input, 0, input.length);
	}
	
	public void update(byte input[], int offset, int len) {
	    for (int i = 0; i < len; i++) {
	        update(input[i + offset]);
	    }
	}

	private void transform() {
		long w[] = new long[80];
		long T[] = new long[2];
		
		for(int i = 0; i < 16; ++i) {
			w[i] = rev(block[i]);
		}
	    for(int i = 16; i < 80; ++i) {
	        w[i] = SSIG1(w[i-2]) + w[i-7] + SSIG0(w[i-15]) + w[i-16];
	    }
	    
        /* Copy context->state[] to working vars */
        dd[0] = state[0];
        dd[1] = state[1];
        dd[2] = state[2];
        dd[3] = state[3];
        dd[4] = state[4];
        dd[5] = state[5];
        dd[6] = state[6];
        dd[7] = state[7];
        
        R(T,w,dd,0,1,2,3,4,5,6,7,0,0x428a2f98d728ae22L);
        R(T,w,dd,7,0,1,2,3,4,5,6,1,0x7137449123ef65cdL);
        R(T,w,dd,6,7,0,1,2,3,4,5,2,0xb5c0fbcfec4d3b2fL);
        R(T,w,dd,5,6,7,0,1,2,3,4,3,0xe9b5dba58189dbbcL);
        R(T,w,dd,4,5,6,7,0,1,2,3,4,0x3956c25bf348b538L);
        R(T,w,dd,3,4,5,6,7,0,1,2,5,0x59f111f1b605d019L);
        R(T,w,dd,2,3,4,5,6,7,0,1,6,0x923f82a4af194f9bL);
        R(T,w,dd,1,2,3,4,5,6,7,0,7,0xab1c5ed5da6d8118L);
        
        R(T,w,dd,0,1,2,3,4,5,6,7,8,0xd807aa98a3030242L);
        R(T,w,dd,7,0,1,2,3,4,5,6,9,0x12835b0145706fbeL);
        R(T,w,dd,6,7,0,1,2,3,4,5,10,0x243185be4ee4b28cL);
        R(T,w,dd,5,6,7,0,1,2,3,4,11,0x550c7dc3d5ffb4e2L);
        R(T,w,dd,4,5,6,7,0,1,2,3,12,0x72be5d74f27b896fL);
        R(T,w,dd,3,4,5,6,7,0,1,2,13,0x80deb1fe3b1696b1L);
        R(T,w,dd,2,3,4,5,6,7,0,1,14,0x9bdc06a725c71235L);
        R(T,w,dd,1,2,3,4,5,6,7,0,15,0xc19bf174cf692694L);
        
        R(T,w,dd,0,1,2,3,4,5,6,7,16,0xe49b69c19ef14ad2L);
        R(T,w,dd,7,0,1,2,3,4,5,6,17,0xefbe4786384f25e3L);
        R(T,w,dd,6,7,0,1,2,3,4,5,18,0x0fc19dc68b8cd5b5L);
        R(T,w,dd,5,6,7,0,1,2,3,4,19,0x240ca1cc77ac9c65L);
        R(T,w,dd,4,5,6,7,0,1,2,3,20,0x2de92c6f592b0275L);
        R(T,w,dd,3,4,5,6,7,0,1,2,21,0x4a7484aa6ea6e483L);
        R(T,w,dd,2,3,4,5,6,7,0,1,22,0x5cb0a9dcbd41fbd4L);
        R(T,w,dd,1,2,3,4,5,6,7,0,23,0x76f988da831153b5L);
        
        R(T,w,dd,0,1,2,3,4,5,6,7,24,0x983e5152ee66dfabL);
        R(T,w,dd,7,0,1,2,3,4,5,6,25,0xa831c66d2db43210L);
        R(T,w,dd,6,7,0,1,2,3,4,5,26,0xb00327c898fb213fL);
        R(T,w,dd,5,6,7,0,1,2,3,4,27,0xbf597fc7beef0ee4L);
        R(T,w,dd,4,5,6,7,0,1,2,3,28,0xc6e00bf33da88fc2L);
        R(T,w,dd,3,4,5,6,7,0,1,2,29,0xd5a79147930aa725L);
        R(T,w,dd,2,3,4,5,6,7,0,1,30,0x06ca6351e003826fL);
        R(T,w,dd,1,2,3,4,5,6,7,0,31,0x142929670a0e6e70L);
        
        R(T,w,dd,0,1,2,3,4,5,6,7,32,0x27b70a8546d22ffcL);
        R(T,w,dd,7,0,1,2,3,4,5,6,33,0x2e1b21385c26c926L);
        R(T,w,dd,6,7,0,1,2,3,4,5,34,0x4d2c6dfc5ac42aedL);
        R(T,w,dd,5,6,7,0,1,2,3,4,35,0x53380d139d95b3dfL);
        R(T,w,dd,4,5,6,7,0,1,2,3,36,0x650a73548baf63deL);
        R(T,w,dd,3,4,5,6,7,0,1,2,37,0x766a0abb3c77b2a8L);
        R(T,w,dd,2,3,4,5,6,7,0,1,38,0x81c2c92e47edaee6L);
        R(T,w,dd,1,2,3,4,5,6,7,0,39,0x92722c851482353bL);
        
        R(T,w,dd,0,1,2,3,4,5,6,7,40,0xa2bfe8a14cf10364L);
        R(T,w,dd,7,0,1,2,3,4,5,6,41,0xa81a664bbc423001L);
        R(T,w,dd,6,7,0,1,2,3,4,5,42,0xc24b8b70d0f89791L);
        R(T,w,dd,5,6,7,0,1,2,3,4,43,0xc76c51a30654be30L);
        R(T,w,dd,4,5,6,7,0,1,2,3,44,0xd192e819d6ef5218L);
        R(T,w,dd,3,4,5,6,7,0,1,2,45,0xd69906245565a910L);
        R(T,w,dd,2,3,4,5,6,7,0,1,46,0xf40e35855771202aL);
        R(T,w,dd,1,2,3,4,5,6,7,0,47,0x106aa07032bbd1b8L);

        R(T,w,dd,0,1,2,3,4,5,6,7,48,0x19a4c116b8d2d0c8L);
        R(T,w,dd,7,0,1,2,3,4,5,6,49,0x1e376c085141ab53L);
        R(T,w,dd,6,7,0,1,2,3,4,5,50,0x2748774cdf8eeb99L);
        R(T,w,dd,5,6,7,0,1,2,3,4,51,0x34b0bcb5e19b48a8L);
        R(T,w,dd,4,5,6,7,0,1,2,3,52,0x391c0cb3c5c95a63L);
        R(T,w,dd,3,4,5,6,7,0,1,2,53,0x4ed8aa4ae3418acbL);
        R(T,w,dd,2,3,4,5,6,7,0,1,54,0x5b9cca4f7763e373L);
        R(T,w,dd,1,2,3,4,5,6,7,0,55,0x682e6ff3d6b2b8a3L);
        
        R(T,w,dd,0,1,2,3,4,5,6,7,56,0x748f82ee5defb2fcL);
        R(T,w,dd,7,0,1,2,3,4,5,6,57,0x78a5636f43172f60L);
        R(T,w,dd,6,7,0,1,2,3,4,5,58,0x84c87814a1f0ab72L);
        R(T,w,dd,5,6,7,0,1,2,3,4,59,0x8cc702081a6439ecL);
        R(T,w,dd,4,5,6,7,0,1,2,3,60,0x90befffa23631e28L);
        R(T,w,dd,3,4,5,6,7,0,1,2,61,0xa4506cebde82bde9L);
        R(T,w,dd,2,3,4,5,6,7,0,1,62,0xbef9a3f7b2c67915L);
        R(T,w,dd,1,2,3,4,5,6,7,0,63,0xc67178f2e372532bL);

        R(T,w,dd,0,1,2,3,4,5,6,7,64,0xca273eceea26619cL);
        R(T,w,dd,7,0,1,2,3,4,5,6,65,0xd186b8c721c0c207L);
        R(T,w,dd,6,7,0,1,2,3,4,5,66,0xeada7dd6cde0eb1eL);
        R(T,w,dd,5,6,7,0,1,2,3,4,67,0xf57d4f7fee6ed178L);
        R(T,w,dd,4,5,6,7,0,1,2,3,68,0x06f067aa72176fbaL);
        R(T,w,dd,3,4,5,6,7,0,1,2,69,0x0a637dc5a2c898a6L);
        R(T,w,dd,2,3,4,5,6,7,0,1,70,0x113f9804bef90daeL);
        R(T,w,dd,1,2,3,4,5,6,7,0,71,0x1b710b35131c471bL);

        R(T,w,dd,0,1,2,3,4,5,6,7,72,0x28db77f523047d84L);
        R(T,w,dd,7,0,1,2,3,4,5,6,73,0x32caab7b40c72493L);
        R(T,w,dd,6,7,0,1,2,3,4,5,74,0x3c9ebe0a15c9bebcL);
        R(T,w,dd,5,6,7,0,1,2,3,4,75,0x431d67c49c100d4cL);
        R(T,w,dd,4,5,6,7,0,1,2,3,76,0x4cc5d4becb3e42b6L);
        R(T,w,dd,3,4,5,6,7,0,1,2,77,0x597f299cfc657e2aL);
        R(T,w,dd,2,3,4,5,6,7,0,1,78,0x5fcb6fab3ad6faecL);
        R(T,w,dd,1,2,3,4,5,6,7,0,79,0x6c44198c4a475817L);
        
        /* Add the working vars back into context.state[] */
        state[0] += dd[0];
        state[1] += dd[1];
        state[2] += dd[2];
        state[3] += dd[3];
        state[4] += dd[4];
        state[5] += dd[5];
        state[6] += dd[6];
        state[7] += dd[7];
	}
	
	private static long rev(long w) {

	    return ((w & 0x00000000000000FFL) << 56)
	        | ((w & 0x000000000000FF00L) << 40)
	        | ((w & 0x0000000000FF0000L) << 24)
	        | ((w & 0x00000000FF000000L) << 8)
	        | ((w & 0x000000FF00000000L) >>> 8)
	        | ((w & 0x0000FF0000000000L) >>> 24)
	        | ((w & 0x00FF000000000000L) >>> 40)
	        | ((w & 0xFF00000000000000L) >>> 56);
	}
	    
    private long ROTR(long W, long N) {
    	return (W >>> N) | (W << (64-N));
    }
    
    private long CH(long X, long Y, long Z) { 
    	return (X & Y) ^ ((~X) & Z);
    }
    
    private long MAJ(long X, long Y, long Z) {
    	return (X & Y) ^ (X & Z) ^ (Y & Z);
    }
    
    private long BSIG0(long X) {
    	return ROTR(X,28) ^ ROTR(X,34) ^ ROTR(X,39);
    }
    
    private long BSIG1(long X) {
    	return ROTR(X,14) ^ ROTR(X,18) ^ ROTR(X,41);
    }
    
    private long SSIG0(long X) {
    	return ROTR(X,1) ^ ROTR(X,8) ^ (X >>> 7);
    }
    
    private long SSIG1(long X) {
    	return ROTR(X,19) ^ ROTR(X,61) ^ (X >>> 6);
    }

    private void R(long[] T, long[] w, long[] dd, int indexA, int indexB, int indexC, int indexD, int indexE, int indexF, int indexG, int indexH, int indexW, long K){
	    T[0] = dd[indexH] + BSIG1(dd[indexE]) + CH(dd[indexE],dd[indexF],dd[indexG]) + K + w[indexW];
	    T[1] = BSIG0(dd[indexA]) + MAJ(dd[indexA],dd[indexB],dd[indexC]);
	    dd[indexD] += T[0];
	    dd[indexH] = T[0] + T[1];   
    }
    
    private void finish() {
        final byte bits[] = new byte[8];

        for (int i = 0; i < 8; i++) {
            bits[i] = (byte) ((count >>> (((7 - i) * 8))) & 0xff);
        }

        update((byte) 128);
        while (blockIndex != 120) {
            update((byte) 0);
        }
        // This should cause a transform to happen.
        update(bits);
        for (int i = 0; i < this.getResultLength()/8; i++) {
            digestBits[i] = (byte) ((state[i >> 3] >> ((7 - (i & 7)) * 8)) & 0xff);
        }
    }
}
