
package science.unlicense.impl.cryptography.hash;

import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.WrapInputStream;

/**
 * Inputstream which calculate hash function while reading.
 *
 * @author Johann Sorel
 */
public class HashInputStream extends WrapInputStream{

    private final HashFunction hashFunction;

    public HashInputStream(final HashFunction hashFunction, final ByteInputStream in) {
        super(in);
        this.hashFunction = hashFunction;
    }

    public void hashReset(){
        hashFunction.reset();
    }

    public long hashValue(){
        return hashFunction.getResultLong();
    }

    @Override
    public int read() throws IOException {
        final int b = in.read();
        if(b != -1){
            hashFunction.update(b);
        }
        return b;
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        int nb = in.read(buffer, offset, length);
        if(nb!=-1) hashFunction.update(buffer,offset,nb);
        return nb;
    }
    
}
