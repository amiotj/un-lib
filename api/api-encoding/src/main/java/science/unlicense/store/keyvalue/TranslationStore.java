
package science.unlicense.store.keyvalue;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.AbstractCharArray;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.DefaultLChars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.character.Language;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.country.Country;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class TranslationStore {
    
    private final Dictionary trs = new HashDictionary();
    private final CharEncoding encoding;
    private Path path;

    public TranslationStore() {
        this(null);
    }
    
    /**
     * Store from path with default encoding (UTF-8).
     * 
     * @param path 
     */
    public TranslationStore(Path path) {
        this(path, CharEncodings.UTF_8);
    }

    public TranslationStore(Path path, CharEncoding encoding) {
        this.path = path;
        this.encoding = encoding;
    }

    /**
     * List available langues in this store.
     * @return Iterator, never null, can be empty.
     */
    public Iterator getLanguages(){
        return trs.getKeys().createIterator();
    }
    
    public LChars getChars(Chars key){
        return new PropLChars(null, key);
    }

    /**
     * Load translations from original path.
     * 
     * @return this store
     * @throws science.unlicense.api.io.IOException
     */
    public TranslationStore load() throws IOException {
        return load(path);
    }
    
    /**
     * Load translations from given path.
     * 
     * @param path
     * @return this store
     * @throws science.unlicense.api.io.IOException
     */
    public TranslationStore load(final Path path) throws IOException {
        final Chars name = path.getName().split('.')[0];
        final Path parent = path.getParent();
        final Iterator ite = parent.getChildren().createIterator();
        while(ite.hasNext()){
            final Path p = (Path) ite.next();
            final Chars pname = p.getName();
            if(pname.startsWith(name)){
                //TODO only handle 1 division for now, improve it for N divisions
                //translation file
                final Chars country = pname.truncate(0, name.getCharLength()).split('.')[0];
                final KeyValueStore lstore = new KeyValueStore(p,encoding).load();
                final Language l = Country.getForIsoCode3(new Chars(country)).asLanguage();
                trs.add(l, lstore);
            }
        }
        return this;
    }
    
    
    private class PropLChars extends AbstractCharArray implements LChars{

        private final Language language;
        private final Chars code;

        public PropLChars(Language language, Chars code) {
            this.language = language;
            this.code = code;
        }
        
        protected byte[] getBytesInternal() {
            final KeyValueStore kv;
            if(language==null){
                kv = (KeyValueStore) ((Pair)trs.getPairs().createIterator().next()).getValue2();
            }else{
                kv = (KeyValueStore) trs.getValue(language);
            }
            if(kv==null) return Arrays.ARRAY_BYTE_EMPTY;
            final Chars c = kv.getProperty(code);
            if(c==null) return Arrays.ARRAY_BYTE_EMPTY;
            return c.toBytes();
        }

        protected boolean isByteInternalCopy() {
            return true;
        }

        public CharEncoding getEncoding() {
            return encoding;
        }

        public LChars translate(Language language) {
            return new PropLChars(language,code);
        }

        public Language getLanguage() {
            return language;
        }

        public Iterator getLanguages() {
            return TranslationStore.this.getLanguages();
        }

        public Chars toCharsAll() {
            return new DefaultLChars(this).toCharsAll();
        }
    }
    
}
