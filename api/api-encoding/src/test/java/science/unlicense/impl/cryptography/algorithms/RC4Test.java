package science.unlicense.impl.cryptography.algorithms;

import science.unlicense.impl.cryptography.algorithms.RC4;
import org.junit.Assert;

import org.junit.Test;

/**
 * 
 * @author Francois Berder
 *
 */
public class RC4Test {

    // 128 bits key
    private final byte[] key = {
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 
    };
    
    @Test
    public void test() {
        
        RC4 cipherAlgo = new RC4(key);
        
        byte[] text = new byte[64];
        for(int i = 0; i < 64; ++i)
            text[i] = (byte) (64 - i);

        byte[] cipherText = cipherAlgo.cipher(text);
        
        RC4 invCipherAlgo = new RC4(key);
        byte[] result = invCipherAlgo.invCipher(cipherText);
        Assert.assertArrayEquals(text, result);
    }

}
