package science.unlicense.impl.cryptography.hash;

import science.unlicense.impl.cryptography.hash.SHA256;
import org.junit.Assert;

import org.junit.Test;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

/**
 * 
 * @author Francois Berder
 *
 */
public class SHA256Test {

	@Test
	public void test() {
        final SHA256 s = new SHA256();

        // "abc"
        String z = "abc";
        s.reset();
        s.update((byte) 'a');
        s.update((byte) 'b');
        s.update((byte) 'c');
        Assert.assertEquals(new Chars("BA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9CB410FF61F20015AD"), Int32.encodeHexa(s.getResultBytes()));
        
        // "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        s.reset();
        s.update(z.getBytes());
        Assert.assertEquals(new Chars("248D6A61D20638B8E5C026930C3E6039A33CE45964FF2167F6ECEDD419DB06C1"), Int32.encodeHexa(s.getResultBytes()));

        // A million repetitions of "a"
        s.reset();
        for (int i = 0; i < 1000000; i++) {
            s.update((byte) 'a');
        }
        Assert.assertEquals(new Chars("CDC76E5C9914FB9281A1C7E284D73E67F1809A48A497200E046D39CCC7112CD0"), Int32.encodeHexa(s.getResultBytes()));
	}

}
