

package science.unlicense.impl.cryptography.kdf;

import java.security.GeneralSecurityException;
import org.junit.Test;
import org.junit.Assert;
import static science.unlicense.impl.cryptography.kdf.PBKDF2.pbkdf2;

/**
 * PBKDF2 tests.
 * 
 * @author Thibaut Cuvelier
 * @author Johann Sorel
 */
public class PBKDF2Test {
    
    @Test
    public void pbkdf2Test() throws GeneralSecurityException {
        int iterations = 10;
        int size = 8;
        final byte[] salt = { 10, 10, 10, 10, 10 };
        String password = "4242";
        final byte[] pswdB = password.getBytes();
        final byte[] hash = pbkdf2(pswdB, salt, iterations, size, "HmacSHA1");

        Assert.assertArrayEquals(new byte[]{90, 112, 82, -8, 35, -61, 86, - 51}, hash);
    }
    
}
