package science.unlicense.impl.io.rle;

import science.unlicense.impl.io.rle.RLEInputStream;
import java.nio.charset.Charset;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class RLETest {


    /**
     * test RLE reading
     */
    @Test
    public void readTest() throws IOException{
        final String expected = "WWWWWWWWWWWWabcBBBB"; //12Wabc4B

        final byte[] buffer = new byte[]{
          (byte)139, // 128 + 12 - 1 = 139
          0x57,      //W
          (byte)2,   // raw for 3 next byte , RLE n+1
          0x61,      //a
          0x62,      //b
          0x63,      //c
          (byte)131, // 128 + 4 - 1 = 131
          0x42,      //B
        };

        final ByteInputStream stream = new ArrayInputStream(buffer);
        final RLEInputStream rel = new RLEInputStream(stream);

        final byte[] res = new byte[12+3+4];
        rel.read(res,0,res.length);

        Assert.assertEquals(-1,rel.read());

        String str = new String(res, Charset.forName("US-ASCII"));
        Assert.assertEquals(expected,str);

    }

}
