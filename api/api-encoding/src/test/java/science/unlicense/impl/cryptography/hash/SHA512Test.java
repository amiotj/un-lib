package science.unlicense.impl.cryptography.hash;

import science.unlicense.impl.cryptography.hash.SHA512;
import org.junit.Assert;

import org.junit.Test;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

/**
 * 
 * @author Francois Berder
 *
 */
public class SHA512Test {

	@Test
	public void test() {
        final SHA512 s = new SHA512();

        // "abc"
        String z = "abc";
        s.reset();
        s.update((byte) 'a');
        s.update((byte) 'b');
        s.update((byte) 'c');
        Assert.assertEquals(new Chars("DDAF35A193617ABACC417349AE20413112E6FA4E89A97EA20A9EEEE64B55D39A2192992A274FC1A836BA3C23A3FEEBBD454D4423643CE80E2A9AC94FA54CA49F"), Int32.encodeHexa(s.getResultBytes()));
        
        // "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        s.reset();
        s.update(z.getBytes());
        Assert.assertEquals(new Chars("204A8FC6DDA82F0A0CED7BEB8E08A41657C16EF468B228A8279BE331A703C33596FD15C13B1B07F9AA1D3BEA57789CA031AD85C7A71DD70354EC631238CA3445"), Int32.encodeHexa(s.getResultBytes()));

        // A million repetitions of "a"
        s.reset();
        for (int i = 0; i < 1000000; i++) {
            s.update((byte) 'a');
        }
        Assert.assertEquals(new Chars("E718483D0CE769644E2E42C7BC15B4638E1F98B13B2044285632A803AFA973EBDE0FF244877EA60A4CB0432CE577C31BEB009C5C2C49AA2E4EADB217AD8CC09B"), Int32.encodeHexa(s.getResultBytes()));
	}

}
