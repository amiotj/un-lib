package science.unlicense.impl.cryptography.hash;

import science.unlicense.impl.cryptography.hash.SHA224;
import org.junit.Assert;

import org.junit.Test;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

/**
 * 
 * @author Francois Berder
 *
 */
public class SHA224Test {

	@Test
	public void test() {
        final SHA224 s = new SHA224();

        // "abc"
        String z = "abc";
        s.reset();
        s.update((byte) 'a');
        s.update((byte) 'b');
        s.update((byte) 'c');
        Assert.assertEquals(new Chars("23097D223405D8228642A477BDA255B32AADBCE4BDA0B3F7E36C9DA7"), Int32.encodeHexa(s.getResultBytes()));
        
        // "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        s.reset();
        s.update(z.getBytes());
        Assert.assertEquals(new Chars("75388B16512776CC5DBA5DA1FD890150B0C6455CB4F58B1952522525"), Int32.encodeHexa(s.getResultBytes()));

        // A million repetitions of "a"
        s.reset();
        for (int i = 0; i < 1000000; i++) {
            s.update((byte) 'a');
        }
        Assert.assertEquals(new Chars("20794655980C91D8BBB4C1EA97618A4BF03F42581948B2EE4EE7AD67"), Int32.encodeHexa(s.getResultBytes()));
	}

}
