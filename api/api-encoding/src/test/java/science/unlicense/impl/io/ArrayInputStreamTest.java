package science.unlicense.impl.io;

import science.unlicense.api.io.IOException;
import science.unlicense.api.io.ArrayInputStream;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Johann Sorel
 */
public class ArrayInputStreamTest {

    @Test
    public void readTest() throws IOException{

        final byte[] data = new byte[]{1,2,3,4,5};

        final ArrayInputStream stream = new ArrayInputStream(data);

        Assert.assertEquals(1,stream.read());
        Assert.assertEquals(2,stream.read());
        Assert.assertEquals(3,stream.read());
        Assert.assertEquals(4,stream.read());
        Assert.assertEquals(5,stream.read());
        Assert.assertEquals(-1,stream.read());

        //should not raise any error
        stream.close();

    }

}
