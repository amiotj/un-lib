package science.unlicense.impl.cryptography.algorithms;

import science.unlicense.impl.cryptography.algorithms.AES;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author bertrand
 */
public class AESTest {
    
    public static byte[] hexStringToByteArray( String hexString ) {
        String hexa = hexString.replaceAll("\\s", "");
        byte[] byteArray = new byte[hexa.length()/2];
        for( int i=0; i<byteArray.length; i++ ) {
            String hex = hexa.substring(2*i, 2*i+2);
            int valInt = Integer.parseInt(hex, 16);
            byteArray[i] = (byte)valInt;
        }
        return byteArray;
    }
    
    private static final String[][] testAESData = new String[][]{
        {   // AES-128
            "00112233445566778899aabbccddeeff", // plainText
            "000102030405060708090a0b0c0d0e0f", // key (128 bits)
            "69c4e0d86a7b0430d8cdb78070b4c55a" // cipherText
        },{ // AES-192
            "00112233445566778899aabbccddeeff", // plainText
            "000102030405060708090a0b0c0d0e0f1011121314151617", // key (192 bits)
            "dda97ca4864cdfe06eaf70a0ec0d7191" // cipherText
        },{ // AES-256
            "00112233445566778899aabbccddeeff", // plainText
            "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f", // key (256bits)
            "8ea2b7ca516745bfeafc49904b496089" // cipherText
        },};
    
    /**
     * Test of go method, of class AES.
     */
    @Test
    public void testCipher() {
        for( String[] data : testAESData ) {
            byte[] plainText = hexStringToByteArray(data[0]);
            byte[] key = hexStringToByteArray(data[1]);
            byte[] cipherText = hexStringToByteArray(data[2]);
            
            AES instance = new AES( key );
            byte[] result = instance.cipher(plainText);
            
            Assert.assertArrayEquals( cipherText, result );
        }
    }
    
    /**
     * Test of go method, of class AES.
     */
    @Test
    public void testInvCipher() {
        for( String[] data : testAESData ) {
            byte[] plainText = hexStringToByteArray(data[0]);
            byte[] key = hexStringToByteArray(data[1]);
            byte[] cipherText = hexStringToByteArray(data[2]);
            
            AES instance = new AES( key );
            byte[] result = instance.invCipher(cipherText);
            
            Assert.assertArrayEquals( plainText, result );
        }
    }
    
    // =========================================================================
    // ======== Code used in debugging functions which are now private =========
    // =========================================================================
    
//    private static byte[] charArrayToByteArray( char[] charArray ) {
//        byte[] byteArray = new byte[ charArray.length];
//        for( int i=0; i<byteArray.length; i++ ) {
//            byteArray[i] = (byte)charArray[i];
//        }
//        return byteArray;
//    }
    
//    private static final char[][][]subByteSDataTest = new char[][][]{
//        {
//            {   // input
//                0x19, 0x3d, 0xe3, 0xbe,
//                0xa0, 0xf4, 0xe2, 0x2b,
//                0x9a, 0xc6, 0x8d, 0x2a,
//                0xe9, 0xf8, 0x48, 0x08,},
//            {   // subByted input
//                0xd4, 0x27, 0x11, 0xae,
//                0xe0, 0xbf, 0x98, 0xf1,
//                0xb8, 0xb4, 0x5d, 0xe5,
//                0x1e, 0x41, 0x52, 0x30,}
//        }
//    };
//    
//    private static final char[][][] shiftRowsDataTest = new char[][][]{
//        {
//            {   // input
//                0xd4, 0x27, 0x11, 0xae,
//                0xe0, 0xbf, 0x98, 0xf1,
//                0xb8, 0xb4, 0x5d, 0xe5,
//                0x1e, 0x41, 0x52, 0x30,},
//            {   // shifted input
//                0xd4, 0xbf, 0x5d, 0x30,
//                0xe0, 0xb4, 0x52, 0xae,
//                0xb8, 0x41, 0x11, 0xf1,
//                0x1e, 0x27, 0x98, 0xe5,}
//        }
//    };
//
//    private static final char[][][] mixColumnsDataTest = new char[][][]{
//        {
//            {
//                0xd4, 0xbf, 0x5d, 0x30,
//                0xe0, 0xb4, 0x52, 0xae,
//                0xb8, 0x41, 0x11, 0xf1,
//                0x1e, 0x27, 0x98, 0xe5,},
//            {
//                0x04, 0x66, 0x81, 0xe5,
//                0xe0, 0xcb, 0x19, 0x9a,
//                0x48, 0xf8, 0xd3, 0x7a,
//                0x28, 0x06, 0x26, 0x4c,}
//        },
//        {
//            {
//                0x49, 0xdb, 0x87, 0x3b,
//                0x45, 0x39, 0x53, 0x89,
//                0x7f, 0x02, 0xd2, 0xf1,
//                0x77, 0xde, 0x96, 0x1a,},
//            {
//                0x58, 0x4d, 0xca, 0xf1,
//                0x1b, 0x4b, 0x5a, 0xac,
//                0xdb, 0xe7, 0xca, 0xa8,
//                0x1b, 0x6b, 0xb0, 0xe5,}
//        }
//    };
    
//    /**
//     * Test of modulo_0x11B method, of class AES.
//     */
//    @Test
//    public void testModulo_0x11B() {
//        
//        final int[][] moduloDataTest = new int[][]{
//            {  0x113,  0x11B,  0x8 },
//            { 0x2B79, 0x11B,  0xC1 },
//        };
//        
//        System.out.println("modulo_0x11B");
//        
//        for( int[] data : moduloDataTest ) {
//            int p = data[0];
//            int expResult = data[2];
//            int result = AES.modulo_0x11B(p);
//            Assert.assertEquals(expResult, result);
//        }
//    }

//    /**
//     * Test of subBytes method, of class AES.
//     */
//    @Test
//    public void testSubBytes() {
//        System.out.println("byteSub");
//        byte[] keyDontCare = new byte[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
//        for( char[][] data : subByteSDataTest ) {
//            byte[] input = charArrayToByteArray( data[0] );
//            AES instance = new AES( keyDontCare );
//            instance.init(input);
//            instance.subBytes(AES.S_BOX);
//            byte[] expResult = charArrayToByteArray( data[1] );
//            byte[] result = instance.getState();
//            Assert.assertArrayEquals( expResult, result );
//        }
//    }
    
//    /**
//     * Test of shiftRows method, of class AES.
//     */
//    @Test
//    public void testShiftRows() {
//        System.out.println("shiftRows");
//        byte[] keyDontCare = new byte[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
//        for( char[][] data : shiftRowsDataTest ) {
//            byte[] input = charArrayToByteArray( data[0] );
//            AES instance = new AES( keyDontCare );
//            instance.init(input);
//            instance.shiftRows();
//            byte[] expResult = charArrayToByteArray( data[1] );
//            byte[] result = instance.getState();
//            Assert.assertArrayEquals( expResult, result );
//        }
//    }

//    /**
//     * Test of invShiftRows method, of class AES.
//     */
//    @Test
//    public void testInvShiftRows() {
//        System.out.println("invShiftRows");
//        byte[] keyDontCare = new byte[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
//        for( char[][] data : shiftRowsDataTest ) {
//            byte[] input = charArrayToByteArray( data[1] );
//            AES instance = new AES( keyDontCare );
//            instance.init(input);
//            instance.invShiftRows();
//            byte[] expResult = charArrayToByteArray( data[0] );
//            byte[] result = instance.getState();
//            Assert.assertArrayEquals( expResult, result );
//        }
//    }

//    /**
//     * Test of mixColumns method, of class AES.
//     */
//    @Test
//    public void testMixColumns() {
//        System.out.println("mixColumns");
//        byte[] keyDontCare = new byte[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
//        for( char[][] data : mixColumnsDataTest ) {
//            byte[] input = charArrayToByteArray( data[0] );
//            AES instance = new AES( keyDontCare );
//            instance.init(input);
//            System.out.println(instance.affiche());
//            instance.mixColumns();
//            byte[] expResult = charArrayToByteArray( data[1] );
//            byte[] result = instance.getState();
//            System.out.println(instance.affiche());
//            Assert.assertArrayEquals( expResult, result );
//        }
//    }
    
//    /**
//     * Test of invMixColumns method, of class AES.
//     */
//    @Test
//    public void testInvMixColumns() {
//        System.out.println("invMixColumns");
//        byte[] keyDontCare = new byte[]{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
//        for( char[][] data : mixColumnsDataTest ) {
//            byte[] input = charArrayToByteArray( data[1] );
//            AES instance = new AES( keyDontCare );
//            instance.init(input);
//            System.out.println(instance.affiche());
//            instance.invMixColumns();
//            byte[] expResult = charArrayToByteArray( data[0] );
//            byte[] result = instance.getState();
//            System.out.println(instance.affiche());
//            Assert.assertArrayEquals( expResult, result );
//        }
//    }

//    /**
//     * Test of keyExpansion method, of class AES.
//     */
//    @Test
//    public void testKeyExpansion() {
//        
//        String[][] keyExpansionDataTest = new String[][]{
//            {   // keyString
//                "2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c",
//                // expansion
//                "2b7e151628aed2a6abf7158809cf4f3c" + // w3 from key
//                "a0fafe1788542cb123a339392a6c7605" + // w7
//                "f2c295f27a96b9435935807a7359f67f" + // w11
//                "3d80477d4716fe3e1e237e446d7a883b" + // w15
//                "ef44a541a8525b7fb671253bdb0bad00" + // w19
//                "d4d1c6f87c839d87caf2b8bc11f915bc" + // w23
//                "6d88a37a110b3efddbf98641ca0093fd" + // w27
//                "4e54f70e5f5fc9f384a64fb24ea6dc4f" + // w31
//                "ead27321b58dbad2312bf5607f8d292f" + // w35
//                "ac7766f319fadc2128d12941575c006e" + // w39
//                "d014f9a8c9ee2589e13f0cc8b6630ca6"    // w43
//            }, {// keyString
//                "8e 73 b0 f7 da 0e 64 52 c8 10 f3 2b 80 90 79 e5 62 f8 ea d2 52 2c 6b 7b",
//                // expansion
//                "8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b" +
//                "fe0c91f72402f5a5ec12068e6c827f6b0e7a95b95c56fec2" +
//                "4db7b4bd69b5411885a74796e92538fde75fad44bb095386" +
//                "485af05721efb14fa448f6d94d6dce24aa326360113b30e6" +
//                "a25e7ed583b1cf9a27f939436a94f767c0a69407d19da4e1" +
//                "ec1786eb6fa64971485f703222cb8755e26d135233f0b7b3" +
//                "40beeb282f18a2596747d26b458c553ea7e1466c9411f1df" +
//                "821f750aad07d753ca4005388fcc5006282d166abc3ce7b5" +
//                "e98ba06f448c773c8ecc720401002202"
//            }, {// keyString
//                "60 3d eb 10 15 ca 71 be 2b 73 ae f0 85 7d 77 81 1f 35 2c 07 3b 61 08 d7 2d 98 10 a3 09 14 df f4",
//                // expansion
//                "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4" +
//                "9ba354118e6925afa51a8b5f2067fcdea8b09c1a93d194cdbe49846eb75d5b9a" +
//                "d59aecb85bf3c917fee94248de8ebe96b5a9328a2678a647983122292f6c79b3" +
//                "812c81addadf48ba24360af2fab8b46498c5bfc9bebd198e268c3ba709e04214" +
//                "68007bacb2df331696e939e46c518d80c814e20476a9fb8a5025c02d59c58239" +
//                "de1369676ccc5a71fa2563959674ee155886ca5d2e2f31d77e0af1fa27cf73c3" +
//                "749c47ab18501ddae2757e4f7401905acafaaae3e4d59b349adf6acebd10190d" +
//                "fe4890d1e6188d0b046df344706c631e"
//            }
//        };
//        
//        System.out.println("keyExpansion");
//        
//        for (String[] data : keyExpansionDataTest) {
//            byte[] key = HelperFunctions.hexStringToByteArray(data[0]);
//            byte[] expW = HelperFunctions.hexStringToByteArray(data[1]);
//            AES instance = new AES(key);
//            instance.keyExpansion();
//            Assert.assertArrayEquals(expW, instance.getW());
//        }
//    }
    
//
//    /**
//     * Test of addRoundKey method, of class AES.
//     */
//    @Test
//    public void testAddRoundKey() {
//        
//        char[][][] mixColumnsDataTest = new char[][][]{
//            {
//                {
//                    0x04, 0x66, 0x81, 0xe5,
//                    0xe0, 0xcb, 0x19, 0x9a,
//                    0x48, 0xf8, 0xd3, 0x7a,
//                    0x28, 0x06, 0x26, 0x4c,
//                }, { 
//                    0xa4, 0x9c, 0x7f, 0xf2,
//                    0x68, 0x9f, 0x35, 0x2b,
//                    0x6b, 0x5b, 0xea, 0x43,
//                    0x02, 0x6a, 0x50, 0x49,
//                }, {
//                    0x01
//                }
//            },
//            {
//                {
//                    0x58, 0x4d, 0xca, 0xf1,
//                    0x1b, 0x4b, 0x5a, 0xac,
//                    0xdb, 0xe7, 0xca, 0xa8,
//                    0x1b, 0x6b, 0xb0, 0xe5,
//                }, { 
//                    0xaa, 0x8f, 0x5f, 0x03,
//                    0x61, 0xdd, 0xe3, 0xef,
//                    0x82, 0xd2, 0x4a, 0xd2,
//                    0x68, 0x32, 0x46, 0x9a,
//                }, {
//                    0x02
//                }
//            }
//        };
//        
//        System.out.println("addRoundKey");
//        
//        String keyString = "2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c";
//        byte[] key = HelperFunctions.hexStringToByteArray(keyString);
//        
//        for( char[][] data : mixColumnsDataTest ) {
//            
//            byte[] input = charArrayToByteArray( data[0] );
//            
//            AES instance = new AES( key );
//            instance.init(input);
//            System.out.println(instance.affiche());
//            instance.addRoundKey(data[2][0]);
//            
//            byte[] expResult = charArrayToByteArray( data[1] );
//            byte[] result = instance.getState();
//            System.out.println(instance.affiche());
//            Assert.assertArrayEquals( expResult, result );
//        }
//    }
    
}
