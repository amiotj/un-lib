
package science.unlicense.impl.io.ur;

import science.unlicense.impl.io.ur.URLUtilities;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class URLUtilitiesTest {
    
    @Test
    public void urlDecode(){
        //U+0F3B TIBETAN MARK GUG RTAGS GYAS
        //U+0FBF TIBETAN KU RU KHA BZHI MIG CAN
        final Chars encoded = new Chars("%E0%BC%BB%E0%BE%BF");
        final Chars decoded = URLUtilities.decode(encoded);
        final Chars expected = new Chars(new int[]{0x0F3B,0x0FBF});
        Assert.assertEquals(expected,decoded);
    }
    
    @Test
    public void urlEncode(){
        //U+0F3B TIBETAN MARK GUG RTAGS GYAS
        //U+0FBF TIBETAN KU RU KHA BZHI MIG CAN
        final Chars base = new Chars(new int[]{0x0F3B,0x0FBF});
        final Chars encoded = URLUtilities.encode(base);
        final Chars expected = new Chars("%E0%BC%BB%E0%BE%BF");
        Assert.assertEquals(expected,encoded);
    }
    
}
