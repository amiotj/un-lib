package science.unlicense.impl.cryptography.modes;

import science.unlicense.impl.cryptography.modes.CBC;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.array.Arrays;
import science.unlicense.impl.cryptography.algorithms.AES;
import science.unlicense.impl.cryptography.paddings.InvalidPadException;
import science.unlicense.impl.cryptography.paddings.PKCS5Padding;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Bertrand COTE
 */
public class CBCTest {
    
    // ========== Helper function ==========
    
    public static byte[] hexStringToByteArray( String hexString ) {
        String hexa = hexString.replaceAll("\\s", "");
        byte[] byteArray = new byte[hexa.length()/2];
        for( int i=0; i<byteArray.length; i++ ) {
            String hex = hexa.substring(2*i, 2*i+2);
            int valInt = Integer.parseInt(hex, 16);
            byteArray[i] = (byte)valInt;
        }
        return byteArray;
    }
    
    // ======================================
    
    private static String[][] CBCDataTest = new String[][]{
        {   // key
            "140b41b22a29beb4061bda66b6747e14",
            // plain text
            "4ca00ff4c898d61e1edbf1800618fb28" + // IV
            "426173696320434243206d6f64652065" + // plain text: block 1
            "6e6372797074696f6e206e6565647320" + // plain text: block 2
            "70616464696e672e",                  // plain text: block 3
            // ciper text
            "4ca00ff4c898d61e1edbf1800618fb28" + // IV
            "28a226d160dad07883d04e008a7897ee" + // cipher text: block 1
            "2e4b7465d5290d0c0e6c6822236e1daa" + // cipher text: block 2
            "fb94ffe0c5da05d9476be028ad7c1d81"   // cipher text: block 3
        }, 
        {   // key
            "140b41b22a29beb4061bda66b6747e14",
            // plain text
            "5b68629feb8606f9a6667670b75b38a5" + // IV
            "4f757220696d706c656d656e74617469" + // plain text: block 1
            "6f6e20757365732072616e642e204956",  // plain text: block 2
            // ciper text
            "5b68629feb8606f9a6667670b75b38a5" + // IV
            "b4832d0f26e1ab7da33249de7d4afc48" + // cipher text: block 1
            "e713ac646ace36e872ad5fb8a512428a" + // cipher text: block 2
            "6e21364b0c374df45503473c5242a253"   // cipher text: block 3
        }
    };
    
    public CBCTest() {
    }

    /**
     * Test of cipher method, of class CBC.
     */
    @Test
    public void testCipher_byteArr() {
        System.out.println("cipher");
        for( String[] data : CBCDataTest ) {
            byte[] key = hexStringToByteArray(data[0]);
            byte[] pt = hexStringToByteArray(data[1]);
            CBC instance = new CBC(new AES(key), new PKCS5Padding(16) );
            byte[] expResult = hexStringToByteArray(data[2]);
            byte[] result = instance.cipher(pt);
            Assert.assertArrayEquals(expResult, result);
        }
    }

    /**
     * Test of invCipher method, of class CBC.
     */
    @Test
    public void testInvCipher_byteArr() {
        System.out.println("invCipher");
        for (String[] data : CBCDataTest) {
            byte[] key = hexStringToByteArray(data[0]);
            byte[] ct = hexStringToByteArray(data[2]);
            CBC instance = new CBC(new AES(key), new PKCS5Padding(16));
            byte[] expResult = hexStringToByteArray(data[1]);
            byte[] result = null;
            try {
                result = instance.invCipher(ct);
            } catch (InvalidPadException ipe) {
                // TODO
            }
            expResult = Arrays.copy(expResult, 16, expResult.length-16);
            Assert.assertArrayEquals(expResult, result);
        }
    }
    
    /**
     * Test of cipher method, of class CBC.
     */
    @Test
    public void testCipher_ByteInputStream_ByteOutputStream() {
        
        System.out.println("cipher");
        for( String[] data : CBCDataTest ) {
            
            byte[] key = hexStringToByteArray(data[0]);
            byte[] pt = hexStringToByteArray(data[1]);
            byte[] expResult = hexStringToByteArray(data[2]);
            CBC instance = new CBC(new AES(key), new PKCS5Padding(16) );
            
            ByteInputStream input = new ArrayInputStream(pt);
            ArrayOutputStream output = new ArrayOutputStream();
            
            try {
                instance.cipher(input, output);
                
            } catch (IOException ioe) {
                // TODO
            } finally {
                try {
                    input.close();
                    output.close();
                } catch (IOException ioe) {
                    // TODO
                }
            }
            
            byte[] result = output.getBuffer().toArrayByte();
            Assert.assertArrayEquals(expResult, result);
            
        }
    }
    
    /**
     * Test of invCipher method, of class CBC.
     */
    @Test
    public void testInvCipher_ByteInputStream_ByteOutputStream() {
        System.out.println("invCipher");
        for (String[] data : CBCDataTest) {
            
            byte[] key = hexStringToByteArray(data[0]);
            byte[] ct = hexStringToByteArray(data[2]);
            CBC instance = new CBC(new AES(key), new PKCS5Padding(16));
            byte[] expResult = hexStringToByteArray(data[1]);
            expResult = Arrays.copy(expResult, 16, expResult.length-16);
            
            ByteInputStream input = new ArrayInputStream(ct);
            ArrayOutputStream output = new ArrayOutputStream();
            
            byte[] result = null;
            try {
                instance.invCipher(input, output);
                result = output.getBuffer().toArrayByte();
                
            } catch (IOException ioe) {
                // TODO
            }catch (InvalidPadException ipe) {
                // TODO
            } finally {
                try {
                    input.close();
                    output.close();
                } catch (IOException ioe) {
                    // TODO
                }
            }
            
            Assert.assertArrayEquals(expResult, result);
        }
    }
    
}
