
package science.unlicense.impl.io.base64;

import science.unlicense.impl.io.base64.Base64;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Base64Test {

    @Test
    public void base64ReadWriteTest() throws IOException {
        final Chars base64 = new Chars("aGVsbG8gd29ybGQsIHRvZGF5IGlzIDIwIHNlcHRlbWJlciAxOTY4Lg==");
        final byte[] text = "hello world, today is 20 september 1968.".getBytes();

        Assert.assertArrayEquals(text, Base64.decode(base64));
        Assert.assertEquals(base64, Base64.encodeBytes(text));

    }

}
