package science.unlicense.impl.cryptography.hash;

import science.unlicense.impl.cryptography.hash.Adler32;
import science.unlicense.impl.cryptography.hash.CRC32;
import java.io.UnsupportedEncodingException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test checksum algorithms.
 *
 * @author Johann Sorel
 */
public class ChecksumTest {

    @Test
    public void CRC32Test() throws UnsupportedEncodingException {

        final String str = "hello world this is the UN project";
        final byte[] bytes = str.getBytes("US-ASCII");

        CRC32 checksum = new CRC32();
        checksum.update(bytes);
        long value = checksum.getResultLong() & 0xffffffffL;
        Assert.assertEquals(2185167914l, value);
    }
    
    @Test
    public void Adler32Test() throws UnsupportedEncodingException {

        final String str = "hello world this is the UN project";
        final byte[] bytes = str.getBytes("US-ASCII");

        Adler32 checksum = new Adler32();
        checksum.update(bytes);
        long value = checksum.getResultLong() & 0xffffffffL;
        Assert.assertEquals(3683650668l, value);
    }
    
}
