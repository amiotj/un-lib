package science.unlicense.impl.cryptography.paddings;

import science.unlicense.impl.cryptography.paddings.InvalidPadException;
import science.unlicense.impl.cryptography.paddings.PKCS5Padding;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Bertrand COTE
 */
public class PKCS5PaddingTest {

    static byte[][][] PKCS5DataTest = new byte[][][]{
        // ========== NO Exceptions thrown =====================================
        { //{ blockLength, Unpad Exception } ( with Unpad Exception =0x00=false and 0x01=true )
            // blockLength
            {(byte) 0x08, (byte)0x00},
            // raw text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01,},
            // padded text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01, (byte) 0x04, (byte) 0x04, (byte) 0x04, (byte) 0x04,}
        },
        { //{ blockLength, Unpad Exception } ( 0x00=false and 0x01=true )
            {(byte) 0x02, (byte)0x00},
            // raw text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01,},
            // padded text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01, (byte) 0x02, (byte) 0x02,}
        },
        { //{ blockLength, Unpad Exception } ( 0x00=false and 0x01=true )
            {(byte) 0x02, (byte)0x00},
            // raw text
            {},
            // padded text
            {(byte) 0x02, (byte) 0x02,}
        },
        // ========== Exceptions thrown ========================================
        { //{ blockLength, Unpad Exception } ( 0x00=false and 0x01=true )
            {(byte) 0x08, (byte)0x01},
            // raw text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01,},
            // padded text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01, (byte) 0x04, (byte) 0x03, (byte) 0x04, (byte) 0x04,}
        },
        { //{ blockLength, Unpad Exception } ( 0x00=false and 0x01=true )
            {(byte) 0x02, (byte)0x01},
            // raw text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01,},
            // padded text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01, (byte) 0x03, (byte) 0x02,}
        },
        { //{ blockLength, Unpad Exception } ( 0x00=false and 0x01=true )
            {(byte) 0x02, (byte)0x01},
            // raw text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01,},
            // padded text
            {(byte) 0x04, (byte) 0x03, (byte) 0x02, (byte) 0x01, (byte) 0x03, (byte) 0x03,}
        }
    };

    /**
     * Test of pad method, of class PKCS5Padding.
     */
    @Test
    public void testPad() {
        System.out.println("padd");
        for (byte[][] data : PKCS5DataTest) {
            boolean exceptionToBeTrowned = (data[0][1]==0x01);
            
            if( !exceptionToBeTrowned ) {
                byte[] text = data[1];
                int blockLength = data[0][0];
                byte[] expResult = data[2];
                
                PKCS5Padding instance = new PKCS5Padding(blockLength);
                byte[] result = instance.pad(text);
                Assert.assertArrayEquals(expResult, result);
            }
        }
    }

    /**
     * Test of unPad method, of class PKCS5Padding.
     */
    @Test
    public void testUnPad() {
        System.out.println("unPadd");
        
        // tests without exceptions throwned:
        for (byte[][] data : PKCS5DataTest) {
            byte[] text = data[2];
            int blockLength = data[0][0];
            byte[] expResult = data[1];
            boolean exceptionToBeTrowned = (data[0][1]==0x01);

            PKCS5Padding instance = new PKCS5Padding(blockLength);
            byte[] result = null;
            try {
                result = instance.unPad(text);
            } catch (InvalidPadException ipe) {
                //Logger.getLogger(PKCS5PaddingTest.class.getName()).log(Level.SEVERE, null, ipe);
                Assert.assertTrue(exceptionToBeTrowned);
            }
            if( !exceptionToBeTrowned ) {
                Assert.assertArrayEquals(expResult, result);
            }
        }
    }

}
