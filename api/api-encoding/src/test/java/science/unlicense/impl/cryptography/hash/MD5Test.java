

package science.unlicense.impl.cryptography.hash;

import science.unlicense.impl.cryptography.hash.MD5;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

/**
 * MD5 Test cases.
 * 
 * @author Johann Sorel
 */
public class MD5Test {
    
    @Test
    public void test(){
        
        final MD5 md5 = new MD5();
        md5.update(new Chars("The quick brown fox jumps over the lazy dog").toBytes());
        Assert.assertEquals(new Chars("9E107D9D372BB6826BD81D3542A419D6"), Int32.encodeHexa(md5.getResultBytes()));
        
        md5.reset();
        md5.update(new Chars("The quick brown fox jumps over the lazy dog.").toBytes());
        Assert.assertEquals(new Chars("E4D909C290D0FB1CA068FFADDF22CBD0"), Int32.encodeHexa(md5.getResultBytes()));
        
        md5.reset();
        md5.update(Chars.EMPTY.toBytes());
        Assert.assertEquals(new Chars("D41D8CD98F00B204E9800998ECF8427E"), Int32.encodeHexa(md5.getResultBytes()));
    }
    
}
