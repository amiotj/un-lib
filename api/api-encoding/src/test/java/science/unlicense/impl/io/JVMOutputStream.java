
package science.unlicense.impl.io;

import science.unlicense.api.io.IOException;
import java.io.OutputStream;
import science.unlicense.api.io.AbstractOutputStream;

/**
 *
 * @author Johann Sorel
 */
public class JVMOutputStream extends AbstractOutputStream{

    private final OutputStream out;

    public JVMOutputStream(OutputStream out) {
        this.out = out;
    }

    @Override
    public void write(byte b) throws IOException {
        try {
            out.write(b);
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    @Override
    public void write(byte[] buffer) throws IOException {
        try {
            out.write(buffer);
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        try {
            out.write(buffer,offset,length);
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    @Override
    public void flush() throws IOException {
        try {
            out.flush();
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            out.close();
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

}
