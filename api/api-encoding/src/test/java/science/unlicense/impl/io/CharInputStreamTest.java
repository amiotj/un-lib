package science.unlicense.impl.io;

import science.unlicense.api.io.IOException;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.ArrayInputStream;
import org.junit.Test;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;

import org.junit.Assert;

/**
 * @author Johann Sorel
 */
public class CharInputStreamTest {

    @Test
    public void readCharTest() throws IOException{

        final byte[] data = new byte[]{1,2,3,4,5};
        final ArrayInputStream sub = new ArrayInputStream(data);
        final CharInputStream stream = new CharInputStream(sub, CharEncodings.US_ASCII);

        Char c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{1},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII, c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{2},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{3},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{4},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());
        c = stream.readChar();
        Assert.assertArrayEquals(new byte[]{5},c.toBytes());
        Assert.assertEquals(CharEncodings.US_ASCII,c.getEncoding());

        c = stream.readChar();
        Assert.assertNull(c);

        //should not raise any error
        stream.close();

    }

}
