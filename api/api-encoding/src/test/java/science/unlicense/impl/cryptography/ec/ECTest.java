package science.unlicense.impl.cryptography.ec;

import science.unlicense.impl.cryptography.ec.ECCrypt;
import science.unlicense.impl.cryptography.ec.ECPoint;
import java.math.BigInteger;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import static science.unlicense.impl.cryptography.ec.ECField.*;

/**
 * Test the functionality and performance of the Elliptic Curve classes.
 */
public class ECTest {

    static long clock() {
        return System.currentTimeMillis();
    }

    static void printf(String fmt, int i) {
        System.out.printf(fmt, i);
    }

    static void printf(String fmt, float f) {
        System.out.printf(fmt, f);
    }

    private final Random rand = new Random();
    private ECCrypt ec;
    private BigInteger priv;
    private BigInteger pub;
    private BigInteger secret;
    private static boolean debug = false;

    @Before
    public void beforeTest() {
        ECPoint cp = new ECPoint();
        cp.random(rand);
        ec = new ECCrypt(cp);
        priv = new BigInteger(255, rand);
        pub = ec.makePublicKey(priv);
        secret = new BigInteger(255, rand);	// random secret
    }

    @Test
    public void testKeyExchange() {
        if (debug) {
            System.err.println("Secret = " + secret.toString(36));
            System.err.println("Public key = " + pub.toString(36));
        }
        ECCrypt.Pair p = ec.encodeSecret(pub, secret);
        if (debug) {
            System.err.println("Public msg = " + p.r.toString(36));
            System.err.println("Shared secret = " + p.s.toString(36));
        }
        BigInteger s = ec.decodeSecret(priv, p.r);
        if (debug) {
            System.err.println("Recovered secret = " + s.toString(36));
            System.err.print("Key exchange test: ");
        }
        Assert.assertTrue(s.compareTo(p.s) == 0);
    }

    @Test
    public void testSign() {
        BigInteger mac = new BigInteger(128, rand);
        if (debug) {
            System.err.println("MAC = " + mac.toString(36));
        }
        ECCrypt.Pair sig = ec.sign(priv, secret, mac);
        if (debug) {
            System.err.println("sig.r = " + sig.r.toString(36));
            System.err.println("sig.s = " + sig.s.toString(36));
            System.err.print("DSA pos test: ");
        }
        Assert.assertTrue(ec.verify(pub, mac, sig));
        if (debug) {
            System.err.print("DSA neg test: ");
        }
        sig.r = sig.r.add(BigInteger.valueOf(1));
        Assert.assertTrue(!ec.verify(pub, mac, sig));
    }

    static int test_count = 100;

    /**
     * slowly evaluates to the trace of p
     */
    private static boolean gfSlowTrace(char[] p) {
        final char[] t = gfNew();

        gfCopy(t, p);
        for (int i = 1; i < GF_M; i++) {
            gfSquare(t, t);
            gfAdd(t, t, p);
        }
        return (t[0] != 0);
    }

    @Test
    public void testgf() {
        int afail = 0, mfail = 0, dfail = 0, sfail = 0, ifail = 0, rfail = 0,
                tfail = 0, qfail = 0;
        char[] f = gfNew(), g = gfNew(), h = gfNew(), x = gfNew(),
                y = gfNew(), z = gfNew();
        long elapsed, quad = 0L;

        //printf("Executing %d field self tests...", test_count);
        elapsed = -clock();
        for (int i = 0; i < test_count; i++) {
            gfRandom(f, rand);
            gfRandom(g, rand);
            gfRandom(h, rand);

            /* addition test: f+g = g+f */
            gfAdd(x, f, g);
            gfAdd(y, g, f);
            if (!gfEqual(x, y)) {
                afail++;
                /* printf ("Addition test #%d failed!\n", i); */
            }
            /* multiplication test: f*g = g*f */
            gfMultiply(x, f, g);
            gfMultiply(y, g, f);
            if (!gfEqual(x, y)) {
                mfail++;
                /* printf ("Multiplication test #%d failed!\n", i); */
            }
            /* distribution test: f*(g+h) = f*g + f*h */
            gfMultiply(x, f, g);
            gfMultiply(y, f, h);
            gfAdd(y, x, y);
            gfAdd(z, g, h);
            gfMultiply(x, f, z);
            if (!gfEqual(x, y)) {
                dfail++;
                /* printf ("Distribution test #%d failed!\n", i); */
            }
            /* squaring test: f^2 = f*f */
            gfSquare(x, f);
            gfMultiply(y, f, f);
            if (!gfEqual(x, y)) {
                sfail++;
                /* printf ("Squaring test #%d failed:\n", i); */
            }
            /* inversion test: g*(f/g) = f */
            if (g[0] != 0) {
                gfInvert(x, g);
                gfMultiply(y, f, x);
                gfMultiply(x, g, y);
                if (!gfEqual(x, f)) {
                    ifail++;
                    /* printf ("Inversion test #%d failed!\n", i); */
                }
            }
            /* square root test: sqrt(b)^2 = b */
            char b = (char) (rand.nextInt() & TOGGLE);
            if (b != 0) {
                z[0] = 1;
                z[1] = b;
            } else {
                z[0] = 0;
            }
            gfSquareRoot(y, b);
            gfSquare(x, y);
            if (!gfEqual(x, z)) {
                rfail++;
                /* printf ("Square root test #%d failed!\n", i); */
            }
            /* trace test: slow tr(f) = tr(f) */
            if (gfTrace(f) != gfSlowTrace(f)) {
                tfail++;
                /* printf ("Trace test #%d failed!\n", i); */
            }
            /* quadratic equation solution test: x^2 + x = f (where tr(f) = 0)*/
            if (!gfTrace(f)) {
                quad -= clock();
                gfQuadSolve(x, f);
                quad += clock();
                gfSquare(y, x);
                gfAdd(y, y, x);
                if (!gfEqual(y, f)) {
                    qfail++;
                    /* printf ("Quadratic equation test #%d failed!\n", i); */
                }
            }

        }
        elapsed += clock();
        //printf(" done, elapsed time = %.1f s.\n", (float)elapsed/1000);
        if (quad > 0) //printf("Quad elapsed time = %.1f s.\n", (float)quad/1000);
        {
            if (afail != 0) {
                printf("---> %d additions failed <---\n", afail);
            }
        }
        if (mfail != 0) {
            printf("---> %d multiplications failed <---\n", mfail);
        }
        if (dfail != 0) {
            printf("---> %d distributions failed <---\n", dfail);
        }
        if (sfail != 0) {
            printf("---> %d squarings failed <---\n", sfail);
        }
        if (ifail != 0) {
            printf("---> %d inversions failed <---\n", ifail);
        }
        if (rfail != 0) {
            printf("---> %d square roots failed <---\n", rfail);
        }
        if (tfail != 0) {
            printf("---> %d traces failed <---\n", tfail);
        }
        if (qfail != 0) {
            printf("---> %d quadratic equations failed <---\n", qfail);
        }
    } /* gfSelfTest */


    /**
     * perform test_count self tests
     */
    @Test
    public void testArith() {
        int nfail = 0, afail = 0, sfail = 0,
                cfail = 0, qfail = 0, pfail = 0, yfail = 0;
        ECPoint f = new ECPoint(), g = new ECPoint(),
                x = new ECPoint(), y = new ECPoint();
        long elapsed = 0L;

        //printf ("Executing %d curve self tests...", test_count);
        for (int i = 0; i < test_count; i++) {
            f.random(rand);
            g.random(rand);

            /* negation test: -(-f) = f */
            x.copy(f);
            x.negate();
            x.negate();
            if (!x.equals(f)) {
                nfail++;
                /* printf ("Addition test #%d failed!\n", i); */
            }

            /* addition test: f+g = g+f */
            x.copy(f);
            x.add(g);
            y.copy(g);
            y.add(f);
            if (!x.equals(y)) {
                afail++;
                /* printf ("Addition test #%d failed!\n", i); */
            }

            /* subtraction test: f-g = f+(-g) */
            x.copy(f);
            x.sub(g);
            y.copy(g);
            y.negate();
            y.add(f);
            if (!x.equals(y)) {
                sfail++;
                /* printf ("Subtraction test #%d failed!\n", i); */
            }

            /* quadruplication test: 2*(2*f) = f + f + f + f */
            x.copy(f);
            x.dbl();
            x.dbl();
            y.clear();
            y.add(f);
            y.add(f);
            y.add(f);
            y.add(f);
            if (!x.equals(y)) {
                qfail++;
                /* printf ("Quadruplication test #%d failed!\n", i); */
            }

            /* scalar multiplication commutativity test: m*(n*f) = n*(m*f) */
            x.copy(f);
            y.copy(f);
            BigInteger m = new BigInteger(GF_M, rand);
            BigInteger n = new BigInteger(GF_M, rand);
      //x.unpack(BigInteger.valueOf(9229921186L));
            //y.copy(x);
            //BigInteger m = BigInteger.valueOf(6119085717L);
            //BigInteger n = BigInteger.valueOf(7015983782L);
            elapsed -= clock();
            x.mult(n);
            x.mult(m);
            y.mult(m);
            y.mult(n);
            elapsed += clock();
            if (!x.equals(y)) {
                cfail++;
                System.out.println("f = " + f.pack());
                System.out.println("m = " + m);
                System.out.println("n = " + n);
                /* printf ("Commutativity test #%d failed!\n", i); */
            }

            /* y calculation test: */
            boolean yb = f.Ybit();
            x.clear();
            x.copyX(f);
            x.calcY(yb);
            if (!f.equals(x)) {
                yfail++;
                /* printf ("Y calculation test #%d failed!\n", i); */
            }

            /* packing test: unpack (pack (f)) = f */
            BigInteger p = f.pack();
            x.unpack(p);
            if (!f.equals(x)) {
                pfail++;
                /* printf ("Packing test #%d failed!\n", i); */
            }
        }
        /* printf (" done, scalar multiplication time: %.3f s/op.\n",
         (float)elapsed/1000/((test_count != 0)?4*test_count:4)); */
        if (nfail != 0) {
            printf("---> %d negations failed <---\n", nfail);
        }
        if (afail != 0) {
            printf("---> %d additions failed <---\n", afail);
        }
        if (sfail != 0) {
            printf("---> %d subtractions failed <---\n", sfail);
        }
        if (qfail != 0) {
            printf("---> %d quadruplications failed <---\n", qfail);
        }
        if (cfail != 0) {
            printf("---> %d commutativities failed <---\n", cfail);
        }
        if (yfail != 0) {
            printf("---> %d y calculations failed <---\n", yfail);
        }
        if (pfail != 0) {
            printf("---> %d packings failed <---\n", pfail);
        }
        Assert.assertFalse(nfail != 0 || afail != 0 || sfail != 0 || qfail != 0
                || cfail != 0 || yfail != 0 || pfail != 0);
    } /* ecSelfTest */

}
