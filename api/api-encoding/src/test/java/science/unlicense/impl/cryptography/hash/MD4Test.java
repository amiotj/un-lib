package science.unlicense.impl.cryptography.hash;

import science.unlicense.impl.cryptography.hash.MD4;
import org.junit.Assert;

import org.junit.Test;

import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;

public class MD4Test {

    @Test
    public void test() {
        MD4 md4 = new MD4();
        
        // "abc"
        String z = "abc";
        md4.reset();
        md4.update((byte) 'a');
        md4.update((byte) 'b');
        md4.update((byte) 'c');
        Assert.assertEquals(new Chars("A448017AAF21D8525FC10AE87AA6729D"), Int32.encodeHexa(md4.getResultBytes()));
               
        //  "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        z = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
        md4.reset();
        md4.update(z.getBytes());
        Assert.assertEquals(new Chars("4691A9EC81B1A6BD1AB8557240B245C5"), Int32.encodeHexa(md4.getResultBytes()));

        // A million repetitions of "a"
        md4.reset();
        for (int i = 0; i < 1000000; i++) {
            md4.update((byte) 'a');
        }
        Assert.assertEquals(new Chars("BBCE80CC6BB65E5C6745E30D4EECA9A4"), Int32.encodeHexa(md4.getResultBytes()));
    }

}
