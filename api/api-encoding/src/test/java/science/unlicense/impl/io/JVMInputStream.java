package science.unlicense.impl.io;

import science.unlicense.api.io.IOException;
import java.io.InputStream;
import science.unlicense.api.io.AbstractInputStream;

/**
 * Wraps a JVM input stream as a ByteInputStream.
 *
 * @author Johann Sorel
 */
public class JVMInputStream extends AbstractInputStream {

    private final InputStream in;

    public JVMInputStream(InputStream in){
        this.in = in;
    }

    public int read() throws IOException {
        try {
            return in.read();
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    @Override
    public int read(byte[] buffer) throws IOException {
        try {
            return in.read(buffer);
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        try {
            return in.read(buffer,offset,length);
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    public long skip(long n) throws IOException {
        try {
            return in.skip(n);
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

    public void close() throws IOException {
        try {
            in.close();
        } catch (java.io.IOException ex) {
            throw new IOException(ex.getMessage(),ex);
        }
    }

}
