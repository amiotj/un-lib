
package science.unlicense.impl.io.base85;

import science.unlicense.impl.io.base85.Ascii85;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class Ascii85Test {

    @Test
    public void encodeTest(){
        final Chars base85 = new Chars("<~87cURD]iJ0@qfX\"GAhM<A0C~>");
        final byte[] text = "Hello ascii world!".getBytes();

        //assertArrayEquals(text, Base85.decode(base85));
        Assert.assertEquals(base85, Ascii85.encodeBytes(text));


    }

    @Test
    public void decodeTest(){
        final Chars base85 = new Chars("<~87cURD]iJ0@qfX\"GAhM<A0C~>");
        final byte[] text = "Hello ascii world!".getBytes();
        
        final byte[] res = Ascii85.decode(base85);
        Assert.assertArrayEquals(text, res);

    }


}
