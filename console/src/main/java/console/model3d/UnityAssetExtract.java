

package console.model3d;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.task.AbstractTaskDescriptor;
import science.unlicense.api.task.Task;
import science.unlicense.api.task.TaskDescriptor;
import science.unlicense.api.path.Path;
import science.unlicense.impl.model3d.unity.model.asset.UnityAsset;
import science.unlicense.impl.model3d.unity.model.asset.UnityAssetObject;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class UnityAssetExtract extends AbstractTask{

    public static final TaskDescriptor DESCRIPTOR = new Descriptor();
    
    public UnityAssetExtract() {
        super(DESCRIPTOR);
    }

    
    public Document execute() {
        final Chars input = (Chars) inputParameters.getFieldValue(new Chars("input"));
        Chars output = (Chars) inputParameters.getFieldValue(new Chars("output"));
                
                
        final Path inPath = Paths.resolve(input);
        final Path outPath;
        if(output==null){
            final Chars inName = new Chars(inPath.getName());
            final int index = new Chars(inPath.getName()).getFirstOccurence('.');
            final Chars name;
            if(index>=0){
                name = inName.truncate(0, index).concat(new Chars("_extract"));
            }else{
                name = inName.concat(new Chars("_extract"));
            }
            outPath = inPath.getParent().resolve(name);
        }else{
            outPath = Paths.resolve(output);
        }
        
        final UnityAsset asset = new UnityAsset(inPath);
        try{
            outPath.createContainer();
                    
            asset.read();
            System.out.println("Unity version : "+asset.unityVersion);
            final Iterator ite = asset.getIterator();
            while(ite.hasNext()){
                UnityAssetObject obj = (UnityAssetObject) ite.next();
                final String name = obj.ref.classID+"_"+obj.ref.offset+"_"+obj.ref.length+".bin";
                System.out.println(" > extract : "+name);
                obj.load();
                Path outFile = outPath.resolve(new Chars(name));
                ByteOutputStream outStream = outFile.createOutputStream();
                outStream.write(obj.objectBytes);
                outStream.flush();
                outStream.close();
            }
            
        }catch(Exception ex){
            throw new RuntimeException(ex);
        }
        
        return outputParameters;
    }
    
    private static class Descriptor extends AbstractTaskDescriptor{

    private Descriptor() {
            super(new Chars("console.model3d.unityassetextract"), new Chars("Extract content of a Unity asset file"), Chars.EMPTY,
                    new DefaultDocumentType(new Chars("input"), null,null,false,new FieldType[]{
                        new FieldTypeBuilder(new Chars("input")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("output")).valueClass(Chars.class).build()
                    },null),
                    new DefaultDocumentType(new Chars("output"),null,null,false,null,null));
        }

        public Task create() {
            return new UnityAssetExtract();
        }
    }
    
}
