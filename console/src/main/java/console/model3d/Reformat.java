

package console.model3d;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model3d.Model3DFormat;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.model3d.Model3Ds;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.task.AbstractTaskDescriptor;
import science.unlicense.api.task.Task;
import science.unlicense.api.task.TaskDescriptor;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class Reformat extends AbstractTask{

    public static final TaskDescriptor DESCRIPTOR = new Descriptor();
    
    public Reformat() {
        super(DESCRIPTOR);
    }

    
    public Document execute() {
        final Chars input = (Chars) inputParameters.getFieldValue(new Chars("input"));
        final Chars format = (Chars) inputParameters.getFieldValue(new Chars("format"));
        Chars output = (Chars) inputParameters.getFieldValue(new Chars("output"));
        
        final Model3DFormat modelformat = Model3Ds.getFormat(format);
        
        if(output==null){
            final int index =input.getLastOccurence('.');
        }
                
        final Path inPath = Paths.resolve(input);
        final Path outPath;
        if(output==null){
            final Chars inName = new Chars(inPath.getName());
            final int index = new Chars(inPath.getName()).getFirstOccurence('.');
            final Chars name;
            if(index>=0){
                name = inName.truncate(0, index).concat('.').concat(modelformat.getExtensions()[0]);
            }else{
                name = inName.concat('.').concat(modelformat.getExtensions()[0]);
            }
            outPath = inPath.getParent().resolve(name);
        }else{
            outPath = Paths.resolve(output);
        }
        
        try{
            final Model3DStore inStore = Model3Ds.read(inPath);
            final Model3DStore outStore = modelformat.open(outPath);
            outStore.writeElements(inStore.getElements());
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(),ex);
        }catch(StoreException ex){
            throw new RuntimeException(ex.getMessage(),ex);
        }
        
        return outputParameters;
    }
    
    private static class Descriptor extends AbstractTaskDescriptor{

    private Descriptor() {
            super(new Chars("console.model3d.reformat"), new Chars("Reformat model 3d"), Chars.EMPTY,
                    new DefaultDocumentType(new Chars("input"), null,null,false,new FieldType[]{
                        new FieldTypeBuilder(new Chars("input")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("format")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("output")).valueClass(Chars.class).build()
                    },null),
                    new DefaultDocumentType(new Chars("output"),null,null,false,null,null));
        }

        public Task create() {
            return new Reformat();
        }
    }
    
}
