
package console.archive;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.doc.DefaultDocumentType;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.FieldType;
import science.unlicense.api.model.doc.FieldTypeBuilder;
import science.unlicense.api.path.Path;
import science.unlicense.api.task.AbstractTask;
import science.unlicense.api.task.AbstractTaskDescriptor;
import science.unlicense.api.task.Task;
import science.unlicense.api.task.TaskDescriptor;
import science.unlicense.api.archive.ArchiveFormat;
import science.unlicense.api.archive.Archives;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class Extract extends AbstractTask{

    public static final TaskDescriptor DESCRIPTOR = new Descriptor();
    
    public Extract() {
        super(DESCRIPTOR);
    }

    public Document execute() {
        final Chars input = (Chars) inputParameters.getFieldValue(new Chars("input"));
        final Chars encoding = (Chars) inputParameters.getFieldValue(new Chars("encoding"));
        final Chars output = (Chars) inputParameters.getFieldValue(new Chars("output"));
        
        final Path inPath = Paths.resolve(input);
        
        final Path outPath;
        if(output==null){
            outPath = inPath.getParent().resolve( Paths.stripExtension(new Chars(inPath.getName())));
        }else{
            outPath = Paths.resolve(input);
        }
        
        final Dictionary params = new HashDictionary();
        if(encoding!=null){
            params.add(ArchiveFormat.PARAM_ENCODING.getId(), CharEncodings.find(encoding));
        }
        
        try {
            Archives.extract(inPath, outPath, params);
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
        return outputParameters;
    }

    private static class Descriptor extends AbstractTaskDescriptor{

    private Descriptor() {
            super(new Chars("console.archive.extract"), new Chars("Extract archive content"), Chars.EMPTY,
                    new DefaultDocumentType(new Chars("input"), null,null,false,new FieldType[]{
                        new FieldTypeBuilder(new Chars("input")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("encoding")).valueClass(Chars.class).build(),
                        new FieldTypeBuilder(new Chars("output")).valueClass(Chars.class).build()
                    },null),
                    new DefaultDocumentType(new Chars("output"),null,null,false,null,null));
        }

        public Task create() {
            return new Extract();
        }
    }
}
