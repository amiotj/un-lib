
package science.unlicense.impl.code.gcode;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.code.gcode.model.GBlock;

/**
 *
 * @author Johann Sorel
 */
public class GCodeWriter extends AbstractWriter {

    public GCodeWriter() {
    }
    
    public void writer(GBlock block) throws IOException {
        final CharOutputStream cs = getOutputAsCharStream(CharEncodings.UTF_8);
        
        if (block.commands.length==0) {
            if(block.comment!= null){
                cs.write(';').write(' ').write(block.comment);
            }
        } else {
            
        }
        
        
    }
    
}
