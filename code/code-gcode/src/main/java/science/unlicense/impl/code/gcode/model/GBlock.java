
package science.unlicense.impl.code.gcode.model;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class GBlock {
 
    public GCommand[] commands;
    /**
     * Optional comment.
     */
    public Chars comment;
    
}
