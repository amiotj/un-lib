
package science.unlicense.impl.code.gcode;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.code.CodeFormat;
import science.unlicense.api.code.CodeProducer;
import science.unlicense.api.store.DefaultFormat;
import science.unlicense.impl.code.GrammarCodeFileReader;

/**
 * G-Code numerical command language.
 *
 * References :
 * https://en.wikipedia.org/wiki/G-code
 * http://reprap.org/wiki/G-code
 * http://wiki.solidoodle.com/gcode-guide
 *
 * @author Johann Sorel
 */
public class GFormat extends DefaultFormat implements CodeFormat {

    public static final GFormat INSTANCE = new GFormat();

    private GFormat() {
        super(new Chars("gcode"),
              new Chars("G-Code"),
              new Chars("Numerical command G-Code language"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("mpt"),
                  new Chars("mpf"),
                  new Chars("nc"),
                  new Chars("g"),
                  new Chars("gco"),
                  new Chars("gcode")
              },
              new byte[][]{});
    }

    @Override
    public CodeFileReader createReader() {
        return new GrammarCodeFileReader(null);
    }

    @Override
    public CodeProducer createProducer() {
        return null;
    }
    
}
