
package science.unlicense.impl.code.gcode;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Float64;
import science.unlicense.impl.code.gcode.model.GBlock;
import science.unlicense.impl.code.gcode.model.GCommand;

/**
 *
 * @author Johann Sorel
 */
public class GReader extends AbstractReader {

    private int lineNb = 0;
    
    public GReader() {
    }
    
    public GBlock nexBlock() throws IOException{
        
        final CharInputStream cs = getInputAsCharStream(CharEncodings.UTF_8);
        
        for(Chars line = cs.readLine();line!=null;line = cs.readLine()){
            lineNb++;
            line = line.trim();
            if(line.isEmpty()) continue;
            
            final GBlock block = new GBlock();
            if (line.startsWith(';')) {
                //comment line
                block.commands = new GCommand[0];
                block.comment = line.truncate(1, -1).trim();
            } else {
                final int length = line.getCharLength();
                final Sequence commands = new ArraySequence();
                int cursor = 0;
                for (;cursor<length;) {
                    final int c = line.getUnicode(cursor);
                    
                    if (c ==' ' || c=='\t') {
                        cursor++;
                    } else if (c == '(') {
                        //a comment between ()
                        final int end = line.getFirstOccurence(')',cursor);
                        if(end<0) {
                            throw new IOException("Unfinished comment line "+lineNb);
                        }
                        final Chars comment = line.truncate(cursor+1, end);
                        if (commands.isEmpty()) {
                            //add comment to block
                            block.comment = block.comment==null ? comment : block.comment.concat(',').concat(comment);
                        } else {
                            //add comment to command
                            final GCommand command = (GCommand) commands.get(commands.getSize()-1);
                            command.comment = command.comment==null ? comment : command.comment.concat(',').concat(comment);
                        }
                        cursor=end+1;
                    } else if (c == ';') {
                        //an end line comment
                        final Chars comment = line.truncate(cursor+1, -1).trim();
                        //add comment to block
                        block.comment = block.comment==null ? comment : block.comment.concat(',').concat(comment);
                        break;
                    } else {
                        //a command
                        int end = line.getFirstOccurence(' ',cursor);
                        if(end<0) end = line.getCharLength();
                        final GCommand command = new GCommand();
                        command.code = c;
                        command.value = Float64.decode(line, cursor+1, end);
                        commands.add(command);
                        cursor=end+1;
                    }
                }
                
                block.commands = (GCommand[]) commands.toArray(GCommand.class);
            }
            
            return block;
        }
        
        //nothing left
        return null;
    }
    
}
