
package science.unlicense.impl.code.glsl;

import science.unlicense.impl.code.glsl.GLSLReader;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GLSLReaderTest {

    @Test
    public void readEmptyProgram() throws IOException{
        
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/EmptyProgram.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        Assert.assertEquals(new Chars(
                "program\n" +
                "├─ version\n" +
                "│  └─ NUMBER[0:9,0:12]='330'\n" +
                "└─ function\n" +
                "   ├─ VOID[2:0,2:4]='void'\n" +
                "   ├─ WORD[2:5,2:9]='main'\n" +
                "   └─ statements"), 
                node.toCharsTree(10));
        
    }

    @Test
    public void readExpression() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/ExpressionProgram.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node.toCharsTree(20));

    }

    @Test
    public void readBranching() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/branching.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);

    }
    
    @Test
    public void readLayout() throws IOException{

        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/layout.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);

    }

    @Test
    public void readExample() throws IOException{
        
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
    }
        
    @Test
    public void readExample2() throws IOException{
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example2.glsl")));
        final SyntaxNode node = reader.readAST();
        System.out.println(node);
        
    }
    
    @Test
    public void readExample3() throws IOException{
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example3.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
        
    }
    
    @Test
    public void readExample4() throws IOException{
        final GLSLReader reader = new GLSLReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/language/glsl/Example4.glsl")));
        final SyntaxNode node = reader.readAST();
        node.trim(GLSLReader.TRIM);
        System.out.println(node);
        
    }
    
}
