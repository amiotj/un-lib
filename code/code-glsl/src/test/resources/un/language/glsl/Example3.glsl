#version 330
layout(location = 0) in vec2 l_position;


uniform mat3 MV;
uniform mat3 P;


out VsDataModel{
    vec4 position_model;
    vec4 position_proj;
} vsData;



void main(){
    vsData.position_model = vec4(l_position.x,l_position.y,0,1);
    vec3 test = (P*MV) * vec3(l_position.x,l_position.y,1.0);
    vsData.position_proj = vec4(test,1);
    gl_Position = vsData.position_proj;
}