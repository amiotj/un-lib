#version 330
layout(location = 0) in vec3 l_position;
layout(location = 1) in vec3 l_normal;
layout(location = 2) in vec2 l_uv;


uniform mat4 UNI_M;
uniform mat4 UNI_V;
uniform mat4 UNI_P;
uniform vec2 UNI_PX;


out VsDataModel{
    flat mat4 V;
    vec2 uv;
    vec4 normal_camera;
    vec4 normal_model;
    vec4 normal_proj;
    vec4 normal_world;
    vec4 position_camera;
    vec4 position_model;
    vec4 position_proj;
    vec4 position_world;
} vsData;

float w_position_factor = 1.0;
mat4 M;
mat4 MV;
mat4 MVP;
mat4 P;
mat4 V;
vec2 PX;
vec3 w_position_model = vec3(0,0,0);


void main(){
    M = UNI_M;
    V = UNI_V;
    P = UNI_P;
    PX = UNI_PX;
    MV = V*M;
    MVP = P*V*M;
    // morph actor may have set the position before, we don't want to override it
    w_position_model = w_position_model + l_position*w_position_factor;
    vsData.position_model = vec4(w_position_model,1);
    vsData.position_world = M * vec4(w_position_model,1);
    vsData.position_camera = MV * vec4(w_position_model,1);
    vsData.position_proj = MVP * vec4(w_position_model,1);
    vsData.normal_model = vec4(l_normal,0);
    vsData.normal_world = M * vec4(l_normal,0);
    vsData.normal_camera = MV * vec4(l_normal,0);
    vsData.normal_proj = MVP * vec4(l_normal,0);
    vsData.uv  = l_uv;
    gl_Position = MVP * vec4(w_position_model,1);
}