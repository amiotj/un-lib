
package science.unlicense.impl.code.glsl.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class GLSLShader {
    
    private Chars comment;
    private GLSLVersion version;
    private final Sequence extensions = new ArraySequence();
    private final Sequence layouts = new ArraySequence();
    private final Sequence structures = new ArraySequence();
    private final Sequence variables = new ArraySequence();
    private final Sequence functions = new ArraySequence();

    public GLSLShader() {
    }

    public void reset(){
        comment = null;
        setVersion(null);
        extensions.removeAll();
        layouts.removeAll();
        structures.removeAll();
        variables.removeAll();
        functions.removeAll();
    }
    
    public GLSLVersion getVersion() {
        return version;
    }

    public void setVersion(GLSLVersion version) {
        this.version = version;
    }

    public Chars getComment() {
        return comment;
    }

    public void setComment(Chars comment) {
        this.comment = comment;
    }
    
    public Sequence getExtensions() {
        return extensions;
    }

    public Sequence getLayouts() {
        return layouts;
    }

    public Sequence getStructures() {
        return structures;
    }

    public Sequence getVariables() {
        return variables;
    }

    public Sequence getFunctions() {
        return functions;
    }
        
}
