
package science.unlicense.impl.code.glsl;

import science.unlicense.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public class GLSLConstants {
    
    
    //TODO exaustive list
    public static final Chars KW_UNIFORM   = new Chars("uniform");
    public static final Chars KW_LOCATION  = new Chars("location");
    public static final Chars KW_LAYOUT    = new Chars("layout");
    public static final Chars KW_MAIN      = new Chars("main");
    public static final Chars KW_VAR       = new Chars("var");
    public static final Chars KW_IN        = new Chars("in");
    public static final Chars KW_OUT       = new Chars("out");
    public static final Chars KW_CONST     = new Chars("const");
    
    public static final Chars KW_PREPROC_VERSION   = new Chars("#version");    
    public static final Chars KW_PREPROC_EXTENSION = new Chars("#extension");    
    public static final Chars KW_PREPROC_LINE      = new Chars("#line");    
    
    
    public static final Chars KW_EXTENSION_ALL      = new Chars("all");
    public static final Chars KW_BEHAVIOR_ENABLE    = new Chars("enable");
    public static final Chars KW_BEHAVIOR_REQUIRE   = new Chars("require");
    public static final Chars KW_BEHAVIOR_WARN      = new Chars("warn");
    public static final Chars KW_BEHAVIOR_DISABLE   = new Chars("disable");

    //primitive types
    public static final Chars KW_BOOL           = new Chars("bool");
    public static final Chars KW_INT            = new Chars("int");
    public static final Chars KW_UINT           = new Chars("uint");
    public static final Chars KW_FLOAT          = new Chars("float");
    public static final Chars KW_DOUBLE         = new Chars("double");
    public static final Chars KW_BVEC2          = new Chars("bvec2");
    public static final Chars KW_BVEC3          = new Chars("bvec3");
    public static final Chars KW_BVEC4          = new Chars("bvec4");
    public static final Chars KW_IVEC2          = new Chars("ivec2");
    public static final Chars KW_IVEC3          = new Chars("ivec3");
    public static final Chars KW_IVEC4          = new Chars("ivec4");
    public static final Chars KW_UVEC2          = new Chars("uvec2");
    public static final Chars KW_UVEC3          = new Chars("uvec3");
    public static final Chars KW_UVEC4          = new Chars("uvec4");
    public static final Chars KW_VEC2           = new Chars("vec2");
    public static final Chars KW_VEC3           = new Chars("vec3");
    public static final Chars KW_VEC4           = new Chars("vec4");
    public static final Chars KW_DVEC2          = new Chars("dvec2");
    public static final Chars KW_DVEC3          = new Chars("dvec3");
    public static final Chars KW_DVEC4          = new Chars("dvec4");
    
    public static final Chars KW_MAT2           = new Chars("mat2");
    public static final Chars KW_MAT2x2         = new Chars("mat2x2");
    public static final Chars KW_MAT2x3         = new Chars("mat2x3");
    public static final Chars KW_MAT2x4         = new Chars("mat2x4");
    public static final Chars KW_MAT3           = new Chars("mat3");
    public static final Chars KW_MAT3x2         = new Chars("mat3x2");
    public static final Chars KW_MAT3x3         = new Chars("mat3x3");
    public static final Chars KW_MAT3x4         = new Chars("mat3x4");
    public static final Chars KW_MAT4           = new Chars("mat4");
    public static final Chars KW_MAT4x2         = new Chars("mat4x2");
    public static final Chars KW_MAT4x3         = new Chars("mat4x3");
    public static final Chars KW_MAT4x4         = new Chars("mat4x4");
    
    public static final Chars KW_SAMPLER2D      = new Chars("sampler2D");
    public static final Chars KW_ISAMPLER2D     = new Chars("isampler2D");
    public static final Chars KW_USAMPLER2D     = new Chars("usampler2D");
    public static final Chars KW_SAMPLER2DMS    = new Chars("sampler2DMS");
    public static final Chars KW_SAMPLERCUBE    = new Chars("samplerCube");
    public static final Chars KW_ISAMPLERBUFFER = new Chars("isamplerBuffer");
    public static final Chars KW_SAMPLERBUFFER  = new Chars("samplerBuffer");
    
    
    
}