
package science.unlicense.impl.code.glsl.model;

import science.unlicense.api.character.Chars;

/**
 * 
 * @author Johann Sorel
 */
public class GLSLExtension {

    private Chars comment;
    
    public Chars getComment() {
        return comment;
    }

    public void setComment(Chars comment) {
        this.comment = comment;
    }
    
}
