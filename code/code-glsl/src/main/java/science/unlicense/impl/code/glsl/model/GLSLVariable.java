
package science.unlicense.impl.code.glsl.model;

import science.unlicense.api.character.Chars;

/**
 * 
 * @author Johann Sorel
 */
public class GLSLVariable {

    private Chars comment;
    private int type;
    private Chars name;
    
    public Chars getComment() {
        return comment;
    }

    public void setComment(Chars comment) {
        this.comment = comment;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        this.name = name;
    }
    
    
    
}
