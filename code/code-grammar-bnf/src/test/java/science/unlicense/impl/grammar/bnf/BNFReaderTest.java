
package science.unlicense.impl.grammar.bnf;

import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.impl.grammar.bnf.BNFReader;

/**
 *
 * @author Johann Sorel
 */
public class BNFReaderTest {

     private static final CharEncoding ENC = CharEncodings.US_ASCII;

    @Test
    public void readTest() throws Exception {

        final String bnf = "<somerule> ::= <r1> <r2> ";
        final Chars cs = new Chars(bnf,ENC);
        final ByteInputStream in = new ArrayInputStream(cs.toBytes());


        BNFReader reader = new BNFReader();
        reader.setInput(in);
        reader.setEncoding(ENC);

        Dictionary dico = reader.read();
        final Iterator ite = dico.getPairs().createIterator();
        while(ite.hasNext()){
            Pair pair = (Pair) ite.next();
            System.out.println(pair.getValue1());
            System.out.println(pair.getValue2());
        }

    }
}
