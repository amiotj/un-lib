package science.unlicense.impl.grammar.bnf;

import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.CharEncoding;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_DOT;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_DOUBLEDOT;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_EQUAL;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_GROUP1_END;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_GROUP1_START;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_GROUP2_END;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_GROUP2_START;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_OPT_END;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_OPT_START;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_OR;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_RULENAME_END;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_BNF_RULENAME_START;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_NEWLINE;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_SPACE;
import static science.unlicense.impl.grammar.bnf.BNFConstants.CHAR_TAB;


/**
 * Strict BNF grammar reader.
 * See ABNF,EBNF,RBNF for alternatives.
 *
 * @author Johann Sorel
 */
public class BNFReader {

    //grammar bnf in given encoding.
    private byte[] space;
    private byte[] tab;
    private byte[] linejump;

    private byte[] ruleNameStart;
    private byte[] ruleNameEnd;
    private byte[] or;
    private byte[] dot;
    private byte[] doubledot;
    private byte[] equal;
    private byte[] lbracket;
    private byte[] rbracket;
    private byte[] lgroup1;
    private byte[] rgroup1;
    private byte[] lgroup2;
    private byte[] rgroup2;

    private final ByteSequence buffer = new ByteSequence();
    private Object input;
    private CharEncoding encoding;
    private boolean closeStream = false;
    private CharInputStream stream;
    private Dictionary elements = new HashDictionary();

    public BNFReader(){}

    public final void setInput(Object source) {
        this.input = source;
    }

    public void setEncoding(CharEncoding encoding) {
        this.encoding = encoding;
        this.space = encoding.toBytes(CHAR_SPACE);
        this.tab = encoding.toBytes(CHAR_TAB);
        this.linejump = encoding.toBytes(CHAR_NEWLINE);

        this.ruleNameStart = encoding.toBytes(CHAR_BNF_RULENAME_START);
        this.ruleNameEnd = encoding.toBytes(CHAR_BNF_RULENAME_END);
        this.or = encoding.toBytes(CHAR_BNF_OR);
        this.dot = encoding.toBytes(CHAR_BNF_DOT);
        this.doubledot = encoding.toBytes(CHAR_BNF_DOUBLEDOT);
        this.equal = encoding.toBytes(CHAR_BNF_EQUAL);
        this.lbracket = encoding.toBytes(CHAR_BNF_OPT_START);
        this.rbracket = encoding.toBytes(CHAR_BNF_OPT_END);
        this.lgroup1 = encoding.toBytes(CHAR_BNF_GROUP1_START);
        this.rgroup1 = encoding.toBytes(CHAR_BNF_GROUP1_END);
        this.lgroup2 = encoding.toBytes(CHAR_BNF_GROUP2_START);
        this.rgroup2 = encoding.toBytes(CHAR_BNF_GROUP2_END);
    }

    public Object getInput() {
        return input;
    }

    public CharEncoding getEncoding() {
        return encoding;
    }

    private CharInputStream getInputStream() throws IOException {
        if(stream != null){
            return stream;
        }

        if(input instanceof ByteInputStream){
            stream = new CharInputStream((ByteInputStream)input,encoding,new Char(linejump,encoding));
            //we did not create the stream, we do not close it.
            closeStream = false;
            return stream;
        }else if(input instanceof Path){
            stream = new CharInputStream(((Path)input).createInputStream(),encoding);
            closeStream = true;
            return stream;
        }else{
            throw new IOException("Unsupported input");
        }
    }

    public Dictionary read() throws IOException {
        final CharInputStream ds = getInputStream();

        //rules may be on several lines, group them on one line
        final CharBuffer cs = new CharBuffer(encoding);
        byte[] line;
        while((line=ds.readLineAsBytes())!= null){
            if(line.length == 0){
                continue;
            }

            if(cs.isEmpty()){
                cs.append(line);
            }else{
                if(Arrays.equals(tab,0,tab.length,line,0)){
                    //rule is on several lines
                    cs.append(line);
                }else{
                    //new rule
                    parseRule(cs);
                    cs.append(line);
                }
            }
        }

        if(!cs.isEmpty()){
           parseRule(cs);
        }

        return elements;
    }

    private void parseRule(CharBuffer cs) throws IOException{
        Rule rule = null;

        final CharIterator ite = cs.toChars().createIterator();
        while(ite.hasNext()){
            if(ite.nextEquals(space) || ite.nextEquals(tab) || ite.nextEquals(linejump)){
                //ignore this char
                ite.skip();
                continue;
            }

            if(rule == null){
                if(ite.nextEquals(ruleNameStart)){
                    ite.skip();
                    rule = ruleFromName(ite);

                    //we expect to find '::='
                    boolean found = false;
                    while(ite.hasNext() && !found){
                        if(ite.nextEquals(space) || ite.nextEquals(tab) || ite.nextEquals(linejump)){
                            //ignore this char
                            ite.skip();
                            continue;
                        }
                        if(ite.nextEquals(doubledot)){
                            ite.skip();
                            if(ite.nextEquals(doubledot)){
                                ite.skip();
                                if(ite.nextEquals(equal)){
                                    ite.skip();
                                    //ok
                                    found = true;
                                }
                            }
                        }
                    }
                    if(!found){
                        throw new IOException("Invalid rule definition, was expecting a '::=' for rule :"+rule.getName());
                    }
                }else{
                    throw new IOException("Invalid rule definition, was expecting a '<' character but found a :"+ite.next());
                }
            }else{
                readExpression(rule, ite, null);
            }
        }


        if(rule.getName() == null || rule.getChildren().isEmpty()){
            throw new IOException("Invalid rule definition : "+rule.getName());
        }

        cs.reset();
    }

    private void readExpression(Rule parent, CharIterator ite, byte[] end) throws IOException {
        final Sequence elements = new ArraySequence();

        boolean hasOr = false;
        while(ite.hasNext()){
            if(end != null && ite.nextEquals(end)){
                ite.skip();
                break;
            }

            if(ite.nextEquals(space) || ite.nextEquals(tab) || ite.nextEquals(linejump)){
                //ignore this char
                ite.skip();
                continue;
            }

            //rule elements
            if(ite.nextEquals(ruleNameStart)){
                ite.skip();
                final Rule sub = ruleFromName(ite);
                elements.add(new Cardinal(sub, 1, 1));
            }else if(ite.nextEquals(or)){
                //or, wait for the full expression
                ite.skip();
                elements.add(or);
                hasOr = true;
            }else if(ite.nextEquals(lbracket)){
                ite.skip();
                final Rule sub = new Rule();
                readExpression(sub, ite, rbracket);
                elements.add(new Cardinal(sub,0,1));
            }else if(ite.nextEquals(lgroup1)){
                ite.skip();
                final Rule sub = new Rule();
                readExpression(sub,ite,rgroup1);
                elements.add(new Cardinal(sub, 1,1));
            }else if(ite.nextEquals(lgroup2)){
                ite.skip();
                final Rule sub = new Rule();
                readExpression(sub,ite,rgroup2);
                elements.add(new Cardinal(sub, 1,1));
            }else if(ite.nextEquals(dot)){
                //3 dots for a 0:N repetition
                ite.skip();
                if(ite.nextEquals(dot)){
                    ite.skip();
                    if(ite.nextEquals(dot)){
                        ite.skip();
                        Cardinal c = (Cardinal) elements.get(elements.getSize()-1);
                        c.setMinOccurences(0);
                        c.setMaxOccurences(-1);
                        continue;
                    }
                }

                throw new IOException("Uncomplete repetition (...)");
            }else{
                //a terminal
                final Terminal sub = terminal(ite);
                elements.add(new Cardinal(sub, 1,1));
            }
        }

        if(hasOr){
            parent.setChoice(true);
            final Sequence choiceEle = new ArraySequence();
            for(int i=0,n=elements.getSize();i<n;i++){
                final Object obj = elements.get(i);
                if(obj == or){
                    if(choiceEle.isEmpty()){
                        throw new IOException("empty choice condition");
                    }else{
                        final Rule r = new Rule(choiceEle);
                        parent.getChildren().add(new Cardinal(r, 1, 1));
                    }
                    choiceEle.removeAll();
                }else{
                    choiceEle.add(obj);
                }
            }
            if(!choiceEle.isEmpty()){
                final Rule r = new Rule(choiceEle);
                parent.getChildren().add(new Cardinal(r, 1, 1));
                choiceEle.removeAll();
            }

        }else{
            parent.getChildren().addAll(elements);
        }

    }

    private Rule ruleFromName(CharIterator ite) throws IOException {
        buffer.removeAll();

        while(ite.hasNext()){
            if(ite.nextEquals(ruleNameEnd)){
                //ok
                ite.skip();
                final Chars name = new Chars(buffer.toArrayByte(),encoding);
                Rule r = (Rule) elements.getValue(name);
                if(r==null){
                    r = new Rule();
                    r.setName(name);
                    elements.add(name,r);
                }
                return r;
            }
            ite.nextToBuffer(buffer);
        }

        throw new IOException("Invalid rule definition, was expecting a '>'");
    }

    private Terminal terminal(CharIterator ite) throws IOException {
        buffer.removeAll();

        do{
            if(ite.nextEquals(space) || ite.nextEquals(tab) || ite.nextEquals(linejump)){
                //end of terminal
                ite.skip();
                break;
            }
            ite.nextToBuffer(buffer);
        }while(ite.hasNext());

        final Chars val = new Chars(buffer.toArrayByte(),encoding);
        final Terminal terminal = new Terminal();
        terminal.setValue(val);
        return terminal;
    }
}
