package science.unlicense.impl.grammar.bnf;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.model.tree.Node;

/**
 * @author Johann Sorel
 */
public class Cardinal extends DefaultNode {

    private int minOccurences = 1;
    private int maxOccurences = 1;

    public Cardinal(Node child){
        super(true);
        getChildren().add(child);
    }

    public Cardinal(Node child, int min, int max){
        super(true);
        getChildren().add(child);
        this.minOccurences = min;
        this.maxOccurences = max;
    }

    public int getMinOccurences() {
        return minOccurences;
    }

    public void setMinOccurences(int minOccurences) {
        this.minOccurences = minOccurences;
    }

    public int getMaxOccurences() {
        return maxOccurences;
    }

    public void setMaxOccurences(int maxOccurences) {
        this.maxOccurences = maxOccurences;
    }

    public Chars thisToChars(){
        return new Chars(""+minOccurences+":"+maxOccurences);
    }

    public Chars toChars(){
        Chars str = thisToChars();
        return str.concat('-').concat(((Node)getChildren().get(0)).toChars());
    }

    public Chars toCharsTree(int depth){
        Chars str = thisToChars();
        return str.concat('-').concat(((Node)getChildren().get(0)).toCharsTree(depth));
    }

}
