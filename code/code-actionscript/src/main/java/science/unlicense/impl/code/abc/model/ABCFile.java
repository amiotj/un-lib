
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;


/**
 *
 * @author Johann Sorel
 */
public class ABCFile extends ABCBlock {

    /** u16 */
    public int minorVersion;
    /** u16 */
    public int majorVersion;
    /** ABCConstantPool */
    public ABCConstantPool constantPool;
    /** count : u30 value : ABCMethod */
    public ABCMethod[] methods;
    /** count : u30 value : ABCMetadataInfo */
    public ABCMetadataInfo[] metadatas;
    /** count : u30 value : ABCInstance */
    /** count : u30 value : ABCClass */
    public ABCInstance[] instances;
    public ABCClass[] classes;
    /** count : u30 value : ABCScript */
    public ABCScript[] scripts;
    /** count : u30 value : ABCMethodBody */
    public ABCMethodBody[] methodBodies;
    
    public void read(DataInputStream ds) throws IOException {
        minorVersion = ds.readUShort();
        majorVersion = ds.readUShort();
        constantPool = new ABCConstantPool();
        constantPool.read(ds);
        methods = new ABCMethod[readU30(ds)];
        for(int i=0;i<methods.length;i++){
            methods[i] = new ABCMethod();
            methods[i].read(ds);
        }
        metadatas = new ABCMetadataInfo[readU30(ds)];
        for(int i=0;i<metadatas.length;i++){
            metadatas[i] = new ABCMetadataInfo();
            metadatas[i].read(ds);
        }
        instances = new ABCInstance[readU30(ds)];
        for(int i=0;i<instances.length;i++){
            instances[i] = new ABCInstance();
            instances[i].read(ds);
        }
        classes = new ABCClass[instances.length];
        for(int i=0;i<classes.length;i++){
            classes[i] = new ABCClass();
            classes[i].read(ds);
        }
        scripts = new ABCScript[readU30(ds)];
        for(int i=0;i<scripts.length;i++){
            scripts[i] = new ABCScript();
            scripts[i].read(ds);
        }
        methodBodies = new ABCMethodBody[readU30(ds)];
        for(int i=0;i<methodBodies.length;i++){
            methodBodies[i] = new ABCMethodBody();
            methodBodies[i].read(ds);
        }
    }
    
    public void write(DataOutputStream ds) throws IOException {
        ds.writeUShort(minorVersion);
        ds.writeUShort(majorVersion);
        constantPool.write(ds);

        writeU30(ds, methods.length);
        for(int i=0;i<methods.length;i++) methods[i].write(ds);

        writeU30(ds, metadatas.length);
        for(int i=0;i<metadatas.length;i++) metadatas[i].write(ds);

        writeU30(ds, instances.length);
        for(int i=0;i<instances.length;i++) instances[i].write(ds);
        for(int i=0;i<classes.length;i++) classes[i].write(ds);

        writeU30(ds, scripts.length);
        for(int i=0;i<scripts.length;i++) scripts[i].write(ds);

        writeU30(ds, methodBodies.length);
        for(int i=0;i<methodBodies.length;i++) methodBodies[i].write(ds);

    }
            
}
