
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCNamespaceSet extends ABCBlock {

    /**
     * u30 count
     * u30 ns[count]
     */
    public int[] namespaces;
    
    public void read(DataInputStream ds) throws IOException {
        namespaces = readU30Array(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30Array(ds, namespaces);
    }

}
