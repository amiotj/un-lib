
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCOptionalParameter extends ABCBlock {

    /** u30 val
     * u8 kind
     */
    public int val;
    public int kind;
    
    public void read(DataInputStream ds) throws IOException {
        val = readU30(ds);
        kind = ds.readUByte();
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, val);
        ds.writeUByte(kind);
    }

}
