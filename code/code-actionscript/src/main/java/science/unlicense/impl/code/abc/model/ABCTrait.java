
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCTrait extends ABCBlock {

//    u30 name
//    u8 kind
//    u8 data[]
//    u30 metadata_count
//    u30 metadata[metadata_count]

    public void read(DataInputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

    public void write(DataOutputStream ds) throws IOException {
        throw new UnimplementedException("Not supported yet.");
    }

}
