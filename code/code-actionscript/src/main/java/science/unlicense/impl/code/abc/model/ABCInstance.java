
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCInstance extends ABCBlock {

    /** u30 */
    public int name;
    /** u30 */
    public int superName;
    /** u8 */
    public int flags;
    /** u30 */
    public int protectedNs;
    /**
     * u30 count
     * u30 values
     */
    public int[] interfaces;
    /** u30 iinit */
    public int iinit;
    /**
     * u30 count
     * ABCTrait
     */
    public ABCTrait[] traits;
    
    public void read(DataInputStream ds) throws IOException {
        name = readU30(ds);
        superName = readU30(ds);
        flags = readU30(ds);
        protectedNs = readU30(ds);
        interfaces = new int[readU30(ds)];
        for(int i=0;i<interfaces.length;i++) interfaces[i] = readU30(ds);
        iinit = readU30(ds);

        traits = new ABCTrait[readU30(ds)];
        for(int i=0;i<traits.length;i++){
            traits[i] = new ABCTrait();
            traits[i].read(ds);
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, name);
        writeU30(ds, superName);
        writeU30(ds, flags);
        writeU30(ds, protectedNs);
        writeU30(ds, interfaces.length);
        for(int i=0;i<interfaces.length;i++) writeU30(ds, interfaces[i]);
        writeU30(ds, iinit);
        writeU30(ds, traits.length);
        for(int i=0;i<traits.length;i++) traits[i].write(ds);

    }

}
