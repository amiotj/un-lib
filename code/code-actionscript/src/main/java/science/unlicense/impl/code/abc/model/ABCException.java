
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCException extends ABCBlock {

    /** u30 */
    public int from;
    /** u30 */
    public int to;
    /** u30 */
    public int target;
    /** u30 */
    public int exc_type;
    /** u30 */
    public int var_name;

    public void read(DataInputStream ds) throws IOException {
        from = readU30(ds);
        to = readU30(ds);
        target = readU30(ds);
        exc_type = readU30(ds);
        var_name = readU30(ds);
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, from);
        writeU30(ds, to);
        writeU30(ds, target);
        writeU30(ds, exc_type);
        writeU30(ds, var_name);
    }

}
