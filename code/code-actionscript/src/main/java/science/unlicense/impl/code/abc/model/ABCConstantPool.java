
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCConstantPool extends ABCBlock {

    /** count : u30  value : s32 */
    public int[] integers;
    /** count : u30  value : u32 */
    public int[] uintegers;
    /** count : u30  value : d64 */
    public double[] doubles;
    /** count : u30  value : strings */
    public Chars[] strings;
    /** count : u30  value : ABCNamespace */
    public ABCNamespace[] namespaces;
    /** count : u30  value : ABCNamespaceSet */
    public ABCNamespaceSet[] namespaceSets;
    /** count : u30  value : ABCMultiName */
    public ABCMultiName[] multinames;
    
    public void read(DataInputStream ds) throws IOException {
        integers = new int[readU30(ds)];
        for(int i=0;i<integers.length;i++){
            integers[i] = readS32(ds);
        }
        uintegers = new int[readU30(ds)];
        for(int i=0;i<uintegers.length;i++){
            uintegers[i] = readU32(ds);
        }
        doubles = new double[readU30(ds)];
        for(int i=0;i<doubles.length;i++){
            doubles[i] = ds.readDouble();
        }
        strings = new Chars[readU30(ds)];
        for(int i=0;i<strings.length;i++){
            strings[i] = readChars(ds);
        }
        namespaces = new ABCNamespace[readU30(ds)];
        for(int i=0;i<namespaces.length;i++){
            namespaces[i] = new ABCNamespace();
            namespaces[i].read(ds);
        }
        namespaceSets = new ABCNamespaceSet[readU30(ds)];
        for(int i=0;i<namespaceSets.length;i++){
            namespaceSets[i] = new ABCNamespaceSet();
            namespaceSets[i].read(ds);
        }
        multinames = new ABCMultiName[readU30(ds)];
        for(int i=0;i<multinames.length;i++){
            multinames[i] = new ABCMultiName();
            multinames[i].read(ds);
        }
    }
    
    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, integers.length);
        for(int i=0;i<integers.length;i++) writeS32(ds, integers[i]);
        writeU30(ds, uintegers.length);
        for(int i=0;i<uintegers.length;i++) writeU32(ds, uintegers[i]);
        writeU30(ds, doubles.length);
        for(int i=0;i<doubles.length;i++) ds.writeDouble(doubles[i]);
        writeU30(ds, strings.length);
        for(int i=0;i<strings.length;i++) writeChars(ds, strings[i]);
        writeU30(ds, namespaces.length);
        for(int i=0;i<namespaces.length;i++) namespaces[i].write(ds);
        writeU30(ds, namespaceSets.length);
        for(int i=0;i<namespaceSets.length;i++) namespaceSets[i].write(ds);
        writeU30(ds, multinames.length);
        for(int i=0;i<multinames.length;i++) multinames[i].write(ds);
    }
    
}
