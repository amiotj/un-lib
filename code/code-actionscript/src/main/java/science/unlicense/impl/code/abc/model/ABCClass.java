
package science.unlicense.impl.code.abc.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ABCClass extends ABCBlock {

    /** u30 */
    public int cinit;
    /** count : u30 */
    public ABCTrait[] traits;

    public void read(DataInputStream ds) throws IOException {
        cinit = readU30(ds);
        traits = new ABCTrait[readU30(ds)];
        for(int i=0;i<traits.length;i++){
            traits[i] = new ABCTrait();
            traits[i].read(ds);
        }
    }

    public void write(DataOutputStream ds) throws IOException {
        writeU30(ds, cinit);
        writeU30(ds, traits.length);
        for(int i=0;i<traits.length;i++){
            traits[i].write(ds);
        }
    }

}
