
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class CodeSection extends Section {

    public FunctionBody[] bodies;

    @Override
    public void read(DataInputStream ds) throws IOException {
        bodies = new FunctionBody[(int)ds.readVarLengthUInt()];
        for (int i=0;i<bodies.length;i++){
            bodies[i] = new FunctionBody();
            bodies[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(bodies.length);
        for (int i=0;i<bodies.length;i++){
            bodies[i].write(ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(super.toChars(), bodies);
    }

}
