
package science.unlicense.impl.code.wasm;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.code.CodeFormat;
import science.unlicense.api.code.CodeProducer;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * Resources :
 * http://webassembly.org/docs/binary-encoding/#high-level-structure
 *
 * @author Johann Sorel
 */
public class WasmFormat extends DefaultFormat implements CodeFormat {

    public static final WasmFormat INSTANCE = new WasmFormat();

    private WasmFormat() {
        super(new Chars("wasm"),
              new Chars("WASM"),
              new Chars("Web assembly"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("wasm")
              },
              new byte[][]{
                  WasmConstants.SIGNATURE
              });
    }

    @Override
    public CodeFileReader createReader() {
        throw new UnimplementedException();
    }

    @Override
    public CodeProducer createProducer() {
        throw new UnimplementedException();
    }

}
