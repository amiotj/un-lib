
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.number.Int32;

/**
 *
 * @author Johann Sorel
 */
public class ElementSection extends Section {

    public Entry[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        final int count = (int)ds.readVarLengthUInt();
        entries = new Entry[count];
        for (int i=0;i<count;i++) {
            entries[i] = new Entry();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++){
            entries[i].write(ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars(this.getClass().getSimpleName()), entries);
    }
    
    public static class Entry extends CObject {

        public int index;
        public Operands offset;
        public int[] elems;

        public void read(DataInputStream ds) throws IOException {
            index = (int)ds.readVarLengthUInt();
            offset = new Operands();
            offset.read(ds);
            elems = new int[(int)ds.readVarLengthUInt()];
            for (int i=0;i<elems.length;i++) elems[i] = (int)ds.readVarLengthUInt();
        }

        public void write(DataOutputStream ds) throws IOException {
            ds.writeVarLengthUInt(index);
            offset.write(ds);
            ds.writeVarLengthUInt(elems.length);
            for (int i=0;i<elems.length;i++) ds.writeVarLengthUInt(elems[i]);
        }

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(Int32.encode(index));
            cb.append('\n');
            cb.append(offset.toChars());
            cb.append('\n');
            cb.append(Arrays.toChars(elems));
            return cb.toChars();
        }

    }

}
