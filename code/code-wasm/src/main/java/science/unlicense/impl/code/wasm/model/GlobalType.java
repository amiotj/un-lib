
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.code.wasm.WasmConstants;

/**
 *
 * @author Johann Sorel
 */
public class GlobalType extends CObject {

    public byte content_type;
    public boolean mutable;

    public void read(DataInputStream ds) throws IOException {
        content_type = ds.readByte();
        mutable = ds.readVarLengthUInt() != 0;
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeByte(content_type);
        ds.writeVarLengthUInt(mutable ? 1 : 0);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(WasmConstants.typeName(content_type));
        cb.append(' ');
        cb.append(mutable ? "mutable":"");
        return cb.toChars();
    }

}
