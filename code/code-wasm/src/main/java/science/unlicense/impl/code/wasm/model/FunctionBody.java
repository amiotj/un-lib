
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.number.Int32;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class FunctionBody extends CObject{

    public Entry[] locals;
    public Operands code;

    public void read(DataInputStream ds) throws IOException {
        final int body_size = (int)ds.readVarLengthUInt();
        locals = new Entry[(int)ds.readVarLengthUInt()];
        for(int i=0;i<locals.length;i++) {
            locals[i] = new Entry();
            locals[i].read(ds);
        }
        code = new Operands();
        code.read(ds);
    }

    public void write(DataOutputStream ds) throws IOException {

        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ods = new DataOutputStream(out,NumberEncoding.LITTLE_ENDIAN);
        ods.writeVarLengthUInt(locals.length);
        for(int i=0;i<locals.length;i++) {
            locals[i].write(ods);
        }
        code.write(ods);
        ods.flush();
        final byte[] data = out.getBuffer().toArrayByte();
        ds.writeVarLengthUInt(data.length);
        ds.write(data);
    }

    @Override
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(new Chars("FunctionBody\n"));
        if (locals.length!=0) {
            cb.append(Nodes.toChars(new Chars("locals"), locals));
        }
        cb.append(code.toChars());
        return cb.toChars();
    }


    public static class Entry extends CObject{

        public int count;
        public byte type;

        public void read(DataInputStream ds) throws IOException {
            count = (int)ds.readVarLengthUInt();
            type = ds.readByte();
        }

        public void write(DataOutputStream ds) throws IOException {
            ds.writeVarLengthUInt(count);
            ds.writeByte(type);
        }

        @Override
        public Chars toChars() {
            return Int32.encode(count).concat(':').concat(Int32.encodeHexa(type&0xFF));
        }

    }

}
