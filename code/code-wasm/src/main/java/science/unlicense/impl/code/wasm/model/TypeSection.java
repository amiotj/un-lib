
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class TypeSection extends Section {

    public FunctionType[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        final int count = (int)ds.readVarLengthUInt();
        entries = new FunctionType[count];
        for (int i=0;i<count;i++) {
            entries[i] = new FunctionType();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++){
            entries[i].write(ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars(this.getClass().getSimpleName()), entries);
    }

}
