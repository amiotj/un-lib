
package science.unlicense.impl.code.wasm.model;

import java.lang.reflect.Field;
import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.code.wasm.WasmConstants;
import static science.unlicense.impl.code.wasm.WasmConstants.*;

/**
 *
 * @author Johann Sorel
 */
public class Operand extends CObject {
    
    public byte code;
    public Object[] immediates = Arrays.ARRAY_OBJECT_EMPTY;
    
    public void read(DataInputStream ds) throws IOException {
        code = ds.readByte();
        switch (code) {
            //Flow operators
            case OP_UNREACHABLE : break;
            case OP_NOP         : break;
            case OP_BLOCK       : 
            case OP_LOOP        : 
            case OP_IF          : immediates = new Object[]{ds.readVarLengthInt()}; break;
            case OP_ELSE        : break;
            case OP_END         : break;
            case OP_BR          : immediates = new Object[]{ds.readVarLengthUInt()}; break;
            case OP_BR_IF       : immediates = new Object[]{ds.readVarLengthUInt()}; break;
            case OP_BR_TABLE    : {
                                    long nb = ds.readVarLengthUInt();
                                    long[] table = new long[(int)nb];
                                    for(int i=0;i<nb;i++) table[i] = ds.readVarLengthUInt();
                                    long def = ds.readVarLengthUInt();
                                    immediates = new Object[]{nb,table,def};
                                } break;
            case OP_RETURN      : break;

            //Call operators
            case OP_CALL            : immediates = new Object[]{ds.readVarLengthUInt()}; break;
            case OP_CALL_INDIRECT   : immediates = new Object[]{ds.readVarLengthUInt(),ds.readVarLengthUInt()}; break;

            //Parametric operators
            case OP_DROP    : break;
            case OP_SELECT  : break;

            //Variable access
            case OP_GET_LOCAL   : 
            case OP_SET_LOCAL   : 
            case OP_TEE_LOCAL   : 
            case OP_GET_GLOBAL  : 
            case OP_SET_GLOBAL  : immediates = new Object[]{ds.readVarLengthUInt()}; break;

            //Memory related operators
            case OP_I32_LOAD       : 
            case OP_I64_LOAD       : 
            case OP_F32_LOAD       : 
            case OP_F64_LOAD       : 
            case OP_I32_LOAD8_S    : 
            case OP_I32_LOAD8_U    : 
            case OP_I32_LOAD16_S   : 
            case OP_I32_LOAD16_U   : 
            case OP_I64_LOAD8_S    : 
            case OP_I64_LOAD8_U    : 
            case OP_I64_LOAD16_S   : 
            case OP_I64_LOAD16_U   : 
            case OP_I64_LOAD32_S   : 
            case OP_I64_LOAD32_U   : 
            case OP_I32_STORE      : 
            case OP_I64_STORE      : 
            case OP_F32_STORE      : 
            case OP_F64_STORE      : 
            case OP_I32_STORE8     : 
            case OP_I32_STORE16    : 
            case OP_I64_STORE8     : 
            case OP_I64_STORE16    : 
            case OP_I64_STORE32    : immediates = new Object[]{ds.readVarLengthUInt(),ds.readVarLengthUInt()}; break;
            case OP_CURRENT_MEMORY : immediates = new Object[]{ds.readVarLengthUInt()}; break;
            case OP_GROW_MEMORY    : immediates = new Object[]{ds.readVarLengthUInt()}; break;

            //Constants
            case OP_I32_CONST : immediates = new Object[]{ds.readVarLengthInt()}; break;
            case OP_I64_CONST : immediates = new Object[]{ds.readVarLengthInt()}; break;
            case OP_F32_CONST : immediates = new Object[]{ds.readFloat()}; break;
            case OP_F64_CONST : immediates = new Object[]{ds.readDouble()}; break;

            //Comparison operators
            case OP_I32_EQZ  : 
            case OP_I32_EQ   : 
            case OP_I32_NE   : 
            case OP_I32_LT_S : 
            case OP_I32_LT_U : 
            case OP_I32_GT_S : 
            case OP_I32_GT_U : 
            case OP_I32_LE_S : 
            case OP_I32_LE_U : 
            case OP_I32_GE_S : 
            case OP_I32_GE_U : 
            case OP_I64_EQZ  : 
            case OP_I64_EQ   : 
            case OP_I64_NE   : 
            case OP_I64_LT_S : 
            case OP_I64_LT_U : 
            case OP_I64_GT_S : 
            case OP_I64_GT_U : 
            case OP_I64_LE_S : 
            case OP_I64_LE_U : 
            case OP_I64_GE_S : 
            case OP_I64_GE_U : 
            case OP_F32_EQ   : 
            case OP_F32_NE   : 
            case OP_F32_LT   : 
            case OP_F32_GT   : 
            case OP_F32_LE   : 
            case OP_F32_GE   : 
            case OP_F64_EQ   : 
            case OP_F64_NE   : 
            case OP_F64_LT   : 
            case OP_F64_GT   : 
            case OP_F64_LE   : 
            case OP_F64_GE   : break;

            //Numeric operators
            case OP_I32_CLZ      : 
            case OP_I32_CTZ      : 
            case OP_I32_POPCNT   : 
            case OP_I32_ADD      : 
            case OP_I32_SUB      : 
            case OP_I32_MUL      : 
            case OP_I32_DIV_S    : 
            case OP_I32_DIV_U    : 
            case OP_I32_REM_S    : 
            case OP_I32_REM_U    : 
            case OP_I32_AND      : 
            case OP_I32_OR       : 
            case OP_I32_XOR      : 
            case OP_I32_SHL      : 
            case OP_I32_SHR_S    : 
            case OP_I32_SHR_U    : 
            case OP_I32_ROTL     : 
            case OP_I32_ROTR     : 
            case OP_I64_CLZ      : 
            case OP_I64_CTZ      : 
            case OP_I64_POPCNT   : 
            case OP_I64_ADD      : 
            case OP_I64_SUB      : 
            case OP_I64_MUL      : 
            case OP_I64_DIV_S    : 
            case OP_I64_DIV_U    : 
            case OP_I64_REM_S    : 
            case OP_I64_REM_U    : 
            case OP_I64_AND      : 
            case OP_I64_OR       : 
            case OP_I64_XOR      : 
            case OP_I64_SHL      : 
            case OP_I64_SHR_S    : 
            case OP_I64_SHR_U    : 
            case OP_I64_ROTL     : 
            case OP_I64_ROTR     : 
            case OP_F32_ABS      : 
            case OP_F32_NEG      : 
            case OP_F32_CEIL     : 
            case OP_F32_FLOOR    : 
            case OP_F32_TRUNC    : 
            case OP_F32_NEAREST  : 
            case OP_F32_SQRT     : 
            case OP_F32_ADD      : 
            case OP_F32_SUB      : 
            case OP_F32_MUL      : 
            case OP_F32_DIV      : 
            case OP_F32_MIN      : 
            case OP_F32_MAX      : 
            case OP_F32_COPYSIGN : 
            case OP_F64_ABS      : 
            case OP_F64_NEG      : 
            case OP_F64_CEIL     : 
            case OP_F64_FLOOR    : 
            case OP_F64_TRUNC    : 
            case OP_F64_NEAREST  : 
            case OP_F64_SQRT     : 
            case OP_F64_ADD      : 
            case OP_F64_SUB      : 
            case OP_F64_MUL      : 
            case OP_F64_DIV      : 
            case OP_F64_MIN      : 
            case OP_F64_MAX      : 
            case OP_F64_COPYSIGN : break;

            //Conversions
            case OP_I32_WRAP_I64      :
            case OP_I32_TRUNC_S_F32   :
            case OP_I32_TRUNC_U_F32   :
            case OP_I32_TRUNC_S_F64   :
            case OP_I32_TRUNC_U_F64   :
            case OP_I64_EXTEND_S_I32  :
            case OP_I64_EXTEND_U_I32  :
            case OP_I64_TRUNC_S_F32   :
            case OP_I64_TRUNC_U_F32   :
            case OP_I64_TRUNC_S_F64   :
            case OP_I64_TRUNC_U_F64   :
            case OP_F32_CONVERT_S_I32 :
            case OP_F32_CONVERT_U_I32 :
            case OP_F32_CONVERT_S_I64 :
            case OP_F32_CONVERT_U_I64 :
            case OP_F32_DEMOTE_F64    :
            case OP_F64_CONVERT_S_I32 :
            case OP_F64_CONVERT_U_I32 :
            case OP_F64_CONVERT_S_I64 :
            case OP_F64_CONVERT_U_I64 :
            case OP_F64_PROMOTE_F32   : break;

            //Reinterpretations
            case OP_I32_REINTERPRET_F32 :
            case OP_I64_REINTERPRET_F64 :
            case OP_F32_REINTERPRET_I32 :
            case OP_F64_REINTERPRET_I64 : break;
            default: throw new IOException("Unexpected operator : "+Int32.encodeHexa(code&0xFF));
        }
        
    }

    public void write(DataOutputStream ds) throws IOException {
        ds.writeByte(code);
        switch (code) {
            //Flow operators
            case OP_UNREACHABLE : break;
            case OP_NOP         : break;
            case OP_BLOCK       :
            case OP_LOOP        :
            case OP_IF          : ds.writeVarLengthInt((Long)immediates[0]); break;
            case OP_ELSE        : break;
            case OP_END         : break;
            case OP_BR          : ds.writeVarLengthUInt((Long)immediates[0]); break;
            case OP_BR_IF       : ds.writeVarLengthUInt((Long)immediates[0]); break;
            case OP_BR_TABLE    : {
                                    ds.writeVarLengthUInt((Long)immediates[0]);
                                    long[] table = (long[]) immediates[1];
                                    for(int i=0;i<table.length;i++) ds.writeVarLengthUInt((Long)table[i]);
                                    ds.writeVarLengthUInt((Long)immediates[2]);
                                } break;
            case OP_RETURN      : break;

            //Call operators
            case OP_CALL            : ds.writeVarLengthUInt((Long)immediates[0]); break;
            case OP_CALL_INDIRECT   : ds.writeVarLengthUInt((Long)immediates[0]);ds.writeVarLengthUInt((Long)immediates[1]); break;

            //Parametric operators
            case OP_DROP    : break;
            case OP_SELECT  : break;

            //Variable access
            case OP_GET_LOCAL   :
            case OP_SET_LOCAL   :
            case OP_TEE_LOCAL   :
            case OP_GET_GLOBAL  :
            case OP_SET_GLOBAL  : ds.writeVarLengthUInt((Long)immediates[0]); break;

            //Memory related operators
            case OP_I32_LOAD       :
            case OP_I64_LOAD       :
            case OP_F32_LOAD       :
            case OP_F64_LOAD       :
            case OP_I32_LOAD8_S    :
            case OP_I32_LOAD8_U    :
            case OP_I32_LOAD16_S   :
            case OP_I32_LOAD16_U   :
            case OP_I64_LOAD8_S    :
            case OP_I64_LOAD8_U    :
            case OP_I64_LOAD16_S   :
            case OP_I64_LOAD16_U   :
            case OP_I64_LOAD32_S   :
            case OP_I64_LOAD32_U   :
            case OP_I32_STORE      :
            case OP_I64_STORE      :
            case OP_F32_STORE      :
            case OP_F64_STORE      :
            case OP_I32_STORE8     :
            case OP_I32_STORE16    :
            case OP_I64_STORE8     :
            case OP_I64_STORE16    :
            case OP_I64_STORE32    : ds.writeVarLengthUInt((Long)immediates[0]); ds.writeVarLengthUInt((Long)immediates[0]); break;
            case OP_CURRENT_MEMORY : ds.writeVarLengthUInt((Long)immediates[0]); break;
            case OP_GROW_MEMORY    : ds.writeVarLengthUInt((Long)immediates[0]); break;

            //Constants
            case OP_I32_CONST : ds.writeVarLengthInt((Long)immediates[0]); break;
            case OP_I64_CONST : ds.writeVarLengthInt((Long)immediates[0]); break;
            case OP_F32_CONST : ds.writeFloat((Float)immediates[0]); break;
            case OP_F64_CONST : ds.writeDouble((Double)immediates[0]); break;

            //Comparison operators
            case OP_I32_EQZ  :
            case OP_I32_EQ   :
            case OP_I32_NE   :
            case OP_I32_LT_S :
            case OP_I32_LT_U :
            case OP_I32_GT_S :
            case OP_I32_GT_U :
            case OP_I32_LE_S :
            case OP_I32_LE_U :
            case OP_I32_GE_S :
            case OP_I32_GE_U :
            case OP_I64_EQZ  :
            case OP_I64_EQ   :
            case OP_I64_NE   :
            case OP_I64_LT_S :
            case OP_I64_LT_U :
            case OP_I64_GT_S :
            case OP_I64_GT_U :
            case OP_I64_LE_S :
            case OP_I64_LE_U :
            case OP_I64_GE_S :
            case OP_I64_GE_U :
            case OP_F32_EQ   :
            case OP_F32_NE   :
            case OP_F32_LT   :
            case OP_F32_GT   :
            case OP_F32_LE   :
            case OP_F32_GE   :
            case OP_F64_EQ   :
            case OP_F64_NE   :
            case OP_F64_LT   :
            case OP_F64_GT   :
            case OP_F64_LE   :
            case OP_F64_GE   : break;

            //Numeric operators
            case OP_I32_CLZ      :
            case OP_I32_CTZ      :
            case OP_I32_POPCNT   :
            case OP_I32_ADD      :
            case OP_I32_SUB      :
            case OP_I32_MUL      :
            case OP_I32_DIV_S    :
            case OP_I32_DIV_U    :
            case OP_I32_REM_S    :
            case OP_I32_REM_U    :
            case OP_I32_AND      :
            case OP_I32_OR       :
            case OP_I32_XOR      :
            case OP_I32_SHL      :
            case OP_I32_SHR_S    :
            case OP_I32_SHR_U    :
            case OP_I32_ROTL     :
            case OP_I32_ROTR     :
            case OP_I64_CLZ      :
            case OP_I64_CTZ      :
            case OP_I64_POPCNT   :
            case OP_I64_ADD      :
            case OP_I64_SUB      :
            case OP_I64_MUL      :
            case OP_I64_DIV_S    :
            case OP_I64_DIV_U    :
            case OP_I64_REM_S    :
            case OP_I64_REM_U    :
            case OP_I64_AND      :
            case OP_I64_OR       :
            case OP_I64_XOR      :
            case OP_I64_SHL      :
            case OP_I64_SHR_S    :
            case OP_I64_SHR_U    :
            case OP_I64_ROTL     :
            case OP_I64_ROTR     :
            case OP_F32_ABS      :
            case OP_F32_NEG      :
            case OP_F32_CEIL     :
            case OP_F32_FLOOR    :
            case OP_F32_TRUNC    :
            case OP_F32_NEAREST  :
            case OP_F32_SQRT     :
            case OP_F32_ADD      :
            case OP_F32_SUB      :
            case OP_F32_MUL      :
            case OP_F32_DIV      :
            case OP_F32_MIN      :
            case OP_F32_MAX      :
            case OP_F32_COPYSIGN :
            case OP_F64_ABS      :
            case OP_F64_NEG      :
            case OP_F64_CEIL     :
            case OP_F64_FLOOR    :
            case OP_F64_TRUNC    :
            case OP_F64_NEAREST  :
            case OP_F64_SQRT     :
            case OP_F64_ADD      :
            case OP_F64_SUB      :
            case OP_F64_MUL      :
            case OP_F64_DIV      :
            case OP_F64_MIN      :
            case OP_F64_MAX      :
            case OP_F64_COPYSIGN : break;

            //Conversions
            case OP_I32_WRAP_I64      :
            case OP_I32_TRUNC_S_F32   :
            case OP_I32_TRUNC_U_F32   :
            case OP_I32_TRUNC_S_F64   :
            case OP_I32_TRUNC_U_F64   :
            case OP_I64_EXTEND_S_I32  :
            case OP_I64_EXTEND_U_I32  :
            case OP_I64_TRUNC_S_F32   :
            case OP_I64_TRUNC_U_F32   :
            case OP_I64_TRUNC_S_F64   :
            case OP_I64_TRUNC_U_F64   :
            case OP_F32_CONVERT_S_I32 :
            case OP_F32_CONVERT_U_I32 :
            case OP_F32_CONVERT_S_I64 :
            case OP_F32_CONVERT_U_I64 :
            case OP_F32_DEMOTE_F64    :
            case OP_F64_CONVERT_S_I32 :
            case OP_F64_CONVERT_U_I32 :
            case OP_F64_CONVERT_S_I64 :
            case OP_F64_CONVERT_U_I64 :
            case OP_F64_PROMOTE_F32   : break;

            //Reinterpretations
            case OP_I32_REINTERPRET_F32 :
            case OP_I64_REINTERPRET_F64 :
            case OP_F32_REINTERPRET_I32 :
            case OP_F64_REINTERPRET_I64 : break;
            default: throw new IOException("Unexpected operator : "+Int32.encodeHexa(code&0xFF));
        }
    }

    @Override
    public Chars toChars() {
        Chars name = new Chars("unknown");
        try {
            for (Field field : WasmConstants.class.getDeclaredFields()) {
                if(field.getName().startsWith("OP_") && field.getByte(null) == code) {
                    name = new Chars(field.getName().substring(3).toLowerCase());
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        
        if(immediates.length==0) return name;
        
        final CharBuffer cb = new CharBuffer();
        cb.append(name);
        cb.append(' ');
        for(int i=0;i<immediates.length;i++) {
            if(i!=0) cb.append(' ');
            cb.append(CObjects.toChars(immediates[i]));
        }
        return cb.toChars();
    }
    
}
