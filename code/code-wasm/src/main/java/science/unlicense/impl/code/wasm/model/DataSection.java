
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DataSection extends Section {

    public Entry[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        entries = new Entry[(int)ds.readVarLengthUInt()];
        for (int i=0;i<entries.length;i++){
            entries[i] = new Entry();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++){
            entries[i].write(ds);
        }
    }

    public static class Entry {

        public int index;
        public Operands offset;
        public byte[] data;

        public void read(DataInputStream ds) throws IOException {
            index = (int)ds.readVarLengthUInt();
            offset = new Operands();
            offset.read(ds);
            data = ds.readBytes((int)ds.readVarLengthUInt());
        }

        public void write(DataOutputStream ds) throws IOException {
            ds.writeVarLengthUInt(index);
            offset.write(ds);
            ds.writeVarLengthUInt(data.length);
            ds.write(data);
        }

    }

}
