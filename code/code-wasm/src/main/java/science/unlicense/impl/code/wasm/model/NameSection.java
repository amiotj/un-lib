
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 * TODO
 * 
 * @author Johann Sorel
 */
public class NameSection extends Section {

    @Override
    public void read(DataInputStream ds) throws IOException {
        ds.skipFully(payload_len);
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.skipFully(payload_len);
    }

}
