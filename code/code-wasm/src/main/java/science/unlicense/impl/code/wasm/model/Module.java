
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.code.wasm.WasmConstants;

/**
 *
 * @author Johann Sorel
 */
public class Module extends CObject{

    public int version;
    public final Sequence sections = new ArraySequence();
    
    public void read(DataInputStream ds) throws IOException {
        if (!Arrays.equals(WasmConstants.SIGNATURE,ds.readBytes(4))) {
            throw new IOException("Unvalid signature");
        }
        version = ds.readInt();

        for (int b=ds.read();b>=0;b=ds.read()) {
            final Section section;
            switch(b) {
                case  0 : section = new CustomSection(); break;
                case  1 : section = new TypeSection(); break;
                case  2 : section = new ImportSection(); break;
                case  3 : section = new FunctionSection(); break;
                case  4 : section = new TableSection(); break;
                case  5 : section = new MemorySection(); break;
                case  6 : section = new GlobalSection(); break;
                case  7 : section = new ExportSection(); break;
                case  8 : section = new StartSection(); break;
                case  9 : section = new ElementSection(); break;
                case 10 : section = new CodeSection(); break;
                case 11 : section = new DataSection(); break;
                default : throw new IOException("Unexpected section "+b);
            }
            sections.add(section);
            section.id = b;
            section.payload_len = (int)ds.readVarLengthUInt();
            section.read(ds);
        }

    }

    public void write(DataOutputStream ds) throws IOException {
        ds.write(WasmConstants.SIGNATURE);
        ds.writeInt(version);

        for (int i=0,n=sections.getSize();i<n;i++) {
            final Section section = (Section) sections.get(i);
            ds.write((byte) section.id);

            final ArrayOutputStream out = new ArrayOutputStream();
            final DataOutputStream ods = new DataOutputStream(out,NumberEncoding.LITTLE_ENDIAN);
            section.write(ods);
            ods.flush();
            final byte[] data = out.getBuffer().toArrayByte();
            ds.writeVarLengthUInt(data.length); //payload_len
            ds.write(data);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars("Module"), sections);
    }
    
}
