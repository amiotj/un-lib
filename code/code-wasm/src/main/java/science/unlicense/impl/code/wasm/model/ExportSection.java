
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.code.wasm.WasmConstants;

/**
 *
 * @author Johann Sorel
 */
public class ExportSection extends Section {

    public Entry[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        entries = new Entry[(int)ds.readVarLengthUInt()];
        for (int i=0;i<entries.length;i++){
            entries[i] = new Entry();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++){
            entries[i].write(ds);
        }
    }

    @Override
    public Chars toChars() {
        return Nodes.toChars(new Chars(this.getClass().getSimpleName()), entries);
    }
    
    public static class Entry extends CObject{

        public Chars field_str;
        public byte kind;
        public int index;

        public void read(DataInputStream ds) throws IOException {
            field_str = new Chars(ds.readBytes((int)ds.readVarLengthUInt()));
            kind = ds.readByte();
            index = (int)ds.readVarLengthUInt();
        }

        public void write(DataOutputStream ds) throws IOException {
            final byte[] nameBytes = field_str.toBytes(CharEncodings.UTF_8);
            ds.writeVarLengthUInt(nameBytes.length);
            ds.write(nameBytes);
            ds.writeByte(kind);
            ds.writeVarLengthUInt(index);
        }

        @Override
        public Chars toChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(field_str);
            cb.append(' ');
            cb.append(WasmConstants.externalKindName(kind));
            cb.append(' ');
            cb.append(Int32.encode(index));
            return cb.toChars();
        }

    }
}
