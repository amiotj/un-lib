
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class MemorySection extends Section {

    public MemoryType[] entries;

    @Override
    public void read(DataInputStream ds) throws IOException {
        entries = new MemoryType[(int)ds.readVarLengthUInt()];
        for (int i=0;i<entries.length;i++){
            entries[i] = new MemoryType();
            entries[i].read(ds);
        }
    }

    @Override
    public void write(DataOutputStream ds) throws IOException {
        ds.writeVarLengthUInt(entries.length);
        for (int i=0;i<entries.length;i++){
            entries[i].write(ds);
        }
    }
    
}
