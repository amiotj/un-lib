
package science.unlicense.impl.code.wasm.model;

import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public abstract class Section extends CObject {

    public int id;
    public int payload_len;

    public abstract void read(DataInputStream ds) throws IOException;

    public abstract void write(DataOutputStream ds) throws IOException;

    @Override
    public Chars toChars() {
        return new Chars(this.getClass().getSimpleName());
    }
    
}
