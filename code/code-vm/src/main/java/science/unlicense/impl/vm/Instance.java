
package science.unlicense.impl.vm;

import science.unlicense.api.code.Class;
import science.unlicense.api.code.inst.Reference;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;

/**
 *
 * @author Johann Sorel
 */
public class Instance {

    private final science.unlicense.api.code.Class clazz;
    private final Reference ref;
    private final Dictionary properties = new HashDictionary();

    public Instance(Reference ref, Class clazz) {
        this.ref = ref;
        this.clazz = clazz;
    }

    public Reference getReference() {
        return ref;
    }

    public Class getClazz() {
        return clazz;
    }

    public Instance get(Reference ref){
        if(ref==Reference.SELF){
            return this;
        }else{
            return (Instance) properties.getValue(ref);
        }
    }

    public void set(Reference ref, Instance value){
        properties.add(ref, value);
    }

}
