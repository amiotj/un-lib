
package science.unlicense.impl.code.sql;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.code.CodeFormat;
import science.unlicense.api.code.CodeProducer;
import science.unlicense.api.store.DefaultFormat;
import science.unlicense.impl.code.GrammarCodeFileReader;

/**
 *
 * @author Johann Sorel
 */
public class SQLFormat extends DefaultFormat implements CodeFormat {

    public static final SQLFormat INSTANCE = new SQLFormat();

    private SQLFormat() {
        super(new Chars("sql"),
              new Chars("SQL"),
              new Chars("Structured Query Language file"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("sql")
              },
              new byte[][]{});
    }

    @Override
    public CodeFileReader createReader() {
        return new GrammarCodeFileReader(null);
    }

    @Override
    public CodeProducer createProducer() {
        return null;
    }
    
}
