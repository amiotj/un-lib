
package science.unlicense.impl.compiler.so.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.compiler.so.ELFConstants;

/**
 *
 * @author Johann Sorel
 */
public class ELFSectionHeader {

    /** Elf32_Word */
    public long sh_name;
    /** Elf32_Word */
    public long sh_type;
    /** Elf32_Word */
    public long sh_flags;
    /** Elf32_Addr */
    public long sh_addr;
    /** Elf32_Off */
    public long sh_offset;
    /** Elf32_Word */
    public long sh_size;
    /** Elf32_Word */
    public long sh_link;
    /** Elf32_Word */
    public long sh_info;
    /** Elf32_Word */
    public long sh_adralign;
    /** Elf32_Word */
    public long sh_entsize;
    
    public void read(DataInputStream ds, ELFHeader header) throws IOException{
        
        if(header.encodingsize == ELFConstants.ENC_32){
            sh_name     = ds.readUInt();
            sh_type     = ds.readUInt();
            sh_flags    = ds.readUInt();
            sh_addr     = ds.readUInt();
            sh_offset   = ds.readUInt();
            sh_size     = ds.readUInt();
            sh_link     = ds.readUInt();
            sh_info     = ds.readUInt();
            sh_adralign = ds.readUInt();
            sh_entsize  = ds.readUInt();
        }else{
            sh_name     = ds.readUInt();
            sh_type     = ds.readUInt();
            sh_flags    = ds.readLong();
            sh_addr     = ds.readLong();
            sh_offset   = ds.readLong();
            sh_size     = ds.readLong();
            sh_link     = ds.readUInt();
            sh_info     = ds.readUInt();
            sh_adralign = ds.readLong();
            sh_entsize  = ds.readLong();
            
        }
    }
    
}
