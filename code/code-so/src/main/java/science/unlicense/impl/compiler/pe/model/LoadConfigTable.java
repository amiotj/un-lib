

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class LoadConfigTable extends Section {

    /** 4 bytes */
    public int Characteristics;
    /** 4 bytes */
    public int TimeDateStamp;
    /** 2 bytes */
    public short MajorVersion;
    /** 2 bytes */
    public short MinorVersion;
    /** 4 bytes */
    public int GlobalFlagsClear;
    /** 4 bytes */
    public int GlobalFlagsSet;
    /** 4 bytes */
    public int CriticalSectionDefaultTimeout;
    /** 8 bytes */
    public long DeCommitFreeBlockThreshold;
    /** 8 bytes */
    public long DeCommitTotalFreeThreshold;
    /** 8 bytes */
    public long LockPrefixTable;
    /** 8 bytes */
    public long MaximumAllocationSize;
    /** 8 bytes */
    public long VirtualMemoryThreshold;
    /** 8 bytes */
    public long ProcessAffinityMask;
    /** 4 bytes */
    public int ProcessHeapFlags;
    /** 2 bytes */
    public short CSDVersion;
    /** 2 bytes */
    public short Reserved;
    /** 8 bytes */
    public long EditList;
    /** 4/8 bytes */
    public long SecurityCookie;
    /** 4/8 bytes */
    public long SEHandlerTable;
    /** 4/8 bytes */
    public long SEHandlerCount;


    public LoadConfigTable(SectionHeader header) {
        super(header, new Chars("?????"));
    }

}
