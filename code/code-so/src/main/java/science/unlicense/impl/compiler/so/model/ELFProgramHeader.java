
package science.unlicense.impl.compiler.so.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.compiler.so.ELFConstants;

/**
 *
 * @author Johann Sorel
 */
public class ELFProgramHeader {

    /** uint32_t */
    public long p_type;
    /** Elf32_Off */
    public long p_offset;
    /** Elf32_Addr */
    public long p_vaddr;
    /** Elf32_Addr */
    public long p_paddr;
    /** uint32_t */
    public long p_filesz;
    /** uint32_t */
    public long p_memsz;
    /** uint32_t */
    public long p_flags;
    /** uint32_t */
    public long p_align;
    
    public void read(DataInputStream ds, ELFHeader header) throws IOException{
        
        if(header.encodingsize == ELFConstants.ENC_32){
            p_type      = ds.readUInt();
            p_offset    = ds.readUInt();
            p_vaddr     = ds.readUInt();
            p_paddr     = ds.readUInt();
            p_filesz    = ds.readUInt();
            p_memsz     = ds.readUInt();
            p_flags     = ds.readUInt();
            p_align     = ds.readUInt();
        }else{
            p_type      = ds.readUInt();
            p_flags     = ds.readUInt();
            p_offset    = ds.readLong();
            p_vaddr     = ds.readLong();
            p_paddr     = ds.readLong();
            p_filesz    = ds.readLong();
            p_memsz     = ds.readLong();
            p_align     = ds.readLong();
        }
        
    }
    
}
