
package science.unlicense.impl.compiler.pe;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class DosHeader {

    /** header size, with magic number */
    public static int BYTE_SIZE = 62;

    /** WORD : Bytes on last page of file */
    public short e_cblp;
    /** WORD : Pages in file */
    public short e_cp;
    /** WORD : Relocations */
    public short e_crlc;
    /** WORD : Size of header in paragraphs */
    public short e_cparhdr;
    /** WORD : Minimum extra paragraphs needed */
    public short e_minalloc;
    /** WORD : Maximum extra paragraphs needed */
    public short e_maxalloc;
    /** WORD : Initial (relative) SS value */
    public short e_ss;
    /** WORD : Initial SP value */
    public short e_sp;
    /** WORD : Checksum */
    public short e_csum;
    /** WORD : Initial IP value */
    public short e_ip;
    /** WORD : Initial (relative) CS value */
    public short e_cs;
    /** WORD : File address of relocation table */
    public short e_lfarlc;
    /** WORD : Overlay number */
    public short e_ovno;
    /** WORD : Reserved words */
    public short[] e_res = new short[4];
    /** WORD : OEM identifier (for e_oeminfo) */
    public short e_oemid;
    /** WORD : OEM information; e_oemid specific */
    public short e_oeminfo;
    /** WORD : Reserved words */
    public short[] e_res2 = new short[10];
    /** LONG : File address of new exe header */
    public int e_lfanew;

    /**
     * Read header from given stream.
     * @param ds
     */
    public void read(DataInputStream ds) throws IOException{
        e_cblp = ds.readShort();
        e_cp = ds.readShort();
        e_crlc = ds.readShort();
        e_cparhdr = ds.readShort();
        e_minalloc = ds.readShort();
        e_maxalloc = ds.readShort();
        e_ss = ds.readShort();
        e_sp = ds.readShort();
        e_csum = ds.readShort();
        e_ip = ds.readShort();
        e_cs = ds.readShort();
        e_lfarlc = ds.readShort();
        e_ovno = ds.readShort();
        for(int i=0;i<e_res.length;i++){
            e_res[i] = ds.readShort();
        }
        e_oemid = ds.readShort();
        e_oeminfo = ds.readShort();
        for(int i=0;i<e_res2.length;i++){
            e_res2[i] = ds.readShort();
        }
        e_lfanew = ds.readInt();
    }

    @Override
    public String toString() {
        return "DosHeader{" + "e_cblp=" + e_cblp + ", e_cp=" + e_cp + ", e_crlc=" + e_crlc + ", e_cparhdr=" + e_cparhdr + ", e_minalloc=" + e_minalloc + ", e_maxalloc=" + e_maxalloc + ", e_ss=" + e_ss + ", e_sp=" + e_sp + ", e_csum=" + e_csum + ", e_ip=" + e_ip + ", e_cs=" + e_cs + ", e_lfarlc=" + e_lfarlc + ", e_ovno=" + e_ovno + ", e_res=" + e_res + ", e_oemid=" + e_oemid + ", e_oeminfo=" + e_oeminfo + ", e_res2=" + e_res2 + ", e_lfanew=" + e_lfanew + '}';
    }



}
