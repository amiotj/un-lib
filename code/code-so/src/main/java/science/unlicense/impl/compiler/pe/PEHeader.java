
package science.unlicense.impl.compiler.pe;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class PEHeader extends CObject{

    public FileHeader fileHeader;
    public OptionalHeader optHeader;

    public void read(DataInputStream ds) throws IOException{
        fileHeader = new FileHeader();
        fileHeader.read(ds);

        if(fileHeader.SizeOfOptionalHeader != 0){
            optHeader = new OptionalHeader();
            optHeader.read(ds);
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(fileHeader.toChars());
        if(optHeader!=null){
            cb.append('\n');
            cb.append(optHeader.toChars());
        }

        return cb.toChars();
    }

}
