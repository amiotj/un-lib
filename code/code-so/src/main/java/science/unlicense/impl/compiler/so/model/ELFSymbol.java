
package science.unlicense.impl.compiler.so.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.compiler.so.ELFConstants;

/**
 *
 * @author Johann Sorel
 */
public class ELFSymbol {
    
    /** uint32_t */
    public long st_name;
    /** Elf32_Addr */
    public long st_value;
    /** uint32_t */
    public long st_size;
    /** unsigned char */
    public long st_info;
    /** unsigned char */
    public long st_other;
    /** uint16_t */
    public long st_shndx;
               
    public void read(DataInputStream ds, ELFHeader header) throws IOException{
        
        if(header.encodingsize == ELFConstants.ENC_32){
            st_name     = ds.readUInt();
            st_value    = ds.readUInt();
            st_size     = ds.readUInt();
            st_info     = ds.readUByte();
            st_other    = ds.readUByte();
            st_shndx    = ds.readUShort();
        }else{
            st_name     = ds.readUInt();
            st_info     = ds.readUByte();
            st_other    = ds.readUByte();
            st_shndx    = ds.readUShort();
            st_value    = ds.readLong();
            st_size     = ds.readLong();
        }
        
    }
    
}
