
package science.unlicense.impl.compiler.pe;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class OptionalHeader extends CObject{

    public static class DataDirectory{
        /** DWORD */
        public int VirtualAddress;
        /** DWORD */
        public int Size;
    }

    /** header size, with magic number */
    public static int BYTE_SIZE = 96;

    /** WORD */
    public short Magic;
    /** BYTE */
    public byte MajorLinkerVersion;
    /** BYTE */
    public byte MinorLinkerVersion;
    /** DWORD */
    public int SizeOfCode;
    /** DWORD */
    public int SizeOfInitializedData;
    /** DWORD */
    public int SizeOfUninitializedData;
    /** DWORD */
    public int AddressOfEntryPoint;
    /** DWORD */
    public int BaseOfCode;
    /** DWORD : filled only for magic type = IMAGE_NT_OPTIONAL_HDR32_MAGIC */
    public int BaseOfData;
    /** DWORD/LONG depends if it is a PE32 or PE32+ */
    public long ImageBase;
    /** DWORD */
    public int SectionAlignment;
    /** DWORD */
    public int FileAlignment;
    /** WORD */
    public short MajorOperatingSystemVersion;
    /** WORD */
    public short MinorOperatingSystemVersion;
    /** WORD */
    public short MajorImageVersion;
    /** WORD */
    public short MinorImageVersion;
    /** WORD */
    public short MajorSubsystemVersion;
    /** WORD */
    public short MinorSubsystemVersion;
    /** DWORD */
    public int Win32VersionValue;
    /** DWORD */
    public int SizeOfImage;
    /** DWORD */
    public int SizeOfHeaders;
    /** DWORD */
    public int CheckSum;
    /** WORD */
    public short Subsystem;
    /** WORD */
    public short DllCharacteristics;
    /** DWORD/LONG depends if it is a PE32 or PE32+ */
    public long SizeOfStackReserve;
    /** DWORD/LONG depends if it is a PE32 or PE32+ */
    public long SizeOfStackCommit;
    /** DWORD/LONG depends if it is a PE32 or PE32+ */
    public long SizeOfHeapReserve;
    /** DWORD/LONG depends if it is a PE32 or PE32+ */
    public long SizeOfHeapCommit;
    /** DWORD */
    public int LoaderFlags;
    /** DWORD */
    public int NumberOfRvaAndSizes;
    /** size is NumberOfRvaAndSizes */
    public DataDirectory[] directories;

    public void read(DataInputStream ds) throws IOException {
        Magic = ds.readShort();
        MajorLinkerVersion = ds.readByte();
        MinorLinkerVersion = ds.readByte();
        SizeOfCode = ds.readInt();
        SizeOfInitializedData = ds.readInt();
        SizeOfUninitializedData = ds.readInt();
        AddressOfEntryPoint = ds.readInt();
        BaseOfCode = ds.readInt();
        if(Magic == PEConstants.IMAGE_NT_OPTIONAL_HDR32_MAGIC){
            BaseOfData = ds.readInt();
            ImageBase = ds.readInt();
        }else{
            ImageBase = ds.readLong();
        }
        SectionAlignment = ds.readInt();
        FileAlignment = ds.readInt();
        MajorOperatingSystemVersion = ds.readShort();
        MinorOperatingSystemVersion = ds.readShort();
        MajorImageVersion = ds.readShort();
        MinorImageVersion = ds.readShort();
        MajorSubsystemVersion = ds.readShort();
        MinorSubsystemVersion = ds.readShort();
        Win32VersionValue = ds.readInt();
        SizeOfImage = ds.readInt();
        SizeOfHeaders = ds.readInt();
        CheckSum = ds.readInt();
        Subsystem = ds.readShort();
        DllCharacteristics = ds.readShort();
        if(Magic == PEConstants.IMAGE_NT_OPTIONAL_HDR32_MAGIC){
            SizeOfStackReserve = ds.readInt();
            SizeOfStackCommit = ds.readInt();
            SizeOfHeapReserve = ds.readInt();
            SizeOfHeapCommit = ds.readInt();
        }else{
            SizeOfStackReserve = ds.readLong();
            SizeOfStackCommit = ds.readLong();
            SizeOfHeapReserve = ds.readLong();
            SizeOfHeapCommit = ds.readLong();
        }
        LoaderFlags = ds.readInt();
        NumberOfRvaAndSizes = ds.readInt();

        directories = new DataDirectory[NumberOfRvaAndSizes];
        for(int i=0;i<NumberOfRvaAndSizes;i++){
            directories[i] = new DataDirectory();
            directories[i].VirtualAddress = ds.readInt();
            directories[i].Size = ds.readInt();
        }

    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("OptionalHeader:\n");
        cb.append("- Magic=" + Magic).append('\n');
        cb.append("- MajorLinkerVersion=" + MajorLinkerVersion).append('\n');
        cb.append("- MinorLinkerVersion=" + MinorLinkerVersion).append('\n');
        cb.append("- SizeOfCode=" + SizeOfCode).append('\n');
        cb.append("- SizeOfInitializedData=" + SizeOfInitializedData).append('\n');
        cb.append("- SizeOfUninitializedData=" + SizeOfUninitializedData).append('\n');
        cb.append("- AddressOfEntryPoint=" + AddressOfEntryPoint).append('\n');
        cb.append("- BaseOfCode=" + BaseOfCode).append('\n');
        cb.append("- BaseOfData=" + BaseOfData).append('\n');
        cb.append("- ImageBase=" + ImageBase).append('\n');
        cb.append("- SectionAlignment=" + SectionAlignment).append('\n');
        cb.append("- FileAlignment=" + FileAlignment).append('\n');
        cb.append("- MajorOperatingSystemVersion=" + MajorOperatingSystemVersion).append('\n');
        cb.append("- MinorOperatingSystemVersion=" + MinorOperatingSystemVersion).append('\n');
        cb.append("- MajorImageVersion=" + MajorImageVersion).append('\n');
        cb.append("- MinorImageVersion=" + MinorImageVersion).append('\n');
        cb.append("- MajorSubsystemVersion=" + MajorSubsystemVersion).append('\n');
        cb.append("- MinorSubsystemVersion=" + MinorSubsystemVersion).append('\n');
        cb.append("- Win32VersionValue=" + Win32VersionValue).append('\n');
        cb.append("- SizeOfImage=" + SizeOfImage).append('\n');
        cb.append("- SizeOfHeaders=" + SizeOfHeaders).append('\n');
        cb.append("- CheckSum=" + CheckSum).append('\n');
        cb.append("- Subsystem=" + Subsystem).append('\n');
        cb.append("- DllCharacteristics=" + DllCharacteristics).append('\n');
        cb.append("- SizeOfStackReserve=" + SizeOfStackReserve).append('\n');
        cb.append("- SizeOfStackCommit=" + SizeOfStackCommit).append('\n');
        cb.append("- SizeOfHeapReserve=" + SizeOfHeapReserve).append('\n');
        cb.append("- SizeOfHeapCommit=" + SizeOfHeapCommit).append('\n');
        cb.append("- NumberOfRvaAndSizes=" + NumberOfRvaAndSizes).append('\n');
        return cb.toChars();
    }


}
