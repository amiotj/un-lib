
package science.unlicense.impl.compiler.pe;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class FileHeader extends CObject {

    /** WORD */
    public short Machine;
    /** WORD */
    public short NumberOfSections;
    /** DWORD : number of seconds since 1970 */
    public int TimeDateStamp;
    /** DWORD */
    public int PointerToSymbolTable;
    /** DWORD */
    public int NumberOfSymbols;
    /** WORD */
    public short SizeOfOptionalHeader;
    /** WORD */
    public short Characteristics;

    public void read(DataInputStream ds) throws IOException{
        Machine = ds.readShort();
        NumberOfSections = ds.readShort();
        TimeDateStamp = ds.readInt();
        PointerToSymbolTable = ds.readInt();
        NumberOfSymbols = ds.readInt();
        SizeOfOptionalHeader = ds.readShort();
        Characteristics = ds.readShort();
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("FileHeader:\n");
        cb.append("- Machine=" + Machine).append('\n');
        cb.append("- NumberOfSections=" + NumberOfSections).append('\n');
        cb.append("- TimeDateStamp=" + TimeDateStamp).append('\n');
        cb.append("- PointerToSymbolTable=" + PointerToSymbolTable).append('\n');
        cb.append("- NumberOfSymbols=" + NumberOfSymbols).append('\n');
        cb.append("- SizeOfOptionalHeader=" + SizeOfOptionalHeader).append('\n');
        cb.append("- Characteristics=" + Characteristics).append('\n');
        return cb.toChars();
    }

}
