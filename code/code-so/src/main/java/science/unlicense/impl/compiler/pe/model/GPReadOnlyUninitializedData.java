

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class GPReadOnlyUninitializedData extends Section {

    public GPReadOnlyUninitializedData(SectionHeader header) {
        super(header, new Chars(".srdata"));
    }

}
