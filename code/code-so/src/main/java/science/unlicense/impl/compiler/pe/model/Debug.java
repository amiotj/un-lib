

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class Debug extends Section {

    public static final class DebugDirectory{
        /** 4bytes */
        public int Characteristics;
        /** 4bytes */
        public int TimeDateStamp;
        /** 2bytes */
        public short MajorVersion;
        /** 2bytes */
        public short MinorVersion;
        /** 4bytes */
        public int Type;
        /** 4bytes */
        public int SizeOfData;
        /** 4bytes */
        public int AddressOfRawData;
        /** 4bytes */
        public int PointerToRawData;

    }

    public Debug(SectionHeader header) {
        super(header, new Chars(".debug"));
    }

}
