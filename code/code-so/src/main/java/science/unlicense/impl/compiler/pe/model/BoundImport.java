

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class BoundImport extends Section {

    public BoundImport(SectionHeader header) {
        super(header, new Chars("?????"));
    }

}
