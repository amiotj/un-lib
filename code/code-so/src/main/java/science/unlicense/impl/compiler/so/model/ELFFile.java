
package science.unlicense.impl.compiler.so.model;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.compiler.pe.SectionHeader;
import science.unlicense.impl.io.BacktrackInputStream;

/**
 *
 * @author Johann Sorel
 */
public class ELFFile {

    public ELFHeader header;
    public SectionHeader[] sections;
    
    public void read(BacktrackInputStream ds) throws IOException {
        
        //read header
        header = new ELFHeader();
        header.read(ds);
        
        //read section headers
        sections = new SectionHeader[header.e_shnum];
        
        
        //TODO section header, program headers, strings, symbols ...
    }
    
    public void write(DataOutputStream ds) {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
