

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ExecutableCode extends Section {

    public ExecutableCode(SectionHeader header) {
        super(header, new Chars(".text"));
    }

}
