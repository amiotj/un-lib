

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class RelocationTable extends Section {

    public static final class BaseRelocationBlock{
        /** 4bytes */
        public int PageRVA;
        /** 4bytes */
        public int BlockSize;
        /** types :
         * - 4 bits Type
         * - 12 bits Offset
         */
        public short[] types;
    }

    public static final class Relocation{
        /** 4bytes */
        public int VirtualAddress;
        /** 4bytes */
        public int SymbolTableIndex;
        /** 2bytes */
        public short type;
    }

    public Relocation[] relocations;

    public RelocationTable(SectionHeader header) {
        super(header, new Chars(".reloc"));
    }

    public void read(DataInputStream ds) throws IOException {
        super.read(ds);
        //TODO
    }

}
