
package science.unlicense.impl.compiler.so;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.compiler.so.model.ELFFile;
import science.unlicense.impl.io.BacktrackInputStream;

/**
 * Executable Link File reader.
 * 
 * @author Johann Sorel
 */
public class ELFReader extends AbstractReader{
        
    public ELFReader() {
    }
        
    public ELFFile read() throws IOException{
        final BacktrackInputStream ds = getInputAsBacktrackStream();
        final ELFFile file = new ELFFile();
        file.read(ds);
        return file;
    }
    
}
