
package science.unlicense.impl.compiler.so.model;

import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.compiler.so.ELFConstants;

/**
 *
 * @author Johann Sorel
 */
public class ELFDynamic {
    
    /** Elf32_Sword */
    public long d_tag;
    /** Elf32_Word    or d_ptr */
    public long d_val;
    
    public void read(DataInputStream ds, ELFHeader header) throws IOException{
        
        if(header.encodingsize == ELFConstants.ENC_32){
            d_tag = ds.readInt();
            d_val = ds.readUInt();
        }else{
            d_tag = ds.readLong();
            d_val = ds.readLong();
        }
    }
    
}
