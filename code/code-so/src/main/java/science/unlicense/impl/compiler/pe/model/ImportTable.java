

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ImportTable extends Section {

    public static final class ImportDirectory{
        /** 4bytes ImportLookupTableRVA*/
        public int Characteristics;
        /** 4bytes*/
        public int DateStamp;
        /** 4bytes*/
        public int ForwarderChain;
        /** 4bytes*/
        public int NameRVA;
        /** 4bytes Import Address Table RVA*/
        public int ThunkTable;
    }

    public static final class ImportLookupTable{
        /** 1bits */
        public int OrdinalNameFlag;
        /** 16bits */
        public int OrdinalNumber;
        /** 31bits */
        public int HintNameTableRVA;
    }

    public static final class ImportNameTable{
        /** 2bytes */
        public int Hint;
        /** */
        public Chars Name;
        /** 0 or 1byte */
        public int Pad;
    }

    public ImportTable(SectionHeader header) {
        super(header, new Chars(".idata"));
    }

}
