
package science.unlicense.impl.compiler.so.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.compiler.so.ELFConstants;
import science.unlicense.impl.io.BacktrackInputStream;

/**
 * ELF Header.
 * 
 * @author Johann Sorel
 */
public class ELFHeader {
    
    //file identifier
    public byte encodingsize;
    public byte endianess;
    public byte version;
    public byte abi;
    public byte abiversion;
    
    public NumberEncoding encoding;
    
    /** uint16_t */
    public int e_type;
    /** uint16_t */
    public int e_machine;
    /** uint32_t */
    public int e_version;
    /** ElfN_Addr : starting address for the program */
    public long e_entry;
    /** ElfN_Off : starting offset of the program headers */
    public long e_phoff;
    /** ElfN_Off : starting offset of the section headers */
    public long e_shoff;
    /** uint32_t */
    public int e_flags;
    /** uint16_t : size of this elf header, should be 64 for x64 and 52 for x86*/
    public int e_ehsize;
    /** uint16_t : size of a program header entry*/
    public int e_phentsize;
    /** uint16_t : number of entries in the program table*/
    public int e_phnum;
    /** uint16_t : size of a section table entry */
    public int e_shentsize;
    /** uint16_t : number of entries in the section table */
    public int e_shnum;
    /** uint16_t : */
    public int e_shstrndx;
    
    public void read(BacktrackInputStream bs) throws IOException{
        final DataInputStream ds = new DataInputStream(bs);
        final byte[] eident = ds.readFully(new byte[16]);
        if(!Arrays.equals(eident,0,4,ELFConstants.SIGNATURE,0)){
            throw new IOException("File is not and ELF.");
        }
        
        encodingsize = eident[4];
        endianess = eident[5];
        version = eident[6];
        abi = eident[7];
        abiversion = eident[8];
        
        if(endianess==ELFConstants.END_LSB){
            encoding = NumberEncoding.LITTLE_ENDIAN;
        }else{
            encoding = NumberEncoding.BIG_ENDIAN;
        }
        
        e_type = ds.readUShort(encoding);
        e_machine = ds.readUShort(encoding);
        e_version = ds.readInt(encoding);
        if(encodingsize == ELFConstants.ENC_32){
            e_entry = ds.readUInt(encoding);
            e_phoff = ds.readUInt(encoding);
            e_shoff = ds.readUInt(encoding);
        }else{
            e_entry = ds.readLong(encoding);
            e_phoff = ds.readLong(encoding);
            e_shoff = ds.readLong(encoding);
        }
        e_flags = ds.readInt(encoding);
        e_ehsize = ds.readUShort(encoding);
        e_phentsize = ds.readUShort(encoding);
        e_phnum = ds.readUShort(encoding);
        e_shentsize = ds.readUShort(encoding);
        e_shnum = ds.readUShort(encoding);
        e_shstrndx = ds.readUShort(encoding);
        
    }
    
}
