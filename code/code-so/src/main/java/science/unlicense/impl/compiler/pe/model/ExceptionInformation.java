

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ExceptionInformation extends Section {

    public ExceptionInformation(SectionHeader header) {
        super(header, new Chars(".xdata"));
    }

}
