

package science.unlicense.impl.compiler.pe.model;

import java.lang.reflect.InvocationTargetException;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.impl.compiler.pe.SectionHeader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class Section {

    private static final Dictionary SECTION_MAP = new HashDictionary();
    static {
        SECTION_MAP.add(new Chars("?????"),     BoundImport.class);
        SECTION_MAP.add(new Chars(".cormeta"),  CLRRuntimeHeader.class);
        SECTION_MAP.add(new Chars("?????"),     CertificationTable.class);
        SECTION_MAP.add(new Chars(".debug"),    Debug.class);
        SECTION_MAP.add(new Chars("?????"),     DelayImportDescriptor.class);
        SECTION_MAP.add(new Chars(".drectve"),  DirectiveSection.class);
        SECTION_MAP.add(new Chars(".sxdata"),   ExceptionHandlerData.class);
        SECTION_MAP.add(new Chars(".xdata"),    ExceptionInformation.class);
        SECTION_MAP.add(new Chars(".pdata"),    ExceptionTable.class);
        SECTION_MAP.add(new Chars(".text"),     ExecutableCode.class);
        SECTION_MAP.add(new Chars(".edata"),    ExportTable.class);
        SECTION_MAP.add(new Chars(".sdata"),    GPInitializedData.class);
        SECTION_MAP.add(new Chars(".srdata"),   GPReadOnlyUninitializedData.class);
        SECTION_MAP.add(new Chars(".vsdata"),   GPRelativeInitializedData.class);
        SECTION_MAP.add(new Chars(".sbss"),     GPUninitializedData.class);
        SECTION_MAP.add(new Chars("?????"),     GlobalPointer.class);
        SECTION_MAP.add(new Chars("?????"),     ImportAddressTable.class);
        SECTION_MAP.add(new Chars(".idata"),    ImportTable.class);
        SECTION_MAP.add(new Chars(".data"),     InitializedData.class);
        SECTION_MAP.add(new Chars(".drective"), LinkerOptions.class);
        SECTION_MAP.add(new Chars("?????"),     LoadConfigTable.class);
        SECTION_MAP.add(new Chars(".rdata"),    ReadOnlyInitializedData.class);
        SECTION_MAP.add(new Chars(".idlsym"),   RegisteredSEH.class);
        SECTION_MAP.add(new Chars(".reloc"),    RelocationTable.class);
        SECTION_MAP.add(new Chars(".rsrc"),     ResourceTable.class);
        SECTION_MAP.add(new Chars(".file"),     SymbolTable.class);
        SECTION_MAP.add(new Chars(".tls"),      ThreadLocalStorageTable.class);
        SECTION_MAP.add(new Chars(".bss"),      UninitializedData.class);
    }
    
    private final SectionHeader header;
    private final Chars commonName;

    public Section(SectionHeader header, Chars commonName) {
        this.header = header;
        this.commonName = commonName;
    }

    public SectionHeader getHeader() {
        return header;
    }

    public Chars getCommonName() {
        return commonName;
    }

    public void read(DataInputStream ds) throws IOException {
        //do nothing, expect subclasses to override it
    }

    public static Section getSection(SectionHeader header){
        final Chars name = header.Name;
        final Class c = (Class) SECTION_MAP.getValue(name);
        if(c==null){
            return null;
        }
        try {
            return (Section) c.getConstructor(SectionHeader.class).newInstance(header);
        } catch (InstantiationException ex) {
            Loggers.get().log(ex,Logger.LEVEL_INFORMATION);
        } catch (IllegalAccessException ex) {
            Loggers.get().log(ex,Logger.LEVEL_INFORMATION);
        } catch (InvalidArgumentException ex) {
            Loggers.get().log(ex,Logger.LEVEL_INFORMATION);
        } catch (InvocationTargetException ex) {
            Loggers.get().log(ex,Logger.LEVEL_INFORMATION);
        } catch (NoSuchMethodException ex) {
            Loggers.get().log(ex,Logger.LEVEL_INFORMATION);
        }
        return null;
    }
    
}
