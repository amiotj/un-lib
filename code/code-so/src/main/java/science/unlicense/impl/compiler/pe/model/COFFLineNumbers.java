

package science.unlicense.impl.compiler.pe.model;

/**
 *
 * @author Johann Sorel
 */
public class COFFLineNumbers {

    /** 4bytes*/
    public int Type_SymbolTableIndex;
    /** 4bytes*/
    public int Type_VirtualAddress;
    /** 2bytes*/
    public int Linenumber;

}
