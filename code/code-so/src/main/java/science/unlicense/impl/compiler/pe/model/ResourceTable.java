

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ResourceTable extends Section {

    public static final class ResourceDirectoryTable{
        /** 4 bytes */
        public int Characteristics;
        /** 4 bytes */
        public int DateStamp;
        /** 2 bytes */
        public int MajorVersion;
        /** 2 bytes */
        public int MinorVersion;
        /** 2 bytes */
        public int NumberOfNameEntries;
        /** 2 bytes */
        public int NumberOfIDEntries;
    }

    public static final class ResourceDirectoryEntries{
        /** 4 bytes */
        public int NameRVA;
        /** 4 bytes */
        public int IntegerID;
        /** 4 bytes */
        public int DataEntryRVA;
        /** 4 bytes */
        public int SubdirectoryRVA;
    }

    public static final class ResourceDataEntry{
        /** 4 bytes */
        public int DataRVA;
        /** 4 bytes */
        public int Size;
        /** 4 bytes */
        public int Codepage;
        /** 4 bytes */
        public int Reserved;
    }

    public static final class ResourceDirectoryString{
        /** 2 bytes */
        public int Length;
        /** variable length unicode string */
        public Chars string;

    }

    public ResourceTable(SectionHeader header) {
        super(header, new Chars(".rsrc"));
    }

}
