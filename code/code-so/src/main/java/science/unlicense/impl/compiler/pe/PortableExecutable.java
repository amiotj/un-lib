
package science.unlicense.impl.compiler.pe;

import science.unlicense.impl.compiler.pe.model.Section;

/**
 *
 * @author Johann Sorel
 */
public class PortableExecutable {

    public DosHeader dosHeader;
    public DosCode dosCode;

    public PEHeader peHeader;

    public SectionHeader[] sectionHeaders;
    public Section[] sections;

}
