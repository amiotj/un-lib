
package science.unlicense.impl.compiler.pe;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class SectionHeader extends CObject {

    /** BYTE  Name[IMAGE_SIZEOF_SHORT_NAME]; */
    public Chars Name;
    /**
     * union {
     *   DWORD PhysicalAddress;
     *   DWORD VirtualSize;
     * } Misc;
     */
    public int misc;
    /** DWORD */
    public int VirtualAddress;
    /** DWORD */
    public int SizeOfRawData;
    /** DWORD */
    public int PointerToRawData;
    /** DWORD */
    public int PointerToRelocations;
    /** DWORD */
    public int PointerToLinenumbers;
    /** WORD */
    public short NumberOfRelocations;
    /** WORD */
    public short NumberOfLinenumbers;
    /** DWORD */
    public int Characteristics;

    public void read(DataInputStream ds) throws IOException {
        Name = ds.readBlockZeroTerminatedChars(8, CharEncodings.US_ASCII);
        misc = ds.readInt();
        VirtualAddress = ds.readInt();
        SizeOfRawData = ds.readInt();
        PointerToRawData = ds.readInt();
        PointerToRelocations = ds.readInt();
        PointerToLinenumbers = ds.readInt();
        NumberOfRelocations = ds.readShort();
        NumberOfLinenumbers = ds.readShort();
        Characteristics = ds.readInt();
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("SectionHeader:\n");
        cb.append("- Name=" + Name).append('\n');
        cb.append("- misc=" + misc).append('\n');
        cb.append("- VirtualAddress=" + VirtualAddress).append('\n');
        cb.append("- SizeOfRawData=" + SizeOfRawData).append('\n');
        cb.append("- PointerToRawData=" + PointerToRawData).append('\n');
        cb.append("- PointerToRelocations=" + PointerToRelocations).append('\n');
        cb.append("- PointerToLinenumbers=" + PointerToLinenumbers).append('\n');
        cb.append("- NumberOfRelocations=" + NumberOfRelocations).append('\n');
        cb.append("- NumberOfLinenumbers=" + NumberOfLinenumbers).append('\n');
        cb.append("- Characteristics=" + Characteristics).append('\n');
        return cb.toChars();
    }

}
