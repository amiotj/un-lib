

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class GPInitializedData extends Section {

    public GPInitializedData(SectionHeader header) {
        super(header, new Chars(".sdata"));
    }

}
