

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ReadOnlyInitializedData extends Section {

    public ReadOnlyInitializedData(SectionHeader header) {
        super(header, new Chars(".rdata"));
    }

}
