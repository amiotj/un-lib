

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class SymbolTable extends Section {

    public static final class Symbol{
        /** 8bytes */
        public Chars Name;
        /** 4bytes */
        public int Value;
        /** 2bytes */
        public short SectionNumber;
        /** 2bytes */
        public short Type;
        /** 1byte */
        public byte StorageClass;
        /** 1byte */
        public byte NumberOfAuxSymbols;
    }

    public static final class SymbolNameRepresentation{
        /** 8bytes : 0 terminated UTF-8 encoding */
        public Chars ShortName;
        /** 4bytes */
        public int Zeroes;
        /** 4bytes */
        public int Offset;
    }

    public SymbolTable(SectionHeader header) {
        super(header, new Chars(".file"));
    }

}
