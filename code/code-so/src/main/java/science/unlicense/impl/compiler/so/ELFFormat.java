
package science.unlicense.impl.compiler.so;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * Informations :
 * http://en.wikipedia.org/wiki/Executable_and_Linkable_Format
 * http://man7.org/linux/man-pages/man5/elf.5.html
 * http://wiki.osdev.org/ELF
 * https://refspecs.linuxbase.org/
 * 
 * @author Johann Sorel
 */
public class ELFFormat extends DefaultFormat {

    public static final ELFFormat INSTANCE = new ELFFormat();

    private ELFFormat() {
        super(new Chars("ELF"),
              new Chars("ELF"),
              new Chars("Executable and Linkable Format"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("axf"),
                  new Chars("bin"),
                  new Chars("elf"),
                  new Chars("o"),
                  new Chars("prx"),
                  new Chars("puff"),
                  new Chars("so")
              },
              new byte[][]{ELFConstants.SIGNATURE});
    }

}
