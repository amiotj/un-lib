

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class LinkerOptions extends Section {

    public LinkerOptions(SectionHeader header) {
        super(header, new Chars(".drective"));
    }

}
