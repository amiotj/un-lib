

package science.unlicense.impl.compiler.pe.model;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.compiler.pe.SectionHeader;

/**
 *
 * @author Johann Sorel
 */
public class ExceptionTable extends Section {

    /**
     * for 32-bits MIPS
     */
    public static final class MIPSFunction{
        /** 4bytes */
        public int BeginAddress;
        /** 4bytes */
        public int EndAddress;
        /** 4bytes */
        public int ExceptionHandler;
        /** 4bytes */
        public int HandlerData;
        /** 4bytes */
        public int PrologEndAddress;
    }

    /**
     * for ARM PowerPC SH3 SH4
     */
    public static final class ARMFunction{
        /** 4bytes */
        public int BeginAddress;
        /** 8 bits */
        public int PrologLength;
        /** 22 bits */
        public int FunctionLength;
        /** 1 bit */
        public int b32Flag;
        /** 1 bit */
        public int ExceptionFlag;
    }

    /**
     * for x64 and itanium
     */
    public static final class X64Function{
        /** 4bytes */
        public int BeginAddress;
        /** 4bytes */
        public int EndAddress;
        /** 4bytes */
        public int UnwindInformation;
    }

    /**
     *
     */
    public static final class ARMv7Function{
        /** 4bytes */
        public int BeginAddress;
        /** 4bytes */
        public int UnwindInformation;
    }

    public ExceptionTable(SectionHeader header) {
        super(header, new Chars(".pdata"));
    }

}
