
package science.unlicense.impl.code.java.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.meta.DefaultMeta;
import science.unlicense.api.code.meta.Scope;
import science.unlicense.api.number.Int32;

/**
 * Java scopes.
 * 
 * @author Johann Sorel
 */
public class JavaScope extends DefaultMeta implements Scope{

    public static final Chars ID = new Chars("java-scope");
    public static final int TYPE_PUBLIC = 0;
    public static final int TYPE_PACKAGE = 1;
    public static final int TYPE_PROTECTED = 2;
    public static final int TYPE_PRIVATE = 3;
    
    public static final JavaScope PUBLIC = new JavaScope(TYPE_PUBLIC);
    public static final JavaScope PACKAGE = new JavaScope(TYPE_PACKAGE);
    public static final JavaScope PROTECTED = new JavaScope(TYPE_PROTECTED);
    public static final JavaScope PRIVATE = new JavaScope(TYPE_PRIVATE);

    
    private int type =  TYPE_PUBLIC;
    
    public JavaScope() {
        super(ID);
    }

    public JavaScope(int type) {
        super(ID);
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int getHash() {
        int hash = 7;
        hash = 29 * hash + this.type;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JavaScope other = (JavaScope) obj;
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public Chars toChars() {
        return id.concat(':').concat(Int32.encode(type));
    }
    
}
