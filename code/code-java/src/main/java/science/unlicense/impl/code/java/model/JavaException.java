
package science.unlicense.impl.code.java.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.meta.DefaultMeta;

/**
 *
 * @author Johann Sorel
 */
public class JavaException extends DefaultMeta{

    public static final Chars ID = new Chars("java-exception");

    private JavaClass exceptionClass;

    public JavaException() {
        super(ID);
    }

    public JavaException(JavaClass exceptionClass) {
        super(ID);
        this.exceptionClass = exceptionClass;
    }

    public JavaClass getExceptionClass() {
        return exceptionClass;
    }

    public void setExceptionClass(JavaClass exceptionClass) {
        this.exceptionClass = exceptionClass;
    }



}
