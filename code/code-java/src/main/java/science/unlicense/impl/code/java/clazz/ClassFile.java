
package science.unlicense.impl.code.java.clazz;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class ClassFile {

    public int             minor_version;
    public int             major_version;
    public Constant[]      constant_pool;
    public int             access_flags;
    public int             this_class;
    public int             super_class;
    public int[]           interfaces;
    public FieldInfo[]     fields;
    public MethodInfo[]    methods;
    public AttributeInfo[] attributes;

    public void read(DataInputStream ds) throws IOException {
        if(ds.readInt() != ClassConstants.SIGNATURE){
            throw new IOException("Invalid class file signature");
        }

        minor_version = ds.readUShort();
        major_version = ds.readUShort();

        final int constant_pool_count = ds.readUShort();
        constant_pool = new Constant[constant_pool_count-1];
        for(int i=0;i<constant_pool.length;i++){
            final int type = ds.readUByte();
            switch(type){
                case ClassConstants.CONSTANT_CLASS              : constant_pool[i] = new ClassConstant(); break;
                case ClassConstants.CONSTANT_FIELDREF           : constant_pool[i] = new FieldRedConstant(); break;
                case ClassConstants.CONSTANT_METHODREF          : constant_pool[i] = new MethodRefConstant(); break;
                case ClassConstants.CONSTANT_INTERFACEMETHODREF : constant_pool[i] = new InterfaceMethodRefConstant(); break;
                case ClassConstants.CONSTANT_STRING             : constant_pool[i] = new StringConstant(); break;
                case ClassConstants.CONSTANT_INTEGER            : constant_pool[i] = new IntegerConstant(); break;
                case ClassConstants.CONSTANT_FLOAT              : constant_pool[i] = new FloatConstant(); break;
                case ClassConstants.CONSTANT_LONG               : constant_pool[i] = new LongConstant(); break;
                case ClassConstants.CONSTANT_DOUBLE             : constant_pool[i] = new DoubleConstant(); break;
                case ClassConstants.CONSTANT_NAMEANDTYPE        : constant_pool[i] = new NameAndTypeConstant(); break;
                case ClassConstants.CONSTANT_UTF8               : constant_pool[i] = new UTF8Constant(); break;
                case ClassConstants.CONSTANT_METHODHANDLE       : constant_pool[i] = new MethodHandleConstant(); break;
                case ClassConstants.CONSTANT_METHODTYPE         : constant_pool[i] = new MethodTypeConstant(); break;
                case ClassConstants.CONSTANT_INVOKEDYNAMIC      : constant_pool[i] = new InvokeDynamicConstant(); break;
                default: throw new IOException("Unkown constant type : "+type);
            }
            constant_pool[i].read(ds);
        }

        access_flags = ds.readUShort();
        this_class   = ds.readUShort();
        super_class  = ds.readUShort();

        final int interfaces_count = ds.readUShort();
        interfaces = ds.readUShort(interfaces_count);

        final int fields_count = ds.readUShort();
        fields = new FieldInfo[fields_count];
        for(int i=0;i<fields_count;i++){
            fields[i] = new FieldInfo();
            fields[i].read(ds);
        }


        final int methods_count = ds.readUShort();
        methods = new MethodInfo[methods_count];
        for(int i=0;i<methods_count;i++){
            methods[i] = new MethodInfo();
            methods[i].read(ds);
        }

        final int attributes_count = ds.readUShort();
        attributes = new AttributeInfo[attributes_count];
        for(int i=0;i<attributes_count;i++){
            attributes[i] = new AttributeInfo();
            attributes[i].read(ds);
        }
    }


    public static abstract class Constant{
        public abstract void read(DataInputStream ds) throws IOException;
    }

    public static final class ClassConstant extends Constant{
        public int name_index;

        public void read(DataInputStream ds) throws IOException {
            name_index = ds.readUShort();
        }
    }

    public static final class FieldRedConstant extends Constant{
        public int class_index;
        public int name_and_type_index;

        public void read(DataInputStream ds) throws IOException {
            class_index = ds.readUShort();
            name_and_type_index = ds.readUShort();
        }
    }

    public static final class MethodRefConstant extends Constant{
        public int class_index;
        public int name_and_type_index;

        public void read(DataInputStream ds) throws IOException {
            class_index = ds.readUShort();
            name_and_type_index = ds.readUShort();
        }
    }

    public static final class InterfaceMethodRefConstant extends Constant{
        public int class_index;
        public int name_and_type_index;

        public void read(DataInputStream ds) throws IOException {
            class_index = ds.readUShort();
            name_and_type_index = ds.readUShort();
        }
    }

    public static final class StringConstant extends Constant{
        public int string_index;

        public void read(DataInputStream ds) throws IOException {
            string_index = ds.readUShort();
        }
    }

    public static final class IntegerConstant extends Constant{
        public int value;

        public void read(DataInputStream ds) throws IOException {
            value = ds.readInt();
        }
    }

    public static final class FloatConstant extends Constant{
        public float value;

        public void read(DataInputStream ds) throws IOException {
            value = ds.readFloat();
        }
    }

    public static final class LongConstant extends Constant{
        public long value;

        public void read(DataInputStream ds) throws IOException {
            value = ds.readLong();
        }
    }

    public static final class DoubleConstant extends Constant{
        public double value;

        public void read(DataInputStream ds) throws IOException {
            value = ds.readDouble();
        }
    }

    public static final class NameAndTypeConstant extends Constant{
        public int name_index;
        public int descriptor_index;

        public void read(DataInputStream ds) throws IOException {
            name_index = ds.readUShort();
            descriptor_index = ds.readUShort();
        }
    }

    public static final class UTF8Constant extends Constant{
        public Chars value;

        public void read(DataInputStream ds) throws IOException {
            final int length = ds.readUShort();
            value = new Chars(ds.readFully(new byte[length]),CharEncodings.UTF_8);
        }
    }

    public static final class MethodHandleConstant extends Constant{
        public int reference_kind;
        public int reference_index;

        public void read(DataInputStream ds) throws IOException {
            reference_kind = ds.readUByte();
            reference_index = ds.readUShort();
        }
    }

    public static final class MethodTypeConstant extends Constant{
        public int descriptor_index;

        public void read(DataInputStream ds) throws IOException {
            descriptor_index = ds.readUShort();
        }
    }

    public static final class InvokeDynamicConstant extends Constant{
        public int bootstrap_method_attr_index;
        public int name_and_type_index;

        public void read(DataInputStream ds) throws IOException {
            bootstrap_method_attr_index = ds.readUShort();
            name_and_type_index = ds.readUShort();
        }
    }


    public static final class FieldInfo{
        public int             access_flags;
        public int             name_index;
        public int             descriptor_index;
        public AttributeInfo[] attributes;

        public void read(DataInputStream ds) throws IOException {
            access_flags     = ds.readUShort();
            name_index       = ds.readUShort();
            descriptor_index = ds.readUShort();
            attributes = new AttributeInfo[ds.readUShort()];
            for(int i=0;i<attributes.length;i++){
                attributes[i] = new AttributeInfo();
                attributes[i].read(ds);
            }
        }
    }

    public static final class MethodInfo{
        public int             access_flags;
        public int             name_index;
        public int             descriptor_index;
        public AttributeInfo[] attributes;

        public void read(DataInputStream ds) throws IOException {
            access_flags     = ds.readUShort();
            name_index       = ds.readUShort();
            descriptor_index = ds.readUShort();
            attributes = new AttributeInfo[ds.readUShort()];
            for(int i=0;i<attributes.length;i++){
                attributes[i] = new AttributeInfo();
                attributes[i].read(ds);
            }
        }
    }

    public static final class AttributeInfo{
        public int attribute_name_index;
        public byte[] info;

        public void read(DataInputStream ds) throws IOException {
            attribute_name_index = ds.readUShort();
            final int attribute_length = ds.readUShort();
            info = ds.readFully(new byte[attribute_length]);
        }

    }

}
