
package science.unlicense.impl.code.java;

import science.unlicense.api.character.Chars;
import science.unlicense.api.character.Char;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.TokenGroup;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.parser.Rule;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public final class JavaConstants {

    public static final Char  SPACE = new Char(' ');
    public static final Chars SPACE2 = new Chars("  ");
    public static final Chars SPACE4 = new Chars("    ");
    public static final Char  NL = new Char('\n');
    public static final Chars JAVADOC_START = new Chars("/**");
    public static final Chars JAVADOC = new Chars("*");
    public static final Chars JAVADOC_END = new Chars("*/");
    public static final Chars ONELINE_COMMENT = new Chars("//");

    public static final Char SEMICOLON = new Char(';');
    public static final Char EQUAL = new Char('=');
    public static final Char NOT = new Char('!');
    public static final Char AND = new Char('&');
    public static final Char OR = new Char('|');
    public static final Char PLUS = new Char('+');
    public static final Char MINUS = new Char('-');
    public static final Char MULTIPLY = new Char('*');
    public static final Char DIVIDE = new Char('/');
    public static final Char LSHIFT = new Char('<');
    public static final Char RSHIFT = new Char('>');
    public static final Char ANNOTATION = new Char('@');
    public static final Char POWER = new Char('^');
    public static final Char SINGLEQUOTE = new Char('\'');
    public static final Char DOUBLEQUOTE = new Char('"');

    public static final Chars EQUAL2 = new Chars("==");
    public static final Chars AND2 = new Chars("&&");
    public static final Chars OR2 = new Chars("&&");
    public static final Chars LSHIFT2 = new Chars("<<");
    public static final Chars LSHIFT3 = new Chars("<<<");
    public static final Chars RSHIFT2 = new Chars(">>");
    public static final Chars RSHIFT3 = new Chars(">>>");

    public static final Char LPAREN = new Char('(');
    public static final Char RPAREN = new Char(')');
    public static final Char LBRACE = new Char('{');
    public static final Char RBRACE = new Char('}');
    public static final Char LBRACKET = new Char('[');
    public static final Char RBRACKET = new Char(']');

    public static final Chars ABSTRACT = new Chars("abstract");
    public static final Chars ASSERT = new Chars("assert");
    public static final Chars BOOLEAN = new Chars("boolean");
    public static final Chars BREAK = new Chars("break");
    public static final Chars BYTE = new Chars("byte");
    public static final Chars CASE = new Chars("case");
    public static final Chars CATCH = new Chars("catch");
    public static final Chars CHAR = new Chars("char");
    public static final Chars CLASS = new Chars("class");
    public static final Chars CONST = new Chars("const");
    public static final Chars CONTINUE = new Chars("continue");
    public static final Chars DEFAULT = new Chars("default");
    public static final Chars DO = new Chars("do");
    public static final Chars DOUBLE = new Chars("double");
    public static final Chars ELSE = new Chars("else");
    public static final Chars ENUM = new Chars("enum");
    public static final Chars EXTENDS = new Chars("extends");
    public static final Chars FINAL = new Chars("final");
    public static final Chars FINALLY = new Chars("finally");
    public static final Chars FLOAT = new Chars("float");
    public static final Chars FOR = new Chars("for");
    public static final Chars GOTO = new Chars("goto");
    public static final Chars IF = new Chars("if");
    public static final Chars IMPLEMENTS = new Chars("implements");
    public static final Chars IMPORTS = new Chars("import");
    public static final Chars INSTANCEOF = new Chars("instanceof");
    public static final Chars INT = new Chars("int");
    public static final Chars INTERFACE = new Chars("interface");
    public static final Chars LONG = new Chars("long");
    public static final Chars NATIVE = new Chars("native");
    public static final Chars NEW = new Chars("new");
    public static final Chars PACKAGE = new Chars("package");
    public static final Chars PRIVATE = new Chars("private");
    public static final Chars PROTECTED = new Chars("protected");
    public static final Chars PUBLIC = new Chars("public");
    public static final Chars RETURN = new Chars("return");
    public static final Chars SHORT = new Chars("short");
    public static final Chars STATIC = new Chars("static");
    public static final Chars STRICTFP = new Chars("strictfp");
    public static final Chars SUPER = new Chars("super");
    public static final Chars SWITCH = new Chars("switch");
    public static final Chars SYNCHRONIZED = new Chars("synchronized");
    public static final Chars THIS = new Chars("this");
    public static final Chars THROW = new Chars("throw");
    public static final Chars THROWS = new Chars("throws");
    public static final Chars TRANSIENT = new Chars("transient");
    public static final Chars TRY = new Chars("try");
    public static final Chars VOID = new Chars("void");
    public static final Chars VOLATILE = new Chars("volatile");
    public static final Chars WHILE = new Chars("while");
    public static final Chars FALSE = new Chars("false");
    public static final Chars NULL = new Chars("null");
    public static final Chars TRUE = new Chars("true");

    public static final Chars[] ALL_KEYWORDS = {
            ABSTRACT,
            ASSERT,
            BOOLEAN,
            BREAK,
            BYTE,
            CASE,
            CATCH,
            CHAR,
            CLASS,
            CONST,
            CONTINUE,
            DEFAULT,
            DO,
            DOUBLE,
            ELSE,
            ENUM,
            EXTENDS,
            FINAL,
            FINALLY,
            FLOAT,
            FOR,
            GOTO,
            IF,
            IMPLEMENTS,
            IMPORTS,
            INSTANCEOF,
            INT,
            INTERFACE,
            LONG,
            NATIVE,
            NEW,
            PACKAGE,
            PRIVATE,
            PROTECTED,
            PUBLIC,
            RETURN,
            SHORT,
            STATIC,
            STRICTFP,
            SUPER,
            SWITCH,
            SYNCHRONIZED,
            THIS,
            THROW,
            THROWS,
            TRANSIENT,
            TRY,
            VOID,
            VOLATILE,
            WHILE,
            FALSE,
            NULL,
            TRUE
            };
    
    
    public static final TokenType TOKEN_SEMI,TOKEN_COM,TOKEN_DOT,TOKEN_ANNOTE,
            TOKEN_DDOTS,TOKEN_CHOICE,TOKEN_MUL,TOKEN_DIV,TOKEN_PLUS,TOKEN_MIN,
            TOKEN_NEG,TOKEN_MOD,TOKEN_INC,TOKEN_DEC,TOKEN_POW,TOKEN_EQUAL,
            TOKEN_EQUALEQUAL,TOKEN_GREAT,TOKEN_GREATEQUAL,TOKEN_LOWER,
            TOKEN_LOWEREQUAL,TOKEN_NOTEQUAL,TOKEN_PLUSEQUAL,TOKEN_MINEQUAL,
            TOKEN_DIVEQUAL,TOKEN_MULEQUAL,TOKEN_ANDEQUAL,TOKEN_OREQUAL,
            TOKEN_POWEQUAL,TOKEN_MODEQUAL,TOKEN_LSHIFT2EQUAL,TOKEN_RSHIFT2EQUAL,
            TOKEN_RSHIFT3EQUAL,TOKEN_ANYNUM,TOKEN_OR1,TOKEN_OR2,TOKEN_AND1,
            TOKEN_AND2,TOKEN_NOT,TOKEN_LP,TOKEN_RP,TOKEN_LA,TOKEN_RA,TOKEN_LB,
            TOKEN_RB,TOKEN_ABSTRACT,TOKEN_ASSERT,TOKEN_BOOLEAN,TOKEN_BREAK,
            TOKEN_BYTE,TOKEN_CASE,TOKEN_CATCH,TOKEN_CHAR,TOKEN_CLASS,TOKEN_CONST,
            TOKEN_CONTINUE,TOKEN_DEFAULT,TOKEN_DO,TOKEN_DOUBLE,TOKEN_ELSE,
            TOKEN_ELSEIF,TOKEN_ENUM,TOKEN_EXTENDS,TOKEN_FINAL,TOKEN_FINALLY,
            TOKEN_FLOAT,TOKEN_FOR,TOKEN_GOTO,TOKEN_IF,TOKEN_IMPLEMENTS,
            TOKEN_IMPORT,TOKEN_INSTANCEOF,TOKEN_INT,TOKEN_INTERFACE,TOKEN_LONG,
            TOKEN_NATIVE,TOKEN_NEW,TOKEN_PACKAGE,TOKEN_PRIVATE,TOKEN_PROTECTED,
            TOKEN_PUBLIC,TOKEN_RETURN,TOKEN_SHORT,TOKEN_STATIC,TOKEN_STRICTFP,
            TOKEN_SUPER,TOKEN_SWITCH,TOKEN_SYNCHRONIZED,TOKEN_THIS,TOKEN_THROW,
            TOKEN_THROWS,TOKEN_TRANSIENT,TOKEN_TRY,TOKEN_VOID,TOKEN_VOLATILE,
            TOKEN_WHILE,TOKEN_FALSE,TOKEN_NULL,TOKEN_TRUE,TOKEN_WS,TOKEN_COMMENT1,
            TOKEN_COMMENT2,TOKEN_WORD,TOKEN_NUMBER,TOKEN_STRING,TOKEN_CHARVAL,
            TOKEN_HEXA;
    
    public static final Rule RULE_WS,RULE_PRIMITIVE,RULE_REF,RULE_IMPORTREF,
            RULE_PACKAGE,RULE_IMPORT,RULE_COMMENT,RULE_SCOPE,RULE_BITOP,
            RULE_BOOLOP,RULE_OP,RULE_TYPE,RULE_ARRAYVALUES,RULE_ARRAYTYPEEMPTY,
            RULE_ARRAYTYPE,RULE_ARRAYTPENEW,RULE_PARAMETER,RULE_PARAMETERS,
            RULE_NEWOBJECT,RULE_NEWARRAY,RULE_VARIABLE_EVAL,RULE_VARIABLE,
            RULE_SUFFIX_CALL,RULE_SUFFIX_AFF,RULE_PREFIX,RULE_CAST,RULE_LITERAL,
            RULE_EXP_GROUP,RULE_SEG,RULE_SEG_CALL,RULE_SEG_ARRAY,RULE_EXPPATH,
            RULE_EXPATOM,RULE_EXP1,RULE_EXP0,RULE_EXP,RULE_ARGUMENTS,
            RULE_AFFECTATION,RULE_GENERICELE,RULE_GENERIC,RULE_IF,RULE_FOR,
            RULE_FOREACH,RULE_WHILE,RULE_DOWHILE,RULE_SWITCH,RULE_CATCH,
            RULE_FINALLY,RULE_TRY,RULE_SYNC,RULE_SUBBLOCK,RULE_BRANCHING,
            RULE_RETURN,RULE_THROW,RULE_ASSERT,RULE_LABEL,RULE_STATEMENT,
            RULE_STATEMENTS,RULE_BLOCK,RULE_QUALIFIER,RULE_ANNOTATION,
            RULE_ANNOTATIONS,RULE_THROWS,RULE_FUNCTION,RULE_EXTENDS,
            RULE_IMPLEMENTS,RULE_CLASSBLOCK,RULE_CLASSCONTENT,RULE_CLASS,
            RULE_INTERFACE,RULE_ENUMVAL,RULE_ENUMVALS,RULE_ENUM,RULE_FILE;
    
    
    private static final TokenGroup TOKENS;
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    static {
        try{
            //parse grammar
            final UNGrammarReader reader = new UNGrammarReader();
            final HashDictionary tokenGroups = new HashDictionary();
            reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/java.gr")));
            reader.read(tokenGroups, RULES);
            
            TOKENS = (TokenGroup) tokenGroups.getValue(new Chars("DEFAULT"));
            
            TOKEN_SEMI = (TokenType)TOKENS.getValue(new Chars("SEMI"));
            TOKEN_COM = (TokenType)TOKENS.getValue(new Chars("COM"));
            TOKEN_DOT = (TokenType)TOKENS.getValue(new Chars("DOT"));
            TOKEN_ANNOTE = (TokenType)TOKENS.getValue(new Chars("ANNOTE"));
            TOKEN_DDOTS = (TokenType)TOKENS.getValue(new Chars("DDOTS"));
            TOKEN_CHOICE = (TokenType)TOKENS.getValue(new Chars("CHOICE"));
            TOKEN_MUL = (TokenType)TOKENS.getValue(new Chars("MUL"));
            TOKEN_DIV = (TokenType)TOKENS.getValue(new Chars("DIV"));
            TOKEN_PLUS = (TokenType)TOKENS.getValue(new Chars("PLUS"));
            TOKEN_MIN = (TokenType)TOKENS.getValue(new Chars("MIN"));
            TOKEN_NEG = (TokenType)TOKENS.getValue(new Chars("NEG"));
            TOKEN_MOD = (TokenType)TOKENS.getValue(new Chars("MOD"));
            TOKEN_INC = (TokenType)TOKENS.getValue(new Chars("INC"));
            TOKEN_DEC = (TokenType)TOKENS.getValue(new Chars("DEC"));
            TOKEN_POW = (TokenType)TOKENS.getValue(new Chars("POW"));
            TOKEN_EQUAL = (TokenType)TOKENS.getValue(new Chars("EQUAL"));
            TOKEN_EQUALEQUAL = (TokenType)TOKENS.getValue(new Chars("EQUALEQUAL"));
            TOKEN_GREAT = (TokenType)TOKENS.getValue(new Chars("GREAT"));
            TOKEN_GREATEQUAL = (TokenType)TOKENS.getValue(new Chars("GREATEQUAL"));
            TOKEN_LOWER = (TokenType)TOKENS.getValue(new Chars("LOWER"));
            TOKEN_LOWEREQUAL = (TokenType)TOKENS.getValue(new Chars("LOWEREQUAL"));
            TOKEN_NOTEQUAL = (TokenType)TOKENS.getValue(new Chars("NOTEQUAL"));
            TOKEN_PLUSEQUAL = (TokenType)TOKENS.getValue(new Chars("PLUSEQUAL"));
            TOKEN_MINEQUAL = (TokenType)TOKENS.getValue(new Chars("MINEQUAL"));
            TOKEN_DIVEQUAL = (TokenType)TOKENS.getValue(new Chars("DIVEQUAL"));
            TOKEN_MULEQUAL = (TokenType)TOKENS.getValue(new Chars("MULEQUAL"));
            TOKEN_ANDEQUAL = (TokenType)TOKENS.getValue(new Chars("ANDEQUAL"));
            TOKEN_OREQUAL = (TokenType)TOKENS.getValue(new Chars("OREQUAL"));
            TOKEN_POWEQUAL = (TokenType)TOKENS.getValue(new Chars("POWEQUAL"));
            TOKEN_MODEQUAL = (TokenType)TOKENS.getValue(new Chars("MODEQUAL"));
            TOKEN_LSHIFT2EQUAL = (TokenType)TOKENS.getValue(new Chars("LSHIFT2EQUAL"));
            TOKEN_RSHIFT2EQUAL = (TokenType)TOKENS.getValue(new Chars("RSHIFT2EQUAL"));
            TOKEN_RSHIFT3EQUAL = (TokenType)TOKENS.getValue(new Chars("RSHIFT3EQUAL"));
            TOKEN_ANYNUM = (TokenType)TOKENS.getValue(new Chars("ANYNUM"));
            TOKEN_OR1 = (TokenType)TOKENS.getValue(new Chars("OR1"));
            TOKEN_OR2 = (TokenType)TOKENS.getValue(new Chars("OR2"));
            TOKEN_AND1 = (TokenType)TOKENS.getValue(new Chars("AND1"));
            TOKEN_AND2 = (TokenType)TOKENS.getValue(new Chars("AND2"));
            TOKEN_NOT = (TokenType)TOKENS.getValue(new Chars("NOT"));
            TOKEN_LP = (TokenType)TOKENS.getValue(new Chars("LP"));
            TOKEN_RP = (TokenType)TOKENS.getValue(new Chars("RP"));
            TOKEN_LA = (TokenType)TOKENS.getValue(new Chars("LA"));
            TOKEN_RA = (TokenType)TOKENS.getValue(new Chars("RA"));
            TOKEN_LB = (TokenType)TOKENS.getValue(new Chars("LB"));
            TOKEN_RB = (TokenType)TOKENS.getValue(new Chars("RB"));
            TOKEN_ABSTRACT = (TokenType)TOKENS.getValue(new Chars("ABSTRACT"));
            TOKEN_ASSERT = (TokenType)TOKENS.getValue(new Chars("ASSERT"));
            TOKEN_BOOLEAN = (TokenType)TOKENS.getValue(new Chars("BOOLEAN"));
            TOKEN_BREAK = (TokenType)TOKENS.getValue(new Chars("BREAK"));
            TOKEN_BYTE = (TokenType)TOKENS.getValue(new Chars("BYTE"));
            TOKEN_CASE = (TokenType)TOKENS.getValue(new Chars("CASE"));
            TOKEN_CATCH = (TokenType)TOKENS.getValue(new Chars("CATCH"));
            TOKEN_CHAR = (TokenType)TOKENS.getValue(new Chars("CHAR"));
            TOKEN_CLASS = (TokenType)TOKENS.getValue(new Chars("CLASS"));
            TOKEN_CONST = (TokenType)TOKENS.getValue(new Chars("CONST"));
            TOKEN_CONTINUE = (TokenType)TOKENS.getValue(new Chars("CONTINUE"));
            TOKEN_DEFAULT = (TokenType)TOKENS.getValue(new Chars("DEFAULT"));
            TOKEN_DO = (TokenType)TOKENS.getValue(new Chars("DO"));
            TOKEN_DOUBLE = (TokenType)TOKENS.getValue(new Chars("DOUBLE"));
            TOKEN_ELSE = (TokenType)TOKENS.getValue(new Chars("ELSE"));
            TOKEN_ELSEIF = (TokenType)TOKENS.getValue(new Chars("ELSEIF"));
            TOKEN_ENUM = (TokenType)TOKENS.getValue(new Chars("ENUM"));
            TOKEN_EXTENDS = (TokenType)TOKENS.getValue(new Chars("EXTENDS"));
            TOKEN_FINAL = (TokenType)TOKENS.getValue(new Chars("FINAL"));
            TOKEN_FINALLY = (TokenType)TOKENS.getValue(new Chars("FINALLY"));
            TOKEN_FLOAT = (TokenType)TOKENS.getValue(new Chars("FLOAT"));
            TOKEN_FOR = (TokenType)TOKENS.getValue(new Chars("FOR"));
            TOKEN_GOTO = (TokenType)TOKENS.getValue(new Chars("GOTO"));
            TOKEN_IF = (TokenType)TOKENS.getValue(new Chars("IF"));
            TOKEN_IMPLEMENTS = (TokenType)TOKENS.getValue(new Chars("IMPLEMENTS"));
            TOKEN_IMPORT = (TokenType)TOKENS.getValue(new Chars("IMPORT"));
            TOKEN_INSTANCEOF = (TokenType)TOKENS.getValue(new Chars("INSTANCEOF"));
            TOKEN_INT = (TokenType)TOKENS.getValue(new Chars("INT"));
            TOKEN_INTERFACE = (TokenType)TOKENS.getValue(new Chars("INTERFACE"));
            TOKEN_LONG = (TokenType)TOKENS.getValue(new Chars("LONG"));
            TOKEN_NATIVE = (TokenType)TOKENS.getValue(new Chars("NATIVE"));
            TOKEN_NEW = (TokenType)TOKENS.getValue(new Chars("NEW"));
            TOKEN_PACKAGE = (TokenType)TOKENS.getValue(new Chars("PACKAGE"));
            TOKEN_PRIVATE = (TokenType)TOKENS.getValue(new Chars("PRIVATE"));
            TOKEN_PROTECTED = (TokenType)TOKENS.getValue(new Chars("PROTECTED"));
            TOKEN_PUBLIC = (TokenType)TOKENS.getValue(new Chars("PUBLIC"));
            TOKEN_RETURN = (TokenType)TOKENS.getValue(new Chars("RETURN"));
            TOKEN_SHORT = (TokenType)TOKENS.getValue(new Chars("SHORT"));
            TOKEN_STATIC = (TokenType)TOKENS.getValue(new Chars("STATIC"));
            TOKEN_STRICTFP = (TokenType)TOKENS.getValue(new Chars("STRICTFP"));
            TOKEN_SUPER = (TokenType)TOKENS.getValue(new Chars("SUPER"));
            TOKEN_SWITCH = (TokenType)TOKENS.getValue(new Chars("SWITCH"));
            TOKEN_SYNCHRONIZED = (TokenType)TOKENS.getValue(new Chars("SYNCHRONIZED"));
            TOKEN_THIS = (TokenType)TOKENS.getValue(new Chars("THIS"));
            TOKEN_THROW = (TokenType)TOKENS.getValue(new Chars("THROW"));
            TOKEN_THROWS = (TokenType)TOKENS.getValue(new Chars("THROWS"));
            TOKEN_TRANSIENT = (TokenType)TOKENS.getValue(new Chars("TRANSIENT"));
            TOKEN_TRY = (TokenType)TOKENS.getValue(new Chars("TRY"));
            TOKEN_VOID = (TokenType)TOKENS.getValue(new Chars("VOID"));
            TOKEN_VOLATILE = (TokenType)TOKENS.getValue(new Chars("VOLATILE"));
            TOKEN_WHILE = (TokenType)TOKENS.getValue(new Chars("WHILE"));
            TOKEN_FALSE = (TokenType)TOKENS.getValue(new Chars("FALSE"));
            TOKEN_NULL = (TokenType)TOKENS.getValue(new Chars("NULL"));
            TOKEN_TRUE = (TokenType)TOKENS.getValue(new Chars("TRUE"));
            TOKEN_WS = (TokenType)TOKENS.getValue(new Chars("WS"));
            TOKEN_COMMENT1 = (TokenType)TOKENS.getValue(new Chars("COMMENT1"));
            TOKEN_COMMENT2 = (TokenType)TOKENS.getValue(new Chars("COMMENT2"));
            TOKEN_WORD = (TokenType)TOKENS.getValue(new Chars("WORD"));
            TOKEN_NUMBER = (TokenType)TOKENS.getValue(new Chars("NUMBER"));
            TOKEN_STRING = (TokenType)TOKENS.getValue(new Chars("STRING"));
            TOKEN_CHARVAL = (TokenType)TOKENS.getValue(new Chars("CHARVAL"));
            TOKEN_HEXA = (TokenType)TOKENS.getValue(new Chars("HEXA"));
            
            RULE_WS = (Rule) RULES.getValue(new Chars("ws"));
            RULE_PRIMITIVE = (Rule) RULES.getValue(new Chars("primitive"));
            RULE_REF = (Rule) RULES.getValue(new Chars("ref"));
            RULE_IMPORTREF = (Rule) RULES.getValue(new Chars("importref"));
            RULE_PACKAGE = (Rule) RULES.getValue(new Chars("package"));
            RULE_IMPORT = (Rule) RULES.getValue(new Chars("import"));
            RULE_COMMENT = (Rule) RULES.getValue(new Chars("comment"));
            RULE_SCOPE = (Rule) RULES.getValue(new Chars("scope"));
            RULE_BITOP = (Rule) RULES.getValue(new Chars("bitop"));
            RULE_BOOLOP = (Rule) RULES.getValue(new Chars("boolop"));
            RULE_OP = (Rule) RULES.getValue(new Chars("op"));
            RULE_TYPE = (Rule) RULES.getValue(new Chars("type"));
            RULE_ARRAYVALUES = (Rule) RULES.getValue(new Chars("arrayvalues"));
            RULE_ARRAYTYPEEMPTY = (Rule) RULES.getValue(new Chars("arraytypeempty"));
            RULE_ARRAYTYPE = (Rule) RULES.getValue(new Chars("arraytype"));
            RULE_ARRAYTPENEW = (Rule) RULES.getValue(new Chars("arraytypenew"));
            RULE_PARAMETER = (Rule) RULES.getValue(new Chars("parameter"));
            RULE_PARAMETERS = (Rule) RULES.getValue(new Chars("parameters"));
            RULE_NEWOBJECT = (Rule) RULES.getValue(new Chars("newobject"));
            RULE_NEWARRAY = (Rule) RULES.getValue(new Chars("newarray"));
            RULE_VARIABLE_EVAL = (Rule) RULES.getValue(new Chars("variableval"));
            RULE_VARIABLE = (Rule) RULES.getValue(new Chars("variable"));
            RULE_SUFFIX_CALL = (Rule) RULES.getValue(new Chars("suffix_call"));
            RULE_SUFFIX_AFF = (Rule) RULES.getValue(new Chars("suffix_aff"));
            RULE_PREFIX = (Rule) RULES.getValue(new Chars("prefix"));
            RULE_CAST = (Rule) RULES.getValue(new Chars("cast"));
            RULE_LITERAL = (Rule) RULES.getValue(new Chars("literal"));
            RULE_EXP_GROUP = (Rule) RULES.getValue(new Chars("expgroup"));
            RULE_SEG = (Rule) RULES.getValue(new Chars("seg"));
            RULE_SEG_CALL = (Rule) RULES.getValue(new Chars("seg_call"));
            RULE_SEG_ARRAY = (Rule) RULES.getValue(new Chars("seg_array"));
            RULE_EXPPATH = (Rule) RULES.getValue(new Chars("exp_path"));
            RULE_EXPATOM = (Rule) RULES.getValue(new Chars("exp_atom"));
            RULE_EXP1 = (Rule) RULES.getValue(new Chars("exp_1"));
            RULE_EXP0 = (Rule) RULES.getValue(new Chars("exp_0"));
            RULE_EXP = (Rule) RULES.getValue(new Chars("exp"));
            RULE_ARGUMENTS = (Rule) RULES.getValue(new Chars("arguments"));
            RULE_AFFECTATION = (Rule) RULES.getValue(new Chars("affectation"));
            RULE_GENERICELE = (Rule) RULES.getValue(new Chars("genericele"));
            RULE_GENERIC = (Rule) RULES.getValue(new Chars("generic"));
            RULE_IF = (Rule) RULES.getValue(new Chars("if"));
            RULE_FOR = (Rule) RULES.getValue(new Chars("for"));
            RULE_FOREACH = (Rule) RULES.getValue(new Chars("foreach"));
            RULE_WHILE = (Rule) RULES.getValue(new Chars("while"));
            RULE_DOWHILE = (Rule) RULES.getValue(new Chars("dowhile"));
            RULE_SWITCH = (Rule) RULES.getValue(new Chars("switch"));
            RULE_CATCH = (Rule) RULES.getValue(new Chars("catch"));
            RULE_FINALLY = (Rule) RULES.getValue(new Chars("finally"));
            RULE_TRY = (Rule) RULES.getValue(new Chars("try"));
            RULE_SYNC = (Rule) RULES.getValue(new Chars("sync"));
            RULE_SUBBLOCK = (Rule) RULES.getValue(new Chars("subblock"));
            RULE_BRANCHING = (Rule) RULES.getValue(new Chars("branching"));
            RULE_RETURN = (Rule) RULES.getValue(new Chars("return"));
            RULE_THROW = (Rule) RULES.getValue(new Chars("throw"));
            RULE_ASSERT = (Rule) RULES.getValue(new Chars("assert"));
            RULE_LABEL = (Rule) RULES.getValue(new Chars("label"));
            RULE_STATEMENT = (Rule) RULES.getValue(new Chars("statement"));
            RULE_STATEMENTS = (Rule) RULES.getValue(new Chars("statements"));
            RULE_BLOCK = (Rule) RULES.getValue(new Chars("block"));
            RULE_QUALIFIER = (Rule) RULES.getValue(new Chars("qualifier"));
            RULE_ANNOTATION = (Rule) RULES.getValue(new Chars("annotation"));
            RULE_ANNOTATIONS = (Rule) RULES.getValue(new Chars("annotations"));
            RULE_THROWS = (Rule) RULES.getValue(new Chars("throws"));
            RULE_FUNCTION = (Rule) RULES.getValue(new Chars("function"));
            RULE_EXTENDS = (Rule) RULES.getValue(new Chars("extends"));
            RULE_IMPLEMENTS = (Rule) RULES.getValue(new Chars("implements"));
            RULE_CLASSBLOCK = (Rule) RULES.getValue(new Chars("classblock"));
            RULE_CLASSCONTENT = (Rule) RULES.getValue(new Chars("classcontent"));
            RULE_CLASS = (Rule) RULES.getValue(new Chars("class"));
            RULE_INTERFACE = (Rule) RULES.getValue(new Chars("interface"));
            RULE_ENUMVAL = (Rule) RULES.getValue(new Chars("enumval"));
            RULE_ENUMVALS = (Rule) RULES.getValue(new Chars("enumvals"));
            RULE_ENUM = (Rule) RULES.getValue(new Chars("enum"));
            RULE_FILE = (Rule) RULES.getValue(new Chars("file"));
            
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    
    private JavaConstants(){}
    
    
}