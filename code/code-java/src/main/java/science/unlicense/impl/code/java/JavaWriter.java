
package science.unlicense.impl.code.java;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.Function;
import science.unlicense.api.code.Parameter;
import science.unlicense.api.code.Property;
import science.unlicense.api.code.inst.Affectation;
import science.unlicense.api.code.inst.Call;
import science.unlicense.api.code.inst.Declaration;
import science.unlicense.api.code.inst.Forward;
import science.unlicense.api.code.inst.Instruction;
import science.unlicense.api.code.inst.Jump;
import science.unlicense.api.code.inst.Label;
import science.unlicense.api.code.inst.Return;
import science.unlicense.api.code.meta.Meta;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.CharOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.code.java.model.JavaClass;
import science.unlicense.impl.code.java.model.JavaDocumentation;
import science.unlicense.impl.code.java.model.JavaException;
import science.unlicense.impl.code.java.model.JavaScope;

/**
 *
 * @author Johann Sorel
 */
public class JavaWriter extends AbstractWriter{

    private CharOutputStream stream;

    public void write(JavaClass clazz) throws IOException{

        stream = getOutputAsCharStream(CharEncodings.UTF_8);

        final Chars pack = clazz.getPackage();
        final Chars name = clazz.getShortName();

        //write package
        stream.write(JavaConstants.PACKAGE);
        stream.write(JavaConstants.SPACE);
        stream.write(pack);
        stream.write(JavaConstants.SEMICOLON);
        stream.write(JavaConstants.NL);
        stream.write(JavaConstants.NL);
        //write description
        writeDocumentation(clazz.getDocumentation(), 0);
        //write name
        stream.write(JavaConstants.PUBLIC);
        stream.write(JavaConstants.SPACE);
        if(clazz.isInterface()){
            stream.write(JavaConstants.INTERFACE);
        }else if(clazz.isEnum()){
            stream.write(JavaConstants.ENUM);
        }else if(clazz.isAbstract()){
            stream.write(JavaConstants.ABSTRACT);
            stream.write(JavaConstants.SPACE);
            stream.write(JavaConstants.CLASS);
        }else{
            stream.write(JavaConstants.CLASS);
        }        
        stream.write(JavaConstants.SPACE);
        stream.write(name);
        
        //write parent classes
        boolean firstParent = true;
        for(int i=0,n=clazz.parents.getSize();i<n;i++){
            final JavaClass parent = (JavaClass) clazz.parents.get(i);
            if(clazz.isInterface()){
                if(!parent.isInterface()){
                    throw new IOException("Java interfaces can only have interface parents.");
                }else{
                    if(firstParent){
                        stream.write(JavaConstants.SPACE);
                        stream.write(JavaConstants.EXTENDS);
                        stream.write(JavaConstants.SPACE);
                        stream.write(parent.id);
                    }else{
                        stream.write(',');
                        stream.write(JavaConstants.SPACE);
                        stream.write(parent.id);
                    }
                }
            }else{
                if(!parent.isInterface()){
                    if(firstParent){
                        stream.write(JavaConstants.SPACE);
                        stream.write(JavaConstants.EXTENDS);
                        stream.write(JavaConstants.SPACE);
                        stream.write(parent.id);
                    }else{
                        throw new IOException("Java classes can only have one parent.");
                    }
                }
            }
        }
        //write parent interfaces
        if(!clazz.isInterface()){
            boolean firstInterface = true;
            for(int i=0,n=clazz.parents.getSize();i<n;i++){
                final JavaClass parent = (JavaClass) clazz.parents.get(i);
                if(parent.isInterface()){
                    if(firstInterface){
                        stream.write(JavaConstants.SPACE);
                        stream.write(JavaConstants.IMPLEMENTS);
                        stream.write(JavaConstants.SPACE);
                        stream.write(parent.id);
                    }else{
                        stream.write(',');
                        stream.write(JavaConstants.SPACE);
                        stream.write(parent.id);
                    }
                }
            }
        }

        stream.write(JavaConstants.SPACE);
        stream.write(JavaConstants.LBRACE);
        stream.write(JavaConstants.NL);
        
        //write properties
        final Iterator propIte = clazz.properties.createIterator();
        while(propIte.hasNext()){
            writeProperty((Property)propIte.next());
        }
        //write functions
        final Iterator fctIte = clazz.functions.createIterator();
        while(fctIte.hasNext()){
            stream.write(JavaConstants.NL);
            writeFunction(clazz, (Function)fctIte.next());
        }
        //class end
        stream.write(JavaConstants.NL);
        stream.write(JavaConstants.RBRACE);

    }

    private void writeProperty(Property property) throws IOException{

        //write description
        writeDocumentation(property.metas.get(JavaDocumentation.ID), 1);

        stream.write(JavaConstants.SPACE4);

        //scope
        final JavaScope scopeMeta = (JavaScope) property.metas.get(JavaScope.ID);
        final int scope = scopeMeta==null ? JavaScope.TYPE_PUBLIC : scopeMeta.getType();
        if(JavaScope.TYPE_PRIVATE == scope){
            stream.write(JavaConstants.PRIVATE);
        }else if(JavaScope.TYPE_PROTECTED == scope){
            stream.write(JavaConstants.PROTECTED);
        }else if(JavaScope.TYPE_PUBLIC == scope){
            stream.write(JavaConstants.PUBLIC);
        }else if(JavaScope.TYPE_PACKAGE == scope){
            //write nothing
        }else{
            throw new IOException("Unknowned scope : "+scope);
        }
        stream.write(JavaConstants.SPACE);

        //type
        if(property.objclass != null){
            writeClassName(property.objclass);
            stream.write(JavaConstants.SPACE);
        }else{
            stream.write(new Chars(property.primclass.getName()));
            stream.write(JavaConstants.SPACE);
        }

        //name
        stream.write(property.id);
        stream.write(JavaConstants.SEMICOLON);
        stream.write(JavaConstants.NL);
    }

    private void writeFunction(JavaClass clazz, Function function) throws IOException{

        //write description
        writeDocumentation(function.metas.get(JavaDocumentation.ID), 1);

        stream.write(JavaConstants.SPACE4);

        //scope
        if (!clazz.isInterface()) {
            final JavaScope scopeMeta = (JavaScope) function.metas.get(JavaScope.ID);
            final int scope = scopeMeta==null ? JavaScope.TYPE_PUBLIC : scopeMeta.getType();
            if(JavaScope.TYPE_PRIVATE == scope){
                stream.write(JavaConstants.PRIVATE);
            }else if(JavaScope.TYPE_PROTECTED == scope){
                stream.write(JavaConstants.PROTECTED);
            }else if(JavaScope.TYPE_PUBLIC == scope){
                stream.write(JavaConstants.PUBLIC);
            }else if(JavaScope.TYPE_PACKAGE == scope){
                //write nothing
            }else{
                throw new IOException("Unknowned scope : "+scope);
            }
        }

        //output type
        stream.write(JavaConstants.SPACE);
        if(function.outParameters.isEmpty()){
            stream.write(JavaConstants.VOID);
        }else if(function.outParameters.getSize()==1){
            final Parameter out = (Parameter) function.outParameters.get(0);
            writeClassName(out.type);
        }else{
            throw new IOException("Java classes do not support more then one output parameter");
        }

        //name
        stream.write(JavaConstants.SPACE);
        stream.write(function.id);

        //parameters
        stream.write(JavaConstants.SPACE);
        stream.write(JavaConstants.LPAREN);
        for(int i=0,n=function.inParameters.getSize();i<n;i++){
            final Parameter in = (Parameter) function.inParameters.get(i);
            if(i!=0) stream.write(',').write(' ');
            writeClassName(in.type);
            stream.write(JavaConstants.SPACE);
            stream.write(in.id);
        }
        stream.write(JavaConstants.RPAREN);

        //exceptions
        final Sequence exps = function.metas.getAll(JavaException.ID);
        if(!exps.isEmpty()){
            stream.write(JavaConstants.SPACE);
            stream.write(JavaConstants.THROWS);
            stream.write(JavaConstants.SPACE);
            for(int i=0,n=exps.getSize();i<n;i++){
                final JavaException exp = (JavaException) exps.get(i);
                if(i!=0) stream.write(',');
                stream.write(exp.getExceptionClass().id);
            }
        }

        if (clazz.isInterface()) {
            stream.write(';');
            stream.write(JavaConstants.NL);
            return;
        }

        //function start
        stream.write(JavaConstants.SPACE);
        stream.write(JavaConstants.LBRACE);
        stream.write(JavaConstants.NL);

        //Write Code
        if(function.startInstruction!=null){
            writeInstrustion(function.startInstruction, 2);
        }

        //function end
        stream.write(JavaConstants.NL);
        stream.write(JavaConstants.SPACE4);
        stream.write(JavaConstants.RBRACE);
        stream.write(JavaConstants.NL);
        
    }

    private void writeInstrustion(Instruction ist, int nbSp) throws IOException{
        if(ist instanceof Return){
            final Return cdt = (Return) ist;
            sp(nbSp);
            stream.write(JavaConstants.RETURN);
            if(cdt.value!=null){
                stream.write(JavaConstants.SPACE);
                stream.write(CObjects.toChars(cdt.value.id));
            }
            stream.write(';');
            
        }else if(ist instanceof Label){
            throw new IOException("TODO");
        }else if(ist instanceof Jump){
            throw new IOException("TODO");
        }else if(ist instanceof Declaration){
            throw new IOException("TODO");
        }else if(ist instanceof Call){
            throw new IOException("TODO");
        }else if(ist instanceof Affectation){
            throw new IOException("TODO");
        }else{
            throw new IOException("Unexpected instruction "+ist);
        }
        
        if(ist instanceof Forward){
            stream.write(JavaConstants.NL);
            final Instruction next = ((Forward)ist).getNext();
            if(next!=null) writeInstrustion(next,nbSp);
        }
        
    }
    
    private void writeDocumentation(Meta meta, int nbtabs) throws IOException{
        if(!(meta instanceof JavaDocumentation)) return;
        
        final CharArray text = ((JavaDocumentation)meta).getText();
        if(text==null || text.isEmpty()) return;

        sp(nbtabs);stream.write(JavaConstants.JAVADOC_START);
        stream.write(JavaConstants.NL);

        final CharArray[] lines = text.split('\n');
        for(int i=0;i<lines.length;i++){
            lines[i] = lines[i].trim();
            if(lines[i].isEmpty()) continue;
            sp(nbtabs);stream.write(' ').write(JavaConstants.JAVADOC).write(' ');
            stream.write(lines[i].toChars());
            stream.write(JavaConstants.NL);
        }

        sp(nbtabs);stream.write(' ').write(JavaConstants.JAVADOC_END);
        stream.write(JavaConstants.NL);

    }

    private void sp(int nb) throws IOException{
        for(int i=0;i<nb;i++){
            stream.write(JavaConstants.SPACE4);
        }
    }

    private void writeClassName(science.unlicense.api.code.Class clazz) throws IOException {
        if (clazz.id.equals(JavaClass.PRIMITIVE_BOOLEAN.id)) {
            stream.write(JavaConstants.BOOLEAN);
        } else if (clazz.id.equals(JavaClass.PRIMITIVE_BYTE.id)) {
            stream.write(JavaConstants.BYTE);
        } else if (clazz.id.equals(JavaClass.PRIMITIVE_SHORT.id)) {
            stream.write(JavaConstants.SHORT);
        } else if (clazz.id.equals(JavaClass.PRIMITIVE_INT.id)) {
            stream.write(JavaConstants.INT);
        } else if (clazz.id.equals(JavaClass.PRIMITIVE_LONG.id)) {
            stream.write(JavaConstants.LONG);
        } else if (clazz.id.equals(JavaClass.PRIMITIVE_FLOAT.id)) {
            stream.write(JavaConstants.FLOAT);
        } else if (clazz.id.equals(JavaClass.PRIMITIVE_DOUBLE.id)) {
            stream.write(JavaConstants.DOUBLE);
        } else if (clazz.id.equals(JavaClass.OBJECT_CLASS.id)) {
            stream.write(new Chars("Object"));
        } else {
            stream.write(clazz.id);
        }
    }
    
}