
package science.unlicense.impl.code.java.clazz;

import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class ClassReader extends AbstractReader {

    public ClassFile read() throws IOException{
        final ClassFile file = new ClassFile();
        file.read(getInputAsDataStream(NumberEncoding.BIG_ENDIAN));
        return file;
    }

}
