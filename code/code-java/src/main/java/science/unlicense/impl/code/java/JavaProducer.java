
package science.unlicense.impl.code.java;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.AbstractCodeProducer;
import science.unlicense.api.code.CodeContext;
import science.unlicense.api.code.CodeException;
import science.unlicense.api.code.CodeFile;
import science.unlicense.api.code.Parameter;
import science.unlicense.api.code.meta.Meta;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import static science.unlicense.impl.code.java.JavaConstants.RULE_CLASS;
import static science.unlicense.impl.code.java.JavaConstants.RULE_CLASSCONTENT;
import static science.unlicense.impl.code.java.JavaConstants.RULE_COMMENT;
import static science.unlicense.impl.code.java.JavaConstants.RULE_ENUM;
import static science.unlicense.impl.code.java.JavaConstants.RULE_EXTENDS;
import static science.unlicense.impl.code.java.JavaConstants.RULE_FILE;
import static science.unlicense.impl.code.java.JavaConstants.RULE_FUNCTION;
import static science.unlicense.impl.code.java.JavaConstants.RULE_IMPLEMENTS;
import static science.unlicense.impl.code.java.JavaConstants.RULE_IMPORT;
import static science.unlicense.impl.code.java.JavaConstants.RULE_IMPORTREF;
import static science.unlicense.impl.code.java.JavaConstants.RULE_INTERFACE;
import static science.unlicense.impl.code.java.JavaConstants.RULE_PACKAGE;
import static science.unlicense.impl.code.java.JavaConstants.RULE_PARAMETER;
import static science.unlicense.impl.code.java.JavaConstants.RULE_PARAMETERS;
import static science.unlicense.impl.code.java.JavaConstants.RULE_QUALIFIER;
import static science.unlicense.impl.code.java.JavaConstants.RULE_REF;
import static science.unlicense.impl.code.java.JavaConstants.RULE_SCOPE;
import static science.unlicense.impl.code.java.JavaConstants.RULE_TYPE;
import static science.unlicense.impl.code.java.JavaConstants.RULE_VARIABLE;
import static science.unlicense.impl.code.java.JavaConstants.RULE_WS;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_ABSTRACT;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_COMMENT2;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_FINAL;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_NATIVE;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_PRIVATE;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_PROTECTED;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_PUBLIC;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_STATIC;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_SYNCHRONIZED;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_TRANSIENT;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_VOID;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_VOLATILE;
import static science.unlicense.impl.code.java.JavaConstants.TOKEN_WORD;
import science.unlicense.impl.code.java.model.JavaClass;
import science.unlicense.impl.code.java.model.JavaDocumentation;
import science.unlicense.impl.code.java.model.JavaFunction;
import science.unlicense.impl.code.java.model.JavaMetas;
import science.unlicense.impl.code.java.model.JavaProperty;
import science.unlicense.impl.code.java.model.JavaScope;

/**
 *
 * @author Johann Sorel
 */
public class JavaProducer extends AbstractCodeProducer {

    private CodeContext codeContext;

    //current state
    private Chars pack;
    private final Dictionary imports = new HashDictionary();
    private Chars lastComment;

    public JavaProducer() {
    }

    @Override
    public Sequence createCodeFiles(CodeContext context) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public CodeContext createCodeContext(Sequence codeFiles) throws CodeException {
        codeContext = new CodeContext();
        pack = null;
        imports.removeAll();
        lastComment = null;
        
        for(int i=0,n=codeFiles.getSize();i<n;i++){
            final CodeFile cf = (CodeFile) codeFiles.get(i);
            convert(cf.syntax);
        }
        return codeContext;
    }
    
    private void convert(SyntaxNode sn) throws CodeException{
        final Rule rule = sn.getRule();
        if(RULE_FILE==rule){
            convertFile(sn);
        }
    }

    private void convertFile(final SyntaxNode sn) throws CodeException{
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            if(RULE_PACKAGE==rule){
                pack = cn.getChildByRule(RULE_REF).getTokensChars();
            }else if(RULE_IMPORT==rule){
                //TODO handle static and .*
                final SyntaxNode ccn = cn.getChildByRule(RULE_IMPORTREF);
                final Chars fullName = ccn.getTokensChars();
                final Chars[] parts = fullName.split('.');
                imports.add(parts[parts.length-1],fullName);
                
            }else if(RULE_CLASS==rule){
                convertClass(cn);
            }else if(RULE_INTERFACE==rule){
                convertInterface(cn);
            }else if(RULE_ENUM==rule){
                convertEnum(cn);
            }else if(RULE_COMMENT==rule){
                final SyntaxNode cmtToken = cn.getChildByToken(TOKEN_COMMENT2);
                lastComment = (cmtToken==null) ? Chars.EMPTY : cmtToken.getTokensChars();
            }else if(RULE_WS==rule){
                final SyntaxNode ccn = cn.getChildByRule(RULE_COMMENT);
                if(ccn!=null){
                    final SyntaxNode cmtToken = ccn.getChildByToken(TOKEN_COMMENT2);
                    lastComment = (cmtToken==null) ? Chars.EMPTY : cmtToken.getTokensChars();
                }
            }
        }
    }

    private void convertClass(final SyntaxNode sn) throws CodeException{
        final JavaClass clazz = new JavaClass();
        if(lastComment!=null){
            clazz.metas.add(convertDoc(lastComment));
            lastComment = null;
        }
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            final Token token = cn.getToken();
            if(RULE_SCOPE==rule){
                clazz.metas.add(convertScope(cn));
            }else if(RULE_QUALIFIER==rule){
                clazz.metas.add(convertQualifier(cn));
            }else if(RULE_EXTENDS==rule){
                clazz.parents.addAll(convertRefs(cn));
            }else if(RULE_IMPLEMENTS==rule){
                clazz.parents.addAll(convertRefs(cn));
            }else if(RULE_CLASSCONTENT==rule){
                convertClassContent(clazz, cn);
            }else if(token!=null && token.type==TOKEN_WORD){
                clazz.id = pack.concat('.').concat(token.value);
            }
        }
        
        codeContext.elements.add(clazz.id, clazz);
    }

    private void convertInterface(final SyntaxNode sn) throws CodeException{
        final JavaClass clazz = new JavaClass();
        clazz.metas.add(JavaMetas.INTERFACE);
        if(lastComment!=null){
            clazz.metas.add(convertDoc(lastComment));
            lastComment = null;
        }
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            final Token token = cn.getToken();
            if(RULE_SCOPE==rule){
                clazz.metas.add(convertScope(cn));
            }else if(RULE_QUALIFIER==rule){
                clazz.metas.add(convertQualifier(cn));
            }else if(RULE_EXTENDS==rule){
                clazz.parents.addAll(convertRefs(cn));
            }else if(RULE_CLASSCONTENT==rule){
                convertClassContent(clazz, cn);
            }else if(token!=null && token.type==TOKEN_WORD){
                clazz.id = pack.concat('.').concat(token.value);
            }
        }
        
        codeContext.elements.add(clazz.id, clazz);
    }

    private void convertEnum(final SyntaxNode sn) throws CodeException{
        final JavaClass clazz = new JavaClass();
        clazz.metas.add(JavaMetas.ENUM);
        if(lastComment!=null){
            clazz.metas.add(convertDoc(lastComment));
            lastComment = null;
        }
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            final Token token = cn.getToken();
            if(RULE_SCOPE==rule){
                clazz.metas.add(convertScope(cn));
            }else if(RULE_QUALIFIER==rule){
                clazz.metas.add(convertQualifier(cn));
            }else if(RULE_CLASSCONTENT==rule){
                convertClassContent(clazz, cn);
            }else if(token!=null && token.type==TOKEN_WORD){
                clazz.id = pack.concat('.').concat(token.value);
            }
        }
        
        codeContext.elements.add(clazz.id, clazz);
    }

    private void convertClassContent(JavaClass clazz, final SyntaxNode sn) throws CodeException{
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            
            if(rule==RULE_FUNCTION){
                clazz.functions.add(convertFunction(cn));
            }else if(rule==RULE_VARIABLE){
                clazz.properties.addAll(convertProperty(cn));
            }else if(rule==RULE_COMMENT){
                final SyntaxNode cmtToken = cn.getChildByToken(TOKEN_COMMENT2);
                lastComment = (cmtToken==null) ? Chars.EMPTY : cmtToken.getTokensChars();
            }
            
        }
    }

    private JavaFunction convertFunction(SyntaxNode sn) throws CodeException{
        final JavaFunction fct = new JavaFunction();
        if(lastComment!=null){
            fct.metas.add(convertDoc(lastComment));
            lastComment = null;
        }
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            final Token token = cn.getToken();
            if(RULE_SCOPE==rule){
                fct.metas.add(convertScope(cn));
            }else if(RULE_QUALIFIER==rule){
                fct.metas.add(convertQualifier(cn));
            }else if(RULE_TYPE==rule){
                final Parameter param = new Parameter();
                param.id = null;
                param.type = new JavaClass();
                param.type.id = cn.getTokensChars();
                fct.outParameters.add(param);
            }else if(RULE_PARAMETERS==rule){
                fct.inParameters.addAll(convertParameters(cn));
            }else if(token!=null && token.type==TOKEN_WORD){
                fct.id = token.value;
            }else if(token!=null && token.type==TOKEN_VOID){
                //no return type
                final Parameter param = new Parameter();
                param.id = null;
                param.type = new JavaClass();
                param.type.id = token.value;
                fct.outParameters.add(param);
            }
        }
        
        
        return fct;
    }
    
    private Sequence convertProperty(SyntaxNode sn) throws CodeException{
        final Sequence properties = new ArraySequence();
        JavaProperty candidate = new JavaProperty();
        if(lastComment!=null){
            candidate.metas.add(convertDoc(lastComment));
            lastComment = null;
        }
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            final Token token = cn.getToken();
            if(RULE_SCOPE==rule){
                candidate.metas.add(convertScope(cn));
            }else if(RULE_QUALIFIER==rule){
                candidate.metas.add(convertQualifier(cn));
            }else if(RULE_TYPE==rule){
                JavaClass type = new JavaClass();
                type.id = cn.getTokensChars();
                candidate.objclass = type;
            }else if(token!=null && token.type==TOKEN_WORD){
                candidate.id = token.value;
                properties.add(candidate);
                candidate = new JavaProperty(candidate);
            }
        }
        
        return properties;
    }
    
    private Sequence convertParameters(SyntaxNode sn) throws CodeException{
        final Sequence params = new ArraySequence();
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            if(RULE_PARAMETER==rule){
                params.add(convertParameter(cn));
            }
        }
        
        return params;
    }
    
    private Parameter convertParameter(SyntaxNode sn) throws CodeException{
        final Parameter param = new Parameter();
        
        for (Iterator ite=sn.getChildren().createIterator();ite.hasNext();) {
            final SyntaxNode cn = (SyntaxNode)ite.next();
            final Rule rule = cn.getRule();
            final Token token = cn.getToken();
            if(RULE_TYPE==rule){
                param.type = new JavaClass();
                param.type.id = cn.getTokensChars();
            }else if(token!=null && token.type==TOKEN_WORD){
                param.id = token.value;
            }
        }
        
        return param;
    }
    
    private static Meta convertQualifier(SyntaxNode sn) throws CodeException{
        final SyntaxNode qualifierTok = (SyntaxNode) sn.getChildren().get(0);
        final TokenType type = qualifierTok.getToken().type;
        if(TOKEN_SYNCHRONIZED==type){
            return JavaMetas.SYNCHRONIZED;
        }else if(TOKEN_STATIC==type){
            return JavaMetas.STATIC;
        }else if(TOKEN_FINAL==type){
            return JavaMetas.FINAL;
        }else if(TOKEN_VOLATILE==type){
            return JavaMetas.VOLATILE;
        }else if(TOKEN_NATIVE==type){
            return JavaMetas.NATIVE;
        }else if(TOKEN_ABSTRACT==type){
            return JavaMetas.ABSTRACT;
        }else if(TOKEN_TRANSIENT==type){
            return JavaMetas.TRANSIENT;
        }else{
            throw new CodeException("Unexpected qulifier token :"+qualifierTok.getToken());
        }
    }
    
    private static Meta convertScope(SyntaxNode sn) throws CodeException{
        final SyntaxNode qualifierTok = (SyntaxNode) sn.getChildren().get(0);
        final TokenType type = qualifierTok.getToken().type;
        if(TOKEN_PUBLIC==type){
            return JavaScope.PUBLIC;
        }else if(TOKEN_PRIVATE==type){
            return JavaScope.PRIVATE;
        }else if(TOKEN_PROTECTED==type){
            return JavaScope.PROTECTED;
        }else{
            throw new CodeException("Unexpected qulifier token :"+qualifierTok.getToken());
        }
    }
    
    private Sequence convertRefs(SyntaxNode sn) throws CodeException{
        final Sequence refs = new ArraySequence();
        for(int i=0,n=sn.getChildren().getSize();i<n;i++){
            final SyntaxNode cn = (SyntaxNode) sn.getChildren().get(i);
            if(cn.getRule()==RULE_REF){
                refs.add(convertRef(cn));
            }
        }
        return refs;
    }
    
    private Chars convertRef(SyntaxNode sn){
        final Chars name = sn.getTokensChars();
        //try to resolve full name reference
        final Object value = imports.getValue(name);
        if(value instanceof Chars){
            return (Chars) value;
        }else{
            //same package
            return pack.concat('.').concat(name);
        }
    }
    
    private JavaDocumentation convertDoc(Chars node){
        final CharBuffer cb = new CharBuffer();
        final Chars[] parts = node.split('\n');
        for(int i=0;i<parts.length;i++){
            parts[i] = parts[i].trim();
            if(i==0 && parts[i].startsWith(JavaConstants.JAVADOC_START)){
                parts[i] = parts[i].truncate(3, parts[i].getCharLength()).trim();
            }
            if(parts[i].startsWith(JavaConstants.ONELINE_COMMENT)){
                parts[i] = parts[i].truncate(2, parts[i].getCharLength()).trim();
            }else if(parts[i].startsWith(JavaConstants.JAVADOC_END)){
                parts[i] = parts[i].truncate(2, parts[i].getCharLength()).trim();
            }else if(parts[i].startsWith('*')){
                parts[i] = parts[i].truncate(1, parts[i].getCharLength()).trim();
            }
            cb.append(parts[i]).append('\n');
        }
        
        return new JavaDocumentation(cb.toChars().trim());
    }
    
}
