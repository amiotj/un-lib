
package science.unlicense.impl.code.java.model;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class JavaClass extends science.unlicense.api.code.Class{

    public static final JavaClass OBJECT_CLASS = new JavaClass(new Chars("java.lang.Object"));
    public static final JavaClass PRIMITIVE_BOOLEAN = new JavaClass(new Chars("java.lang.boolean"));
    public static final JavaClass PRIMITIVE_BYTE = new JavaClass(new Chars("java.lang.byte"));
    public static final JavaClass PRIMITIVE_SHORT = new JavaClass(new Chars("java.lang.short"));
    public static final JavaClass PRIMITIVE_INT = new JavaClass(new Chars("java.lang.int"));
    public static final JavaClass PRIMITIVE_LONG = new JavaClass(new Chars("java.lang.long"));
    public static final JavaClass PRIMITIVE_FLOAT = new JavaClass(new Chars("java.lang.float"));
    public static final JavaClass PRIMITIVE_DOUBLE = new JavaClass(new Chars("java.lang.double"));
    
    
    public JavaClass() {
    }

    public JavaClass(Chars id) {
        this.id = id;
    }
    
    /**
     * last part of the class full name.
     * @return Chars
     */
    public Chars getShortName(){
        final int separator = id.getLastOccurence('.');
        return id.truncate(separator+1,id.getCharLength());
    }

    /**
     * package part of the class full name.
     * @return Chars
     */
    public Chars getPackage(){
        final int separator = id.getLastOccurence('.');
        return id.truncate(0,separator);
    }
    
    public JavaScope getScope(){
        return (JavaScope) metas.get(JavaScope.ID);
    }
    
    public JavaDocumentation getDocumentation(){
        return (JavaDocumentation) metas.get(JavaDocumentation.ID);
    }
    
    public boolean isInterface(){
        return metas.get(JavaMetas.INTERFACE.getId())!=null;
    }
    
    public void setInterface(boolean inter){
        if(inter){
            if(!isInterface()){
                metas.add(JavaMetas.INTERFACE);
            }
        }else{
            metas.remove(JavaMetas.INTERFACE.getId());
        }
    }
    
    public boolean isEnum(){
        return metas.get(JavaMetas.ENUM.getId())!=null;
    }
    
    public void setEnum(boolean inter){
        if(inter){
            if(!isInterface()){
                metas.add(JavaMetas.ENUM);
            }
        }else{
            metas.remove(JavaMetas.ENUM.getId());
        }
    }
    
}
