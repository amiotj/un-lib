
package science.unlicense.impl.code.java;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.code.CodeFormat;
import science.unlicense.api.code.CodeProducer;
import science.unlicense.api.store.DefaultFormat;
import science.unlicense.impl.code.GrammarCodeFileReader;

/**
 *
 * @author Johann Sorel
 */
public class JavaFormat extends DefaultFormat implements CodeFormat {

    public static final JavaFormat INSTANCE = new JavaFormat();

    private JavaFormat() {
        super(new Chars("java"),
              new Chars("Java"),
              new Chars("Java source file"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("java")
              },
              new byte[][]{});
    }

    @Override
    public CodeFileReader createReader() {
        return new GrammarCodeFileReader(JavaConstants.RULE_FILE);
    }

    @Override
    public CodeProducer createProducer() {
        return new JavaProducer();
    }
    
}
