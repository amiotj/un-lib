
package science.unlicense.impl.code.java.model;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.meta.DefaultMeta;
import science.unlicense.api.code.meta.Documentation;

/**
 *
 * @author Johann Sorel
 */
public class JavaDocumentation extends DefaultMeta implements Documentation {

    public static final Chars ID = new Chars("java-doc");
    
    private CharArray text = Chars.EMPTY;
    
    public JavaDocumentation() {
        super(ID);
    }

    public JavaDocumentation(CharArray text) {
        super(ID);
        this.text = text;
    }

    public CharArray getText() {
        return text;
    }

    public void setText(CharArray text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof JavaDocumentation)) return false;
        return CObjects.equals(text, ((JavaDocumentation)obj).text);
    }
    
    @Override
    public int getHash() {
        int hash = 5;
        hash = 31 * hash + (this.text != null ? this.text.hashCode() : 0);
        return hash;
    }

    @Override
    public Chars toChars() {
        return id.concat(':').concat(text);
    }
}
