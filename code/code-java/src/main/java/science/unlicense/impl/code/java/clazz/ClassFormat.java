
package science.unlicense.impl.code.java.clazz;

import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.code.CodeFormat;
import science.unlicense.api.code.CodeProducer;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.store.DefaultFormat;

/**
 *
 * @author Johann Sorel
 */
public class ClassFormat extends DefaultFormat implements CodeFormat {

    public static final ClassFormat INSTANCE = new ClassFormat();

    private ClassFormat() {
        super(new Chars("class"),
              new Chars("class"),
              new Chars("Java class file"),
              new Chars[]{
              },
              new Chars[]{
                  new Chars("class")
              },
              new byte[][]{});
    }

    @Override
    public CodeFileReader createReader() {
        throw new UnimplementedException("not supported yet.");
    }

    @Override
    public CodeProducer createProducer() {
        throw new UnimplementedException("not supported yet.");
    }

}
