
package science.unlicense.impl.code.java.model;

import science.unlicense.api.code.Function;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class JavaFunction extends Function {

    public Sequence getExceptions(){
        return metas.getAll(JavaException.ID);
    }

}
