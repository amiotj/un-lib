
package science.unlicense.impl.code.java;


import science.unlicense.impl.code.java.JavaWriter;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.Parameter;
import science.unlicense.api.code.inst.Reference;
import science.unlicense.api.code.inst.Return;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.regex.Regex;
import science.unlicense.impl.code.java.model.JavaClass;
import science.unlicense.impl.code.java.model.JavaFunction;
import science.unlicense.impl.code.java.model.JavaProperty;
import science.unlicense.impl.code.java.model.JavaScope;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JavaWriterTest {

    @Test
    public void emptyClass() throws IOException{

        final JavaClass clazz = new JavaClass();
        clazz.id = new Chars("un.test.EmptyClass");

        compareCodes("/un/impl/code/java/EmptyClass.java", clazz);
    }

    @Test
    public void properties() throws IOException{

        final JavaClass attClass = new JavaClass();
        attClass.id = new Chars("science.unlicense.api.character.Chars");
        
        final JavaClass clazz = new JavaClass();
        clazz.id = new Chars("un.test.PropertyClass");

        final JavaProperty prop1 = new JavaProperty(new Chars("att1"), new Chars("attribute 1"), attClass, null, JavaScope.PUBLIC);
        final JavaProperty prop2 = new JavaProperty(new Chars("att2"), new Chars("attribute 2"), attClass, null, JavaScope.PROTECTED);
        final JavaProperty prop3 = new JavaProperty(new Chars("att3"), new Chars("attribute 3"), attClass, null, JavaScope.PACKAGE);
        final JavaProperty prop4 = new JavaProperty(new Chars("att4"), new Chars("attribute 4"), attClass, null, JavaScope.PRIVATE);
        
        clazz.properties.add(prop1);
        clazz.properties.add(prop2);
        clazz.properties.add(prop3);
        clazz.properties.add(prop4);
        
        compareCodes("/un/impl/code/java/PropertyClass.java", clazz);
    }

    @Test
    public void functions() throws IOException{

        final JavaClass attClass = new JavaClass();
        attClass.id = new Chars("science.unlicense.api.character.Chars");


        final JavaClass clazz = new JavaClass();
        clazz.id = new Chars("un.test.FunctionClass");

        final JavaFunction fct1 = new JavaFunction();
        fct1.id = new Chars("fct1");

        final JavaFunction fct2 = new JavaFunction();
        fct2.id = new Chars("fct2");
        final Parameter in1 = new Parameter();
        in1.id = new Chars("firstName");
        in1.type = attClass;
        final Parameter in2 = new Parameter();
        in2.id = new Chars("lastName");
        in2.type = attClass;
        final Parameter out = new Parameter();
        out.id = new Chars("cnt");
        out.type = attClass;

        fct2.inParameters.add(in1);
        fct2.inParameters.add(in2);
        fct2.outParameters.add(out);

        final Reference ret = new Reference(in1.id);
        fct2.startInstruction = new Return(ret);

        clazz.functions.add(fct1);
        clazz.functions.add(fct2);

        compareCodes("/un/impl/code/java/FunctionClass.java", clazz);
    }


    static void compareCodes(String expectedFilePath, JavaClass clazz) throws IOException{
        final ArrayOutputStream bo = new ArrayOutputStream();
        final JavaWriter writer = new JavaWriter();
        writer.setOutput(bo);
        writer.write(clazz);
        final Chars result = new Chars(bo.getBuffer().toArrayByte());

        compareCodes(expectedFilePath, result);
    }

    static void compareCodes(String expectedFilePath, Chars result) throws IOException{
        final byte[] bytes = IOUtilities.readAll(Paths.resolve(new Chars("mod:"+expectedFilePath)).createInputStream());
        Chars expected = new Chars(bytes);
        expected = Regex.replace(expected, new Chars("( |\n|\r|\t)+"), new Chars(" "));
        result = Regex.replace(result, new Chars("( |\n|\r|\t)+"), new Chars(" "));
        
        Assert.assertEquals(expected, result);
    }

}