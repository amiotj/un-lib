
package science.unlicense.impl.code.java.clazz;

import science.unlicense.impl.code.java.clazz.ClassReader;
import science.unlicense.impl.code.java.clazz.ClassFile;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ClassReaderTest {

    @Test
    public void readTest() throws IOException{

        final ClassReader reader = new ClassReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/class/HelloWorld.class")));
        final ClassFile clazz = reader.read();

    }
}
