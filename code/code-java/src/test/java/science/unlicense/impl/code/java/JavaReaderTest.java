
package science.unlicense.impl.code.java;

import science.unlicense.impl.code.java.JavaFormat;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeFileReader;
import science.unlicense.api.io.IOException;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JavaReaderTest {

    @Test
    public void readImportsTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Imports.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
    
    @Test
    public void readExpressionsTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Expressions.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
    
    @Test
    public void readBranchingTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Branching.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
        
    @Test
    public void readClassesTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Classes.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
    
    @Test
    public void readEnumTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Enumeration.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
    
    @Test
    public void readInterfaceTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Interface.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
    
    @Test
    public void readGenericsTest() throws IOException{
        
        final CodeFileReader reader = JavaFormat.INSTANCE.createReader();
        
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/java/Generics.java")));
        
        final SyntaxNode sn = reader.read().syntax;
        System.out.println(sn);
        
    }
    
    
}
