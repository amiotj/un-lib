
package un.language.java;

import science.unlicense.api.character.Chars;

class Branching {

    private static final int A = 4;
    
    void test(){
        
        String line;
        while ((line = "") != null) {
            System.out.println(line);
        }
        
        do {
            System.out.println(line);
        } while ((line = "") != null);
        
        for(String l : new String[]{"a","b"}){
            System.out.println(l);
        }
        
        if(true) return;
        
        for(int i=0;i<10;i++) {
            break;
        }
        
        for(int i=0;i<10;i++) {
            continue;
        }
        
        label1:
        for(int i=0;i<10;i++) {
            label2:
            for(int j=0;j<10;j++) {
                break label1;
            }
            continue label1;
        }
        
        int z=0;
        int y=1;
        for(z=0,y=4;z<10;z++) {
            continue;
        }
        
        for(int i=0,n=20,k=3;i<10 && i<k;i++,k++) {
            continue;
        }
        
        long[] ls = {0,1,2};
        for( long n : ls ) {
            
        }
        
        int v = 0 < 1 ? 2 : 3 ;
        int a = true ? 2 : 3 & 4;
        byte b = 14 < 0 ? 5 : (6 & 0xff) > 7 ? 8 : 9;
        
        synchronized (this) {
            System.out.println("here");
        }
        
        int k = 3;
        switch (k) {
            /** case 1 */
            case 1 : System.out.println("test1");
            /** case 2 */
            case 2 : System.out.println("test2");
            // case 3
            case 3 : 
            /** case 2 */
            case A : System.out.println("test3");
            /** case char */
            case 'r' : System.out.println("test char");
            /** case hexa */
            case 0x45 : System.out.println("test hexa");
            /** case def */
            default : System.out.println("test6");
        }
        
    }
    
    public static Chars toChars(final Object array, int maxlength){
        if(array instanceof boolean[])return toChars((boolean[])array,1);
        else if(array instanceof boolean[][])return toChars((boolean[][])array,1);
        else if(array instanceof boolean[][][])return toChars((boolean[][][])array,1);
        else{
            throw new RuntimeException("Object is not a 1D or 2D array : "+array);
        }
    }
    
}