
package un.language.java;

import science.unlicense.api.collection.ArraySequence;
import java.lang.reflect.AnnotatedType;
import java.util.List;
import science.unlicense.impl.math.Vector;

class Expressions {

    //ugly but can happen
    ;
    
    void test(){
        int a = 2 + 2;
        int b = -2;
        int c = +2;
        int d = a+3;
        int e = 0x4F;
        int f = 4 >> 3;
        int g = 4 >>> 7 ;
        int h = (4 << 3) ;
        int j,l, o = 2;
        int p =-1,r = 8, t;
        int q = ~45;
        int s = '\u250C';
        int u = 3 & ~(1 << 4);
        int v = (byte) (float) (byte) 45;
        int αo = 45;
        a %= 4;
        a ^= 4;
        b <<= 3;
        c >>>= 3;
        s = 3 << 24 ;
        d &= 0xffffffffffffff00L;
        e |= 0xffffffffffffff00L;
        int k = 4 << 3;
        double f0 = 12l;
        double f1 = 12f;
        double f2 = 12d;
        double f3 = 12.456;
        double f4 = 12.456e-3;
        double f5 = 12.;
        double f6 = 12L;
        double f7 = 12F;
        double f8 = 12.;
        double f9 = .75;
        double m1 = 12.*3.9;
        double m2 = 12 ^ 2; 
        long l0 = 0x00000000ffffffffL;
        long l1 = 0x100000000L;
        int inc0 = a ++;
        int inc1 = a--;
        int inc2 = ++a;
        int inc3 = -- a;
        int length = this.getClass().getClasses().length;
        boolean v1 = !true;
        boolean v2 = 10 < -5;
        boolean v3 = 10 < -m1;
        boolean kw1 = "23" instanceof Object;
        boolean kw2 = (Integer)23 instanceof Object;
        boolean bo = a < this.getClass().getAnnotations().length - 1;
        boolean bo2 = (true | false) && (1<2);
        Object obj1 = null;
        Object obj2 = Object.class;
        String str1 = "text";
        String str2 = "te\"xt";
        String str3 = "\\";
        char c1 = 't';
        char c2 = '\'';
        char c3 = '\n';
        Class clazz1 = int.class;
        System.out.println("hello world");
        Object[] array1 = new Object[5];
        Object[] array2 = new Object[]{1,2,3};
        Object[] arraynull = new Object[]{1,null,3};
        int[] array3 = new int[]{1,2,3};
        int[][] array4 = new int[][]{{1,2,3},{4,5,6}};
        int[] array5 = {1,2,3};
        int[]array6 = {1,2,3};
        array6[5] = 45;
        byte temp[] = new byte[8];
        int pp = -(int)array4[0][0];
        float[] farr = new float[]{
            //doc
            10, 17, 38,
            //doc
            16,  /*doc*/ 8, 50,
            22, -1, 62, //doc
        };
        float[][] farr2 = {
            //doc
            {},
            {}
        };
        int cast1 = (byte)12;
        int cast2 = ( Integer ) 23;
        System.out.println((byte)'\n');
        Class intArrayClass = int[].class;
        Class intArray2Class = int[][].class;
        AnnotatedType ant = getClass().getAnnotatedInterfaces()[1];
        final ArraySequence col = new ArraySequence();
        final Class carr = ((Object[])col.toArray()).getClass();
        ((Object) col).getClass();
        ((Object[])col.toArray()).getClass();
        ((Object[])col.toArray())[0].getClass();
        final Plan plan = new Plan(
                new Vector(+100, 0, -100),
                new Vector(-100, 0, -100),
                new Vector(-100, 0, +100),
                new Vector(+100, 0, +100)
                );
        
        Object anonyme = new Object(){
            void call(){
                System.out.println("anonyme");
            }
        };
        
        assert true;
        assert(true);
        assert false : "assert message";
        
        java.util.Iterator<Object> it = java.util.Collections.emptyIterator();
        List<int[]> lst1 = null;
        //this match both expression and declaration
        List<Object> lst2;


        Object fct = this::test;
        
    }
    
}