
package un.language.java;

import science.unlicense.api.io.IOException;
import un.api.tree.Node;


class Classes extends Object implements Runnable{

    private Object child;
    private final String name = "test";
    private java.util.Vector vector;
    private static volatile transient String var = "test";
    static public int[] Table = new int[256];
        
    static {
        System.out.println("working");
    }
    
    private Classes(){
        //constructor
        super();
    }
    
    {
        System.out.println("ugly practice but legal");
    }
    
    private Classes(int i){
        //constructor
        this.child = i;
    }
    
    public void doSomething() throws Exception {
        throw new Exception("error");
    }
    
    public void doSomething2() throws Exception, IOException {
        throw new Exception("error");
    }
    
    public final void work(final String arg) {
    }
    
    public final void varargs(final String ... args) {
    }
    
    public final void vararray(final String args[]) {
    }
    
    static public void changeOrder() {}

    public Node[] getChildren() {
        return new Node[0];
    }
    
    @Override
    public void run() {
        throw new UnimplementedException("Not supported yet.");
    }
    
    private static abstract class SubClass {
        
        abstract int test();
        
    }
    
}