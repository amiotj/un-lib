
package un.language.java;

import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Node;


public enum Enumeration {

    TEST1 ,
    TEST2, ;
    
    private Object child;
    private final String name = "test";
    private java.util.Vector vector;
    private static volatile transient String var = "test";
        
    static {
        System.out.println("working");
    }
    
    
    {
        System.out.println("ugly practice but legal");
    }
        
    public void doSomething() throws Exception {
        throw new Exception("error");
    }
    
    public void doSomething2() throws Exception, IOException {
        throw new Exception("error");
    }
    
    public final void work(final String arg) {
    }

    public Node[] getChildren() {
        return new Node[0];
    }
        
    private static abstract class SubClass {
        
        abstract int test();
        
    }
    
}