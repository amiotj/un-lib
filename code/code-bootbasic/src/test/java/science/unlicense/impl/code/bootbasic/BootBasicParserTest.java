package science.unlicense.impl.code.bootbasic;

import science.unlicense.impl.code.bootbasic.BootBasicParser;
import org.junit.Test;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;

import org.junit.Assert;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Affectation;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Call;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Function;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Literal;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Program;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Reference;

/**
 * @author Johann Sorel
 */
public final class BootBasicParserTest {

    @Test
    public void parseTest() throws IOException {
        final Chars str = new Chars(
                "@demo.hello\n"+
                    "$text < 'Hello\\040world\\041':bin\n"+
                    "host.print $text", CharEncodings.US_ASCII);
        final byte[] bytes = str.toBytes();
        final ArrayInputStream in = new ArrayInputStream(bytes);
        final BootBasicParser parser = new BootBasicParser();
        parser.setInput(in);

        final Program program = parser.parse();

        Assert.assertEquals(0,program.globalVariables.getSize());
        Assert.assertEquals(1,program.functions.getSize());

        final Function fct = (Function) program.functions.get(0);
        Assert.assertEquals(new Chars("demo.hello",CharEncodings.US_ASCII),fct.name);
        Assert.assertEquals(2,fct.operations.getSize());
        final Affectation op1 = (Affectation) fct.operations.get(0);
        final Call op2 = (Call) fct.operations.get(1);
        Assert.assertEquals(new Chars("text",CharEncodings.US_ASCII),op1.ref.value);
        final Literal literal = (Literal) op1.value;
        Assert.assertArrayEquals(new byte[]{72,101,108,108,111,32,119,111,114,108,100,33},literal.value);
        Assert.assertEquals(new Chars("host.print",CharEncodings.US_ASCII),op2.function);
        Assert.assertEquals(1, op2.arguments.getSize());
        Assert.assertEquals(new Chars("text",CharEncodings.US_ASCII),((Reference)op2.arguments.get(0)).value );

    }

}
