package science.unlicense.impl.code.bootbasic;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.AFFECTATION;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Affectation;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.BLANKS;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.COMMENT;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Call;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.DCL_FCT;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.DCL_GVAR;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.DCL_LBL;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.DCL_LVAR;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Expression;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Function;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.JUMP;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Jump;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.LINEEND;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.LITERAL;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Label;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Literal;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Program;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.RETURN;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Reference;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Return;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.SLASH;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.SUBL;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.SUBR;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.TYPE;
import static science.unlicense.impl.code.bootbasic.BootBasicMetaModel.TYPE_BIN;


/**
 * BootBasic parser.
 *
 * @author Johann Sorel
 */
public class BootBasicParser {

    private final ByteSequence buffer = new ByteSequence();
    private final Sequence words = new ArraySequence();
    private boolean finished = false;
    private ByteInputStream stream;

    public BootBasicParser(){}

    public void setInput(ByteInputStream input){
        this.stream = input;
    }

    public Program parse() throws IOException {
        final Program program = new Program();

        Function function = null;
        while(!finished){
            readLine();
            if(words.isEmpty()){
                continue;
            }

            final Chars word = (Chars) words.get(0);
            if(isComment(word)){
                //comment, skip this line
                continue;
            }

            if(function == null){
                //we are outside a function, can be
                // - global variable declaration : %name < exp
                // - function declaration        : @function.name $arg1 $argN
                if(isGVarDeclaration(word)){
                    final Affectation globalVar = parseAffectation();
                    program.globalVariables.add(globalVar);

                }else if(isFctDeclaration(word)){
                    function = new Function();
                    function.name = word.truncate(1,word.getCharLength());
                    //parse arguments names
                    for(int i=1;i<words.getSize();i++){
                        final Chars arg = (Chars) words.get(i);
                        if(!isLVarDeclaration(arg)){
                            throw new IOException("Unexpected argument declaration : "+ word);
                        }
                        final Reference ref = new Reference();
                        ref.value = arg.truncate(1,arg.getCharLength());
                        function.arguments.add(ref);
                    }
                    program.functions.add(function);
                }else{
                    throw new IOException("Unexpected word : "+ word);
                }

            }else{
                //we are inside a function can be
                // - global variable declaration : %name < exp
                // - affectation                 : $name < exp
                // - label declaration           : +label
                // - jump                        : ? label exp
                // - return                      : = $res
                // - call                        : function.name exp1 expN
                if(isGVarDeclaration(word)){
                    final Affectation globalVar = parseAffectation();
                    program.globalVariables.add(globalVar);
                }else if(isLVarDeclaration(word)){
                    final Affectation aff = parseAffectation();
                    function.operations.add(aff);
                }else if(isLblDeclaration(word)){
                    final Label lbl = new Label();
                    lbl.name = word.truncate(1,word.getCharLength());
                    function.operations.add(lbl);
                }else if(isJump(word)){
                    final Jump jmp = new Jump();
                    if(words.getSize() == 2){
                        jmp.label = ((Chars)words.get(1)).truncate(1,word.getCharLength());
                        jmp.condition = null;
                    }else if(words.getSize() > 2){
                        jmp.label = ((Chars)words.get(1)).truncate(1,word.getCharLength());
                        words.remove(0);
                        words.remove(0);
                        jmp.condition = parseExpression();
                    }else{
                        throw new IOException("Unvalid number of arguments on jump condition");
                    }
                    function.operations.add(jmp);
                }else if(isReturn(word)){
                    words.remove(0);
                    final Return ret = new Return();
                    ret.value = parseExpression();
                    function.operations.add(ret);
                }else{
                    final Call call = parseCall();
                    function.operations.add(call);
                }
            }
        }

        return program;

    }

    private Expression parseExpression() throws IOException {
        if(words.isEmpty()){
            throw new IOException("Unvalid empty expression");
        }

        //can be :
        // $var
        // 'literal':type
        // call exp1 expN
        // ( exp )

        final Chars word = (Chars) words.get(0);
        if(isLVarDeclaration(word)){
            words.remove(0);
            final Reference ref = new Reference();
            ref.value =  word.truncate(1,word.getCharLength());
            return ref;
        }else if(isLiteral(word)){
            words.remove(0);
            return parseLiteral(word);
        }else if(isLeftParen(word)){
            words.remove(0);
            //sub expression
            Expression exp = parseExpression();
            //check we have right parenthese now
            if(!isRightParen((Chars)words.get(0))){
                throw new IOException("Missing ')'");
            }
            words.remove(0);
            return exp;
        }else{
            //function call
            return parseCall();
        }

    }

    private Affectation parseAffectation() throws IOException {
        final Chars word = (Chars) words.get(0);
        final Affectation aff = new Affectation();
        aff.ref = new Reference();
        aff.ref.value =  word.truncate(1,word.getCharLength());
        if(words.getSize() < 3 || !isAffectation((Chars) words.get(1)) ){
            throw new IOException("Incorrect affectation for "+ word);
        }
        words.remove(0);
        words.remove(0);
        aff.value = parseExpression();
        return aff;
    }

    private Call parseCall() throws IOException {
        final Chars word = (Chars) words.get(0);
        words.remove(0);
        final Call call = new Call();
        call.function = word;
        while(!words.isEmpty()){
            Expression exp = parseExpression();
            call.arguments.add(exp);
        }
        return call;
    }

    private Literal parseLiteral(Chars word) throws IOException {
        final Literal literal = new Literal();
        literal.value = word.toBytes();
        //size must at least be 6 = '':bin
        if(literal.value.length <6){
            throw new IOException("Unvalid literal definition : "+word);
        }
        if(literal.value[literal.value.length-5] != LITERAL){
            throw new IOException("Unvalid literal definition : "+word);
        }
        if(literal.value[literal.value.length-4] != TYPE){
            throw new IOException("Unvalid literal definition : "+word);
        }

        final byte[] type = Arrays.copy(literal.value,literal.value.length-3,3,new byte[3],0);
        if(Arrays.equals(type,TYPE_BIN)){
            final ByteSequence buf = new ByteSequence();
            for(int i=1;i<literal.value.length-5;i++){
                if(literal.value[i] == SLASH){
                    buf.put((byte)(
                            (literal.value[i+1]-48)*64 +
                            (literal.value[i+2]-48)*8 +
                            (literal.value[i+3]-48)));
                    i+=3;
                }else{
                    buf.put(literal.value[i]);
                }
            }

            literal.value = buf.toArrayByte();
        }else{
            throw new IOException("Unvalid literal definition : "+word);
        }

        return literal;
    }

    private Sequence readLine() throws IOException {
        words.removeAll();
        for(;;){
            final int b = stream.read();
            if(-1 == b){
                //finished stream
                finished = true;
                bufferToWord();
                break;
            }else if(LINEEND == b){
                //finished line
                bufferToWord();
                break;
            }else if(Arrays.contains(BLANKS,b)){
                //word end
                bufferToWord();
            }else if(AFFECTATION == b || SUBL == b || SUBR == b){
                //syntax separators
                bufferToWord();
                buffer.put((byte)b);
                bufferToWord();
            }else{
                //inside a word
                buffer.put((byte)b);
            }
        }

        return words;
    }

    private void bufferToWord(){
        if(buffer.getSize() == 0) return;
        final byte[] data = buffer.toArrayByte();
        buffer.removeAll();
        Chars cs = new Chars(data);
        words.add(cs);
    }

    private static boolean isComment(Chars cs){
        return cs.toBytes()[0] == COMMENT;
    }

    private static boolean isLVarDeclaration(Chars cs){
        return cs.toBytes()[0] == DCL_LVAR;
    }

    private static boolean isGVarDeclaration(Chars cs){
        return cs.toBytes()[0] == DCL_GVAR;
    }

    private static boolean isFctDeclaration(Chars cs){
        return cs.toBytes()[0] == DCL_FCT;
    }

    private static boolean isLblDeclaration(Chars cs){
        return cs.toBytes()[0] == DCL_LBL;
    }

    private static boolean isAffectation(Chars cs){
        return cs.toBytes()[0] == AFFECTATION;
    }

    private static boolean isLiteral(Chars cs){
        return cs.toBytes()[0] == LITERAL;
    }

    private static boolean isLeftParen(Chars cs){
        return cs.toBytes()[0] == SUBL;
    }

    private static boolean isRightParen(Chars cs){
        return cs.toBytes()[0] == SUBR;
    }

    private static boolean isJump(Chars cs){
        return cs.toBytes()[0] == JUMP;
    }

    private static boolean isReturn(Chars cs){
        return cs.toBytes()[0] == RETURN;
    }

}
