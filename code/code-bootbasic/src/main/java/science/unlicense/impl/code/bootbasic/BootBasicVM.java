package science.unlicense.impl.code.bootbasic;

import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.code.bootbasic.BootBasicMetaModel.Program;


/**
 * @author Johann Sorel
 */
public final class BootBasicVM {

    public static void execute(Program program, CharSequence mainFunction, Sequence arguments){


    }

    private class ProgramContext{

        private Dictionary globalVariables = new HashDictionary();

        private Dictionary functions = new HashDictionary();

    }

    private class FunctionContext{

        private Dictionary localVariables = new HashDictionary();

    }



}
