package science.unlicense.impl.code.bootbasic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * BootBasic is an easy to parse language, yet providing enough
 * functionality to support casual applications.
 * It's structure also simplify the implementation of a virtual machine with
 * a garbage collector.
 *
 * @author Johann Sorel
 */
public final class BootBasicMetaModel {

    public static final int[] BLANKS       = new int[]{9,32}; // tab and space
    public static final int LINEEND        = 10; // jump line
    public static final int DCL_LVAR       = 36; // $ local variable declaration
    public static final int DCL_GVAR       = 37; // % global variable declaration
    public static final int DCL_FCT        = 64; // @ function declaration
    public static final int DCL_LBL        = 43; // + label declaration
    public static final int AFFECTATION    = 60; // < variable affection
    public static final int RETURN         = 61; // = function return
    public static final int COMMENT        = 35; // # comment
    public static final int JUMP           = 63; // ? jump (branch)
    public static final int LITERAL        = 39; // ' literal delimitation
    public static final int TYPE           = 58; // : literal type
    public static final int SUBL           = 40; // ( sub expression start
    public static final int SUBR           = 41; // ) sub expression end

    public static final byte[] TYPE_BIN    = new byte[]{98,105,110};
    public static final int SLASH          = 92; // \ use for binary literal

    private BootBasicMetaModel(){}

    public static class Program{
        public final Sequence globalVariables = new ArraySequence(); // affectations
        public final Sequence functions = new ArraySequence(); // Functions
    }

    public static interface Expression{}

    public static class Function{
        public Chars name;
        public final Sequence arguments = new ArraySequence(); // references
        public final Sequence operations = new ArraySequence(); // Operations
    }

    public static class Affectation {
        public Reference ref;
        public Expression value;
    }

    public static class Label {
        public Chars name;
    }

    public static class Jump {
        public Expression condition;
        public Chars label;
    }

    public static class Return {
        public Expression value;
    }

    public static class Reference implements Expression{
        public Chars value;
    }

    public static class Literal implements Expression{
        public byte[] value;
    }

    public static class Call implements Expression{
        public Chars function;
        public final Sequence arguments = new ArraySequence(); // Expressions
    }

}
