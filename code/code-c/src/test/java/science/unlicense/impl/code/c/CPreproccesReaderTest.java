
package science.unlicense.impl.code.c;

import science.unlicense.impl.code.c.CPreprocessReader;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CPreproccesReaderTest {
    
    @Test
    public void readInclude() throws IOException{
        
        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_include.h")));
        SyntaxNode read = reader.read();
        
        System.out.println(read);
    }

    @Test
    public void readDefine() throws IOException{
        
        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_define.h")));
        SyntaxNode read = reader.read();

        System.out.println(read);
    }

    @Test
    public void readError() throws IOException{

        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_error.h")));
        SyntaxNode read = reader.read();

        System.out.println(read);
    }

    @Test
    public void readIfElseTest() throws IOException{
        
        final CPreprocessReader reader = new CPreprocessReader();
        reader.setInput(Paths.resolve(new Chars("mod:/un/impl/code/c/preproc_ifelse.h")));
        SyntaxNode read = reader.read();
        System.out.println(read.toCharsTree(10));
    }
    
}
