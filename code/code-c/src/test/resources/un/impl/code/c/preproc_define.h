
#define _TEST_

#define TEST_DEF0 extern

#define TEST_DEF1 static

#define TEST_DEF2   0

#define POINT_SIZE(x)   (-(x))

#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define SHIFT   (1 << NBSHIFT)

#define Work(p)    play(p)
