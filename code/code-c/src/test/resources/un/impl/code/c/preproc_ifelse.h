
/*
 some documentation 
 author : johann sorel
 */
#include <stdio.h>

int main(void){
#if COND == 2
    printf("case 1");
#elif COND == 3
    printf("case 2");
#elif 1
    printf("case 3");
#else 
    printf("case 4");
#endif 
  return 0;
}


#if defined(STB_TRUETYPE_BIGENDIAN) && !defined(ALLOW_UNALIGNED_TRUETYPE)

#endif
