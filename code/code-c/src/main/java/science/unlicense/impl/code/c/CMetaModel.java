package science.unlicense.impl.code.c;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;

/**
 * C syntax model.
 *
 * @author Johann Sorel
 */
public final class CMetaModel {

    public static final int[] BLANKS       = new int[]{9,32}; // tab and space
    public static final byte NEWLINE        = 10;    // jump line
    public static final byte SPACE          = ' ';    // 
    public static final byte COMMENT        = 42;    // / slash
    public static final byte ADR            = 38;    // & get address
    public static final byte COMMA          = 44;    // ,
    public static final byte COMDOT         = 59;    // ;
    public static final byte PRE            = 35;    // #
    public static final byte LAC            = 123;   // {
    public static final byte RAC            = 125;   // }
    public static final byte LPA            = 40;    // (
    public static final byte RPA            = 41;    // )
    public static final byte LBR            = 91;    // [
    public static final byte RBR            = 93;    // ]
    public static final byte EQU            = 61;    // =
    public static final byte QUE            = 63;    // ?
    public static final byte EXP            = 33;    // !
    public static final byte DDOT           = 58;    // :
    public static final byte SUP            = 60;    // <
    public static final byte INF            = 62;    // >
    public static final byte OR             = 124;   // |
    public static final byte QUO            = 39;    // '
    public static final byte GUI            = 34;    // "

    public static final byte ADD            = 43;    // +
    public static final byte SUB            = 45;    // -
    public static final byte DIV            = 47;    // /
    public static final byte MUL            = 42;    // *
    public static final byte MOD            = 37;    // %

    public static final Chars KW_TYPEDEF     = new Chars("typedef", CharEncodings.US_ASCII);
    public static final Chars KW_STRUCT      = new Chars("struct", CharEncodings.US_ASCII);
    public static final Chars KW_EXTERN      = new Chars("extern", CharEncodings.US_ASCII);
    public static final Chars KW_CONST       = new Chars("const", CharEncodings.US_ASCII);
    public static final Chars KW_ENUM        = new Chars("enum", CharEncodings.US_ASCII);
    public static final Chars KW_STATIC      = new Chars("static", CharEncodings.US_ASCII);
    public static final Chars KW_DEFINE    = new Chars("define", CharEncodings.US_ASCII);

    public static final Chars KW_VOID        = new Chars("void", CharEncodings.US_ASCII);
    public static final Chars KW_CHAR        = new Chars("char", CharEncodings.US_ASCII);
    public static final Chars KW_SHORT       = new Chars("short", CharEncodings.US_ASCII);
    public static final Chars KW_INT         = new Chars("int", CharEncodings.US_ASCII);
    public static final Chars KW_FLOAT       = new Chars("float", CharEncodings.US_ASCII);
    public static final Chars KW_DOUBLE      = new Chars("double", CharEncodings.US_ASCII);
    public static final Chars KW_LONG        = new Chars("long", CharEncodings.US_ASCII);
    public static final Chars KW_UNSIGNED    = new Chars("unsigned", CharEncodings.US_ASCII);

    private CMetaModel(){}

}
