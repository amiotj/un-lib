package science.unlicense.impl.code.c;

import science.unlicense.api.io.ByteInputStream;

/**
 * C parser.
 *
 * @author Johann Sorel
 */
public class CParser {

    private ByteInputStream stream;

    public CParser(){}

    public void setInput(ByteInputStream input){
        this.stream = input;
    }

}
