
package science.unlicense.impl.code.c;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.IOException;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CPreprocessReader extends AbstractReader{
    
    
    private static final OrderedHashDictionary TOKENS = new OrderedHashDictionary();
    private static final OrderedHashDictionary RULES = new OrderedHashDictionary();
    private static final Rule RULE_FILE;
    static {
        try{
            //parse grammar
            final UNGrammarReader reader = new UNGrammarReader();
            reader.setInput(Paths.resolve(new Chars("mod:/science/unlicense/impl/code/c/c-preprocessor.gr")));
            reader.read(TOKENS, RULES);
            RULE_FILE = (Rule) RULES.getValue(new Chars("file"));
            
            
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    public SyntaxNode read() throws IOException{
        final Parser parser = new Parser(TOKENS,RULE_FILE,null);
        parser.setInput(getInput());
        final SyntaxNode sn = parser.parse();
        parser.dispose();
        return sn;
    }

}
