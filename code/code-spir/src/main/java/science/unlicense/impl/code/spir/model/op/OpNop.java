
package science.unlicense.impl.code.spir.model.op;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.code.spir.model.Instruction;
import science.unlicense.impl.code.spir.model.InstructionType;

/**
 *
 * @author Johann Sorel
 */
public class OpNop extends Instruction {

    public static final InstructionType TYPE = new InstructionType(new Chars("OpNop"), 0, InstructionType.OPERAND_EMPTY) {
        public Instruction create() {
            return new OpNop();
        }
    };

    public OpNop() {
        super(TYPE);
    }
    
}
