
package science.unlicense.impl.code.spir;

import science.unlicense.api.character.Chars;
import science.unlicense.api.store.DefaultFormat;

/**
 * Standard Portable Intermediate Representation format.
 * 
 * Specification : 
 * https://www.khronos.org/registry/spir-v/specs/1.0/SPIRV.pdf
 * 
 * NOTE/TODO : make a common API Format/Store for language ?
 * 
 * @author Johann Sorel
 */
public class SpirFormat extends DefaultFormat {

    public static final SpirFormat INSTANCE = new SpirFormat();
    
    public SpirFormat() {
        super(new Chars("spirv"), 
              new Chars("Spir-V"), 
              new Chars("Standard Portable Intermediate Representation"), 
              new Chars[0], 
              new Chars[0], 
              new byte[][]{SpirConstants.SIGNATURE_BE});
    }
    
}
