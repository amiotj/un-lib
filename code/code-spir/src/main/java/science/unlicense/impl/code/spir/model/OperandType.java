
package science.unlicense.impl.code.spir.model;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class OperandType {
    
    public static final ResultType RESULT_TYPE = new ResultType();
    public static final Result RESULT = new Result();
        
    private final Chars name;

    public OperandType(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }
    
    public static class Simple extends OperandType{
        
        final int size;

        /**
         * 
         * @param name common operand name
         * @param size size of the operand value in number of words.
         */
        public Simple(Chars name, int size) {
            super(name);
            this.size = size;
        }

        /**
         * 
         * @return size of the operand value in number of words.
         */
        public int getSize() {
            return size;
        }
    }

    public static class ResultType extends Simple{

        private static final Chars NAME = new Chars("ResultType");
        
        private ResultType() {
            super(NAME,1);
        }
    }
    
    public static class Result extends Simple{

        private static final Chars NAME = new Chars("Result");

        private Result() {
            super(NAME,1);
        }
    }
    
    public static class Aggregate extends OperandType{
        
        final OperandType[] subs;

        public Aggregate(Chars name, OperandType[] subs) {
            super(name);
            this.subs = subs;
        }

        public OperandType[] getOperands() {
            return subs;
        }
    }

}
