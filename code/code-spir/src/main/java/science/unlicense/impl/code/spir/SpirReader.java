
package science.unlicense.impl.code.spir;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.DataInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.code.spir.model.Instruction;
import science.unlicense.impl.code.spir.model.InstructionType;
import science.unlicense.impl.code.spir.model.Module;
import science.unlicense.impl.code.spir.model.OperandType;

/**
 * SPIR-V Reader.
 * 
 * @author Johann Sorel
 */
public class SpirReader extends AbstractReader {

    private static final Dictionary OPCODES = new HashDictionary();
    static {
        
    }
    
    private InstructionType getOpCode(int code){
        InstructionType type = (InstructionType) OPCODES.getValue(code);
        if(type==null){
            type = new InstructionType(new Chars("unknown-"+code),code,new OperandType[0]) {
                public Instruction create() {
                    return new Instruction(this);
                }
            };
            OPCODES.add(code, type);
        }
        return type;
    }
    
    /**
     * 
     * @return Module
     */
    public Module read() throws IOException{
        
        final DataInputStream ds = getInputAsDataStream(NumberEncoding.BIG_ENDIAN);
        
        //detect encoding
        final byte[] sign = ds.readFully(new byte[4]);
        if(Arrays.equals(SpirConstants.SIGNATURE_BE, sign)){
            ds.setEncoding(NumberEncoding.BIG_ENDIAN);
        }else if(Arrays.equals(SpirConstants.SIGNATURE_LE, sign)){
            ds.setEncoding(NumberEncoding.LITTLE_ENDIAN);
        }else{
            throw new IOException("Stream is not a valid SPIR bytecode");
        }
        
        final Module module = new Module();
        module.version      = ds.readInt();
        module.generator    = ds.readInt();
        module.bound        = ds.readInt();
        module.schema       = ds.readInt();
        
        final byte[] arr = new byte[2];
        for(arr[0] = (byte)ds.read();arr[0]!=0;){
            arr[1] = ds.readByte();
            final int wordCount = ds.getEncoding().readUShort(arr, 0);
            final int opcode = ds.readUShort();
            final InstructionType type = getOpCode(opcode);
            final Instruction inst = type.create();
            inst.operands = ds.readInt(new int[wordCount-1]);
            module.instructions.add(inst);
        }
        
        return module;
    }
    
}
