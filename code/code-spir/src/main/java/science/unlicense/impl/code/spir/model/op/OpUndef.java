
package science.unlicense.impl.code.spir.model.op;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.code.spir.model.Instruction;
import science.unlicense.impl.code.spir.model.InstructionType;
import science.unlicense.impl.code.spir.model.OperandType;

/**
 *
 * @author Johann Sorel
 */
public class OpUndef extends Instruction {

    public static final InstructionType TYPE = new InstructionType(new Chars("OpNop"), 45, new OperandType[]{
        OperandType.RESULT_TYPE,OperandType.RESULT}) {
            
        public Instruction create() {
            return new OpUndef();
        }
    };

    public OpUndef() {
        super(TYPE);
    }
    
}
