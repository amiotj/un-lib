
package science.unlicense.impl.code.spir.model;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public abstract class InstructionType {

    public static final OperandType[] OPERAND_EMPTY = new OperandType[0];

    private final Chars name;
    private final int opcode;
    private final OperandType[] operands;

    public InstructionType(Chars name, int opcode, OperandType[] operands) {
        this.name = name;
        this.opcode = opcode;
        this.operands = operands;
    }

    public Chars getName() {
        return name;
    }

    public int getOpcode() {
        return opcode;
    }

    public OperandType[] getOperands() {
        return operands;
    }

    public abstract Instruction create();

}
