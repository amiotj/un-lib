
package science.unlicense.impl.code.spir.model;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * SPIR Module. 
 * Similar to a opengl shader program or opencl kernel.
 * 
 * @author Johann Sorel
 */
public class Module {

    /**
     * SPIR-V version :
     * -  99 pre-release
     * - 100 v1.00
     */
    public int version;
    /**
     * Value associated with a compiler.
     * Metadata information which has no impact on the code.
     * Can be 0 even if such value is discouraged.
     */
    public int generator;
    /**
     * Maximum number used for an id.
     */
    public int bound;
    /**
     * Reserved for instruction schema.
     */
    public int schema;
    
    /**
     * Sequence of instructions
     */
    public final Sequence instructions = new ArraySequence();
    
}
