
package science.unlicense.impl.code.spir.model.op;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.code.spir.model.Instruction;
import science.unlicense.impl.code.spir.model.InstructionType;
import science.unlicense.impl.code.spir.model.OperandType;

/**
 *
 * @author Johann Sorel
 */
public class OpSource extends Instruction {

    public static final InstructionType TYPE = new InstructionType(new Chars("OpSource"), 1, new OperandType[]{
        new OperandType.Simple(new Chars("SourceLanguage"), 1),
        new OperandType.Simple(new Chars("Version"), 1)}) {
            
        public Instruction create() {
            return new OpSource();
        }
    };

    public OpSource() {
        super(TYPE);
    }
    
}
