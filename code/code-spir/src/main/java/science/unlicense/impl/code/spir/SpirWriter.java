
package science.unlicense.impl.code.spir;

import science.unlicense.api.CObjects;
import science.unlicense.api.io.AbstractWriter;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.impl.code.spir.model.Instruction;
import science.unlicense.impl.code.spir.model.Module;

/**
 * SPIR-V Writer.
 * 
 * @author Johann Sorel
 */
public class SpirWriter extends AbstractWriter {

    private NumberEncoding encoding = NumberEncoding.BIG_ENDIAN;    

    public NumberEncoding getEncoding() {
        return encoding;
    }

    public void setEncoding(NumberEncoding encoding) {
        CObjects.ensureNotNull(encoding);
        this.encoding = encoding;
    }
               
    public void write(Module module) throws IOException{
    
        final DataOutputStream ds = getOutputAsDataStream(encoding);
        
        //header
        ds.writeInt(SpirConstants.SIGNATURE);
        ds.writeInt(99);
        ds.writeInt(module.generator);
        ds.writeInt(module.schema);
        
        //instructions
        for(int i=0,n=module.instructions.getSize();i<n;i++){
            final Instruction inst = (Instruction) module.instructions.get(i);
            ds.writeUShort(1 + inst.operands.length);
            ds.writeUShort(inst.type.getOpcode());
            ds.writeInt(inst.operands);
        }
        
    }
    
}
