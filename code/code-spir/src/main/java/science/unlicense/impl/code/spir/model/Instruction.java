
package science.unlicense.impl.code.spir.model;

import science.unlicense.api.array.Arrays;

/**
 * Spir-V instruction.
 * 
 * @author Johann Sorel
 */
public class Instruction {

    public final InstructionType type;
    public int[] operands;

    public Instruction(InstructionType type) {
        this(type,Arrays.ARRAY_INT_EMPTY);
    }

    public Instruction(InstructionType type, int[] operands) {
        this.type = type;
        this.operands = operands;
    }

}
