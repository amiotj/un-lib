
package science.unlicense.api.test;

/**
 * Exception thrown when an assertion fails.
 * 
 * @author Johann Sorel
 */
public class AssertException extends RuntimeException {

    public AssertException() {
    }
    
    public AssertException(String message) {
        super(message);
    }
    
    public AssertException(Object expected, Object value) {
        super("Expected value : "+expected+" but was : "+value);
    }
    
    public AssertException(String message, Throwable cause) {
        super(message,cause);
    }
    
}
