
package science.unlicense.api.test;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

/**
 * Maven test mojo.
 * Similar to JUnit or TestNG.
 * 
 * @author Johann Sorel
 */
@Mojo(name = "run", defaultPhase = LifecyclePhase.TEST, requiresDependencyResolution=ResolutionScope.TEST)
public class TestMojo extends AbstractMojo {

    private static final String BEFORE_CLASS = "beforeClass";
    private static final String AFTER_CLASS = "afterClass";
    private static final String BEFORE_TEST = "beforeTest";
    private static final String AFTER_TEST = "afterTest";
    
    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;
    
    @Parameter(property = "test", required = false, readonly = true)
    private String test;

    
    private String testedClass = null;
    private String testedMethod = null;
        
    @Override
    public void execute() throws MojoExecutionException {
        //check if tests are skipped
        if ("true".equalsIgnoreCase(System.getProperty("skipTests"))) {
            getLog().info("Tests are skipped");
            return;
        }
        
        //check if it's a single test file run
        String testedParam = System.getProperty("test");
        testedClass = null;
        testedMethod = null;
        if (testedParam!=null) {
            final int idx = testedParam.indexOf('$');
            if (idx>0) {
                testedClass = testedParam.substring(0, idx);
                testedMethod = testedParam.substring(idx+1);
            } else {
                testedClass = testedParam;
            }
        }
        
        //create the test classloader
        final List<URL> classpath = new ArrayList<>();
        try {
            listMainClassPathElements(classpath);
            listTestClassPathElements(classpath);
        } catch (DependencyResolutionRequiredException ex) {
            throw new MojoExecutionException(ex.getMessage(), ex);
        }
        final URLClassLoader loader = new URLClassLoader(classpath.toArray(new URL[0]));
        
        //list the test classes to execute
        final List<String> testFiles = project.getTestCompileSourceRoots();
        final List<String> testClasses = new ArrayList<>();
        for (String testFile : testFiles) {
            List<File> lst = listSourceFiles(new File(testFile), null);
            for (File f : lst) {
                String className = f.getPath().replace(testFile, "").replace(File.separatorChar, '.');
                className = className.substring(0, className.lastIndexOf('.'));
                if (className.charAt(0)=='.') className = className.substring(1);
                if (testedClass!=null && !testedClass.equals(className)) continue;
                testClasses.add(className);
            }
        }
        
        boolean globalSucess = true;
        for(String f : testClasses) {
            if (!f.endsWith("Test")) continue;
            getLog().info("Running tests : "+f);
            try {
                final Class<?> candidate = loader.loadClass(f);
                if ((candidate.getModifiers() & Modifier.ABSTRACT) != 0) continue;
                globalSucess &= executeTestClass(candidate);
            } catch (ClassNotFoundException | InstantiationException | 
                     IllegalAccessException | InvocationTargetException ex) {
                throw new MojoExecutionException(ex.getMessage(), ex);
            }
        }
        
        if (!globalSucess) {
            throw new MojoExecutionException("Tests in error.");
        }
    }

    private boolean executeTestClass(Class clazz) throws InstantiationException, 
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        
        boolean globalSucess = true;
        
        final Method beforeClass = getMethod(clazz, BEFORE_CLASS);
        final Method afterClass = getMethod(clazz, AFTER_CLASS);
        final Method beforeTest = getMethod(clazz, BEFORE_TEST);
        final Method afterTest = getMethod(clazz, AFTER_TEST);
        
        final Object instance = clazz.newInstance();
        
        if (beforeClass!=null) beforeClass.invoke(instance);
        
        int nb = 0;
        int sucess = 0;
        for (Method m : clazz.getMethods()) {
            if(!m.getName().startsWith("test")) continue;
            if((m.getModifiers() & Modifier.ABSTRACT) != 0) continue;
            if(m.getParameterCount()!=0) continue;
            if (testedMethod!=null && !testedMethod.equals(m.getName())) continue;
            nb++;
            
            if (beforeTest!=null) beforeTest.invoke(instance);
            
            try {
                m.invoke(instance);
                sucess++;
            }catch(InvocationTargetException ex) {
                globalSucess = false;
                getLog().error("Test fail : "+clazz.getName()+"."+m.getName()+"   "+ex.getMessage(),ex);
            }
            
            if (afterTest!=null) afterTest.invoke(instance);
        }
        
        if (afterClass!=null) afterClass.invoke(instance);
        
        if (nb==sucess) {
            getLog().info("Results : "+sucess+"/"+nb);
        } else {
            getLog().error("Results : "+sucess+"/"+nb);
        }
        
        return globalSucess;
    }
    
    private static List<File> listSourceFiles(File f, List<File> sources){
        if(sources == null){
            sources = new ArrayList<>();
        }

        if(f.isDirectory()){
            for(File c : f.listFiles()){
                listSourceFiles(c, sources);
            }
        }else if(f.getName().endsWith(".java") || f.getName().endsWith(".class")){
            sources.add(f);
        }

        return sources;
    }

    private void listMainClassPathElements(List<URL> projectClasspathList) 
            throws MojoExecutionException, DependencyResolutionRequiredException {
        
        final List<String> classpathElements = project.getCompileClasspathElements();
        for (String element : classpathElements) {
            try {
                projectClasspathList.add(new File(element).toURI().toURL());
            } catch (MalformedURLException e) {
                throw new MojoExecutionException(element + " is an invalid classpath element", e);
            }
        }
    }

    private void listTestClassPathElements(List<URL> projectClasspathList) 
            throws MojoExecutionException, DependencyResolutionRequiredException {
        
        final List<String> classpathElements = project.getTestClasspathElements();
        for (String element : classpathElements) {
            try {
                projectClasspathList.add(new File(element).toURI().toURL());
            } catch (MalformedURLException e) {
                throw new MojoExecutionException(element + " is an invalid classpath element", e);
            }
        }
    }

    private static Method getMethod(Class clazz, String name) {
        try {
            return clazz.getMethod(name);
        } catch (NoSuchMethodException ex) {
            return null;
        } catch (SecurityException ex) {
            return null;
        }
    }
}