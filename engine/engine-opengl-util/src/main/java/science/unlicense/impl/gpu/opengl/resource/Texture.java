
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.image.Image;

/**
 * Resource to manipulate a texture object.
 *
 * @author Johann Sorel
 */
public interface Texture extends Resource,Image{
    
    /**
     * Resize the texture.
     * Warning : all texture datas will be lost.
     *
     * @param gl
     * @param size new image size
     */
    public void resize(GL gl, Extent size);

    /**
     * Get GL texture type.
     * @return GL_TEXTURE_2D, GL_TEXTURE_2D_MULTISAMPLE
     */
    int getTextureType();

    /**
     * Get an opengl description of the texture.
     * @return TextureInfo
     */
    TextureModel getInfo();
    
    /**
     * Unloads the texture and change the internal image data structure.
     * 
     * @param info new texture definition
     */
    void reformat(GL gl, TextureModel info);
    
    /**
     * Indicate if the current texture is up to date with the system image.
     * @return true if texture needs to be reloaded.
     */
    boolean isDirty();

    /**
     * In memory image.
     *
     * @return Image or null if not loaded on system memory
     */
    Image getImage();

    void setImage(Image image);

    void bind(GL gl);

    void unbind(GL gl);

}
