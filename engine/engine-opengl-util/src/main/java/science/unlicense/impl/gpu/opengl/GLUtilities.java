
package science.unlicense.impl.gpu.opengl;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.FloatBuffer;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.number.Primitive;
import science.unlicense.api.color.AlphaBlending;
import static science.unlicense.api.color.AlphaBlending.*;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLBindings;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.gpu.opengl.GLSource;

/**
 *
 * @author Johann Sorel
 */
public final class GLUtilities {

    protected static final int[] PRIMITIVE_TO_GL = new int[]{
        0,
        0,
        0,
        0,
        GLC.TYPE.BYTE,
        GLC.TYPE.UBYTE,
        GLC.TYPE.SHORT,
        GLC.TYPE.USHORT,
        0,
        0,
        GLC.TYPE.INT,
        GLC.TYPE.UINT,
        0,
        0,
        GLC.TYPE.FLOAT,
        GLC.TYPE.DOUBLE
    };
        
    private GLUtilities(){}
    
    /**
    * Clear 	 GL_ZERO 	 GL_ZERO
    * Src       GL_ONE 	 GL_ZERO
    * SrcOver 	 GL_ONE 	 GL_ONE_MINUS_SRC_ALPHA
    * DstOver 	 GL_ONE_MINUS_DST_ALPHA 	 GL_ONE
    * SrcIn 	 GL_DST_ALPHA 	 GL_ZERO
    * DstIn 	 GL_ZERO 	 GL_SRC_ALPHA
    * SrcOut 	 GL_ONE_MINUS_DST_ALPHA 	 GL_ZERO
    * DstOut 	 GL_ZERO 	 GL_ONE_MINUS_SRC_ALPHA
    * Dst       GL_ZERO 	 GL_ONE
    * SrcAtop 	 GL_DST_ALPHA 	 GL_ONE_MINUS_SRC_ALPHA
    * DstAtop 	 GL_ONE_MINUS_DST_ALPHA 	 GL_SRC_ALPHA
    * AlphaXor  GL_ONE_MINUS_DST_ALPHA 	 GL_ONE_MINUS_SRC_ALPHA
    * 
     * @param gl
     * @param blending
    */
    public static void configureBlending(GL gl, AlphaBlending blending){
        gl.asGL1().glEnable(GL_BLEND);
        final int type = blending.getType();
        if(type == CLEAR)           gl.asGL1().glBlendFunc(GL_ZERO,                  GL_ZERO);
        else if(type == SRC)        gl.asGL1().glBlendFunc(GL_ONE,                   GL_ZERO);
        else if(type == SRC_OVER)   gl.asGL1().glBlendFunc(GL_ONE,                   GL_ONE_MINUS_SRC_ALPHA);
        else if(type == DST_OVER)   gl.asGL1().glBlendFunc(GL_ONE_MINUS_DST_ALPHA,   GL_ONE);
        else if(type == SRC_IN)     gl.asGL1().glBlendFunc(GL_DST_ALPHA,             GL_ZERO);
        else if(type == DST_IN)     gl.asGL1().glBlendFunc(GL_ZERO,                  GL_SRC_ALPHA);
        else if(type == SRC_OUT)    gl.asGL1().glBlendFunc(GL_ONE_MINUS_DST_ALPHA,   GL_ZERO);
        else if(type == DST_OUT)    gl.asGL1().glBlendFunc(GL_ZERO,                  GL_ONE_MINUS_SRC_ALPHA);
        else if(type == DST)        gl.asGL1().glBlendFunc(GL_ZERO,                  GL_ONE);
        else if(type == SRC_ATOP)   gl.asGL1().glBlendFunc(GL_DST_ALPHA,             GL_SRC_ALPHA);
        else if(type == DST_ATOP)   gl.asGL1().glBlendFunc(GL_ONE_MINUS_DST_ALPHA,   GL_ONE_MINUS_SRC_ALPHA);
        //if(type == alphaxor)    worker.gl.glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        else throw new RuntimeException("Unknowed alpha blending :"+type);
        gl.asGL1().glBlendEquation(GL_FUNC_ADD);
    }
    
    /**
     * Calculate stride value for given primitive type and tuple size.
     * 
     * @param gpuType, one of :
     * - GL_UNSIGNED_BYTE
     * - GL_BYTE
     * - GL_UNSIGNED_SHORT
     * - GL_SHORT
     * - GL_UNSIGNED_INT
     * - GL_INT 
     * - GL_FLOAT
     * - GL_DOUBLE
     * @param tupleSize number of elements in a tuple
     * @return stride size
     */
    public static int calculateStride(int gpuType, int tupleSize){
        return byteSize(gpuType)*tupleSize;
    }
    
    /**
     * Get GPU primitive type size in bytes.
     * 
     * @param gpuType, one of :
     * - GL_UNSIGNED_BYTE
     * - GL_BYTE
     * - GL_UNSIGNED_SHORT
     * - GL_SHORT
     * - GL_UNSIGNED_INT
     * - GL_INT 
     * - GL_FLOAT
     * - GL_DOUBLE
     * @return value size in bytes
     */
    public static int byteSize(final int gpuType){
        switch(gpuType){
            case GLC.TYPE.UBYTE : 
            case GLC.TYPE.BYTE : 
                return 1;
            case GLC.TYPE.USHORT : 
            case GLC.TYPE.SHORT : 
                return 2;
            case GLC.TYPE.UINT : 
            case GLC.TYPE.INT : 
            case GLC.TYPE.FLOAT : 
                return 4;
            case GLC.TYPE.DOUBLE : 
                return 8;
            default :
                throw new InvalidArgumentException("Unexpected type : "+gpuType);
        }
    }
    
    public static int glTypeToPrimitiveType(int gltype){
        if(gltype == GLC.TYPE.BYTE){
            return Primitive.TYPE_BYTE;
        }else if(gltype == GLC.TYPE.UBYTE){
            return Primitive.TYPE_UBYTE;
        }else if(gltype == GLC.TYPE.SHORT){
            return Primitive.TYPE_SHORT;
        }else if(gltype == GLC.TYPE.USHORT){
            return Primitive.TYPE_USHORT;
        }else if(gltype == GLC.TYPE.INT){
            return Primitive.TYPE_INT;
        }else if(gltype == GLC.TYPE.UINT){
            return Primitive.TYPE_UINT;
        }else if(gltype == GLC.TYPE.FLOAT){
            return Primitive.TYPE_FLOAT;
        }else if(gltype == GLC.Texture.Type.UNSIGNED_INT_24_8){
            return Primitive.TYPE_BYTE;
        }else{
            throw new RuntimeException("Unsupported type :" + gltype);
        }
    }
    
    public static int primitiveTypeToGlType(int primitiveType){
        return PRIMITIVE_TO_GL[primitiveType];
    }
    
    public static Buffer asGLBuffer(science.unlicense.api.buffer.Buffer buffer){
        final Object backEnd = buffer.getBackEnd();
        if(backEnd instanceof byte[]){
            return ByteBuffer.wrap((byte[]) backEnd);
        }else if(backEnd instanceof int[]){
            return IntBuffer.wrap((int[]) backEnd);
        }else if(backEnd instanceof float[]){
            return FloatBuffer.wrap((float[]) backEnd);
        }else if(backEnd instanceof Buffer){
            return (Buffer) backEnd;
        }else{
            throw new InvalidArgumentException("Buffer is not a primitive type : "+buffer);
        }
    }

    public static int inverseCulling(int culling){
        if(culling == GLC.CULLING.BACK) return GLC.CULLING.FRONT;
        else if(culling == GLC.CULLING.FRONT) return GLC.CULLING.BACK;
        else return culling;
    }

    public static GLSource createOffscreenSource(int width, int height) {
        return GLBindings.getBindings()[0].createOffScreen(width, height);
    }

    ////////////////////////////////////////////////////////////////////////////
    // ERROR utils /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Discard any error in the gl stack.
     * @param gl
     */
    public static void discardErrors(GL gl){
        try{
            //we don't want to know about previous exceptions
            //discard them if any.
            GLUtilities.checkGLErrors(gl);
        }catch(GLException ex){
        }
    }

    /**
     * Checks for GL errors, fails with runtime exception.
     */
    public static void checkGLErrorsFail(GL gl) {
        try {
            GLUtilities.checkGLErrors(gl);
        } catch (GLException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    /**
     * Loops on GL error and throw a GLException if any.
     */
    public static void checkGLErrors(GL gl) throws GLException {
        if(gl==null) return;
        int errorNum = gl.asGL1().glGetError();
        if(errorNum == GL_NO_ERROR) return;

        final CharBuffer sb = new CharBuffer();
        do{
            if(errorNum == GL_INVALID_ENUM){
                sb.append("GL_INVALID_ENUM : An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.\n");
            }else if(errorNum == GL_INVALID_VALUE){
                sb.append("GL_INVALID_VALUE : A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.\n");
            }else if(errorNum == GL_INVALID_OPERATION){
                sb.append("GL_INVALID_OPERATION : The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.\n");
            }else if(errorNum == GL_INVALID_FRAMEBUFFER_OPERATION){
                sb.append("GL_INVALID_FRAMEBUFFER_OPERATION : The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.\n");
            }else if(errorNum == GL_OUT_OF_MEMORY){
                sb.append("GL_OUT_OF_MEMORY : There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.\n");
            }else if(errorNum == GL_STACK_UNDERFLOW){
                sb.append("GL_STACK_UNDERFLOW : An attempt has been made to perform an operation that would cause an internal stack to underflow.\n");
            }else if(errorNum == GL_STACK_OVERFLOW){
                sb.append("GL_STACK_OVERFLOW : An attempt has been made to perform an operation that would cause an internal stack to overflow.\n");
            }else{
                sb.append("GL_UNKNOWNED_ERROR : ").append(errorNum).append("\n");
            }

            errorNum = gl.asGL1().glGetError();
        }while(errorNum != GL_NO_ERROR);

        throw new GLException(sb.toString());
    }

}
