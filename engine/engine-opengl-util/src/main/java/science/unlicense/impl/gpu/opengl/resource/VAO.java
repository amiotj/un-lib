
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES3;

/**
 * Vertex array object resource.
 *
 * @author Johann Sorel
 */
public class VAO extends AbstractResource {

    private final Sequence vbos = new ArraySequence();

    //gpu id
    private final int[] bufferId = new int[]{-1};

    public VAO() {}

    /**
     * Sequence of vbo definitions attached to this VAO.
     *
     * @return never null, can be empty.
     */
    public Sequence getVBODefinitions(){
        return vbos;
    }

    /**
     * {@inheritDoc }
     */
    public int getGpuID() {
        return bufferId[0];
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnSystemMemory() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnGpuMemory() {
        return bufferId[0] != -1;
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        throw new UnimplementedException("Not supported yet.");
    }

    /**
     * OpenGL constraints : GL2ES3
     * 
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1) return; //already loaded

        final GL2ES3 gl2 = gl.asGL2ES3();
        
        gl2.glGenVertexArrays(bufferId);
        gl2.glBindVertexArray(bufferId[0]);

        for(int i=0,n=vbos.getSize();i<n;i++){
            final Definition def = (Definition) vbos.get(i);
            def.vbo.loadOnGpuMemory(gl2);
            def.vbo.bind(gl2, def.layoutIndex, def.normalized, def.offset);
        }

        gl2.asGL2ES2().glEnableVertexAttribArray(0);
        gl2.glBindVertexArray(0);
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        //TODO what do we do ? propagate to vbos ?
    }

    /**
     * OpenGL constraints : GL2ES3
     * 
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1){
            gl.asGL2ES3().glDeleteVertexArrays(bufferId);
            bufferId[0] = -1;
        }
    }

    /**
     * Bind this vertex array.
     * 
     * OpenGL constraints : GL2ES3
     *
     * @param gl OpenGL instance
     */
    public void bind(GL gl){
        gl.asGL2ES3().glBindVertexArray(bufferId[0]);
    }

    /**
     * Unbind this vertex array.
     * 
     * OpenGL constraints : GL2ES3
     *
     * @param gl OpenGL instance
     */
    public void unbind(GL gl){
        gl.asGL2ES3().glBindVertexArray(0);
    }

    /**
     * Define the parameters of a VBO in a VAO
     */
    public static class Definition {

        private final VBO vbo;
        private final int layoutIndex;
        private final boolean normalized;
        private final int offset;

        public Definition(VBO vbo, int layoutIndex) {
            this(vbo,layoutIndex,false,0);
        }

        public Definition(VBO vbo, int layoutIndex, boolean normalized, int offset) {
            this.vbo = vbo;
            this.layoutIndex = layoutIndex;
            this.normalized = normalized;
            this.offset = offset;
        }

        public VBO getVbo() {
            return vbo;
        }

        public int getLayoutIndex() {
            return layoutIndex;
        }

        public boolean isNormalized() {
            return normalized;
        }

        public int getOffset() {
            return offset;
        }

    }

}
