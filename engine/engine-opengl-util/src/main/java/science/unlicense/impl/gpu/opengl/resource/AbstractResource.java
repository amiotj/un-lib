
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.logging.Loggers;

/**
 * Abstract resouce implementation.
 * It's only purpose is to track unreleased gpu data.
 *
 * @author Johann Sorel
 */
public abstract class AbstractResource extends CObject implements Resource{

    private final Exception ex = new Exception();
    protected CharArray name = Chars.EMPTY;

    private boolean forgetOnLoad = false;

    public AbstractResource(){
        this(false);
    }
    
    public AbstractResource(boolean forgetOnLoad){
        this.forgetOnLoad = forgetOnLoad;
        ex.fillInStackTrace();
    }

    public CharArray getName() {
        return name;
    }

    public void setName(CharArray name) {
        CObjects.ensureNotNull(name, "name");
        this.name = name;
    }

    public boolean isForgetOnLoad() {
        return forgetOnLoad;
    }

    public void setForgetOnLoad(boolean forget) {
        this.forgetOnLoad = forget;
    }

    protected void finalize() throws Throwable {
        final int gpuId = getGpuID();
        if(gpuId>=0){
            Loggers.get().info(
                    new Chars("Unreleased GPU resource : "+getClass().getSimpleName()
                    +" with gpu-id="+gpuId));
            ex.printStackTrace();
        }
        super.finalize();
    }

}
