
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.exception.InvalidArgumentException;
import static science.unlicense.impl.gpu.opengl.GLC.Texture.*;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.image.Image;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 * Resource to manipulate a texture 2D object.
 *
 * @author Johann Sorel
 */
public class Texture2D extends AbstractTexture implements Texture{

    public static TextureModel COLOR_RGB_CLAMPED(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGB8, Format.RGB, Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel COLOR_RGB_REPEATED(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGB8, Format.RGB, Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.REPEAT);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.REPEAT);
        return tm;
    }
    
    public static TextureModel COLOR_RGBA_CLAMPED(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGBA8, Format.RGBA, Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel COLOR_RGBA_REPEATED(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGBA8, Format.RGBA, Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.REPEAT);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.REPEAT);
        return tm;
    }

    public static TextureModel VEC1_INT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.R32I, Format.RED_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC2_INT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RG32I, Format.RG_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC3_INT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGB32I, Format.RGB_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC4_INT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGBA32I, Format.RGBA_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC1_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.R32F, Format.RED, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC2_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RG32F, Format.RG, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC3_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGB32F, Format.RGB, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC4_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGBA32F, Format.RGBA, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel DEPTH_16F (){
        final TextureModel tm = TextureModel.create2D(InternalFormat.DEPTH_COMPONENT16, Format.DEPTH_COMPONENT, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY,   Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY,   Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,       Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,       Parameters.WRAP_T.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.COMPARE_MODE.KEY, Parameters.COMPARE_MODE.REF_TO_TEXTURE);
        tm.setParameter(Parameters.COMPARE_FUNC.KEY, Parameters.COMPARE_FUNC.LESS);
        return tm;
    }
    
    public static TextureModel DEPTH_32F(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.DEPTH_COMPONENT32F, Format.DEPTH_COMPONENT, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY,   Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY,   Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,       Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,       Parameters.WRAP_T.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.COMPARE_MODE.KEY, Parameters.COMPARE_MODE.REF_TO_TEXTURE);
        tm.setParameter(Parameters.COMPARE_FUNC.KEY, Parameters.COMPARE_FUNC.LESS);
        return tm;
    }
    
    public static TextureModel DEPTH_24(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.DEPTH_COMPONENT24, Format.DEPTH_COMPONENT, Type.UNSIGNED_INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY,   Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY,   Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,       Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,       Parameters.WRAP_T.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.COMPARE_MODE.KEY, Parameters.COMPARE_MODE.REF_TO_TEXTURE);
        tm.setParameter(Parameters.COMPARE_FUNC.KEY, Parameters.COMPARE_FUNC.LESS);
        return tm;
    }

    public static TextureModel DEPTH24_STENCIL8(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.DEPTH24_STENCIL8, Format.DEPTH_STENCIL, Type.UNSIGNED_INT_24_8);
        tm.setParameter(Parameters.MIN_FILTER.KEY,   Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY,   Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,       Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,       Parameters.WRAP_T.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.COMPARE_MODE.KEY, Parameters.COMPARE_MODE.REF_TO_TEXTURE);
        tm.setParameter(Parameters.COMPARE_FUNC.KEY, Parameters.COMPARE_FUNC.LESS);
        return tm;
    }

    public Texture2D(Image image) {
        this(image,false,false);
    }
    
    public Texture2D(Image image, boolean mipmap, boolean clip) {
        super(image, null);
        this.image = TextureUtils.makeCompatible(image,true);
        this.info = TextureUtils.toTextureInfo(this.image,true);
        
        if(mipmap && clip){
            info.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.LINEAR_MIPMAP_LINEAR);
            info.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.LINEAR);
            info.setParameter(GLC.Texture.Parameters.WRAP_S.KEY,     GLC.Texture.Parameters.WRAP_S.CLAMP_TO_BORDER);
            info.setParameter(GLC.Texture.Parameters.WRAP_T.KEY,     GLC.Texture.Parameters.WRAP_T.CLAMP_TO_BORDER);
        }else if(mipmap){
            info.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.LINEAR_MIPMAP_LINEAR);
            info.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.LINEAR);
            info.setParameter(GLC.Texture.Parameters.WRAP_S.KEY,     GLC.Texture.Parameters.WRAP_S.REPEAT);
            info.setParameter(GLC.Texture.Parameters.WRAP_T.KEY,     GLC.Texture.Parameters.WRAP_T.REPEAT);
        }else if(clip){
            info.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.LINEAR);
            info.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.LINEAR);
            info.setParameter(GLC.Texture.Parameters.WRAP_S.KEY,     GLC.Texture.Parameters.WRAP_S.CLAMP_TO_BORDER);
            info.setParameter(GLC.Texture.Parameters.WRAP_T.KEY,     GLC.Texture.Parameters.WRAP_T.CLAMP_TO_BORDER);
        }else{
            info.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.LINEAR);
            info.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.LINEAR);
            info.setParameter(GLC.Texture.Parameters.WRAP_S.KEY,     GLC.Texture.Parameters.WRAP_S.REPEAT);
            info.setParameter(GLC.Texture.Parameters.WRAP_T.KEY,     GLC.Texture.Parameters.WRAP_T.REPEAT);
        }
    }
    
    public Texture2D(Image image, TextureModel info) {
        super(image, info);
        if(info.getTarget()!=GLC.Texture.Target.TEXTURE_2D){
            throw new InvalidArgumentException("Invalid target format, only TEXTURE_2D can be used.");
        }
    }

    public Texture2D(int width, int height, TextureModel model) {
        super(new Extent.Long(width, height), model);
    }
        
    public int getWidth(){
        return (int)size.get(0);
    }
    
    public int getHeight(){
        return (int)size.get(1);
    }

    public void reformat(GL gl, TextureModel info) {
        if(info.getTarget()!=GLC.Texture.Target.TEXTURE_2D){
            throw new InvalidArgumentException("Invalid target format, only TEXTURE_2D can be used.");
        }
        super.reformat(gl, info);
    }
    
    /**
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) {

        if(info != null){
            image = TextureUtils.downloadTexture(gl,texId,size,info);
            GLUtilities.checkGLErrorsFail(gl);
        }else{
            //TODO this is not guarantee, must find a generic way to grab image type
            image = TextureUtils.downloadTexture(gl,texId,size,null);
            GLUtilities.checkGLErrorsFail(gl);
        }
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) {
        if(texId>=0 && !dirty) return;
        dirty = false;

        if(texId>=0){
            //image has changed, reupload datas
            gl.asGL1().glBindTexture(GLC.Texture.Target.TEXTURE_2D, texId);
            if(image==null){
                TextureUtils.createTexture(new Extent.Double(getWidth(), getHeight()), gl, info);
            }else{
                TextureUtils.loadTexture(gl, image, GLC.Texture.Target.TEXTURE_2D);
                if(info.hasMipMap()){
                    generateMipMap(gl);
                }
            }
            gl.asGL1().glBindTexture(GLC.Texture.Target.TEXTURE_2D, 0);
            
        }else{
            //clean gpu memory before loading
            unloadFromGpuMemory(gl);

            if(image==null){
                //create a new texture
                int[] temp = new int[1];
                gl.asGL1().glGenTextures(temp);
                texId = temp[0];
                gl.asGL1().glBindTexture(GLC.Texture.Target.TEXTURE_2D, texId);
                TextureUtils.createTexture(new Extent.Double(getWidth(), getHeight()), gl, info);
                gl.asGL1().glBindTexture(GLC.Texture.Target.TEXTURE_2D, 0);

            }else{
                //load the image on gpu
                texId = TextureUtils.loadTexture(gl, image, info);
            }
        }
        
        if(isForgetOnLoad()){
            image = null;
        }
    }
    
    private void generateMipMap(GL gl){
        bind(gl);
        gl.asGL2ES2().glGenerateMipmap(GL_TEXTURE_2D);
        unbind(gl);
    }

}
