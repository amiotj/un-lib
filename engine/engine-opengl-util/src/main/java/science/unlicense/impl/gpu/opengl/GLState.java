
package science.unlicense.impl.gpu.opengl;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import science.unlicense.api.CObject;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.image.sample.RawModel.*;
import static science.unlicense.impl.gpu.opengl.GLC.GETSET.*;

/**
 * Store all GL state variables.
 * 
 * @author Johann Sorel
 */
public class GLState extends CObject{
    
    private static class Value{
        private final int code;
        private final int type;
        private final int nbVal;
        private final boolean settable;
        /** default value */
        private final Object def;
        
        public Value(int code, int type, int nbVal) {
            this.code = code;
            this.type = type;
            this.nbVal = nbVal;
            this.settable = false;
            this.def = null;
        }
        
        public Value(int code, int type, int nbVal, Object def) {
            this.code = code;
            this.type = type;
            this.nbVal = nbVal;
            this.settable = true;
            this.def = def;
        }

        public int getNbVal(GL gl) {
            return nbVal;
        }
        
        public void reset(GL gl){
            if(settable) set(gl,def);
        }
        
        void set(GL gl, Object value){}

        @Override
        public String toString() {
            final Field[] fields = GLC.GETSET.class.getDeclaredFields();
            try{
                for(Field f : fields){
                    if(f.getInt(null) == code){
                        return f.getName();
                    }
                }
            }catch(IllegalAccessException ex){
                ex.printStackTrace();
            }
            return ""+code;
        }        
    }
    private static final Sequence VALUES = new ArraySequence();
    static {
        VALUES.add(new Value(ACTIVE_TEXTURE,                     TYPE_INT   ,1, GL_TEXTURE0) {void set(GL gl,Object val){gl.asGL1().glActiveTexture((Integer)val);}});
        VALUES.add(new Value(ALIASED_LINE_WIDTH_RANGE,           TYPE_FLOAT ,2));
        VALUES.add(new Value(ARRAY_BUFFER_BINDING,               TYPE_INT   ,1));
        VALUES.add(new Value(BLEND_COLOR,                        TYPE_FLOAT ,4));
        VALUES.add(new Value(BLEND_DST_ALPHA,                    TYPE_INT   ,1));
        VALUES.add(new Value(BLEND_DST_RGB,                      TYPE_INT   ,1));
        VALUES.add(new Value(BLEND_EQUATION_RGB,                 TYPE_INT   ,1));
        VALUES.add(new Value(BLEND_EQUATION_ALPHA,               TYPE_INT   ,1));
        VALUES.add(new Value(BLEND_SRC_ALPHA,                    TYPE_INT   ,1));
        VALUES.add(new Value(BLEND_SRC_RGB,                      TYPE_INT   ,1));
        VALUES.add(new Value(COLOR_CLEAR_VALUE,                  TYPE_FLOAT ,4));
        VALUES.add(new Value(COLOR_WRITEMASK,                    TYPE_1_BIT ,4, new int[]{GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE}));
        VALUES.add(new Value(COMPRESSED_TEXTURE_FORMATS,         TYPE_INT   ,-1){
            public int getNbVal(GL gl) {
                return getInteger(gl, NUM_COMPRESSED_TEXTURE_FORMATS);
            }
        });
        VALUES.add(new Value(MAX_COMPUTE_SHADER_STORAGE_BLOCKS,  TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_SHADER_STORAGE_BLOCKS, TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMPUTE_UNIFORM_BLOCKS,         TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMPUTE_TEXTURE_IMAGE_UNITS,    TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMPUTE_UNIFORM_COMPONENTS,     TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMPUTE_ATOMIC_COUNTERS,        TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS, TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS,TYPE_INT,1));
        //VALUES.add(new Value(MAX_COMPUTE_WORK_GROUP_INVOCATIONS,TYPE_INT   ,1));
//        VALUES.add(new Value(MAX_COMPUTE_WORK_GROUP_COUNT,       TYPE_INT   ,3));
//        VALUES.add(new Value(MAX_COMPUTE_WORK_GROUP_SIZE,        TYPE_INT   ,3));
        VALUES.add(new Value(DISPATCH_INDIRECT_BUFFER_BINDING,   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_DEBUG_GROUP_STACK_DEPTH,        TYPE_INT   ,1));
        VALUES.add(new Value(DEBUG_GROUP_STACK_DEPTH,            TYPE_INT   ,1));
        VALUES.add(new Value(CONTEXT_FLAGS,                      TYPE_INT   ,1));
        VALUES.add(new Value(CURRENT_PROGRAM,                    TYPE_INT   ,1));
        VALUES.add(new Value(DEPTH_CLEAR_VALUE,                  TYPE_DOUBLE,1));
        VALUES.add(new Value(DEPTH_FUNC,                         TYPE_INT   ,1));
        VALUES.add(new Value(DEPTH_RANGE,                        TYPE_DOUBLE,2));
        VALUES.add(new Value(DEPTH_WRITEMASK,                    TYPE_1_BIT ,1));
        VALUES.add(new Value(DOUBLEBUFFER,                       TYPE_1_BIT ,1));
        VALUES.add(new Value(DRAW_BUFFER,                        TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_0,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_1,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_2,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_3,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_4,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_5,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_6,                      TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_BUFFER_7,                      TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_8,                      TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_9,                      TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_10,                     TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_11,                     TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_12,                     TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_13,                     TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_14,                     TYPE_INT   ,1));
//        VALUES.add(new Value(DRAW_BUFFER_15,                     TYPE_INT   ,1));
        VALUES.add(new Value(DRAW_FRAMEBUFFER_BINDING,           TYPE_INT   ,1));
        VALUES.add(new Value(READ_FRAMEBUFFER_BINDING,           TYPE_INT   ,1));
        VALUES.add(new Value(ELEMENT_ARRAY_BUFFER_BINDING,       TYPE_INT   ,1));
        VALUES.add(new Value(FRAGMENT_SHADER_DERIVATIVE_HINT,    TYPE_INT   ,1));
        VALUES.add(new Value(IMPLEMENTATION_COLOR_READ_FORMAT,   TYPE_INT   ,1));
        VALUES.add(new Value(IMPLEMENTATION_COLOR_READ_TYPE,     TYPE_INT   ,1));
        VALUES.add(new Value(LINE_SMOOTH_HINT,                   TYPE_INT   ,1));
        VALUES.add(new Value(LINE_WIDTH,                         TYPE_FLOAT ,1));
        VALUES.add(new Value(LAYER_PROVOKING_VERTEX,             TYPE_INT   ,1));
        VALUES.add(new Value(LOGIC_OP_MODE,                      TYPE_INT   ,1));
        VALUES.add(new Value(MAJOR_VERSION,                      TYPE_INT   ,1));
        VALUES.add(new Value(MAX_3D_TEXTURE_SIZE,                TYPE_INT   ,1));
        VALUES.add(new Value(MAX_ARRAY_TEXTURE_LAYERS,           TYPE_INT   ,1));
        VALUES.add(new Value(MAX_CLIP_DISTANCES,                 TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COLOR_TEXTURE_SAMPLES,          TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_ATOMIC_COUNTERS,       TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS,TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS,TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_TEXTURE_IMAGE_UNITS,   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_UNIFORM_BLOCKS,        TYPE_INT   ,1));
        VALUES.add(new Value(MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS,TYPE_INT   ,1));
        VALUES.add(new Value(MAX_CUBE_MAP_TEXTURE_SIZE,          TYPE_INT   ,1));
        VALUES.add(new Value(MAX_DEPTH_TEXTURE_SAMPLES,          TYPE_INT   ,1));
        VALUES.add(new Value(MAX_DRAW_BUFFERS,                   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_DUAL_SOURCE_DRAW_BUFFERS,       TYPE_INT   ,1));
        VALUES.add(new Value(MAX_ELEMENTS_INDICES,               TYPE_INT   ,1));
        VALUES.add(new Value(MAX_ELEMENTS_VERTICES,              TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAGMENT_ATOMIC_COUNTERS,       TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAGMENT_SHADER_STORAGE_BLOCKS, TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAGMENT_INPUT_COMPONENTS,      TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAGMENT_UNIFORM_COMPONENTS,    TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAGMENT_UNIFORM_VECTORS,       TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAGMENT_UNIFORM_BLOCKS,        TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_WIDTH,              TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_HEIGHT,             TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_LAYERS,             TYPE_INT   ,1));
        VALUES.add(new Value(MAX_FRAMEBUFFER_SAMPLES,            TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_ATOMIC_COUNTERS,       TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_SHADER_STORAGE_BLOCKS, TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_INPUT_COMPONENTS,      TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_OUTPUT_COMPONENTS,     TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_TEXTURE_IMAGE_UNITS,   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_UNIFORM_BLOCKS,        TYPE_INT   ,1));
        VALUES.add(new Value(MAX_GEOMETRY_UNIFORM_COMPONENTS,    TYPE_INT   ,1));
        VALUES.add(new Value(MAX_INTEGER_SAMPLES,                TYPE_INT   ,1));
        VALUES.add(new Value(MIN_MAP_BUFFER_ALIGNMENT,           TYPE_INT   ,1));
        VALUES.add(new Value(MAX_LABEL_LENGTH,                   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_PROGRAM_TEXEL_OFFSET,           TYPE_INT   ,1));
        VALUES.add(new Value(MIN_PROGRAM_TEXEL_OFFSET,           TYPE_INT   ,1));
        VALUES.add(new Value(MAX_RECTANGLE_TEXTURE_SIZE,         TYPE_INT   ,1));
        VALUES.add(new Value(MAX_RENDERBUFFER_SIZE,              TYPE_INT   ,1));
        VALUES.add(new Value(MAX_SAMPLES,                        TYPE_INT   ,1));
        VALUES.add(new Value(MAX_SAMPLE_MASK_WORDS,              TYPE_INT   ,1));
        VALUES.add(new Value(MAX_SERVER_WAIT_TIMEOUT,            TYPE_INT   ,1));
        VALUES.add(new Value(MAX_SHADER_STORAGE_BUFFER_BINDINGS, TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TESS_CONTROL_ATOMIC_COUNTERS,   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TESS_EVALUATION_ATOMIC_COUNTERS,TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS,TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS,TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TEXTURE_BUFFER_SIZE,            TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TEXTURE_IMAGE_UNITS,            TYPE_INT   ,1));
        VALUES.add(new Value(MAX_TEXTURE_LOD_BIAS,               TYPE_DOUBLE,1));
        VALUES.add(new Value(MAX_TEXTURE_SIZE,                   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_UNIFORM_BUFFER_BINDINGS,        TYPE_INT   ,1));
        VALUES.add(new Value(MAX_UNIFORM_BLOCK_SIZE,             TYPE_INT   ,1));
        VALUES.add(new Value(MAX_UNIFORM_LOCATIONS,              TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VARYING_COMPONENTS,             TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VARYING_VECTORS,                TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VARYING_FLOATS,                 TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_ATOMIC_COUNTERS,         TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_ATTRIBS,                 TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_SHADER_STORAGE_BLOCKS,   TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_TEXTURE_IMAGE_UNITS,     TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_UNIFORM_COMPONENTS,      TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_UNIFORM_VECTORS,         TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_OUTPUT_COMPONENTS,       TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_UNIFORM_BLOCKS,          TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VIEWPORT_DIMS,                  TYPE_INT   ,2));
        VALUES.add(new Value(MAX_VIEWPORTS,                      TYPE_INT   ,1));
        VALUES.add(new Value(MINOR_VERSION,                      TYPE_INT   ,1));
        VALUES.add(new Value(NUM_COMPRESSED_TEXTURE_FORMATS,     TYPE_INT   ,1));
        VALUES.add(new Value(NUM_EXTENSIONS,                     TYPE_INT   ,1));
        VALUES.add(new Value(NUM_PROGRAM_BINARY_FORMATS,         TYPE_INT   ,1));
        VALUES.add(new Value(NUM_SHADER_BINARY_FORMATS,          TYPE_INT   ,1));
        VALUES.add(new Value(PACK_ALIGNMENT,                     TYPE_INT   ,1));
        VALUES.add(new Value(PACK_IMAGE_HEIGHT,                  TYPE_INT   ,1));
        VALUES.add(new Value(PACK_LSB_FIRST,                     TYPE_1_BIT ,1));
        VALUES.add(new Value(PACK_ROW_LENGTH,                    TYPE_INT   ,1));
        VALUES.add(new Value(PACK_SKIP_IMAGES,                   TYPE_INT   ,1));
        VALUES.add(new Value(PACK_SKIP_PIXELS,                   TYPE_INT   ,1));
        VALUES.add(new Value(PACK_SKIP_ROWS,                     TYPE_INT   ,1));
        VALUES.add(new Value(PACK_SWAP_BYTES,                    TYPE_1_BIT ,1));
        VALUES.add(new Value(PIXEL_PACK_BUFFER_BINDING,          TYPE_INT   ,1));
        VALUES.add(new Value(PIXEL_UNPACK_BUFFER_BINDING,        TYPE_INT   ,1));
        VALUES.add(new Value(POINT_FADE_THRESHOLD_SIZE,          TYPE_INT   ,1));
        VALUES.add(new Value(PRIMITIVE_RESTART_INDEX,            TYPE_INT   ,1));
        VALUES.add(new Value(PROGRAM_BINARY_FORMATS,             TYPE_INT   ,-1){
            public int getNbVal(GL gl) {
                return getInteger(gl, NUM_PROGRAM_BINARY_FORMATS);
            }
        });
        VALUES.add(new Value(PROGRAM_PIPELINE_BINDING,           TYPE_INT   ,1));
        VALUES.add(new Value(PROVOKING_VERTEX,                   TYPE_INT   ,1));
        VALUES.add(new Value(POINT_SIZE,                         TYPE_FLOAT ,1));
        VALUES.add(new Value(POINT_SIZE_GRANULARITY,             TYPE_INT   ,1));
        VALUES.add(new Value(POINT_SIZE_RANGE,                   TYPE_FLOAT ,2));
        VALUES.add(new Value(POLYGON_OFFSET_FACTOR,              TYPE_FLOAT ,1));
        VALUES.add(new Value(POLYGON_OFFSET_UNITS,               TYPE_FLOAT ,1));
        VALUES.add(new Value(POLYGON_SMOOTH_HINT,                TYPE_INT   ,1));
        VALUES.add(new Value(READ_BUFFER,                        TYPE_INT   ,1));
        VALUES.add(new Value(RENDERBUFFER_BINDING,               TYPE_INT   ,1));
        VALUES.add(new Value(SAMPLE_BUFFERS,                     TYPE_INT   ,1));
        VALUES.add(new Value(SAMPLE_COVERAGE_VALUE,              TYPE_FLOAT ,1));
        VALUES.add(new Value(SAMPLE_COVERAGE_INVERT,             TYPE_1_BIT ,1));
        VALUES.add(new Value(SAMPLER_BINDING,                    TYPE_INT   ,1));
        VALUES.add(new Value(SAMPLES,                            TYPE_INT   ,1));
        VALUES.add(new Value(SCISSOR_BOX,                        TYPE_INT   ,4));
        VALUES.add(new Value(SHADER_COMPILER,                    TYPE_1_BIT ,1));
        VALUES.add(new Value(SHADER_STORAGE_BUFFER_BINDING,      TYPE_INT   ,1));
        VALUES.add(new Value(SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT,TYPE_INT   ,1));
//        VALUES.add(new Value(SHADER_STORAGE_BUFFER_START,        TYPE_INT   ,1));
//        VALUES.add(new Value(SHADER_STORAGE_BUFFER_SIZE,         TYPE_INT   ,1));
        VALUES.add(new Value(SMOOTH_LINE_WIDTH_RANGE,            TYPE_FLOAT ,2));
        VALUES.add(new Value(SMOOTH_LINE_WIDTH_GRANULARITY,      TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_BACK_FAIL,                  TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_BACK_FUNC,                  TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_BACK_PASS_DEPTH_FAIL,       TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_BACK_PASS_DEPTH_PASS,       TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_BACK_REF,                   TYPE_INT   ,1));  
        VALUES.add(new Value(STENCIL_BACK_VALUE_MASK,            TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_BACK_WRITEMASK,             TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_CLEAR_VALUE,                TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_FAIL,                       TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_FUNC,                       TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_PASS_DEPTH_FAIL,            TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_PASS_DEPTH_PASS,            TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_REF,                        TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_VALUE_MASK,                 TYPE_INT   ,1));
        VALUES.add(new Value(STENCIL_WRITEMASK,                  TYPE_INT   ,1));
        VALUES.add(new Value(STEREO,                             TYPE_1_BIT ,1));
        VALUES.add(new Value(SUBPIXEL_BITS,                      TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_1D,                 TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_1D_ARRAY,           TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D,                 TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D_ARRAY,           TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D_MULTISAMPLE,     TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY,TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_3D,                 TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_BUFFER,             TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_CUBE_MAP,           TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BINDING_RECTANGLE,          TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_COMPRESSION_HINT,           TYPE_INT   ,1));
        VALUES.add(new Value(TEXTURE_BUFFER_OFFSET_ALIGNMENT,    TYPE_INT   ,1));
        VALUES.add(new Value(TIMESTAMP,                          TYPE_LONG  ,1));
        VALUES.add(new Value(TRANSFORM_FEEDBACK_BUFFER_BINDING,  TYPE_INT   ,1));
//        VALUES.add(new Value(TRANSFORM_FEEDBACK_BUFFER_START,    TYPE_INT   ,1));
//        VALUES.add(new Value(TRANSFORM_FEEDBACK_BUFFER_SIZE,     TYPE_INT   ,1));
        VALUES.add(new Value(UNIFORM_BUFFER_BINDING,             TYPE_INT   ,1));
        VALUES.add(new Value(UNIFORM_BUFFER_OFFSET_ALIGNMENT,    TYPE_INT   ,1));
//        VALUES.add(new Value(UNIFORM_BUFFER_SIZE,                TYPE_INT   ,1));
//        VALUES.add(new Value(UNIFORM_BUFFER_START,               TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_ALIGNMENT,                   TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_IMAGE_HEIGHT,                TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_LSB_FIRST,                   TYPE_1_BIT ,1));
        VALUES.add(new Value(UNPACK_ROW_LENGTH,                  TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_SKIP_IMAGES,                 TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_SKIP_PIXELS,                 TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_SKIP_ROWS,                   TYPE_INT   ,1));
        VALUES.add(new Value(UNPACK_SWAP_BYTES,                  TYPE_1_BIT ,1));
        VALUES.add(new Value(VERTEX_ARRAY_BINDING,               TYPE_INT   ,1));
//        VALUES.add(new Value(VERTEX_BINDING_DIVISOR,             TYPE_INT   ,1));
//        VALUES.add(new Value(VERTEX_BINDING_OFFSET,              TYPE_INT   ,1));
//        VALUES.add(new Value(VERTEX_BINDING_STRIDE,              TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_ATTRIB_RELATIVE_OFFSET,  TYPE_INT   ,1));
        VALUES.add(new Value(MAX_VERTEX_ATTRIB_BINDINGS,         TYPE_INT   ,1));
        VALUES.add(new Value(VIEWPORT,                           TYPE_INT   ,1));
        VALUES.add(new Value(VIEWPORT_BOUNDS_RANGE,              TYPE_INT   ,2));
        VALUES.add(new Value(VIEWPORT_INDEX_PROVOKING_VERTEX,    TYPE_INT   ,1));
        VALUES.add(new Value(VIEWPORT_SUBPIXEL_BITS,             TYPE_INT   ,1));
        VALUES.add(new Value(MAX_ELEMENT_INDEX,                  TYPE_INT   ,1));

        //enabling states
        VALUES.add(new Value(GLC.GETSET.State.BLEND,                TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.COLOR_LOGIC_OP,       TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.CULL_FACE,            TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.DEPTH_TEST,           TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.DITHER,               TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.LINE_SMOOTH,          TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.PROGRAM_POINT_SIZE,   TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_OFFSET_FILL,  TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_OFFSET_LINE,  TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_OFFSET_POINT, TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.POLYGON_SMOOTH,       TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.SCISSOR_TEST,         TYPE_1_BIT ,1));
        VALUES.add(new Value(GLC.GETSET.State.STENCIL_TEST,         TYPE_1_BIT ,1));

    }
    
    private static final ByteBuffer bb = ByteBuffer.allocate(1);
    private static final IntBuffer ib = IntBuffer.allocate(1);
    private static final LongBuffer lb = LongBuffer.allocate(1);
    private static final FloatBuffer fb = FloatBuffer.allocate(1);
    private static final DoubleBuffer db = DoubleBuffer.allocate(1);
    
    //information values
    private final Dictionary INFO_VALUES = new HashDictionary();
    private final Dictionary SETTED_VALUES = new HashDictionary();
    
    //configurable values
    
    public GLState() {
    }
   
    public Object get(int code){
        final Iterator ite = INFO_VALUES.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Value v = (Value) pair.getValue1();
            if(v.code==code){
                return pair.getValue2();
            }
        }
        return null;
    }
    
    public void save(GL gl){
        //TODO
        INFO_VALUES.removeAll();
        SETTED_VALUES.removeAll();
        for(int i=0,n=VALUES.getSize();i<n;i++){
            final Value v = (Value) VALUES.get(i);
            Object value = null;
            final int nbVal = v.getNbVal(gl);
            if(v.type==RawModel.TYPE_1_BIT){
                value = (nbVal==1) ? getBoolean(gl, v.code) : getBooleans(gl, v.code, nbVal);
            }else if(v.type==RawModel.TYPE_INT){
                value = (nbVal==1) ? getInteger(gl, v.code) : getIntegers(gl, v.code, nbVal);
            }else if(v.type==RawModel.TYPE_LONG){
                value = (nbVal==1) ? getLong(gl, v.code) : getLongs(gl, v.code, nbVal);
            }else if(v.type==RawModel.TYPE_FLOAT){
                value = (nbVal==1) ? getFloat(gl, v.code) : getFloats(gl, v.code, nbVal);
            }else if(v.type==RawModel.TYPE_DOUBLE){
                value = (nbVal==1) ? getDouble(gl, v.code) : getDoubles(gl, v.code, nbVal);
            }
            INFO_VALUES.add(v,value);
        }
    }
    
    public void restore(GL gl){
        //TODO
    }
    
    public static boolean getBoolean(GL gl, int code){
        synchronized(bb){
            gl.asGL1().glGetBooleanv(code, bb);
            GLUtilities.checkGLErrorsFail(gl);
            return bb.get(0)!=0;
        }
    }
    
    public static boolean[] getBooleans(GL gl, int code, int nb){
        final boolean[] vals = new boolean[nb];
        for(int i=0;i<nb;i++) vals[i] = getBooleanI(gl, code, i);
        return vals;
    }
    
    public static boolean getBooleanI(GL gl, int code, int index){
        synchronized(bb){
            gl.asGL2ES3().glGetBooleani_v(code, index, bb);
            GLUtilities.checkGLErrorsFail(gl);
            return bb.get(0)!=0;
        }
    }
    
    public static int getInteger(GL gl, int code){
        synchronized(ib){
            gl.asGL1().glGetIntegerv(code, ib);
            GLUtilities.checkGLErrorsFail(gl);
            return ib.get(0);
        }
    }
    
    public static int[] getIntegers(GL gl, int code, int nb){
        final int[] array = new int[nb];
        gl.asGL1().glGetIntegerv(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }
    
    public static int getIntegerI(GL gl, int code, int index){
        synchronized(ib){
            gl.asGL2ES3().glGetIntegeri_v(code, index, ib);
            GLUtilities.checkGLErrorsFail(gl);
            return ib.get(0);
        }
    }
    
    public static long getLong(GL gl, int code){
        synchronized(lb){
            gl.asGL3().glGetInteger64v(code, lb);
            GLUtilities.checkGLErrorsFail(gl);
            return lb.get(0);
        }
    }
    
    public static long[] getLongs(GL gl, int code, int nb){
        final long[] array = new long[nb];
        gl.asGL3().glGetInteger64v(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }
    
    public static long getLongI(GL gl, int code, int index){
        synchronized(lb){
            gl.asGL3().glGetInteger64i_v(code, index, lb);
            GLUtilities.checkGLErrorsFail(gl);
            return lb.get(0);
        }
    }
    
    public static float getFloat(GL gl, int code){
        synchronized(fb){
            gl.asGL1().glGetFloatv(code, fb);
            GLUtilities.checkGLErrorsFail(gl);
            return fb.get(0);
        }
    }
    
    public static float[] getFloats(GL gl, int code, int nb){
        final float[] array = new float[nb];
        gl.asGL1().glGetFloatv(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }
    
    public static float getFloatI(GL gl, int code, int index){
        synchronized(fb){
            gl.asGL4().glGetFloati_v(code, index, fb);
            GLUtilities.checkGLErrorsFail(gl);
            return fb.get(0);
        }
    }
    
    public static double getDouble(GL gl, int code){        
        synchronized(db){
            gl.asGL1().glGetDoublev(code, db);
            GLUtilities.checkGLErrorsFail(gl);
            return db.get(0);
        }
    }
    
    public static double[] getDoubles(GL gl, int code, int nb){
        final double[] array = new double[nb];
        gl.asGL1().glGetDoublev(code, array);
        GLUtilities.checkGLErrorsFail(gl);
        return array;
    }
    
    public static double getDoubleI(GL gl, int code, int index){
        synchronized(db){
            gl.asGL4().glGetDoublei_v(code, index, db);
            GLUtilities.checkGLErrorsFail(gl);
            return db.get(0);
        }
    }
    

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        final Iterator ite = INFO_VALUES.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            cb.append(pair.getValue1().toString());
            cb.append(" = ");
            final Object v = pair.getValue2();
            if(v!=null && v.getClass().isArray()){
                cb.append(Arrays.toChars(v));
            }else{
                cb.append(v.toString());
            }
            cb.append('\n');
        }
        return cb.toChars();
    }

    public static int getCurrentProgramId(GL gl){
        return getInteger(gl,GLC.GETSET.CURRENT_PROGRAM);
    }

    /**
     * Get GL viewport.
     * 
     * @param gl
     * @return int[]{x,y,width,height}
     */
    public static int[] getViewPort(GL gl){
        return getIntegers(gl, GLC.GETSET.VIEWPORT, 4);
    }
    
}
