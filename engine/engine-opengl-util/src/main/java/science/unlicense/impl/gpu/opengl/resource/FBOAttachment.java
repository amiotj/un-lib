

package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.CObject;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLC.FBO.Attachment;

/**
 *
 * @author Johann Sorel
 */
public class FBOAttachment extends CObject{
    private final int attachment;
    private final int textarget;
    private final Texture texture;
    private final int level;
    private final int layer;

    public FBOAttachment(int attachment, Texture texture) {
        this(attachment, texture.getTextureType(), texture, 0, 0);
    }

    public FBOAttachment(int attachment, Texture texture, int level) {
        this(attachment, texture.getTextureType(), texture, level, 0);
    }

    public FBOAttachment(int attachment, Texture texture, int level, int layer) {
        this(attachment, texture.getTextureType(), texture, level, layer);
    }

    public FBOAttachment(int attachment, int textarget, Texture texture, int level, int layer) {
        this.attachment = attachment;
        this.textarget = textarget;
        this.texture = texture;
        this.level = level;
        this.layer = layer;
    }

    public int getAttachment() {
        return attachment;
    }

    public int getTextarget() {
        return textarget;
    }

    public Texture getTexture() {
        return texture;
    }

    public int getLayer() {
        return layer;
    }

    public int getLevel() {
        return level;
    }
    
    public FBOAttachment createBlitAttachment(){
        final Integer type = getAttachment();
        final Texture tex = getTexture();

        if(tex instanceof Texture2DMS){
            final Texture2DMS tex2dms = (Texture2DMS) tex;
            final int width = tex2dms.getWidth();
            final int height = tex2dms.getHeight();
            final TextureModel exp = tex2dms.info;
            //create equivalent in 2D
            final TextureModel tm = new TextureModel(GLC.GL_TEXTURE_2D, exp.getInternalFormat(), exp.getFormat(), exp.getType());
            for(int i=0,n=exp.getParameterSize();i<n;i++){
                final int[] p = exp.getParameter(i);
                tm.setParameter(p[0], p[1]);
            }
            final Texture2D tex2d = new Texture2D(width, height, tm);
            return new FBOAttachment(type, tex2d);
        }else{
            //creating a blit attachement from a texture which is not multisampled,
            // strange but could be used for copy operations
            final Texture2D tex2dOrig = (Texture2D) tex;
            final int width = tex2dOrig.getWidth();
            final int height = tex2dOrig.getHeight();
            final TextureModel exp = tex2dOrig.info;
            //create equivalent in 2D
            final TextureModel tm = new TextureModel(GLC.GL_TEXTURE_2D, exp.getInternalFormat(), exp.getFormat(), exp.getType());
            for(int i=0,n=exp.getParameterSize();i<n;i++){
                final int[] p = exp.getParameter(i);
                tm.setParameter(p[0], p[1]);
            }
            final Texture2D tex2d = new Texture2D(width, height, tm);
            return new FBOAttachment(type, tex2d);
        }
    }

    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append("Fbo attachement, ");
        switch(attachment){
            case Attachment.COLOR_0 : cb.append("Color 0"); break;
            case Attachment.COLOR_1 : cb.append("Color 1"); break;
            case Attachment.COLOR_2 : cb.append("Color 2"); break;
            case Attachment.COLOR_3 : cb.append("Color 3"); break;
            case Attachment.COLOR_4 : cb.append("Color 4"); break;
            case Attachment.COLOR_5 : cb.append("Color 5"); break;
            case Attachment.COLOR_6 : cb.append("Color 6"); break;
            case Attachment.COLOR_7 : cb.append("Color 7"); break;
            case Attachment.COLOR_8 : cb.append("Color 8"); break;
            case Attachment.COLOR_9 : cb.append("Color 9"); break;
            case Attachment.COLOR_10 : cb.append("Color 10"); break;
            case Attachment.COLOR_11 : cb.append("Color 11"); break;
            case Attachment.COLOR_12 : cb.append("Color 12"); break;
            case Attachment.COLOR_13 : cb.append("Color 13"); break;
            case Attachment.COLOR_14 : cb.append("Color 14"); break;
            case Attachment.COLOR_15 : cb.append("Color 15"); break;
            case Attachment.DEPTH : cb.append("Depth"); break;
            case Attachment.DEPTH_STENCIL : cb.append("Depth+Stencil"); break;
            case Attachment.STENCIL : cb.append("Stencil"); break;
        }
        cb.append(texture.toString());
        return cb.toChars();
    }
    
}
