
package science.unlicense.impl.gpu.opengl.shader;

import science.unlicense.impl.gpu.opengl.resource.ResourceException;

/**
 * Shader related exception.
 *
 * @author Johann Sorel
 */
public class ShaderException extends ResourceException {

    public ShaderException() {
    }
    public ShaderException(String message) {
        super(message);
    }

    public ShaderException(Throwable cause) {
        super(cause);
    }

    public ShaderException(String message, Throwable cause) {
        super(message, cause);
    }

}
