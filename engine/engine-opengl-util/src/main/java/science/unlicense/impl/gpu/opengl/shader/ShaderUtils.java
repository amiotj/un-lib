
package science.unlicense.impl.gpu.opengl.shader;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.collection.primitive.ByteSequence;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.gpu.opengl.GL2ES3;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.code.glsl.GLSLConstants;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import static science.unlicense.impl.gpu.opengl.shader.Uniform.*;

/**
 *
 * @author Johann Sorel
 */
public final class ShaderUtils {

    private static final Dictionary MAPPING = new HashDictionary();
    static {
        MAPPING.add(GLSLConstants.KW_BOOL,            TYPE_BOOL);
        MAPPING.add(GLSLConstants.KW_INT,             TYPE_INT);
        MAPPING.add(GLSLConstants.KW_FLOAT,           TYPE_FLOAT);
        MAPPING.add(GLSLConstants.KW_DOUBLE,          TYPE_DOUBLE);
        MAPPING.add(GLSLConstants.KW_VEC2,            TYPE_VEC2);
        MAPPING.add(GLSLConstants.KW_VEC3,            TYPE_VEC3);
        MAPPING.add(GLSLConstants.KW_VEC4,            TYPE_VEC4);
        MAPPING.add(GLSLConstants.KW_MAT3,            TYPE_MAT3);
        MAPPING.add(GLSLConstants.KW_MAT4,            TYPE_MAT4);
        MAPPING.add(GLSLConstants.KW_SAMPLER2D,       TYPE_SAMPLER2D);
        MAPPING.add(GLSLConstants.KW_ISAMPLER2D,      TYPE_ISAMPLER2D);
        MAPPING.add(GLSLConstants.KW_USAMPLER2D,      TYPE_USAMPLER2D);
        MAPPING.add(GLSLConstants.KW_SAMPLER2DMS,     TYPE_SAMPLER2DMS);
        MAPPING.add(GLSLConstants.KW_SAMPLERCUBE,     TYPE_SAMPLERCUBE);
        MAPPING.add(GLSLConstants.KW_ISAMPLERBUFFER,  TYPE_ISAMPLERBUFFER);
        MAPPING.add(GLSLConstants.KW_SAMPLERBUFFER,   TYPE_SAMPLERBUFFER);
    }
    
    private ShaderUtils(){}

    /**
     * OpenGL constraints : GL2ES2
     * 
     * @param gl
     * @param shaderType : GLC.GLSL.SHADER.X
     * @param shaderCode
     * @return
     */
    public static int loadShader(final GL gl, final int shaderType, final Chars shaderCode) throws ShaderException{
        final GL2ES2 gl2 = gl.asGL2ES2();
        final int shaderId = gl2.glCreateShader(shaderType);

//          TODO optimize shader
//        try{
//            final GLSLReader reader = new GLSLReader();
//            reader.setInput(shaderCode.toBytes(CharEncodings.UTF_8));
//            final SyntaxNode node = reader.read();
//        }catch(Exception ex){
//            throw new ShaderException(ex.getMessage(),ex);
//        }
        
        // compile the shader
        gl2.glShaderSource(shaderId, new CharArray[]{shaderCode});
        gl2.glCompileShader(shaderId);

        // Check for errors
        final int[] Result = new int[1];
        final int[] InfoLogLength = new int[1];
        gl2.glGetShaderiv(shaderId, GL_COMPILE_STATUS, Result);
        gl2.glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, InfoLogLength);
        final java.nio.ByteBuffer shaderErrorMessage = java.nio.ByteBuffer.allocate(InfoLogLength[0]);
        gl2.glGetShaderInfoLog(shaderId, null, shaderErrorMessage);
        shaderErrorMessage.rewind();
        final String error = new String(shaderErrorMessage.array());
        if(error.length()> 1){
            throw new ShaderException(error);
        }
        
        return shaderId;
    }

    public static int loadShaders(
            GL gl, Chars vertexShaderCode, Chars tessControlShaderCode,
            Chars tessEvalShaderCode, Chars geometryShaderCode, Chars fragmentShaderCode) throws ShaderException{
        return loadShaders(gl, vertexShaderCode, tessControlShaderCode, 
                tessEvalShaderCode, geometryShaderCode, fragmentShaderCode,null,-1);
    }

    public static int loadShaders(
            GL gl, Chars vertexShaderCode, Chars tessControlShaderCode,
            Chars tessEvalShaderCode, Chars geometryShaderCode, Chars fragmentShaderCode,
            Chars[] trsFeedbackVars, int trsFeedBAckMode) throws ShaderException{
        final GL2ES2 gl2 = gl.asGL2ES2();
        // Create the shaders
        final int VertexShaderID = loadShader(gl2, GLC.GLSL.SHADER.VERTEX, vertexShaderCode);
        //optional tesselation shaders
        final int TessControlShaderID = (tessControlShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.TESS_CONTROL, tessControlShaderCode);
        final int TessEvalShaderID = (tessEvalShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.TESS_EVAL, tessEvalShaderCode);
        final int GeometryShaderID = (geometryShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.GEOMETRY, geometryShaderCode);
        final int FragmentShaderID = (fragmentShaderCode==null) ? -1 :
                loadShader(gl2, GLC.GLSL.SHADER.FRAGMENT, fragmentShaderCode);
        
        
        final int progId = loadShaderProgram(gl, VertexShaderID, TessControlShaderID, TessEvalShaderID, 
                GeometryShaderID, FragmentShaderID, trsFeedbackVars, trsFeedBAckMode);
        
        return progId;
    }
    
    public static int loadShaderProgram(GL gl, int vertexShaderGlid, int tessControlShaderGlid,
            int tessEvalShaderGlid, int geometryShaderGlid, int fragmentShaderGlid,
            Chars[] trsFeedbackVars, int trsFeedBAckMode)throws ShaderException{
        
        final GL2ES2 gl2 = gl.asGL2ES2();
        
        // Link the program
        final int programID = gl2.glCreateProgram();
                                        gl2.glAttachShader(programID, vertexShaderGlid);
        if(tessControlShaderGlid != -1) gl2.glAttachShader(programID, tessControlShaderGlid);
        if(tessEvalShaderGlid    != -1) gl2.glAttachShader(programID, tessEvalShaderGlid);
        if(geometryShaderGlid    != -1) gl2.glAttachShader(programID, geometryShaderGlid);
        if(fragmentShaderGlid    != -1) gl2.glAttachShader(programID, fragmentShaderGlid);
        
        //set transform feedback
        if(trsFeedbackVars!=null && trsFeedbackVars.length>0){
            final GL2ES3 gl2es3 = gl.asGL2ES3();
            gl2es3.glTransformFeedbackVaryings(programID, trsFeedbackVars, trsFeedBAckMode);
        }
        
        gl2.glLinkProgram(programID);

        // Check the program
        final int[] Result = new int[1];
        final int[] InfoLogLength = new int[1];
        gl2.glGetProgramiv(programID, GL_LINK_STATUS, Result);
        gl2.glGetProgramiv(programID, GL_INFO_LOG_LENGTH, InfoLogLength);
        final java.nio.ByteBuffer ProgramErrorMessage = java.nio.ByteBuffer.allocate(InfoLogLength[0]);
        gl2.glGetProgramInfoLog(programID, null, ProgramErrorMessage);
        final String error = new String(ProgramErrorMessage.array());
        if(error.length()> 1){
            throw new ShaderException(error);
        }

        return programID;
    }
    
    /**
     * Reads the given stream and return an array of Strings.
     *
     * @param stream input stream
     * @return String array
     * @throws IOException
     */
    public static Chars getCharsFromStream(final ByteInputStream stream) throws IOException {
        final ByteSequence buffer = new ByteSequence();

        try {
            int b = stream.read();
            while(b!=-1){
                buffer.put((byte)b);
                b = stream.read();
            }
        } finally {
            stream.close();
        }
        return new Chars(buffer.toArrayByte());
    }
            
    public static Integer GLSLtoGLType(Chars typeName){
        return (Integer)MAPPING.getValue(typeName);
    }
    
}
