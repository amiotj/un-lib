
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.CObjects;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.AbstractTupleBuffer;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Maths;
import science.unlicense.api.gpu.GLBuffer;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractTupleBufferResource extends AbstractTupleBuffer implements Resource{

    //store initialization stack trace, only when assertions are enabled
    private Throwable ex;
    protected CharArray name = Chars.EMPTY;
    private boolean forgetOnLoad = false;
    
    protected Buffer buffer;
    protected boolean dirty = true;
    protected int bytePerElement = 0;
    protected int gpuType;
    protected int size;
    //gpu id
    protected final int[] bufferId = new int[]{-1};

    public AbstractTupleBufferResource() {
        assert((ex=new Throwable())!=null);
    }

    @Override
    protected void updateModel(Extent.Long dimensions, int sampleType, int nbSample) {
        super.updateModel(dimensions, sampleType, nbSample);
        gpuType = GLUtilities.primitiveTypeToGlType(sampleType);
        bytePerElement = GLUtilities.byteSize(gpuType);
    }
    
    public void setBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLUtilities.primitiveTypeToGlType(buffer.getPrimitiveType()), tupleSize);
    }
    
    /**
     * Set VBO datas.
     * 
     * OpenGL constraints : GL2ES2
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setIntBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLC.TYPE.INT, tupleSize);
    }

    /**
     * Set VBO datas.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setFloatBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLC.TYPE.FLOAT, tupleSize);
    }

    /**
     * Set datas.
     *
     * @param buffer data buffer
     * @param gpuType gpu data type
     * @param tupleSize number of value for each tuple
     */
    public void setBuffer(Buffer buffer, int gpuType, int tupleSize) {
        this.buffer = buffer;
        this.size = (int) buffer.getPrimitiveCount();
        updateModel(dimensions, GLUtilities.glTypeToPrimitiveType(gpuType), tupleSize);
        this.dirty = true;
    }
    
    /**
     * {@inheritDoc }
     */
    public CharArray getName() {
        return name;
    }
    
    /**
     * {@inheritDoc }
     */
    public void setName(CharArray name) {
        CObjects.ensureNotNull(name, "name");
        this.name = name;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isForgetOnLoad() {
        return forgetOnLoad;
    }

    /**
     * {@inheritDoc }
     */
    public void setForgetOnLoad(boolean forget) {
        this.forgetOnLoad = forget;
    }
    
    /**
     * Get the number of bytes use for each value in the buffer.
     * This size is derivate from the gpu data type;
     *
     * @return number of byte per value. 0 is data buffer has not been set
     */
    public int getBytePerElement() {
        return bytePerElement;
    }
        
    /**
     * Get number of tuples.
     * size / tupleSize
     * 
     * @return number of tuples.
     */
    public int getTupleCount(){
        return (int) (size / nbSample);
    }
    
    /**
     * Get primitive buffer.
     * Can be null.
     *
     * @return Buffer or null if not loaded on system memory
     */
    public Buffer getPrimitiveBuffer() {
        return buffer;
    }

    /**
     * Get a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer optional buffer
     * @return tuple
     */
    public int[] getTupleInt(int index, int[] buffer){
        if(buffer==null) buffer = new int[nbSample];
        this.buffer.readInt(buffer, 0, Maths.min(nbSample,buffer.length), index*4*nbSample);
        return buffer;
    }
    
    /**
     * Get a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer optional buffer
     * @return tuple
     */
    public float[] getTupleFloat(int index, float[] buffer){
        if(buffer==null) buffer = new float[nbSample];
        this.buffer.readFloat(buffer, 0, Maths.min(nbSample,buffer.length), index*4*nbSample);
        return buffer;
    }
    
    /**
     * Set a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer tuple data
     */
    public void setTupleInt(int index, int[] buffer){
        this.buffer.writeInt(buffer, index*4*nbSample);
    }
    
     /**
     * Set a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer tuple data
     */
    public void setTupleFloat(int index, float[] buffer){
        this.buffer.writeFloat(buffer, index*4*nbSample);
    }
    
    /**
     * Indicate if the system data buffer values has been changed.
     *
     * @return true if datas has been changed compared to the last time
     *          they were loaded on the gpu.
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * Declare the buffer to be dirty.
     * Use when direct modification are made to the buffer.
     */
    public void notifyDirty(){
        dirty = true;
    }
    
    /**
     * {@inheritDoc }
     */
    public int getGpuID() {
        return bufferId[0];
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnSystemMemory() {
        return buffer != null;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnGpuMemory() {
        return bufferId[0] != -1;
    }
    
    /**
     * {@inheritDoc }
     */
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        if(buffer instanceof GLBuffer){
            ((GLBuffer)buffer).dispose();
        }
        buffer = null;
    }
    
    protected void finalize() throws Throwable {
        final int gpuId = getGpuID();
        if(gpuId>=0){
            Loggers.get().info(
                    new Chars("Unreleased GPU resource : "+getClass().getSimpleName()
                    +" with gpu-id="+gpuId));
            if(ex!=null){
                ex.printStackTrace();
            }else{
                Loggers.get().info(new Chars("Enable assertions to view initialization stack trace"));
            }
        }
        super.finalize();
    }
    
}
