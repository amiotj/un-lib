
package science.unlicense.impl.gpu.opengl;

/**
 *
 * @author Johann Sorel
 */
public class GLException extends Exception{

    public GLException() {
    }

    public GLException(String message) {
        super(message);
    }

    public GLException(Throwable cause) {
        super(cause);
    }

    public GLException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
