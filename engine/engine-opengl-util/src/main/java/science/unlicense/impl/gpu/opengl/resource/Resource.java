
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.gpu.opengl.GL;

/**
 * Defines a pack of data which can be shared in system or gpu memory.
 *
 * @author Johann Sorel
 */
public interface Resource {

    /**
     * Get resource name.
     * This is a convinient property to identify resources.
     * 
     * @return CharArra, not null
     */
    CharArray getName();

    /**
     * Set resource name.
     * This is a convinient property to identify resources.
     *
     * @param name, not null
     */
    void setName(CharArray name);

    /**
     * This flags indicate the engine may release the data on system memory
     * once they are loaded on the GPU.
     * Default is true
     * 
     * @return true if system data can be forget when after being loaded on GPU.
     */
    boolean isForgetOnLoad();
    
    /**
     * @see #isForgetOnLoad() 
     * @param forget 
     */
    void setForgetOnLoad(boolean forget);
    
    /**
     * Get id of the resource on the gpu.
     *
     * @return on gou id, generaly negative if not loaded
     */
    int getGpuID();

    /**
     * Indicate if the resource is loaded on the system memory.
     *
     * @return true if on system memory
     */
    boolean isOnSystemMemory();

    /**
     * Indicate if the resource is loaded on the GPU memory.
     *
     * @return true if on GPU memory
     */
    boolean isOnGpuMemory();

    /**
     * Load resource from GPU to system memory.
     *
     * @param gl
     * @throws ResourceException
     */
    void loadOnSystemMemory(GL gl) throws ResourceException;

    /**
     * Load resource from system to GPU memory.
     *
     * @param gl
     * @throws ResourceException
     */
    void loadOnGpuMemory(GL gl) throws ResourceException;

    /**
     * Unload resource from system memory.
     *
     * @param gl
     * @throws ResourceException
     */
    void unloadFromSystemMemory(GL gl) throws ResourceException;

    /**
     * Unload resource from GPU memory.
     *
     * @param gl
     * @throws ResourceException
     */
    void unloadFromGpuMemory(GL gl) throws ResourceException;

}
