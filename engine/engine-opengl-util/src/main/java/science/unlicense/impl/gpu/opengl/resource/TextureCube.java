
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.image.Image;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 * Resource to manipulate a texture cube map object.
 *
 * @author Johann Sorel
 */
public class TextureCube extends AbstractResource {

    private Image imagePositiveX;
    private Image imageNegativeX;
    private Image imagePositiveY;
    private Image imageNegativeY;
    private Image imagePositiveZ;
    private Image imageNegativeZ;

    private int texId = -1;
    private boolean dirty = true;

    public TextureCube() {
    }

    public void setImagePositiveX(Image imagePositiveX) {
        this.imagePositiveX = imagePositiveX;
        dirty = true;
    }

    public Image getImagePositiveX() {
        return imagePositiveX;
    }

    public void setImageNegativeX(Image imageNegativeX) {
        this.imageNegativeX = imageNegativeX;
        dirty = true;
    }

    public Image getImageNegativeX() {
        return imageNegativeX;
    }

    public void setImagePositiveY(Image imagePositiveY) {
        this.imagePositiveY = imagePositiveY;
        dirty = true;
    }

    public Image getImagePositiveY() {
        return imagePositiveY;
    }

    public void setImageNegativeY(Image imageNegativeY) {
        this.imageNegativeY = imageNegativeY;
        dirty = true;
    }

    public Image getImageNegativeY() {
        return imageNegativeY;
    }

    public void setImagePositiveZ(Image imagePositiveZ) {
        this.imagePositiveZ = imagePositiveZ;
        dirty = true;
    }

    public Image getImagePositiveZ() {
        return imagePositiveZ;
    }

    public void setImageNegativeZ(Image imageNegativeZ) {
        this.imageNegativeZ = imageNegativeZ;
        dirty = true;
    }

    public Image getImageNegativeZ() {
        return imageNegativeZ;
    }

    /**
     * Indicate if the current texture is up to date with the system image.
     * @return true if texture needs to be reloaded.
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    public int getGpuID() {
        return texId;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnSystemMemory() {
        return imagePositiveX != null;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnGpuMemory() {
        return texId >= 0;
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) {
        throw new UnimplementedException("Not supported.");
    }

    /**
     * OpenGL constraints : GL2ES2
     * 
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) {
        if(texId>=0 && !dirty) return;
        dirty = false;

        int[] temp = new int[1];
        gl.asGL1().glGenTextures(temp);
        texId = temp[0];
        gl.asGL1().glBindTexture(GL_TEXTURE_CUBE_MAP, texId);
        gl.asGL1().glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        gl.asGL1().glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        gl.asGL1().glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        gl.asGL1().glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        gl.asGL1().glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        TextureUtils.loadTexture(gl, imagePositiveX, GL_TEXTURE_CUBE_MAP_POSITIVE_X);
        TextureUtils.loadTexture(gl, imageNegativeX, GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
        TextureUtils.loadTexture(gl, imagePositiveY, GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
        TextureUtils.loadTexture(gl, imageNegativeY, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
        TextureUtils.loadTexture(gl, imagePositiveZ, GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
        TextureUtils.loadTexture(gl, imageNegativeZ, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

        if(isForgetOnLoad()){
            imagePositiveX = null;
            imageNegativeX = null;
            imagePositiveY = null;
            imageNegativeY = null;
            imagePositiveZ = null;
            imageNegativeZ = null;
        }
        
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromSystemMemory(GL gl) {
        imagePositiveX = null;
        imageNegativeX = null;
        imagePositiveY = null;
        imageNegativeY = null;
        imagePositiveZ = null;
        imageNegativeZ = null;
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) {
        if(texId>=0){
            gl.asGL1().glDeleteTextures(new int[]{texId});
            texId = -1;
            GLUtilities.checkGLErrorsFail(gl);
        }
    }

    public void bind(GL gl){
        gl.asGL1().glBindTexture(GL_TEXTURE_CUBE_MAP, texId);
    }

}
