
package science.unlicense.impl.gpu.opengl.shader;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.AbstractResource;
import science.unlicense.impl.gpu.opengl.resource.ResourceException;

/**
 *
 * @author Johann Sorel
 */
public class ShaderProgram extends AbstractResource {

    //program vertex attributes and uniforms
    private static final Chars UNIFORM = new Chars("uniform");
    private final Dictionary uniforms = new HashDictionary();
    private final Dictionary layouts = new HashDictionary();
    
    private int glid = -1;
    private Chars[] trsFeedBackVars;
    private int trsFeedBackMode;
    
    // vertex, tess control, tess eval, geometry, pixel shaders
    private final Shader[] shaders = new Shader[5];

    public ShaderProgram() {
    }
    
    public ShaderProgram(Chars[] shaderTexts, Chars[] trsFeedBackVars, int trsFeedBackMode){
        for(int i=0;i<shaderTexts.length;i++){
            if(shaderTexts[i]!=null && !shaderTexts[i].isEmpty()){
                setShader(new Shader(shaderTexts[i], i), i);
            }
        }
        setTransferFeedback(trsFeedBackVars, trsFeedBackMode);     
    }
    
    /**
     * 
     * @param type one of Shader.SHADER_X
     * @return 
     */
    public Shader getShader(int type){
        return shaders[type];
    }
    
    public void setShader(Shader shader, int type){
        shaders[type] = shader;
    }
    
    public void setTransferFeedback(Chars[] trsFeedBackVars, int trsFeedBackMode){
        this.trsFeedBackVars = trsFeedBackVars;
        this.trsFeedBackMode = trsFeedBackMode;
    }
    
    /**
     * Indicate if this program uses the tesselation phases.
     * @return true if tesselation is present.
     */
    public boolean usesTesselationShader(){
        return shaders[1] != null;
    }

    /**
     * Indicate if this program uses the geometry phase.
     * @return true if geometry is present.
     */
    public boolean usesGeometryShader(){
        return shaders[3] != null;
    }
    
    public Uniform getUniform(Chars name){
        return (Uniform)uniforms.getValue(name);
    }

    /**
     * OpenGL constraints : GL2ES2
     * 
     * @param name
     * @param gl
     * @param type
     * @return 
     */
    public Uniform getUniform(Chars name, GL gl, int type){
        Uniform uni = (Uniform)uniforms.getValue(name);
        if(uni==null){
            //search the shader program
            final int uniId = gl.asGL2ES2().glGetUniformLocation(glid, name);
            uni = new Uniform(glid,uniId, type);
            GLUtilities.checkGLErrorsFail(gl);
            uniforms.add(name, uni);
        }
        return uni;
    }
    
    /**
     * OpenGL constraints : GL2ES2
     * 
     * @param name
     * @param gl
     * @return 
     */
    public VertexAttribute getVertexAttribute(Chars name, GL gl, int slotSize) {
        VertexAttribute layout = (VertexAttribute)layouts.getValue(name);
        if(layout==null){
            final int idx = gl.asGL2ES2().glGetAttribLocation(glid, name);
            //TODO get type back
            layout = new VertexAttribute(glid, idx, -1,slotSize);
            GLUtilities.checkGLErrorsFail(gl);
            layouts.add(name, layout);
        }
        return layout;
    }
    
    /**
     * Get shader program id.
     *
     * @return shader program id
     */
    public int getGpuID() {
        return glid;
    }

    /**
     * Activate the program.
     * 
     * OpenGL constraints : GL2ES2
     * 
     * @param gl
     */
    public void enable(GL gl){
        if(glid<0) throw new RuntimeException("Program is not loaded.");
        gl.asGL2ES2().glUseProgram(glid);
    }

    /**
     * Desactivate the program.
     * 
     * OpenGL constraints : GL2ES2
     * 
     * @param gl
     */
    public void disable(GL gl){
        gl.asGL2ES2().glUseProgram(0);
    }
    
    public boolean isOnSystemMemory() {
        for(int i=0;i<shaders.length;i++){
            if(shaders[i]!=null && !shaders[i].isOnSystemMemory()){
                return false;
            }
        }
        return true;
    }

    public boolean isOnGpuMemory() {
        return getGpuID()>=0;
    }

    public void loadOnSystemMemory(GL gl) throws ResourceException {
        for(int i=0;i<shaders.length;i++){
            if(shaders[i]!=null){
                shaders[i].loadOnSystemMemory(gl);
            }
        }
    }

    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(isOnGpuMemory()) return;
        
        for(int i=0;i<shaders.length;i++){
            if(shaders[i]!=null){
                shaders[i].loadOnGpuMemory(gl);
            }
        }

        glid = ShaderUtils.loadShaderProgram(gl, 
                shaders[0]==null ? -1 : shaders[0].getGpuID(), 
                shaders[1]==null ? -1 : shaders[1].getGpuID(), 
                shaders[2]==null ? -1 : shaders[2].getGpuID(), 
                shaders[3]==null ? -1 : shaders[3].getGpuID(), 
                shaders[4]==null ? -1 : shaders[4].getGpuID(), 
                trsFeedBackVars, trsFeedBackMode);
        
        loadAssociates(gl);
        
        GLUtilities.checkGLErrorsFail(gl);
    }

    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        for(int i=0;i<shaders.length;i++){
            if(shaders[i]!=null){
                shaders[i].unloadFromSystemMemory(gl);
            }
        }
    }

    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        for(int i=0;i<shaders.length;i++){
            if(shaders[i]!=null){
                shaders[i].unloadFromGpuMemory(gl);
            }
        }
        
        if(glid!=-1){
            gl.asGL2ES2().glDeleteProgram(glid);
            glid = -1;
            uniforms.removeAll();
            layouts.removeAll();
        }
    }
    
    private void loadAssociates(GL gl){
        uniforms.removeAll();
        layouts.removeAll();
        
        //find basic uniforms names
        //this might not be all names because of struct types
        findUniforms(shaders[0]);
        findUniforms(shaders[1]);
        findUniforms(shaders[2]);
        findUniforms(shaders[3]);
        findUniforms(shaders[4]);

        //get uniform ids
        final Iterator ite = uniforms.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Chars name = (Chars) pair.getValue1();
            final Chars type = (Chars) pair.getValue2();
            final Integer itype = ShaderUtils.GLSLtoGLType(type);
            if(itype==null){
                //likely a structure type
                uniforms.add(name, null);
            }else{
                final int nuid = gl.asGL2ES2().glGetUniformLocation(glid, name);
                uniforms.add(name, new Uniform(glid,nuid, itype));
            }
        }    
    }
    
    private void findUniforms(Shader shader){
        if(shader==null) return;
        final Chars code = shader.getCode();
        if(code==null) return;
        
        final Chars[] lines = code.split('\n');
        for(int i=0;i<lines.length;i++){
            if(lines[i].startsWith(UNIFORM)){
                final Chars[] words = lines[i].split(' ');
                Chars name = words[2].replaceAll(';', ' ').trim();
                //store name and type
                uniforms.add(name, words[1].trim());
            }
        }
    }
    
}