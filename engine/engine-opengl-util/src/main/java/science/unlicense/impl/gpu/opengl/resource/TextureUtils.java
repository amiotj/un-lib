
package science.unlicense.impl.gpu.opengl.resource;

import java.nio.ByteBuffer;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DataCursor;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.BandTrsColorModel;
import science.unlicense.api.image.color.DirectColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.image.color.IndexedColorModel;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.image.color.PixelIterator;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.image.sample.InterleavedRawModel;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;
import science.unlicense.impl.image.craw.dxt.DXTRawModel;
import science.unlicense.api.gpu.GLBuffer;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.gpu.opengl.GL3;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.impl.gpu.opengl.GLC;
import static science.unlicense.impl.gpu.opengl.GLC.Texture.Type.*;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 *
 * @author Johann Sorel
 */
public final class TextureUtils {

    private static final int[] ORDER_RGB  = new int[]{0,1,2,-1};
    private static final int[] ORDER_RGBA = new int[]{0,1,2,3};
    private static final int[] ORDER_BGR  = new int[]{2,1,0,-1};
    private static final int[] ORDER_BGRA = new int[]{2,1,0,3};

    private TextureUtils() {}

    public static int loadTexture(GL gl, Image image, TextureModel info){

        final int[] textureId = new int[1];
        gl.asGL1().glGenTextures(textureId);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, textureId[0]);

        
        final Buffer dataBuffer = image.getDataBuffer();
        final java.nio.Buffer imageData = GLUtilities.asGLBuffer(dataBuffer);
        imageData.position(0);
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        //TODO calculate unpack alignment value correctly
        // http://stackoverflow.com/questions/15052463/given-the-pitch-of-an-image-how-to-calculate-the-gl-unpack-alignment
        gl.asGL1().glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        if(info.isCompressed()){
            gl.asGL1().glCompressedTexImage2D(info.getTarget(), 0, info.getFormat(), 
                    width,height, 0, imageData);
            GLUtilities.checkGLErrorsFail(gl);
        }else{
            gl.asGL1().glTexImage2D(info.getTarget(), 0, info.getInternalFormat(), 
                    width,height, 0, info.getFormat(), info.getType(), imageData);
            GLUtilities.checkGLErrorsFail(gl);
        }
        
        for(int i=0,n=info.getParameterSize();i<n;i++){
            final int[] param = info.getParameter(i);
            gl.asGL1().glTexParameterf(GL_TEXTURE_2D, param[0], param[1]);
        }
        if(info.hasMipMap()){
            gl.asGL2ES2().glGenerateMipmap(GL_TEXTURE_2D);
        }
        
        return textureId[0];
    }

    /**
     * Loads a texture using glTexImage2D.
     * Does not set any parameters, does not bind the texture.
     *
     * It only check and fix the image pixel data properly.
     *
     * @param gltype first parameter of glTexImage2D
     */
    public static void loadTexture(final GL gl, final Image image, final int gltype){

        //GL image info
        java.nio.Buffer imageData = null;
        int imageInternalFormat = 0;
        int imageFormat = 0;
        boolean compressed = false;
        
        //UN image
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final RawModel rm = image.getRawModel();
        final ColorModel cm = image.getColorModel();

        if(rm instanceof DXTRawModel){
            compressed = true;
            final DXTRawModel dxtrm = (DXTRawModel) rm;
            final int dxtType = dxtrm.getDxtType();
            if(dxtType==DXTRawModel.TYPE_DXT1){
                imageFormat = GLC.Texture.CompressedFormat.DXT1;
            }else if(dxtType==DXTRawModel.TYPE_DXT2){
                throw new RuntimeException("DXT2 Not supported yet.");
            }else if(dxtType==DXTRawModel.TYPE_DXT3){
                imageFormat = GLC.Texture.CompressedFormat.DXT3;
            }else if(dxtType==DXTRawModel.TYPE_DXT4){
                throw new RuntimeException("DXT4 Not supported yet.");
            }else if(dxtType==DXTRawModel.TYPE_DXT5){
                imageFormat = GLC.Texture.CompressedFormat.DXT5;
            }else{
                throw new RuntimeException("Unexpected DXT image type : "+dxtType);
            }
            final Buffer dataBuffer = image.getDataBuffer();
            imageData = GLUtilities.asGLBuffer(dataBuffer);
            
        }else if(cm instanceof DirectColorModel){
            final DirectColorModel dcm = (DirectColorModel) cm;
            final int nbSample = rm.getSampleCount();
            final int dataType = rm.getPrimitiveType();
            final int[] mapping = dcm.getMapping();
            final Buffer dataBuffer = image.getDataBuffer();

            if(nbSample == 3
                    && dataType == RawModel.TYPE_UBYTE
                    && Arrays.equals(mapping,ORDER_RGB) ){
                //image is RGBA, we can reuse bit buffer directly
                imageInternalFormat = GL_COMPRESSED_RGB;
                imageFormat = GL_RGB;
                imageData = GLUtilities.asGLBuffer(dataBuffer);

            }else if(nbSample == 4
                    && dataType == RawModel.TYPE_UBYTE
                    && Arrays.equals(mapping,ORDER_RGBA)
                    && dcm.isAlphaPreMultiplied()){
                //image is RGBA, we can reuse bit buffer directly
                imageInternalFormat = GL_COMPRESSED_RGBA;
                imageFormat = GL_RGBA;
                imageData = GLUtilities.asGLBuffer(dataBuffer);

            }else if(nbSample == 3
                    && dataType == RawModel.TYPE_UBYTE
                    && Arrays.equals(mapping, ORDER_BGR)){
                //image is BGR, we can reuse bit buffer directly
                imageInternalFormat = GL_RGB;
                imageFormat = GL_BGR;
                imageData = GLUtilities.asGLBuffer(dataBuffer);

            }else if(nbSample == 4
                    && dataType == RawModel.TYPE_UBYTE
                    && Arrays.equals(mapping,ORDER_BGRA)
                    && dcm.isAlphaPreMultiplied()){
                //image is BGRA, we can reuse bit buffer directly
                imageInternalFormat = GL_RGBA;
                imageFormat = GL_BGRA;
                imageData = GLUtilities.asGLBuffer(dataBuffer);
            }
        }


        if(imageData == null){
            //format can not be supported directly in opengl.
            throw new RuntimeException("Image format not supported, use makeCompatible method to transform it.");
        }

        imageData.position(0);

        //TODO calculate unpack alignment value correctly
        // http://stackoverflow.com/questions/15052463/given-the-pitch-of-an-image-how-to-calculate-the-gl-unpack-alignment
        gl.asGL1().glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        if(!compressed){
            gl.asGL1().glTexImage2D(gltype, 0,imageInternalFormat, width,height, 0, imageFormat, GL_UNSIGNED_BYTE, imageData);
        }else{
            gl.asGL1().glCompressedTexImage2D(gltype, 0,imageFormat, width,height, 0, imageData);
        }
        GLUtilities.checkGLErrorsFail(gl);

        GLBuffer.dispose(imageData);
    }

    /**
     * Download a texture from graphics memory and return it as an image.
     * 
     * OpenGL constraints : GL3+
     * 
     * @param gl Opengl content
     * @param textureId texture id
     * @param textureType texture type
     * @param extent texture size
     * @param glFormat texture color type
     * @param glSampleType texture sample type
     * @return Image
     */
    public static Image downloadTexture(final GL gl, final int textureId,
            final Extent.Long extent, TextureModel model){
        //TODO we should be able to retrieve most texture information from GL
        final GL3 gl3 = gl.asGL3();
        
        final int target = model.getTarget();
        final int type = model.getType();
        final int format = model.getFormat();

        final int imageSampleType = GLUtilities.glTypeToPrimitiveType(type);
        final int sampleNbByte = Images.getBitsPerSample(imageSampleType) / 8;
        final int nbSample = glFormatToNbSample(format);

        final ByteBuffer buffer = ByteBuffer.allocateDirect((int) (extent.getL(0)*extent.getL(1)*sampleNbByte*nbSample));
        gl3.asGL2ES2().glBindTexture(target,textureId);
        gl3.asGL1().glGetTexImage(GL_TEXTURE_2D, 0, format, type, buffer);
        GLUtilities.checkGLErrorsFail(gl3);

        buffer.position(0);
        final science.unlicense.api.buffer.Buffer bank = new GLBuffer(buffer, imageSampleType, NumberEncoding.LITTLE_ENDIAN);

        final RawModel sm = new InterleavedRawModel(imageSampleType, nbSample);
        
        final ColorModel cm;
        if(imageSampleType==Primitive.TYPE_UBYTE){
            switch(format){
                case GLC.Texture.Format.BGR : cm = new DirectColorModel(sm, ORDER_BGR, false); break;
                case GLC.Texture.Format.BGRA : cm = new DirectColorModel(sm, ORDER_BGRA, true); break;
                case GLC.Texture.Format.RGB : cm = new DirectColorModel(sm, ORDER_RGB, false); break;
                case GLC.Texture.Format.RGBA : cm = new DirectColorModel(sm, ORDER_RGBA, true); break;
                default : //use first band
                    cm = new DirectColorModel(sm, new int[]{0,0,0,-1}, false);
            }
        }else{
            final int[] mapping = new int[4];
            Arrays.fill(mapping, -1);
            final float[] scale = new float[4];
            final float[] offset = new float[4];
            for(int i=0;i<nbSample;i++){
                mapping[i] = i;
                scale[i] = 0.5f;
                offset[i] = 0.5f;
            }
            cm = new BandTrsColorModel(sm, mapping, scale, offset, true);
        }

        return new DefaultImage(bank, extent, sm, cm);
    }

    public static int glFormatToNbSample(int glFormat){
        if(glFormat == GLC.Texture.Format.RED || glFormat == GLC.Texture.Format.RED_INTEGER){
            return 1;
        }else if(glFormat == GLC.Texture.Format.RG || glFormat == GLC.Texture.Format.RG_INTEGER){
            return 2;
        }else if(glFormat == GLC.Texture.Format.RGB || glFormat == GLC.Texture.Format.RGB_INTEGER
               || glFormat == GLC.Texture.Format.BGR || glFormat == GLC.Texture.Format.BGR_INTEGER){
            return 3;
        }else if(glFormat == GLC.Texture.Format.RGBA || glFormat == GLC.Texture.Format.RGBA_INTEGER
              || glFormat == GLC.Texture.Format.BGRA || glFormat == GLC.Texture.Format.BGRA_INTEGER){
            return 4;
        }else if(glFormat == GLC.Texture.Format.DEPTH_COMPONENT){
            return 1;
        }else if(glFormat == GLC.Texture.Format.DEPTH_STENCIL){
            return 4;
        }else{
            throw new RuntimeException("Unsupported type :" + glFormat);
        }
    }

    /**
     * Check if an image is opaque.
     * Does not contain any alpha component not 1.
     * 
     * @param image
     * @return true if image is opaque
     */
    public static boolean isOpaque(Image image){

        final ColorModel cm = image.getColorModel();
        if(cm instanceof IndexedColorModel){
            final IndexedColorModel icm = (IndexedColorModel) cm;
            final Color[] palette = icm.getPalette().palette;
            for(int i=0;i<palette.length;i++){
                if(palette[i].getAlpha() < 1f){
                    return false;
                }
            }
            return true;
        }else if(cm instanceof DirectColorModel){
            final DirectColorModel dcm = (DirectColorModel) cm;
            if(dcm.getMapping()[3] == -1){
                //no alpha band
                return true;
            }
        }

        //we have to check the full image
        final PixelIterator sm = image.getColorModel().asTupleBuffer(image).createIterator(null);
        for(Color c = sm.nextAsColor(); c!=null; c=sm.nextAsColor()){
            if(c.getAlpha() < 1f){
                    return false;
                }
        }
        
        return true;
    }
        
    /**
     * OpenGL constraints :
     * - GL3+ : texture 2D multisample
     * - GL2ES2+ : texture 3D
     * 
     * @param extent
     * @param gl
     * @param info 
     */
    public static void createTexture(Extent extent, GL gl, TextureModel info) {
        final int width = (int)extent.get(0);
        final int height = (int)extent.get(1);
        
        final int target = info.getTarget();
        final int type = info.getType();
        final int format = info.getFormat();
        final int internalFormat = info.getInternalFormat();
        
        if(target==GL_TEXTURE_2D){
            //create an empty byte buffer for datas
            //we could use GL_ARB_clear_texture but is it not always available
            final int imageSampleType = GLUtilities.glTypeToPrimitiveType(type);
            final int sampleNbByte = Images.getBitsPerSample(imageSampleType) / 8;
            final int nbSample = glFormatToNbSample(format);
            //add an extra line : because some graphics expect more or less %2 sizes
            //the extra line ensure there is enough spare bytes.
            // NOTE : find where in the spec we can detect this case
            final ByteBuffer buffer = ByteBuffer.allocate((width+1)*height*sampleNbByte*nbSample);
            buffer.position(0);
            gl.asGL1().glTexImage2D(
                    GL_TEXTURE_2D,                      //GLenum target
                    0,                                      //GLint level
                    internalFormat,                         //GLint internalFormat
                    width,                                  //GLsizei width
                    height,                                 //GLsizei height
                    0,                                      //GLint border
                    format,                                 //GLenum format
                    type,                                   //GLenum type
                    buffer);                                //const GLvoid * data
        }else if(target==GL_TEXTURE_2D_MULTISAMPLE){
            final GL3 gl3 = gl.asGL3();
            final int sampling = (int)extent.get(2);
            gl3.glTexImage2DMultisample(
                    target,                                 //GLenum target
                    sampling,                               //GLsizei samples
                    internalFormat,                         //GLint internalformat
                    width,                                  //GLsizei width
                    height,                                 //GLsizei height
                    false);                                 //GLboolean fixedsamplelocations
        }else if(target==GL_TEXTURE_3D){
            //create an empty byte buffer for datas
            //we could use GL_ARB_clear_texture but is it not always available
            final GL2ES2 gl2es2 = gl.asGL2ES2();
            final int imageSampleType = GLUtilities.glTypeToPrimitiveType(type);
            final int sampleNbByte = Images.getBitsPerSample(imageSampleType) / 8;
            final int nbSample = glFormatToNbSample(format);
            final int depth = (int)extent.get(2);
            final ByteBuffer buffer = ByteBuffer.allocate(width*height*depth*sampleNbByte*nbSample);
            gl2es2.asGL1().glTexImage3D(
                    target,                                 //GLenum target
                    0,                                      //GLint level, 
                    internalFormat,                         //GLint internalformat
                    width,                                  //GLsizei width
                    height,                                 //GLsizei height
                    depth,                                  //GLsizei depth
                    0,                                      //GLint border
                    format,                                 //GLenum format
                    type,                                   //GLenum type
                    buffer);                                //const GLvoid * data
        }else{
            throw new RuntimeException("Unexpected target type : "+target);
        }
        
        for(int i=0,n=info.getParameterSize();i<n;i++){
            final int[] param = info.getParameter(i);
            gl.asGL1().glTexParameterf(GL_TEXTURE_2D, param[0], param[1]);
        }
        
    }
        
    /**
     * All raw models and color models can not be used directly in opengl.
     * This method will transform the image to match opengl capabilities.
     * 
     * @param image
     * @param preserveColors
     * @return 
     */
    public static Image makeCompatible(Image image, boolean preserveColors){

        final RawModel rm = image.getRawModel();
        final ColorModel cm = image.getColorModel();

        if(rm instanceof DXTRawModel){
            //DXT compression, ok
            return image;
        }
        
        if(preserveColors && cm!=null){
            if(cm instanceof DirectColorModel && rm instanceof InterleavedRawModel){
                final DirectColorModel dcm = (DirectColorModel) cm;
                final int nbSample = rm.getSampleCount();
                final int dataType = rm.getPrimitiveType();
                final int[] mapping = dcm.getMapping();

                if(nbSample == 3
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping,ORDER_RGB) ){
                    //image is RGBA, we can reuse bit buffer directly
                    return image; 

                }else if(nbSample == 4
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping,ORDER_RGBA)
                        && dcm.isAlphaPreMultiplied()){
                    //image is RGBA, we can reuse bit buffer directly
                    return image; 

                }else if(nbSample == 3
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping, ORDER_BGR)){
                    //image is BGR, we can reuse bit buffer directly
                    return image; 

                }else if(nbSample == 4
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping,ORDER_BGRA)
                        && dcm.isAlphaPreMultiplied()){
                     //image is BGRA, we can reuse bit buffer directly
                    return image; 
                }
            }


            //format can not be supported directly in opengl.
            //convert it to a RGBA buffer.
            final int width = (int) image.getExtent().getL(0);
            final int height = (int) image.getExtent().getL(1);
            final Image rgba = Images.create(new Extent.Long(width, height), 
                    Images.IMAGE_TYPE_RGBA_PREMULTIPLIED, GLBufferFactory.INSTANCE);

            final PixelBuffer pb = cm.asTupleBuffer(image);
            final DataCursor imageData = rgba.getDataBuffer().cursor();
            final int[] coord = new int[2];
            final float[] rgbapre = new float[4];
            for (int y = 0; y <height; y++) {
                coord[1] = y;
                for (int x = 0; x < width; x++) {
                    coord[0] = x;
                    final Color color = pb.getColor(coord);
                    color.toRGBAPreMul(rgbapre, 0);
                    imageData.writeByte((byte) (rgbapre[0] * 255));
                    imageData.writeByte((byte) (rgbapre[1] * 255));
                    imageData.writeByte((byte) (rgbapre[2] * 255));
                    imageData.writeByte((byte) (rgbapre[3] * 255));
                }
            }

            return rgba;
        }else{
            //opengl support only interleaved images
            if(rm instanceof InterleavedRawModel){
                final int nbSample = rm.getSampleCount();
                if(nbSample>4) throw new InvalidArgumentException("Images with more then 4 samples are not supported by opengl");
                final int primitiveType = rm.getPrimitiveType();
                final int glType = GLUtilities.primitiveTypeToGlType(primitiveType);
                if(glType==0){
                    throw new InvalidArgumentException("Unsupported primitive type "+primitiveType);
                }
            }else{
                //TODO comvert image to interleaved
                throw new InvalidArgumentException("Only interleaved image model are supported");
            }
            
            return image;
        }
    }
    
    public static TextureModel toTextureInfo(Image image, boolean preserveColors){
        final int imageInternalFormat;
        final int imageFormat;
        final int imageType;
        
        final RawModel rm = image.getRawModel();
        final ColorModel cm = image.getColorModel();

        if(rm instanceof DXTRawModel){
            imageType = GLC.Texture.Type.UNSIGNED_BYTE;
            
            final DXTRawModel dxtrm = (DXTRawModel) rm;
            final int dxtType = dxtrm.getDxtType();
            if(dxtType==DXTRawModel.TYPE_DXT1){
                imageFormat = GLC.Texture.CompressedFormat.DXT1;
            }else if(dxtType==DXTRawModel.TYPE_DXT2){
                throw new RuntimeException("DXT2 Not supported yet.");
            }else if(dxtType==DXTRawModel.TYPE_DXT3){
                imageFormat = GLC.Texture.CompressedFormat.DXT3;
            }else if(dxtType==DXTRawModel.TYPE_DXT4){
                throw new RuntimeException("DXT4 Not supported yet.");
            }else if(dxtType==DXTRawModel.TYPE_DXT5){
                imageFormat = GLC.Texture.CompressedFormat.DXT5;
            }else{
                throw new RuntimeException("Unexpected DXT image type : "+dxtType);
            }
            
            return new TextureModel(GL_TEXTURE_2D,imageFormat, imageType);
            
        }
        
        if(preserveColors && cm!=null){
            imageType = GLC.Texture.Type.UNSIGNED_BYTE;
            
            if(cm instanceof DirectColorModel){
                final DirectColorModel dcm = (DirectColorModel) cm;
                final int nbSample = rm.getSampleCount();
                final int dataType = rm.getPrimitiveType();
                final int[] mapping = dcm.getMapping();

                if(nbSample == 3
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping,ORDER_RGB) ){
                    //image is RGBA, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGB;
                    imageFormat = GLC.Texture.Format.RGB;

                }else if(nbSample == 4
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping,ORDER_RGBA)
                        && dcm.isAlphaPreMultiplied()){
                    //image is RGBA, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGBA;
                    imageFormat = GLC.Texture.Format.RGBA;      

                }else if(nbSample == 3
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping, ORDER_BGR)){
                    //image is BGR, we can reuse bit buffer directly
                    imageInternalFormat = GLC.Texture.InternalFormat.RGB;
                    imageFormat = GLC.Texture.Format.BGR;

                }else if(nbSample == 4
                        && dataType == RawModel.TYPE_UBYTE
                        && Arrays.equals(mapping,ORDER_BGRA)
                        && dcm.isAlphaPreMultiplied()){
                     //image is BGRA, we can reuse bit buffer directly
                     imageInternalFormat = GLC.Texture.InternalFormat.RGBA;
                     imageFormat = GLC.Texture.Format.BGRA;

                }else{
                    throw new InvalidArgumentException("Unsupported image color and sample models.");
                }
            }else{
                throw new InvalidArgumentException("Unsupported image color and sample models.");
            }
            
        }else{
            //opengl support only interleaved images
            if(rm instanceof InterleavedRawModel){
                final int nbSample = rm.getSampleCount();
                if(nbSample==0 || nbSample>4) throw new InvalidArgumentException("Images with more then 4 samples are not supported by opengl");
                final int primitiveType = rm.getPrimitiveType();
                imageType = GLUtilities.primitiveTypeToGlType(primitiveType);
                if(imageType==0){
                    throw new InvalidArgumentException("Unsupported primitive type "+primitiveType);
                }
                
                if(nbSample==1){
                    imageFormat = GLC.Texture.Format.RED_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.R8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.R8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.R16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.R16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.R32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.R32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.R32F; break;
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);
                    }                    
                }else if(nbSample==2){
                    imageFormat = GLC.Texture.Format.RG_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.RG8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.RG8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.RG16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.RG16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.RG32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.RG32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.RG32F; break; 
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);                 
                    }   
                    
                }else if(nbSample==3){
                    imageFormat = GLC.Texture.Format.RGB_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.RGB8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.RGB8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.RGB16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.RGB16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.RGB32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.RGB32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.RGB32F; break;   
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);               
                    }   
                    
                }else if(nbSample==4){
                    imageFormat = GLC.Texture.Format.RGBA_INTEGER;
                    switch(imageType){
                        case BYTE :           imageInternalFormat = GLC.Texture.InternalFormat.RGBA8I; break;
                        case UNSIGNED_BYTE :  imageInternalFormat = GLC.Texture.InternalFormat.RGBA8UI; break;
                        case SHORT :          imageInternalFormat = GLC.Texture.InternalFormat.RGBA16I; break;
                        case UNSIGNED_SHORT : imageInternalFormat = GLC.Texture.InternalFormat.RGBA16UI; break;
                        case INT :            imageInternalFormat = GLC.Texture.InternalFormat.RGBA32I; break;
                        case UNSIGNED_INT :   imageInternalFormat = GLC.Texture.InternalFormat.RGBA32UI; break;
                        case FLOAT :          imageInternalFormat = GLC.Texture.InternalFormat.RGBA32F; break;  
                        default: throw new InvalidArgumentException("Unsupported primitive type "+imageType);                
                    }   
                }else{
                    throw new InvalidArgumentException("Unexpected number of samples "+nbSample);
                }         
                
            }else{
                //TODO comvert image to interleaved
                throw new InvalidArgumentException("Only interleaved image model are supported");
            }
        }
        
        //TODO disable interpolation for integer type textures
        //model.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.NEAREST);
        //model.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.NEAREST);
        
        return TextureModel.create2D(imageInternalFormat, imageFormat, imageType);
    }
    
}
