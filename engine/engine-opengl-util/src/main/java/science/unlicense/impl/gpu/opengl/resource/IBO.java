
package science.unlicense.impl.gpu.opengl.resource;

import java.nio.ByteBuffer;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 * Index Buffer Object resource.
 *
 * @author Johann Sorel
 */
public class IBO extends AbstractTupleBufferResource implements TupleBuffer1D {

    private int capacity;

    /**
     * New IBO with no data set.
     */
    public IBO() {}

    /**
     * New IBO from given integer buffer.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public IBO(Buffer buffer, int tupleSize) {
        setBuffer(buffer, tupleSize);
    }

    /**
     * New IBO from given int array.
     *
     * @param array data array
     * @param tupleSize number of value for each tuple
     */
    public IBO(int[] array, int tupleSize) {
        this(DefaultBufferFactory.wrap(array), tupleSize);
    }

    /**
     * Number of values in the buffer
     * @return number of values in the buffer
     */
    public int getCapacity() {
        return capacity;
    }
    
    /**
     * Set IBO datas.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setBuffer(Buffer buffer, int tupleSize) {
        setBuffer(buffer, GLC.TYPE.INT, tupleSize);
    }

    public void setBuffer(Buffer buffer, int gpuType, int tupleSize) {
        super.setBuffer(buffer, gpuType, tupleSize);
        this.capacity = (int) buffer.getPrimitiveCount();
    }
    
    public int getTuple(int index){
        //TODO change red method based on index value type : byte,short,int
        return buffer.readInt(index*4);
    }
    
    /**
     * OpenGL constraints : GL2GL3
     * 
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        bind(gl);
        
        //buffer data are in little endian
        buffer = DefaultBufferFactory.create(capacity*4, Primitive.TYPE_BYTE, NumberEncoding.LITTLE_ENDIAN);
        
        gl.asGL1().glGetBufferSubData(GL_ELEMENT_ARRAY_BUFFER,0, (ByteBuffer) GLUtilities.asGLBuffer(buffer));
        GLUtilities.checkGLErrorsFail(gl);
        
        buffer = buffer.copy(Primitive.TYPE_INT, NumberEncoding.BIG_ENDIAN);
        
        unbind(gl);
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1) return; //already loaded
        if(buffer==null) throw new ResourceException("No data buffer to load");
        gl.asGL1().glGenBuffers(bufferId);
        gl.asGL1().glBindBuffer(GL_ARRAY_BUFFER, bufferId[0]);
        gl.asGL1().glBufferData(GL_ARRAY_BUFFER, GLUtilities.asGLBuffer(buffer), GL_STATIC_DRAW);
        dirty = false;
        if(isForgetOnLoad()){
            buffer = null;
        }
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1){
            gl.asGL1().glDeleteBuffers(bufferId);
            bufferId[0] = -1;
        }
    }

    /**
     * Bind this index buffer.
     *
     * @param gl OpenGL instance
     */
    public void bind(GL gl){
        gl.asGL1().glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId[0]);
    }

    public void unbind(GL gl){
        gl.asGL1().glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    
    @Override
    public TupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setTuple(int coordinate, Object sample) {
        setTuple(new int[]{coordinate}, sample);
    }

    @Override
    public int getDimension() {
        return (int) getExtent().get(0);
    }

    @Override
    public Object getTuple(int coordinate, Object buffer) {
        return getTuple(new int[]{coordinate}, buffer);
    }

    @Override
    public boolean[] getTupleBoolean(int coordinate, boolean[] buffer) {
        return getTupleBoolean(new int[]{coordinate}, buffer);
    }

    @Override
    public byte[] getTupleByte(int coordinate, byte[] buffer) {
        return getTupleByte(new int[]{coordinate}, buffer);
    }

    @Override
    public int[] getTupleUByte(int coordinate, int[] buffer) {
        return getTupleUByte(new int[]{coordinate}, buffer);
    }

    @Override
    public short[] getTupleShort(int coordinate, short[] buffer) {
        return getTupleShort(new int[]{coordinate}, buffer);
    }

    @Override
    public int[] getTupleUShort(int coordinate, int[] buffer) {
        return getTupleUShort(new int[]{coordinate}, buffer);
    }

    @Override
    public int[] getTupleInt(int coordinate, int[] buffer) {
        return getTupleInt(new int[]{coordinate}, buffer);
    }

    @Override
    public long[] getTupleUInt(int coordinate, long[] buffer) {
        return getTupleUInt(new int[]{coordinate}, buffer);
    }

    @Override
    public long[] getTupleLong(int coordinate, long[] buffer) {
        return getTupleLong(new int[]{coordinate}, buffer);
    }

    @Override
    public float[] getTupleFloat(int coordinate, float[] buffer) {
        return getTupleFloat(new int[]{coordinate}, buffer);
    }

    @Override
    public double[] getTupleDouble(int coordinate, double[] buffer) {
        return getTupleDouble(new int[]{coordinate}, buffer);
    }

    @Override
    public IBO copy() {
        return (IBO) super.copy();
    }

    @Override
    public IBO copy(Buffer buffer) {
        buffer.writeByte(this.buffer.toByteArray(),0);
        return new IBO(buffer, nbSample);
    }
    

}