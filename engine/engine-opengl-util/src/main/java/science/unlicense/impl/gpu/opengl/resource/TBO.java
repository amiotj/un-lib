
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.gpu.opengl.GL3;
import science.unlicense.api.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 * Texture Buffer Object resource.
 *
 * https://www.opengl.org/wiki/Buffer_Texture
 *
 * @author Johann Sorel
 */
public class TBO extends AbstractResource {

    private Buffer buffer;
    private boolean dirty = true;
    private int bytePerElement = 0;
    private int type;
    private int tupleSize;
    private int size;

    //gpu id
    private final int[] bufferId = new int[]{-1};
    private final int[] textureId = new int[]{-1};

    /**
     * New TBO with no data set.
     */
    public TBO() {}

    /**
     * New TBO from given integer buffer.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public TBO(Buffer buffer, int tupleSize) {
        setBuffer(buffer, tupleSize);
    }

    /**
     * New TBO from given int array.
     *
     * @param array data array
     * @param tupleSize number of value for each tuple
     */
    public TBO(int[] array, int tupleSize) {
        this(DefaultBufferFactory.wrap(array), tupleSize);
    }

    /**
     * New TBO from given float array.
     *
     * @param array data array
     * @param tupleSize number of value for each tuple
     */
    public TBO(float[] array, int tupleSize) {
        this(DefaultBufferFactory.wrap(array), tupleSize);
    }

    /**
     * Get TBO data buffer.
     * Can be null.
     *
     * @return Buffer or null if not loaded on system memory
     */
    public Buffer getBuffer() {
        return buffer;
    }

    /**
     * Get the number of bytes use for each value in the buffer.
     * This size is derivate from the gpu data type;
     *
     * @return number of byte per value. 0 is data buffer has not been set
     */
    public int getBytePerElement() {
        return bytePerElement;
    }

    /**
     * Get number of value for each tuple.
     * Tuple can represent various objects like :
     * - vertex position, size = 3
     * - vertex normal, size = 3
     * - texture uv coordinate, size = 2
     *
     * @return number of value for each tuple
     */
    public int getTupleSize() {
        return tupleSize;
    }

    /**
     * Get a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer optional buffer
     * @return tuple
     */
    public int[] getTupleInt(int index, int[] buffer){
        if(buffer==null) buffer = new int[tupleSize];
        this.buffer.readInt(buffer, index*4*tupleSize);
        return buffer;
    }
    
    /**
     * Get a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer optional buffer
     * @return tuple
     */
    public float[] getTupleFloat(int index, float[] buffer){
        if(buffer==null) buffer = new float[tupleSize];
        this.buffer.readFloat(buffer, index*4*tupleSize);
        return buffer;
    }
    
    public void setBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLUtilities.primitiveTypeToGlType(buffer.getPrimitiveType()), tupleSize);
    }
    
    /**
     * Set VBO datas.
     * 
     * OpenGL constraints : GL2ES2
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setIntBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLC.GL_INT, tupleSize);
    }

    /**
     * Set VBO datas.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public void setFloatBuffer(Buffer buffer, int tupleSize) {
        this.setBuffer(buffer, GLC.GL_FLOAT, tupleSize);
    }

    /**
     * Set VBO datas.
     *
     * @param buffer data buffer
     * @param gpuType gpu data type
     * @param tupleSize number of value for each tuple
     */
    public void setBuffer(Buffer buffer, int gpuType, int tupleSize) {
        this.buffer = buffer;

        if(gpuType == GLC.GL_INT || gpuType == GLC.GL_FLOAT){
            bytePerElement = 4;
        }else{
            throw new RuntimeException("Unknowned buffer element type : " + gpuType);
        }
        this.type = gpuType;
        this.tupleSize = tupleSize;
        this.size = (int) buffer.getPrimitiveCount();
        this.dirty = true;
    }

    /**
     * Set a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer tuple data
     */
    public void setTupleInt(int index, int[] buffer){
        this.buffer.writeInt(buffer, index*4*tupleSize);
    }
    
     /**
     * Set a single tuple at index.
     * 
     * @param index tuple index
     * @param buffer tuple data
     */
    public void setTupleFloat(int index, float[] buffer){
        this.buffer.writeFloat(buffer, index*4*tupleSize);
    }
    
    /**
     * Indicate if the system data buffer values has been changed.
     *
     * @return true if datas has been changed compared to the last time
     *          they were loaded on the gpu.
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    public int getGpuID() {
        return bufferId[0];
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnSystemMemory() {
        return buffer != null;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnGpuMemory() {
        return bufferId[0] != -1;
    }

    /**
     * OpenGL constraints : GL2
     * 
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        throw new ResourceException("not supported yet");
    }

    /**
     * OpenGL constraints : GL2GL3
     * 
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1) return; //already loaded
        
        final GL2ES2 gl2 = gl.asGL2ES2();
        
        //generate buffer
        gl2.glGenBuffers(bufferId);
        gl2.glBindBuffer(GLC.GL_TEXTURE_BUFFER, bufferId[0]);
        gl2.glBufferData(GLC.GL_TEXTURE_BUFFER, GLUtilities.asGLBuffer(buffer), GLC.GL_DYNAMIC_DRAW);
        gl2.glBindBuffer(GLC.GL_TEXTURE_BUFFER, 0);
        GLUtilities.checkGLErrorsFail(gl);
        
        //generate and attach texture
        gl2.glGenTextures(textureId);
        gl2.glBindTexture(GLC.GL_TEXTURE_BUFFER, textureId[0]);
        gl2.asGL2ES3().glTexBuffer(GLC.GL_TEXTURE_BUFFER, GLC.GL_RGBA32F, bufferId[0]);
        gl2.glBindTexture(GLC.GL_TEXTURE_BUFFER, 0);
        GLUtilities.checkGLErrorsFail(gl);
        
        dirty = false;
        
        if(isForgetOnLoad()){
            buffer = null;
        }
    }
    
    public void updateData(GL gl){
        gl.asGL1().glBindBuffer(GLC.GL_TEXTURE_BUFFER, bufferId[0]);
        gl.asGL1().glBufferSubData(GLC.GL_TEXTURE_BUFFER, 0, GLUtilities.asGLBuffer(buffer));
        gl.asGL1().glBindBuffer(GLC.GL_TEXTURE_BUFFER, 0);
    }
    
    /**
     * {@inheritDoc }
     */
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        buffer = null;
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1){
            gl.asGL1().glDeleteBuffers(bufferId);
            bufferId[0] = -1;
        }
    }

    /**
     * Bind this buffer providing using given parameters
     *
     * OpenGL constraints : GL2GL3
     * 
     * @param gl OpenGL instance
     */
    public void bind(GL gl){
        gl.asGL1().glBindTexture(GLC.GL_TEXTURE_BUFFER, textureId[0]);
    }
    
    /**
     * OpenGL constraints : GL2GL3
     * 
     * @param gl OpenGL instance
     */
    public void unbind(GL gl){
        gl.asGL1().glBindTexture(GLC.GL_TEXTURE_BUFFER, 0);
    }

    public static final TBO createFloat(int size, int tuleSize){
        return new TBO(DefaultBufferFactory.INSTANCE.createFloat(size), tuleSize);
    }
    
}
