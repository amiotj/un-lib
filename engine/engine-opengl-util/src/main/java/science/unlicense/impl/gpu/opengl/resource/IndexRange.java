
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.character.Chars;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES3;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 * Define and index for drawing.
 * 
 * This range can be used with or without an IBO.
 * 
 * 
 * @author Johann Sorel
 */
public class IndexRange {

    private final int mode;
    private final int indexValueType;
    private final int tupleSize;
    private final int indexOffset;
    private final int byteOffset;
    private final int count;

    public static IndexRange POINTS(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.POINTS, GL_UNSIGNED_INT, 1, offset, count);
    }

    public static IndexRange LINE_STRIP(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.LINE_STRIP, GL_UNSIGNED_INT, 2, offset, count);
    }

    public static IndexRange LINE_LOOP(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.LINE_LOOP, GL_UNSIGNED_INT, 2, offset, count);
    }

    public static IndexRange LINES(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.LINES, GL_UNSIGNED_INT, 2, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexRange LINE_STRIP_ADJACENCY(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.LINE_STRIP_ADJACENCY, GL_UNSIGNED_INT, 2, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexRange LINES_ADJACENCY(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.LINES_ADJACENCY, GL_UNSIGNED_INT, 2, offset, count);
    }

    public static IndexRange TRIANGLE_STRIP(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.TRIANGLE_STRIP, GL_UNSIGNED_INT, 3, offset, count);
    }

    public static IndexRange TRIANGLE_FAN(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.TRIANGLE_FAN, GL_UNSIGNED_INT, 3, offset, count);
    }

    public static IndexRange TRIANGLES(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.TRIANGLES, GL_UNSIGNED_INT, 3, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexRange TRIANGLE_STRIP_ADJACENCY(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.TRIANGLE_STRIP_ADJACENCY, GL_UNSIGNED_INT, 3, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexRange TRIANGLES_ADJACENCY(int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.TRIANGLES_ADJACENCY, GL_UNSIGNED_INT, 3, offset, count);
    }

    /**
     * OpenGL constraints : GL3
     */
    public static IndexRange PATCHES(int tupleSize, int offset, int count) {
        return new IndexRange(GLC.PRIMITIVE.PATCHES, GL_UNSIGNED_INT, tupleSize, offset, count);
    }

    /**
     *
     * @param mode GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES,
     *      GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP,
     *      GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY,
     *      GL_TRIANGLES_ADJACENCY and GL_PATCHES
     * @param indexValueType GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, or GL_UNSIGNED_INT.
     * @param tupleSize informative value of the number of element to mmake a tuple.
     * @param offset offset from the beginning of the IBO
     *               do not multiply by the number of byte of the index value type.
     *               multiplication will be done automaticaly
     * @param count number of values from offset in the IBO
     */
    public IndexRange(int mode, int indexValueType, int tupleSize, int offset, int count) {
        this.mode = mode;
        this.indexValueType = indexValueType;
        this.tupleSize = tupleSize;
        this.indexOffset = offset;
        if (indexValueType == GL_UNSIGNED_BYTE) {
            this.byteOffset = offset;
        } else if (indexValueType == GL_UNSIGNED_SHORT) {
            this.byteOffset = offset * 2;
        } else if (indexValueType == GL_UNSIGNED_INT) {
            this.byteOffset = offset * 4;
        } else {
            throw new RuntimeException("Unexpected index value type : " + indexValueType);
        }
        this.count = count;
    }

    /**
     * Range mode.
     * GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES,
     * GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP,
     * GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY,
     * GL_TRIANGLES_ADJACENCY or GL_PATCHES
     * @return int
     */
    public int getMode() {
        return mode;
    }

    /**
     * Type of values in the index.
     * GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, or GL_UNSIGNED_INT.
     *
     * This value has meaning only when used with an IBO.
     *
     * @return int
     */
    public int getIndexValueType() {
        return indexValueType;
    }

    /**
     * Informative value of the number of element to mmake a tuple.
     * @return int
     */
    public int getTupleSize() {
        return tupleSize;
    }

    /**
     * Offset from the beginning of the IBO.
     * Offset is in BYTES.
     *
     * @return int Offset in bytes.
     */
    public int getBytesOffset() {
        return byteOffset;
    }

    /**
     * Offset from the beginning of the IBO.
     * Offset is an index for the buffer.
     * 
     * @return int Offset in elements in the buffer.
     */
    public int getIndexOffset() {
        return indexOffset;
    }

    /**
     * Number of values in the index range.
     *
     * If used with an IBO, this is the number of values from offset in the IBO.
     *
     * @return int
     */
    public int getCount() {
        return count;
    }
    
    public void draw(GL gl, boolean withIBO){
        if(mode == GLC.PRIMITIVE.PATCHES){
            if(gl.isGL2ES3()){
                //activate patch informations, and change type
                gl.asGL2ES3().glPatchParameteri(GL_PATCH_VERTICES, tupleSize);
                GLUtilities.checkGLErrorsFail(gl);
            }else{
                //TODO log, but we should not log too often either, find a solution
                Loggers.get().log(new Chars("glPatchParameteri not supported for gl version < GL3"), Logger.LEVEL_INFORMATION);
            }
        }
        
        if(withIBO){
            gl.asGL1().glDrawElements(mode,count,indexValueType,byteOffset);
        }else{
            gl.asGL1().glDrawArrays(mode,indexOffset,count);
        }
        GLUtilities.checkGLErrorsFail(gl);
    }
    
    public void drawInstanced(GL gl, boolean withIBO, int nbInstance){
        GL2ES3 gl2es3 = gl.asGL2ES3();
        
        if(mode == GLC.PRIMITIVE.PATCHES){
            if(gl.isGL2ES3()){
                //activate patch informations, and change type
                gl.asGL2ES3().glPatchParameteri(GL_PATCH_VERTICES, tupleSize);
                GLUtilities.checkGLErrorsFail(gl);
            }else{
                //TODO log, but we should not log too often either, find a solution
                Loggers.get().log(new Chars("glPatchParameteri not supported for gl version < GL3"), Logger.LEVEL_INFORMATION);
            }
        }
        
        if(withIBO){
            gl2es3.glDrawElementsInstanced(mode,count,indexValueType,byteOffset,nbInstance);
        }else{
            gl2es3.glDrawArraysInstanced(mode,indexOffset,count,nbInstance);
        }
        
    }
    
}
