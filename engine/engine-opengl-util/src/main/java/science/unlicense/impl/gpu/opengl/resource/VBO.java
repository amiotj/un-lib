
package science.unlicense.impl.gpu.opengl.resource;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL1;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.gpu.opengl.GL2ES3;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 * Vertex Buffer Object resource.
 *
 * http://www.opengl.org/wiki/Vertex_Buffer_Object#Vertex_Buffer_Object
 *
 * @author Johann Sorel
 */
public class VBO extends AbstractTupleBufferResource {

    private int instancingDividor = -1;

    /**
     * New VBO with no data set.
     */
    public VBO() {}

    /**
     * New VBO from given int array.
     *
     * @param array data array
     * @param tupleSize number of value for each tuple
     */
    public VBO(int[] array, int tupleSize) {
        this(DefaultBufferFactory.wrap(array), tupleSize);
    }

    /**
     * New VBO from given float array.
     *
     * @param array data array
     * @param tupleSize number of value for each tuple
     */
    public VBO(float[] array, int tupleSize) {
        this(DefaultBufferFactory.wrap(array), tupleSize);
    }

    /**
     * New VBO from given buffer.
     *
     * @param buffer data buffer
     * @param tupleSize number of value for each tuple
     */
    public VBO(Buffer buffer, int tupleSize) {
        setBuffer(buffer, tupleSize);
    }
    
    public int getInstancingDividor() {
        return instancingDividor;
    }

    public void setInstancingDividor(int instancingDividor) {
        this.instancingDividor = instancingDividor;
    }
        
    /**
     * OpenGL constraints : GL2GL3
     * 
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        bind(gl,0);
        
        final int nbElement = getTupleCount()*getSampleCount();
        final int size = (int)nbElement*getBytePerElement();
        
        final GL1 gl2 = gl.asGL1();
        final ByteBuffer directbb = ByteBuffer.allocateDirect(size);
        GLUtilities.checkGLErrorsFail(gl);
        gl2.glGetBufferSubData(GL_ARRAY_BUFFER,0,directbb);
        GLUtilities.checkGLErrorsFail(gl);
        
        if(gpuType==GLC.TYPE.INT){
            final IntBuffer fb = directbb.order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
            final int[] array = new int[nbElement];
            fb.get(array);
            buffer = DefaultBufferFactory.wrap(array);
        }else if(gpuType==GLC.TYPE.FLOAT){
            final FloatBuffer fb = directbb.order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer();
            final float[] array = new float[nbElement];
            fb.get(array);
            buffer = DefaultBufferFactory.wrap(array);
        }else{
            throw new ResourceException("Not supported yet.");
        }
        
        unbind(gl);
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1 && !dirty) return; //already loaded
        if(buffer==null) throw new ResourceException("No data buffer to load");
        unloadFromGpuMemory(gl);
        gl.asGL1().glGenBuffers(bufferId);
        gl.asGL1().glBindBuffer(GL_ARRAY_BUFFER, bufferId[0]);
        gl.asGL1().glBufferData(GL_ARRAY_BUFFER, GLUtilities.asGLBuffer(buffer), GL_STATIC_DRAW);
        dirty = false;
        if(isForgetOnLoad()){
            buffer = null;
        }
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1){
            gl.asGL1().glDeleteBuffers(bufferId);
            bufferId[0] = -1;
        }
    }

    /**
     * Bind this buffer at given index.
     * Normalized is set to false.
     * Stride is set to zero.
     * Offset is set to zero.
     *
     * @param gl OpenGL instance
     * @param index layout index
     */
    public void bind(GL gl, int index){
        bind(gl,index,false,0);
    }

    /**
     * Bind this buffer providing using given parameters
     *
     * OpenGL constraints : 
     * - GL2ES2 for float type 
     * - GL2ES3 for int type
     * 
     * @param gl OpenGL instance
     * @param index layout index
     * @param normalized usef id float only.
     * @param offset buffer offset
     */
    public void bind(GL gl, int index, boolean normalized, int offset){
        final int stride = GLUtilities.calculateStride(gpuType, nbSample);
        gl.asGL1().glBindBuffer(GL_ARRAY_BUFFER, getGpuID());
        
        //divide in slots of 4 floats
        final int nbSlot = (int)Math.ceil(stride/(16.0));
        final int slotTupleSize = Math.min(nbSample, 4);
        for(int s=0;s<nbSlot;s++){
            if(gpuType == GLC.Texture.Type.INT){
                final GL2ES3 gl2 = gl.asGL2ES3();
                gl2.glVertexAttribIPointer(index,slotTupleSize,gpuType,stride,offset);
            }else if(gpuType == GLC.Texture.Type.FLOAT){
                final GL2ES2 gl2 = gl.asGL2ES2();
                gl2.glVertexAttribPointer(index,slotTupleSize,gpuType,normalized,stride,offset);
            }else{
                throw new RuntimeException("Unexpected type : "+gpuType);
            }
            GLUtilities.checkGLErrorsFail(gl);

            if(gl.isGL2ES3()){
                final GL2ES3 gl2es3 = gl.asGL2ES3();
                if(instancingDividor!=-1){
                    gl2es3.glVertexAttribDivisor(index, instancingDividor);
                }else{
                    //default value
                    gl2es3.glVertexAttribDivisor(index, 0);
                }
            }
            
            offset+=16;
            index++;
        }
    }
    
    public void unbind(GL gl){
        gl.asGL1().glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    public static final VBO createInt(int size, int tuleSize){
        return new VBO(DefaultBufferFactory.create(size, Primitive.TYPE_INT, NumberEncoding.BIG_ENDIAN), tuleSize);
    }
    
    public static final VBO createFloat(int size, int tuleSize){
        return new VBO(DefaultBufferFactory.create(size, Primitive.TYPE_FLOAT, NumberEncoding.BIG_ENDIAN), tuleSize);
    }

    @Override
    public TupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public VBO copy() {
        return (VBO) super.copy();
    }

    @Override
    public VBO copy(Buffer buffer) {
        buffer.writeByte(this.buffer.toByteArray(),0);
        return new VBO(buffer, nbSample);
    }
    
}
