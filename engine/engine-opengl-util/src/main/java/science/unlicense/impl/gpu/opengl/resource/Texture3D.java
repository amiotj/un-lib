

package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.exception.UnimplementedException;
import static science.unlicense.impl.gpu.opengl.GLC.Texture.*;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLC;
import science.unlicense.api.image.Image;

/**
 * OpenGL constraints : GL2ES2
 * 
 * @author Johann Sorel
 */
public class Texture3D extends AbstractTexture{

    public static TextureModel COLOR_RGBA(){
        final TextureModel tm = TextureModel.create3D(InternalFormat.RGBA,Format.RGBA,Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
     public static TextureModel COLOR_RGBA2(){
        final TextureModel tm = TextureModel.create3D(InternalFormat.RGBA2,Format.RGBA,Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC1_FLOAT(){
        final TextureModel tm = TextureModel.create3D(InternalFormat.R32F,Format.RED,Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC2_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RG32F, Format.RG,Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC3_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGB32F,Format.RGB,Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC4_FLOAT(){
        final TextureModel tm = TextureModel.create2D(InternalFormat.RGBA32F,Format.RGBA,Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    
    /**
     * OpenGL constraints : GL2ES2
     * 
     * @param image 
     */
    public Texture3D(Image image, TextureModel info) {
        super(image,info);
    }

    /**
     * OpenGL constraints : GL2ES2
     * 
     * @param width
     * @param height
     * @param depth
     * @param model 
     */
    public Texture3D(int width, int height, int depth, TextureModel model) {
        super(new Extent.Long(width, height, depth),model);
    }

    public int getWidth(){
        return (int)size.get(0);
    }
    
    public int getHeight(){
        return (int)size.get(1);
    }
    
    public int getDepth(){
        return (int)size.get(2);
    }
    
    /**
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        throw new UnimplementedException("Not supported yet.");
    }

    /**
     * OpenGL constraints : GL2ES2
     * 
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(texId>=0 && !dirty) return;
        dirty = false;

        //clean gpu memory before loading
        unloadFromGpuMemory(gl);

        if(image==null){
            //create a new texture
            int[] temp = new int[1];
            gl.asGL1().glGenTextures(temp);
            texId = temp[0];
            gl.asGL1().glBindTexture(GLC.GL_TEXTURE_3D, texId);
            TextureUtils.createTexture(new Extent.Double(getWidth(),getHeight(),getDepth()), gl, info);
            gl.asGL1().glBindTexture(GLC.GL_TEXTURE_3D, 0);

        }else{
            //load the image on gpu
            texId = TextureUtils.loadTexture(gl, image, info);
        }
        
        if(isForgetOnLoad()){
            image = null;
        }
    }
    
}
