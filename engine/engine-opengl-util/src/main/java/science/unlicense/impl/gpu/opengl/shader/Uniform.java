
package science.unlicense.impl.gpu.opengl.shader;

import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLState;

/**
 *
 * @author Johann Sorel
 */
public class Uniform {

    public static final int TYPE_BOOL           = 0;
    public static final int TYPE_INT            = 1;
    public static final int TYPE_FLOAT          = 2;
    public static final int TYPE_DOUBLE         = 3;
    public static final int TYPE_VEC2           = 4;
    public static final int TYPE_VEC3           = 5;
    public static final int TYPE_VEC4           = 6;
    public static final int TYPE_MAT3           = 7;
    public static final int TYPE_MAT4           = 8;
    public static final int TYPE_SAMPLER2D      = 9;
    public static final int TYPE_SAMPLER2DMS    = 10;
    public static final int TYPE_SAMPLERCUBE    = 11;
    public static final int TYPE_ISAMPLERBUFFER = 12;
    public static final int TYPE_SAMPLERBUFFER  = 13;
    public static final int TYPE_ISAMPLER2D     = 14;
    public static final int TYPE_USAMPLER2D     = 15;

    private final int programId;
    private final int gpuId;
    private final int type;

    public Uniform(int programId, int gpuId, int type) {
        this.programId = programId;
        this.gpuId = gpuId;
        this.type = type;
    }

    public boolean exist(){
        return gpuId >= 0;
    }

    public int getProgramId() {
        return programId;
    }

    public int getGpuId() {
        return gpuId;
    }

    public int getType() {
        return type;
    }

    public void setInt(GL2ES2 gl, int value){
        assert inValidState(gl);
        if(exist()) gl.glUniform1i(gpuId, value);
    }

    public void setFloat(GL2ES2 gl, float value){
        assert inValidState(gl);
        if(exist()) gl.glUniform1f(gpuId, value);
    }

    public void setVec2(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if(exist()) gl.glUniform2fv(gpuId, value);
    }

    public void setVec3(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if(exist()) gl.glUniform3fv(gpuId, value);
    }

    public void setVec4(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if(exist()) gl.glUniform4fv(gpuId, value);
    }

    public void setMat3(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if(exist()) gl.glUniformMatrix3fv(gpuId, true, value);
    }

    public void setMat4(GL2ES2 gl, float[] value){
        assert inValidState(gl);
        if(exist()) gl.glUniformMatrix4fv(gpuId, true, value);
    }

    private boolean inValidState(GL2ES2 gl) throws ShaderException{
        final int pid = GLState.getCurrentProgramId(gl);
        if(pid!=programId) throw new ShaderException("Uniform is from a different program, current is "+pid+" was expecting program "+ programId);
        return true;
    }

}
