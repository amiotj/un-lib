
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.impl.gpu.opengl.GLC.Texture.Format;
import science.unlicense.impl.gpu.opengl.GLC.Texture.InternalFormat;
import science.unlicense.impl.gpu.opengl.GLC.Texture.Parameters;
import science.unlicense.impl.gpu.opengl.GLC.Texture.Type;
import science.unlicense.impl.gpu.opengl.GLC.Texture.Target;
import science.unlicense.impl.gpu.opengl.GLC.FBO.Attachment;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLC;

/**
 * Resource to manipulate a multisample texture object.
 *
 * @author Johann Sorel
 */
public class Texture2DMS extends AbstractTexture {

    public static TextureModel COLOR_RGB(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RGB8, Format.RGB, Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
        
    public static TextureModel COLOR_RGBA(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RGBA8, Format.RGBA, Type.UNSIGNED_BYTE);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC1_INT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.R32I, Format.RED_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC2_INT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RG32I, Format.RG_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC3_INT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RGB32I, Format.RGB_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC4_INT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RGBA32I, Format.RGBA_INTEGER, Type.INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.NEAREST);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.NEAREST);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel VEC1_FLOAT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.R32F, Format.RED, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC2_FLOAT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RG32F, Format.RG, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC3_FLOAT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RGB32F, Format.RGB, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }

    public static TextureModel VEC4_FLOAT(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.RGBA32F, Format.RGBA, Type.FLOAT);
        tm.setParameter(Parameters.MIN_FILTER.KEY, Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY, Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,     Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,     Parameters.WRAP_T.CLAMP_TO_BORDER);
        return tm;
    }
    
    public static TextureModel DEPTH_24(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.DEPTH_COMPONENT24, Format.DEPTH_COMPONENT, Type.UNSIGNED_INT);
        tm.setParameter(Parameters.MIN_FILTER.KEY,   Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY,   Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,       Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,       Parameters.WRAP_T.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.COMPARE_MODE.KEY, Parameters.COMPARE_MODE.REF_TO_TEXTURE);
        tm.setParameter(Parameters.COMPARE_FUNC.KEY, Parameters.COMPARE_FUNC.LESS);
        return tm;
    }

    public static TextureModel DEPTH24_STENCIL8(){
        final TextureModel tm = TextureModel.create2DMS(InternalFormat.DEPTH24_STENCIL8, Format.DEPTH_STENCIL, Type.UNSIGNED_INT_24_8);
        tm.setParameter(Parameters.MIN_FILTER.KEY,   Parameters.MIN_FILTER.LINEAR);
        tm.setParameter(Parameters.MAG_FILTER.KEY,   Parameters.MAG_FILTER.LINEAR);
        tm.setParameter(Parameters.WRAP_S.KEY,       Parameters.WRAP_S.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.WRAP_T.KEY,       Parameters.WRAP_T.CLAMP_TO_BORDER);
        tm.setParameter(Parameters.COMPARE_MODE.KEY, Parameters.COMPARE_MODE.REF_TO_TEXTURE);
        tm.setParameter(Parameters.COMPARE_FUNC.KEY, Parameters.COMPARE_FUNC.LESS);
        return tm;
    }
    
    private int nbSample;

    /**
     * OpenGL constraints : GL2ES2
     * 
     * @param width
     * @param height
     * @param model
     * @param nbsample 
     */
    public Texture2DMS(int width, int height, TextureModel model, int nbsample) {
        super(new Extent.Long(width, height), model);
        this.nbSample = nbsample;
    }

    public int getWidth(){
        return (int)size.get(0);
    }
    
    public int getHeight(){
        return (int)size.get(1);
    }
    
    public int getNbSample() {
        return nbSample;
    }

    /**
     * This method will unload the texture.
     * 
     * @param nbSample 
     */
    public void setNbSample(GL gl, int nbSample) {
        if(this.nbSample == nbSample) return;
        this.nbSample = nbSample;
        unloadFromGpuMemory(gl);
    }
    
    public void reformat(GL gl, TextureModel info) {
        if(info.getTarget()!=Target.TEXTURE_2DMS){
            throw new InvalidArgumentException("Invalid target format, only TEXTURE_2DMS can be used.");
        }
        super.reformat(gl, info);
    }
    
    /**
     * {@inheritDoc }
     * 
     * This method will create a temporary fbos and blit the multisapled texture
     * in a second texture.
     */
    public void loadOnSystemMemory(GL gl) {
        //we cannot grab the texture data directly like a normal Texture2D
        //older solution : use a GL4Painter2D , slow
        //new solution : use glBlit with FBO, much better
        final Texture2D blitTex = blit(gl, null);
        if(blitTex!=null){
            blitTex.loadOnSystemMemory(gl);
            image = blitTex.getImage();
            blitTex.unloadFromGpuMemory(gl);
        }
    }

    /**
     * Blit (copy data removing multisampling) this texture.
     * 
     * @param gl
     * @param blitTexture, can be null
     * @return Texture2D
     */
    public Texture2D blit(GL gl, Texture2D blitTexture){
        final int width = getWidth();
        final int height = getHeight();
        if(blitTexture!=null && (blitTexture.getWidth()!=width || blitTexture.getHeight()!=height)){
            throw new InvalidArgumentException("Blit texture size does not match this texture.");
        }

        final int type;
        switch(getInfo().getFormat()){
            case Format.STENCIL_INDEX : type = Attachment.STENCIL; break;
            case Format.DEPTH_COMPONENT : type = Attachment.DEPTH; break;
            case Format.DEPTH_STENCIL : type = Attachment.DEPTH_STENCIL; break;
            default : type = Attachment.COLOR_0; break;
        }
        
        if(type!=Attachment.COLOR_0){
            //TODO
            System.out.println("Blit of stencil or depth texture not supported yet.");
            return null;
        }
        
        final FBO fbo2dms = new FBO(width,height);
        fbo2dms.addAttachment(type, this);

        //target blit fbo
        final FBO fbo2d;
        final Texture2D t2d;
        if(blitTexture==null){
            fbo2d = fbo2dms.createBlitFBO();
            t2d = (Texture2D) fbo2d.getColorTexture();
        }else{
            fbo2d = new FBO(width, height);
            fbo2d.addAttachment(type,blitTexture);
            t2d = blitTexture;
        }

        fbo2dms.loadOnGpuMemory(gl);
        fbo2d.loadOnGpuMemory(gl);
        fbo2dms.blit(gl, fbo2d);

        fbo2dms.removeAttachment(type);
        fbo2d.removeAttachment(type);
        fbo2d.unloadFromGpuMemory(gl);
        fbo2dms.unloadFromGpuMemory(gl);
        return t2d;
    }

    /**
     * OpenGL constraints : GL2ES2
     * 
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) {
        if(texId>=0 && !dirty) return;
        dirty = false;

        //clean gpu memory before loading
        //TODO reload image without unloading buffer if possible
        unloadFromGpuMemory(gl);
        
        if(image==null){
            //create a new texture
            int[] temp = new int[1];
            gl.asGL1().glGenTextures(temp);
            texId = temp[0];
            gl.asGL1().glBindTexture(GLC.GL_TEXTURE_2D_MULTISAMPLE, texId);
            TextureUtils.createTexture(new Extent.Double(getWidth(),getHeight(),nbSample), gl, info);
            gl.asGL1().glBindTexture(GLC.GL_TEXTURE_2D_MULTISAMPLE, 0);

        }else{
            //load the image on gpu
            texId = TextureUtils.loadTexture(gl, image, info);
        }
        
        if(isForgetOnLoad()){
            image = null;
        }
    }

}
