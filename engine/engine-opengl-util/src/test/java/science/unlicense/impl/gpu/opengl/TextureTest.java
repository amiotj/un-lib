
package science.unlicense.impl.gpu.opengl;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import com.jogamp.opengl.GLProfile;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.impl.gpu.jogamp.opengl.JGL;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.jogamp.opengl.JOGLUtils;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.TextureUtils;

/**
 *
 * @author Johann Sorel
 */
public class TextureTest extends GLTest {

    @Test
    public void loadUnloadTexture2D_1B_UBYTE(){

        final byte[] data = new byte[]{47,31,12};
        final Image image = Images.createCustomBand(new Extent.Long(3, 1), 1, RawModel.TYPE_UBYTE);
        image.getDataBuffer().writeByte(data, 0);

        final Texture2D texture = new Texture2D(image, TextureUtils.toTextureInfo(image, false));


        final GLOffscreenAutoDrawable drawable = JOGLUtils.createOffscreenDrawable(1, 1, GLProfile.getDefault());
        drawable.addGLEventListener(new GLEventListener() {
            public void init(GLAutoDrawable glad) {}
            public void dispose(GLAutoDrawable glad) {}
            public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {}

            public void display(GLAutoDrawable glad) {
                final GL gl = new JGL(glad.getGL());
                texture.loadOnGpuMemory(gl);
                texture.unloadFromSystemMemory(gl);
                texture.loadOnSystemMemory(gl);
            }
        });
        drawable.display();
        GLUtilities.checkGLErrorsFail(new JGL(drawable.getGL()));

        final Image resImage = texture.getImage();
        Assert.assertArrayEquals(data, resImage.getDataBuffer().toByteArray());
    }
    
    @Test
    public void loadUnloadTexture2D_2B_BYTE(){

        final byte[] data = new byte[]{-13,127,-121,46};
        final Image image = Images.createCustomBand(new Extent.Long(2, 1), 2, RawModel.TYPE_BYTE);
        image.getDataBuffer().writeByte(data, 0);

        final Texture2D texture = new Texture2D(image, TextureUtils.toTextureInfo(image, false));


        final GLOffscreenAutoDrawable drawable = JOGLUtils.createOffscreenDrawable(1, 1, GLProfile.getDefault());
        drawable.addGLEventListener(new GLEventListener() {
            public void init(GLAutoDrawable glad) {}
            public void dispose(GLAutoDrawable glad) {}
            public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {}

            public void display(GLAutoDrawable glad) {
                final GL gl = new JGL(glad.getGL());
                texture.loadOnGpuMemory(gl);
                texture.unloadFromSystemMemory(gl);
                texture.loadOnSystemMemory(gl);
            }
        });
        drawable.display();
        GLUtilities.checkGLErrorsFail(new JGL(drawable.getGL()));

        final Image resImage = texture.getImage();
        Assert.assertArrayEquals(data, resImage.getDataBuffer().toByteArray());
    }
    
    @Test
    public void loadUnloadTexture2D_RGB(){

        final byte[] data = new byte[]{9,9,9, 8,8,8, 7,7,7, 6,6,6};
        final Image image = Images.create(new Extent.Long(4, 1), Images.IMAGE_TYPE_RGB);
        image.getDataBuffer().writeByte(data, 0);

        final Texture2D texture = new Texture2D(image, TextureUtils.toTextureInfo(image, true));


        final GLOffscreenAutoDrawable drawable = JOGLUtils.createOffscreenDrawable(1, 1, GLProfile.getDefault());
        drawable.addGLEventListener(new GLEventListener() {
            public void init(GLAutoDrawable glad) {}
            public void dispose(GLAutoDrawable glad) {}
            public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {}

            public void display(GLAutoDrawable glad) {
                final GL gl = new JGL(glad.getGL());
                texture.loadOnGpuMemory(gl);
                texture.unloadFromSystemMemory(gl);
                texture.loadOnSystemMemory(gl);
            }
        });
        drawable.display();
        GLUtilities.checkGLErrorsFail(new JGL(drawable.getGL()));

        final Image resImage = texture.getImage();
        Assert.assertArrayEquals(data, resImage.getDataBuffer().toByteArray());

    }

}