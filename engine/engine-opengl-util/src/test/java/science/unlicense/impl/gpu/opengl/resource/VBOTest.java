
package science.unlicense.impl.gpu.opengl.resource;

import science.unlicense.impl.gpu.opengl.resource.VBO;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 *
 * @author Johann Sorel
 */
public class VBOTest extends GLTest {

    private static final float DELTA = 0.00001f;
    
    @Test
    public void loadOnSystemMemoryTest(){
        
        final GLSource gld = GLUtilities.createOffscreenSource(1, 1);
        
        final VBO vbo = new VBO(new float[]{1,2,3,4,5,6}, 3);
        
        gld.getCallbacks().add(new GLCallback() {

            @Override
            public void execute(GLSource source) {
                final GL gl = source.getGL();
                vbo.loadOnGpuMemory(gl);
                vbo.unloadFromSystemMemory(gl);
                Assert.assertNull(vbo.getPrimitiveBuffer());

                vbo.loadOnSystemMemory(gl);
                final Buffer res = vbo.getPrimitiveBuffer();
                Assert.assertNotNull(res);
                Assert.assertArrayEquals(new float[]{1,2,3,4,5,6}, res.toFloatArray(), DELTA);
            }

            @Override
            public void dispose(GLSource source) {
            }
        });
        
        gld.render();
        
    }
    
}
