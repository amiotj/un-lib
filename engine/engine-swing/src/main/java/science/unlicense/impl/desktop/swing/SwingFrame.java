
package science.unlicense.impl.desktop.swing;

import java.awt.Point;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JFrame;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import static science.unlicense.api.desktop.Frame.PROP_CLOSABLE;
import static science.unlicense.api.desktop.UIFrame.PROP_FOCUSED_WIDGET;
import science.unlicense.api.desktop.FrameDecoration;
import science.unlicense.api.desktop.FrameManager;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.desktop.cursor.NamedCursor;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.event.Property;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class SwingFrame extends AbstractEventSource implements UIFrame {

    static { 
        //force loading class and registering default fonts
        SwingFontStore v = SwingFontStore.INSTANCE;
    }
    private final SwingFrameDecoration DEFAULT_DECO = new SwingFrameDecoration();
    private final SwingFrame parent;
    final JFrame jframe = new JFrame();
    private final SwingWContainer pane = new SwingWContainer();
    private Widget focused = null;
    private Cursor defaultCursor = NamedCursor.DEFAULT;
    private Cursor currentCursor = NamedCursor.DEFAULT;
    private FrameDecoration decoration = DEFAULT_DECO;


    private final AtomicBoolean disposed = new AtomicBoolean(false);

    public SwingFrame(SwingFrame parent,boolean translucent) {
        this.parent = parent;
        jframe.setContentPane(pane);
        jframe.setLocationRelativeTo(null);
        pane.container.setFrame(this);
        DEFAULT_DECO.setFrame(this);
    }

    @Override
    public FrameManager getManager() {
        return SwingFrameManager.INSTANCE;
    }

    @Override
    public WContainer getContainer() {
        return pane.container;
    }

    @Override
    public Widget getFocusedWidget() {
        return focused;
    }

    @Override
    public void setFocusedWidget(Widget focused) {
        if(CObjects.equals(this.focused, focused)) return;
        final Widget old = this.focused;
        if(old!=null) old.setFocused(false);
        this.focused = focused;
        if(this.focused!=null) this.focused.setFocused(true);
        sendPropertyEvent(this, PROP_FOCUSED_WIDGET, old, this.focused);
    }

    @Override
    public CharArray getTitle() {
        return new Chars(jframe.getTitle());
    }

    @Override
    public void setTitle(CharArray title) {
        jframe.setTitle(title==null ? "" : title.toString());
    }
    
    @Override
    public Property varTitle() {
        return getProperty(PROP_TITLE);
    }
    
    @Override
    public Cursor getCursor() {
        return defaultCursor;
    }

    @Override
    public void setCursor(Cursor defaultCursor) {
        final boolean updateCurrent = currentCursor == this.defaultCursor;
        this.defaultCursor = defaultCursor;
        if(updateCurrent) ;//TODO
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        jframe.setLocation((int)location.getX(), (int)location.getY());
    }

    @Override
    public Tuple getOnScreenLocation() {
        final Point pos = jframe.getLocationOnScreen();
        return new DefaultTuple(pos.x, pos.y);
    }

    @Override
    public void setSize(int width, int height) {
        jframe.setSize(width, height);
    }

    @Override
    public Extent getSize() {
        return new Extent.Double(jframe.getWidth(),jframe.getHeight());
    }

    @Override
    public void setVisible(boolean visible) {
        jframe.setVisible(visible);
    }

    @Override
    public boolean isVisible() {
        return jframe.isVisible();
    }
    
    @Override
    public Property varVisible() {
        return getProperty(PROP_VISIBLE);
    }
    
    @Override
    public FrameDecoration getSystemDecoration() {
        return DEFAULT_DECO;
    }

    @Override
    public void setDecoration(FrameDecoration decoration) {
        this.decoration = decoration;
        jframe.setUndecorated(this.decoration==null);
    }

    @Override
    public FrameDecoration getDecoration() {
        return decoration;
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        jframe.setAlwaysOnTop(ontop);
    }

    @Override
    public boolean isAlwaysOnTop() {
        return jframe.isAlwaysOnTop();
    }

    @Override
    public boolean isTranslucent() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void dispose() {
        jframe.dispose();
        synchronized(disposed){
            disposed.set(true);
            disposed.notify();
        }
    }

    @Override
    public int getState() {
        final int state = jframe.getExtendedState();
        if (state==JFrame.MAXIMIZED_BOTH) {
            if(jframe.isUndecorated()) {
                return UIFrame.STATE_FULLSCREEN;
            } else {
                return UIFrame.STATE_MAXIMIZED;
            }
        } else if(state==JFrame.MAXIMIZED_HORIZ) {
            return UIFrame.STATE_MAXIMIZED_HORIZONTAL;
        } else if(state==JFrame.MAXIMIZED_VERT) {
            return UIFrame.STATE_MAXIMIZED_VERTICAL;
        } else if(state==JFrame.ICONIFIED) {
            return UIFrame.STATE_MINIMIZED;
        }
        return UIFrame.STATE_NORMAL;
    }

    @Override
    public void setState(int state) {
        if (state==UIFrame.STATE_FULLSCREEN) {
            jframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
            jframe.dispose();
            jframe.setUndecorated(true);
            jframe.setVisible(true);
        } else if (state==UIFrame.STATE_MAXIMIZED) {
            jframe.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else if (state==UIFrame.STATE_MAXIMIZED_HORIZONTAL) {
            jframe.setExtendedState(JFrame.MAXIMIZED_HORIZ);
        } else if (state==UIFrame.STATE_MAXIMIZED_VERTICAL) {
            jframe.setExtendedState(JFrame.MAXIMIZED_VERT);
        } else if (state==UIFrame.STATE_NORMAL) {
            jframe.setExtendedState(JFrame.NORMAL);
        } else if (state==UIFrame.STATE_MINIMIZED) {
            jframe.setExtendedState(JFrame.ICONIFIED);
        }
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMaximizable() {
        throw new UnimplementedException("Not supported yet.");
    }
    
    @Override
    public Property varMaximizable() {
        return getProperty(PROP_MAXIMIZABLE);
    }
    
    @Override
    public void setMinimizable(boolean minimizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMinimizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Property varMinimizable() {
        return getProperty(PROP_MINIMIZABLE);
    }
    
    @Override
    public void setClosable(boolean closable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isClosable() {
        throw new UnimplementedException("Not supported yet.");
    }
    
    @Override
    public Property varClosable() {
        return getProperty(PROP_CLOSABLE);
    }
    
    @Override
    public UIFrame getParentFrame() {
        return parent;
    }

    @Override
    public Sequence getChildrenFrames() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isModale() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isDisposed() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void waitForDisposal() {
        synchronized(disposed) {
            while (!disposed.get()) {
                try {
                    disposed.wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}