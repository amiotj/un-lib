
package science.unlicense.impl.desktop.swing;

import java.awt.Insets;
import science.unlicense.api.desktop.Frame;
import science.unlicense.api.desktop.FrameDecoration;
import science.unlicense.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public class SwingFrameDecoration implements FrameDecoration {

    private SwingFrame frame;

    public SwingFrame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = (SwingFrame) frame;
    }

    public Margin getMargin() {
        final Insets insets = frame.jframe.getInsets();
        return new Margin(insets.top, insets.right, insets.bottom, insets.left);
    }

}
