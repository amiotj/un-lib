
package science.unlicense.impl.desktop.swing;

import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class AWTUtils {
    
    public static Shape toShape(Geometry2D geom){
        if (geom==null) return null;

        if (geom instanceof Point) {
            final Point g = (Point) geom;
            return new Rectangle2D.Double(g.getX(), g.getY(), 0, 0);
        } else {
            final PathIterator ite = geom.createPathIterator();
            final GeneralPath path = new GeneralPath();

            final TupleRW p = new DefaultTuple(4);
            final TupleRW c1 = new DefaultTuple(4);
            final TupleRW c2 = new DefaultTuple(4);
            while(ite.next()){
                final int type = ite.getType();
                if (type==PathIterator.TYPE_MOVE_TO) {
                    ite.getPosition(p);
                    path.moveTo(p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_LINE_TO) {
                    ite.getPosition(p);
                    path.lineTo(p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_ARC) {
                    ite.getPosition(p);
                    path.lineTo(p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_CUBIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    ite.getSecondControl(c2);
                    path.curveTo(c1.getX(), c1.getY(), c2.getX(), c2.getY(), p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_QUADRATIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    path.quadTo(c1.getX(), c1.getY(), p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_CLOSE) {
                    path.closePath();
                }
            }
            return path;
        }
    }
    
    public static Geometry2D toGeometry2D(Shape shape) {
        if (shape==null) return null;
        
        final java.awt.geom.PathIterator ite = shape.getPathIterator(null);
        
        final Path path = new Path();
        
        final double[] coords = new double[8];
        for (;!ite.isDone(); ite.next()) {
            final int type = ite.currentSegment(coords);
            if (type==java.awt.geom.PathIterator.SEG_MOVETO) {
                path.appendMoveTo(coords[0], coords[1]);
            } else if(type==java.awt.geom.PathIterator.SEG_LINETO) {
                path.appendLineTo(coords[0], coords[1]);
            } else if(type==java.awt.geom.PathIterator.SEG_CUBICTO) {
                path.appendCubicTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
            } else if(type==java.awt.geom.PathIterator.SEG_QUADTO) {
                path.appendQuadTo(coords[0], coords[1], coords[2], coords[3]);
            } else if(type==java.awt.geom.PathIterator.SEG_CLOSE) {
                path.appendClose();
            }
        }
        
        return path;
    }
    
}
