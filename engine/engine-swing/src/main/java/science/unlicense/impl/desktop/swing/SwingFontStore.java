
package science.unlicense.impl.desktop.swing;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.image.BufferedImage;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;
import science.unlicense.api.collection.primitive.IntSet;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.system.jvm.JVMSystem;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class SwingFontStore implements FontStore {

    
    static final GraphicsEnvironment ENV = GraphicsEnvironment.getLocalGraphicsEnvironment();
    static final Graphics2D GRAPHICS = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics();
    static final FontRenderContext CTX = GRAPHICS.getFontRenderContext();
    
    public static final SwingFontStore INSTANCE = new SwingFontStore();
    
    private Set families;
    private Dictionary fonts;
    
    private SwingFontStore() {
        try {
            registerFont(Paths.resolve(new Chars("mod:/font/Tuffy.ttf")));
            registerFont(Paths.resolve(new Chars("mod:/font/Mona.ttf")));
            registerFont(Paths.resolve(new Chars("mod:/font/Unicon.ttf")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        
    }
    
    @Override
    public synchronized Set getFamilies() {
        if (families==null) {
            families = new HashSet();
            final String[] names = ENV.getAvailableFontFamilyNames();
            for (int i=0;i<names.length;i++) {
                families.add(new Chars(names[i]));
            }
            fonts = new HashDictionary();
            final Font[] allFonts = ENV.getAllFonts();
            for (int i=0;i<allFonts.length;i++) {
                fonts.add(new Chars(allFonts[i].getFamily()), allFonts[i]);
            }
        }
        return families;
    }

    @Override
    public boolean containsFamily(Chars name) {
        return getFamilies().contains(name);
    }

    @Override
    public FontMetadata getMetadata(Chars family) {
        final Font font = (Font) fonts.getValue(family);
        return new SwingFontMetadata(font);
    }

    @Override
    public synchronized Geometry2D getGlyph(int unicode, Chars family) {
        final Font font = (Font) fonts.getValue(family);
        final GlyphVector glyph = font.createGlyphVector(CTX, new int[]{unicode});
        final Shape shape = glyph.getGlyphOutline(0);
        return AWTUtils.toGeometry2D(shape);
    }

    @Override
    public Geometry2D getGlyph(Char character, Chars family) {
        return getGlyph(character.toUnicode(), family);
    }

    @Override
    public synchronized IntSet listCharacters(Chars family) {
        final Font font = (Font) fonts.getValue(family);
        throw new UnimplementedException();
    }

    @Override
    public synchronized void registerFont(Path path) throws IOException {
        final ByteInputStream is = path.createInputStream();
        try {
            final Font font = Font.createFont(Font.TRUETYPE_FONT, JVMSystem.toInputStream(is));
            ENV.registerFont(font);
            families = null;
        } catch (FontFormatException ex) {
            throw new IOException(ex);
        } catch (java.io.IOException ex) {
            throw new IOException(ex);
        }
    }
    
}
