
package science.unlicense.impl.desktop.swing;

import java.awt.Dimension;
import science.unlicense.api.character.Chars;
import science.unlicense.api.desktop.Frame;
import science.unlicense.api.desktop.FrameManager;
import science.unlicense.api.desktop.cursor.NamedCursor;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.desktop.UIFrame;

/**
 *
 * @author Johann Sorel
 */
public class SwingFrameManager implements FrameManager{

    public static final SwingFrameManager INSTANCE = new SwingFrameManager();
    private static final Chars NAME = new Chars("Swing");

    public SwingFrameManager() {
    }

    @Override
    public Chars getName() {
        return NAME;
    }

    @Override
    public Chars[] getCursorNames() {
        return new Chars[]{NamedCursor.DEFAULT.getName(),NamedCursor.NULL.getName()};
    }

    @Override
    public UIFrame createFrame(boolean translucent) {
        return createFrame(null, false, translucent);
    }

    @Override
    public UIFrame createFrame(Frame parent, boolean modale, boolean translucent) {
        return new SwingFrame((SwingFrame) parent,translucent);
    }

    @Override
    public Extent getDisplaySize() {
        final Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        return new Extent.Double(screenSize.width, screenSize.height);
    }

    @Override
    public void setIcon(Chars imagePath) {
        //TODO
    }


}
