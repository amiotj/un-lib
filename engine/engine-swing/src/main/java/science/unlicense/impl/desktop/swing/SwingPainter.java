
package science.unlicense.impl.desktop.swing;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.painter2d.AbstractPainter2D;
import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.api.painter2d.LinearGradientPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.api.painter2d.RadialGradientPaint;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class SwingPainter extends AbstractPainter2D{

    private static final AffineTransform IDENTITY = new AffineTransform();
    private final Graphics2D g;

    public SwingPainter(Graphics2D g) {
        this(g, new Affine2());
    }

    public SwingPainter(Graphics2D g, Affine2 matrix) {
        super(matrix);
        this.g = g;
    }

    @Override
    public FontStore getFontStore() {
        return SwingFontStore.INSTANCE;
    }

    @Override
    public void setClip(Geometry2D geom) {
        super.setClip(geom);
        if(geom!=null) {
            g.setTransform(IDENTITY);
            g.setClip(AWTUtils.toShape(new TransformedGeometry2D(geom, getFinalTransform())));
        } else {
            g.setClip(null);
        }
    }

    @Override
    public void fill(CharArray text, float x, float y) {
        final Paint fill = toPaint(paint);
        final Composite composite = toComposite(alphaBlending);
        g.setFont(toFont(font));
        g.setComposite(composite);
        g.setPaint(fill);
        g.setTransform(toTransform(getFinalTransform()));
        g.drawString(text.toString(), x, y);
    }

    @Override
    public void fill(Geometry2D geom) {

        final Shape shp = AWTUtils.toShape(geom);
        final Paint fill = toPaint(paint);
        final Composite composite = toComposite(alphaBlending);
        final AffineTransform trs = toTransform(getFinalTransform());

        if(shp!=null && fill!=null){
            g.setTransform(trs);
            g.setComposite(composite);
            g.setPaint(fill);
            g.fill(shp);
        }

    }

    @Override
    public void stroke(Geometry2D geom) {
        final Shape shp = AWTUtils.toShape(geom);
        final Paint fill = toPaint(paint);
        final Stroke stroke = toStroke(brush);
        final Composite composite = toComposite(alphaBlending);
        final AffineTransform trs = toTransform(getFinalTransform());

        if(shp!=null && fill!=null){
            g.setTransform(trs);
            g.setComposite(composite);
            g.setPaint(fill);
            g.setStroke(stroke);
            g.draw(shp);
        }
    }

    @Override
    public void paint(Image image, Affine2 transform) {
        final BufferedImage img = toImage(image);
        final Composite composite = toComposite(alphaBlending);
        AffineTransform trs = toTransform(transform);
        if(trs==null) trs = new AffineTransform();

        if(img!=null){
            g.setTransform(toTransform(getFinalTransform()));
            g.setComposite(composite);
            g.drawImage(img, trs, null);
        }
    }

    @Override
    public Painter2D derivate(Affine2 transform) {
        Affine2 m = (Affine2) getFinalTransform().multiply(transform);
        return new SwingPainter(g, m);
    }

    @Override
    public void dispose() {
        g.dispose();
    }

    private Paint toPaint(science.unlicense.api.painter2d.Paint paint){
        if (paint==null) return null;

        Paint p;
        if (paint instanceof ColorPaint) {
            p = toColor( ((ColorPaint)paint).getColor());
        } else if (paint instanceof LinearGradientPaint) {
            final LinearGradientPaint lgp = (LinearGradientPaint)paint;
            final science.unlicense.api.color.Color[] colors = lgp.getColors();
            final Color[] array = new Color[colors.length];
            for(int i=0;i<array.length;i++) array[i] = toColor(colors[i]);

            return new java.awt.LinearGradientPaint(
                    new Point2D.Double(lgp.getStartX(), lgp.getStartY()),
                    new Point2D.Double(lgp.getEndX(),lgp.getEndY()),
                    Arrays.reformatToFloat(lgp.getPositions()),
                    array);
        } else if (paint instanceof RadialGradientPaint) {
            p = new Color(((RadialGradientPaint)paint).getColors()[0].toARGB());
        } else {
            throw new InvalidArgumentException("Unexpected type : "+paint.getClass());
        }
        return p;
    }

    private Stroke toStroke(Brush brush){
        if (brush instanceof PlainBrush) {
            final PlainBrush b = (PlainBrush) this.brush;
            return new BasicStroke(b.getWidth());
        } else {
            throw new InvalidArgumentException("Unexpected type : "+brush.getClass());
        }
    }

    private Composite toComposite(AlphaBlending blending){
        if (blending==null) return null;

        final float alpha = blending.getAlpha();
        final int type = blending.getType();
        //AlphaBlending.DST
        final int g2dtype;
        if(type==AlphaBlending.CLEAR)           g2dtype = AlphaComposite.CLEAR;
        else if(type==AlphaBlending.XOR)        g2dtype = AlphaComposite.XOR;
        else if(type==AlphaBlending.DST)        g2dtype = AlphaComposite.DST;
        else if(type==AlphaBlending.DST_ATOP)   g2dtype = AlphaComposite.DST_ATOP;
        else if(type==AlphaBlending.DST_IN)     g2dtype = AlphaComposite.DST_IN;
        else if(type==AlphaBlending.DST_OUT)    g2dtype = AlphaComposite.DST_OUT;
        else if(type==AlphaBlending.DST_OVER)   g2dtype = AlphaComposite.DST_OVER;
        else if(type==AlphaBlending.SRC)        g2dtype = AlphaComposite.SRC;
        else if(type==AlphaBlending.SRC_ATOP)   g2dtype = AlphaComposite.SRC_ATOP;
        else if(type==AlphaBlending.SRC_IN)     g2dtype = AlphaComposite.SRC_IN;
        else if(type==AlphaBlending.SRC_OUT)    g2dtype = AlphaComposite.SRC_OUT;
        else if(type==AlphaBlending.SRC_OVER)   g2dtype = AlphaComposite.SRC_OVER;
        else throw new InvalidArgumentException("Unexpected type : "+type);

        return AlphaComposite.getInstance(g2dtype, alpha);
    }

    private BufferedImage toImage(Image image){
        if (image==null) return null;

        //format can not be supported directly in opengl.
        //convert it to a RGBA buffer.
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final PixelBuffer pb = image.getColorModel().asTupleBuffer(image);
        final int[] coord = new int[2];

        final BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (int y = 0; y <height; y++) {
            coord[1] = y;
            for (int x = 0; x < width; x++) {
                coord[0] = x;
                bi.setRGB(x, y, pb.getARGB(coord));
            }
        }

        return bi;
    }

    public static AffineTransform toTransform(Affine2 trs){
        if (trs==null) return null;

        return new AffineTransform(
                         trs.get(0, 0), trs.get(1, 0),
                         trs.get(0, 1), trs.get(1, 1),
                         trs.get(0, 2), trs.get(1, 2));
    }

    public static Affine2 toMatrix(AffineTransform trs){
        if (trs==null) return null;

        return new Affine2(
                trs.getScaleX(), trs.getShearX(), trs.getTranslateX(),
                trs.getShearY(), trs.getScaleY(), trs.getTranslateY());
    }

    private Color toColor(science.unlicense.api.color.Color color) {
        return new Color(color.toARGB(),true);
    }

    private Font toFont(science.unlicense.api.painter2d.Font font) {
        final String family = font.getFamilies()[0].toString();
        final int weight = font.getWeight();
        int fw = Font.PLAIN;
        if(weight==science.unlicense.api.painter2d.Font.WEIGHT_BOLD){
            fw = Font.BOLD;
        }
        return new Font(family, fw, (int)font.getSize());
    }

}