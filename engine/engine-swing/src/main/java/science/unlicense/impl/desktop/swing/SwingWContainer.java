
package science.unlicense.impl.desktop.swing;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import javax.swing.JComponent;
import javax.swing.RepaintManager;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.DragAndDropMessage;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.engine.ui.widget.Widgets;

/**
 *
 * @author Johann Sorel
 */
public class SwingWContainer extends JComponent {

    final WContainer container = new WContainer();

    public SwingWContainer(){

        container.addEventListener(new PropertyPredicate(WContainer.PROPERTY_DIRTY), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                BBox bbox = (BBox) ((PropertyMessage)event.getMessage()).getNewValue();
                double x = bbox.getMin(0)+getWidth()/2.0;
                double y = bbox.getMin(1)+getHeight()/2.0;
                double w = bbox.getSpan(0);
                double h = bbox.getSpan(1);
                repaint((int)x,(int)y, (int)(w+0.5), (int)(h+0.5));
            }
        });
        container.getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint:@color-background}"));

        setFocusable(true);

        final EventAdaptor adaptor = new EventAdaptor();
        addComponentListener(adaptor);
        addMouseListener(adaptor);
        addMouseMotionListener(adaptor);
        addMouseWheelListener(adaptor);
        addKeyListener(adaptor);

    }

    @Override
    protected synchronized void paintComponent(Graphics g) {
        super.paintComponent(g);
        final Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        final double trsX = getWidth()/2.0;
        final double trsY = getHeight()/2.0;
        
        Rectangle clip = g2d.getClipBounds();
        if(clip==null) clip = getBounds();

        final BBox box = new BBox(2);
        box.setRange(0, clip.x-trsX, clip.x+clip.width-trsX);
        box.setRange(1, clip.y-trsY, clip.y+clip.height-trsY);
        
        final AffineTransform trs = new AffineTransform(g2d.getTransform());
        trs.translate(trsX,trsY);
        
        final SwingPainter painter = new SwingPainter(g2d, SwingPainter.toMatrix(trs));
        ViewRenderLoop.render(container, painter, box);
    }

    private static Tuple getMousePosition(MouseEvent e) {
        return new DefaultTuple(
                e.getX()-e.getComponent().getWidth()/2.0,
                e.getY()-e.getComponent().getHeight()/2.0);
    }

    private static Tuple getScreenPosition(MouseEvent e) {
        return new DefaultTuple(e.getXOnScreen(), e.getYOnScreen());
    }

    private static int getMouseButton(MouseEvent e) {
        int button = e.getButton();
        if (button==MouseEvent.BUTTON1) {
            return MouseMessage.BUTTON_1;
        } else if (button==MouseEvent.BUTTON2) {
            return MouseMessage.BUTTON_2;
        } else if (button==MouseEvent.BUTTON3) {
            return MouseMessage.BUTTON_3;
        } else {
            //todo
            return MouseMessage.BUTTON_1;
        }
    }

    private class EventAdaptor implements MouseListener, MouseMotionListener, MouseWheelListener, ComponentListener, KeyListener {

        @Override
        public void componentResized(ComponentEvent e) {
            container.setEffectiveExtent(new Extent.Long(getWidth(), getHeight()));
        }

        @Override
        public void componentMoved(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_MOVE,
                    0, 0, getMousePosition(e), getScreenPosition(e), 0, true);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_MOVE,
                    0, 0, getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_TYPED,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mousePressed(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_PRESS,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            final Tuple mousePos = getMousePosition(e);
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_RELEASE,
                    getMouseButton(e), e.getClickCount(), mousePos, getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));

            final UIFrame frame = SwingWContainer.this.container.getFrame();
            final Widget focused = frame.getFocusedWidget();
            //if we have a focused widget and last event was a drag consumed then
            //we only forward the event to this widget
            final Tuple inWidgetPosition = mousePos;
//            if(isFocusedDragging){
//                inWidgetPosition = Widgets.convertEvent(focused, event);
//            }else{
//                inWidgetPosition = transposeToWidget(e.getPoint(),e,null);
//            }
//
//            isFocusedDragging = focused!=null && e.isDragging() && e.isConsumed();

            //handle ddrag & drops events
            final Sequence drops = science.unlicense.system.System.get().getDragAndDrapBag().getAttachments();
            if(!drops.isEmpty()){
                if(message.getType()==MouseMessage.TYPE_RELEASE){
                    final DragAndDropMessage dropEvent = new DragAndDropMessage(DragAndDropMessage.TYPE_DROP);
                    //check for drag & drop objects
                    final Sequence stack = Widgets.pruneStackAt(container, inWidgetPosition.getX(), inWidgetPosition.getY());
                    //search for the first widget to accept the drop
                    for(int i=stack.getSize()-1;i>=0;i--){
                        final Widget comp = (Widget) stack.get(i);
                        comp.receiveEvent(new Event(null,dropEvent));
                        if(dropEvent.isConsumed()){
                            break;
                        }
                    }
                    //remove all drop requests consumed or not
                    drops.removeAll();

                }else if(message.getType()==MouseMessage.TYPE_MOVE){
                    //TODO sen drag&drop enter/exit events
                }
            }

        }

        @Override
        public void mouseEntered(MouseEvent e) {
            requestFocus();
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_ENTER,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_EXIT,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), 0, false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            final MouseMessage message = new MouseMessage(MouseMessage.TYPE_WHEEL,
                    getMouseButton(e), e.getClickCount(), getMousePosition(e), getScreenPosition(e), -e.getWheelRotation(), false);
            container.receiveEvent(new Event(null, message));
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            //send the keyboard event only to the focused widget
            final UIFrame frame = container.getFrame();
            if (frame==null) return;
            final Widget focusedWidget = frame.getFocusedWidget();
            if (focusedWidget==null) return;
            final KeyMessage message = new KeyMessage(KeyMessage.TYPE_PRESS,e.getKeyChar(),toUnKeyCode(e.getKeyCode()));
            focusedWidget.receiveEvent(new Event(null, message));
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //send the keyboard event only to the focused widget
            final UIFrame frame = container.getFrame();
            if (frame==null) return;
            final Widget focusedWidget = frame.getFocusedWidget();
            if (focusedWidget==null) return;
            final KeyMessage message = new KeyMessage(KeyMessage.TYPE_RELEASE,e.getKeyChar(),toUnKeyCode(e.getKeyCode()));
            focusedWidget.receiveEvent(new Event(null, message));
        }

    }

    private static int toUnKeyCode(int keyCode){
        switch(keyCode){
            case KeyEvent.VK_BEGIN :        return KeyMessage.KC_BEGIN;
            case KeyEvent.VK_END :          return KeyMessage.KC_END;
            case KeyEvent.VK_ENTER :        return KeyMessage.KC_ENTER;
            case KeyEvent.VK_DELETE :       return KeyMessage.KC_DELETE;
            case KeyEvent.VK_TAB :          return KeyMessage.KC_TAB;
            case KeyEvent.VK_CONTROL :      return KeyMessage.KC_CONTROL;
            case KeyEvent.VK_ALT :          return KeyMessage.KC_ALT;
            case KeyEvent.VK_SHIFT :        return KeyMessage.KC_SHIFT;
            case KeyEvent.VK_BACK_SPACE :   return KeyMessage.KC_BACKSPACE;
            case KeyEvent.VK_LEFT :         return KeyMessage.KC_LEFT;
            case KeyEvent.VK_UP :           return KeyMessage.KC_UP;
            case KeyEvent.VK_RIGHT :        return KeyMessage.KC_RIGHT;
            case KeyEvent.VK_DOWN :         return KeyMessage.KC_DOWN;
            default: return 0;
        }
    }

}