
package science.unlicense.impl.desktop.swing;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Rectangle2D;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.painter2d.DerivateFontMetadata;
import science.unlicense.api.painter2d.FontMetadata;

/**
 *
 * @author Johann Sorel
 */
public class SwingFontMetadata implements FontMetadata {

    private final Font font;
    private final FontMetrics metrics;

    public SwingFontMetadata(Font font) {
        this.font = font;
        this.metrics = SwingFontStore.GRAPHICS.getFontMetrics(font);
    }
    
    @Override
    public BBox getGlyphBox() {
        final Rectangle2D rect = font.getMaxCharBounds(SwingFontStore.CTX);
        final BBox bbox = new BBox(2);
        bbox.setRange(0, rect.getMinX(), rect.getMaxX());
        bbox.setRange(1, rect.getMinX(), rect.getMaxX());
        return bbox;
    }

    @Override
    public double getAscent() {
        return metrics.getAscent();
    }

    @Override
    public double getDescent() {
        return metrics.getDescent();
    }

    @Override
    public double getLineGap() {
        return metrics.getMaxDecent();
    }

    @Override
    public double getAdvanceWidthMax() {
        return metrics.getMaxAdvance();
    }

    @Override
    public double getAdvanceWidth(int unicode) {
        return metrics.charWidth(unicode);
    }

    @Override
    public double getAdvanceWidth(Char c) {
        return getAdvanceWidth(c.toUnicode());
    }

    @Override
    public double getMinLeftSideBearing() {
        return 0;
    }

    @Override
    public double getMinRightSideBearing() {
        return 0;
    }

    @Override
    public double getXMaxExtent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BBox getCharBox(Char c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BBox getCharsBox(CharArray text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FontMetadata derivate(double fontSize) {
        return new DerivateFontMetadata(this, fontSize);
    }
    
}
