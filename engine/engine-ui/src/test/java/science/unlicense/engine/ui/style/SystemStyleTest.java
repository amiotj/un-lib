
package science.unlicense.engine.ui.style;

import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import org.junit.Assert;
import org.junit.Test;

/**
 * System style test.
 * 
 * @author Johann Sorel
 */
public class SystemStyleTest {
 
    @Test
    public void defaultTest() {
        
        final WStyle style = SystemStyle.INSTANCE;
        Assert.assertEquals(46,style.getRules().getSize());
        
    }
    
}
