
package science.unlicense.engine.ui.style;

import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.style.ShapeStyle;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.api.layout.Margin;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.predicate.Constant;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class WidgetStylesTest {

    private static final double DELTA = 0.0000001;

    private static final Chars PROP_TEST = new Chars("test");
    
    @Test
    public void readShapeStyleTest(){
        final Document doc = new DefaultDocument();
        doc.getField(Widget.STYLE_PROP_RADIUS).setValue(new Constant(3));
        doc.getField(Widget.STYLE_PROP_BRUSH).setValue(new Constant(new PlainBrush(3, PlainBrush.LINECAP_ROUND)));
        doc.getField(Widget.STYLE_PROP_BRUSH_PAINT).setValue(new Constant(new ColorPaint(Color.GREEN)));
        doc.getField(Widget.STYLE_PROP_FILL_PAINT).setValue(new Constant(new ColorPaint(Color.BLUE)));
        doc.getField(Widget.STYLE_PROP_MARGIN).setValue(new Constant(6));
        
        final WSpace lbl = new WSpace(new Extent.Double(2));
        final WStyle style = lbl.getStyle();
        style.getSelfRule().getField(PROP_TEST).setValue(doc);
        
        final ShapeStyle[] shapeStyles = WidgetStyles.readShapeStyle(style, PROP_TEST);
        Assert.assertEquals(1,shapeStyles.length);
        final ShapeStyle ss = shapeStyles[0];
        Assert.assertEquals(new PlainBrush(3, PlainBrush.LINECAP_ROUND),ss.getBrush());
        Assert.assertEquals(new ColorPaint(Color.GREEN),ss.getBrushPaint());
        Assert.assertEquals(new ColorPaint(Color.BLUE),ss.getFillPaint());
        Assert.assertEquals(new Margin(6, 6, 6, 6),ss.getMargin());
    }

    @Test
    public void StyleFilterTest(){
        final StyleDocument doc = new StyleDocument();
        doc.setProperties(new Chars("filter:$Enable"));

        final WLabel lbl = new WLabel();
        Assert.assertEquals(Boolean.TRUE,doc.getFilter().evaluate(lbl));

        lbl.setEnable(false);
        Assert.assertEquals(Boolean.FALSE,doc.getFilter().evaluate(lbl));
    }
    
    @Test
    public void mergeSimpleNoCreateTest(){
        final Chars prop1 = new Chars("prop1");
        final Chars prop2 = new Chars("prop2");
        final Chars prop3 = new Chars("prop3");
        
        final Document base = new DefaultDocument();
        final Document diff = new DefaultDocument();
        base.getField(prop1).setValue(1);
        base.getField(prop3).setValue(5);
        diff.getField(prop2).setValue(3);
        diff.getField(prop3).setValue(10);
        
        final Document res = WidgetStyles.mergeDoc(base, diff, false);
        Assert.assertTrue(res == base);
        Assert.assertEquals((Integer)1,res.getField(prop1).getValue());
        Assert.assertEquals((Integer)3,res.getField(prop2).getValue());
        Assert.assertEquals((Integer)10,res.getField(prop3).getValue());
    }
    
    @Test
    public void mergeSimpleCreateTest(){
        final Chars prop1 = new Chars("prop1");
        final Chars prop2 = new Chars("prop2");
        final Chars prop3 = new Chars("prop3");
        
        final Document base = new DefaultDocument();
        final Document diff = new DefaultDocument();
        base.getField(prop1).setValue(1);
        base.getField(prop3).setValue(5);
        diff.getField(prop2).setValue(3);
        diff.getField(prop3).setValue(10);
        
        final Document res = WidgetStyles.mergeDoc(base, diff, true);
        Assert.assertTrue(res != base);
        Assert.assertEquals((Integer)1,res.getField(prop1).getValue());
        Assert.assertEquals((Integer)3,res.getField(prop2).getValue());
        Assert.assertEquals((Integer)10,res.getField(prop3).getValue());
    }
    
    @Test
    public void mergeCreateTest(){
        final Chars prop = new Chars("prop");
        final Chars prop1 = new Chars("prop1");
        final Chars prop2 = new Chars("prop2");
        final Chars prop3 = new Chars("prop3");
        
        final Document base = new DefaultDocument();
        final Document baseSub = new DefaultDocument();
        final Document diff = new DefaultDocument();
        final Document diffSub = new DefaultDocument();
        baseSub.getField(prop1).setValue(1);
        baseSub.getField(prop3).setValue(5);
        diffSub.getField(prop2).setValue(3);
        diffSub.getField(prop3).setValue(10);
        
        base.getField(prop).setValue(baseSub);
        diff.getField(prop).setValue(diffSub);
        
        final Document res = WidgetStyles.mergeDoc(base, diff, true);
        Assert.assertTrue(res != base);
        final Document resSub = (Document) res.getField(prop).getValue();
        
        Assert.assertEquals((Integer)1,resSub.getField(prop1).getValue());
        Assert.assertEquals((Integer)3,resSub.getField(prop2).getValue());
        Assert.assertEquals((Integer)10,resSub.getField(prop3).getValue());
    }

    @Test
    public void marginTest(){
        final WSpace lbl = new WSpace();
        final WStyle style = lbl.getStyle();
        style.getSelfRule().setProperties(new Chars("margin:6"));

        Extents extents = lbl.getExtents(null, null);
        Assert.assertEquals(12,extents.minX,DELTA);
        Assert.assertEquals(12,extents.minY,DELTA);
        Assert.assertEquals(12,extents.bestX,DELTA);
        Assert.assertEquals(12,extents.bestY,DELTA);
        Assert.assertEquals(Double.POSITIVE_INFINITY,extents.maxX,DELTA);
        Assert.assertEquals(Double.POSITIVE_INFINITY,extents.maxY,DELTA);
    }

}
