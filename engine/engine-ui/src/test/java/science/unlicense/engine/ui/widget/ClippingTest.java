
package science.unlicense.engine.ui.widget;

import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WContainer;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.engine.ui.MockPainter2D;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.visual.WidgetView;

/**
 * Test the clipping is properly set when rendering widgets.
 *
 * @author Johann Sorel
 */
public class ClippingTest {

    @Test
    public void testClipping(){

        final WContainer container = new WContainer(new GridLayout(2, 2));
        final WCounter c1 = new WCounter();
        final WCounter c2 = new WCounter();
        final WCounter c3 = new WCounter();
        final WCounter c4 = new WCounter();
        container.getChildren().add(c1);
        container.getChildren().add(c2);
        container.getChildren().add(c3);
        container.getChildren().add(c4);
        container.setEffectiveExtent(new Extent.Double(100, 100));
        container.getNodeTransform().setToTranslation(new double[]{50,50});

        //paint everything
        final Painter2D painter = new MockPainter2D();
        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{100,100}));
        Assert.assertEquals(1, c1.counter); c1.counter=0;
        Assert.assertEquals(1, c2.counter); c2.counter=0;
        Assert.assertEquals(1, c3.counter); c3.counter=0;
        Assert.assertEquals(1, c4.counter); c4.counter=0;

        //paint a part in the middle, intersecting all children
        ViewRenderLoop.render(container, painter, new BBox(new double[]{25,25}, new double[]{75,75}));
        Assert.assertEquals(1, c1.counter); c1.counter=0;
        Assert.assertEquals(1, c2.counter); c2.counter=0;
        Assert.assertEquals(1, c3.counter); c3.counter=0;
        Assert.assertEquals(1, c4.counter); c4.counter=0;

        //paint only the 2 widgets on the right
        ViewRenderLoop.render(container, painter, new BBox(new double[]{50,0}, new double[]{100,100}));
        Assert.assertEquals(0, c1.counter); c1.counter=0;
        Assert.assertEquals(1, c2.counter); c2.counter=0;
        Assert.assertEquals(0, c3.counter); c3.counter=0;
        Assert.assertEquals(1, c4.counter); c4.counter=0;

    }

    private static class WCounter extends WSpace{

        private int counter = 0;

        public WCounter() {
            super(new Extent.Double(1, 1));
            setView(new WidgetView(this){
                protected void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox) {
                    super.renderSelf(painter, dirtyArea, innerBBox);
                     counter++;
                }
            });
        }

    }

}
