
package science.unlicense.engine.ui.widget;

import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpace;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.AbsoluteLayout;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class MouseEventTest {
    
    @Test
    public void receiveEventTest(){
        final Listener listener = new Listener();
        
        final WContainer c = new WContainer();
        c.addEventListener(MouseMessage.PREDICATE, listener);

        Tuple position = new Vector(0, 0);
        Tuple screenposition = new Vector(0, 0);
        c.receiveEvent(new Event(null, new MouseMessage(MouseMessage.TYPE_TYPED, MouseMessage.BUTTON_1, 1, position, screenposition, 0, false)));
        
        Assert.assertEquals(1,listener.events.getSize());
        
    }
    
    @Test
    public void childReceiveEventTest(){
        final Listener listener = new Listener();
        
        final WContainer c = new WContainer(new GridLayout(2, 2));
        c.setEffectiveExtent(new Extent.Double(20,20));
        c.getNodeTransform().setToTranslation(new double[]{10,10});
        
        final WSpace space1 = new WSpace(new Extent.Double(10, 10));
        final WSpace space2 = new WSpace(new Extent.Double(10, 10));
        final WSpace space3 = new WSpace(new Extent.Double(10, 10));
        final WSpace space4 = new WSpace(new Extent.Double(10, 10));
        c.getChildren().add(space1);
        c.getChildren().add(space2);
        c.getChildren().add(space3);
        c.getChildren().add(space4);
        space1.addEventListener(MouseMessage.PREDICATE, listener);

        Tuple position = new Vector(-5, -5);
        Tuple screenposition = new Vector(0, 0);
        c.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_TYPED, MouseMessage.BUTTON_1, 1, position, screenposition, 0, false)));
        
        //mouse enter + click
        Assert.assertEquals(2,listener.events.getSize());
        
    }
    
    @Test
    public void mouseEnterExitTest(){
        final Listener l1 = new Listener();
        final Listener l2 = new Listener();
        final Listener l3 = new Listener();
        
        final WContainer c1 = new WContainer(new AbsoluteLayout());
        c1.setEffectiveExtent(new Extent.Double(30,30));
        c1.addEventListener(MouseMessage.PREDICATE, l1);
        
        final WContainer c2 = new WContainer(new AbsoluteLayout());
        c2.setEffectiveExtent(new Extent.Double(10,10));
        c2.addEventListener(MouseMessage.PREDICATE, l2);
        
        final WSpace c3 = new WSpace(new Extent.Double(10, 10));
        c3.setEffectiveExtent(new Extent.Double(6, 6));
        c3.addEventListener(MouseMessage.PREDICATE, l3);
        
        c1.getChildren().add(c2);
        c2.getChildren().add(c3);
        
        TupleRW position = new Vector(0, 0);
        TupleRW screenposition = new Vector(0, 0);
        c1.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_MOVE, 0, -1, position, screenposition, 0, false)));
        
        Assert.assertEquals(1, l1.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage)l1.events.get(0)).getType());
        Assert.assertEquals(2, l2.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_ENTER, ((MouseMessage)l2.events.get(0)).getType());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage)l2.events.get(1)).getType());
        Assert.assertEquals(2, l3.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_ENTER, ((MouseMessage)l3.events.get(0)).getType());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage)l3.events.get(1)).getType());
        
        l1.events.removeAll();
        l2.events.removeAll();
        l3.events.removeAll();
        
        position.setXY(-100, -100);
        screenposition.setXY(-100, -100);
        c1.receiveEvent(new Event(null,new MouseMessage(MouseMessage.TYPE_MOVE, 0, -1, position, screenposition, 0, false)));
        
        Assert.assertEquals(1, l1.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_MOVE, ((MouseMessage)l1.events.get(0)).getType());
        Assert.assertEquals(1, l2.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_EXIT, ((MouseMessage)l2.events.get(0)).getType());
        Assert.assertEquals(1, l3.events.getSize());
        Assert.assertEquals(MouseMessage.TYPE_EXIT, ((MouseMessage)l3.events.get(0)).getType());
        
        
    }
    
    
    private static class Listener implements EventListener{

        private final Sequence events = new ArraySequence();
        
        @Override
        public void receiveEvent(Event event) {
            if(event.getMessage() instanceof MouseMessage){
                events.add(event.getMessage());
            }
        }
        
    }
    
}
