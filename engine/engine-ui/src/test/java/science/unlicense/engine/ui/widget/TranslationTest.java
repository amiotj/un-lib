

package science.unlicense.engine.ui.widget;

import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.DefaultLChars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.country.Country;
import org.junit.Assert;
import science.unlicense.api.geometry.Extent;

/**
 * Test automatic interface translation and update.
 * 
 * @author Johann Sorel
 */
public class TranslationTest {
    
    @Test
    public void testLabel(){
        
        final Dictionary trs = new HashDictionary();
        trs.add(Country.GBR.asLanguage(), new Chars("Hello world!"));
        trs.add(Country.FRA.asLanguage(), new Chars("Bonjour le monde!"));
        final LChars text = new DefaultLChars(trs, Country.GBR.asLanguage());        
        final WLabel label = new WLabel(text);
        
        //sanity test
        Assert.assertEquals(Country.GBR.asLanguage(),label.getLanguage());
        final Extent gbrExt = label.getExtents().getBest(null);
        
        //check property is set
        label.setLanguage(Country.FRA.asLanguage(), true);
        Assert.assertEquals(Country.FRA.asLanguage(),label.getLanguage());
        
        //check best size has changed
        final Extent fraExt = label.getExtents().getBest(null);
        Assert.assertTrue(gbrExt.get(0) < fraExt.get(0));
        
    }
    
    @Test
    public void testRecursive(){
        
        final WContainer parent = new WContainer();
        final WLabel lbl1 = new WLabel();
        final WLabel lbl2 = new WLabel();
        parent.getChildren().add(lbl1);
        parent.getChildren().add(lbl2);
        
        //sanity test
        Assert.assertEquals(Country.GBR.asLanguage(),parent.getLanguage());
        Assert.assertEquals(Country.GBR.asLanguage(),lbl1.getLanguage());
        Assert.assertEquals(Country.GBR.asLanguage(),lbl2.getLanguage());
        
        //check recursive
        parent.setLanguage(Country.FRA.asLanguage(), true);
        Assert.assertEquals(Country.FRA.asLanguage(),parent.getLanguage());
        Assert.assertEquals(Country.FRA.asLanguage(),lbl1.getLanguage());
        Assert.assertEquals(Country.FRA.asLanguage(),lbl2.getLanguage());
        
        //check not recursive
        parent.setLanguage(Country.GBR.asLanguage(), false);
        Assert.assertEquals(Country.GBR.asLanguage(),parent.getLanguage());
        Assert.assertEquals(Country.FRA.asLanguage(),lbl1.getLanguage());
        Assert.assertEquals(Country.FRA.asLanguage(),lbl2.getLanguage());
        
        
    }
    
}
