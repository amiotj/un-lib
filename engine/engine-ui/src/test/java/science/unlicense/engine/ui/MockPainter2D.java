

package science.unlicense.engine.ui;

import science.unlicense.api.painter2d.AbstractPainter2D;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.api.image.Image;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.font.ttf.TTFFontStore;

/**
 * Fake painter2D used for tests.
 * 
 * @author Johann Sorel
 */
public class MockPainter2D extends AbstractPainter2D{

    public MockPainter2D() {
    }

    public MockPainter2D(Affine2 trs) {
        super(trs);
    }

    @Override
    public void fill(Geometry2D geom) {

    }

    @Override
    public void stroke(Geometry2D geom) {

    }

    @Override
    public void paint(Image image, Affine2 transform) {

    }

    @Override
    public void dispose() {
        
    }

    @Override
    public FontStore getFontStore() {
        return TTFFontStore.INSTANCE;
    }

}
