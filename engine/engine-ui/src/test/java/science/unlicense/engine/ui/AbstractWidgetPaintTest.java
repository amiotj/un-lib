

package science.unlicense.engine.ui;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Ignore;
import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.engine.ui.visual.ViewRenderLoop;

/**
 * Test widget painting.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractWidgetPaintTest {
   
    protected abstract ImagePainter2D createPainter(int width,int height);
    
    @Ignore
    @Test
    public void sanityTest(){
        
        final ImagePainter2D painter = createPainter(20, 20);
        
        //build a scene devided in 4 squares
        final WContainer container = new WContainer();
        container.setLayout(new GridLayout(2, 2));
        final WContainer upperLeft = new WContainer();
        final WContainer upperRight = new WContainer();
        final WContainer bottomLeft = new WContainer();
        container.getChildren().add(upperLeft);
        container.getChildren().add(upperRight);
        container.getChildren().add(bottomLeft);
        
//        upperLeft.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.RED)));
//        upperRight.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.GREEN)));
//        bottomLeft.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.BLUE)));
//        container.getStyle().getClass(WContainer.STYLE_ENABLE)
//                .setProperty(WContainer.STYLE_PROP_BACKGROUND, new Constant(new ColorPaint(Color.BLACK)));
        
        //render the widgets scene
        container.setEffectiveExtent(new Extent.Double(20, 20));
        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{20,20}));
        painter.flush();
        
        //check the image content
        final Image image = painter.getImage();
        final PixelBuffer sm = image.getColorModel().asTupleBuffer(image);
        
        final Color c1 = sm.getColor(new int[]{ 0, 0});
        final Color c2 = sm.getColor(new int[]{10, 0});
        final Color c3 = sm.getColor(new int[]{ 0,10});
        final Color c4 = sm.getColor(new int[]{10,10});
        
        Assert.assertEquals(c1, Color.RED);
        Assert.assertEquals(c2, Color.GREEN);
        Assert.assertEquals(c3, Color.BLUE);
        Assert.assertEquals(c4, Color.BLACK);
        
    }
    
}
