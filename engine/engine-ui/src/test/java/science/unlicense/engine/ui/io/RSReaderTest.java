
package science.unlicense.engine.ui.io;

import science.unlicense.engine.ui.io.EqualsPredicate;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.io.PropertyName;
import science.unlicense.engine.ui.io.RSWriter;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import org.junit.Assert;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.predicate.Constant;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WidgetStyles;

/**
 *
 * @author Johann Sorel
 */
public class RSReaderTest {

    @Test
    public void readDocArrayTest(){
        final Chars text = new Chars(
                "{ base : {"
              + "filter : true,"
              + "multi.1 : {name:'one'},"
              + "multi.2 : {name:'two'},"
              + "multi.4 : {name:'four'}"
              + "} }");
        
        final WStyle style = RSReader.readStyle(text);        
        final StyleDocument rule = (StyleDocument) style.getRules().get(0);
        final Object[] array = WidgetStyles.getFieldValues(rule.getField(new Chars("multi")));
        Assert.assertEquals(5, array.length);
        final Document doc0 = (Document) array[0];
        final Document doc1 = (Document) array[1];
        final Document doc2 = (Document) array[2];
        final Document doc3 = (Document) array[3];
        final Document doc4 = (Document) array[4];
        Assert.assertNull(doc0);
        Assert.assertEquals(new Constant(new Chars("one")), doc1.getField(new Chars("name")).getValue());
        Assert.assertEquals(new Constant(new Chars("two")), doc2.getField(new Chars("name")).getValue());
        Assert.assertNull(doc3);
        Assert.assertEquals(new Constant(new Chars("four")), doc4.getField(new Chars("name")).getValue());
        
    }

    @Test
    public void writeNoneProperty(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : true\n"
              + "border : none\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        final StyleDocument rule = (StyleDocument) style.getRules().get(0);
        final Object[] array = WidgetStyles.getFieldValues(rule.getField(new Chars("border")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof Constant);
        Assert.assertTrue(WidgetStyles.isNone(array[0]));
        
        Assert.assertEquals(text, RSWriter.rulesToChars(style));

    }

    @Test
    public void writeRuleProperty(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : true\n"
              + "border : {\n"
              + "  color : rgba(255 255 255 255)\n"
              + "  }\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        Assert.assertEquals(text, RSWriter.rulesToChars(style));

    }

    @Test
    public void writeFunctionProperty(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : true\n"
              + "border : {\n"
              + "  color : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-main 0 1 1.8) 1 rotateHSL(@color-main 0 1 1.2))\n"
              + "  }\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        Assert.assertEquals(text, RSWriter.rulesToChars(style));
    }

    @Test
    public void readArrayTest(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : true\n"
              + "vals : [1,2,3,4]\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        final StyleDocument rule = (StyleDocument) style.getRules().get(0);
        final Object[] array = WidgetStyles.getFieldValues(rule.getField(new Chars("vals")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof Constant);
        final Object value = ((Constant)array[0]).getValue();
        Assert.assertTrue(value instanceof Sequence);
        final Sequence values = (Sequence) value;
        Assert.assertEquals(4, values.getSize());
        Assert.assertEquals(1, ((Constant)values.get(0)).getValue());
        Assert.assertEquals(2, ((Constant)values.get(1)).getValue());
        Assert.assertEquals(3, ((Constant)values.get(2)).getValue());
        Assert.assertEquals(4, ((Constant)values.get(3)).getValue());

        Assert.assertEquals(text, RSWriter.rulesToChars(style));

    }

    @Test
    public void readStyleTest(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : true\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        final StyleDocument rule = (StyleDocument) style.getRules().get(0);

        Assert.assertEquals(text, RSWriter.rulesToChars(style));

    }

    @Test
    public void readEscapeTest(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : true\n"
              + "val : '\\u0048'\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        final StyleDocument rule = (StyleDocument) style.getRules().get(0);
        final Object[] array = WidgetStyles.getFieldValues(rule.getField(new Chars("val")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof Constant);
        Assert.assertEquals(new Chars("H"), ((Constant)array[0]).getValue());

        //assertEquals(text, rule.toChars());

    }

    @Test
    public void readFilterTest(){
        final Chars text = new Chars(
                "{base : {\n"
              + "filter : $abc = true\n"
              + "}}");

        final WStyle style = RSReader.readStyle(text);
        final StyleDocument rule = (StyleDocument) style.getRules().get(0);
        final Object[] array = WidgetStyles.getFieldValues(rule.getField(new Chars("filter")));
        Assert.assertEquals(1, array.length);
        Assert.assertTrue(array[0] instanceof EqualsPredicate);
        EqualsPredicate predicate = (EqualsPredicate) array[0];
        Assert.assertEquals(new PropertyName(new Chars("abc")), predicate.getExp1());
        Assert.assertEquals(new Constant(true), predicate.getExp2());

        Assert.assertEquals(text, RSWriter.rulesToChars(style));

    }

    /**
     * Test reading properties
     */
    @Test
    public void readPropertiesTest() throws IOException{
//        final Chars text = new Chars(
//                "border-brush-width : 1\n" +
//                "border-brush-linecap : 'round'\n" +
//                "border-radius : [3,1,2]\n" +
//                "|some comments\n"+
//                "border-brush-paint : #FF0000\n" +
//                "byreference1 : @system:font\n" +
//                "|some comments\n"+
//                "|some comments\n"+
//                "byreference2 : @bgcolor\n" +
//                "border-fill-paint : lineargradientfill('%',0,0,0,100,0,#FFFFFF,1,#AAAAAA)");
//
//        final Dictionary properties = WCSParser.readProperties(text);
//
//        Assert.assertEquals(7, properties.getSize());
//
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-width")), new Constant(1d));
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-linecap")), new Constant(new Chars("round")));
//        Assert.assertEquals(properties.getValue(new Chars("border-radius")), new Constant(
//                new ArraySequence( new Object[]{
//                    new Constant(3d),
//                    new Constant(1d),
//                    new Constant(2d)})));
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-paint")), new Constant(new Color(255, 0, 0, 255)));
//        Assert.assertEquals(properties.getValue(new Chars("byreference1")),
//                new ExpressionReference(new Chars("system"), new Chars("font")));
//        Assert.assertEquals(properties.getValue(new Chars("byreference2")),
//                new ExpressionReference(null,new Chars("bgcolor")));
//        //TODO test gradient, not sure of definition yet
    }

    /**
     * Test reading classes
     */
    @Test
    public void readClasses() throws IOException{
//        final Chars text = new Chars(
//                "| some comments\n" +
//                "myclass-enable {\n" +
//                "    border-brush-width : 1\n" +
//                "    | some comments\n"+
//                "    border-brush-linecap : 'round'\n" +
//                "}\n" +
//                "| some comments\n"+
//                "\n" +
//                "myclass-event {\n" +
//                "    | some comments\n"+
//                "    border-brush-paint : #FF0000\n" +
//                "    byreference1 : @system:font\n" +
//                "    byreference2 : @bgcolor\n" +
//                "    | some comments\n"+
//                "}");
//
//        final WStyle style = WCSParser.readStyle(text);
//
//        Dictionary properties = style.getClass(new Chars("myclass-enable")).getLocalProperties();
//        Assert.assertEquals(2, properties.getSize());
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-width")), new Constant(1d));
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-linecap")), new Constant(new Chars("round")));
//
//
//        properties = style.getClass(new Chars("myclass-event")).getLocalProperties();
//        Assert.assertEquals(3, properties.getSize());
//        Assert.assertEquals(properties.getValue(new Chars("border-brush-paint")), new Constant(new Color(255, 0, 0, 255)));
//        Assert.assertEquals(properties.getValue(new Chars("byreference1")),
//                new ExpressionReference(new Chars("system"), new Chars("font")));
//        Assert.assertEquals(properties.getValue(new Chars("byreference2")),
//                new ExpressionReference(null,new Chars("bgcolor")));
    }


}
