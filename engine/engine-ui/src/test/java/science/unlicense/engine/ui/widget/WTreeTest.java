
package science.unlicense.engine.ui.widget;

import science.unlicense.engine.ui.widget.WTree;
import org.junit.Test;
import science.unlicense.api.geometry.Extent;
import org.junit.Assert;
import org.junit.Ignore;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.ui.model.TreeRowModel;

/**
 * Test WTree widget.
 *
 * @author Johann Sorel
 */
public class WTreeTest {

    @Ignore
    @Test
    public void testBestExtent(){

        final Node root = new NoTextNode();
        final TreeRowModel model = new TreeRowModel(root);

        final WTree tree = new WTree();
        tree.setRowModel(model);
        Extent extent = tree.getExtents().getBest(null);
        Assert.assertEquals(new Extent.Double(0, 16), extent);

        //set renderer
        tree.setRowHeight(-1);//dynamic row height
        extent = tree.getExtents().getBest(null);
        Assert.assertEquals(new Extent.Double(100, 30), extent);

        //change model
        final Node child1 = new NoTextNode();
        final Node child2 = new NoTextNode();
        root.getChildren().add(child1);
        root.getChildren().add(child2);

        extent = tree.getExtents().getBest(null);
        Assert.assertEquals(new Extent.Double(100, 30), extent);

        //expand root node
        model.setFold(root, true);
        extent = tree.getExtents().getBest(null);
        Assert.assertEquals(new Extent.Double(112, 90), extent);

        //add another level
        final Node child11 = new NoTextNode();
        final Node child21 = new NoTextNode();
        child1.getChildren().add(child11);
        child2.getChildren().add(child21);
        extent = tree.getExtents().getBest(null);
        Assert.assertEquals(new Extent.Double(112, 90), extent);

        //expand child node
        model.setFold(child1, true);
        model.setFold(child2, true);
        extent = tree.getExtents().getBest(null);
        Assert.assertEquals(new Extent.Double(124, 150), extent);

    }


    private static class NoTextNode extends DefaultNode{

        public NoTextNode() {
            super(true);
        }

        public Chars toChars() {
            return Chars.EMPTY;
        }

    }

}
