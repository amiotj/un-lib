
package science.unlicense.api.desktop;

import science.unlicense.api.desktop.AbstractFrame;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractUIFrame extends AbstractFrame implements UIFrame{

    public AbstractUIFrame(UIFrame parent) {
        super(parent);
    }

}
