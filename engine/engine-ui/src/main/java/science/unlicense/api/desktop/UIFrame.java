
package science.unlicense.api.desktop;

import science.unlicense.api.desktop.Frame;
import science.unlicense.api.character.Chars;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;

/**
 * A UI Frame is Frame with a container panel.
 *
 * @author Johann Sorel
 */
public interface UIFrame extends Frame{

    public static final Chars PROP_FOCUSED_WIDGET = new Chars("FocusedWidget");

    /**
     * Get frame main container.
     * 
     * @return WContainer
     */
    WContainer getContainer();

    /**
     * Get focused widget.
     * 
     * @return Widget, can be null
     */
    Widget getFocusedWidget();
    
    /**
     * Set focused widget.
     * 
     * @param widget can be null.
     */
    void setFocusedWidget(Widget widget);
    
}
