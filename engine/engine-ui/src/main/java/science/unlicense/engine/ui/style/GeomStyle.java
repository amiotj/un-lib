
package science.unlicense.engine.ui.style;

import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.Paint;

/**
 * Define a widget border.
 * 
 * @author Johann Sorel
 */
public class GeomStyle {
        
    protected Brush brush;
    protected Paint brushPaint;
    protected Paint fillPaint;

    public GeomStyle() {
    }

    public GeomStyle(Brush brush, Paint brushPaint, Paint fillPaint) {
        this.brush = brush;
        this.brushPaint = brushPaint;
        this.fillPaint = fillPaint;
    }

    /**
     * Get brush to use for rendering the border shape.
     * @return Brush, can be null
     */
    public Brush getBrush() {
        return brush;
    }

    public void setBrush(Brush brush) {
        this.brush = brush;
    }

    /**
     * Get brush paint to use for rendering the border shape.
     * @return Paint, can be null
     */
    public Paint getBrushPaint() {
        return brushPaint;
    }

    public void setBrushPaint(Paint brushPaint) {
        this.brushPaint = brushPaint;
    }
    
    /**
     * Get the inner paint to fill the border shape.
     * @return Paint, can be null
     */
    public Paint getFillPaint() {
        return fillPaint;
    }

    public void setFillPaint(Paint fillPaint) {
        this.fillPaint = fillPaint;
    }

}
