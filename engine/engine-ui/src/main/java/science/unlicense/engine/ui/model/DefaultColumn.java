

package science.unlicense.engine.ui.model;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WCell;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class DefaultColumn extends AbstractColumn {

    protected final Widget header;
    protected final ObjectPresenter presenter;
    protected int bestWidth = FormLayout.SIZE_AUTO;

    public DefaultColumn(){
        this((Widget)null,null);
    }
    
    public DefaultColumn(Chars text, ObjectPresenter presenter) {
        this(createHeader(text),presenter);
    }
    
    public DefaultColumn(Widget header, ObjectPresenter presenter) {
        if(header==null)header = new WLabel();
        if(presenter==null) presenter = new DefaultObjectPresenter();
        this.header = header;
        this.presenter = presenter;
    }
    
    public Widget getHeader() {
        return header;
    }

    public int getBestWidth() {
        return bestWidth;
    }

    public void setBestWidth(int bestWidth) {
        this.bestWidth = bestWidth;
    }

    public Object getCellValue(Object candidate) {
        return candidate;
    }

    public ObjectPresenter getPresenter() {
        return presenter;
    }

    public WCell createCell(WRow row) {
        return new WCell(row, this);
    }
    
    private static WLabel createHeader(CharArray text){
        final WLabel lbl = new WLabel(text);
        lbl.setVerticalAlignment(WLabel.VALIGN_CENTER);
        lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);
        return lbl;
    }
    
}
