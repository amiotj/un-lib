package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.visual.GraphicGeometryView;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 * Widget displaying a geometry.
 *
 * @author Johann Sorel
 */
public class WGraphicGeometry extends WLeaf{

    /**
     * Property for the widget geometry.
     */
    public static final Chars PROPERTY_GEOMETRY = new Chars("geometry");

    // style classes
    public static final Chars STYLE_ENABLE = new Chars("wgeometry-enable");
    public static final Chars STYLE_DISABLE = new Chars("wgeometry-disable");

    /** Geometry border and fill : BorderStyle class */
    public static final Chars STYLE_GEOM_PREFIX = new Chars("geom");

    public WGraphicGeometry() {
        setView(new GraphicGeometryView(this));
    }

    /**
     * Get geometry.
     * @return Geometry2D, can be null
     */
    public Geometry2D getGeometry() {
        return (Geometry2D)getPropertyValue(PROPERTY_GEOMETRY);
    }

    /**
     * Set geometry.
     * @param geom can be null
     */
    public void setGeometry(Geometry2D geom) {
        setPropertyValue(PROPERTY_GEOMETRY,geom);
    }

    /**
     * Get geometry variable.
     * @return Variable.
     */
    public Variable varGeometry(){
        return getProperty(PROPERTY_GEOMETRY);
    }
    
}
