
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventListener;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.api.predicate.Variable;

/**
 * A two state button.
 *
 * @author Johann Sorel
 */
public class WSwitch extends WButton{

    /**
     * Property for the widget check value.
     */
    public static final Chars PROPERTY_CHECK = WCheckBox.PROPERTY_CHECK;

    public WSwitch() {
    }

    public WSwitch(CharArray text){
        super(text);
    }

    public WSwitch(CharArray text, Widget graphic){
        super(text, graphic);
    }

    public WSwitch(CharArray text, Widget graphic, EventListener lst){
        super(text, graphic, lst);
    }

    public WSwitch(ActionExecutable exec){
        super(exec);
    }

    /**
     * Set check value.
     * @param check
     */
    public void setCheck(boolean check) {
        setPropertyValue(PROPERTY_CHECK,check, Boolean.FALSE);
    }

    /**
     * Get check value.
     * @return boolean
     */
    public boolean isCheck() {
        //test equality, value could be null at first
        return (Boolean)getPropertyValue(PROPERTY_CHECK, Boolean.FALSE);
    }

    /**
     * Get check variable.
     * @return Variable.
     */
    public Variable varCheck(){
        return getProperty(PROPERTY_CHECK);
    }
    
    /**
     * Overload click event to change button state.
     */
    public void doClick() {
        setCheck(!isCheck());
        super.doClick();
    }

}
