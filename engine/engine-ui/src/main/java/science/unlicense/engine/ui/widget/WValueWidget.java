
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;

/**
 * Edition widget such as check boxes, text fields, spinners work and edit a single
 * value. 
 * This is true for primitive types but also for more complex editors like
 * path chooser or color chooser.
 * 
 * It is encouraged to extend this class when a widget follow this model.
 * It is also encouraged to support null values.
 * 
 * @author Johann Sorel
 */
public interface WValueWidget extends Widget{

    /**
     * Property for the widget value change.
     */
    public static final Chars PROPERTY_VALUE = new Chars("Value");
    
    /**
     * Get widget value.
     * 
     * @return value, can be null.
     */
    Object getValue();

    /**
     * Set widget value.
     * 
     * @param value, can be null.
     */
    void setValue(Object value);
    
    /**
     * Get value variable.
     * 
     * @return Variable, never null.
     */
    Variable varValue();
    
}
