
package science.unlicense.engine.ui.model;

import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTreePathPresenter implements TreePathPresenter {

    public Widget createPathWidget(int[] depths) {
        WTreePath path = new WTreePath();
        path.setDepths(depths);
        return path;
    }

    public double getDepthWidth() {
        return WTreePath.DEPTH;
    }

}
