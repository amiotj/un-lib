package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.ui.model.SliderModel;
import science.unlicense.engine.ui.visual.SliderView;

/**
 * @author Johann Sorel
 */
public class WSlider extends AbstractValueWidget{
    
    public static final Chars PROPERTY_MODEL = WRangeSlider.PROPERTY_MODEL;

    public WSlider(SliderModel model) {
        this(model,model.getDefaultValue());
    }

    public WSlider(SliderModel model, Object value) {
        super(false);
        setPropertyValue(PROPERTY_MODEL, model);
        setPropertyValue(PROPERTY_VALUE, value);
        setView(new SliderView(this));
    }

    public SliderModel getModel() {
        return (SliderModel) getPropertyValue(PROPERTY_MODEL);
    }

    public void setModel(SliderModel model) {
        setPropertyValue(PROPERTY_MODEL, model);
    }

    public Tuple getOnSreenPosition(Widget child) {
        throw new UnimplementedException("Not supported.");
    }
    
}
