
package science.unlicense.engine.ui.component;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.model.SpinnerEditor;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.engine.ui.widget.WSpinner;

/**
 * HSL widget color editor.
 * 
 * @author Johann Sorel
 */
public class WRGBColorEditor extends WContainer {
    
    public static final Chars PROPERTY_VALUE = SpinnerEditor.PROPERTY_VALUE;
    
    private final WLabel wRed   = new WLabel(new Chars("R"));
    private final WLabel wGreen = new WLabel(new Chars("G"));
    private final WLabel wBlue  = new WLabel(new Chars("B"));
    private final WSpinner rslider = new WSpinner(new NumberSpinnerModel(Integer.class,0,0,255,1));
    private final WSpinner gslider = new WSpinner(new NumberSpinnerModel(Integer.class,0,0,255,1));
    private final WSpinner bslider = new WSpinner(new NumberSpinnerModel(Integer.class,0,0,255,1));
    private boolean updating = false;
    
    
    public WRGBColorEditor() {
        super(new FormLayout());
        
        //TODO I18N
        addChild(wRed,   FillConstraint.builder().coord(0, 0).build());
        addChild(rslider,FillConstraint.builder().coord(1, 0).build());
        addChild(wGreen, FillConstraint.builder().coord(0, 1).build());
        addChild(gslider,FillConstraint.builder().coord(1, 1).build());
        addChild(wBlue,  FillConstraint.builder().coord(0, 2).build());
        addChild(bslider,FillConstraint.builder().coord(1, 2).build());
        
        final EventListener listener = new EventListener() {
            public void receiveEvent(Event event) {
                if(updating) return;
                setValue(new Color((Integer)rslider.getValue(),(Integer)gslider.getValue(),(Integer)bslider.getValue()));
            }
        };
        
        final Predicate p = new PropertyPredicate(WSlider.PROPERTY_VALUE);
        rslider.addEventListener(p, listener);
        gslider.addEventListener(p, listener);
        bslider.addEventListener(p, listener);
    }
    
    /**
     * Set color value.
     * @param color
     */
    public void setValue(Color color) {
        updating = true;
        rslider.setValue(color.getRed()*255);
        gslider.setValue(color.getGreen()*255);
        bslider.setValue(color.getBlue()*255);
        updating = false;
        setPropertyValue(PROPERTY_VALUE,color);
    }

    /**
     * Get color value.
     * @return Color
     */
    public Color getValue() {
        return (Color)getPropertyValue(PROPERTY_VALUE);
    }
    
}
