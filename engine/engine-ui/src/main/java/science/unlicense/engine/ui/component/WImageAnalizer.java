
package science.unlicense.engine.ui.component;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.image.Image;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTree;

/**
 * View content of an image.
 * Samples and metadatas
 * 
 * @author Johann Sorel
 */
public class WImageAnalizer extends WContainer {

    private final WTree metaTree = new WTree();
    private final WTable dataTable = new WTable();
    private Image image;
    
    public WImageAnalizer() {
        super(new BorderLayout());
        
        final WTabContainer tabs = new WTabContainer();
        tabs.addTab(metaTree, new WLabel(new Chars("Metadatas")));
        tabs.addTab(dataTable, new WLabel(new Chars("Datas")));
        addChild(tabs, BorderConstraint.CENTER);
    }

    public Image getImage(){
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
        update();
    }
    
    private void update(){
        metaTree.setRowModel(new TreeRowModel(new DefaultNode(true)));
        dataTable.setRowModel(new DefaultRowModel());
        
        if(image==null) return;
        
        final int nbCol = (int) image.getExtent().getL(0);
        final int nbRow = (int) image.getExtent().getL(1);
        
        final Sequence rows = new ArraySequence();
        for(int i=0;i<nbRow;i++) rows.add(new ImageRow(i));
        
        
        final Sequence columns = dataTable.getColumns();
        for(int i=0;i<nbCol;i++)columns.add(new ImageColumn(i));
        
        dataTable.setRowModel(new DefaultRowModel(rows));
        
    }
    
    private class ImageRow {
        private int row;

        public ImageRow(int row) {
            this.row = row;
        }
        
        public Chars getSample(int col){
            final Object sample = image.getRawModel().asTupleBuffer(image).getTuple(new int[]{col,row}, null);
            return CObjects.toChars(sample);
        }
    }
    
    private class ImageColumn extends DefaultColumn{
        private int col;

        public ImageColumn(int col) {
            super(new Chars(""+col), null);
            this.col = col;
        }

        public Object getCellValue(Object candidate) {
            return ((ImageRow)candidate).getSample(col);
        }
        
    }
    
}
