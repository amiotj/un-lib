
package science.unlicense.engine.ui.model;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.EventSource;

/**
 * Row model.
 * Used by WList,WComboBox and indirectly by WTable.
 *
 * @author Johann Sorel
 */
public interface RowModel extends EventSource {

    /**
     * Sequence view of the row model.
     * @return
     */
    Sequence asSequence();

    /**
     * Get number of rows in the model.
     * @return number of elements
     */
    int getSize();

    /**
     * Get row value at given index.
     * @param index
     * @return Object
     */
    Object getElement(int index);
    
}
