
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Triplet;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.AbsoluteLayout;
import science.unlicense.api.layout.Layout;
import science.unlicense.api.layout.LayoutConstraint;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.engine.ui.visual.ContainerView;
import static science.unlicense.engine.ui.widget.Widget.PROPERTY_DIRTY;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.TupleRW;

/**
 * Widget containing other widgets.
 *
 * @author Johann Sorel
 */
public class WContainer extends AbstractWidget {

    /**
     * Property indicating when the layout changed.
     */
    public static final Chars PROPERTY_LAYOUT = new Chars("Layout");

    private Layout layout;

    //rendering state
    protected boolean mouseOver = false;
    private volatile boolean updatingLayout = false;
    private volatile boolean updatingContent = false;

    //last mouse in widget
    private HashSet oldMouseWidgets = new HashSet(Hasher.IDENTITY,1);
    private HashSet newMouseWidgets = new HashSet(Hasher.IDENTITY,1);

    private final EventListener layoutListener = new EventListener() {
        public void receiveEvent(Event event) {
            if(updatingContent) return;
            updateLayoutUpdate();
        }
    };
    
    public WContainer() {
        this(new AbsoluteLayout());
    }

    public WContainer(Layout layout) {
        super(true);
        setView(new ContainerView(this));
        setLayout(layout);
        
        getStyle().addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                //margins may have changed
                updateLayoutSize(getInnerExtent());
            }
        });
    }

    public void addChild(Widget child, LayoutConstraint constraint) {
        child.setLayoutConstraint(constraint);
        super.getChildren().add(child);
    }
    
    /**
     * Get container layout.
     * @return Layout never null
     */
    public Layout getLayout() {
        return layout;
    }

    /**
     * Set container layout.
     * @param layout must not be null
     */
    public void setLayout(Layout layout) {
        if(this.layout!=null){
            this.layout.removeEventListener(new PropertyPredicate(Layout.PROPERTY_DIRTY), layoutListener);
            this.layout.setPositionables(null);
        }
        final Layout old = this.layout;
        this.layout = layout;
        this.layout.addEventListener(new PropertyPredicate(Layout.PROPERTY_DIRTY), layoutListener);
        this.layout.setPositionables(children);
        updateLayoutSize(getInnerExtent());
        sendPropertyEvent(this, PROPERTY_LAYOUT, old, layout);
    }

    private void updateLayoutUpdate(){
        startUpdatingLayout();
        layout.update();
        endUpdatingLayout();
    }
    
    protected void startUpdatingLayout(){
        updatingLayout = true;
    }
    
    protected void endUpdatingLayout(){
        updatingLayout = false;
        updateExtents();
        setDirty();
    }
    
    protected void startUpdatingContent(){
        updatingContent = true;
    }
    
    protected void endUpdatingContent(){
        updatingContent = false;
        updateLayoutUpdate();
    }
    
    private void updateLayoutSize(BBox innerExtent){
        //TODO : listen to style changes
        if(layout!=null){
            this.layout.setView(innerExtent);
        }
    }
        
    /**
     * Passthrough event to children.
     *
     * @param event
     */
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        //event comes from the parent
        if(event.getMessage() instanceof MouseMessage){
            final MouseMessage mevent = (MouseMessage) event.getMessage();

            HashSet temp = oldMouseWidgets;
            oldMouseWidgets = newMouseWidgets;
            newMouseWidgets = temp;
            newMouseWidgets.removeAll();
            
            //clip mouse events to forward to correct childrens
            final BBox searchBBox = new BBox((TupleRW)mevent.getMousePosition(), (TupleRW)mevent.getMousePosition());
            
            final Layout layout = getLayout();
            for(Iterator ite=layout.getPositionables(searchBBox);ite.hasNext();){
                final Triplet trio                  = (Triplet) ite.next();
                final Widget child                  = (Widget) trio.getValue1();
                if(Widgets.forwardEvent(this, child, mevent, oldMouseWidgets.remove(child))){
                    newMouseWidgets.add(child);
                }
            }
            
            //send exit event on old widgets
            final Iterator oldIte = oldMouseWidgets.createIterator();
            while(oldIte.hasNext()){
                Widget old = (Widget) oldIte.next();
                Widgets.forwardEvent(this, old, mevent, true);
            }
            oldMouseWidgets.removeAll();
            
        }
    }

    protected void receiveEventChildren(Event event) {
        if(updatingLayout){
            //when updating a layout there may be a lot of such events
            //we wait until update is finished to send a global event
            return;
        }
        
        if(event.getMessage() instanceof PropertyMessage){
            final PropertyMessage pevent = (PropertyMessage) event.getMessage();
            final Widget widget = (Widget) event.getSource();
            
            if(NodeTransform.PROPERTY_MATRIX.equals(pevent.getPropertyName())){
                //child transform changed
                //calculate the dirty area
                final Matrix oldMatrix = (Matrix) pevent.getOldValue();
                final Matrix newMatrix = (Matrix) pevent.getNewValue();
                final BBox bbox = widget.getView().calculateVisualExtent(null);
                final BBox dirty = Geometries.transform(bbox, oldMatrix, null);
                dirty.expand(Geometries.transform(bbox, newMatrix, bbox));
                setDirty(dirty);
                
            }else if(PROPERTY_DIRTY.equals(pevent.getPropertyName())){
                final BBox dirtyExt = (BBox) pevent.getNewValue();                
                if(!widget.getEffectiveExtent().isEmpty()){
                    //convert from child space to this widget coordinate space
                    final Matrix m = widget.getNodeTransform().asMatrix();
                    setDirty(Geometries.transform(dirtyExt, m, null));
                }else{
                    setDirty();
                }
            }else if(PROPERTY_CHILDFOCUSED.equals(pevent.getPropertyName())){
                setChildFocused((Boolean) pevent.getNewValue());
            }else if(PROPERTY_FOCUSED.equals(pevent.getPropertyName())){
                setChildFocused((Boolean) pevent.getNewValue());
            }
        }
    }
    
    public Tuple getOnSreenPosition(Widget child) {
        final Tuple point = getOnSreenPosition();
        if(point==null){
            //not displayed
            return null;
        }

        final NodeTransform trs = child.getNodeTransform();
        final Extent extent = child.getEffectiveExtent();
        final Extent thisExt = getEffectiveExtent();

        if(!extent.isEmpty()){
            double[] position = trs.transform(new double[]{0, 0, 1}, null);
            return new DefaultTuple(
                    point.getX()+((thisExt.get(0)/2.0)+position[0]-extent.get(0)/2.0),
                    point.getY()+((thisExt.get(1)/2.0)+position[1]-extent.get(1)/2.0));

        }else{
            return null;
        }
    }

    @Override
    protected void sendPropertyEvent(EventSource source, Chars name, Object oldValue, Object newValue) {
        super.sendPropertyEvent(source, name, oldValue, newValue);
        if(PROPERTY_EFFECTIVE_EXTENT.equals(name)){
            updateLayoutSize(getInnerExtent(this, (Extent) newValue));
        }
    }

}
