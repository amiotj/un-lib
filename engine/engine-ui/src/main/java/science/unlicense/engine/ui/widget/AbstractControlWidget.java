
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;

/**
 *
 * @author Johann Sorel
 */
public class AbstractControlWidget extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_EDITION_VALID = new Chars("EditionValid");
    
    protected void setEditionValid(boolean valid) {
        setPropertyValue(PROPERTY_EDITION_VALID,valid);
    }

    public boolean isEditionValid() {
        return (Boolean)getPropertyValue(PROPERTY_EDITION_VALID, Boolean.TRUE);
    }
    
    @Override
    public Object getValue() {
        return getPropertyValue(WValueWidget.PROPERTY_VALUE);
    }

    @Override
    public void setValue(Object value) {
        setPropertyValue(WValueWidget.PROPERTY_VALUE, value);
    }

    @Override
    public Variable varValue() {
        return getProperty(WValueWidget.PROPERTY_VALUE);
    }

}
