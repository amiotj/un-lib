
package science.unlicense.engine.ui.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class DefaultTreeColumn extends DefaultColumn {
    
    private final TreePathPresenter treePresenter;
    private final ObjectPresenter wrapPresenter = new WrapPresenter();
    private TreeRowModel treeModel;

    public DefaultTreeColumn() {
        this(null);
    }

    public DefaultTreeColumn(TreePathPresenter treePresenter) {
        this(null,(Widget)null,null);
    }

    public DefaultTreeColumn(TreePathPresenter treePresenter, Chars text, ObjectPresenter presenter) {
        this(null, new WLabel(text), presenter);
    }

    public DefaultTreeColumn(TreePathPresenter treePresenter, Widget header, ObjectPresenter presenter) {
        super(header, presenter);
        this.treePresenter = (treePresenter!=null)? treePresenter : new DefaultTreePathPresenter();
    }

    public TreeRowModel getRowModel() {
        return treeModel;
    }

    public void setRowModel(TreeRowModel treeModel) {
        this.treeModel = treeModel;
    }

    public TreePathPresenter getTreePresenter() {
        return treePresenter;
    }

    public ObjectPresenter getPresenter() {
        return wrapPresenter;
    }
    
    private final class WrapPresenter implements ObjectPresenter {

        public Widget createWidget(Object candidate) {
            
            final Node node = (Node) candidate;
            final int[] depth = getRowModel().getDepth(node);
            final Widget path = treePresenter.createPathWidget(depth);

            // path + action widgets
            final FormLayout layout = new FormLayout();
            layout.setRowSize(0, FormLayout.SIZE_EXPAND);
            layout.setColumnSize(2, FormLayout.SIZE_EXPAND);
            final WContainer struct = new WContainer(layout);
            struct.addChild(path, new FillConstraint(0,0,1,1,true,true,FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER));

            
            if(node.canHaveChildren()){
                struct.addChild(new WFoldButton(treeModel, node), new FillConstraint(1,0,1,1,false,false,FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER));
            }

            // main widget
            final Widget tree = DefaultTreeColumn.this.presenter.createWidget(candidate);
            struct.addChild(tree, new FillConstraint(2,0,1,1,true,true,FillConstraint.HALIGN_CENTER,FillConstraint.VALIGN_CENTER));
            
            return struct;
        }
        
    }

}
