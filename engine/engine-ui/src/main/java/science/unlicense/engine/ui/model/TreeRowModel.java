
package science.unlicense.engine.ui.model;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.MessagePredicate;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.model.tree.ViewNode;
import science.unlicense.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public class TreeRowModel extends AbstractEventSource implements RowModel, EventListener {
    
    public static final Chars PROP_CELLPRESENTER = new Chars("cellpresenter");
    public static final Chars PROP_PATHPRESENTER = new Chars("pathpresenter");
    
    private final Node root;
    private final Predicate filter;
    private Sequence flatView = null;
    private final Dictionary depthView = new HashDictionary(Hasher.IDENTITY);
    private final Set unfolded = new HashSet();
    private final boolean rootVisible;
    
    public TreeRowModel(Node root) {
        this(root,Predicate.TRUE,true);
    }

    public TreeRowModel(Node root,Predicate filter, boolean rootVisible) {
        this.root = root;
        this.filter = filter;
        this.rootVisible = rootVisible;
        this.root.addEventListener(NodeMessage.PREDICATE, this);
    }

    /**
     * Get the tree root node.
     * @return Node
     */
    public Node getRoot() {
        return root;
    }

    public Predicate getFilter() {
        return filter;
    }
    
    /**
     * indicate if given node is open.
     *
     * @param node
     * @return
     */
    public boolean isUnfold(Node node){
        node = ViewNode.unWrap(node);
        return unfolded.contains(node);
    }

    /**
     * Set given node fold state.
     *
     * @param node
     * @param open
     */
    public synchronized void setFold(Node node, boolean open){
        final Node unwrapped = ViewNode.unWrap(node);
        final boolean contains = unfolded.contains(unwrapped);
        if(!open && contains){
            unfolded.remove(unwrapped);
            flatView = null;
            fireNodeClose(node);
            
        }else if(!contains && open){
            unfolded.add(unwrapped);
            flatView = null;
            fireNodeOpen(node);
        }
    }
    
    public synchronized Sequence asSequence() {
        if(flatView==null){
            flatView = new ArraySequence();
            depthView.removeAll();
            calculateNodeRows(getRoot(), new int[0], flatView);
        }
        return flatView;
    }

    public int[] getDepth(Object node){
        return (int[]) depthView.getValue(node);
    }
    
    public int getSize() {
        return asSequence().getSize();
    }

    public Object getElement(int index) {
        return asSequence().get(index);
    }
            
    private void calculateNodeRows(final Node candidate,
            int[] depths, Sequence datas){

        final boolean isRoot = candidate==root;

        if(!isRoot || rootVisible){
            datas.add(candidate);
        }
        depthView.add(candidate, Arrays.copy(depths, new int[depths.length]));

        //loop on children
        if(isUnfold(candidate) || (isRoot && !rootVisible)){
            depths = Arrays.copy(depths);
            if(depths.length>0){
                if(depths[depths.length-1]== WTreePath.TYPE_MIDDLE){
                    depths[depths.length-1] = WTreePath.TYPE_LINE;
                }else if(depths[depths.length-1]== WTreePath.TYPE_MIDDLE){
                    depths[depths.length-1] = WTreePath.TYPE_LINE;
                }else{
                    depths[depths.length-1] = WTreePath.TYPE_EMPTY;
                }
            }

            
            Sequence children = new ArraySequence(candidate.getChildren());
            children = filter==Predicate.TRUE ? children : Collections.filter(children, filter);
            for(int i=0,n=children.getSize();i<n;i++){
                //choose the type of visual node
                final int[] childDepths;
                if(isRoot && !rootVisible){
                    childDepths = new int[0];
                }else{
                    final int intertype = (i==n-1) ?
                        WTreePath.TYPE_END : WTreePath.TYPE_MIDDLE;
                    childDepths = Arrays.insert(depths, depths.length, intertype);
                }
                
                calculateNodeRows((Node) children.get(i), childDepths, datas);
            }
        }
    }
    
    public Node findParent(final Node candidate, final int[] index){
        index[0] = -1;
        final Node[] parent = new Node[1];
        getRoot().accept(new DefaultNodeVisitor(){
            public Object visit(Node node, Object context) {
                if(index[0]>=0) return null;
                final Object[] childs = node.getChildren().toArray();
                for(int i=0;i<childs.length;i++){
                    if(childs[i] == candidate){
                        index[0] = i;
                        parent[0] = node;
                        return null;
                    }
                }
                return super.visit(node, context);
            }

        }, parent);

        return parent[0];
    }
    
    public Class[] getEventClasses() {
        return new Class[]{
            NodeMessage.class,
            TreeRowModel.FoldMessage.class
        };
    }

    public void receiveEvent(Event event) {
        //tree elements has changed, forward event to widget
        if(hasListeners()) getEventManager().sendEvent(event);

        flatView = null;
        getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_REPLACEALL, -1, -1, null, null)));
    }
    
    protected void fireNodeOpen(Node n){
        if(hasListeners()){
            //TODO collection event is wrong, make it better if possible
            getEventManager().sendEvent(new Event(this,new CollectionMessage(CollectionMessage.TYPE_ADD, -1, -1, null, null)));
            getEventManager().sendEvent(new Event(this,new TreeRowModel.FoldMessage(TreeRowModel.FoldMessage.TYPE_FOLD, new Node[0], new Node[]{n})));
        }
    }

    protected void fireNodeClose(Node n){
        if(hasListeners()){
            //TODO collection event is wrong, make it better if possible
            getEventManager().sendEvent(new Event(this, new CollectionMessage(CollectionMessage.TYPE_REMOVE, -1, -1, null, null)));
            getEventManager().sendEvent(new Event(this, new TreeRowModel.FoldMessage(TreeRowModel.FoldMessage.TYPE_FOLD, new Node[]{n}, new Node[0])));
        }
    }
    
    /**
     * Recursivly set fold state of node and it's children.
     * 
     * @param model
     * @param node
     * @param open 
     */
    public static void setFoldRecursive(final TreeRowModel model, Node node, final boolean open){
        node.accept(new DefaultNodeVisitor(){
            @Override
            public Object visit(Node node, Object context) {
                model.setFold(node, open);
                return super.visit(node, context);
                
            }}, null);
    }
    
    /**
     * Tree model event, for selection and fold states.
     */
    public static class FoldMessage extends DefaultEventMessage{

        public static final Predicate PREDICATE = new MessagePredicate(FoldMessage.class);

        public static final int TYPE_SELECTION = 1;
        public static final int TYPE_FOLD = 2;

        private final int type;

        private final Node[] newNodes;
        private final Node[] oldNodes;

        public FoldMessage(int type, Node[] oldNodes, Node[] newNodes) {
            super(false);
            this.type = type;
            this.newNodes = newNodes;
            this.oldNodes = oldNodes;
        }

        public int getType() {
            return type;
        }

        /**
         * Get old nodes.
         * @return Node[] never null
         */
        public Node[] getOldNodes() {
            return Arrays.copy(oldNodes, new Node[oldNodes.length]);
        }

        /**
         * Get new nodes.
         * @return Node[] never null
         */
        public Node[] getNewNodes() {
            return Arrays.copy(newNodes, new Node[newNodes.length]);
        }

    }
    
}
