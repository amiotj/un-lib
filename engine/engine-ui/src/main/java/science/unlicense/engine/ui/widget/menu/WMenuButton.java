
package science.unlicense.engine.ui.widget.menu;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.PairLayout;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Toolbar or Menu button.
 * 
 * @author Johann Sorel
 */
public class WMenuButton extends WButton {

    public WMenuButton() {
        this((CharArray)null);
    }

    public WMenuButton(CharArray text){
        this(text,null);
    }

    public WMenuButton(CharArray text, Widget graphic){
        this(text, graphic, null);
    }

    public WMenuButton(CharArray text, Widget graphic, EventListener lst){
        super(text, graphic, lst);
        setHorizontalAlignment(PairLayout.HALIGN_LEFT);
    }

    public WMenuButton(ActionExecutable exec){
        super(exec);
        setHorizontalAlignment(PairLayout.HALIGN_LEFT);
    }

}
