package science.unlicense.engine.ui.widget;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.Language;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.country.Country;
import science.unlicense.api.desktop.DragAndDropMessage;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.AbstractPositionableNode;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.visual.View;
import science.unlicense.engine.ui.visual.WidgetView;
import static science.unlicense.engine.ui.widget.Widget.PROPERTY_DIRTY;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.event.DefaultProperty;
import science.unlicense.api.event.Property;

/**
 * Base widget class.
 * Extend Widget class to make a container widget.
 * Extend WLeaf class to make a leaf widget which won't have any children.
 *
 * Widgets all share common properties which are :
 * - min/best/max extent
 * - enable state
 * - visible state
 * - tooltip
 * - popup
 * - border
 * - background
 * - style
 *
 * @author Johann Sorel
 */
public abstract class AbstractWidget extends AbstractPositionableNode implements EventListener,Widget {

    private static final Language DEFAULT_LANGUAGE = Country.GBR.asLanguage();

    private final Set flags = new HashSet();
    // properties
    private final Dictionary properties = new HashDictionary();
    //mouse state, used by styles
    protected int mouseState = -1;
    
    private final WStyle style = new WStyle(this);
    
    public AbstractWidget(boolean allowChildren) {
        super(allowChildren);
        //set default property values
        properties.add(PROPERTY_VIEW, new WidgetView(this));
        
        //the only things that make a widget dirty are :
        // - node transform change : only for parents
        // - node effective extent change
        // - a visual change
        modelTransform.addEventListener(null, this);
        
    }

    protected Object getPropertyValue(Chars name){
        return properties.getValue(name);
    }

    /**
     * Get property value, if value is null the default value is returned.
     * 
     * @param name
     * @param defaultValue
     * @return
     */
    protected Object getPropertyValue(Chars name,Object defaultValue){
        Object val = properties.getValue(name);
        return val==null ? defaultValue : val;
    }
    
    /**
     * Set property value, sends property event if value has changed.
     *
     * @param name property name
     * @param value, new value
     * @return true if value has changed
     */
    protected boolean setPropertyValue(Chars name, Object value){
        return setPropertyValue(name, value, null);
    }

    /**
     * Set property value, sends property event if value has changed.
     * if the new value is equal to the default value, then the value
     * will not be stored in the internal properties dictionary.
     *
     * @param name property name
     * @param value, new value
     * @param defaultValue
     * @return true if value has changed
     */
    protected boolean setPropertyValue(Chars name, Object value, Object defaultValue){
        Object oldVal = properties.getValue(name);
        if(oldVal==null) oldVal = defaultValue;
        final boolean changed = !CObjects.equals(oldVal, value);
        if(changed){
            if(value==null || CObjects.equals(value, defaultValue)){
                properties.remove(name);
            }else{
                properties.add(name, value);
            }
            sendPropertyEvent(this, name, oldVal, value);
        }
        return changed;
    }

    /**
     * {@inheritDoc }
     */
    public void setEffectiveExtent(Extent extent) {
//        //enforce user extents
//        extent = extent.copy();
//        getExtents().constraint(extent);
        if(effExtent!=null && effExtent.equals(extent)) return;
        final Extent old = this.effExtent.copy();
        this.effExtent.set(extent);
        sendPropertyEvent(this,PROPERTY_EFFECTIVE_EXTENT, old, extent);
        //send a dirty event
        final BBox bbox = new BBox(2);
        double maxSpanX = Maths.max(extent.get(0),old.get(0));
        double maxSpanY = Maths.max(extent.get(1),old.get(1));
        bbox.setRange(0, -maxSpanX/2.0, maxSpanX/2.0);
        bbox.setRange(1, -maxSpanY/2.0, maxSpanY/2.0);
        setDirty(bbox);
    }

    public Property varId() {
        return getProperty(PROPERTY_ID);
    }

    public Property varVisible() {
        return getProperty(PROPERTY_VISIBLE);
    }

    public Property varExtents() {
        return getProperty(PROPERTY_EXTENTS);
    }

    public Property varReserveSpace() {
        return getProperty(PROPERTY_RESERVE_SPACE);
    }
    
    public Property varEffectiveExtent() {
        return getProperty(PROPERTY_EFFECTIVE_EXTENT);
    }
    
    public Property varEnable() {
        return getProperty(PROPERTY_ENABLE);
    }
    
    /**
     * Set widget id.
     * 
     * @param id not null
     */
    public void setId(Chars id) {
        setPropertyValue(PROPERTY_ID, id, Chars.EMPTY);
    }

    /**
     * @return widget id, never null
     */
    public Chars getId() {
        return (Chars) getPropertyValue(PROPERTY_ID, Chars.EMPTY);
    }
    
    /**
     * Set widget state, enable or disable.
     * @param enable
     */
    public void setEnable(boolean enable) {
        final boolean oldStackEnable = isStackEnable();
        if(setPropertyValue(PROPERTY_ENABLE, enable, Boolean.TRUE)){
            final boolean newStackEnable = isStackEnable();
            if(oldStackEnable!= newStackEnable){
                sendPropertyEvent(this, PROPERTY_STACKENABLE, oldStackEnable, newStackEnable);
            }
        }
    }

    /**
     * @return true if widget is active
     */
    public boolean isEnable() {
        //test equality, value could be null at first
        return (Boolean)getPropertyValue(PROPERTY_ENABLE, Boolean.TRUE);
    }

    /**
     * search throw parents to find if any of them is disable.
     * @return true if all parents in the stack are enable.
     */
    public final boolean isStackEnable(){
        boolean enable = isEnable();
        Widget parent = (Widget) getParent();
        while(parent!=null && enable){
            enable = parent.isEnable();
            parent = (Widget) parent.getParent();
        }
        return enable;
    }

    /**
     * Indicate if the widget has a focused child.
     * 
     * @return true if widget has a focused child
     */
    public final boolean isChildFocused(){
        //test equality, value could be null at first
        return (Boolean)getPropertyValue(PROPERTY_CHILDFOCUSED,Boolean.FALSE);
    }
    
    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * USE method requestFocus.
     * 
     * @param focused 
     */
    protected void setChildFocused(boolean focused){
        setPropertyValue(PROPERTY_CHILDFOCUSED, focused, Boolean.FALSE);
    }
    
    /**
     * Indicate if the widget is focused.
     * 
     * @return true if widget is focused
     */
    public final boolean isFocused(){
        //test equality, value could be null at first
        return (Boolean)getPropertyValue(PROPERTY_FOCUSED,Boolean.FALSE);
    }
    
    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * USE method requestFocus.
     * 
     * @param focused 
     */
    public void setFocused(boolean focused){
        setPropertyValue(PROPERTY_FOCUSED, focused, Boolean.FALSE);
    }
    
    /**
     * Request focus on this widget.
     */
    public void requestFocus(){
        final UIFrame frame = getFrame();
        if(frame==null) return;
        frame.setFocusedWidget(this);
    }
            
    /**
     * Set widget tooltip.
     * @param tooltip, can be null
     */
    public void setToolTip(Widget tooltip) {
        setPropertyValue(PROPERTY_TOOLTIP, tooltip);
    }

    /**
     * @return popup tooltip, can be null.
     */
    public Widget getToolTip() {
        return (Widget)getPropertyValue(PROPERTY_TOOLTIP);
    }

    /**
     * Set widget popup.
     * @param popup, can be null
     */
    public void setPopup(Widget popup) {
        setPropertyValue(PROPERTY_POPUP, popup);
    }

    /**
     * @return popup widget, can be null.
     */
    public Widget getPopup() {
        return (Widget)getPropertyValue(PROPERTY_POPUP);
    }

    /**
     * Set widget language.
     * @param language, can not be null
     * @param recursive, loop on childrens to set language
     */
    public void setLanguage(Language language, boolean recursive) {
        setPropertyValue(PROPERTY_LANGUAGE, language, DEFAULT_LANGUAGE);
        if(recursive){
            final Iterator ite = children.createIterator();
            while (ite.hasNext()) {
                ((Widget)ite.next()).setLanguage(language, recursive);
            }
        }
    }

    /**
     * @return language, can not be null.
     */
    public Language getLanguage() {
        return (Language) getPropertyValue(PROPERTY_LANGUAGE, DEFAULT_LANGUAGE);
    }
    
    /**
     * Set widget cursor.
     * @param cursor, can be null
     */
    public void setCursor(Cursor cursor) {
        setPropertyValue(PROPERTY_CURSOR, cursor);
    }

    /**
     * @return cursor  can be null.
     */
    public Cursor getCursor() {
        return (Cursor)getPropertyValue(PROPERTY_CURSOR);
    }
    
    /**
     * search throw parents to find the first cursor
     * @return Cursor, can be null
     */
    public final Cursor getStackCursor(){
        Cursor cursor = getCursor();
        Widget parent = (Widget) getParent();
        while(parent!=null && cursor==null){
            cursor = parent.getCursor();
            parent = (Widget) parent.getParent();
        }
        return cursor;
    }
    
    protected void getExtentsInternal(Extents buffer, Extent constraint) {
        computeViewAndMarginExtents(buffer, constraint);
    }
    
    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * Reaffect extents which are not overridden.
     */
    public boolean updateExtents(){
        return super.updateExtents();
    }

    private void computeViewAndMarginExtents(Extents extents, Extent constraint){
        final View view = getView();
        if(view!=null) view.getExtents(extents,constraint);
        
        //append border margins
        final Margin margin = WidgetStyles.readMargin(style, null, STYLE_PROP_MARGIN); 
        final double marginSumX = margin.right+margin.left;
        final double marginSumY = margin.top+margin.bottom;
        extents.minX = extents.minX+marginSumX;
        extents.minY = extents.minY+marginSumY;
        extents.bestX = extents.bestX+marginSumX;
        extents.bestY = extents.bestY+marginSumY;
        extents.maxX = extents.maxX+marginSumX;
        extents.maxY = extents.maxY+marginSumY;
        
    }
    
    /**
     * The widget effective inner bounding box, excluding margins and overlaps.
     * @return BoundingBox
     */
    public final BBox getInnerExtent(){
        return getInnerExtent(this, getEffectiveExtent());
    }
        
    public static final BBox getInnerExtent(Widget widget, Extent extent){
        final BBox ext = AbstractWidget.centeredBBox(extent,null);
        final double middleX = ext.getMiddle(0);
        final double middleY = ext.getMiddle(1);
        final Margin margin = WidgetStyles.readMargin(widget.getStyle(), null, STYLE_PROP_MARGIN);
        
        double minX = ext.getMin(0)+margin.left;
        if(minX>middleX) minX = middleX;
        double maxX = ext.getMax(0)-margin.right;
        if(maxX<middleX) maxX = middleX;
        
        double minY = ext.getMin(1)+margin.top;
        if(minY>middleY) minY = middleY;
        double maxY = ext.getMax(1)-margin.bottom;
        if(maxY<middleY) maxY = middleY;
        
        ext.setRange(0, minX,maxX);
        ext.setRange(1, minY,maxY);
        return ext;
    }
    
    /**
     * INTERNAL USE ONLY, DO NOT CALL THIS METHOD YOURSELF !
     * @param frame
     */
    public final void setFrame(UIFrame frame){
        setPropertyValue(PROPERTY_FRAME, frame);
    }

    public UIFrame getFrame(){
        final Widget parent = (Widget) getParent();
        if(parent!=null) return parent.getFrame();
        return (UIFrame) getPropertyValue(PROPERTY_FRAME);
    }

    /**
     * Style definition,
     * @return
     */
    public final WStyle getStyle(){
        return style;
    }

    public Chars thisToChars() {
        return new Chars(this.getClass().getSimpleName());
    }

    /**
     * Flags are similar to CSS style classes.
     * It is a list of Chars mainly used to mark widgets.
     * 
     * @return Set, never null
     */
    public Set getFlags() {
        return flags;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // rendering ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public synchronized void setView(View view) {
        final View old = getView();
        boolean diff = old!=null && !old.equals(view);
        if(diff){
            //release view resources
            old.dispose();
        }
        setPropertyValue(PROPERTY_VIEW, view);
        if(diff){
            updateExtents();
            setDirty();
        }
    }
    
    public View getView(){
        return (View) getPropertyValue(PROPERTY_VIEW);
    }

    
    public void setDirty(){
        setDirty(null);
    }

    /**
     * Set dirty.
     * @param dirtyArea
     */
    public void setDirty(BBox dirtyArea){
        final BBox dirty = new BBox(2);
        if(dirtyArea==null){
            getView().calculateVisualExtent(dirty);
        }else{
            dirty.set(dirtyArea);
        }
        
        sendPropertyEvent(this,PROPERTY_DIRTY, null, dirty);
    }
        
    ////////////////////////////////////////////////////////////////////////////
    // popup and tooltip ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public abstract Tuple getOnSreenPosition(Widget child);

    public Tuple getOnSreenPosition(){
        final Widget parent = (Widget) getParent();
        if(parent!=null){
            return parent.getOnSreenPosition(this);
        }else if(getFrame()!=null){
            return getFrame().getOnScreenLocation();
        }else{
            //not displayed
            return null;
        }
    }

    /**
     * Force showing tooltip.
     */
    public void showTooltip(){
        WPopup.showAt(this, getToolTip(), new Point(new DefaultTuple(0, 0)));
    }

    /**
     * Force showing popup.
     */
    public void showPopup(){
        WPopup.showAt(this, getPopup(), new Point(new DefaultTuple(0, 0)));
    }


    ////////////////////////////////////////////////////////////////////////////
    // events //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * All widgets provide at least events of type :
     * - MouseEvent
     * - KeyEvent
     * - PropertyEvent
     * @return Class[]
     */
    public Class[] getEventClasses() {
        return new Class[]{
            PropertyMessage.class,
            MouseMessage.class,
            NodeMessage.class,
            KeyMessage.class};
    }

    /**
     * Catch event from children if rerendering is needed.
     * @param event
     */
    public final void receiveEvent(Event event) {
        if(event.getSource()==modelTransform){
            //transform has change, send the event to the parent
            final SceneNode parent = getParent();
            if(parent!=null ){
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                sendPropertyEvent(this, NodeTransform.PROPERTY_MATRIX, pe.getOldValue(), pe.getNewValue());
            }
        }
        
        super.receiveEvent(event);

        //event can come from a parent or a children
        final EventSource source = event.getSource();
        if(containsChildIdentity(source)){
            receiveEventChildren(event);
        }else{
            if(event.getMessage() instanceof MouseMessage){
                final MouseMessage me = (MouseMessage) event.getMessage();
                final int type = me.getType();
                if(type!=mouseState){
                    boolean oldMouseOver = isMouseOver();
                    boolean oldMousePress = isMousePress();
                    if(me.getType() == MouseMessage.TYPE_EXIT){
                        mouseState = -1;
                    }else{
                        mouseState = type;
                    }
                    if(oldMouseOver!=isMouseOver()){
                        sendPropertyEvent(this, PROPERTY_MOUSEOVER, oldMouseOver, isMouseOver());
                    }
                    if(oldMousePress!=isMousePress()){
                        sendPropertyEvent(this, PROPERTY_MOUSEPRESS, oldMousePress, isMousePress());
                    }
                    
                }
                
            }
            
            receiveEventParent(event);
        }
        
        //check triggers
        final Sequence triggers = style.getStackProperties().getTriggers();
        for(int i=0,n=triggers.getSize();i<n;i++){
            final StyleDocument trigger = (StyleDocument) triggers.get(i);
            //check if trigger should be activated
            if(Boolean.TRUE.equals(trigger.getFilter().evaluate(event))){
                //execute properties, those should be functions.
                final Iterator ite = trigger.getFieldNames().createIterator();
                while(ite.hasNext()){
                    Object val = trigger.getFieldValue((Chars) ite.next());
                    if(val instanceof Expression){
                        ((Expression)val).evaluate(this);
                    }
                }
            }
        }
    }

    protected void receiveEventParent(Event event) {
        
        final View view = getView();
        if(view!=null) view.receiveEvent(event);
        
        //special case for MouseEvent or KeyEvent.
        final EventMessage message = event.getMessage();
        if(message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if(!me.isConsumed()
               && me.getType() == MouseMessage.TYPE_RELEASE
               && me.getButton() == MouseMessage.BUTTON_3){
                //show the popup
                WPopup.showAt(this, getPopup(), new Point((TupleRW) me.getMousePosition()));
            }
            
            if(hasListeners()){
                getEventManager().sendEvent(new Event(this, me));
            }
        }else if(message instanceof KeyMessage){
            if(hasListeners()){
                final KeyMessage ke = (KeyMessage) message;
                getEventManager().sendEvent(new Event(this, ke));
            }
        }else if(message instanceof DragAndDropMessage){
            if(hasListeners()){
                getEventManager().sendEvent(new Event(this, message));
            }
        }else if(message instanceof PropertyMessage && ((PropertyMessage)message).getPropertyName().equals(PROPERTY_STACKENABLE)){
            final PropertyMessage pe = (PropertyMessage) message;
            if((Boolean)pe.getNewValue()){
                if(isEnable()){
                    //stack enable state has changed
                    sendPropertyEvent(this, PROPERTY_STACKENABLE, false, true);
                }
            }else{
                if(isEnable()){
                    //stack enable state has changed
                    sendPropertyEvent(this, PROPERTY_STACKENABLE, true, false);
                }
            }
        }
    }

    protected void receiveEventChildren(Event event) {
        
    }

    @Override
    protected void sendPropertyEvent(EventSource source, Chars name, Object oldValue, Object newValue) {
        super.sendPropertyEvent(source, name, oldValue, newValue);
        
        //stack events are passed down to children
        if(PROPERTY_STACKENABLE.equals(name) && !children.isEmpty()){
            final PropertyMessage pe = new PropertyMessage(name, oldValue, newValue);
            final Event event = new Event(source, pe);
            final Iterator ite = children.createIterator();
            while (ite.hasNext()) {
                ((Widget)ite.next()).receiveEvent(event);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // mouse event state ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    public Property varMouseOver() {
        return new DefaultProperty(this, PROPERTY_MOUSEOVER);
    }
    
    public boolean isMouseOver(){
        return mouseState != -1;
    }
    
    public Property varMousePress() {
        return new DefaultProperty(this, PROPERTY_MOUSEPRESS);
    }
    
    public boolean isMousePress(){
        return mouseState == MouseMessage.TYPE_PRESS;
    }
    

    ////////////////////////////////////////////////////////////////////////////
    // children properties and listener ////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    protected void addChildren(int index, Object[] array) {
        for(Object n : array){
            final Widget widget = (Widget) n;
            //update children language
            widget.setLanguage(getLanguage(), true);
        }
        super.addChildren(index,array);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Utils for all widgets ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * FOR INTERNAL USE ONLY.
     * 
     * @return 
     */
    public EventManager getEventManager() {
        return super.getEventManager();
    }
    
}
