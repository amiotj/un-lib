

package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public class WPreviewView extends WInteractiveView {
    
    public WPreviewView() {
        super(createGlyph("\uE027"), new Chars("Preview"),false);
    }
        
}
