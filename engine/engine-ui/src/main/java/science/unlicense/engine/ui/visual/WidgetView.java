
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.model.doc.DocMessage;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.task.Task;
import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.ShapeStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.Widget;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_MARGIN;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_VISUAL_MARGIN;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Rectangle;

/**
 *
 * @author Johann Sorel
 */
public class WidgetView implements View {

    protected Extent INFINITE = new Extent.Double(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

    protected final Widget widget;
    protected final EventListener widgetListener = new EventListener() {
            public void receiveEvent(Event event) {
                receiveWidgetEvent((PropertyMessage)event.getMessage());
            }
        };
    protected final EventListener styleListener = new EventListener() {
            public void receiveEvent(Event event) {
                receiveStyleEvent((DocMessage)event.getMessage());
            }
        };

    public WidgetView(final Widget widget) {
        this.widget = widget;
        this.widget.addEventListener(PropertyMessage.PREDICATE, widgetListener);
        this.widget.getStyle().getStackProperties().addEventListener(DocMessage.PREDICATE, styleListener);
    }

    public Widget getWidget() {
        return widget;
    }
    
    /**
     * Get the most appropriate text translation if text is an LChars
     * @param text
     * @return CharArray
     */
    protected CharArray getTranslatedText(CharArray text){
        if(text instanceof LChars){
            //search for a translation
            LChars trs = ((LChars)text).translate(widget.getLanguage());
            if(trs!=null){
                return trs;
            }else{
                return text;
            }
            
        }else if(text instanceof Chars){
            return (Chars) text;
        }
        return text;
    }
    
    public void getExtents(Extents buffer, Extent constraint) {
        buffer.minX = 0;
        buffer.minY = 0;
        buffer.bestX = 0;
        buffer.bestY = 0;
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }
    
    public BBox calculateVisualExtent(BBox buffer) {
        if(buffer==null) buffer = new BBox(2);
        final Margin bmargin = WidgetStyles.readMargin(widget.getStyle(), null, STYLE_PROP_MARGIN);
        final Margin vmargin = WidgetStyles.readMargin(widget.getStyle(), null, STYLE_PROP_VISUAL_MARGIN);
        final Margin margin;
        if(bmargin!=null && vmargin!=null){
            margin = bmargin.expand(vmargin);
        }else if(bmargin!=null){
            margin = bmargin;
        }else if(vmargin!=null){
            margin = vmargin;
        }else{
            margin = null;
        }

        final Extent current = widget.getEffectiveExtent();
        final double hx = current.get(0) / 2.0;
        final double hy = current.get(1) / 2.0;
        
        if(margin!=null){
            //use defined visual margin
            buffer.setRange(0,-hx-margin.left,hx+margin.right);
            buffer.setRange(1,-hy-margin.bottom,hy+margin.top);
        }else{
            //use default widget size
            buffer.setRange(0,-hx,hx);
            buffer.setRange(1,-hy,hy);
        }
        return buffer;
    }

    public final void render(Painter2D painter, BBox dirtyArea) {
        
        if(!widget.isVisible()){
            //no visible, don't render it
            return;
        }
        
        //save the old clip
        final Geometry2D oldClip = painter.getClip();

        Rectangle newClip = new Rectangle(calculateVisualExtent(null));
        if(dirtyArea!=null){
            //intersect with the old clip
            newClip = newClip.intersection(new Rectangle(dirtyArea));
            if(newClip==null) return;
        }
        
        painter.setClip(newClip);
        
        final BBox innerBBox = widget.getInnerExtent();
        renderBorderAndBackground(painter);
        renderSelf(painter,dirtyArea,innerBBox);
        
        //restore the old clip
        painter.setClip(oldClip);
    }
    
    public final void renderEffects(Painter2D painter){
        
        final Task[] effects = WidgetStyles.readEffects(widget.getStyle(),Widget.STYLE_PROP_EFFECT);
        if(effects.length>0){
            //create a separate painter fbo
            //TODO
        }
        
        if(effects.length>0){
            //apply effects on this image
            for(Task t : effects){
                painter.execute(t, new Rectangle(widget.getEffectiveExtent()));
            }
        }
        
    }

    public void receiveEvent(Event event) {

    }

    public void receiveWidgetEvent(PropertyMessage event) {
        
    }
    
    public void receiveStyleEvent(DocMessage event) {
        widget.updateExtents();
        widget.setDirty();
    }

    protected void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox){
        
    }
    
    protected void renderBorderAndBackground(Painter2D painter){

        final WStyle style = widget.getStyle();
        final ShapeStyle[] background = WidgetStyles.readShapeStyle(style, Widget.STYLE_PROP_BACKGROUND);
        final ShapeStyle[] border = WidgetStyles.readShapeStyle(style, Widget.STYLE_PROP_BORDER);

        //render the backgrounds
        final Rectangle rect = new Rectangle(widget.getBoundingBox());
        renderShape(painter, background, rect);
        
        //render the borders
        rect.set(widget.getInnerExtent());
        renderShapeForExt(painter, border, rect);
    }
    
    protected void renderShape(Painter2D painter, ShapeStyle[] shapes, Rectangle rect){
        for(ShapeStyle shape : shapes){
            renderShape(painter, shape, rect);
        }
    }
    
    protected void renderShapeForExt(Painter2D painter, ShapeStyle[] shapes, Rectangle rect){
        for(ShapeStyle shape : shapes){
            renderShapeForExt(painter, shape, rect);
        }
    }
    
    protected void renderShapeForExt(Painter2D painter, ShapeStyle shape, Rectangle rect){
        if(shape==null || rect==null) return;

        //render the border and fill
        final Geometry2D geom = shape.getShape(rect);
        renderShape(painter, shape, geom);
    }
    
    protected void renderShape(Painter2D painter, ShapeStyle shape, Geometry2D geom){
        if(shape==null || geom==null) return;

        //render the border and fill
        final Paint fillPaint = shape.getFillPaint();
        if(fillPaint!=null){
            painter.setPaint(fillPaint);
            painter.fill(geom);
        }
        
        final Brush brush = shape.getBrush();
        final Paint brushPaint = shape.getBrushPaint();
        if(brush!=null && brushPaint!=null){
            painter.setBrush(brush);
            painter.setPaint(brushPaint);
            painter.stroke(geom);
        }
    }
    
    public void dispose(){
        widget.removeEventListener(PropertyMessage.PREDICATE, widgetListener);
        widget.getStyle().getStackProperties().removeEventListener(DocMessage.PREDICATE, styleListener);
    }
    
}
