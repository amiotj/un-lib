
package science.unlicense.engine.ui.model;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class NumberSpinnerModel extends CObject implements SpinnerModel{

    private final Class numberClass;
    private final double def;
    private final Double min;
    private final Double max;
    private final double step;
    
    public NumberSpinnerModel(Class numberClass, Number def, Number min, Number max, Number step){
        this.numberClass = numberClass;
        this.def = def.doubleValue();
        this.min = (min!=null) ? min.doubleValue() : null;
        this.max = (max!=null) ? max.doubleValue() : null;
        this.step = step.doubleValue();
    }

    public boolean isValid(Object value) {
        if (!(value instanceof Number)) return false;
        double i = ((Number)value).doubleValue();
        if (max!=null && i>max) return false;
        if (min!=null && i<min) return false;
        return true;
    }

    public Number getDefaultValue() {
        return toType(def);
    }

    public SpinnerEditor createEditor() {
        return new NumberEditor();
    }

    public boolean hasNextValue(Object currentValue) {
        double i = ((Number)currentValue).doubleValue();
        return (max==null) ? true : (i<max);
    }

    public boolean hasPreviousValue(Object currentValue) {
        double i = ((Number)currentValue).doubleValue();
        return (min==null) ? true : (i>min);
    }

    public Number nextValue(Object currentValue) {
        double i = ((Number)currentValue).doubleValue();
        return toType(i+step);
    }

    public Number previousValue(Object currentValue) {
        double i = ((Number)currentValue).doubleValue();
        return toType(i-step);
    }
    
    private Number toType(Number value){
        if(numberClass == Byte.class){
            return value.byteValue();
        }else if(numberClass == Short.class){
            return value.shortValue();
        }else if(numberClass == Integer.class){
            return value.intValue();
        }else if(numberClass == Long.class){
            return value.longValue();
        }else if(numberClass == Float.class){
            return value.floatValue();
        }else if(numberClass == Double.class){
            return value.doubleValue();
        }else{
            throw new RuntimeException("Unsupported number class :"+numberClass.getName());
        }
    }
    
    public Chars toChars(){
        final CharBuffer cb = new CharBuffer();
        cb.append("NumberSpinnerModel[");
        cb.append(numberClass.getSimpleName());
        cb.append(',');
        cb.append(min);
        cb.append(',');
        cb.append(max);
        cb.append("]");
        return cb.toChars();
    }
    
    private final class NumberEditor extends WTextField implements SpinnerEditor{

        private boolean updating = false;
        private Object value = 0d;

        public NumberEditor() {
            getFlags().add(new Chars("WSpinner-NumberEditor"));
            setValidator(new Predicate() {
                public Boolean evaluate(Object candidate) {
                    try{
                        if(numberClass == Byte.class){
                            candidate = Byte.valueOf(String.valueOf(candidate));
                        }else if(numberClass == Short.class){
                            candidate = Short.valueOf(String.valueOf(candidate));
                        }else if(numberClass == Integer.class){
                            candidate = Integer.valueOf(String.valueOf(candidate));
                        }else if(numberClass == Long.class){
                            candidate = Long.valueOf(String.valueOf(candidate));
                        }else if(numberClass == Float.class){
                            candidate = Float.valueOf(String.valueOf(candidate));
                        }else if(numberClass == Double.class){
                            candidate = Double.valueOf(String.valueOf(candidate));
                        }
                    }catch(NumberFormatException ex){
                        return false;
                    }
                    return isValid(candidate);
                }
            });
        }

        public void setText(CharArray text) {
            if(!CObjects.equals(getText(),text)){
                super.setText(text);
                if(!updating){
                    Object old = value;

                    if(numberClass == Byte.class){
                        value = Byte.valueOf(text.toString());
                    }else if(numberClass == Short.class){
                        value = Short.valueOf(text.toString());
                    }else if(numberClass == Integer.class){
                        value = Integer.valueOf(text.toString());
                    }else if(numberClass == Long.class){
                        value = Long.valueOf(text.toString());
                    }else if(numberClass == Float.class){
                        value = Float.valueOf(text.toString());
                    }else if(numberClass == Double.class){
                        value = Double.valueOf(text.toString());
                    }
                    
                    sendPropertyEvent(this,PROPERTY_VALUE, old, value);
                }
            }
        }

        public Widget getWidget() {
            return this;
        }

        public Number getValue() {
            return toType((Number)value);
        }

        public void setValue(Object value) {
            updating=true;
            this.value = value;

            Number v = (Number)value;
            Chars text;
            if(numberClass == Byte.class){
                text = Int32.encode(v.intValue());
            }else if(numberClass == Short.class){
                text = Int32.encode(v.intValue());
            }else if(numberClass == Integer.class){
                text = Int32.encode(v.intValue());
            }else if(numberClass == Long.class){
                text = Int32.encode(v.intValue());
            }else if(numberClass == Float.class){
                text = Float64.encode(v.floatValue());
            }else if(numberClass == Double.class){
                text = Float64.encode(v.doubleValue());
            }else{
                throw new InvalidArgumentException("Unvalid number class "+ numberClass);
            }

            setText(text);
            updating=false;
        }

    }

}
