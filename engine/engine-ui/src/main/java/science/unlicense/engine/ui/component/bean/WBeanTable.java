
package science.unlicense.engine.ui.component.bean;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.DefaultProperty;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.Properties;
import science.unlicense.api.event.Property;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class WBeanTable extends WContainer implements WValueWidget {

    private final Sequence editors = new ArraySequence();

    private final WTable attributeTable = new WTable();

    public WBeanTable() {
        super(new BorderLayout());
        addChild(attributeTable, BorderConstraint.CENTER);

        final DefaultColumn nameColumn = new DefaultColumn(new Chars("Property"), new PropertyNamePresenter());
        nameColumn.setBestWidth(140);
        final DefaultColumn valueColumn = new DefaultColumn(new Chars("Value"), new PropertyValuePresenter());
        valueColumn.setBestWidth(FormLayout.SIZE_EXPAND);
        attributeTable.getColumns().add(nameColumn);
        attributeTable.getColumns().add(valueColumn);

        //default editors
        editors.add(CharArrayEditor.INSTANCE);
        editors.add(BooleanEditor.INSTANCE);
        editors.add(DoubleEditor.INSTANCE);
        editors.add(IntegerEditor.INSTANCE);
    }

    @Override
    public EventSource getValue() {
        return (EventSource) getPropertyValue(PROPERTY_VALUE);
    }

    @Override
    public void setValue(Object value) {
        if (setPropertyValue(PROPERTY_VALUE, value)) {

            final Sequence properties = new ArraySequence();
            if (value instanceof EventSource) {
                final Sequence propNames = Properties.listProperties(value.getClass());
                final Iterator ite = propNames.createIterator();
                while (ite.hasNext()) {
                    final Chars propName = (Chars) ite.next();
                    final Property prop = new DefaultProperty((EventSource) value, propName);

                    for(int k=0,kn=editors.getSize();k<kn;k++){
                        final Widget editor = ((PropertyEditor)editors.get(k)).create(prop);
                        if(editor!=null){
                            properties.add(prop);
                            break;
                        }
                    }
                }
            }
            attributeTable.setRowModel(new DefaultRowModel(properties));
        }
    }

    @Override
    public Variable varValue() {
        return getProperty(PROPERTY_VALUE);
    }

    public Sequence getEditors() {
        return editors;
    }


    private static final class PropertyNamePresenter implements ObjectPresenter{
        public Widget createWidget(Object candidate) {
            final Property property = (Property) candidate;
            final WLabel lbl = new WLabel(property.getName());
            lbl.getStyle().getSelfRule().setProperties(new Chars("margin:2"));
            return lbl;
        }
    }

    private final class PropertyValuePresenter implements ObjectPresenter{
        public Widget createWidget(Object candidate) {
            final Property property = (Property) candidate;

            for(int k=0,kn=editors.getSize();k<kn;k++){
                final Widget editor = ((PropertyEditor)editors.get(k)).create(property);
                if(editor!=null){
                    return editor;
                }
            }

            return new WSpace();
        }
    }

}
