
package science.unlicense.engine.ui.io;

import java.lang.reflect.Method;
import science.unlicense.api.CObject;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.predicate.Expression;
import static science.unlicense.engine.ui.io.EqualsPredicate.CLASS;

/**
 *
 * @author Johann Sorel
 */
public class PropertyName extends CObject implements Expression{

    private final HashDictionary cache = new HashDictionary();

    private final Chars propertyName;
    private final String str1;
    private final String str2;

    public PropertyName(Chars name) {
        this.propertyName = name;
        str1 = "get"+propertyName;
        str2 = "is"+propertyName;
    }

    public Chars getName() {
        return propertyName;
    }
    
    public Object evaluate(Object candidate) {
        
        if(CLASS.equals(propertyName)){
            return candidate!=null ? candidate.getClass() : null;
        }
        
        final Class clazz = candidate.getClass();
        Method getter = (Method) cache.getValue(clazz);
        if(getter==null){
            final Method[] methods = candidate.getClass().getMethods();
            String name;
            for(Method m : methods){
                name = m.getName();
                if(str1.equals(name) || str2.equals(name)){
                    getter = m;
                    cache.add(clazz, m);
                    break;
                }
            }
        }

        Object val = null;
        if(getter!=null){
            try{
                val = getter.invoke(candidate);
            }catch(Exception e){
                //TODO proper log, keep it like this until new widget style is done
                Loggers.get().log(new Chars("Could not invoke property "+propertyName + " on "+candidate.getClass().getName()), Logger.LEVEL_WARNING);
            }
        }
        
        return val;
    }

    public Chars toChars() {
        return new Chars("$").concat(propertyName);
    }
    public int getHash() {
        int hash = 7;
        hash = 23 * hash + (this.propertyName != null ? this.propertyName.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PropertyName other = (PropertyName) obj;
        if (this.propertyName != other.propertyName && (this.propertyName == null || !this.propertyName.equals(other.propertyName))) {
            return false;
        }
        return true;
    }

}
