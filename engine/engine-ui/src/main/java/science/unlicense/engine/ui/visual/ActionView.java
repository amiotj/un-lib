
package science.unlicense.engine.ui.visual;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WAction;

/**
 *
 * @author Johann Sorel
 */
public class ActionView extends ContainerView{

    public ActionView(WAction labeled) {
        super(labeled);
    }

    public WAction getWidget() {
        return (WAction) super.getWidget();
    }

    public void receiveEvent(Event event) {
        final EventMessage message = event.getMessage();
        if(message instanceof MouseMessage && !message.isConsumed() && getWidget().isStackEnable()){
            final MouseMessage me = (MouseMessage) message;
            final int type = me.getType();
            
            if(MouseMessage.TYPE_PRESS == type && me.getButton() == MouseMessage.BUTTON_1){
                getWidget().requestFocus();
                me.consume();
            }else if(MouseMessage.TYPE_RELEASE == type && me.getButton() == MouseMessage.BUTTON_1){
                me.consume();
                //fire action event
                getWidget().doClick();
            }
        }
    }

}
