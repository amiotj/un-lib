
package science.unlicense.engine.ui.visual;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public class ViewRenderLoop {

    /**
     *
     * @param widget widget to render
     * @param parentPainter painter 
     * @param parentDirty dirty box 
     */
    public static void render(final Widget widget, final Painter2D parentPainter, final BBox parentDirty){
        if(!widget.isVisible()) return;

        final View view = widget.getView();
        if(view==null) return;
        
        //check intersection
        final NodeTransform trs = widget.getNodeTransform();
        
        final double[] lower = parentDirty.getLower().getValues();
        final double[] upper = parentDirty.getUpper().getValues();
        final double[] dirtyCoords = {
            lower[0],lower[1],
            lower[0],upper[1],
            upper[0],upper[1],
            upper[0],lower[1]
        };
        

        //check intersection with the dirty box
        final double[] childDirty = new double[8];
        trs.inverseTransform(dirtyCoords, 0, childDirty, 0, 4);
        final BBox dirtyBBox = new BBox(2);
        dirtyBBox.setRange(0, childDirty[0], childDirty[0]);
        dirtyBBox.setRange(1, childDirty[1], childDirty[1]);
        dirtyBBox.expand(childDirty[2], childDirty[3]);
        dirtyBBox.expand(childDirty[4], childDirty[5]);
        dirtyBBox.expand(childDirty[6], childDirty[7]);

        //test intersection
        final BBox visualDimension = view.calculateVisualExtent(null);
        if(!dirtyBBox.intersects(visualDimension,false)) return;
        
        //reduce dirty area to widget bounds
        dirtyBBox.localIntersect(visualDimension);
        
        final Painter2D painter = parentPainter.derivate(new Affine2(trs));
        
        
        //render it
        view.render(painter, dirtyBBox);
        
        if(!(widget instanceof WContainer)){
            view.renderEffects(painter);
            return;
        }
        
        
        //render children        
        final WContainer container = (WContainer) widget;
        final Iterator ite = container.getChildren().createIterator();
                
        while(ite.hasNext()) {
            final Widget child = (Widget) ite.next();
            render(child, painter, dirtyBBox);
        }
                
        view.renderEffects(painter);
    }
    
}
