
package science.unlicense.engine.ui.model;

/**
 *
 * @author Johann Sorel
 */
public interface SpinnerModel {

    Object getDefaultValue();

    SpinnerEditor createEditor();

    boolean isValid(Object value);
    
    boolean hasNextValue(Object currentValue);

    boolean hasPreviousValue(Object currentValue);

    Object nextValue(Object currentValue);

    Object previousValue(Object currentValue);

}
