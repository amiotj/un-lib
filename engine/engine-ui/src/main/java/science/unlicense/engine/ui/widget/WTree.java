
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.TreeRowModel;

/**
 * Display a one column tree.
 *
 * @author Johann Sorel
 */
public class WTree extends AbstractRowWidget {
    
    private final DefaultTreeColumn colModel;

    public WTree() {
        this(null);
    }
    
    public WTree(ObjectPresenter presenter) {
        super(new TreeRowModel(new DefaultNode(true)));
        colModel = new DefaultTreeColumn(null,(Chars)null,presenter);
        colModel.setBestWidth(FormLayout.SIZE_EXPAND);
        colModel.setRowModel(getRowModel());
        setHeaderVisible(false);
        getColumns().add(colModel);
    }

    public TreeRowModel getRowModel() {
        return (TreeRowModel) super.getRowModel();
    }

    public void setRowModel(TreeRowModel rowModel) {
        colModel.setRowModel(rowModel);
        super.setRowModel(rowModel);
    }

    public DefaultTreeColumn getColumn() {
        return colModel;
    }
    
}
