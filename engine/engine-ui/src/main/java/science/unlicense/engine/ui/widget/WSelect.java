package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultObjectPresenter;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.RowModel;

/**
 * @author Johann Sorel
 */
public class WSelect extends AbstractControlWidget{

    public static final Chars PROPERTY_SELECTION = new Chars("Selection");
    public static final Chars PROPERTY_RENDERER = new Chars("Renderer");

    private final WAction dropdownbutton = new WAction(null, null, new EventListener() {
        public void receiveEvent(Event event) {
            dropdown.getChildren().removeAll();
            dropdown.setLayout(new BorderLayout());
            
            final WTable list = new WTable(model, new DefaultColumn());
            dropdown.getChildren().add(list);
            
            int offsetx = 0;
            int offsety = (int)getEffectiveExtent().get(1);
            dropdown.showAt(WSelect.this, offsetx, offsety);
        }
    });

    private final EventListener modelListener = new EventListener() {
        public void receiveEvent(Event event) {
            getChildren().remove(presented);
            presented = getRenderer().createWidget(getSelection());
            getChildren().add(presented);
            setDirty();
        }
    };
    
    private final WPopup dropdown = new WPopup();
    
    private RowModel model;
    private Widget presented;
    

    public WSelect() {
        this(new DefaultRowModel());
    }

    public WSelect(RowModel model) {
        setPropertyValue(PROPERTY_RENDERER, new DefaultObjectPresenter());
        final FormLayout layout = new FormLayout();
        layout.setColumnSize(0, FormLayout.SIZE_EXPAND);
        setLayout(layout);
        
        dropdownbutton.getFlags().add(new Chars("WSelect-DownButton"));
        final Extents overExts = dropdownbutton.getOverrideExtents();
        overExts.setAll(16, 16);
        dropdownbutton.setOverrideExtents(overExts);
        
        this.model = model;
        this.presented = getRenderer().createWidget(getSelection());
        addChild(presented,      FillConstraint.builder().coord(0, 0).build());
        addChild(dropdownbutton, FillConstraint.builder().coord(1, 0).build());
    }

    public void setModel(RowModel model) {
        if(this.model!=null){
            this.model.removeEventListener(CollectionMessage.PREDICATE, modelListener);
        }
        this.model = model;
        updateExtents();
        setDirty();
        if(this.model!=null){
            this.model.addEventListener(CollectionMessage.PREDICATE, modelListener);
        }
    }

    public RowModel getModel() {
        return model;
    }

    public void setRenderer(ObjectPresenter renderer) {
        setPropertyValue(PROPERTY_RENDERER, renderer);
        updateExtents();
        setDirty();
    }

    public ObjectPresenter getRenderer() {
        return (ObjectPresenter) getPropertyValue(PROPERTY_RENDERER);
    }

    public Object getSelection() {
        return getPropertyValue(PROPERTY_SELECTION);
    }
    
    public void setSelection(Object selection){
        setPropertyValue(PROPERTY_SELECTION, selection);
    }
        
}