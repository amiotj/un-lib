
package science.unlicense.engine.ui.ievent;

import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.api.event.MessagePredicate;
import science.unlicense.api.predicate.Predicate;

/**
 * Action event message, often use on clickable widgets.
 * 
 * @author Johann Sorel
 */
public class ActionMessage extends DefaultEventMessage {

    public static final Predicate PREDICATE = new MessagePredicate(ActionMessage.class);

    public ActionMessage() {
        super(true);
    }
    
}
