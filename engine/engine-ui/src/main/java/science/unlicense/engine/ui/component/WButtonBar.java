

package science.unlicense.engine.ui.component;

import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WContainer;

/**
 *
 * @author Johann Sorel
 */
public class WButtonBar extends WContainer{

    public WButtonBar() {
        setLayout(new FormLayout());
    }

}
