
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.Widget;

/**
 * A Widget view why extent size is based on font height.
 * 
 * @author Johann Sorel
 */
public class FontRelativeView extends WidgetView{

    private static final CharArray text = new Chars(" ");
    
    public FontRelativeView(final Widget widget) {
        super(widget);
    }

    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(widget.getStyle(),Widget.STYLE_PROP_FONT);

        final Extent extent = new Extent.Double(0,0);

        //get text size
        final FontMetadata meta = font!=null ? FontContext.INSTANCE.getMetadata(font.getFont()) : null;

        if(meta!=null){
            final BBox bbox = meta.getCharsBox(text);
            extent.set(0, bbox.getSpan(1));
            extent.set(1, bbox.getSpan(1));
        }

        buffer.setAll(extent);
    }
    
}
