
package science.unlicense.engine.ui.io;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.predicate.Expression;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.DefaultTuple;

/**
 * Tuple which values are expressions.
 * 
 * @author Johann Sorel
 */
public class TupleExpression extends DefaultTuple implements Expression{

    private final Expression exp0;
    private final Expression exp1;
    
    public TupleExpression(Expression exp0, Expression exp1) {
        super(2);
        this.exp0 = exp0;
        this.exp1 = exp1;
    }

    public Tuple evaluate(Object candidate) {
        if(exp0!=null) values[0] = (Double)exp0.evaluate(candidate);
        if(exp1!=null) values[1] = (Double)exp1.evaluate(candidate);
        return this;
    }
    
    //TODO other units
    public static final class PercentValue implements Expression{

        private final double value;
        private final boolean vertical;

        public PercentValue(double value,boolean vertical) {
            this.value = value;
            this.vertical = vertical;
        }
        
        public Object evaluate(Object candidate) {
            if(candidate instanceof Widget){
                final Extent ext = ((Widget)candidate).getEffectiveExtent();
                return ext.get(vertical?1:0) * value;
            }else if(candidate instanceof Rectangle){
                final Rectangle ext = (Rectangle)candidate;
                if(vertical){
                    return ext.getY() + ext.getHeight()*value;
                }else{
                    return ext.getX() + ext.getWidth()*value;
                }                
            }
            throw new UnimplementedException("TODO");
        }
    }
    
    public static final class Add implements Expression{

        private final Expression exp0;
        private final Expression exp1;

        public Add(Expression exp0, Expression exp1) {
            this.exp0 = exp0;
            this.exp1 = exp1;
        }
        
        public Object evaluate(Object candidate) {
            return (Double)exp0.evaluate(candidate) + (Double)exp1.evaluate(candidate);
        }
    }
    
    public static final class Sub implements Expression{

        private final Expression exp0;
        private final Expression exp1;

        public Sub(Expression exp0, Expression exp1) {
            this.exp0 = exp0;
            this.exp1 = exp1;
        }
        
        public Object evaluate(Object candidate) {
            return (Double)exp0.evaluate(candidate) - (Double)exp1.evaluate(candidate);
        }
    }
    
}
