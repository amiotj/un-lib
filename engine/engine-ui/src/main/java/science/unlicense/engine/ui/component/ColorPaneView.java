
package science.unlicense.engine.ui.component;

import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.impl.color.colorspace.HSL;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class ColorPaneView extends WidgetView{

    private static final HSL hsl = HSL.INSTANCE;
    
    private Image hslImage = null;
    
    public ColorPaneView(WColorPane widget) {
        super(widget);
    }

    public WColorPane getWidget() {
        return (WColorPane) super.getWidget();
    }

    public void calculateExtents(Extent min, Extent best, Extent max) {
        min.set(0, 120);
        min.set(1, 60);
        best.set(min);
        max.set(min);
    }

    protected void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox) {
        super.renderSelf(painter, dirtyArea, innerBBox);

        final Extent ext = widget.getEffectiveExtent();
        if(hslImage == null || hslImage.getExtent().get(0) != ext.get(0)
                            || hslImage.getExtent().get(1) != ext.get(1)){
            //component size differ from image size
            hslImage = createHSLLinearImage(ext);
        }
        painter.paint(hslImage, new Affine2(
                1, 0, -ext.get(0)/2.0, 
                0, 1, -ext.get(1)/2.0));

        final TupleRW coord = toCoord(ext, getWidget().getValue());
        final Geometry2D circle = new Circle(coord, 3);
        painter.setPaint(new ColorPaint(Color.WHITE));
        painter.fill(circle);
        painter.setPaint(new ColorPaint(Color.BLACK));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.stroke(circle);
    }
    
    /**
     * Convert color to HSL image coordinate.
     * @param ext
     * @param x
     * @param y
     * @return
     */
    private static TupleRW toCoord(Extent ext, Color color){
        if(color==null) color = Color.WHITE;
        final float[] rgba = color.toRGBA();
        final float[] buffer = new float[3];
        hsl.toSpace(rgba, 0, buffer, 0, 1);
        float x = (buffer[0] * (float)ext.get(0)) / 360f;
        float y =  -(buffer[2]-1f)  * (float)ext.get(1);
        return new DefaultTuple(x-ext.get(0)/2.0, y-ext.get(1)/2.0);
    }
    
    /**
     * Create an HSL linear image.
     *
     * @param width
     * @param height
     * @return
     */
    private static Image createHSLLinearImage(Extent ext){
        final int width = (int)ext.get(0);
        final int height = (int)ext.get(1);


        final Image image = Images.create(new Extent.Long(width,height),Images.IMAGE_TYPE_RGBA);
        PixelBuffer pm = image.getColorModel().asTupleBuffer(image);
        final float[] buffer = new float[3];
        final float[] rgba = new float[4];

        //fixed saturation
        buffer[1] = 1f;
        for(int x=0;x<width;x++){
            // Hue
            buffer[0] = (x*360f) / width;
            for(int y=0;y<height;y++){
                //ligthness
                buffer[2] = 1f - ((float)y / height);

                hsl.toRGBA(buffer, 0, rgba, 0, 1);
                pm.setRGBA(new int[]{x,y},rgba);
            }
        }

        return image;
    }
    
}
