
package science.unlicense.engine.ui.component.bean;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class CharArrayEditor implements PropertyEditor {

    public static final CharArrayEditor INSTANCE = new CharArrayEditor();

    private CharArrayEditor(){}

    public Widget create(Property property) {
        if(CharArray.class.isAssignableFrom(property.getValueClass())){
            final WTextField textField = new WTextField();
            textField.varText().sync(property);
            return textField;
        }
        return null;
    }

}
