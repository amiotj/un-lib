
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Property;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.WRow;
import static science.unlicense.engine.ui.widget.AbstractRowWidget.STYLE_CELL;

/**
 *
 * @author Johann Sorel
 */
public class WCell extends WContainer{
    
    public static final Chars PROPERTY_VALUE = WValueWidget.PROPERTY_VALUE;
    
    private final WRow row;
    private final Column column;

    public WCell(WRow row, Column column) {
        super(new BorderLayout());
        getFlags().add(STYLE_CELL);
        this.row = row;
        this.column = column;
    }

    public WRow getRow() {
        return row;
    }

    public Column getColumn() {
        return column;
    }
    
    public final Object getValue(){
        return getPropertyValue(PROPERTY_VALUE);
    }
    
    public final void setValue(Object value){
        if(setPropertyValue(PROPERTY_VALUE, value)){
            update(value);
        }
    }
    
    public final Property varValue(){
        return getProperty(PROPERTY_VALUE);
    }
    
    protected void update(Object newValue){
        final Widget content = column.getPresenter().createWidget(column.getCellValue(newValue));
        content.setLayoutConstraint(BorderConstraint.CENTER);
        children.replaceAll(new Node[]{content});
    }
    
}
