
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.api.character.Chars;
import science.unlicense.api.desktop.Frame;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;

/**
 *
 * @author Johann Sorel
 */
public class WSystemDecoration extends WFrameDecoration {
    
    private static final Chars FONT_STYLE = new Chars("{subs : {filter:true\nborder:none\nmargin:2\nfont:{family:font('Unicon' 9 'none')\n}\n}}");
    
    private final WLabel title = new WLabel();
    private final WFrameDrag drag = new WFrameDrag(new BorderLayout());
    private final WFrameResize resizeBottom = new WFrameResize();
    
    private final WButton minimize = new WButton(new Chars("\ue01D"), null,new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            getFrame().setState(Frame.STATE_MINIMIZED);
        }
    });
    private final WButton pin = new WButton(new Chars("\ue020"), null,new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            getFrame().setAlwaysonTop(!getFrame().isAlwaysOnTop());
        }
    });
    private final WButton maximize = new WButton(new Chars("\ue01E"), null,new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            if(getFrame().getState()==Frame.STATE_MAXIMIZED){
                getFrame().setState(Frame.STATE_NORMAL);
            }else{
                getFrame().setState(Frame.STATE_MAXIMIZED);
            }
        }
    });
    private final WButton close = new WButton(new Chars("\ue01F"), null,new EventListener(){
        @Override
        public void receiveEvent(Event event) {
            getFrame().dispose();
        }
    });
    
    public WSystemDecoration(){
        
        content.getFlags().add(new Chars("systemframe"));
        
        final WContainer header = new WContainer(new FormLayout());
        header.getFlags().add(new Chars("systemframe-header"));
        
        minimize.getStyle().addRules(FONT_STYLE);
        pin.getStyle().addRules(FONT_STYLE);
        maximize.getStyle().addRules(FONT_STYLE);
        close.getStyle().addRules(FONT_STYLE);
        minimize.setReserveSpace(false);
        pin.setReserveSpace(false);
        maximize.setReserveSpace(false);
        close.setReserveSpace(false);
        
        ((FormLayout)header.getLayout()).setColumnSize(0, FormLayout.SIZE_EXPAND);
                
        drag.addChild(title,BorderConstraint.CENTER);
        header.addChild(drag,       FillConstraint.builder().coord(0, 0).build());
        header.addChild(minimize,   FillConstraint.builder().coord(1, 0).build());
        header.addChild(pin,        FillConstraint.builder().coord(2, 0).build());
        header.addChild(maximize,   FillConstraint.builder().coord(3, 0).build());
        header.addChild(close,      FillConstraint.builder().coord(4, 0).build());
                
        final Extents overrideExtents = resizeBottom.getOverrideExtents();
        overrideExtents.bestX = 2;overrideExtents.bestY = 3;
        resizeBottom.setOverrideExtents(overrideExtents);
                
        content.addChild(header,BorderConstraint.TOP);
        content.addChild(resizeBottom,BorderConstraint.BOTTOM);
        content.addChild(new WSpace(new Extent.Double(1, 1)),BorderConstraint.LEFT);
        content.addChild(new WSpace(new Extent.Double(1, 1)),BorderConstraint.RIGHT);
    }

    @Override
    public void setFrame(Frame frame) {
        super.setFrame(frame);
        if(frame!=null){
            title.varText().sync(frame.varTitle());
            minimize.varVisible().sync(frame.varMinimizable());
            maximize.varVisible().sync(frame.varMaximizable());
            close.varVisible().sync(frame.varClosable());
        }else{
            title.varText().unsync();
            minimize.varVisible().unsync();
            pin.varVisible().unsync();
            maximize.varVisible().unsync();
            close.varVisible().unsync();
        }
    }
    
}
