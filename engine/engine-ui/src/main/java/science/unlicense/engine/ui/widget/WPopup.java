
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.desktop.FrameManagers;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.desktop.UIFrame;

/**
 *
 * @author Johann Sorel
 */
public class WPopup extends WContainer{

    public WPopup() {
    }
    
    /**
     * Display the popup at target widget location.
     *
     * @param target
     * @param offsetX X offset from target upper left corner
     * @param offsetY Y offset from target upper left corner
     */
    public void showAt(final Widget target, final int offsetX, final int offsetY){
        new Thread(){
            @Override
            public void run() {
                final UIFrame frame;
                if(target==null){
                    frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
                }else{
                    frame = (UIFrame) target.getFrame().getManager().createFrame(target.getFrame(), false, false);
                }

                final WContainer container = frame.getContainer();
                container.setLayout(new BorderLayout());
                container.addChild(WPopup.this, BorderConstraint.CENTER);

                final Extents ext = WPopup.this.getExtents();

                final TupleRW pt;
                if(target==null){
                    pt = new Vector(offsetX, offsetY);
                }else{
                    pt = (TupleRW) target.getOnSreenPosition();
                    if(pt==null){
                        //target is not visible,
                        Loggers.get().info(new Chars("Trying to display a popup for an unvisible parent : "+target.getId()));
                        return;
                    }
                    pt.setX(pt.getX()+offsetX);
                    pt.setY(pt.getY()+offsetY);
                }

                frame.setDecoration(null);
                frame.setSize((int)ext.bestX, (int)ext.bestY);
                frame.setOnScreenLocation(pt);

                //listen to mouse exit to close popup
                container.addEventListener(MouseMessage.PREDICATE, new EventListener() {
                    public void receiveEvent(Event event) {
                        final MouseMessage me = (MouseMessage) event.getMessage();
                        if(me.getType() == MouseMessage.TYPE_EXIT){
                            container.getChildren().remove(WPopup.this);
                            frame.dispose();
                        }
                    }
                });

                frame.setVisible(true);
            }
        }.start();
        
    }

    /**
     * Display given widget (popup/tooltip/other) at component position.
     *
     * @param parent
     * @param candidate widget to display
     * @param position offset from component upper left corner.
     */
    public static void showAt(Widget parent, Widget candidate, Point position){
        if(candidate==null) return;
        final UIFrame frame = parent.getFrame();
        if(frame==null) return;

        final TupleRW pt = (TupleRW) parent.getOnSreenPosition();
        if(pt == null) return;

        pt.setX(pt.getX()+position.getX());
        pt.setY(pt.getY()+position.getY());

        final Extent bestExtent = candidate.getExtents().getBest(null);
        final UIFrame dialog = (UIFrame) frame.getManager().createFrame(parent.getFrame(), true, false);

        candidate.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if(me.getType() == MouseMessage.TYPE_EXIT){
                    dialog.dispose();
                }
            }
        });

        //show the popup
        final WContainer content = dialog.getContainer();
        content.setLayout(new BorderLayout());
        content.addChild(candidate, BorderConstraint.CENTER);

        dialog.setDecoration(null);
        dialog.setAlwaysonTop(true);
        dialog.setSize((int)bestExtent.get(0), (int)bestExtent.get(1));
        dialog.setOnScreenLocation(pt);
        dialog.setVisible(true);
    }
    
}
