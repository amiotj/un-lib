
package science.unlicense.engine.ui.style;

import science.unlicense.api.CObjects;
import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.Paint;

/**
 *
 * @author Johann Sorel
 */
public class FontStyle extends GeomStyle {
    
    private Font font;

    public FontStyle() {
    }

    public FontStyle(Font font, Brush brush, Paint brushPaint, Paint fillPaint) {
        super(brush,brushPaint,fillPaint);
        CObjects.ensureNotNull(font);
        this.font = font;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        CObjects.ensureNotNull(font);
        this.font = font;
    }
     
}
