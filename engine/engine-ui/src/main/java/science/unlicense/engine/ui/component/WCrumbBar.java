
package science.unlicense.engine.ui.component;

import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.engine.ui.widget.WContainer;

/**
 * A Breadcrumb bar or WCrumbBar is a button bar which visually shows a path
 * made of different children.
 * 
 * @author Johann Sorel
 */
public class WCrumbBar extends WContainer {

    public WCrumbBar() {
        setLayout(new FormLayout());
    }
    
    public static class WCrumb extends WContainer {

        public WCrumb() {
            super(new BorderLayout());
        }
        
        /**
         * Get crumb index in it's parent.
         * @return index.
         */
        public int getIndex(){
            final SceneNode parent = getParent();
            if(parent==null) return 0;
            return parent.getChildren().search(this);
        }
        
    }
    
}
