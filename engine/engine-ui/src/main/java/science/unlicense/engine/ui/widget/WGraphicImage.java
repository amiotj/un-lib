
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.image.Image;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.visual.GraphicImageView;

/**
 * Widget holding a single image.
 * 
 * @author Johann Sorel
 */
public class WGraphicImage extends WLeaf{
    
    public static final int FITTING_SCALED = 0;
    public static final int FITTING_CENTERED = 1;
    public static final int FITTING_STRETCHED = 2;
    public static final int FITTING_ZOOMED = 3;
    public static final int FITTING_CORNER = 4;

    /**
     * Property for the widget image.
     */
    public static final Chars PROPERTY_IMAGE = new Chars("Image");
    /**
     * Property for the widget image fitting method.
     */
    public static final Chars PROPERTY_FITTING = new Chars("Fitting");
    
    public WGraphicImage() {
        this(null);
    }
    
    public WGraphicImage(Image image) {
        setPropertyValue(PROPERTY_IMAGE, image);
        setView(new GraphicImageView(this));
    }
    
    /**
     * Get graphic image.
     * @return Image, can be null
     */
    public Image getImage() {
        return (Image)getPropertyValue(PROPERTY_IMAGE);
    }

    /**
     * Set graphic image.
     * @param image can be null
     */
    public void setImage(Image image) {
       setPropertyValue(PROPERTY_IMAGE,image);
    }
    
    /**
     * Get image variable.
     * @return Variable.
     */
    public Variable varImage(){
        return getProperty(PROPERTY_IMAGE);
    }

    /**
     * Get image fitting mode.
     * @return Image fitting mode, default is
     */
    public int getFitting() {
        return (Integer)getPropertyValue(PROPERTY_FITTING, FITTING_SCALED);
    }

    /**
     * Set image fitting mode.
     * @param fitting 
     */
    public void setFitting(int fitting) {
       setPropertyValue(PROPERTY_FITTING,fitting, FITTING_SCALED);
    }

    /**
     * Get fitting variable.
     * @return Variable.
     */
    public Variable varFitting(){
        return getProperty(PROPERTY_FITTING);
    }
}
