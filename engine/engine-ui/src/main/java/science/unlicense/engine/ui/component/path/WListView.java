
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.api.layout.DisplacementLayout;
import science.unlicense.api.layout.LineLayout;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.path.Path;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WScrollContainer;

/**
 *
 * @author Johann Sorel
 */
public class WListView extends AbstractPathView {

    private final WContainer list = new WContainer(new LineLayout());
    private final WScrollContainer scroll = new WScrollContainer(list);

    private final EventListener selectionListener = new EventListener() {
        public void receiveEvent(Event event) {
            if(event.getMessage() instanceof PropertyMessage){
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if(WThumbnail.PROPERTY_SELECTED.equals(pe.getPropertyName())){
                    if(Boolean.TRUE.equals(pe.getNewValue())){
                        //path selected
                        final Path p = ((WThumbnail)event.getSource()).getPath();
                        setSelectedPath(new Path[]{p});
                    }else{
                        //path unselected
                        final Path p = ((WThumbnail)event.getSource()).getPath();
                        Path[] selectedPaths = getSelectedPath();
                        int idx = Arrays.getFirstOccurence(selectedPaths, 0, selectedPaths.length, p);
                        if(idx>=0){
                            final Path[] sel = (Path[]) Arrays.remove(selectedPaths, idx);
                            setSelectedPath(sel);
                        }
                    }
                }
            }else if(event.getMessage() instanceof MouseMessage){
                final MouseMessage me = (MouseMessage) event.getMessage();
                final WThumbnail source = (WThumbnail) event.getSource();
                try{
                    if(me.getType()==MouseMessage.TYPE_PRESS && me.getNbClick()>1 && source.getPath().isContainer()){
                        //open selected container
                        setViewRoot(source.getPath());
                    }
                }catch(IOException ex){}
            }
        }
    };
    public WListView() {
        this(18);
    }
    
    public WListView(int rowSize) {
        super(createGlyph("\uE025"),new Chars("List"));
        setLayout(new BorderLayout());
        addChild(scroll, BorderConstraint.CENTER);
        final DisplacementLayout l = (DisplacementLayout) scroll.getScrollingContainer().getLayout();
        l.setFillHeight(true);
        
        final LineLayout layout = (LineLayout) list.getLayout();
        layout.setDirection(LineLayout.DIRECTION_VERTICAL);
        layout.setHorizontalAlignement(LineLayout.HALIGN_LEFT);
        layout.setVerticalAlignement(LineLayout.VALIGN_TOP);
        layout.setDistance(4);
        list.getStyle().getSelfRule().setProperties(new Chars("margin:5"));
    }

    public void setViewRoot(Path viewRoot) {
        if(setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot)){
            update(true);
        }
    }

    public void setSelectedPath(Path[] path) {
        if(setPropertyValue(PROPERTY_SELECTED_PATHS, path)){
            update(false);
        }
    }

    public void setFilter(Predicate filter) {
        if(setPropertyValue(PROPERTY_FILTER,filter)){
            update(true);
        }
    }
    
    private void update(boolean all){
        final Path viewRoot = getViewRoot();

        if(all){
            //reset scroll position
            scroll.getVerticalScrollBar().setRatio(0);
            scroll.getHorizontalScrollBar().setRatio(0);
            
            Sequence datas;
            if(viewRoot==null){
                datas = new ArraySequence();
            }else{
                datas = new ArraySequence(viewRoot.getChildren());
            }

            final Predicate filter = getFilterInternal(true);
            datas = Collections.filter(datas, filter);

            //sort by name and type(folder/file)
            Collections.sort(datas, WPathChooser.FILE_SORTER);

            final Sequence elements = new ArraySequence();
            for(int i=0;i<datas.getSize();i++){
                elements.add(createWidget(datas.get(i)));
            }
            list.getChildren().replaceAll(elements);
        }

        //set selected paths
        final Sequence elements = list.getChildren();
        final Path[] selectedPaths = getSelectedPath();
        for(int i=0,n=elements.getSize();i<n;i++){
            WThumbnail prev = (WThumbnail) elements.get(i);
            prev.removeEventListener(Predicate.TRUE, selectionListener);
            prev.setSelected(Arrays.contains(selectedPaths, prev.getPath()));
            prev.addEventListener(Predicate.TRUE, selectionListener);
        }

    }

    private WLabel createWidget(Object candidate) {

        if(candidate instanceof Path){
            final Path path = (Path) candidate;
            final WThumbnail thumbnail = new WThumbnail(path, false, null, true, false, getFilter());
            thumbnail.setGraphicPlacement(WLabel.GRAPHIC_LEFT);
            return thumbnail;
        }else{
            final WLabel label = new WLabel();
            return label;
        }
    }
    
}
