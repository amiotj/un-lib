
package science.unlicense.engine.ui.component.bean;

import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class BooleanEditor implements PropertyEditor {

    public static final BooleanEditor INSTANCE = new BooleanEditor();

    private BooleanEditor(){}

    public Widget create(Property property) {
        if(Boolean.class.isAssignableFrom(property.getValueClass())){
            final WCheckBox editor = new WCheckBox();
            editor.varCheck().sync(property);
            return editor;
        }
        return null;
    }

}
