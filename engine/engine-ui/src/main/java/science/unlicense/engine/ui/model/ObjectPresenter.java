
package science.unlicense.engine.ui.model;

import science.unlicense.engine.ui.widget.Widget;

/**
 * Object presenter.
 * Creates a widget for both view and edition.
 *
 * @author Johann Sorel
 */
public interface ObjectPresenter {

    Widget createWidget(Object candidate);

}
