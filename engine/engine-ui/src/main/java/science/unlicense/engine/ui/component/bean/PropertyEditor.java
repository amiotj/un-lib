
package science.unlicense.engine.ui.component.bean;

import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public interface PropertyEditor {

    Widget create(Property property);

}
