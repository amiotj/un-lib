
package science.unlicense.engine.ui.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventSource;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public interface SpinnerEditor extends EventSource{

    /**
     * Property for the widget edited value change.
     */
    public static final Chars PROPERTY_VALUE = new Chars("Value");

    Widget getWidget();

    Object getValue();

    void setValue(Object value);

}
