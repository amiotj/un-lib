
package science.unlicense.engine.ui.ievent;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.task.Executable;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;

/**
 * Executable associated to ActionEvents.
 *
 * @author Johann Sorel
 */
public interface ActionExecutable extends Executable, EventSource{

    Image getImage(Extent extent);

    Chars getText();

    boolean isActive();

}
