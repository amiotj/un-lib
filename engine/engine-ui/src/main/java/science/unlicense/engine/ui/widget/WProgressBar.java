
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.visual.ProgressBarView;

/**
 * Widget progress bar.
 *
 * @author Johann Sorel
 */
public class WProgressBar extends WLeaf {

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = new Chars("text");
    /**
     * Property for the progress bar undetermined state.
     */
    public static final Chars PROPERTY_UNDETERMINED = new Chars("undetermined");
    /**
     * Property for the progress bar progression state.
     */
    public static final Chars PROPERTY_PROGRESS = new Chars("progress");

    
    public WProgressBar() {
        this(false);
    }

    public WProgressBar(boolean undetermined) {
        setUndetermined(undetermined);
        setView(new ProgressBarView(this));
    }

    /**
     * Get progress text.
     * @return CharArray, can be null
     */
    public CharArray getText() {
        return (CharArray) getPropertyValue(PROPERTY_TEXT);
    }
    
    /**
     * Set progress bar text.
     * 
     * @param text can be null
     */
    public void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT,text);
    }
    
    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varText(){
        return getProperty(PROPERTY_TEXT);
    }
    
    /**
     * Get progress value.
     * @return double, between 0 and 1
     */
    public double getProgress() {
        return (Double)getPropertyValue(PROPERTY_PROGRESS,0.0);
    }

    public void setProgress(double progress){
        setPropertyValue(PROPERTY_PROGRESS,progress,0.0);
    }
    
    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varProgress(){
        return getProperty(PROPERTY_PROGRESS);
    }
    
    /**
     * Set undeterminated state.
     * @param undetermined
     */
    public void setUndetermined(boolean undetermined) {
        setPropertyValue(PROPERTY_UNDETERMINED,undetermined,Boolean.FALSE);
    }

    /**
     * Get undeterminated state.
     * @return boolean true if undeterminated
     */
    public boolean isUndetermined() {
        return (Boolean)getPropertyValue(PROPERTY_UNDETERMINED,Boolean.FALSE);
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varUndetermined(){
        return getProperty(PROPERTY_UNDETERMINED);
    }
    
}