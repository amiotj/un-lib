
package science.unlicense.engine.ui.widget;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * Convinient methods on widgets.
 *
 * @author Johann Sorel
 */
public final class Widgets {

    private Widgets(){}

    /**
     * Forward mouse event to widget.
     * The given mouse event is expected to be in the root coordinate system.
     *
     * @param child
     * @param mevent
     */
    public static Vector convertEvent(Widget child, Event event){
        //clip mouse events to forward to correct childrens
        final MouseMessage mevent = (MouseMessage) event.getMessage();
        final Tuple mpos = mevent.getMousePosition();
        final Tuple spos = mevent.getMouseScreenPosition();
        final double[] mouseCoord = new double[]{mpos.getX(),mpos.getY(),1};

        //test intersection with widget
        child.getRootToNodeSpace().transform(mouseCoord, mouseCoord);
        final Vector mousePos = new Vector(mouseCoord[0], mouseCoord[1]);
        
        final MouseMessage cevent = new MouseMessage(mevent.getType(),
                mevent.getButton(), 
                mevent.getNbClick(),
                mousePos,
                spos,mevent.getWheelOffset(),mevent.isDragging());
        if(mevent.isConsumed()) cevent.consume();
        child.receiveEvent(new Event(event.getSource(), cevent));
        if(cevent.isConsumed()) mevent.consume();
        return mousePos.copy();
    }
    
    /**
     * Forward mouse event to widget if it intersects it's extent.
     *
     * @param parent
     * @param child
     * @param wasInWidget indicate if previous mouse event was in widget,
     *     this might create additional mouse enter or mouse exit events.
     * @param mevent
     * @return true if mousevent position is in widget.
     */
    public static boolean forwardEvent(Widget parent, Widget child, MouseMessage mevent, boolean wasInWidget){
        //clip mouse events to forward to correct childrens
        final Tuple mpos = mevent.getMousePosition();
        final Tuple spos = mevent.getMouseScreenPosition();
        final double[] mouseCoord = new double[]{mpos.getX(),mpos.getY(),1};

        //test intersection with widget
        child.getNodeTransform().inverseTransform(mouseCoord,mouseCoord);
        final Vector mousePos = new Vector(mouseCoord[0], mouseCoord[1]);
        
        final boolean mouseInWidget;
        //we exclude border intersection, must be the same approach as Layout.getPositionalbe(bbox) method
        if(child.getBoundingBox().intersects(new Vector(mousePos),false)){
            //mouse is in the widget
            if(wasInWidget){
                //mouse was already in the widget, just propage the event
                final MouseMessage cevent = new MouseMessage(mevent.getType(),
                        mevent.getButton(),
                        mevent.getNbClick(),mousePos,
                        spos,mevent.getWheelOffset(),mevent.isDragging());
                if(mevent.isConsumed()) cevent.consume();
                child.receiveEvent(new Event(parent,cevent));
                if(cevent.isConsumed()) mevent.consume();
            }else{
                if(mevent.getType()!=MouseMessage.TYPE_ENTER){
                    //mouse was out of widget, we first generate a mouse enter event
                    final MouseMessage enterEvent = new MouseMessage(MouseMessage.TYPE_ENTER, -1, -1,
                            mousePos, spos,0,mevent.isDragging());
                    child.receiveEvent(new Event(parent,enterEvent));
                }
                //then propage the true event
                final MouseMessage tevent = new MouseMessage(mevent.getType(), mevent.getButton(),
                        mevent.getNbClick(),
                        mousePos, spos,mevent.getWheelOffset(),mevent.isDragging());
                if(mevent.isConsumed()) tevent.consume();
                child.receiveEvent(new Event(parent,tevent));
                if(tevent.isConsumed()) mevent.consume();
            }
            mouseInWidget = true;
        }else{
            //mouse is out of widget
            if(wasInWidget){
                //mouse was in the widget, send a mouse exit event
                final MouseMessage eevent = new MouseMessage(MouseMessage.TYPE_EXIT, -1, -1,
                        mousePos, spos,0,mevent.isDragging());
                child.receiveEvent(new Event(parent,eevent));
            }
            mouseInWidget = false;
        }

        return mouseInWidget;
    }

    /**
     * Find the children at given position in the parent.
     *
     * @param parent
     * @param x
     * @param y
     * @return
     */
    public static Widget getChildAt(WContainer parent, double x, double y){

        BBox ext;
        final double[] mousePos = new double[]{x,y};
        final double[] childSpace = new double[2];
        final Vector childPos = new Vector(childSpace);

        final Object[] children = parent.getChildren().toArray();
        for(int i=0;i<children.length;i++){
            final Widget w = (Widget) children[i];
            ext = w.getBoundingBox();
            w.getNodeTransform().inverseTransform(mousePos, childSpace);

            //check if it intersect
            if(ext.intersects(childPos)){
                return w;
            }
        }

        return null;
    }

    public static Widget pickAt(WContainer parent, double x, double y){
        final double[] mousePos = new double[]{x,y,1};
        Widget result = parent;
        
        while(result!=null && result instanceof WContainer){
            final Widget c = getChildAt((WContainer)result, mousePos[0],mousePos[1]);
            if(c==null) break;
            c.getNodeTransform().inverseTransform(mousePos,mousePos);
            result = c;     
        }
        
        return result;
    }

    /**
     * Get the stack of widgets at given position.
     * The first widget in the sequence is the given container.
     *
     * @param parent
     * @param x
     * @param y
     * @return
     */
    public static Sequence pruneStackAt(WContainer parent, double x, double y){
        final double[] mousePos = new double[]{x,y,1};
        final Sequence seq = new ArraySequence();
        Widget result = parent;
        seq.add(result);

        while(result!=null && result instanceof WContainer){
            final Widget c = getChildAt((WContainer)result, mousePos[0],mousePos[1]);
            if(c==null) break;
            c.getNodeTransform().inverseTransform(mousePos,mousePos);
            result = c;
            seq.add(result);
        }

        return seq;
    }
    
}
