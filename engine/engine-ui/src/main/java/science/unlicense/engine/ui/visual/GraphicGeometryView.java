
package science.unlicense.engine.ui.visual;

import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.ShapeStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WGraphicGeometry;
import static science.unlicense.engine.ui.widget.WGraphicGeometry.STYLE_GEOM_PREFIX;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 *
 * @author Johann Sorel
 */
public class GraphicGeometryView extends WidgetView {

    public GraphicGeometryView(WGraphicGeometry widget) {
        super(widget);
    }

    public WGraphicGeometry getWidget() {
        return (WGraphicGeometry) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {

        //get shape size
        final Geometry2D geom = getWidget().getGeometry();
        final Extent extent;
        if(geom!=null){
            final BBox bbox = geom.getBoundingBox();
            extent = new Extent.Double(bbox.getSpan(0),bbox.getSpan(1));
        }else{
            extent = new Extent.Double(0, 0);
        }

        buffer.setAll(extent);
    }

    public void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {

        final ShapeStyle[] geomborder = WidgetStyles.readShapeStyle(getWidget().getStyle(),STYLE_GEOM_PREFIX);

        final Geometry2D baseGeom = getWidget().getGeometry();
        for(ShapeStyle ss : geomborder){
            renderShape(painter, ss, baseGeom);
        }
        
    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        if(WGraphicGeometry.PROPERTY_GEOMETRY.equals(event.getPropertyName())){
            widget.updateExtents();
            widget.setDirty();
        }
        
    }

}
