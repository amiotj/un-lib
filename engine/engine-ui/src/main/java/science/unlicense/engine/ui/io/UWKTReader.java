
package science.unlicense.engine.ui.io;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.number.Float64;
import science.unlicense.api.predicate.Constant;
import science.unlicense.api.predicate.Expression;
import science.unlicense.engine.ui.io.TupleExpression.Add;
import science.unlicense.engine.ui.io.TupleExpression.PercentValue;
import science.unlicense.engine.ui.io.TupleExpression.Sub;
import science.unlicense.impl.model2d.wkt.WKTReader;

/**
 * Extend WKT reader to support value expressions and basic +/- math operators.
 * 
 * @author Johann Sorel
 */
public class UWKTReader extends WKTReader {

    private  Tuple readCoordinateExp() {
        Expression exp0 = readExpression(false);
        Expression exp1 = readExpression(true);
        return new TupleExpression(exp0,exp1);
    }

    @Override
    protected TupleBuffer1D readCoordinates() throws IOException {
        final Sequence coords = new ArraySequence();
        while(true){
            skipBlanks();
            coords.add(readCoordinateExp());
            skipBlanks();
            if(!ite.hasNext()) throw new IOException("No enough characters");
            int c =ite.peekToUnicode();
            if(c != ',') break;
            ite.skip();
        }
        return new TupleBuffer1DExpression(coords);
    }

    private Expression readExpression(boolean vertical){
        skipBlanks();
        
        Expression exp = null;
        
        final double val = Float64.decode(ite, false);
        int c = ite.peekToUnicode();
        if(c=='%'){
            ite.skip();
            exp = new PercentValue(val/100.0,vertical);
            c = ite.peekToUnicode();
        }else{
            exp = new Constant(val);
            c = ite.peekToUnicode();
        }
        if(c=='+'){
            ite.skip();
            exp = new Add(exp,readExpression(vertical));
        }else if(c=='-'){
            ite.skip();
            exp = new Sub(exp,readExpression(vertical));
        }
        
        return exp;
        
    }
    
    
}
