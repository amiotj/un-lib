
package science.unlicense.engine.ui.model;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Set;
import science.unlicense.engine.ui.widget.WSwitch;

/**
 * Handle groups of WCheck  or WSwitch widgets, 
 * to ensure only one at the time can be selected.
 *
 * @author Johann Sorel
 */
public class CheckGroup implements EventListener {

    private final Set checks = new HashSet();

    public CheckGroup() {
    }

    public void add(WCheckBox check){
        if(checks.add(check)){
            check.addEventListener(PropertyMessage.PREDICATE, this);
        }
    }
    
    public void add(WSwitch check){
        if(checks.add(check)){
            check.addEventListener(PropertyMessage.PREDICATE, this);
        }
    }

    public void remove(WCheckBox check){
        if(checks.remove(check)){
            check.removeEventListener(PropertyMessage.PREDICATE, this);
        }
    }
    
    public void remove(WSwitch check){
        if(checks.remove(check)){
            check.removeEventListener(PropertyMessage.PREDICATE, this);
        }
    }

    public void receiveEvent(Event event) {
        final PropertyMessage pe = (PropertyMessage) event.getMessage();
        if(!pe.getPropertyName().equals(WCheckBox.PROPERTY_CHECK)) return;
        final Object source = event.getSource();
        if(pe.getNewValue() == Boolean.TRUE){
            //unselect other checks
            final Iterator ite = checks.createIterator();
            while(ite.hasNext()){
                final Object o = ite.next();
                if(o instanceof WCheckBox){
                    final WCheckBox c = (WCheckBox)o;
                    if(!c.equals(source)){
                        c.setCheck(false);
                    }
                }else if(o instanceof WSwitch){
                    final WSwitch c = (WSwitch)o;
                    if(!c.equals(source)){
                        c.setCheck(false);
                    }
                }
                
            }
        }
    }

}
