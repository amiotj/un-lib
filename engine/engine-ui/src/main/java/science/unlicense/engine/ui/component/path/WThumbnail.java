
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lang.Reflection;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.StackConstraint;
import science.unlicense.api.layout.StackLayout;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.store.Format;
import science.unlicense.api.store.Formats;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.logging.Loggers;
import science.unlicense.engine.ui.visual.ContainerView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class WThumbnail extends WLabel {

    public static final Chars PROPERTY_SELECTED = new Chars("Selected");
    
    private static final Extent THUMBNAIL_SIZE = new Extent.Double(128, 128);
    private static final Chars CACHE_FORMAT = new Chars("png");
    
    private final Path path;
    private volatile boolean updatingImage = false;
    private final boolean useCache;
    private final boolean usePreview;
    private final Chars cacheFormat;
    private final Predicate filter;

    public WThumbnail(Path path, boolean useCache, Chars cacheFormat) {
        this(path, useCache, cacheFormat, true, true, null);
    }

    public WThumbnail(Path path, boolean useCache, Chars cacheFormat, boolean showText, boolean preview, Predicate filter) {
        setView(new PreviewView(this));
        if(cacheFormat==null) cacheFormat = CACHE_FORMAT;
        if(filter==null) filter = Predicate.TRUE;
        
        this.path = path;
        this.useCache = useCache;
        this.cacheFormat = cacheFormat;
        this.usePreview = preview;
        this.filter = filter;
        setVerticalAlignment(WLabel.VALIGN_CENTER);
        setHorizontalAlignment(WLabel.HALIGN_CENTER);
        setGraphicPlacement(WLabel.GRAPHIC_TOP);
        if (showText) {
            setText(new Chars(path.getName()));
        }
        addEventListener(MouseMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if (me.getType() == MouseMessage.TYPE_TYPED) {
                    setSelected(!isSelected());
                }
            }
        });
    }

    public boolean isSelected() {
        return (Boolean) getPropertyValue(PROPERTY_SELECTED, Boolean.FALSE);
    }

    public void setSelected(boolean selected) {
        setPropertyValue(PROPERTY_SELECTED, selected, Boolean.FALSE);
    }

    public Path getPath() {
        return path;
    }

    synchronized void updateImage() {
        if (updatingImage) {
            return;
        }
        updatingImage = true;
        new Thread() {
            public void run() {
                
                try {
                    if(path.isContainer()){

                        if(usePreview){
                            final WLeaf leaf = new WSpace(1,1);
                            leaf.getStyle().getSelfRule().setProperties(WPathChooser.STYLE_FOLDER);
                            leaf.getStyle().getSelfRule().setProperties(new Chars("margin:3"));
                            final WContainer cnt = new WContainer(new StackLayout());
                            cnt.addChild(leaf, new StackConstraint(0));
                            setGraphic(cnt);

                            final Path p = searchMostRepresentative(path.getChildren());
                            if(p != null){
                                final Widget gra = createGraphic(p, useCache);
                                gra.getStyle().getSelfRule().setProperties(new Chars("margin:[30,3,3,22]"));
                                cnt.addChild(gra, new StackConstraint(1));
                            }
                        }else{
                            final WLeaf leaf = new WSpace(16,16);
                            leaf.getStyle().getSelfRule().setProperties(WPathChooser.STYLE_FOLDER);
                            leaf.getStyle().getSelfRule().setProperties(new Chars("margin:0"));
                            setGraphic(leaf);
                        }
                    }else{
                        if(usePreview){
                            setGraphic(createGraphic(path, useCache));
                        }else{
                            final WLeaf leaf = new WSpace(16,16);
                            leaf.getStyle().getSelfRule().setProperties(WPathChooser.STYLE_FILE);
                            leaf.getStyle().getSelfRule().setProperties(new Chars("margin:0"));
                            setGraphic(leaf);
                        }
                    }
                } catch (IOException ex) {
                    Loggers.get().info(ex);
                }
            }
        }.start();
    }

    private Widget createGraphic(Path path, boolean useCache){
        
        //try to load from cache first
        Path thumbPath = null;
        Image img = null;
        if (useCache) {
            try {
                thumbPath = PathPresenters.createThumbnailPath(path);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                img = Images.read(thumbPath);
            } catch (Exception ex) {
            }
        }

        generateImage:
        if (img == null) {
            //generate and save the thumbnail
            img = PathPresenters.generateImage(path, THUMBNAIL_SIZE);
            if (useCache && img != null) {
                try {
                    Images.write(img, cacheFormat, thumbPath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        if(img==null){
            final WLeaf leaf = new WSpace(1,1);
            leaf.getStyle().getSelfRule().setProperties(WPathChooser.STYLE_FILE);
            leaf.getStyle().getSelfRule().setProperties(new Chars("margin:3"));
            return leaf;
        }else{
            final WGraphicImage graphic = new WGraphicImage();
            Extents overrideExtents = graphic.getOverrideExtents();
            overrideExtents.minX = 1; overrideExtents.minY = 1;
            overrideExtents.maxX = Double.MAX_VALUE; overrideExtents.maxY = Double.MAX_VALUE;
            graphic.setOverrideExtents(overrideExtents);
            graphic.setFitting(WGraphicImage.FITTING_SCALED);
            graphic.setImage(img);
            return graphic;
        }

    }
        
    /**
     * Search the most representative path in the array.
     * This method checks if the paths are images, medias or 3d models.
     * Priority is given in the order :
     * - 3d model
     * - media
     * - image
     * 
     * if multiple instances exist then the first one is used.
     * 
     * @param paths
     * @return 
     */
    private Path searchMostRepresentative(Collection paths) throws IOException{

        Path candidate = null;
        Chars extension;
        Format format;
        boolean isMediaFormat = false;
        boolean isImageFormat = false;

        final Iterator ite = paths.createIterator();
        while (ite.hasNext()) {
            final Path p = (Path) ite.next();
            if(p.isContainer() || !filter.evaluate(p) ) continue;
            extension = Paths.getExtension(new Chars(p.getName()));
            try{
                format = Formats.getFormatForExtension(extension);
            }catch(InvalidArgumentException ex){continue;}
            if(isModel3DFormat(format)) return p;
            else if(isMediaFormat(format) && !isMediaFormat){ candidate = p; isMediaFormat=true;}
            else if(isImageFormat(format) && !isImageFormat){ candidate = p; isImageFormat=true;}
        }
        return candidate;
    }

    private static boolean isImageFormat(Format format){
        if(format==null) return false;
        final Sequence classes = Reflection.getInterfaces(format.getClass());
        for(int i=0,n=classes.getSize();i<n;i++){
            if(((Class)classes.get(i)).getSimpleName().equals("ImageFormat")) return true;
        }
        return false;
    }

    private static boolean isMediaFormat(Format format){
        if(format==null) return false;
        final Sequence classes = Reflection.getInterfaces(format.getClass());
        for(int i=0,n=classes.getSize();i<n;i++){
            if(((Class)classes.get(i)).getSimpleName().equals("MediaFormat")) return true;
        }
        return false;
    }

    private static boolean isModel3DFormat(Format format){
        if(format==null) return false;
        final Sequence classes = Reflection.getInterfaces(format.getClass());
        for(int i=0,n=classes.getSize();i<n;i++){
            if(((Class)classes.get(i)).getSimpleName().equals("Model3DFormat")) return true;
        }
        return false;
    }

    private final class PreviewView extends ContainerView{

        public PreviewView(WThumbnail labeled) {
            super(labeled);
        }

        protected void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {
            super.renderSelf(painter, dirtyBBox, innerBBox);
            if(!updatingImage){
                ((WThumbnail)getWidget()).updateImage();
            }
        }
        
    }
}
