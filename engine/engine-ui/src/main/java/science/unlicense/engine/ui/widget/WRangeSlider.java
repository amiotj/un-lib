package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.model.SliderModel;
import science.unlicense.engine.ui.visual.RangeSliderView;

/**
 * 
 * @author Johann Sorel
 */
public class WRangeSlider extends WLeaf{
    
    public static final Chars PROPERTY_VALUE_MIN = new Chars("ValueMin");
    public static final Chars PROPERTY_VALUE_MAX = new Chars("ValueMax");
    public static final Chars PROPERTY_INNER_RANGE = new Chars("InnerRange");
    public static final Chars PROPERTY_MODEL = new Chars("Model");

    public WRangeSlider(SliderModel model) {
        this(model,model.getDefaultValue());
    }

    public WRangeSlider(SliderModel model, Object value) {
        setPropertyValue(PROPERTY_MODEL, model);
        setPropertyValue(PROPERTY_VALUE_MIN, value);
        setPropertyValue(PROPERTY_VALUE_MAX, value);
        setView(new RangeSliderView(this));
    }

    public SliderModel getModel() {
        return (SliderModel) getPropertyValue(PROPERTY_MODEL);
    }

    public void setModel(SliderModel model) {
        setPropertyValue(PROPERTY_MODEL, model);
    }
    
    public Object getValueMin() {
        return getPropertyValue(PROPERTY_VALUE_MIN);
    }

    public void setValueMin(Object value) {
        setPropertyValue(PROPERTY_VALUE_MIN, value);
    }
    /**
     * Get value min variable.
     * @return Variable.
     */
    public Variable varValueMin(){
        return getProperty(PROPERTY_VALUE_MIN);
    }
    
    public Object getValueMax() {
        return getPropertyValue(PROPERTY_VALUE_MAX);
    }

    public void setValueMax(Object value) {
        setPropertyValue(PROPERTY_VALUE_MAX, value);
    }
    
    /**
     * Get value max variable.
     * @return Variable.
     */
    public Variable varValueMax(){
        return getProperty(PROPERTY_VALUE_MAX);
    }
    
    public boolean isInnerRange() {
        return (Boolean)getPropertyValue(PROPERTY_INNER_RANGE,Boolean.TRUE);
    }

    public void setInnerRange(boolean inner) {
        setPropertyValue(PROPERTY_INNER_RANGE, inner,Boolean.TRUE);
    }
    
    /**
     * Get inner range variable.
     * @return Variable.
     */
    public Variable varInnerRange(){
        return getProperty(PROPERTY_INNER_RANGE);
    }

}
