
package science.unlicense.engine.ui.widget;

import science.unlicense.api.anim.DefaultTimer;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.layout.AbsoluteLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.PairLayout;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.impl.anim.TransformAnimation;
import science.unlicense.impl.anim.TransformTimeSerieHelper;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;

/**
 * Widget loader.
 *
 * @author Johann Sorel
 */
public class WLoader extends WContainer{

    public static final int GRAPHIC_TOP = 0;
    public static final int GRAPHIC_RIGHT = 1;
    public static final int GRAPHIC_BOTTOM = 2;
    public static final int GRAPHIC_LEFT = 3;

    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;

    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = new Chars("Text");
    /**
     * Property for the widget image.
     */
    public static final Chars PROPERTY_GRAPHIC = new Chars("Graphic");
    /**
     * Property for the image placement.
     */
    public static final Chars PROPERTY_IMAGE_PLACEMENT = new Chars("GraphicPlacement");
    /**
     * Property for the horizontal alignement.
     */
    public static final Chars PROPERTY_HALIGN = new Chars("HorizontalAlignment");
    /**
     * Property for the vertical alignment.
     */
    public static final Chars PROPERTY_VALIGN = new Chars("VerticalAlignment");

    private final WGraphicText textWidget;
    private final WGraphicGeometry imgWidget;
    private final PairLayout pairLayout;

    public WLoader() {
        this(null);
    }

    public WLoader(CharArray text) {
        textWidget = new WGraphicText(text);

        final WContainer freePane = new WContainer(new AbsoluteLayout());
        freePane.setOverrideExtents(new Extents(30, 30));

        final Font font = new Font(new Chars[]{new Chars("Unicon")}, 48, Font.WEIGHT_NONE);
        Geometry2D glyph = FontContext.INSTANCE.getGlyph(font, '\uE029');
        Transform mtr = Projections.scaled(glyph.getBoundingBox(), new BBox(new double[]{-15,-15}, new double[]{15,15}));
        glyph = new TransformedGeometry2D(glyph, mtr);
        imgWidget = new WGraphicGeometry();
        imgWidget.setGeometry(glyph);
        imgWidget.setOverrideExtents(new Extents(30, 30));
        freePane.getChildren().add(imgWidget);

        TransformTimeSerieHelper helper = new TransformTimeSerieHelper(2);
        TransformAnimation trs = helper.on(imgWidget).repeat(0)
                .at(0).rotation(Maths.HALF_PI)
                .at(1000).rotation(Maths.HALF_PI)
                .at(1500).rotation(Maths.HALF_PI)
                .at(2000).rotation(Maths.HALF_PI)
                .build();
        trs.setTimer(new DefaultTimer(50));
        trs.start();

        pairLayout = new PairLayout();
        setLayout(pairLayout);
        if(text!=null && !text.isEmpty()){
            children.add(textWidget);
        }
        children.add(freePane);
    }

    /**
     * Get label text.
     * @return CharArray, can be null
     */
    public CharArray getText() {
        return textWidget == null ? null : textWidget.getText();
    }

    /**
     * Set label text.
     * @param text can be null
     */
    public void setText(CharArray text) {
        textWidget.setText(text);
        if(text==null || text.isEmpty()){
            children.remove(textWidget);
            textWidget.setText(text);
        }else{
            textWidget.setText(text);
            if(getChildren().getSize() == 0 || getChildren().get(0) != textWidget){
                children.add(0,textWidget);
            }
        }
    }

    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varText(){
        return getProperty(PROPERTY_TEXT);
    }

    /**
     * Get graphic placement relative to text
     * @return int
     */
    public int getGraphicPlacement() {
        return pairLayout.getRelativePosition();
    }

    /**
     * Set graphic placement relative to text
     * @param place
     */
    public void setGraphicPlacement(int place) {
        pairLayout.setRelativePosition(place);
    }

    /**
     * Get text and graphic horizontal alignment.
     * @return int
     */
    public int getHorizontalAlignment() {
        return pairLayout.getHorizontalAlignement();
    }

    /**
     * Set text and graphic horizontal alignment.
     * @param align
     */
    public void setHorizontalAlignment(int align) {
       pairLayout.setHorizontalAlignement(align);
    }

    /**
     * Get text and graphic vertical alignment.
     * @return int
     */
    public int getVerticalAlignment() {
        return pairLayout.getVerticalAlignement();
    }

    /**
     * Set text and graphic vertical alignment.
     * @param align
     */
    public void setVerticalAlignment(int align) {
        pairLayout.setVerticalAlignement(align);
    }

    @Override
    protected void receiveEventChildren(Event event) {
        super.receiveEventChildren(event);

        //catch and forward text and image events
        if(event.getSource() == textWidget
                && event.getMessage() instanceof PropertyMessage
                && ((PropertyMessage)event.getMessage()).getPropertyName() == WGraphicText.PROPERTY_TEXT){
            //TODO resend event
            //TODO do the same with image and layout properties
        }

    }
    
}