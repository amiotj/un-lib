
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.LineLayout;
import science.unlicense.api.model.tree.Node;

/**
 * Container, displaying tabs for each child component.
 *
 * @author Johann Sorel
 */
public class WTabContainer extends WContainer {

    public static final int TAB_POSITION_TOP = 0;
    public static final int TAB_POSITION_BOTTOM = 1;
    public static final int TAB_POSITION_LEFT = 2;
    public static final int TAB_POSITION_RIGHT = 3;

    /**
     * Property for the active ab index.
     */
    public static final Chars PROPERTY_ACTIVE_TAB = new Chars("tabActive");
    /**
     * Property for the tabs position.
     */
    public static final Chars PROPERTY_TAB_POSITION = new Chars("TabPosition");
    /**
     * Property for the active tab state.
     */
    public static final Chars PROPERTY_ACTIVE = new Chars("Active");


    /** tab border : BorderStyle class */
    public static final Chars STYLE_PROP_TAB_PREFIX = new Chars("tab-");
    /** tab inner margin */
    public static final Chars STYLE_PROP_TAB_MARGIN = new Chars("tab-inner-margin");

    private int tabActive = -1;
    private int tabPosition = TAB_POSITION_TOP;
    final Sequence tabs = new ArraySequence();
    private final WTabBar tabBar = new WTabBar();
    private final WContainer center = new WContainer(new BorderLayout());

    public WTabContainer() {
        super(new BorderLayout());
        addChild(tabBar,BorderConstraint.TOP);
        addChild(center,BorderConstraint.CENTER);
    }

    public void addTab(Widget child, Widget tabHeader){
        addTab(child, tabHeader, getChildren().getSize());
    }

    public void addTab(Widget child, Widget tabHeader, int index){
        final WTab tab = new WTab(tabHeader, child);
        tabs.add(tab);
        tabBar.getChildren().removeAll();
        
        for(int i=0,n=tabs.getSize();i<n;i++){
            final WTab tabi = (WTab) tabs.get(i);
            tabi.setTabPosition(tabPosition);
            tabBar.getChildren().add(tabi);
        }
        
        //first tab added, activate it
        if(tabs.getSize()==1){
            setActiveTab(0);
        }
    }

    public void removeTab(Widget content){
        //TODO
    }
    
    public void removeTabs(){
        tabBar.getChildren().removeAll();
        tabs.removeAll();
        setActiveTab(-1);
        
    }
    
    /**
     * Set active tab.
     * @param visibletab set visible tab
     */
    public void setActiveTab(int visibletab) {
        if(this.tabActive == visibletab) return;
        if(visibletab>=tabs.getSize()){
            throw new InvalidArgumentException("Invalid tab index "+visibletab+" only have "+tabs.getSize()+" tabs");
        }
        
        for(int i=0,n=tabs.getSize();i<n;i++){
            final WTab tab = (WTab) tabs.get(i);
            tab.setActive(visibletab==i);
        }
        
        this.tabActive = visibletab;
        
        if(visibletab>=0){
            ((WTab)tabs.get(visibletab)).tabContent.setLayoutConstraint(BorderConstraint.CENTER);
            center.getChildren().replaceAll(new Node[]{((WTab)tabs.get(visibletab)).tabContent});
        }else{
            center.getChildren().removeAll();
        }
    }

    /**
     * Get active tab index.
     * @return int, can be negative if no tab active
     */
    public int getActiveTab(){
        return tabActive;
    }

    /**
     * Set tab position.
     * @param tabposition set tab position, one of TAB_POSITION_X
     */
    public void setTabPosition(int tabposition) {
        if(this.tabPosition != tabposition){
            final int oldValue = this.tabPosition;
            this.tabPosition = tabposition;
            sendPropertyEvent(this,PROPERTY_TAB_POSITION, oldValue, tabposition);
        }
        
        children.remove(tabBar);
        final LineLayout tabLayout = (LineLayout) tabBar.getLayout();
        
        
        if(tabposition==TAB_POSITION_BOTTOM){
            tabLayout.setDirection(LineLayout.DIRECTION_HORIZONTAL);
            tabLayout.setVerticalAlignement(LineLayout.VALIGN_TOP);
            addChild(tabBar, BorderConstraint.BOTTOM);
        }else if(tabposition==TAB_POSITION_TOP){
            tabLayout.setDirection(LineLayout.DIRECTION_HORIZONTAL);
            tabLayout.setVerticalAlignement(LineLayout.VALIGN_BOTTOM);
            addChild(tabBar, BorderConstraint.TOP);
        }else if(tabposition==TAB_POSITION_LEFT){
            tabLayout.setDirection(LineLayout.DIRECTION_VERTICAL);
            tabLayout.setHorizontalAlignement(LineLayout.HALIGN_RIGHT);
            addChild(tabBar, BorderConstraint.LEFT);
        }else if(tabposition==TAB_POSITION_RIGHT){
            tabLayout.setDirection(LineLayout.DIRECTION_VERTICAL);
            tabLayout.setHorizontalAlignement(LineLayout.HALIGN_LEFT);
            addChild(tabBar, BorderConstraint.RIGHT);
        }
        
        tabBar.fireTabEvent();
    }

    /**
     * Get tab position.
     * @return int, one of TAB_POSITION_X
     */
    public int getTabPosition(){
        return tabPosition;
    }

    public final class WTabBar extends WContainer {
        private WTabBar(){
            super(new LineLayout());
            final LineLayout fl = (LineLayout) getLayout();
            fl.setDistance(4);
            fl.setVerticalAlignement(LineLayout.VALIGN_BOTTOM);
        }
        
        public int getTabPosition(){
            return tabPosition;
        }
        
        private void fireTabEvent(){
            sendPropertyEvent(WTabBar.this, PROPERTY_TAB_POSITION, 0, tabPosition);
        }
    }
    
    public final class WTab extends WContainer {

        private final Widget tabHeader;
        final Widget tabContent;
        private boolean active = false;
        private int position;

        public WTab(Widget tabHeader, Widget tabContent) {
            super.setLayout(new BorderLayout());
            this.tabHeader = tabHeader;
            this.tabContent = tabContent;
            addChild(tabHeader, BorderConstraint.CENTER);
        }

        public Widget getTabHeader() {
            return tabHeader;
        }

        public Widget getTabContent() {
            return tabContent;
        }

        public boolean isActive() {
            return active;
        }

        public int getTabPosition() {
            return position;
        }

        void setTabPosition(int position) {
            if (this.position == position) {
                return;
            }
            final int old = this.position;
            this.position = position;
            WTab.this.sendPropertyEvent(WTab.this, WTabContainer.PROPERTY_TAB_POSITION, old, position);
        }

        public void setActive(boolean active) {
            if (this.active == active) {
                return;
            }
            this.active = active;
            WTab.this.sendPropertyEvent(WTab.this, WTabContainer.PROPERTY_ACTIVE, !active, active);
        }

        protected void receiveEventParent(Event event) {
            //catch event, pass to children
            super.receiveEventParent(event);
            //if it is not consumed and is a mouse left click we activate this tab
            if (event.getMessage() instanceof MouseMessage && !event.getMessage().isConsumed()) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if (me.getButton() == MouseMessage.BUTTON_1) {
                    me.consume();
                    setActiveTab(tabs.search(WTab.this));
                }
            }
        }

    }

}