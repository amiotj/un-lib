

package science.unlicense.engine.ui.io;

import science.unlicense.api.CObject;
import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.model.tree.Nodes;

/**
 *
 * @author Johann Sorel
 */
public class EqualsPredicate extends CObject implements Predicate {

    public static final Chars CLASS = new Chars("Class",CharEncodings.US_ASCII);
    
    private final Expression exp1;
    private final Expression exp2;

    public EqualsPredicate(Expression exp1, Expression exp2) {
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public Expression getExp1() {
        return exp1;
    }

    public Expression getExp2() {
        return exp2;
    }

    public Chars getPropertyName() {
        return ((PropertyName)exp1).getName();
    }
    
    public Boolean evaluate(Object candidate) {
        if(candidate==null) return false;
        
        final Object obj1 = exp1.evaluate(candidate);
        final Object obj2 = exp2.evaluate(candidate);
        
        if(obj1 instanceof Class){
            return ((Class)obj2).isAssignableFrom((Class)obj1);
        }
        
        if(obj1 instanceof Number && obj2 instanceof Number){
            return ((Number)obj1).doubleValue() == ((Number)obj2).doubleValue();
        }
        
        return CObjects.equals(obj1, obj2);
    }

    public Chars toChars() {
        return Nodes.toChars(new Chars("Equals"), new Object[]{exp1,exp2});
    }

    public int getHash() {
        int hash = 7;
        hash = 29 * hash + (this.exp1 != null ? this.exp1.hashCode() : 0);
        hash = 29 * hash + (this.exp2 != null ? this.exp2.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EqualsPredicate other = (EqualsPredicate) obj;
        if (this.exp1 != other.exp1 && (this.exp1 == null || !this.exp1.equals(other.exp1))) {
            return false;
        }
        if (this.exp2 != other.exp2 && (this.exp2 == null || !this.exp2.equals(other.exp2))) {
            return false;
        }
        return true;
    }

    
    
}
