
package science.unlicense.engine.ui.visual;

import science.unlicense.api.event.EventListener;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.painter2d.Painter2D;

/**
 *
 * @author Johann Sorel
 */
public interface View extends EventListener{

    /**
     * Render current widget.
     *
     * @param painter
     * @param dirtyBBox
     */
    void render(Painter2D painter, BBox dirtyBBox);

    void renderEffects(Painter2D painter);
    
    /**
     * Calculate widget minimum, best and maximum size.
     *
     */
    void getExtents(Extents buffer, Extent constraint);
                
    /**
     * For the current size, get the visual extent.
     * The visual extent may be larger or smaller, this allows widgets to overlaps.
     * 
     * @param buffer 
     */
    BBox calculateVisualExtent(BBox buffer);

    /**
     * Release any used resources.
     * This method is called before view is removed from a widget.
     */
    void dispose();
    
}
