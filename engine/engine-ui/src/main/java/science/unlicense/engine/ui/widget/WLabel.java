package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;

/**
 * Widget displaying a text message and optional image.
 *
 * @author Johann Sorel
 */
public class WLabel extends AbstractLabeled{

    public WLabel() {
        this(null,null);
    }

    public WLabel(CharArray text) {
        this(text,null);
    }
    
    public WLabel(CharArray text, Widget graphic) {
        super(text,graphic);
        setHorizontalAlignment(HALIGN_LEFT);
    }

}
