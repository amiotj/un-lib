
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.api.desktop.FrameDecoration;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Layout;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.layout.Positionable;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.api.desktop.Frame;

/**
 * Frame decoration using a border layout container.
 * 
 * @author Johann Sorel
 */
public class WFrameDecoration implements FrameDecoration {

    protected final WContainer content;
    private Frame frame;
    
    public WFrameDecoration() {
        content = new WContainer(new BorderLayout()){
            public void setLayout(Layout layout) {
                if(!(layout instanceof BorderLayout)){
                    throw new RuntimeException("You are not allowed to modified the layout typeof a frame decoration, only BorderLayout are allowed.");
                }
                super.setLayout(layout);
            }
        };
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public WContainer getWidget() {
        return content;
    }

    @Override
    public Margin getMargin() {
        final BorderLayout layout = (BorderLayout) content.getLayout();
        final Positionable top = layout.getTop();
        final Positionable bottom = layout.getBottom();
        final Positionable left = layout.getLeft();
        final Positionable right = layout.getRight();
        double mtop     = (top==null) ? 0 : top.getExtents(null, null).bestY;
        double mbottom  = (bottom==null) ? 0 : bottom.getExtents(null, null).bestY;
        double mleft    = (left==null) ? 0 : left.getExtents(null, null).bestY;
        double mright   = (right==null) ? 0 : right.getExtents(null, null).bestY;
        return new Margin(mtop,mright,mbottom,mleft);
    }
}
