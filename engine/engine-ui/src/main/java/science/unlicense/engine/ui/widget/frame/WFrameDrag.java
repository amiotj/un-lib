
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.layout.Layout;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.impl.math.Vector;

/**
 * Widget container which catch mouse event to drag the current frame. 
 * 
 * @author Johann Sorel
 */
public class WFrameDrag extends WContainer {

    private final Vector realPosition = new Vector(2);
    private final Vector borderDist = new Vector(2);
    private boolean dragging = false;

    public WFrameDrag() {}

    public WFrameDrag(Layout layout) {
        super(layout);
    }
    
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        final EventMessage message = event.getMessage();
        if(message.isConsumed() || !(message instanceof MouseMessage) ) return;
        
        final MouseMessage e = (MouseMessage) message;
        final int type = e.getType();

        final int mousebutton = e.getButton();
        if(MouseMessage.TYPE_PRESS==type){
            message.consume();
            dragging = true;
            borderDist.set(e.getMouseScreenPosition());
            borderDist.localSubtract(getFrame().getOnScreenLocation());
            requestFocus();
        }else if(MouseMessage.TYPE_RELEASE==type || MouseMessage.TYPE_EXIT==type){
            dragging = false;
            
        }else if(MouseMessage.TYPE_MOVE==type && dragging && mousebutton==1){
            message.consume();
            realPosition.set(e.getMouseScreenPosition());
            getFrame().setOnScreenLocation(realPosition.subtract(borderDist));
        }
    }
    
}
