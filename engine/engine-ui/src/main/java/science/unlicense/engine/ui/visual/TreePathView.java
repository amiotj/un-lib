
package science.unlicense.engine.ui.visual;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.model.WTreePath;
import static science.unlicense.engine.ui.model.WTreePath.DEPTH;
import static science.unlicense.engine.ui.model.WTreePath.TYPE_END;
import static science.unlicense.engine.ui.model.WTreePath.TYPE_LINE;
import static science.unlicense.engine.ui.model.WTreePath.TYPE_MIDDLE;
import science.unlicense.engine.ui.style.SystemStyle;
import static science.unlicense.engine.ui.style.WidgetStyles.asPaint;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class TreePathView extends WidgetView{

    private final TupleRW lineStart = new DefaultTuple(2);
    private final TupleRW lineEnd = new DefaultTuple(2);
    private final Segment line = new Segment(lineStart, lineEnd);
    
    public TreePathView(WTreePath widget) {
        super(widget);
    }

    public WTreePath getWidget() {
        return (WTreePath) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {
        final int[] depths = getWidget().getDepths();
        if(depths == null){
            buffer.setAll(0, DEPTH);
        }else{
            buffer.setAll(depths.length*DEPTH, DEPTH);
        }
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    protected void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox) {

        final int[] depths = getWidget().getDepths();
        final BBox bbox = widget.getBoundingBox();
        final double minx = bbox.getMin(0);
        final double miny = bbox.getMin(1);
        final double maxx = bbox.getMax(0);
        final double maxy = bbox.getMax(1);
        final Paint color = asPaint(getWidget().getStyle().getPropertyValue(getWidget().getStyle().getStackProperties(),SystemStyle.COLOR_DELIMITER));
        painter.setPaint(color);
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));

        //draw the graph visual
        for(int i=0;i<depths.length;i++){
            if(depths[i]==TYPE_MIDDLE){
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(miny);
                lineEnd.setX(minx + i*DEPTH + (DEPTH/2));
                lineEnd.setY(maxy);
                painter.stroke(line);
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(0);
                lineEnd.setX(minx + i*DEPTH + DEPTH);
                lineEnd.setY(0);
                painter.stroke(line);
            }else if(depths[i]==TYPE_LINE){
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(miny);
                lineEnd.setX(minx + i*DEPTH + (DEPTH/2));
                lineEnd.setY(maxy);
                painter.stroke(line);
            }else if(depths[i]==TYPE_END){
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(miny);
                lineEnd.setX(minx + i*DEPTH + (DEPTH/2));
                lineEnd.setY(0);
                painter.stroke(line);
                lineStart.setX(minx + i*DEPTH + (DEPTH/2));
                lineStart.setY(0);
                lineEnd.setX(minx + i*DEPTH + DEPTH);
                lineEnd.setY(0);
                painter.stroke(line);
            }
        }
    }
}
