

package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.layout.PairLayout;
import science.unlicense.api.predicate.Variable;

/**
 * Internal class, parent of Label and button.
 * 
 * @author Johann Sorel
 */
public class AbstractLabeled extends WContainer{

    public static final int GRAPHIC_TOP = 0;
    public static final int GRAPHIC_RIGHT = 1;
    public static final int GRAPHIC_BOTTOM = 2;
    public static final int GRAPHIC_LEFT = 3;
    
    public static final int HALIGN_LEFT = 0;
    public static final int HALIGN_CENTER = 1;
    public static final int HALIGN_RIGHT = 2;
    
    public static final int VALIGN_TOP = 0;
    public static final int VALIGN_CENTER = 1;
    public static final int VALIGN_BOTTOM = 2;
    
    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = new Chars("Text");
    /**
     * Property for the widget image.
     */
    public static final Chars PROPERTY_GRAPHIC = new Chars("Graphic");
    /**
     * Property for the image placement.
     */
    public static final Chars PROPERTY_IMAGE_PLACEMENT = new Chars("GraphicPlacement");
    /**
     * Property for the horizontal alignement.
     */
    public static final Chars PROPERTY_HALIGN = new Chars("HorizontalAlignment");
    /**
     * Property for the vertical alignment.
     */
    public static final Chars PROPERTY_VALIGN = new Chars("VerticalAlignment");

    private final WGraphicText textWidget;
    private Widget graphicWidget;
    private final PairLayout pairLayout;
    
    public AbstractLabeled() {
        this(null);
    }

    public AbstractLabeled(CharArray text) {
        this(text,null);
    }
    
    public AbstractLabeled(CharArray text, Widget graphic) {
        textWidget = new WGraphicText(text);
        pairLayout = new PairLayout();
        setLayout(pairLayout);
        if(text!=null && !text.isEmpty()){
            children.add(textWidget);
        }
        if(graphic!=null){
            this.graphicWidget = graphic;
            children.add(graphic);
        }
    }

    /**
     * Get label text.
     * @return CharArray, can be null
     */
    public CharArray getText() {
        return textWidget == null ? null : textWidget.getText();
    }

    /**
     * Set label text.
     * @param text can be null
     */
    public void setText(CharArray text) {
        textWidget.setText(text);
        if(text==null || text.isEmpty()){
            children.remove(textWidget);
            textWidget.setText(text);
        }else{
            textWidget.setText(text);
            if(getChildren().getSize() == 0 || getChildren().get(0) != textWidget){
                children.add(0,textWidget);
            }
        }
    }
    
    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varText(){
        return getProperty(PROPERTY_TEXT);
    }

    /**
     * Get label graphic.
     * @return Widget, can be null
     */
    public Widget getGraphic() {
        return graphicWidget;
    }

    /**
     * Set label graphic.
     * @param graphic can be null
     */
    public void setGraphic(Widget graphic) {
        children.remove(graphicWidget);
        graphicWidget = graphic;
        if(graphicWidget!=null){
            children.add(graphicWidget);
        }
    }
    
    /**
     * Get image variable.
     * @return Variable.
     */
    public Variable varGraphic(){
        return getProperty(PROPERTY_GRAPHIC);
    }
    
    /**
     * Get graphic placement relative to text
     * @return int
     */
    public int getGraphicPlacement() {
        return pairLayout.getRelativePosition();
    }

    /**
     * Set graphic placement relative to text
     * @param place
     */
    public void setGraphicPlacement(int place) {
        pairLayout.setRelativePosition(place);
    }
    
    /**
     * Get text and graphic horizontal alignment.
     * @return int
     */
    public int getHorizontalAlignment() {
        return pairLayout.getHorizontalAlignement();
    }

    /**
     * Set text and graphic horizontal alignment.
     * @param align
     */
    public void setHorizontalAlignment(int align) {
       pairLayout.setHorizontalAlignement(align);
    }
    
    /**
     * Get text and graphic vertical alignment.
     * @return int
     */
    public int getVerticalAlignment() {
        return pairLayout.getVerticalAlignement();
    }

    /**
     * Set text and graphic vertical alignment.
     * @param align
     */
    public void setVerticalAlignment(int align) {
        pairLayout.setVerticalAlignement(align);
    }

    @Override
    protected void receiveEventChildren(Event event) {
        super.receiveEventChildren(event);
        
        //catch and forward text and image events
        if(event.getSource() == textWidget 
                && event.getMessage() instanceof PropertyMessage
                && ((PropertyMessage)event.getMessage()).getPropertyName() == WGraphicText.PROPERTY_TEXT){
            //TODO resend event
            //TODO do the same with image and layout properties
        }
        
    }
    
    
    
}
