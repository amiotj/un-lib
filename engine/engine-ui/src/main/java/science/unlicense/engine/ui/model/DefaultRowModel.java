
package science.unlicense.engine.ui.model;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;

/**
 * Default row model, backed by a Sequence.
 * @author Johann Sorel
 */
public class DefaultRowModel extends AbstractEventSource implements RowModel,EventListener {

    private final Sequence elements;

    public DefaultRowModel() {
        this(new ArraySequence());
    }

    public DefaultRowModel(Sequence elements) {
        this.elements = elements;
        this.elements.addEventListener(CollectionMessage.PREDICATE, this);
    }

    public Sequence asSequence() {
        return elements;
    }

    public int getSize() {
        return elements.getSize();
    }

    public Object getElement(int index) {
        return elements.get(index);
    }

    public void receiveEvent(Event event) {
        //sequence elements has changed, forward event to widget
        if(hasListeners()) getEventManager().sendEvent(event);
    }

}
