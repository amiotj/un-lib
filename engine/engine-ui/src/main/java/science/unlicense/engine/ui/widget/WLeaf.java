
package science.unlicense.engine.ui.widget;

import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Tuple;

/**
 * a leaf widget, can not have any children.
 * @author Johann Sorel
 */
public abstract class WLeaf extends AbstractWidget{

    public WLeaf() {
        super(false);
    }

    public Tuple getOnSreenPosition(Widget child) {
        throw new RuntimeException("Leaf widget, does not have children.");
    }
    
    /**
     * Overide method to send a dirty event when value changes.
     * 
     * @param extent 
     */
    public void setEffectiveExtent(Extent extent) {
        if(!this.effExtent.equals(extent)){
            super.setEffectiveExtent(extent);
            setDirty();
        }
    }
}
