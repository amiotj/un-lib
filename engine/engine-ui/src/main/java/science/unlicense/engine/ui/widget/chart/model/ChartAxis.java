
package science.unlicense.engine.ui.widget.chart.model;

import science.unlicense.api.math.transform.Transform;
import science.unlicense.api.unit.Unit;

/**
 *
 * @author Johann Sorel
 */
public interface ChartAxis {
    
    public static final int POSITION2D_TOP = 1;
    public static final int POSITION2D_BOTTOM = 2;
    public static final int POSITION2D_LEFT = 3;
    public static final int POSITION2D_RIGHT = 4;
    
    /**
     * Anchor indicate where the ticks text is located relative to the axis.
     */
    public static final int ANCHOR_LEFTSIDE = 5;
    public static final int ANCHOR_RIGHTSIDE = 6;
    public static final int ANCHOR_OVER = 7;
    
    /**
     * Orientation of tick texts.
     * NOTE : use an angle ?
     */
    public static final int ORIENTATION_PARALLAL = 1;
    public static final int ORIENTATION_PERPENDICULAR = 2;
    
    /**
     * Axis unit.
     * 
     * @return 
     */
    Unit getUnit();
    
    Transform getCanvasToAxisTransform();
    
}
