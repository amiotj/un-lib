
package science.unlicense.engine.ui.visual;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.layout.SplitLayout;
import science.unlicense.engine.ui.widget.WSplitContainer;

/**
 *
 * @author Johann Sorel
 */
public class SplitView extends ContainerView {

    public SplitView(WSplitContainer widget) {
        super(widget);
    }

    public WSplitContainer getWidget() {
        return (WSplitContainer) super.getWidget();
    }

    
    private boolean dragging = false;

    private void updateDividerPosition(Tuple mousePosition){
        final WSplitContainer split = getWidget();
        if(split.isVerticalSplit()){
            split.setSplitPosition(mousePosition.getY()-split.getBoundingBox().getMin(1));
        }else{
            split.setSplitPosition(mousePosition.getX()-split.getBoundingBox().getMin(0));
        }
    }

    /**
     * Passthrought event to childrens.
     *
     * @param event
     */
    public void receiveEvent(Event event) {
        final WSplitContainer split = getWidget();
        final EventSource source = event.getSource();
        if(split.getChildren().contains(source)) return;

        //event comes from the parent
        final EventMessage message = event.getMessage();
        if(message instanceof MouseMessage){
            final MouseMessage mevent = (MouseMessage) message;
            final int type = mevent.getType();

            final Tuple mp = mevent.getMousePosition();

            final SplitLayout layout = ((SplitLayout)split.getLayout());
            final BBox ext = split.getBoundingBox();
            final double separatorSize = layout.getSplitGap();
            final double separatorPosition = layout.getSplitPosition();

            final BBox separator = new BBox(ext);
            if(split.isVerticalSplit()){
                separator.setRange(1, ext.getMin(1)+separatorPosition, ext.getMin(1)+separatorPosition+separatorSize);
            }else{
                separator.setRange(0, ext.getMin(0)+separatorPosition, ext.getMin(0)+separatorPosition+separatorSize);
            }

            final boolean inSeparator = separator.intersects(mp);

            //check separator dragging actions
            if(type == MouseMessage.TYPE_RELEASE && dragging){
                dragging = false;
                return;
            }else if(type == MouseMessage.TYPE_PRESS && !dragging && inSeparator){
                dragging = true;
                return;
            }else if(type == MouseMessage.TYPE_MOVE && dragging){
                updateDividerPosition(mp);
                return;
            }
            dragging = false;
        }

    }
    
}
