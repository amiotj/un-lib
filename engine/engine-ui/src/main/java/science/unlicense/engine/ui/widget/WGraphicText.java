
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.visual.GraphicTextView;

/**
 * Widget holding a single text.
 * 
 * @author Johann Sorel
 */
public class WGraphicText extends WLeaf{
    
    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = new Chars("Text");
    
    public WGraphicText() {
        this(null);
    }
    
    public WGraphicText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT, text);
        setView(new GraphicTextView(this));
    }
    
    /**
     * Get graphic text.
     * @return CharArray, can be null
     */
    public CharArray getText() {
        return (CharArray)getPropertyValue(PROPERTY_TEXT);
    }

    /**
     * Set graphic text.
     * @param text can be null
     */
    public void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT,text);
    }
    
    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varText(){
        return getProperty(PROPERTY_TEXT);
    }
    
}
