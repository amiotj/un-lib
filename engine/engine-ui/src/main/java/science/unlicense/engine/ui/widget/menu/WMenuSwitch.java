
package science.unlicense.engine.ui.widget.menu;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.PairLayout;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Toolbar or Menu switch button.
 *
 * @author Johann Sorel
 */
public class WMenuSwitch extends WSwitch {

    public WMenuSwitch() {
        this((CharArray)null);
    }

    public WMenuSwitch(CharArray text){
        this(text,null);
    }

    public WMenuSwitch(CharArray text, Widget graphic){
        this(text, graphic,null);
    }

    public WMenuSwitch(CharArray text, Widget graphic, EventListener lst){
        super(text, graphic, lst);        
        setHorizontalAlignment(PairLayout.HALIGN_LEFT);
    }

    public WMenuSwitch(ActionExecutable exec){
        super(exec);
        setHorizontalAlignment(PairLayout.HALIGN_LEFT);
    }

}
