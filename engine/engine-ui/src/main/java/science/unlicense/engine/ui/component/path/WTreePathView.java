

package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.predicate.AbstractPredicate;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.predicate.And;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.model.tree.DefaultNamedNode;
import science.unlicense.api.model.tree.NamedNode;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.WTree;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.model.tree.AbstractNode;
import science.unlicense.api.model.tree.DefaultNode;
import science.unlicense.api.model.tree.ViewNode;
import science.unlicense.api.path.Path;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Tree view.
 *
 * @author Johann Sorel
 */
public class WTreePathView extends AbstractPathView{

    private static final Predicate ONLY_CONTAINER = new AbstractPredicate() {
        public Boolean evaluate(Object candidate) {
            if(!(candidate instanceof Path)) return true;
            try {
                return ((Path)candidate).isContainer();
            } catch (IOException ex) {
                Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
            }
            return false;
        }
    };
    private static final Predicate ONLY_NOHIDDEN = new AbstractPredicate() {
        public Boolean evaluate(Object candidate) {
            if(!(candidate instanceof Path)) return true;
            final Object res = ((Path)candidate).getPathInfo(Path.INFO_HIDDEN);
            return res==null || !Boolean.TRUE.equals(res);
        }
    };

    private final WTree tree = new WTree(new FolderPresenter());

    private final TreeRowModel viewModel;

    public WTreePathView() {
        super(null,new Chars("Tree"));
        setLayout(new BorderLayout());
        addChild(tree, BorderConstraint.CENTER);
        
        final Node root = new DefaultNamedNode(new Chars("host"),true);

        final NamedNode fileRoots = science.unlicense.system.System.get().getProperties()
                .getSystemTree().search(new Chars("system/fileRoots"));
        final Iterator ite = fileRoots.getChildren().createIterator();
        while (ite.hasNext()) {
            Path path = (Path)((NamedNode)ite.next()).getValue();
//            //we want to see inside archives and mount points
//            path = new CrossingPath(path);
//            final ViewNode rootView = new ViewNode(path, Predicate.TRUE, WPathChooser.FILE_SORTER);
//            rootView.setCacheChildren(true);
            root.getChildren().add(new PathNode(path));
        }
        
        viewModel = new TreeRowModel(root,Predicate.TRUE,false);
        tree.setRowModel(viewModel);
        tree.setVerticalScrollVisibility(WTree.SCROLL_VISIBLE_NEEDED);
        tree.setHorizontalScrollVisibility(WTree.SCROLL_VISIBLE_NEVER);
        tree.getStyle().addRules(new Chars("{\nSystem : {\ncolor-delimiter: rgba(0 0 0 0)\n}\n}"));
        
        //listen to node selection
        tree.getSelection().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final Sequence sel = tree.getSelection();
                if(!sel.isEmpty()){
                    Object obj = sel.get(0);
                    if(obj instanceof PathNode){
                        setViewRoot( ((PathNode)obj).path);
                    }
                }
            }
        });
    }

    public void setViewRoot(Path viewRoot) {
        if(setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot)){
            //open path
            final TreeRowModel rowModel = (TreeRowModel) tree.getRowModel();
            for(Path p=viewRoot.getParent();p!=null;p=p.getParent()){
                rowModel.setFold(new PathNode(p), true);
            }
            //TODO does not work as expected, because of view nodes ?
            tree.getSelection().replaceAll(new Object[]{new PathNode(viewRoot)});
        }
    }

    private static class PathNode extends AbstractNode{

        private static final Predicate FILTER = new And(new Predicate[]{ONLY_NOHIDDEN,ONLY_CONTAINER});

        private final Path path;
        private Sequence childs;
        private Boolean canHaveChildren;

        public PathNode(Path path) {
            this.path = path;
        }

        @Override
        public Sequence getChildren() {
            if(childs==null){
                final Path[] subs = (Path[]) path.getChildren().toArray(Path.class);
                childs = new ArraySequence();
                for(int i=0;i<subs.length;i++){
                    if(FILTER.evaluate(subs[i])){
                        childs.add(subs[i]);
                    }
                }
                //sort by name and type(folder/file)
                Collections.sort(childs, WPathChooser.FILE_SORTER);
                //convert to PathNode objects
                for(int i=0,n=childs.getSize();i<n;i++){
                    childs.replace(i, new PathNode((Path) childs.get(i)));
                }
            }
            return childs;
        }

        @Override
        public boolean canHaveChildren() {
            if(canHaveChildren==null){
                try {
                    canHaveChildren = path.isContainer();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    canHaveChildren = false;
                }
            }
            return canHaveChildren;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof PathNode){
                return ((PathNode)obj).path.equals(path);
            }
            return false;
        }

        @Override
        public Chars toChars() {
            if(path.getName().isEmpty() && path.getParent()==null){
                return new Chars("/");
            }else{
                return new Chars(path.getName());
            }
        }

    }

    private static final class FolderPresenter implements ObjectPresenter{

        @Override
        public Widget createWidget(Object candidate) {
            
            final WLeaf leaf = new WSpace(14,14);
            leaf.getStyle().getSelfRule().setProperties(WPathChooser.STYLE_FOLDER);
            leaf.getStyle().getSelfRule().setProperties(new Chars("margin:0"));
            leaf.setOverrideExtents(new Extents(14, 14));

            if(candidate instanceof Node){
                candidate = ViewNode.unWrap((Node)candidate);
            }

            Chars text;
            if(candidate instanceof NamedNode){
                text = ((NamedNode)candidate).getName();
            }else if(candidate instanceof Path){
                text =  new Chars(((Path)candidate).getName());
            }else if(candidate instanceof DefaultNode){
                text =  ((DefaultNode)candidate).thisToChars();
            }else{
                text = new Chars(String.valueOf(candidate));
            }
            if(text.getCharLength()==0){
                text = new Chars(" ");
            }

            final WLabel label = new WLabel(text, leaf);
            label.setVerticalAlignment(WLabel.VALIGN_CENTER);
            label.setHorizontalAlignment(WLabel.HALIGN_LEFT);
            label.setGraphicPlacement(WLabel.GRAPHIC_LEFT);
            return label;
        }

    }

}
