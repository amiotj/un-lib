
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.visual.TextAreaView;

/**
 * Text area widget.
 * 
 * @author Johann Sorel
 */
public class WTextArea extends WLeaf{

    /**
     * Property for the widget text.
     */
    public static final Chars PROPERTY_TEXT = WTextField.PROPERTY_TEXT;
    public static final Chars PROPERTY_EDITED_TEXT = WTextField.PROPERTY_EDITED_TEXT;
    public static final Chars PROPERTY_PREVIEW_TEXT = WTextField.PROPERTY_PREVIEW_TEXT;
    public static final Chars PROPERTY_VALIDATOR = WTextField.PROPERTY_VALIDATOR;
    public static final Chars PROPERTY_EDITION_VALID = WTextField.PROPERTY_EDITION_VALID;

    public WTextArea() {
        this(Chars.EMPTY);
    }

    public WTextArea(CharArray text) {
        setText(text);
        setView(new TextAreaView(this));
    }

    /**
     * Set text area text.
     * @param text can be null
     */
    public void setText(CharArray text) {
        setPropertyValue(PROPERTY_TEXT,text);
        setEditedText(text);
    }
        
    /**
     * Get text area text.
     * @return Chars, can be null
     */
    public CharArray getText() {
        return (CharArray)getPropertyValue(PROPERTY_TEXT);
    }
    
    /**
     * Get text variable.
     * @return Variable.
     */
    public Variable varText(){
        return getProperty(PROPERTY_TEXT);
    }
    
    public void setEditedText(CharArray text) {
        if(setPropertyValue(PROPERTY_EDITED_TEXT,text)){
            //check if text is valid
            final boolean state = isEditedTextValid();
            setEditionValid(state);
            //if valid set the edited value as base text
            if(state) setText(text);
        }
    }
    
    public CharArray getEditedText() {
        return (CharArray)getPropertyValue(PROPERTY_EDITED_TEXT);
    }
    
    /**
     * Get edited text variable.
     * @return Variable.
     */
    public Variable varEditedText(){
        return getProperty(PROPERTY_EDITED_TEXT);
    }

    public void setValidator(Predicate text) {
        setPropertyValue(PROPERTY_VALIDATOR,text);
        setEditionValid(isEditedTextValid());
    }

    public Predicate getValidator() {
        return (Predicate)getPropertyValue(PROPERTY_VALIDATOR);
    }

    public Variable varEditionValid(){
        return getProperty(PROPERTY_EDITION_VALID);
    }

    private void setEditionValid(boolean valid) {
        setPropertyValue(PROPERTY_EDITION_VALID,valid);
    }

    public boolean isEditionValid() {
        return (Boolean)getPropertyValue(PROPERTY_EDITION_VALID, Boolean.TRUE);
    }

    private boolean isEditedTextValid(){
        final Predicate validator = getValidator();
        return validator==null || validator.evaluate(getEditedText());
    }

    /**
     * Get edited text variable.
     * @return Variable.
     */
    public Variable varValidator(){
        return getProperty(PROPERTY_VALIDATOR);
    }

    public void setPreviewText(CharArray text) {
        setPropertyValue(PROPERTY_PREVIEW_TEXT,text);
    }
    
    public CharArray getPreviewText() {
        return (CharArray)getPropertyValue(PROPERTY_PREVIEW_TEXT);
    }
    
    /**
     * Get preview text variable.
     * @return Variable.
     */
    public Variable varPreviewText(){
        return getProperty(PROPERTY_PREVIEW_TEXT);
    }
    
}