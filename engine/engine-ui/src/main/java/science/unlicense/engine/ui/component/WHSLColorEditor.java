
package science.unlicense.engine.ui.component;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.engine.ui.model.NumberSliderModel;
import science.unlicense.engine.ui.model.SpinnerEditor;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.impl.color.colorspace.HSL;

/**
 * HSL widget color editor.
 * 
 * @author Johann Sorel
 */
public class WHSLColorEditor extends WContainer {
    
    public static final Chars PROPERTY_VALUE = SpinnerEditor.PROPERTY_VALUE;
    
    private final WSlider hslider = new WSlider(new NumberSliderModel(Integer.class, 0, 360, 180), 180);
    private final WSlider sslider = new WSlider(new NumberSliderModel(Float.class, 0f, 1f, 0.5f), 0.5f);
    private final WSlider lslider = new WSlider(new NumberSliderModel(Float.class, 0f, 1f, 0.5f), 0.5f);
    private boolean updating = false;
    
    
    public WHSLColorEditor() {
        super(new FormLayout());
        
        //TODO I18N
        addChild(new WLabel(new Chars("H")), FillConstraint.builder().coord(0, 0).build());
        addChild(new WLabel(new Chars("S")), FillConstraint.builder().coord(0, 1).build());
        addChild(new WLabel(new Chars("L")), FillConstraint.builder().coord(0, 2).build());
        addChild(hslider, FillConstraint.builder().coord(1, 0).build());
        addChild(sslider, FillConstraint.builder().coord(1, 1).build());
        addChild(lslider, FillConstraint.builder().coord(1, 2).build());
        
        final EventListener listener = new EventListener() {
            public void receiveEvent(Event event) {
                if(updating) return;
                
                final float[] rgba = new float[4];
                HSL.INSTANCE.toRGBA(new float[]{(Integer)hslider.getValue(),
                    (Float)sslider.getValue(),(Float)lslider.getValue()}, rgba);
                setValue(new Color(rgba));
            }
        };
        
        final Predicate p = new PropertyPredicate(WSlider.PROPERTY_VALUE);
        hslider.addEventListener(p, listener);
        sslider.addEventListener(p, listener);
        lslider.addEventListener(p, listener);
    }
    
    /**
     * Set color value.
     * @param color
     */
    public void setValue(Color color) {
        updating = true;
        final float[] hsl = new float[3];
        HSL.INSTANCE.toSpace(color.toRGBA(), hsl);
        hslider.setValue((int)hsl[0]);
        sslider.setValue(hsl[1]);
        lslider.setValue(hsl[2]);
        updating = false;
        setPropertyValue(PROPERTY_VALUE,color);
    }

    /**
     * Get color value.
     * @return Color
     */
    public Color getValue() {
        return (Color)getPropertyValue(PROPERTY_VALUE);
    }
    
}
