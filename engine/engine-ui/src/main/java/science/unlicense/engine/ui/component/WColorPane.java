

package science.unlicense.engine.ui.component;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.api.color.Color;
import science.unlicense.impl.color.colorspace.HSL;
import science.unlicense.api.geometry.Extent;
import science.unlicense.engine.ui.model.SpinnerEditor;

/**
 * Color chooser.
 *
 * @author Johann Sorel
 */
public class WColorPane extends WLeaf {

    /**
     * Property for the widget color.
     */
    public static final Chars PROPERTY_VALUE = SpinnerEditor.PROPERTY_VALUE;
    
    private static final HSL hsl = HSL.INSTANCE;

    public WColorPane() {
        setView(new ColorPaneView(this));
    }

    public Color getValue() {
        return (Color)getPropertyValue(PROPERTY_VALUE);
    }

    public void setValue(Color color) {
        if(setPropertyValue(PROPERTY_VALUE,color)){
            setDirty();
        }
    }

    protected void receiveEventParent(Event event) {

        if(event.getMessage() instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) event.getMessage();
            final int type = me.getType();
            if(MouseMessage.TYPE_PRESS == type && me.getButton() == MouseMessage.BUTTON_1){
                me.consume();
                setValue(toColor(getEffectiveExtent(),
                        (float)me.getMousePosition().getX(),
                        (float)me.getMousePosition().getY()));
            }
        }

        //forward mouse and key event to listeners if any.
        super.receiveEventParent(event);
    }

    /**
     * Convert mouse coordinate on image in a color.
     * @param ext
     * @param x
     * @param y
     * @return
     */
    private static Color toColor(Extent ext, float x, float y){
        x+=ext.get(0)/2.0;
        y+=ext.get(1)/2.0;
        final float[] buffer = new float[3];
        buffer[0] = (x*360f) / (float)ext.get(0);
        buffer[1] = 1f;
        buffer[2] = 1f - ((float)y / (float)ext.get(1));
        final float[] rgba = new float[4];
        hsl.toRGBA(buffer, 0, rgba, 0, 1);
        return new Color(rgba);
    }

    
}