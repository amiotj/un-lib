
package science.unlicense.engine.ui.component.bean;

import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class IntegerEditor implements PropertyEditor {

    public static final IntegerEditor INSTANCE = new IntegerEditor();

    private IntegerEditor(){}

    public Widget create(Property property) {
        if(Integer.class.isAssignableFrom(property.getValueClass())){
            final WSpinner spinner = new WSpinner(new NumberSpinnerModel(Integer.class, 0, 0, Integer.MAX_VALUE, 1));
            spinner.varValue().sync(property);
            return spinner;
        }
        return null;
    }

}
