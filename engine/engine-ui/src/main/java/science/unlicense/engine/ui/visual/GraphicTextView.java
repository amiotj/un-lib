
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.model.doc.DocMessage;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.scenegraph.s2d.Graphic2DFactory;
import science.unlicense.api.scenegraph.s2d.TextNode2D;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.AbstractLabeled;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class GraphicTextView extends WidgetView {
    
    private final WGraphicText wtext;
    
    //visual cache
    private SceneNode graphic = null;

    public GraphicTextView(WGraphicText graphicText) {
        super(graphicText);
        this.wtext = graphicText;
    }

    public WGraphicText getWidget() {
        return (WGraphicText) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(getWidget().getStyle(),Widget.STYLE_PROP_FONT);
        if(font!=null){
            final CharArray text = getTranslatedText(getWidget().getText());
            final FontMetadata meta = FontContext.INSTANCE.getMetadata(font.getFont());
            final Extent extent = meta.getCharsBox(getTranslatedText(text)).getExtent();
            buffer.setAll(extent);

        }else{
            buffer.setAll(0, 0);
        }

    }
    
    private SceneNode getGraphic(Painter2D painter){
        if(graphic!=null) return graphic;
                        
        final BBox innerBBox = getWidget().getInnerExtent();                
        
        final Graphic2DFactory factory = painter.getGraphicFactory();
        final SceneNode node = factory.createNode();
        graphic = node;

        final CharArray text = wtext.getText();
        final FontStyle fontStyle = WidgetStyles.readFontStyle(wtext.getStyle(),Widget.STYLE_PROP_FONT);

        //prepare the informations we need
        Font font = null;
        FontMetadata meta = null;
        
        if(text!=null && fontStyle != null){
            font = fontStyle.getFont();
            if(font!=null){
                //calculate appropriate text position
                meta = FontContext.INSTANCE.getMetadata(font);
                final CharArray finalText = getTranslatedText(text);
                final BBox textExtent = meta.getCharsBox(finalText);
                
                final double[] trs = new double[]{
                    (innerBBox.getSpan(0)-textExtent.getSpan(0))/2.0 
                          + innerBBox.getMin(0)
                          -textExtent.getMin(0),
                    (innerBBox.getSpan(1)-textExtent.getSpan(1))/2.0 
                          + innerBBox.getMin(1)
                          +meta.getAscent()
                };
                
                final TextNode2D txtNode = new TextNode2D();
                txtNode.setFont(font);
                txtNode.setText(finalText);
                txtNode.setAnchor(new Vector(2));
                txtNode.setFills(new Paint[]{fontStyle.getFillPaint()});
                txtNode.getNodeTransform().setToTranslation(trs);
                node.getChildren().add(txtNode);
            }
        }
        
        return graphic;
    }
    
    protected void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {
        final SceneNode node = getGraphic(painter);
        if(node!=null) painter.render(node);
    }

    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if(  WGraphicText.PROPERTY_TEXT.equals(name)
          || AbstractLabeled.PROPERTY_LANGUAGE.equals(name)
                ){
            graphic = null;
            widget.updateExtents();
            widget.setDirty();
        }
    }

    public void receiveStyleEvent(DocMessage event) {
        super.receiveStyleEvent(event);
        graphic = null;
    }
    
}
