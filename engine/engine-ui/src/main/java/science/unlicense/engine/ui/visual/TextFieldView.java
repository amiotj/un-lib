
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.CharIterator;
import science.unlicense.api.character.Chars;
import science.unlicense.api.character.DefaultLChars;
import science.unlicense.api.character.LChars;
import science.unlicense.api.character.Language;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.api.layout.Extents;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Segment;

/**
 *
 * @author Johann Sorel
 */
public class TextFieldView extends WidgetView{

    private int caretPosition = -1;
    
    public TextFieldView(final WTextField widget) {
        super(widget);
        
        widget.addEventListener(new PropertyPredicate(Widget.PROPERTY_FOCUSED), new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if(Boolean.FALSE.equals(pe.getNewValue())){
                    //textfield lost focus
                    caretPosition = -1;
                }else{
                    //start edition
                    CharArray text = widget.getText();
                    caretPosition = text==null ? 0 : text.getCharLength();
                }
            }
        });
    }

    public WTextField getWidget() {
        return (WTextField) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(widget.getStyle(),Widget.STYLE_PROP_FONT);

        final Extent extent = new Extent.Double(0,0);

        //get text size
        final FontMetadata meta = font!=null ? FontContext.INSTANCE.getMetadata(font.getFont()) : null;

        CharArray text = getWidget().getEditedText();
        if(text==null || text.isEmpty()){
            text = new Chars(" ");
        }

        if(meta!=null){
            final BBox bbox = meta.getCharsBox(text);
            extent.set(0, extent.get(0)+bbox.getSpan(0));
            extent.set(1, extent.get(1)+bbox.getSpan(1));
        }

        buffer.setAll(extent);
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    public void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {

        final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getStyle(),Widget.STYLE_PROP_FONT);
        final boolean focused = widget.isFocused();
        
        //render the text
        final CharArray edited = getWidget().getEditedText();
        if(edited!=null && !edited.isEmpty() && fontStyle != null){
            final Font font = fontStyle.getFont();
            if(font!=null){
                painter.setFont(font);
                //calculate appropriate text position
                final FontMetadata meta = FontContext.INSTANCE.getMetadata(font);
                final BBox bbox = meta.getCharsBox(edited);
                final float xoffset = (float)innerBBox.getMin(0)+1;
                final float yoffset = (float)
                        (innerBBox.getMin(1)+(innerBBox.getSpan(1)-bbox.getSpan(1))/2 + meta.getAscent());

                if(fontStyle.getFillPaint()!=null){
                    painter.setPaint(fontStyle.getFillPaint());
                    painter.fill(edited, xoffset, yoffset);
                }
                if(fontStyle.getBrush()!=null && fontStyle.getBrushPaint()!=null){
                    painter.setBrush(fontStyle.getBrush());
                    painter.setPaint(fontStyle.getBrushPaint());
                    painter.stroke(edited, xoffset, yoffset);
                }
                
                //draw the caret
                if(focused && fontStyle.getFillPaint()!=null && caretPosition>=0){
                    final double ascent = meta.getAscent();
                    final double offset = xoffset + Font.calculateOffset(font,edited,caretPosition);
                    painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                    painter.setPaint(fontStyle.getFillPaint());
                    final Geometry2D geom = new Segment(offset+1, yoffset+1, offset+1, yoffset-ascent-1);
                    painter.stroke(geom);
                }
                
            }
        }
    }
    
    public void receiveEvent(Event event) {
        if(!widget.isStackEnable()) return;

        final EventMessage message = event.getMessage();
        if(message instanceof KeyMessage && widget.isFocused()){
            final KeyMessage ke = (KeyMessage) message;
            final int type = ke.getType();
            if(KeyMessage.TYPE_PRESS == type){
                final WTextField textField = getWidget();
                CharArray edited = textField.getEditedText();
                if(edited==null) edited = Chars.EMPTY;
                final int code = ke.getCode();
                final int codePoint = ke.getCodePoint();

                if(code==KeyMessage.KC_BACKSPACE){
                    if(caretPosition==0 || edited.isEmpty()) return;
                    //erase char
                    Chars t = edited.truncate(0, caretPosition-1).concat(edited.truncate(caretPosition, edited.getCharLength())).toChars();
                    caretPosition = caretPosition-1;
                    textField.setEditedText(copyAndChange(t, edited));
                    
                }else if(code==KeyMessage.KC_DELETE){
                    if(edited.isEmpty() || caretPosition>=edited.getCharLength()-1) return;
                    //erase char
                    Chars t = edited.truncate(0, caretPosition).concat(edited.truncate(caretPosition+1, edited.getCharLength())).toChars();
                    caretPosition = caretPosition-1;
                    textField.setEditedText(copyAndChange(t, edited));
                }else if(code==KeyMessage.KC_ENTER){
                    //no ligne jump in text field
                }else if(code==KeyMessage.KC_BEGIN){
                    //move caret to line start
                    final CharArray[] split = edited.split('\n');
                    int line = 0;
                    for(int i=0;i<split.length;i++){
                        if(line+split[i].getCharLength() >= caretPosition){
                            caretPosition = line;
                            widget.setDirty();
                            break;
                        }
                        line += split[i].getCharLength()+1;
                    }
                }else if(code==KeyMessage.KC_END){
                    //move caret to line end
                    final CharArray[] split = edited.split('\n');
                    int line = 0;
                    for(int i=0;i<split.length;i++){
                        if(line+split[i].getCharLength() >= caretPosition){
                            caretPosition = line+split[i].getCharLength();
                            widget.setDirty();
                            break;
                        }
                        line += split[i].getCharLength()+1;
                    }
                }else if(code==KeyMessage.KC_LEFT){
                    caretPosition = Maths.max(caretPosition-1, 0);
                    widget.setDirty();
                }else if(code==KeyMessage.KC_RIGHT){
                    caretPosition = Maths.min(caretPosition+1, textField.getEditedText().getCharLength());
                    widget.setDirty();                    
                }else if(code>0){
                    //unknown control, ignore it
                }else if(codePoint>0){
                    Chars t = edited.truncate(0, caretPosition).concat(codePoint).concat(edited.truncate(caretPosition, edited.getCharLength())).toChars();
                    caretPosition = caretPosition+1;
                    textField.setEditedText(copyAndChange(t, edited));
                }
            }
        }else if(message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if(me.getType() == MouseMessage.TYPE_TYPED){

                if(!widget.isFocused()) widget.requestFocus();

                //move caret position to mouse click
                double mx = me.getMousePosition().getX();
                double my = me.getMousePosition().getY();
                final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getStyle(),Widget.STYLE_PROP_FONT);
                final BBox innerBBox = widget.getInnerExtent();
                final CharArray edited = getWidget().getEditedText();

                if(edited!=null && !edited.isEmpty() && fontStyle != null){
                    final Font font = fontStyle.getFont();
                    if(font!=null){
                        final FontMetadata meta = FontContext.INSTANCE.getMetadata(font);

                        float yoffset = (float) innerBBox.getMin(1);
                        int caretPosition = 0;
                        CharArray line = edited;
                        final BBox bbox = meta.getCharsBox(line);
                        final float xoffset = (float)innerBBox.getMin(0)+1;
                        final float lineHeight = (float) (float) bbox.getSpan(1);
                        yoffset += lineHeight;
                        if(yoffset>my){
                            //find caret x offset
                            final double spacing = meta.getAdvanceWidth(' ');
                            final CharIterator ite = line.createIterator();

                            double offset = 0.0;
                            int caretChar = 0;
                            while(ite.hasNext()){
                                final int c = ite.nextToUnicode();
                                final double s;
                                if(c == ' '){
                                    //TODO handle control caracters
                                    s = spacing;
                                }else{
                                    s = meta.getAdvanceWidth(c);
                                }
                                if(offset+xoffset+s >= mx){
                                    caretPosition += caretChar;
                                    this.caretPosition = caretPosition;
                                    widget.setDirty();
                                    return;
                                }
                                offset += s;
                                caretChar++;
                            }

                            //at this point we know the mouse click was after the end of the line
                            //we place the caret at the last character position
                            caretPosition += caretChar;
                            this.caretPosition = caretPosition;
                            widget.setDirty();
                            return;
                        }
                    }
                }
            }
        }
    }

    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if(  WTextField.PROPERTY_TEXT.equals(name)
          || WTextField.PROPERTY_LANGUAGE.equals(name)
          || WTextField.PROPERTY_EDITED_TEXT.equals(name)
          || WTextField.PROPERTY_PREVIEW_TEXT.equals(name)){
            widget.updateExtents();
            widget.setDirty();
        } else if(WTextField.PROPERTY_FOCUSED.equals(name)){
            widget.setDirty();
        }
    }

    private static CharArray copyAndChange(Chars newText, CharArray ca){
        
        if(ca instanceof LChars){
            final LChars lc = (LChars) ca;
            final Dictionary dico = new HashDictionary();
            final Language def = lc.getLanguage();
            final Iterator ite = lc.getLanguages();
            while(ite.hasNext()){
                final Language lg = (Language) ite.next();
                if(lg.equals(def)){
                    //change the default translation value
                    dico.add(lg, newText);
                }else{
                    dico.add(lg, lc.translate(lg).toChars());
                }
            }
            return new DefaultLChars(dico, def);
        }else{
            return newText;
        }
        
    }
    
}
