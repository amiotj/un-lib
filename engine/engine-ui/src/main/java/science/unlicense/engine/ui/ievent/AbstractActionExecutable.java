
package science.unlicense.engine.ui.ievent;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;

/**
 *
 * @author Johann Sorel
 */
public class AbstractActionExecutable extends AbstractEventSource implements ActionExecutable{

    public static final Chars PROPERTY_IMAGE = new Chars("image");
    public static final Chars PROPERTY_TEXT = new Chars("text");
    public static final Chars PROPERTY_ACTIVE = new Chars("active");

    private Image image = null;
    private Chars text = Chars.EMPTY;
    private boolean active = true;

    public Image getImage(Extent extent) {
        return image;
    }

    public void setImage(Image image) {
        if(CObjects.equals(this.image,image)) return;
        Image old = this.image;
        this.image = image;
        sendPropertyEvent(this, PROPERTY_IMAGE, old, image);
    }

    public Chars getText() {
        return text;
    }

    public void setText(Chars text) {
        if(CObjects.equals(this.text,text)) return;
        Chars old = this.text;
        this.text = text;
        sendPropertyEvent(this, PROPERTY_TEXT, old, text);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        if(this.active == active) return;
        boolean old = this.active;
        this.active = active;
        sendPropertyEvent(this, PROPERTY_ACTIVE, old, active);
    }

    public Object execute() {
        return null;
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

}
