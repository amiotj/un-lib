
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.widget.AbstractControlWidget;
import science.unlicense.engine.ui.widget.WAction;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class WPathField extends AbstractControlWidget {

    /**
     * Property for the widget path.
     */
    public static final Chars PROPERTY_PATH = new Chars("Path");

    private final WTextField textField = new WTextField();
    private final WAction openChooser = new WAction();

    public WPathField() {
        setLayout(new BorderLayout());
        addChild(textField, BorderConstraint.CENTER);
        addChild(openChooser, BorderConstraint.RIGHT);
        textField.getFlags().add(new Chars("WPathField-Editor"));
        openChooser.getFlags().add(new Chars("WPathField-Button"));

        openChooser.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                new Thread() {
                    public void run() {
                        final WPathChooser chooser = new WPathChooser();
                        Path path = getPath();
                        if (path!=null) {
                            chooser.setSelectedPath(new Path[]{path});
                        }
                        path = chooser.showOpenDialog(WPathField.this.getFrame());
                        if (path!=null) setPath(path);
                    }
                }.start();
            }
        });
        
        textField.varText().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final PropertyMessage pm = (PropertyMessage) event.getMessage();
                final Chars chars = (Chars) pm.getNewValue();
                final Path path;
                try {
                    path = Paths.resolve(chars);
                } catch (Exception ex){
                    setEditionValid(false);
                    return;
                }
                setPath(path);
            }
        });
    }

    public Path getPath() {
        return (Path) getPropertyValue(PROPERTY_PATH);
    }

    public void setPath(Path path){
        if(setPropertyValue(PROPERTY_PATH, path)){
            if(path==null){
                textField.setText(Chars.EMPTY);
                setEditionValid(false);
            }else{
                textField.setText(new Chars(path.toURI()));
                setEditionValid(true);
            }
        }
    }

    /**
     * Get path variable.
     * @return Variable.
     */
    public Variable varPath(){
        return getProperty(PROPERTY_PATH);
    }

}
