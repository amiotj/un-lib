
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Predicate;

/**
 * Used in the path chooser, provide a custom presentation of the navigation state.
 *
 * @author Johann Sorel
 */
public interface PathView extends EventSource{

    /**
     * Selected paths property name, used in PropertyEvent.
     */
    public static final Chars PROPERTY_SELECTED_PATHS = new Chars("SelectedPath");
    /**
     * View root property name, used in PropertyEvent.
     */
    public static final Chars PROPERTY_VIEW_ROOT = new Chars("ViewRoot");
    /**
     * Hidden files property name, used in PropertyEvent.
     */
    public static final Chars PROPERTY_HIDDEN_FILE_VISIBLE = new Chars("hiddenFiles");
    /**
     * View root property name, used in PropertyEvent.
     */
    public static final Chars PROPERTY_MULTISELECTION = new Chars("multiselection");
    /**
     * View path filter used in PropertyEvent.
     */
    public static final Chars PROPERTY_FILTER = new Chars("Filter");
    
    /**
     * Icon used for this view type.
     * @return image, can be null
     */
    Widget getIcon();
    
    /**
     * Name or title of this view type.
     * @return Chars , never null
     */
    Chars getTitle();
    
    /**
     * Set hidden file visible or not.
     * @param showHidden true to view hidden files
     */
    void setHiddenFileVisible(boolean showHidden);
    
    /**
     * Get hidden files visibility state.
     * @return true if hidden files are unvisible
     */
    boolean isHiddenFileVisible();
    
    /**
     * 
     * @return hidden file visible property
     */
    Property varHiddenFileVisible();

    /**
     * Set multiple selection state.
     * @param multiple true to allow multiple selection 
     */
    void setMultiSelection(boolean multiple);
    
    /**
     * Get multiple selection state
     * @return true if multiple selection is possible.
     */
    boolean isMultiSelection();

    /**
     *
     * @return multiple selection property
     */
    Property varMultiSelection();

    /**
     * Set the view root. this property is used by implementation which
     * can display only a single folder at the time.
     * General tree like implementations may ignore this information.
     * @param viewRoot 
     */
    void setViewRoot(Path viewRoot);
    
    /**
     * Get displayed view root path.
     * @return Path, currently displayed view root, may be null
     */
    Path getViewRoot();

    /**
     *
     * @return view root property
     */
    Property varViewRoot();

    /**
     * Set selected paths.
     * @param path 
     */
    void setSelectedPath(Path[] path);
        
    /**
     * Get currently selection paths
     * @return Path[] never null but can be empty
     */
    Path[] getSelectedPath();

    /**
     *
     * @return selected paths property
     */
    Property varSelectedPath();

    /**
     * Set the view path filter.
     * @param filter
     */
    void setFilter(Predicate filter);

    /**
     * Get view path filter.
     * @return Predicate, currently path filter, may be null
     */
    Predicate getFilter();

    /**
     *
     * @return view path filter.
     */
    Property varFilter();

    /**
     * Get the view widget.
     * @return Widget never null
     */
    Widget getViewWidget();

}
