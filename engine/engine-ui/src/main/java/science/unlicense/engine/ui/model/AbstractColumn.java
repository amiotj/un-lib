
package science.unlicense.engine.ui.model;

import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;

/**
 * Abstract column model.
 *
 * @author Johann Sorel
 */
public abstract class AbstractColumn extends AbstractEventSource implements Column,EventListener {

    public AbstractColumn() {
    }

    public void receiveEvent(Event event) {
        //sequence elements has changed, forward event to widget
        if(hasListeners()) getEventManager().sendEvent(event);
    }

}
