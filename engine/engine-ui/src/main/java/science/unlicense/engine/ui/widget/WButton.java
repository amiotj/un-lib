package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.event.EventListener;
import science.unlicense.engine.ui.ievent.ActionExecutable;

/**
 * Button type widget.
 * 
 * @author Johann Sorel
 */
public class WButton extends WAction {

    public WButton() {
        this(null,(Widget)null);
    }

    public WButton(CharArray text) {
        this(text,(Widget)null);
    }

    public WButton(CharArray text, Widget graphic) {
        this(text,graphic,null);
    }
    
    public WButton(CharArray text, Widget graphic, EventListener listener) {
        super(text,graphic,listener);
    }

    public WButton(final ActionExecutable exec){
        super(exec);
    }

}
