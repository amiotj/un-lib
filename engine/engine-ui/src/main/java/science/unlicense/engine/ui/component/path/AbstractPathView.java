
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Property;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.ui.widget.WGraphicText;

/**
 *
 * @author Johann Sorel
 */
public class AbstractPathView extends WContainer implements PathView {

    static final Chars FONT_STYLE = new Chars("subs : {\nfilter:true\nborder:none\nfont:{\nfill-paint:@color-main\nfamily:font('Unicon' 14 'none')\n}\n}");
    static final Path[] DEFAULT_SELECTED_PATHS = new Path[0];
    
    private final Widget icon;
    private final Chars title;

    public AbstractPathView(Widget icon, Chars title) {
        this.icon = icon;
        this.title = title;
    }

    /**
     * {@inheritDoc }
     */
    public Widget getIcon() {
        return icon;
    }

    /**
     * {@inheritDoc }
     */
    public Chars getTitle() {
        return title;
    }
    
    /**
     * {@inheritDoc }
     */
    public void setHiddenFileVisible(boolean showHidden) {
        setPropertyValue(PROPERTY_HIDDEN_FILE_VISIBLE, showHidden, Boolean.FALSE);
    }

    /**
     * {@inheritDoc }
     */
    public boolean isHiddenFileVisible() {
        return (Boolean)getPropertyValue(PROPERTY_HIDDEN_FILE_VISIBLE, Boolean.FALSE);
    }

    /**
     * {@inheritDoc }
     */
    public Property varHiddenFileVisible(){
        return getProperty(PROPERTY_HIDDEN_FILE_VISIBLE);
    }

    /**
     * {@inheritDoc }
     */
    public void setMultiSelection(boolean multiple) {
        setPropertyValue(PROPERTY_MULTISELECTION, multiple, Boolean.FALSE);
    }

    /**
     * {@inheritDoc }
     */
    public boolean isMultiSelection() {
        return (Boolean)getPropertyValue(PROPERTY_MULTISELECTION, Boolean.FALSE);
    }

    /**
     * {@inheritDoc }
     */
    public Property varMultiSelection(){
        return getProperty(PROPERTY_MULTISELECTION);
    }

    /**
     * {@inheritDoc }
     */
    public void setViewRoot(Path viewRoot) {
        setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot);
    }

    /**
     * {@inheritDoc }
     */
    public Path getViewRoot() {
        return (Path) getPropertyValue(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public Property varViewRoot(){
        return getProperty(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public void setFilter(Predicate filter) {
        setPropertyValue(PROPERTY_FILTER, filter);
    }

    /**
     * {@inheritDoc }
     */
    public Predicate getFilter() {
        return (Predicate) getPropertyValue(PROPERTY_FILTER);
    }

    /**
     * {@inheritDoc }
     */
    public Property varFilter(){
        return getProperty(PROPERTY_FILTER);
    }

    /**
     * {@inheritDoc }
     */
    public void setSelectedPath(Path[] path) {
        setPropertyValue(PROPERTY_SELECTED_PATHS, path, DEFAULT_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    public Path[] getSelectedPath() {
        return (Path[]) getPropertyValue(PROPERTY_SELECTED_PATHS, DEFAULT_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    public Property varSelectedPath(){
        return getProperty(PROPERTY_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    public Widget getViewWidget() {
        return this;
    }
    
    /**
     * Combine hidden and filter predicates
     * @return 
     */
    protected Predicate getFilterInternal(final boolean folder){
        final boolean showHiddenFiles = isHiddenFileVisible();
        final Predicate filter = getFilter();
        return new Predicate() {
            public Boolean evaluate(Object candidate) {
                final Path p = (Path)candidate;
                if(!(showHiddenFiles || !Boolean.TRUE.equals(p.getPathInfo(Path.INFO_HIDDEN)))){
                    return false;
                }
                try {
                    if(folder && p.isContainer()) return true;
                } catch (IOException ex) {}
                return (filter==null || filter.evaluate(candidate));
            }
        };
    }

    static boolean equals(Path[] p1, Path[] p2){
        if(p1==null && p2==null) return true;
        if(p1==null || p2==null) return false;
        return Arrays.equals(p1, p2);
    }
 
    static WGraphicText createGlyph(String code){
        final WGraphicText t = new WGraphicText(new Chars(code));
        t.getStyle().getSelfRule().setProperties(FONT_STYLE);
        return t;
    }
    
}
