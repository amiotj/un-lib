
package science.unlicense.engine.ui.io;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.predicate.Function;
import science.unlicense.api.predicate.FunctionResolver;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.LinearGradientPaint;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.math.Maths;
import science.unlicense.api.predicate.ClassMethod;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.api.painter2d.RadialGradientPaint;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.image.process.paint.RotateColorOperator;
import science.unlicense.impl.math.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class RSFunctions implements FunctionResolver{

    public static final FunctionResolver INSTANCE;
    static {
        try {
            INSTANCE = new RSFunctions();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }
        
    private static final Chars POURCENT = new Chars("%");
    private static final Chars PIXEL = new Chars("pixel");
    
    private final Dictionary functions = new HashDictionary();
    
    private RSFunctions() throws ClassNotFoundException{
        for(Class c : RSReader.getFunctionClasses()){
            for(Method m : c.getMethods()){
                if(Modifier.isStatic(m.getModifiers())){
                    functions.add(new Chars(m.getName()), m);
                }
            }
        }
    }
    
    public Function resolve(Chars name, Expression[] parameters) {
        final Method method = (Method) functions.getValue(name);
        return (method!=null) ? new ClassMethod(method, parameters) : null;
    }

    /**
     * Rotate color in HSL.
     * This operation is similar to image RotateColorOperator.
     * 
     * @param color
     * @param h , This value will be added to the hue and wrapped to 0 - 360
     * @param s , This value will be multiplied to the pixel saturation and clipped to 0 - 1
     * @param l , This value will be multiplied to the pixel lightning and clipped to 0 - 1
     * @return rotated color
     */
    public static Color rotateHSL(Color color, double h, double s, double l) {
        return RotateColorOperator.rotate(color, h, s, l);
    }
    
    /**
     * Adjust color alpha component.
     * 
     * @param color
     * @param r, This value will be multiplied to the pixel alpha and clipped to 0 - 1
     * @return 
     */
    public static Color rotateAlpha(Color color, double r) {
        float[] rgba = color.toRGBA();
        rgba[3] *= r;
        rgba[3] = Maths.clamp(rgba[3], 0, 1);
        return new Color(rgba);
    }
    
    public static Paint colorfill(Color color) {
        return new ColorPaint(color);
    }

    public static Color rgb(int r,int g,int b) {
        return new Color(r, g, b,255);
    }

    public static Color rgba(int r,int g,int b,int a) {
        return new Color(r, g, b, a);
    }

    public static Class toClass(Chars name) throws ClassNotFoundException {
        return Class.forName(name.toString());
    }

    public static Paint lineargradientfill(Object candidate, Expression[] expressions) {

        final Object[] values = evaluateParameters(candidate,expressions);

        final Chars unit   = (Chars) values[0];
        double startx = ((Number) values[1]).doubleValue();
        double starty = ((Number) values[2]).doubleValue();
        double endx   = ((Number) values[3]).doubleValue();
        double endy   = ((Number) values[4]).doubleValue();

        final int nbSteps = (values.length-5)/2;
        final double[] steps = new double[nbSteps];
        final Color[] colors = new Color[nbSteps];
        for(int k=5,i=0;k<values.length;k+=2,i++){
            steps[i] = ((Number) values[k]).doubleValue();
            colors[i] = (Color) values[k+1];
        }

        if(candidate instanceof WStyle){
            candidate = ((WStyle)candidate).getWidget();
        }

        if(POURCENT.equals(unit) && candidate instanceof Widget){
            final Widget widget = (Widget) candidate;
            final BBox size = widget.getBoundingBox();
            startx = size.getMin(0) + size.getSpan(0)*startx;
            starty = size.getMin(1) + size.getSpan(1)*starty;
            endx = size.getMin(0) + size.getSpan(0)*endx;
            endy = size.getMin(1) + size.getSpan(1)*endy;
        }

        return new LinearGradientPaint(startx, starty, endx, endy, steps, colors);
    }

    public static Paint radialgradientfill(Object candidate, Expression[] expressions) {
        final Object[] values = evaluateParameters(candidate, expressions);
        
        final Chars unit   = (Chars) values[0];
        double centerx  = ((Number) values[1]).doubleValue();
        double centery  = ((Number) values[2]).doubleValue();
        double radius   = ((Number) values[3]).doubleValue();
        double focusx   = ((Number) values[4]).doubleValue();
        double focusy   = ((Number) values[5]).doubleValue();

        final int nbSteps = (values.length-6)/2;
        final double[] steps = new double[nbSteps];
        final Color[] colors = new Color[nbSteps];
        for(int k=6,i=0;k<values.length;k+=2,i++){
            steps[i] = ((Number) values[k]).doubleValue();
            colors[i] = (Color) values[k+1];
        }

        if(candidate instanceof WStyle){
            candidate = ((WStyle)candidate).getWidget();
        }

        if(POURCENT.equals(unit) && candidate instanceof Widget){
            final Widget widget = (Widget) candidate;
            final Extent size = widget.getEffectiveExtent();
            centerx *= size.get(0);
            centery *= size.get(1);
            focusx *= size.get(0);
            focusy *= size.get(1);
        }

        return new RadialGradientPaint(centerx, centery, radius, focusx, focusy, steps, colors);
    }
    
    public static Paint imagefill(Object candidate, Expression[] expressions) {
        //TODO find a convinient way to describe image pattern and placement
        throw new UnimplementedException("Not supported yet");
//        final Object[] values = evaluateParameters(candidate, expressions);
        
//        final Chars path   = (Chars) values[0];
//        final int fitting = (Integer) values[1];
//        
//        Matrix trs;
//        if(fitting == WGraphicImage.FITTING_CENTERED) trs = Projections.centeredMatrix(imgExt,innerBBox);
//        else if(fitting == WGraphicImage.FITTING_ZOOMED) trs = Projections.zoomedMatrix(imgExt,innerBBox);
//        else if(fitting == WGraphicImage.FITTING_STRETCHED) trs = Projections.stretchedMatrix(imgExt,innerBBox);
//        else trs = Projections.scaledMatrix(imgExt,innerBBox);
//        
//        final Image image = Images.read(Paths.resolve(path.toString()));
        
//        return new ImagePaint(image, trs, ImagePaint.WARP_NONE, ImagePaint.WARP_NONE);
    }
    
    public static PlainBrush plainbrush(double width, Chars cap) {
        return new PlainBrush((float)width, PlainBrush.LINECAP_ROUND);
    }
    
    public static Font font(Chars families, double size, Chars weight){
        return new Font(families.split(','), (float)size, Font.WEIGHT_NONE);
    }

    public static Geometry glyph(Chars family, Chars unicode){
        final Font font = font(family, 1, null);
        Geometry2D geom =  FontContext.INSTANCE.getGlyph(font, unicode.getUnicode(0));
        //flip it
        final Affine2 trs = new Affine2(1, 0, 0,
                                        0,-1, 0);
        return new TransformedGeometry2D(geom, trs);
    }

    private static Object[] evaluateParameters(Object candidate, Expression[] parameters){
        final Object[] values = new Object[parameters.length];
        for(int i=0;i<values.length;i++){
            values[i] = parameters[i].evaluate(candidate);
        }
        return values;
    }
    
}
