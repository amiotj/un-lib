package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.character.Language;
import science.unlicense.api.collection.Set;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.layout.Positionable;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.visual.View;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.event.Property;

/**
 * Widget interface.
 *
 * Widgets all share common properties which are :
 * - min/best/max extent
 * - enable state
 * - visible state
 * - tooltip
 * - popup
 * - border
 * - background
 * - style
 *
 * @author Johann Sorel
 */
public interface Widget extends Positionable, SceneNode {

    // style properties shared by most widgets.
    /** Text font : Font class */
    public static final Chars STYLE_PROP_FONT = new Chars("font");
    public static final Chars STYLE_PROP_BORDER = new Chars("border");
    public static final Chars STYLE_PROP_BACKGROUND = new Chars("background");
    
    /** radius : double class */
    public static final Chars STYLE_PROP_FAMILY = new Chars("family");
    /** radius : double class */
    public static final Chars STYLE_PROP_RADIUS = new Chars("radius");
    /** brush : Brush class */
    public static final Chars STYLE_PROP_BRUSH = new Chars("brush");
    /** brush paint : Paint class */
    public static final Chars STYLE_PROP_BRUSH_PAINT = new Chars("brush-paint");
    /** fill paint : Paint class */
    public static final Chars STYLE_PROP_FILL_PAINT = new Chars("fill-paint");
    /** visual margin : double class, this margin is used at rendering time for clipping */
    public static final Chars STYLE_PROP_VISUAL_MARGIN = new Chars("visual-margin");
    /** margin : double class, this margin is used by layouts */
    public static final Chars STYLE_PROP_MARGIN = new Chars("margin");
    /** shape : String class */
    public static final Chars STYLE_PROP_SHAPE = new Chars("shape");
    /** transform : 6 doubles : affine transform in col order */
    public static final Chars STYLE_PROP_TRANSFORM = new Chars("transform");
    /** fitting : define how the border shape if fitted, values are scaled, centered, stretched, zoomed, corner */
    public static final Chars STYLE_PROP_FITTING = new Chars("fitting");
    /** effect : special effects on widget, blur, morphologic,... could be anything visual */
    public static final Chars STYLE_PROP_EFFECT = new Chars("effect");

    /**
     * Property indicating when the widget id changed.
     */
    public static final Chars PROPERTY_ID = new Chars("Id");
    /**
     * Property indicating when the cursor changes.
     */
    public static final Chars PROPERTY_CURSOR = new Chars("Cursor");
    /**
     * Property indicating the widget needs to be repainted or not.
     */
    public static final Chars PROPERTY_DIRTY = new Chars("Dirty");
    /**
     * Property indicating if the widget is enable.
     */
    public static final Chars PROPERTY_ENABLE = new Chars("Enable");
    /**
     * Property indicating if the widget is enable.
     */
    public static final Chars PROPERTY_STACKENABLE = new Chars("StackEnable");
    /**
     * Property indicating if the widget is focused.
     */
    public static final Chars PROPERTY_FOCUSED = new Chars("Focused");
    /**
     * Property indicating if the widget has a focused child.
     */
    public static final Chars PROPERTY_CHILDFOCUSED = new Chars("ChildFocused");
    /**
     * Property indicating when the popup widget changed.
     */
    public static final Chars PROPERTY_POPUP = new Chars("Popup");
    /**
     * Property indicating when the tooltip widget changed.
     */
    public static final Chars PROPERTY_TOOLTIP = new Chars("Tooltip");
    /**
     * Property indicating when the frame changed.
     */
    public static final Chars PROPERTY_FRAME = new Chars("Frame");
    /**
     * Property indicating when the view changed.
     */
    public static final Chars PROPERTY_VIEW = new Chars("View");
    /**
     * Property indicating when the widget language changed.
     */
    public static final Chars PROPERTY_LANGUAGE = new Chars("Language");
    /**
     * Property indicating the widget parent.
     */
    public static final Chars PROPERTY_MOUSEOVER = new Chars("MouseOver");
    /**
     * Property indicating the widget parent.
     */
    public static final Chars PROPERTY_MOUSEPRESS = new Chars("MousePress");
    
    /**
     * Set widget id.
     * 
     * @param id not null
     */
    void setId(Chars id);

    /**
     * @return widget id, never null
     */
    Chars getId();

    /**
     * @return id variable
     */
    Variable varId();
    
    /**
     * @return visible variable
     */
    Variable varVisible();
    
    /**
     * @return extents variable
     */
    Variable varExtents();
        
    /**
     * @return effective extent variable
     */
    Variable varEffectiveExtent();
    
    /**
     * Set widget state, enable or disable.
     * @param enable
     */
    void setEnable(boolean enable);

    /**
     * @return true if widget is active
     */
    boolean isEnable();

    /**
     * @return enable variable
     */
    Variable varEnable();
    
    /**
     * search throw parents to find if any of them is disable.
     * @return true if all parents in the stack are enable.
     */
    boolean isStackEnable();
    
    /**
     * Indicate if the widget has a focused child.
     * 
     * @return true if widget has a focused child
     */
    boolean isChildFocused();
    
    /**
     * Indicate if the widget is focused.
     * 
     * @return true if widget is focused
     */
    boolean isFocused();
    
    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * USE method requestFocus.
     * 
     * @param focused 
     */
    void setFocused(boolean focused);
    
    /**
     * Request focus on this widget.
     */
    void requestFocus();
            
    /**
     * Set widget tooltip.
     * 
     * @param tooltip, can be null
     */
    void setToolTip(Widget tooltip);

    /**
     * @return popup tooltip, can be null.
     */
    Widget getToolTip();
    
    /**
     * Set widget popup.
     * @param popup, can be null
     */
    void setPopup(Widget popup);

    /**
     * @return popup widget, can be null.
     */
    Widget getPopup();

    /**
     * Set widget language.
     * @param language, can not be null
     * @param recursive, loop on childrens to set language
     */
    void setLanguage(Language language, boolean recursive);

    /**
     * @return language, can not be null.
     */
    Language getLanguage();
    
    /**
     * Set widget cursor.
     * @param cursor, can be null
     */
    void setCursor(Cursor cursor);

    /**
     * @return cursor  can be null.
     */
    Cursor getCursor();
    
    /**
     * search throw parents to find the first cursor
     * @return Cursor, can be null
     */
    Cursor getStackCursor();
    
    /**
     * WARNING : INTERNAL USE ONLY, DO NOT CALL IT YOURSELF.
     * Reaffect extents which are not overridden.
     */
    boolean updateExtents();

    /**
     * The widget effective inner bounding box, excluding margins and overlaps.
     * @return BoundingBox
     */
    BBox getInnerExtent();
        
    /**
     * INTERNAL USE ONLY, DO NOT CALL THIS METHOD YOURSELF !
     * @param frame
     */
    void setFrame(UIFrame frame);

    UIFrame getFrame();

    /**
     * Style definition,
     * @return
     */
    WStyle getStyle();

    /**
     * Flags are similar to CSS style classes.
     * It is a list of Chars mainly used to mark widgets.
     * 
     * @return Set, never null
     */
    Set getFlags();
    
    ////////////////////////////////////////////////////////////////////////////
    // rendering ///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    void setView(View view);
    
    View getView();

    void setDirty();

    /**
     * Set dirty.
     * @param dirtyArea
     */
    void setDirty(BBox dirtyArea);
        
    ////////////////////////////////////////////////////////////////////////////
    // popup and tooltip ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get on screen position of a children of this widget.
     *
     * @return position of null if widget is not displayed.
     */
    Tuple getOnSreenPosition(Widget child);

    /**
     * Get on screen position of this widget.
     *
     * @return position of null if widget is not displayed.
     */
    Tuple getOnSreenPosition();

    /**
     * Force showing tooltip.
     */
    void showTooltip();

    /**
     * Force showing popup.
     */
    void showPopup();


    ////////////////////////////////////////////////////////////////////////////
    // events //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Catch events.
     * Such as mouse and keyboard events.
     * 
     * @param event
     */
    void receiveEvent(Event event);

    ////////////////////////////////////////////////////////////////////////////
    // mouse event state ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @return mouse over variable
     */
    Property varMouseOver();
    
    /**
     * Utility method used by styles to get current mouse state relative to this widget.
     *
     * @return true if mouse is over this widget
     */
    boolean isMouseOver();

    /**
     * @return mouse over variable
     */
    Property varMousePress();
    
    /**
     * Utility method used by styles to get current mouse state relative to this widget.
     * 
     * @return true if mouse is pressed this widget
     */
    boolean isMousePress();
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Utils for all widgets ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * FOR INTERNAL USE ONLY.
     * 
     * @return 
     */
    EventManager getEventManager();
    
}
