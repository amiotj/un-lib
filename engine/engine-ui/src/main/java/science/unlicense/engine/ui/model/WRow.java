
package science.unlicense.engine.ui.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.widget.AbstractRowWidget;
import static science.unlicense.engine.ui.widget.AbstractRowWidget.STYLE_ROW;
import science.unlicense.engine.ui.widget.WCell;
import science.unlicense.engine.ui.widget.WLeaf;

/**
 *
 * @author Johann Sorel
 */
public class WRow extends WLeaf {

    public static final Chars PROPERTY_SELECTED = new Chars("Selected");
    public static final Chars PROPERTY_INDEX = new Chars("Index");
    public static final Chars PROPERTY_VALUE = new Chars("Value");
    
    // Column -> WCell
    private final Dictionary cells = new HashDictionary(Hasher.IDENTITY);
    private final AbstractRowWidget rowWidget;
    
    public WRow(AbstractRowWidget rowWidget) {
        getFlags().add(STYLE_ROW);
        this.rowWidget = rowWidget;
    }

    public AbstractRowWidget getRowWidget() {
        return rowWidget;
    }
    
    public Object getValue() {
        return getPropertyValue(PROPERTY_VALUE);
    }
    
    public void setValue(Object value){
        setPropertyValue(PROPERTY_VALUE, value);
    }

    public Property varValue(){
        return getProperty(PROPERTY_VALUE);
    }
    
    public boolean isSelected(){
        //test equality, value could be null at first
        return (Boolean) getPropertyValue(PROPERTY_SELECTED, Boolean.FALSE);
    }
    
    public void setSelected(boolean selected){
        setPropertyValue(PROPERTY_SELECTED, selected, Boolean.FALSE);
    }
    
    public int getIndex(){
        return (Integer) getPropertyValue(PROPERTY_INDEX,0);
    }
    
    public void setIndex(int index){
        setPropertyValue(PROPERTY_INDEX, index, 0);
    }
            
    public WCell getCell(Column column){
        WCell cell = (WCell) cells.getValue(column);
        if(cell==null){
            cell = (WCell) column.createCell(this);
            cell.varValue().sync(varValue());
            cells.add(column, cell);
        }
        return cell;
    }
    
}
