
package science.unlicense.engine.ui.model;

import science.unlicense.api.character.Chars;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.ui.widget.WButton;

/**
 *
 * @author Johann Sorel
 */
public class WFoldButton extends WButton {

    public static final Chars PROPERTY_UNFOLD = new Chars("UnFold");
    private final TreeRowModel treeModel;
    private final Node node;

    public WFoldButton(TreeRowModel treeModel, Node node) {
        this.treeModel = treeModel;
        this.node = node;
        setOverrideExtents(new Extents(14, 13));
    }

    public void doClick() {
        super.doClick();
        boolean foldState = !treeModel.isUnfold(node);
        treeModel.setFold(node, !treeModel.isUnfold(node));
        sendPropertyEvent(node, PROPERTY_UNFOLD, foldState, !foldState);
    }

    public boolean isUnFold() {
        if(treeModel==null) return false;
        return treeModel.isUnfold(node);
    }

}
