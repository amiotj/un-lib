

package science.unlicense.engine.ui.widget.menu;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.engine.ui.widget.WPopup;

/**
 *
 * @author Johann Sorel
 */
public class WMenuDropDown extends WMenuButton {

    public static final int POPUP_LEFT = 0;
    public static final int POPUP_RIGHT = 1;
    public static final int POPUP_TOP = 2;
    public static final int POPUP_BOTTOM = 3;

    private final WPopup dropdown = new WPopup();
    private int dropLocation = POPUP_BOTTOM;

    public WMenuDropDown() {
        dropdown.setLayout(new GridLayout(-1, 1));

        addEventListener(ActionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final WPopup dropdown = getDropdown();
                int offsetx = 0;
                int offsety = 0;
                if(dropLocation == POPUP_LEFT){
                    offsetx = (int)-dropdown.getExtents(null, null).bestX;
                }else if(dropLocation == POPUP_RIGHT){
                    offsetx = (int)getEffectiveExtent().get(0);
                }else if(dropLocation == POPUP_TOP){
                    offsety = (int)-dropdown.getExtents(null, null).bestY;
                }else if(dropLocation == POPUP_BOTTOM){
                    offsety = (int)getEffectiveExtent().get(1);
                }
                dropdown.showAt(WMenuDropDown.this, offsetx, offsety);
            }
        });
    }

    public WPopup getDropdown() {
        return dropdown;
    }

    public int getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(int dropLocation) {
        this.dropLocation = dropLocation;
    }
    
}
