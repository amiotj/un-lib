package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;

/**
 * A WSeparator is a horizontal of vertical line.
 * 
 * @author Johann Sorel
 */
public class WSeparator extends WLeaf{

    public static final Chars PROPERTY_HORIZONTAL = new Chars("Horizontal");
    
    /**
     * Create a new horizontal separator.
     */
    public WSeparator() {
        this(true);
    }
    
    /**
     * Create a new separator.
     * 
     * @param horizontal 
     */
    public WSeparator(boolean horizontal) {
        setHorizontal(horizontal);
    }
        
    public void setHorizontal(boolean horizontal) {
        setPropertyValue(PROPERTY_HORIZONTAL, horizontal,Boolean.TRUE);
    }

    public boolean isHorizontal() {
        return (Boolean)getPropertyValue(PROPERTY_HORIZONTAL,Boolean.TRUE);
    }
    
}
