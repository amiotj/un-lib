
package science.unlicense.engine.ui.style;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.system.path.Paths;

/**
 * Global style informations defining system main colors and font.
 *
 * @author Johann Sorel
 */
public final class SystemStyle extends WStyle{

    public static final Chars COLOR_BACKGROUND = new Chars("color-background");
    public static final Chars COLOR_TRANSPARENT = new Chars("color-transparent");
    public static final Chars COLOR_MAIN = new Chars("color-main");
    public static final Chars COLOR_FOCUS = new Chars("color-focus");
    public static final Chars COLOR_DELIMITER = new Chars("color-delimiter");
    public static final Chars COLOR_TEXT = new Chars("color-text");
    public static final Chars COLOR_TEXT_BACKGROUND = new Chars("color-textarea-background");

    public static final Chars RULE_SYSTEM = new Chars("System");

    public static final Chars THEME_LIGHT = new Chars("mod:/un/engine/ui/widget/default.style");
    public static final Chars THEME_DARK = new Chars("mod:/un/engine/ui/widget/dark.style");

    public static final WStyle INSTANCE = new SystemStyle();

    private SystemStyle() {
        super(null);
        try{
            final WStyle style = RSReader.readStyle(Paths.resolve(THEME_LIGHT));
            getRules().addAll(style.getRules());
        }catch(InvalidArgumentException ex){
            throw new RuntimeException("Failed to load System default style "+ex.getMessage(),ex);
        }
    }

    public static final Color getSystemColor(Chars name){
        return (Color) INSTANCE.getPropertyValue(name);
    }

    public Iterator createRuleIterator(){
        return getRules().createIterator();
    }

}
