
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.model.SliderModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.widget.WRangeSlider;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.RoundedRectangle;

/**
 *
 * @author Johann Sorel
 */
public class RangeSliderView extends WidgetView{

    private static final double MARK_RADIUS = 7;
    
    private boolean pressed = false;
    
    public RangeSliderView(WRangeSlider widget) {
        super(widget);
    }

    public WRangeSlider getWidget() {
        return (WRangeSlider) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {
        buffer.minX = 120;
        buffer.minY = 14;
        buffer.bestX = buffer.minX;
        buffer.bestY = buffer.minY;
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    protected void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {
        final double width = innerBBox.getSpan(0);
        final double height = innerBBox.getSpan(1);
        final boolean inner = getWidget().isInnerRange();
        
        double barHeight = 8;

        //paint the ramp
        final RoundedRectangle rect = new RoundedRectangle(
                innerBBox.getMin(0) + MARK_RADIUS, 
                innerBBox.getMin(1) + (height-barHeight)/2, 
                width-MARK_RADIUS*2, 
                barHeight);
        final Color bgColor = SystemStyle.getSystemColor(SystemStyle.COLOR_TEXT_BACKGROUND);
        final Color borderColor = SystemStyle.getSystemColor(SystemStyle.COLOR_MAIN);
        painter.setPaint(new ColorPaint(bgColor));
        painter.fill(rect);
        painter.setPaint(new ColorPaint(borderColor));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.stroke(rect);

        //paint range
        final double span = width-MARK_RADIUS*2;
        final SliderModel model = getWidget().getModel();
        double ratioMin = model.getRatio(getWidget().getValueMin());
        ratioMin *= span;
        double ratioMax = model.getRatio(getWidget().getValueMax());
        ratioMax *= span;        
        final Color markerColor = SystemStyle.getSystemColor(SystemStyle.COLOR_FOCUS);
        
        //paint range
        painter.setPaint(new ColorPaint(markerColor));
        if(inner){
            final RoundedRectangle range = new RoundedRectangle(
                    innerBBox.getMin(0) + ratioMin, 
                    innerBBox.getMin(1) + (height-barHeight)/2, 
                    ratioMax-ratioMin, 
                    barHeight);
            painter.fill(range);
        }else{
            final RoundedRectangle range1 = new RoundedRectangle(
                    innerBBox.getMin(0) +MARK_RADIUS, 
                    innerBBox.getMin(1) + (height-barHeight)/2, 
                    ratioMin, 
                    barHeight);
            final RoundedRectangle range2 = new RoundedRectangle(
                    innerBBox.getMin(0) + ratioMax, 
                    innerBBox.getMin(1) + (height-barHeight)/2, 
                    span-ratioMax+MARK_RADIUS, 
                    barHeight);
            painter.fill(range1);
            painter.fill(range2);
        }
        
        //paint the markers
        final Circle circleMin = new Circle(
                innerBBox.getMin(0) + ratioMin+MARK_RADIUS, 
                innerBBox.getMin(1) + height/2, 
                MARK_RADIUS);
        final Circle circleMax = new Circle(
                innerBBox.getMin(0) + ratioMax+MARK_RADIUS, 
                innerBBox.getMin(1) + height/2, 
                MARK_RADIUS);
        painter.fill(circleMin);
        painter.fill(circleMax);
        painter.setPaint(new ColorPaint(borderColor));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.stroke(circleMin);
        painter.stroke(circleMax);

    }
        
    public void receiveEvent(Event event) {
        if(!widget.isStackEnable()) return;
        final EventMessage message = event.getMessage();
        
        if(!message.isConsumed() && message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if(me.getType() == MouseMessage.TYPE_PRESS){
                pressed = true;
                updateValue(me);
                me.consume();
            }else if(me.getType() == MouseMessage.TYPE_MOVE){
                if(pressed){
                    updateValue(me);
                    me.consume();
                }
            }else if(me.getType() == MouseMessage.TYPE_RELEASE){
                pressed = false;
                me.consume();
            }else if(me.getType() == MouseMessage.TYPE_EXIT){
                pressed = false;
            }
        }
    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);
        
        final Chars pname = event.getPropertyName();
        if(   WRangeSlider.PROPERTY_INNER_RANGE.equals(pname)
           || WRangeSlider.PROPERTY_VALUE_MIN.equals(pname)
           || WRangeSlider.PROPERTY_VALUE_MAX.equals(pname) ){
            widget.setDirty();
        }
    }

    private void updateValue(MouseMessage me){
        final WRangeSlider slider = getWidget();
        final Tuple tuple = me.getMousePosition();        
        final BBox bbox = widget.getBoundingBox();
        final double width = bbox.getSpan(0) - MARK_RADIUS*2;
        final double x = Maths.clamp(tuple.getX()-MARK_RADIUS-bbox.getMin(0), 0, width);
        final SliderModel model = slider.getModel();
        final Object value = model.getValue(x/width);
        
        final Number valueMin = (Number) slider.getValueMin();
        final Number valueMax = (Number) slider.getValueMax();
        
        //move the closest edge
        double dmin = Math.abs( ((Number)value).doubleValue() - valueMin.doubleValue());
        double dmax = Math.abs( ((Number)value).doubleValue() - valueMax.doubleValue());
        if(dmax<dmin){
            slider.setValueMax(value);
        }else{
            slider.setValueMin(value);
        }
    }
    
}
