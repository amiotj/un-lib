
package science.unlicense.engine.ui.component.bean;

import science.unlicense.api.event.Property;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;

/**
 *
 * @author Johann Sorel
 */
public class DoubleEditor implements PropertyEditor {

    public static final DoubleEditor INSTANCE = new DoubleEditor();

    private DoubleEditor(){}

    public Widget create(Property property) {
        if(Double.class.isAssignableFrom(property.getValueClass())){
            final WSpinner spinner = new WSpinner(new NumberSpinnerModel(Double.class, 0.0, 0.0, Double.MAX_VALUE, 1.0));
            spinner.varValue().sync(property);
            return spinner;
        }
        return null;
    }

}
