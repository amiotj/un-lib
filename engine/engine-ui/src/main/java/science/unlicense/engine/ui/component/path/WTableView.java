
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.number.Int64;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultObjectPresenter;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.Format;
import science.unlicense.api.store.Formats;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.time.format.ISO8601;

/**
 *
 * @author Johann Sorel
 */
public class WTableView extends AbstractPathView {
    
    public static final int PREVIEW_NONE = 0;
    public static final int PREVIEW_MIME = 1;
    public static final int PREVIEW_IMAGE = 2;
    
    private final WTable table = new WTable();
    private final EventListener tableListener = new EventListener() {
        public void receiveEvent(Event event) {
            final Object[] array = table.getSelection().toArray();
            final Path[] paths = new Path[array.length];
            for(int i=0;i<paths.length;i++) paths[i] = (Path) array[i];
            WTableView.this.setSelectedPath(paths);
        }
    };

    private final PreviewPresenter delegatePresenter = new PreviewPresenter();
    private int previewType;
    
    public WTableView() {
        this(PREVIEW_IMAGE,28);
    }
    
    public WTableView(int previewType, int rowSize) {
        super(createGlyph("\uE026"),new Chars("Detail"));
        this.previewType = previewType;
        setLayout(new BorderLayout());
        addChild(table, BorderConstraint.CENTER);
        
        table.setRowHeight(rowSize);
        
        final Column cmPreview = new DefaultColumn(Chars.EMPTY,delegatePresenter);
        cmPreview.setBestWidth(32);
        final Column cmName = new DefaultColumn(new Chars("Name"),new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                final WLabel lbl = new WLabel();
                if(candidate instanceof Path){
                    lbl.setHorizontalAlignment(WLabel.HALIGN_LEFT);
                    lbl.setText(((Path) candidate).getName());
                    lbl.getStyle().getSelfRule().setProperties(new Chars("margin:[1,1,1,3]"));
                }
                return lbl;
            }
        });
        cmName.setBestWidth(FormLayout.SIZE_EXPAND);

        final Column cmMime = new DefaultColumn(new Chars("Mime-type"),new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                final WLabel lbl = new WLabel();
                if(candidate instanceof Path){
                    final Format format = Formats.findFormat((Path)candidate);
                    if(format!=null){
                        final Chars[] mimeTypes = format.getMimeTypes();
                        if(mimeTypes.length>0){
                            lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);
                            lbl.setText(mimeTypes[0]);
                        }
                    }
                }
                return lbl;
            }
        });
        cmMime.setBestWidth(120);

        final Column cmSize = new DefaultColumn(new Chars("Size"),new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                final WLabel lbl = new WLabel();
                if(candidate instanceof Path){
                    Path p = (Path) candidate;
                    try {
                        if(!p.isContainer()) {
                            final Object cdt = p.getPathInfo(Path.INFO_OCTETSIZE);
                            if(cdt instanceof Number) {
                                lbl.setHorizontalAlignment(WLabel.HALIGN_RIGHT);
                                long size = ((Number)cdt).longValue();
                                Chars end = new Chars(" o  ");
                                if (size > 1024l*1024l*1024l*1024l) {
                                    end = new Chars(" To ");
                                    size /= 1024l*1024l*1024l*1024l;
                                }else if (size > 1024l*1024l*1024l) {
                                    end = new Chars(" Go ");
                                    size /= 1024l*1024l*1024l;
                                }else if (size > 1024l*1024l) {
                                    end = new Chars(" Mo ");
                                    size /= 1024l*1024l;
                                }else if (size > 1024l) {
                                    end = new Chars(" Ko ");
                                    size /= 1024l;
                                }
                                lbl.setText(Int64.encode(size).concat(end));
                            }
                        }
                    } catch (IOException ex) {
                    }
                }
                return lbl;
            }
        });
        cmSize.setBestWidth(80);

        final Column cmDate = new DefaultColumn(new Chars("Modified"),new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                final WLabel lbl = new WLabel();
                if(candidate instanceof Path){
                    Path p = (Path) candidate;
                    try {
                        if(!p.isContainer()) {
                            final Object cdt = p.getPathInfo(Path.INFO_LASTMODIFIED);
                            if(cdt instanceof Number) {
                                lbl.setHorizontalAlignment(WLabel.HALIGN_RIGHT);
                                lbl.setText(ISO8601.toChars(((Number) cdt).longValue()));
                                lbl.getStyle().getSelfRule().setProperties(new Chars("margin:1"));
                            }
                        }
                    } catch (IOException ex) {
                    }
                }
                return lbl;
            }
        });
        cmDate.setBestWidth(170);
        
        table.getColumns().add(cmPreview);
        table.getColumns().add(cmName);
        table.getColumns().add(cmMime);
        table.getColumns().add(cmSize);
        table.getColumns().add(cmDate);
        table.getSelection().addEventListener(CollectionMessage.PREDICATE, tableListener);

        //open folder on double click
        table.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage message = (MouseMessage) event.getMessage();
                if (message.getNbClick() >= 2){
                    Object[] selection = table.getSelection().toArray();
                    if (selection.length>0 && selection[0] instanceof Path) {
                        try {
                            if (((Path)selection[0]).isContainer()) {
                                //open selected container
                                setViewRoot((Path) selection[0]);
                            }
                        } catch (IOException ex) {
                        }
                    }
                }
            }
        });
    }

    public void setViewRoot(Path viewRoot) {
        if(setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot)){
            updateTable(true);
        }
    }

    public void setSelectedPath(Path[] path) {
        if(setPropertyValue(PROPERTY_SELECTED_PATHS, path)){
            updateTable(false);
        }
    }

    public void setFilter(Predicate filter) {
        if(setPropertyValue(PROPERTY_FILTER,filter)){
            updateTable(true);
        }
    }
    
    private void updateTable(boolean all){

        if(all){
            final Path viewRoot = getViewRoot();

            Sequence datas;
            if(viewRoot==null){
                datas = new ArraySequence();
            }else{
                datas = new ArraySequence(viewRoot.getChildren());
            }

            final Predicate filter = getFilterInternal(true);
            datas = Collections.filter(datas, filter);

            //sort by name and type(folder/file)
            Collections.sort(datas, WPathChooser.FILE_SORTER);

            final RowModel rowModel = new DefaultRowModel(datas);
            table.setRowModel(rowModel);
        }

        //restore the selection
        table.getSelection().replaceAll(getSelectedPath());
    }

    private class PreviewPresenter extends DefaultObjectPresenter{

        public Widget createWidget(Object candidate) {
            if(previewType != PREVIEW_NONE){
                //TODO add image preview
                if(candidate instanceof Path){
                    //TODO handle mime type
                    WThumbnail thumbnail = new WThumbnail((Path)candidate, true, null,false,true, getFilter());
                    thumbnail.getStyle().getSelfRule().setProperties(new Chars("margin:2"));
                    return thumbnail;
                }
            }
            return new WSpace();
        }
    }
    
}
