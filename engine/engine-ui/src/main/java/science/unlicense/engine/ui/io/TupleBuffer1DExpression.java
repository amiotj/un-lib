
package science.unlicense.engine.ui.io;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.AbstractTupleBuffer1D;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.number.Primitive;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.geometry.Geometries;

/**
 *
 * @author Johann Sorel
 */
public class TupleBuffer1DExpression extends AbstractTupleBuffer1D {

    private final Sequence coords;
    private Buffer buffer = null;
    private Widget widget;
    
    public TupleBuffer1DExpression() {
        this.coords = null;
    }

    public TupleBuffer1DExpression(Sequence tuples) {
        this.coords = tuples;
        updateModel(new Extent.Long((long)tuples.getSize()), Primitive.TYPE_DOUBLE, ((Tuple)tuples.get(0)).getSize());
    }

    public Widget getWidget() {
        return widget;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }
    
    @Override
    public Buffer getPrimitiveBuffer() {
        if(buffer==null){
            buffer = Geometries.toTupleBuffer(coords).getPrimitiveBuffer();
        }
        return buffer;
    }

    @Override
    public TupleBuffer create(Extent.Long dimensions, BufferFactory factory) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int getDimension() {
        return (int) getExtent().getL(0);
    }

    @Override
    public TupleBuffer1D copy(Buffer buffer) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public TupleBuffer1D copy() {
        return new TupleBuffer1DExpression(coords);
    }
    
    @Override
    public double[] getTupleDouble(int[] coordinate, double[] buffer) {
        if(buffer==null) buffer = new double[nbSample];
        final Tuple t = (Tuple) coords.get(coordinate[0]);
        t.toArrayDouble(buffer);
        return buffer;
    }
    
    @Override
    public void setTuple(int coordinate, Object sample) {
        throw new UnimplementedException("Not supported yet.");
    }
    
    public void evaluate(Object widget){
        buffer = null;
        for(int i=0,n=coords.getSize();i<n;i++){
            final TupleExpression te = (TupleExpression) coords.get(i);
            te.evaluate(widget);
        }
    }
    
}
