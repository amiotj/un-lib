

package science.unlicense.engine.ui.component.path;

import science.unlicense.api.character.Chars;
import science.unlicense.api.predicate.Predicate;

/**
 *
 * @author Johann Sorel
 */
public interface PathPredicate extends Predicate{
    
    /**
     * 
     * @return Chars
     */
    Chars getText();
    
}
