
package science.unlicense.engine.ui.visual;

import science.unlicense.api.model.doc.DocMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.Layout;
import science.unlicense.engine.ui.widget.WContainer;

/**
 *
 * @author Johann Sorel
 */
public class ContainerView extends WidgetView {

    public ContainerView(WContainer widget) {
        super(widget);
    }

    public WContainer getWidget() {
        return (WContainer) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {
        final Layout layout = getWidget().getLayout();
        if(layout==null) return;
        if(constraint==null || layout.getView().getExtent().equals(constraint)){
            buffer.set(layout.getExtents());
        }else{
            layout.getExtents(buffer, constraint);
        }
    }

    public void receiveStyleEvent(DocMessage event) {
        super.receiveStyleEvent(event);
        Layout layout = getWidget().getLayout();
        layout.setView(getWidget().getInnerExtent());
    }
    
}
