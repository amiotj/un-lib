
package science.unlicense.engine.ui.style;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.model.doc.DocMessage;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.event.WeakEventListener;
import science.unlicense.api.predicate.ClassPredicate;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.engine.ui.io.EqualsPredicate;
import science.unlicense.engine.ui.io.InPredicate;
import science.unlicense.engine.ui.io.ModExpression;
import science.unlicense.engine.ui.io.PropertyName;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.io.RSReader;

/**
 * 
 * @author Johann Sorel
 */
public class WStyle extends AbstractEventSource implements EventListener{
    
    public static final Chars PROPERTY_FLAGS = new Chars("Flags");
    public static final Chars STYLE_EVENT = new Chars("style");
    
    private final Widget widget;
    private final Sequence rules = new ArraySequence();
    private final StyleDocument selfRule = new StyleDocument();

    private WeakEventListener parentStyleListener = null;
    
    private final StyleDocument stackProperties = new StyleDocument();
    
    private Set styleChangingProperties = null;
    private Set filterChangingProperties = null;

    public WStyle(final Widget widget) {
        this.widget = widget;
        
        if(widget!=null){
            widget.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
                public void receiveEvent(Event event) {
                    final PropertyMessage pe = (PropertyMessage) event.getMessage();
                    final Chars pn = pe.getPropertyName();

                    //listen to parent change to update style property list
                    if(Widget.PROPERTY_PARENT.equals(pn)){
                        //detach listener
                        parentStyleListener.release();

                        //listen to new parent
                        Widget parent = (Widget) pe.getNewValue();
                        if(parent instanceof Widget){
                            parentStyleListener = new WeakEventListener(((Widget)parent).getStyle(), PropertyMessage.PREDICATE, WStyle.this);
                        }else{
                            //listen to the root
                            parentStyleListener = new WeakEventListener(SystemStyle.INSTANCE, PropertyMessage.PREDICATE, WStyle.this);
                        }
                        notifyChanged();
                        return;
                    }

                    synchronized (WStyle.this){
                        getValueProperties();
                        if(styleChangingProperties.contains(pn) || filterChangingProperties.contains(pn)){
                            //this property may affect the style, ask for a repaint
                            updateStackProperties();
                        }
                    }
                }
            });
            
            //listen to flags change
            widget.getFlags().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
                public void receiveEvent(Event event) {
                    //this property may affect the style, ask for a repaint
                    synchronized (WStyle.this){
                        getValueProperties();
                        if(styleChangingProperties.contains(PROPERTY_FLAGS) || filterChangingProperties.contains(PROPERTY_FLAGS)){
                            notifyChanged();
                        }
                    }
                }
            });
        }
        
        //clean the style property list on changes and repaint widget
        rules.addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                notifyChanged();
            }
        });
        
        //clean the style property list on changes and repaint widget
        //TODO event on dictionnary
        selfRule.addEventListener(DocMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                notifyChanged();
            }
        });
        
        updateStackProperties();
        
        //listen to parent style
        if(widget!=null){
            SceneNode parent = widget.getParent();
            if(parent instanceof Widget){
                parentStyleListener = new WeakEventListener(((Widget)parent).getStyle(), PropertyMessage.PREDICATE, WStyle.this);
            }else{
                //listen to the root
                parentStyleListener = new WeakEventListener(SystemStyle.INSTANCE, PropertyMessage.PREDICATE, WStyle.this);
            }
        }
        
    }

    public void notifyChanged(){
        styleChangingProperties = null;
        filterChangingProperties = null;
        updateStackProperties();
        sendPropertyEvent(this, STYLE_EVENT, null, null);
    }
    
    /**
     * @return widget having this style
     */
    public Widget getWidget() {
        return widget;
    }

    public StyleDocument getRule(Chars name){
        for(int i=0,n=rules.getSize();i<n;i++){
            final StyleDocument rule = (StyleDocument) rules.get(i);
            if(rule.getName().equals(name)){
                return rule;
            }
        }
        return null;
    }
    
    public Sequence getRules() {
        return rules;
    }

    /**
     * This rule only applies to this widget an is not inherited.
     * it is applied last in the rules chain.
     * @return StyleRule
     */
    public StyleDocument getSelfRule() {
        return selfRule;
    }
    
    public void addRules(Chars text) {
        rules.addAll(RSReader.readStyle(text).getRules());
    }
    
    public Iterator createRuleIterator(){
        if(widget==null) return Collections.emptyIterator();
        
        final Widget parent = (Widget) widget.getParent();
        if(rules.isEmpty()){
            if(parent!=null){
                return parent.getStyle().createRuleIterator();
            }else{
                //use default system style
                return SystemStyle.INSTANCE.createRuleIterator();
            }
        }else{
            if(parent!=null){
                return Collections.serie(new Iterator[]{parent.getStyle().createRuleIterator(),rules.createIterator()});
            }else{
                //use default system style
                return Collections.serie(new Iterator[]{SystemStyle.INSTANCE.createRuleIterator(),rules.createIterator()});
            }
        }
    }
    
    private Iterator createRuleIteratorInternal(){
        if(selfRule.getFieldNames().isEmpty()){
            return createRuleIterator();
        }else{
            return Collections.serie(new Iterator[]{createRuleIterator(),Collections.singletonIterator(selfRule)});
        }
    }
    
    public StyleDocument getStackProperties(){
        return stackProperties;
    }
    
    private void updateStackProperties(){
        final StyleDocument dico = new StyleDocument();
        loop(dico, createRuleIteratorInternal());
        
        //remove the filter property, irevelant now and may cause unnecessary events
        dico.setFilter(Predicate.TRUE);
        
        stackProperties.set(dico);
        stackProperties.setFilter(dico.getFilter());
    }
    
    private void loop(Document dico, Iterator ite){
        while(ite.hasNext()){
            final StyleDocument rule = (StyleDocument) ite.next();
            if(Boolean.TRUE.equals(rule.getFilter().evaluate(widget))){
                WidgetStyles.mergeDoc(dico, rule, false, true);
                loop(dico,rule.getRules().createIterator());
            }
        }
    }
    
    private Set getValueProperties(){
        if(styleChangingProperties!= null) return styleChangingProperties;
        final Set setValueProps = new HashSet();
        final Set setFilterProps = new HashSet();
        extractUsedProperties(setValueProps, setFilterProps, createRuleIteratorInternal());
        styleChangingProperties = setValueProps;
        filterChangingProperties = setFilterProps;
        return setValueProps;
    }
        
    private void extractUsedProperties(Set setValueProps, Set setFilterProps, Iterator ite){
        while(ite.hasNext()){
            final StyleDocument rule = (StyleDocument) ite.next();
            final Expression pe = rule.getFilter();
            
            extractPropertyNames(pe, setFilterProps);
            
            if(Boolean.TRUE.equals(pe.evaluate(widget))){
                //loop on all properties
                extractPropertyNames(rule, setValueProps);
                
                //search sub rules
                extractUsedProperties(setValueProps,setFilterProps,rule.getRules().createIterator());
            }
        }
    }
    
    public Object getPropertyValue(Chars name){
        final Expression exp = (Expression) getStackProperties().getField(name).getValue();
        if(exp==null) return null;
        return exp.evaluate(widget);
    }
    
    public Object getPropertyValue(Document doc, Chars name){
        final Expression exp = (Expression) doc.getField(name).getValue();
        if(exp==null) return null;
        return exp.evaluate(widget);
    }
    
    private void extractPropertyNames(Object val,Set names){
        if(val instanceof StyleDocument){
            final StyleDocument doc = (StyleDocument) val;
            final Iterator propIte = doc.getFieldNames().createIterator();
            while(propIte.hasNext()){
                Object[] vals = WidgetStyles.getFieldValues(doc.getField((Chars)propIte.next()));
                for(Object v : vals){
                    extractPropertyNames(v, names);
                }
            }
        }else if(val instanceof Expression){
            extractPropertyNames((Expression) val, names);
        }
    }
    
    private void extractPropertyNames(Expression exp, Set names){
        if(exp instanceof Predicate){
            if(exp instanceof EqualsPredicate){
                final EqualsPredicate p = (EqualsPredicate) exp;
                extractPropertyNames(p.getExp1(), names);
                extractPropertyNames(p.getExp2(), names);
            }else if(exp instanceof InPredicate){
                final InPredicate p = (InPredicate) exp;
                extractPropertyNames(p.getExp1(), names);
                extractPropertyNames(p.getExp2(), names);
            }
        }else if(exp instanceof PropertyName){
            names.add(((PropertyName)exp).getName());
        }else if(exp instanceof ModExpression){
            final ModExpression p = (ModExpression) exp;
            extractPropertyNames(p.getExp1(), names);
            extractPropertyNames(p.getExp2(), names);
        }
    }

    @Override
    public void receiveEvent(Event event) {
        notifyChanged();
    }

}
