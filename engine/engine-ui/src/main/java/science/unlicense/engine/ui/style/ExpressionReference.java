
package science.unlicense.engine.ui.style;

import science.unlicense.api.predicate.Expression;
import science.unlicense.api.character.Chars;
import science.unlicense.engine.ui.widget.Widget;

/**
 * Redirect expression evaluation to referenced expression.
 * 
 * @author Johann Sorel
 */
public class ExpressionReference implements Expression {

    private final Chars referenceProperty;

    public ExpressionReference(Chars referenceProperty) {
        this.referenceProperty = referenceProperty;
    }

    public Chars getReferenceProperty() {
        return referenceProperty;
    }
    
    public Object evaluate(Object candidate) {
        final Widget w = (Widget) candidate;
        final WStyle style = w.getStyle();
        return style.getPropertyValue(referenceProperty);
    }

    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.referenceProperty != null ? this.referenceProperty.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExpressionReference other = (ExpressionReference) obj;
        if (this.referenceProperty != other.referenceProperty && (this.referenceProperty == null || !this.referenceProperty.equals(other.referenceProperty))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return referenceProperty.toString();
    }
    
}
