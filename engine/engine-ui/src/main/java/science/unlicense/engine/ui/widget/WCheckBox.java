package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.predicate.Variable;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.engine.ui.visual.FontRelativeView;

/**
 * Two state widget, checked or unchecked.
 *
 * @author Johann Sorel
 */
public class WCheckBox extends AbstractLabeled{

    /**
     * Property for the widget check value.
     */
    public static final Chars PROPERTY_CHECK = new Chars("Check");

    final Graphic graphic = new Graphic();

    public WCheckBox() {
        this(null,null);
    }

    public WCheckBox(CharArray text) {
        this(text,null);
    }

    /**
     *
     * @param text
     * @param listener listener to attach on the Check property
     */
    public WCheckBox(CharArray text, EventListener listener) {
        setHorizontalAlignment(HALIGN_LEFT);
        setGraphicPlacement(GRAPHIC_LEFT);
        setGraphic(graphic);   
        setText(text);
        if(listener!=null){
            addEventListener(new PropertyPredicate(PROPERTY_CHECK), listener);
        }     
    }
    
    /**
     * Set check value.
     * @param check
     */
    public void setCheck(boolean check) {
        if(setPropertyValue(PROPERTY_CHECK,check,Boolean.FALSE)){
            graphic.sendPropertyEvent(PROPERTY_CHECK, !check, check);
        }
    }

    /**
     * Get check value.
     * @return boolean
     */
    public boolean isCheck() {
        //test equality, value could be null at first
        return (Boolean)getPropertyValue(PROPERTY_CHECK,Boolean.FALSE);
    }

    /**
     * Get check variable.
     * @return Variable.
     */
    public Variable varCheck(){
        return getProperty(PROPERTY_CHECK);
    }
    
    @Override
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        if(event.getMessage() instanceof MouseMessage && isStackEnable()){
            final MouseMessage me = (MouseMessage) event.getMessage();
            final int type = me.getType();

            if(MouseMessage.TYPE_PRESS == type && me.getButton() == MouseMessage.BUTTON_1){
                requestFocus();
                me.consume();
            }else if(MouseMessage.TYPE_RELEASE == type && me.getButton() == MouseMessage.BUTTON_1){
                me.consume();
                setCheck(!isCheck());
            }
        }
    }
    
    public class Graphic extends WLeaf{

        public Graphic() {
            setView(new FontRelativeView(Graphic.this));
        }

        private void sendPropertyEvent(Chars name, Object oldValue, Object newValue) {
            sendPropertyEvent(Graphic.this, name, oldValue, newValue);
        }

        public void setCheck(boolean check) {
            WCheckBox.this.setCheck(check);
        }

        public boolean isCheck() {
            return WCheckBox.this.isCheck();
        }
                
    }
    
}
