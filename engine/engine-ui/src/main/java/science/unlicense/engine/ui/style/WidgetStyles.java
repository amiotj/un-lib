
package science.unlicense.engine.ui.style;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.model.doc.Field;
import science.unlicense.api.geometry.TupleBuffer1D;
import science.unlicense.api.image.process.AbstractImageTaskDescriptor;
import science.unlicense.api.predicate.Constant;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.Maths;
import science.unlicense.api.task.Task;
import science.unlicense.api.task.TaskDescriptor;
import science.unlicense.api.task.Tasks;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.engine.ui.io.TupleBuffer1DExpression;
import science.unlicense.engine.ui.io.UWKTReader;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_BRUSH;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_BRUSH_PAINT;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_FAMILY;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_FILL_PAINT;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_FITTING;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_MARGIN;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_RADIUS;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_SHAPE;
import static science.unlicense.engine.ui.widget.Widget.STYLE_PROP_TRANSFORM;
import science.unlicense.impl.geometry.s2d.Curve;
import science.unlicense.impl.geometry.s2d.CurvePolygon;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.image.process.ConvolutionMatrices;
import science.unlicense.impl.image.process.ConvolveOperator;
import science.unlicense.impl.math.Affine2;

/**
 * convenient methods to manipulate widget styles.
 *
 * @author Johann Sorel
 */
public final class WidgetStyles {
    
    public static final Chars NONE = new Chars("none");
    public static final Constant NULL_CST = new Constant(null);

    private WidgetStyles() {
    }

    public static Margin readMargin(final WStyle style, Document doc, Chars propName){
        final Object marginDef = doc==null ? style.getPropertyValue(propName) : style.getPropertyValue(doc,propName);
            
        final Margin margin;
        if(marginDef instanceof Number){
            //same margin in all directions
            final double m = ((Number)marginDef).doubleValue();
            margin = new Margin(m, m, m, m);
        }else if(marginDef instanceof Sequence){
            final Sequence seq = (Sequence) marginDef;
            if(seq.getSize()==1){
                final double m = ((Number)((Constant)seq.get(0)).getValue()).doubleValue();
                margin = new Margin(m, m, m, m);
            }else{
                //size for each side
                margin = new Margin(
                        ((Number)((Constant)seq.get(0)).getValue()).doubleValue(),
                        ((Number)((Constant)seq.get(1)).getValue()).doubleValue(),
                        ((Number)((Constant)seq.get(2)).getValue()).doubleValue(),
                        ((Number)((Constant)seq.get(3)).getValue()).doubleValue()
                );
            }
        }else{
            //default or unvalid, set to zero
            margin = new Margin(0);
        }
        
        return margin;
    }
    
    public static AffineRW readTransform(final WStyle style, Document doc, Chars propName){
        final Object marginDef = doc==null ? style.getPropertyValue(propName) : style.getPropertyValue(doc,propName);
            
        AffineRW trs = null;
        if(marginDef instanceof Sequence){
            final Sequence seq = (Sequence) marginDef;
            //size for each side
            final double scaleX = ((Number)((Constant)seq.get(0)).getValue()).doubleValue();
            final double shearY = ((Number)((Constant)seq.get(1)).getValue()).doubleValue();
            final double shearX = ((Number)((Constant)seq.get(2)).getValue()).doubleValue();
            final double scaleY = ((Number)((Constant)seq.get(3)).getValue()).doubleValue();
            final double trsX   = ((Number)((Constant)seq.get(4)).getValue()).doubleValue();
            final double trsY   = ((Number)((Constant)seq.get(5)).getValue()).doubleValue();
            trs = new Affine2(
                    scaleX,shearX,trsX,
                    shearY,scaleY,trsY
            );
        }
        
        return trs;
    }
    
    /**
     * Rebuild shape styles from style.
     *
     * @param style
     * @param propName the property name containing the definition
     * @return ShapeStyle array
     */
    public static ShapeStyle[] readShapeStyle(final WStyle style, Chars propName){
        final Object[] values = WidgetStyles.getFieldValues(style.getStackProperties().getField(propName));
        final ShapeStyle[] borders = new ShapeStyle[values.length];
        for(int i=0;i<values.length;i++){
            if(values[i] instanceof Document){
                borders[i] = readShapeStyle(style, (Document)values[i]);
            }
        }
        return borders;
    }
    
    /**
     * Rebuild geometry styles from style.
     *
     * @param style
     * @param propName the property name containing the definition
     * @return GeomStyle array
     */
    public static GeomStyle[] readGeomStyle(final WStyle style, Chars propName){
        final Object[] values = WidgetStyles.getFieldValues(style.getStackProperties().getField(propName));
        final GeomStyle[] borders = new GeomStyle[values.length];
        for(int i=0;i<values.length;i++){
            if(values[i] instanceof Document){
                borders[i] = readGeomStyle(style, (Document)values[i]);
            }
        }
        return borders;
    }
    
    /**
     * Rebuild font style from style.
     *
     * @param style
     * @param propName the property name containing the definition
     * @return FontStyle
     */
    public static FontStyle readFontStyle(final WStyle style, Chars propName){
        final Object value = style.getStackProperties().getField(propName).getValue();
        if(value instanceof Document){
            return readFontStyle(style, (Document)value);
        }
        return null;
    }
    
    public static GeomStyle readGeomStyle(final WStyle style, final Document doc){
        final GeomStyle candidate = new GeomStyle();
        readElementStyle(style, doc, candidate);
        return candidate;
    }
    
    public static ShapeStyle readShapeStyle(final WStyle style, final Document doc){
        final ShapeStyle candidate = new ShapeStyle();
        readElementStyle(style, doc, candidate);
        return candidate;
    }
    
    public static FontStyle readFontStyle(final WStyle style, final Document doc){
        final FontStyle candidate = new FontStyle();
        readElementStyle(style, doc, candidate);
        return candidate;
    }

    public static Task[] readEffects(final WStyle style, Chars propName){
        final Object[] values = WidgetStyles.getFieldValues(style.getStackProperties().getField(propName));
        final Task[] effects = new Task[values.length];
        for(int i=0;i<values.length;i++){
            //TODO replace this by something configurable
            final TaskDescriptor desc = Tasks.getDescriptor(new Chars("convolve"));
            final Document param = new DefaultDocument(desc.getInputType());
            param.setFieldValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),null);
            param.setFieldValue(ConvolveOperator.INPUT_CONV_MATRIX.getId(),ConvolutionMatrices.SOBEL_HORIZONTAL);
            final Task op = desc.create();
            op.setInput(param);
            effects[i] = op;
        }
        return effects;
    }
    
    public static void readElementStyle(final WStyle style, final Document doc, GeomStyle candidate){
        if(doc==null) return;
        
        Brush brush =(Brush) style.getPropertyValue(doc,STYLE_PROP_BRUSH);
        final Paint brushPaint = asPaint(style.getPropertyValue(doc,STYLE_PROP_BRUSH_PAINT));
        final Paint fillPaint = asPaint(style.getPropertyValue(doc,STYLE_PROP_FILL_PAINT));
        candidate.setBrush(brush);
        candidate.setBrushPaint(brushPaint);
        candidate.setFillPaint(fillPaint);
        
        if(candidate instanceof ShapeStyle){
            Object radius = style.getPropertyValue(doc,STYLE_PROP_RADIUS);
            Object shape = style.getPropertyValue(doc,STYLE_PROP_SHAPE);
            
            final Margin margin = readMargin(style, doc, STYLE_PROP_MARGIN);            
            final ShapeStyle border = (ShapeStyle) candidate;
            border.setMargin(margin);
            border.setTransform(readTransform(style, doc, STYLE_PROP_TRANSFORM));
            border.setFitting((Chars) style.getPropertyValue(doc,STYLE_PROP_FITTING));

            if(shape!=null){
                final Geometry2D geom;
                if(shape instanceof Geometry2D){
                    geom = (Geometry2D) shape;
                }else{
                    try {
                        geom = (Geometry2D) new UWKTReader().read((Chars)shape);
                    } catch (IOException ex) {
                        throw new InvalidArgumentException("Unvalide shape : "+shape);
                    }
                }
                border.setCorners(null);
                border.setGeometry(geom);
            }else if(radius instanceof Number){
                final double r = ((Number)radius).doubleValue();
                border.setCorners(new double[]{r,r,r,r});
            }else if(radius instanceof Sequence){
                final Sequence seq = (Sequence) radius;
                double[] corners = new double[4];
                for(int i=0;i<seq.getSize();i++){
                    corners[i] = ((Number)((Constant)seq.get(i)).getValue()).doubleValue();
                }
                border.setCorners(corners);
            }else{
                border.setCorners(new double[]{0,0,0,0});
            }
            
        }else if(candidate instanceof FontStyle){
            final Font font = (Font) style.getPropertyValue(doc,STYLE_PROP_FAMILY);
            final FontStyle fs = (FontStyle) candidate;
            fs.setFont(font);
        }
        
    }

    public static Paint asPaint(Object candidate){
        if(candidate instanceof Paint){
            return (Paint) candidate;
        }else if(candidate instanceof Color){
            return new ColorPaint((Color)candidate);
        }else{
            return null;
        }
    }

    public static Document mergeDoc(Document base, Document diff, boolean createNew){
        return mergeDoc(base, diff, createNew, false);
    }

    /**
     *
     * @param base
     * @param diff
     * @param createNew
     * @param skipRules
     * @return
     */
    public static Document mergeDoc(Document base, Document diff, boolean createNew, boolean skipRules){
        //we don't use this since it will completly replace Document type values
        //we want to merge them
        //base.overrideProperties(over);
        
        if(base==null){
            if(createNew){
                return (Document) smartCopy(diff);
            }else{
                throw new InvalidArgumentException("Can not have base document null and createNew at false");
            }
        }else if(diff==null){
            if(createNew){
                return (Document) smartCopy(base);
            }else{
                return base;
            }
        }
        
        //merge docs
        final Document output;
        if(createNew){
            output = (Document) smartCopy(base);
        }else{
            output = base;
        }
        
        final Iterator ite = diff.getFieldNames().createIterator();
        while(ite.hasNext()){
            final Chars name = (Chars) ite.next();
            final Field baseProp = base.getField(name);
            final Field diffProp = diff.getField(name);
            final Field outProp = output.getField(name);
            final Object[] baseValues = getFieldValues(baseProp);
            final Object[] diffValues = getFieldValues(diffProp);
            
            if(diffValues.length==0){
                //nothing to replace
            }else if(baseValues.length==0){
                //replace values
                setFieldValues(outProp, diffValues);
            }else{
                //smart merge
                final int size = Maths.max(baseValues.length,diffValues.length);
                final Object[] res = new Object[size];
                for(int i=0;i<size;i++){
                    if(baseValues.length<=i){
                        //no more base values
                        res[i] = smartCopy(diffValues[i]);
                    }else if(diffValues.length<=i){
                        //no more diff values
                        res[i] = smartCopy(baseValues[i]);
                    }else{
                        ///merge
                        Object bv = baseValues[i];
                        Object dv = diffValues[i];
                        if(isNone(dv)){
                            //force null value
                            res[i] = NULL_CST;
                        }else if(isNone(bv)){
                            res[i] = smartCopy(dv);
                        }else if(bv instanceof Document || dv instanceof Document){
                            //merge doc
                            if(skipRules && isRuleOrTrigger((Document)dv)) {
                                res[i] = smartCopy(baseValues[i]);
                            } else {
                                res[i] = mergeDoc((Document)bv, (Document)dv, true, skipRules);
                            }
                        }else{
                            //replace value
                            res[i] = smartCopy(dv);
                        }
                    }
                }
                setFieldValues(outProp, res);
            }
        }
        
        return output;
    }
    
    private static Object smartCopy(Object obj){
        if(obj instanceof Document){
            final Document copy;
            if(obj instanceof StyleDocument){
                copy = new StyleDocument();
                ((StyleDocument)copy).setName(((StyleDocument)obj).getName());
            }else{
                copy = new DefaultDocument();
            }
            
            final Document doc = (Document) obj;
            final Iterator ite = doc.getFieldNames().createIterator();
            while(ite.hasNext()){
                final Chars name = (Chars) ite.next();
                final Object[] values = WidgetStyles.getFieldValues(doc.getField(name));
                final Object[] cp = new Object[values.length];
                for(int i=0;i<cp.length;i++){
                    cp[i] = smartCopy(values[i]);
                }
                setFieldValues(copy.getField(name), cp);
            }
            return copy;
        }else if(isNone(obj)){
            return null;
        }else{
            return obj;
        }
    }

    /**
     * Test if an expression is a 'none'.
     * None expressions are used to erase field values.
     *
     * @param candidate
     * @return true if given object is a non expression
     */
    public static boolean isNone(Object candidate){
        return NONE.equals(candidate) ||
         (candidate instanceof Constant && ((Constant)candidate).getValue() == null);
    }

    private static boolean isRuleOrTrigger(Document doc) {
        if(doc instanceof StyleDocument) {
            return ((StyleDocument)doc).isRule() || ((StyleDocument)doc).isTrigger();
        }
        return false;
    }
    
    public static void evaluate(Geometry2D geom, Object widget){
        if(geom instanceof Polygon){
            final Polygon p = (Polygon) geom;
            evaluate(p.getExterior(),widget);
        }else if(geom instanceof Polyline){
            final Polyline p = (Polyline) geom;
            evaluate(p,widget);
        }else if(geom instanceof CurvePolygon){
            final CurvePolygon p = (CurvePolygon) geom;
            evaluate(p.getExterior(),widget);
        }else if(geom instanceof TransformedGeometry2D){
            final TransformedGeometry2D p = (TransformedGeometry2D) geom;
            //TODO
        }else if(geom instanceof Curve){
            //TODO
        }else if(geom instanceof Path){
            final Path p = (Path) geom;
            //TODO
        }else{
            throw new UnimplementedException("Geometry type not supported yet : "+geom.getClass());
        }
    }
    
    @SuppressWarnings("empty-statement")
    private static void evaluate(Polyline geom, Object widget){
        final TupleBuffer1D coords = geom.getCoordinates();
        if(coords instanceof TupleBuffer1DExpression){
            ((TupleBuffer1DExpression)coords).evaluate(widget);
        }
    }

    public static Object getFieldValue(Field field) {
        Object value = field.getValue();
        if (value==null) {
            return null;
        } else if(value.getClass().isArray()) {
            return Arrays.getValue(value,0);
        } else {
            return value;
        }
    }

    public static Object[] getFieldValues(Field field) {
        Object value = field.getValue();
        if (value==null) {
            return Arrays.ARRAY_OBJECT_EMPTY;
        } else if(value.getClass().isArray()) {
            return (Object[])value;
        } else {
            return new Object[]{value};
        }
    }

    public static void setFieldValues(Field field, Object value) {
        if (value==null) {
            field.setValue(null);
        } else if(value instanceof Object[] && ((Object[])value).length==1) {
            field.setValue(((Object[])value)[0]);
        } else if(value.getClass().isArray() && Arrays.getSize(value)==1) {
            field.setValue(Arrays.getValue(value,0));
        } else {
            field.setValue(value);
        }
    }

}
