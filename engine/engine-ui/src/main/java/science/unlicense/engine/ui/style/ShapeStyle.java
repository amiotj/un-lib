
package science.unlicense.engine.ui.style;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.math.AffineRW;
import static science.unlicense.api.math.Maths.min;
import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.Paint;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Path;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.RoundedRectangle;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.math.Affine2;

/**
 * Define a widget border.
 * 
 * @author Johann Sorel
 */
public class ShapeStyle extends GeomStyle{
    
    public static final Chars FITTING_SCALED = new Chars("scaled");
    public static final Chars FITTING_CENTERED = new Chars("centered");
    public static final Chars FITTING_STRETCHED = new Chars("stretched");
    public static final Chars FITTING_ZOOMED = new Chars("zoomed");
    public static final Chars FITTING_CORNER = new Chars("corner");

    public static final int MARGIN_TOP = 0;
    public static final int MARGIN_RIGHT = 1;
    public static final int MARGIN_BOTTOM = 2;
    public static final int MARGIN_LEFT = 3;
    
    // RUL, RUR, RLR, RLL
    private double[] cns;
    private Margin margin;
    private Geometry2D geometry;
    private Transform transform;
    private Chars fitting;

    public ShapeStyle() {
        this(0,0,0,0,null,null,null,null,null);
    }
    
    public ShapeStyle(double round, Brush brush, Paint brushPaint, Paint fillPaint, Margin margin, Geometry2D geometry) {
        this(round,round,round,round,brush,brushPaint,fillPaint,margin, geometry);
    }

    public ShapeStyle(double rul, double rur, double rlr, double rll,
            Brush brush, Paint brushPaint, Paint fillPaint, Margin margin,Geometry2D geometry) {
        super(brush,brushPaint,fillPaint);
        this.cns = new double[]{rul,rur,rlr,rll};
        this.margin = margin;
        this.geometry = geometry;
    }

    /**
     * Create the border shape.
     * @param extent
     * @return Geometry2D, can be null
     */
    public Geometry2D getShape(Rectangle extent) {
        
        //append margins
        extent = new Rectangle(extent);
        extent.setX(extent.getX()-margin.left);
        extent.setY(extent.getY()-margin.top);
        extent.setWidth(extent.getWidth() + margin.right + margin.left);
        extent.setHeight(extent.getHeight() + margin.top + margin.bottom);
        
        Geometry2D geom;
        if(geometry!=null){
            WidgetStyles.evaluate(geometry, extent);
            geom = geometry;
        }else if(cns[0]==cns[1] && cns[0]==cns[2] && cns[0]==cns[3]){
            geom = new RoundedRectangle(extent.getX(), extent.getY(), 
                    extent.getWidth(), extent.getHeight(),cns[0],cns[0]);
        }else{
            //various radius, make a custom path
            final double x = extent.getX();
            final double y = extent.getY();
            final double width = extent.getWidth();
            final double height = extent.getHeight();
            final double mx = width/2.0;
            final double my = height/2.0;

            final Path path = new Path();
            path.appendMoveTo(x,                   y+min(my,cns[0]));
            path.appendQuadTo(x,                   y,           x+min(mx,cns[0]),      y);
            path.appendLineTo(x+width-min(mx,cns[1]), y);
            path.appendQuadTo(x+width,             y,           x+width,            y+min(my,cns[1]));
            path.appendLineTo(x+width,             y+height-min(my,cns[2]));
            path.appendQuadTo(x+width,             y+height,    x+width-min(mx,cns[2]),y+height);
            path.appendLineTo(x+min(mx,cns[3]),    y+height);
            path.appendQuadTo(x,                   y+height,    x,                  y+height-min(my,cns[3]));
            path.appendClose();
            geom = path;
        }

        //apply fitting
        if(fitting!=null){
            AffineRW trs;
            final BBox glyphBox = geom.getBoundingBox();
            final BBox target = extent.getBoundingBox();
            if(FITTING_SCALED.equals(fitting, true, true)){
                trs = Projections.scaled(glyphBox,target);
            }else if(FITTING_CENTERED.equals(fitting, true, true)){
                trs = Projections.centered(glyphBox,target);
            }else if(FITTING_STRETCHED.equals(fitting, true, true)){
                trs = Projections.stretched(glyphBox,target);
            }else if(FITTING_ZOOMED.equals(fitting, true, true)){
                trs = Projections.zoomed(glyphBox,target);
            }else if(FITTING_CORNER.equals(fitting, true, true)){
                trs = new Affine2();
                trs.set(0, 2, target.getMin(0));
                trs.set(1, 2, target.getMin(1));
            }else{
                throw new InvalidArgumentException("Unvalid fitting "+fitting);
            }
            geom = new TransformedGeometry2D(geom, trs);
        }

        //apply transform
        if(transform!=null){
            geom = new TransformedGeometry2D(geom, transform);
        }
        
        return geom;
    }

    /**
     * Get the border expected margin, this is used to offset the inner
     * parts of the widget.
     * @return Margin, never null 
     */
    public Margin getMargin() {
        return margin;
    }

    public void setMargin(Margin margin) {
        this.margin = margin;
    }

    public Geometry2D getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry2D geometry) {
        this.geometry = geometry;
    }

    public double[] getCorners() {
        return cns;
    }

    public void setCorners(double[] corners) {
        this.cns = corners;
    }

    public Transform getTransform() {
        return transform;
    }

    public void setTransform(Transform transform) {
        this.transform = transform;
    }

    public Chars getFitting() {
        return fitting;
    }

    public void setFitting(Chars fitting) {
        this.fitting = fitting;
    }

}
