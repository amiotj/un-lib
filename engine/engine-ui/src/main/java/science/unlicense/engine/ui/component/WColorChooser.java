

package science.unlicense.engine.ui.component;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.predicate.Variable;
import science.unlicense.engine.ui.model.SpinnerEditor;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.WValueWidget;

/**
 *
 * @author Johann Sorel
 */
public class WColorChooser extends WContainer implements WValueWidget{

    /**
     * Property for the widget color.
     */
    public static final Chars PROPERTY_VALUE = SpinnerEditor.PROPERTY_VALUE;
    
    private final WColorPane view = new WColorPane();
    private final WColorSquare square = new WColorSquare();
    private final WRGBColorEditor rgbEditor = new WRGBColorEditor();
    private final WHSLColorEditor hslEditor = new WHSLColorEditor();
    
    public WColorChooser() {
        this(Color.RED);
    }
    public WColorChooser(Color baseColor) {
        super(new FormLayout());
        setValue(baseColor);
        
        addChild(view, FillConstraint.builder().coord(0, 0).fill(true, true).build());
        addChild(square, FillConstraint.builder().coord(0, 1).fill(true, true).span(2, 1).build());
        
        final WTabContainer tabs = new WTabContainer();
        addChild(tabs, FillConstraint.builder().coord(1, 0).fill(true, true).build());
        tabs.addTab(rgbEditor, new WLabel(new Chars("RGB")));
        tabs.addTab(hslEditor, new WLabel(new Chars("HSL")));
        
        varValue().sync(view.getProperty(PROPERTY_VALUE));
        varValue().sync(square.getProperty(PROPERTY_VALUE));
        varValue().sync(rgbEditor.getProperty(PROPERTY_VALUE));
        varValue().sync(hslEditor.getProperty(PROPERTY_VALUE));        
    }
    
    public Color getValue() {
        return (Color)getPropertyValue(PROPERTY_VALUE);
    }

    public void setValue(Color color) {
        setPropertyValue(PROPERTY_VALUE,color);
    }

    @Override
    public void setValue(Object value) {
        setPropertyValue(PROPERTY_VALUE,value);
    }

    @Override
    public Variable varValue() {
        return getProperty(PROPERTY_VALUE);
    }
    
    public static final class WColorSquare extends WSpace{
        
        public WColorSquare() {
            super(new Extent.Double(20, 20));
            getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint:colorfill(\"Value\");};"));
        }

        public Color getValue() {
            return (Color)getPropertyValue(PROPERTY_VALUE);
        }

        public void setValue(Color color) {
            setPropertyValue(PROPERTY_VALUE,color);
        }
        
    }
    
}
