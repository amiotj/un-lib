

package science.unlicense.engine.ui.widget;

/**
 * Object used in mutiple widgets.
 * 
 * @author Johann Sorel
 */
public final class WConstants {
        
    private WConstants(){}
    
}
