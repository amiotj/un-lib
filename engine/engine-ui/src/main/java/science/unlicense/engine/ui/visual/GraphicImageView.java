
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.scenegraph.s2d.Graphic2DFactory;
import science.unlicense.api.scenegraph.s2d.ImageNode2D;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.Matrix3x3;

/**
 *
 * @author Johann Sorel
 */
public class GraphicImageView extends WidgetView {
    
    private final WGraphicImage wimg;
    
    //visual cache
    private SceneNode graphic = null;

    public GraphicImageView(WGraphicImage labeled) {
        super(labeled);
        this.wimg = labeled;
    }

    public WGraphicImage getWidget() {
        return (WGraphicImage) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {

        final Image image = getWidget().getImage();
        if(image != null){
            Extent.Long extent = image.getExtent();
            buffer.setAll(extent);
        }else{
            buffer.setAll(0, 0);
        }
    }
    
    private SceneNode getGraphic(Painter2D painter){

        final Image image = wimg.getImage();

        SceneNode node = graphic;
        if(graphic==null){
            final Graphic2DFactory factory = painter.getGraphicFactory();
            if(image!=null){
                final ImageNode2D imgNode = factory.createImageNode();
                imgNode.setImage(image);
                node = imgNode;
            }else{
                node = factory.createNode();
            }
        }

        //adjust fitting
        if(image!=null){
            final BBox innerBBox = getWidget().getInnerExtent();
            final int fitting = wimg.getFitting();

            final Extent imageExtent = image.getExtent();
            final BBox imgExt = new BBox(imageExtent);
            AffineRW trs;
            if(fitting == WGraphicImage.FITTING_CENTERED) trs = Projections.centered(imgExt,innerBBox);
            else if(fitting == WGraphicImage.FITTING_ZOOMED) trs = Projections.zoomed(imgExt,innerBBox);
            else if(fitting == WGraphicImage.FITTING_STRETCHED) trs = Projections.stretched(imgExt,innerBBox);
            else if(fitting == WGraphicImage.FITTING_CORNER){
                trs = new Affine2();
                trs.set(0, 2, innerBBox.getMin(0));
                trs.set(1, 2, innerBBox.getMin(1));
            }
            else trs = Projections.scaled(imgExt,innerBBox);
            node.getNodeTransform().set(trs);
        }


        graphic = node;
        return graphic;
    }
    
    protected void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {
        final SceneNode node = getGraphic(painter);
        if(node!=null) painter.render(node);
    }

    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if( WGraphicImage.PROPERTY_IMAGE.equals(name) ||
            WGraphicImage.PROPERTY_FITTING.equals(name) ){
            graphic = null;
            widget.updateExtents();
            widget.setDirty();
        }
    }

}
