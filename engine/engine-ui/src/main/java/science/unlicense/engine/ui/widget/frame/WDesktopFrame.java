
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.FrameDecoration;
import science.unlicense.api.desktop.FrameManager;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.layout.StackConstraint;
import science.unlicense.api.layout.StackLayout;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.event.Property;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.UnimplementedException;

/**
 *
 * @author Johann Sorel
 */
public class WDesktopFrame extends WContainer implements UIFrame {
    
    
    private final WDesktop desktop;
    private final WContainer contentPane = new WContainer(new BorderLayout());
    private final WSpace padLeft = new WSpace();
    private final WSpace padRight = new WSpace();
    private final WSpace padTop = new WSpace();
    private final WSpace padBottom = new WSpace();
    private final WContainer content = new WContainer();
    private WFrameDecoration decoration = null;
    private boolean disposed = false;

    public WDesktopFrame(WDesktop desktop) {
        super(new StackLayout());
        this.desktop = desktop;
        contentPane.addChild(content, BorderConstraint.CENTER);
        contentPane.addChild(padLeft, BorderConstraint.LEFT);
        contentPane.addChild(padRight, BorderConstraint.RIGHT);
        contentPane.addChild(padTop, BorderConstraint.TOP);
        contentPane.addChild(padBottom, BorderConstraint.BOTTOM);
        addChild(contentPane, new StackConstraint(1));
        
        setDecoration(new WSystemDecoration());
    }

    @Override
    public UIFrame getParentFrame() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Sequence getChildrenFrames() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public FrameManager getManager() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public UIFrame getFrame(){
        return this;
    }
    
    @Override
    public WContainer getContainer() {
        return content;
    }

    @Override
    public Widget getFocusedWidget() {
        return (Widget) getPropertyValue(PROP_FOCUSED_WIDGET);
    }

    @Override
    public void setFocusedWidget(Widget widget) {
        setPropertyValue(PROP_FOCUSED_WIDGET, widget);
        desktop.getFrame().setFocusedWidget(widget);
    }

    @Override
    public CharArray getTitle() {
        return (CharArray) getPropertyValue(PROP_TITLE);
    }

    @Override
    public void setTitle(CharArray title) {
        setPropertyValue(PROP_TITLE, title);
    }

    @Override
    public Property varTitle() {
        return getProperty(PROP_TITLE);
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        final Extent ext = desktop.getEffectiveExtent();
        final Extent size = getEffectiveExtent();
        getNodeTransform().getTranslation().setXY(
                location.getX()-ext.get(0)/2.0 + size.get(0)/2.0, 
                location.getY()-ext.get(1)/2.0 + size.get(1)/2.0);
        getNodeTransform().notifyChanged();
    }

    @Override
    public Tuple getOnScreenLocation() {
        final TupleRW t = getNodeTransform().getTranslation().copy();
        final Extent ext = desktop.getEffectiveExtent();
        final Extent size = getEffectiveExtent();
        t.setXY(
                t.getX() + ext.get(0)/2.0 - size.get(0)/2.0, 
                t.getY() + ext.get(1)/2.0 - size.get(1)/2.0);
        return t;
    }

    @Override
    public void setSize(int width, int height) {
        //when resizing we must preserve the top left corner position
        final Tuple pos = getOnScreenLocation();
        setOverrideExtents(new Extents(width, height));
        setEffectiveExtent(new Extent.Double(width, height));
        setOnScreenLocation(pos);
    }

    @Override
    public Extent getSize() {
        return getEffectiveExtent();
    }

    @Override
    public FrameDecoration getSystemDecoration() {
        return new WSystemDecoration();
    }

    @Override
    public void setDecoration(FrameDecoration decoration) {
        if(this.decoration==decoration) return;
        if(!(decoration instanceof WFrameDecoration)){
            throw new InvalidArgumentException("Frame decoration must be a WFrameDecoration");
        }
        
        if(this.decoration!=null){
            children.remove(((WFrameDecoration)this.decoration).getWidget());
            this.decoration.setFrame(null);
        }
        
        this.decoration = (WFrameDecoration) decoration;
        
        if(this.decoration!=null){
            this.decoration.getWidget().setLayoutConstraint(new StackConstraint(0));
            children.add(0,this.decoration.getWidget());
            final Margin margin = decoration.getMargin();
            padLeft.setOverrideExtents(new Extents(margin.left, 0));
            padRight.setOverrideExtents(new Extents(margin.right, 0));
            padTop.setOverrideExtents(new Extents(0, margin.top));
            padBottom.setOverrideExtents(new Extents(0, margin.bottom));
            this.decoration.setFrame(this);
        }else{
            padLeft.setOverrideExtents(new Extents(0, 0));
            padRight.setOverrideExtents(new Extents(0, 0));
            padTop.setOverrideExtents(new Extents(0, 0));
            padBottom.setOverrideExtents(new Extents(0, 0));
        }
    }

    @Override
    public FrameDecoration getDecoration() {
        return decoration;
    }

    @Override
    public int getState() {
        return (Integer) getPropertyValue(PROP_STATE,STATE_NORMAL);
    }

    @Override
    public void setState(int state) {
        setPropertyValue(PROP_STATE, state, STATE_NORMAL);
        //TODO
    }
    
    @Override
    public void setMaximizable(boolean maximizable) {
        setPropertyValue(PROP_MAXIMIZABLE,maximizable,Boolean.TRUE);
    }

    @Override
    public boolean isMaximizable() {
        return (Boolean)getPropertyValue(PROP_MAXIMIZABLE,Boolean.TRUE);
    }

    @Override
    public Property varMaximizable() {
        return getProperty(PROP_MAXIMIZABLE);
    }

    @Override
    public void setMinimizable(boolean minimizable) {
        setPropertyValue(PROP_MINIMIZABLE,minimizable,Boolean.TRUE);
    }

    @Override
    public boolean isMinimizable() {
        return (Boolean)getPropertyValue(PROP_MINIMIZABLE,Boolean.TRUE);
    }
    
    @Override
    public Property varMinimizable() {
        return getProperty(PROP_MINIMIZABLE);
    }
    
    @Override
    public void setClosable(boolean closable) {
        setPropertyValue(PROP_CLOSABLE,closable,Boolean.TRUE);
    }

    @Override
    public boolean isClosable() {
        return (Boolean)getPropertyValue(PROP_CLOSABLE,Boolean.TRUE);
    }
        
    @Override
    public Property varClosable() {
        return getProperty(PROP_CLOSABLE);
    }
    
    @Override
    public void setAlwaysonTop(boolean ontop) {
        setPropertyValue(PROP_ALWAYSONTOP,ontop,Boolean.FALSE);
    }

    @Override
    public boolean isAlwaysOnTop() {
        return (Boolean)getPropertyValue(PROP_ALWAYSONTOP,Boolean.FALSE);
    }

    @Override
    public boolean isTranslucent() {
        return true;
    }

    @Override
    public void dispose() {
        disposed = true;
        desktop.getChildren().remove(this);
    }

    @Override
    public boolean isDisposed() {
        return disposed;
    }

    @Override
    public boolean isModale() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void waitForDisposal() {
        throw new UnimplementedException("Not supported yet.");
    }

}
