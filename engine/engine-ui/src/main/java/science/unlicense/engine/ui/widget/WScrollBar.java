
package science.unlicense.engine.ui.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.engine.ui.visual.ScrollBarView;

/**
 *
 * @author Johann Sorel
 */
public class WScrollBar extends WContainer{

    public static final Chars FLAG_TOP      = new Chars("scrollbar-top");
    public static final Chars FLAG_BOTTOM   = new Chars("scrollbar-bottom");
    public static final Chars FLAG_LEFT     = new Chars("scrollbar-left");
    public static final Chars FLAG_RIGHT    = new Chars("scrollbar-right");
    
    public static final Chars STYPE_PROP_CURSOR_PAINT = new Chars("cursor-paint");
    
    public static final Chars PROPERTY_HORIZONTAL = new Chars("Horizontal");
    public static final Chars PROPERTY_RATIO = new Chars("Ratio");
    public static final Chars PROPERTY_CURSORSIZE = new Chars("CursorSize");
    public static final Chars PROPERTY_STEP = new Chars("Step");
    public static final Chars PROPERTY_HANDLEVISIBLE = new Chars("HandleVisible");
    
    private final WAction corner1 = new WAction(null,null,new EventListener() {
        public void receiveEvent(Event event) {
            double d = getRatio()-getStep();
            if(d<0)d=0;
            setRatio(d);
        }
    });
    private final WAction corner2 = new WAction(null,null,new EventListener() {
        public void receiveEvent(Event event) {
            double d = getRatio()+getStep();
            if(d>1)d=1;
            setRatio(d);
        }
    });
    private final Scroll scroll = new Scroll();
    
    public WScrollBar() {
        this(false);
    }
    
    public WScrollBar(boolean horizontal) {
        setPropertyValue(PROPERTY_HORIZONTAL, horizontal);
        setPropertyValue(PROPERTY_RATIO, 0.0);
        setPropertyValue(PROPERTY_CURSORSIZE, 0.0);
        setPropertyValue(PROPERTY_STEP, 0.1);
                
        final BorderLayout layout = new BorderLayout();
        setLayout(layout);
        children.add(scroll);
        
        updateLayout();
    }
    
    private void updateLayout(){
        if(isHorizontal()){
            corner1.getFlags().replaceAll(new Object[]{FLAG_LEFT});
            corner2.getFlags().replaceAll(new Object[]{FLAG_RIGHT});
            corner1.setLayoutConstraint(BorderConstraint.LEFT);
            scroll.setLayoutConstraint(BorderConstraint.CENTER);
            corner2.setLayoutConstraint(BorderConstraint.RIGHT);
        }else{
            corner1.getFlags().replaceAll(new Object[]{FLAG_TOP});
            corner2.getFlags().replaceAll(new Object[]{FLAG_BOTTOM});
            corner1.setLayoutConstraint(BorderConstraint.TOP);
            scroll.setLayoutConstraint(BorderConstraint.CENTER);
            corner2.setLayoutConstraint(BorderConstraint.BOTTOM);
        } 
    }

    public void setRatio(double ratio) {
        final double oldRatio = getRatio();
        if(setPropertyValue(PROPERTY_RATIO, ratio)){
            scroll.sendRatioEvent(oldRatio, ratio);
        }
    }

    public double getRatio() {
        return (Double)getPropertyValue(PROPERTY_RATIO);
    }

    public void setStep(double step) {
        setPropertyValue(PROPERTY_STEP, step);
    }

    public double getStep() {
        return (Double)getPropertyValue(PROPERTY_STEP);
    }

    public void setHorizontal(boolean horizontal) {
        if(setPropertyValue(PROPERTY_HORIZONTAL, horizontal)){
            updateLayout();
        }
    }

    public boolean isHorizontal() {
        return (Boolean)getPropertyValue(PROPERTY_HORIZONTAL);
    }

    public void setCursorSize(double cursorSize) {
        setPropertyValue(PROPERTY_CURSORSIZE, cursorSize);
    }

    public double getCursorSize() {
        return (Double)getPropertyValue(PROPERTY_CURSORSIZE);
    }

    public void setHandleVisible(boolean visible) {
        if (setPropertyValue(PROPERTY_HANDLEVISIBLE, visible, Boolean.FALSE)) {
            if (visible) {
                children.add(corner1);
                children.add(corner2);
            } else {
                children.remove(corner1);
                children.remove(corner2);
            }
        }
    }

    public boolean isHandleVisible() {
        return Boolean.TRUE.equals(getPropertyValue(PROPERTY_HANDLEVISIBLE, Boolean.FALSE));
    }

    public class Scroll extends WLeaf{

        public Scroll() {
            setView(new ScrollBarView(this));
        }

        private void sendRatioEvent(Object oldValue, Object newValue) {
            sendPropertyEvent(Scroll.this, PROPERTY_RATIO, oldValue, newValue);
        }
        
        public double getRatio() {
            return WScrollBar.this.getRatio();
        }
    
        public WScrollBar getScrollBar(){
            return WScrollBar.this;
        }
        
    }
    
}
