
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Layout;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;

/**
 * Widget container which catch mouse event to resize the current frame. 
 * 
 * @author Johann Sorel
 */
public class WFrameResize extends WContainer {

    private final Vector startPos = new Vector(2);
    private final Extent startExt = new Extent.Double(2);
    private boolean dragging = false;
    private boolean resizeVertical = true;
    private boolean resizeHorizontal = true;

    public WFrameResize() {}

    public WFrameResize(Layout layout) {
        super(layout);
    }

    public boolean isResizeHorizontal() {
        return resizeHorizontal;
    }

    public boolean isResizeVertical() {
        return resizeVertical;
    }

    public void setResizeHorizontal(boolean resizeHorizontal) {
        this.resizeHorizontal = resizeHorizontal;
    }

    public void setResizeVertical(boolean resizeVertical) {
        this.resizeVertical = resizeVertical;
    }
    
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        final EventMessage message = event.getMessage();
        if(message.isConsumed() || !(message instanceof MouseMessage) ) return;
                
        final MouseMessage e = (MouseMessage) message;
        final int type = e.getType();

        final int mousebutton = e.getButton();
        if(MouseMessage.TYPE_PRESS==type){
            e.consume();
            dragging = true;
            startPos.set(e.getMouseScreenPosition());
            startExt.set(getFrame().getSize());
            requestFocus();

        }else if(MouseMessage.TYPE_RELEASE==type || MouseMessage.TYPE_EXIT==type){
            dragging = false;
            
        }else if(MouseMessage.TYPE_MOVE==type && dragging && mousebutton == 1){
            e.consume();
            double[] diff = Vectors.subtract(e.getMouseScreenPosition().getValues(), startPos.getValues());
            if(!resizeHorizontal) diff[0] = 0;
            if(!resizeVertical) diff[1] = 0;
            getFrame().setSize(
                    (int)(startExt.get(0)+diff[0]), 
                    (int)(startExt.get(1)+diff[1]));
        }

    }
}
