
package science.unlicense.engine.ui.widget.chart.model;

import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public interface Chart {
    
    Sequence getAxis();
    
}
