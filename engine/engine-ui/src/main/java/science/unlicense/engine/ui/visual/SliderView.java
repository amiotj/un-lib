
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.impl.geometry.s2d.Circle;
import science.unlicense.impl.geometry.s2d.RoundedRectangle;

/**
 *
 * @author Johann Sorel
 */
public class SliderView extends WidgetView {

    private static final double MARK_RADIUS = 7;
    private boolean pressed = false;
    
    public SliderView(WSlider widget) {
        super(widget);
    }

    public WSlider getWidget() {
        return (WSlider) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {
        buffer.minX = 120;
        buffer.minY = 14;
        buffer.bestX = buffer.minX;
        buffer.bestY = buffer.minY;
        buffer.maxX = Double.POSITIVE_INFINITY;
        buffer.maxY = Double.POSITIVE_INFINITY;
    }

    protected void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {
        final double width = innerBBox.getSpan(0);
        final double height = innerBBox.getSpan(1);

        double barHeight = 8;

        //paint the ramp
        final RoundedRectangle rect = new RoundedRectangle(
                innerBBox.getMin(0) + MARK_RADIUS, 
                innerBBox.getMin(1) + (height-barHeight)/2, 
                width-MARK_RADIUS*2, 
                barHeight);
        final Color bgColor = SystemStyle.getSystemColor(SystemStyle.COLOR_TEXT_BACKGROUND);
        final Color borderColor = SystemStyle.getSystemColor(SystemStyle.COLOR_MAIN);
        painter.setPaint(new ColorPaint(bgColor));
        painter.fill(rect);
        painter.setPaint(new ColorPaint(borderColor));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.stroke(rect);

        //paint the marker
        double ratio = getWidget().getModel().getRatio(getWidget().getValue());
        ratio *= width-MARK_RADIUS*2;
        final Color markerColor = SystemStyle.getSystemColor(SystemStyle.COLOR_FOCUS);
        final Circle circle = new Circle(
                innerBBox.getMin(0) + ratio+MARK_RADIUS, 
                innerBBox.getMin(1) + height/2, 
                MARK_RADIUS);
        painter.setPaint(new ColorPaint(markerColor));
        painter.fill(circle);
        painter.setPaint(new ColorPaint(borderColor));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.stroke(circle);

    }
    
    public void receiveEvent(Event event) {

        final EventMessage message = event.getMessage();
        if(!message.isConsumed() && message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if(me.getType() == MouseMessage.TYPE_PRESS){
                pressed = true;
                updateValue(me);
                me.consume();
            }else if(me.getType() == MouseMessage.TYPE_MOVE){
                if(pressed){
                    updateValue(me);
                    me.consume();
                }
            }else if(me.getType() == MouseMessage.TYPE_RELEASE){
                pressed = false;
                me.consume();
            }else if(me.getType() == MouseMessage.TYPE_EXIT){
                pressed = false;
            }
        }

    }

    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);
        
        final Chars pname = event.getPropertyName();
        if(   WSlider.PROPERTY_MODEL.equals(pname)
           || WSlider.PROPERTY_VALUE.equals(pname) ){
            widget.setDirty();
        }
    }
    
    private void updateValue(MouseMessage me){
        final Tuple tuple = me.getMousePosition();
        final BBox bbox = widget.getBoundingBox();
        final double width = bbox.getSpan(0) - MARK_RADIUS*2;
        final double x = Maths.clamp(tuple.getX()-MARK_RADIUS-bbox.getMin(0), 0, width);
        final Object value = getWidget().getModel().getValue(x/width);
        getWidget().setValue(value);
    }
    
}
