
package science.unlicense.engine.ui.style;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.model.doc.DefaultDocument;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.ui.io.RSReader;

/**
 * A style rule define a filter, a set of properties and sub-rules.
 * 
 * @author Johann Sorel
 */
public class StyleDocument extends DefaultDocument {
    
    /** Flag object used to set to null a property value */
    public static final Object NONE = new Object();
    public static final Chars PROP_FILTER = new Chars("filter");
    public static final Chars PROP_ISTRIGGER = new Chars("_isTrigger");
        
    private Chars name = Chars.EMPTY;

    public StyleDocument() {
        super(true);
    }

    public Chars getName() {
        return name;
    }

    public void setName(Chars name) {
        CObjects.ensureNotNull(name);
        this.name = name;
    }
    
    public Expression getFilter(){
        Object value = getFieldValue(PROP_FILTER);
        return (Expression) ((value instanceof Expression) ? value : Predicate.TRUE);
    }

    public void setFilter(Expression filter) {
        setProperty(PROP_FILTER, filter);
    }
    
    public boolean isTrigger(){
        return getFieldValue(PROP_ISTRIGGER) != null;
    }
    
    public boolean isRule(){
        return getFieldValue(PROP_FILTER) != null;
    }
    
    public Sequence getRules() {
        Sequence seq = null;
        
        final Iterator ite = getFieldNames().createIterator();
        while(ite.hasNext()){
            Object val = getFieldValue((Chars) ite.next());
            if(val instanceof StyleDocument && ((StyleDocument)val).isRule() ){
                if(seq==null) seq = new ArraySequence();
                seq.add(val);
            }
        }
        return seq==null ? Collections.emptySequence() : Collections.readOnlySequence(seq);
    }
    
    public Sequence getTriggers(){
        Sequence seq = null;
        final Iterator ite = properties.getValues().createIterator();
        while(ite.hasNext()){
            Object val = ite.next();
            if(val instanceof StyleDocument && ((StyleDocument)val).isTrigger()){
                if(seq==null) seq = new ArraySequence();
                seq.add(val);
            }
        }
        return seq==null ? Collections.emptySequence() : seq;
    }

    public void setProperty(Chars key, Chars value) {
        try {
            setProperty(key, RSReader.readExpression(value,true));
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }
    
    public void setProperty(Chars key, Expression value) {
        setFieldValue(key,value);
    }

    public void setProperties(Chars chars) throws InvalidArgumentException {
        final WStyle style = RSReader.readStyle(new Chars("{n : {filter:true\n").concat(chars).concat(new Chars("\n}}")));
        final StyleDocument sr = (StyleDocument) style.getRules().get(0);
        WidgetStyles.mergeDoc(this, sr, false);
    }

}
