
package science.unlicense.engine.ui.io;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Characters;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedHashDictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.lexer.Lexer;
import science.unlicense.api.lexer.Token;
import science.unlicense.api.lexer.TokenType;
import science.unlicense.api.number.Float64;
import science.unlicense.api.parser.Parser;
import science.unlicense.api.parser.Rule;
import science.unlicense.api.parser.SyntaxNode;
import science.unlicense.api.predicate.Constant;
import science.unlicense.api.predicate.Expression;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.ui.style.ExpressionReference;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.impl.grammar.io.UNGrammarReader;
import science.unlicense.api.model.doc.Field;
import science.unlicense.api.lexer.TokenGroup;
import science.unlicense.api.model.doc.Document;
import science.unlicense.api.number.Int32;
import science.unlicense.api.predicate.And;
import science.unlicense.api.predicate.ClassMethod;
import science.unlicense.api.predicate.Not;
import science.unlicense.api.predicate.Or;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.impl.binding.hjson.HJSONReader;
import science.unlicense.impl.binding.json.JSONUtilities;
import science.unlicense.system.path.Paths;
import science.unlicense.system.util.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public class RSReader {
    
    private static final Chars RULE_ID          = new Chars("id");
    private static final Chars RULE_ARRAY       = new Chars("arr");
    private static final Chars RULE_LITERAL     = new Chars("lit");
    private static final Chars RULE_FUNCTION    = new Chars("fct");
    private static final Chars RULE_EXPRESSION  = new Chars("exp");
    private static final Chars RULE_BASIC       = new Chars("basic");
    private static final Chars RULE_INDEX       = new Chars("idx");
    private static final Chars RULE_DOC         = new Chars("doc");
    private static final Chars RULE_LOGIC       = new Chars("logic");
    
    private static final Chars TOKEN_EQUAL      = new Chars("EQUAL");
    private static final Chars TOKEN_IN         = new Chars("IN");
    private static final Chars TOKEN_MOD        = new Chars("MOD");
    private static final Chars TOKEN_TRUE       = new Chars("TRUE");
    private static final Chars TOKEN_FALSE      = new Chars("FALSE");
    private static final Chars TOKEN_NONE       = new Chars("NONE");
    private static final Chars TOKEN_NUMBER     = new Chars("NUMBER");    
    private static final Chars TOKEN_WORD       = new Chars("WORD");
    private static final Chars TOKEN_PROPERTY   = new Chars("PROPERTY");
    private static final Chars TOKEN_STRING     = new Chars("STRING");
    private static final Chars TOKEN_REF        = new Chars("REFERENCE");
    private static final Chars TOKEN_AND        = new Chars("AND");
    private static final Chars TOKEN_OR         = new Chars("OR");
    
    private static final Predicate TRIM;
    private static final Lexer LEXER;
    private static final Parser PARSER_EXP;

    //OPTIMISATION : very commun case
    private static final PropertyName CLASS_PROPERTYNAME = new PropertyName(EqualsPredicate.CLASS);
    
    static {
        
        try{
            final UNGrammarReader reader = new UNGrammarReader();
            reader.setInput(Paths.resolve(new Chars("mod:/un/engine/ui/io/rs.gr")));
            final HashDictionary groups = new HashDictionary();
            final OrderedHashDictionary rules = new OrderedHashDictionary();
            reader.read(groups, rules);
            
            final TokenGroup tokens = (TokenGroup) groups.getValues().createIterator().next();

            // grammar parser is already tested        
            final Rule ruleExp = (Rule) rules.getValue(new Chars("exp"));

            //prepare lexer
            LEXER = new Lexer();
            LEXER.setTokenGroup(tokens);

            //prepare parser
            PARSER_EXP = new Parser(ruleExp);

            final TokenType[] toExclude = new TokenType[]{
                (TokenType) tokens.getValue(new Chars("WS")),
                (TokenType) tokens.getValue(new Chars("COM")),
                (TokenType) tokens.getValue(new Chars("BL")),
                (TokenType) tokens.getValue(new Chars("BR")),
                (TokenType) tokens.getValue(new Chars("PL")),
                (TokenType) tokens.getValue(new Chars("PR"))
            };

            TRIM = new Predicate() {
                public Boolean evaluate(Object candidate) {
                    final SyntaxNode sn = (SyntaxNode) candidate;
                    final Token token = sn.getToken();
                    return token!=null && Arrays.containsIdentity(toExclude, 0, toExclude.length, token.type);
                }
            };
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
        
    public static synchronized WStyle readStyle(Object input) throws InvalidArgumentException{

        try {
            if (input instanceof CharArray) input = ((CharArray)input).toBytes();
            final HJSONReader reader = new HJSONReader();
            reader.setInput(input);
            final Document doc = JSONUtilities.readAsDocument(reader);

            final WStyle style = new WStyle(null);

            final Iterator ite = doc.getFieldNames().createIterator();
            while (ite.hasNext()) {
                final Chars ruleName = (Chars)ite.next();
                Document rule = (Document) doc.getFieldValue(ruleName);
                style.getRules().add(toStyleDoc(ruleName, rule));
            }
            return style;
        } catch (IOException ex) {
            throw new InvalidArgumentException(ex.getMessage(), ex);
        }
    }

    private static StyleDocument toStyleDoc(Chars name, Document doc) throws IOException{
        final StyleDocument sd = new StyleDocument();
        if(name!=null) sd.setName(name);

        final Iterator ite = doc.getFieldNames().createIterator();
        while (ite.hasNext()) {
            Chars fieldName = (Chars)ite.next();
            Object value = doc.getFieldValue(fieldName);
            final int sep = fieldName.getFirstOccurence('.');
            int idx = -1;
            if (sep>0) {
               idx = Int32.decode(fieldName, sep+1, -1);
               fieldName = fieldName.truncate(0, sep);
            }
            value = jsonValueToStyleValue(null, value);

            if (idx>=0) {
                final Field prop = sd.getField(fieldName);
                Object[] values = WidgetStyles.getFieldValues(prop);
                if(values.length<=idx){
                    values = Arrays.resize(values, idx+1);
                }
                values[idx] = value;
                WidgetStyles.setFieldValues(prop, values);
            } else {
                sd.setFieldValue(fieldName, value);
            }
        }

        return sd;
    }

     private static Object jsonValueToStyleValue(Chars fieldName, Object value) throws IOException{

        if (value instanceof Chars) {
            value = readExpression((Chars)value,true);
        } else if(value instanceof Document) {
            value = toStyleDoc(fieldName, (Document)value);
        } else if(value instanceof Sequence) {
            final Sequence s = (Sequence) value;
            for (int i=0,n=s.getSize();i<n;i++) {
                Object e = jsonValueToStyleValue(fieldName, s.get(i));
                s.replace(i, e);
            }
        } else if(value.getClass().isArray()) {
            final int size = Arrays.getSize(value);
            final Sequence table = new ArraySequence(size);
            for (int i=0;i<size;i++) {
                table.add( jsonValueToStyleValue(fieldName, Arrays.getValue(value, i)) );
            }
            value = new Constant(table);
        } else if(value != null) {
            //already type mapped by json
            value = new Constant(value);
        } else {
            throw new IOException("Null value ");
        }

        return value;
    }

     /**
      *
      * @param text
      * @param optimize will try to replace static expressions by literals
      * @return
      * @throws IOException
      */
    public static synchronized Expression readExpression(Chars text, boolean optimize) throws IOException{
        final ArrayInputStream is = new ArrayInputStream(text.toBytes());
        
        //parse
        LEXER.setInput(is);
        PARSER_EXP.setInput(LEXER);
        final SyntaxNode node = PARSER_EXP.parse();
        node.trim(TRIM);
        
        Expression exp =  readExpression(node);
        if (optimize) {
            exp = optimize(exp);
        }
        return exp;
    }
    
    private static Expression readExpression(SyntaxNode node) throws IOException{

        final SyntaxNode logicNode = node.getChildByRule(RULE_LOGIC);
        if(logicNode != null){
            return readLogicFilter(logicNode);
        }else{
            return Predicate.TRUE;
        }
    }

    private static Expression readLogicFilter(SyntaxNode sn) throws IOException{
        final Sequence exps = sn.getChildren();
        if(exps.getSize()==1){
            return readBasic((SyntaxNode) exps.get(0));
        }else if(exps.getSize()!=3){
            return readBasic((SyntaxNode) exps.get(0));
        }
        final Expression exp1 = readBasic((SyntaxNode) exps.get(0));
        final Expression exp2 = readExpression((SyntaxNode) exps.get(2));

        if(sn.getChildByToken(TOKEN_AND)!=null){
            return new And(new Predicate[]{(Predicate)exp1, (Predicate)exp2});
        }else if(sn.getChildByToken(TOKEN_OR)!=null){
            return new Or(new Predicate[]{(Predicate)exp1, (Predicate)exp2});
        }else if(sn.getChildByToken(TOKEN_IN)!=null){
            return new InPredicate(exp1, exp2);
        }else if(sn.getChildByToken(TOKEN_MOD)!=null){
            return new ModExpression(exp1, exp2);
        }else if(sn.getChildByToken(TOKEN_EQUAL)!=null){
            return new EqualsPredicate(exp1, exp2);
        }else{
            throw new IOException("Unexpected");
        }
    }

    private static Expression readBasic(SyntaxNode sn) throws IOException{
        final SyntaxNode filterNode = sn.getChildByRule(RULE_EXPRESSION);
        if(filterNode != null){
            return readExpression(filterNode);
        }
            
        final SyntaxNode idNode = sn.getChildByRule(RULE_ID);
        if(idNode!=null){
            return readId(idNode);
        }
        
        final SyntaxNode refNode = sn.getChildByToken(TOKEN_REF);
        if(refNode!=null){
            //expression reference
            final Chars text = refNode.getToken().value;
            final Chars value = text.truncate(1, text.getCharLength());
            return new ExpressionReference(value);
        }
        
        final SyntaxNode litNode = sn.getChildByRule(RULE_LITERAL);
        if(litNode!=null){
            return readLiteral(litNode);
        }
        
        final SyntaxNode fctNode = sn.getChildByRule(RULE_FUNCTION);
        if(fctNode!=null){
            return readFunction(fctNode);
        }
        
        throw new IOException("Invalid expression "+sn);
    }
    
    private static Expression readId(SyntaxNode sn) throws IOException{
        final SyntaxNode child = (SyntaxNode) sn.getChildren().get(0);
        Chars id = child.getToken().value;
        if(id.startsWith('$')){
            id = id.truncate(1, id.getCharLength());
        }
        id = replaceConstantNames(id);
        if(id.equals(EqualsPredicate.CLASS)){
            return CLASS_PROPERTYNAME;
        }else{
            return new PropertyName(id);
        }
    }
    
    private static Expression readLiteral(SyntaxNode sn) throws IOException{
                
        final SyntaxNode numNode = sn.getChildByToken(TOKEN_NUMBER);
        if(numNode!=null){
            try{
                return new Constant(Int32.decode(numNode.getToken().value));
            }catch(Exception ex){
                return new Constant(Float64.decode(numNode.getToken().value));
            }
        }
        
        final SyntaxNode strNode = sn.getChildByToken(TOKEN_STRING);
        if(strNode!=null){
            final Chars text = strNode.getToken().value;
            final Chars value = text.truncate(1, text.getCharLength()-1);
            return new Constant(Characters.resolveEscapes(value));
        }
        
        final SyntaxNode trueNode = sn.getChildByToken(TOKEN_TRUE);
        if(trueNode!=null){
            return new Constant(Boolean.TRUE);
        }
        
        final SyntaxNode falseNode = sn.getChildByToken(TOKEN_FALSE);
        if(falseNode!=null){
            return new Constant(Boolean.FALSE);
        }
        
        final SyntaxNode noneNode = sn.getChildByToken(TOKEN_NONE);
        if(noneNode!=null){
            return WidgetStyles.NULL_CST;
        }
        
        final SyntaxNode arrNode = sn.getChildByRule(RULE_ARRAY);
        if(arrNode!=null){
            final Sequence array = new ArraySequence();
            for(int i=0,n=arrNode.getChildren().getSize();i<n;i++){
                array.add(readExpression((SyntaxNode) arrNode.getChildren().get(i)));
            }
            return new Constant(array);
        }
        
        throw new IOException("Invalid literal "+sn);
    }
        
    private static Expression readFunction(SyntaxNode sn) throws IOException{
        final Chars name = sn.getChildByToken(TOKEN_WORD).getToken().value;
        final Sequence exps = sn.getChildrenByRule(RULE_EXPRESSION);
        final Sequence array = new ArraySequence();
        for(int i=0,n=exps.getSize();i<n;i++){
            array.add(readExpression((SyntaxNode) exps.get(i)));
        }
        final Expression[] arr = new Expression[exps.getSize()];
        Collections.copy(array, arr, 0);
        
        return RSFunctions.INSTANCE.resolve(name, arr);
    }

    /**
     * Lists available widget functions.
     * @return 
     */
    public static Class[] getFunctionClasses() throws ClassNotFoundException{
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/widget/functions"), 
                Predicates.instanceOf(Chars.class));
        final Class[] formats = new Class[results.getSize()];
        final Iterator ite = results.createIterator();
        int offset = 0;
        while(ite.hasNext()){
            formats[offset] = Class.forName(((Chars)ite.next()).toJVMString());
            offset++;
        }
        return formats;
    }

    private static Chars replaceConstantNames(Chars candidate){
        //TODO find a more efficient way to replace constants
        if(StyleDocument.PROP_FILTER.equals(candidate)){
            return StyleDocument.PROP_FILTER;
        }else if(StyleDocument.PROP_ISTRIGGER.equals(candidate)){
            return StyleDocument.PROP_ISTRIGGER;
        }else if(EqualsPredicate.CLASS.equals(candidate)){
            return EqualsPredicate.CLASS;
        }
        return candidate;
    }

    private static Expression optimize(Expression exp) {
        if (exp instanceof Constant || exp instanceof PropertyName || exp instanceof ExpressionReference) {
            //can no optimize
            return exp;
        } else if (exp instanceof And) {
            And fct = (And) exp;
            Predicate[] parameters = fct.getPrecidates();

            boolean changed = false;
            boolean allConstant = true;
            for (int i=0;i<parameters.length;i++) {
                Expression opt = optimize(parameters[i]);
                if(opt!=parameters[i]) {
                    changed = true;
                    parameters[i] = (Predicate) opt;
                }
                allConstant &= (opt instanceof Constant);
            }

            if (changed) {
                fct = new And(parameters);
            }
            if (allConstant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
        } else if (exp instanceof Or) {
            Or fct = (Or) exp;
            Predicate[] parameters = fct.getPrecidates();

            boolean changed = false;
            boolean allConstant = true;
            for (int i=0;i<parameters.length;i++) {
                Expression opt = optimize(parameters[i]);
                if(opt!=parameters[i]) {
                    changed = true;
                    parameters[i] = (Predicate) opt;
                }
                allConstant &= (opt instanceof Constant);
            }

            if (changed) {
                fct = new Or(parameters);
            }
            if (allConstant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
        } else if (exp instanceof Not) {
            Not fct = (Not) exp;
            Expression exp1 = optimize(fct.getPredicate());
            if (exp1!=fct.getPredicate()) {
                fct = new Not((Predicate) exp1);
            }
            if (exp1 instanceof Constant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
        } else if (exp instanceof ClassMethod) {
            ClassMethod fct = (ClassMethod) exp;
            Expression[] parameters = fct.getParameters();
            boolean changed = false;
            boolean allConstant = true;
            for (int i=0;i<parameters.length;i++) {
                Expression opt = optimize(parameters[i]);
                if(opt!=parameters[i]) {
                    changed = true;
                    parameters[i] = opt;
                }
                allConstant &= (opt instanceof Constant);
            }

            if (changed) {
                fct = new ClassMethod(fct.getMethod(), parameters);
            }
            if (allConstant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }
            
        } else if (exp instanceof EqualsPredicate) {
            EqualsPredicate fct = (EqualsPredicate) exp;
            Expression exp1 = optimize(fct.getExp1());
            Expression exp2 = optimize(fct.getExp2());
            if (exp1!=fct.getExp1() || exp2!=fct.getExp2()) {
                fct = new EqualsPredicate(exp1, exp2);
            }
            if (exp1 instanceof Constant && exp2 instanceof Constant) {
                //evaluate now
                return new Constant(fct.evaluate(null));
            } else {
                return fct;
            }

        } else {
            //unknown, can't optimize
            return exp;
        }
    }

}
