
package science.unlicense.engine.ui.model;

/**
 *
 * @author Johann Sorel
 */
public interface SliderModel {

    Object getDefaultValue();

    boolean hasNextValue(Object currentValue);

    boolean hasPreviousValue(Object currentValue);

    Object nextValue(Object currentValue);

    Object previousValue(Object currentValue);

    /**
     * Slider ramp goes from 0 to 1.
     * @param ratio between 0 and 1
     * @return Object
     */
    Object getValue(double ratio);

    /**
     *
     * @param value
     * @return
     */
    double getRatio(Object value);

}
