
package science.unlicense.engine.ui.component;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.AbstractControlWidget;
import science.unlicense.engine.ui.widget.WAction;

/**
 * TODO make a proper date/time chooser.
 * TODO need to finish date api.
 * 
 * @author Johann Sorel
 */
public class WDateField extends AbstractControlWidget {

    private final WAction choose = new WAction(null, null, new EventListener() {
        public void receiveEvent(Event event) {
        }
    });



    public WDateField(){
        setLayout(new BorderLayout());
        addChild(choose, BorderConstraint.RIGHT);
        choose.getFlags().add(new Chars("WDateChooser-Button"));

    }

}
