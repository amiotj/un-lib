
package science.unlicense.engine.ui.visual;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.engine.ui.style.FontStyle;
import science.unlicense.engine.ui.style.ShapeStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WProgressBar;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.geometry.s2d.Rectangle;

/**
 *
 * @author Johann Sorel
 */
public class ProgressBarView extends WidgetView{

    public ProgressBarView(WProgressBar widget) {
        super(widget);
    }

    public WProgressBar getWidget() {
        return (WProgressBar) super.getWidget();
    }

    public void getExtents(Extents buffer, Extent constraint) {

        final FontStyle font = WidgetStyles.readFontStyle(widget.getStyle(),Widget.STYLE_PROP_FONT);
        final Extent extent = new Extent.Double(20,20);

        //get text size
        final FontMetadata meta = FontContext.INSTANCE.getMetadata(font.getFont());

        final CharArray text = getTranslatedText(getWidget().getText());
        if(meta!=null && text != null){
            final BBox bbox = meta.getCharsBox(text);
            extent.set(0, bbox.getSpan(0));
            extent.set(1, bbox.getSpan(1));
        }

        buffer.setAll(extent);
    }
    
    public void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox) {
        final Extent extent = widget.getEffectiveExtent();
        final double width = extent.get(0);
        final double height = extent.get(1);

        final FontStyle fontStyle = WidgetStyles.readFontStyle(widget.getStyle(),Widget.STYLE_PROP_FONT);
        final ShapeStyle[] border = WidgetStyles.readShapeStyle(widget.getStyle(),new Chars("progress"));

        //render the border and fill
        final double progress = getWidget().getProgress();
        final Rectangle fillgeom;
        if(progress==1){
            fillgeom = new Rectangle(innerBBox);
        }else if(getWidget().isUndetermined()){
            fillgeom = new Rectangle(innerBBox.getMin(0),innerBBox.getMin(1),extent.get(0), extent.get(1)/2d);
            //TODO some kind of animation needed
        }else{
            fillgeom = new Rectangle(innerBBox.getMin(0),innerBBox.getMin(1),extent.get(0)*progress, extent.get(1));
        }
        renderShape(painter, border, fillgeom);
        
        //render the text
        final CharArray text = getTranslatedText(getWidget().getText());
        if(text!=null && !text.isEmpty() && fontStyle != null){
            final Font font = fontStyle.getFont();
            if(font!=null){
                painter.setFont(font);
                //calculate appropriate text position
                //TODO : for now horizontal centered, vertical centered
                final FontMetadata meta = FontContext.INSTANCE.getMetadata(font);
                final BBox bbox = meta.getCharsBox(text);
                final float xoffset = (float)(-bbox.getSpan(0))/2;
                final float yoffset = (float)((-bbox.getSpan(1))/2 + meta.getAscent());

                //TODO calculate accurate position based on font
                if(fontStyle.getFillPaint()!=null){
                    painter.setPaint(fontStyle.getFillPaint());
                    painter.fill(text, xoffset, yoffset);
                }
                if(fontStyle.getBrush()!=null && fontStyle.getBrushPaint()!=null){
                    painter.setBrush(fontStyle.getBrush());
                    painter.setPaint(fontStyle.getBrushPaint());
                    painter.stroke(text, xoffset, yoffset);
                }
            }
        }
    }
    
    @Override
    public void receiveWidgetEvent(PropertyMessage event) {
        super.receiveWidgetEvent(event);

        final Chars name = event.getPropertyName();
        if(WProgressBar.PROPERTY_TEXT.equals(name) ||
           WProgressBar.PROPERTY_LANGUAGE.equals(name)){
            widget.updateExtents();
            widget.setDirty();
        }else if(WProgressBar.PROPERTY_PROGRESS.equals(name) ||
                WProgressBar.PROPERTY_UNDETERMINED.equals(name)){
            widget.setDirty();
        }
        
    }
    
}
