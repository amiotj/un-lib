
package science.unlicense.engine.ui.component.path;

import science.unlicense.api.Sorter;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.desktop.FrameManager;
import science.unlicense.api.desktop.FrameManagers;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Property;
import science.unlicense.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.api.layout.SplitConstraint;
import science.unlicense.engine.ui.model.CheckGroup;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.widget.WSelect;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.menu.WMenuSwitch;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.LineLayout;
import science.unlicense.api.layout.SplitLayout;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_FILTER;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_SELECTED_PATHS;
import static science.unlicense.engine.ui.component.path.PathView.PROPERTY_VIEW_ROOT;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.system.path.Paths;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.engine.ui.widget.WSeparator;

/**
 * Path chooser component.
 *
 * @author Johann Sorel
 */
public class WPathChooser extends WContainer {

    private static final Chars FONT_STYLE = new Chars("{subs : {\nfilter:true\nmargin:none\nborder:none\nfont:{fill-paint:@color-main\nfamily:font('Unicon' 14 'none')\n}\n}\n}");
    private static final Chars FONT_STYLE_SMALL = new Chars("{subs : {\nfilter:true\nmargin:[5,0,5,0]\nborder:none\nfont:{\nfill-paint:@color-background\nfamily:font('Unicon' 10 'none')\n}\n}\n}");

    static final Chars STYLE_FILE = new Chars("border.0:{\nshape:glyph('Unicon' '\uE02A')\nfill-paint:@color-main\nfitting:'scaled'\n}");
    static final Chars STYLE_FOLDER = new Chars("border.0:{shape:glyph('Unicon' '\uE016')\nfill-paint:@color-focus\nfitting:'scaled'\n}");
    static final Chars STYLE_BACK = new Chars("border.0:{shape:glyph('Unicon' '\uE016')\nfill-paint:@color-focus\nfitting:'scaled'\n}"
                                            + "border.1:{shape:glyph('Unicon' '\uE00F')\nfill-paint:@color-background\nfitting:'scaled'\nmargin:[-40,-5,-5,-20]\n}");

    static final Sorter FILE_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            try{
                if(first instanceof Path && second instanceof Path){
                    final Path p1 = (Path) first;
                    final Path p2 = (Path) second;
                    if(p1.isContainer() && p2.isContainer()){
                        return p1.getName().toString().compareToIgnoreCase(p2.getName().toString());
                    }else if(p1.isContainer()){
                        return -1;
                    }else if(p2.isContainer()){
                        return +1;
                    }else{
                        return p1.getName().toString().compareToIgnoreCase(p2.getName().toString());
                    }
                }
            }catch(IOException ex){
                ex.printStackTrace();
                return 0;
            }
            return 0;
        }
    };

    //top bar
    private final WMenuButton goToHome = new WMenuButton(new Chars("\uE024"), null,new EventListener() {
        public void receiveEvent(Event event) {
            setViewRoot(Paths.resolve((Chars)science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home")).getValue()));
        }
    });
    private final WTextField pathText = new WTextField();
    private final WMenuButton showHidden = new WMenuButton(new Chars("\uE029"), null,new EventListener() {
        public void receiveEvent(Event event) {
            final boolean viewHidden = !leftView.isHiddenFileVisible();
            leftView.setHiddenFileVisible(viewHidden);
            centerView.setHiddenFileVisible(viewHidden);
        }
    });
    private final WMenuButton previous = new WMenuButton(new Chars("\uE02F"), null,new EventListener() {
        public void receiveEvent(Event event) {
        }
    });
    private final WMenuButton next = new WMenuButton(new Chars("\uE02E"), null,new EventListener() {
        public void receiveEvent(Event event) {
        }
    });
    private final WMenuButton retract = new WMenuButton(new Chars("\uE004"), null,new EventListener() {
        double previous = 100;
        public void receiveEvent(Event event) {
            final SplitLayout l = (SplitLayout) centerSplit.getLayout();
            if(l.getSplitPosition()==0){
                l.setSplitPosition(previous);
            }else{
                previous = l.getSplitPosition();
                l.setSplitPosition(0);
            }
        }
    });
    private final WContainer leftViewsButtons = new WContainer(new GridLayout(1, -1));
    private final WContainer centerViewsButtons = new WContainer(new GridLayout(1, -1));
    private final WContainer shortcutButtons = new WContainer(new GridLayout(-1, 1));
    private final WContainer leftSplit = new WContainer(new BorderLayout());

    //bottom bar
    private final WButton ok = new WButton(new Chars("Ok"), null,new EventListener() {
        public void receiveEvent(Event event) {
            new Thread(){
                public void run() {
                    if(dialog!=null){
                        dialog.setVisible(false);
                        dialog.dispose();
                        dialog = null;
                    }
                }
            }.start();
        }
    });
    private final WButton cancel = new WButton(new Chars("Cancel"), null,new EventListener() {
        public void receiveEvent(Event event) {
            setSelectedPath(new Path[0]);
            new Thread(){
                public void run() {
                    if(dialog!=null){
                        dialog.setVisible(false);
                        dialog.dispose();
                        dialog = null;
                    }
                }
            }.start();
        }
    });
    
    //selection informations
    private UIFrame dialog = null;
    
    //predicate informations
    private final Sequence predicates = new ArraySequence();
    private final RowModel predModel = new DefaultRowModel(predicates);
    private final WSelect predSelect = new WSelect(predModel);

    //views
    private final WSplitContainer centerSplit = new WSplitContainer();
    private final WContainer bottomPane = new WContainer(new BorderLayout());
    private PathView[] leftViews = new PathView[0];
    private WSwitch[] leftSwitchs = new WSwitch[0];
    private PathView[] centerViews = new PathView[0];
    private WSwitch[] centerSwitchs = new WSwitch[0];
    //currently active views
    private PathView leftView = null;
    private PathView centerView = null;

    public WPathChooser() {
        super(new BorderLayout());
        predicates.add(Predicate.TRUE);
        
        goToHome.getStyle().addRules(FONT_STYLE);
        showHidden.getStyle().addRules(FONT_STYLE);
        previous.getStyle().addRules(FONT_STYLE);
        next.getStyle().addRules(FONT_STYLE);
        retract.getStyle().addRules(FONT_STYLE_SMALL);
        predSelect.setVisible(false);
        
        //change the split to have a retract button
        final WContainer space = new WContainer(new LineLayout());
        space.getFlags().add(new Chars("SplitSeparator"));
        space.addChild(retract, null);
        space.getStyle().getSelfRule().setProperties(new Chars("background:{\nfill-paint:@color-delimiter\n}"));
        ((SplitLayout) centerSplit.getLayout()).setSplitGap(space.getExtents().bestX);
        centerSplit.getChildren().removeAll();
        centerSplit.addChild(space, SplitConstraint.SPLITTER);

        //build shortcuts
        if(science.unlicense.system.System.get().getType().equals(science.unlicense.system.System.TYPE_UNIX)){
            final String userName = science.unlicense.system.System.get().getProperties().getSystemTree().search(new Chars("system/user/name")).getValue().toString();
            final Path p = Paths.resolve(new Chars("file:/media/"+userName));
            final Path[] mounts = (Path[]) p.getChildren().toArray(Path.class);
            for(int i=0;i<mounts.length;i++){
                final Path mp = mounts[i];
                final WSpace leaf = new WSpace(14, 14);
                leaf.getStyle().getSelfRule().setProperties(WPathChooser.STYLE_FOLDER);
                leaf.getStyle().getSelfRule().setProperties(new Chars("margin:0"));
                leaf.setOverrideExtents(new Extents(14, 14));
                final WMenuButton go = new WMenuButton(new Chars(mp.getName()),leaf,new EventListener() {
                    public void receiveEvent(Event event) {
                        setViewRoot(mp);
                    }
                });
                shortcutButtons.getChildren().add(go);
            }
            shortcutButtons.addChild(new WSeparator(true), null);
        }
        shortcutButtons.getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint   : lineargradientfill('%' 0 0 0 1 0 rotateHSL(@color-background 0 1 0.95) 1 rotateHSL(@color-background 0 1 1.05))\n}"));
        leftSplit.addChild(shortcutButtons, BorderConstraint.TOP);

        //top panel
        final WButtonBar topPane = new WButtonBar();
        ((FormLayout)topPane.getLayout()).setColumnSize(4, FormLayout.SIZE_EXPAND);
        topPane.addChild(leftViewsButtons,  FillConstraint.builder().build());
        topPane.addChild(previous,          FillConstraint.builder().coord(1, 0).fill(true, true).build());
        topPane.addChild(next,              FillConstraint.builder().coord(2, 0).fill(true, true).build());
        topPane.addChild(goToHome,          FillConstraint.builder().coord(3, 0).fill(true, true).build());
        topPane.addChild(pathText,          FillConstraint.builder().coord(4, 0).fill(true, true).build());
        topPane.addChild(centerViewsButtons,FillConstraint.builder().coord(5, 0).fill(true, true).build());
                
        //bottom panel
        //predicates list
        final WContainer predPane = new WContainer(new BorderLayout());
        predPane.addChild(predSelect,BorderConstraint.CENTER);        
        //ok/cancel pane
        final WContainer rightPane = new WContainer(new GridLayout(1, 2, 10, 10));
        rightPane.getChildren().add(cancel);
        rightPane.getChildren().add(ok);
        final WContainer okcancelPane = new WContainer(new BorderLayout());
        okcancelPane.getStyle().getSelfRule().setProperties(new Chars("margin:[8,8,8,8]"));
        okcancelPane.addChild(rightPane, BorderConstraint.RIGHT);
        bottomPane.addChild(predPane, BorderConstraint.CENTER);
        bottomPane.addChild(okcancelPane, BorderConstraint.RIGHT);

        //add all blocks
        addChild(topPane,BorderConstraint.TOP);
        addChild(centerSplit,BorderConstraint.CENTER);
        
        setLeftViews(new PathView[]{new WTreePathView()});
        setCenterViews(new PathView[]{
            new WListView(),
            new WTableView(),
            new WPreviewView(),
            new WInteractiveView()});

        setViewRoot(Paths.resolve((Chars)science.unlicense.system.System.get().getProperties()
                    .getSystemTree().search(new Chars("system/user/home")).getValue()));
    }

    /**
     * Set the possible left side explorer views.
     * If there is more then one, buttons will be visible to switch between them.
     * @param views can be null
     */
    public void setLeftViews(PathView[] views){
        leftViewsButtons.getChildren().removeAll();
        setSelectedLeftView(null);
        
        if(views==null) views = new PathView[0];
        this.leftViews = Arrays.copy(views, new PathView[views.length]);
                
        if(leftViews.length>0){
            //rebuild chooser buttons
            final CheckGroup group = new CheckGroup();
            leftSwitchs = new WSwitch[views.length];
            for(int i=0;i<views.length;i++){
                final PathView pv = views[i];
                pv.varFilter().sync(varFilter());
                leftSwitchs[i] = new WMenuSwitch(null, pv.getIcon(), new EventListener() {
                    public void receiveEvent(Event event) {
                        if(((WSwitch)event.getSource()).isCheck()){
                            setSelectedLeftView(pv);
                        }
                    }
                });
                group.add(leftSwitchs[i]);
                if(views.length>1) leftViewsButtons.getChildren().add(leftSwitchs[i]);
            }
            
            setSelectedLeftView(leftViews[0]);
        }
    }
    
    /**
     * Get currently used left side views.
     * @return PathView array, never null but can be empty
     */
    public PathView[] getLeftViews(){
        return Arrays.copy(leftViews, new PathView[leftViews.length]);
    }
    
    /**
     * Set active left side view.
     * @param view , use null to desactivate left view
     */
    public void setSelectedLeftView(PathView view){
        if(leftView==view) return;
        if(view!=null && !Arrays.contains(leftViews, view)){
            throw new InvalidArgumentException("View is not in the list");
        }
        //remove previous view
        if(leftView!=null){
            leftView.varViewRoot().unsync();
            leftView.varSelectedPath().unsync();
            leftSplit.getChildren().remove(leftView.getViewWidget());
        }
        this.leftView = view;
        if(view!=null){
            leftSwitchs[Arrays.getFirstOccurence(leftViews, 0, leftViews.length, view)].setCheck(true);
            //add new view
            view.varViewRoot().sync(varViewRoot());
            view.varSelectedPath().sync(varSelectedPath());
            leftSplit.addChild(view.getViewWidget(), BorderConstraint.CENTER);
            if(!centerSplit.getChildren().contains(leftSplit)){
                centerSplit.addChild(leftSplit, SplitConstraint.LEFT);
            }
        }else{
            centerSplit.getChildren().remove(leftSplit);
        }
    }
    
    /**
     * Get active left side view.
     * @return PathView, can be null
     */
    public PathView getSelectedLeftView(){
        return leftView;
    }
    
    /**
     * Set the possible center side explorer views.
     * If there is more then one, buttons will be visible to switch between them.
     * @param views can be null
     */
    public void setCenterViews(PathView[] views){
        centerViewsButtons.getChildren().removeAll();
        setSelectedCenterView(null);
        
        if(views==null) views = new PathView[0];
        this.centerViews = Arrays.copy(views, new PathView[views.length]);
                
        if(centerViews.length>0){
            //rebuild chooser buttons
            final CheckGroup group = new CheckGroup();
            centerSwitchs = new WSwitch[views.length];
            for(int i=0;i<views.length;i++){
                final PathView pv = views[i];
                pv.varFilter().sync(varFilter());
                centerSwitchs[i] = new WMenuSwitch(null, pv.getIcon(), new EventListener() {
                    public void receiveEvent(Event event) {
                        if(((WSwitch)event.getSource()).isCheck()){
                            setSelectedCenterView(pv);
                        }
                    }
                });
                group.add(centerSwitchs[i]);
                if(views.length>1) centerViewsButtons.getChildren().add(centerSwitchs[i]);
            }
            
            setSelectedCenterView(centerViews[0]);
        }
    }
    
    /**
     * Get currently used center side views.
     * @return PathView array, never null but can be empty
     */
    public PathView[] getCenterViews(){
        return Arrays.copy(centerViews, new PathView[centerViews.length]);
    }
    
    /**
     * Set active center side view.
     * @param view , use null to desactivate left view
     */
    public void setSelectedCenterView(PathView view){
        if(centerView==view) return;
        if(view!=null && !Arrays.contains(centerViews, view)){
            throw new InvalidArgumentException("View is not in the list");
        }
        //remove previous view
        if(centerView!=null){
            centerView.varViewRoot().unsync();
            centerView.varSelectedPath().unsync();
            centerSplit.getChildren().remove(centerView.getViewWidget());
        }
        this.centerView = view;
        if(view!=null){
            centerSwitchs[Arrays.getFirstOccurence(centerViews, 0, centerViews.length, view)].setCheck(true);
            //add new view
            if(centerView!=null){
                centerView.varViewRoot().sync(varViewRoot());
                centerView.varSelectedPath().sync(varSelectedPath());
                centerSplit.addChild(centerView.getViewWidget(), SplitConstraint.RIGHT);
            }
        }
    }
    
    /**
     * Get active center side view.
     * @return PathView, can be null
     */
    public PathView getSelectedCenterView(){
        return centerView;
    }

    /**
     * {@inheritDoc }
     */
    public void setViewRoot(Path viewRoot) {
        if(setPropertyValue(PROPERTY_VIEW_ROOT, viewRoot)){
            if(viewRoot==null){
                pathText.setText(Chars.EMPTY);
            }else{
                pathText.setText(new Chars(viewRoot.toURI()));
            }
        }
    }

    /**
     * {@inheritDoc }
     */
    public Path getViewRoot() {
        return (Path) getPropertyValue(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public Property varViewRoot(){
        return getProperty(PROPERTY_VIEW_ROOT);
    }

    /**
     * {@inheritDoc }
     */
    public void setSelectedPath(Path[] path) {
        if(setPropertyValue(PROPERTY_SELECTED_PATHS, path, AbstractPathView.DEFAULT_SELECTED_PATHS)){
            if(path!=null && path.length>0){
                pathText.setText(new Chars(path[0].toURI()));
            }
        }
    }

    /**
     * {@inheritDoc }
     */
    public Path[] getSelectedPath() {
        return (Path[]) getPropertyValue(PROPERTY_SELECTED_PATHS,AbstractPathView.DEFAULT_SELECTED_PATHS);
    }

    /**
     * {@inheritDoc }
     */
    public Property varSelectedPath(){
        return getProperty(PROPERTY_SELECTED_PATHS);
    }

    public void setFilter(Predicate filter) {
        predSelect.setSelection(filter);
        setPropertyValue(PROPERTY_FILTER, filter);
    }

    public Predicate getFilter() {
        return (Predicate) getPropertyValue(PROPERTY_FILTER);
    }

    public Property varFilter(){
        return getProperty(PROPERTY_FILTER);
    }

    public void setPredicates(Sequence predicates){
        final Predicate selected = getFilter();
        this.predicates.replaceAll(predicates);
        setFilter(selected);
        if(predicates.isEmpty() || (predicates.getSize()==1 && predicates.get(0) == Predicate.TRUE)){
            predSelect.setVisible(false);
        }else{
            predSelect.setVisible(true);
        }
    }
    
    public Path showOpenDialog(UIFrame parent){
        if(parent!=null){
            return showOpenDialog(parent.getManager(), parent);
        }else{
            return showOpenDialog(FrameManagers.getFrameManager());
        }
    }

    public Path showOpenDialog(FrameManager framemanager){
        return showOpenDialog(framemanager!=null?framemanager:FrameManagers.getFrameManager(), (UIFrame)null);
    }

    private Path showOpenDialog(FrameManager frameManager, UIFrame parent){
        addChild(bottomPane,BorderConstraint.BOTTOM);

        final UIFrame dialog;
        if(parent!=null){
            dialog = (UIFrame) parent.getManager().createFrame(parent, true, false);
        }else if(frameManager!=null){
            dialog = (UIFrame) frameManager.createFrame(parent, true, false);
        }else{
            dialog = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        }
        final WContainer contentPane = dialog.getContainer();
        contentPane.setLayout(new BorderLayout());
        contentPane.addChild(this, BorderConstraint.CENTER);
        this.dialog = dialog;

        dialog.setSize(640, 480);
        dialog.setTitle(new Chars("Path chooser"));
        dialog.setVisible(true);
        dialog.waitForDisposal();

        return Paths.resolve(pathText.getText().toChars());
    }

    public Path showSaveDialog(UIFrame parent){
        if(parent!=null){
            return showSaveDialog(parent.getManager(), parent);
        }else{
            return showSaveDialog(FrameManagers.getFrameManager());
        }
    }

    public Path showSaveDialog(FrameManager framemanager){
        return showOpenDialog(framemanager!=null?framemanager:FrameManagers.getFrameManager(), (UIFrame)null);
    }

    private Path showSaveDialog(FrameManager frameManager, UIFrame parent){
        addChild(bottomPane,BorderConstraint.BOTTOM);
        
        final UIFrame dialog;
        if(parent!=null){
            dialog = (UIFrame) parent.getManager().createFrame(parent, true, false);
        }else if(frameManager!=null){
            dialog = (UIFrame) frameManager.createFrame(parent, true, false);
        }else{
            dialog = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        }
        final WContainer contentPane = dialog.getContainer();
        contentPane.setLayout(new BorderLayout());
        contentPane.addChild(this, BorderConstraint.CENTER);
        this.dialog = dialog;

        dialog.setSize(640, 480);
        dialog.setTitle(new Chars("Path chooser"));
        dialog.setVisible(true);
        dialog.waitForDisposal();

        return Paths.resolve(pathText.getText().toChars());
    }

    public static Path showOpenDialog(Predicate predicate){
        return showOpenDialog(FrameManagers.getFrameManager(),predicate);
    }

    public static Path showOpenDialog(FrameManager frameManager, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if(predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showOpenDialog(frameManager);
    }

    public static Path showOpenDialog(UIFrame parent, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if(predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showOpenDialog(parent);
    }

    public static Path showSaveDialog(Predicate predicate){
        return showSaveDialog(FrameManagers.getFrameManager(),predicate);
    }

    public static Path showSaveDialog(FrameManager frameManager, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if(predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showSaveDialog(frameManager);
    }

    public static Path showSaveDialog(UIFrame parent, Predicate predicate){
        final WPathChooser chooser = new WPathChooser();
        if(predicate!=null){
            chooser.setFilter(predicate);
        }
        return chooser.showSaveDialog(parent);
    }


}
