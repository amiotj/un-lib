
package science.unlicense.engine.ui.widget;

import science.unlicense.api.predicate.Variable;

/**
 * Abstract value widget.
 * 
 * @author Johann Sorel
 */
public abstract class AbstractValueWidget extends AbstractWidget implements WValueWidget{

    /**
     * 
     * @param allowChildren true to allow children nodes
     */
    public AbstractValueWidget(boolean allowChildren) {
        super(allowChildren);
        setPropertyValue(PROPERTY_VALUE, null);
    }
    
    /**
     * Get widget value.
     * 
     * @return value, can be null.
     */
    public Object getValue(){
        return getPropertyValue(PROPERTY_VALUE);
    }

    /**
     * Set widget value.
     * 
     * @param value, can be null.
     */
    public void setValue(Object value){
        setPropertyValue(PROPERTY_VALUE,value);
    }
    
    public Variable varValue(){
        return getProperty(PROPERTY_VALUE);
    }
    
}
