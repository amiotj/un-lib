
package science.unlicense.engine.ui.widget.frame;

import science.unlicense.api.layout.AbsoluteLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.desktop.UIFrame;

/**
 * Container displaying frames.
 *
 * @author Johann Sorel
 */
public class WDesktop extends WContainer{

    public WDesktop() {
        super(new AbsoluteLayout());
    }

    public UIFrame addFrame(){
        final WDesktopFrame frame = new WDesktopFrame(this);
        children.add(frame);
        frame.setOnScreenLocation(new Vector(50, 50));
        frame.setSize(100,100);
        return frame;
    }

}
