
package science.unlicense.impl.painter2d.jsweet;

import jsweet.dom.CanvasGradient;
import jsweet.dom.CanvasPattern;
import jsweet.dom.CanvasRenderingContext2D;
import jsweet.dom.HTMLCanvasElement;
import jsweet.dom.ImageData;
import static jsweet.util.Globals.union;
import jsweet.util.StringTypes;
import jsweet.util.union.Union3;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.painter2d.AbstractPainter2D;
import science.unlicense.api.painter2d.Brush;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.api.painter2d.LinearGradientPaint;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.api.painter2d.RadialGradientPaint;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public class JSPainter2D extends AbstractPainter2D {

    private final HTMLCanvasElement canvas;
    private final CanvasRenderingContext2D g;

    public JSPainter2D(HTMLCanvasElement canvas) {
        this.canvas = canvas;
        g = canvas.getContext(StringTypes._2d);
    }

    @Override
    public void setClip(Geometry2D geom) {
        super.setClip(geom);
        if(geom!=null) {
            g.setTransform(1, 0, 0, 1, 0, 0);
            pushGeometry(new TransformedGeometry2D(geom, getFinalTransform()));
            g.clip();
        } else {
            Rectangle rect = new Rectangle(0, 0, canvas.width, canvas.height);
            pushGeometry(rect);
            g.clip();
        }
    }

    @Override
    public void fill(CharArray text, float x, float y) {
        final Union3<String, CanvasGradient, CanvasPattern> fill = toPaint(paint);
        pushFont(font);
        pushBlending(alphaBlending);
        g.fillStyle = fill;
        pushTransform(getFinalTransform());
        g.fillText(text.toString(), x, y);
    }

    @Override
    public void fill(Geometry2D geom) {
        final Union3<String, CanvasGradient, CanvasPattern> fill = toPaint(paint);
        final Affine2 trs = getFinalTransform();

        if(geom!=null && fill!=null){
            pushTransform(trs);
            pushBlending(alphaBlending);
            g.fillStyle = fill;
            pushGeometry(geom);
            g.fill();
        }

    }

    @Override
    public void stroke(Geometry2D geom) {
        final Union3<String, CanvasGradient, CanvasPattern> fill = toPaint(paint);
        final Affine2 trs = getFinalTransform();

        if(geom!=null && fill!=null){
            pushTransform(trs);
            pushBlending(alphaBlending);
            pushGeometry(geom);
            g.strokeStyle = fill;
            pushStroke(brush);
            g.stroke();
        }
    }

    @Override
    public void paint(Image image, Affine2 trs) {
        final HTMLCanvasElement img = toImage(image);
        if(trs==null) trs = new Affine2();

        if(img!=null){
            pushTransform(getFinalTransform());
            pushBlending(alphaBlending);
            g.drawImage(img, 0, 0);
        }
    }

    @Override
    public FontStore getFontStore() {
        return null; //TODO
    }
    
    @Override
    public void dispose() {
    }

    private Union3<String, CanvasGradient, CanvasPattern> toPaint(science.unlicense.api.painter2d.Paint paint){
        if (paint==null) return null;

        Union3<String, CanvasGradient, CanvasPattern> p;
        if (paint instanceof ColorPaint) {
            String str = toCSSColor(((ColorPaint)paint).getColor());
            p = union(str);
        } else if (paint instanceof LinearGradientPaint) {

            final LinearGradientPaint lgp = (LinearGradientPaint)paint;
            final CanvasGradient cg = g.createLinearGradient(lgp.getStartX(), lgp.getStartY(), lgp.getEndX(),lgp.getEndY());
            final Color[] colors = lgp.getColors();
            final double[] positions = lgp.getPositions();
            for (int i=0;i<colors.length;i++) {
                cg.addColorStop(positions[i], toCSSColor(colors[i]));
            }
            p = union(cg);
        } else if (paint instanceof RadialGradientPaint) {
            final RadialGradientPaint lgp = (RadialGradientPaint)paint;
            final CanvasGradient cg = g.createRadialGradient(lgp.getCenterX(), lgp.getCenterY(), lgp.getRadius(), lgp.getFocusX(), lgp.getFocusY(), lgp.getRadius());
            final Color[] colors = lgp.getColors();
            final double[] positions = lgp.getPositions();
            for (int i=0;i<colors.length;i++) {
                cg.addColorStop(positions[i], toCSSColor(colors[i]));
            }
            p = union(cg);
        } else {
            throw new InvalidArgumentException("Unexpected type : "+paint.getClass());
        }
        return p;
    }

    private HTMLCanvasElement toImage(Image image){
        if (image==null) return null;

        //format can not be supported directly in opengl.
        //convert it to a RGBA buffer.
        final int width = (int) image.getExtent().getL(0);
        final int height = (int) image.getExtent().getL(1);
        final PixelBuffer pb = image.getColorModel().asTupleBuffer(image);
        final int[] coord = new int[2];

        final HTMLCanvasElement ele = new HTMLCanvasElement();
        ele.width = width;
        ele.height = height;
        final CanvasRenderingContext2D ctx = ele.getContext(StringTypes._2d);
        final ImageData imgData = ctx.createImageData(width,height);

        for (int y = 0; y <height; y++) {
            coord[1] = y;
            for (int x = 0; x < width; x++) {
                coord[0] = x;
                Color color = pb.getColor(coord);
                int off = (y*width + x)*4;
                imgData.data.set(off+0, color.getAlpha()*255);
                imgData.data.set(off+1, color.getRed()*255);
                imgData.data.set(off+2, color.getGreen()*255);
                imgData.data.set(off+3, color.getBlue()*255);
            }
        }
        ctx.putImageData(imgData, 0, 0);

        return ele;
    }

    private String toCSSColor(science.unlicense.api.color.Color color) {
        final int argb = color.toARGB();
        return "rgb(" + (argb >> 16 & 0xFF) + "," + (argb >> 8 & 0xFF) + "," + (argb & 0xFF) + ")";
    }

    private void pushTransform(Affine2 affine) {
        g.setTransform(
                affine.getM00(), affine.getM10(),
                affine.getM01(), affine.getM11(),
                affine.getM02(), affine.getM12());
    }

    private void pushGeometry(Geometry2D geom){
        g.beginPath();
        if (geom==null) return;

        if (geom instanceof Point) {
            final Point p = (Point) geom;
            g.moveTo(p.getX(), p.getY());
        } else {
            final PathIterator ite = geom.createPathIterator();

            final TupleRW p = new DefaultTuple(4);
            final TupleRW c1 = new DefaultTuple(4);
            final TupleRW c2 = new DefaultTuple(4);
            while(ite.next()){
                final int type = ite.getType();
                if (type==PathIterator.TYPE_MOVE_TO) {
                    ite.getPosition(p);
                    g.moveTo(p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_LINE_TO) {
                    ite.getPosition(p);
                    g.lineTo(p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_ARC) {
                    ite.getPosition(p);
                    g.lineTo(p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_CUBIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    ite.getSecondControl(c2);
                    g.bezierCurveTo(c1.getX(), c1.getY(), c2.getX(), c2.getY(), p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_QUADRATIC) {
                    ite.getPosition(p);
                    ite.getFirstControl(c1);
                    g.quadraticCurveTo(c1.getX(), c1.getY(), p.getX(), p.getY());
                } else if(type==PathIterator.TYPE_CLOSE) {
                    g.closePath();
                }
            }
        }
    }

    private void pushStroke(Brush brush) {

        if (brush instanceof PlainBrush) {
            final PlainBrush b = (PlainBrush) this.brush;
            final int cap = b.getLineCap();
            final int join = b.getLineJoin();
            final float[] dashes = b.getDashes();
            final float offset = b.getOffset();
            final float width = b.getWidth();
            final float miterLimit = b.getMiterLimit();

            g.lineCap = (cap==PlainBrush.LINECAP_BUTT) ? "butt" : (cap==PlainBrush.LINECAP_ROUND) ? "round" : "square";
            g.lineJoin = (join==PlainBrush.LINEJOIN_BEVEL) ? "bevel" : (join==PlainBrush.LINEJOIN_ROUND) ? "round" : "miter";
            g.setLineDash(Arrays.reformatDouble(dashes));
            g.lineDashOffset = offset;
            g.lineWidth = width;
            g.miterLimit = miterLimit;

        } else {
            throw new InvalidArgumentException("Unexpected type : "+brush.getClass());
        }
    }

    private void pushFont(Font font) {
        final String family = font.getFamilies()[0].toString();
        final int weight = font.getWeight();
        final float size = font.getSize();
        g.font = ""+(int)size+"px "+family;
    }

    private void pushBlending(AlphaBlending blending) {
        g.globalAlpha = blending.getAlpha();
        final int type = blending.getType();
        switch (type) {

            case AlphaBlending.CLEAR :      throw new InvalidArgumentException("Unsupported type : "+type);
            case AlphaBlending.XOR :        g.globalCompositeOperation = "xor"; break;
            case AlphaBlending.DST :        throw new InvalidArgumentException("Unsupported type : "+type);
            case AlphaBlending.DST_ATOP :   g.globalCompositeOperation = "destination-atop"; break;
            case AlphaBlending.DST_IN :     g.globalCompositeOperation = "destination-in"; break;
            case AlphaBlending.DST_OUT :    g.globalCompositeOperation = "destination-out"; break;
            case AlphaBlending.DST_OVER :   g.globalCompositeOperation = "destination-over"; break;
            case AlphaBlending.SRC :        g.globalCompositeOperation = "copy"; break;
            case AlphaBlending.SRC_ATOP :   g.globalCompositeOperation = "source-atop"; break;
            case AlphaBlending.SRC_IN :     g.globalCompositeOperation = "source-in"; break;
            case AlphaBlending.SRC_OUT :    g.globalCompositeOperation = "source-out"; break;
            case AlphaBlending.SRC_OVER :   g.globalCompositeOperation = "source-over"; break;
            default : throw new InvalidArgumentException("Unexpected type : "+type);
        }

    }

}
