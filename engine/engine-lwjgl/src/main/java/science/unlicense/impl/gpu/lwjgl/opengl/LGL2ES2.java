
package science.unlicense.impl.gpu.lwjgl.opengl;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.opengles.GLES20;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.gpu.opengl.GL1;
import science.unlicense.api.gpu.opengl.GL1ES;
import science.unlicense.api.gpu.opengl.GL2;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.gpu.opengl.GL2ES3;
import science.unlicense.api.gpu.opengl.GL3;
import science.unlicense.api.gpu.opengl.GL4;
import science.unlicense.api.gpu.opengl.GLES220;

/**
 *
 * @author Johann Sorel
 */
public class LGL2ES2 implements GLES220,science.unlicense.api.gpu.opengl.GL2ES2{

    private LGL base;

    LGL2ES2(LGL gl) {
        this.base = gl;
    }

    @Override
    public boolean isGL1() {
        return base.isGL1();
    }

    @Override
    public boolean isGL2() {
        return base.isGL2();
    }

    @Override
    public boolean isGL3() {
        return base.isGL3();
    }

    @Override
    public boolean isGL4() {
        return base.isGL4();
    }

    @Override
    public boolean isGL1ES() {
        return base.isGL1ES();
    }

    @Override
    public boolean isGL2ES2() {
        return base.isGL2ES2();
    }

    @Override
    public boolean isGL2ES3() {
        return base.isGL2ES3();
    }
    
    public GL1 asGL1() {
        return base.asGL1();
    }

    public GL2 asGL2() {
        return base.asGL2();
    }

    public GL3 asGL3() {
        return base.asGL3();
    }

    public GL4 asGL4() {
        return base.asGL4();
    }

    public GL1ES asGL1ES() {
        return base.asGL1ES();
    }

    public GL2ES2 asGL2ES2() {
        return base.asGL2ES2();
    }

    public GL2ES3 asGL2ES3() {
        return base.asGL2ES3();
    }

    @Override
    public void glActiveTexture(int texture) {
        GLES20.glActiveTexture(texture);
    }

    @Override
    public void glAttachShader(int program, int shader) {
        GLES20.glAttachShader(program, shader);
    }

    @Override
    public void glBindAttribLocation(int program, int index, CharArray name) {
        GLES20.glBindAttribLocation(program, index, name.toString());
    }

    @Override
    public void glBindBuffer(int target, int buffer) {
        GLES20.glBindBuffer(target, buffer);
    }

    @Override
    public void glBindFramebuffer(int target, int framebuffer) {
        GLES20.glBindFramebuffer(target, framebuffer);
    }

    @Override
    public void glBindRenderbuffer(int target, int renderbuffer) {
        GLES20.glBindRenderbuffer(target, renderbuffer);
    }

    @Override
    public void glBindTexture(int target, int texture) {
        GLES20.glBindTexture(target, texture);
    }

    @Override
    public void glBlendColor(float red, float green, float blue, float alpha) {
        GLES20.glBlendColor(red, green, blue, alpha);
    }

    @Override
    public void glBlendEquation(int mode) {
        GLES20.glBlendEquation(mode);
    }

    @Override
    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        GLES20.glBlendEquationSeparate(modeRGB, modeAlpha);
    }

    @Override
    public void glBlendFunc(int sfactor, int dfactor) {
        GLES20.glBlendFunc(sfactor, dfactor);
    }

    @Override
    public void glBlendFuncSeparate(int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha) {
        GLES20.glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
    }

    @Override
    public void glBufferData(int target, Buffer data, int usage) {
        if (data instanceof ByteBuffer) GLES20.glBufferData(target, (ByteBuffer)data, usage);
        if (data instanceof ShortBuffer) GLES20.glBufferData(target, (ShortBuffer)data, usage);
        if (data instanceof IntBuffer) GLES20.glBufferData(target, (IntBuffer)data, usage);
        if (data instanceof FloatBuffer) GLES20.glBufferData(target, (FloatBuffer)data, usage);
        else throw new InvalidArgumentException("Unsupported buffer "+data);
        
    }

    @Override
    public void glBufferSubData(int target, long offset, Buffer data) {
        if (data instanceof ByteBuffer) GLES20.glBufferSubData(target, offset, (ByteBuffer)data);
        if (data instanceof ShortBuffer) GLES20.glBufferSubData(target, offset, (ShortBuffer)data);
        if (data instanceof IntBuffer) GLES20.glBufferSubData(target, offset, (IntBuffer)data);
        if (data instanceof FloatBuffer) GLES20.glBufferSubData(target, offset, (FloatBuffer)data);
        else throw new InvalidArgumentException("Unsupported buffer "+data);
    }

    @Override
    public int glCheckFramebufferStatus(int target) {
        return GLES20.glCheckFramebufferStatus(target);
    }

    @Override
    public void glClear(int mask) {
        GLES20.glClear(mask);
    }

    @Override
    public void glClearColor(float red, float green, float blue, float alpha) {
        GLES20.glClearColor(red, green, blue, alpha);
    }

    @Override
    public void glClearDepthf(float d) {
        GLES20.glClearDepthf(d);
    }

    @Override
    public void glClearStencil(int s) {
        GLES20.glClearStencil(s);
    }

    @Override
    public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
        GLES20.glColorMask(red, green, blue, alpha);
    }

    @Override
    public void glCompileShader(int shader) {
        GLES20.glCompileShader(shader);
    }

    @Override
    public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, Buffer data) {
        GLES20.glCompressedTexImage2D(target, level, internalformat, width, height, border, (ByteBuffer) data);
    }

    @Override
    public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, Buffer data) {
        GLES20.glCompressedTexSubImage2D(target,  level,  xoffset,  yoffset,  width,  height,  format, (ByteBuffer) data);
    }

    @Override
    public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
        GLES20.glCopyTexImage2D(target, level, internalformat, x, y, width, height, border);
    }

    @Override
    public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
        GLES20.glCopyTexSubImage2D( target,  level,  xoffset,  yoffset,  x,  y,  width,  height);
    }

    @Override
    public int glCreateProgram() {
        return GLES20.glCreateProgram();
    }

    @Override
    public int glCreateShader(int type) {
        return GLES20.glCreateShader( type);
    }

    @Override
    public void glCullFace(int mode) {
        GLES20.glCullFace( mode);
    }

    @Override
    public void glDeleteBuffers(IntBuffer buffers) {
        GLES20.glDeleteBuffers(buffers);
    }
    
    @Override
    public void glDeleteBuffers(int[] buffers) {
        GLES20.glDeleteBuffers(buffers);
    }

    @Override
    public void glDeleteFramebuffers(IntBuffer framebuffers) {
        GLES20.glDeleteFramebuffers(framebuffers);
    }

    @Override
    public void glDeleteFramebuffers(int[] framebuffers) {
        GLES20.glDeleteFramebuffers(framebuffers);
    }
    
    @Override
    public void glDeleteProgram(int program) {
        GLES20.glDeleteProgram( program);
    }

    @Override
    public void glDeleteRenderbuffers(IntBuffer renderbuffers) {
        GLES20.glDeleteRenderbuffers(renderbuffers);
    }

    @Override
    public void glDeleteShader(int shader) {
        GLES20.glDeleteShader( shader);
    }

    @Override
    public void glDeleteTextures(IntBuffer textures) {
        GLES20.glDeleteTextures(textures);
    }

    @Override
    public void glDepthFunc(int func) {
        GLES20.glDepthFunc( func);
    }

    @Override
    public void glDepthMask(boolean flag) {
        GLES20.glDepthMask( flag);
    }

    @Override
    public void glDepthRangef(float n, float f) {
        GLES20.glDepthRangef( n, f);
    }

    @Override
    public void glDetachShader(int program, int shader) {
        GLES20.glDetachShader( program, shader);
    }

    @Override
    public void glDisable(int cap) {
        GLES20.glDisable( cap);
    }

    @Override
    public void glDisableVertexAttribArray(int index) {
        GLES20.glDisableVertexAttribArray( index);
    }

    @Override
    public void glDrawArrays(int mode, int first, int count) {
        GLES20.glDrawArrays( mode, first, count);
    }

    @Override
    public void glDrawElements(int mode, int count, int type, long indices) {
        GLES20.glDrawElements( mode, count, type, indices);
    }

    @Override
    public void glEnable(int cap) {
        GLES20.glEnable( cap);
    }

    @Override
    public void glEnableVertexAttribArray(int index) {
        GLES20.glEnableVertexAttribArray( index);
    }

    @Override
    public void glFinish() {
        GLES20.glFinish();
    }

    @Override
    public void glFlush() {
        GLES20.glFlush();
    }

    @Override
    public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
        GLES20.glFramebufferRenderbuffer( target, attachment, renderbuffertarget, renderbuffer);
    }

    @Override
    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        GLES20.glFramebufferTexture2D( target, attachment, textarget, texture, level);
    }

    @Override
    public void glFrontFace(int mode) {
        GLES20.glFrontFace( mode);
    }

    @Override
    public void glGenBuffers(IntBuffer buffers) {
        GLES20.glGenBuffers(buffers);
    }

    @Override
    public void glGenBuffers(int[] buffers) {
        GLES20.glGenBuffers(buffers);
    }

    @Override
    public void glGenerateMipmap(int target) {
        GLES20.glGenerateMipmap( target);
    }

    @Override
    public void glGenFramebuffers(IntBuffer framebuffers) {
        GLES20.glGenFramebuffers(framebuffers);
    }
    
    @Override
    public void glGenFramebuffers(int[] framebuffers) {
        GLES20.glGenFramebuffers(framebuffers);
    }

    @Override
    public void glGenRenderbuffers(IntBuffer renderbuffers) {
        GLES20.glGenRenderbuffers(renderbuffers);
    }

    @Override
    public void glGenTextures(IntBuffer textures) {
        GLES20.glGenTextures(textures);
    }

    @Override
    public void glGenTextures(int[] textures) {
        GLES20.glGenTextures(textures);
    }

    @Override
    public void glGetActiveAttrib(int program, int index, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
        throw new UnimplementedException("todo");
    }

    @Override
    public void glGetActiveUniform(int program, int index, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
        throw new UnimplementedException("todo");
    }

    @Override
    public void glGetAttachedShaders(int program, IntBuffer count, IntBuffer shaders) {
        GLES20.glGetAttachedShaders( program, count, shaders);
    }

    @Override
    public int glGetAttribLocation(int program, CharArray name) {
        return GLES20.glGetAttribLocation( program, name.toString());
    }

    @Override
    public void glGetBooleanv(int pname, ByteBuffer data) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetBufferParameteriv(int target, int pname, IntBuffer params) {
        GLES20.glGetBufferParameteriv( target, pname, params);
    }

    @Override
    public int glGetError() {
        return GLES20.glGetError();
    }

    @Override
    public void glGetFloatv(int pname, FloatBuffer data) {
        GLES20.glGetFloatv( pname, data);
    }

    @Override
    public void glGetFloatv(int pname, float[] data) {
        GLES20.glGetFloatv( pname, data);
    }

    @Override
    public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, IntBuffer params) {
        GLES20.glGetFramebufferAttachmentParameteriv( target, attachment, pname, params);
    }

    @Override
    public void glGetIntegerv(int pname, IntBuffer data) {
        GLES20.glGetIntegerv( pname, data);
    }

    @Override
    public void glGetIntegerv(int pname, int[] data) {
        GLES20.glGetIntegerv( pname, data);
    }

    @Override
    public void glGetProgramiv(int program, int pname, IntBuffer params) {
        GLES20.glGetProgramiv(program, pname, params);
    }

    @Override
    public void glGetProgramiv(int program, int pname, int[] params) {
        GLES20.glGetProgramiv(program, pname, params);
    }

    @Override
    public void glGetProgramInfoLog(int program, int[] length, ByteBuffer infoLog) {
        GLES20.glGetProgramInfoLog(program, length, infoLog);
    }

    @Override
    public void glGetRenderbufferParameteriv(int target, int pname, IntBuffer params) {
        GLES20.glGetRenderbufferParameteriv( target, pname, params);
    }

    @Override
    public void glGetShaderiv(int shader, int pname, IntBuffer params) {
        GLES20.glGetShaderiv( shader, pname, params);
    }

    @Override
    public void glGetShaderiv(int shader, int pname, int[] params) {
        GLES20.glGetShaderiv(shader, pname, params);
    }

    @Override
    public void glGetShaderInfoLog(int shader, IntBuffer length, ByteBuffer infoLog) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, IntBuffer range, IntBuffer precision) {
        GLES20.glGetShaderPrecisionFormat( shadertype, precisiontype, range, precision);
    }

    @Override
    public void glGetShaderSource(int shader, IntBuffer length, ByteBuffer source) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public byte glGetString(int name) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glGetTexParameterfv(int target, int pname, FloatBuffer params) {
        GLES20.glGetTexParameterfv( target, pname, params);
    }

    @Override
    public void glGetTexParameteriv(int target, int pname, IntBuffer params) {
        GLES20.glGetTexParameteriv( target, pname, params);
    }

    @Override
    public void glGetUniformfv(int program, int location, FloatBuffer params) {
        GLES20.glGetUniformfv( program, location, params);
    }

    @Override
    public void glGetUniformiv(int program, int location, IntBuffer params) {
        GLES20.glGetUniformiv( program, location, params);
    }

    @Override
    public int glGetUniformLocation(int program, CharArray name) {
        return GLES20.glGetUniformLocation( program, name.toString());
    }

    @Override
    public void glGetVertexAttribfv(int index, int pname, FloatBuffer params) {
        GLES20.glGetVertexAttribfv( index, pname, params);
    }

    @Override
    public void glGetVertexAttribiv(int index, int pname, IntBuffer params) {
        GLES20.glGetVertexAttribiv( index, pname, params);
    }

    @Override
    public void glGetVertexAttribPointerv(int index, int pname, ByteBuffer pointer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glHint(int target, int mode) {
        GLES20.glHint( target, mode);
    }

    @Override
    public boolean glIsBuffer(int buffer) {
        return GLES20.glIsBuffer( buffer);
    }

    @Override
    public boolean glIsEnabled(int cap) {
        return GLES20.glIsEnabled( cap);
    }

    @Override
    public boolean glIsFramebuffer(int framebuffer) {
        return GLES20.glIsFramebuffer( framebuffer);
    }

    @Override
    public boolean glIsProgram(int program) {
        return GLES20.glIsProgram( program);
    }

    @Override
    public boolean glIsRenderbuffer(int renderbuffer) {
        return GLES20.glIsRenderbuffer( renderbuffer);
    }

    @Override
    public boolean glIsShader(int shader) {
        return GLES20.glIsShader( shader);
    }

    @Override
    public boolean glIsTexture(int texture) {
        return GLES20.glIsTexture( texture);
    }

    @Override
    public void glLineWidth(float width) {
        GLES20.glLineWidth( width);
    }

    @Override
    public void glLinkProgram(int program) {
        GLES20.glLinkProgram( program);
    }

    @Override
    public void glPixelStorei(int pname, int param) {
        GLES20.glPixelStorei( pname, param);
    }

    @Override
    public void glPolygonOffset(float factor, float units) {
        GLES20.glPolygonOffset( factor, units);
    }

    @Override
    public void glReadPixels(int x, int y, int width, int height, int format, int type, ByteBuffer pixels) {
        GLES20.glReadPixels( x, y, width, height, format, type, pixels);
    }

    @Override
    public void glReleaseShaderCompiler() {
        GLES20.glReleaseShaderCompiler();
    }

    @Override
    public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
        GLES20.glRenderbufferStorage( target, internalformat, width, height);
    }

    @Override
    public void glSampleCoverage(float value, boolean invert) {
        GLES20.glSampleCoverage(value, invert);
    }

    @Override
    public void glScissor(int x, int y, int width, int height) {
        GLES20.glScissor(x, y, width, height);
    }

    @Override
    public void glShaderBinary(IntBuffer shaders, int binaryformat, ByteBuffer binary) {
        GLES20.glShaderBinary(shaders, binaryformat, binary);
    }

    @Override
    public void glShaderSource(int shader, CharArray[] strings) {
        GLES20.glShaderSource(shader, LGL.toString(strings));
    }

    @Override
    public void glStencilFunc(int func, int ref, int mask) {
        GLES20.glStencilFunc(func, ref, mask);
    }

    @Override
    public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
        GLES20.glStencilFuncSeparate(face, func, ref, mask);
    }

    @Override
    public void glStencilMask(int mask) {
        GLES20.glStencilMask(mask);
    }

    @Override
    public void glStencilMaskSeparate(int face, int mask) {
        GLES20.glStencilMaskSeparate(face, mask);
    }

    @Override
    public void glStencilOp(int fail, int zfail, int zpass) {
        GLES20.glStencilOp(fail, zfail, zpass);
    }

    @Override
    public void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass) {
        GLES20.glStencilOpSeparate(face, sfail, dpfail, dppass);
    }

    @Override
    public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
        if (pixels instanceof ByteBuffer) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (ByteBuffer)pixels);
        if (pixels instanceof ShortBuffer) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (ShortBuffer)pixels);
        if (pixels instanceof IntBuffer) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (IntBuffer)pixels);
        if (pixels instanceof FloatBuffer) GLES20.glTexImage2D(target, level, internalformat, width, height, border, format, type, (FloatBuffer)pixels);
        else throw new InvalidArgumentException("Unsupported buffer "+pixels);
    }

    @Override
    public void glTexParameterf(int target, int pname, float param) {
        GLES20.glTexParameterf(target, pname, param);
    }

    @Override
    public void glTexParameterfv(int target, int pname, FloatBuffer params) {
        GLES20.glTexParameterfv(target, pname, params);
    }

    @Override
    public void glTexParameteri(int target, int pname, int param) {
        GLES20.glTexParameteri(target, pname, param);
    }

    @Override
    public void glTexParameteriv(int target, int pname, IntBuffer params) {
        GLES20.glTexParameteriv(target, pname, params);
    }

    @Override
    public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, ByteBuffer pixels) {
        GLES20.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
    }

    @Override
    public void glUniform1fv(int location, FloatBuffer value) {
        GLES20.glUniform1fv(location, value);
    }

    @Override
    public void glUniform1fv(int location, float[] value) {
        GLES20.glUniform1fv(location, value);
    }

    @Override
    public void glUniform2fv(int location, FloatBuffer value) {
        GLES20.glUniform2fv(location, value);
    }

    @Override
    public void glUniform2fv(int location, float[] value) {
        GLES20.glUniform2fv(location, value);
    }

    @Override
    public void glUniform3fv(int location, FloatBuffer value) {
        GLES20.glUniform3fv(location, value);
    }

    @Override
    public void glUniform3fv(int location, float[] value) {
        GLES20.glUniform3fv(location, value);
    }

    @Override
    public void glUniform4fv(int location, FloatBuffer value) {
        GLES20.glUniform4fv(location, value);
    }

    @Override
    public void glUniform4fv(int location, float[] value) {
        GLES20.glUniform4fv(location, value);
    }

    @Override
    public void glUniform1iv(int location, IntBuffer value) {
        GLES20.glUniform1iv(location, value);
    }

    @Override
    public void glUniform1iv(int location, int[] value) {
        GLES20.glUniform1iv(location, value);
    }

    @Override
    public void glUniform2iv(int location, IntBuffer value) {
        GLES20.glUniform2iv(location, value);
    }

    @Override
    public void glUniform2iv(int location, int[] value) {
        GLES20.glUniform2iv(location, value);
    }

    @Override
    public void glUniform3iv(int location, IntBuffer value) {
        GLES20.glUniform3iv(location, value);
    }

    @Override
    public void glUniform3iv(int location, int[] value) {
        GLES20.glUniform3iv(location, value);
    }

    @Override
    public void glUniform4iv(int location, IntBuffer value) {
        GLES20.glUniform4iv(location, value);
    }

    @Override
    public void glUniform4iv(int location, int[] value) {
        GLES20.glUniform4iv(location, value);
    }

    @Override
    public void glUniform1f(int location, float v0) {
        GLES20.glUniform1f(location, v0);
    }

    @Override
    public void glUniform1i(int location, int v0) {
        GLES20.glUniform1i(location, v0);
    }

    @Override
    public void glUniform2f(int location, float v0, float v1) {
        GLES20.glUniform2f(location, v0, v1);
    }

    @Override
    public void glUniform2i(int location, int v0, int v1) {
        GLES20.glUniform2i(location, v0, v1);
    }

    @Override
    public void glUniform3f(int location, float v0, float v1, float v2) {
        GLES20.glUniform3f(location, v0, v1, v2);
    }

    @Override
    public void glUniform3i(int location, int v0, int v1, int v2) {
        GLES20.glUniform3i(location, v0, v1, v2);
    }

    @Override
    public void glUniform4f(int location, float v0, float v1, float v2, float v3) {
        GLES20.glUniform4f(location, v0, v1, v2, v3);
    }

    @Override
    public void glUniform4i(int location, int v0, int v1, int v2, int v3) {
        GLES20.glUniform4i(location, v0, v1, v2, v3);
    }

    @Override
    public void glUniformMatrix2fv(int location, boolean transpose, FloatBuffer value) {
        GLES20.glUniformMatrix2fv(location, transpose, value);
    }

    @Override
    public void glUniformMatrix2fv(int location, boolean transpose, float[] value) {
        GLES20.glUniformMatrix2fv(location, transpose, value);
    }

    @Override
    public void glUniformMatrix3fv(int location, boolean transpose, FloatBuffer value) {
        GLES20.glUniformMatrix3fv(location, transpose, value);
    }

    @Override
    public void glUniformMatrix3fv(int location, boolean transpose, float[] value) {
        GLES20.glUniformMatrix3fv(location, transpose, value);
    }

    @Override
    public void glUniformMatrix4fv(int location, boolean transpose, FloatBuffer value) {
        GLES20.glUniformMatrix4fv(location, transpose, value);
    }

    @Override
    public void glUniformMatrix4fv(int location, boolean transpose, float[] value) {
        GLES20.glUniformMatrix4fv(location, transpose, value);
    }

    @Override
    public void glUseProgram(int program) {
        GLES20.glUseProgram(program);
    }

    @Override
    public void glValidateProgram(int program) {
        GLES20.glValidateProgram(program);
    }

    @Override
    public void glVertexAttrib1f(int index, float x) {
        GLES20.glVertexAttrib1f(index, x);
    }

    @Override
    public void glVertexAttrib1fv(int index, FloatBuffer v) {
        GLES20.glVertexAttrib1fv(index, v);
    }

    @Override
    public void glVertexAttrib2f(int index, float x, float y) {
        GLES20.glVertexAttrib2f(index, x, y);
    }

    @Override
    public void glVertexAttrib2fv(int index, FloatBuffer v) {
        GLES20.glVertexAttrib2fv(index, v);
    }

    @Override
    public void glVertexAttrib3f(int index, float x, float y, float z) {
        GLES20.glVertexAttrib3f(index, x, y, z);
    }

    @Override
    public void glVertexAttrib3fv(int index, FloatBuffer v) {
        GLES20.glVertexAttrib3fv(index, v);
    }

    @Override
    public void glVertexAttrib4f(int index, float x, float y, float z, float w) {
        GLES20.glVertexAttrib4f(index, x, y, z, w);
    }

    @Override
    public void glVertexAttrib4fv(int index, FloatBuffer v) {
        GLES20.glVertexAttrib4fv(index, v);
    }

    @Override
    public void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, long pointer) {
        GLES20.glVertexAttribPointer(index, size, type, normalized, stride, pointer);
    }

    @Override
    public void glViewport(int x, int y, int width, int height) {
        GLES20.glViewport(x, y, width, height);
    }

}