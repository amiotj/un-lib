
package science.unlicense.impl.gpu.lwjgl.opengl;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.gpu.opengl.GLBinding;
import science.unlicense.api.gpu.opengl.GLFrame;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public class LGLBinding implements GLBinding {

    public static final LGLBinding INSTANCE = new LGLBinding();

    @Override
    public GLSource createOffScreen(int width, int height) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public GLFrame createFrame(boolean opaque) {
        return new LWJGLFrame();
    }

    @Override
    public Margin getFrameMargin() {
        throw new UnimplementedException("Not supported yet.");
    }

}
