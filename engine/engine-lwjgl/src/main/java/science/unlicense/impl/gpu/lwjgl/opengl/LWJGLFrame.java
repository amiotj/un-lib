package science.unlicense.impl.gpu.lwjgl.opengl;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GLFrame;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.math.Tuple;

/**
 * TODO
 */
public class LWJGLFrame extends AbstractEventSource implements GLFrame, GLFWKeyCallbackI {

    private final GLSource source = new LGLSource();
    // The window handle
    private long window;

    private CharArray title;

    public void run() {

        try {
            init();
            loop();

            // Free the window callbacks and destroy the window
            glfwFreeCallbacks(window);
            glfwDestroyWindow(window);
        } finally {
            // Terminate GLFW and free the error callback
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }
    }

    public LWJGLFrame(){
        // TODO : Print errors, find a better approach
        GLFWErrorCallback.createPrint(System.err).set();
    }

    private void init() {

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        // Configure our window
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        int WIDTH = 300;
        int HEIGHT = 300;

        // Create the window
        window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World!", NULL, NULL);
        if (window == NULL) {
            throw new RuntimeException("Failed to create the GLFW window");
        }

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, new GLFWKeyCallbackI() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                    glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
                }
            }
        });

        // Get the resolution of the primary monitor
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        // Center our window
        glfwSetWindowPos(
                window,
                (vidmode.width() - WIDTH) / 2,
                (vidmode.height() - HEIGHT) / 2
        );

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);
    }

    private void loop() {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        final GLCapabilities capa = GL.createCapabilities();

        // Set the clear color
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            glfwSwapBuffers(window); // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();
        }
    }

    @Override
    public GLSource getSource() {
        return source;
    }

    @Override
    public CharArray getTitle() {
        return title;
    }

    @Override
    public void setTitle(CharArray title) {
        this.title = title;
        glfwSetWindowTitle(window, title.toString());
    }

    /**
     *
     * @return true is pointer is visible
     */
    public boolean isPointerVisible() {
        throw new UnimplementedException("Not supported");
    }

    /**
     *
     * @param visible pointer visible state
     */
    public void setPointerVisible(boolean visible){
        if (visible) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        } else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            //grab the cursor, prevent it going out
            //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }

    @Override
    public Cursor getCursor() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setCursor(Cursor cursor) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public Tuple getOnScreenLocation() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setSize(int width, int height) {
        glfwSetWindowSize(window, width, height);
    }

    @Override
    public Extent getSize() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            glfwShowWindow(window);
        } else {
            glfwHideWindow(window);
        }
    }

    @Override
    public boolean isVisible() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public int getState() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setState(int state) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMaximizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setMinimizable(boolean minimizable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isMinimizable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setClosable(boolean closable) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isClosable() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public boolean isAlwaysOnTop() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void dispose() {
        glfwSetWindowShouldClose(window, true);
    }

    @Override
    public boolean isDisposed() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void waitForDisposal() {
        throw new UnimplementedException("Not supported yet.");
    }

    @Override
    public void setDecorated(boolean decorated) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDecorated() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isOpaque() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void invoke(long l, int i, int i1, int i2, int i3) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void callback(long l) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSignature() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long address() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}