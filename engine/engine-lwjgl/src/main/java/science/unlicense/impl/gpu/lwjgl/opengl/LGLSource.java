
package science.unlicense.impl.gpu.lwjgl.opengl;

import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import org.lwjgl.opengl.GLCapabilities;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLBinding;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;

/**
 *
 * @author Johann Sorel
 */
public class LGLSource implements GLSource{

    private final Sequence callbacks = new ArraySequence();

    private final GL ugl = new LGL();
    int x;
    int y;
    int width;
    int height;

    public LGLSource() {
    }

    void init(long window) {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        final GLCapabilities capa = org.lwjgl.opengl.GL.createCapabilities();

        // Set the clear color
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            //todo get view size
//            this.x = x;
//            this.y = y;
//            this.width = width;
//            this.height = height;

            glfwSwapBuffers(window); // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();
        }
    }

    @Override
    public GLBinding getBinding() {
        return LGLBinding.INSTANCE;
    }

    @Override
    public GL getGL() {
        return ugl;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public Sequence getCallbacks() {
        return callbacks;
    }

    @Override
    public void render() {
        //todo
    }

    @Override
    public void dispose() {
        //todo
    }

//    private void renderInternal(){
//        for(int i=0,n=callbacks.getSize();i<n;i++) {
//            ((GLCallback)callbacks.get(i)).execute(this);
//        }
//    }
//
//    @Override
//    public void dispose(GLAutoDrawable glad) {
//        setDrawable(glad);
//        for(int i=0,n=callbacks.getSize();i<n;i++) {
//            ((GLCallback)callbacks.get(i)).dispose(this);
//        }
//    }
}
