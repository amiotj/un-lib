

package science.unlicense.impl.code.css;

import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.AbstractReader;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;

/**
 *
 * @author Johann Sorel
 */
public class CSSReader extends AbstractReader {
    
    public CSSProperties readProperties() throws IOException{
        final CSSProperties properties = new CSSProperties();
        
        final ByteInputStream in = getInputAsByteStream();
        final byte[] data = IOUtilities.readAll(in);
        final Chars text = new Chars(data, CharEncodings.UTF_8);
        final Chars[] parts = text.split(CSSConstants.PROPERTY_END);
        for(int i=0;i<parts.length;i++){
            final Chars[] ele = parts[i].split(CSSConstants.PROPERTY_SEP);
            properties.add(ele[0], ele[1]);
        }
        
        return properties;
    }
    
    public CSSStyleSheet readStyleSheet(){
        
        final CSSStyleSheet css = new CSSStyleSheet();
        
        
        return css;
        
    }
    
}
