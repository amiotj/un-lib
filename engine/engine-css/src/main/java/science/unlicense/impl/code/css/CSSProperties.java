
package science.unlicense.impl.code.css;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.number.Float64;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.Colors;

/**
 *
 * @author Johann Sorel
 */
public class CSSProperties extends HashDictionary{
    
    private static final Chars NONE = new Chars("none");
    
    public Color getColor(Chars name){
        final Chars txt = (Chars) getValue(name);
        if(txt==null) return null;
        if(NONE.equals(txt)) return null;
        return Colors.fromHexa(txt);
    }
    
    public Double getDouble(Chars name){
        final Chars txt = (Chars) getValue(name);
        if(txt==null) return null;
        return Float64.decode(txt);
    }
    
}
