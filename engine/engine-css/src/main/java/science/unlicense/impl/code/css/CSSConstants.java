

package science.unlicense.impl.code.css;


/**
 *
 * @author Johann Sorel
 */
public final class CSSConstants {
    
    public static final int CLASS_START = '{';
    public static final int CLASS_END = '}';
    public static final int PROPERTY_SEP = ':';
    public static final int PROPERTY_END = ';';
    
    private CSSConstants(){}
    
}
