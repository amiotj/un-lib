

package science.unlicense.impl.code.css;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class CSSStyleSheet {
    
    private final Sequence classes = new ArraySequence();

    public CSSStyleSheet() {
    }

    public Sequence getClasses() {
        return classes;
    }
    
}
