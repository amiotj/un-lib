
package science.unlicense.impl.code.css;

/**
 *
 * @author Johann Sorel
 */
public class CSSClass {
    
    private final CSSProperties properties = new CSSProperties();
    
    private CSSClass(){
        
    }

    public CSSProperties getProperties() {
        return properties;
    }
    
}
