#version 400

<STRUCTURE>

<LAYOUT>
layout(triangles, invocations = 1) in;
layout(triangle_strip, max_vertices = $SNBC) out;

<UNIFORM>
uniform float spawnScale = 0.2;

<VARIABLE_IN>
vec4 position_model;
vec3 normal_model;

<VARIABLE_OUT>
vec4 position_world;
vec4 position_camera;
vec3 normal_model;
vec3 normal_camera;
vec2 uv;

<VARIABLE_WS>
const float TPI = 3.14*2.0;
const vec3 SPAWN_VERTEX[$SNB] = vec3[$SNB]($S_VERTEX);
const vec3 SPAWN_NORMAL[$SNB] = vec3[$SNB]($S_NORMAL);
const vec2 SPAWN_UV[$SNB] = vec2[$SNB]($S_UV);
const vec4 POSES[5] = vec4[5](vec4(0.8,0.1,0.1,0.1*TPI),vec4(0.1,0.8,0.1,0.3*TPI),vec4(0.1,0.1,0.8,0.5*TPI),vec4(0.2,0.5,0.3,0.7*TPI),vec4(0.3,0.2,0.5,0.9*TPI));
mat3 btn;

<FUNCTION>
mat3 createRotation3(float angle, vec3 axis) {

    float fCos = cos(angle);
    float fSin = sin(angle);
    float fOneMinusCos = (1.0) - fCos;
    float fX2 = axis.x * axis.x;
    float fY2 = axis.y * axis.y;
    float fZ2 = axis.z * axis.z;
    float fXYM = axis.x * axis.y * fOneMinusCos;
    float fXZM = axis.x * axis.z * fOneMinusCos;
    float fYZM = axis.y * axis.z * fOneMinusCos;
    float fXSin = axis.x * fSin;
    float fYSin = axis.y * fSin;
    float fZSin = axis.z * fSin;

    return mat3(
        fX2 * fOneMinusCos + fCos, fXYM + fZSin,              fXZM - fYSin,
        fXYM - fZSin,              fY2 * fOneMinusCos + fCos, fYZM + fXSin,
        fXZM + fYSin,              fYZM - fXSin,              fZ2 * fOneMinusCos + fCos);
}

mat3 createRotation3(vec3 v1, vec3 v2) {
    float angle = acos(dot(v1,v2));
    if(angle==0){
        //vectors are colinear
        return mat3(1);
    }
    vec3 axis = normalize(cross(v1,v2));
    return createRotation3(angle, axis);
}

vec4 toModelCoord(vec3 t){
    return vec4(    t.x * inData[0].position_model.xyz
                  + t.y * inData[1].position_model.xyz
                  + t.z * inData[2].position_model.xyz
                  , 1);
}

void createVertex(vec4 p, vec3 n, vec2 uv){
    outData.normal_model = n;
    outData.normal_camera = (MVP * vec4(n,0)).xyz;
    outData.position_world = M * p;
    outData.position_camera = MVP * p;
    outData.uv = uv;
    gl_Position = outData.position_camera;
    EmitVertex();
}

vec3 rot(vec3 v, float co, float si){
    return vec3(v.x*co, v.y, v.z*si);
}

void spawn(vec4 c, vec3 n, float rot){

    //check camera distance
    vec4 v = MV * c;
    if(length(v.xyz) > 30) return;

    //add some randomness
    rot = rot  + gl_PrimitiveIDIn + gl_InvocationID;
    float co = cos(rot);
    float si = sin(rot);
    mat3 r = mat3(co,0,-si, 0,1,0, si,0,co);

    for(int i=0;i<$SNB;){
        createVertex(c + vec4(btn*r*SPAWN_VERTEX[i],0),btn*r*SPAWN_NORMAL[i], SPAWN_UV[i]);
        i++;
        createVertex(c + vec4(btn*r*SPAWN_VERTEX[i],0),btn*r*SPAWN_NORMAL[i], SPAWN_UV[i]);
        i++;
        createVertex(c + vec4(btn*r*SPAWN_VERTEX[i],0),btn*r*SPAWN_NORMAL[i], SPAWN_UV[i]);
        i++;
        EndPrimitive();
    }
}

<OPERATION>

    //calculate normal
    vec3 u = inData[1].position_model.xyz - inData[0].position_model.xyz;
    vec3 v = inData[2].position_model.xyz - inData[0].position_model.xyz;
    vec3 n = normalize(cross(u, v));

    btn = createRotation3(vec3(0,1,0), n);
    btn = (spawnScale/2.0) * btn;

    for(int i=0;i<$SMAX;i++){
        spawn(toModelCoord(POSES[i].xyz),n,POSES[i].w);
    }
