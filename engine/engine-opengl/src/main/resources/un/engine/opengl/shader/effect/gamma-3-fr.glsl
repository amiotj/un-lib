#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform vec3 gamma = vec3(2.2,2.2,2.2);

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>

    //TODO consideration for premultiplied colors ?
    outColor = texture(sampler0, inData.uv);
    outColor.xyz = pow(outColor.xyz, 1.0/gamma);