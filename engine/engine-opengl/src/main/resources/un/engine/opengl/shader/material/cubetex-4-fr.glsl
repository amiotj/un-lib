#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform samplerCube $prefix_tex;
uniform float $prefix_ratio = 1.0;

<VARIABLE_IN>
vec3 $prefix_uvz;

<VARIABLE_WS>

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    $produce = $method(texture($prefix_tex, inData.$prefix_uvz)*$prefix_ratio,$produce);

