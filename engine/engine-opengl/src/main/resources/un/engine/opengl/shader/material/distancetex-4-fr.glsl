#version 330

<STRUCTURE>
// Distance map contour texturing, Stefan Gustavson 2009
// A re-implementation of Green's method, with an
// 8-bit distance map but explicit texel interpolation.
// This code is in the public domain.

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;
//spriteIndex,spacing,width,height
uniform vec4 $prefix_sprite = vec4(0,0,1,1);
uniform float $prefix_ratio = 1.0;
uniform vec2 $prefix_scale = vec2(1.0,1.0);
uniform vec2 $prefix_offset = vec2(0.0,0.0);

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>
const float smoothing = 1.0/16.0;

<VARIABLE_OUT>

<FUNCTION>
vec2 get$prefix_UV(vec2 uv){
    ivec2 texSize = textureSize($prefix_tex,0);
    float spriteIndex = $prefix_sprite.x;
    float spacing = $prefix_sprite.y;
    float width = $prefix_sprite.z;
    float height = $prefix_sprite.w;
    float currentCol = int(mod(spriteIndex,width));
    float currentRow = int(spriteIndex/width);
    float imgWidth = float(texSize.x);
    float imgHeight = float(texSize.y);

    float trsX = (currentCol*(width+spacing)) / imgWidth;
    float trsY = (currentRow*(height+spacing)) / imgHeight;
    float scaleX = width/imgWidth;
    float scaleY = height/imgHeight;

    return vec2((uv.x*scaleX)+trsX,(uv.y*scaleY)+trsY);
}

<OPERATION>
    //vec2 $prefix_suv = get$prefix_UV(inData.uv);
    //$prefix_suv -= vec2(0.5,0.5);
    //$prefix_suv *= $prefix_scale;
    //$prefix_suv += $prefix_offset;
    //$prefix_suv += vec2(0.5,0.5);
    //$produce = $method(texture($prefix_tex, $prefix_suv).rgba*$prefix_ratio,$produce);

    //////////////////////////////////////////

    ivec2 texSize = textureSize($prefix_tex,0);
    float imgWidth = float(texSize.x);
    float imgHeight = float(texSize.y);

    float onestepu = 1.0 / imgWidth;
    float onestepv = 1.0 / imgHeight;

    // Scale texcoords to range ([0,imgWidth], [0,imgHeight])
    vec2 uv = inData.uv * vec2(imgWidth, imgHeight);

    // Compute texel-local (u,v) coordinates for the four closest texels
    vec2 uv00 = floor(uv - vec2(0.5)); // Lower left corner of lower left texel
    vec2 uvthis = floor(uv); // Lower left corner of texel containing (u,v)
    vec2 uvlerp = uv - uv00 - vec2(0.5); // Texel-local lerp blends [0,1]

    // Perform explicit texture interpolation of D coefficient.
    // This works around the currently very bad texture interpolation
    // precision in ATI hardware.

    // Center st00 on lower left texel and rescale to [0,1] for texture lookup
    vec2 st00 = (uv00  + vec2(0.5)) * vec2(onestepu, onestepv);

    // Compute g_u, g_v, D coefficients from four closest 8-bit RGBA texels
    vec4 rawtex00 = texture2D($prefix_tex, st00);
    vec4 rawtex10 = texture2D($prefix_tex, st00 + vec2(0.5*onestepu, 0.0));
    vec4 rawtex01 = texture2D($prefix_tex, st00 + vec2(0.0, 0.5*onestepv));
    vec4 rawtex11 = texture2D($prefix_tex, st00 + vec2(0.5*onestepu, 0.5*onestepv));

    // Restore the value for D from its 8-bit encoding
    vec2 D00_10 = 16.0*(vec2(rawtex00.r, rawtex10.r)-0.50196);
    vec2 D01_11 = 16.0*(vec2(rawtex01.r, rawtex11.r)-0.50196);

    // Interpolate D between four closest texels
    vec2 uvlocal = fract(uv)-0.5; // Texel-local uv coordinates [-0.5,0.5]
    // Interpolate along v
    vec2 D0_1 = mix(D00_10, D01_11, uvlerp.y);
    // Interpolate along u
    float D = mix(D0_1.x, D0_1.y, uvlerp.x);

    // Perform anisotropic analytic antialiasing (fwidth() is slightly wrong)
    float aastep = length(vec2(dFdx(D), dFdy(D)));

    // 'pattern' is 1 where D>0, 0 where D<0, with proper AA around D=0.
    float pattern = smoothstep(-aastep, aastep, D);

    // Final fragment color
    vec4 r = vec4(pattern,pattern,pattern,1.0);
    $produce = $method(r*$prefix_ratio,$produce);
