#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec3 l_position;

<UNIFORM>
uniform mat4 MVP;

<VARIABLE_OUT>
vec4 position_proj;

<FUNCTION>

<OPERATION>
    outData.position = MVP*vec4(l_position,1);
    gl_Position = outData.position_proj;