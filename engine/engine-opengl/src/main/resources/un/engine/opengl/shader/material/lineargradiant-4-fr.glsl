#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform mat3 MV;
uniform mat3 P;
uniform int NBSTEP;
uniform float GSTEPS[32];
uniform vec4 GCOLORS[32];
uniform vec2 GSTART;
uniform vec2 GEND;

<VARIABLE_WS>

<FUNCTION>

float distance(vec2 c){
    vec3 vstart = MV*vec3(GSTART,1);
    vec3 vend = MV*vec3(GEND,1);
    float adj = vend.x - vstart.x;
    float opp = vend.y - vstart.y;
    float hyp = sqrt(adj*adj + opp*opp);
    return ( (vstart.y - c.y)*(vstart.y - vend.y) - (vstart.x - c.x)*(vend.x - vstart.x) ) / (hyp*hyp) ;
}

<OPERATION>
    float d = clamp(distance(gl_FragCoord.xy),0.0,1.0);

    //find interval
    material.color = vec4(0);
    for(int index=0;index<NBSTEP-1;index++){
        if(d >= GSTEPS[index] && d <= GSTEPS[index+1]){
            d = (d-GSTEPS[index]) / (GSTEPS[index+1]-GSTEPS[index]);
            vec4 c0 = GCOLORS[index];
            vec4 c1 = GCOLORS[index+1];
            material.color = mix(c0, c1, d);
            break;
        }
    }

