#version 400

<STRUCTURE>

<LAYOUT>
layout(triangles) in;
layout(line_strip, max_vertices = 2) out;

<UNIFORM>
uniform float normalLength = 1.0;

<VARIABLE_IN>
vec4 position_model;
vec3 normal_model;

<VARIABLE_OUT>
vec3 normal_model;

<FUNCTION>

<OPERATION>

    //calculate normal
    vec3 u = inData[1].position_model.xyz - inData[0].position_model.xyz;
    vec3 v = inData[2].position_model.xyz - inData[0].position_model.xyz;
    vec3 n = normalize(cross(u, v));
    n = (MVP * vec4(n,0)).xyz;
    n *= normalLength;

    //calculate triangle center
    vec4 c = (inData[0].position_model + inData[1].position_model + inData[2].position_model) / 3.0;
    c = MVP * c;


    gl_Position = c;
    outData.normal_model = vec3(0);
    EmitVertex();
    gl_Position = c + vec4(n,0);
    outData.normal_model = vec3(0);
    EmitVertex();
    EndPrimitive();