#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform float bloomFactor;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>

    //calculate the average color of near pixels
    //TODO use a gaussian matrix ? allow custom size ?
    vec4 avg = vec4(0);
    int x;
    int y;
    for(y=-4;y<4;y++){
        for(x=-4;x<4;x++){
            avg += texture(sampler0, inData.uv + vec2(x, y)*PX.x*4);
        }
    }
    avg *= bloomFactor;

    //calculate current fragment lightness
    //we use the same function as is RGB to HSL
    vec4 tex = texture(sampler0, inData.uv);
    float lightness = ( max(max(tex.r,tex.g),tex.b) + min(min(tex.r,tex.g),tex.b) ) *0.5;

    //arbitrary linear interpolation to calculate factor
    float scale = 0.005 + (1.0-lightness)*0.006;
    outColor = avg*avg*scale + tex;
