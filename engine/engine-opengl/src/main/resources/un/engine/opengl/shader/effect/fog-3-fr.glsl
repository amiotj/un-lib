#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
// sampler0 = default RGBA image
// sampler1 = depth image
// fog params :
// -x : density
uniform vec4 fogParams;
uniform vec4 fogColor;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>
const float E = 2.71828182845904523536028747135266249;

<FUNCTION>
vec4 fog(vec4 color, float depth, float density){
    float ratio = pow(E, -pow(depth*density, 2));
    return mix(fogColor, color, ratio);
}       

<OPERATION>
    //depth is negative relative to camera position
    //float depth = -texture(sampler1, inData.uv).z;
    //depth = depth / 20.0;
    //depth = clamp(depth, 0.0, 1.0);
    //color = vec4(0);
    //color += texture(sampler0, inData.uv) * (1.0-depth);
    //color += fogColor * depth;

    float depth = -texture(sampler1, inData.uv).z;
    outColor = texture(sampler0, inData.uv);
    outColor = fog(outColor, depth, fogParams.x);

