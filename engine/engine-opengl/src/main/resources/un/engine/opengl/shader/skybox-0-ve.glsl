#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec3 l_position;

<UNIFORM>
uniform mat4 MVP;
uniform vec3 CAMERA;

<VARIABLE_OUT>
vec3 coord;

<FUNCTION>

<OPERATION>
    outData.coord = l_position;
    gl_Position = (MVP * vec4(l_position+CAMERA, 1.0));
    //place the skybox at the furthest distance
    gl_Position.z = gl_Position.w -0.00001;