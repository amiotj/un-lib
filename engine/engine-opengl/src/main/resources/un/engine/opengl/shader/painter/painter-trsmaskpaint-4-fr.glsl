#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform usampler2D SAMPLER;
uniform vec4 COLOR;
uniform float BUFFER = 1.0;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_OUT>

<FUNCTION>

bool maskAt(vec2 uv){
    vec2 uvimg = uv*viewSize*4;
    ivec2 coord = ivec2(floor(uvimg));
    ivec2 pixel = coord / 4;
    int v = int(texture(SAMPLER,uv).r);
    int col = coord.x - pixel.x*4;
    int row = coord.y - pixel.y*4;
    int mask = 1 << ((4*row) + col);
    return (v & mask) != 0;
}

<VARIABLE_WS>
//each bit mask value
const int s01 = 1;
const int s02 = 2;
const int s03 = 4;
const int s04 = 8;
const int s05 = 16;
const int s06 = 32;
const int s07 = 64;
const int s08 = 128;
const int s09 = 256;
const int s10 = 512;
const int s11 = 1024;
const int s12 = 2048;
const int s13 = 4096;
const int s14 = 8192;
const int s15 = 16384;
const int s16 = 32768;

vec2 viewSize = vec2(1,1);

<OPERATION>
    viewSize = vec2(textureSize(SAMPLER,0));

    float nb = 0;
    float pas = 0.33;
    float alpha = 0.0;
    for(float x=-1.0;x<=1.0;x+=pas){
        for(float y=-1.0;y<=1.0;y+=pas){
            if(maskAt(inData.uv + vec2(x,y)*BUFFER)) alpha++;
            nb += 1.0;
        }
    }

    alpha = alpha/nb;

    //enhance border
    //alpha = smoothstep(0.0,1.0,alpha);

    outColor = COLOR*alpha;
    //outColor = vec4(inData.uv,0,1);
    //outColor = COLOR;
