#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform mat4 UNI_P;
uniform vec2 UNI_PX;

<VARIABLE_IN>

<VARIABLE_OUT>

<VARIABLE_WS>
mat4 M;
mat4 V;
mat4 P;
vec2 PX;
mat4 MV;
mat4 MVP;


<FUNCTION>

<OPERATION>
    P = UNI_P;
    PX = UNI_PX;
    MV = V*M;
    MVP = P*V*M;
