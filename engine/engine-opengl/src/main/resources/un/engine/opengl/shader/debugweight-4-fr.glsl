#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec4 debugweight;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    material.color = inData.debugweight;
    