#version 330
<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in vec3 l_color;

<UNIFORM>

<VARIABLE_OUT>
vec4 color;

<FUNCTION>

<OPERATION>
    outData.color = vec4(l_color,1);