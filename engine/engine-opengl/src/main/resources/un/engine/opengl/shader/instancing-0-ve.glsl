#version 330

<STRUCTURE>

<LAYOUT>
layout(location = $IDX) in mat4 l_insttrs;

<UNIFORM>
uniform mat4 UNI_L;
uniform mat4 UNI_M;
uniform mat4 UNI_V;
uniform mat4 UNI_P;
uniform vec2 UNI_PX;

<VARIABLE_WS>
mat4 M;
mat4 V;
mat4 P;
vec2 PX;
mat4 MV;
mat4 MVP;

<VARIABLE_OUT>
flat mat4 M;
flat mat4 V;
flat mat4 P;
flat vec2 PX;

<VARIABLE_WS>
mat4 M;
mat4 V;
mat4 P;
vec2 PX;
mat4 MV;
mat4 MVP;

<OPERATION>
    M = UNI_M * (l_insttrs * UNI_L) ;
    V = UNI_V;
    P = UNI_P;
    PX = UNI_PX;
    MV = V*M;
    MVP = P*V*M;


    outData.M = M;
    outData.V = V;
    outData.P = P;
    outData.PX = PX;