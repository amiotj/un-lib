#version 330

<STRUCTURE>

<LAYOUT>
layout(location=4) out int outMeshId;
layout(location=5) out int outVertexId;

<UNIFORM>
uniform int PICK_COLOR = 0;

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    //mesh and vertex picking
    outMeshId = PICK_COLOR;
    if(gl_PrimitiveID>=0) outVertexId = gl_PrimitiveID;