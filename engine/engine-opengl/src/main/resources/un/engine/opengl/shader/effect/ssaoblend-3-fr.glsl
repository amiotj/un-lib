#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>
vec3 hue2rgb(float h1, float c, float x){
    vec3 rgb = vec3(0);
    //if(h==0)     { rgba[0] = 0; rgba[1]=0; rgba[2]=0; }
    if(h1<1)     { rgb.x = c; rgb.y=x; rgb.z=0; }
    else if(h1<2){ rgb.x = x; rgb.y=c; rgb.z=0; }
    else if(h1<3){ rgb.x = 0; rgb.y=c; rgb.z=x; }
    else if(h1<4){ rgb.x = 0; rgb.y=x; rgb.z=c; }
    else if(h1<5){ rgb.x = x; rgb.y=0; rgb.z=c; }
    else         { rgb.x = c; rgb.y=0; rgb.z=x; }
    return rgb;
}

vec3 hsl2rgb(vec3 hsl) {
    float h = hsl.x;
    float s = hsl.y;
    float l = hsl.z;

    float c = (1- abs(2*l-1)) * s;
    float h1 = h / 60;
    float x = c * (1 - abs(mod(h1,2) - 1));        
    vec3 rgb = hue2rgb(h1,c,x);	
    float m = l - c/2;
    rgb += m;
    return rgb;
}

vec3 rgb2hsl(vec3 rgb){
    float r = rgb.x;
    float g = rgb.y;
    float b = rgb.z;
    float ma = max(max(r,g),b);
    float mi = min(min(r,g),b);
    float c = ma - mi;

    //compute H
    float h = 0.0;
    if (c == 0) {
        h = 0.0;
    }else if(ma == r){
        h = (g-b)/c;
    }else if(ma == g){
        h = (b-r)/c + 2.0;
    }else if(ma == b){
        h = (r-g)/c + 4.0;
    }
    if(h<0.0){
        h += 6.0;
    }
    h = h/6.0 * 360.0;

    //compute L
    float l = 0.5 * (ma + mi);

    //compute S
    float s;
    if (c == 0) {
        s = 0;
    } else {
        s = c / (1.0 - abs(2.0*l-1.0) );
    }

    return vec3(h,s,l);
}


<OPERATION>
    //TODO make a more general blend phase, not just for SSAO
    vec4 baseTex = texture(sampler0,inData.uv);
    float occ = 1.0 - texture(sampler1,inData.uv).r;
    
    //TODO this produces black/yellow artifacts but better color preservation
    //vec3 hsl = rgb2hsl(baseTex.xyz);
    //hsl.z = clamp(hsl.z*occ,0,1);
    //vec3 rgb = hsl2rgb(hsl);

    outColor = vec4(baseTex.xyz*occ, baseTex.a);
    