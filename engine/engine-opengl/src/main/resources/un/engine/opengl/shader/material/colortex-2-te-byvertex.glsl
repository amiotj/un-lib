#version 400
<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_IN>
vec4 color;

<VARIABLE_OUT>
vec4 color;

<FUNCTION>

<OPERATION>
    float t0 = gl_TessCoord.x * inData[0].color.x + gl_TessCoord.y * inData[1].color.x+ + gl_TessCoord.z * inData[2].color.x;
    float t1 = gl_TessCoord.x * inData[0].color.y + gl_TessCoord.y * inData[1].color.y+ + gl_TessCoord.z * inData[2].color.y;
    float t2 = gl_TessCoord.x * inData[0].color.z + gl_TessCoord.y * inData[1].color.z+ + gl_TessCoord.z * inData[2].color.z;
    float t3 = gl_TessCoord.x * inData[0].color.w + gl_TessCoord.y * inData[1].color.w+ + gl_TessCoord.z * inData[2].color.w;
    outData.color = vec4(t0,t1,t2,t3);