#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    outColor = material.color;