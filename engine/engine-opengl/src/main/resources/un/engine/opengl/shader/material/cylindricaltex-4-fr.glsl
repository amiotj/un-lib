#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;
uniform float $prefix_ratio = 1.0;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    $produce = $method(texture($prefix_tex, inData.uvz)*$prefix_ratio,$produce);
