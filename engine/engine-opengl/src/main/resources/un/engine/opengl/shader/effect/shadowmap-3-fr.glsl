#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>
vec2 computeMoments(float depth){
    vec2 moments = vec2(depth,0);
    float dx = dFdx(depth);
    float dy = dFdy(depth);
    moments.y = depth*depth + 0.25 * (dx * dx + dy * dy);
    return moments;
}

vec2 computeDepth(){
    float depth = abs((V * inData.position_world).z);
    return vec2(depth, depth*depth);
}

<OPERATION>
    