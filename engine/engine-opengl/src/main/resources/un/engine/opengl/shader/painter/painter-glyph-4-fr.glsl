#version 330
//
// @author Johann Sorel

precision highp float;

layout(location=0) out int outColor;

uniform vec2 SIZE;

in Data {
    flat int quad;
    flat vec2 p0;
    flat vec2 p1;
    flat vec2 p2;
} data;

//each bit mask value
const int s01 = 1;
const int s02 = 2;
const int s03 = 4;
const int s04 = 8;
const int s05 = 16;
const int s06 = 32;
const int s07 = 64;
const int s08 = 128;
const int s09 = 256;
const int s10 = 512;
const int s11 = 1024;
const int s12 = 2048;
const int s13 = 4096;
const int s14 = 8192;
const int s15 = 16384;
const int s16 = 32768;

vec3 computeBary(vec2 a, vec2 b, vec2 c, vec2 p){
    vec2 v0 = b-a;
    vec2 v1 = c-a;
    vec2 v2 = p-a;
    float d00 = dot(v0,v0);
    float d01 = dot(v0,v1);
    float d11 = dot(v1,v1);
    float d20 = dot(v2,v0);
    float d21 = dot(v2,v1);
    float denom = d00 * d11 - d01 * d01;
    float v = (d11 * d20 - d01 * d21) / denom;
    float w = (d00 * d21 - d01 * d20) / denom;
    float u = 1.0 - v - w;
    return vec3(u, v, w);
}

float sign(vec2 a, vec2 b, vec2 c){
    return (a.x-c.x)*(b.y-c.y)-(b.x-c.x)*(a.y-c.y);
}

bool pointInTriangle(vec2 v1, vec2 v2, vec2 v3, vec2 p){
    bool b0 = sign(p,v1,v2) <= 0.0;
    bool b1 = sign(p,v2,v3) <= 0.0;
    bool b2 = sign(p,v3,v1) <= 0.0;
    return ((b0 == b1) && (b1 == b2));
}

bool inTriangle(vec2 p){
    return pointInTriangle(data.p0,data.p1,data.p2,p);
}

//test if the point un is inside the quadratic curve
bool inCurve(vec2 v){
    float vr = (v.x*v.x) - v.y;
    return vr <= 0.0 ;
}

bool inCurveTriangle(vec2 p){
    vec3 bary = computeBary(data.p0,data.p1,data.p2,p);
    vec2 uv = bary.x * vec2(0.5,0) 
            + bary.y * vec2(0,0) 
            + bary.z * vec2(1,1);
    return inTriangle(p) && inCurve(uv);
}

void main(void){

    int r = 0;

    //fragment position in projection space
    vec2 p = (gl_FragCoord.xy-(SIZE/2.0)) / (SIZE/2.0);

    //derivate on x and y directions
    vec2 dx = vec2(2.0/SIZE.x,0.0);
    vec2 dy = vec2(0.0,2.0/SIZE.y);
    vec2 ux = dx/8.0;
    vec2 uy = dy/8.0;

    vec2 ox1 = -ux*3;
    vec2 ox2 = -ux*1;
    vec2 ox3 = +ux*1;
    vec2 ox4 = +ux*3;

    vec2 oy1 = -uy*3;
    vec2 oy2 = -uy*1;
    vec2 oy3 = +uy*1;
    vec2 oy4 = +uy*3;

    if(data.quad == 1){
        // quadratic curve

        //4x4 samples
        if(inCurveTriangle(p+ox1+oy1)) r |= s01;
        if(inCurveTriangle(p+ox2+oy1)) r |= s02;
        if(inCurveTriangle(p+ox3+oy1)) r |= s03;
        if(inCurveTriangle(p+ox4+oy1)) r |= s04;

        if(inCurveTriangle(p+ox1+oy2)) r |= s05;
        if(inCurveTriangle(p+ox2+oy2)) r |= s06;
        if(inCurveTriangle(p+ox3+oy2)) r |= s07;
        if(inCurveTriangle(p+ox4+oy2)) r |= s08;

        if(inCurveTriangle(p+ox1+oy3)) r |= s09;
        if(inCurveTriangle(p+ox2+oy3)) r |= s10;
        if(inCurveTriangle(p+ox3+oy3)) r |= s11;
        if(inCurveTriangle(p+ox4+oy3)) r |= s12;

        if(inCurveTriangle(p+ox1+oy4)) r |= s13;
        if(inCurveTriangle(p+ox2+oy4)) r |= s14;
        if(inCurveTriangle(p+ox3+oy4)) r |= s15;
        if(inCurveTriangle(p+ox4+oy4)) r |= s16;
    }else{
        //straight line

        if(inTriangle(p+ox1+oy1)) r |= s01;
        if(inTriangle(p+ox2+oy1)) r |= s02;
        if(inTriangle(p+ox3+oy1)) r |= s03;
        if(inTriangle(p+ox4+oy1)) r |= s04;

        if(inTriangle(p+ox1+oy2)) r |= s05;
        if(inTriangle(p+ox2+oy2)) r |= s06;
        if(inTriangle(p+ox3+oy2)) r |= s07;
        if(inTriangle(p+ox4+oy2)) r |= s08;

        if(inTriangle(p+ox1+oy3)) r |= s09;
        if(inTriangle(p+ox2+oy3)) r |= s10;
        if(inTriangle(p+ox3+oy3)) r |= s11;
        if(inTriangle(p+ox4+oy3)) r |= s12;

        if(inTriangle(p+ox1+oy4)) r |= s13;
        if(inTriangle(p+ox2+oy4)) r |= s14;
        if(inTriangle(p+ox3+oy4)) r |= s15;
        if(inTriangle(p+ox4+oy4)) r |= s16;
    }

    outColor = r;
}
