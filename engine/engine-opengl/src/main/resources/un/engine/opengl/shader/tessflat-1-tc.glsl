#version 400

<STRUCTURE>

<LAYOUT>
layout (vertices = 3) out;

<UNIFORM>
uniform int innerfactor;
uniform int outterfactor;

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<FUNCTION>

<OPERATION>
    outData[gl_InvocationID].position_world  = inData[gl_InvocationID].position_world;
    outData[gl_InvocationID].position_model  = inData[gl_InvocationID].position_model;
    outData[gl_InvocationID].position_camera = inData[gl_InvocationID].position_camera;
    outData[gl_InvocationID].position_proj   = inData[gl_InvocationID].position_proj;
    outData[gl_InvocationID].normal_world    = inData[gl_InvocationID].normal_world;
    outData[gl_InvocationID].normal_model    = inData[gl_InvocationID].normal_model;

    //tessellation factor
    gl_TessLevelInner[0] = innerfactor;
    gl_TessLevelOuter[0] = outterfactor;
    gl_TessLevelOuter[1] = outterfactor;
    gl_TessLevelOuter[2] = outterfactor;