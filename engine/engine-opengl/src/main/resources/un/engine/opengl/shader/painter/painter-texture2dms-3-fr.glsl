#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform sampler2DMS SAMPLER;
uniform int NBSAMPLE;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    vec2 iTmp = textureSize(SAMPLER);
    vec2 tmp = iTmp * inData.uv;

    vec4 tcolor = vec4(0,0,0,0);
    for(int i=0; i<NBSAMPLE; i++){
        tcolor = tcolor + texelFetch(SAMPLER, ivec2(tmp), i);
    }

    outColor = vec4(tcolor/NBSAMPLE);

