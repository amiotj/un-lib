#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform sampler2D $prefix_tex;
uniform vec2 $prefix_screensize;
uniform float $prefix_ratio = 1.0;

<VARIABLE_IN>

<VARIABLE_WS>

<VARIABLE_OUT>

<FUNCTION>

<OPERATION>
    vec2 $prefix_uv = vec2(gl_FragCoord.x/$prefix_screensize.x,1-(gl_FragCoord.y/$prefix_screensize.y));
    $produce = $method(texture($prefix_tex, $prefix_uv).rgba*$prefix_ratio,$produce);
