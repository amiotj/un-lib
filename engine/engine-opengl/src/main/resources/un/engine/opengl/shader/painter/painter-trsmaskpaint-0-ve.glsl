#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform mat3 MVP;
uniform mat3 MASKTRS;

<VARIABLE_OUT>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

const vec2[4] coords = vec2[4](vec2(1,0),vec2(0,0),vec2(0,1), vec2(1,1));


<OPERATION>
    vec3 coord = vec3(coords[gl_VertexID],1);
    gl_Position = vec4(MVP * coord,1);
    //gl_Position = vec4(coords[gl_VertexID],1);
    //position is [-1...+1] and there is no transform,
    //we convert it to UV coordinates here to avoid a vbo.
    outData.uv = (MASKTRS * coord).xy;
