#version 320
<STRUCTURE>

<LAYOUT>
layout(triangles_adjacency) in;
layout(triangle_strip, max_vertices = 12) out;

<UNIFORM>
uniform float borderWidth = 0.01;

<VARIABLE_IN>
vec4 position_model;
vec4 position_proj;
vec3 normal_model;
vec3 normal_proj;

<VARIABLE_OUT>
vec4 position_camera;
vec3 normal_model;

<FUNCTION>
bool cameraFront(){
    return inData[0].position_camera.z < 0 
        && inData[2].position_camera.z < 0
        && inData[1].position_camera.z < 0;
}

bool isFront(vec3 A, vec3 B, vec3 C){
    vec3 u = B - A;
    vec3 v = C - A;
    vec3 n = cross(u, v);
    return n.z < 0.0; //face camera
}

void emitVertice(vec4 proj){
    outData.normal_model = vec3(0,0,1);
    gl_Position = outData.position_proj; 
    EmitVertex();
}

void emitEdge(vec3 P0, vec3 P1){

    vec2 V = normalize(P1.xy - P0.xy);
    vec2 N = vec2(-V.y, V.x) * UNI_PX*borderWidth;

    emitVertice(vec4(P1.xy + N, P1.z, 1));
    emitVertice(vec4(P1.xy - N, P1.z, 1));
    emitVertice(vec4(P0.xy + N, P0.z, 1));
    emitVertice(vec4(P0.xy - N, P0.z, 1));
    EndPrimitive();
}

<OPERATION>
    vec3 v0 = inData[0].position_proj.xyz / inData[0].position_proj.w;
    vec3 v1 = inData[1].position_proj.xyz / inData[1].position_proj.w;
    vec3 v2 = inData[2].position_proj.xyz / inData[2].position_proj.w;
    vec3 v3 = inData[3].position_proj.xyz / inData[3].position_proj.w;
    vec3 v4 = inData[4].position_proj.xyz / inData[4].position_proj.w;
    vec3 v5 = inData[5].position_proj.xyz / inData[5].position_proj.w;

    bool t024 = isFront(inData[0].position_camera.xyz, inData[2].position_camera.xyz, inData[4].position_camera.xyz);
    bool t012 = isFront(inData[0].position_camera.xyz, inData[1].position_camera.xyz, inData[2].position_camera.xyz);
    bool t234 = isFront(inData[2].position_camera.xyz, inData[3].position_camera.xyz, inData[4].position_camera.xyz);
    bool t045 = isFront(inData[0].position_camera.xyz, inData[4].position_camera.xyz, inData[5].position_camera.xyz);

    if(t024 != t012) emitEdge(v0, v2);
    if(t024 != t234) emitEdge(v2, v4);
    if(t024 != t045) emitEdge(v4, v0);

