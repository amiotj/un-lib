#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>

<VARIABLE_OUT>
mat3 tangentMatrix;

<FUNCTION>

<OPERATION>

    vec3 bitangent = cross (l_normal, l_tangent.xyz) * l_tangent.w;
    outData.tangentMatrix = transpose(mat3(
        l_tangent.xyz,
        bitangent,
        l_normal
        )); 


