#version 400

<STRUCTURE>

<LAYOUT>
layout (triangles, equal_spacing, ccw) in;

<UNIFORM>

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<FUNCTION>
vec3 process(vec3 v0, vec3 v1, vec3 v2){
    return gl_TessCoord.x * v0
         + gl_TessCoord.y * v1
         + gl_TessCoord.z * v2;
}


<OPERATION>
    outData.position_model  = process(inData[0].position_model, inData[1].position_model,inData[2].position_model,1.0);
    outData.position_world  = M * outData.position_model;
    outData.position_camera = MV * outData.position_model;
    outData.position_proj   = MVP * outData.position_model;
    gl_Position = outData.position_proj;

    outData.normal_model  = process(inData[0].normal_model, inData[1].normal_model,inData[2].normal_model);
    outData.normal_world  = (M * vec4(outData.normal_model,0.0)).xyz;

