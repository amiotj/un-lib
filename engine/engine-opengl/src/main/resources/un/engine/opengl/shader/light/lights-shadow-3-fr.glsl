#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform int LIGHT_NB;
uniform vec4 LIGHT_AMBIENT;
uniform float CELLSHADINGSTEPS = 0.0;
uniform Light LIGHTS[5]; //fix size
uniform sampler2D SHADOWMAPS[5]; //fix size


<VARIABLE_WS>


<FUNCTION>
void recalculateNormals(){
    normal_model = normalize(normal_model);
    normal_world = normalize((M * vec4(normal_model,0)).xyz);
    normal_camera = normalize((MV * vec4(normal_model,0)).xyz);
    normal_proj = normalize((MVP * vec4(normal_model,0)).xyz);
}

float chebyshevUpperBound(vec2 moments, float t){  
    // One-tailed inequality valid if t > moments.x  
    float p = float(t <= moments.x);  
    // Compute variance.  
    float variance = moments.y - (moments.x*moments.x);  
    // variance = max(variance, g_MinVariance);
    // Compute probabilistic upper bound.  
    float d = t - moments.x;  
    float p_max = variance / (variance + d*d);  
    return max(p, p_max);  
}  

float shadowContribution(vec2 lightTexCoord, float distanceToLight, sampler2D shadowSampler){  
    // Read the moments from the variance shadow map.  
    vec2 moments = texture(shadowSampler, lightTexCoord).xy;  
    // Compute the Chebyshev upper bound.  
    return chebyshevUpperBound(moments, distanceToLight);  
}  

float calculateShadowFactor(sampler2D shadowMap){
    //convert fragment position to light space
    vec4 fragPosLightSpace = (fragLight.v * vec4 (inData.position_world.xyz, 1.0));
    vec4 fragPosLightProj = (fragLight.p * fragPosLightSpace);
    float distToLight = -fragPosLightSpace.z;

    //calculate shadow map coordinate
    vec4 shadowCoord = fragPosLightProj;
    shadowCoord.xyz /= shadowCoord.w;
    shadowCoord.xy += 1.0;
    shadowCoord.xy *= 0.5;

    return shadowContribution(shadowCoord.xy, distToLight, shadowMap);
}

float cellShading(float value){
    if(CELLSHADINGSTEPS>0){
        //cell shading, also called Toon shading
        value = int(value * CELLSHADINGSTEPS) / CELLSHADINGSTEPS;
    }
    return value;
}

float calcSpecularFactor(vec3 fragNormal, vec3 toLight, vec3 toEye){

    // Standard reflection method, slower but more accurate
    vec3 reflection = reflect(-toLight, fragNormal);
    float specularFactor = clamp(dot(reflection, toEye),0.0,1.0);
    specularFactor = pow(specularFactor, 5.0);

    // Blinn-Phong method, faster but less accurate
    //vec3 halfWay = normalize(toEye + toLight);
    //float specularFactor = max(dot(halfWay, fragNormal),0.0);
    //specularFactor = pow(specularFactor, material.shininess);

    if(CELLSHADINGSTEPS>0){
        //cell shading, also called Toon shading
        specularFactor = specularFactor < 0.5 ? 0.0 : 1.0;
    }
    
    return specularFactor;
}

void directionalLight(){
    vec3 fragToLightDirection = vec3(V * vec4(-fragLight.spotDirection.xyz, 0.0));

    float diffuseRatio = clamp(dot(fragToLightDirection,frag.normal), 0.0, 1.0);
    diffuseRatio = cellShading(diffuseRatio);
    frag.diffuse = (fragLight.diffuse * material.color) * diffuseRatio;

    float specularFactor = 0.0;
    if(diffuseRatio>0.0){
        specularFactor = calcSpecularFactor(frag.normal,fragToLightDirection,frag.eyeDirection);
    }
    frag.specular = (fragLight.specular * material.specular) * specularFactor;
}

void pointLight(){

    //convert light position to eye space
    vec3 lightCameraSpace = vec3( V * vec4(fragLight.position.xyz, 1.0) );

    // point light or spotlight (or other kind of light)
    vec3 fragToLightVector    = lightCameraSpace - inData.position_camera.xyz;
    float fragToLightDistance = length(fragToLightVector);
    vec3 fragToLightDirection = normalize(fragToLightVector);
    float attenuation = 1.0 / (fragLight.constantAttenuation
                       + fragLight.linearAttenuation    * fragToLightDistance
                       + fragLight.quadraticAttenuation * fragToLightDistance * fragToLightDistance);
    
    float diffuseRatio = max(dot(fragToLightDirection,frag.normal), 0.0);
    diffuseRatio = cellShading(diffuseRatio);
    frag.diffuse = (fragLight.diffuse * material.color) * diffuseRatio;

    float specularFactor = 0.0;
    if(diffuseRatio>0.0){
        specularFactor = calcSpecularFactor(frag.normal,fragToLightDirection,frag.eyeDirection);
    }
    frag.specular = (fragLight.specular * material.specular) * specularFactor;

    frag.diffuse  *= attenuation;
    frag.specular *= attenuation;
}

void spotLight(){
    //convert light position to eye space
    vec3 lightCameraSpace = vec3( V * vec4(fragLight.position.xyz, 1.0) );

    vec3 fragToLightVector    = lightCameraSpace - inData.position_camera.xyz;
    float fragToLightDistance = length(fragToLightVector);
    vec3 fragToLightDirection = normalize(fragToLightVector);
    float attenuation = 1.0 / (fragLight.constantAttenuation
                       + fragLight.linearAttenuation    * fragToLightDistance
                       + fragLight.quadraticAttenuation * fragToLightDistance * fragToLightDistance);

    vec3 spotDirection = vec3(V * vec4(fragLight.spotDirection.xyz, 0.0) );
    float clampedCosine = max(0.0, dot(-fragToLightDirection, normalize(spotDirection)));
    if (clampedCosine < cos(radians(fragLight.spotCutoff))) {
        // outside of spotlight cone
        attenuation = 0.0;
    } else {
        attenuation = attenuation * pow(clampedCosine, fragLight.spotExponent);
    }

    float diffuseRatio = max(dot(fragToLightDirection,frag.normal), 0.0);
    diffuseRatio = cellShading(diffuseRatio);
    frag.diffuse = (fragLight.diffuse * material.color) * diffuseRatio;

    float specularFactor = 0.0;
    if(diffuseRatio>0.0){
        specularFactor = calcSpecularFactor(frag.normal,fragToLightDirection,frag.eyeDirection);
    }
    frag.specular = (fragLight.specular * material.specular) * specularFactor;

    frag.diffuse  *= attenuation;
    frag.specular *= attenuation;
}

void applyShadow(int index){
    if(fragLight.hasShadowMap > 0){
        float shadowFactor = calculateShadowFactor(SHADOWMAPS[index]);
        frag.diffuse *= shadowFactor;
        frag.specular *= shadowFactor;
    }
}


<OPERATION>

    if(normal_model.x == 99){
        normal_model = normalize(inData.normal_model.xyz);
    }
    recalculateNormals();

    float alpha = material.color.w;

    //skip transparent and nearly transparent fragments
    if(alpha < 0.01) discard;

    if(material.ambient.x < 0){
        //use diffuse as ambient
        material.ambient = material.color;
    }
    if(material.specular.x < 0){
        //use diffuse as specular
        material.specular = material.color;
    }

    //frag.eyeDirection = -normalize((V* vec4(inData.position_world.xyz,0)).xyz);
    frag.eyeDirection = normalize(-inData.position_camera.xyz);
    frag.normal = normal_camera;

    vec3 totalAmbient = vec3(0);
    vec3 totalSpecular = vec3(0);
    vec3 totalDiffuse = vec3(0);

    // initialize total lighting with ambient lighting
    totalAmbient = vec3(LIGHT_AMBIENT) * vec3(material.ambient*material.color);

    for (int index = 0; index < LIGHT_NB; index++) {
        fragLight = LIGHTS[index];

        //calculate light/material result diffuse and specular
        if (0.0 == LIGHTS[index].position.w) {
            // directional
            directionalLight();
        } else if (LIGHTS[index].spotCutoff <= 90.0) {
            // spotlight
            spotLight();
        } else {
            // point light
            pointLight();
        }

        //apply shadow factor
        //applyShadow(index);

        totalSpecular += frag.specular.xyz;
        totalDiffuse  += frag.diffuse.xyz;
    }

    material.color = vec4(totalAmbient + totalDiffuse + totalSpecular, alpha);

    //TODO put this in a different shader : gamma correction
    //vec3 gm = vec3(1.0/2.2);
    //material.color = vec4(pow(material.color.xyz, gm), material.color.a);