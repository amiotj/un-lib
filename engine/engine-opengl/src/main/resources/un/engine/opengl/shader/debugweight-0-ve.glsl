#version 330

<STRUCTURE>

<LAYOUT>

<UNIFORM>
uniform int debugJointIndex;

<VARIABLE_OUT>
vec4 debugweight;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>

    outData.debugweight = vec4(0.6,0.6,0.6,0.6);
    for(int i=0;i<NBJOINT;i++){
        if(l_jointindex[i]==debugJointIndex){
            outData.debugweight = vec4(l_jointweight[i], 0.0, 1.0-l_jointweight[i], 1.0);
        }
    }
