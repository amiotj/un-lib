#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform mat4 V;
uniform int LIGHT_NB;
uniform vec4 LIGHT_AMBIENT;
uniform float CELLSHADINGSTEPS = 0.0;
uniform Light LIGHTS[5]; //fix size

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>

    vec4 diffuse = texture(sampler0,inData.uv);
    vec4 ambient = vec4(1);
    vec4 specular = texture(sampler1,inData.uv);
    float shininess = 1.0;

    vec3 position_camera = vec3(V * vec4(texture(sampler2,inData.uv).xyz,1));
    vec3 normal_camera = vec3(V * vec4(texture(sampler3,inData.uv).xyz,0));

    float alpha = diffuse.w;

    //skip transparent and nearly transparent fragments
    if(alpha < 0.01) discard;

    vec3 fragToEyeDirection = -normalize(position_camera);
    vec3 fragNormal = normal_camera;
    vec3 fragToLightDirection;
    float attenuation;

    vec3 totalAmbient = vec3(0);
    vec3 totalSpecular = vec3(0);
    vec3 totalDiffuse = vec3(0);

    // initialize total lighting with ambient lighting
    totalAmbient = vec3(LIGHT_AMBIENT) * vec3(ambient*diffuse);

    for (int index = 0; index < LIGHT_NB; index++) {

        if (0.0 == LIGHTS[index].position.w) {
            // directional light
            attenuation = 1.0; // no attenuation
            fragToLightDirection = normalize(LIGHTS[index].position.xyz);
        } else {

            //convert light position to eye space
            vec3 lightCameraSpace = vec3( V * vec4(LIGHTS[index].position.xyz, 1.0) );

            // point light or spotlight (or other kind of light)
            vec3 fragToLightVector    = lightCameraSpace - position_camera;
            float fragToLightDistance = length(fragToLightVector);
            fragToLightDirection = normalize(fragToLightVector);
            attenuation = 1.0 / (LIGHTS[index].constantAttenuation
                               + LIGHTS[index].linearAttenuation    * fragToLightDistance
                               + LIGHTS[index].quadraticAttenuation * fragToLightDistance * fragToLightDistance);

            if (LIGHTS[index].spotCutoff <= 90.0) {
                // spotlight
                vec3 spotDirection = vec3(V * vec4(LIGHTS[index].spotDirection.xyz, 0.0) );
                float clampedCosine = max(0.0, dot(-fragToLightDirection, normalize(spotDirection)));
                if (clampedCosine < cos(radians(LIGHTS[index].spotCutoff))) {
                    // outside of spotlight cone
                    attenuation = 0.0;
                } else {
                    attenuation = attenuation * pow(clampedCosine, LIGHTS[index].spotExponent);
                }
            }
        }

        float diffuseRatio = max(dot(fragToLightDirection,fragNormal), 0.0);
        if(CELLSHADINGSTEPS>0){
            //cell shading, also called Toon shading
            diffuseRatio = int(diffuseRatio * CELLSHADINGSTEPS) / CELLSHADINGSTEPS;
        }

        vec3 diffuseReflection = attenuation
            * vec3(LIGHTS[index].diffuse) * vec3(diffuse)
            * diffuseRatio;

        // Standard reflection method, slower but more accurate
        vec3 reflection = reflect(-fragToLightDirection, fragNormal);
        float specularFactor = max(dot(reflection, fragToEyeDirection),0.0);
        specularFactor = pow(specularFactor, shininess);

        // Blinn-Phong method, faster but less accurate
        //vec3 halfWay = normalize(fragToEyeDirection + fragToLightDirection);
        //float specularFactor = max(dot(halfWay, fragNormal),0.0);
        //specularFactor = pow(specularFactor, shininess);

        if(CELLSHADINGSTEPS>0){
            //cell shading, also called Toon shading
            specularFactor = specularFactor < 0.5 ? 0.0 : 1.0;
        }

        vec3 specularReflection = attenuation 
            * vec3(LIGHTS[index].specular) * vec3(specular)
            * specularFactor;

        totalSpecular += specularReflection;
        totalDiffuse  += diffuseReflection;
    }

    outColor = vec4(totalAmbient + totalDiffuse + totalSpecular, alpha);
