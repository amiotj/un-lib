#version 400

<STRUCTURE>
struct PPatch {
    float ij;
    float jk;
    float ik;
};

<LAYOUT>
layout(triangles, equal_spacing, ccw) in;

<UNIFORM>

<VARIABLE_IN>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;
PPatch opatch;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_camera;
vec4 position_proj;
vec3 normal_model;
vec3 normal_world;

<VARIABLE_WS>


<FUNCTION>
vec3 process(vec3 v0, vec3 v1, vec3 v2){
    return gl_TessCoord.x * v0
         + gl_TessCoord.y * v1
         + gl_TessCoord.z * v2;
}

<OPERATION>

    vec3 ij = vec3(inData[0].opatch.ij, inData[1].opatch.ij, inData[2].opatch.ij);
    vec3 jk = vec3(inData[0].opatch.jk, inData[1].opatch.jk, inData[2].opatch.jk);
    vec3 ik = vec3(inData[0].opatch.ik, inData[1].opatch.ik, inData[2].opatch.ik);

    vec3 square = gl_TessCoord * gl_TessCoord;
    vec3 phongPos   = square.x * inData[0].position_model.xyz
                    + square.y * inData[1].position_model.xyz
                    + square.z * inData[2].position_model.xyz
                    + gl_TessCoord.x * gl_TessCoord.y * ij
                    + gl_TessCoord.y * gl_TessCoord.z * jk
                    + gl_TessCoord.x * gl_TessCoord.z * ik;

    outData.position_model  = vec4(phongPos,1.0);
    outData.position_world  = M * outData.position_model;
    outData.position_camera = MV * outData.position_model;
    outData.position_proj   = MVP * outData.position_model;
    gl_Position = outData.position_proj;

    vec3 n = process(inData[0].normal_model, inData[1].normal_model,inData[2].normal_model);
    outData.normal_model  = n;
    outData.normal_world  = (M * vec4(n,0.0)).xyz;
