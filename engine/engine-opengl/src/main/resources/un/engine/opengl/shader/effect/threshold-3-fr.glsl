#version 330

<STRUCTURE>

<LAYOUT>
layout(location=0) out vec4 outColor;

<UNIFORM>
uniform vec3 THRESHOLD;

<VARIABLE_IN>
vec2 uv;

<VARIABLE_WS>

<FUNCTION>

<OPERATION>
    outColor = texture(sampler0, inData.uv);
    outColor.rgb -= THRESHOLD;