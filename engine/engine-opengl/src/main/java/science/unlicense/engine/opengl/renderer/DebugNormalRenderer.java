

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.Actors;

/**
 * Display the normals stored in the mesh shell.
 * 
 * @author Johann Sorel
 */
public class DebugNormalRenderer extends AbstractRenderer {

    private static final ShaderTemplate NORMAL_GE;
    private static final ShaderTemplate NORMAL_FR;
    static {
        try{
            NORMAL_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugnormal-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
            NORMAL_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugnormal-4-fr.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private float normalLength = 1.0f;
    private ActorProgram program;
    private Uniform uniLength;

    public DebugNormalRenderer() {
    }

    public DebugNormalRenderer(float normalLength) {
        this.normalLength = normalLength;
    }

    public float getNormalLength() {
        return normalLength;
    }

    public void setNormalLength(float normalLength) {
        this.normalLength = normalLength;
    }
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        final Mesh mesh = (Mesh) node;

        if(program==null){
            program = new ActorProgram();
            
            //build shell actor
            final Shape shape = mesh.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(mesh, shape);
            program.setExecutor(shellActor);

            final DefaultActor defaultActor = new DefaultActor(new Chars("DebugNormal"),false,
                    null, null, null, NORMAL_GE, NORMAL_FR, true, false){
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniLength.setFloat(context.getGL().asGL2ES2(), normalLength);
                    }
                };

            program.getActors().add(shellActor);
            program.getActors().add(defaultActor);
            program.compile(context);
            uniLength = program.getUniform(new Chars("normalLength"));
        }

        program.render(context, camera, mesh);

    }
    
    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
        }
        uniLength = null;
    }
}
