
package science.unlicense.engine.opengl.scenegraph.s2d;

import science.unlicense.api.image.Image;
import science.unlicense.api.scenegraph.s2d.ImageNode2D;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class GLImageNode2D extends ImageNode2D{

    private Texture2D texture;
    
    public void setImage(Image image) {
        if(this.image == image) return;
        super.setImage(image);
        if(texture!=null){
            this.texture.setImage(image);
        }
    }

    @Override
    public Image getImage() {
        return getTexture().getImage();
    }

    public Texture2D getTexture() {
        if(texture==null && image!=null){
            texture = new Texture2D(image);
            texture.setForgetOnLoad(false);
        }
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

}
