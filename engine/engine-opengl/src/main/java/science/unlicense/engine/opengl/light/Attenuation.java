
package science.unlicense.engine.opengl.light;

/**
 * Light attenuation.
 * 
 * Collada 1.5 :
 * The <constant_attenuation>, <linear_attenuation>, and <quadratic_attenuation> are
 * used to calculate the total attenuation of this light given a distance. 
 * 
 * The equation used is :
 * A = constant_attenuation + ( Dist * linear_attenuation ) + (( Dist^2 ) * quadratic_attenuation )
 * 
 * @author Johann Sorel
 */
public final class Attenuation {
    
    private float constant;
    private float linear;
    private float quadratic;

    public Attenuation() {
        constant = 1;
        linear = 0;
        quadratic = 0;
    }

    public Attenuation(float constant, float linear, float quadratic) {
        this.constant = constant;
        this.linear = linear;
        this.quadratic = quadratic;
    }

    public float getConstant() {
        return constant;
    }
    
    public void setConstant(float constant) {
        this.constant = constant;
    }

    public float getLinear() {
        return linear;
    }

    public void setLinear(float linear) {
        this.linear = linear;
    }

    public float getquadratic() {
        return quadratic;
    }

    public void setQuadratic(float exp) {
        this.quadratic = exp;
    }
        
}
