

package science.unlicense.engine.opengl.control;

import science.unlicense.api.game.navmap.NavigationMap;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Vector;

/**
 * Enforce player navigation map.
 * 
 * @author Johann Sorel
 */
public class PlayerNavigationMapConstraint  implements Updater {
 
    private final NavigationMap navMap;
    private final GLNode node;
    private final Vector lastPosition = new Vector(3);

    public PlayerNavigationMapConstraint(NavigationMap navMap) {
        this(navMap,null);
    }

    public PlayerNavigationMapConstraint(NavigationMap navMap, GLNode node) {
        this.navMap = navMap;
        this.node = node;
    }

    public synchronized void update(RenderContext context, GLNode node) {
        if(this.node!=null) node = this.node;
        final Vector position = new Vector(node.getNodeTransform().getTranslation());
        final Vector buffer = new Vector(3);
        
        navMap.navigate(lastPosition, position.subtract(lastPosition), buffer);
        lastPosition.set(buffer);
        if(!buffer.equals(position)){
            node.getNodeTransform().getTranslation().set(buffer);
            node.getNodeTransform().notifyChanged();
        }
    }
    
}
