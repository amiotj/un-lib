
package science.unlicense.engine.opengl.painter.gl3.task;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.engine.opengl.scenegraph.s2d.GLGeometry2D;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;

/**
 * TODO review this API design.
 * Not satisfying.
 *
 * @author Johann Sorel
 */
public class FillGeometry2 extends PainterTask{

    private final GLGeometry2D geom;

    private final Object paint;
    private final Matrix3x3 mv;
    private final AlphaBlending blending;

    
    public FillGeometry2(GLGeometry2D geom, Object paint, Matrix3x3 mv, AlphaBlending blending) {
        this.geom = geom;
        this.paint = paint;
        this.mv = mv;
        this.blending = blending;
    }

    public void execute(GL3Painter2D worker) {

        final Color color;
        if(paint instanceof ColorPaint){
            color = ((ColorPaint)paint).getColor();
        }else if(paint instanceof Color){
            color = (Color) paint;
        }else{
            color = Color.BLUE;
        }

        //clean the mask area
        final BBox bboxModel = geom.getGeometry().getBoundingBox();
        final BBox bboxProj = new BBox(bboxModel);
        Geometries.transform(bboxProj, mv, bboxProj);
        final Clear clear = new Clear(bboxProj, true);
        clear.execute(worker);

        
        //render the mask
        geom.calculateMask();
        final VBO maskVBO = geom.getMaskVBO();
        maskVBO.loadOnGpuMemory(worker.gl);
        maskVBO.unbind(worker.gl);

        worker.mask.loadOnGpuMemory(worker.gl);
        worker.mask.bind(worker.gl);

        worker.gl.glDisable(GL_DEPTH_TEST);
        worker.gl.glDisable(GL_BLEND);
        worker.gl.glEnable(GLC.GETSET.State.COLOR_LOGIC_OP);
        worker.gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);
        worker.programs.createMaskProg.loadOnGpuMemory(worker.gl);
        worker.programs.createMaskProg.enable(worker.gl);


        final VertexAttribute va1 = worker.programs.createMaskProg.getVertexAttribute(new Chars("l_position1"), worker.gl, 1);
        final VertexAttribute va2 = worker.programs.createMaskProg.getVertexAttribute(new Chars("l_position2"), worker.gl, 1);
        final VertexAttribute va3 = worker.programs.createMaskProg.getVertexAttribute(new Chars("l_position3"), worker.gl, 1);
        final VertexAttribute va4 = worker.programs.createMaskProg.getVertexAttribute(new Chars("l_position4"), worker.gl, 1);
        final VertexAttribute va5 = worker.programs.createMaskProg.getVertexAttribute(new Chars("l_position5"), worker.gl, 1);
        va1.enable(worker.gl, maskVBO, 0*3*4);
        va2.enable(worker.gl, maskVBO, 1*3*4);
        va3.enable(worker.gl, maskVBO, 2*3*4);
        va4.enable(worker.gl, maskVBO, 3*3*4);
        va5.enable(worker.gl, maskVBO, 4*3*4);
        worker.programs.createMaskProg.getUniform(new Chars("SIZE")).setVec2(worker.gl, new float[]{worker.mask.getWidth(),worker.mask.getHeight()});
        worker.programs.createMaskProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
        worker.programs.createMaskProg.uniformP.setMat3(worker.gl, worker.pArray);

        worker.gl.glDrawArrays(GL_TRIANGLES,0,maskVBO.getTupleCount());
        GLUtilities.checkGLErrorsFail(worker.gl);

        worker.programs.createMaskProg.va.disable(worker.gl);
        worker.programs.createMaskProg.disable(worker.gl);
        maskVBO.unbind(worker.gl);

        //render using the mask
        worker.fbo.bind(worker.gl);
        worker.gl.glDisable(GLC.GETSET.State.COLOR_LOGIC_OP);
        configureBlending(worker,blending);
        worker.programs.fillMaskProg.enable(worker.gl);
//        worker.programs.fillMaskProg.uniformMV.setMat3(worker.gl, mv.toArrayFloat());
//        worker.programs.fillMaskProg.uniformP.setMat3(worker.gl, worker.pArray);
//        worker.programs.fillMaskProg.uniformRECT.setVec4(worker.gl, new float[]{
//            (float)bboxModel.getMin(0), (float)bboxModel.getMin(1),
//            (float)bboxModel.getSpan(0), (float)bboxModel.getSpan(1)});
        worker.programs.fillMaskProg.uniformCOLOR.setVec4(worker.gl, color.toRGBAPreMul());

        //bind texture and sampler
        final Texture texture = worker.mask.getColorTexture();
        texture.loadOnSystemMemory(worker.gl);

        final int[] reservedTexture = new int[]{33984,0};
        worker.gl.glActiveTexture(reservedTexture[0]);
        texture.bind(worker.gl);
        worker.programs.fillMaskProg.uniformMASK.setInt(worker.gl, reservedTexture[1]);
        GLUtilities.checkGLErrorsFail(worker.gl);

        worker.gl.glDrawArrays(GL_TRIANGLES,0,6);
        GLUtilities.checkGLErrorsFail(worker.gl);

        //release sampler
        worker.gl.glActiveTexture(reservedTexture[0]);
        texture.unbind(worker.gl);

        worker.programs.fillMaskProg.disable(worker.gl);

    }

}
