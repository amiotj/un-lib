
package science.unlicense.engine.opengl.tessellation;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.renderer.actor.AbstractActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;

/**
 * Subdivides the mesh triangle, keeping them on the triangle surface.
 *
 * @author Johann Sorel
 */
public class FlatTessellator extends AbstractActor implements Tessellator{

    public static final Chars TYPE_VEC2 = new Chars("vec2");
    public static final Chars TYPE_VEC3 = new Chars("vec3");
    public static final Chars TYPE_VEC4 = new Chars("vec4");

    private static final Chars PREFIX_OUT = new Chars("outData[gl_InvocationID].");
    private static final Chars PREFIX_IN = new Chars("inData[gl_InvocationID].");
    private static final Chars EQUAL = new Chars(" = ");

    static final ShaderTemplate SHADER_TC;
    static final ShaderTemplate SHADER_TE;
    static {
        try{
            SHADER_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessflat-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            SHADER_TE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessflat-2-te.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private int innerFactor;
    private int outterFactor;
    private boolean dirty = true;

    private final Chars[] transferValues;
    private final Chars[] transferTypes;

    /**
     *
     * @param innerFactor
     * @param outterFactor
     */
    public FlatTessellator(int innerFactor, int outterFactor) {
        super(null);
        this.innerFactor = innerFactor;
        this.outterFactor = outterFactor;
        this.transferValues = new Chars[0];
        this.transferTypes = new Chars[0];
    }

    /**
     *
     * @param innerFactor
     * @param outterFactor
     * @param transferValues
     * @param transferTypes
     */
    public FlatTessellator(int innerFactor, int outterFactor, Chars[] transferValues, Chars[] transferTypes) {
        super(null);
        this.innerFactor = innerFactor;
        this.outterFactor = outterFactor;
        this.transferValues = transferValues;
        this.transferTypes = transferTypes;
    }

    public int getMinGLSLVersion() {
        return Maths.max(
                SHADER_TC.getMinGLSLVersion(), 
                SHADER_TE.getMinGLSLVersion());
    }

    public int getInnerFactor() {
        return innerFactor;
    }

    public void setInnerFactor(int innerFactor) {
        this.innerFactor = innerFactor;
    }

    public int getOutterFactor() {
        return outterFactor;
    }

    public void setOutterFactor(int outterFactor) {
        this.outterFactor = outterFactor;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    public boolean usesGeometryShader() {
        return false;
    }

    /**
     * {@inheritDoc }
     */
    public boolean usesTesselationShader() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate tessControlShader = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate tessEvalShader = template.getTesselationEvalShaderTemplate();
        dirty = false;


        for(int i=0;i<transferValues.length;i++){
            //set in and out values
            final Chars var = transferTypes[i].concat(' ').concat(transferValues[i]).concat(';');
            tessControlShader.addVariableIn(var);
            tessControlShader.addVariableOut(var);
            //pass value from in to out without modification
            tessControlShader.addOperation(
                    PREFIX_OUT.concat(transferValues[i]).concat(EQUAL).concat(
                    PREFIX_IN).concat(transferValues[i]).concat(';'));
        }
        
        tessControlShader.append(SHADER_TC);
        tessEvalShader.append(SHADER_TE);
    }

    /**
     * {@inheritDoc }
     */
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Uniform infactor = program.getUniform(new Chars("innerfactor"));
        final Uniform outfactor = program.getUniform(new Chars("outterfactor"));
        infactor.setInt(gl, innerFactor);
        outfactor.setInt(gl, innerFactor);
    }

}
