
package science.unlicense.engine.opengl.light;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.impl.math.Vector;

/**
 * Projective light are similar to directional light but have an origin in space.
 * 
 * 
 * @author Johann Sorel
 */
public class ProjectiveLight extends Light{
    
    public ProjectiveLight() {
        this(Color.WHITE,Color.WHITE);
    }

    public ProjectiveLight(Color diffuse, Color specular) {
        super(diffuse,specular);
        setName(new Chars("Directional light"));
    }

    public Tuple getWorldSpaceDirection() {
        final Affine rootToNode = getNodeToRootSpace();
        Vector forward = (Vector) rootToNode.toMatrix().transform(new Vector(0, 0, -1, 0), new Vector(4));
        return forward.getXYZ().localNormalize();
    }

    /**
     * Create the directional light best camera to produce a shadowmap.
     *
     * @param camera
     * @return
     */
    public CameraMono createFittingCamera(CameraMono camera){

        //get frustrum corners
        final Vector[] corners = camera.calculateFrustrumCorners();

        //convert them to light space
        final Affine vm = calculateNodeToView(camera).invert();
        for(int i=0;i<8;i++){
            vm.transform(corners[i],corners[i]);
        }

        //get boundingbox
        final BBox bbox = new BBox(corners[0].copy(),corners[0].copy());
        for(int i=1;i<8;i++){
            bbox.expand(corners[i]);
        }

        final CameraMono lightCamera = new CameraMono();
        lightCamera.setNearPlane(0.1);
        lightCamera.setFarPlane(10000);
        lightCamera.setFieldOfView(60);
        
        return lightCamera;
    }

}
