
package science.unlicense.engine.opengl.scenegraph.s2d;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.scenegraph.s2d.TextNode2D;

/**
 *
 * @author Johann Sorel
 */
public class GLTextNode2D extends TextNode2D{

    @Override
    public void setText(CharArray text) {
        super.setText(text);
    }

}
