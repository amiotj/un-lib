
package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.renderer.actor.Actors;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.renderer.actor.MorphActor;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 *
 * @author Johann Sorel
 */
public class MeshRenderer extends AbstractRenderer {

    private final Mesh mesh;
    private ActorProgram program;
    //complementary shader actors
    private final Sequence extShaderActors = new ArraySequence();
    
    protected boolean useLights = false;

    public MeshRenderer(Mesh mesh) {
        this.mesh = mesh;

        state.setEnable(GLC.GETSET.State.DITHER, true);
        state.setEnable(GLC.GETSET.State.MULTISAMPLE, true);
        state.setEnable(GLC.GETSET.State.SAMPLE_ALPHA_TO_COVERAGE, true);
        state.setEnable(GLC.GETSET.State.DEPTH_TEST, true);
        state.setEnable(GLC.GETSET.State.BLEND, true);
        state.setDepthMask(true);
        state.setDepthFunc(GL_LESS);
    }
    
    public Sequence getExtShaderActors() {
        return extShaderActors;
    }
    
    public void setUseLights(boolean useLights) {
        this.useLights = useLights;
    }

    public boolean isUseLights() {
        return useLights;
    }
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        if(program == null){
            buildRenderer(mesh, context);
        }
        if(!program.isOnGpuMemory()){
            program.compile(context);
        }
        program.render(context, camera, mesh);
    }
    
    private void buildRenderer(Mesh mesh, RenderContext ctx) {
        
        program = new ActorProgram();
        final Sequence actors = program.getActors();

        //build shell actor
        final Shape shape = mesh.getShape();
        final ActorExecutor executor = Actors.buildExecutor(mesh, shape);
        actors.add(executor);
        if(shape instanceof Shell){
            final MorphSet ms = ((Shell)shape).getMorphs();
            if(ms!=null){
                actors.add(new MorphActor(ms));
            }
        }

        if(mesh.getTessellator()!=null) actors.add(mesh.getTessellator());
        if(mesh.getMaterial()!=null) actors.add(new MaterialActor(mesh.getMaterial(),useLights));
        //check additional actors
        actors.addAll(extShaderActors);
                
        program.compile(ctx);
        program.setExecutor(executor);
    }
    
    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
            program = null;
        }
    }
    
}
