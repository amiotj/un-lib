
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.math.Affine;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 *
 * @author Johann Sorel
 */
public class GL2ES2DebugSkeletonRenderer extends AbstractRenderer {

    private boolean viewBody = true;
    private boolean viewBodyVelocity = true;
    private boolean viewBodyForce = true;

    public GL2ES2DebugSkeletonRenderer() {
    }

    public boolean isBodyVelocityVisible() {
        return viewBodyVelocity;
    }

    public void setBodyVisible(boolean visible) {
        this.viewBody = visible;
    }

    public boolean isBodyVisible() {
        return viewBody;
    }

    public void setBodyVelocityVisible(boolean visible) {
        this.viewBodyVelocity = visible;
    }

    public void setBodyForceVisible(boolean visible) {
        this.viewBodyForce = visible;
    }

    public boolean isBodyForceVisible() {
        return viewBodyForce;
    }

    @Override
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        if(!(node instanceof MultipartMesh)) return;

        final Skeleton skeleton = ((MultipartMesh)node).getSkeleton();
        final Sequence joints = skeleton.getAllJoints();


        final GeometryMesh m = new GeometryMesh(new BBox(new double[]{-0.1,-0.1,-0.1}, new double[]{0.1,0.1,0.1}));
        m.getMaterial().setDiffuse(Color.RED);
        final MeshRenderer renderer = (MeshRenderer) m.getRenderers().get(0);
        renderer.getState().setEnable(GLC.GETSET.State.DEPTH_TEST, false);
        renderer.getState().setEnable(GLC.GETSET.State.CULL_FACE, false);
        renderer.getState().setDepthMask(false);
        renderer.getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        
        for(int i=0,n=joints.getSize();i<n;i++){
            final Joint joint = (Joint) joints.get(i);

            Affine trs = joint.getNodeToRootSpace();

            m.getNodeTransform().set(trs);
            renderer.render(context, camera, node);
        }
        m.dispose(context);

    }

    @Override
    public void dispose(GLProcessContext context) {

    }

}
