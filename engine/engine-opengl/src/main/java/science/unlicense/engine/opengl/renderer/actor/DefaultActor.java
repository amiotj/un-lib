

package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.engine.opengl.shader.ShaderTemplate;

/**
 * Fixed shader actor with predefined templates.
 *
 * @author Johann Sorel
 */
public class DefaultActor extends AbstractActor{

    protected final ShaderProgramTemplate programTemplate;
    protected final boolean tessOptional;
    protected final boolean geomOptional;

    public DefaultActor(Chars reuseId,boolean concurrent,Chars vs,Chars tc,Chars te,Chars gs,Chars fr,
            boolean tessOptional,boolean geomOptional) throws IOException {
        super(reuseId,concurrent);
        this.programTemplate   = new ShaderProgramTemplate();
        if(vs!=null) programTemplate.getVertexShaderTemplate().loadFromChars(vs, ShaderTemplate.SHADER_VERTEX);
        if(tc!=null) programTemplate.getTessaltionControlShaderTemplate().loadFromChars(tc, ShaderTemplate.SHADER_TESS_CONTROL);
        if(te!=null) programTemplate.getTesselationEvalShaderTemplate().loadFromChars(te, ShaderTemplate.SHADER_TESS_EVAL);
        if(gs!=null) programTemplate.getGeometryShaderTemplate().loadFromChars(gs, ShaderTemplate.SHADER_GEOMETRY);
        if(fr!=null) programTemplate.getFragmentShaderTemplate().loadFromChars(fr, ShaderTemplate.SHADER_FRAGMENT);

        this.tessOptional = tessOptional;
        this.geomOptional = geomOptional;
    }

    public DefaultActor(Chars reuseId,boolean concurrent,Path program,boolean tessOptional, boolean geomOptional) throws IOException {
        super(reuseId,concurrent);
        this.programTemplate = ShaderProgramTemplate.load(program);
        this.tessOptional = tessOptional;
        this.geomOptional = geomOptional;
    }
    public DefaultActor(Chars reuseId,boolean concurrent,ShaderProgramTemplate template,
            boolean tessOptional,boolean geomOptional) throws IOException {
        super(reuseId,concurrent);
        this.programTemplate = template;
        this.tessOptional = tessOptional;
        this.geomOptional = geomOptional;
    }

    public DefaultActor(Chars reuseId,boolean concurrent,Path vs,Path tc,Path te,Path gs,Path fr,
            boolean tessOptional, boolean geomOptional) throws IOException {
        this(   reuseId,concurrent,
                vs!=null ? ShaderTemplate.create(vs,ShaderTemplate.SHADER_VERTEX) : null,
                tc!=null ? ShaderTemplate.create(tc,ShaderTemplate.SHADER_TESS_CONTROL) : null,
                te!=null ? ShaderTemplate.create(te,ShaderTemplate.SHADER_TESS_EVAL) : null,
                gs!=null ? ShaderTemplate.create(gs,ShaderTemplate.SHADER_GEOMETRY) : null,
                fr!=null ? ShaderTemplate.create(fr,ShaderTemplate.SHADER_FRAGMENT) : null,
                tessOptional,
                geomOptional);
    }

    public DefaultActor(
            Chars reuseId,
            boolean concurrent,
            ShaderTemplate vs,
            ShaderTemplate tc,
            ShaderTemplate te,
            ShaderTemplate gs,
            ShaderTemplate fr,
            boolean tessOptional,
            boolean geomOptional) {
        super(reuseId,concurrent);
        this.programTemplate = new ShaderProgramTemplate(vs,tc,te,gs,fr);
        this.tessOptional = tessOptional;
        this.geomOptional = geomOptional;
    }

    /**
     * {@inheritDoc }
     */
    public int getMinGLSLVersion() {
        return programTemplate.getMinGLSLVersion();
    }

    /**
     * {@inheritDoc }
     */
    public boolean usesTesselationShader() {
        return !tessOptional;
    }

    /**
     * {@inheritDoc }
     */
    public boolean usesGeometryShader() {
        return !geomOptional;
    }

    /**
     * {@inheritDoc }
     */
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        template.append(this.programTemplate,tess,geom);
    }

}
