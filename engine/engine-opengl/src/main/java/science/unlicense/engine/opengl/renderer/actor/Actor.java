package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;

/**
 * A shader actor is an object acting is the rendering process. It it taking
 * part in the shader construction process and rendering.
 *
 * @author Johann Sorel
 */
public interface Actor {

    //shader properties name conventions
    public static final Chars INDATA = new Chars("inData.");
    public static final Chars OUTDATA = new Chars("outData.");
    public static final Chars CV_WORK_VERTEX_MODEL  = new Chars("w_position_model");
    public static final Chars CV_VERTEX_MODEL  = new Chars("position_model");
    public static final Chars CV_VERTEX_WORLD  = new Chars("position_world");
    public static final Chars CV_VERTEX_CAMERA = new Chars("position_camera");
    public static final Chars CV_VERTEX_PROJ   = new Chars("position_proj");
    public static final Chars CV_NORMAL_MODEL  = new Chars("normal_model");
    public static final Chars CV_NORMAL_WORLD  = new Chars("normal_world");
    public static final Chars CV_NORMAL_CAMERA = new Chars("normal_camera");
    public static final Chars CV_NORMAL_PROJ   = new Chars("normal_proj");
    public static final Chars CV_UV            = new Chars("uv");
    public static final Chars CV_TANGENT_MODEL  = new Chars("tangent_model");
    public static final Chars CV_TANGENT_WORLD  = new Chars("tangent_world");
    public static final Chars CV_TANGENT_CAMERA = new Chars("tangent_camera");
    public static final Chars CV_TANGENT_PROJ   = new Chars("tangent_proj");
    
    /**
     * Shader actor GLSL program parts can often be reused by multiple
     * meshes. this method provide a way to identify such shader actors
     * to allow reusing shader programs (associations of compiled shader actors).
     * @return unique id or null if it can not be reused
     */
    Chars getReuseUID();
    
    /**
     * Get the minimum GLSL version this actor needs.
     * Value is expected to be the same as the first line in a GLSL program.
     * Example : #110, #300, #400
     * 
     * @return minimum required version of GLSL
     */
    int getMinGLSLVersion();
    
    /**
     * Indicate if the actor needs to update it's resources.
     * @return true if is dirty.
     */
    boolean isDirty();

    /**
     * Indicate if this actor will use tessalation shaders.
     * @return true if uses tesselation.
     */
    boolean usesTesselationShader();

    /**
     * Indicate if this actor will use geometry shader.
     * @return true if uses geometry shader.
     */
    boolean usesGeometryShader();

    /**
     * Called to initialize the shader program.
     *
     * @param context
     * @param template
     * @param tess
     * @param geom
     */
    void initProgram(final RenderContext context,ShaderProgramTemplate template, boolean tess, boolean geom);

    /**
     * Called before the program is enabled.
     * 
     * @param context
     * @param candidate 
     */
    void preExecutionGL(RenderContext context, Object candidate);

    /**
     * Called before rendering the mesh.
     * The program is already enabled at this state.
     * Appropriate time to bind resources.
     *
     * @param context
     * @param program
     */
    void preDrawGL(RenderContext context, ActorProgram program);

    /**
     * Called after rendering the mesh.
     * Appropriate time to unbind resources.
     *
     * @param context
     * @param program
     */
    void postDrawGL(RenderContext context, ActorProgram program);

    /**
     * Clear used resources on the gpu.
     * @param context
     */
    void dispose(GLProcessContext context);
}
