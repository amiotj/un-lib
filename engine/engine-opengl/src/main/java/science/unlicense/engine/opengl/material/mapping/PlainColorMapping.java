
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.api.color.Color;

/**
 * Single color mapping.
 * 
 * @author Johann Sorel
 */
public final class PlainColorMapping implements Mapping{

    private Color color;    
    private boolean dirty = true;

    /**
     * Default color mapping.
     * With a red color for all vertices.
     */
    public PlainColorMapping() {
        this(Color.RED);
    }

    public PlainColorMapping(Color color) {
        setColor(color);
    }

    public boolean isOpaque(){
        return color.getAlpha() >= 1f;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color){
        this.color = color;
    }

    public void dispose(GLProcessContext context) {
        
    }

}
