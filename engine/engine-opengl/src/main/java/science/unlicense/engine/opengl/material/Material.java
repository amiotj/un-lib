
package science.unlicense.engine.opengl.material;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.CubeMapping;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.api.color.Color;

/**
 * A material describe a stack of layers providing cumulative informations
 * on diffuse, ambient, specular and normal mapping.
 *
 * @author Johann Sorel
 */
public class Material {

    private final Sequence layers = new ArraySequence();

    private CharArray name = Chars.EMPTY;
    private Color ambient = Color.WHITE;
    private Color diffuse = Color.TRS_BLACK;
    private Color specular = Color.BLACK;
    private float shininess = 5.0f;
    
    private int cellShading = 0;

    public Material() {
    }
    
    /**
     * Copy a material.
     * @param material 
     */
    public Material(Material material) {
        this.cellShading = material.cellShading;
        this.layers.addAll(material.layers);
    }

    /**
     * Get material name.
     * 
     * @return Chars, not null
     */
    public CharArray getName() {
        return name;
    }

    /**
     * Set material name.
     *
     * @param name not null.
     */
    public void setName(CharArray name) {
        CObjects.ensureNotNull(name, "name");
        this.name = name;
    }

    /**
     * Indicate if the material will generate an opaque objets.
     * This can be used to sort meshes for better rendering of translucent
     * objects.
     *
     * @return true if final material color is fully opaque.
     */
    public boolean isOpaque(){
        final Layer alpha = getLayer(Layer.TYPE_ALPHA);
        if(alpha!=null) return false;
        final Layer diffuse = getLayer(Layer.TYPE_DIFFUSE);
        if(diffuse==null) return true;
        final Mapping mapping = diffuse.getMapping();
        if(mapping instanceof PlainColorMapping){
            final PlainColorMapping cm = (PlainColorMapping) mapping;
            return cm.isOpaque();
        }else if(mapping instanceof UVMapping){
            final UVMapping um = (UVMapping) mapping;
            return um.isOpaque();
        }else if(mapping instanceof CubeMapping){
            final CubeMapping um = (CubeMapping) mapping;
            return um.isOpaque();
        }
        return true;
    }

    /**
     * Add or replace layer with the same type.
     * 
     * @param layer 
     */
    public void putOrReplaceLayer(Layer layer) {
        Layer previous = getLayer(layer.getType());
        if(previous != null){
            //TODO properly unload this
            layers.remove(previous);
        }
        layers.add(layer);
    }
    
    public Sequence getLayers() {
        return layers;
    }

    /**
     * Get the first layer of given type.
     * @param type
     * @return 
     */
    public Layer getLayer(int type){
        //TODO support multiple layers
        //is that really useful ? need more knowledge on what is used in games.
        for(int i=0;i<layers.getSize();i++){
            final Layer l = (Layer) layers.get(i);
            if(l.getType() == type){
                return l;
            }
        }
        return null;
    }

    public int getCellShading(){
        return cellShading;
    }

    public void setCellShading(int cellShading) {
        this.cellShading = cellShading;
    }

    public Color getAmbient() {
        return ambient;
    }

    public void setAmbient(Color ambient) {
        this.ambient = ambient;
    }

    public Color getSpecular() {
        return specular;
    }

    public void setSpecular(Color specular) {
        this.specular = specular;
    }

    public Color getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(Color diffuse) {
        this.diffuse = diffuse;
    }

    public float getShininess() {
        return shininess;
    }

    public void setShininess(float shininess) {
        this.shininess = shininess;
    }
    
    /**
     * Inddicate if some resources are not up to date.
     * @return
     */
    public boolean isDirty(){
        for(int i=0;i<layers.getSize();i++){
            final Layer l = (Layer) layers.get(i);
            if(l.isDirty()) return true;
        }
        return false;
    }

    /**
     * Dispose all used ressources.
     * @param context
     */
    public void dispose(GLProcessContext context) {
        for(int i=0;i<layers.getSize();i++){
            final Layer l = (Layer) layers.get(i);
            l.dispose(context);
        }
    }

}
