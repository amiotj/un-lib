
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.shader.ShaderTemplate;

/**
 * Material structure, fragment shader with Material structure and methods.
 *
 * @author Johann Sorel
 */
public final class StructMaterial {

    public static final ShaderTemplate TEMPLATE;
    static {
        try{
            TEMPLATE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/material-base-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    /** vec4 */
    public static final Chars MAT_AMBIENT = new Chars("material.ambient");
    /** vec4 */
    public static final Chars MAT_DIFFUSE = new Chars("material.color");
    /** vec4 */
    public static final Chars MAT_SPECULAR = new Chars("material.specular");
    /** float */
    public static final Chars MAT_SHININESS = new Chars("material.shininess");
    
    public static final Chars METHOD_ADDITIVE = new Chars("methodAddition");
    public static final Chars METHOD_MULTIPLY = new Chars("methodMultiply");
    public static final Chars METHOD_SRC_OVER = new Chars("methodSrcOver");
    
    private StructMaterial(){}
    
}
