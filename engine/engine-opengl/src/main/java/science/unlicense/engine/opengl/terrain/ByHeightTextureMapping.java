

package science.unlicense.engine.opengl.terrain;

import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class ByHeightTextureMapping {
    
    private HeightRange[] ranges;

    public ByHeightTextureMapping(HeightRange[] ranges) {
        this.ranges = ranges;
    }
    
    
    
    public static class HeightRange{
        public Texture2D texture;
        public float lower;
        public float upper;

        public HeightRange(Texture2D texture, float lower, float upper) {
            this.texture = texture;
            this.lower = lower;
            this.upper = upper;
        }
    }
}
