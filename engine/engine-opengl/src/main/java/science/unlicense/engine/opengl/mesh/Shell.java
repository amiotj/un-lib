
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.DefaultIntBuffer;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.number.Primitive;
import science.unlicense.engine.opengl.operation.ShellBBoxCalculator;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import static science.unlicense.impl.math.Vectors.*;

/**
 * Defines the boundary of the mesh.
 * Vertices, normals and indices.
 *
 * @author Johann Sorel
 */
public class Shell extends AbstractShape{
    
    protected VBO vertices;
    protected VBO normals;
    protected VBO tangents;
    protected VBO uvs;
    protected IBO indexes;
    protected IndexRange[] ranges;
        
    protected MorphSet morphs;

    //BoundingBox cache
    private BBox bbox;
    
    public void setBBox(BBox bbox) {
        this.bbox = bbox;
    }
    
    public BBox getBBox(){
        if(bbox==null){
            calculateBBox();
        }
        return (bbox!=null)?new BBox(bbox) : null;
    }
        
    /**
     * Calculate shell bounding box from vertices array.
     */
    public void calculateBBox(){
        bbox = new ShellBBoxCalculator(this).getBBox();
    }
    
    /**
     * Calculate normals from vertices and indices.
     * 
     * TODO : this handle only triangle and normals are not smooth using adjacent
     * triangles. 
     */
    public void calculateNormals(){
        final IntCursor icursor = createIBOCursor();
        final VBO normals = VBO.createFloat((int) vertices.getPrimitiveBuffer().getPrimitiveCount(), 3);
        final float[] v0 = new float[3];
        final float[] v1 = new float[3];
        final float[] v2 = new float[3];
        final float[] normal = new float[3];
        final float[] temp = new float[3];
        
        // accumulate normal vectors
        for(IndexRange r : getModes()){
            final int offset = r.getIndexOffset();
            for(int i=0,n=r.getCount();i<n;i+=3){
                final int idx0 = icursor.read(i+offset+0);
                final int idx1 = icursor.read(i+offset+1);
                final int idx2 = icursor.read(i+offset+2);
                vertices.getTupleFloat(idx0, v0);
                vertices.getTupleFloat(idx1, v1);
                vertices.getTupleFloat(idx2, v2);
                Geometries.calculateNormal(v0, v1, v2, normal);
                
                //a normal may already be set at given index, we accumulate
                //values and we will normalize them in a second loop
                add( normals.getTupleFloat(idx0, temp), normal, temp);
                normals.setTupleFloat(idx0, temp);
                add( normals.getTupleFloat(idx1, temp), normal, temp);
                normals.setTupleFloat(idx1, temp);
                add( normals.getTupleFloat(idx2, temp), normal, temp);
                normals.setTupleFloat(idx2, temp);
            }
        }
        
        //normalize normals
        for(IndexRange r : getModes()){
            final int offset = r.getIndexOffset();
            for(int i=0,n=r.getCount();i<n;i+=3){
                final int idx0 = icursor.read(i+offset+0);
                final int idx1 = icursor.read(i+offset+1);
                final int idx2 = icursor.read(i+offset+2);
                
                //normalize
                normalize(normals.getTupleFloat(idx0, temp), temp);
                normals.setTupleFloat(idx0, temp);
                normalize(normals.getTupleFloat(idx1, temp), temp);
                normals.setTupleFloat(idx1, temp);
                normalize(normals.getTupleFloat(idx2, temp), temp);
                normals.setTupleFloat(idx2, temp);
            }
        }
        
        setNormals(normals);
    }
    
    /**
     * Calculate tangents from shell vertices and uvs.
     * 
     * Resources : 
     * http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/
     * 
     */
    public void calculateTangents(){
        final IntCursor icursor = createIBOCursor();
        
        //we store tangents + handedness
        final VBO tangents = VBO.createFloat((int) (vertices.getPrimitiveBuffer().getPrimitiveCount()*4/3), 4);
        final VBO bitangents = VBO.createFloat((int) vertices.getPrimitiveBuffer().getPrimitiveCount(), 3);
        final float[] v0 = new float[3];
        final float[] v1 = new float[3];
        final float[] v2 = new float[3];
        final float[] uv0 = new float[2];
        final float[] uv1 = new float[2];
        final float[] uv2 = new float[2];
        final float[] temp3 = new float[3];
        final float[] temp4 = new float[4];
        
        final float[] deltaPos1 = new float[3];
        final float[] deltaPos2 = new float[3];
        final float[] deltaUV1 = new float[2];
        final float[] deltaUV2 = new float[2];
        final float[] tangent = new float[3];
        final float[] bitangent = new float[3];
        
        // accumulate tangents vectors
        for(IndexRange range : getModes()){
            final int offset = range.getIndexOffset();
            for(int i=0,n=range.getCount();i<n;i+=3){
                final int idx0 = icursor.read(i+offset+0);
                final int idx1 = icursor.read(i+offset+1);
                final int idx2 = icursor.read(i+offset+2);
                vertices.getTupleFloat(idx0, v0);
                vertices.getTupleFloat(idx1, v1);
                vertices.getTupleFloat(idx2, v2);
                uvs.getTupleFloat(idx0, uv0);
                uvs.getTupleFloat(idx1, uv1);
                uvs.getTupleFloat(idx2, uv2);
                
                // Edges of the triangle : postion delta
                subtract(v1, v0, deltaPos1);
                subtract(v2, v0, deltaPos2);
                // UV delta
                subtract(uv1, uv0, deltaUV1);
                subtract(uv2, uv0, deltaUV2);
                
                float ra = 1.0f / (deltaUV1[0] * deltaUV2[1] - deltaUV1[1] * deltaUV2[0]);
                scale(
                    subtract(
                            scale(deltaPos1, deltaUV2[1]), 
                            scale(deltaPos2, deltaUV1[1])),
                        ra, tangent);
                
                //bitangent
                scale(
                    subtract(
                            scale(deltaPos2, deltaUV1[0]), 
                            scale(deltaPos1, deltaUV2[0])),
                        ra, bitangent);
                
                //a tangent may already be set at given index, we accumulate
                //values and we will normalize them in a second loop
                add( tangents.getTupleFloat(idx0, temp3), tangent, temp3);
                tangents.setTupleFloat(idx0, temp3);
                add( tangents.getTupleFloat(idx1, temp3), tangent, temp3);
                tangents.setTupleFloat(idx1, temp3);
                add( tangents.getTupleFloat(idx2, temp3), tangent, temp3);
                tangents.setTupleFloat(idx2, temp3);
                
                add( bitangents.getTupleFloat(idx0, temp3), bitangent, temp3);
                bitangents.setTupleFloat(idx0, temp3);
                add( bitangents.getTupleFloat(idx1, temp3), bitangent, temp3);
                bitangents.setTupleFloat(idx1, temp3);
                add( bitangents.getTupleFloat(idx2, temp3), bitangent, temp3);
                bitangents.setTupleFloat(idx2, temp3);
            }
        }
        
        //normalize tangents + handedness
        final float[] normal = new float[3];
        
        for(IndexRange r : getModes()){
            final int offset = r.getIndexOffset();
            for(int i=0,n=r.getCount();i<n;i++){
                final int idx0 = icursor.read(i+offset+0);
                
                normals.getTupleFloat(idx0, normal);
                tangents.getTupleFloat(idx0, tangent);
                bitangents.getTupleFloat(idx0, bitangent);
                
                // Gram-Schmidt orthogonalize
                final float[] t = normalize(
                        subtract(tangent,
                            scale(normal,dot(normal, tangent)))
                        );
                // Calculate handedness
                final float handedness = (dot(cross(normal,tangent),bitangent) < 0f) ? -1 : +1;
                
                final float[] tw = new float[]{t[0],t[1],t[2], handedness};
                tangents.setTupleFloat(idx0, tw);
            }
        }
        
        setTangents(tangents);
    }
    
    private IntCursor createIBOCursor(){
        final IBO ibo = getIndexes();
        final int primitiveType = ibo.getPrimitiveType();
        final IntCursor icursor;
        if(primitiveType==Primitive.TYPE_INT || primitiveType==Primitive.TYPE_UINT){
            icursor = ibo.getPrimitiveBuffer().cursorInt();
        }else if(primitiveType==Primitive.TYPE_SHORT){
            short[] shorts = ibo.getPrimitiveBuffer().toShortArray();
            int[] ints = Arrays.reformatToInt(shorts);
            icursor = new DefaultIntBuffer(ints).cursorInt();
        }else if(primitiveType==Primitive.TYPE_USHORT){
            final int[] array = ibo.getPrimitiveBuffer().toUShortArray();
            icursor = new DefaultIntBuffer(array).cursorInt();
        }else{
            throw new InvalidArgumentException("Primitive type not supported yet : "+primitiveType);
        }
        return icursor;
    }
    
    public void setVertices(VBO vertexVBO) {
        this.vertices = vertexVBO;
        this.bbox = null;
    }

    public VBO getVertices() {
        return vertices;
    }

    public void setNormals(VBO normalVBO) {
        this.normals = normalVBO;
    }

    public VBO getNormals() {
        return normals;
    }

    /**
     * Expects orthogonalised and normalised tangents.
     * x,Y,Z = tangent
     * W = handedness
     * 
     * @param tangents VBO element of size 4
     */
    public void setTangents(VBO tangents) {
        this.tangents = tangents;
    }

    public VBO getTangents() {
        return tangents;
    }

    /**
     * Texture UVS.
     * @param uvs VBO element of size 2
     */
    public void setUVs(VBO uvs) {
        this.uvs = uvs;
    }

    public VBO getUVs() {
        return uvs;
    }

    public void setIndexes(IBO indexVBO, IndexRange range) {
        this.indexes = indexVBO;
        this.ranges = new IndexRange[]{range};
    }

    public void setIndexes(IBO indexVBO, IndexRange[] ranges) {
        this.indexes = indexVBO;
        this.ranges = ranges;
    }

    /**
     * Shell index buffer.
     * @return IBO
     */
    public IBO getIndexes() {
        return indexes;
    }

    /**
     * Definition of the data types in the index.
     * @return IBO.Range array
     */
    public IndexRange[] getModes() {
        return ranges;
    }

    public MorphSet getMorphs() {
        return morphs;
    }

    public void setMorphs(MorphSet morphs) {
        this.morphs = morphs;
    }
    
    /**
     * Create a copy using the same resources.
     *
     * @return copy
     */
    public Shell copy() {
        final Shell copy = new Shell();
        copy.vertices = vertices;
        copy.normals = normals;
        copy.tangents = tangents;
        copy.uvs = uvs;
        copy.indexes = indexes;
        copy.ranges = ranges;
        return copy;
    }
    
    public boolean isDirty() {
        return (vertices!=null && vertices.isDirty())
            || (normals!=null && normals.isDirty())
            || (tangents!=null && tangents.isDirty())
            || (uvs!=null && uvs.isDirty())
            || (indexes!=null && indexes.isDirty());
    }
    
    public void dispose(GLProcessContext context) {
        if(vertices!=null)  vertices.unloadFromGpuMemory(context.getGL());
        if(normals!=null)   normals.unloadFromGpuMemory(context.getGL());
        if(tangents!=null)  tangents.unloadFromGpuMemory(context.getGL());
        if(uvs!=null)       uvs.unloadFromGpuMemory(context.getGL());
        if(indexes!=null)   indexes.unloadFromGpuMemory(context.getGL());
        if(morphs!=null){
            final Sequence seq = morphs.getMorphs();
            for(Iterator ite=seq.createIterator();ite.hasNext();){
                final MorphTarget mt = (MorphTarget) ite.next();
                mt.getVertices().unloadFromGpuMemory(context.getGL());
            }
        }
    }

}