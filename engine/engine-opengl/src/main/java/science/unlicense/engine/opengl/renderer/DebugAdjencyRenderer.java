

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.Actors;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;

/**
 *
 * @author Johann Sorel
 */
public class DebugAdjencyRenderer extends AbstractRenderer {

    private static final ShaderTemplate NORMAL_GE;
    static {
        try{
            NORMAL_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugadjency-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private ActorProgram program;
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        final Mesh mesh = (Mesh) node;

        if(program==null){
            program = new ActorProgram();
            
            //build material actor
            final Material material = new Material();
            //material.setMesh(mesh);
            material.setDiffuse(Color.BLUE);
            final MaterialActor materialActor = new MaterialActor(material,false);

            //build shell actor
            final Shape shape = mesh.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(mesh, shape);
            program.setExecutor(shellActor);

            final DefaultActor defaultActor = new DefaultActor(new Chars("DebugAdjency"),false,
                    null, null, null, NORMAL_GE, null, true, false);
            
            program.getActors().add(shellActor);
            program.getActors().add(defaultActor);
            program.getActors().add(materialActor);
            program.compile(context);
        }

        program.render(context, camera, mesh);
    }
    
    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
            program = null;
        }
    }

}