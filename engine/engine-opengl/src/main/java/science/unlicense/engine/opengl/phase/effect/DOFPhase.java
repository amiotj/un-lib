
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.scenegraph.Camera;

/**
 *
 * Depth of Field effect.
 *
 * @author Johann Sorel
 */
public class DOFPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_VIEW = new Chars("V");
    private static final Chars UNIFORM_PARAMS = new Chars("dofParams");
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/dof-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final DOFActor actor = new DOFActor();
    
    private Camera camera;
    private float nearBlurDepth;
    private float focalPlaneDepthMin;
    private float focalPlaneDepthMax;
    private float farBlurDepth;
    // blurriness cutoff constant for objects behind the focal plane
    private float cutoff;
    
    private Uniform uniView;
    private Uniform uniParams;
    
    public DOFPhase(Texture texture, Texture positionWorldTexture, Texture bluredTexture,
            float nearBlurDepth, float focalPlaneDepthMin, float focalPlaneDepthMax, 
            float farBlurDepth, float cutoff, Camera camera) {
        this(null,texture,positionWorldTexture,bluredTexture,nearBlurDepth,focalPlaneDepthMin,
                focalPlaneDepthMax,farBlurDepth,cutoff,camera);
    }
    
    public DOFPhase(FBO output, Texture texture, Texture positionWorldTexture, Texture bluredTexture, 
            float nearBlurDepth, float focalPlaneDepthMin, float focalPlaneDepthMax, 
            float farBlurDepth, float cutoff, Camera camera) {
        super(output,texture,positionWorldTexture,bluredTexture);
        this.nearBlurDepth = nearBlurDepth;
        this.focalPlaneDepthMin = focalPlaneDepthMin;
        this.focalPlaneDepthMax = focalPlaneDepthMax;
        this.farBlurDepth = farBlurDepth;
        this.cutoff = cutoff;
        this.camera = camera;
    }

    public float getNearBlurDepth() {
        return nearBlurDepth;
    }

    public void setNearBlurDepth(float nearBlurDepth) {
        this.nearBlurDepth = nearBlurDepth;
    }

    public float getFocalPlaneDepthMin() {
        return focalPlaneDepthMin;
    }
    
    public float getFocalPlaneDepthMax() {
        return focalPlaneDepthMax;
    }

    public void setFocalPlaneDepth(float focalPlaneDepth) {
        setFocalPlaneDepth(focalPlaneDepth,focalPlaneDepth);
    }
    
    public void setFocalPlaneDepth(float focalPlaneDepthMin, float focalPlaneDepthMax) {
        this.focalPlaneDepthMin = focalPlaneDepthMin;
        this.focalPlaneDepthMax = focalPlaneDepthMax;
    }
    
    public float getFarBlurDepth() {
        return farBlurDepth;
    }

    public void setFarBlurDepth(float farBlurDepth) {
        this.farBlurDepth = farBlurDepth;
    }

    public float getCutoff() {
        return cutoff;
    }

    public void setCutoff(float cutoff) {
        this.cutoff = cutoff;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }
    
    protected DOFActor getActor() {
        return actor;
    }

    private final class DOFActor extends DefaultActor{

        public DOFActor() {
            super(new Chars("DepthOfField"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if(uniParams == null){
                uniView = program.getUniform(UNIFORM_VIEW);
                uniParams = program.getUniform(UNIFORM_PARAMS);
            }
            uniView.setMat4(gl, camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniParams.setVec4(gl, new float[]{nearBlurDepth,focalPlaneDepthMin,focalPlaneDepthMax,farBlurDepth});
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniView = null;
            uniParams = null;
        }

    }
    
}

