
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.UnimplementedException;

/**
 * TODO
 * 
 * @author Johann Sorel
 */
public class SpecularActor extends AbstractActor{

    public SpecularActor() {
        super(new Chars("SpecularMapping"));
    }

    public int getMinGLSLVersion() {
        throw new UnimplementedException("Not supported yet.");
    }

}
