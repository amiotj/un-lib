
package science.unlicense.engine.opengl.mesh;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.image.Image;
import science.unlicense.api.media.ImagePacket;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.renderer.MeshRenderer;

/**
 *
 * @author Johann Sorel
 */
public class MediaPlane extends GLNode{
    
    private final MediaReadStream reader;

    //cache all video images
    //TODO must improve this
    private final Sequence times = new ArraySequence();
    private final Sequence textures = new ArraySequence();

    private final Plan plan;
    private final UVMapping uvmapping;
    private Extent.Long extent;

    public MediaPlane(MediaReadStream reader) {
        this(reader,
             new Vector(-0.5, -0.5, 0),
             new Vector(-0.5, +0.5, 0),
             new Vector(+0.5, +0.5, 0),
             new Vector(+0.5, -0.5, 0));

    }
    
    public MediaPlane(MediaReadStream reader, Vector v1,Vector v2,Vector v3,Vector v4) {
        this.reader = reader;

        uvmapping = new UVMapping();
        final VBO uv = new VBO(new float[]{
            0,1,
            0,0,
            1,0,
            1,1
        },2);
        plan = new Plan(v1,v2,v3,v4);
        plan.getMaterial().putOrReplaceLayer(new Layer(uvmapping));
        ((Shell)plan.getShape()).setUVs(uv);
        ((MeshRenderer)plan.getRenderers().get(0)).getState().setCulling(-1);
        children.add(plan);

        getUpdaters().add(new VideoUpdater());

        try{
            for(MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
                final Image mi = ((ImagePacket)pack).getImage();
                extent = mi.getExtent();
                final Texture2D tr = new Texture2D(mi);
                times.add(pack.getStartTime());
                textures.add(tr);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    public Extent.Long getExtent() {
        return extent;
    }
    
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        for(int i=0,n=textures.getSize();i<n;i++){
            ((Texture2D)textures.get(i)).unloadFromGpuMemory(context.getGL());
        }
    }
    

    private class VideoUpdater implements Updater{

        long before = -1;
        long beforeNano = 0;

        public void update(RenderContext context, GLNode node) {

            long candidate;
            while(true){
                if(before==-1) before = context.getTimeNano();
                long time = (context.getTimeNano()-before) / 1000000;

                for(int i=0,n=times.getSize();i<n;i++){
                    candidate = (Long)times.get(i);
                    if(candidate>=time){
                        final Texture2D tr = (Texture2D) textures.get(i);
                        if(!tr.isOnGpuMemory()){
                            tr.loadOnGpuMemory(context.getGL());
                        }
                        uvmapping.setTexture(tr);
                        return;
                    }
                }

                //new loop
                before = -1;
            }

        }

    }

}
