
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.material.mapping.ByVertexColorMapping;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * A 3D crosshair.
 *
 * @author Johann Sorel
 */
public class Crosshair3D extends Mesh{

    public Crosshair3D() {
        this(true);
    }
    
    public Crosshair3D(boolean colored) {
        this(colored,false);
    }
    
    public Crosshair3D(boolean colored, boolean showPositive) {

        final float[] vertex = {
            -1, 0, 0,+1, 0, 0,
             0,-1, 0, 0,+1, 0,
             0, 0,-1, 0, 0,+1
        };
        final float[] normal = {
             0, 1, 0, 0, 1, 0,
             0, 1, 0, 0, 1, 0,
             0, 1, 0, 0, 1, 0
        };
        final int[] index = {
            0,1,
            2,3,
            4,5
        };

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertex, 3));
        shell.setNormals(new VBO(normal, 3));
        shell.setIndexes(new IBO(index, 2), IndexRange.LINES(0, 6));
        setShape(shell);

        if(colored){
            final ByVertexColorMapping paint = new ByVertexColorMapping();
            paint.setHasAlpha(false);
            final Buffer colors = GLBufferFactory.INSTANCE.createFloat(18);
            colors.cursorFloat().write(new float[]{
                1,0,0,1,0,0,
                0,1,0,0,1,0,
                0,0,1,0,0,1});
            paint.setColors(colors);
            getMaterial().putOrReplaceLayer(new Layer(paint));
        }else{
            getMaterial().setDiffuse(Color.WHITE);
        }
        
        if(showPositive){
            final Mesh xdir = new GeometryMesh(new Sphere(0.04));
            final Mesh ydir = new GeometryMesh(new Sphere(0.04));
            final Mesh zdir = new GeometryMesh(new Sphere(0.04));
            xdir.getNodeTransform().getTranslation().setXYZ(1, 0, 0);
            ydir.getNodeTransform().getTranslation().setXYZ(0, 1, 0);
            zdir.getNodeTransform().getTranslation().setXYZ(0, 0, 1);
            xdir.getNodeTransform().notifyChanged();
            ydir.getNodeTransform().notifyChanged();
            zdir.getNodeTransform().notifyChanged();
            if(colored){
                xdir.getMaterial().setDiffuse(Color.RED);
                ydir.getMaterial().setDiffuse(Color.GREEN);
                zdir.getMaterial().setDiffuse(Color.BLUE);
            }else{
                xdir.getMaterial().setDiffuse(Color.WHITE);
                ydir.getMaterial().setDiffuse(Color.WHITE);
                zdir.getMaterial().setDiffuse(Color.WHITE);
            }
            
            children.add(xdir);
            children.add(ydir);
            children.add(zdir);
        }
        
    }

}
