
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.engine.opengl.GLExecutable;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.GLResourceManager;
import science.unlicense.engine.opengl.GLStatistics;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.geometry.s2d.Rectangle;

/**
 * Rendering Context, link a GLScene, a Camera and timing.
 * The rendering process is composed of a sequence of Phase, allowing
 * some preprocessing or effects.
 *
 * @author Johann Sorel
 */
public class DefaultRenderContext implements RenderContext {

    private GLProcessContext glcontext;
    private GLNode scene;

    public DefaultRenderContext(GLProcessContext glcontext, GLNode scene) {
        this.glcontext = glcontext;
        this.scene = scene;
    }

    public void setProcessContext(GLProcessContext ctx){
        this.glcontext = ctx;
    }

    public Rectangle getGLRectangle() {
        return glcontext.getGLRectangle();
    }

    public Rectangle getViewRectangle() {
        return glcontext.getViewRectangle();
    }
    
    public GLNode getScene() {
        return scene;
    }

    public void setScene(GLNode scene) {
        this.scene = scene;
    }

    public long getTimeNano() {
        return glcontext.getTimeNano();
    }

    public long getDiffTimeNano() {
        return glcontext.getDiffTimeNano();        
    }
    
    public float getTimeSecond() {
        return glcontext.getTimeSecond();
    }

    public float getDiffTimeSecond() {
        return glcontext.getDiffTimeSecond();        
    }

    public GLSource getSource() {
        return glcontext.getSource();
    }

    public GL getGL() {
        return glcontext.getGL();
    }

    public void addTask(GLExecutable executable) {
        glcontext.addTask(executable);
    }

    public Sequence getPhases() {
        return glcontext.getPhases();
    }

    public GLResourceManager getResourceManager() {
        return glcontext.getResourceManager();
    }

    public GLStatistics getStatistics() {
        return glcontext.getStatistics();
    }

}