
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GL1;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.math.Matrix;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.operation.ShellVisitor;

/**
 * Simple color shell rendering visitor.
 * Loops on all geometry primitives and render them using the fixed pipeline GL2.
 *
 * @author Johann Sorel
 */
final class GL2ShellPaintVisitor extends ShellVisitor {

    private GL1 gl;
    private Matrix MV;
    private Matrix P;
    private Color color = Color.WHITE;

    public GL2ShellPaintVisitor(Shell shell, GL1 gl, Matrix MV, Matrix P) {
        super(shell);
        this.gl = gl;
        this.MV = MV;
        this.P = P;
    }

    public GL1 getGl() {
        return gl;
    }

    public void setGl(GL1 gl) {
        this.gl = gl;
    }

    public Matrix getMV() {
        return MV;
    }

    public void setMV(Matrix MV) {
        this.MV = MV;
    }

    public Matrix getP() {
        return P;
    }

    public void setP(Matrix P) {
        this.P = P;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    protected void visit(Vertex vertex) {
        //do nothing
    }

    @Override
    public void visit() {
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadMatrixf(P.toArrayFloatColOrder());
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadMatrixf(MV.toArrayFloatColOrder());
        gl.glColor3f(color.getRed(), color.getGreen(), color.getBlue());
        gl.glDisable(GL_DEPTH_TEST);
        gl.glDisable(GL_CULL_FACE);
        gl.glDisable(GL_LIGHTING);
        gl.glDepthMask(false);
        super.visit();
        gl.glEnable(GL_DEPTH_TEST);
        gl.glEnable(GL_CULL_FACE);
        gl.glEnable(GL_LIGHTING);
        gl.glDepthMask(true);
        gl.glFlush();
    }

    @Override
    protected void visit(Triangle candidate) {
        gl.glBegin(GL_TRIANGLES);
        gl.glVertex3fv(candidate.v0.vertice);
        gl.glVertex3fv(candidate.v1.vertice);
        gl.glVertex3fv(candidate.v2.vertice);
        gl.glEnd();
    }

    @Override
    protected void visit(Line candidate) {
        gl.glBegin(GL_LINES);
        gl.glVertex3fv(candidate.v0.vertice);
        gl.glVertex3fv(candidate.v1.vertice);
        gl.glEnd();
    }

    @Override
    protected void visit(Point candidate) {
        gl.glBegin(GL_POINTS);
        gl.glVertex3fv(candidate.v0.vertice);
        gl.glEnd();
    }

}
