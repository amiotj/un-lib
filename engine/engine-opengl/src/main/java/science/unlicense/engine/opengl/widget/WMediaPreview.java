

package science.unlicense.engine.opengl.widget;

import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyPredicate;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.media.Medias;
import science.unlicense.engine.opengl.mesh.MediaPlane;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.api.io.IOException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.AffineRW;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.path.Path;
import science.unlicense.engine.ui.widget.WGraphicImage;
import static science.unlicense.engine.ui.widget.WGraphicImage.PROPERTY_FITTING;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class WMediaPreview extends WGLNode {

    public static final int FITTING_SCALED = 0;
    public static final int FITTING_CENTERED = 1;
    public static final int FITTING_STRETCHED = 2;
    public static final int FITTING_ZOOMED = 3;
    public static final int FITTING_CORNER = 4;
    
    private final Path path;
    private final MediaPlane mediaPlane;
    private final Listener lst = new Listener();
    
    public WMediaPreview(Path path) throws IOException {
        this.path = path;
        
        //open a video media
        final MediaStore store = Medias.open(path);
        final MediaReadParameters mediaParams = new MediaReadParameters();
        mediaParams.setStreamIndexes(new int[]{0});
        final MediaReadStream videoReader = store.createReader(mediaParams);

        //create a media plane
        mediaPlane = new MediaPlane(videoReader,
             //small z offset to avoid fragment flickering
             new Vector(-0.5, +0.5, 0.0001),
             new Vector(-0.5, -0.5, 0.0001),
             new Vector(+0.5, -0.5, 0.0001),
             new Vector(+0.5, +0.5, 0.0001));
        inSceneNode.getChildren().add(mediaPlane);

        addEventListener(new PropertyPredicate(Widget.PROPERTY_EFFECTIVE_EXTENT), lst);
        getNodeTransform().addEventListener(null, lst);
        lst.receiveEvent(null);
    }

    public Extent.Long getMediaExtent(){
        return mediaPlane.getExtent();
    }

    /**
     * Get image fitting mode.
     * @return Image fitting mode, default is
     */
    public int getFitting() {
        return (Integer)getPropertyValue(PROPERTY_FITTING,0);
    }

    /**
     * Set image fitting mode.
     * @param fitting
     */
    public void setFitting(int fitting) {
        if(setPropertyValue(PROPERTY_FITTING,fitting)){
            lst.receiveEvent(null);
        }
    }

    private class Listener implements EventListener{

        @Override
        public void receiveEvent(Event event) {

            final BBox imgExt = new BBox(mediaPlane.getExtent());
            //transform the media plane [-0.5,+0.5] to image ratio
            final BBox mediabbox = new BBox(2);
            mediabbox.setRange(0, -0.5, +0.5);
            mediabbox.setRange(1, -0.5, +0.5);
            double rY = imgExt.getSpan(1)/imgExt.getSpan(0);
            Matrix4x4 ratio = new Matrix4x4(
                    1, 0, 0, 0,
                    0, rY, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1);
            mediabbox.setRange(1, mediabbox.getMin(1)*rY, mediabbox.getMax(1)*rY);


            final BBox target = getBoundingBox();
            final int fitting = getFitting();
            AffineRW trs;
            if(fitting == WGraphicImage.FITTING_CENTERED) trs = Projections.centered(mediabbox,target);
            else if(fitting == WGraphicImage.FITTING_ZOOMED) trs = Projections.zoomed(mediabbox,target);
            else if(fitting == WGraphicImage.FITTING_STRETCHED) trs = Projections.stretched(mediabbox,target);
            else if(fitting == WGraphicImage.FITTING_CORNER){
                trs = new Affine2();
                trs.set(0, 2, target.getMin(0));
                trs.set(1, 2, target.getMin(1));
            }
            else trs = Projections.scaled(mediabbox,target);



            final Matrix4x4 scale = new Matrix4x4(
                    trs.get(0, 0),0,0,trs.get(0, 2),
                    0,trs.get(1, 1),0,trs.get(1, 2),
                    0,0,1,0,
                    0,0,0,1
            );
            scale.localMultiply(ratio);

            mediaPlane.getNodeTransform().set(scale);
        }
    }

}
