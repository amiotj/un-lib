
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.shader.ShaderTemplate;

/**
 * Light structure, fragment shader with Light and fragement structures and methods.
 *
 * @author Johann Sorel
 */
public final class StructLight {

    public static final ShaderTemplate TEMPLATE;
    static {
        try{
            TEMPLATE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/light/lights-base-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    //LIGHT
    /** vec4 : in world space*/
    public static final Chars LIGHT_POSITION = new Chars("position");
    /** vec4 */
    public static final Chars LIGHT_DIFFUSE = new Chars("diffuse");
    /** vec4 */
    public static final Chars LIGHT_SPECULAR = new Chars("specular");
    /** float */
    public static final Chars LIGHT_CONSTANT_ATTENUATION = new Chars("constantAttenuation");
    /** float */
    public static final Chars LIGHT_LINEAR_ATTENUATION = new Chars("linearAttenuation");
    /** float */
    public static final Chars LIGHT_QUADRATIC_ATTENUATION = new Chars("quadraticAttenuation");
    /** float */
    public static final Chars LIGHT_SPOT_CUTOFF = new Chars("spotCutoff");
    /** float */
    public static final Chars LIGHT_SPOT_EXPONENT = new Chars("spotExponent");
    /** vec3 */
    public static final Chars LIGHT_SPOT_DIRECTION = new Chars("spotDirection");
    /** int */
    public static final Chars LIGHT_HAS_SHADOWMAP = new Chars("hasShadowMap");
    /** mat4 */
    public static final Chars LIGHT_VIEW = new Chars("v");
    /** mat4 */
    public static final Chars LIGHT_PROJECTION = new Chars("p");

    //FRAGEMENT
    /** vec3 */
    public static final Chars FRAG_NORMAL = new Chars("normal");
    /** vec3 */
    public static final Chars FRAG_EYE_DIRECTION = new Chars("eyeDirection");
    /** Light */
    public static final Chars FRAG_LIGHT = new Chars("light");
    /** vec4 */
    public static final Chars FRAG_DIFFUSE = new Chars("diffuse");
    /** vec4 */
    public static final Chars FRAG_SPECULAR = new Chars("specular");

    private StructLight(){}

}
