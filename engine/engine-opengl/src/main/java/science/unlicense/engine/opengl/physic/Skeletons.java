

package science.unlicense.engine.opengl.physic;

import science.unlicense.api.anim.Animation;
import science.unlicense.api.anim.CompoundAnimation;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.math.Affine;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public final class Skeletons {

    /**
     * Calculate vertice position, taking in account skeleton pose.
     * 
     * @param mesh
     * @param vertexId 
     */
    public static Vector evaluatePosition(Mesh mesh, int vertexId) {
        final SkinShell shell = (SkinShell) mesh.getShape();
        final VBO joints = ((SkinShell)shell).getJointIndexes();
        final VBO weights = ((SkinShell)shell).getWeights();
        final VBO vertices = shell.getVertices();
        Vector v0 = new Vector(vertices.getTupleFloat(vertexId, null));
        
        //check if there is an animation
        final MultipartMesh mpm = (MultipartMesh) mesh.getParent();
        final Skeleton skeleton =  mpm.getSkeleton();
        
        final int[] js = joints.getTupleInt(vertexId, null);
        final float[] ws = weights.getTupleFloat(vertexId, null);
        
        Vector l_position4 = new Vector(v0);
        Vector posModel = new Vector(0,0,0);
        for(int i=0;i<js.length;i++){
            final Joint jt = (Joint) skeleton.getJoint(js[i]);
            final double w = ws[i];
            final Affine jointMatrix = jt.getBindPose().multiply(jt.getInvertBindPose());
            posModel.localAdd(new Vector(jointMatrix.transform(l_position4,null)).scale(w));
        }
        return posModel;
    }
    
    private Skeletons(){}
    
    /**
     * Build relative skeleton pose from skeleton bind pose.
     * 
     * @param jointRelativeFrom
     * @return RelativeSkeletonPose
     */
    public static RelativeSkeletonPose toRelative(final Skeleton skePose, final int jointRelativeFrom){
        
        final RelativeSkeletonPose pose = new RelativeSkeletonPose();
            
        if(jointRelativeFrom == JointKeyFrame.FROM_BASE){
        
            //rebuild the base skeleton to calculate the joint differences
            final Skeleton skeBase = skePose.copy();
            skeBase.resetToBase();
           
            final Sequence bindJoints = skePose.getChildren();
            final Sequence baseJoints = skeBase.getChildren();
            for(int i=0,n=bindJoints.getSize();i<n;i++){
                toRelative((Joint)bindJoints.get(i), (Joint)baseJoints.get(i), pose);
            }

        }else{
            throw new RuntimeException("Not supported yet");
        }
        return pose;
    }
    
    private static void toRelative(Joint bind, Joint base, RelativeSkeletonPose pose){
        final NodeTransform bindTrs = bind.getNodeTransform();
        final NodeTransform baseTrs = base.getNodeTransform();
        final JointKeyFrame jp = new JointKeyFrame();
        jp.setValue(new NodeTransform(3));
        final NodeTransform trs = jp.getValue();
        jp.setFrom(JointKeyFrame.FROM_BASE);
        jp.setJoint(bind.getName());
        trs.getTranslation().set(bindTrs.getTranslation().subtract(baseTrs.getTranslation()));
        trs.getRotation().set(bindTrs.getRotation().multiply(baseTrs.getRotation().invert()));
        trs.notifyChanged();
        pose.getJointPoses().add(jp);
        
        //loop on childrens
        final Object[] bindChildren = bind.getChildren().toArray();
        final Object[] baseChildren = base.getChildren().toArray();
        for(int i=0;i<bindChildren.length;i++){
            toRelative((Joint)bindChildren[i], (Joint)baseChildren[i], pose);
        }
        
    }
    
    public static JointKeyFrame toRelative(Joint jt, final int jointRelativeFrom){
        throw new RuntimeException("TODO");
//        final Matrix bindPose = jt.getBindPose();
//        bindPose.set(jt.getInvertBindPose());
//        bindPose.localInvert();
//        //at this stage, the bind pose contains the root to joint matrix
//
//        final SceneNode parent = jt.getParent();
//        if(parent instanceof Joint){
//            final Joint jpa = (Joint) parent;
//            //we remove the root to parent transform
//            final Matrix parentToRoot = jpa.getBindPose().invert();
//            final Matrix parentToJoint = bindPose.multiply(parentToRoot);
//            //convert the matrix back to a NodeTransform
//            jt.getModelTransform().set(parentToJoint);
//        }else{
//            //no parent, the bind pose is the same as parent to joint matrix
//            jt.getModelTransform().set(bindPose);
//        }
    }
    
    /**
     * Attach an animation to a mesh and skeleton.
     * 
     * @param anim Animation to map
     * @param skeleton skeleton to link : used by skeleton animations
     * @param node node to link : used by morph animations
     * @param nameAliases : possible aliases for name mapping
     */
    public static void mapAnimation(Animation anim, Skeleton skeleton, GLNode node, Dictionary nameAliases){
        if(anim instanceof SkeletonAnimation){
            final SkeletonAnimation skeAnim = (SkeletonAnimation) anim;
            SkeletonAnimationResolver.map(skeleton, skeAnim, nameAliases);
            
        }else if(anim instanceof MorphAnimation){
            final MorphAnimation morphAnim = (MorphAnimation) anim;
            morphAnim.setNode(node);
            
        }else if(anim instanceof CompoundAnimation){
            final Sequence subs = ((CompoundAnimation)anim).getElements();
            for(int i=0,n=subs.getSize();i<n;i++){
                mapAnimation((Animation) subs.get(i), skeleton, node, nameAliases);
            }
        }
    }
    
}
