

package science.unlicense.engine.opengl.phase.shadow;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.scenegraph.Camera;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * TODO
 * 
 * @author Johann Sorel
 */
public class LightspacePerspectiveShadowMapsPhase extends AbstractFboPhase{

    private final ShadowMapVisitor visitor;
    private GLNode scene;
    
    public LightspacePerspectiveShadowMapsPhase(GLNode scene, Camera camera, int textureSize) {
        this(Chars.EMPTY,scene,camera,textureSize);
    }
    
    public LightspacePerspectiveShadowMapsPhase(Chars id, GLNode scene, Camera camera, int textureSize) {
        super(id);
        this.scene = scene;
        visitor = new ShadowMapVisitor(textureSize);
        visitor.setCamera((CameraMono) camera);
    }
    
    public GLNode getScene(){
        return scene;
    }
    
    public void setScene(GLNode scene) {
        this.scene = scene;
    }

    public Camera getCamera() {
        return visitor.getCamera();
    }

    public void setCamera(Camera camera) {
        visitor.setCamera((CameraMono) camera);
    }
    
    public void setTextureSize(int size){
        visitor.setTextureSize(size);
    }
    
    public int getTextureSize(int size){
        return visitor.getTextureSize();
    }

    protected void processInternal(GLProcessContext ctx) throws GLException {
        scene.accept(visitor, ctx);
    }

    
    
}