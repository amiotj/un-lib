
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DataCursor;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * A flat quad build from 4 points.
 *
 * @author Johann Sorel
 */
public class Plan extends Mesh{

    public Plan() {
        this(new Vector(-0.5, -0.5, 0),
             new Vector(-0.5, +0.5, 0),
             new Vector(+0.5, +0.5, 0),
             new Vector(+0.5, -0.5, 0));
    }

    public Plan(Vector v1,Vector v2,Vector v3,Vector v4) {
        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(16);
        final DataCursor vc = vertices.cursor();
        vc.writeFloat(v1.toArrayFloat());
        vc.writeFloat(v2.toArrayFloat());
        vc.writeFloat(v3.toArrayFloat());
        vc.writeFloat(v4.toArrayFloat());

        final Buffer indices = GLBufferFactory.INSTANCE.createInt(6);
        indices.writeInt(new int[]{
            0,1,2,
            2,3,0
        },0);

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setIndexes(new IBO(indices,3),IndexRange.TRIANGLES(0, 6));
        shell.calculateNormals();
        setShape(shell);
    }

    public Shell getShape() {
        return (Shell) super.getShape();
    }

}
