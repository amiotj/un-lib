

package science.unlicense.engine.opengl.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.media.MediaReadParameters;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.media.Medias;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.api.painter2d.Painters;
import science.unlicense.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.component.path.AbstractPathPresenter;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.geometry.s2d.RoundedRectangle;
import science.unlicense.api.image.Image;
import science.unlicense.api.media.ImagePacket;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.impl.image.process.geometric.RescaleOperator;
import science.unlicense.api.path.Path;

/**
 *
 * @author Johann Sorel
 */
public class MediaPathPresenter extends AbstractPathPresenter{

    public static final MediaPathPresenter INSTANCE = new MediaPathPresenter();

    private Image mimeImage = null;
    
    public float getPriority() {
        return 3;
    }
    
    public boolean canHandle(Path path) {
        try {
            return Medias.canDecode(path);
        } catch (IOException ex) {
            return false;
        }
    }
    
    public Image createMimeImage(Extent.Long size) {
        if(mimeImage != null && mimeImage.getExtent().get(0)== size.get(0)){
            return mimeImage;
        }
        //create a small mime image
        final ImagePainter2D painter = Painters.getPainterManager().createPainter((int)size.get(0), (int)size.get(1));
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
        painter.setPaint(new ColorPaint(Color.BLACK));
        painter.stroke(new RoundedRectangle(2, 2, size.get(0)-4, size.get(1)-4));
        painter.flush();
        mimeImage = painter.getImage();
        painter.dispose();
        return mimeImage;
    }

    public Image createImage(Path path, Extent.Long size) {
        
        Image image = null;
        try{
            //open a video media
            final MediaStore store = Medias.open(path);
            final MediaReadParameters mediaParams = new MediaReadParameters();
            mediaParams.setStreamIndexes(new int[]{0});
            final MediaReadStream reader = store.createReader(mediaParams);
        
            for(MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
                image = ((ImagePacket)pack).getImage();
                break;
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
        
        try {
            //TODO image is not centered
            final double scalex = size.get(0) / image.getExtent().get(0);
            final double scaley = size.get(1) / image.getExtent().get(1);
            final Extent ext;
            if(scalex<scaley){
                ext = new Extent.Double(size.get(0), image.getExtent().get(1)*scalex);
            }else{
                ext = new Extent.Double(image.getExtent().get(0)*scaley, size.get(1));
            }
            image = RescaleOperator.execute(image, ext);
            
            return image;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public Widget createInteractive(Path path) {
        try {
            WMediaPreview prev =  new WMediaPreview(path);
            prev.setFitting(WMediaPreview.FITTING_SCALED);

            return prev;
        } catch (IOException ex) {
            ex.printStackTrace();
            return new WLabel(new Chars(ex.getMessage()));
        }
    }
    
}
