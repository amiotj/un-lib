
package science.unlicense.engine.opengl.mesh;

import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;

/**
 * A simple node combining msehes to create a small direction marker.
 *
 * @author Johann Sorel
 */
public class XYZMarker extends GLNode{

    public XYZMarker() {
        init();
    }

    private void init(){
        final float length = 5f;
        final float width = 0.1f;
        final float half = length/2f;

        final GeometryMesh x = new GeometryMesh(new BBox(new double[]{0,-width,-width},new double[]{length,width,width}));
        final GeometryMesh y = new GeometryMesh(new BBox(new double[]{-width,0,-width},new double[]{width,length,width}));
        final GeometryMesh z = new GeometryMesh(new BBox(new double[]{-width,-width,0},new double[]{width,width,length}));
        x.getMaterial().setDiffuse(Color.RED);
        y.getMaterial().setDiffuse(Color.GREEN);
        z.getMaterial().setDiffuse(Color.BLUE);
        children.add(x);
        children.add(y);
        children.add(z);
    }

}
