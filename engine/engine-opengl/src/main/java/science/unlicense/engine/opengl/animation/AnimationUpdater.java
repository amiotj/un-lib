
package science.unlicense.engine.opengl.animation;

import science.unlicense.api.anim.Animation;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 *
 * @author Johann Sorel
 */
public class AnimationUpdater implements Updater {

    private final Animation animation;

    public AnimationUpdater(Animation animation) {
        this.animation = animation;
    }
    
    public Animation getAnimation() {
        return animation;
    }
    
    public void update(RenderContext context, GLNode node) {
        animation.pulse(context.getTimeNano());
    }
    
}
