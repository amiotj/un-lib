
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.io.IOException;

/**
 * Box blur. TODO
 *
 * @author Johann Sorel
 */
public class BlurPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/blur-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final BlurActor actor = new BlurActor();
    
    public BlurPhase(FBO output,Texture texture1) {
        super(output, texture1);
    }
        
    protected BlurActor getActor() {
        return actor;
    }

    private final class BlurActor extends DefaultActor{

        public BlurActor() {
            super(null,false,null,null,null,null,SHADER_FR,true,true);
        }
    }
}
