

package science.unlicense.engine.opengl;

import science.unlicense.api.task.Executable;

/**
 * A GLExecutable is a task run the the OpenGL thread.
 * Those can be used to manipulate resources like allocation or disposal.
 * Could also be use to update the rendering phases and scene.
 * 
 * @author Johann Sorel
 */
public abstract class GLExecutable implements Executable{

    /**
     * Context which executed this task.
     */
    protected GLProcessContext context;

    public void setContext(GLProcessContext context) {
        this.context = context;
    }
        
}
