
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.collection.Iterator;
import science.unlicense.impl.anim.KeyFrameTransformTimeSerie;

/**
 *
 * @author Johann Sorel
 */
public class JointTimeSerie extends KeyFrameTransformTimeSerie {

    private Object jointIdentifier;
    private Joint joint;

    public JointTimeSerie() {
        this.joint = null;
    }

    public JointTimeSerie(Joint joint) {
        this.joint = joint;
    }
    
    /**
     * Joint identifier, to find back joint in the skeleton.
     * Can be a Chars, a number, an uri or else.
     * @return Object
     */
    public Object getJointIdentifier() {
        return jointIdentifier;
    }

    public void setJointIdentifier(Object jointIdentifier) {
        this.jointIdentifier = jointIdentifier;
    }

    /**
     * Joint affected by this time serie.
     * @return Joint, can be null if it has not been resolved yet.
     */
    public Joint getJoint() {
        return joint;
    }

    public void setJoint(Joint joint) {
        this.joint = joint;

        //set joint on each JointFrame
        final Iterator ite = frames.createIterator();
        while(ite.hasNext()){
            final JointKeyFrame f = (JointKeyFrame)ite.next();
            f.setJoint(joint);
        }
    }
    
    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append("Joint time serie, ");
        if(joint!=null){
            sb.append(joint.getName()).append(" : ");
        }else{
            sb.append(jointIdentifier).append(" : ");
        }

        final Iterator ite = frames.createIterator();
        while(ite.hasNext()){
            final JointKeyFrame f = (JointKeyFrame)ite.next();
            sb.appendNumber(f.getTime()).append(f.getValue()).append(',');
        }
        return new Chars(sb.toString());
    }

}
