
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.TextureUtils;
import science.unlicense.api.image.Image;
import science.unlicense.impl.math.Vector;

/**
 * UV buffer is declared on the Mesh.
 * Can handle sprite.
 *
 * @author Johann Sorel
 */
public class UVMapping implements Mapping {

    private volatile Texture2D texture;
    private Boolean opaque = null;
    private SpriteInfo sprite = null;
    private Vector scale = new Vector(1.0,1.0);
    private Vector offset = new Vector(0.0,0.0);

    public UVMapping() {
    }

    public UVMapping(Texture2D texture) {
        this.texture = texture;
    }

    /**
     * Scale relative to texture center.
     * @return 
     */
    public Vector getScale() {
        return scale;
    }

    public void setScale(Vector scale) {
        this.scale = scale;
    }

    public Vector getOffset() {
        return offset;
    }

    public void setOffset(Vector offset) {
        this.offset = offset;
    }

    public SpriteInfo getSprite() {
        return sprite;
    }

    public void setSprite(SpriteInfo sprite) {
        this.sprite = sprite;
    }
    
    public boolean isOpaque(){
        if(opaque==null) checkOpaque();
        return opaque;
    }

    private void checkOpaque(){
        //check opacity
        opaque = true;
        final Image image = texture.getImage();
        if(image==null)return;
        opaque = TextureUtils.isOpaque(image);
    }

    public Texture2D getTexture() {
        return texture;
    }

    public void setTexture(Texture2D texture) {
        this.texture = texture;
    }

    public boolean isDirty() {
        if(texture!=null){
            return texture.isDirty();
        }
        return false;
    }

    public void dispose(GLProcessContext context) {
        if(texture!=null){
            texture.unloadFromGpuMemory(context.getGL());
        }
    }

}
