package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.resource.TBO;
import science.unlicense.impl.math.Matrix4x4;

/**
 * A multipart mesh is a group of nodes sharing common resources. often
 * vertexbuffers, skeleton, physics or animations.
 *
 * TODO think about a proper organisation of meshes divided in plenty of parts
 * and materials.
 *
 * @author Johann Sorel
 */
public class MultipartMesh extends GLNode {

    private Skeleton skeleton;
    private final Sequence constraints = new ArraySequence();
    private TBO jointMatrixTbo;
    private long tboUpdate = -1;
    private final AnimationSet animations = new AnimationSet();

    public MultipartMesh() {
    }

    public MultipartMesh(CoordinateSystem cs) {
        super(cs);
    }

    /**
     * Shortcut to find the skeleton node.
     * @return skeleton
     */
    public Skeleton getSkeleton() {
        return skeleton;
    }

    /**
     * Replace skeleton.
     * The skeleton is a children node.
     *
     * @param skeleton
     */
    public void setSkeleton(Skeleton skeleton) {
        if(this.skeleton!=null){
            children.remove(this.skeleton);
        }
        this.skeleton = skeleton;
        if(this.skeleton!=null){
            children.add(skeleton);
        }
    }

    public AnimationSet getAnimationSet() {
        return animations;
    }

    /**
     * Physic constraints associate to this model.
     * @return Sequence, never null
     */
    public Sequence getConstraints() {
        return constraints;
    }

    public long getTboUpdate() {
        return tboUpdate;
    }

    public void setTboUpdate(long tboUpdate) {
        this.tboUpdate = tboUpdate;
    }
    
    public void buildTBO(){
        final int nb = skeleton.getAllJoints().getSize();
        //4*4 matrix for each joint
        jointMatrixTbo = new TBO(new float[nb*16], nb);
        jointMatrixTbo.setForgetOnLoad(false);
    }
    
    public void updateTBO(){
        if(jointMatrixTbo==null) buildTBO();
        final Matrix4x4 matrix = new Matrix4x4();
        final float[] array = new float[16];
        
        final Sequence joints = skeleton.getAllJoints();
        final FloatCursor fb = jointMatrixTbo.getBuffer().cursorFloat();
        for(int i=0,n=joints.getSize();i<n;i++){
            final Joint jt = (Joint)skeleton.getJoint(i);
            jt.getBindPose().multiply(jt.getInvertBindPose()).toMatrix(matrix);
            fb.write(matrix.toArrayFloatColOrder(array));
        }
    }
    
    public TBO getTBO() {
        if(jointMatrixTbo==null) buildTBO();
        return jointMatrixTbo;
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        if(jointMatrixTbo!=null){
            jointMatrixTbo.unloadFromGpuMemory(context.getGL());
            jointMatrixTbo = null;
        }
    }
    
}
