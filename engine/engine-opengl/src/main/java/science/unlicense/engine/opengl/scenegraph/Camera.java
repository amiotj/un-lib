
package science.unlicense.engine.opengl.scenegraph;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.geometry.s2d.Rectangle;

/**
 * Collada 1.5 :
 * A camera embodies the eye point of the viewer looking at the visual scene. It is a device that captures
 * visual images of a scene. A camera has a position and orientation in the scene. This is the viewpoint of the
 * camera as seen by the camera’s optics or lens.
 *
 * @author Johann Sorel
 */
public abstract class Camera extends GLNode {

    /**
     * Called by the Rendering context before rendering the scene.
     * @param context
     * @param nanotime
     */
    public abstract void update(RenderContext context, long nanotime, Rectangle renderSize);

}
