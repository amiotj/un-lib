

package science.unlicense.engine.opengl.control;

import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Set;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventMessage;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;

/**
 * Stores the current state of keyboard and mouse.
 * Can support mouse wrap.
 * 
 * 
 * @author Johann Sorel
 */
public class GestureState {
    
    //current event
    public Event currentEvent;
    
    //keyboard state
    public final Set keyboardPressedKeys = new HashSet();
    public int keyboardCodePoint;
    public int keyboardEventType;
    
    //mouse state
    public final Set mousePressedKeys = new HashSet();
    public boolean mouseDrag = false;
    public int mouseButton;
    public int mouseEventType;
    public double wheelScroll;
    
    public double currentMouseX;
    public double currentMouseY;
    public double lastMouseX;
    public double lastMouseY;
    public double mouseDiffX;
    public double mouseDiffY;
    
    
    public void update(Event event){        
        if(event==null) return;
        currentEvent = event;
        
        //reset values
        mouseEventType = 0;
        wheelScroll = 0;

        final EventMessage message = event.getMessage();
        if(message instanceof MouseMessage){
            final MouseMessage e = (MouseMessage) message;
            mouseEventType = e.getType();
            
            updateMouseMove(e);
            if (message.isConsumed()) return;
            

            if(MouseMessage.TYPE_PRESS==mouseEventType){
                mouseButton = e.getButton();
                mouseDrag = true;
            }else if(MouseMessage.TYPE_RELEASE==mouseEventType){
                mouseButton = 0;
                mouseDrag = false;
            }else if(MouseMessage.TYPE_TYPED==mouseEventType){
                mouseButton = e.getButton();
            }else if(MouseMessage.TYPE_MOVE==mouseEventType){
                mouseButton = e.getButton();
            }else if(MouseMessage.TYPE_WHEEL==mouseEventType){
                wheelScroll = e.getWheelOffset();
            }else if(MouseMessage.TYPE_ENTER==mouseEventType){
                mouseButton = e.getButton();
            }else if(MouseMessage.TYPE_EXIT==mouseEventType){
                mouseButton = e.getButton();
            }
            
            if(mouseEventType==MouseMessage.TYPE_PRESS){
                mousePressedKeys.add(e.getButton());
            }else if(mouseEventType==MouseMessage.TYPE_RELEASE){
                mousePressedKeys.remove(e.getButton());
            }

        }else if(message instanceof KeyMessage){
            
            if (message.isConsumed()) return;
            final KeyMessage e = (KeyMessage) message;
            keyboardEventType = e.getType();
            keyboardCodePoint = e.getCodePoint();
            
            if(keyboardEventType==KeyMessage.TYPE_PRESS){
                keyboardPressedKeys.add(keyboardCodePoint);
            }else if(keyboardEventType==KeyMessage.TYPE_RELEASE){
                keyboardPressedKeys.remove(keyboardCodePoint);
            }
            
        }
        
    }
    
    /**
     * Inform gesture state that mouse has warped to given position
     * 
     * @param x
     * @param y 
     */
    public void mouseWarped(double x, double y){
        currentMouseX = x;
        currentMouseY = y;
        lastMouseX = currentMouseX-mouseDiffX;
        lastMouseY = currentMouseY-mouseDiffY;
    }
    
    private void updateMouseMove(MouseMessage e){
        lastMouseX = currentMouseX;
        lastMouseY = currentMouseY;
        currentMouseX = e.getMousePosition().getX();
        currentMouseY = e.getMousePosition().getY();
        mouseDiffX = currentMouseX-lastMouseX;
        mouseDiffY = currentMouseY-lastMouseY;
    }
    
    
}
