

package science.unlicense.engine.opengl.renderer;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.light.PointLight;
import science.unlicense.engine.opengl.light.SpotLight;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Draw a Sphere or a Cone for point and spot lights.
 * TODO
 * 
 * @author Johann Sorel
 */
public class DebugLightRenderer extends AbstractRenderer {
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        if(node instanceof PointLight){
            //draw a sphere            
            
        }else if(node instanceof SpotLight){
            //draw a cone
        }
    }

    public void dispose(GLProcessContext context) {
    }
    
}
