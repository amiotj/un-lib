

package science.unlicense.engine.opengl.operation;

import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.image.Image;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.geometry.operation.Intersects;
import science.unlicense.impl.geometry.operation.IntersectsExecutors;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Grid;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * Calculate a 3D image cube of 1 bit.
 * A bit incidate a triangle intersects the voxel.
 * 
 * @author Johann Sorel
 */
public class GridBuilder {
    
    private final NodeVisitor visitor = new DefaultNodeVisitor(){
        public Object visit(Node node, Object context) {
            
            if(node instanceof Mesh){
                try {
                    append((Mesh)node);
                } catch (OperationException ex) {
                    throw new RuntimeException(ex);
                }
            }
            
            return super.visit(node, context);
        }
    };
    
    private final Grid grid;
    private final Extent imageSize;
    private final Image result;
    private final TupleBuffer sm;
    private final boolean[] sample = new boolean[1];
    private final Affine3 sceneToImage;
    
    //cache for voxel intersection
    private final Vector v0 = new Vector(3);
    private final Vector v1 = new Vector(3);
    private final Vector v2 = new Vector(3);
    private final Triangle triangle = new Triangle(v0, v1, v2);
    private final BBox voxel = new BBox(3);
    private final Intersects intersect = new Intersects(triangle, voxel);
    private final int[] lower = new int[3];
    private final int[] upper = new int[3];

    public GridBuilder(BBox bbox, Extent.Long imageSize) {
        this.grid = new Grid(bbox, imageSize);
        this.imageSize = imageSize;
        this.result = grid.getImage();
        this.sm = result.getRawModel().asTupleBuffer(result);
        this.sceneToImage = grid.getGeomToGrid();
    }

    public Grid getResult() {
        return grid;
    }
    
    public void append(SceneNode node){
        node.accept(visitor, null);
    }
    
    public void append(final Mesh mesh) throws OperationException{
        final Shape shape = mesh.getShape();
        if(!(shape instanceof Shell)) return;
        final Shell shell = (Shell) shape;
        
        //loop on all triangles
        final IntCursor ibuff = shell.getIndexes().getPrimitiveBuffer().cursorInt();
        final VBO vertices = shell.getVertices();
        final float[] v0 = new float[3];
        final float[] v1 = new float[3];
        final float[] v2 = new float[3];
        for(IndexRange r : shell.getModes()){
            final int offset = r.getIndexOffset();
            for(int i=0,n=r.getCount();i<n;i+=3){
                final int idx0 = ibuff.read(i+offset+0);
                final int idx1 = ibuff.read(i+offset+1);
                final int idx2 = ibuff.read(i+offset+2);
                vertices.getTupleFloat(idx0, v0);
                vertices.getTupleFloat(idx1, v1);
                vertices.getTupleFloat(idx2, v2);
                this.v0.set(v0);
                this.v1.set(v1);
                this.v2.set(v2);
                                
                sceneToImage.transform(this.v0, this.v0);
                sceneToImage.transform(this.v1, this.v1);
                sceneToImage.transform(this.v2, this.v2);
                
                testTriangle();
            }
        }
    }
    
    public void appendTriangle(Vector v0, Vector v1, Vector v2) throws OperationException{
        this.v0.set(v0);
        this.v1.set(v1);
        this.v2.set(v2);
        sceneToImage.transform(this.v0, this.v0);
        sceneToImage.transform(this.v1, this.v1);
        sceneToImage.transform(this.v2, this.v2);
        testTriangle();
    }
    
    private void testTriangle() throws OperationException{
        
        //compute the possible intersection voxels
        for(int i=0;i<3;i++){
            lower[i] = (int)Math.floor( Math.min(Math.min(v0.get(i), v1.get(i)), v2.get(i)) );
            upper[i] = (int)Math.ceil( Math.max(Math.max(v0.get(i), v1.get(i)), v2.get(i)) );
                        
            //ensure we don't go out of image
            if(lower[i]<0)lower[i]=0;
            if(upper[i]>imageSize.get(i))upper[i]=(int) imageSize.get(i);
            
        }
        
        //loop on each voxel
        final int[] coord = new int[3];
        for(coord[0]=lower[0];coord[0]<upper[0];coord[0]++){
            for(coord[1]=lower[1];coord[1]<upper[1];coord[1]++){
                for(coord[2]=lower[2];coord[2]<upper[2];coord[2]++){
                    sm.getTuple(coord, sample);
                    
                    //no need to test voxel if it is already intersecting
                    if(!sample[0]){
                        //check for intersection
                        voxel.setRange(0, coord[0], coord[0]+1);
                        voxel.setRange(1, coord[1], coord[1]+1);
                        voxel.setRange(2, coord[2], coord[2]+1);
                        sample[0] = (Boolean) IntersectsExecutors.TRIANGLE_BBOX.execute(intersect);
                        if(sample[0]) sm.setTuple(coord, sample);
                    }
                }
            }
        }
    }
    
}
