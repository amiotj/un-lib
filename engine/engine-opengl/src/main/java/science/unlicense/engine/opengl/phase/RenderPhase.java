
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.gpu.opengl.GL1;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.renderer.Renderer;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 * Rendering phase.
 * 
 * Render on the default frame buffer or in FBO.
 *
 * @author Johann Sorel
 */
public class RenderPhase extends AbstractFboPhase{
    
    private final FlattenVisitor flattener = new FlattenVisitor();
    private final DefaultRenderContext renderContext = new DefaultRenderContext(null, null);
    private CameraMono camera;

    private boolean useBlending = false;
    
    public RenderPhase(GLNode root,CameraMono camera) {
        this(root,camera,null);
    }
    
     /**
     * Default defered rendering, only output the diffuse color in fbo.
     * @param camera
     * @param root
     * @param fbo 
     */
    public RenderPhase(GLNode root,CameraMono camera, FBO fbo) {
        super(fbo);
        this.renderContext.setScene(root);
        this.camera = camera;
    }

    public void setUseBlending(boolean useBlending) {
        this.useBlending = useBlending;
    }

    public boolean isUseBlending() {
        return useBlending;
    }
    
    public CameraMono getCamera() {
        return camera;
    }

    public void setCamera(CameraMono camera) {
        this.camera = camera;
    }

    public GLNode getRoot() {
        return renderContext.getScene();
    }

    public void setRoot(GLNode root) {
        renderContext.setScene(root);
    }
            
    protected void processInternal(GLProcessContext glctx) throws GLException {
        
        renderContext.setProcessContext(glctx);
        
        //update the camera
        final FBO outFbo = getOutputFbo();
        if(outFbo!=null){
            camera.update(renderContext, glctx.getTimeNano(), new Rectangle(0, 0, outFbo.getWidth(), outFbo.getHeight()));
        }else{
            camera.update(renderContext, glctx.getTimeNano(), renderContext.getViewRectangle());
        }
        
        
        final GL1 gl = glctx.getGL().asGL1();
        gl.glSampleCoverage(1.f, true);
        
        
        if(useBlending){
            GLUtilities.configureBlending(gl, AlphaBlending.DEFAULT);
        }else{
            //we must disable blending if we are doing a GBO rendering
            gl.glDisable(GL_BLEND);
        }
        
        //find all nodes, sort them, opaque first, translucent after to reduce artifacts
        flattener.reset();
                
        renderContext.getScene().accept(flattener, renderContext);
        Sequence nodes = flattener.getCollection();
//        Collections.sort(nodes, OPACITY_SORTER);
        //TODO : this works but is really slow
//        final OpacityAndDistanceWorker w = new OpacityAndDistanceWorker(camera);
//        nodes = w.prepare(nodes);
        
        //do the rendering
        for(int i=0,n=nodes.getSize();i<n;i++){
            processRenderers((GLNode)nodes.get(i));
        }

    }

    protected void processRenderers(GLNode glNode){
        final Sequence renderers = glNode.getRenderers();
        for(int i=0,n=renderers.getSize();i<n;i++){
            final Renderer renderer = (Renderer) renderers.get(i);
            if(renderer.getPhasePredicate().evaluate(this)){
                renderer.render(renderContext, camera, glNode);
            }
        }
    }
}