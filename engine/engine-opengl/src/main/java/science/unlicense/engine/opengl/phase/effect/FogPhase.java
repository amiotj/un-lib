
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 *
 * Docs :
 * http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=15
 * http://codeflow.org/entries/2010/dec/09/minecraft-like-rendering-experiments-in-opengl-4/
 * 
 * TODO : improve fog using a half space
 * http://www.terathon.com/lengyel/Lengyel-UnifiedFog.pdf
 * 
 * TODO : fix it to use world position or depth instead of camera position
 * 
 * @author Johann Sorel
 */
public class FogPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_PARAMS = new Chars("fogParams");
    private static final Chars UNIFORM_COLOR = new Chars("fogColor");
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/fog-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final FogActor actor = new FogActor();
    
    private float density = 0.05f;
    private Color color = Color.GRAY_LIGHT;
    
    private Uniform uniParams;
    private Uniform uniColor;
    
    public FogPhase(Texture texture, Texture depthTexture) {
        this(null,texture,depthTexture);
    }
    
    public FogPhase(FBO output, Texture texture, Texture depthTexture) {
        super(output,texture,depthTexture);
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    protected FogActor getActor() {
        return actor;
    }

    private final class FogActor extends DefaultActor{

        public FogActor() {
            super(new Chars("Fog"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if(uniParams == null){
                uniParams = program.getUniform(UNIFORM_PARAMS);
                uniColor = program.getUniform(UNIFORM_COLOR);
            }
            uniParams.setVec4(gl, new float[]{density,density,density,density});
            uniColor.setVec4(gl, color.toRGBAPreMul());
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniParams = null;
            uniColor = null;
        }
        
    }
    
}
