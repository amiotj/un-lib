
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Gamma correction phase.
 *
 * Resources :
 * http://fr.wikipedia.org/wiki/Correction_gamma
 *
 * @author Johann Sorel
 */
public class GammaCorrectionPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_FACTOR = new Chars("gamma");
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/gamma-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final GammaActor actor = new GammaActor();
    private float[] gammaFactor;
    private Uniform uniFactor;

    public GammaCorrectionPhase(Texture texture) {
        this(null,texture,new float[]{2.2f,2.2f,2.2f});
    }
    
    public GammaCorrectionPhase(Texture texture, float[] gammaFactor) {
        this(null,texture,gammaFactor);
    }
    
    public GammaCorrectionPhase(FBO output, Texture texture, float[] gammaFactor) {
        super(output,texture);
        this.gammaFactor = gammaFactor;
    }

    public float[] getGammaFactor() {
        return gammaFactor;
    }

    public void setGammaFactor(float[] bloomFactor) {
        this.gammaFactor = bloomFactor;
    }

    protected GammaActor getActor() {
        return actor;
    }

    private final class GammaActor extends DefaultActor{

        public GammaActor() {
            super(new Chars("Gamma"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if(uniFactor == null){
                uniFactor = program.getUniform(UNIFORM_FACTOR);
            }
            uniFactor.setVec3(gl, gammaFactor);
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniFactor = null;
        }
        
    }
    
}
