
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.exception.UnimplementedException;

/**
 * TODO
 * 
 * @author Johann Sorel
 */
public class AlphaActor extends AbstractActor{

    public AlphaActor() {
        super(null);
    }

    @Override
    public int getMinGLSLVersion() {
        throw new UnimplementedException("Not supported yet.");
    }

}
