
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.physic.AbstractWorld;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.force.Force;
import science.unlicense.api.physic.force.Singularity;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeSequence;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.predicate.ClassPredicate;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * A GL Scene world mapping the scene content.
 *
 * @author Johann Sorel
 */
public class SceneWorld extends AbstractWorld{
    
    /**
     * Visitor to find all rigid bodies in the scene.
     * returns a sequence of rigid bodies.
     */
    public static final NodeVisitor FORCE_VISITOR = new DefaultNodeVisitor(){

        public Sequence visit(Node node, Object context) {
            if(node instanceof MultipartMesh){
                final MultipartMesh mpm = (MultipartMesh) node;
                ((Sequence)context).addAll(mpm.getConstraints());
            }
            super.visit(node, context);
            return (Sequence) context;
        }
    };
    

    private final Sequence singularities = new ArraySequence();
    private final Sequence forces = new ArraySequence();
    private Sequence bodies;
    private Sequence skeletons;
    private GLNode scene;

    public SceneWorld(GLNode scene) {
        super(3);
        this.scene = scene;
        bodies = new NodeSequence(scene, true, new ClassPredicate(RigidBody.class));
        skeletons = new NodeSequence(scene, true, new ClassPredicate(Skeleton.class));
    }
    
    public void setScene(GLNode scene){
        this.scene = scene;
        bodies = new NodeSequence(scene, true, new ClassPredicate(RigidBody.class));
        skeletons = new NodeSequence(scene, true, new ClassPredicate(Skeleton.class));
    }

    public GLNode getScene() {
        return scene;
    }

    public Sequence getSkeletons() {
        return skeletons;
    }

    public Sequence getSingularities() {
        return singularities;
    }

    public Sequence getBodies() {
        return bodies;
    }

    public void addSingularity(Singularity singularity) {
        singularities.add(singularity);
    }

    public void removeSingularity(Singularity singularity) {
        singularities.remove(singularity);
    }

    public void addBody(Body body) {
        throw new UnimplementedException("Not supported.");
    }

    public void removeBody(Body body) {
        throw new UnimplementedException("Not supported.");
    }

    public Sequence getForces() {
        final Sequence seq = new ArraySequence();
        //defensive copy
        GLNode cp = scene;
        if(cp!=null){
            cp.accept(FORCE_VISITOR, seq);
            seq.addAll(forces);
        }
        return seq;
    }

    public void addForce(Force force) {
        forces.add(force);
    }

    public void removeForce(Force force) {
        forces.remove(force);
    }

    public void removeAll() {
        throw new UnimplementedException("Not supported.");
    }

}
