
package science.unlicense.engine.opengl.painter.gl3;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.engine.opengl.painter.gl3.task.BlitImage;
import science.unlicense.engine.opengl.painter.gl3.task.Dispose;
import science.unlicense.engine.opengl.painter.gl3.task.LoadImage;
import science.unlicense.engine.opengl.painter.gl3.task.Resize;
import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.Texture2DMS;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 * OpenGL 2D painter implementation.
 *
 * @author Johann Sorel
 */
public class GL3ImagePainter2D extends GL3Painter2D implements ImagePainter2D {

    private final GLSource source;
    private final FBO fbo;

    public GL3ImagePainter2D(int width, int height) {
        this(width,height,0);
    }

    public GL3ImagePainter2D(int width, int height, int multisample) {
        this( createFBO(width, height, multisample));
    }

    private static FBO createFBO(int width, int height, int multisample){
        final FBO fbo = new FBO(width, width);
        if(multisample>0){
            fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2DMS(width, height,Texture2DMS.COLOR_RGBA(),multisample));
            fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2DMS(width, height,Texture2DMS.DEPTH24_STENCIL8(),multisample));
        }else{
            fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
            fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));
        }
        return fbo;
    }
    
    /**
     * GLPainter 2D, providing a drawable.
     * If drawable is not nul, then ensure the flush method is called in the main
     * event loop.
     *
     * @param fbo
     */
    public GL3ImagePainter2D(FBO fbo) {
        super(fbo.getWidth(), fbo.getHeight(), fbo.getMultiSamplingValue());

        this.fbo = fbo;
        this.source = GLUtilities.createOffscreenSource(1, 1);

        //listen to source events
        this.source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                GL3ImagePainter2D.this.flush(GL3ImagePainter2D.this.fbo,source);
            }
            @Override
            public void dispose(GLSource source) {
            }
        });
    }

    public FBO getFbo() {
        return fbo;
    }

    @Override
    public synchronized Image getImage(){
        final Texture texture = fbo.getColorTexture();
        final LoadImage task = new LoadImage(texture);
        addTask(task);
        flush();
        return task.getImage();
    }

    public synchronized Texture2D blitTexture(Texture2D blitTexture){
        final Texture texture = fbo.getColorTexture();
        if(!(texture instanceof Texture2DMS)){
            throw new IllegalStateException("Painter must have multisampling enable to use blit operation.");
        }
        final BlitImage task = new BlitImage((Texture2DMS) texture,blitTexture);
        addTask(task);
        flush();
        return task.getBlit();
    }

    public Image getDepthStencilImage(){
        final Texture texture = fbo.getDepthStencilTexture();
        if(texture instanceof Texture2DMS){
            throw new UnimplementedException("todo");
//            final GL3ImagePainter2D painter = new GL3ImagePainter2D(
//                    (int)texture.getExtent().getL(0), (int)texture.getExtent().getL(1),0, source);
//            painter.paint(texture, null);
//            painter.flush();
//            Image image = painter.getImage();
//            painter.dispose();
//            return image;
        }else{
            final LoadImage task = new LoadImage(texture);
            addTask(task);
            flush();
            return task.getImage();
        }

    }

    public void flush() {
        source.render();
    }

    public void dispose(){
        addTask(new Dispose());
        flush();
        source.dispose();
    }

}
