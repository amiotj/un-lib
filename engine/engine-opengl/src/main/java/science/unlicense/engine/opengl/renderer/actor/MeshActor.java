
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.system.path.Paths;

/**
 * Base mesh shader actor.
 *
 * @author Johann Sorel
 */
public final class MeshActor {

    public static final Chars UNIFORM_M = new Chars("UNI_M");
    public static final Chars UNIFORM_V = new Chars("UNI_V");
    public static final Chars UNIFORM_P = new Chars("UNI_P");
    public static final Chars UNIFORM_PIXELSIZE = new Chars("UNI_PX");
    public static final Chars OP_SET_M = new Chars("    M = UNI_M;");
    public static final Chars OP_SET_V = new Chars("    V = UNI_V;");

    public static final Actor INSTANCE;
    public static final ShaderTemplate TEMPLATE_VE;
    public static final ShaderTemplate TEMPLATE_TC;
    public static final ShaderTemplate TEMPLATE_TE;
    public static final ShaderTemplate TEMPLATE_GE;
    public static final ShaderTemplate TEMPLATE_FR;
    
    static {
        try{
            TEMPLATE_VE = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/mesh-base-0-ve.glsl")),ShaderTemplate.SHADER_VERTEX);
            TEMPLATE_TC = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/mesh-base-1-tc.glsl")),ShaderTemplate.SHADER_TESS_CONTROL);
            TEMPLATE_TE = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/mesh-base-2-te.glsl")),ShaderTemplate.SHADER_TESS_EVAL);
            TEMPLATE_GE = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/mesh-base-3-ge.glsl")),ShaderTemplate.SHADER_GEOMETRY);
            TEMPLATE_FR = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/mesh-base-4-fr.glsl")),ShaderTemplate.SHADER_FRAGMENT);
            INSTANCE = new DefaultActor(
                new Chars("MeshBase"), true,
                TEMPLATE_VE,
                TEMPLATE_TC,
                TEMPLATE_TE,
                TEMPLATE_GE,
                TEMPLATE_FR,
                true, true){

                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        //set pixel size
                        final Uniform uni = program.getUniform(UNIFORM_PIXELSIZE);
                        uni.setVec2(context.getGL().asGL2ES2(), new float[]{
                            1.0f/(float)context.getViewRectangle().getWidth(),
                            1.0f/(float)context.getViewRectangle().getHeight()});
                    }
                
                };

        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private MeshActor(){}

}
