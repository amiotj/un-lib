
package science.unlicense.engine.opengl.animation;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Before calling the display method on the GL scene.
 * A first iteration if made on all scene node, notifying the associated Updaters of each node.
 * This can be used for animations or informations display (FPS,...).
 *
 * @author Johann Sorel
 */
public interface Updater {

    void update(RenderContext context, GLNode node);

}
