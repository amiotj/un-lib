
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.number.Int32;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.AbstractActor;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.image.process.ConvolutionMatrix;

/**
 * Convolution operation shader.
 *
 * @author Johann Sorel
 */
public class ConvolutionPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/convolution-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final ConvolutionMatrix matrix;
    private final ConvolutionActor actor = new ConvolutionActor();

    public ConvolutionPhase(Texture texture, ConvolutionMatrix matrix) {
        this(null,texture,matrix);
    }

    public ConvolutionPhase(FBO output, Texture texture, ConvolutionMatrix matrix) {
        super(output,texture);
        this.matrix = matrix;
    }
    
    public ConvolutionMatrix getMatrix() {
        return matrix;
    }

    protected ConvolutionActor getActor() {
        return actor;
    }


    private final class ConvolutionActor extends AbstractActor{

        public ConvolutionActor() {
            super(null);
        }

        public int getMinGLSLVersion() {
            return SHADER_FR.getMinGLSLVersion();
        }
        
        public void initProgram(RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
            super.initProgram(ctx, template, tess, geom);
            final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();

            final float[] vals = matrix.toArrayFloat();

            final CharBuffer cb = new CharBuffer();
            for(int i=0;i<vals.length;i++){
                if(i!=0) cb.append(',');
                cb.appendNumber(vals[i]);
            }

            ShaderTemplate copy = new ShaderTemplate(ShaderTemplate.SHADER_FRAGMENT);
            copy.append(SHADER_FR);
            copy.replaceTexts(new Chars("$vals"), cb.toChars());
            copy.replaceTexts(new Chars("$radius"), Int32.encode(matrix.getNbCol()/2));
            copy.replaceTexts(new Chars("$size"), Int32.encode(matrix.getNbCol()*matrix.getNbRow()));

            fragmentShader.append(copy);
        }
        
    }
    
}
