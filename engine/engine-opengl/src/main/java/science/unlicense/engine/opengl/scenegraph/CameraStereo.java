
package science.unlicense.engine.opengl.scenegraph;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;

/**
 * Camera managing 2 mono cameras for left and right eyes.
 * 
 * There are multiple approachs for stereoscopic effect, this class handle only :
 * - Toe-In
 * - Asymetric
 * 
 * Docs (many available on the web) :
 * http://paulbourke.net/stereographics/stereorender/
 * http://www.orthostereo.com/geometryopengl.html
 * http://developer.download.nvidia.com/presentations/2009/GDC/GDC09-3DVision-The_In_and_Out.pdf
 * ...
 * 
 * @author Johann Sorel
 */
public class CameraStereo extends AbstractCamera {

    public static final int MODE_TOEIN = 0;
    public static final int MODE_ASYMETRIC = 1;
        
    private final CameraEye leftEye;
    private final CameraEye rightEye;
    private double eyesDistance;
    private double convergenceDistance;
    private int mode;

    /**
     * 
     * @param rightHanded is scene using a right handed coordinate system.
     */
    public CameraStereo(boolean rightHanded) {
        this(rightHanded,1);
    }
    
    /**
     * 
     * @param rightHanded is scene using a right handed coordinate system.
     * @param eyesDistance distance between eyes.
     */
    public CameraStereo(boolean rightHanded, double eyesDistance) {
        this(rightHanded,1,10);
    }
    
    /**
     * 
     * @param rightHanded is scene using a right handed coordinate system.
     * @param eyesDistance distance between eyes.
     * @param convergenceDistance distance of the convergence plan of left and right eyes
     */
    public CameraStereo(boolean rightHanded, double eyesDistance, double convergenceDistance) {
        super(rightHanded,true);
        this.leftEye = new CameraEye(rightHanded);
        this.rightEye = new CameraEye(rightHanded);
        this.eyesDistance = eyesDistance;
        this.convergenceDistance = convergenceDistance;
        this.mode = MODE_ASYMETRIC;
        
        getChildren().add(this.leftEye);
        getChildren().add(this.rightEye);
    }

    /**
     * Get camera for the left eye.
     * @return CameraMono
     */
    public CameraMono getLeftCamera() {
        return leftEye;
    }
    
    /**
     * Get camera for the right eye.
     * @return CameraMono
     */
    public CameraMono getRightCamera() {
        return rightEye;
    }

    /**
     * 
     * @return distance between eyes
     */
    public double getEyesDistance() {
        return eyesDistance;
    }

    /**
     * Set distance between eyes.
     * @param eyesDistance new distance
     */
    public void setEyesDistance(double eyesDistance) {
        this.eyesDistance = eyesDistance;
        setDirty(true);
    }

    /**
     * Convergence distance, also called focal length.
     * 
     * @return convergence distance
     */
    public double getConvergenceDistance() {
        return convergenceDistance;
    }

    /**
     * 
     * @param convergenceDistance 
     */
    public void setConvergenceDistance(double convergenceDistance) {
        this.convergenceDistance = convergenceDistance;
        setDirty(true);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
    
    protected void calculateMatrices() {
        //center point projection matrix update
        //this may be used by process who do not requiere stereo separation
        super.calculateMatrices();
        
        //set the left and right eye as not dirty, we will update them here.
        leftEye.setDirty(false);
        rightEye.setDirty(false);
        
        //ensure the left and right camera are at proper distance
        getRightAxis().scale(-eyesDistance/2.0,leftEye.getNodeTransform().getTranslation());
        getRightAxis().scale(eyesDistance/2.0,rightEye.getNodeTransform().getTranslation());
        
        if(true/*mode==MODE_TOEIN*/){
            //this mode focuses on a single point
            //not a very nice stereo effect due to vertical parallax
            
            //update the cameras direction
            final Vector target = getForward();
            target.localScale(convergenceDistance);
            double[][] m = Matrices.lookAt(leftEye.getNodeTransform().getTranslation(),target, getUpAxis(), null);
            leftEye.getNodeTransform().getRotation().set(m);
            m = Matrices.lookAt(rightEye.getNodeTransform().getTranslation(),target, getUpAxis(), null);
            rightEye.getNodeTransform().getRotation().set(m);
            
            //update the cameras projection
            leftEye.getProjectionMatrixInternal().set(this.projectionMatrix);
            rightEye.getProjectionMatrixInternal().set(this.projectionMatrix);
            
        }else{
            //TODO
        }
        
        leftEye.getNodeTransform().notifyChanged();
        rightEye.getNodeTransform().notifyChanged();
    }
    
    private class CameraEye extends CameraMono{

        public CameraEye(boolean rightHanded) {
            super(rightHanded);
        }

        public int getCameraType() {
            return cameraType;
        }

        public double getFarPlane() {
            return CameraStereo.this.getFarPlane();
        }

        public void setFarPlane(double farPlane) {
            CameraStereo.this.setFarPlane(farPlane);
        }
        
        public double getFieldOfView() {
            return CameraStereo.this.getFieldOfView();
        }

        @Override
        public void setFieldOfView(double fieldOfView) {
            CameraStereo.this.setFieldOfView(fieldOfView);
        }
        
        public double getNearPlane() {
            return CameraStereo.this.getNearPlane();
        }

        public void setNearPlane(double nearPlane) {
            CameraStereo.this.setNearPlane(nearPlane);
        }

        public Vector getRightAxis() {
            return CameraStereo.this.getRightAxis();
        }

        public void setRightAxis(Vector value) {
            CameraStereo.this.setRightAxis(value);
        }

        public Vector getUpAxis() {
            return CameraStereo.this.getUpAxis();
        }

        public void setUpAxis(Vector value) {
            CameraStereo.this.setUpAxis(value);
        }
        
        protected void calculateMatrices() {
            CameraStereo.this.calculateMatrices();
        }

        private Matrix4x4 getProjectionMatrixInternal() {
            return projectionMatrix;
        }

        public void update(RenderContext context, long nanotime, Rectangle renderSize) {
            //do nothing, parent stereo camera handle it
        }
        
    }
    
}
