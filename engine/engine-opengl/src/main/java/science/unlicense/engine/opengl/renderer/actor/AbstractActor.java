

package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;

/**
 * Shader actor doing nothing.
 *
 * @author Johann Sorel
 */
public abstract class AbstractActor implements Actor{

    /**
     * Keep the current program state.
     * This is to ensure the actor is not used in multiple programs
     * without being disposed between them.
     */
    protected boolean initialized = false;
    protected final boolean concurrent;
    private final Chars reuseId;
        
    public AbstractActor(Chars reuseId) {
        this(reuseId,false);
    }
    
    public AbstractActor(Chars reuseId, boolean concurrent) {
        this.reuseId = reuseId;
        this.concurrent = concurrent;
    }
    
    public Chars getReuseUID() {
        return reuseId;
    }

    public boolean isDirty() {
        return false;
    }

    public boolean usesTesselationShader() {
        return false;
    }

    public boolean usesGeometryShader() {
        return false;
    }

    public void initProgram(RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom) {
        if(concurrent) return;
        if(initialized) throw new RuntimeException("This actor has been initialized by another program but not disposed before reuse.");
        initialized = true;
    }

    public void preExecutionGL(RenderContext context, Object candidate) {
    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
    }

    public void dispose(GLProcessContext context) {
        initialized = false;
    }
    
}

