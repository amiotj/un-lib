

package science.unlicense.engine.opengl.control;

import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 * Enforce rotation only on horizontal and vertical axes.
 * This updater will remove any roll element.
 * 
 * 
 * @author Johann Sorel
 */
public class PlayerOrientationConstraint implements Updater {
    
    private final double minH;
    private final double maxH;
    private final double minV;
    private final double maxV;

    /**
     * 
     * @param minH min horizontale angle, NaN for none, in radians
     * @param maxH max horizontale angle, NaN for none, in radians
     * @param minV min vertical angle, NaN for none, in radians
     * @param maxV max vertical angle, NaN for none, in radians
     */
    public PlayerOrientationConstraint(double minH, double maxH, double minV, double maxV) {
        this.minH = minH;
        this.maxH = maxH;
        this.minV = minV;
        this.maxV = maxV;
    }
    
    public void update(RenderContext context, GLNode node) {
        final CameraMono camera = (CameraMono) node;

        Matrix3x3 m = new Matrix3x3(node.getNodeTransform().getRotation());
        Vector forward = new Vector(0, 0, 1);
        Vector ref = (Vector) m.transform(forward);

        double horizontalAngle = forward.shortestAngle(new Vector(ref.getX(), 0, ref.getZ()).localNormalize());
        if (ref.getX() < 0) {
            horizontalAngle = -horizontalAngle;
        }
        if(!Double.isNaN(minH) && !Double.isNaN(maxH)){
            horizontalAngle = Maths.clamp(horizontalAngle, minH, maxH);
        }

        double verticalAngle = ref.shortestAngle(new Vector(ref.getX(), 0, ref.getZ()).localNormalize());
        if (ref.getY() < 0) {
            verticalAngle = -verticalAngle;
        }
        if(!Double.isNaN(minV) && !Double.isNaN(maxV)){
            verticalAngle = Maths.clamp(verticalAngle, minV, maxV);
        }

        //rebuild matrix
        m.setToIdentity();
        if (!Double.isNaN(horizontalAngle)) {
            final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, camera.getUpAxis());
            m.localMultiply(mh);
        }
        if (!Double.isNaN(verticalAngle)) {
            final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, camera.getRightAxis());
            m.localMultiply(mv);
        }
        node.getNodeTransform().getRotation().set(m);
        node.getNodeTransform().notifyChanged();
    }
    
}
