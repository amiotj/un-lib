
package science.unlicense.engine.opengl;

import science.unlicense.engine.opengl.light.Light;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;

/**
 * OpenGL rendering context utilities.
 * 
 * @author Johann Sorel
 */
public class ContextUtilities {
    
    /**
     * Visitor to find all lights in the scene.
     * returns a sequence of lights.
     */
    public static final NodeVisitor LIGHTS_VISITOR = new DefaultNodeVisitor(){

        public Sequence visit(Node node, Object context) {
            if(node instanceof Light){
                ((Sequence)context).add(node);
            }
            return (Sequence) super.visit(node, context);
        }
    };
        
    
    
}
