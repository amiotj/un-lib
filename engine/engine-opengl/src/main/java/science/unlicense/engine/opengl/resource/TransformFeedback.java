
package science.unlicense.engine.opengl.resource;

import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.gpu.opengl.resource.AbstractResource;
import science.unlicense.impl.gpu.opengl.resource.ResourceException;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES3;
import static science.unlicense.api.gpu.opengl.GLC.*;

/**
 * OpenGL constraints : GL2ES3
 * 
 * @author Johann Sorel
 */
public class TransformFeedback extends AbstractResource {

    private final Sequence resultVBO = new ArraySequence();
    
    //gpu id
    private final int[] bufferId = new int[]{-1};
    private final int[] queryId = new int[]{-1};
    
    public TransformFeedback() {
    }

    public Sequence getResultVBO() {
        return resultVBO;
    }
    
    public int getGpuID() {
        return bufferId[0];
    }

    public boolean isOnSystemMemory() {
        for(int i=0,n=resultVBO.getSize();i<n;i++){
            if(!((VBO)resultVBO.get(i)).isOnSystemMemory()){
                return false;
            }
        }
        return true;
    }

    public boolean isOnGpuMemory() {
        return bufferId[0] != -1;
    }

    public void loadOnSystemMemory(GL gl) throws ResourceException {
        for(int i=0,n=resultVBO.getSize();i<n;i++){
            ((VBO)resultVBO.get(i)).loadOnSystemMemory(gl);
        }
    }

    public void loadOnGpuMemory(GL gl) throws ResourceException {
        if(bufferId[0]!=-1) return; //already loaded
        
        for(int i=0,n=resultVBO.getSize();i<n;i++){
            ((VBO)resultVBO.get(i)).loadOnGpuMemory(gl);
        }
        
        final GL2ES3 gl2 = gl.asGL2ES3();
        gl2.glGenTransformFeedbacks(bufferId);
        gl2.glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, bufferId[0]);
        for(int i=0,n=resultVBO.getSize();i<n;i++){
            final VBO vbo = ((VBO)resultVBO.get(i));
            gl2.glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo.getGpuID());
        }
        gl2.glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
        gl2.glGenQueries(queryId);
    }

    public void unloadFromSystemMemory(GL gl) throws ResourceException {
        for(int i=0,n=resultVBO.getSize();i<n;i++){
            ((VBO)resultVBO.get(i)).unloadFromSystemMemory(gl);
        }
    }

    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        for(int i=0,n=resultVBO.getSize();i<n;i++){
            ((VBO)resultVBO.get(i)).unloadFromGpuMemory(gl);
        }
    }
    
    public void bind(GL gl){
        gl.asGL2ES3().glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, getGpuID());
    }
    
    public void unbind(GL gl){
        gl.asGL2ES3().glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
    }
    
    /**
     * 
     * @param gl
     * @param primitiveMode one of GLC.TRANSFORM_FEEDBACK.PRIMITIVE.*
     */
    public void begin(GL gl, int primitiveMode){
        gl.asGL2ES3().glBeginTransformFeedback(primitiveMode);
    }
        
    public void pause(GL gl){
        gl.asGL2ES3().glPauseTransformFeedback();
    }
    
    public void resume(GL gl){
        gl.asGL2ES3().glResumeTransformFeedback();
    }
    
    public void end(GL gl){
        gl.asGL2ES3().glEndTransformFeedback();
    }
    
    public void beginQuery(GL gl){
        gl.asGL2ES3().glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, queryId[0]);
    }
    
    public void endQuery(GL gl){
        gl.asGL2ES3().glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
    }
    
}
