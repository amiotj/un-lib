
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Bloom effect.
 * 
 * Acts as a blur for bright fragments.
 *
 * @author Johann Sorel
 */
public class BloomPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_FACTOR = new Chars("bloomFactor");
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/bloom-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final BloomActor actor = new BloomActor();
    private float bloomFactor;
    private Uniform uniFactor;

    public BloomPhase(Texture texture) {
        this(null,texture,0.2f);
    }
    
    public BloomPhase(Texture texture, float bloomFactor) {
        this(null,texture,bloomFactor);
    }
    
    public BloomPhase(FBO output, Texture texture, float bloomFactor) {
        super(output,texture);
        this.bloomFactor = bloomFactor;
    }

    public float getBloomFactor() {
        return bloomFactor;
    }

    public void setBloomFactor(float bloomFactor) {
        this.bloomFactor = bloomFactor;
    }

    protected BloomActor getActor() {
        return actor;
    }

    private final class BloomActor extends DefaultActor{

        public BloomActor() {
            super(new Chars("Bloom"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if(uniFactor == null){
                uniFactor = program.getUniform(UNIFORM_FACTOR);
            }
            uniFactor.setFloat(gl, bloomFactor);
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniFactor = null;
        }
        
    }
    
}
