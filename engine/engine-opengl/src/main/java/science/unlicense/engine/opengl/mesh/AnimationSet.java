
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.anim.Animation;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public class AnimationSet {

    private final Sequence morphs = new ArraySequence();
       
    /**
     * List animation names.
     * @return Chars[] never null
     */
    public Chars[] getAnimationNames(){
        final Chars[] names = new Chars[morphs.getSize()];
        for(int i=0;i<names.length;i++){
            final MorphTarget mf = (MorphTarget) morphs.get(i);
            names[i] = mf.getName();
        }
        return names;
    }
    
    /**
     * Get animation with given name.
     * @param name searched frame name
     * @return MorphFrame or null if no frame with this name exist
     */
    public Animation getAnimation(Chars name){
        for(int i=0,n=morphs.getSize();i<n;i++){
            final Animation mf = (Animation) morphs.get(i);
            if(name.equals(mf.getName())){
                return mf;
            }
        }
        return null;
    }
    
    /**
     * Modifiable sequence of all animations.
     * @return Sequence never null, can be empty
     */
    public Sequence getAnimations() {
        return morphs;
    }
    
}
