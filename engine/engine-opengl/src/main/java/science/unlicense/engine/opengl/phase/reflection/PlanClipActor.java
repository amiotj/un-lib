

package science.unlicense.engine.opengl.phase.reflection;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Actor which discard fragments under the given plan equation.
 * 
 * @author Johann Sorel
 */
public class PlanClipActor extends DefaultActor {

    private static final ShaderTemplate PLANECLIP_FR;
    private static final Chars CLIP_PLAN = new Chars("CLIP_PLAN");
    static {
        try{
            PLANECLIP_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/reflectionclip-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final ReflectionVisitor visitor;
    private final float[] planEquation;
    private Uniform planClip;
    
    public PlanClipActor(ReflectionVisitor visitor)  {
        super(new Chars("PlanClip"), false,null, null, null, null, PLANECLIP_FR, true, true);
        this.visitor = visitor;
        this.planEquation = null;
    }

    public PlanClipActor(float[] planEquation)  {
        super(new Chars("PlanClip"), false,null, null, null, null, PLANECLIP_FR, true, true);
        this.visitor = null;
        this.planEquation = planEquation;
    }
    
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        
        if(planClip==null){
            planClip = program.getUniform(CLIP_PLAN);
        }
        if(planEquation!=null){
            planClip.setVec4(context.getGL().asGL2ES2(), planEquation);
        }else{
            final float[] pe = visitor.getPlanEquation();
            planClip.setVec4(context.getGL().asGL2ES2(), pe);
        }
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        planClip = null;
    }
    
}
