
package science.unlicense.engine.opengl.renderer.actor;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.mapping.DualParaboloidMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Dual paraboloid texturing shader actor.
 * 
 * @author Johann Sorel
 */
public class TextureDualParaboloidActor extends AbstractMaterialValueActor{


    private static final ShaderTemplate DIFFUSE_TEXTURE_VE;
    private static final ShaderTemplate DIFFUSE_TEXTURE_TC;
    private static final ShaderTemplate DIFFUSE_TEXTURE_TE;
    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/dualparaboloidtex-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            DIFFUSE_TEXTURE_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/dualparaboloidtex-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            DIFFUSE_TEXTURE_TE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/dualparaboloidtex-2-te.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/dualparaboloidtex-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final DualParaboloidMapping mapping;


    //GL loaded informations
    private int[] reservedTextureFront;
    private int[] reservedTextureBack;
    private Uniform uniformFront;
    private Uniform uniformBack;
    
    public TextureDualParaboloidActor(Chars produce, Chars method, Chars uniquePrefix, DualParaboloidMapping mapping) {
        super(new Chars("DualParaboloidMapping"),false,produce, method, uniquePrefix,
                DIFFUSE_TEXTURE_VE,DIFFUSE_TEXTURE_TC,DIFFUSE_TEXTURE_TE,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }
    
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Texture2D textureFront = mapping.getTextureFront();
        final Texture2D textureBack = mapping.getTextureBack();
        //load texture, image may have changed, no effect if already loaded
        textureFront.loadOnGpuMemory(gl);
        textureBack.loadOnGpuMemory(gl);
        
        // Bind textures
        if(uniformFront == null){
            uniformFront = program.getUniform(uniquePrefix.concat(new Chars("tex0")));
            uniformBack = program.getUniform(uniquePrefix.concat(new Chars("tex1")));
        }
        //front texture
        reservedTextureFront = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTextureFront[0]);
        textureFront.bind(gl);
        uniformFront.setInt(gl, reservedTextureFront[1]);
        //back texture
        reservedTextureBack = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTextureBack[0]);
        textureBack.bind(gl);
        uniformBack.setInt(gl, reservedTextureBack[1]);
        
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        gl.asGL1().glActiveTexture(reservedTextureFront[0]);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTextureFront[0]);
        
        gl.asGL1().glActiveTexture(reservedTextureBack[0]);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTextureBack[0]);
        
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniformFront = null;
        uniformBack = null;
    }
    
}
