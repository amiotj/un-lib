
package science.unlicense.engine.opengl.light;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;

/**
 * Point light.
 * Position is provided by the GLNode matrix.
 * 
 * Colladata 1.5 :
 * A point light source radiates light in all directions from a known location in space. 
 * The intensity of a point light source is attenuated as the distance to the light source increases.
 * 
 * @author Johann Sorel
 */
public class PointLight extends Light{
    
    //attenuation
    private Attenuation attenuation;

    public PointLight() {
        this(Color.WHITE,Color.WHITE,new Attenuation(1f, 1f, 1f));
    }

    public PointLight(Color diffuse, Color specular, final Attenuation attenuation) {
        super(diffuse,specular);
        setName(new Chars("Point light"));
        this.attenuation = attenuation;
    }

    public Tuple getPosition(){
        return getRootToNodeSpace().transform(new DefaultTuple(0, 0, 0),null);
    }
    
    public Attenuation getAttenuation() {
        return attenuation;
    }

    public void setAttenuation(Attenuation attenuation) {
        this.attenuation = attenuation;
    }

}
