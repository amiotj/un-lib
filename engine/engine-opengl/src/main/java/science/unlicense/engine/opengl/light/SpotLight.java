
package science.unlicense.engine.opengl.light;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;

/**
 * Spot light.
 * Position and Direction are provided by the GLNode matrix.
 * 
 * Collada 1.5 :
 * A spot light source radiates light in one direction in a cone shape from a known location in space. The
 * intensity of the light is attenuated as the radiation angle increases away from the direction of the light
 * source. The intensity of a spot light source is also attenuated as the distance to the light source increases.
 * 
 * @author Johann Sorel
 */
public class SpotLight extends PointLight{
    
    // light cone angle
    private float falloffAngle;
    private float falloffExponent;

    public SpotLight() {
        this(Color.WHITE,Color.WHITE, new Attenuation(0.001f,  0.001f,  0.001f), 20f,10f);
    }

    public SpotLight(Color diffuse, Color specular, final Attenuation attenuation, 
            final float falloffAngle, final float falloffExponent) {
        super(diffuse,specular,attenuation);
        setName(new Chars("Spot light"));
        this.falloffAngle = falloffAngle;
        this.falloffExponent = falloffExponent;
    }

    public Tuple getWorldSpaceDirection() {
        final Affine rootToNode = getNodeToRootSpace();
        Vector forward = (Vector) rootToNode.toMatrix().transform(new Vector(0, 0, -1, 0), new Vector(4));
        return forward.getXYZ().localNormalize();
    }

    public float getFallOffAngle() {
        return falloffAngle;
    }

    public void setFallOffAngle(float angle) {
        this.falloffAngle = angle;
    }
    
    public float getFallOffExponent() {
        return falloffExponent;
    }

    public void setFallOffExponent(float exp) {
        this.falloffExponent = exp;
    }
    
}
