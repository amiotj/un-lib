
package science.unlicense.engine.opengl.phase.picking;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.image.Image;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;

/**
 *
 * @author Johann Sorel
 */
public class PickResetPhase extends AbstractFboPhase {

    /**
     * Indicate nothing is at this position,
     */
    public static final int NONE = 0;
    /** Indicate there was on object at this pixel position, 
     * it was not pickable */
    public static final int OCCLUSION = 1;

    
    //next color
    private final Dictionary index = new HashDictionary();
    private int inc = 2;

    private final Sequence pickQueries = new ArraySequence();
    private final Texture2D meshPicktexture;
    private final Texture2D vertexPicktexture;

    public PickResetPhase(Texture2D meshPicktexture, Texture2D vertexPicktexture) {
        this.meshPicktexture = meshPicktexture;
        this.vertexPicktexture = vertexPicktexture;
    }
    
    public void pickAt(Tuple position,EventListener listener){
        pickQueries.add(new Object[]{position,listener});
    }
    
    public void reset(){
        inc = 2;
        index.removeAll();
    }
    
    public Mesh get(int value){
        return (Mesh) index.getValue(value);
    }
        
    public int next(Mesh mesh){
        //calculate this mesh id and store it in the index
        if(mesh.isPickable()){
            inc++;
            index.add(inc, mesh);
            return inc;
        }else{
            //render it as occlusion
            return OCCLUSION;
        }
    }
    
    protected void processInternal(GLProcessContext ctx) throws GLException {
        
        if(!pickQueries.isEmpty()){
            meshPicktexture.loadOnSystemMemory(ctx.getGL());
            final Image meshImage = meshPicktexture.getImage();
            Image vertexImage = null;
            if(vertexPicktexture!=null){
                vertexPicktexture.loadOnSystemMemory(ctx.getGL());
                vertexImage = vertexPicktexture.getImage();
            }
            
            //answer the pick requests
            final TupleBuffer mb = meshImage.getRawModel().asTupleBuffer(meshImage);
            final Iterator ite = pickQueries.createIterator();
            while(ite.hasNext()){
                final Object[] array = (Object[]) ite.next();
                final Tuple mousePose = (Tuple) array[0];
                final EventListener lst = (EventListener) array[1];
                final int[] color = new int[1];
                mb.getTupleInt(new int[]{(int)mousePose.getX(),
                    (int)meshPicktexture.getExtent().getL(1) - (int)mousePose.getY()}, color);
                final GLNode node = (GLNode) get(color[0]);
                
                int vid = 0;
                if(vertexImage!=null){
                    final TupleBuffer vb = vertexImage.getRawModel().asTupleBuffer(vertexImage);
                    vb.getTupleInt(new int[]{(int)mousePose.getX(),
                        (int)meshPicktexture.getExtent().getL(1) - (int)mousePose.getY()}, color);
                    vid = color[0];
                }
                lst.receiveEvent(new Event(this, new PickMessage(node,vid)));
            }
            pickQueries.removeAll();
        }
        
        reset();
    }
    
}
