

package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.impl.gpu.opengl.GLException;

/**
 * Light scattering (god ray).
 *
 * Resources :
 * http://http.developer.nvidia.com/GPUGems3/gpugems3_ch13.html
 *
 * 
 * @author Johann Sorel
 */
public class LightScatteringPhase extends RenderPhase {

    private Mesh light;

    private static final Material BLACK = new Material();
    static {
        BLACK.setDiffuse(Color.BLACK);
    }

    public LightScatteringPhase() {
        super(null, null, null);
    }

    @Override
    protected void processInternal(GLProcessContext glctx) throws GLException {
        super.processInternal(glctx);
        //at this stage the fbo contains the scene with only the light rendered
    }

//    protected ActorProgram getOrCreateProgram(RenderContext context, Mesh mesh) {
//
//        //build a default program
//        ActorProgram program = (ActorProgram) mesh.programs.getValue(programId);
//        if(program == null){
//            program = buildProgram(mesh,context, mesh.getShape(), mesh.getTessellator(), BLACK, null);
//            mesh.programs.add(programId, program);
//        }
//
//        return program;
//    }
//
//    public ActorProgram buildProgram(Mesh mesh,RenderContext ctx, Shape shape,
//        Tessellator tessellator, Material material, Sequence extActors) {
//
//        //build the final collector
//        final FragmentCollectorActor collector = new FragmentCollectorActor(mappings);
//
//        //build material actor
//        final MaterialActor materialActor = null;
//
//        //build shell actor
//        final Actor shellActor = Actors.buildActor(mesh, shape);
//
//        final ActorProgram program = new ActorProgram();
//        final Sequence actors = program.getActors();
//
//        //build morph actor if present
//        if(shape instanceof Shell){
//            final MorphSet ms = ((Shell)shape).getMorphs();
//            if(ms!=null){
//                actors.add(new MorphActor(ms));
//            }
//        }
//
//        actors.add(shellActor);
//        if(tessellator!=null) actors.add(tessellator);
//        if(material!=null) actors.add(materialActor);
//        //check additional actors
//        if(extActors!=null) actors.addAll(extActors);
//        actors.add(collector);
//
//        program.buildProgram(ctx);
//
//        return program;
//    }

}
