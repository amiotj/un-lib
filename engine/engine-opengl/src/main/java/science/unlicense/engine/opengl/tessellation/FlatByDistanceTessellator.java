
package science.unlicense.engine.opengl.tessellation;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.renderer.actor.AbstractActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;

/**
 * Subdivides the mesh triangle, keeping them on the triangle surface.
 *
 * @author Johann Sorel
 */
public class FlatByDistanceTessellator extends AbstractActor implements Tessellator{

    private static final ShaderTemplate SHADER_TC;
    static {
        try{
            SHADER_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/tessflatDistance-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private float factor = 0.2f;
    private float max = 5.0f;
    private boolean dirty = true;

    public FlatByDistanceTessellator() {
        super(null);
    }
    
    public FlatByDistanceTessellator(float factor,float max) {
        super(null);
        this.factor = factor;
        this.max = max;
    }


    public int getMinGLSLVersion() {
        return Maths.max(
                SHADER_TC.getMinGLSLVersion(), 
                FlatTessellator.SHADER_TE.getMinGLSLVersion());
    }

    public void setLinearFactor(float factor) {
        this.factor = factor;
    }

    public float getLinearFactor() {
        return factor;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public float getMax() {
        return max;
    }
    
    /**
     * {@inheritDoc }
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * {@inheritDoc }
     */
    public boolean usesGeometryShader() {
        return false;
    }

    /**
     * {@inheritDoc }
     */
    public boolean usesTesselationShader() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate tessControlShader = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate tessEvalShader = template.getTesselationEvalShaderTemplate();
        dirty = false;

        tessControlShader.append(SHADER_TC);
        tessEvalShader.append(FlatTessellator.SHADER_TE);
    }

    /**
     * {@inheritDoc }
     */
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Uniform infactor = program.getUniform(new Chars("UNI_LINEAR"));
        infactor.setVec2(gl, new float[]{factor,max});
    }

}
