
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.gpu.opengl.GL1;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.model.tree.NodeSequence;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.predicate.ClassPredicate;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class GL2DebugRigidBodyRenderer extends AbstractRenderer {

    private boolean viewBody = true;
    private boolean viewBodyVelocity = true;
    private boolean viewBodyForce = true;

    public GL2DebugRigidBodyRenderer() {
    }

    public boolean isBodyVelocityVisible() {
        return viewBodyVelocity;
    }

    public void setBodyVisible(boolean visible) {
        this.viewBody = visible;
    }

    public boolean isBodyVisible() {
        return viewBody;
    }

    public void setBodyVelocityVisible(boolean visible) {
        this.viewBodyVelocity = visible;
    }

    public void setBodyForceVisible(boolean visible) {
        this.viewBodyForce = visible;
    }

    public boolean isBodyForceVisible() {
        return viewBodyForce;
    }

    @Override
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        final Sequence bodies = new NodeSequence(node, true, new ClassPredicate(RigidBody.class));

        final GL1 gl = context.getGL().asGL1();
        final Affine V = camera.getRootToNodeSpace();
        final Matrix P = camera.getProjectionMatrix();

        final Vector p0 = new Vector(4);
        final Vector p1 = new Vector(4);
        final Vector p2 = new Vector(4);

        //render rigid body geometry
        if(viewBody){
            if(gl.isGL2ES2()){
                gl.asGL1().glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }else{
                //TODO log, but we should not log too often either, find a solution
                Loggers.get().log(new Chars("glPolygonMode not supported for gl version < GL2GL3"), Logger.LEVEL_INFORMATION);
            }

            for(int i=0,n=bodies.getSize();i<n;i++){
                final Body body = (Body) bodies.get(i);
                if(body instanceof RigidBody){
                    Geometry geometry = ((RigidBody) body).getGeometry();

                    final Affine M = body.getNodeToRootSpace();
                    final Affine MV = V.multiply(M);

                    final GeometryMesh m;
                    try{
                        m = new GeometryMesh(geometry);
                    }catch(Exception ex){continue;}
                    GL2ShellPaintVisitor v = new GL2ShellPaintVisitor(m.getShape(),gl,MV.toMatrix(),P);
                    v.visit();
                }
            }

            if(gl.isGL2ES2()){
                gl.asGL1().glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
        }

        //render velocities and forces
        if(viewBodyVelocity || viewBodyForce){
            gl.glLineWidth(1f);
            gl.glBegin(GL_LINES);
            for(int i=0,n=bodies.getSize();i<n;i++){
                final Body body = (Body) bodies.get(i);

                final Affine M = body.getNodeToRootSpace();
                final MatrixRW MVP = P.multiply(V.multiply(M).toMatrix());

                p0.setXYZW(0, 0, 0, 1);
                p1.set(body.getMotion().getVelocity());p1.setW(1);
                p2.set(body.getMotion().getForce().scale(0.1));p2.setW(1);
                MVP.transform(p0,p0);
                MVP.transform(p1,p1);
                MVP.transform(p2,p2);
                if(p0.getZ()<0) return; //check not outside screen

                if(viewBodyVelocity){
                    gl.glColor3f(0f, 1f, 0f);
                    gl.glVertex2f((float)(p0.getX()/p0.getZ()),(float)(p0.getY()/p0.getZ()));
                    gl.glVertex2f((float)(p1.getX()/p1.getZ()),(float)(p1.getY()/p1.getZ()));
                }
                if(viewBodyForce){
                    gl.glColor3f(0f, 0f, 1f);
                    gl.glVertex2f((float)(p0.getX()/p0.getZ()),(float)(p0.getY()/p0.getZ()));
                    gl.glVertex2f((float)(p2.getX()/p2.getZ()),(float)(p2.getY()/p2.getZ()));
                }
            }
            gl.glEnd();
        }
        
    }

    @Override
    public void dispose(GLProcessContext context) {

    }

}
