

package science.unlicense.engine.opengl.widget;

import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Angles;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.model3d.Model3Ds;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.control.OrbitController;
import science.unlicense.engine.opengl.light.AmbientLight;
import science.unlicense.engine.opengl.light.PointLight;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.ui.component.path.AbstractPathPresenter;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.image.process.geometric.FlipVerticalOperator;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2DMS;

/**
 *
 * @author Johann Sorel
 */
public class Model3DPathPresenter extends AbstractPathPresenter{
    
    public static final Model3DPathPresenter INSTANCE = new Model3DPathPresenter();
    
    public float getPriority() {
        return 2;
    }
    
    public boolean canHandle(Path path) {
        try {
            return Model3Ds.canDecode(path);
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public Image createImage(Path path, Extent.Long size) {
        
        try{
            final Model3DStore store = Model3Ds.read(path);

            final GLNode scene = new GLNode();
            final GLNode base = new GLNode();
            scene.getChildren().add(base);

            //calculate bbox of objet
            final Collection col = store.getElements();
            final Iterator ite = col.createIterator();
            while(ite.hasNext()){
                final Object obj = ite.next();
                if(obj instanceof Node){
                    if(obj instanceof GLNode){
                        base.getChildren().add((GLNode)obj);
                    }
                }
            }
            
            BBox bbox = base.getBBox();
            if(bbox==null)bbox = new BBox(3);
            
            //create a target node at the center of the bbox
            final GLNode target = new GLNode();
            target.getNodeTransform().getTranslation().set(bbox.getMiddle());
            target.getNodeTransform().notifyChanged();
            scene.getChildren().add(target);
            

            //OpenGL offscreen drawable.
            final int width = (int)size.get(0);
            final int height = (int)size.get(1);
            final GLSource source = GLUtilities.createOffscreenSource(width, height);
            final DefaultGLProcessContext context = new DefaultGLProcessContext();
            source.getCallbacks().add(context);

            //some lightning
            scene.getChildren().add(new AmbientLight(Color.WHITE,Color.WHITE));
            
            final PointLight point = new PointLight();
            point.getNodeTransform().getTranslation().set(bbox.getUpper());
            point.getNodeTransform().getTranslation().localScale(1.2);
            point.getNodeTransform().notifyChanged();
            scene.getChildren().add(point);
            
            
            //build the scene
            final CameraMono camera = new CameraMono();
            target.getChildren().add(camera);

            //calculate camera position from object bbox
            final OrbitController controller = new OrbitController((EventSource)null, new Vector(0, 1, 0), new Vector(1, 0, 0), target);
            camera.getUpdaters().add(controller);
            
            //set camera at good distance
            final double fov = camera.getFieldOfView();
            final double spanX = bbox.getSpan(0);
            final double distX = spanX / Math.tan(fov);
            final double spanY = bbox.getSpan(1);
            final double distY = spanY / Math.tan(fov);
            // x2 because screen space is [-1...+1]
            // x1.2 to compensate perspective effect
            final float dist = (float)(Maths.max(distX,distY) * 2.0 * 1.2);
            
            controller.setDistance(dist); 
            controller.setVerticalAngle(Angles.degreeToRadian(15));
            controller.setHorizontalAngle(Angles.degreeToRadian(15));
            

            //prepare the rendering phases
            final Image[] result = new Image[1];
            final FBO fboMs = new FBO(width, height);
            fboMs.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2DMS(width, height,Texture2DMS.COLOR_RGBA(), 4));
            fboMs.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2DMS(width, height,Texture2DMS.DEPTH24_STENCIL8(), 4));
            final FBO blit = fboMs.createBlitFBO();
            context.getPhases().add(new ClearPhase(fboMs));
            context.getPhases().add(new ClearPhase(blit));
            context.getPhases().add(new UpdatePhase(scene));
            final RenderPhase renderPhase = new RenderPhase(scene, camera, fboMs);
            renderPhase.setBlitFbo(blit);
            context.getPhases().add(renderPhase);        
            context.getPhases().add(new AbstractFboPhase() {
                protected void processInternal(GLProcessContext ctx) throws GLException {
                    //get the result image
                    blit.getColorTexture().loadOnSystemMemory(ctx.getGL());
                    result[0] = blit.getColorTexture().getImage();
                    //release resources
                    scene.dispose(ctx);
                    fboMs.unloadFromGpuMemory(ctx.getGL());
                    blit.unloadFromGpuMemory(ctx.getGL());
                }
            });

            //render it
            source.render();
            //release OpenGL
            source.dispose();

            //flip the image back up
            result[0] = new FlipVerticalOperator().execute(result[0]);
            
            return result[0];
        }catch(IOException ex){
            ex.printStackTrace();
        }catch(StoreException ex){
            ex.printStackTrace();
        }
        
        return null;
    }

    
    
    public Widget createInteractive(Path path) {
        return new WModel3DPreview(path);
    }
    
}
