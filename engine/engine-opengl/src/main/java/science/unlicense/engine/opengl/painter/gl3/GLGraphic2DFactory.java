
package science.unlicense.engine.opengl.painter.gl3;

import science.unlicense.engine.opengl.scenegraph.s2d.GLGeometryNode2D;
import science.unlicense.engine.opengl.scenegraph.s2d.GLImageNode2D;
import science.unlicense.engine.opengl.scenegraph.s2d.GLTextNode2D;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.scenegraph.s2d.GeometryNode2D;
import science.unlicense.api.scenegraph.s2d.Graphic2DFactory;
import science.unlicense.api.scenegraph.s2d.ImageNode2D;
import science.unlicense.api.scenegraph.s2d.TextNode2D;

/**
 *
 * @author Johann Sorel
 */
public class GLGraphic2DFactory implements Graphic2DFactory{

    @Override
    public SceneNode createNode() {
        return new DefaultSceneNode(2);
    }

    @Override
    public GeometryNode2D createGeometryNode() {
        return new GLGeometryNode2D();
    }

    @Override
    public ImageNode2D createImageNode() {
        return new GLImageNode2D();
    }

    @Override
    public TextNode2D createTextNode() {
        return new GLTextNode2D();
    }

}
