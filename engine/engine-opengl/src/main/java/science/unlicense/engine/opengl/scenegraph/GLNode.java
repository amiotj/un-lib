
package science.unlicense.engine.opengl.scenegraph;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.number.Int32;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.renderer.Renderer;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 *
 * @author Johann Sorel
 */
public class GLNode extends DefaultSceneNode {

    private final Sequence updaters = new ArraySequence();
    private final Sequence renderers = new ArraySequence();

    private int id = -1;
    private Chars name = Chars.EMPTY;
    private boolean visible = true;

    public GLNode() {
        this(CoordinateSystem.CARTESIAN3D_METRIC_RIGH_HANDED);
    }
    
    public GLNode(CoordinateSystem cs) {
        super(cs,true);
        this.coordinateSystem = cs;
    }

    /**
     * Get this node and children data bounding box in this node coordinate system.
     * BBox is relative to this node transform.
     * 
     * @return BBox , can be null if no data.
     */
    public BBox getBBox(){
        return getBBox(coordinateSystem);
    }
    
    /**
     * Get this node and children data bounding box in given coordinate system.
     * BBox is relative to this node transform.
     * 
     * @param cs not null
     * @return BBox , can be null if no data.
     */
    public BBox getBBox(CoordinateSystem cs){
        CObjects.ensureNotNull(cs);
        BBox bbox = null;
        final Iterator ite = this.children.createIterator();
        while (ite.hasNext()) {
            final Object next = ite.next();
            if(next instanceof GLNode){
                final GLNode child = (GLNode) next;
                final BBox cbbox = child.getBBox(cs);
                if(cbbox!=null){
                    //convert it to this node space
                    final NodeTransform m = child.getNodeTransform();
                    m.inverseTransform(cbbox.getLower(),cbbox.getLower());
                    m.inverseTransform(cbbox.getUpper(),cbbox.getUpper());
                    cbbox.reorder();
                    if(bbox==null){
                        bbox = new BBox(cbbox);
                    }else{
                        bbox.expand(cbbox);
                    }
                }
            }
        }
        
        return bbox;
    }
    
    /**
     * Get node id
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * Set node id.
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get node name.
     * @return Chars, can be null.
     */
    public Chars getName() {
        return name;
    }

    /**
     * Set node name.
     * @param name , can be null.
     */
    public void setName(Chars name) {
        this.name = name;
    }

    /**
     * Indicate if the mesh is visible.
     * Default is true.
     * @param visible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }
    
    /**
     * Get updaters of this node.
     * @see science.unlicense.engine.opengl.animation.Updater
     * 
     * @return Sequence of Updater, never null.
     */
    public Sequence getUpdaters() {
        return updaters;
    }

    /**
     * Get renderers of this node.
     * @see science.unlicense.engine.opengl.renderer.Renderer
     * 
     * @return Sequence of Renderer, never null.
     */
    public Sequence getRenderers() {
        return renderers;
    }

    /**
     * Calculate the Node to Camera coordinate system matrix.
     * @param camera
     * @return Matrix4 never null
     */
    public Affine calculateNodeToView(Camera camera){
        return camera.getRootToNodeSpace().multiply(getNodeToRootSpace());
    }

    /**
     * Calculate transformation matrice relative to the camera,
     * including view and projection transforms.
     * @param camera
     * @return Matrix44
     */
    public Matrix calculateMVP(CameraMono camera){
        final Affine mv = calculateNodeToView(camera);
        final Matrix proj = camera.getProjectionMatrix();
        return proj.multiply(mv.toMatrix());
    }

    public void dispose(GLProcessContext context) {
        //dispose rendered
        for(int i=0,n=renderers.getSize();i<n;i++){
            final Renderer renderer = (Renderer) renderers.get(i);
            renderer.dispose(context);
        }
        
        //dispose children
        final Iterator ite = children.createIterator();
        while(ite.hasNext()){
            final Object child = ite.next();
            if(child==null || !(child instanceof GLNode))continue;
            ((GLNode)child).dispose(context);
        }
    }

    public Chars thisToChars() {
        return CObjects.toChars(getName()).concat('(').concat(Int32.encode(getId())).concat(')');
    }

}
