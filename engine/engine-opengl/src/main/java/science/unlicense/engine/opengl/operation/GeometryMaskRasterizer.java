
package science.unlicense.engine.opengl.operation;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.color.colorspace.ColorSpaces;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.gpu.opengl.GL4;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.AbstractColorModel;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.number.Numbers;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.task.FontPage;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.gpu.opengl.shader.Shader;
import science.unlicense.impl.gpu.opengl.shader.ShaderProgram;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class GeometryMaskRasterizer {


    private static final int[] BLACKS = new int[17];
    public static final ColorModel COLOR_MODEL;
    static{
        float step = 1f/(BLACKS.length-1);
        for(int i=0;i<BLACKS.length;i++){
            float f = step*i;
            BLACKS[BLACKS.length-1-i] = new Color(0f,0f,0f,1f-f).toARGB();
        }

        COLOR_MODEL = new AbstractColorModel(ColorSpaces.getColorSpaces()[0]) {
            public Color toColor(Object sample) {
                return new Color(toColorARGB(sample));
            }
            public int toColorARGB(Object sample) {
                int[] data = (int[]) sample;
                int nb = Numbers.countBitsSet(data[0]);
                
                return BLACKS[nb];
            }
            public Object toSample(Color color) {
                throw new UnimplementedException("Not supported yet.");
            }
            public Object toSample(int color) {
                throw new UnimplementedException("Not supported yet.");
            }
        };

    }

    private static final int width = 512;
    private static final int height = 512;


    public static Image renderGlyph(Geometry2D geom) throws IOException{

        final Image[] image = new Image[1];

        final GLSource source = GLUtilities.createOffscreenSource(width, height);
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        source.getCallbacks().add(context);


        final FBO fbo = new FBO(width, height);
        fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,FontPage.VEC1_USHORT()));

        //build the scene
        final GLNode scene = new GLNode();
        scene.getRenderers().add(new GlyphRenderer(geom));

        context.getPhases().add(new RenderPhase(scene,new CameraMono(),fbo));
        context.getPhases().add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                fbo.getColorTexture().loadOnSystemMemory((GL4)ctx.getGL());
                image[0] = fbo.getColorTexture().getImage();
            }
        });

        //render it
        source.render();

        return image[0];
    }


    public static Image to1BitParSample(Image image){
        //rebuild 4x4 image
        final TupleBuffer b = image.getRawModel().asTupleBuffer(image);
        final int[] buffer = new int[1];
        final long width = image.getExtent().getL(0);
        final long height = image.getExtent().getL(1);
        final Image all = Images.create(new Extent.Long(width*4, height*4), Images.IMAGE_TYPE_RGB);
        final TupleBuffer o = all.getRawModel().asTupleBuffer(all);
        for(int x=0;x<width;x++){
            for(int y=0;y<height;y++){
                b.getTupleUShort(new int[]{x,y}, buffer);
                final int v = buffer[0];

                byte r01 = (byte) ((v & 1) !=0 ? 255 : 0);
                byte r02 = (byte) ((v & 2) !=0 ? 255 : 0);
                byte r03 = (byte) ((v & 4) !=0 ? 255 : 0);
                byte r04 = (byte) ((v & 8) !=0 ? 255 : 0);
                byte r05 = (byte) ((v & 16) !=0 ? 255 : 0);
                byte r06 = (byte) ((v & 32) !=0 ? 255 : 0);
                byte r07 = (byte) ((v & 64) !=0 ? 255 : 0);
                byte r08 = (byte) ((v & 128) !=0 ? 255 : 0);
                byte r09 = (byte) ((v & 256) !=0 ? 255 : 0);
                byte r10 = (byte) ((v & 512) !=0 ? 255 : 0);
                byte r11 = (byte) ((v & 1024) !=0 ? 255 : 0);
                byte r12 = (byte) ((v & 2048) !=0 ? 255 : 0);
                byte r13 = (byte) ((v & 4096) !=0 ? 255 : 0);
                byte r14 = (byte) ((v & 8192) !=0 ? 255 : 0);
                byte r15 = (byte) ((v & 16384) !=0 ? 255 : 0);
                byte r16 = (byte) ((v & 32768) !=0 ? 255 : 0);

                o.setTuple(new int[]{x*4+0,y*4+0}, new byte[]{r01,r01,r01});
                o.setTuple(new int[]{x*4+1,y*4+0}, new byte[]{r02,r02,r02});
                o.setTuple(new int[]{x*4+2,y*4+0}, new byte[]{r03,r03,r03});
                o.setTuple(new int[]{x*4+3,y*4+0}, new byte[]{r04,r04,r04});
                o.setTuple(new int[]{x*4+0,y*4+1}, new byte[]{r05,r05,r05});
                o.setTuple(new int[]{x*4+1,y*4+1}, new byte[]{r06,r06,r06});
                o.setTuple(new int[]{x*4+2,y*4+1}, new byte[]{r07,r07,r07});
                o.setTuple(new int[]{x*4+3,y*4+1}, new byte[]{r08,r08,r08});
                o.setTuple(new int[]{x*4+0,y*4+2}, new byte[]{r09,r09,r09});
                o.setTuple(new int[]{x*4+1,y*4+2}, new byte[]{r10,r10,r10});
                o.setTuple(new int[]{x*4+2,y*4+2}, new byte[]{r11,r11,r11});
                o.setTuple(new int[]{x*4+3,y*4+2}, new byte[]{r12,r12,r12});
                o.setTuple(new int[]{x*4+0,y*4+3}, new byte[]{r13,r13,r13});
                o.setTuple(new int[]{x*4+1,y*4+3}, new byte[]{r14,r14,r14});
                o.setTuple(new int[]{x*4+2,y*4+3}, new byte[]{r15,r15,r15});
                o.setTuple(new int[]{x*4+3,y*4+3}, new byte[]{r16,r16,r16});

            }
        }

        return all;
    }

    private static class GlyphRenderer extends AbstractRenderer{

        private final VBO vbo;
        private final ShaderProgram program;
        private final Matrix MV;
        private final MatrixRW P;
        private final IndexRange idx1;

        private GlyphRenderer(Geometry2D geom) throws IOException{
            state.setEnable(GLC.GETSET.State.COLOR_LOGIC_OP, true);

            final BBox geomBBox = geom.getBoundingBox();
            final BBox target = new BBox(2);
            target.setRange(0, -1, 1);
            target.setRange(1, -1, 1);

            MV = Projections.scaled(geomBBox, target).toMatrix();
            P = new Matrix3x3().setToIdentity();


            final FloatSequence seq = new FloatSequence();
            final PathIterator ite = geom.createPathIterator();

            final Vector anchor = new Vector(-1,0,0);
            final Vector current = new Vector(3);
            final Vector previous = new Vector(3);
            final Vector first = new Vector(3);
            final Vector control = new Vector(3);

            seq.put(previous.toArrayFloat());
            seq.put(previous.toArrayFloat());

            while(ite.next()){
                int type = ite.getType();
                if(PathIterator.TYPE_MOVE_TO==type){
                    ite.getPosition(first);
                    ite.getPosition(current);
                }else if(PathIterator.TYPE_LINE_TO==type){
                    ite.getPosition(current);
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    seq.put(anchor.toArrayFloat());
                    seq.put(current.toArrayFloat());
                    seq.put(previous.toArrayFloat());
                }else if(PathIterator.TYPE_CLOSE==type){
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    seq.put(anchor.toArrayFloat());
                    seq.put(first.toArrayFloat());
                    seq.put(previous.toArrayFloat());
                }else if(PathIterator.TYPE_QUADRATIC==type){
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    ite.getPosition(current);
                    seq.put(anchor.toArrayFloat());
                    seq.put(current.toArrayFloat());
                    seq.put(previous.toArrayFloat());

                    // special triangle
                    ite.getFirstControl(control);
                    control.setZ(1);
                    current.setZ(1);
                    previous.setZ(1);
                    seq.put(control.toArrayFloat());
                    seq.put(current.toArrayFloat());
                    seq.put(previous.toArrayFloat());

                }else{
                    throw new RuntimeException("TODO " + type);
                }

                previous.set(current);
            }

            float[] coords = seq.toArrayFloat();
            vbo = new VBO(coords,3);

            final Shader vertexShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-0-ve.glsl")).createInputStream())),Shader.SHADER_VERTEX);
            final Shader fragmentShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-4-fr.glsl")).createInputStream())),Shader.SHADER_FRAGMENT);
            program = new ShaderProgram();
            program.setShader(vertexShader, Shader.SHADER_VERTEX);
            program.setShader(fragmentShader, Shader.SHADER_FRAGMENT);
            idx1 = IndexRange.TRIANGLES(0, (coords.length-6)/3);

        }

        @Override
        public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
            final GL4 gl = context.getGL().asGL4();

            gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);

            program.loadOnGpuMemory(gl);
            program.enable(gl);

            final VertexAttribute va1 = program.getVertexAttribute(new Chars("l_position1"), gl, 1);
            final VertexAttribute va2 = program.getVertexAttribute(new Chars("l_position2"), gl, 1);
            final VertexAttribute va3 = program.getVertexAttribute(new Chars("l_position3"), gl, 1);
            final VertexAttribute va4 = program.getVertexAttribute(new Chars("l_position4"), gl, 1);
            final VertexAttribute va5 = program.getVertexAttribute(new Chars("l_position5"), gl, 1);
            va1.enable(gl.asGL2ES2(), vbo, 0*3*4);
            va2.enable(gl.asGL2ES2(), vbo, 1*3*4);
            va3.enable(gl.asGL2ES2(), vbo, 2*3*4);
            va4.enable(gl.asGL2ES2(), vbo, 3*3*4);
            va5.enable(gl.asGL2ES2(), vbo, 4*3*4);
            program.getUniform(new Chars("MV")).setMat3(gl.asGL2ES2(), MV.toArrayFloat());
            program.getUniform(new Chars("P")).setMat3(gl.asGL2ES2(), P.toArrayFloat());
            program.getUniform(new Chars("SIZE")).setVec2(gl.asGL2ES2(), new float[]{width,height});

            idx1.draw(gl, false);
            program.disable(gl);
            vbo.unbind(gl);

        }

        @Override
        public void dispose(GLProcessContext context) {
        }

    }

}
