
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.collection.Iterator;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.CoordinateSystems;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.engine.opengl.renderer.Renderer;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.tessellation.Tessellator;

/**
 * Stores mesh model informations.
 *
 * @author Johann Sorel
 */
public class Mesh extends GLNode{

    //flags to update the mesh when needed
    private static final int SHAPE_DIRTY = 1 << 2;
    private static final int MATERIAL_DIRTY = 1 << 4;
    private static final int TESSELATOR_DIRTY = 1 << 5;

    //mesh model and texture variables
    private Shape shape;
    private Material material = new Material();
    private Tessellator tessellator;

    //various environement informations
    private boolean pickable = true;
    
    //loading state
    private int dirty = 0;
    private boolean loaded = false;
    

    public Mesh() {
        this(CoordinateSystem.CARTESIAN3D_METRIC_RIGH_HANDED);
    }

    public Mesh(CoordinateSystem cs) {
        super(cs);
        getRenderers().add(new MeshRenderer(this));
    }

    public boolean isDirty(){
        return (!loaded || dirty!=0)
            || (shape      !=null && shape.isDirty())
            || (material   !=null && material.isDirty())
            || (tessellator!=null && tessellator.isDirty());
    }

    /**
     * {@inheritDoc }     
     */
    public BBox getBBox(CoordinateSystem cs) {
        BBox bbox = shape.getBBox();
        if(bbox==null)return null;
        
        if(this.coordinateSystem.equals(cs)) return bbox;
        
        final Transform m = CoordinateSystems.createTransform(coordinateSystem, cs);
        m.transform(bbox.getLower(),bbox.getLower());
        m.transform(bbox.getUpper(),bbox.getUpper());
        bbox.reorder();
        
        return bbox;
    }
    
    

    ////////////////////////////////////////////////////////////////////////////
    // Mesh model informations /////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Reuse all informations form given mesh.
     * This does not duplicate materials or any vbo.
     * 
     * @param mesh 
     */
    public void copy(Mesh mesh){
        this.modelTransform.set(mesh.modelTransform);
        this.coordinateSystem = mesh.coordinateSystem;
        
        this.shape = mesh.shape;
        this.material = mesh.material;
        this.tessellator = mesh.tessellator;
        this.pickable = mesh.pickable;
        this.dirty = mesh.dirty;
        this.loaded = mesh.loaded;
    }
        
    public void setShape(Shape shell) {
        this.shape = shell;
        dirty |= SHAPE_DIRTY;
    }

    public Shape getShape() {
        return (Shape) shape;
    }

    public void setMaterial(Material material) {
        this.material = material;
        dirty |= MATERIAL_DIRTY;
    }

    public Material getMaterial() {
        return material;
    }

    public void setTessellator(Tessellator material) {
        this.tessellator = material;
        dirty |= TESSELATOR_DIRTY;
    }

    public Tessellator getTessellator() {
        return tessellator;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Environement informations ///////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Indicate if the mesh is pickable.
     * Default is false.
     * @param pickable
     */
    public void setPickable(boolean pickable) {
        this.pickable = pickable;
    }

    public boolean isPickable() {
        return pickable;
    }

    ////////////////////////////////////////////////////////////////////////////
    // data loading ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    private void gpuCleanResources(final RenderContext ctx){
        final GL gl = ctx.getGL();
        GLUtilities.checkGLErrorsFail(gl);
        shape.dispose(ctx);
        GLUtilities.checkGLErrorsFail(gl);
    }

    private void gpuLoadResources(final RenderContext ctx){
        gpuCleanResources(ctx);
        loaded = true;
        dirty = 0;
    }

    public void updateResources(RenderContext context){
        gpuLoadResources(context);
    }
    
    public void dispose(GLProcessContext context) {
        if(shape!=null) shape.dispose(context);
        if(material!=null) material.dispose(context);
        if(tessellator!=null) tessellator.dispose(context);

        //unload associated renderers
        final Iterator ite = getRenderers().createIterator();
        while(ite.hasNext()){
            final Renderer renderer = (Renderer) ite.next();
            renderer.dispose(context);
        }
        super.dispose(context);
    }

}