
package science.unlicense.engine.opengl.operation;

import science.unlicense.api.geometry.BBox;
import science.unlicense.engine.opengl.mesh.Shell;

/**
 *
 * @author Johann Sorel
 */
public class ShellBBoxCalculator extends ShellVisitor {

    private BBox bbox = null;
    
    public ShellBBoxCalculator(Shell shell) {
        super(shell);
    }

    public BBox getBBox() {
        bbox = null;
        visit();
        return bbox;
    }

    protected void visit(Vertex v) {
        if(v.visited) return;
        
        if(bbox==null){
            bbox = new BBox(3);
            bbox.setRange(0, v.vertice[0], v.vertice[0]);
            bbox.setRange(1, v.vertice[1], v.vertice[1]);
            bbox.setRange(2, v.vertice[2], v.vertice[2]);
        }else{
            //expand it
            bbox.expand(v.vertice);
        }
    }

}
