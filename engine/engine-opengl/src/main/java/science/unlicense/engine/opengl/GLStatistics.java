
package science.unlicense.engine.opengl;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.model.tree.DefaultNode;

/**
 * Store GLProcessContext execution statistics.
 * Informations are stored for one execution.
 * Once the execution is finished, and event is send with the global
 * statistics.
 * 
 * @author Johann Sorel
 */
public class GLStatistics extends AbstractEventSource{

    public static final Chars PROPERTY_RESULT = new Chars("Result");
    
    private final GLProcessContext context;
    private StatEntry root;
    private float logLevel = 1;

    public GLStatistics(GLProcessContext context) {
        this.context = context;
    }

    public float getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(float logLevel) {
        this.logLevel = logLevel;
    }
    
    public void start(){
        root = new StatEntry(null, context, new Chars("run"), System.nanoTime());
    }
    
    public void end(){
        root.setEnd(System.nanoTime());
        if(hasListeners()){
            sendPropertyEvent(this, PROPERTY_RESULT, null, root);
        }
        root = null;
    }
    
    public void startLog(Object source, Chars task){
        if(root==null ||!hasListeners()) return;
        final StatEntry entry = new StatEntry(root, source, task, System.nanoTime());
        root.getChildren().add(entry);
        root = entry;
    }
    
    public void endLog(){
        if(root==null ||!hasListeners()) return;
        root.setEnd(System.nanoTime());
        root = root.parent;
    }
    
    public static class StatEntry extends DefaultNode{

        private final StatEntry parent;
        public final Object source;
        public final Chars task;
        public final long startNano;
        public long endNano;
        
        public StatEntry(StatEntry parent, Object source, Chars task, long startNano) {
            super(true);
            this.parent = parent;
            this.source = source;
            this.task = task;
            this.startNano = startNano;
        }

        public StatEntry getParent() {
            return parent;
        }

        public Object getSource() {
            return source;
        }

        public Chars getTask() {
            return task;
        }

        public long getStartNano() {
            return startNano;
        }

        public long getEndNano() {
            return endNano;
        }
        
        public void setEnd(long timeNano){
            this.endNano = timeNano;
        }

        @Override
        public Chars thisToChars() {
            final CharBuffer cb = new CharBuffer();
            cb.append(source.getClass().getSimpleName());
            if(task!=null){
                cb.append(':');
                cb.append(task);
            }
            cb.append(' ').append('-').append(' ');
            long diff = endNano-startNano;
            long ms = diff / 1000000;
            diff %= 1000000;
            cb.append(""+ms+"ms "+diff+"ns");
            return cb.toChars();
        }
        
    }
    
}
