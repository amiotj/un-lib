

package science.unlicense.engine.opengl.phase.reflection;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Vector;

/**
 * Loop on scene nodes, when a mesh with a reflection mapping is found it will
 * be updated using a reflected camera.
 * 
 * @author Johann Sorel
 */
public class ReflectionVisitor extends DefaultNodeVisitor{

    //TODO : this should be relative to camera upward axis, or coordinate system up ?
    private static final Affine3 FLIP_MATRIX = new Affine3(
                1, 0, 0, 0, 
                0,-1, 0, 0, 
                0, 0, 1, 0);
    
    private final RenderPhase baseRenderPhase;
    private final ClearPhase clearPhase;
    private final DeferredRenderPhase reflectRender;
    private final float[] planEquation = new float[4];
    private final float[] n = new float[4];
    private final float[] v = new float[4];
    
    public ReflectionVisitor(RenderPhase baseRenderPhase) {
        this.baseRenderPhase = baseRenderPhase;
        clearPhase = new ClearPhase();
        reflectRender = new DeferredRenderPhase(null, null, null);
    }

    public float[] getPlanEquation() {
        return planEquation;
    }

    public Object visit(Node node, Object context) {
        if(node instanceof GLNode){
            final GLNode glNode = ((GLNode)node);
            //skip nodes which are not visible
            if(!glNode.isVisible()) return null;
            if(glNode instanceof Mesh){
                final Mesh mesh = (Mesh) glNode;
                
                final Sequence layers = mesh.getMaterial().getLayers();
                for(int i=0,n=layers.getSize();i<n;i++){
                    final Layer layer = (Layer) layers.get(i);
                    final Mapping mapping = layer.getMapping();
                    if(mapping instanceof ReflectionMapping){
                        try {
                            visit(mesh, (ReflectionMapping)mapping, (GLProcessContext)context);
                        } catch (GLException ex) {
                            ex.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }

        //loop on childrens
        return super.visit(node, context);
    }

    public void visit(Mesh mesh, ReflectionMapping mapping, GLProcessContext context) throws GLException{
        if(!mesh.isVisible()) return;
        
        //we expect the texture to be a plan
        //get the normal of the plan
        final Shell shell = (Shell) mesh.getShape();
        shell.getNormals().getTupleFloat(0, n);
        shell.getVertices().getTupleFloat(0, v);
        n[3] = 0;
        v[3] = 1;
        
        final Matrix planToRootSpace = mesh.getRootToNodeSpace().toMatrix();
        planToRootSpace.transform(n, n);
        planToRootSpace.transform(v, v);
        
        final Plane plan = new Plane(new Vector(n[0],n[1],n[2]), new Vector(v[0],v[1],v[2]));
        final float d = (float) plan.getD();
                
        planEquation[0] = n[0];
        planEquation[1] = n[1];
        planEquation[2] = n[2];
        planEquation[3] = d;
        
        //calculate reflection matrix
        // http://en.wikipedia.org/wiki/Transformation_matrix : reflection
        final Affine3 reflectionMatrix = new Affine3(
                1-2*n[0]*n[0],  -2*n[0]*n[1],  -2*n[0]*n[2], -2*n[0]*d, 
                 -2*n[0]*n[1], 1-2*n[1]*n[1],  -2*n[1]*n[2], -2*n[1]*d, 
                 -2*n[0]*n[2],  -2*n[1]*n[2], 1-2*n[2]*n[2], -2*n[2]*d);
                
        final CameraMono camera = baseRenderPhase.getCamera();
        final Affine rootToCamera = camera.getRootToNodeSpace();
        final AffineRW res = FLIP_MATRIX.multiply(rootToCamera);
        res.localMultiply(reflectionMatrix);
        
        final CameraMono reflectionCamera = new CameraMono();
        reflectionCamera.copy(camera);
        reflectionCamera.setRootToNodeSpace(res);
                
        mapping.update(context,
                (int)context.getViewRectangle().getWidth(),
                (int)context.getViewRectangle().getHeight());
        
        final SceneNode root = baseRenderPhase.getRoot();
                
        final FBO fbo = mapping.getGbo();
        
        clearPhase.setOutputFbo(fbo);
        reflectRender.setRoot((GLNode)root);
        reflectRender.setCamera(reflectionCamera);
        reflectRender.setOutputFbo(fbo);
        
        mesh.setVisible(false);
        clearPhase.process(context);
        reflectRender.process(context);
        mesh.setVisible(true);
                
    }
    
}