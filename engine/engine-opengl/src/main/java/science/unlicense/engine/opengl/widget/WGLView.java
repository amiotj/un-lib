
package science.unlicense.engine.opengl.widget;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.DragAndDropMessage;
import static science.unlicense.api.desktop.UIFrame.PROP_FOCUSED_WIDGET;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import science.unlicense.engine.opengl.painter.gl3.task.Release;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.ui.ievent.EventStack;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.image.Image;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.Widgets;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class WGLView extends AbstractEventSource implements EventListener{
    
    //mouse and keyboard event stack, we stack them and apply send before rendering
    private final EventStack eventStack = new EventStack();
    private final WContainer container = new WContainer();
    private Widget focused = null;
    private boolean isFocusedDragging = false;
    private Logger logger = Loggers.get();

    //texture updater
    private RepaintThread worker;

    //mouse events
    private boolean mouseInPlan = false;
    private Tuple lastMousePosition = null;

    //drag and drop events
    
    //contains children with special 3d requierements
    private final Collection children3d = new HashSet(Hasher.IDENTITY);
    private final Collection toRender = new HashSet(Hasher.IDENTITY);
    private final Collection toDispose = new HashSet(Hasher.IDENTITY);
    
    public WGLView(final int width, final int height, int nbSample, GLProcessContext ctx, final Value link) {
        this(width,height,nbSample,ctx.getSource(),link);
    }
    
    public WGLView(final int width, final int height, int nbSample, GLSource ctx, final Value link) {
        this(width,height);
        configureGL(nbSample,ctx,link);
    }
    
    public WGLView(final int width, final int height) {

        //make container listen to the WPlan events
        addEventListener(MouseMessage.PREDICATE, container);
        addEventListener(KeyMessage.PREDICATE, container);

        //listen to container events to update 3d sub nodes
        container.addEventListener(NodeMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                sync=false;
            }
        });

        //set the container size
        container.setEffectiveExtent(new Extent.Double(width,height));
        container.getNodeTransform().setToTranslation(new double[]{width/2.0,height/2.0});
        container.setDirty();
    }
    
    public void configureGL(int nbSample, GLSource ctx, final Value link){
        this.worker = new RepaintThread(link,nbSample,container,logger);
        this.worker.start();
        
        container.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if(Widget.PROPERTY_DIRTY.equals(pe.getPropertyName())){
                    link.putDirty((BBox) pe.getNewValue());
                }
            }
        });
    }
    
    public boolean isGLInitialized(){
        return worker!=null;
    }
    
    public WContainer getContainer() {
        return container;
    }

    public Widget getFocused() {
        return focused;
    }

    public void setFocused(Widget focused) {
        if(CObjects.equals(this.focused, focused)) return;
        final Widget old = this.focused;
        if(old!=null) old.setFocused(false);
        this.focused = focused;
        if(this.focused!=null) this.focused.setFocused(true);
        sendPropertyEvent(this, PROP_FOCUSED_WIDGET, old, this.focused);
    }
    
    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void setExtent(Extent ex){
        if(!container.getEffectiveExtent().equals(ex)){
            container.setEffectiveExtent(ex);
            container.getNodeTransform().setToTranslation(new double[]{ex.get(0)/2.0,ex.get(1)/2.0});
        }
    }
    
    public void dispose(){
        worker.kill = true;
        synchronized (worker.link.dirty){
            worker.link.dirty.notifyAll();
        }
        worker.painter.dispose();
    }
    
    public void receiveEvent(Event event) {
        eventStack.put(event);
        if(worker==null) return;
        synchronized (worker.link.dirty){
            worker.link.dirty.notifyAll();
        }
    }

    private void flushEvents(){
        Object[] events = eventStack.flush();
        for(int i=0;i<events.length;i++){
            flushEvent((Event) events[i]);
        }
    }
    
    private void flushEvent(Event event){
        //catch frame mouse event and propagate to widgets
        final Class eventClass = event.getMessage().getClass();
        if(eventClass == MouseMessage.class){
            final MouseMessage e = (MouseMessage) event.getMessage();
            lastMousePosition = e.getMousePosition();
            
            //if we have a focused widget and last event was a drag consumed then
            //we only forward the event to this widget
            final Tuple inWidgetPosition;
            if(isFocusedDragging){
                inWidgetPosition = Widgets.convertEvent(focused, event);
            }else{
                inWidgetPosition = transposeToWidget(lastMousePosition,e,null);
            }
                
            isFocusedDragging = focused!=null && e.isDragging() && e.isConsumed();

            //handle ddrag & drops events
            final Sequence drops = science.unlicense.system.System.get().getDragAndDrapBag().getAttachments();
            if(!drops.isEmpty()){
                if(e.getType()==MouseMessage.TYPE_RELEASE){
                    final DragAndDropMessage dropEvent = new DragAndDropMessage(DragAndDropMessage.TYPE_DROP);
                    //check for drag & drop objects
                    final Sequence stack = Widgets.pruneStackAt(container, inWidgetPosition.getX(), inWidgetPosition.getY());
                    //search for the first widget to accept the drop
                    for(int i=stack.getSize()-1;i>=0;i--){
                        final Widget comp = (Widget) stack.get(i);
                        comp.receiveEvent(new Event(null,dropEvent));
                        if(dropEvent.isConsumed()){
                            break;
                        }
                    }
                    //remove all drop requests consumed or not
                    drops.removeAll();

                }else if(e.getType()==MouseMessage.TYPE_MOVE){
                    //TODO sen drag&drop enter/exit events
                }
            }

        }else if(eventClass == KeyMessage.class){
            final KeyMessage e = (KeyMessage) event.getMessage();
            transposeToWidget(lastMousePosition,null,e);
        }
    }
    
    /**
     * Adapt event to widget space.
     */
    private Tuple transposeToWidget(Tuple position, MouseMessage me, KeyMessage ke){
        if(position==null) return null;

        Tuple imageCSPoint = position.copy();
        final Extent extent = container.getEffectiveExtent();
        Tuple containerCSPoint = container.getNodeTransform().inverseTransform(position,null);

        MouseMessage resend = null;
        
        if(imageCSPoint.getX()<0 || imageCSPoint.getX()>extent.get(0) || imageCSPoint.getY()<0 || imageCSPoint.getY()>extent.get(1)){
            //out of plan
            if(me.isDragging()){
                //something is consuming events, likely a drag, we continue to forward events
                resend = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(),me.getWheelOffset(),me.isDragging());
                if(hasListeners()) getEventManager().sendEvent(new Event(this,resend));
                    
            }else if(mouseInPlan && me!=null){
                //mouse was in the plan, send a mouse exit event
                final MouseMessage event = new MouseMessage(MouseMessage.TYPE_EXIT, -1, -1, containerCSPoint, me.getMouseScreenPosition(),0,me.isDragging());
                mouseInPlan = false;
                if(hasListeners()) getEventManager().sendEvent(new Event(this,event));
            }
        }else{
            if(me!=null){
                if(mouseInPlan){
                    //mouse was already in the plan, just propage the event
                    resend = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(),me.getWheelOffset(),me.isDragging());
                    if(hasListeners()) getEventManager().sendEvent(new Event(this,resend));
                }else{
                    //mouse was out of the plan, we first generate a mouse enter event
                    mouseInPlan = true;
                    final MouseMessage enterEvent = new MouseMessage(MouseMessage.TYPE_ENTER, -1, -1, containerCSPoint, me.getMouseScreenPosition(),0,me.isDragging());
                    if(hasListeners()) getEventManager().sendEvent(new Event(this,enterEvent));
                    //then propage the true event
                    resend = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(),me.getWheelOffset(),me.isDragging());
                    if(hasListeners()) getEventManager().sendEvent(new Event(this,resend));
                }
            }
            if(ke!=null){
                if(mouseInPlan && hasListeners()){
                    getEventManager().sendEvent(new Event(this,ke));
                }
            }
        }
        
        if(resend!=null){
            //consume original event if widget event was used
            if(resend.isConsumed()) me.consume();
        }

        return containerCSPoint.copy();
    }

    
    // sync possible 3D child nodes ////////////////////////////////////////////
    
    private boolean sync = false;
        
    private synchronized void sync3DChildren(){
        if(sync) return;
        sync = true;

        toRender.removeAll();
        toDispose.replaceAll(children3d);
        
        final WContainer root = getContainer();
        root.accept(new DefaultNodeVisitor() {
            public Object visit(Node node, Object context) {
                super.visit(node, context);
                if(node instanceof WGLNode){
                    final WGLNode glnode = (WGLNode) node;
                    toDispose.remove(glnode);
                    toRender.add(glnode);
                }
                return null;
            }
        }, null);

        //dispose old childrens
        children3d.replaceAll(toRender);
    }
    
    public void renderGL(GLProcessContext context) {
        sync3DChildren();
        
        synchronized(toRender){
            for(Iterator ite=toRender.createIterator();ite.hasNext();){
                final WGLNode key = (WGLNode) ite.next();
                key.renderGL(context, null);
            }
        }
        
        synchronized(toDispose){
            for(Iterator ite=toDispose.createIterator();ite.hasNext();){
                final WGLNode key = (WGLNode) ite.next();
                final GLNode todisp = key.getInSceneNode();
                if(todisp!=null){
                    todisp.dispose(context);
                }
            }
            toDispose.removeAll();
        }
    }
        
    public Class[] getEventClasses() {
        return new Class[]{
            MouseMessage.class,
            KeyMessage.class
        };
    }

    public static abstract class Value {
        
        private final BBox dirty = new BBox(2);

        public abstract void swapTexture(Image base);

        public Value() {
            dirty.setToNaN();
        }

        void getDirty(BBox cp) {
            synchronized(dirty){
                cp.set(dirty);
                dirty.setToNaN();
            }
        }

        void putDirty(BBox n) {
            synchronized(dirty){
                if(this.dirty.isValid()){
                    this.dirty.expand(n);
                }else{
                    this.dirty.set(n);
                }
                dirty.notifyAll();
            }
        }
    }

    class RepaintThread extends Thread{
        
        final GL3ImagePainter2D painter;
        final Value link;
        final WContainer container;
        final Logger logger;
        final int nbSample;
        final BBox cp = new BBox(2);
        volatile boolean kill = false;

        public RepaintThread(Value link, int nbSample,WContainer container, Logger logger) {
            this.painter = new GL3ImagePainter2D(1, 1, nbSample);
            this.link = link;
            this.container = container;
            this.logger = logger;
            this.nbSample = nbSample;
        }
        
        public void run(){
            try{
                Texture2D tex = null;
                for(;;){
                    if(kill) return;
                        
                    //don't repaint right away, widgets event may not be completly
                    //finished, waiting a little avoids fast consecutive repaints
                    synchronized(link.dirty){
                        flushEvents();
                        link.getDirty(cp);
                        if(!cp.isValid() || cp.isEmpty()){
                            link.dirty.wait();
                            continue;
                        }
                    }
                                        
                    if(cp.isValid() && !cp.isEmpty()){
                        //long before = System.currentTimeMillis();
                        final Extent ext = container.getEffectiveExtent();

                        //convert dirty area to node space
                        final Matrix m = container.getNodeTransform().asMatrix();

                        //TODO
                        //cp.set(new BBox(container.getEffectiveExtent()));
                        //cp = new BBox(container.getEffectiveExtent());
                        Geometries.transform(cp, m, cp);
                        cp.setRange(0, Math.floor(cp.getMin(0)), Math.ceil(cp.getMax(0)));
                        cp.setRange(1, Math.floor(cp.getMin(1)), Math.ceil(cp.getMax(1)));

                        final int width = Math.max(1, (int)ext.get(0));
                        final int height = Math.max(1, (int)ext.get(1));
                        painter.setSize(new Extent.Long(width, height));
                        painter.clear(cp);
                        //TODO we need to flush to update the painter matrix
                        //find why some operation still have the old matrix parameter
                        painter.flush();
                        ViewRenderLoop.render(container, painter, cp);
//                            long after = System.currentTimeMillis();
//                            System.out.println("render time : "+(after-before)+" ms");
//                        if(nbSample>0){
//                            if(tex!=null){
//                                //verify blit texture size
//                                Extent size = painter.getSize();
//                                if(tex.getWidth()!=size.get(0) || tex.getHeight()!=size.get(1)){
//                                    painter.addTask(new Release(tex));
//                                    tex= null;
//                                }
//                            }
//                            tex = painter.blitTexture(tex);
//                        }else{
//                            throw new InvalidArgumentException("TODO");
//                            //tex = (Texture2D) painter.getFbo().getColorTexture();
//                        }
                        link.swapTexture(painter.getImage());
                        //tex = link.swapTexture(tex);
                    }
                }
            }catch(Exception ex){
                logger.critical(ex);
            }catch(Throwable ex){
                logger.critical(new Chars("Throwable : "+ex.getMessage()+" "+ex));
            }
        }
    }

}
