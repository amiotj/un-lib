
package science.unlicense.engine.opengl;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 *
 * @author Johann Sorel
 */
public class GLEngineUtils {

    
    /** loop in scene tree and calculate bbox */
    private static final NodeVisitor BBOX_VISITOR = new DefaultNodeVisitor() {
        public Object visit(Node node, Object context) {
            if (node instanceof Mesh) {
                final Mesh mesh = (Mesh) node;
                final Shape shape = mesh.getShape();
                final Object[] a = (Object[]) context;

                if (a[0] == null) {
                    a[0] = new BBox(shape.getBBox());
                } else {
                    ((BBox) a[0]).expand(shape.getBBox());
                }
            }
            return super.visit(node, context);
        }
    };
    
    private GLEngineUtils(){}
    
    
    /**
     * Loop in scene tree and calculate bbox of all meshes.
     * 
     * @param scene
     * @return 
     */
    public static BBox calculateBBox(GLNode scene){
        final Object[] bboxA = new Object[1];
        scene.accept(BBOX_VISITOR, bboxA);
        return (BBox) bboxA[0];
    }
    
}
