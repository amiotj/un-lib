
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 *
 * @author Johann Sorel
 */
public class UniformNoisePhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/uniformnoise-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final UNActor actor = new UNActor();
    private Uniform timeUniform;

    public UniformNoisePhase(Texture texture) {
        super(texture);
    }
    
    public UniformNoisePhase(FBO output, Texture texture) {
        super(output, texture);
    }

    protected UNActor getActor() {
        return actor;
    }

    private final class UNActor extends DefaultActor{

        public UNActor() {
            super(new Chars("UniformNoise"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();
            timeUniform = program.getUniform(new Chars("time"));
            timeUniform.setFloat(gl, (float) Math.random());
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            timeUniform = null;
        }
        
    }
    
}
