

package science.unlicense.engine.opengl.physic;

import science.unlicense.api.anim.AbstractAnimation;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Mesh morph animation.
 * 
 * @author Johann Sorel
 */
public class MorphAnimation extends AbstractAnimation {

    private static final NodeVisitor visitor = new DefaultNodeVisitor(){

        public Object visit(Node node, Object context) {
            
            if(node instanceof Mesh){
                final Mesh mesh = (Mesh) node;
                final Shape shape = mesh.getShape();
                if(shape instanceof Shell){
                    final MorphSet ms = ((Shell)shape).getMorphs();
                    if(ms!=null){
                        final Dictionary morphState = (Dictionary) context;
                        for(Iterator ite = ms.getMorphs().createIterator();ite.hasNext();){
                            final MorphTarget mf = (MorphTarget) ite.next();
                            final Double def = (Double) morphState.getValue(mf.getName());
                            if(def!=null){
                                mf.setFactor(def.floatValue());
                            }else{
                                mf.setFactor(0);
                            }
                        }
                    }
                }
            }
            return super.visit(node, context);
        }
    };
    
    private final Dictionary series = new HashDictionary();
    private final Dictionary frame = new HashDictionary();
    private GLNode node;

    public Dictionary getSeries() {
        return series;
    }

    public GLNode getNode() {
        return node;
    }

    public void setNode(GLNode node) {
        this.node = node;
    }
    
    public double getLength() {
        double length = 0;
        final Iterator ite = series.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final MorphTimeSerie serie = (MorphTimeSerie) pair.getValue2();
            final double l = serie.getLength();
            if(l>length) length = l;
        }
        return length;
    }

    public void update() {
        if(node==null) return;
        
        final double time = getTime();
        final Iterator ite = series.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final MorphTimeSerie serie = (MorphTimeSerie) pair.getValue2();
            frame.add(serie.getMorphName(), serie.interpolate(time));
        }
        
        node.accept(visitor, frame);
    }
    
}
