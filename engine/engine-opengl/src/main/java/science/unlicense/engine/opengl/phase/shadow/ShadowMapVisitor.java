

package science.unlicense.engine.opengl.phase.shadow;

import science.unlicense.api.character.Chars;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.light.Light;
import science.unlicense.engine.opengl.light.ShadowMap;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderPhase;

/**
 *
 * @author Johann Sorel
 */
public class ShadowMapVisitor extends DefaultNodeVisitor{

    private final ClearPhase clearPhase;
    private final RenderPhase depthRender;
    private CameraMono camera;
    private int textureSize;
    
    public ShadowMapVisitor(int textureSize) {
        this.textureSize = textureSize;
        clearPhase = new ClearPhase();
        depthRender = new ShadowRenderPhase();
    }

    public void setCamera(CameraMono camera) {
        this.camera = camera;
    }

    public CameraMono getCamera() {
        return camera;
    }

    public Object visit(Node node, Object context) {
        if(node instanceof GLNode){
            final GLNode glNode = ((GLNode)node);
            //skip nodes which are not visible
            if(!glNode.isVisible()) return null;
            if(glNode instanceof Light){
                try {
                    visit((Light)glNode, (GLProcessContext)context);
                } catch (GLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        //loop on childrens
        return super.visit(node, context);
    }

    public void visit(Light light, GLProcessContext context) throws GLException{
        if(!light.isCastShadows()){
            light.setShadowMap(null);
            return;
        }
        
        final SceneNode root = light.getRoot();
        
        ShadowMap shadowMap = light.getShadowMap();
        if(shadowMap==null){
            shadowMap = new ShadowMap();
        }
        shadowMap.update(camera,light,textureSize);
        
        //ensure size is correct
        shadowMap.getFbo().resize(context.getGL(), textureSize, textureSize);
        
        final CameraMono camera = shadowMap.getCamera();
        final FBO fbo = shadowMap.getFbo();
        
        clearPhase.setOutputFbo(fbo);
        depthRender.setRoot((GLNode)root);
        depthRender.setCamera(camera);
        depthRender.setOutputFbo(fbo);
        
        clearPhase.process(context);
        depthRender.process(context);
        
//        final Texture2D tex = (Texture2D) fbo.getColorTexture();
//        tex.loadOnSystemMemory(context.getGL());
//        final Image image = tex.getImage();
//        System.out.println(Images.toChars(image, null));
//        System.out.println("la");
        
    }

    public void setTextureSize(int size) {
        this.textureSize = size;
    }

    public int getTextureSize() {
        return textureSize;
    }

    private static final class ShadowRenderPhase extends RenderPhase{

        private static final ShaderTemplate SHA_FR;
        static {
            try{
                SHA_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/shadowmap-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);

            }catch(IOException ex){
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }

        private final DefaultActor SHA = new DefaultActor(
                new Chars("shadowmap"), false,null, null, null, null, SHA_FR, true, true);
        
        public ShadowRenderPhase() {
            super(null, null, null);
        }

//        protected ActorProgram getOrCreateProgram(RenderContext context, Mesh mesh) {
//
//            //build a default program
//            ActorProgram program = (ActorProgram) mesh.programs.getValue(programId);
//            if(program == null){
//                program = buildProgram(mesh,context, mesh.getShape(), mesh.getTessellator(), null, null);
//                mesh.programs.add(programId, program);
//            }
//
//            return program;
//        }
//        
//        public ActorProgram buildProgram(Mesh mesh,RenderContext ctx, Shape shape,
//            Tessellator tessellator, Material material, Sequence extActors) {
//
//            //build the final collector
//            final FragmentCollectorActor collector = new FragmentCollectorActor(mappings);
//
//            //build shell actor
//            final Actor shellActor = Actors.buildActor(mesh, shape);
//
//            final ActorProgram program = new ActorProgram();
//            final Sequence actors = program.getActors();
//            
//            //build morph actor if present
//            if(shape instanceof Shell){
//                final MorphSet ms = ((Shell)shape).getMorphs();
//                if(ms!=null){
//                    actors.add(new MorphActor(ms));
//                }
//            }
//
//            actors.add(shellActor);
//            if(tessellator!=null) actors.add(tessellator);
//            //check additional actors
//            if(extActors!=null) actors.addAll(extActors);
//            actors.add(SHA);
//            actors.add(collector);
//
//            program.buildProgram(ctx);
//
//            return program;
//        }
        
    }
    
}