
package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.CObjects;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractRenderer implements Renderer{
    
    protected Predicate phasePredicate;
    protected RendererState state = new RendererState();

    public AbstractRenderer() {
        this(Predicate.TRUE);
    }
    
    public AbstractRenderer(Predicate phasePredicate) {
        CObjects.ensureNotNull(phasePredicate);
        this.phasePredicate = phasePredicate;
    }

    public Predicate getPhasePredicate() {
        return phasePredicate;
    }

    public void setPhasePredicate(Predicate predicate) {
        CObjects.ensureNotNull(predicate);
        this.phasePredicate = predicate;
    }

    public RendererState getState() {
        return state;
    }

    public void setState(RendererState state) {
        CObjects.ensureNotNull(state);
        this.state = state;
    }

    /**
     * Ensure the render state is called before renderInternal.
     * 
     * @param context
     * @param camera
     * @param node
     */
    public final void render(RenderContext context, CameraMono camera, GLNode node) {
        state.configure(context.getGL());
        renderInternal(context, camera, node);
    }

    protected void renderInternal(RenderContext context, CameraMono camera, GLNode node){

    }
    
}
