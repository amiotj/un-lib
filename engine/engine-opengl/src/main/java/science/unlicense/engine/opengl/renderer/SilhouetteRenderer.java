

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.mesh.MeshUtilities;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.Actors;

/**
 *
 * Nice explication here :
 * http://prideout.net/blog/?p=54
 * 
 * Shader is derived from prideout.
 * 
 * @author Johann Sorel
 */
public class SilhouetteRenderer extends AbstractRenderer {

    private static final ShaderTemplate SILHOUETTE_GE;
    static {
        try{
            SILHOUETTE_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/silhouette-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final Mesh mesh;
    private ActorProgram program;
    private final Material material = new Material();
    private float borderWidth;
    private Uniform uniWidth;

    /**
     * 
     * @param mesh
     * @param color silhouette border color
     * @param borderWidth silhouette border width
     */
    public SilhouetteRenderer(Mesh mesh, Color color, float borderWidth) {
        this.mesh = mesh;
        this.borderWidth = borderWidth;
        
        //TODO find a different way, avoid modifying the base mesh if possible
        final Shell shape = (Shell) mesh.getShape();
        
        MeshUtilities.convertToTriangleAdjency(shape);
        //prepare program
        
        //build material actor

        material.setDiffuse(color);
        final MaterialActor materialActor = new MaterialActor(material,false);

        //build shell actor
        final ActorExecutor shellActor = Actors.buildExecutor(mesh, shape);

        //silhouette actor
        final SilhouetteActor silhouetteActor = new SilhouetteActor();
                
        program = new ActorProgram();
        program.getActors().add(silhouetteActor);
        program.getActors().add(shellActor);
        program.getActors().add(materialActor);
        program.setExecutor(shellActor);
    }
        
    /**
     * Set silhouette border color.
     * 
     * @param c 
     */
    public void setColor(Color c){
        material.setDiffuse(c);
    }

    /**
     * Set silhouette border width.
     * 
     * @param borderWidth 
     */
    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        
        if(!program.isOnGpuMemory()){
            program.compile(context);
        }
        
        program.render(context, camera, mesh);
        
    }
    
    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
        }
    }
    
    private class SilhouetteActor extends DefaultActor{

        public SilhouetteActor() {
            super(new Chars("Silhouette"),false,null,null,null,SILHOUETTE_GE,null,true,false);
        }
                
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            if(uniWidth==null){
                uniWidth = program.getUniform(new Chars("borderWidth"));
            }
            uniWidth.setFloat(context.getGL().asGL2ES2(), borderWidth);
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniWidth = null;
        }
        
    }
    
}
