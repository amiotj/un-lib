
package science.unlicense.engine.opengl.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL2ES2;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.io.IOException;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.system.path.Paths;

/**
 * A leaf panel which displays on opengl view.
 * 
 * @author Johann Sorel
 */
public class WGLCanvas extends WGLNode{

    private final DefaultGLProcessContext context = new DefaultGLProcessContext();
    private GLSource drawable;
    private AnimatorThread thread;
    private FBO fbo;
    
    private volatile boolean running = true;
    private final GLCallback gllistener = new GLCallback() {
            public void dispose(GLSource glad) {
                context.dispose(glad);
            }
            public void execute(GLSource glad) {
                //resize fbo if needed
                if(fbo!=null){
                    final Extent ext = getEffectiveExtent();
                    fbo.resize(glad.getGL(), (int)ext.get(0), (int)ext.get(1));
                }
                context.execute(glad);
            }
        };
    
    private ActorProgram program;
    
    public WGLCanvas() {
    }

    public GLSource getDrawable() {
        return drawable;
    }

    public DefaultGLProcessContext getContext() {
        return context;
    }

    public FBO getFinalFbo() {
        return fbo;
    }

    public void setFinalFbo(FBO fbo) {
        this.fbo = fbo;
    }
    
    public void startRendering(){
        if(thread != null) return;
        running = true;
        thread = new AnimatorThread();
        thread.start();
    }

    public void stopRendering(){
        if(thread == null) return;
        running = false;
        synchronized(thread.LOCK){
            thread.LOCK.notifyAll();
        }
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Loggers.get().log(ex, science.unlicense.api.logging.Logger.LEVEL_INFORMATION);
        }
        thread = null;
    }
    
    /**
     * Dispose opengl resources.
     */
    public void dispose(){
        drawable.dispose();
    }
    
    private class AnimatorThread extends Thread{
        private final Object LOCK = new Object();
        public void run() {
            while(running){
                context.execute(drawable);
                try {
                    sleep(500);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    @Override
    protected void renderGL(GLProcessContext context, Matrix4x4 SceneToWidget) {
        super.renderGL(context, SceneToWidget);
        
        if(drawable==null){
            
            try{
                final PaintExecutor exec = new PaintExecutor();
                program = new ActorProgram();
                program.getActors().add(exec);
                program.setExecutor(exec);
                program.compile((RenderContext) context);
            }catch(IOException ex){
                ex.printStackTrace();
            }
        }
        
        program.render((RenderContext) context, null, null);
        
    }
    
    private class PaintExecutor extends DefaultActor implements ActorExecutor {

        private Uniform uSampler;
        private Uniform uMVP;
    
        public PaintExecutor() throws IOException {
            super(new Chars("GLCanvas"), false, 
                    Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-texture-0-ve.glsl")),
                    null,
                    null,
                    null,
                    Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-texture2d-3-fr.glsl")),
                    true, true);
        }

        @Override
        public void render(ActorProgram program, RenderContext context, CameraMono camera, GLNode node) {
            if(fbo!=null){
                if(uMVP==null){
                    uMVP = program.getUniform(new Chars("MVP"));
                    uSampler = program.getUniform(new Chars("SAMPLER"));
                }
                
                final GL2ES2 gl = context.getGL().asGL2ES2();
                final Texture texture = fbo.getColorTexture();
                texture.loadOnGpuMemory(gl);

                final Extent frameExt = getFrame().getSize();
                final double frameWidth = frameExt.get(0);
                final double frameHeight = frameExt.get(1);
                final Matrix4x4 wgToGL = new Matrix4x4().setToIdentity();
                wgToGL.set(0, 0, 2.0/frameWidth);
                wgToGL.set(1, 1, -2.0/frameHeight);
                wgToGL.set(0, 3, -1);
                wgToGL.set(1, 3, +1);
                
                final Extent effectiveExtent = getEffectiveExtent();
                final double width = effectiveExtent.get(0);
                final double height = effectiveExtent.get(1);
                final Matrix4x4 scale = new Matrix4x4(
                        width/2.0,0,0,0,
                        0,-height/2.0,0,0,
                        0,0,1,0,
                        0,0,0,1
                );
                
                //the triangles are made to fill [-1...+1]
                //compute transformation
                
                Affine ttt = getNodeToRootSpace();
                Matrix4x4 nodeToWg = new Matrix4x4().setToIdentity();
                nodeToWg.set(0, 0, ttt.get(0, 0));
                nodeToWg.set(0, 1, ttt.get(0, 1));
                nodeToWg.set(1, 0, ttt.get(1, 0));
                nodeToWg.set(1, 1, ttt.get(1, 1));
                nodeToWg.set(0, 3, ttt.get(0, 2));
                nodeToWg.set(1, 3, ttt.get(1, 2));
                
                Matrix m = wgToGL.multiply(nodeToWg).multiply(scale);

                program.enable(gl);
                uMVP.setMat4(gl, m.toArrayFloat());

                //bind texture and sampler
                final int[] reservedTexture = context.getResourceManager().reserveTextureId();
                gl.glActiveTexture(reservedTexture[0]);
                texture.bind(gl);
                uSampler.setInt(gl, reservedTexture[1]);
                GLUtilities.checkGLErrorsFail(gl);

                gl.glDrawArrays(GL_TRIANGLES,0,6);
                GLUtilities.checkGLErrorsFail(gl);

                //release sampler
                gl.glActiveTexture(reservedTexture[0]);
                texture.unbind(gl);
                context.getResourceManager().releaseTextureId(reservedTexture[0]);

                program.disable(gl);
            }
        }
    }
}
