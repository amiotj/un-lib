
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 * Distance field mapping.
 * 
 * UV buffer is declared on the Mesh.
 * Can handle sprite.
 *
 * @author Johann Sorel
 */
public final class DistanceFieldMapping extends UVMapping {

    public DistanceFieldMapping() {
    }

    public DistanceFieldMapping(Texture2D texture) {
        super(texture);
    }

}
