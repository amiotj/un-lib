

package science.unlicense.engine.opengl.renderer;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.operation.SkinBuffer;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.Actors;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * Silhouette/outline rendering using backface method.
 * this consist by enlarging the mesh, and render only the backfaces.
 * 
 * TODO : DRAFT WORK, experimental
 * 
 * @author Johann Sorel
 */
public class SilhouetteRendererBackFaceMethod extends AbstractRenderer {
        
    private final Mesh mesh;
    private Shape shape;
    private ActorProgram program;
    private Color color;
    private float width;
    private DummyMesh resizedMesh;

    /**
     * 
     * @param mesh
     * @param color silhouette border color
     * @param width silhouette border width
     */
    public SilhouetteRendererBackFaceMethod(Mesh mesh, Color color, float width) {
        this.mesh = mesh;
        this.color = color;
        this.width = width;

        state.setEnable(GLC.GETSET.State.DITHER, true);
        state.setEnable(GLC.GETSET.State.MULTISAMPLE, true);
        state.setEnable(GLC.GETSET.State.SAMPLE_ALPHA_TO_COVERAGE, true);
        state.setEnable(GLC.GETSET.State.DEPTH_TEST, true);
        state.setEnable(GLC.GETSET.State.BLEND, true);
        state.setDepthMask(true);
        state.setDepthFunc(GL_LESS);
        state.setCulling(GLC.CULLING.FRONT);
        
        shape = mesh.getShape();
        
        if(shape instanceof SkinShell){
            resizedMesh = new DummyMesh();
            resizedMesh.copy(mesh);
        
            //make a buffer expanding around bones
            final SkinShell ss = ((SkinShell)shape).copy();
            
            //copy the vertices vbo, we are going to modify it
            VBO vertices = ss.getVertices();
            vertices = new VBO(vertices.getPrimitiveBuffer().toFloatArray(), vertices.getSampleCount());
            
            //copy morphs vertices
            //TODO : doesn't work well yet
//            MorphSet morphs = ss.getMorphs();
//            if(morphs!=null){
//                final Sequence cp = new ArraySequence(morphs.getMorphs());
//                for(int i=0,n=cp.getSize();i<n;i++){
//                    final MorphTarget m = (MorphTarget) cp.get(i);
//                    VBO mv = ss.getVertices();
//                    mv = new VBO(mv.getBuffer().toFloatArray(), mv.getTupleSize());
//                    m.setVertices(mv);
//                    
//                    //set those vertices on the shell and run the buffer
//                    ss.setVertices(mv);
//                    new SkinBuffer(ss, 1, width);
//                }
//            }
            
            ss.setVertices(vertices);
            new SkinBuffer(ss, 1, width);
            resizedMesh.setShape(ss);
            shape = ss;
            
        }
//        else if(shape instanceof Shell){
//            //scale for shell centroid
//            //TODO doesn't work well
//            final BBox bbox = resizedMesh.getShape().getBBox();
//            final Tuple center = bbox.getMiddle();
//            final Matrix trsMatrix = new Matrix4();
//            trsMatrix.setCol(3, new Vector(center).negate().getValues());
//            final Matrix trsMatrix2 = new Matrix4();
//            trsMatrix2.setCol(3, center.getValues());
//
//            final Matrix4 scaleMatrix = new Matrix4().setToIdentity();
//            scaleMatrix.localScale(1+width);
//
//            Matrix matrix = resizedMesh.getNodeTransform().asMatrix().copy();
//            matrix.localAdd(trsMatrix);
//            matrix.localMultiply(scaleMatrix);
//            matrix.localAdd(trsMatrix2);
//            resizedMesh.getNodeTransform().set(matrix);
//        }
        
    }
        
    /**
     * Set silhouette border color.
     * 
     * @param c 
     */
    public void setColor(Color c){
        color = c;
    }

    /**
     * Set silhouette border width.
     * 
     * @param borderWidth 
     */
    public void setBorderWidth(float borderWidth) {
        this.width = borderWidth;
    }
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        
        if(program==null){
            final Material material = new Material(mesh.getMaterial());
            material.setDiffuse(color);
            final MaterialActor materialActor = new MaterialActor(material,false);
            final ActorExecutor shellActor = Actors.buildExecutor(resizedMesh, shape);
        
            program = new ActorProgram();
            program.getActors().add(shellActor);
            program.getActors().add(materialActor);
            program.setExecutor(shellActor);
        }
        
        if(!program.isOnGpuMemory()){
            program.compile(context);
        }
        
        //disable depth writing
        final GL gl = context.getGL();
        gl.asGL1().glDepthMask(false);
        
        program.render(context, camera, resizedMesh);
                
        //reactive depth writing
        gl.asGL1().glDepthMask(true);
    }
    
    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
            program = null;
        }
        ((SkinShell)resizedMesh.getShape()).getVertices().unloadFromGpuMemory(context.getGL());
    }
        
    private class DummyMesh extends Mesh{
        @Override
        public SceneNode getParent() {
            return mesh.getParent();
        }
    }
    
}
