

package science.unlicense.engine.opengl.phase;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Sequence of phases.
 *
 * @author Johann Sorel
 */
public class PhaseSequence extends AbstractPhase {

    private final Sequence phases = new ArraySequence();

    public Sequence getPhases() {
        return phases;
    }

    public void processInt(GLProcessContext context) throws GLException {
        for(int i=0,n=phases.getSize();i<n;i++){
            final Phase phase = (Phase)phases.get(i);
            if(!phase.isEnable()) continue;
            phase.process(context);
        }
    }

}
