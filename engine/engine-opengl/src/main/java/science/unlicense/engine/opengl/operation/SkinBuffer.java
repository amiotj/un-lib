
package science.unlicense.engine.opengl.operation;

import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.impl.geometry.operation.Nearest;
import science.unlicense.impl.geometry.operation.NearestExecutors;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.math.Vector;


/**
 * Shrink or expand mesh skin around it's armature.
 * 
 * @author Johann Sorel
 */
public class SkinBuffer extends ShellVisitor{

    private final Vector v0 = new Vector(3);
    private final Vector v1 = new Vector(3);
    private final Vector vNew = new Vector(3);
    private final Vector constant = new Vector(3);
    private final double a;
    private final double b;
    
    public SkinBuffer(SkinShell shell, double a, double b) {
        super(shell);
        this.a = a;
        this.b = b;
        
        visit();
    }

    @Override
    protected void visit(Vertex vertex) {
        if(vertex.visited) return;
        
        int nbBone = 0;
        for(int i=0;i<vertex.jointWeights.length;i++){
            if(vertex.jointWeights[i]>0) nbBone++;
        }
        
        Vector pushOrigin;
        if(nbBone==1){
            v0.setAll(0);
            vertex.joints[0].getNodeToRootSpace().transform(v0, v0);
            pushOrigin = v0;
            
        }else if(nbBone==2){
            v0.setAll(0);
            vertex.joints[0].getNodeToRootSpace().transform(v0, v0);
            v1.setAll(0);
            vertex.joints[1].getNodeToRootSpace().transform(v1, v1);
            vNew.set(vertex.vertice);
            
            //find nearest
            Nearest n = new Nearest(new science.unlicense.impl.geometry.Point(vNew), new Segment(v0, v1));
            Geometry[] res;            
            try {
                res = (Geometry[]) NearestExecutors.POINT_LINE.execute(n);
            } catch (OperationException ex) {
               throw new RuntimeException(ex);
            }
            v0.set(((science.unlicense.impl.geometry.Point)res[1]).getCoordinate());
            pushOrigin = v0;
            
        }else{
            //TODO            
            v0.setAll(0);
            vertex.joints[0].getNodeToRootSpace().transform(v0, v0);
            v1.setAll(0);
            vertex.joints[1].getNodeToRootSpace().transform(v1, v1);
            vNew.set(vertex.vertice);
            
            //find nearest
            Nearest n = new Nearest(new science.unlicense.impl.geometry.Point(vNew), new Segment(v0, v1));
            Geometry[] res;            
            try {
                res = (Geometry[]) NearestExecutors.POINT_LINE.execute(n);
            } catch (OperationException ex) {
               throw new RuntimeException(ex);
            }
            v0.set(((science.unlicense.impl.geometry.Point)res[1]).getCoordinate());
            pushOrigin = v0;
            
        }
        
        vNew.set(vertex.vertice);
        vNew.localSubtract(pushOrigin);
                        
        constant.set(vNew);
        constant.localNormalize();
        constant.localScale(b);
        vNew.localScale(a);
        vNew.localAdd(pushOrigin);
        vNew.localAdd(constant);
        vNew.toArrayFloat(vertex.vertice);
        
    }

}
