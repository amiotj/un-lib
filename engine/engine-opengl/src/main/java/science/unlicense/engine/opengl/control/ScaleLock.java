
package science.unlicense.engine.opengl.control;

import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Matrix4x4;

/**
 * Preserve scale, based on canvas size.
 * TODO, a parameter in the scale is missing, ratio is not exact.
 * @author Johann Sorel
 */
public class ScaleLock implements Updater{

    private final GLNode target;
    private final double scale;

    public ScaleLock(GLNode target, double scale) {
        this.target = target;
        this.scale = scale;
    }

    public void update(RenderContext context, GLNode node) {
        double scaley = 1.0/context.getViewRectangle().getHeight();
        scaley *= scale;
        Matrix4x4 m = new Matrix4x4().setToIdentity();
        m.set(0, 0, scaley);
        m.set(1, 1, scaley);
        m.set(2, 2, scaley);
        target.getNodeTransform().set(m);
    }
}
