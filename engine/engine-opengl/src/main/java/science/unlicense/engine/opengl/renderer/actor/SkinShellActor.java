
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.TBO;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;

/**
 *
 * @author Johann Sorel
 */
public class SkinShellActor extends ShellActor {
    
    public static final Chars LAYOUT_JOINTWEIGHT = new Chars("l_jointweight");
    public static final Chars LAYOUT_JOINTINDEX = new Chars("l_jointindex");
    public static final Chars LAYOUT_DEFORMTYPE = new Chars("l_deformtype");
    
    protected static final Chars PROG_OP_VERTEX_MODEL  = new Chars("    outData.position_model = posModel;");
    protected static final Chars PROG_OP_VERTEX_WORLD  = new Chars("    outData.position_world = M * posModel;");
    protected static final Chars PROG_OP_VERTEX_CAMERA = new Chars("    outData.position_camera = MV * posModel;");
    protected static final Chars PROG_OP_VERTEX_PROJ   = new Chars("    outData.position_proj = MVP * posModel;");
    protected static final Chars PROG_OP_NORMAL_MODEL  = new Chars("    outData.normal_model = norModel.xyz;");
    protected static final Chars PROG_OP_NORMAL_WORLD  = new Chars("    outData.normal_world = (M * norModel).xyz;");
    protected static final Chars PROG_OP_NORMAL_CAMERA = new Chars("    outData.normal_camera = (MV * norModel).xyz;");
    protected static final Chars PROG_OP_NORMAL_PROJ   = new Chars("    outData.normal_proj = (MVP * norModel).xyz;");

    protected static final Chars PROG_OP_BUILDIN_PROJ   = new Chars("    gl_Position = MVP * posModel;");

    private static final ShaderTemplate SHADER_VE;
    static {
        try{
            SHADER_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/shellskin-tbo-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    //shader information, holds weight,index buffer,vertex,normal
    private String base = null;
    private Uniform uniTbo;
    private Uniform uniNbJoint;
    private VertexAttribute attVertice;
    private VertexAttribute attNormal;
    private VertexAttribute attUv;
    private VertexAttribute attTangent;
    private VertexAttribute attJointWeight;
    private VertexAttribute attJointIndex;
    private VertexAttribute attDeformType;
    private int[] reservedTexture;

    public SkinShellActor(Mesh mesh) {
        super(mesh);
    }

    private SkinShell getShell(){
        return (SkinShell) mesh.getShape();
    }

    public Chars getReuseUID() {
        final SkinShell shell = getShell();
        Chars uid = super.getReuseUID();
        final int maxWeightPerVertex = shell.getMaxWeightPerVertex();
        uid = uid.concat(new Chars("_"+maxWeightPerVertex));
        return uid;
    }

    public boolean isDirty() {
        final SkinShell shell = getShell();
        //we don't test the joint vbo because we use a reduced list
        return super.isDirty() || shell.getWeights().isDirty();
    }

    /**
     * {@inheritDoc }
     *
     * Override direct shell shader.
     *
     * @param ctx
     * @param template
     * @param tess
     * @param geom
     */
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        if(concurrent) return;
        if(initialized) throw new RuntimeException("This actor has been initialized by another program but not disposed before reuse.");
        initialized = true;
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();

        
        vs.append(MeshActor.TEMPLATE_VE);
        if(tess)tc.append(MeshActor.TEMPLATE_TC);
        if(tess)te.append(MeshActor.TEMPLATE_TE);
        if(geom)gs.append(MeshActor.TEMPLATE_GE);
        if(fg!=null){
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_M).concat(';'));
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_V).concat(';'));
            fg.addOperation(MeshActor.OP_SET_M);
            fg.addOperation(MeshActor.OP_SET_V);
            fg.append(MeshActor.TEMPLATE_FR);
        }
        
        //shaders
        final ShaderTemplate cp = new ShaderTemplate(ShaderTemplate.SHADER_VERTEX);
        cp.append(SHADER_VE);

        final SkinShell shell = getShell();
        final int maxWeightPerVertex = shell.getMaxWeightPerVertex();
        if(maxWeightPerVertex < 1){
            //should not happen if model is correct but we are tolerant
            cp.replaceTexts(new Chars("$weightType"), new Chars("float[1]"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("int[1]"));
        }else if(maxWeightPerVertex == 1){
            cp.replaceTexts(new Chars("$weightType"), new Chars("float[1]"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("int[1]"));
        }else if(maxWeightPerVertex == 2){
            cp.replaceTexts(new Chars("$weightType"), new Chars("vec2"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("ivec2"));
        }else if(maxWeightPerVertex == 3){
            cp.replaceTexts(new Chars("$weightType"), new Chars("vec3"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("ivec3"));
        }else if(maxWeightPerVertex == 4){
            cp.replaceTexts(new Chars("$weightType"), new Chars("vec4"));
            cp.replaceTexts(new Chars("$indextType"), new Chars("ivec4"));
        }else if(maxWeightPerVertex>4){
            throw new RuntimeException("number of joint per vertex must be between 1 and 4. Was "+maxWeightPerVertex);
        }
        vs.append(cp);

        //load VBOs and build program
        if(shell.getVertices()!=null){
            //always set the layout, might be used by some other shader actor
            vs.addLayout(PROG_LAYOUT_VERTEX);
            vs.addVariableOut(PROG_OUT_VERTEX_MODEL); vs.addOperation(PROG_OP_VERTEX_MODEL);
            vs.addVariableOut(PROG_OUT_VERTEX_WORLD); vs.addOperation(PROG_OP_VERTEX_WORLD);
            vs.addVariableOut(PROG_OUT_VERTEX_CAMERA); vs.addOperation(PROG_OP_VERTEX_CAMERA);
            vs.addVariableOut(PROG_OUT_VERTEX_PROJ); vs.addOperation(PROG_OP_VERTEX_PROJ);
        }
        if(shell.getNormals()!=null){
            vs.addLayout(PROG_LAYOUT_NORMAL);
            vs.addVariableOut(PROG_OUT_NORMAL_MODEL); vs.addOperation(PROG_OP_NORMAL_MODEL);
            vs.addVariableOut(PROG_OUT_NORMAL_WORLD); vs.addOperation(PROG_OP_NORMAL_WORLD);
            vs.addVariableOut(PROG_OUT_NORMAL_CAMERA); vs.addOperation(PROG_OP_NORMAL_CAMERA);
            vs.addVariableOut(PROG_OUT_NORMAL_PROJ); vs.addOperation(PROG_OP_NORMAL_PROJ);
        }
        if(shell.getUVs()!=null){
            vs.addLayout(PROG_LAYOUT_UV);
            vs.addVariableOut(PROG_OUT_UV);
            vs.addOperation(PROG_OP_UV);
        }
        if(shell.getTangents()!=null){
            vs.addLayout(PROG_LAYOUT_TANGENT);
            vs.addVariableOut(PROG_OUT_TANGENT_MODEL); vs.addOperation(PROG_OP_TANGENT_MODEL);
            vs.addVariableOut(PROG_OUT_TANGENT_WORLD); vs.addOperation(PROG_OP_TANGENT_WORLD);
            vs.addVariableOut(PROG_OUT_TANGENT_CAMERA); vs.addOperation(PROG_OP_TANGENT_CAMERA);
            vs.addVariableOut(PROG_OUT_TANGENT_PROJ); vs.addOperation(PROG_OP_TANGENT_PROJ);
        }
        
        vs.addOperation(PROG_OP_BUILDIN_PROJ);

    }

    /**
     * {@inheritDoc }
     */
    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        final SkinShell shell = getShell();
        
        //ensure weights and indexes are loaded
        shell.getWeights().loadOnGpuMemory(gl);
        shell.getJointIndexes().loadOnGpuMemory(gl);
        
        if(base == null){
            //get all uniform and attributes ids
            uniNbJoint      = program.getUniform(new Chars("NBJOINT"));
            uniTbo          = program.getUniform(new Chars("JOINTSAMPLER"));
            attVertice      = program.getVertexAttribute(LAYOUT_VERTEX, gl);
            attNormal       = program.getVertexAttribute(LAYOUT_NORMAL, gl);
            attUv           = program.getVertexAttribute(LAYOUT_UV, gl);
            attTangent      = program.getVertexAttribute(LAYOUT_TANGENT, gl);
            attJointWeight  = program.getVertexAttribute(LAYOUT_JOINTWEIGHT, gl);
            attJointIndex   = program.getVertexAttribute(LAYOUT_JOINTINDEX, gl);
            attDeformType   = program.getVertexAttribute(LAYOUT_DEFORMTYPE, gl);
        }
        final int maxWeightPerVertex = shell.getMaxWeightPerVertex();
        uniNbJoint.setInt(gl, maxWeightPerVertex);
        
        //bind TBO
        final MultipartMesh mpm = (MultipartMesh) mesh.getParent();
        final TBO tbo = mpm.getTBO();
        tbo.loadOnGpuMemory(gl);
        
        //update tbo if needed, it is used by several skins at the same time
        //check update time to update it only once by rendering
        if(mpm.getTboUpdate()!=context.getTimeNano()){
            mpm.setTboUpdate(context.getTimeNano());
            mpm.updateTBO();
            tbo.updateData(gl);
        }
        
        
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        tbo.bind(gl);
        GLUtilities.checkGLErrorsFail(gl);
        uniTbo.setInt(gl, reservedTexture[1]);        
        GLUtilities.checkGLErrorsFail(gl);

        //bind buffers
        attVertice.enable(gl, shell.getVertices());
        attNormal.enable(gl, shell.getNormals());
        attUv.enable(gl, shell.getUVs());
        attTangent.enable(gl, shell.getTangents());
        attJointWeight.enable(gl, shell.getWeights());
        attJointIndex.enable(gl, shell.getJointIndexes());
        attDeformType.enable(gl, shell.getDeformVBO());
        
        //indices
        shell.getIndexes().loadOnGpuMemory(gl);
        shell.getIndexes().bind(gl);
    }

    /**
     * {@inheritDoc }
     */
    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        attVertice.disable(gl);
        attNormal.disable(gl);
        attUv.disable(gl);
        attTangent.disable(gl);
        attJointWeight.disable(gl);
        attJointIndex.disable(gl);
        attDeformType.disable(gl);
                
        // unbind TBO
        final MultipartMesh mpm = (MultipartMesh) mesh.getParent();
        final TBO tbo = mpm.getTBO();
        gl.glActiveTexture(reservedTexture[0]);
        tbo.unbind(gl);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniTbo = null;
        uniNbJoint = null;
        attVertice = null;
        attNormal = null;
        attUv = null;
        attTangent = null;
        attJointWeight = null;
        attJointIndex = null;
        attDeformType = null;
    }

}
