
package science.unlicense.engine.opengl.phase;

import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.resource.FBO;

/**
 * Render to default output.
 * 
 * Output variables :
 * - glColor = Diffuse color
 * 
 * @author Johann Sorel
 */
public class DirectRenderPhase extends RenderPhase {
    
    public DirectRenderPhase(GLNode root, CameraMono camera) {
        super(root, camera);
    }
    
    public DirectRenderPhase(GLNode root, CameraMono camera, FBO fbo) {
        super(root, camera, fbo);
    }
    
}
