
package science.unlicense.engine.opengl.terrain;

import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.collection.primitive.IntSequence;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.image.Image;

/**
 *
 * @author Johann Sorel
 */
public class Terrain extends Mesh {

    private Image heightMap;
    private double scale;
    private double offset;
    
    public Terrain() {
    }

    public Image getHeightMap() {
        return heightMap;
    }

    public void setHeightMap(Image heightMap) {
        this.heightMap = heightMap;
    }
    
    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }
    
    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }
    
    public void build(){
        final TupleBuffer sm = heightMap.getRawModel().asTupleBuffer(heightMap);
        final double[] samples = new double[sm.getSampleCount()];
        final int[] coords = new int[2];
        final int width = (int) heightMap.getExtent().getL(0);
        final int height = (int) heightMap.getExtent().getL(1);
        
        //TODO we could use less points in the IBO by using triangle strips.
        final int nbVertex = width*height;
        final float[] vertices = new float[nbVertex*3];
        final IntSequence ib = new IntSequence();
        int k=0;
        for(int y=0;y<height;y++){
            for(int x=0;x<width;x++){
                coords[0]=x;coords[1]=y;
                sm.getTupleDouble(coords, samples);
                vertices[k++]=x;
                vertices[k++]=(float)(samples[0]*scale+offset);
                vertices[k++]=y;
                
                if(y<height-1 && x<width-1){
                    int i1 = y*width + x;
                    int i2 = y*width + x + 1;
                    int i3 = (y+1)*width + x;
                    int i4 = (y+1)*width + x + 1;
                    ib.put(i1);
                    ib.put(i3);
                    ib.put(i2);
                    ib.put(i3);
                    ib.put(i4);
                    ib.put(i2);
                }
            }
        }
        
        final Shell shell = new Shell();
        shell.setVertices(new VBO(DefaultBufferFactory.wrap(vertices), 3));
        shell.setIndexes(new IBO(ib.toArrayInt(), 3), IndexRange.TRIANGLES(0, ib.getSize()));
        shell.calculateNormals();
        shell.calculateBBox();
        
        setShape(shell);
        
    }
    
}
