
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.character.Chars;
import science.unlicense.api.event.EventSource;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * A phase in the rendering process.
 *
 * @author Johann Sorel
 */
public interface Phase extends EventSource {

    public static final Chars PROPERTY_ENABLE = new Chars("Enable");
    
    /**
     * Phase id, can be used to identify phases.
     * Default value is an empty char sequence.
     * 
     * @return Chars, never null
     */
    Chars getId();
    
    /**
     * Set phase id.
     * 
     * @param id not null
     */
    void setId(Chars id);
    
    /**
     * Indicate if phase is enable.
     * Disabled phases are not processed.
     * 
     * @return true if enable
     */
    boolean isEnable();

    /**
     * Set phase enabled state.
     * 
     * @param enable true to enable phase
     */
    void setEnable(boolean enable);

    /**
     * Process this phase.
     * 
     * @param context
     * @throws GLException
     */
    void process(GLProcessContext context) throws GLException;

    /**
     * Release any resources used.
     * 
     * @param ctx 
     */
    void dispose(GLProcessContext ctx);
    
}
