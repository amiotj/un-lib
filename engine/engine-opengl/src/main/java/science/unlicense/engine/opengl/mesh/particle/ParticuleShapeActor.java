
package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.Actor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.renderer.actor.MeshActor;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_M;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_P;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_V;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;
import science.unlicense.system.path.Paths;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;

/**
 *
 * @author Johann Sorel
 */
public class ParticuleShapeActor extends DefaultActor implements ActorExecutor {

    public static final Chars LAYOUT_VELOCITY = new Chars("l_velocity");
    public static final Chars LAYOUT_TIME = new Chars("l_time");
    public static final Chars UNIFORM_GRAVITY = new Chars("GRAVITY");
    public static final Chars UNIFORM_TIME = new Chars("TIME");
    public static final Chars UNIFORM_LIFESPAN = new Chars("LIFESPAN");
    
    private static final ShaderTemplate TEMPLATE;
    static {
        try{
            TEMPLATE = ShaderTemplate.create(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/particle/particle-emiter-0-ve.glsl")), ShaderTemplate.SHADER_VERTEX);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final ParticuleShape shape;
    private Actor presenterActor;

    //shader variables
    private VertexAttribute attVelocity;
    private VertexAttribute attTime;
    private Uniform uniformM;
    private Uniform uniformV;
    private Uniform uniformP;
    private Uniform uniTime = null;
    private Uniform uniGravity = null;
    private Uniform uniLifeSpan = null;
        
    public ParticuleShapeActor(ParticuleShape shape) {
        super(new Chars("ParticuleShape"),false,TEMPLATE,null,null,null,null,true,true);
        this.shape = shape;
    }

    private Actor getPresenterActor(){
        if(presenterActor==null){
            presenterActor = shape.getPresenter().createActor();
        }
        return presenterActor;
    }

    @Override
    public boolean usesGeometryShader() {
        return super.usesGeometryShader() || getPresenterActor().usesGeometryShader();
    }

    @Override
    public boolean usesTesselationShader() {
        return super.usesTesselationShader() || getPresenterActor().usesTesselationShader();
    }
    
    public int getMinGLSLVersion() {
        return TEMPLATE.getMinGLSLVersion();
    }

    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();
        
        vs.append(MeshActor.TEMPLATE_VE);
        if(tess)tc.append(MeshActor.TEMPLATE_TC);
        if(tess)te.append(MeshActor.TEMPLATE_TE);
        if(geom)gs.append(MeshActor.TEMPLATE_GE);
        if(fg!=null)fg.append(MeshActor.TEMPLATE_FR);
        
        super.initProgram(ctx, template,tess,geom);
        
        getPresenterActor().initProgram(ctx, template, tess, geom);
    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        
        final GL2ES2 gl = context.getGL().asGL2ES2();
        if(uniTime==null){
            uniTime = program.getUniform(UNIFORM_TIME);
            uniGravity = program.getUniform(UNIFORM_GRAVITY);
            uniLifeSpan = program.getUniform(UNIFORM_LIFESPAN);
        }
        uniTime.setFloat(gl, (float)context.getTimeNano()/1000000000f);
        uniLifeSpan.setFloat(gl, (float) shape.getLifeSpan());
        uniGravity.setVec3(gl, shape.getGravity().toArrayFloat());
        
        getPresenterActor().preDrawGL(context, program);
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        getPresenterActor().postDrawGL(context, program);
    }
    
    @Override
    public void render(ActorProgram program, RenderContext context, CameraMono camera, GLNode node) {
        
        final Mesh mesh = (Mesh) node;
        
        if(uniformM==null){
            try {
                uniformM = program.getUniform(UNIFORM_M);
                uniformV = program.getUniform(UNIFORM_V);
                uniformP = program.getUniform(UNIFORM_P);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        
        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        //prepare actors
        program.preExecutionGL(context, null);

        program.preDrawGL(context);

        ////////////////////////////////////////////////////////////////////////
        // DISPLAY : rendering /////////////////////////////////////////////////

        //set uniforms, Model->World->Projection matrix        
        try{
            uniformM.setMat4(gl,    node.getNodeToRootSpace().toMatrix().toArrayFloat());
            uniformV.setMat4(gl,    camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformP.setMat4(gl,    camera.getProjectionMatrix().toArrayFloat());
        }catch(Throwable ex){
            //we may catch java.lang.InvalidArgumentException: Can not inverse
            ex.printStackTrace();
            return;
        }

        render(context, mesh, program);
        
        program.postDrawGL(context);
    }
    
    private void render(GLProcessContext context, Mesh mesh, ActorProgram program) {

        final GL2ES2 gl = context.getGL().asGL2ES2();
        final ParticuleShape shape = (ParticuleShape) mesh.getShape();
        
        if(shape.getVertices() ==null){
            attVelocity = program.getVertexAttribute(ParticuleShapeActor.LAYOUT_VELOCITY, gl);
            attTime = program.getVertexAttribute(ParticuleShapeActor.LAYOUT_TIME, gl);
            
            final int nbParticule = shape.getNbParticule();
            final double spawnRadius = shape.getSpawnRadius();
            final float[] velocities = new float[nbParticule*3];
            final float[] starts = new float[nbParticule];
            final int[] idx = new int[nbParticule];

            float t_acum = 0;
            int j=0;

            for(int i=0; i<nbParticule; i+=1, j+=3, t_acum+=shape.getLifeSpan()/nbParticule){
                idx[i] = i;
                starts[i] = t_acum;            
                float randx =  (float)((Math.random()-0.5) * spawnRadius);
                float randz =  (float)((Math.random()-0.5) * spawnRadius);
                velocities[j+0] = randx;
                velocities[j+1] = 0.0f;
                velocities[j+2] = randz;
            }

            shape.setVertices(new VBO(velocities, 3));
            shape.setTimes(new VBO(starts, 1));
            shape.setIndex(new IBO(idx, 1));
            shape.setRange(IndexRange.POINTS(0, idx.length));
        }

        final IBO index = shape.getIndex();
        final IndexRange range = shape.getRange();
        
        index.loadOnGpuMemory(gl);

        attVelocity.enable(gl, shape.getVertices());
        attTime.enable(gl, shape.getTimes());

        index.bind(gl);
        range.draw(gl, true);
        GLUtilities.checkGLErrorsFail(gl);

        index.unbind(gl);
        attVelocity.disable(gl);
        attTime.disable(gl);
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        getPresenterActor().dispose(context);
        uniTime = null;
        uniGravity = null;
        uniLifeSpan = null;
    }
    
}
