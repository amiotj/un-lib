
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.ByVertexColorMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;

/**
 * By vertex color actor.
 * 
 * @author Johann Sorel
 */
public class ByVertexColorActor extends AbstractMaterialValueActor{

    public static final Chars LAYOUT_COLOR = new Chars("l_color");
    
    private static final ShaderTemplate DIFFUSE_COLOR_VE_BYVERTEX;
    private static final ShaderTemplate DIFFUSE_COLOR_TC_BYVERTEX;
    private static final ShaderTemplate DIFFUSE_COLOR_TE_BYVERTEX;
    private static final ShaderTemplate DIFFUSE_COLOR_FR_BYVERTEX;
    static {
        try{
            DIFFUSE_COLOR_VE_BYVERTEX = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/colortex-0-ve-byvertex.glsl"), ShaderTemplate.SHADER_VERTEX);
            DIFFUSE_COLOR_TC_BYVERTEX = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/colortex-1-tc-byvertex.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            DIFFUSE_COLOR_TE_BYVERTEX = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/colortex-2-te-byvertex.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
            DIFFUSE_COLOR_FR_BYVERTEX = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/colortex-3-fr-byvertex.glsl"), ShaderTemplate.SHADER_FRAGMENT);            
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars REUSE_ID_BYVERTEX = new Chars("ColorByVertex");
    
    private final ByVertexColorMapping mapping;

    //GL loaded informations
    private VertexAttribute attColor;
    
    public ByVertexColorActor(Chars produce, Chars method, Chars uniquePrefix, ByVertexColorMapping mapping) {
        super(Chars.EMPTY,false,produce,method,uniquePrefix,null,null,null,null,null);
        this.mapping = mapping;
    }

    public ByVertexColorMapping getMapping() {
        return mapping;
    }

    public boolean isDirty() {
        return mapping.isDirty();
    }

    public Chars getReuseUID() {
        return super.getReuseUID().concat(REUSE_ID_BYVERTEX);
    }

    public int getMinGLSLVersion() {
        return Maths.max(DIFFUSE_COLOR_VE_BYVERTEX.getMinGLSLVersion(), 
                         DIFFUSE_COLOR_FR_BYVERTEX.getMinGLSLVersion());
    }
    
    public void initProgram(final RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();
        mapping.setDirty(false);

        final ShaderTemplate vertexShader = template.getVertexShaderTemplate();
        final ShaderTemplate tessControlShader = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate tessEvalShader = template.getTesselationEvalShaderTemplate();

        vertexShader.append(DIFFUSE_COLOR_VE_BYVERTEX);
        vertexShader.replaceTexts(PRODUCE_MARKER, produce);
        vertexShader.replaceTexts(METHOD_MARKER, method);
        vertexShader.replaceTexts(PREFIX_MARKER, uniquePrefix);
        fragmentShader.append(DIFFUSE_COLOR_FR_BYVERTEX);
        fragmentShader.replaceTexts(PRODUCE_MARKER, produce);
        fragmentShader.replaceTexts(METHOD_MARKER, method);
        fragmentShader.replaceTexts(PREFIX_MARKER, uniquePrefix);

        //fill tesselation shader if needed
        if(tessControlShader!=null){
            tessControlShader.append(DIFFUSE_COLOR_TC_BYVERTEX);
            tessControlShader.replaceTexts(PRODUCE_MARKER, produce);
            tessControlShader.replaceTexts(METHOD_MARKER, method);
            tessControlShader.replaceTexts(PREFIX_MARKER, uniquePrefix);
            tessEvalShader.append(DIFFUSE_COLOR_TE_BYVERTEX);
            tessEvalShader.replaceTexts(PRODUCE_MARKER, produce);
            tessEvalShader.replaceTexts(METHOD_MARKER, method);
            tessEvalShader.replaceTexts(PREFIX_MARKER, uniquePrefix);
        }

    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        if(attColor==null) attColor = program.getVertexAttribute(LAYOUT_COLOR, gl);
        attColor.enable(gl, mapping.getColors());
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        if(attColor!=null) attColor.disable(gl);
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        attColor = null;
    }
    
}