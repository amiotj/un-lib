

package science.unlicense.engine.opengl.renderer;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Draw bounding box of current Mesh of MultiPartMesh
 * TODO
 * 
 * @author Johann Sorel
 */
public class DebugBBoxRenderer extends AbstractRenderer {
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        if(!(node instanceof Mesh)) return;
        
//        final Mesh mesh = (Mesh) node;
//        final BBox bbox = mesh.getShape().getBBox();
//        if(bbox==null) return;
//
//        final Mesh bboxMesh = new GeometryMesh(bbox);
//        bboxMesh.getMaterial().setLightVulnerable(false);
//        bboxMesh.setWireframe(true);
//        
//        mesh.addChild(bboxMesh);
//        RenderPhase.processRenderers(bboxMesh, context, camera);
//        mesh.removeChild(bboxMesh);
//        bboxMesh.dispose(context);
    }

    public void dispose(GLProcessContext context) {
    }
    
}
