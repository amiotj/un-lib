
package science.unlicense.engine.opengl.shader;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Char;
import science.unlicense.api.character.CharEncodings;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArrayOrderedSet;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.ArraySequenceSet;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Pair;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.io.ByteInputStream;
import science.unlicense.api.io.CharInputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.path.Path;
import science.unlicense.api.number.Int32;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.api.io.ArrayInputStream;
import science.unlicense.api.math.Maths;
import science.unlicense.system.path.Paths;

/**
 * A shader template represent an empty shader file.
 * Textures,materials, lights,... will fill it with various elements :
 * - uniform
 * - in/out variables
 * - functions
 * - ...
 *
 * The generated shader will have the form :
 *
 * {layouts}
 * {uniforms}
 * {structures}
 * in inModel {
 *     {variables_in}
 * } inData;
 * out outModel {
 *     {variables_out}
 * } outData;
 * {variables_ws}
 * {functions}
 *
 * main(){
 *   {operations}
 * }
 *
 * @author Johann Sorel
 */
public class ShaderTemplate {

    public static final int SHADER_VERTEX = 0;
    public static final int SHADER_TESS_CONTROL = 1;
    public static final int SHADER_TESS_EVAL = 2;
    public static final int SHADER_GEOMETRY = 3;
    public static final int SHADER_FRAGMENT = 4;

    private static final Chars AUTO_LAYOUT_INDEX = new Chars("$IDX");
    
    private static final Chars DELIM_STRUCTURE = new Chars("<STRUCTURE>");
    private static final Chars DELIM_LAYOUT = new Chars("<LAYOUT>");
    private static final Chars DELIM_UNIFORM = new Chars("<UNIFORM>");
    private static final Chars DELIM_VARIABLE_IN = new Chars("<VARIABLE_IN>");
    private static final Chars DELIM_VARIABLE_WS = new Chars("<VARIABLE_WS>");
    private static final Chars DELIM_VARIABLE_OUT = new Chars("<VARIABLE_OUT>");
    private static final Chars DELIM_FUNCTION = new Chars("<FUNCTION>");
    private static final Chars DELIM_OPERATION = new Chars("<OPERATION>");

    private static final Chars[] DATA_VALUE_NAME = new Chars[]{
        new Chars("vsData"),
        new Chars("tsData"),
        new Chars("teData"),
        new Chars("geData"),
        new Chars("frData")
    };
    private static final Chars[] DATA_MODEL_NAME = new Chars[]{
        new Chars("VsDataModel"),
        new Chars("TsDataModel"),
        new Chars("TeDataModel"),
        new Chars("GeDataModel"),
        new Chars("FrDataModel")
    };

    private static final Dictionary LAYOUT_SIZE = new HashDictionary();
    static {
        LAYOUT_SIZE.add(new Chars("int"), 1);
        LAYOUT_SIZE.add(new Chars("float"), 1);
        LAYOUT_SIZE.add(new Chars("ivec2"), 1);
        LAYOUT_SIZE.add(new Chars("ivec3"), 1);
        LAYOUT_SIZE.add(new Chars("ivec4"), 1);
        LAYOUT_SIZE.add(new Chars("vec2"), 1);
        LAYOUT_SIZE.add(new Chars("vec3"), 1);
        LAYOUT_SIZE.add(new Chars("vec4"), 1);
        LAYOUT_SIZE.add(new Chars("mat4"), 4);
        LAYOUT_SIZE.add(new Chars("mat4x4"), 4);
        LAYOUT_SIZE.add(new Chars("mat4x3"), 3);
        LAYOUT_SIZE.add(new Chars("mat4x2"), 2);
        LAYOUT_SIZE.add(new Chars("mat3"), 3);
        LAYOUT_SIZE.add(new Chars("mat3x3"), 3);
        LAYOUT_SIZE.add(new Chars("mat3x2"), 2);
        LAYOUT_SIZE.add(new Chars("mat2"), 2);
        LAYOUT_SIZE.add(new Chars("mat2x2"), 2);
        
        //TODO wait for GLSL parser and classes to have real types
        LAYOUT_SIZE.add(new Chars("float[1]"), 1);
        LAYOUT_SIZE.add(new Chars("float[2]"), 1);
        LAYOUT_SIZE.add(new Chars("float[3]"), 1);
        LAYOUT_SIZE.add(new Chars("float[4]"), 1);
        LAYOUT_SIZE.add(new Chars("float[5]"), 2);
        LAYOUT_SIZE.add(new Chars("float[6]"), 2);
        LAYOUT_SIZE.add(new Chars("float[7]"), 2);
        LAYOUT_SIZE.add(new Chars("float[8]"), 2);
        LAYOUT_SIZE.add(new Chars("int[1]"), 1);
        LAYOUT_SIZE.add(new Chars("int[2]"), 1);
        LAYOUT_SIZE.add(new Chars("int[3]"), 1);
        LAYOUT_SIZE.add(new Chars("int[4]"), 1);
        LAYOUT_SIZE.add(new Chars("int[5]"), 2);
        LAYOUT_SIZE.add(new Chars("int[6]"), 2);
        LAYOUT_SIZE.add(new Chars("int[7]"), 2);
        LAYOUT_SIZE.add(new Chars("int[8]"), 2);
    }
    

    private int type;
    private final CharBuffer buffer = new CharBuffer(CharEncodings.US_ASCII);
    private final Sequence structures = new ArraySequence();
    private final Sequence layouts = new ArraySequence();
    private final Sequence uniforms = new ArraySequence();
    private final ArraySequenceSet variablesIn = new ArraySequenceSet();
    private final ArraySequenceSet variablesWs = new ArraySequenceSet();
    private final ArraySequenceSet variablesOut = new ArraySequenceSet();
    private final Sequence functions = new ArraySequence();
    private final Sequence operations = new ArraySequence();
    private int inc = 0;
    private int minGlslVersion = GLC.GLSL.V110_GL20;

    public ShaderTemplate(int type){
        this.type = type;
    }
    
    public ShaderTemplate(ShaderTemplate toCopy){
        this.type = toCopy.type;
        append(toCopy);
    }

    public Sequence getStructures() {
        return structures;
    }

    public Sequence getLayouts() {
        return layouts;
    }

    public Sequence getUniforms() {
        return uniforms;
    }

    public ArraySequenceSet getVariablesIn() {
        return variablesIn;
    }

    public ArraySequenceSet getVariablesWs() {
        return variablesWs;
    }

    public ArraySequenceSet getVariablesOut() {
        return variablesOut;
    }

    public Sequence getFunctions() {
        return functions;
    }

    public Sequence getOperations() {
        return operations;
    }

    /**
     * Append given template instruction in this template.
     * @param other
     */
    public void append(ShaderTemplate other){
        this.minGlslVersion = Maths.max(this.minGlslVersion,other.minGlslVersion);
        this.structures.addAll(other.structures);
        this.layouts.addAll(other.layouts);
        this.uniforms.addAll(other.uniforms);
        this.variablesIn.addAll(other.variablesIn);
        this.variablesWs.addAll(other.variablesWs);
        this.variablesOut.addAll(other.variablesOut);
        this.functions.addAll(other.functions);
        this.operations.addAll(other.operations);
    }

    /**
     * Loops on all instructions replacing key text by value text in the dictionary.
     * This can be used to fill variables inserted in the template.
     *
     * @param replacement
     */
    public void replaceTexts(Dictionary replacement){
        final Iterator ite = replacement.getPairs().createIterator();
        while(ite.hasNext()){
            final Pair pair = (Pair) ite.next();
            final Chars oldc = (Chars)pair.getValue1();
            final Chars newc = (Chars)pair.getValue2();
            replaceTexts(oldc, newc);
        }
    }

    /**
     * Loops on all instructions replacing key text by value text in the dictionary.
     * This can be used to fill variables inserted in the template.
     */
    public void replaceTexts(Chars oldc, Chars newc){
        replace(structures, oldc, newc);
        replace(layouts, oldc, newc);
        replace(uniforms, oldc, newc);
        replace(variablesIn, oldc, newc);
        replace(variablesWs, oldc, newc);
        replace(variablesOut, oldc, newc);
        replace(functions, oldc, newc);
        replace(operations, oldc, newc);
    }

    private static void replace(Sequence seq, Chars oldc, Chars newc){
        for(int i=0,n=seq.getSize();i<n;i++){
            Chars candidate = (Chars) seq.get(i);
            candidate = candidate.replaceAll(oldc, newc);
            seq.replace(i, candidate);
        }
    }

    private static void replace(Collection seq, Chars oldc, Chars newc){
        final Iterator ite = seq.createIterator();
        while(ite.hasNext()){
            Chars candidate = (Chars) ite.next();
            Chars res = candidate.replaceAll(oldc, newc);
            if(!candidate.equals(res)){
                seq.remove(candidate);
                seq.add(res);
            }
        }
    }

    /**
     * Get the shader type, one of SHADER_*** constants.
     * @return int, shader type
     */
    public int getShaderType(){
        return type;
    }

    /**
     * Get the minimum GLSL version this actor needs.
     * Value is expected to be the same as the first line in a GLSL program.
     * Example : #110, #300, #400
     * 
     * @return minimum required version of GLSL
     */
    public int getMinGLSLVersion(){
        return minGlslVersion;
    }

    public void setMinGLSLVersion(int minGlslVersion) {
        this.minGlslVersion = minGlslVersion;
    }
        
    /**
     * @return true is modifications have been applied on this shader template
     */
    public boolean isModified(){
        return inc !=0
            || !layouts.isEmpty()
            || !uniforms.isEmpty()
            || !structures.isEmpty()
            || !variablesIn.isEmpty()
            || !variablesWs.isEmpty()
            || !variablesOut.isEmpty()
            || !functions.isEmpty()
            || !operations.isEmpty();
    }

    /**
     * Clear the template of all registered elements.
     * Returning it to it's original empty form.
     */
    public void reset(){
        type = SHADER_VERTEX;
        layouts.removeAll();
        uniforms.removeAll();
        structures.removeAll();
        variablesIn.removeAll();
        variablesWs.removeAll();
        variablesOut.removeAll();
        functions.removeAll();
        operations.removeAll();
        inc = 0;
        minGlslVersion = GLC.GLSL.V110_GL20;
    }

    /**
     * Generate a unique variable name, this reduce the risk of variable
     * name clash in the shader.
     * @param name
     * @return Chars
     */
    public Chars makeUnique(Chars name){
        buffer.reset();
        buffer.append(new Char('$'));
        buffer.append(Int32.encode(inc++));
        buffer.append(name);
        return buffer.toChars();
    }

    public void addLayout(Chars code){
        layouts.add(code);
    }

    public void addUniform(Chars code){
        uniforms.add(code);
    }

    public void addStructure(Chars code){
        structures.add(code);
    }

    public void addVariableIn(Chars code){
        variablesIn.add(code);
    }

    public void addVariableWs(Chars code){
        variablesWs.add(code);
    }

    public void addVariableOut(Chars code){
        variablesOut.add(code);
    }

    public void addFunction(Chars code){
        functions.add(code);
    }

    public void addOperation(Chars code){
        operations.add(code);
    }

    public void loadFromChars(Chars text, int type) throws IOException{
        final ArrayInputStream is = new ArrayInputStream(text.toBytes());
        final CharInputStream ci = new CharInputStream(is, text.getEncoding(), new Char('\n'));
        load(ci, type);
    }

    public void load(Chars path, int type) throws IOException{
        final Path p = Paths.resolve(path);
        load(p, type);
    }

    public void load(Path p, int type) throws IOException{
        final ByteInputStream bi = p.createInputStream();
        final CharInputStream ci = new CharInputStream(bi, CharEncodings.UTF_8, new Char('\n'));
        load(ci, type);
    }

    private void load(CharInputStream ci, int type) throws IOException{
        this.type = type;
        boolean versionSet = false;

        Chars instructionType = null;
        for(Chars line=ci.readLine();line!=null;line=ci.readLine()){
            final Chars trimLine = line.trim();
            if(trimLine.isEmpty()) continue;
            line = line.trimEnd();

            if(trimLine.startsWith(new Chars("#version"))){
                if(versionSet) throw new IOException("GLSL #version can only be defined once in file");
                final Chars versionStr = trimLine.split(' ')[1];
                minGlslVersion = Int32.decode(versionStr);
                versionSet = true;
                continue;
            }
            if(trimLine.equals(DELIM_STRUCTURE)){ instructionType = DELIM_STRUCTURE; continue;}
            if(trimLine.equals(DELIM_LAYOUT   )){ instructionType = DELIM_LAYOUT;    continue;}
            if(trimLine.equals(DELIM_UNIFORM  )){ instructionType = DELIM_UNIFORM;   continue;}
            if(trimLine.equals(DELIM_VARIABLE_IN)){ instructionType = DELIM_VARIABLE_IN;  continue;}
            if(trimLine.equals(DELIM_VARIABLE_WS)){ instructionType = DELIM_VARIABLE_WS;  continue;}
            if(trimLine.equals(DELIM_VARIABLE_OUT)){instructionType = DELIM_VARIABLE_OUT;continue;}
            if(trimLine.equals(DELIM_FUNCTION )){ instructionType = DELIM_FUNCTION;  continue;}
            if(trimLine.equals(DELIM_OPERATION)){ instructionType = DELIM_OPERATION; continue;}

            if(     DELIM_STRUCTURE     == instructionType){ addStructure   (line);}
            else if(DELIM_LAYOUT        == instructionType){ addLayout      (line);}
            else if(DELIM_UNIFORM       == instructionType){ addUniform     (line);}
            else if(DELIM_VARIABLE_IN   == instructionType){ addVariableIn  (line);}
            else if(DELIM_VARIABLE_WS   == instructionType){ addVariableWs  (line);}
            else if(DELIM_VARIABLE_OUT  == instructionType){ addVariableOut (line);}
            else if(DELIM_FUNCTION      == instructionType){ addFunction    (line);}
            else if(DELIM_OPERATION     == instructionType){ addOperation   (line);}
            else {throw new IOException("Instruction type not set." + instructionType);}
        }

        ci.close();

        if(!versionSet) throw new IOException("GLSL #version has not been defined in file");
    }

    /**
     * Load template from text program template
     * @param text
     * @param type
     * @return ShaderTemplate
     * @throws science.unlicense.api.io.IOException
     */
    public static ShaderTemplate createFromChars(Chars text, int type) throws IOException{
        final ShaderTemplate template = new ShaderTemplate(0);
        template.loadFromChars(text, type);
        return template;
    }

    /**
     * Load template from given path.
     * @param path
     * @param type
     * @return ShaderTemplate
     * @throws science.unlicense.api.io.IOException
     */
    public static ShaderTemplate create(Chars path, int type) throws IOException{
        final ShaderTemplate template = new ShaderTemplate(0);
        template.load(path, type);
        return template;
    }

    /**
     * Load template from given path.
     * @param p
     * @param type
     * @return ShaderTemplate
     * @throws science.unlicense.api.io.IOException
     */
    public static ShaderTemplate create(Path p, int type) throws IOException{
        final ShaderTemplate template = new ShaderTemplate(0);
        template.load(p, type);
        return template;
    }


    /**
     * Generate the shader code.
     * @param previousStage : name of the previous stage
     * @return Chars
     */
    public Chars toCharacters(int previousStage){

        // HEADER
        buffer.reset();
        buffer.append("#version "+getMinGLSLVersion()+"\n");

        // LAYOUTS
        if(type==SHADER_VERTEX){
            //replace automatic indexes
            int index = 0;
            for(int i=0,n=layouts.getSize();i<n;i++){
                Chars layout = (Chars)layouts.get(i);
                layout = layout.replaceAll(AUTO_LAYOUT_INDEX, Int32.encode(index));
                index += getSize(layout);
                buffer.append(layout).append("\n");
            }
        }else{
            for(int i=0,n=layouts.getSize();i<n;i++){
                buffer.append((Chars)layouts.get(i)).append("\n");
            }
        }
        buffer.append("\n");

        // STRUCTURES
        for(int i=0,n=structures.getSize();i<n;i++){
            buffer.append((Chars)structures.get(i)).append("\n");
        }
        buffer.append("\n");

        // UNIFORMS
        for(int i=0,n=uniforms.getSize();i<n;i++){
            buffer.append((Chars)uniforms.get(i)).append("\n");
        }
        buffer.append("\n");

        // INPUT STRUCTURE
        if(!variablesIn.isEmpty()){
            buffer.append("\nin InModel{\n");
            final Object[] inVars = sort(variablesIn);
            for(int i=0;i<inVars.length;i++){
                final Chars line = (Chars) inVars[i];
                buffer.append("    ").append(line).append("\n");
            }
            buffer.append("} inData");
            if(type == SHADER_TESS_CONTROL || type == SHADER_TESS_EVAL || type == SHADER_GEOMETRY){
                buffer.append("[]");
            }
            buffer.append(";\n");
        }

        // OUTPUT VARIABLES
        if(!variablesOut.isEmpty()){
            buffer.append("\nout OutModel{\n");
            final Object[] outVars = sort(variablesOut);
            for(int i=0;i<outVars.length;i++){
                final Chars line = (Chars) outVars[i];
                buffer.append("    ").append(line).append("\n");
            }
            buffer.append("} outData");
            if(type == SHADER_TESS_CONTROL){
                buffer.append("[]");
            }
            buffer.append(";\n");
        }

        // WORK VARIABLES
        buffer.append("\n");
        for(Iterator ite = variablesWs.createIterator();ite.hasNext();){
            final Chars line = (Chars) ite.next();
            buffer.append(line).append("\n");
        }
        buffer.append("\n");

        // FUNCTIONS
        for(int i=0,n=functions.getSize();i<n;i++){
            buffer.append((Chars)functions.get(i)).append("\n");
        }
        buffer.append("\n");

        // MAIN FUNCTION
        buffer.append("void main(){\n");
        for(int i=0,n=operations.getSize();i<n;i++){
            buffer.append((Chars)operations.get(i)).append("\n");
        }
        buffer.append("}");


        //Now replace inData and outData with linking names
        Chars text = buffer.toChars();
        if(previousStage != -1){
            text = text.replaceAll(new Chars("inData"), DATA_VALUE_NAME[previousStage]);
            text = text.replaceAll(new Chars("InModel"), DATA_MODEL_NAME[previousStage]);
        }
        text = text.replaceAll(new Chars("outData"), DATA_VALUE_NAME[type]);
        text = text.replaceAll(new Chars("OutModel"), DATA_MODEL_NAME[type]);

        return text;
    }

    private static Object[] sort(Collection data){
        final Object[] array = data.toArray();
        Arrays.sort(array);
        return array;
    }

    private static int getSize(Chars layout){
        int occ = layout.getFirstOccurence(')');
        layout = layout.truncate(occ+1, layout.getCharLength());
        final Chars type = layout.trim().split(' ')[1];
        final Integer size = (Integer) LAYOUT_SIZE.getValue(type);
        if(size==null) throw new RuntimeException("Unknown type size : "+type);
        return size;
    }
    
}
