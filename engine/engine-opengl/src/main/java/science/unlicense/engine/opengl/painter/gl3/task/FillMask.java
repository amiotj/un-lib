
package science.unlicense.engine.opengl.painter.gl3.task;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.AffineRW;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Font;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.Affines;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class FillMask extends PainterTask{

    private static final BBox SHADER_BBOX = new BBox(new double[]{0,0}, new double[]{1,1});

    private final Font font;
    private final int unicode;

    private final Object paint;
    private final Affine2 mv;
    private final AlphaBlending blending;

    
    public FillMask(Font font, int unicode, Object paint, Affine2 mv, AlphaBlending blending) {
        this.font = font;
        this.unicode = unicode;
        this.paint = paint;
        this.mv = mv;
        this.blending = blending;
    }

    public void execute(GL3Painter2D worker) {

        final Color color;
        if(paint instanceof ColorPaint){
            color = ((ColorPaint)paint).getColor();
        }else if(paint instanceof Color){
            color = (Color) paint;
        }else{
            color = Color.BLUE;
        }

        BBox glyphBox = font.getMetadata().getGlyphBox();
        AffineRW stretchedMatrix = Projections.stretched(SHADER_BBOX, glyphBox);

        try {
            final FontPage fontPage = worker.getFontPage(font);
            final Texture2D texture = fontPage.load();
            stretchedMatrix = mv.multiply(stretchedMatrix);

            final Matrix3x3 maskTrs = fontPage.getGlyphTransform(unicode);
            final Affine mvp = worker.p.multiply(stretchedMatrix);

            Tuple trs = Affines.transformNormal(stretchedMatrix.invert(), new Vector(1,0),null);
            trs = maskTrs.transform(new Vector(trs,0));
            double ratio = Vectors.length(trs.getXY().toArrayDouble());

            //render using the mask
            configureBlending(worker,blending);
            worker.programs.fillMaskProg.enable(worker.gl);
            worker.programs.fillMaskProg.uniformMVP.setMat3(worker.gl, mvp.toMatrix().toArrayFloat());
            worker.programs.fillMaskProg.uniformMASKTRS.setMat3(worker.gl, maskTrs.toArrayFloat());
            worker.programs.fillMaskProg.uniformCOLOR.setVec4(worker.gl, color.toRGBAPreMul());
            worker.programs.fillMaskProg.uniformBUFFER.setFloat(worker.gl, (float) (ratio/2.0));

            //bind texture and sampler
            final int[] reservedTexture = new int[]{33984,0};
            worker.gl.glActiveTexture(reservedTexture[0]);
            texture.loadOnGpuMemory(worker.gl);
            texture.bind(worker.gl);
            worker.programs.fillMaskProg.uniformMASK.setInt(worker.gl, reservedTexture[1]);
            GLUtilities.checkGLErrorsFail(worker.gl);

            worker.gl.glDrawArrays(GL_TRIANGLE_FAN,0,4);
            GLUtilities.checkGLErrorsFail(worker.gl);

            //release sampler
            worker.gl.glActiveTexture(reservedTexture[0]);
            texture.unbind(worker.gl);

            worker.programs.fillMaskProg.disable(worker.gl);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
