
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.resource.AbstractResource;
import science.unlicense.impl.gpu.opengl.resource.ResourceException;
import science.unlicense.impl.gpu.opengl.shader.ShaderProgram;

/**
 *
 * @author Johann Sorel
 */
public class ActorProgram extends AbstractResource {

    private static final Logger LOGGER = Loggers.get();
    
    protected final Sequence transformFeedBackVars = new ArraySequence();
    protected int transformFeedBackMode = GLC.TRANSFORM_FEEDBACK.MODE_INTERLEAVED;
    protected final Sequence actors = new ArraySequence();
    
    private Chars uid;
    private ShaderProgram program;
    private ActorExecutor executor;

    public ActorProgram() {
    }

    public ActorExecutor getExecutor() {
        return executor;
    }

    public void setExecutor(ActorExecutor executor) {
        this.executor = executor;
    }
    
    public Sequence getActors() {
        return actors;
    }
    
    public Sequence getTransformFeedBackVars() {
        return transformFeedBackVars;
    }

    public int getTransformFeedBackMode() {
        return transformFeedBackMode;
    }

    public void render(RenderContext context, CameraMono camera, GLNode node) {
        if(program!=null && executor!=null){
            executor.render(this, context, camera, node);
        }
    }
    
    /**
     * One of GLC.TRANSFORM_FEEDBACK
     * 
     * @param transformFeedBackMode 
     */
    public void setTransformFeedBackMode(int transformFeedBackMode) {
        this.transformFeedBackMode = transformFeedBackMode;
    }
    
    /**
     * Merge and compile the shader parts of each actors in a shader program.
     * 
     * @param context 
     */
    public void compile(final RenderContext context){               
        //ensure previous program is released
        releaseProgram(context);
        
        //build the program
        final GL gl = context.getGL();
        uid = buildReuseID(gl);
        
        Chars[] trsfbVars = null;
        int trsfbMode = -1;
        if(!getTransformFeedBackVars().isEmpty()){
            trsfbVars = new Chars[getTransformFeedBackVars().getSize()];
            Collections.copy(getTransformFeedBackVars(), trsfbVars, 0);
            trsfbMode = getTransformFeedBackMode();
        }
        
        if(uid==null){
            LOGGER.debug(new Chars("Create a not reusable program."));
            //we can not reuse this program, compile and return it
            final Chars[] shaderTexts = buildShaderCode(context);
            program = new ShaderProgram(shaderTexts, trsfbVars, trsfbMode);
            program.loadOnGpuMemory(gl);
            //check for errors
            GLUtilities.checkGLErrorsFail(gl);
        }else{
            program = context.getResourceManager().aquireProgram(uid);
            if(program==null){
                LOGGER.debug(new Chars("Create new reusable program with id : "+uid));
                final Chars[] shaderTexts = buildShaderCode(context);
                program = new ShaderProgram(shaderTexts, trsfbVars, trsfbMode);
                program.loadOnGpuMemory(gl);
                GLUtilities.checkGLErrorsFail(gl);
                context.getResourceManager().registerProgram(uid, program);
            }
        }
        
    }

    /**
     * Loop on actors to call 'preExecutionGL'.
     */
    public void preExecutionGL(RenderContext context, Object candidate){
        //prepare actors
        for(int i=0,n=actors.getSize();i<n;i++){
            ((Actor)actors.get(i)).preExecutionGL(context, candidate);
        }
    }

    /**
     * Enables program and loop on actors to call 'preDrawGL'.
     */
    public void preDrawGL(RenderContext context){
        final GL gl = context.getGL();
        //select shader program
        enable(gl);
        //prepare actors
        for(int i=0,n=actors.getSize();i<n;i++){
            ((Actor)actors.get(i)).preDrawGL(context, this);
        }
        GLUtilities.checkGLErrorsFail(gl);
    }

    /**
     * Disable program and loop on actors to call 'postDrawGL'.
     */
    public void postDrawGL(RenderContext context){
        final GL gl = context.getGL();
        //clean actors
        for(int i=0,n=actors.getSize();i<n;i++){
            ((Actor)actors.get(i)).postDrawGL(context, this);
        }
        //cleanup
        disable(gl);
    }
    
    public void releaseProgram(final GLProcessContext context){
        if(executor!=null){
            executor.dispose(context);
        }
        if(program!=null){
            context.getResourceManager().releaseProgram(uid, context);
            program = null;
        }
        for(int i=0,n=actors.getSize();i<n;i++){
            ((Actor)actors.get(i)).dispose(context);
        }
    }
    
    /***
     * Build the GLSL text code from actors.
     * 
     * @param ctx
     * @return Chars[5] {vertex, tesseControl, tessEval, geom, fragment}
     */
    public Chars[] buildShaderCode(final RenderContext ctx){
        
        //check if tesselation or geometry phases will be neede
        boolean tess = false;
        boolean geom = false;

        for(int i=0,n=actors.getSize();i<n;i++){
            final Actor actor = (Actor) actors.get(i);
            tess |= actor.usesTesselationShader();
            geom |= actor.usesGeometryShader();
        }


        //prepare shader program template
        final ShaderProgramTemplate template = new ShaderProgramTemplate();

        //fill templates with actor code parts
        for(int i=0,n=actors.getSize();i<n;i++){
            final Actor actor = (Actor) actors.get(i);
            actor.initProgram(ctx,template,tess,geom);
        }

        final ShaderTemplate vertexShader      = template.getVertexShaderTemplate();
        final ShaderTemplate tessControlShader = !tess ? null : template.getTessaltionControlShaderTemplate();
        final ShaderTemplate tessEvalShader    = !tess ? null : template.getTesselationEvalShaderTemplate();
        final ShaderTemplate geometryShader    = !geom ? null : template.getGeometryShaderTemplate();
        final ShaderTemplate fragmentShader    = template.getFragmentShaderTemplate();
        
        //ensure in and out parameters match
        final Sequence seq = new ArraySequence();
        seq.add(vertexShader);
        if(tessControlShader!=null && tessControlShader.isModified()){
            seq.add(tessControlShader);
            seq.add(tessEvalShader);
        }
        if(geometryShader!=null && geometryShader.isModified()){
            seq.add(geometryShader);
        }
        if(fragmentShader.isModified()){
            seq.add(fragmentShader);
        }

        int previousId = -1;
        ShaderTemplate previous = null;
        ShaderTemplate current;
        ShaderTemplate next;
        final Chars[] shaderTexts = new Chars[5];
        for(int i=0,n=seq.getSize();i<n;i++){
            current = (ShaderTemplate) seq.get(i);
            next = (i<n-1) ? (ShaderTemplate) seq.get(i+1) : null;

            //ensure in and out parameters match
            ensureInOutDataMatch(previous, current);
            ensureInOutDataMatch(current, next);
            //generate text program
            shaderTexts[current.getShaderType()] = current.toCharacters(previousId);

            previousId = current.getShaderType();
            previous = current;
        }
        
        return shaderTexts;
    }
    
    private static void ensureInOutDataMatch(ShaderTemplate stage1, ShaderTemplate stage2){
        if(stage1==null || stage2==null)return;
        stage1.getVariablesOut().addAll(stage2.getVariablesIn());
        stage2.getVariablesIn().addAll(stage1.getVariablesOut());

    }

    /**
     * Get shader program id.
     *
     * @return shader program id
     */
    public int getGpuID() {
        return program==null ? -1 : program.getGpuID();
    }

    /**
     * Indicate if this program uses the tesselation phases.
     * @return true if tesselation is present.
     */
    public boolean usesTesselationShader(){
        for(int i=0,n=actors.getSize();i<n;i++){
            if(((Actor)actors.get(i)).usesTesselationShader() ) return true; 
        }
        return false;
    }

    /**
     * Indicate if this program uses the geometry phase.
     * @return true if geometry is present.
     */
    public boolean usesGeometryShader(){
        for(int i=0,n=actors.getSize();i<n;i++){
            if(((Actor)actors.get(i)).usesGeometryShader()) return true; 
        }
        return false;
    }

    public final Uniform getUniform(Chars name){
        return program.getUniform(name);
    }

    public final Uniform getUniform(Chars name, GL gl, int type){
        return program.getUniform(name, gl, type);
    }

    public final VertexAttribute getVertexAttribute(Chars name, GL gl){
        return program.getVertexAttribute(name, gl, 1);
    }
    
    public final VertexAttribute getVertexAttribute(Chars name, GL gl, int slotsize){
        return program.getVertexAttribute(name, gl, slotsize);
    }
    
    /**
     * {@inheritDoc }
     */
    public boolean isOnSystemMemory() {
        return true;
    }

    /**
     * {@inheritDoc }
     */
    public boolean isOnGpuMemory() {
        return getGpuID()>=0;
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnSystemMemory(GL gl) throws ResourceException {
        //can't do that
        
    }

    /**
     * {@inheritDoc }
     */
    public void loadOnGpuMemory(GL gl) throws ResourceException {
        throw new RuntimeException("Should not be called");
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromSystemMemory(GL gl) throws ResourceException {
    }

    /**
     * {@inheritDoc }
     */
    public void unloadFromGpuMemory(GL gl) throws ResourceException {
        throw new RuntimeException("Should not be called");
    }

    /**
     * Activate the program.
     * 
     * OpenGL constraints : GL2ES2
     * 
     * @param gl
     */
    public void enable(GL gl){
        program.enable(gl);
    }

    /**
     * Desactivate the program.
     * 
     * OpenGL constraints : GL2ES2
     * 
     * @param gl
     */
    public void disable(GL gl){
        program.disable(gl);
    }

    public Chars buildReuseID(GL gl){
        final CharBuffer buffer = new CharBuffer();
        buffer.append(System.identityHashCode(gl));
        
        for(int i=0;i<actors.getSize();i++){
            final Actor actor = (Actor) actors.get(i);
            final Chars uid = actor.getReuseUID();
            if(uid==null) return null;
            buffer.append(uid).append('-');
        }
        
        return buffer.toChars();
    }
    
}
