

package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;

/**
 * List multiple morph target.
 * 
 * @author Johann Sorel
 */
public class MorphSet {
    
    private final Sequence morphs = new ArraySequence();
       
    /**
     * List morph names.
     * @return Chars[] never null
     */
    public Chars[] getMorphNames(){
        final Chars[] names = new Chars[morphs.getSize()];
        for(int i=0;i<names.length;i++){
            final MorphTarget mf = (MorphTarget) morphs.get(i);
            names[i] = mf.getName();
        }
        return names;
    }
    
    /**
     * Get morph frame with given name.
     * @param name searched frame name
     * @return MorphFrame or null if no frame with this name exist
     */
    public MorphTarget getMorph(Chars name){
        for(int i=0,n=morphs.getSize();i<n;i++){
            final MorphTarget mf = (MorphTarget) morphs.get(i);
            if(name.equals(mf.getName())){
                return mf;
            }
        }
        return null;
    }
    
    /**
     * Modifiable sequence of all morph.
     * @return Sequence never null, can be empty
     */
    public Sequence getMorphs() {
        return morphs;
    }
    
    public MorphSet deepCopy(){
        final MorphSet cp = new MorphSet();
        for(int i=0,n=morphs.getSize();i<n;i++){
            cp.morphs.add(((MorphTarget)morphs.get(i)).deepCopy());
        }
        return cp;
    }
    
}
