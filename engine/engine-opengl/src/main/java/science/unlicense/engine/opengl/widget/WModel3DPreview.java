

package science.unlicense.engine.opengl.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.color.Color;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.control.OrbitController;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.AbsoluteLayout;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.api.layout.StackConstraint;
import science.unlicense.api.layout.StackLayout;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Angles;
import science.unlicense.api.math.Maths;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model3d.Model3DStore;
import science.unlicense.api.model3d.Model3Ds;
import science.unlicense.api.path.Path;
import science.unlicense.api.store.StoreException;
import science.unlicense.engine.opengl.light.AmbientLight;
import science.unlicense.engine.opengl.light.PointLight;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.operation.ShellVisitor;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class WModel3DPreview extends WContainer{

    private final Path path;
    
    public WModel3DPreview(Path path) {
        super(new BorderLayout());
        this.path = path;
        
        final WButton wload = new WButton(new Chars("Load model"), null,new EventListener() {
            public void receiveEvent(Event event) {
                new Thread(){
                    public void run() {
                        load();
                    }
                }.start();
            }
        });

        final WContainer absContainer = new WContainer(new AbsoluteLayout());
        absContainer.getChildren().add(wload);
        addChild(absContainer, BorderConstraint.CENTER);
    }
    
    private void load(){
        try {
            final WGLCanvas canvas = new WGLCanvas();
            final GLProcessContext context = canvas.getContext();

            //prepare the output FBO
            final int width = (int)getEffectiveExtent().get(0);
            final int height = (int)getEffectiveExtent().get(1);
            final FBO fbo = new FBO(width, height);
            fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
            fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));
            canvas.setFinalFbo(fbo);


            final Model3DStore store = Model3Ds.read(path);

            final GLNode scene = new GLNode();
            final GLNode base = new GLNode();
            scene.getChildren().add(base);

            //calculate bbox of objet
            final Collection col = store.getElements();
            final Iterator ite = col.createIterator();
            while(ite.hasNext()){
                final Object obj = ite.next();
                if(obj instanceof Node){
                    if(obj instanceof GLNode){
                        base.getChildren().add((GLNode)obj);
                    }
                }
            }

            //calculate a few informations
            //0 : nb mesh
            //1 : nb vertices
            //2 : nb triangles
            final int[] nbs = new int[3];
            scene.accept(new DefaultNodeVisitor(){
                @Override
                public Object visit(Node node, Object context) {
                    if(node instanceof Mesh){
                        nbs[0]++;
                        Shape shape = ((Mesh)node).getShape();
                        if(shape instanceof Shell){
                            nbs[1] += ((Shell) shape).getVertices().getTupleCount();
                            final Shell shell = (Shell) shape;
                            final ShellVisitor s = new ShellVisitor(shell) {
                                protected void visit(ShellVisitor.Vertex vertex) {}
                                protected void visit(ShellVisitor.Triangle candidate) {
                                    nbs[2]++;
                                    super.visit(candidate);
                                }
                            };
                            s.visit();
                        }
                    }
                    return super.visit(node, context);
                }
            }, null);
            final WContainer infos = new WContainer(new FormLayout(4,4));
            infos.getStyle().getSelfRule().setProperties(new Chars("margin:5;"));
            infos.addChild(new WLabel(new Chars("Meshes : "+nbs[0])), FillConstraint.builder().coord(0, 0).build());
            infos.addChild(new WLabel(new Chars("Vertices : "+nbs[1])), FillConstraint.builder().coord(0, 1).build());
            infos.addChild(new WLabel(new Chars("Triangles : "+nbs[2])), FillConstraint.builder().coord(0, 2).build());


            BBox bbox = base.getBBox();
            if(bbox==null)bbox = new BBox(3);

            //create a target node at the center of the bbox
            final GLNode target = new GLNode();
            target.getNodeTransform().getTranslation().set(bbox.getMiddle());
            target.getNodeTransform().notifyChanged();
            scene.getChildren().add(target);



            //some lightning
            scene.getChildren().add(new AmbientLight(Color.WHITE,Color.WHITE));

            final PointLight point = new PointLight();
            point.getNodeTransform().getTranslation().set(bbox.getUpper());
            point.getNodeTransform().getTranslation().localScale(1.2);
            point.getNodeTransform().notifyChanged();
            scene.getChildren().add(point);


            //build the scene
            final CameraMono camera = new CameraMono();
            camera.setNearPlane(0.1);
            camera.setFarPlane(10000);
            target.getChildren().add(camera);

            //calculate camera position from object bbox
            final OrbitController controller = new OrbitController((EventSource)this, new Vector(0, 1, 0), new Vector(1, 0, 0), target);
            controller.configureDefault();
            camera.getUpdaters().add(controller);

            //set camera at good distance
            final double fov = camera.getFieldOfView();
            final double spanX = bbox.getSpan(0);
            final double distX = spanX / Math.tan(fov);
            final double spanY = bbox.getSpan(1);
            final double distY = spanY / Math.tan(fov);
            // x2 because screen space is [-1...+1]
            // x1.2 to compensate perspective effect
            final float dist = (float)(Maths.max(distX,distY) * 2.0 * 1.2);

            controller.setDistance(dist);
            controller.setVerticalAngle(Angles.degreeToRadian(15));
            controller.setHorizontalAngle(Angles.degreeToRadian(15));

            context.getPhases().add(new ClearPhase(fbo));
            context.getPhases().add(new UpdatePhase(scene));
            context.getPhases().add(new RenderPhase(scene,camera,fbo));
            canvas.startRendering();

            getChildren().removeAll();
            setLayout(new StackLayout());
            addChild(canvas, new StackConstraint(0));
            addChild(infos, new StackConstraint(1));

            
        } catch (IOException ex) {
            Loggers.get().warning(ex);
        } catch (StoreException ex) {
            Loggers.get().warning(ex);
        }
    }
    
}
