

package science.unlicense.engine.opengl.phase;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.engine.opengl.GLExecutable;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Stores single execution tasks.
 * 
 * @author Johann Sorel
 */
public class TasksPhase extends AbstractFboPhase{

    private final Sequence tasks = new ArraySequence();

    public TasksPhase() {
    }

    public TasksPhase(Chars id) {
        super(id);
    }
    
    public void addTask(GLExecutable executable) {
        synchronized(tasks){
            tasks.add(executable);
        }
    }
    
    public void runTasks(GLProcessContext ctx){
        synchronized(tasks){
            for(int i=0,n=tasks.getSize();i<n;i++){
                final GLExecutable task = (GLExecutable) tasks.get(i);
                task.setContext(ctx);
                task.execute();
            }
            tasks.removeAll();
        }
    }
    
    protected void processInternal(GLProcessContext ctx) {
        runTasks(ctx);
    }
    
}
