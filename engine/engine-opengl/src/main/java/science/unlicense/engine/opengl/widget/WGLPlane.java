
package science.unlicense.engine.opengl.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collection;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Hasher;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeMessage;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.Camera;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.gpu.GLBuffer;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * Display a UI container.
 * Will forward the WFrame mouse and keyboard events
 *
 * TODO : reuse/merge with WGLView
 * 
 * @author Johann Sorel
 */
public class WGLPlane extends Mesh implements EventListener{

    private static final Image EMPTY = Images.create(new Extent.Long(1, 1, 1),Images.IMAGE_TYPE_RGBA,GLBufferFactory.INSTANCE);
    private final WContainer container = new WContainer();
    
    //contains children with special 3d requierements
    private final Dictionary children3d = new HashDictionary(Hasher.IDENTITY);
    private final Collection toDispose = new ArraySequence();

    private final Triangle plan = new Triangle(
            new Vector(0,0,0),
            new Vector(0,1,0),
            new Vector(1,1,0));
    private Logger logger = Loggers.get();

    //event informations
    private boolean mouseInPlan = false;
    private final Camera camera;

    //texture updater
    private final WGLView.Value link = new WGLView.Value(){
        
        private Texture2D tex1 = null;
        private Texture2D tex2 = null;

        @Override
        public void swapTexture(Image base) {
//            UVMapping mapping = (UVMapping) getMaterial().getLayer(Layer.TYPE_DIFFUSE).getMapping();
//            mapping.setTexture((Texture2D) base);

            //swap texture buffer
//            tex2 = tex1;
//            tex1 = base;
//            return tex2;
        }
    };
    
    private WGLView view;
    
    public WGLPlane(final int width, final int height, Camera camera) {
        this(width,height,4,camera);
    }
    
    public WGLPlane(final int width, final int height, int nbSample, Camera camera) {
        this.camera = camera;

        //make container listen to the WPlan events
        addEventListener(MouseMessage.PREDICATE, container);
        addEventListener(KeyMessage.PREDICATE, container);

        final GLBuffer vertices = GLBufferFactory.INSTANCE.createFloat(12);
        vertices.cursorFloat().write(new float[]{
            0,0,0,
            0,1,0,
            1,1,0,
            1,0,0
        });
        final GLBuffer normals = GLBufferFactory.INSTANCE.createFloat(12);
        normals.cursorFloat().write(new float[]{
            0,1,0,
            0,1,0,
            0,1,0,
            0,1,0
        });

        final GLBuffer indices = GLBufferFactory.INSTANCE.createInt(6);
        indices.cursorInt().write(new int[]{
            1,0,3,
            3,2,1
        });

        final GLBuffer uv = GLBufferFactory.INSTANCE.createFloat(8);
        uv.cursorFloat().write(new float[]{
            0,1,
            0,0,
            1,0,
            1,1
        });

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3), IndexRange.TRIANGLES(0, 6));
        shell.setUVs(new VBO(uv,2));
        setShape(shell);


        final UVMapping texture = new UVMapping(new Texture2D(EMPTY,false,false));
        getMaterial().putOrReplaceLayer(new Layer(texture));
        
        //listen to container events to update 3d sub nodes
        container.addEventListener(NodeMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                NodeMessage ne = (NodeMessage) event.getMessage();
                while(ne!=null){
                    //TODO bug in events here, no sub events
//                    if(ne.getType() == NodeEvent.TYPE_UPDATE){
//                        final Event sub = ne.getTrigger();
//                        if(sub instanceof NodeEvent){
//                            ne = (NodeEvent) sub;
//                        }else{
//                            ne=null;
//                        }
//                    }else{
                        //update 3d children tree
                        sync=false;
                        break;
//                    }
                }
            }
        });
        container.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                if(Widget.PROPERTY_DIRTY.equals(pe.getPropertyName())){
                    link.putDirty((BBox) pe.getNewValue());
                }
            }
        });
                
        //updater will refresh the image at proper time in rendering cycle
        getUpdaters().add(new PlanUpdater());
        
        //set the container size
        container.getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint:$color-background;};"));
        container.setEffectiveExtent(new Extent.Double(width,height));
                container.getNodeTransform().setToTranslation(new double[]{width/2.0,height/2.0});
        container.setDirty();
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
        
    /**
     * Get the UI widget.
     * @return WContainer
     */
    public WContainer getContainer() {
        return container;
    }
    
    /**
     * Convert the mouse event from 3d space to widget space
     * @param me 3d space mouse event
     * @return MouseEvent or null if out of plan
     */
    private void transposeToWidget(Tuple position, MouseMessage me, KeyMessage ke){
        if(position==null)return;

        //adjust position with camera world and render area
        final CameraMono cam = (CameraMono)camera;
        final Rectangle renderArea = cam.getRenderArea();
        final double height = renderArea.getY()+renderArea.getHeight();
        double diffV = 0;
        final GLProcessContext context = cam.getContext();
        if(context!=null){ //TODO not nice, should find another way to obtain the window size
            diffV = context.getGLRectangle().getHeight()-height;
        }
        final Ray ray = cam.calculateRayWorldCS((int)(position.getX()-renderArea.getX()), (int)(position.getY()-diffV));

        //convert ray to plan space
        final Matrix wm = getRootToNodeSpace().toMatrix();
        final Vector temp = new Vector(ray.getPosition(),1);
        wm.transform(temp,temp);
        ray.getPosition().set(temp.getXYZ());
        temp.set(ray.getDirection());
        temp.setZ(0);
        wm.transform(temp,temp);
        ray.getDirection().set(temp.getXYZ());

        //calculate intersection point
        TupleRW inter;
        try {
            inter = ((Point)ray.intersection(plan)).getCoordinate();
        } catch (OperationException ex) {
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
            return;
        }

        if(inter==null || Double.isNaN(inter.getX()) || Double.isNaN(inter.getY()) ){
            //no intersection
            return;
        }

        //this is in 3d plan space
        inter = new DefaultTuple(inter.getX(), inter.getY(),1);
        
        //convert to image space        
        final int pixelWidth = (int) container.getEffectiveExtent().get(0);
        final int pixelHeight = (int) container.getEffectiveExtent().get(1);
        inter.setXY(
                inter.getX() * pixelWidth,
                inter.getY() * -pixelHeight + pixelHeight);
        final Extent extent = container.getEffectiveExtent();
        
        //convert to container transform space
        Tuple containerCSPoint = container.getNodeTransform().inverseTransform(inter,null);
        
        if(inter.getX()<0 || inter.getX()>extent.get(0) || inter.getY()<0 || inter.getY()>extent.get(1)){
            //out of plan
            if(mouseInPlan && me!=null){
                //mouse was in the plan, send a mouse exit event
                final MouseMessage event = new MouseMessage(MouseMessage.TYPE_EXIT, -1, -1, containerCSPoint, me.getMouseScreenPosition(), 0,false);
                mouseInPlan = false;
                if(hasListeners()) getEventManager().sendEvent(new Event(this, event));
            }
        }else{
            if(me!=null){
                if(mouseInPlan){
                    //mouse was already in the plan, just propage the event
                    final MouseMessage event = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(), me.getWheelOffset(),me.isDragging());
                    if(hasListeners()) getEventManager().sendEvent(new Event(this,event));
                    if(event.isConsumed())me.consume();
                }else{
                    //mouse was out of the plan, we first generate a mouse enter event
                    mouseInPlan = true;
                    final MouseMessage enterEvent = new MouseMessage(MouseMessage.TYPE_ENTER, -1, -1, containerCSPoint, me.getMouseScreenPosition(),0,me.isDragging());
                    if(hasListeners()) getEventManager().sendEvent(new Event(this,enterEvent));
                    //then propage the true event
                    final MouseMessage event = new MouseMessage(me.getType(), me.getButton(), me.getNbClick(), containerCSPoint, me.getMouseScreenPosition(),me.getWheelOffset(),me.isDragging());
                    if(hasListeners()) getEventManager().sendEvent(new Event(this,event));
                    if(event.isConsumed())me.consume();
                }
            }
            if(ke!=null){
                if(mouseInPlan && hasListeners()){
                    getEventManager().sendEvent(new Event(this, ke));
                }
            }
        }

    }

    public void dispose(GLProcessContext context) {
        view.dispose();
        super.dispose(context);
    }

    // sync possible 3D child nodes ////////////////////////////////////////////
    
    private boolean sync = false;

    protected Object getChildNode(int index) {
        sync3DChildren();
        return super.getChildNode(index);
    }

    private synchronized void sync3DChildren(){
        if(sync) return;
        sync = true;
        
        final Collection keys = children3d.getKeys();
        
        final WContainer root = getContainer();
        root.accept(new DefaultNodeVisitor() {
            public Object visit(Node node, Object context) {
                super.visit(node, context);
                if(node instanceof WGLNode){
                    final WGLNode glnode = (WGLNode) node;
                    GLNode scaleNode = (GLNode) children3d.getValue(glnode);
                    if(scaleNode==null){
                        //new node
                        final int pixelWidth = (int) container.getEffectiveExtent().get(0);
                        final int pixelHeight = (int) container.getEffectiveExtent().get(1);
                        final Matrix4x4 nodeToImage3D = new Matrix4x4().setToIdentity();
                        nodeToImage3D.set(0, 0, pixelWidth);
                        nodeToImage3D.set(1, 1, -pixelHeight);
                        nodeToImage3D.set(1, 3, pixelHeight);
                        
                        final GLNode inSceneNode = glnode.getInSceneNode();
                        scaleNode = new GLNode();
                        scaleNode.getChildren().add(inSceneNode);
                        scaleNode.getNodeTransform().set(nodeToImage3D.invert());
                        children3d.add(glnode, scaleNode);
                        getChildren().add(scaleNode);
                    }else{
                        //already existing node
                        keys.remove(glnode);
                    }
                }
                return null;
            }
        }, null);
        
        //dispose old childrens
        synchronized(toDispose){
            toDispose.addAll(keys);
        }
    }

    public Texture2D getTexture(){
        final UVMapping mapping = (UVMapping)getMaterial().getLayer(Layer.TYPE_DIFFUSE).getMapping();
        return mapping.getTexture();
    }
    
    // events //////////////////////////////////////////////////////////////////

    private Tuple position = null;

    public void receiveEvent(Event event) {

        if(!isVisible()) return;

        //catch frame mouse event and propagate to widgets
        final Class eventClass = event.getClass();
        if(eventClass == MouseMessage.class){
            final MouseMessage e = (MouseMessage) event.getMessage();
            position = e.getMousePosition();
            transposeToWidget(position,e,null);
        }else if(eventClass == KeyMessage.class){
            final KeyMessage e = (KeyMessage) event.getMessage();
            transposeToWidget(position,null,e);
        }

    }

    public Class[] getEventClasses() {
        return new Class[]{
            MouseMessage.class,
            KeyMessage.class
        };
    }

    private class PlanUpdater implements Updater{

        public void update(RenderContext context, GLNode node) {
            
            if(view==null){
                final GLSource sharedDrawable = null;//GLUtilities.createDummyDrawable(context.getGLDrawable());
                final DefaultGLProcessContext sharedCtx = new DefaultGLProcessContext();
                sharedDrawable.getCallbacks().add(sharedCtx);
        
                view = new WGLView(1, 1, 4, sharedCtx, link);
                view.setLogger(logger);
            }

            synchronized(toDispose){
                for(Iterator ite=toDispose.createIterator();ite.hasNext();){
                    final Object key = ite.next();
                    final GLNode todisp = (GLNode) children3d.remove(key);
                    getChildren().remove(todisp);
                    if(todisp!=null){
                        todisp.dispose(context);
                    }
                }
                toDispose.removeAll();
            }
            
            if(!isVisible()) return;

        }
    }

}
