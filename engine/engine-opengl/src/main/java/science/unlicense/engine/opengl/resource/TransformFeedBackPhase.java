
package science.unlicense.engine.opengl.resource;

import science.unlicense.api.gpu.opengl.GL1;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;

/**
 *
 * @author Johann Sorel
 */
public class TransformFeedBackPhase extends AbstractFboPhase {

    private TransformFeedback feedback;
    private ActorProgram program = new ActorProgram();
    private int primitiveType = GLC.TRANSFORM_FEEDBACK.PRIMITIVE.POINTS;

    public TransformFeedBackPhase() {
    }

    public TransformFeedback getFeedback() {
        return feedback;
    }

    public void setFeedback(TransformFeedback feedback) {
        this.feedback = feedback;
    }

    public void setProgram(ActorProgram program) {
        program = program;
    }

    public ActorProgram getProgram() {
        return program;
    }

    public ActorExecutor getExecutor() {
        return program.getExecutor();
    }

    public void setExecutor(ActorExecutor executor) {
        program.setExecutor(executor);
    }

    public int getPrimitiveType() {
        return primitiveType;
    }

    public void setPrimitiveType(int primitiveType) {
        this.primitiveType = primitiveType;
    }

    @Override
    protected void processInternal(GLProcessContext ctx) throws GLException {
        final GL1 gl = ctx.getGL().asGL1();

        gl.glEnable(GL_RASTERIZER_DISCARD);
        feedback.loadOnGpuMemory(gl);
        feedback.bind(gl);
        program.render((RenderContext) ctx, null, null);

        feedback.unbind(gl);
        gl.glDisable(GL_RASTERIZER_DISCARD);
    }

}
