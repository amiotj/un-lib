
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.TextureUtils;
import science.unlicense.api.image.Image;

/**
 * Two textures are used to map the mesh.
 * 
 * http://members.gamedev.net/JasonZ/Paraboloid/DualParaboloidMappingInTheVertexShader.pdf
 * http://www.ozone3d.net/tutorials/glsl_texturing_p04.php
 * 
 * @author Johann Sorel
 */
public final class DualParaboloidMapping implements Mapping {

    private Texture2D textureFront;
    private Texture2D textureBack;
    private Boolean opaque = null;

    public DualParaboloidMapping() {
    }

    public DualParaboloidMapping(Texture2D textureFront, Texture2D textureBack) {
        this.textureFront = textureFront;
        this.textureBack = textureBack;
    }

    public boolean isOpaque(){
        if(opaque==null) checkOpaque();
        return opaque;
    }

    private void checkOpaque(){
        //check opacity
        opaque = true;
        Image image = textureFront.getImage();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
        image = textureBack.getImage();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
    }

    public Texture2D getTextureFront() {
        return textureFront;
    }

    public void setTextureFront(Texture2D texture) {
        this.textureFront = texture;
    }
    
    public Texture2D getTextureBack() {
        return textureBack;
    }

    public void setTextureBack(Texture2D texture) {
        this.textureBack = texture;
    }

    public boolean isDirty() {
        return (textureFront!=null && textureFront.isDirty())
            || (textureBack!=null && textureBack.isDirty());
    }

    public void dispose(GLProcessContext context) {
        if(textureFront!=null){
            textureFront.unloadFromGpuMemory(context.getGL());
        }
        if(textureBack!=null){
            textureBack.unloadFromGpuMemory(context.getGL());
        }
    }

}
