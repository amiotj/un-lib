
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DataCursor;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * 
 * TODO : add subdivision tools to transform icosahedron in icosphere.
 * 
 * @author Sylvain Doremus (Castor3D engine)
 * @author Johann Sorel (adapted to unlicense and ported to java)
 */
public class IcoSphere extends Mesh {

    public IcoSphere(double m_radius) {
                
        final float phi = (float) ((1.0 + Math.sqrt(5.0)) / 2.0);
	final float X = (float) (m_radius / Math.sqrt( phi * Math.sqrt( 5.0)));
	final float Z = X * phi;
	final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(12*3);
	final Buffer normals = GLBufferFactory.INSTANCE.createFloat(12*3);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt(20*3);

        final DataCursor vertexCursor = vertices.cursor();
        final DataCursor normalCursor = normals.cursor();
        final DataCursor indiceCursor = indices.cursor();
        
        
        Vector vec = new Vector(3);
	// on crée les 12 points le composant
        vec.setXYZ(-X,  0,  Z);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( X,  0,  Z);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ(-X,  0, -Z);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( X,  0, -Z);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( 0,  Z,  X);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( 0,  Z, -X);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( 0, -Z,  X);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( 0, -Z, -X);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( Z,  X,  0);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ(-Z,  X,  0);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ( Z, -X,  0);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        vec.setXYZ(-Z, -X,  0);
        vertexCursor.writeFloat(vec.toArrayFloat());
        normalCursor.writeFloat(vec.localNormalize().toArrayFloat());
        
        indiceCursor.writeInt( 0).writeInt( 1).writeInt( 4);
        indiceCursor.writeInt( 9).writeInt( 0).writeInt( 4);
        indiceCursor.writeInt( 9).writeInt( 4).writeInt( 5);
        indiceCursor.writeInt( 4).writeInt( 8).writeInt( 5);
        indiceCursor.writeInt( 4).writeInt( 1).writeInt( 8);
        indiceCursor.writeInt( 1).writeInt(10).writeInt( 8);
        indiceCursor.writeInt( 3).writeInt( 8).writeInt(10);
        indiceCursor.writeInt( 3).writeInt( 5).writeInt( 8);
        indiceCursor.writeInt( 2).writeInt( 5).writeInt( 3);
        indiceCursor.writeInt( 7).writeInt( 2).writeInt( 3);
        indiceCursor.writeInt( 3).writeInt(10).writeInt( 7);
        indiceCursor.writeInt( 6).writeInt( 7).writeInt(10);
        indiceCursor.writeInt( 6).writeInt(11).writeInt( 7);
        indiceCursor.writeInt(11).writeInt( 6).writeInt( 0);
        indiceCursor.writeInt( 1).writeInt( 0).writeInt( 6);
        indiceCursor.writeInt( 1).writeInt( 6).writeInt(10);
        indiceCursor.writeInt( 0).writeInt( 9).writeInt(11);
        indiceCursor.writeInt(11).writeInt( 9).writeInt( 2);
        indiceCursor.writeInt( 9).writeInt( 5).writeInt( 2);
        indiceCursor.writeInt( 2).writeInt( 7).writeInt(11);
        
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices, 3));
        shell.setNormals(new VBO(normals, 3));
        shell.setIndexes(new IBO(indices, 3), IndexRange.TRIANGLES(0, (int) indices.getPrimitiveCount()));
        setShape(shell);
        
        
        //TODO texture coordinates
//	for( int i = 0 ; i < 20 ; i++ ){
//		Vertex::GetPosition( l_pSubmesh->GetPoint( l_pFaces[i]->GetVertexIndex( 0 ) ), l_ptCoordsA );
//		Vertex::GetPosition( l_pSubmesh->GetPoint( l_pFaces[i]->GetVertexIndex( 1 ) ), l_ptCoordsB );
//		Vertex::GetPosition( l_pSubmesh->GetPoint( l_pFaces[i]->GetVertexIndex( 2 ) ), l_ptCoordsC );
//
//		u = real( 0.5 ) * (real( 1.0 ) + atan2( l_ptCoordsA[2], l_ptCoordsA[0] ) * (1 / Angle::Pi));
//		v = acos( l_ptCoordsA[1] ) * (1 / Angle::Pi);
//		Vertex::SetTexCoord( l_pSubmesh->GetPoint( l_pFaces[i]->GetVertexIndex( 0 ) ), u, v );
//
//		u = real( 0.5 ) * (real( 1.0 ) + atan2( l_ptCoordsB[2], l_ptCoordsB[0] ) * (1 / Angle::Pi));
//		v = acos( l_ptCoordsB[1] ) * (1 / Angle::Pi);
//		Vertex::SetTexCoord( l_pSubmesh->GetPoint( l_pFaces[i]->GetVertexIndex( 1 ) ), u, v );
//
//		u = real( 0.5 ) * (real( 1.0 ) + atan2( l_ptCoordsC[2], l_ptCoordsC[0] ) * (1 / Angle::Pi));
//		v = acos( l_ptCoordsC[1] ) * (1 / Angle::Pi);
//		Vertex::SetTexCoord( l_pSubmesh->GetPoint( l_pFaces[i]->GetVertexIndex( 2 ) ), u, v );
//	}
//	for( VertexPtrArrayIt l_it = l_pSubmesh->VerticesBegin() ; l_it != l_pSubmesh->VerticesEnd() ; ++l_it ){
//		Vertex::GetNormal( (*l_it), l_ptNml );
//		SphericalVertex l_vsVertex1( l_ptNml );
//		Vertex::SetTexCoord( (*l_it), l_vsVertex1.m_rPhi, l_vsVertex1.m_rTheta );
//	}
        
    }
    
    
    
}
