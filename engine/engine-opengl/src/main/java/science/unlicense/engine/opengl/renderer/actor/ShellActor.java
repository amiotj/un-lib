
package science.unlicense.engine.opengl.renderer.actor;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_M;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_P;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_PIXELSIZE;
import static science.unlicense.engine.opengl.renderer.actor.MeshActor.UNIFORM_V;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;

/**
 *
 * @author Johann Sorel
 */
public class ShellActor extends AbstractActor implements ActorExecutor{
    
    public static final ShaderTemplate SKIN_VE;
    static {
        try{
            SKIN_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/skin-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    public static final Chars LAYOUT_VERTEX  = new Chars("l_position");
    public static final Chars LAYOUT_NORMAL  = new Chars("l_normal");
    public static final Chars LAYOUT_UV      = new Chars("l_uv");
    public static final Chars LAYOUT_TANGENT = new Chars("l_tangent");
    
    protected static final Chars PROG_LAYOUT_VERTEX  = new Chars("layout(location = $IDX) in vec3 l_position;");
    protected static final Chars PROG_LAYOUT_NORMAL  = new Chars("layout(location = $IDX) in vec3 l_normal;");
    protected static final Chars PROG_LAYOUT_UV      = new Chars("layout(location = $IDX) in vec2 l_uv;");
    protected static final Chars PROG_LAYOUT_TANGENT = new Chars("layout(location = $IDX) in vec4 l_tangent;");

    protected static final Chars PROG_OUT_VERTEX_MODEL  = new Chars("vec4 ").concat(CV_VERTEX_MODEL).concat(';');
    protected static final Chars PROG_OUT_VERTEX_WORLD  = new Chars("vec4 ").concat(CV_VERTEX_WORLD).concat(';');
    protected static final Chars PROG_OUT_VERTEX_CAMERA = new Chars("vec4 ").concat(CV_VERTEX_CAMERA).concat(';');
    protected static final Chars PROG_OUT_VERTEX_PROJ   = new Chars("vec4 ").concat(CV_VERTEX_PROJ).concat(';');
    protected static final Chars PROG_OUT_NORMAL_MODEL  = new Chars("vec3 ").concat(CV_NORMAL_MODEL).concat(';');
    protected static final Chars PROG_OUT_NORMAL_WORLD  = new Chars("vec3 ").concat(CV_NORMAL_WORLD).concat(';');
    protected static final Chars PROG_OUT_NORMAL_CAMERA = new Chars("vec3 ").concat(CV_NORMAL_CAMERA).concat(';');
    protected static final Chars PROG_OUT_NORMAL_PROJ   = new Chars("vec3 ").concat(CV_NORMAL_PROJ).concat(';');

    protected static final Chars PROG_OUT_UV      = new Chars("vec2 ").concat(CV_UV).concat(';');
    protected static final Chars PROG_OUT_TANGENT_MODEL  = new Chars("vec4 ").concat(CV_TANGENT_MODEL).concat(';');
    protected static final Chars PROG_OUT_TANGENT_WORLD  = new Chars("vec4 ").concat(CV_TANGENT_WORLD).concat(';');
    protected static final Chars PROG_OUT_TANGENT_CAMERA = new Chars("vec4 ").concat(CV_TANGENT_CAMERA).concat(';');
    protected static final Chars PROG_OUT_TANGENT_PROJ   = new Chars("vec4 ").concat(CV_TANGENT_PROJ).concat(';');
    
    protected static final Chars PROG_OP_VERTEX_MODEL   = new Chars("    outData.position_model = vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_VERTEX_WORLD   = new Chars("    outData.position_world = M * vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_VERTEX_CAMERA  = new Chars("    outData.position_camera = MV * vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_VERTEX_PROJ    = new Chars("    outData.position_proj = MVP * vec4("+CV_WORK_VERTEX_MODEL+",1);");
    protected static final Chars PROG_OP_NORMAL_MODEL   = new Chars("    outData.normal_model = (vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_NORMAL_WORLD   = new Chars("    outData.normal_world = (M * vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_NORMAL_CAMERA  = new Chars("    outData.normal_camera = (MV * vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_NORMAL_PROJ    = new Chars("    outData.normal_proj = (MVP * vec4(l_normal,0)).xyz;");
    protected static final Chars PROG_OP_TANGENT_MODEL  = new Chars("    outData.tangent_model = l_tangent;");
    protected static final Chars PROG_OP_TANGENT_WORLD  = new Chars("    outData.tangent_world = M * vec4(l_tangent.xyz,0);");
    protected static final Chars PROG_OP_TANGENT_CAMERA = new Chars("    outData.tangent_camera = MV * vec4(l_tangent.xyz,0);");
    protected static final Chars PROG_OP_TANGENT_PROJ   = new Chars("    outData.tangent_proj = MVP * vec4(l_tangent.xyz,0);");

    protected static final Chars PROG_OP_UV      = new Chars("    outData.uv  = l_uv;");

    protected static final Chars PROG_OP_BUILDIN_PROJ   = new Chars("    gl_Position = MVP * vec4("+CV_WORK_VERTEX_MODEL+",1);");

    protected final Mesh mesh;
    private VertexAttribute attVertice;
    private VertexAttribute attNormal;
    private VertexAttribute attUv;
    private VertexAttribute attTangent;
    
    public ShellActor(Mesh mesh) {
        super(null);
        this.mesh = mesh;
    }

    private Shell getShell(){
        return (Shell) mesh.getShape();
    }

    public int getMinGLSLVersion() {
        return SKIN_VE.getMinGLSLVersion();
    }

    public Chars getReuseUID() {
        final Shell shell = getShell();
        final CharBuffer cb = new CharBuffer();
        cb.append(shell.getVertices()!=null    ? "V1":"V0");
        cb.append(shell.getNormals()!=null     ? "N1":"N0");
        cb.append(shell.getIndexes()!=null     ? "I1":"I0");
        cb.append(shell.getUVs()!=null         ? "U1":"U0");
        cb.append(shell.getTangents()!=null    ? "T1":"T0");
        return cb.toChars();
    }
    
    public boolean isDirty() {
        final Shell shell = getShell();
        return shell.getVertices().isDirty() || shell.getNormals().isDirty() || shell.getIndexes().isDirty();
    }

    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        final ShaderTemplate tc = template.getTessaltionControlShaderTemplate();
        final ShaderTemplate te = template.getTesselationEvalShaderTemplate();
        final ShaderTemplate gs = template.getGeometryShaderTemplate();
        final ShaderTemplate fg = template.getFragmentShaderTemplate();
       
        vs.append(MeshActor.TEMPLATE_VE);
        if(tess)tc.append(MeshActor.TEMPLATE_TC);
        if(tess)te.append(MeshActor.TEMPLATE_TE);
        if(geom)gs.append(MeshActor.TEMPLATE_GE);
        if(fg!=null){
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_M).concat(';'));
            fg.addUniform(new Chars("uniform mat4 ").concat(MeshActor.UNIFORM_V).concat(';'));
            fg.addOperation(MeshActor.OP_SET_M);
            fg.addOperation(MeshActor.OP_SET_V);
            fg.append(MeshActor.TEMPLATE_FR);
        }
        
        vs.append(SKIN_VE);
        
        final Shell shell = getShell();
        // build program
        if(shell.getVertices()!=null){
            //always set the layout, might be used by some other shader actor
            vs.addLayout(PROG_LAYOUT_VERTEX);
            vs.addVariableOut(PROG_OUT_VERTEX_MODEL); vs.addOperation(PROG_OP_VERTEX_MODEL);
            vs.addVariableOut(PROG_OUT_VERTEX_WORLD); vs.addOperation(PROG_OP_VERTEX_WORLD); 
            vs.addVariableOut(PROG_OUT_VERTEX_CAMERA); vs.addOperation(PROG_OP_VERTEX_CAMERA);
            vs.addVariableOut(PROG_OUT_VERTEX_PROJ); vs.addOperation(PROG_OP_VERTEX_PROJ);
        }
        if(shell.getNormals()!=null){
            vs.addLayout(PROG_LAYOUT_NORMAL);
            vs.addVariableOut(PROG_OUT_NORMAL_MODEL); vs.addOperation(PROG_OP_NORMAL_MODEL);
            vs.addVariableOut(PROG_OUT_NORMAL_WORLD); vs.addOperation(PROG_OP_NORMAL_WORLD);
            vs.addVariableOut(PROG_OUT_NORMAL_CAMERA); vs.addOperation(PROG_OP_NORMAL_CAMERA);
            vs.addVariableOut(PROG_OUT_NORMAL_PROJ); vs.addOperation(PROG_OP_NORMAL_PROJ);
        }
        if(shell.getUVs()!=null){
            vs.addLayout(PROG_LAYOUT_UV);
            vs.addVariableOut(PROG_OUT_UV);
            vs.addOperation(PROG_OP_UV);
        }
        if(shell.getTangents()!=null){
            vs.addLayout(PROG_LAYOUT_TANGENT);
            vs.addVariableOut(PROG_OUT_TANGENT_MODEL); vs.addOperation(PROG_OP_TANGENT_MODEL);
            vs.addVariableOut(PROG_OUT_TANGENT_WORLD); vs.addOperation(PROG_OP_TANGENT_WORLD);
            vs.addVariableOut(PROG_OUT_TANGENT_CAMERA); vs.addOperation(PROG_OP_TANGENT_CAMERA);
            vs.addVariableOut(PROG_OUT_TANGENT_PROJ); vs.addOperation(PROG_OP_TANGENT_PROJ);
        }

        vs.addOperation(PROG_OP_BUILDIN_PROJ);

    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        
        if(attVertice == null){
            //get all uniform and attributes ids
            attVertice      = program.getVertexAttribute(LAYOUT_VERTEX, gl);
            attNormal       = program.getVertexAttribute(LAYOUT_NORMAL, gl);
            attUv           = program.getVertexAttribute(LAYOUT_UV, gl);
            attTangent      = program.getVertexAttribute(LAYOUT_TANGENT, gl);
        }
        
        //bind buffers
        final Shell shell = getShell();
        attVertice.enable(gl, shell.getVertices());
        attNormal.enable(gl, shell.getNormals());
        attUv.enable(gl, shell.getUVs());
        attTangent.enable(gl, shell.getTangents());

        //indices
        shell.getIndexes().loadOnGpuMemory(gl);
        shell.getIndexes().bind(gl);
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        attVertice.disable(gl);
        attNormal.disable(gl);
        attUv.disable(gl);
        attTangent.disable(gl);
    }
    
    public void render(ActorProgram program, RenderContext context, CameraMono camera, GLNode node) {
        
        final Mesh mesh = (Mesh) node;

        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        program.preExecutionGL(context, node);
        program.preDrawGL(context);

        ////////////////////////////////////////////////////////////////////////
        // DISPLAY : rendering /////////////////////////////////////////////////
        
        
        //set uniforms, Model->World->Projection matrix        
        try{
            final Uniform uniformM = program.getUniform(UNIFORM_M);
            final Uniform uniformV = program.getUniform(UNIFORM_V);
            final Uniform uniformP = program.getUniform(UNIFORM_P);
            final Uniform uniformPX = program.getUniform(UNIFORM_PIXELSIZE);
                    
            uniformM.setMat4(gl,    node.getNodeToRootSpace().toMatrix().toArrayFloat());
            uniformV.setMat4(gl,    camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniformP.setMat4(gl,    camera.getProjectionMatrix().toArrayFloat());
            uniformPX.setVec2(gl, new float[]{
                1.0f/(float)context.getViewRectangle().getWidth(),
                1.0f/(float)context.getViewRectangle().getHeight()});
        }catch(Throwable ex){
            //we may catch java.lang.InvalidArgumentException: Can not inverse
            ex.printStackTrace();
            return;
        }
                
//        //draw only wanted face
//        int culling = mesh.getCullFace();
//        if(culling==-1){
//            gl.glDisable(GL.GL_CULL_FACE);
//        }else{
//            gl.glEnable(GL.GL_CULL_FACE);
//            if(mesh.getNodeToRootSpace().det()<0){
//                //since there are difference coordinate system, clockwise may be inverted
//                //we fix this by reversing the culling
//                culling = GLUtilities.inverseCulling(culling);
//            }
//            gl.glCullFace(culling);
//        }
        
        render(context, mesh, program);
        
        //reset values
        gl.glDisable(GL_CULL_FACE);
        
        program.postDrawGL(context);
        
    }
    
    public void render(GLProcessContext context, Mesh mesh, ActorProgram program) {
        final GL gl = context.getGL();
        final IndexRange[] ranges = ((Shell)mesh.getShape()).getModes();
        for(int i=0;i<ranges.length;i++){
            ranges[i].draw(gl, true);
        }
        GLUtilities.discardErrors(gl);
        GLUtilities.checkGLErrorsFail(gl);
    }
    
    public void dispose(GLProcessContext context) {
        super.dispose(context);
        attVertice      = null;
        attNormal       = null;
        attUv           = null;
        attTangent      = null;
    }

}
