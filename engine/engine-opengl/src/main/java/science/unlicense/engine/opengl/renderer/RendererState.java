
package science.unlicense.engine.opengl.renderer;

import java.lang.reflect.Field;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.number.Numbers;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 * The rendered state stores the configuration of the rendering in the context.
 * This includes blending configuration, cull face, polygon mode, etc...
 *
 * This model is inspired from GLTF 'states' property in the 'Technique' type :
 * https://github.com/KhronosGroup/glTF/blob/master/specification/README.md
 *
 * @author Johann Sorel
 */
public class RendererState {

    private static final int[] STATES;
    static {
        final Field[] fields = GLC.GETSET.State.class.getDeclaredFields();
        STATES = new int[fields.length];
        for(int i=0;i<fields.length;i++){
            try {
                STATES[i] = (Integer)fields[i].get(null);
            } catch (Exception ex) {
                throw new RuntimeException(ex.getMessage(),ex);
            }
        }
        Arrays.sort(STATES);
    }
    
    /**
     * enabled states for this renderer.
     */
    private long enabled = 0l;
    private int polygonMode = GLC.POLYGON_MODE.FILL;
    private int blendFunction1 = GL_ONE;
    private int blendFunction2 = GL_ONE_MINUS_SRC_ALPHA;
    private int blendEquation = GL_FUNC_ADD;
    private boolean depthMask = false;
    private int depthFunc = GL_LESS;

    private int culling = -1;
    private short lineStipple = 0;
    private float lineWidth = 1;

    public RendererState() {
        //OpenGL has 2 states enabled by default : GL_DITHER and GL_MULTISAMPLE
        //Spec : https://www.opengl.org/sdk/docs/man/html/glEnable.xhtml
        setEnable(GLC.GETSET.State.DITHER, true);
        setEnable(GLC.GETSET.State.MULTISAMPLE, true);
    }

    /**
     * Returns true if given state is enable.
     * 
     * @param state one of GLC.GETSET.State.*
     * @return true if enable
     */
    public boolean isEnable(int state){
        final int index = Arrays.binarySearch(state, STATES);
        if(index<0) throw new InvalidArgumentException("Unknown state "+state);
        return (enabled & (1l<<index)) != 0;
    }

    /**
     * Set stable enabled value.
     *
     * @param state one of GLC.GETSET.State.*
     * @param enable
     */
    public void setEnable(int state, boolean enable){
        final int index = Arrays.binarySearch(state, STATES);
        if(index<0) throw new InvalidArgumentException("Unknown state "+state);
        enabled = Numbers.setBit(enabled, enable, index);
    }

    /**
     * Returns rendering polygon mode, one of GLC.POLYGON_MODE.*
     *
     * @param polygonMode
     */
    public void setPolygonMode(int polygonMode) {
        this.polygonMode = polygonMode;
    }

    /**
     * Get rendering polygon mode.
     * 
     * @return one of GLC.POLYGON_MODE.*
     */
    public int getPolygonMode() {
        return polygonMode;
    }

    /**
     * Configure blending.
     * 
     * @param blendFunction1 function first parameter
     * @param blendFunction2 function second parameter
     * @param blendEquation equation
     */
    public void setBlending(int blendFunction1, int blendFunction2, int blendEquation){
        this.blendFunction1 = blendFunction1;
        this.blendFunction2 = blendFunction2;
        this.blendEquation = blendEquation;
    }

    /**
     * Get blend function source parameter
     * @return
     */
    public int getBlendFunctionSource() {
        return blendFunction1;
    }

    /**
     * Get blend function source parameter.
     * @return
     */
    public int getBlendFunctionDestination() {
        return blendFunction2;
    }

    /**
     * Get blend equation.
     * @return
     */
    public int getBlendEquation() {
        return blendEquation;
    }

    /**
     * Get depth function.
     * @return
     */
    public int getDepthFunc() {
        return depthFunc;
    }

    /**
     * Set depth function.
     * @param depthFunc
     */
    public void setDepthFunc(int depthFunc) {
        this.depthFunc = depthFunc;
    }

    /**
     * Set depth mask
     * @param depthMask
     */
    public void setDepthMask(boolean depthMask) {
        this.depthMask = depthMask;
    }

    /**
     * Get depth mask.
     * @return
     */
    public boolean getDepthMask() {
        return depthMask;
    }

    /**
     * Set culling state.
     * One of GLC.Culling.* or -1
     * 
     * @param culling
     */
    public void setCulling(int culling) {
        this.culling = culling;
    }

    /**
     * Returns culling face.
     *
     * @return
     */
    public int getCulling() {
        return culling;
    }

    /**
     * Line stipple pattern.
     *
     * @param lineStiple
     */
    public void setLineStipple(short lineStiple) {
        this.lineStipple = lineStiple;
    }

    /**
     * Get line stipple pattern.
     * GL2+
     *
     * @return stipple pattern
     */
    public short getLineStipple() {
        return lineStipple;
    }

    /**
     * Set line width.
     *
     * @return
     */
    public float getLineWidth() {
        return lineWidth;
    }

    /**
     * Returns line width.
     *
     * @param lineWidth
     */
    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
    }

    
    public void configure(GL gl){
        //configure states
        for(int i=0;i<STATES.length;i++){
            if((enabled & (1l<<i)) != 0){
                gl.asGL1().glEnable(STATES[i]);
            }else{
                gl.asGL1().glDisable(STATES[i]);
            }
        }
        GLUtilities.checkGLErrorsFail(gl);

        //polygon mode
        if(gl.isGL2()){
            gl.asGL1().glPolygonMode(GL_FRONT_AND_BACK, polygonMode);
        }else if(polygonMode != GLC.POLYGON_MODE.FILL){
            //TODO log, but we should not log too often either, find a solution
            Loggers.get().log(new Chars("glPolygonMode not supported for gl version < GL2GL3"), Logger.LEVEL_INFORMATION);
        }

        //blending
        gl.asGL1().glBlendFunc(blendFunction1,blendFunction2);
        gl.asGL1().glBlendEquation(blendEquation);

        //depth
        gl.asGL1().glDepthMask(depthMask);
        gl.asGL1().glDepthFunc(depthFunc);

        GLUtilities.checkGLErrorsFail(gl);
        //culling
        if(culling==-1){
            gl.asGL1().glDisable(GLC.GETSET.State.CULL_FACE);
        }else{
            gl.asGL1().glEnable(GLC.GETSET.State.CULL_FACE);
            gl.asGL1().glCullFace(culling);
        }
        GLUtilities.checkGLErrorsFail(gl);

        //line stipple
        gl.asGL1().glLineWidth(lineWidth);
        if(gl.isGL2()){
            if(lineStipple!=0){
                gl.asGL1().glEnable(GL_LINE_STIPPLE);
                gl.asGL1().glLineStipple(1,lineStipple);
            }else{
                gl.asGL1().glDisable(GL_LINE_STIPPLE);
            }
        }
        GLUtilities.checkGLErrorsFail(gl);
    }

}
