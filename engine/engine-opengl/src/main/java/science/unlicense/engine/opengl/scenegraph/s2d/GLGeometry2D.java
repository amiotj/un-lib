
package science.unlicense.engine.opengl.scenegraph.s2d;

import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.geometry.path.FlattenPathIterator;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.triangulate.EarClipping;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * 
 * @author Johann Sorel
 */
public class GLGeometry2D {

    private static final double[] RESOLUTION = new double[]{1,1};
    
    private final Geometry2D geom;
    
    private boolean fillSet = false;
    private VBO fillVBO;
    private VBO maskVBO;
    
    private boolean contourSet = false;
    private VBO contourVBO;
    private IBO contourIBO;
    private IBO contourAdjIBO;
    
    public GLGeometry2D(Geometry2D geom) {
        this.geom = geom;
    }

    public Geometry2D getGeometry() {
        return geom;
    }

    public VBO getFillVBO(){
        return fillVBO;
    }

    public VBO getMaskVBO() {
        return maskVBO;
    }

    public VBO getContourVBO(){
        return contourVBO;
    }
    
    public IBO getContourIBO(){
        return contourIBO;
    }
    
    public IBO getContourAdjencyIBO(){
        return contourAdjIBO;
    }

    /**
     * 
     * @return true if geometry has a contour
     */
    public boolean calculateContour(){
        if(contourSet || geom==null) return contourVBO!=null;
        contourSet = true;
        
        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), RESOLUTION);

        final Sequence points = new ArraySequence();

        //stroke caches
        final TupleRW segmentStart = new DefaultTuple(2);
        final TupleRW segmentEnd = new DefaultTuple(2);
        //path first coordinate, used when on a close segment
        final TupleRW pathStart = new DefaultTuple(2);
        
        while(ite.next()){
            final int type = ite.getType();
            if(type==PathIterator.TYPE_MOVE_TO){
                ite.getPosition(segmentEnd);
                ite.getPosition(pathStart);
            }else if(PathIterator.TYPE_LINE_TO == type || PathIterator.TYPE_CLOSE == type){
                if(PathIterator.TYPE_CLOSE == type){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                }else{
                    ite.getPosition(segmentEnd);
                }
                points.add(segmentStart.toArrayFloat());
                points.add(segmentEnd.toArrayFloat());
            }
            segmentStart.set(segmentEnd);
        }

        if(points.getSize()==0){
            //nothing to paint
            return false;
        }else if(points.getSize()==1){
            //duplicate point
            points.add(points.get(0));
        }

        final int size = points.getSize();
        final FloatCursor vertices = GLBufferFactory.INSTANCE.createFloat(size*2).cursorFloat();
        final IntCursor indices = GLBufferFactory.INSTANCE.createInt(size).cursorInt();
        final IntCursor indiceAdjs = GLBufferFactory.INSTANCE.createInt(size*2).cursorInt();
        for(int i=0,k=0;i<size-1;i+=2,k+=2){
            final float[] coordStart = (float[]) points.get(i);
            final float[] coordEnd = (float[]) points.get(i+1);
            vertices.write(coordStart).write(coordEnd);
            indices.write(k).write(k+1);
            
            indiceAdjs.write( i==0 ? k : k-2);
            indiceAdjs.write(k+0);
            indiceAdjs.write(k+1);
            indiceAdjs.write( i==size-2 ? k+1 : k+3);
        }
        contourVBO = new VBO(vertices.getBuffer(), 2);
        contourIBO = new IBO(indices.getBuffer(), 2);
        contourAdjIBO = new IBO(indiceAdjs.getBuffer(), 4);
        
        return true;
    }
    
    public boolean calculateFill(){
        if(fillSet || geom==null) return fillVBO!=null;
        fillSet = true;
        
        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), RESOLUTION);

        final Sequence triangles;
        try {
            final EarClipping triangulator = new EarClipping();
            triangles = triangulator.triangulate(ite);
        } catch (OperationException ex) {
            ex.printStackTrace();
            return false;
        }

        final int size = triangles.getSize();
        final FloatCursor vertices = GLBufferFactory.INSTANCE.createFloat(size*3*2).cursorFloat();
        int k=0;
        for(int i=0;i<size;i++){
            final Tuple[] triangle = (Tuple[])triangles.get(i);
            vertices.write(triangle[0].toArrayFloat());
            vertices.write(triangle[2].toArrayFloat());
            vertices.write(triangle[1].toArrayFloat());
        }

        fillVBO = new VBO(vertices.getBuffer(),2);
        
        return true;
    }

    public void calculateMask(){
        if(maskVBO!=null) return;

        final FloatSequence seq = new FloatSequence();
        final PathIterator ite = geom.createPathIterator();

        final Vector anchor = new Vector(-1,-1,0);
        final Vector current = new Vector(3);
        final Vector previous = new Vector(3);
        final Vector first = new Vector(3);
        final Vector control = new Vector(3);

        seq.put(previous.toArrayFloat());
        seq.put(previous.toArrayFloat());
            
        while(ite.next()){
            int type = ite.getType();
            if(PathIterator.TYPE_MOVE_TO==type){
                ite.getPosition(first);
                ite.getPosition(current);
            }else if(PathIterator.TYPE_LINE_TO==type){
                ite.getPosition(current);
                control.setZ(0);
                current.setZ(0);
                previous.setZ(0);
                seq.put(anchor.toArrayFloat());
                seq.put(current.toArrayFloat());
                seq.put(previous.toArrayFloat());
            }else if(PathIterator.TYPE_CLOSE==type){
                control.setZ(0);
                current.setZ(0);
                previous.setZ(0);
                seq.put(anchor.toArrayFloat());
                seq.put(first.toArrayFloat());
                seq.put(previous.toArrayFloat());
            }else if(PathIterator.TYPE_QUADRATIC==type){
                control.setZ(0);
                current.setZ(0);
                previous.setZ(0);
                ite.getPosition(current);
                seq.put(anchor.toArrayFloat());
                seq.put(current.toArrayFloat());
                seq.put(previous.toArrayFloat());

                // special triangle
                ite.getFirstControl(control);
                control.setZ(1);
                current.setZ(1);
                previous.setZ(1);
                seq.put(control.toArrayFloat());
                seq.put(current.toArrayFloat());
                seq.put(previous.toArrayFloat());

            }else{
                throw new RuntimeException("TODO " + type);
            }

            previous.set(current);
        }

        float[] coords = seq.toArrayFloat();
        maskVBO = new VBO(coords,3);
    }

    
    public void dispose(GL gl){
        if(contourIBO!=null){
            contourIBO.unloadFromSystemMemory(gl);
            contourIBO.unloadFromGpuMemory(gl);
        }
        if(contourAdjIBO!=null){
            contourAdjIBO.unloadFromSystemMemory(gl);
            contourAdjIBO.unloadFromGpuMemory(gl);
        }
        if(contourVBO!=null){
            contourVBO.unloadFromSystemMemory(gl);
            contourVBO.unloadFromGpuMemory(gl);
        }
        if(fillVBO!=null){
            fillVBO.unloadFromSystemMemory(gl);
            fillVBO.unloadFromGpuMemory(gl);
        }
    }
}
