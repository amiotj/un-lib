
package science.unlicense.engine.opengl.mesh;

import science.unlicense.engine.opengl.renderer.actor.ShellActor;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.TextureCube;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.impl.math.DefaultMatrix;
import science.unlicense.api.image.Image;
import science.unlicense.api.math.Matrix;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.api.gpu.opengl.GL;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;
import science.unlicense.system.path.Paths;

/**
 * A Skybox, display a cube with 6 pictures placed at the limit of the
 * field of view. Giving the camera the impression of an horizon.
 * 
 * @author Johann Sorel
 */
public class SkyBox extends GLNode {

    private TextureCube texture = new TextureCube();
    private final VBO vertexVbo;
    private final IBO indiceVbo;
    private final IndexRange indxRange;
    
    private final SkyBoxRenderer renderer = new SkyBoxRenderer();
    
    public SkyBox() {
        this(null, null, null, null, null, null);
    }

    public SkyBox(Image top, Image bottom, Image left, Image right, Image front, Image back) {
                
        final Buffer vertexBuffer = GLBufferFactory.INSTANCE.createFloat(24*3);
        final Buffer indexBuffer = GLBufferFactory.INSTANCE.createInt(36);

        final float[][] vertices = new float[8][3];
        vertices[0] = new float[]{-1, -1, -1};
        vertices[1] = new float[]{+1, -1, -1};
        vertices[2] = new float[]{+1, +1, -1};
        vertices[3] = new float[]{-1, +1, -1};
        vertices[4] = new float[]{+1, -1, +1};
        vertices[5] = new float[]{-1, -1, +1};
        vertices[6] = new float[]{+1, +1, +1};
        vertices[7] = new float[]{-1, +1, +1};

        final FloatCursor vertexCursor = vertexBuffer.cursorFloat();
        
        //back
        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[1]);
        vertexCursor.write(vertices[2]);
        vertexCursor.write(vertices[3]);
        //front
        vertexCursor.write(vertices[4]);
        vertexCursor.write(vertices[5]);
        vertexCursor.write(vertices[7]);
        vertexCursor.write(vertices[6]);
        //rigth
        vertexCursor.write(vertices[1]);
        vertexCursor.write(vertices[4]);
        vertexCursor.write(vertices[6]);
        vertexCursor.write(vertices[2]);
        //left
        vertexCursor.write(vertices[5]);
        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[3]);
        vertexCursor.write(vertices[7]);
        //up
        vertexCursor.write(vertices[2]);
        vertexCursor.write(vertices[6]);
        vertexCursor.write(vertices[7]);
        vertexCursor.write(vertices[3]);
        //down
        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[5]);
        vertexCursor.write(vertices[4]);
        vertexCursor.write(vertices[1]);
        
        final int[] indices = {
            2, 1, 0, //back
            3, 2, 0,
            6, 5, 4, //front
            7, 6, 4,
            10, 9, 8,
            11, 10, 8,
            14, 13, 12,
            15, 14, 12,
            18, 17, 16,
            19, 18, 16,
            22, 21, 20,
            23, 22, 20 };
        indexBuffer.writeInt(indices,0);
        
        vertexVbo = new VBO(vertexBuffer, 3);
        indiceVbo = new IBO(indexBuffer, 3);
        indxRange = IndexRange.TRIANGLES(0, 36);
        
        texture.setImagePositiveX(right);
        texture.setImagePositiveY(top);
        texture.setImagePositiveZ(front);
        texture.setImageNegativeX(left);
        texture.setImageNegativeY(bottom);
        texture.setImageNegativeZ(back);
        
        getRenderers().add(renderer);
        
    }

    public TextureCube getTexture() {
        return texture;
    }

    public void setTexture(TextureCube texture) {
        this.texture = texture;
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        if(renderer.program!=null){
            renderer.program.releaseProgram(context);
        }
        
        vertexVbo.unloadFromGpuMemory(context.getGL());
        indiceVbo.unloadFromGpuMemory(context.getGL());
        texture.unloadFromGpuMemory(context.getGL());
    }

    private class SkyBoxRenderer extends AbstractRenderer {

        private ActorProgram program;
        private Uniform uniMVP;
        private Uniform uniTex;
        private Uniform uniCamera;
        private VertexAttribute attPosition;
        
        public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
            
            final GL2ES2 gl = context.getGL().asGL2ES2();
            
            if(program==null){
                
                program = new ActorProgram();
                try {
                    program.getActors().add(new DefaultActor(new Chars("skybox"),false,
                            Paths.resolve(new Chars("mod:/un/engine/opengl/shader/skybox-0-ve.glsl")),
                            null,
                            null,
                            null,
                            Paths.resolve(new Chars("mod:/un/engine/opengl/shader/skybox-4-fr.glsl")),
                            true, true));
                } catch (IOException ex) {
                    throw new RuntimeException(ex.getMessage(),ex);
                }
                program.compile(context);
                uniMVP = program.getUniform(new Chars("MVP"));
                uniTex = program.getUniform(new Chars("skybox"));
                uniCamera = program.getUniform(new Chars("CAMERA"));
                attPosition = program.getVertexAttribute(ShellActor.LAYOUT_VERTEX, gl);
            }
            
            program.enable(gl);
            
            //calculate mvp
            final Matrix mvp = ((GLNode)camera.getRoot()).calculateMVP(camera);
            uniMVP.setMat4(gl, mvp.toArrayFloat());
            final float[] center = camera.getNodeToRootSpace().transform(new float[]{0,0,0,1}, null);
            uniCamera.setVec3(gl, new float[]{center[0],center[1],center[2]});
            
            indiceVbo.loadOnGpuMemory(gl);
            texture.loadOnGpuMemory(gl);
            
            attPosition.enable(gl, vertexVbo);
            indiceVbo.bind(gl);
            
            final int[] reservedTexture = context.getResourceManager().reserveTextureId();
            gl.glActiveTexture(reservedTexture[0]);
            texture.bind(gl);
            uniTex.setInt(gl, reservedTexture[1]);
                        
            if(gl.isGL1()){
                gl.asGL1().glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
            gl.glDisable(GL_CULL_FACE);
            
            //disable depth test
            gl.glDisable(GL_DEPTH_TEST);
            gl.glDepthMask(false);
            
            gl.glDrawElements(
                indxRange.getMode(),
                indxRange.getCount(),
                indxRange.getIndexValueType(),
                indxRange.getBytesOffset()
            );
            
            
            //enable depth test
            gl.glDepthMask(true);
            gl.glEnable(GL_DEPTH_TEST);
            
            //unbind resources
            indiceVbo.unbind(gl);            
            attPosition.disable(gl);
            gl.glActiveTexture(reservedTexture[0]);
            gl.glBindTexture(GL_TEXTURE_2D, 0);
            context.getResourceManager().releaseTextureId(reservedTexture[0]);
            
            program.disable(gl);
        }

        public void dispose(GLProcessContext context) {
            if(program!=null){
                program.releaseProgram(context);
            }
        }
        
    }
    
}
