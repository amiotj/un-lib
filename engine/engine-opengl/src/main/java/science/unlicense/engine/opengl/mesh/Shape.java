

package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.api.geometry.BBox;

/**
 * A shape is the definition of a mesh geometry, envelope.
 * 
 * Static or animated meshes have Shell or SkinShell shapes with
 * predefined vertices,normals and uvs.
 * 
 * Dynamic shapes, like particules, billboard or functions also exist to allow
 * computation of vertices,normals,uvs dynamicaly, often is the geometry shader.
 * Those can be used to simulate geophysic behaviors, fire, water, drops, snow ...
 * 
 * @author Johann Sorel
 */
public interface Shape {
 
    /**
     * Get node name.
     * @return Chars, can be null.
     */
    Chars getName();

    /**
     * Set node name.
     * @param name , can be null.
     */
    void setName(Chars name);
    
    BBox getBBox();
            
    boolean isDirty();
        
    void dispose(GLProcessContext context);

    void calculateBBox();
    
    Shape copy();
        
}
