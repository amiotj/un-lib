
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.GBO;

/**
 *
 * @author Johann Sorel
 */
public class ReflectionMapping implements Mapping {

    private GBO gbo;

    public ReflectionMapping() {
    }

    public GBO getGbo() {
        return gbo;
    }

    public void setFbo(GBO gbo) {
        this.gbo = gbo;
    }
    
    public void update(GLProcessContext context, int width, int height){
        if(gbo==null){
            gbo = new GBO(width, height,0,true,false,false,false,false,false);
        }else{
            gbo.resize(context.getGL(), width, height);
        }
    }

    public boolean isDirty() {
        return false;
    }

    public void dispose(GLProcessContext context) {
        if(gbo!=null){
            gbo.unloadFromGpuMemory(context.getGL());
            gbo.unloadFromSystemMemory(context.getGL());
        }
    }
    
}
