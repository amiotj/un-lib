
package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * A Renderer is an rendering extension used to draw temporary or special
 * elements for a node.
 * 
 * Renderers are often used for debugging purpose, like drawing bounding boxes,
 * inner skeleton, invisible objects like lights, path prediction trajectory, 
 * mesh normals ...
 * 
 * @author Johann Sorel
 */
public interface Renderer {

    /**
     * Renderers can be used in various phases of the rendering pipeline, to
     * avoid possible errors this predicate filters the phases to match
     * only the appropriate ones.
     * 
     * @return Predicate, never null
     */
    Predicate getPhasePredicate();

    /**
     * Set phase predicate.
     * 
     * @param predicate, not null
     */
    void setPhasePredicate(Predicate predicate);
    
    /**
     * Returns the rendering state configuration.
     * @return RenderState, never null
     */
    RendererState getState();

    /**
     * Set rendering state.
     *
     * @param state, not null
     */
    void setState(RendererState state);

    /**
     * 
     * @param context current rendering context
     * @param camera camera associate to this context
     * @param node Node to be rendered
     */
    void render(final RenderContext context, final CameraMono camera, final GLNode node);

    /**
     * Dispose renderer and his associated resources.
     * 
     * @param context 
     */
    void dispose(GLProcessContext context);
    
}
