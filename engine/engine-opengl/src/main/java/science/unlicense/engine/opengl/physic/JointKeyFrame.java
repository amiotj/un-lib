

package science.unlicense.engine.opengl.physic;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.impl.anim.TransformKeyFrame;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.impl.math.Vector;

/**
 * An unresolved joint pose.
 * Stores informations about a joint.
 *
 * @author Johann Sorel
 */
public class JointKeyFrame extends TransformKeyFrame {

    public static final Chars PROPERTY_JOINT = new Chars("Joint");

    /**
     * Pose informations are relative to joint base.
     */
    public static final int FROM_BASE = 0;
    /**
     * Pose informations are relative to model origin.
     */
    public static final int FROM_ORIGIN = 1;

    private Object joint;
    private int from;

    public JointKeyFrame() {
        from = FROM_BASE;
    }
    
    public JointKeyFrame(Object jointIdentifier, double time) {
        super(time, null);
        this.joint = jointIdentifier;
        from = FROM_BASE;
    }

    public JointKeyFrame(Object jointIdentifier, Vector position, Quaternion rotation, int from) {
        this(jointIdentifier,position,rotation, createScale(position), from);
    }
    
    public JointKeyFrame(Object jointIdentifier, Vector position, Quaternion rotation, Vector scale, int from) {
        super(0, position, rotation, scale);
        this.joint = jointIdentifier;
    }

    private static Vector createScale(Vector position){
        final Vector scale = new Vector(position.getSize());
        Arrays.fill(scale.getValues(), 1);
        return scale;
    }
    
    /**
     * Joint or joint identifier, to find back joint in the skeleton.
     * Can be a Chars, a number, an uri or the joint itself.
     * @return
     */
    public Object getJoint() {
        return joint;
    }

    public void setJoint(Object jointId) {
        if(CObjects.equals(this.joint, jointId)) return;
        final Object old = this.joint;
        this.joint = jointId;
        sendPropertyEvent(this, PROPERTY_JOINT, old, this.joint);
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
    
    public Chars toChars() {
        final CharBuffer cb = new CharBuffer();
        cb.append(CObjects.toChars(joint));
        cb.append(' ');
        cb.append(CObjects.toChars(value));
        return cb.toChars();
    }

}
