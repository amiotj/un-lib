

package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL1;
import science.unlicense.api.gpu.opengl.GL2ES2;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.renderer.actor.AbstractActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.math.Vectors;
import science.unlicense.system.path.Paths;

/**
 * SSAO Phase (Screen Space Ambiant Occlusion).
 *
 * references :
 * http://en.wikipedia.org/wiki/Ambient_occlusion
 * http://en.wikipedia.org/wiki/Screen_space_ambient_occlusion
 *
 * tutorials :
 * http://blog.evoserv.at/index.php/2012/12/hemispherical-screen-space-ambient-occlusion-ssao-for-deferred-renderers-using-openglglsl/
 * http://john-chapman-graphics.blogspot.fr/2013/01/ssao-tutorial.html
 * http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/a-simple-and-practical-approach-to-ssao-r2753
 *
 *
 * @author Johann Sorel
 */
public class SSAOPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_KERNELSIZE = new Chars("samplesNb");
    private static final Chars UNIFORM_KERNELRADIUS = new Chars("samplesRadius");
    private static final Chars UNIFORM_VIEW = new Chars("V");
    private static final Chars UNIFORM_PROJECTION = new Chars("P");
    private static final Chars UNIFORM_RANDOM = new Chars("randomSampler");
    private static final Chars UNIFORM_INTENSITY = new Chars("intensity");
    private static final Chars UNIFORM_SCALE = new Chars("scale");
    private static final Chars UNIFORM_BIAS = new Chars("bias");

    //TODO replace by a configurable value
    private static final int KERNEL_SIZE = 128;
    private static final float[][] KERNEL_VALUES = new float[KERNEL_SIZE][2];
    
    private static final Image RANDOM_TEXTURE;
    static {
        try {
            RANDOM_TEXTURE = Images.read(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/effect/random.jpg")));
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(),ex);
        }
    }

    private static final ShaderTemplate SSAO_FR;
    static {
        try{
            SSAO_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/ssao-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }

        for(int i=0;i<KERNEL_SIZE;i++){
            // generate a random vector
            KERNEL_VALUES[i][0] = (float)(Math.random() * 2.0 - 1.0); // [-1..+1]
            KERNEL_VALUES[i][1] = (float)(Math.random() * 2.0 - 1.0); // [-1..+1]
            Vectors.normalize(KERNEL_VALUES[i], KERNEL_VALUES[i]);
            Vectors.scale(KERNEL_VALUES[i], (float)Math.random(), KERNEL_VALUES[i]);
        }

    }

    private final SSAOActor actor = new SSAOActor();
    private float kernelRadius = 1.5f;
    private float intensity = 2.0f;
    private float scale = 1.0f;
    private float bias = 0.5f;
    
    private CameraMono camera;
    private final Texture2D randomTex = new Texture2D(RANDOM_TEXTURE);    
    private final Uniform[] uniKernel = new Uniform[KERNEL_SIZE];
    private Uniform uniView;
    private Uniform uniSamplesNb;
    private Uniform uniSamplesRadius;
    private Uniform uniProjection;
    private Uniform uniIntensity;
    private Uniform uniScale;
    private Uniform uniBias;
    private Uniform uniRandom;
    private int[] texReserved;

    public SSAOPhase(CameraMono camera, Texture positionWorld, Texture normalWorld) {
        this(null,camera,positionWorld,normalWorld);
    }
    
    public SSAOPhase(FBO output, CameraMono camera, Texture positionWorld, Texture normalWorld) {
        super(output,positionWorld,normalWorld);
        this.camera = camera;
    }

    public CameraMono getCamera() {
        return camera;
    }

    public void setCamera(CameraMono camera) {
        this.camera = camera;
    }

    public float getKernelRadius() {
        return kernelRadius;
    }

    public void setKernelRadius(float kernelRadius) {
        this.kernelRadius = kernelRadius;
    }

    public float getIntensity() {
        return intensity;
    }

    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getBias() {
        return bias;
    }

    public void setBias(float bias) {
        this.bias = bias;
    }
    
    protected SSAOActor getActor() {
        return actor;
    }

    private final class SSAOActor extends AbstractActor{

        public SSAOActor() {
            super(new Chars("SSAO"));
        }
        
        public int getMinGLSLVersion() {
            return SSAO_FR.getMinGLSLVersion();
        }
        
        public void initProgram(RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
            super.initProgram(ctx, template, tess, geom);
            final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();
            fragmentShader.append(SSAO_FR);
        }

        public void preDrawGL(RenderContext context, ActorProgram program) {
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if(uniSamplesNb == null){
                for(int i=0;i<uniKernel.length;i++){
                    uniKernel[i] = program.getUniform(new Chars("samplesVec["+i+"]"),gl, Uniform.TYPE_VEC3);
                }
                uniView         = program.getUniform(UNIFORM_VIEW);
                uniSamplesNb    = program.getUniform(UNIFORM_KERNELSIZE);
                uniSamplesRadius= program.getUniform(UNIFORM_KERNELRADIUS);
                uniProjection   = program.getUniform(UNIFORM_PROJECTION);
                uniIntensity    = program.getUniform(UNIFORM_INTENSITY);
                uniScale        = program.getUniform(UNIFORM_SCALE);
                uniBias         = program.getUniform(UNIFORM_BIAS);
                uniRandom       = program.getUniform(UNIFORM_RANDOM);
            }

            //kernel uniforms
            uniSamplesNb.setInt(gl, KERNEL_SIZE);
            uniSamplesRadius.setFloat(gl, kernelRadius);
            for(int i=0;i<KERNEL_SIZE;i++){
                uniKernel[i].setVec2(gl, KERNEL_VALUES[i]);
            }
            uniView.setMat4(gl, camera.getRootToNodeSpace().toMatrix().toArrayFloat());
            uniProjection.setMat4(gl, camera.getProjectionMatrix().toArrayFloat());
            uniIntensity.setFloat(gl, intensity);
            uniScale.setFloat(gl, scale);
            uniBias.setFloat(gl, bias);
            
            //attach random texture
            randomTex.loadOnGpuMemory(gl);
            texReserved = context.getResourceManager().reserveTextureId();
            gl.glActiveTexture(texReserved[0]);
            randomTex.bind(gl);
            uniRandom.setInt(gl, texReserved[1]);
            
        }

        public void postDrawGL(RenderContext context, ActorProgram program) {
            super.postDrawGL(context, program);
            final GL1 gl = context.getGL().asGL1();
            
            gl.glActiveTexture(texReserved[0]);
            gl.glBindTexture(GL_TEXTURE_2D, 0);
            context.getResourceManager().releaseTextureId(texReserved[0]);
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            Arrays.fill(uniKernel, null);
            uniView = null;
            uniSamplesNb = null;
            uniSamplesRadius = null;
            uniProjection = null;
            uniIntensity = null;
            uniScale = null;
            uniBias = null;
            uniRandom = null;
        }
        
    }
    
}
