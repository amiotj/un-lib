

package science.unlicense.engine.opengl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.impl.gpu.opengl.shader.ShaderProgram;

/**
 * Keep track of currently loaded informations.
 * It is recommended that any classes loading data on the gpu declare it here.
 * This allows to keep track of memory usage and simplify detection of memory leaks.
 * 
 * TODO : add textures, vbo, ibo,...etc...
 * 
 * @author Johann Sorel
 */
public class GLResourceManager {
    
    private static final Logger LOGGER = Loggers.get();
    
    // reuseid -> Object[]{ShaderProgramCompiled, counter integer}
    private final Dictionary shaderprograms = new HashDictionary();
    private final GLProcessContext context;

    //various counter for opengl ressources
    //mask of 32bit : 32 available textures of opengl
    private int freeTextures = 0;
    
    public GLResourceManager(GLProcessContext context) {
        this.context = context;
    }
    
    /**
     * Acquire a texture id.
     * array[0] = texture id to be used in : gl.glActiveTexture(GL.GL_TEXTURE1);
     * array[1] = texture index to be used in : gl.glUniform1i(uniformTextureId, 1);
     * @return int[]
     */
    public int[] reserveTextureId(){
        for(int i=0;i<32;i++){
            if((freeTextures & (1<<i)) == 0){
                freeTextures |= 1<<i;
                return new int[]{i+33984,i};
            }
        }
        //no free texture, unlikely
        throw new RuntimeException("No free texture id, 32 used ! likely a bug, check for texture references not released.");
    }

    /**
     * Release a texture id.
     *
     * @param index array[0] or array[1] from reserveTextureId method
     */
    public void releaseTextureId(int index){
        if(index>33983) index -= 33984;
        freeTextures ^= (1 << index);
    }
    
    public void registerProgram(Chars uid, ShaderProgram program){
        Object[] res = (Object[]) shaderprograms.getValue(uid);
        if(res!=null){
            throw new RuntimeException("Program UID "+uid+" already used");
        }
        shaderprograms.add(uid, new Object[]{program,1});
    }
    
    public ShaderProgram aquireProgram(Chars uid){
        Object[] res = (Object[]) shaderprograms.getValue(uid);
        if(res!=null){
            //increment counter and return compiled program
            res[1] = ((Integer)res[1])+1;
            return (ShaderProgram) res[0];
        }
        return null;
    }
    
    public void releaseProgram(Chars uid, GLProcessContext context){
        Object[] res = (Object[]) shaderprograms.getValue(uid);
        if(res==null){
            throw new RuntimeException("Program UID "+uid+" does not exist");
        }
        res[1] = ((Integer)res[1])-1;
        if((Integer)res[1] == 0){
            //unload the program, it is not used by anyone
            final ShaderProgram program = (ShaderProgram) res[0];
            program.unloadFromGpuMemory(context.getGL());
            shaderprograms.remove(uid);
        }
    }
            
}