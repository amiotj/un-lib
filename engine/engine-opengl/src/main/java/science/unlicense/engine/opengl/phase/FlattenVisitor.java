

package science.unlicense.engine.opengl.phase;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * flatten a tree structure in a sequence.
 * only Mesh and Scene nodes are stored.
 *
 * @author Johann Sorel
 */
public class FlattenVisitor extends DefaultNodeVisitor{

    private final Sequence col = new ArraySequence();

    public void reset(){
        col.removeAll();
    }

    public Sequence getCollection() {
        return col;
    }

    public Object visit(Node node, Object context) {
        if(node instanceof GLNode){
            //skip nodes which are not visible
            if(!((GLNode)node).isVisible()) return null;
        }
        super.visit(node, context);
        if(node instanceof GLNode){
            col.add(node);
        }
        return null;
    }

}
