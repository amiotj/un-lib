

package science.unlicense.engine.opengl.physic;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedSet;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.Set;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import static science.unlicense.engine.opengl.physic.SkeletonPoseResolver.findJoint;
import science.unlicense.impl.math.transform.NodeTransform;

/**
 * Attach a skeleton to an animation.
 *
 * @author Johann Sorel
 */
public class SkeletonAnimationResolver {

    /**
     * 
     * @param skeleton
     * @param animation
     * @param nameAliases models may have different names for bones
     *        when a bone is not found the aliases are searched 
     */
    public static void map(Skeleton skeleton, SkeletonAnimation animation, Dictionary nameAliases){

        final Set resolved = new HashSet();
        
        final Sequence series = animation.getSeries();
        for(int i=0,n=series.getSize();i<n;i++){
            final JointTimeSerie serie = (JointTimeSerie) series.get(i);
            
            CharArray serieId;
            final Joint rj = serie.getJoint();
            if(rj!=null){
                serieId = rj.getName();
            }else{
                serieId = CObjects.toChars(serie.getJointIdentifier());
            }
            
            final Joint joint = findJoint(skeleton, serieId, nameAliases);
                        
            if(joint!=null){
                if(resolved.contains(joint.getName())){
                    System.out.println("Joint : "+joint.getName()+" is mapped multiple times");
                    continue;
                }
                resolved.add(joint.getName());
                
                serie.setJointIdentifier(joint.getName());
                serie.setJoint(joint);
                final OrderedSet set = serie.getFrames();
                final Iterator ite = set.createIterator();
                while(ite.hasNext()){
                    final JointKeyFrame jf = (JointKeyFrame) ite.next();
                    final NodeTransform trs = joint.getNodeTransform();
                    final NodeTransform ptrs = jf.getValue();
                    final MatrixRW res = trs.asMatrix().multiply(ptrs.asMatrix());
                    jf.getValue().set(res);
                }
            }else{
                System.out.println("No bone for joint time serie : "+serieId);
            }
        }
        
        animation.setSkeleton(skeleton);
    }

}
