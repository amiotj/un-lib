
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.collection.Iterator;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.Texture2DMS;
import static science.unlicense.impl.gpu.opengl.GLC.FBO.Attachment.*;
import science.unlicense.impl.gpu.opengl.resource.FBOAttachment;
import science.unlicense.impl.gpu.opengl.resource.Texture;

/**
 *
 * @author Johann Sorel
 */
public class GBO extends FBO {
    
    private GBO(int width, int height, boolean internal) {
        super(width, height);
    }
    
    public GBO(int width, int height) {
        this(width, height,0);
    }
    
    public GBO(int width, int height, int subSampling){
        this(width, height, subSampling, true, true, true, true, true, true);
    }
    
    public GBO(int width, int height, int subSampling, 
            boolean diffuse, boolean specular, boolean position, 
            boolean normal, boolean meshId, boolean vertexId){
        super(width, height);
        
        if(subSampling>0){
            if(diffuse) addAttachment(COLOR_0, new Texture2DMS(width, height, Texture2DMS.COLOR_RGBA(), subSampling));
            if(specular)addAttachment(COLOR_1, new Texture2DMS(width, height, Texture2DMS.COLOR_RGBA(), subSampling));
            if(position)addAttachment(COLOR_2, new Texture2DMS(width, height, Texture2DMS.VEC3_FLOAT(), subSampling));
            if(normal)  addAttachment(COLOR_3, new Texture2DMS(width, height, Texture2DMS.VEC3_FLOAT(), subSampling));
            if(meshId)  addAttachment(COLOR_4, new Texture2DMS(width, height, Texture2DMS.VEC1_INT(), subSampling));
            if(vertexId)addAttachment(COLOR_5, new Texture2DMS(width, height, Texture2DMS.VEC1_INT(), subSampling));
            addAttachment(DEPTH_STENCIL, new Texture2DMS(width, height, Texture2DMS.DEPTH24_STENCIL8(), subSampling)); 
        }else{
            if(diffuse) addAttachment(COLOR_0, new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED()));
            if(specular)addAttachment(COLOR_1, new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED()));
            if(position)addAttachment(COLOR_2, new Texture2D(width, height, Texture2D.VEC3_FLOAT()));
            if(normal)  addAttachment(COLOR_3, new Texture2D(width, height, Texture2D.VEC3_FLOAT()));
            if(meshId)  addAttachment(COLOR_4, new Texture2D(width, height, Texture2D.VEC1_INT()));
            if(vertexId)addAttachment(COLOR_5, new Texture2D(width, height, Texture2D.VEC1_INT()));
            addAttachment(DEPTH_STENCIL, new Texture2D(width, height, Texture2D.DEPTH24_STENCIL8())); 
        }
    }
    
    public Texture getDiffuseTexture() {
        return getTexture(COLOR_0);
    }
    
    public Texture getSpecularTexture() {
        return getTexture(COLOR_1);
    }
    
    public Texture getPositionTexture() {
        return getTexture(COLOR_2);
    }
    
    public Texture getNormalTexture() {
        return getTexture(COLOR_3);
    }
    
    public Texture getMeshIdTexture() {
        return getTexture(COLOR_4);
    }
    
    public Texture getVertexIdTexture() {
        return getTexture(COLOR_5);
    }

    public GBO createBlitFBO() {
        final GBO blit = new GBO(getWidth(), getHeight(),true);        
        final Iterator typeIte = attachments.createIterator();
        while(typeIte.hasNext()){
            final FBOAttachment att = (FBOAttachment) typeIte.next();            
            blit.addAttachment(att.createBlitAttachment());
        }        
        return blit;
    }
    
}
