
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Oil painting effect.
 * 
 * @author Johann Sorel
 */
public class OilPaintingPhase extends AbstractTexturePhase {

    private static final Chars UNIFORM_PARAMS = new Chars("oilParams");
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/oil-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final OilActor actor = new OilActor();
    private int radius;
    private int nbLevel;
    private Uniform uniParams;

    public OilPaintingPhase(Texture texture) {
        this(null,texture,5,20);
    }
    
    public OilPaintingPhase(Texture texture, int radius, int nbLevel) {
        this(null,texture,radius,nbLevel);
    }
    
    public OilPaintingPhase(FBO output, Texture texture, int radius, int nbLevel) {
        super(output,texture);
        this.radius = radius;
        this.nbLevel = nbLevel;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getNbLevel() {
        return nbLevel;
    }

    public void setNbLevel(int nbLevel) {
        this.nbLevel = nbLevel;
    }

    protected OilActor getActor() {
        return actor;
    }

    private final class OilActor extends DefaultActor{

        public OilActor() {
            super(new Chars("OilPainting"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();

            //get all uniforms
            if(uniParams == null){
                uniParams = program.getUniform(UNIFORM_PARAMS);
            }
            uniParams.setVec2(gl, new float[]{radius,nbLevel});
        }

        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniParams = null;
        }

    }
    
}
