
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.Sorter;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.operation.OperationException;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.math.transform.RearrangeTransform;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.s2d.Polygon;
import science.unlicense.impl.geometry.s2d.Polyline;
import science.unlicense.impl.geometry.s3d.Plane;
import science.unlicense.impl.geometry.triangulate.EarClipping;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.Vectors;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * Mesh utilities.
 * 
 * @author Johann Sorel
 */
public final class MeshUtilities {

    /**
     * Sort meshes based on there material opacity.
     * This sorter accept normal nodes to be included.
     */
    public static final Sorter OPACITY_SORTER = new Sorter() {
        @Override
        public int sort(Object first, Object second) {

            if(!(first instanceof Mesh)) return +1;
            if(!(second instanceof Mesh)) return -1;

            final boolean m1 = ((Mesh) first).getMaterial().isOpaque();
            final boolean m2 = ((Mesh) second).getMaterial().isOpaque();

            if(m1 && m2){
                return 0;
            }else if(m1){
                return -1;
            }else if(m2){
                return +1;
            }else{
                return 0;
            }
        }
    };
    
    private static final Transform[] FLATTEN = new Transform[]{
        RearrangeTransform.create(new int[]{-1, 0, 1}),
        RearrangeTransform.create(new int[]{ 0,-1, 1}),
        RearrangeTransform.create(new int[]{ 0, 1,-1})
    };
    
    private MeshUtilities(){};
    
    /**
     * Normalize all tuples in given vbo.
     * @param vbo 
     */
    public static void normalize(VBO vbo){
        final float[] tuple = new float[vbo.getSampleCount()];
        for(int i=0,n=vbo.getTupleCount();i<n;i++){
            vbo.getTupleFloat(i, tuple);
            Vectors.normalize(tuple, tuple);
            vbo.setTupleFloat(i, tuple);
        }
    }
    
    /**
     * Ensure the sum of each tuple is 1.
     * @param vbo 
     */
    public static void sumToOne(VBO vbo){
        final float[] tuple = new float[vbo.getSampleCount()];
        for(int i=0,n=vbo.getTupleCount();i<n;i++){
            vbo.getTupleFloat(i, tuple);
            final float sum = Maths.sum(tuple);
            Vectors.scale(tuple, 1f/sum, tuple);
            vbo.setTupleFloat(i, tuple);
        }
    }
    
    /**
     * NOTE : this is not a perfect algorithm,
     * it search the smallest dimension and and triangulate in 2D.
     * NOTE : we could improve it by projection on the most efficient plane.
     * 
     * @param vertices
     * @return triangles indices int[n][3]
     */
    public static int[][] triangulate(Tuple[] vertices) throws OperationException{
        
        final BBox bbox = new BBox(3);
        bbox.getLower().set(vertices[0]);
        bbox.getUpper().set(vertices[0]);
        for(int i=1;i<vertices.length;i++){
            bbox.expand(vertices[i]);
        }
        
        //find thinest dimension
        int smallIndex = 0;
        for(int i=1;i<3;i++){
            if(bbox.getSpan(i)< bbox.getSpan(smallIndex)){
                smallIndex = i;
            }
        }
        
        //convert to 2d
        final Tuple[] v2d = new Tuple[vertices.length];
        for(int i=0;i<vertices.length;i++){
            v2d[i] = FLATTEN[smallIndex].transform(vertices[i], null);
        }
        
        //TODO : improve triangulation classes to return indexes if possible
        final EarClipping clip = new EarClipping();
        final Sequence res = clip.triangulate(new Polygon(new Polyline(Geometries.toTupleBuffer(v2d)), null));
        
        //find indexes back
        final int[][] trs = new int[res.getSize()][3];
        for(int i=0;i<trs.length;i++){
            final Tuple[] triangle = (Tuple[]) res.get(i);
            for(int k=0;k<3;k++){
                trs[i][k] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[k]);
            }
        }
        
        return trs;
    }
    
    public static int[][] triangulate(Tuple[] vertices, Tuple projectionPlanNormal) throws OperationException{
        
        final Plane p = new Plane(new Vector(projectionPlanNormal), new Vector(vertices[0]));
        //we will rotate cordinates to remove the z component one point in projected on plan
        final Matrix3x3 rot = Matrix3x3.createRotation(p.getNormal(), new Vector(0, 0, 1));
        if(p.getNormal().add(new Vector(0, 0, 1)).length() == 0.0){
            //if normal is (0,0,-1) the matrix will contains NaN
            rot.setToIdentity();
        }

        //convert to 2d
        final TupleRW[] v2d = new TupleRW[vertices.length+1];
        for(int i=0;i<vertices.length;i++){
            v2d[i] = new Vector(Geometries.projectPointOnPlan(vertices[i].getValues(), p.getNormal().getValues(), p.getD()));
            rot.transform(v2d[i], v2d[i]);
        }
        //close the polyline, first must equals last
        v2d[v2d.length-1] = v2d[0].copy();
        
        
        //TODO : improve triangulation classes to return indexes if possible
        final EarClipping clip = new EarClipping();
        final Sequence res = clip.triangulate(new Polygon(new Polyline(Geometries.toTupleBuffer(v2d)), null));
        
        //preserve clockwise 
        final boolean clockWise = Geometries.isClockWise(new ArraySequence(v2d));
        
        //find indexes back
        final int[][] trs = new int[res.getSize()][3];
        for(int i=0;i<trs.length;i++){
            final Tuple[] triangle = (Tuple[]) res.get(i);
            
            final boolean cw = !Geometries.isCounterClockwise(triangle[0].getValues(), triangle[1].getValues(), triangle[2].getValues());
            if(cw == clockWise){
                trs[i][0] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[0]);
                trs[i][1] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[1]);
                trs[i][2] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[2]);
            }else{
                trs[i][2] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[0]);
                trs[i][1] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[1]);
                trs[i][0] = Arrays.getFirstOccurence(v2d, 0, v2d.length, triangle[2]);
            }
            
        }
        
        return trs;
    }
    
    /**
     * Convert index buffer to triangle adjency.
     * 
     * @param shell 
     */
    public static void convertToTriangleAdjency(Shell shell){
        if(shell.getModes().length!=1){
            throw new UnimplementedException("only 1 range supported");
        }
        
        final int mode = shell.getModes()[0].getMode();
        if(mode == GL_TRIANGLES_ADJACENCY){
            //already in adjency mode
            return;
        }else if(mode != GL_TRIANGLES){
            throw new UnimplementedException("only triangles type supported");
        }
        
        final Buffer ib = createAdjencyIndex(shell.getIndexes().getPrimitiveBuffer());
        final IBO adjencyIndex = new IBO(ib, 6);
        shell.setIndexes(adjencyIndex, IndexRange.TRIANGLES_ADJACENCY(0, (int) ib.getPrimitiveCount()));
    }
    
    /**
     * Build adjency triangle index from triangle indexes.
     * @param ib
     * @return 
     */
    public static Buffer createAdjencyIndex(final Buffer ib){
        
        final Sequence triangles = new ArraySequence();
        
        final WorkSpace ws = new WorkSpace();
        
        //find min and max index used
        IntCursor cs = ib.cursorInt();
        for(int i=0,k,n=(int) ib.getPrimitiveCount();i<n;i++){
            k = cs.read();
            if(k<ws.minIdx) ws.minIdx = k;
            if(k>ws.maxIdx) ws.maxIdx = k;
        }
        
        ws.idx = new Edge[ws.maxIdx-ws.minIdx+1][0];
        
        
        cs.setPosition(0);
        for(int i=0,n=(int) ib.getPrimitiveCount();i<n;i+=3){
            final int t1 = cs.read(i+0);
            final int t2 = cs.read(i+1);
            final int t3 = cs.read(i+2);
            final Triangle t = new Triangle();
            t.edge1 = new Edge(t1,t2,t3);
            t.edge2 = new Edge(t2,t3,t1);
            t.edge3 = new Edge(t3,t1,t2);
            triangles.add(t);
            putTE(ws, t.edge1);
            putTE(ws, t.edge2);
            putTE(ws, t.edge3);
        }
        
        final science.unlicense.api.collection.primitive.IntSequence buffer = new science.unlicense.api.collection.primitive.IntSequence();
        for(int i=0,n=triangles.getSize();i<n;i++){
            final Triangle t = (Triangle) triangles.get(i);
            buffer.put(t.edge1.e1);
            buildAdj(ws, buffer, t.edge1);
            buffer.put(t.edge1.e2);
            buildAdj(ws, buffer, t.edge2);
            buffer.put(t.edge2.e2);
            buildAdj(ws, buffer, t.edge3);
        }
        
        final int[] array = buffer.toArrayInt();
        
        if(array.length != ib.getPrimitiveCount()*2){
            throw new RuntimeException("Error calculation adjency triangle indexes.");
        }
        
        final Buffer nb = ib.getFactory().createInt(array.length);
        nb.cursorInt().write(array);
        return nb;
    }
    
    private static void putTE(WorkSpace dico, Edge e){
        final int rIdx = Maths.min(e.e1, e.e2) - dico.minIdx;
        final Edge[] m0 = dico.idx[rIdx];
        dico.idx[rIdx] = (Edge[])Arrays.insert(m0, m0.length, e);
    }
    
    private static void buildAdj(WorkSpace dico, science.unlicense.api.collection.primitive.IntSequence buffer, Edge e){
        final int rIdx = Maths.min(e.e1, e.e2) - dico.minIdx;
        final Edge[] m = dico.idx[rIdx];
        
        //loop on all edges using this vertex to find the sibling
        Edge sibling = null;
        for(Edge cdt : m){
            if(cdt == e){
                //same edge ignore it
                continue;
            }
            
            if( (cdt.e1==e.e1 && cdt.e2==e.e2) || (cdt.e1==e.e2 && cdt.e2==e.e1) ){
                //found sibling
                sibling = cdt;
                break;
            }
        }
        
        if(sibling==null){
            //no adjency edge, copy the index of the edge
            //TODO this produce a flat triangle, is there no better solution ?
            buffer.put(e.e2);
        }else{
            buffer.put(sibling.opposite);
        }
    }
    
    private static final class Triangle{
        
        private Edge edge1;
        private Edge edge2;
        private Edge edge3;

    }
    
    private static final class Edge{
        
        private final int e1;
        private final int e2;
        private final int opposite;

        public Edge(int e1, int e2, int opposite) {
            this.e1 = e1;
            this.e2 = e2;
            this.opposite = opposite;
        }

        @Override
        public int hashCode() {
            return e1+e2;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Edge other = (Edge) obj;
            return (this.e1 == other.e1 && this.e2 == other.e2)
                || (this.e1 == other.e2 && this.e2 == other.e1);
        }
        
    }
    
    private static final class WorkSpace{
        public int minIdx = Integer.MAX_VALUE;
        public int maxIdx = Integer.MIN_VALUE;
        /** indice : Object[]{edge,edge,...} */
        public Edge[][] idx;
    }
    
    
}
