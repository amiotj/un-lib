
package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 *
 * @author Johann Sorel
 */
public class ThresdholdPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/threshold-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final ThresholdActor actor = new ThresholdActor();
    private Uniform uniLimit;
    private float[] limit;

    public ThresdholdPhase(Texture texture) {
        super(texture);
    }
    
    public ThresdholdPhase(FBO output, Texture texture) {
        super(output, texture);
    }

    public ThresdholdPhase(FBO output, Texture texture, float[] limit) {
        super(output, texture);
        this.limit = limit;
    }

    protected ThresholdActor getActor() {
        return actor;
    }

    private final class ThresholdActor extends DefaultActor{

        public ThresholdActor() {
            super(new Chars("Threshold"),false,null,null,null,null,SHADER_FR,true,true);
        }
        
        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL2ES2 gl = context.getGL().asGL2ES2();
            uniLimit = program.getUniform(new Chars("THRESHOLD"));
            uniLimit.setVec3(gl,limit);
        }

        @Override
        public void dispose(GLProcessContext context) {
            super.dispose(context);
            uniLimit = null;
        }
        
    }
    
}
