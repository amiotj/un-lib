
package science.unlicense.engine.opengl.renderer.actor;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.mapping.SphericalMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * Spherical texturing shader actor.
 * 
 * @author Johann Sorel
 */
public class TextureSphericalActor extends AbstractMaterialValueActor{


    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/sphericaltex-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final SphericalMapping mapping;


    //GL loaded informations
    private int[] reservedTexture;
    private Uniform uniform;
    
    public TextureSphericalActor(Chars produce, Chars method, Chars uniquePrefix, SphericalMapping mapping) {
        super(new Chars("SphericalMapping"),false, produce, method, uniquePrefix,
                null,null,null,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }
    
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Texture2D texture = mapping.getTexture();
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        // Bind textures
        if(uniform == null){
            uniform = program.getUniform(uniquePrefix.concat(new Chars("tex")));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        uniform.setInt(gl, reservedTexture[1]);
        
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        gl.asGL1().glActiveTexture(reservedTexture[0]);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);
        
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniform = null;
    }

}
