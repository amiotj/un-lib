
package science.unlicense.engine.opengl.material;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.Mapping;

/**
 *
 * @author Johann Sorel
 */
public class Layer {

    /**
     * Material diffuse color.
     */
    public static final int TYPE_DIFFUSE = 1;
    /**
     * Material specular color.
     * Knowned as specular mapping.
     */
    public static final int TYPE_SPECULAR = 2;
    /**
     * Material ambient color.
     * Knowned as ambient mapping.
     */
    public static final int TYPE_AMBIENT = 3;
    /**
     * Used to correct model normals.
     * Knowned as normal/bump mapping.
     */
    public static final int TYPE_NORMAL = 4;
    /**
     * Combined with an opaque diffuse layers to obtain a translucent image.
     */
    public static final int TYPE_ALPHA = 5;

    /** 
     * When several layer affect the same output : this value is added to base value
     */
    public static final int METHOD_ADDITIVE = 1;
    /** 
     * When several layer affect the same output : base value is multiplied by this value
     */
    public static final int METHOD_MULTIPLY = 2;
    /** 
     * When several layer affect the same output : base value is blend using porter-duff src-over rule
     */
    public static final int METHOD_SRC_OVER = 3;
    
    private Mapping mapping;
    private float ratio = 1.0f; // 0-1
    private int type = TYPE_DIFFUSE;
    private int method = METHOD_SRC_OVER;

    public Layer(){}

    public Layer(Mapping mapping) {
        this.mapping = mapping;
    }

    public Layer(Mapping mapping, int type) {
        this.mapping = mapping;
        this.type = type;
    }
    
    public Layer(Mapping mapping, int type, int method) {
        this.mapping = mapping;
        this.type = type;
        this.method = method;
    }
    
    public Layer(Mapping mapping, int type, int method, float ratio) {
        this.mapping = mapping;
        this.type = type;
        this.method = method;
        this.ratio = ratio;
    }

    public Mapping getMapping() {
        return mapping;
    }

    public void setMapping(Mapping mapping) {
        this.mapping = mapping;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public float getRatio() {
        return ratio;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }
    
    public boolean isDirty() {
        if(mapping != null){
            return mapping.isDirty();
        }
        return false;
    }

    public void dispose(GLProcessContext context) {
        if(mapping != null){
            mapping.dispose(context);
        }
    }

}
