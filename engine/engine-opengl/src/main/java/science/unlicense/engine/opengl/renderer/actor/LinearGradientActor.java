
package science.unlicense.engine.opengl.renderer.actor;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.painter2d.LinearGradientPaint;

/**
 *
 * @author Johann Sorel
 */
public class LinearGradientActor extends AbstractMaterialValueActor{
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/lineargradiant-4-fr.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private static final Chars UNIFORM_GSTART = new Chars("GSTART");
    private static final Chars UNIFORM_GEND = new Chars("GEND");
    private static final Chars UNIFORM_NBSTEP = new Chars("NBSTEP");
    
    private Uniform uniformGstart;
    private Uniform uniformGend;
    private Uniform uniformNbStep;
    
    private LinearGradientPaint gradient;

    public LinearGradientActor(Chars baseReuseId, Chars produce, Chars method, Chars uniquePrefix) {
        super(baseReuseId, false, produce, method, uniquePrefix, null, null, null, null, SHADER_FR);
    }

    public void setGradient(LinearGradientPaint gradient) {
        this.gradient = gradient;
    }

    public LinearGradientPaint getGradient() {
        return gradient;
    }

    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom) {
        super.initProgram(ctx, template, tess, geom);
    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        uniformGstart = program.getUniform(UNIFORM_GSTART);
        uniformGend = program.getUniform(UNIFORM_GEND);
        uniformNbStep = program.getUniform(UNIFORM_NBSTEP);
                
        final GL2ES2 gl = context.getGL().asGL2ES2();
        
        //we need the coordinates in pixel cs, we are going to calculate distance with the
        //gl_fragcoord which is in this cs.
        final float[] s = new float[]{(float)gradient.getStartX(),(float)gradient.getStartY(),1};
        final float[] e = new float[]{(float)gradient.getEndX(),(float)gradient.getEndY(),1};
        uniformGstart.setVec2(gl, s);
        uniformGend.setVec2(gl, e);

        final Color[] colors = gradient.getColors();
        final double[] steps = gradient.getPositions();
        uniformNbStep.setInt(gl, steps.length);
        for(int i=0;i<steps.length;i++){
            final Uniform us = program.getUniform(new Chars("GSTEPS["+i+"]"),gl,Uniform.TYPE_FLOAT);
            final Uniform uc = program.getUniform(new Chars("GCOLORS["+i+"]"),gl,Uniform.TYPE_VEC4);
            us.setFloat(gl, (float)steps[i]);
            uc.setVec4(gl, colors[i].toRGBAPreMul());
        }
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniformGstart = null;
        uniformGend = null;
        uniformNbStep = null;
    }

}
