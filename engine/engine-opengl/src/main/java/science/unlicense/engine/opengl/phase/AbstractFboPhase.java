
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.gpu.opengl.GLState;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;

/**
 * An abstract phase works takes care to configure the output, standard or FBO.
 *
 * Several tasks can be done using this technique :
 * - Screen snapshot
 * - Light shadow projection
 * - Picking
 * - pre-processing effect : bloom, stencil ...
 * - ... and so on ...
 *
 * @author Johann Sorel
 */
public abstract class AbstractFboPhase extends AbstractPhase {

    public static final Chars PROPERTY_OUTPUTFBO = new Chars("OutputFbo");
    public static final Chars PROPERTY_BLITFBO = new Chars("BlitFbo");
    
    private volatile FBO outputFbo;
    private volatile FBO blitFbo;
    private boolean enable = true;

    public AbstractFboPhase() {
    }

    public AbstractFboPhase(Chars id) {
       this(id,null,null);
    }
    
    public AbstractFboPhase(FBO renderFbo) {
        this(Chars.EMPTY,renderFbo,null);
    }
    
    public AbstractFboPhase(FBO renderFbo, FBO blitFbo) {
        this(Chars.EMPTY,renderFbo,blitFbo);
    }
    
    public AbstractFboPhase(Chars id, FBO renderFbo) {
        this(id,renderFbo,null);
    }
    
    public AbstractFboPhase(Chars id, FBO renderFbo, FBO blitFbo) {
        super(id);
        this.outputFbo = renderFbo;
        this.blitFbo = blitFbo;
    }
    
    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        if(this.enable==enable) return;
        this.enable = enable;
        sendPropertyEvent(this, PROPERTY_ENABLE, !enable, enable);
    }

    /**
     * Get output FBO
     * @return FBO can be null
     */
    public FBO getOutputFbo() {
        return outputFbo;
    }

    /**
     * Set output FBO
     * @param fbo , null for main output
     */
    public void setOutputFbo(FBO fbo) {
        if(this.outputFbo == fbo) return;
        final FBO old = this.outputFbo;
        this.outputFbo = fbo;
        sendPropertyEvent(this, PROPERTY_OUTPUTFBO, old, fbo);
    }

    public FBO getBlitFbo() {
        return blitFbo;
    }

    public void setBlitFbo(FBO blitFbo) {
        if(this.blitFbo == blitFbo) return;
        final FBO old = this.blitFbo;
        this.blitFbo = blitFbo;
        sendPropertyEvent(this, PROPERTY_BLITFBO, old, blitFbo);
    }
    
    protected final void processInt(GLProcessContext context) throws GLException {
        if(outputFbo==null){
            
            //we must ensure we respect the context view rectangle
            final GL gl = context.getGL();
            final int[] viewPort = GLState.getViewPort(gl);
            final Rectangle viewRect = context.getViewRectangle();
            gl.asGL1().glViewport((int)viewRect.getX(), (int)viewRect.getY(), (int)viewRect.getWidth(), (int)viewRect.getHeight());
            
            //render directly
            processInternal(context);
            
            //restore previous view port
            gl.asGL1().glViewport(viewPort[0],viewPort[1],viewPort[2],viewPort[3]);
            

        }else{
            //render in the fbo
            final GL gl = context.getGL();

            //ensure it is loaded
            outputFbo.loadOnGpuMemory(gl);

            //bind the framebuffer
            outputFbo.bind(gl);

            //do the work
            processInternal(context);
            
            // Unbind the framebuffer, and revert to default framebuffer
            outputFbo.unbind(gl);
            GLUtilities.checkGLErrors(gl);
            
            //blit the fbo if there is one
            if(blitFbo!=null){
                //ensure it is loaded
                blitFbo.loadOnGpuMemory(gl);
                outputFbo.blit(gl, blitFbo);
            }
            
        }
    }

    /**
     * Do the work.
     * The output main or FBO has already been take care of.
     *
     * @param ctx
     * @throws GLException
     */
    protected abstract void processInternal(GLProcessContext ctx) throws GLException;

    @Override
    public void dispose(GLProcessContext ctx) {
        super.dispose(ctx);
        final GL gl = ctx.getGL();
        if(outputFbo!=null) outputFbo.unloadFromGpuMemory(gl);
        if(blitFbo!=null) blitFbo.unloadFromGpuMemory(gl);
    }
}
