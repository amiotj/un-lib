
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.engine.opengl.scenegraph.s2d.GLGeometry2D;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.impl.gpu.opengl.resource.Resource;

/**
 *
 * @author Johann Sorel
 */
public class Release extends PainterTask{

    private final Object candidate;


    public Release(Object candidate) {
        this.candidate = candidate;
    }
    
    public void execute(GL3Painter2D worker) {
        if(candidate==null) return;
        
        if(candidate instanceof GLGeometry2D){
            ((GLGeometry2D)candidate).dispose(worker.gl);
        }else if(candidate instanceof Resource){
            ((Resource)candidate).unloadFromGpuMemory(worker.gl);
        }else{
            throw new InvalidArgumentException("Unexpected object to release : "+candidate);
        }
    }

}
