

package science.unlicense.engine.opengl.widget;

import science.unlicense.api.character.Chars;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.engine.ui.model.ArraySpinnerModel;
import science.unlicense.engine.ui.model.NumberSliderModel;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WRangeSlider;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.impl.game.phase.GamePhases;
import static science.unlicense.impl.game.phase.GamePhases.*;
import science.unlicense.store.keyvalue.TranslationStore;

/**
 * Widget panel to configure the GamePhases parameters.
 * 
 * @author Johann Sorel 
 */
public class WGameGraphics extends WContainer{
        
    //RESOLUTION
    private final WLabel resLbl             = new WLabel();
    private final WSpinner resWidth         = new WSpinner(new NumberSpinnerModel(Integer.class,0,0,100000,1), 0);
    private final WSpinner resHeight        = new WSpinner(new NumberSpinnerModel(Integer.class,0,0,100000,1), 0);
    
    //RGB/RGBA
    private final WLabel alphaLbl           = new WLabel();
    private final WCheckBox alpha           = new WCheckBox(new Chars(" "));
    
    //SHADOW
    private final WLabel shadowLbl          = new WLabel();
    private final WCheckBox shadow          = new WCheckBox(new Chars(" "));
    private final WSpinner shadowTexture    = new WSpinner(new NumberSpinnerModel(Integer.class,512,32f,2048f,1f), 512f);
    
    //MSAA
    private final WLabel aaLbl              = new WLabel();
    private final WSpinner aaSpinner        = new WSpinner(new ArraySpinnerModel(ANTIALIAZING));
        
    //DOF
    private final WLabel dofLbl             = new WLabel();
    private final WCheckBox dof             = new WCheckBox(new Chars(" "));
    private final WRangeSlider dofFocal     = new WRangeSlider(new NumberSliderModel(Float.class,0, 30, 5));
    private final WSpinner dofBlur          = new WSpinner(new NumberSpinnerModel(Float.class,4f,2f,8f,1f), 4f);
        
    //BLOOM
    private final WLabel bloomLbl           = new WLabel();
    private final WCheckBox bloom           = new WCheckBox(new Chars(" "));
    private final WSpinner bloomIntensity   = new WSpinner(new NumberSpinnerModel(Float.class,0,0f,10f,1f), 0f);

    //GAMMA
    private final WLabel gammaLbl           = new WLabel();
    private final WSpinner gammaIntensity   = new WSpinner(new NumberSpinnerModel(Float.class,1f,0f,10f,0.1f), 1f);

    //SSAO
    private final WLabel ssaoLbl            = new WLabel();
    private final WLabel ssaoRadiusLbl      = new WLabel();
    private final WLabel ssaoIntensityLbl   = new WLabel();
    private final WLabel ssaoScaleLbl       = new WLabel();
    private final WLabel ssaoBiasLbl        = new WLabel();
    private final WCheckBox ssao            = new WCheckBox(new Chars(" "));
    private final WSpinner ssaoRadius       = new WSpinner(new NumberSpinnerModel(Float.class,0,0f,10f,1f), 0f);
    private final WSpinner ssaoIntensity    = new WSpinner(new NumberSpinnerModel(Float.class,0,0f,10f,1f), 0f);
    private final WSpinner ssaoScale        = new WSpinner(new NumberSpinnerModel(Float.class,0,0f,10f,1f), 0f);
    private final WSpinner ssaoBias         = new WSpinner(new NumberSpinnerModel(Float.class,0,0f,10f,1f), 0f);
    
    
    /**
     * 
     * @param gamePhases
     */
    public WGameGraphics(GamePhases gamePhases) {   
        this(gamePhases,null);
    }
    
    /**
     * 
     * @param gamePhases
     * @param bundle translation bundle, can be null
     */
    public WGameGraphics(GamePhases gamePhases, TranslationStore bundle) {   
        
        final FormLayout layout = new FormLayout();
        layout.setDefaultRowSpace(3);
        layout.setColumnSpace(0, 10);
        setLayout(layout);
        
        if(bundle!=null){
            resLbl.setText(bundle.getChars(new Chars("config.resolution")));
            alphaLbl.setText(bundle.getChars(new Chars("config.alpha")));
            shadowLbl.setText(bundle.getChars(new Chars("config.shadow")));
            aaLbl.setText(bundle.getChars(new Chars("config.aa")));
            dofLbl.setText(bundle.getChars(new Chars("config.dof")));
            bloomLbl.setText(bundle.getChars(new Chars("config.bloom")));
            gammaLbl.setText(bundle.getChars(new Chars("config.gamma")));
            ssaoLbl.setText(bundle.getChars(new Chars("config.ssao")));
            ssaoRadiusLbl.setText(bundle.getChars(new Chars("config.ssaoradius")));
            ssaoIntensityLbl.setText(bundle.getChars(new Chars("config.ssaointensity")));
            ssaoScaleLbl.setText(bundle.getChars(new Chars("config.ssaoscale")));
            ssaoBiasLbl.setText(bundle.getChars(new Chars("config.ssaobias")));
        }else{
            alphaLbl.setText(new Chars("Resolution"));
            alphaLbl.setText(new Chars("Alpha"));
            shadowLbl.setText(new Chars("Shadow"));
            aaLbl.setText(new Chars("AntiAliazing"));
            dofLbl.setText(new Chars("Depth of field"));
            bloomLbl.setText(new Chars("Bloom"));
            gammaLbl.setText(new Chars("Gamma"));
            ssaoLbl.setText(new Chars("SSAO"));
            ssaoRadiusLbl.setText(new Chars("Radius"));
            ssaoIntensityLbl.setText(new Chars("Intensity"));
            ssaoScaleLbl.setText(new Chars("Scale"));
            ssaoBiasLbl.setText(new Chars("Bias"));
        }
        
        int y = -1;
        
        //RESOLUTION
        y++;
        addChild(resLbl,    FillConstraint.builder().coord(0, y).build());
        addChild(resWidth,  FillConstraint.builder().coord(1, y).build());
        addChild(resHeight, FillConstraint.builder().coord(2, y).build());
        resWidth.varValue().sync(gamePhases.getProperty(PROPERTY_RESOLUTION_WIDTH));
        resHeight.varValue().sync(gamePhases.getProperty(PROPERTY_RESOLUTION_HEIGHT));
        
        //ALPHA
        y++;
        addChild(alphaLbl,     FillConstraint.builder().coord(0, y).build());
        addChild(alpha,        FillConstraint.builder().coord(1, y).build());
        alpha.varCheck().sync(gamePhases.getProperty(PROPERTY_ALPHA));
        
        //SHADOW
        y++;
        addChild(shadowLbl,          FillConstraint.builder().coord(0, y).build());
        addChild(shadow,             FillConstraint.builder().coord(1, y).build());
        addChild(shadowTexture,      FillConstraint.builder().coord(2, y).build());
        shadow.varCheck().sync(gamePhases.getProperty(PROPERTY_SHADOW));
        shadowTexture.varValue().sync(gamePhases.getProperty(PROPERTY_SHADOW_TEXTURE_SIZE));
        
        //MSAA and FXAA
        y++;
        addChild(aaLbl,            FillConstraint.builder().coord(0, y).build());
        addChild(aaSpinner,        FillConstraint.builder().coord(1, y).build());
        aaSpinner.varValue().sync(gamePhases.getProperty(PROPERTY_AASTATE));
                
        //DOF
        y++;
        dofFocal.setInnerRange(false);
        addChild(dofLbl,            FillConstraint.builder().coord(0, y).build());
        addChild(dof,               FillConstraint.builder().coord(1, y).build());
        addChild(dofFocal,          FillConstraint.builder().coord(2, y).build());
        addChild(dofBlur,           FillConstraint.builder().coord(3, y).build());
        dof.varCheck().sync(gamePhases.getProperty(PROPERTY_DOF));
        dofFocal.varValueMin().sync(gamePhases.getProperty(PROPERTY_DOF_MIN));
        dofFocal.varValueMax().sync(gamePhases.getProperty(PROPERTY_DOF_MAX));
        dofBlur.varValue().sync(gamePhases.getProperty(PROPERTY_DOF_RADIUS));
        
        //BLOOM
        y++;
        addChild(bloomLbl,          FillConstraint.builder().coord(0, y).build());
        addChild(bloom,             FillConstraint.builder().coord(1, y).build());
        addChild(bloomIntensity,    FillConstraint.builder().coord(2, y).build());
        bloom.varCheck().sync(gamePhases.getProperty(PROPERTY_BLOOM));
        bloomIntensity.varValue().sync(gamePhases.getProperty(PROPERTY_BLOOM_VALUE));

        //GAMMA
        y++;
        addChild(gammaLbl,          FillConstraint.builder().coord(0, y).build());
        addChild(gammaIntensity,    FillConstraint.builder().coord(1, y).build());
        gammaIntensity.varValue().sync(gamePhases.getProperty(PROPERTY_GAMMA_VALUE));
        
        //SSAO
        y++;
        addChild(ssaoLbl,           FillConstraint.builder().coord(0, y).build());
        addChild(ssao,              FillConstraint.builder().coord(1, y).build());
        y++;
        addChild(ssaoRadiusLbl,     FillConstraint.builder().coord(1, y).build());
        addChild(ssaoRadius,        FillConstraint.builder().coord(2, y).build());
        y++;
        addChild(ssaoIntensityLbl,  FillConstraint.builder().coord(1, y).build());
        addChild(ssaoIntensity,     FillConstraint.builder().coord(2, y).build());
        y++;
        addChild(ssaoScaleLbl,      FillConstraint.builder().coord(1, y).build());
        addChild(ssaoScale,         FillConstraint.builder().coord(2, y).build());
        y++;
        addChild(ssaoBiasLbl,       FillConstraint.builder().coord(1, y).build());
        addChild(ssaoBias,          FillConstraint.builder().coord(2, y).build());
        ssao.varCheck().sync(gamePhases.getProperty(PROPERTY_SSAO));
        ssaoRadius.varValue().sync(gamePhases.getProperty(PROPERTY_SSAO_RADIUS));
        ssaoIntensity.varValue().sync(gamePhases.getProperty(PROPERTY_SSAO_INTENSITY));
        ssaoScale.varValue().sync(gamePhases.getProperty(PROPERTY_SSAO_SCALE));
        ssaoBias.varValue().sync(gamePhases.getProperty(PROPERTY_SSAO_BIAS));
        
    }
    
}
