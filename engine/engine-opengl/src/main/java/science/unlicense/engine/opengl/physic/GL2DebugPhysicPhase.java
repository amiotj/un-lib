
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.gpu.opengl.GL1;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.force.Force;
import science.unlicense.api.physic.force.Spring;
import science.unlicense.api.physic.operation.Collision;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.phase.AbstractPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.impl.gpu.opengl.resource.FBO;

/**
 *
 * @author Johann Sorel
 */
public class GL2DebugPhysicPhase extends AbstractPhase{

    private final GLNode scene = new GLNode();
    private final World world;
    private final RenderPhase renderPhase;

    private boolean viewBody = true;
    private boolean viewBodyVelocity = true;
    private boolean viewBodyForce = true;
    private boolean viewConstraint = true;
    private boolean viewCollision = true;

    public GL2DebugPhysicPhase(World world, CameraMono camera, FBO fbo) {
        this.world = world;
        renderPhase = new RenderPhase(scene,camera,fbo);
    }

    public World getWorld() {
        return world;
    }

    public boolean isBodyVelocityVisible() {
        return viewBodyVelocity;
    }

    public void setBodyVisible(boolean visible) {
        this.viewBody = visible;
    }

    public boolean isBodyVisible() {
        return viewBody;
    }

    public void setBodyVelocityVisible(boolean visible) {
        this.viewBodyVelocity = visible;
    }

    public void setBodyForceVisible(boolean visible) {
        this.viewBodyForce = visible;
    }

    public boolean isBodyForceVisible() {
        return viewBodyForce;
    }

    public boolean isForceVisible() {
        return viewConstraint;
    }
    
    public void setForceVisible(boolean visible) {
        this.viewConstraint = visible;
    }

    public boolean isCollisionVisible() {
        return viewCollision;
    }

    public void setCollisionVisible(boolean visible) {
        this.viewCollision = visible;
    }


    @Override
    public void processInt(GLProcessContext context) throws GLException {

        final Sequence bodies = world.getBodies();

        final GL1 gl = context.getGL().asGL1();
        final Affine V = renderPhase.getCamera().getRootToNodeSpace();
        final Matrix P = renderPhase.getCamera().getProjectionMatrix();

        final Vector p0 = new Vector(4);
        final Vector p1 = new Vector(4);
        final Vector p2 = new Vector(4);

        //render rigid body geometry
        if(viewBody){
            if(gl.isGL1()){
                gl.asGL1().glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            }else{
                //TODO log, but we should not log too often either, find a solution
                Loggers.get().log(new Chars("glPolygonMode not supported for gl version < GL2GL3"), Logger.LEVEL_INFORMATION);
            }

            for(int i=0,n=bodies.getSize();i<n;i++){
                final Body body = (Body) bodies.get(i);
                if(body instanceof RigidBody){
                    Geometry geometry = ((RigidBody) body).getGeometry();

                    final Affine M = body.getNodeToRootSpace();
                    final Affine MV = V.multiply(M);

                    final GeometryMesh m;
                    try{
                        m = new GeometryMesh(geometry);
                    }catch(Exception ex){continue;}
                    GL2ShellPaintVisitor v = new GL2ShellPaintVisitor(m.getShape(),gl,MV.toMatrix(),P);
                    v.visit();
                }
            }

            if(gl.isGL1()){
                gl.asGL1().glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            }
        }

        //render constraints
        if(viewConstraint){
            gl.glPushAttrib(GL_ENABLE_BIT);
            gl.glLineWidth(6f);
            gl.glLineStipple(1, (short)0xAAAA);
            gl.glEnable(GL_LINE_STIPPLE);
            gl.glBegin(GL_LINES);
            gl.glColor3f(1f, 1f, 1f);

            final Sequence forces = world.getForces();
            for(int i=0,n=forces.getSize();i<n;i++){
                final Force force = (Force) forces.get(i);
                if(force instanceof Spring){
                    final RigidBody rb1 = ((Spring)force).getFirstEnd();
                    final RigidBody rb2 = ((Spring)force).getSecondEnd();

                    p1.setXYZW(0, 0, 0, 1);
                    p2.setXYZW(0, 0, 0, 1);

                    Affine M = rb1.getNodeToRootSpace();
                    MatrixRW MVP = P.multiply(V.multiply(M).toMatrix());
                    MVP.transform(p1,p1);
                    if(p1.getZ()<0) return; //check not outside screen

                    M = rb2.getNodeToRootSpace();
                    MVP = P.multiply(V.multiply(M).toMatrix());
                    MVP.transform(p2,p2);

                    gl.glVertex2f((float)(p1.getX()/p1.getZ()),(float)(p1.getY()/p1.getZ()));
                    gl.glVertex2f((float)(p2.getX()/p2.getZ()),(float)(p2.getY()/p2.getZ()));

                }
            }
            gl.glEnd();
            gl.glLineWidth(1f);
            gl.glPopAttrib();
        }


        //render velocities and forces
        if(viewBodyVelocity || viewBodyForce){
            gl.glLineWidth(1f);
            gl.glBegin(GL_LINES);
            for(int i=0,n=bodies.getSize();i<n;i++){
                final Body body = (Body) bodies.get(i);

                final Affine M = body.getNodeToRootSpace();
                final MatrixRW MVP = P.multiply(V.multiply(M).toMatrix());

                p0.setXYZW(0, 0, 0, 1);
                p1.set(body.getMotion().getVelocity());p1.setW(1);
                p2.set(body.getMotion().getForce().scale(0.1));p2.setW(1);
                MVP.transform(p0,p0);
                MVP.transform(p1,p1);
                MVP.transform(p2,p2);
                if(p0.getZ()<0) return; //check not outside screen

                if(viewBodyVelocity){
                    gl.glColor3f(0f, 1f, 0f);
                    gl.glVertex2f((float)(p0.getX()/p0.getZ()),(float)(p0.getY()/p0.getZ()));
                    gl.glVertex2f((float)(p1.getX()/p1.getZ()),(float)(p1.getY()/p1.getZ()));
                }
                if(viewBodyForce){
                    gl.glColor3f(0f, 0f, 1f);
                    gl.glVertex2f((float)(p0.getX()/p0.getZ()),(float)(p0.getY()/p0.getZ()));
                    gl.glVertex2f((float)(p2.getX()/p2.getZ()),(float)(p2.getY()/p2.getZ()));
                }
            }
            gl.glEnd();
        }

        //render collisions
        gl.glPointSize(12f);
        if(viewCollision){
            final Sequence collisions = world.getCollisionsState().getCollisions();
            for(int i=0,n=collisions.getSize();i<n;i++){
                final Collision collision = (Collision) collisions.get(i);

                final Geometry[] impactGeometries = collision.getImpactGeometries();

                final Point point1 = (Point) impactGeometries[0];
                final Point point2 = (Point) impactGeometries[1];

                final MatrixRW MVP = P.multiply(V.toMatrix());
                p1.set(point1.getCoordinate());p1.setW(1);
                p2.set(point2.getCoordinate());p2.setW(1);
                MVP.transform(p1,p1);
                MVP.transform(p2,p2);
                if(p1.getZ()<0) return; //check not outside screen

                gl.glBegin(GL_POINTS);
                gl.glColor3f(1f, 0f, 0f);
                gl.glVertex2f((float)(p1.getX()/p1.getZ()),(float)(p1.getY()/p1.getZ()));
                gl.glVertex2f((float)(p2.getX()/p2.getZ()),(float)(p2.getY()/p2.getZ()));
                gl.glEnd();

            }
        }

        //create meshes
        

        //render the scene
        renderPhase.process(context);
        
    }
    
}
