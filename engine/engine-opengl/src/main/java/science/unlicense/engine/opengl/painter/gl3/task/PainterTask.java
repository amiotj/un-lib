
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;

/**
 *
 * @author Johann Sorel
 */
public abstract class PainterTask{

    public abstract void execute(GL3Painter2D worker);

    protected void configureBlending(GL3Painter2D worker, AlphaBlending blending){
        if(blending!=null){
            GLUtilities.configureBlending(worker.gl, blending);
        }
    }
    
}
