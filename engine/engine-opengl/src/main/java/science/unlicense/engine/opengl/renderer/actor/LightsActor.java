
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.gpu.opengl.GL2ES2;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.opengl.ContextUtilities;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.light.AmbientLight;
import science.unlicense.engine.opengl.light.DirectionalLight;
import science.unlicense.engine.opengl.light.Light;
import science.unlicense.engine.opengl.light.PointLight;
import science.unlicense.engine.opengl.light.ShadowMap;
import science.unlicense.engine.opengl.light.SpotLight;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.Texture;

/**
 *
 * @author Johann Sorel
 */
public class LightsActor extends AbstractActor{

    public static final Chars LIGHT_NB = new Chars("LIGHT_NB");
    public static final Chars LIGHT_AMBIENT = new Chars("LIGHT_AMBIENT");
    public static final Chars CELLSHADINGSTEPS = new Chars("CELLSHADINGSTEPS");
    
    private static final ShaderTemplate SHADOW_FR;
    static {
        try{
            SHADOW_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/light/lights-shadow-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final Material material;

    //lights informations
    private boolean dirty = true;
    private String base;
    private int nbLight;
    private Uniform uniformLightNb;
    private Uniform uniformLightAmbient;
    private Uniform uniformNbCellShadingSteps;
    private final Uniform[][] uniformLight = new Uniform[5][13];
    private final int[][] shadowMapReserved = new int[5][2];
    private final float[] lightAmbiant = new float[4];
    private final float[][] lights_position = new float[5][4];
    private final float[][] lights_diffuse = new float[5][4];
    private final float[][] lights_specular = new float[5][4];
    private final float[] lights_constant = new float[5];
    private final float[] lights_linear = new float[5];
    private final float[] lights_quadratic = new float[5];
    private final float[] lights_falloffangle = new float[5];
    private final float[] lights_falloffexp = new float[5];
    private final float[][] lights_direction = new float[5][3];
    // model matrix, projection matrix, shadowmap
    private final Object[][] lightsExt = new Object[5][3];

    public LightsActor(Material material) {
        super(new Chars("Lights"));
        this.material = material;
    }
    
    public int getMinGLSLVersion() {
        return SHADOW_FR.getMinGLSLVersion();
    }
    
    public boolean isDirty() {
        return dirty;
    }

    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();
        dirty = false;
        base = null;
        fragmentShader.append(StructLight.TEMPLATE);
        fragmentShader.append(SHADOW_FR);
    }


    public void preDrawGL(RenderContext context, ActorProgram program) {

        if(dirty){
            dirty = false;
        }

        final GL2ES2 gl = context.getGL().asGL2ES2();
        GLUtilities.checkGLErrorsFail(gl);

        //grab the uniform ids
        if(base==null){
            uniformLightNb = program.getUniform(LIGHT_NB);
            uniformLightAmbient = program.getUniform(LIGHT_AMBIENT);
            uniformNbCellShadingSteps = program.getUniform(CELLSHADINGSTEPS);

            for(int i=0;i<5;i++){
                base = "LIGHTS["+i+"].";
                uniformLight[i][0] = program.getUniform(new Chars(base+"position"),gl, Uniform.TYPE_VEC4);
                uniformLight[i][1] = program.getUniform(new Chars(base+"diffuse"),gl, Uniform.TYPE_VEC4);
                uniformLight[i][2] = program.getUniform(new Chars(base+"specular"),gl, Uniform.TYPE_VEC4);
                uniformLight[i][3] = program.getUniform(new Chars(base+"constantAttenuation"),gl, Uniform.TYPE_FLOAT);
                uniformLight[i][4] = program.getUniform(new Chars(base+"linearAttenuation"),gl, Uniform.TYPE_FLOAT);
                uniformLight[i][5] = program.getUniform(new Chars(base+"quadraticAttenuation"),gl, Uniform.TYPE_FLOAT);
                uniformLight[i][6] = program.getUniform(new Chars(base+"spotCutoff"),gl, Uniform.TYPE_FLOAT);
                uniformLight[i][7] = program.getUniform(new Chars(base+"spotExponent"),gl, Uniform.TYPE_FLOAT);
                uniformLight[i][8] = program.getUniform(new Chars(base+"spotDirection"),gl, Uniform.TYPE_VEC3);
                uniformLight[i][9] = program.getUniform(new Chars(base+"hasShadowMap"),gl, Uniform.TYPE_INT);
                uniformLight[i][10] = program.getUniform(new Chars(base+"v"),gl, Uniform.TYPE_MAT4);
                uniformLight[i][11] = program.getUniform(new Chars(base+"p"),gl, Uniform.TYPE_MAT4);
                uniformLight[i][12] = program.getUniform(new Chars("SHADOWMAPS["+i+"]"),gl, Uniform.TYPE_SAMPLER2D);
                GLUtilities.checkGLErrorsFail(gl);
                lights_position[i] = new float[]{0f,1f,2f,1f};
                lights_diffuse[i] = new float[]{1f,0f,0f,1f};
                lights_specular[i] = new float[]{1f,1f,1f,1f};
                lights_constant[i] = 1f;
                lights_linear[i] = 1f;
                lights_quadratic[i] = 1f;
                lights_falloffangle[i] = 1f;
                lights_falloffexp[i] = 0f;
                lights_direction[i] = new float[]{1f,1f,1f};
            }
        }

        //update lights final RenderingContext ctx
        findLights(context);

        uniformLightNb.setInt(gl, nbLight);
        uniformLightAmbient.setVec4(gl, lightAmbiant);
        uniformNbCellShadingSteps.setFloat(gl, material.getCellShading());
        GLUtilities.checkGLErrorsFail(gl);

        //TODO popssible improvement using UBO
        // http://www.opengl.org/wiki/Uniform_Buffer_Objects
        for(int i=0;i<nbLight;i++){
            //vec4 position;
            //vec4 diffuse;
            //vec4 specular;
            //float constantAttenuation, linearAttenuation, quadraticAttenuation;
            //float spotCutoff, spotExponent;
            //vec3 spotDirection;
            gl.glUniform4fv(uniformLight[i][0].getGpuId(), lights_position[i]);
            gl.glUniform4fv(uniformLight[i][1].getGpuId(), lights_diffuse[i]);
            gl.glUniform4fv(uniformLight[i][2].getGpuId(), lights_specular[i]);
            gl.glUniform1f(uniformLight[i][3].getGpuId(), lights_constant[i]);
            gl.glUniform1f(uniformLight[i][4].getGpuId(), lights_linear[i]);
            gl.glUniform1f(uniformLight[i][5].getGpuId(), lights_quadratic[i]);
            gl.glUniform1f(uniformLight[i][6].getGpuId(), lights_falloffangle[i]);
            gl.glUniform1f(uniformLight[i][7].getGpuId(), lights_falloffexp[i]);
            gl.glUniform3fv(uniformLight[i][8].getGpuId(), lights_direction[i]);
            
            uniformLight[i][ 9].setInt(gl, (lightsExt[i][2]!=null) ? 1:0 );
            if(lightsExt[i][2]!=null){
                uniformLight[i][10].setMat4(gl, ((Matrix)lightsExt[i][0]).toArrayFloat() );
                uniformLight[i][11].setMat4(gl, ((Matrix)lightsExt[i][1]).toArrayFloat() );
                shadowMapReserved[i] = context.getResourceManager().reserveTextureId();
                gl.glActiveTexture(shadowMapReserved[i][0]);
                ((Texture)lightsExt[i][2]).bind(gl);
                uniformLight[i][12].setInt(gl, shadowMapReserved[i][1]);
                GLUtilities.checkGLErrorsFail(gl);
            }
        }

    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        // unbind textures
        for(int i=0;i<nbLight;i++){
            if(lightsExt[i][2]!=null){
                gl.glActiveTexture(shadowMapReserved[i][0]);
                gl.glBindTexture(GL_TEXTURE_2D, 0);
                context.getResourceManager().releaseTextureId(shadowMapReserved[i][0]);
            }
        }
        GLUtilities.checkGLErrorsFail(gl);
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        base = null;
        uniformLightNb = null;
        uniformLightAmbient = null;
        uniformNbCellShadingSteps = null;
        for (int x = 0; x < uniformLight.length; x++) {
            Arrays.fill(uniformLight[x], null);
        }
    }

    private void findLights(final RenderContext ctx){
        final Sequence lightCol = new ArraySequence();
        ContextUtilities.LIGHTS_VISITOR.visit(ctx.getScene(),lightCol);
        AmbientLight ambient = null;
        final Sequence others = new ArraySequence();
        if(lightCol!=null){
            for(int i=0,n=lightCol.getSize();i<n;i++){
                Light light = (Light) lightCol.get(i);
                if(light instanceof AmbientLight){
                    ambient = (AmbientLight) light;
                }else if(light instanceof PointLight){
                    others.add(light);
                }else if(light instanceof DirectionalLight){
                    others.add(light);
                }
            }
        }

        if(ambient!=null){
            Arrays.fill(lightAmbiant, 1.0f);
            ambient.getDiffuse().toRGBAPreMul(lightAmbiant,0);
        }else{
            lightAmbiant[0] = 0f;
            lightAmbiant[1] = 0f;
            lightAmbiant[2] = 0f;
            lightAmbiant[3] = 1.0f;
        }

        //System.out.println("------");
        //fill light values
        nbLight = Maths.min(others.getSize(),5);
        for(int i=0;i<nbLight;i++){
            final Light light = (Light) others.get(i);
            final Affine ntw = light.getNodeToRootSpace();
            final Vector worldPosition = new Vector(0, 0, 0);
            ntw.transform(worldPosition,worldPosition);

            //vec4 position;
            lights_position[i][ 0] = (float)worldPosition.get(0);
            lights_position[i][ 1] = (float)worldPosition.get(1);
            lights_position[i][ 2] = (float)worldPosition.get(2);
            lights_position[i][ 3] = 1f;

            //vec4 diffuse;
            light.getDiffuse().toRGBAPreMul(lights_diffuse[i],0);

            //vec4 specular;
            light.getSpecular().toRGBAPreMul(lights_specular[i],0);

            if(light instanceof DirectionalLight){
                final DirectionalLight direct = (DirectionalLight) light;
                final Tuple direction = direct.getWorldSpaceDirection();
                lights_direction[i][0] = (float)direction.getX();
                lights_direction[i][1] = (float)direction.getY();
                lights_direction[i][2] = (float)direction.getZ();
                lights_position[i][ 3] = 0f;


            }else if(light instanceof SpotLight){
                final SpotLight sl = (SpotLight) light;
                //float constantAttenuation, linearAttenuation, quadraticAttenuation;
                lights_constant[i] = sl.getAttenuation().getConstant();
                lights_linear[i] = sl.getAttenuation().getLinear();
                lights_quadratic[i] = sl.getAttenuation().getquadratic();

                //float spotCutoff, spotExponent;
                lights_falloffangle[i] = sl.getFallOffAngle();
                lights_falloffexp[i] = sl.getFallOffExponent();

                //vec3 spotDirection;
                final Tuple direction = sl.getWorldSpaceDirection();
                lights_direction[i][0] = (float)direction.getX();
                lights_direction[i][1] = (float)direction.getY();
                lights_direction[i][2] = (float)direction.getZ();

            }else if(light instanceof PointLight){
                final PointLight pl = (PointLight) light;
                //float constantAttenuation, linearAttenuation, quadraticAttenuation;
                lights_constant[i] = pl.getAttenuation().getConstant();
                lights_linear[i] = pl.getAttenuation().getLinear();
                lights_quadratic[i] = pl.getAttenuation().getquadratic();
                lights_falloffangle[i] = 180f;

            }else{
                throw new RuntimeException("Unknowned light type");
            }

            //shadow map
            final ShadowMap sm = light.getShadowMap();
            if(sm!=null && sm.getFbo().isOnGpuMemory()){
                final CameraMono camera = sm.getCamera();
                lightsExt[i][0] = camera.getRootToNodeSpace();
                lightsExt[i][1] = camera.getProjectionMatrix();
                lightsExt[i][2] = sm.getFbo().getTexture(GLC.FBO.Attachment.COLOR_0);
            }
            
        }

    }

}
