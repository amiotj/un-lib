
package science.unlicense.engine.opengl.mesh;

import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * 
 * @author Johann Sorel
 */
public class InstancingShell extends Shell{

    protected VBO instanceTransforms;

    public InstancingShell() {
    }

    public void set(Shell shell){
        setVertices(shell.getVertices());
        setNormals(shell.getNormals());
        setTangents(shell.getTangents());
        setUVs(shell.getUVs());
        setIndexes(shell.getIndexes(), shell.getModes());
        setMorphs(shell.getMorphs());
    }
    
    public VBO getInstanceTransforms() {
        return instanceTransforms;
    }

    public void setInstanceTransforms(VBO instanceTransforms) {
        this.instanceTransforms = instanceTransforms;
    }
    
}
