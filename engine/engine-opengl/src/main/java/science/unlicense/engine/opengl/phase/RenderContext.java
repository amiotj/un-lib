
package science.unlicense.engine.opengl.phase;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Rendering Context, link a GLScene, a Camera and timing.
 * The rendering process is composed of a sequence of Phase, allowing
 * some preprocessing or effects.
 *
 * @author Johann Sorel
 */
public interface RenderContext extends GLProcessContext {

    GLNode getScene();

}
