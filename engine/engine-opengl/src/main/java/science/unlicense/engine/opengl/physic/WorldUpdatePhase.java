
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.physic.World;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractPhase;

/**
 * Update phase for physic world.
 *
 * @author Johann Sorel
 */
public class WorldUpdatePhase extends AbstractPhase{

    private final World world;

    public WorldUpdatePhase(World world) {
        this.world = world;
    }

    public WorldUpdatePhase(Chars id,World world) {
        super(id);
        this.world = world;
    }
    
    public World getWorld() {
        return world;
    }

    public void processInt(GLProcessContext context) throws GLException {
        if(!isEnable()) return;
        world.update(context.getDiffTimeSecond());

        if(world instanceof SceneWorld){
            //update skeletons, those are a special case, their bind pose must be updated
            //after physics and animation operations.
            final Iterator ite = ((SceneWorld)world).getSkeletons().createIterator();
            while(ite.hasNext()){
                final Skeleton skeleton = (Skeleton) ite.next();
                skeleton.updateBindPose();
            }
        }
    }

}
