
package science.unlicense.engine.opengl.renderer.actor;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;

/**
 *
 * @author Johann Sorel
 */
public class NormalActor extends AbstractActor{

    private static final ShaderTemplate NORMAL_VE;
    private static final ShaderTemplate NORMAL_FR;
    static {
        try{
            //http://antongerdelan.net/opengl/normal_mapping.html
            //http://en.wikibooks.org/wiki/GLSL_Programming/Unity/Lighting_of_Bumpy_Surfaces
            NORMAL_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/normalmapping-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            NORMAL_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/normalmapping-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final UVMapping mapping;

    //GL loaded informations
    private boolean shaderDirty = true;
    private boolean resourceDirty = true;
    private int[] reservedTexture;
    private Uniform uniform;

    public NormalActor(UVMapping mapping) {
        super(new Chars("NormalMapping"));
        this.mapping = mapping;
    }

    public int getMinGLSLVersion() {
        return Maths.max(NORMAL_VE.getMinGLSLVersion(), NORMAL_FR.getMinGLSLVersion());
    }
    
    public boolean isDirty() {
        return false;
    }

   public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate vertexShader = template.getVertexShaderTemplate();
        final ShaderTemplate fragmentShader = template.getFragmentShaderTemplate();
        shaderDirty = false;

        //fill fragment shader
        uniform = null;
        vertexShader.append(NORMAL_VE);
        fragmentShader.append(NORMAL_FR);
    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Texture2D texture = mapping.getTexture();
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);
        resourceDirty = false;

        // Bind textures
        if(uniform == null){
            uniform = program.getUniform(new Chars("normalSampler0"));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        uniform.setInt(gl, reservedTexture[1]);

        GLUtilities.checkGLErrorsFail(gl);
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        gl.asGL1().glActiveTexture(reservedTexture[0]);
        gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
        context.getResourceManager().releaseTextureId(reservedTexture[0]);
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniform = null;
    }

}
