
package science.unlicense.engine.opengl.scenegraph.s2d;

import science.unlicense.api.scenegraph.s2d.GeometryNode2D;
import science.unlicense.impl.geometry.s2d.Geometry2D;

/**
 *
 * @author Johann Sorel
 */
public class GLGeometryNode2D extends GeometryNode2D{

    private GLGeometry2D glGeometry;
    
    public void setGeometry(Geometry2D geometry) {
        super.setGeometry(geometry);
        glGeometry = null;
    }

    public GLGeometry2D getGlGeometry() {
        if(glGeometry==null && geometry!=null){
            glGeometry = new GLGeometry2D(geometry);
        }
        return glGeometry;
    }

    
    
}
