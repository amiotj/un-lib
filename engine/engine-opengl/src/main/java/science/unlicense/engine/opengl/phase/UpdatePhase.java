
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.scenegraph.Camera;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 * This phase loops on all nodes calling the updaters.
 * Also updates the camera.
 *
 * it should be called first in the rendering phases.
 *
 * @author Johann Sorel
 */
public class UpdatePhase extends AbstractPhase{

    private final NodeVisitor CAMERA_VISITOR = new DefaultNodeVisitor(){
        public Object visit(final Node node, final Object context) {
            if(node instanceof Camera){
                updateNode((RenderContext)context, (GLNode) node);
            }
            return super.visit(node, context);
        }
    };
    private final NodeVisitor NODE_VISITOR = new DefaultNodeVisitor(){
        public Object visit(final Node node, final Object context) {
            if(node instanceof GLNode){
                updateNode((RenderContext)context, (GLNode) node);
            }
            return super.visit(node, context);
        }
    };

    private final DefaultRenderContext renderContext = new DefaultRenderContext(null, null);

    public UpdatePhase(GLNode root) {
        this(Chars.EMPTY,root);
    }
    
    public UpdatePhase(Chars id, GLNode root) {
        super(id);
        this.renderContext.setScene(root);
    }

    public GLNode getScene(){
        return renderContext.getScene();
    }
    
    public void setScene(GLNode scene){
        renderContext.setScene(scene);
    }
    
    public void processInt(GLProcessContext context) throws GLException {
        renderContext.setProcessContext(context);
        
        //make a first pass to update the cameras only in case the projection changes
        renderContext.getScene().accept(CAMERA_VISITOR, renderContext);

        //then update the scene nodes
        renderContext.getScene().accept(NODE_VISITOR, renderContext);
    }

    protected void updateNode(final RenderContext context, final GLNode node){
        final Iterator iteup = node.getUpdaters().createIterator();
        while(iteup.hasNext()){
            final Updater up = (Updater) iteup.next();
            if(up==null)continue;
            up.update(context,node);
        }
    }
}
