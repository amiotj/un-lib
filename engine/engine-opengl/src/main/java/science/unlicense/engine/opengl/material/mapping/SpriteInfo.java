
package science.unlicense.engine.opengl.material.mapping;

/**
 *
 * @author Johann Sorel
 */
public class SpriteInfo {

    private int width = 1;
    private int height = 1;
    private int nbCols = 1;
    private int nbRows = 1;
    private int spacing = 0;
    private int currentRow = 0;
    private int currentCol = 0;

    public SpriteInfo() {
    }
    
    /**
     * 
     * @param width
     * @param height
     * @param nbCols
     * @param nbRows
     * @param spacing spacing in pixel between each sprite
     */
    public SpriteInfo(int width,int height,int nbCols,int nbRows,int spacing) {
        this.width = width;
        this.height = height;
        this.nbCols = nbCols;
        this.nbRows = nbRows;
        this.spacing = spacing;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getNbCols() {
        return nbCols;
    }

    public int getNbRows() {
        return nbRows;
    }

    public void setCurrent(int col, int row){
        this.currentCol = col;
        this.currentRow = row;
    }
    
    public int getCurrentCol(){
        return currentCol;
    }
    
    public int getCurrentRow(){
        return currentRow;
    }
    
    /**
     * Parameters for shader.
     * [ImageIndex, spacing, width, height]
     * @return 
     */
    public float[] toParameters(){
        return new float[]{currentRow*width+currentCol,spacing,width,height};
    }
    
}
