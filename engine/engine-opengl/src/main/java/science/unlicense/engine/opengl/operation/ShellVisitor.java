
package science.unlicense.engine.opengl.operation;

import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.primitive.IntSet;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * Loop on all shell vertex informations.
 * 
 * @author Johann Sorel
 */
public abstract class ShellVisitor {

    private final IntSet visited = new IntSet();
    
    private Shell shell;
    private VBO vertices;
    private VBO normals;
    private VBO tangents;
    private VBO uvs;
    private IBO ibo;
    private IndexRange[] modes;

    private int nbBone = 0;
    private Skeleton skeleton = null;
    private VBO joints = null;
    private VBO weights = null;
    
    public ShellVisitor(Shell shell){
        this.shell = shell;
        vertices = (shell.getVertices()!=null && shell.getVertices().isOnSystemMemory()) ? shell.getVertices() : null;
        normals = (shell.getNormals()!=null && shell.getNormals().isOnSystemMemory()) ? shell.getNormals() : null;
        tangents = (shell.getTangents()!=null && shell.getTangents().isOnSystemMemory()) ? shell.getTangents() : null;
        uvs = (shell.getUVs()!=null && shell.getUVs().isOnSystemMemory()) ? shell.getUVs() : null;
        ibo = shell.getIndexes();
        modes = shell.getModes();
                
        if(shell instanceof SkinShell){
            final SkinShell ss = (SkinShell) shell;
            nbBone = ss.getMaxWeightPerVertex();
            skeleton = ss.getSkeleton();
            joints = (ss.getJointIndexes()!=null && ss.getJointIndexes().isOnSystemMemory()) ? ss.getJointIndexes() : null;
            weights = (ss.getWeights()!=null && ss.getWeights().isOnSystemMemory()) ? ss.getWeights() : null;
        }
    }

    public void reset(Shell shell){
        this.shell = shell;
        vertices = (shell.getVertices()!=null && shell.getVertices().isOnSystemMemory()) ? shell.getVertices() : null;
        normals = (shell.getNormals()!=null && shell.getNormals().isOnSystemMemory()) ? shell.getNormals() : null;
        tangents = (shell.getTangents()!=null && shell.getTangents().isOnSystemMemory()) ? shell.getTangents() : null;
        uvs = (shell.getUVs()!=null && shell.getUVs().isOnSystemMemory()) ? shell.getUVs() : null;
        ibo = shell.getIndexes();
        modes = shell.getModes();

        if(shell instanceof SkinShell){
            final SkinShell ss = (SkinShell) shell;
            nbBone = ss.getMaxWeightPerVertex();
            skeleton = ss.getSkeleton();
            joints = (ss.getJointIndexes()!=null && ss.getJointIndexes().isOnSystemMemory()) ? ss.getJointIndexes() : null;
            weights = (ss.getWeights()!=null && ss.getWeights().isOnSystemMemory()) ? ss.getWeights() : null;
        }else{
            nbBone = 0;
            skeleton = null;
            joints = null;
            weights = null;
        }

        visited.removeAll();
    }

    public void visit(){
        visited.removeAll();
        
        final IntCursor cursor = ibo.getPrimitiveBuffer().cursorInt();
        final Triangle t = new Triangle(nbBone);
        final Line l = new Line(nbBone);
        final Point p = new Point(nbBone);
        
        for(int m=0;m<modes.length;m++){
            final IndexRange range = modes[m];            
            final int mode = range.getMode();
            final int offset = range.getIndexOffset();
            final int cnt = range.getCount();
            
            if(mode==GLC.PRIMITIVE.POINTS){
                for(int i=0,n=cnt;i<n;i++){
                    final int idx0 = cursor.read(i+offset+0);
                    readPoint(p, idx0);
                    visit(p);
                    writePoint(p);
                }
            }else if(mode==GLC.PRIMITIVE.LINES){
                for(int i=0,n=cnt;i<n;i+=2){
                    final int idx0 = cursor.read(i+offset+0);
                    final int idx1 = cursor.read(i+offset+1);
                    readLine(l, idx0, idx1);
                    visit(l);
                    writeLine(l);
                }
            }else if(mode==GLC.PRIMITIVE.LINES_ADJACENCY){
                throw new UnimplementedException("TODO");
            }else if(mode==GLC.PRIMITIVE.LINE_LOOP){
                throw new UnimplementedException("TODO");
            }else if(mode==GLC.PRIMITIVE.LINE_STRIP){
                throw new UnimplementedException("TODO");
            }else if(mode==GLC.PRIMITIVE.LINE_STRIP_ADJACENCY){
                throw new UnimplementedException("TODO");
            }else if(mode==GLC.PRIMITIVE.TRIANGLES){
                for(int i=0,n=cnt;i<n;i+=3){
                    final int idx0 = cursor.read(i+offset+0);
                    final int idx1 = cursor.read(i+offset+1);
                    final int idx2 = cursor.read(i+offset+2);
                    readTriangle(t, idx0, idx1, idx2);
                    visit(t);
                    writeTriangle(t);
                }
            }else if(mode==GLC.PRIMITIVE.TRIANGLES_ADJACENCY){
                //TODO make a real type triangleadjency, same for fan and strip
                for(int i=0,n=cnt;i<n;i+=6){
                    final int idx0 = cursor.read(i+offset+0);
                    final int idx1 = cursor.read(i+offset+2);
                    final int idx2 = cursor.read(i+offset+4);
                    readTriangle(t, idx0, idx1, idx2);
                    visit(t);
                    writeTriangle(t);
                }
            }else if(mode==GLC.PRIMITIVE.TRIANGLE_FAN){
                final int idx0 = cursor.read(offset+0);
                for(int i=1,n=cnt-1;i<n;i++){
                    final int idx1 = cursor.read(i+offset);
                    final int idx2 = cursor.read(i+offset+1);
                    readTriangle(t, idx0, idx1, idx2);
                    visit(t);
                    writeTriangle(t);
                }
            }else if(mode==GLC.PRIMITIVE.TRIANGLE_STRIP){
                int idx0 = cursor.read(offset+0);
                int idx1 = cursor.read(offset+1);
                for(int i=2,n=cnt-1;i<n;i++){
                    final int idx2 = cursor.read(offset+i);
                    readTriangle(t, idx0, idx1, idx2);
                    visit(t);
                    writeTriangle(t);
                    idx0 = idx1;
                    idx1 = idx2;
                }
            }else if(mode==GLC.PRIMITIVE.TRIANGLE_STRIP_ADJACENCY){
                throw new UnimplementedException("TODO");
            }else if(mode==GLC.PRIMITIVE.PATCHES){
                throw new UnimplementedException("TODO");
            }else{
                throw new UnimplementedException("TODO");
            }
        }
    }
    
    private void readPoint(Point p, int idx0){
        readVertex(p.v0, idx0);
    }
    
    private void writePoint(Point p){
        writeVertex(p.v0);
    }
    
    private void readTriangle(Triangle p, int idx0, int idx1, int idx2){
        readVertex(p.v0, idx0);
        readVertex(p.v1, idx1);
        readVertex(p.v2, idx2);
    }
    
    private void writeTriangle(Triangle p){
        writeVertex(p.v0);
        writeVertex(p.v1);
        writeVertex(p.v2);
    }
    
    private void readLine(Line p, int idx0, int idx1){
        readVertex(p.v0, idx0);
        readVertex(p.v1, idx1);
    }
    
    private void writeLine(Line p){
        writeVertex(p.v0);
        writeVertex(p.v1);
    }
    
    private void readVertex(Vertex v, int idx){
        v.index = idx;
        v.visited = visited.contains(idx);
        if(!v.visited) visited.add(idx);
        if(vertices!=null)  vertices.getTupleFloat(idx, v.vertice);
        if(normals!=null)   normals.getTupleFloat(idx, v.normal);
        if(tangents!=null)  tangents.getTupleFloat(idx, v.tangent);
        if(uvs!=null)       uvs.getTupleFloat(idx, v.uv);
        if(joints!=null)    joints.getTupleInt(idx, v.jointIndexes);
        if(weights!=null)   weights.getTupleFloat(idx, v.jointWeights);
        for(int i=0;i<v.joints.length;i++){
            v.joints[i] = (Joint) skeleton.getAllJoints().get(v.jointIndexes[i]);
        }
    }
    
    private void writeVertex(Vertex v){
        if(vertices!=null)  vertices.setTupleFloat(v.index, v.vertice);
        if(normals!=null)   normals.setTupleFloat(v.index, v.normal);
        if(tangents!=null)  tangents.setTupleFloat(v.index, v.tangent);
        if(uvs!=null)       uvs.setTupleFloat(v.index, v.uv);
        if(joints!=null)    joints.setTupleInt(v.index, v.jointIndexes);
        if(weights!=null)   weights.setTupleFloat(v.index, v.jointWeights);
    }
    
    protected void visit(Triangle candidate){
        visit(candidate.v0);
        visit(candidate.v1);
        visit(candidate.v2);
    }
    
    protected void visit(Line candidate){
        visit(candidate.v0);
        visit(candidate.v1);
    }
    
    protected void visit(Point candidate){
        visit(candidate.v0);
    }
    
    protected abstract void visit(Vertex vertex);
    
    public static final class Triangle{
        public final Vertex v0;
        public final Vertex v1;
        public final Vertex v2;

        private Triangle(int nbBone) {
            v0 = new Vertex(nbBone);
            v1 = new Vertex(nbBone);
            v2 = new Vertex(nbBone);
        }
    }
    
    public static final class Line{
        public final Vertex v0;
        public final Vertex v1;
        
        private Line(int nbBone) {
            v0 = new Vertex(nbBone);
            v1 = new Vertex(nbBone);
        }
    }
    
    public static final class Point{
        public final Vertex v0;
        
        private Point(int nbBone) {
            v0 = new Vertex(nbBone);
        }
    }
    
    public static final class Vertex{
        public int index;
        /** True if this vertex has already been visited as part of another primitive */
        public boolean visited;
        public final float[] vertice = new float[3];
        public final float[] normal = new float[3];
        public final float[] tangent = new float[4];
        public final float[] uv = new float[2];
        public final int[] jointIndexes;
        public final float[] jointWeights;
        public final Joint[] joints;
        
        private Vertex(int nbBone){
            jointIndexes = new int[nbBone];
            jointWeights = new float[nbBone];
            joints = new Joint[nbBone];
        }
    }
    
}
