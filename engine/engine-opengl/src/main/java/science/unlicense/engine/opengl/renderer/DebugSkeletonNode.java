

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.constraint.CopyTransformConstraint;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.control.ConstraintUpdater;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.math.Matrix;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.api.math.Affine;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * A scene node which display the mesh skeleton and rigid bodies.
 * 
 * @author Johann Sorel
 */
public class DebugSkeletonNode extends GLNode{
    
    public static final Chars PROPERTY_JOINT = new Chars("joint");
    public static final Chars PROPERTY_STATE = new Chars("state");
    public static final Chars PROPERTY_RIGIDBODY = new Chars("rigidbody");
    
    
    private static final NodeVisitor RESIZE_JOINT = new DefaultNodeVisitor() {
        public Object visit(Node node, Object context) {
            if(node instanceof GeometryMesh && ((GeometryMesh)node).getProperties().getValue(PROPERTY_JOINT) != null){
                final double sphereSize = (Float)context;
                final GeometryMesh glNode = ((GeometryMesh)node);
                if(glNode.getGeometry() instanceof Sphere){
                    //resize only spheres, not lines
                    final NodeTransform trs = glNode.getNodeTransform();
                    trs.getScale().setXYZ(sphereSize, sphereSize, sphereSize);
                    trs.notifyChanged();
                }
            }
            return super.visit(node, context);
        }
    };
    private static final NodeVisitor SHOW_JOINT = new DefaultNodeVisitor() {
        public Object visit(Node node, Object context) {
            if(node instanceof GLNode && ((GLNode)node).getProperties().getValue(PROPERTY_JOINT) != null){
                final GLNode glNode = ((GLNode)node);
                glNode.setVisible((Boolean)context);
            }
            return super.visit(node, context);
        }
    };
//    public static final NodeVisitor SHOW_CONSTRAINTS = new DefaultNodeVisitor() {
//        public Object visit(Node node, Object context) {
//            if(node instanceof GLNode && ((GLNode)node).getProperties().getValue(PROPERTY_JOINT) != null){
//                final GLNode glNode = ((GLNode)node);
//                glNode.setVisible((Boolean)context);
//            }
//            return super.visit(node, context);
//        }
//    };
    public static final NodeVisitor SHOW_PHYSIC = new DefaultNodeVisitor() {
        public Object visit(Node node, Object context) {
            if(node instanceof GLNode && ((GLNode)node).getProperties().getValue(PROPERTY_RIGIDBODY) != null){
                final GLNode glNode = ((GLNode)node);
                glNode.setVisible((Boolean)context);
            }
            return super.visit(node, context);
        }
    };
    
    
    private final CoordinateSystem cs;
    private final Skeleton skeleton;
    private float jointSize = 0.1f;
    private boolean jointVisible = false;
    private boolean rigidBodyVisible = false;
    
    public DebugSkeletonNode(MultipartMesh mpm){
        this.cs = mpm.getLocalCoordinateSystem();
        this.skeleton = mpm.getSkeleton();
        
        setName(new Chars("SKELETON DEBUG"));
        
        final Sequence workroots = skeleton.getChildren();
        for(int i=0,n=workroots.getSize();i<n;i++){
            final GLNode gljoint = debugJoint((Joint)workroots.get(i));
            getChildren().add(gljoint);
        }
        
        setJointSize(jointSize);
        setJointVisible(jointVisible);
        setRigidBodyVisible(rigidBodyVisible);
    }
    
    public void setJointSize(float size){
        this.jointSize = size;
        accept(RESIZE_JOINT, size);
    }
    
    public float getJointSize(){
        return jointSize;
    }

    public void setJointVisible(boolean jointVisible) {
        this.jointVisible = jointVisible;
        accept(SHOW_JOINT, jointVisible);
    }

    public boolean isJointVisible() {
        return jointVisible;
    }

    public void setRigidBodyVisible(boolean rigidBodyVisible) {
        this.rigidBodyVisible = rigidBodyVisible;
        accept(SHOW_PHYSIC, rigidBodyVisible);
    }

    public boolean isRigidBodyVisible() {
        return rigidBodyVisible;
    }
    
    private GLNode debugJoint(final Joint joint) {
        final GLNode node = new GLNode(cs);

        node.getUpdaters().add(new ConstraintUpdater(new CopyTransformConstraint(node, joint, 1)));

        final Color color = getColor(joint, skeleton);
        
        for(int i=joint.getChildren().getSize()-1;i>=0;i--){
            final Node nc = (Node) joint.getChildren().get(i);

            if(nc instanceof Joint){
                final Joint child = (Joint) nc;

                //add a line toward the child
                final Affine childmatrix = child.getNodeTransform();
                final Tuple t = childmatrix.transform(new Vector(0, 0, 0),null);
                float[] end = t.toArrayFloat();
                final Mesh line = new Mesh();
                line.setLocalCoordinateSystem(cs);
                line.getProperties().add(PROPERTY_JOINT, joint);
                line.getMaterial().setDiffuse(color);
                line.setPickable(false);
                final Shell shell = new Shell();
                final FloatCursor fl = GLBufferFactory.INSTANCE.createFloat(2*3).cursorFloat();
                fl.write(0).write(0).write(0);
                fl.write(end);
                shell.setVertices(new VBO(fl.getBuffer(),3));
                shell.setNormals(new VBO(fl.getBuffer(),3));
                shell.setIndexes(new IBO(new int[]{0,1}, 2), IndexRange.LINES(0, 2));
                line.setShape(shell);
                node.getChildren().add(line);

                //build the child tree
                final GLNode childdj = debugJoint(child);
                node.getChildren().add(childdj);
            }else if(nc instanceof RigidBody){
                final RigidBody body = (RigidBody) nc;
                final Geometry geom = body.getGeometry();

                final Mesh ele;
                if(GeometryMesh.canHandle(geom.getClass())){
                    ele = new GeometryMesh(geom, 10, 10);
                }else{
                    ele = null;
                    System.out.println("Can not display geometry of type : "+geom.getClass());
                }

                if(ele != null){
                    ele.setLocalCoordinateSystem(cs);
                    ele.getProperties().add(PROPERTY_RIGIDBODY, body);
                   ((MeshRenderer)ele.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
                    ele.getNodeTransform().set(body.getNodeTransform());
                    ele.getNodeTransform().notifyChanged();
                    ele.setPickable(false);
                    final Color rbColor = getColor(body);
                    ele.getMaterial().setDiffuse(rbColor);
                    node.getChildren().add(ele);
                }

            }
        }

        //small sphere at joint root
        final Mesh sphere = new GeometryMesh(new Sphere(1), 10, 10);
        sphere.setLocalCoordinateSystem(cs);
        sphere.getMaterial().setDiffuse(color);
        ((MeshRenderer)sphere.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        sphere.setPickable(true);
        sphere.getProperties().add(PROPERTY_JOINT, joint);
        sphere.getNodeTransform().getScale().setXYZ(jointSize, jointSize, jointSize);
        sphere.getNodeTransform().notifyChanged();
        node.getChildren().add(sphere);

        //constraints
//        final Sequence csts = joint.getConstraints();
//        for(int i=0,n=csts.getSize();i<n;i++){
//            final Constraint cst = (Constraint) csts.get(i);
//            if(cst instanceof AngleLimitConstraint){
//                final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
//                final double min = alc.getLimits().getMin(0);
//                final double max = alc.getLimits().getMax(0);
//                final Mesh cone = new GeometryMesh(new Cone(1,0.5), 10, 10);
//                cone.getMaterial().putLayer(new Layer(new ColorMapping(color)));
//                cone.getMaterial().setLightVulnerable(false);
//                cone.setPickable(false);
//                //cone.getProperties().add(PROPERTY_JOINT, joint);
//                //cone.getModelTransform().getScale().setXYZ(shpereSize, shpereSize, shpereSize);
//                cone.getModelTransform().notifyChanged();
//                node.addChild(cone);
//            }else{
//                System.out.println("can not display joint constraint of type : "+cst.getClass().getSimpleName());
//            }
//        }
        
        return node;
    }

    public static Color getColor(Joint jt, Skeleton skeleton){
        final Color color;
        if(jt.isKinematicTarget(skeleton)){
            color = Color.RED;
        }else if(jt.isKinematicEffector(skeleton)){
            color = Color.GREEN;
        }else if(jt.isKinematic(skeleton)){
            color = Color.BLUE;
        }else{
            color = Color.GRAY_LIGHT;
        }
        return color;
    }
    
    public static Color getColor(RigidBody body){
        return  body.isFree() ? Color.BLUE : Color.RED;
    }
    
}
