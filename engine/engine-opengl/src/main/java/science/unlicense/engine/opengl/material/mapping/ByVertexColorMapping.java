
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * By vertex color texture.
 * 
 * @author Johann Sorel
 */
public final class ByVertexColorMapping implements Mapping{

    private VBO colors;
    private boolean hasAlpha;
    private boolean dirty = true;
    //informations
    private boolean opaque = false;

    /**
     * Default color mapping.
     * With a red color for all vertices.
     */
    public ByVertexColorMapping() {
        final Buffer colors = GLBufferFactory.INSTANCE.createFloat(3);
        colors.cursorFloat().write(new float[]{1,0,0});
        this.colors = new VBO(colors, 3);
        hasAlpha = false;
        opaque = true;
    }

    public ByVertexColorMapping(Buffer colors, boolean hasAlpha){
        this.colors = new VBO(colors,hasAlpha ? 4 : 3);
        this.hasAlpha = hasAlpha;
        checkOpaque();
    }

    public boolean isOpaque(){
        return opaque;
    }

    private void checkOpaque(){
        if(!hasAlpha){
            opaque = true;
            return;
        }

        opaque = true;
        if(colors==null) return;

        final Buffer buffer = colors.getPrimitiveBuffer();
        final long size = buffer.getPrimitiveCount();
        for(long i=3;i<size;i+=4){
            float f = buffer.readFloat(i*4);
            if(f<1f){
                opaque = false;
                break;
            }
        }
    }


    /**
     * {@inheritDoc }
     */
    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    /**
     * Float buffer for colors.
     * 3 or 4 components based on hasAlpha value.
     * @return FloatBuffer
     */
    public VBO getColors() {
        return colors;
    }

    /**
     * Set colors buffer.
     * 3 or 4 components based on hasAlpha value.
     * @param colors
     */
    public void setColors(Buffer colors) {
        this.colors = new VBO(colors,hasAlpha ? 4 : 3);
        checkOpaque();
    }

    /**
     * Get if colors are expressed with 3 or 4 components.
     * RGB or RGBA.
     * @return true if colors are expressed with alpha
     */
    public boolean getHasAlpha() {
        return hasAlpha;
    }

    /**
     * Set colors type, RGB or RGBA.
     * 3 or 4 components by colors.
     * @param hasAlpha
     */
    public void setHasAlpha(boolean hasAlpha) {
        this.hasAlpha = hasAlpha;
        checkOpaque();
    }

    public void dispose(GLProcessContext context) {
        
    }

}
