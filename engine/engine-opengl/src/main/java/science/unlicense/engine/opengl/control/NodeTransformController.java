
package science.unlicense.engine.opengl.control;

import science.unlicense.api.math.MatrixRW;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;

/**
 * Utility class to control a node movements.
 * For example once attached to a camera node it can be linked to mouse
 * and keyboard events to control navigation.
 *
 * @author Johann Sorel
 */
public class NodeTransformController implements Updater{

    private final GLNode node;
    private final Vector up;
    private final Vector right;
    private final boolean rightHanded;

    //diff
    private final Vector translation = new Vector(3);
    private final Matrix3x3 rotation = new Matrix3x3().setToIdentity();

    public NodeTransformController(GLNode node, Vector up, Vector right) {
        this(node, up, right, true);
    }
    
    public NodeTransformController(GLNode node, Vector up, Vector right, boolean rightHanded) {
        this.node = node;
        this.up = up;
        this.right = right;
        this.rightHanded = rightHanded;
    }

    public Vector getUp() {
        return up.copy();
    }

    public Vector getRight() {
        return right.copy();
    }

    public Vector getForward() {
        final Vector forward = getUp().cross(getRight(), null);
        forward.localNormalize();
        return forward;
    }

    public void moveForward(float scale){
        final Vector forward = getForward();
        move(forward, scale);
    }

    public void moveBackward(float scale){
        final Vector backward = getForward();
        backward.localNegate();
        backward.localNormalize();
        move(backward, scale);
    }

    public void moveLeft(float scale){
        move(getRight().negate(null), scale);
    }

    public void moveRight(float scale){
        move(getRight(), scale);
    }

    public void moveUp(float scale){
        move(getUp(), scale);
    }

    public void moveDown(float scale){
        move(getUp().negate(null), scale);
    }

    public void move(Vector direction, float scale){
        direction = direction.scale(scale);
        translation.localAdd(direction);
    }

    public void rotate(Vector axis, float angle){
        final double[][] rotMatrice = Matrices.createRotation3(angle, axis, null);
        final Matrix3x3 r = new Matrix3x3(rotMatrice);

        r.localMultiply(rotation);
        rotation.set(r);
    }

    @Override
    public void update(RenderContext context, GLNode node) {

        if(translation.isZero() && rotation.isIdentity()) return;
        
        final NodeTransform trs = node.getNodeTransform();
        final MatrixRW nodeRotation = trs.getRotation();

        nodeRotation.transform(translation,translation);
        nodeRotation.localMultiply(rotation);

        trs.getTranslation().localAdd(translation);
        trs.notifyChanged();

        //reset our diffs
        translation.setToZero();
        rotation.setToIdentity();
    }

}
