

package science.unlicense.engine.opengl.phase.picking;

import java.nio.IntBuffer;
import science.unlicense.api.event.DefaultEventMessage;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * @author Johann Sorel
 */
public class PickMessage extends DefaultEventMessage {
    private final GLNode selection;
    private final int primitiveId;

    public PickMessage(GLNode pickedObject, int primitiveId) {
        super(false);
        this.selection = pickedObject;
        this.primitiveId = primitiveId;
    }

    /**
     * Get the picked scene object.
     * @return GLNode, usualy a mesh
     */
    public GLNode getSelection() {
        return selection;
    }

    /**
     * Get the gl_PrimitiveId.
     * @return 
     */
    public int getPrimitiveId() {
        return primitiveId;
    }
    
    /**
     * Try to find the closest vertex in the selected object.
     * @return vertex index or -1 if it could not calculated.
     */
    public int[] findVertexId(GLProcessContext ctx) {
        final Mesh mesh = (Mesh) selection;
        
        final Shape shape = mesh.getShape();
        if(!(shape instanceof Shell)) return new int[]{-1,-1,-1};
        
        final Shell shell = (Shell) shape;
        final IBO ibo = shell.getIndexes();        
        final IntBuffer ib = (IntBuffer) ibo.getPrimitiveBuffer();
        
        final IndexRange range = shell.getModes()[0];
        final int offset = range.getIndexOffset();
        
        final int vid0 = ib.get(offset+primitiveId*3+0);
        final int vid1 = ib.get(offset+primitiveId*3+1);
        final int vid2 = ib.get(offset+primitiveId*3+2);
        
        return new int[]{vid0,vid1,vid2};
    }
    
}
