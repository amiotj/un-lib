
package science.unlicense.engine.opengl.material.mapping;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.gpu.opengl.resource.TextureCube;
import science.unlicense.impl.gpu.opengl.resource.TextureUtils;
import science.unlicense.api.image.Image;

/**
 * Cube mapping texturing.
 * 
 * @author Johann Sorel
 */
public final class CubeMapping implements Mapping {

    private TextureCube texture;
    private Boolean opaque = null;

    public CubeMapping() {
    }

    public CubeMapping(TextureCube texture) {
        this.texture = texture;
    }

    public boolean isOpaque(){
        if(opaque==null) checkOpaque();
        return opaque;
    }

    private void checkOpaque(){
        opaque = true;
        Image image = texture.getImageNegativeX();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
        image = texture.getImageNegativeY();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
        image = texture.getImageNegativeZ();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
        image = texture.getImagePositiveX();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
        image = texture.getImagePositiveY();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
        image = texture.getImagePositiveZ();
        if(image!=null) opaque &= TextureUtils.isOpaque(image);
    }

    public TextureCube getTexture() {
        return texture;
    }

    public void setTexture(TextureCube texture) {
        this.texture = texture;
    }

    public boolean isDirty() {
        if(texture!=null){
            return texture.isDirty();
        }
        return false;
    }

    public void dispose(GLProcessContext context) {
        if(texture!=null){
            texture.unloadFromGpuMemory(context.getGL());
        }
    }
    
}
