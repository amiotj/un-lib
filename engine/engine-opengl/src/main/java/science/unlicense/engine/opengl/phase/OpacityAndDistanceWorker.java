
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.Sorter;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.geometry.BBox;
import science.unlicense.impl.geometry.Point;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.math.Vector;

/**
 * Sort objects by opacity and distance.
 * 
 * Order produces :
 * - Opaque close distance
 * - Opaque far distance
 * - Translucent far distance
 * - Translucent close distance
 * 
 * @author Johann Sorel
 */
public class OpacityAndDistanceWorker {

    private final GLNode origin;
    private final FlattenVisitor flattener = new FlattenVisitor();
    private final Sequence opaques = new ArraySequence();
    private final Sequence translucents = new ArraySequence();
    private final Dictionary distances = new HashDictionary();
    private final Sequence sorted = new ArraySequence();

    private final Sorter distanceSort = new Sorter() {
            public int sort(Object first, Object second) {
                Double d1 = (Double) distances.getValue(first);
                Double d2 = (Double) distances.getValue(second);
                if(d1==null || d1.isNaN()) return -1;
                if(d2==null || d2.isNaN()) return -1;
                if(d1.doubleValue() < d2.doubleValue()){
                    return -1;
                }else if(d1.doubleValue() > d2.doubleValue()){
                    return +1;
                }else{
                    return 0;
                }
            }
        };
    
    private final Point originPoint;
    
    public OpacityAndDistanceWorker(GLNode origin) {
        this.origin = origin;
        
        final Affine m = origin.getNodeToRootSpace();
        final Vector p = new Vector(0, 0, 0);
        originPoint = new Point(m.transform(p,p));
    }
    
    public Sequence prepare(Sequence objects){
        opaques.removeAll();
        translucents.removeAll();
        flattener.reset();
        distances.removeAll();
        sorted.removeAll();
                
        final Iterator ite = objects.createIterator();
        while(ite.hasNext()){
            final Object obj = ite.next();
            double dist = Double.NaN;
            if(obj instanceof Mesh){
                final Mesh mesh = (Mesh) obj;
                final BBox bbox = mesh.getShape().getBBox();
                if(bbox!=null){
                    final Affine m = mesh.getNodeToRootSpace();
                    final TupleRW wl = m.transform(bbox.getLower(),null);
                    final TupleRW wu = m.transform(bbox.getUpper(),null);
                    final BBox wbbox = new BBox(wl, wl.copy());
                    wbbox.expand(wu);
                    try {
                        dist = wbbox.distance(originPoint);
                    } catch (OperationException ex) {
                        ex.printStackTrace();
                    }
                }
                
                if(mesh.getMaterial().isOpaque()){
                    opaques.add(mesh);
                }else{
                    translucents.add(mesh);
                }
                distances.add(obj,dist);
            }
        }
        
        Collections.sort(opaques,distanceSort);
        Collections.sort(translucents,distanceSort);
        
        final Sequence inv = Collections.reverse(translucents);
        sorted.addAll(opaques);
        sorted.addAll(inv);
        
        return sorted;
    }
    
    
    
    public Sequence prepare(Node scene){
        opaques.removeAll();
        translucents.removeAll();
        flattener.reset();
        distances.removeAll();
        
        
        //flatten the scene tree and remove nodes marked unvisible.
        scene.accept(flattener, null);
        final Sequence result = flattener.getCollection();
                
        //separate opaque and translucent elements
        for(int i=0,n=result.getSize();i<n;i++){
            Node node = (Node) result.get(i);
            if(node instanceof Mesh && !((Mesh)node).getMaterial().isOpaque()){
                translucents.add(node);
            }else{
                opaques.add(node);
            }
            
            //calculate distance, used later
            if(node instanceof Mesh){
                final BBox bbox = ((Mesh)node).getShape().getBBox();
                //TODO must handle camera to node transform
            }
            
        }
        
        //sort translucent by distance
        //TODO
        
        
        return result;
    }
    
    
    public int sort(Object first, Object second) {

        if(!(first instanceof Mesh)) return +1;
        if(!(second instanceof Mesh)) return -1;

        final boolean m1 = ((Mesh) first).getMaterial().isOpaque();
        final boolean m2 = ((Mesh) second).getMaterial().isOpaque();

        if(m1 && m2){
            //sort by distance
            final double distance1 = distance((Mesh) first);
            final double distance2 = distance((Mesh) second);
            if(distance1<distance2){
                return -1;
            }else if(distance1>distance2){
                return +1;
            }else{
                return 0;
            }
        }else if(m1){
            return -1;
        }else if(m2){
            return +1;
        }else{
            //sort by inverse distance
            final double distance1 = distance((Mesh) first);
            final double distance2 = distance((Mesh) second);
            if(distance1>distance2){
                return -1;
            }else if(distance1<distance2){
                return +1;
            }else{
                return 0;
            }
        }
    }
    
    private double distance(Mesh mesh){
        //TODO merge oriented geometry rotation+scale with NodeTransform
        //Use neaest points to find if mesh is in front or behind camera
        return 0;
    }
    
}