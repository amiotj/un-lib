

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.Actors;

/**
 * Display the normals stored in the mesh shell.
 * 
 * @author Johann Sorel
 */
public class DebugBoneWeightRenderer extends AbstractRenderer {

    private static final ShaderTemplate WEIGHT_VE;
    private static final ShaderTemplate WEIGHT_FR;
    static {
        try{
            WEIGHT_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugweight-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            WEIGHT_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugweight-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private int jointIndex = 0;
    private ActorProgram program;
    private Uniform uniJointIndex;

    public DebugBoneWeightRenderer() {
    }

    public DebugBoneWeightRenderer(int jointIndex) {
        this.jointIndex = jointIndex;
    }

    public int getJointIndex() {
        return jointIndex;
    }

    public void setJointIndex(int jointIndex) {
        this.jointIndex = jointIndex;
    }

    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        final Mesh mesh = (Mesh) node;
        
        if(program==null){
            program = new ActorProgram();
            
            //build material actor
            final Material material = new Material();
            //material.setMesh(mesh);
            material.setDiffuse(Color.RED);
            final MaterialActor materialActor = new MaterialActor(material,false);

            //build shell actor
            final Shape shape = mesh.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(mesh, shape);
            program.setExecutor(shellActor);

            final DefaultActor debugActor = new DefaultActor(new Chars("DebugJointWeight"),false,
                    WEIGHT_VE, null, null, null, WEIGHT_FR, true, true){
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniJointIndex.setInt(context.getGL().asGL2ES2(), jointIndex);
                    }
                };
            
            program.getActors().add(shellActor);
            program.getActors().add(materialActor);
            program.getActors().add(debugActor);
            program.compile(context);
            uniJointIndex = program.getUniform(new Chars("debugJointIndex"));
        }

        program.render(context, camera, mesh);
    }

    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
        }
    }
    
}
