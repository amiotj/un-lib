

package science.unlicense.engine.opengl.phase.effect;

import science.unlicense.api.character.Chars;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.io.IOException;

/**
 * Blend two textures
 * 
 * @author Johann Sorel
 */
public class BlendPhase extends AbstractTexturePhase {
    
    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/effect/blend-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final BlendActor actor = new BlendActor();
    
    public BlendPhase(FBO output,Texture texture1, Texture texture2) {
        super(output, texture1, texture2);
    }
        
    protected BlendActor getActor() {
        return actor;
    }

    private final class BlendActor extends DefaultActor{

        public BlendActor() {
            super(null,false,null,null,null,null,SHADER_FR,true,true);
        }
        
    }
}
