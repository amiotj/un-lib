
package science.unlicense.engine.opengl.light;

import science.unlicense.api.math.Maths;
import science.unlicense.impl.math.Vector;

/**
 * Utils for lights.
 *
 * @author Johann Sorel
 */
public final class Lights {

    private Lights(){}

    /**
     * Phong light model.
     *
     * @param ambient color
     * @param diffuse color
     * @param specular color
     * @param lightPosition in camera space
     * @param lightShininess
     * @param fragmentPosition in camera space
     * @param fragmentNormal in camera space, expected to be normalized
     * @param buffer to store result, can be null
     * @return Tuple, same as buffer is not null, new tuple otherwise
     */
    public static float[] phong(float[] ambient, float[] diffuse, float[] specular,
                                     Vector lightPosition, float lightShininess,
                                     Vector fragmentPosition, Vector fragmentNormal,
                                     float[] buffer){

        // fragment to light direction
        final Vector fragToLight = lightPosition.subtract(fragmentPosition).localNormalize();

        final float lambertian = Maths.max( (float)fragToLight.dot(fragmentNormal), 0f);

        float specFactor = 0;
        if(lambertian > 0f){
            // Eye direction
            final Vector eyeToFrag = fragmentPosition.negate().localNormalize();
            final Vector reflect = fragToLight.negate().reflect(fragmentNormal);
            final float angle = (float) Maths.max(reflect.dot(eyeToFrag), 0f);
            specFactor = (float) Math.pow(angle, lightShininess/4.0);
        }

        if(buffer==null) buffer = new float[ambient.length];

        buffer[0] = ambient[0] + (diffuse[0]*lambertian) + (specular[0]*specFactor);
        buffer[1] = ambient[1] + (diffuse[1]*lambertian) + (specular[1]*specFactor);
        buffer[2] = ambient[2] + (diffuse[2]*lambertian) + (specular[2]*specFactor);
        return buffer;
    }

    /**
     * Blinn-Phong light model.
     *
     * @param ambient color
     * @param diffuse color
     * @param specular color
     * @param lightPosition in camera space
     * @param lightShininess
     * @param fragmentPosition in camera space
     * @param fragmentNormal in camera space, expected to be normalized
     * @param buffer to store result, can be null
     * @return Tuple, same as buffer is not null, new tuple otherwise
     */
    public static float[] blinnPhong(float[] ambient, float[] diffuse, float[] specular,
                                     Vector lightPosition, float lightShininess,
                                     Vector fragmentPosition, Vector fragmentNormal,
                                     float[] buffer){

        // fragment to light direction
        final Vector fragToLight = lightPosition.subtract(fragmentPosition).localNormalize();

        final float lambertian = Maths.max( (float)fragToLight.dot(fragmentNormal), 0f);

        float specFactor = 0;
        if(lambertian > 0f){
            // Eye direction
            final Vector eyeToFrag = fragmentPosition.negate().localNormalize();
            //Blinn simplification
            final Vector halfDir = fragToLight.add(eyeToFrag).localNormalize();
            final float specAngle = Maths.max( (float)halfDir.dot(fragmentNormal), 0f);
            specFactor = (float) Math.pow(specAngle, lightShininess);
        }

        if(buffer==null) buffer = new float[ambient.length];

        buffer[0] = ambient[0] + (diffuse[0]*lambertian) + (specular[0]*specFactor);
        buffer[1] = ambient[1] + (diffuse[1]*lambertian) + (specular[1]*specFactor);
        buffer[2] = ambient[2] + (diffuse[2]*lambertian) + (specular[2]*specFactor);
        return buffer;
    }


}
