
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.physic.constraint.Constraint;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.anim.KeyFrameAnimation;
import science.unlicense.api.anim.KeyFrameTimeSerie;
import science.unlicense.impl.anim.TransformAnimation;

/**
 *
 * @author Johann Sorel
 */
public class SkeletonAnimation extends KeyFrameAnimation {
    
    private CoordinateSystem coordinateSystem = CoordinateSystem.CARTESIAN3D_METRIC_RIGH_HANDED;
    private Skeleton skeleton;
    private final TransformAnimation anim = new TransformAnimation();

    public SkeletonAnimation() {
    }

    public SkeletonAnimation(Skeleton skeleton) {
        this.skeleton = skeleton;
    }
    
    public SkeletonAnimation(Skeleton skeleton, CoordinateSystem cs) {
        this.skeleton = skeleton;
        this.coordinateSystem = cs;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void setSkeleton(Skeleton skeleton) {
        this.skeleton = skeleton;
    }

    /**
     * Get the coordinate system in which the values are declared.
     * @return CoordinateSystem
     */
    public CoordinateSystem getCoordinateSystem() {
        return coordinateSystem;
    }

    /**
     * Set the coordinate system in which the values are declared.
     * This method do not change the stored coordinates.
     * Use CSUtilies to transform them.
     * 
     * @param coordinateSystem
     */
    public void setCoordinateSystem(CoordinateSystem coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }
    
    /**
     * Get the serie for given bone name
     * @param boneName not null
     * @return JointTimeSerie may be null
     */
    public JointTimeSerie getJointTimeSerie(Chars boneName){
        for(int i=0,n=timeSeries.getSize();i<n;i++){
            final JointTimeSerie jts = (JointTimeSerie) timeSeries.get(i);
            final Joint joint = jts.getJoint();
            if(joint!=null){
                //test the joint name
                if(boneName.equals(jts.getJoint().getName())){
                    return jts;
                }
            }else{
                //test the joint identifier
                if(boneName.equals(jts.getJointIdentifier())){
                    return jts;
                }
            }

        }
        return null;
    }

    public void stop() {
        super.stop();
        if(skeleton!=null){
            skeleton.resetToBase();
        }
    }

    public Chars toChars() {
        final CharBuffer sb = new CharBuffer();
        sb.append("Skeleton animation : \n");
        for(int i=0,n=timeSeries.getSize();i<n;i++){
            final JointTimeSerie serie = (JointTimeSerie) timeSeries.get(i);
            sb.append(serie.toChars()).append('\n');
        }
        return sb.toChars();
    }

    @Override
    public void update() {
        super.update();
        
        skeleton.updateBindPose();
        skeleton.solveKinematics();
        skeleton.updateBindPose();
        
        //ensure constraints
        final Sequence joints = skeleton.getAllJoints();
        for(int i=0,n=joints.getSize();i<n;i++){
            final Joint jt = (Joint) joints.get(i);
            final Sequence constraints = jt.getConstraints();
            if(!constraints.isEmpty()){
                final Iterator ite = constraints.createIterator();
                while(ite.hasNext()){
                    final Constraint cst = (Constraint) ite.next();
                    cst.apply();
                }
            }
        }
        skeleton.updateBindPose();
        
//        if(!paused){
//            //update physics
//            //TODO this should be in the world physics
//            final World w = new SceneWorld(node);
//            w.addSingularity(new Gravity(1));
//            w.setIntegrator(new VerletIntegrator(3));
//            w.update(diffMilli/1000.0);
//        }
    }

    @Override
    protected void update(KeyFrameTimeSerie timeSerie) {
        final JointTimeSerie serie = (JointTimeSerie) timeSerie;
        anim.setNode(serie.getJoint());
        anim.setTimeSerie(serie);
        anim.setTime(getTime());
        anim.update();
    }

}
