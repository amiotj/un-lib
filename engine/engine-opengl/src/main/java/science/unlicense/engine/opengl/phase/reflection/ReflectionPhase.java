

package science.unlicense.engine.opengl.phase.reflection;

import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Reflection phase
 * 
 * @author Johann Sorel
 */
public class ReflectionPhase extends AbstractFboPhase{

    private final ReflectionVisitor visitor;
    private final RenderPhase baseRenderPhase;
    
    public ReflectionPhase(RenderPhase baseRenderPhase) {
        this.baseRenderPhase = baseRenderPhase;
        visitor = new ReflectionVisitor(baseRenderPhase);
    }
        
    protected void processInternal(GLProcessContext ctx) throws GLException {
        final GLNode root = baseRenderPhase.getRoot();
        root.accept(visitor, ctx);
    }

}