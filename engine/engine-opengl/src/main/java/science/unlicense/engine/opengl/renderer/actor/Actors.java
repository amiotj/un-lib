
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.engine.opengl.mesh.BillBoard;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.mesh.InstancingShell;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.particle.ParticuleShape;
import science.unlicense.engine.opengl.mesh.particle.ParticuleShapeActor;
import science.unlicense.engine.opengl.physic.SkinShell;

/**
 *
 * @author Johann Sorel
 */
public class Actors {
            
    /**
     * Find a shader actor and build an actor for given object.
     * 
     * @return ShaderActor
     */
    public static ActorExecutor buildExecutor(Mesh mesh, Shape shape){
        
        if(shape instanceof SkinShell) return new SkinShellActor(mesh);
        else if(shape instanceof InstancingShell) return new InstancingShellActor(mesh);
        else if(shape instanceof Shell) return new ShellActor(mesh);
        else if(shape instanceof ParticuleShape) return new ParticuleShapeActor((ParticuleShape) shape);
        else if(shape instanceof BillBoard) return new BillBoardShapeActor((BillBoard) shape);
        
        throw new RuntimeException("No executor for : "+shape);
    }
}
