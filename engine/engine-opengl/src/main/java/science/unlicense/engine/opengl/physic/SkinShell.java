
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * A skin holds the relation between a mesh vertices and
 * the skeleton joints.
 *
 * @author Johann Sorel
 */
public class SkinShell extends Shell {
    
    private Skeleton skeleton;
    private int maxWeightPerVertex;

    /**
     * Bone weight for each vertex,
     * size is nbVertex * nbWeightPerVertex
     */
    private VBO weightVBO;

    /**
     * Bones for each vertex,
     * size is nbVertex * nbWeightPerVertex
     */
    private VBO jointVBO;

    /**
     * Deform type for each vertex,
     * 1 float : 0=linear,1=spherical
     * 3 float : spherical rotation center
     */
    private VBO deformVBO;
    
    public SkinShell() {
        super();
    }

    public void setSkeleton(Skeleton skeleton) {
        this.skeleton = skeleton;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void setMaxWeightPerVertex(int maxWeightPerVertex) {
        this.maxWeightPerVertex = maxWeightPerVertex;
    }

    public int getMaxWeightPerVertex() {
        return maxWeightPerVertex;
    }

    /**
     * Bone weight for each vertex,
     * size is nbVertex * nbWeightPerVertex
     */
    public void setWeights(VBO weightBuffer) {
        this.weightVBO = weightBuffer;
    }

    public VBO getWeights() {
        return weightVBO;
    }

    /**
     * Bones for each vertex,
     * size is nbVertex * nbWeightPerVertex
     */
    public void setJointIndexes(VBO jointBuffer) {
        this.jointVBO = jointBuffer;
    }

    public VBO getJointIndexes() {
        return jointVBO;
    }

    /**
     * Deform type for each vertex,
     * 1 float : 0=linear,1=spherical
     * 3 float : spherical rotation center
     * 
     * if null, it is considered linear
     */
    public void setDeformVBO(VBO deformVBO) {
        this.deformVBO = deformVBO;
    }

    public VBO getDeformVBO() {
        return deformVBO;
    }
    
    public SkinShell copy() {
        final SkinShell copy = new SkinShell();
        copy.vertices = vertices;
        copy.normals = normals;
        copy.tangents = tangents;
        copy.uvs = uvs;
        copy.indexes = indexes;
        copy.ranges = ranges;
        copy.skeleton = skeleton;
        copy.maxWeightPerVertex = maxWeightPerVertex;
        copy.weightVBO = weightVBO;
        copy.jointVBO = jointVBO;
        copy.deformVBO = deformVBO;
        copy.morphs = morphs;
        return copy;
    }
    
    public boolean isDirty() {
        return super.isDirty()
            || (weightVBO!=null && weightVBO.isDirty())
            || (jointVBO!=null && jointVBO.isDirty())
            || (deformVBO!=null && deformVBO.isDirty());
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        if(weightVBO!=null)  weightVBO.unloadFromGpuMemory(context.getGL());
        if(jointVBO!=null)   jointVBO.unloadFromGpuMemory(context.getGL());
        if(deformVBO!=null)  deformVBO.unloadFromGpuMemory(context.getGL());
    }

}
