
package science.unlicense.engine.opengl.phase;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.GLStatistics;
import science.unlicense.impl.gpu.opengl.GLException;

/**
 * An abstract phase, handles events and enable state.
 *
 * @author Johann Sorel
 */
public abstract class AbstractPhase extends AbstractEventSource implements Phase {

    private boolean enable = true;
    private Chars id = Chars.EMPTY;

    public AbstractPhase() {
    }
    
    public AbstractPhase(Chars id) {
        CObjects.ensureNotNull(id);
        this.id = id;
    }

    public Chars getId() {
        return id;
    }

    public void setId(Chars id) {
        CObjects.ensureNotNull(id);
        this.id = id;
    }
    
    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        if(this.enable==enable) return;
        this.enable = enable;
        sendPropertyEvent(this, PROPERTY_ENABLE, !enable, enable);
    }

    public final void process(GLProcessContext context) throws GLException {
        final GLStatistics stats = context.getStatistics();
        if(!id.isEmpty()){
            stats.startLog(this, id);
        }else{
            stats.startLog(this, null);
        }
        processInt(context);
        stats.endLog();
    }

    protected void processInt(GLProcessContext context) throws GLException {
        
    }

    public void dispose(GLProcessContext ctx) {
        
    }
    
}
