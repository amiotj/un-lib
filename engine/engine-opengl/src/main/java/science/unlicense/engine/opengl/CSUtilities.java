
package science.unlicense.engine.opengl;

import science.unlicense.api.anim.Animation;
import science.unlicense.api.anim.CompoundAnimation;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.OrderedSet;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.CoordinateSystems;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.physic.constraint.AngleLimitConstraint;
import science.unlicense.api.physic.constraint.Constraint;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.api.model.tree.Node;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.physic.RelativeSkeletonPose;
import science.unlicense.engine.opengl.physic.SkeletonAnimation;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.api.geometry.BBox;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Quaternion;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.engine.opengl.physic.JointKeyFrame;
import science.unlicense.engine.opengl.physic.JointTimeSerie;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.api.math.Affine;

/**
 * Coordinate System utilities.
 *
 * @author Johann Sorel
 */
public final class CSUtilities {

    private static final Matrix3x3 INVERT_HAND = new Matrix3x3(
            1,  0,  0,
            0,  1,  0,
            0,  0, -1);

    private CSUtilities() {}

    /**
     * Invert coordinates : left hand CS <> right hand CS.
     * Z value is inverted.
     *
     * @param vbo : expect a float buffer, tuple size 3
     */
    public static void invertHandCS(VBO vbo){
        if(vbo==null) return;

        final FloatCursor fb = vbo.getPrimitiveBuffer().cursorFloat();
        for(int i=2,n=(int) fb.getBuffer().getPrimitiveCount();i<n;i+=3){
            fb.write(-fb.read(i), i);
        }
    }
    
    /**
     * Invert index : left hand CS <> right hand CS.
     * triangles clockwise direction are inverted.
     *
     * @param ibo : expect an int buffer, tuple size 3
     */
    public static void invertHandCS(IBO ibo, IndexRange[] ranges){
        if(ibo==null) return;

        for(int k=0;k<ranges.length;k++){
            final IndexRange range = ranges[k];
            final int mode = range.getMode();
            final int nb = range.getCount();
            final int ofs = range.getIndexOffset();
            
            final IntCursor ib = ibo.getPrimitiveBuffer().cursorInt();
            ib.setPosition(ofs);
            
            
            if(mode==GL_POINTS || mode==GL_LINES || mode==GL_LINE_LOOP 
              || mode==GL_LINE_STRIP || mode==GL_LINE_STRIP_ADJACENCY 
              || mode==GL_LINES_ADJACENCY){
                //nothing to do
            }else if(mode==GL_TRIANGLES){
                for(int i=0;i<nb;i+=3){
                    int v1 = ib.read(ofs+i+1);
                    int v2 = ib.read(ofs+i+2);
                    ib.write(v2, ofs+i+1);
                    ib.write(v1, ofs+i+2);
                }
            }else if(mode==GL_TRIANGLE_STRIP){
                throw new InvalidArgumentException("Triangle strip inversion not supported yet.");
            }else if(mode==GL_TRIANGLE_FAN){
                throw new InvalidArgumentException("Triangle fan inversion not supported yet.");
            }else if(mode==GL_TRIANGLE_STRIP_ADJACENCY){
                throw new InvalidArgumentException("Triangle strip adjency inversion not supported yet.");
            }else if(mode==GL_TRIANGLES_ADJACENCY){
                for(int i=0;i<nb;i+=6){
                    int v0 = ib.read(ofs+i+0);
                    int v1 = ib.read(ofs+i+1);
                    int v2 = ib.read(ofs+i+2);
                    int v3 = ib.read(ofs+i+3);
                    int v4 = ib.read(ofs+i+4);
                    int v5 = ib.read(ofs+i+5);
                    ib.write(v0, ofs+i+0);
                    ib.write(v5, ofs+i+1);
                    ib.write(v4, ofs+i+2);
                    ib.write(v3, ofs+i+3);
                    ib.write(v2, ofs+i+4);
                    ib.write(v1, ofs+i+5);
                }
            }else if(mode==GL_PATCHES){
                throw new InvalidArgumentException("Patche inversion not supported yet.");
            }
        }
        
    }

    public static void invertHandCS(TupleRW tuple){
        if(tuple==null) return;
        tuple.setZ(-tuple.getZ());
    }

    public static void invertHandCS(Quaternion q){
        if(q==null) return;
        final Matrix3x3 m = q.toMatrix3();
        invertHandCS(m);
        q.fromMatrix(m);
    }

    public static void invertHandCS(MatrixRW matrix){
        if(matrix==null) return;
        if(matrix.getNbCol()==3 && matrix.getNbRow()==3){
            matrix.set(0, 2, -matrix.get(0, 2));
            matrix.set(1, 2, -matrix.get(1, 2));
            matrix.set(2, 0, -matrix.get(2, 0));
            matrix.set(2, 1, -matrix.get(2, 1));
        }else if(matrix.getNbCol()==4 && matrix.getNbRow()==4){
            matrix.set(0, 2, -matrix.get(0, 2));
            matrix.set(1, 2, -matrix.get(1, 2));
            matrix.set(3, 2, -matrix.get(3, 2));
            matrix.set(2, 0, -matrix.get(2, 0));
            matrix.set(2, 1, -matrix.get(2, 1));
            matrix.set(2, 3, -matrix.get(2, 3));
        }else{
            throw new RuntimeException("Invalid matrix, must be 3x3 or 4x4");
        }
    }

    public static void invertHandCS(NodeTransform q){
        if(q==null) return;
        invertHandCS(q.getTranslation());
        invertHandCS(q.getRotation());
        q.notifyChanged();
    }
    
    public static void invertHandCS(MorphTarget mf){
        if(mf==null) return;
        invertHandCS(mf.getVertices());
    }
    
    public static void invertHandCS(MorphSet ms){
        if(ms==null) return;
        for(int i=0,n=ms.getMorphs().getSize();i<n;i++){
            invertHandCS((MorphTarget)ms.getMorphs().get(i));
        }
    }
    
    public static void invertHandCS(Shell shell){
        if(shell==null) return;
        invertHandCS(shell.getVertices());
        invertHandCS(shell.getNormals());
        invertHandCS(shell.getIndexes(),shell.getModes());
        invertHandCS(shell.getTangents());
        invertHandCS(shell.getMorphs());
        
        //recalculate shell bbox
        shell.calculateBBox();
    }

    public static void invertHandCS(SceneNode node){
        final NodeTransform trs = node.getNodeTransform();
        invertHandCS(trs.getTranslation());
        invertHandCS(trs.getRotation());
        trs.notifyChanged();
        
        if(node instanceof Joint){
            final Joint joint = (Joint) node;
            final Sequence seq = joint.getConstraints();
            for(int i=0,n=seq.getSize();i<n;i++){
                final Constraint cst = (Constraint) seq.get(i);
                if(cst instanceof AngleLimitConstraint){
                    final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
                    final BBox limit = alc.getLimits();
                    
                    //the z axis direction change, so it's the angle around X which must be fixed
                    double min = -limit.getMin(0);
                    double max = -limit.getMax(0);
                    limit.setRange(0, Math.min(min, max), Math.max(min, max));
                }
            }
        }else if(node instanceof RigidBody){
        }

        //loop on children
        for(Iterator ite=node.getChildren().createIterator();ite.hasNext();){
            final Object next = ite.next();
            if(next instanceof SceneNode){
                invertHandCS((SceneNode)next);
            }
        }
    }

    public static void invertHandCS(Skeleton skeleton){
        final Sequence roots = skeleton.getChildren();
        for(int i=0,n=roots.getSize();i<n;i++){
            invertHandCS((SceneNode) roots.get(i));
        }
    }

    public static void invertHandCS(RelativeSkeletonPose skeleton){
        final Sequence poses = skeleton.getJointPoses();
        for(int i=0,n=poses.getSize();i<n;i++){
            final JointKeyFrame jp = (JointKeyFrame) poses.get(i);
            invertHandCS(jp.getValue());
        }
    }

    public static void invertHandCS(SkeletonAnimation animation){
        //TODO
    }
    
    public static void invertHandCS(Mesh mesh){
        invertHandCS((Node)mesh);
    }
    
    public static void invertHandCS(MultipartMesh pmesh){
        invertHandCS((Node)pmesh);
    }

    private static void invertHandCS(Node node){
        if(node instanceof Mesh){
            final Shell shell = (Shell) ((Mesh)node).getShape();
            invertHandCS(shell);
        }else if(node instanceof MultipartMesh){
            final Skeleton skeleton = ((MultipartMesh)node).getSkeleton();
            if(skeleton!=null){
                invertHandCS(skeleton);
            }
        }        
        
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            invertHandCS((Node)ite.next());
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    public static void transform(VBO vbo, Transform trs){
        if(vbo==null) return;

        final Buffer fb = vbo.getPrimitiveBuffer();
        float[] array = fb.toFloatArray();
        array = trs.transform(array, 0, new float[array.length],0, array.length/vbo.getSampleCount());
        fb.writeFloat(array,0);
    }
    
    public static void transform(IBO ibo, IndexRange[] ranges, Transform trs){
        if(ibo==null) return;

//        for(int k=0;k<ranges.length;k++){
//            final IBO.Range range = ranges[k];
//            final int mode = range.getMode();
//            final int nb = range.getCount();
//            final int ofs = range.getIndexOffset();
//            
//            final IntBuffer ib = ibo.getBuffer();
//            ib.position(ofs);
//            
//            
//            if(mode==GL.GL_POINTS || mode==GL.GL_LINES || mode==GL.GL_LINE_LOOP 
//              || mode==GL.GL_LINE_STRIP || mode==GL3.GL_LINE_STRIP_ADJACENCY 
//              || mode==GL3.GL_LINES_ADJACENCY){
//                //nothing to do
//            }else if(mode==GL.GL_TRIANGLES){
//                for(int i=0;i<nb;i+=3){
//                    int v1 = ib.get(ofs+i+1);
//                    int v2 = ib.get(ofs+i+2);
//                    ib.put(ofs+i+1, v2);
//                    ib.put(ofs+i+2, v1);
//                }
//            }else if(mode==GL.GL_TRIANGLE_STRIP){
//                throw new InvalidArgumentException("Triangle strip inversion not supported yet.");
//            }else if(mode==GL.GL_TRIANGLE_FAN){
//                throw new InvalidArgumentException("Triangle fan inversion not supported yet.");
//            }else if(mode==GL3.GL_TRIANGLE_STRIP_ADJACENCY){
//                throw new InvalidArgumentException("Triangle strip adjency inversion not supported yet.");
//            }else if(mode==GL3.GL_TRIANGLES_ADJACENCY){
//                for(int i=0;i<nb;i+=6){
//                    int v0 = ib.get(ofs+i+0);
//                    int v1 = ib.get(ofs+i+1);
//                    int v2 = ib.get(ofs+i+2);
//                    int v3 = ib.get(ofs+i+3);
//                    int v4 = ib.get(ofs+i+4);
//                    int v5 = ib.get(ofs+i+5);
//                    ib.put(ofs+i+0, v0);
//                    ib.put(ofs+i+1, v5);
//                    ib.put(ofs+i+2, v4);
//                    ib.put(ofs+i+3, v3);
//                    ib.put(ofs+i+4, v2);
//                    ib.put(ofs+i+5, v1);
//                }
//            }else if(mode==GL3.GL_PATCHES){
//                throw new InvalidArgumentException("Patche inversion not supported yet.");
//            }
//        }
        
    }

    public static void transform(TupleRW tuple, Transform trs){
        if(tuple==null) return;
        trs.transform(tuple, tuple);
    }

    public static void transform(Quaternion q, Transform trs){
        if(q==null) return;
        final Matrix3x3 m = q.toMatrix3();
        transform(m,trs);
        q.fromMatrix(m);
    }

    public static void transform(Matrix3x3 matrix, Transform trs){
        if(matrix==null) return;
        final Matrix m2 = ((Affine)trs).toMatrix();
        matrix.set(m2.multiply(matrix));
    }

    public static void transform(Matrix4x4 matrix, Transform trs){
        if(matrix==null) return;
        final Matrix m2 = ((Affine)trs).toMatrix();
        matrix.set(m2.multiply(matrix));
    }

    public static void transform(MatrixRW matrix, Transform trs){
        if(matrix==null) return;
        final Matrix m2 = ((Affine)trs).toMatrix();
        matrix.set(m2.multiply(matrix));
    }

    public static void transform(NodeTransform q, Transform trs){
        if(q==null) return;
        final MatrixRW m = q.asMatrix().copy();
        transform(m, trs);
        q.set(m);
    }
    
    public static void transform(MorphTarget mf, Transform trs){
        if(mf==null) return;
        transform(mf.getVertices(), trs);
    }
    
    public static void transform(MorphSet ms, Transform trs){
        if(ms==null) return;
        for(int i=0,n=ms.getMorphs().getSize();i<n;i++){
            transform((MorphTarget)ms.getMorphs().get(i), trs);
        }
    }
    
    public static void transform(Shell shell, Transform trs){
        if(shell==null) return;
        transform(shell.getVertices(), trs);
        transform(shell.getNormals(), trs);
        transform(shell.getIndexes(),shell.getModes(), trs);
        transform(shell.getTangents(), trs);
        transform(shell.getMorphs(), trs);
        
        //recalculate shell bbox
        shell.calculateBBox();
    }
    
    public static void transform(SceneNode node, Transform cstrs){
        final NodeTransform trs = node.getNodeTransform();
        transform(trs, cstrs);
        
//        if(node instanceof Joint){
//            final Joint joint = (Joint) node;
//            final Sequence seq = joint.getConstraints();
//            for(int i=0,n=seq.getSize();i<n;i++){
//                final Constraint cst = (Constraint) seq.get(i);
//                if(cst instanceof AngleLimitConstraint){
//                    final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
//                    final BBox limit = alc.getLimits();
//                    
//                    //the z axis direction change, so it's the angle around X which must be fixed
//                    double min = -limit.getMin(0);
//                    double max = -limit.getMax(0);
//                    limit.setRange(0, Math.min(min, max), Math.max(min, max));
//                }
//            }
//        }else if(node instanceof RigidBody){
//        }

        //loop on children
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            Object next = ite.next();
            if(next instanceof SceneNode){
                transform((SceneNode)next, cstrs);
            }
        }
    }

    public static void transform(Skeleton skeleton, Transform trs){
        
        //TODO still not good, but better
        skeleton.resetToWorldPose();
        
        final Sequence joints = skeleton.getAllJoints();
        for(int i=0,n=joints.getSize();i<n;i++){
            transform( ((SceneNode)joints.get(i)).getNodeTransform(), trs);
        }
        
        skeleton.reverseWorldPose();
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();
    }

    public static void transform(RelativeSkeletonPose skeleton, Transform trs){
        final Sequence poses = skeleton.getJointPoses();
        for(int i=0,n=poses.getSize();i<n;i++){
            final JointKeyFrame jp = (JointKeyFrame) poses.get(i);
            transform(jp.getValue(), trs);
        }
    }

    public static void transform(JointKeyFrame pose, Transform trs){
        transform(pose.getValue(), trs);
    }
    
    public static void transform(JointTimeSerie serie, Transform trs){
        final OrderedSet frames = serie.getFrames();
        final Iterator ite = frames.createIterator();
        while(ite.hasNext()){
            final JointKeyFrame frame = (JointKeyFrame) ite.next();
            transform(frame, trs);
        }
    }
    
    public static void transform(SkeletonAnimation animation, Transform trs){
        final Sequence series = animation.getSeries();
        for(int i=0,n=series.getSize();i<n;i++){
            final JointTimeSerie serie = (JointTimeSerie) series.get(i);
            transform(serie, trs);
        }
    }
    
    public static void transform(Animation animation, Transform trs){
        if(animation instanceof SkeletonAnimation){
            transform((SkeletonAnimation)animation, trs);
        }else if(animation instanceof CompoundAnimation){
            final CompoundAnimation ca = (CompoundAnimation) animation;
            final Sequence elements = ca.getElements();
            for(int i=0,n=elements.getSize();i<n;i++){
                transform((Animation)elements.get(i), trs);
            }
        }
    }
    
    public static void transform(Mesh mesh, Transform trs){
        transform((Node)mesh, trs);
    }
    
    public static void transform(MultipartMesh pmesh, Transform trs){
        transform((Node)pmesh, trs);
    }

    public static void transform(Node node, Transform trs){
        if(node instanceof Mesh){
            final Shell shell = (Shell) ((Mesh)node).getShape();
            transform(shell, trs);
        }else if(node instanceof MultipartMesh){
            final Skeleton skeleton = ((MultipartMesh)node).getSkeleton();
            if(skeleton!=null){
                transform(skeleton, trs);
            }
        }        
        
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            transform((Node)ite.next(), trs);
        }
    }
    
    public static void transform(Node node, CoordinateSystem target){
        if(node instanceof GLNode){
            final GLNode glnode = (GLNode) node;
            final CoordinateSystem nodeCs = glnode.getCoordinateSystem();
            if(!nodeCs.equals(target)){
                final Transform trs = CoordinateSystems.createTransform(nodeCs, target);                
//                transform(glnode.getNodeTransform(), trs);
                
                if(node instanceof Mesh){
                    final Shell shell = (Shell) ((Mesh)node).getShape();
                    transform(shell, trs);
                }else if(node instanceof MultipartMesh){
                    final Skeleton skeleton = ((MultipartMesh)node).getSkeleton();
                    if(skeleton!=null){
                        transform(skeleton, trs);
                    }
                }     
                glnode.setLocalCoordinateSystem(target);
            }
        }
        
        for (Iterator ite=node.getChildren().createIterator();ite.hasNext();) {
            transform((Node)ite.next(), target);
        }
    }
    
}
