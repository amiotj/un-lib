

package science.unlicense.engine.opengl.renderer;

import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.math.Affine;
import science.unlicense.impl.geometry.s3d.Grid;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 * Display a Grid as red cube where voxel is fill.
 * 
 * @author Johann Sorel
 */
public class DebugGridNode extends GLNode{
    
    private final Grid grid;

    public DebugGridNode(Grid grid, Extent ext) {
        this.grid = grid;
        
        final TupleBuffer gridSm = grid.getImage().getRawModel().asTupleBuffer(grid.getImage());
        final Affine geomToGrid = grid.getGeomToGrid();
        final Affine gridToGeom = geomToGrid.invert();
        
        final BBox voxel = new BBox(
                new double[]{0, 0, 0},
                new double[]{+1, +1, +1});
        final Mesh model = new GeometryMesh(voxel);

        
        final boolean[] sample = new boolean[1];
        final int[] coord = new int[3];
        final GLNode cells = new GLNode();
        cells.getNodeTransform().set(gridToGeom);
        for(int nx=(int)ext.get(0);coord[0]<nx;coord[0]+=1){
            coord[1]=0;
            for(int ny=(int)ext.get(1);coord[1]<ny;coord[1]+=1){
                coord[2]=0;
                for(int nz=(int) ext.get(2);coord[2]<nz;coord[2]+=1){
                    gridSm.getTupleBoolean(coord, sample);
                    if(sample[0]){
                        final Mesh cell = new Mesh();
                        cell.setShape(model.getShape());
                        cell.getMaterial().setDiffuse(sample[0]?Color.RED:Color.BLUE);
                        cell.getNodeTransform().getTranslation().setXYZ(coord[0],coord[1],coord[2]);
                        cell.getNodeTransform().notifyChanged();
                        ((MeshRenderer)cell.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
                        cells.getChildren().add(cell);
                    }
                }
            }
        }
        children.add(cells);
    }
    
}
