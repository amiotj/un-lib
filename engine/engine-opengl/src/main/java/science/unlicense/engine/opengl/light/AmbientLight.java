
package science.unlicense.engine.opengl.light;

import science.unlicense.api.character.Chars;
import science.unlicense.api.color.Color;

/**
 * Ambient light.
 * 
 * Colladata 1.5 : An ambient light is one that lights everything evenly, 
 * regardless of location or orientation.
 * 
 * @author Johann Sorel
 */
public class AmbientLight extends Light{

    public AmbientLight() {
        this(Color.WHITE,Color.WHITE);
    }
    
    public AmbientLight(Color diffuse, Color specular) {
        super(diffuse,specular);
        setName(new Chars("Ambiant light"));
    }
    
}
