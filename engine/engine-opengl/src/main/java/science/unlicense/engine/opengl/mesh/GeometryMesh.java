

package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.buffer.IntCursor;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.geometry.Geometries;
import science.unlicense.impl.geometry.s2d.Segment;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Capsule;
import science.unlicense.impl.geometry.s3d.Cone;
import science.unlicense.impl.geometry.s3d.Cylinder;
import science.unlicense.impl.geometry.s3d.Disk;
import science.unlicense.impl.geometry.s3d.Ellipsoid;
import science.unlicense.impl.geometry.s3d.Quad;
import science.unlicense.impl.geometry.s3d.Sphere;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.gpu.GLBufferFactory;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 * Encapsulate a 3D Geometry and displays it as a gl mesh.
 *
 * @author Johann Sorel
 */
public class GeometryMesh extends Mesh {

    private Geometry geometry;
    private int nbRing;
    private int nbSector;

    public GeometryMesh(Geometry geometry) {
        this(geometry,10,10);
    }

    public GeometryMesh(Geometry geometry, int nbSector) {
        this(geometry,nbSector,nbSector);
    }

    public GeometryMesh(Geometry geometry, int nbRing, int nbSector) {
        this.geometry = geometry;
        this.nbRing = nbRing;
        this.nbSector = nbSector;
        buildShell();
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void updateGeometry(Geometry geom){
        this.geometry = geom;
        buildShell();
    }

    public int getNbRing() {
        return nbRing;
    }

    public void setNbRing(int nbRing) {
        this.nbRing = nbRing;
    }

    public int getNbSector() {
        return nbSector;
    }

    public void setNbSector(int nbSector) {
        this.nbSector = nbSector;
    }

    public Shell getShape() {
        return (Shell) super.getShape();
    }
    
    /**
     * Update the vertices and normals.
     * Rebuild them from the geometry.
     * Call this method after making changes on the geometry.
     */
    public void buildShell(){
        if(geometry instanceof BBox){
            buildShell((BBox) geometry);
        }else if(geometry instanceof Disk){
            buildShell((Disk) geometry);
        }else if(geometry instanceof Sphere){
            buildShell((Sphere) geometry);
        }else if(geometry instanceof Capsule){
            buildShell((Capsule) geometry);
        }else if(geometry instanceof Ellipsoid){
            buildShell((Ellipsoid) geometry);
        }else if(geometry instanceof Cylinder){
            buildShell((Cylinder) geometry);
        }else if(geometry instanceof Cone){
            buildShell((Cone) geometry);
        }else if(geometry instanceof Quad){
            buildShell((Quad) geometry);
        }else if(geometry instanceof science.unlicense.impl.geometry.s2d.Triangle){
            buildShell((science.unlicense.impl.geometry.s2d.Triangle) geometry);
        }else if(geometry instanceof Segment){
            buildShell((Segment) geometry);
        }else{
            throw new RuntimeException("Unexpected geometry type : "+ geometry);
        }
    }

    private void buildShell(BBox bbox){
        
        final Tuple center = bbox.getMiddle();
        final float lengthX = (float) bbox.getSpan(0);
        final float lengthY = (float) bbox.getSpan(1);
        final float lengthZ = (float) bbox.getSpan(2);
        
        final Buffer vertexBuffer = GLBufferFactory.INSTANCE.createFloat(24*3);
        final Buffer normalBuffer = GLBufferFactory.INSTANCE.createFloat(24*3);
        final Buffer indexBuffer = GLBufferFactory.INSTANCE.createInt(36);
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertexBuffer,3));
        shell.setNormals(new VBO(normalBuffer,3));
        shell.setIndexes(new IBO(indexBuffer,2),IndexRange.TRIANGLES(0, 36));
        setShape(shell);

        final float cx = (float) center.get(0);
        final float cy = (float) center.get(1);
        final float cz = (float) center.get(2);
        final float halfx = lengthX/2;
        final float halfy = lengthY/2;
        final float halfz = lengthZ/2;

        final float[][] vertices = new float[8][3];
        vertices[0] = new float[]{cx-halfx, cy-halfy, cz-halfz};
        vertices[1] = new float[]{cx+halfx, cy-halfy, cz-halfz};
        vertices[2] = new float[]{cx+halfx, cy+halfy, cz-halfz};
        vertices[3] = new float[]{cx-halfx, cy+halfy, cz-halfz};
        vertices[4] = new float[]{cx+halfx, cy-halfy, cz+halfz};
        vertices[5] = new float[]{cx-halfx, cy-halfy, cz+halfz};
        vertices[6] = new float[]{cx+halfx, cy+halfy, cz+halfz};
        vertices[7] = new float[]{cx-halfx, cy+halfy, cz+halfz};

        final float[] normals = new float[3];

        final FloatCursor vertexCursor = vertexBuffer.cursorFloat();
        final FloatCursor normalCursor = normalBuffer.cursorFloat();
        //back
        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[1]);
        vertexCursor.write(vertices[2]);
        vertexCursor.write(vertices[3]);
        normals[0]=0; normals[1]=0; normals[2]=-1;
        for(int i=0;i<4;i++) normalCursor.write(normals);

        //front
        vertexCursor.write(vertices[4]);
        vertexCursor.write(vertices[5]);
        vertexCursor.write(vertices[7]);
        vertexCursor.write(vertices[6]);
        normals[0]=0; normals[1]=0; normals[2]=1;
        for(int i=0;i<4;i++) normalCursor.write(normals);

        //rigth
        vertexCursor.write(vertices[1]);
        vertexCursor.write(vertices[4]);
        vertexCursor.write(vertices[6]);
        vertexCursor.write(vertices[2]);
        normals[0]=1; normals[1]=0; normals[2]=0;
        for(int i=0;i<4;i++) normalCursor.write(normals);

        //left
        vertexCursor.write(vertices[5]);
        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[3]);
        vertexCursor.write(vertices[7]);
        normals[0]=-1; normals[1]=0; normals[2]=0;
        for(int i=0;i<4;i++) normalCursor.write(normals);

        //up
        vertexCursor.write(vertices[2]);
        vertexCursor.write(vertices[6]);
        vertexCursor.write(vertices[7]);
        vertexCursor.write(vertices[3]);
        normals[0]=0; normals[1]=1; normals[2]=0;
        for(int i=0;i<4;i++) normalCursor.write(normals);

        //down
        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[5]);
        vertexCursor.write(vertices[4]);
        vertexCursor.write(vertices[1]);
        normals[0]=0; normals[1]=-1; normals[2]=0;
        for(int i=0;i<4;i++) normalCursor.write(normals);


        final int[] indices = {
            2, 1, 0, //back
            3, 2, 0,
            6, 5, 4, //front
            7, 6, 4,
            10, 9, 8,
            11, 10, 8,
            14, 13, 12,
            15, 14, 12,
            18, 17, 16,
            19, 18, 16,
            22, 21, 20,
            23, 22, 20 };
        indexBuffer.writeInt(indices,0);
    }
    
    private void buildShell(Disk disk){
        final float hAngle = 1f / (nbSector);
        final float radius = (float) disk.getRadius();

        //build a triangle fan
        final int nbVertice = (2+nbSector)*3;
        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt(2+nbSector);

        int index = 0;
        
        final FloatCursor vertexCursor = vertices.cursorFloat();
        final FloatCursor normalCursor = normals.cursorFloat();
        final IntCursor indiceCursor = indices.cursorInt();

        //center point
        vertexCursor.write(0).write(0).write(0);
        indiceCursor.write(index++);

        for(int s=0; s<nbSector+1; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;

            //first point : top left
            final float[] tl = point(half,leftha,radius, 0);
            vertexCursor.write(tl);
            normalCursor.write(tl);
            indiceCursor.write(index++);
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3), IndexRange.TRIANGLE_FAN(0, (int) indices.getPrimitiveCount()));
        setShape(shell);
    }

    private void buildShell(Sphere sphere){
        final float radius = (float) sphere.getRadius();
        final float vAngle = 1f / (nbRing-1);
        final float hAngle = 1f / (nbSector-1);

        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(nbRing*nbSector*3);
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(nbRing*nbSector*3);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt((nbRing-1)*(nbSector-1)*6);

        final FloatCursor vertexCursor = vertices.cursorFloat();
        final FloatCursor normalCursor = normals.cursorFloat();
        final IntCursor indiceCursor = indices.cursorInt();
        
        final double[] center = sphere.getCenter().getValues();
        for(int v = 0; v < nbRing; v++) {
            final double va = Math.PI * v * vAngle;
            for(int h = 0; h < nbSector; h++) {
                final double ha = 2*Math.PI * h * hAngle;
                final float y = (float) Math.sin(- Math.PI/2d + va);
                final float x = (float) (Math.cos(ha) * Math.sin(va));
                final float z = (float) (Math.sin(ha) * Math.sin(va));
                vertexCursor.write(x*radius+(float)center[0]);
                vertexCursor.write(y*radius+(float)center[1]);
                vertexCursor.write(z*radius+(float)center[2]);
                normalCursor.write(x);
                normalCursor.write(y);
                normalCursor.write(z);

                if(v!=nbRing-1 && h!=nbSector-1){
                    //last ring, we don't create triangles
                    //first triangle
                    indiceCursor.write( v * nbSector + h );
                    indiceCursor.write( (v+1) * nbSector + h );
                    indiceCursor.write( v * nbSector + (h+1) );
                    //second triangle
                    indiceCursor.write( v * nbSector + (h+1) );
                    indiceCursor.write( (v+1) * nbSector + h );
                    indiceCursor.write( (v+1) * nbSector + (h+1) );
                }
            }
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3), IndexRange.TRIANGLES(0, (int) indices.getPrimitiveCount()));
        setShape(shell);
    }

    private void buildShell(Capsule capsule){
        final float height = (float)capsule.getHeight();
        final float radius = (float)capsule.getRadius();
        final float upper = height/2;
        final float lower = -height/2;
        final float vAngle = 1f / (nbRing);
        final float hAngle = 1f / (nbSector);

        final int nbVertice = nbRing*nbSector*3*4
                            + nbSector*3*4
                            + nbRing*nbSector*3*4;
        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt(
                nbRing*nbSector*6 //top half-sphere
               +nbSector*6 //cylinder
               +nbRing*nbSector*6 //bottom half sphere
        );
        
        final FloatCursor vertexCursor = vertices.cursorFloat();
        final FloatCursor normalCursor = normals.cursorFloat();
        final IntCursor indiceCursor = indices.cursorInt();

        //TODO we duplicate many points here

        int index = 0;
        final float[] tl = new float[3];
        final float[] tr = new float[3];
        final float[] bl = new float[3];
        final float[] br = new float[3];
        final float[] tln = new float[3];
        final float[] trn = new float[3];
        final float[] bln = new float[3];
        final float[] brn = new float[3];

        //TOP half-sphere
        for(int v = 0; v < nbRing; v++) {
            final double topva = Math.PI/2 * v * vAngle;
            final double bottomva = Math.PI/2 * (v+1) * vAngle;

            for(int s = 0; s < nbSector; s++) {
                final double leftha = 2*Math.PI * s * hAngle;
                final double righttha = 2*Math.PI * (s+1) * hAngle;

                point(topva,leftha,radius, upper, tl, tln);
                point(topva,righttha,radius, upper, tr, trn);
                point(bottomva,leftha,radius, upper, bl, bln);
                point(bottomva,righttha,radius, upper, br, brn);

                vertexCursor.write(tl).write(tr).write(bl).write(br);
                normalCursor.write(tln).write(trn).write(bln).write(brn);

                int itl = index++;
                int itr = index++;
                int ibl = index++;
                int ibr = index++;

                indiceCursor.write(itl).write(itr).write(ibl);
                indiceCursor.write(itr).write(ibr).write(ibl);
            }
        }

        //Cylinder
        for(int s = 0; s < nbSector; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;
            final double righttha = 2*Math.PI * (s+1) * hAngle;

            point(half,leftha,radius, upper, tl, tln);
            point(half,righttha,radius, upper, tr, trn);
            point(half,leftha,radius, lower, bl, bln);
            point(half,righttha,radius, lower, br, brn);

            vertexCursor.write(tl).write(tr).write(bl).write(br);
            normalCursor.write(tln).write(trn).write(bln).write(brn);

            int itl = index++;
            int itr = index++;
            int ibl = index++;
            int ibr = index++;

            indiceCursor.write(itl).write(itr).write(ibl);
            indiceCursor.write(itr).write(ibr).write(ibl);
        }

        //Bottom half-sphere
        for(int v = 0; v < nbRing; v++) {
            double topva = Math.PI/2 * v * vAngle + Math.PI/2;
            double bottomva = Math.PI/2 * (v+1) * vAngle + Math.PI/2;

            for(int s = 0; s < nbSector; s++) {
                final double leftha = 2*Math.PI * s * hAngle;
                final double righttha = 2*Math.PI * (s+1) * hAngle;

                point(topva,leftha,radius, lower, tl, tln);
                point(topva,righttha,radius, lower, tr, trn);
                point(bottomva,leftha,radius, lower, bl, bln);
                point(bottomva,righttha,radius, lower, br, brn);

                vertexCursor.write(tl).write(tr).write(bl).write(br);
                normalCursor.write(tln).write(trn).write(bln).write(brn);
                
                int itl = index++;
                int itr = index++;
                int ibl = index++;
                int ibr = index++;

                indiceCursor.write(itl).write(itr).write(ibl);
                indiceCursor.write(itr).write(ibr).write(ibl);
            }
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3), IndexRange.TRIANGLES(0, (int) indices.getPrimitiveCount()));
        setShape(shell);
    }

    private void buildShell(final Ellipsoid ellipsoid){
        final float radiusX = (float) ellipsoid.getRadiusX();
        final float radiusY = (float) ellipsoid.getRadiusY();
        final float radiusZ = (float) ellipsoid.getRadiusZ();
        final float vAngle = 1f / (nbRing-1);
        final float hAngle = 1f / (nbSector-1);

        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(nbRing*nbSector*3);
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(nbRing*nbSector*3);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt(nbRing*nbSector*6);
        
        final FloatCursor vertexCursor = vertices.cursorFloat();
        final FloatCursor normalCursor = normals.cursorFloat();
        final IntCursor indiceCursor = indices.cursorInt();

        for(int v = 0; v < nbRing; v++) {
            final double va = - Math.PI/2d + Math.PI * v * vAngle;
            for(int h = 0; h < nbSector; h++) {
                final double ha = 2*Math.PI * h * hAngle;
                final float x = (float) (Math.cos(va) * Math.sin(ha));
                final float y = (float) (Math.sin(va) * Math.sin(ha));
                final float z = (float) (Math.cos(ha));
                vertexCursor.write(x*radiusX);
                vertexCursor.write(y*radiusY);
                vertexCursor.write(z*radiusZ);
                normalCursor.write(x);
                normalCursor.write(y);
                normalCursor.write(z);

                //first triangle
                indiceCursor.write( v * nbSector + h );
                indiceCursor.write( v * nbSector + (h+1) );
                indiceCursor.write( (v+1) * nbSector + h );
                //second triangle
                indiceCursor.write( v * nbSector + (h+1) );
                indiceCursor.write( (v+1) * nbSector + h );
                indiceCursor.write( (v+1) * nbSector + (h+1) );
            }
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3), IndexRange.TRIANGLES(0, (int) indices.getPrimitiveCount()));
        setShape(shell);
    }

    private void buildShell(final Cylinder cylinder){
        final float height = (float)cylinder.getHeight();
        final float radius = (float)cylinder.getRadius();
        final float upper = height/2;
        final float lower = -height/2;
        final float hAngle = 1f / (nbSector);

        final int fannb = 2+nbSector;
        final int cylindernb = nbSector*6;
        final int nbVertice = (2+nbSector)*3 //top disk
                            + nbSector*3*4 //cylinder
                            + (2+nbSector)*3; //bottom disk
        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt(
                fannb //top disk
               +nbSector*6 //cylinder
               +fannb //bottom disk
        );
        
        final FloatCursor vertexCursor = vertices.cursorFloat();
        final FloatCursor normalCursor = normals.cursorFloat();
        final IntCursor indiceCursor = indices.cursorInt();

        int index = 0;

        //top disk
        vertexCursor.write(0).write(upper).write(0);
        indiceCursor.write(index++);
        for(int s=0; s<nbSector+1; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;

            //first point : top left
            final float[] tl = point(half,leftha,radius, upper);
            vertexCursor.write(tl);
            normalCursor.write(tl);
            indiceCursor.write(index++);
        }

        //Cylinder
        for(int s = 0; s < nbSector; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;
            final double righttha = 2*Math.PI * (s+1) * hAngle;

            //first point : top left
            final float[] tl = point(half,leftha,radius, upper);
            final float[] tr = point(half,righttha,radius, upper);
            final float[] bl = point(half,leftha,radius, lower);
            final float[] br = point(half,righttha,radius, lower);

            vertexCursor.write(tl).write(tr).write(bl).write(br);
            normalCursor.write(tl).write(tr).write(bl).write(br);

            int itl = index++;
            int itr = index++;
            int ibl = index++;
            int ibr = index++;

            indiceCursor.write(itl).write(itr).write(ibl);
            indiceCursor.write(itr).write(ibr).write(ibl);
        }

        //Bottom disk
        vertexCursor.write(0).write(lower).write(0);
        indiceCursor.write(index++);
        for(int s=0; s<nbSector+1; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;

            //first point : top left
            final float[] tl = point(half,leftha,radius, lower);
            vertexCursor.write(tl);
            normalCursor.write(tl);
            indiceCursor.write(index++);
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3), new IndexRange[]{
            IndexRange.TRIANGLE_FAN(0, fannb),
            IndexRange.TRIANGLES(fannb, cylindernb),
            IndexRange.TRIANGLE_FAN(cylindernb+fannb, fannb)
                    });
        setShape(shell);
    }

    private void buildShell(Cone cone){
        final float hAngle = 1f / (nbSector);
        final float radius = (float) cone.getRadius();
        final float height = (float) cone.getHeight();
        final float upper = height/2;
        final float lower = -height/2;

        //build 2 triangle fans
        final int fannb = (2+nbSector);
        final int nbVertice = (2+nbSector)*3*2;
        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(nbVertice);
        final Buffer indices = GLBufferFactory.INSTANCE.createInt(fannb*2);

        final FloatCursor vertexCursor = vertices.cursorFloat();
        final FloatCursor normalCursor = normals.cursorFloat();
        final IntCursor indiceCursor = indices.cursorInt();
        
        int index = 0;

        //base disk
        vertexCursor.write(0).write(lower).write(0);
        indiceCursor.write(index++);
        for(int s=0; s<nbSector+1; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;

            //first point : top left
            final float[] tl = point(half,leftha,radius, lower);
            vertexCursor.write(tl);
            normalCursor.write(tl);
            indiceCursor.write(index++);
        }

        //cone
        vertexCursor.write(0).write(upper).write(0);
        indiceCursor.write(index++);
        for(int s=0; s<nbSector+1; s++) {
            final double half = Math.PI /2;
            final double leftha = 2*Math.PI * s * hAngle;

            //first point : top left
            final float[] tl = point(half,leftha,radius, lower);
            vertexCursor.write(tl);
            normalCursor.write(tl);
            indiceCursor.write(index++);
        }

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,1), new IndexRange[]{
            IndexRange.TRIANGLE_FAN(0,fannb),
            IndexRange.TRIANGLE_FAN(fannb,fannb)
            });
        setShape(shell);
    }

    private void buildShell(Quad quad){
        final Vector v1 = new Vector(-quad.getWidth()/2.0,  quad.getHeight()/2.0, 0.0);
        final Vector v2 = new Vector(-quad.getWidth()/2.0, -quad.getHeight()/2.0, 0.0);
        final Vector v3 = new Vector( quad.getWidth()/2.0, -quad.getHeight()/2.0, 0.0);
        final Vector v4 = new Vector( quad.getWidth()/2.0,  quad.getHeight()/2.0, 0.0);
        final Buffer vertices = GLBufferFactory.INSTANCE.createFloat(16);
        final FloatCursor vertexCursor = vertices.cursorFloat();
        vertexCursor.write(v1.toArrayFloat());
        vertexCursor.write(v2.toArrayFloat());
        vertexCursor.write(v3.toArrayFloat());
        vertexCursor.write(v4.toArrayFloat());

        final Vector normal = Geometries.calculateNormal(v1, v2, v3);
        final float[] na = normal.toArrayFloat();
        final Buffer normals = GLBufferFactory.INSTANCE.createFloat(16);
        final FloatCursor normalCursor = normals.cursorFloat();
        normalCursor.write(na);
        normalCursor.write(na);
        normalCursor.write(na);
        normalCursor.write(na);

        final Buffer indices = GLBufferFactory.INSTANCE.createInt(6);
        final IntCursor indiceCursor = indices.cursorInt();
        indiceCursor.write(new int[]{
            0,1,2,
            2,3,0
        });

        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertices,3));
        shell.setNormals(new VBO(normals,3));
        shell.setIndexes(new IBO(indices,3),IndexRange.TRIANGLES(0, 6));
        setShape(shell);
    }
    
    private void buildShell(Triangle tri){
        
        final Buffer vertexBuffer = GLBufferFactory.INSTANCE.createFloat(3*3);
        final Buffer normalBuffer = GLBufferFactory.INSTANCE.createFloat(3*3);
        final Buffer indexBuffer = GLBufferFactory.INSTANCE.createInt(3);
        final FloatCursor vertexCursor = vertexBuffer.cursorFloat();
        final FloatCursor normalCursor = normalBuffer.cursorFloat();
        
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertexBuffer,3));
        shell.setNormals(new VBO(normalBuffer,3));
        shell.setIndexes(new IBO(indexBuffer,3),IndexRange.TRIANGLES(0, 3));
        setShape(shell);

        final float[][] vertices = new float[3][3];
        vertices[0] = tri.getFirstCoord().toArrayFloat();
        vertices[1] = tri.getSecondCoord().toArrayFloat();
        vertices[2] = tri.getThirdCoord().toArrayFloat();

        final float[] normals = new float[3];

        vertexCursor.write(vertices[0]);
        vertexCursor.write(vertices[1]);
        vertexCursor.write(vertices[2]);
        normals[0]=0; normals[1]=0; normals[2]=-1;
        for(int i=0;i<3;i++) normalCursor.write(normals);

        final int[] indices = {0, 1, 2};
        indexBuffer.writeInt(indices,0);
    }

    private void buildShell(Segment segment){

        final Buffer vertexBuffer = GLBufferFactory.INSTANCE.createFloat(6);
        vertexBuffer.cursorFloat().write(segment.getStart().toArrayFloat()).write(segment.getEnd().toArrayFloat());
        final Shell shell = new Shell();
        shell.setVertices(new VBO(vertexBuffer,3));
        shell.setIndexes(null, IndexRange.LINES(0, 6));
        setShape(shell);

    }
    /**
     * Test if the given geometry class is supported by the GeometryMesh.
     * 
     * @param geometryClass, not null
     * @return true if geometry type is supported
     */
    public static boolean canHandle(final Class geometryClass){
        return BBox.class.isAssignableFrom(geometryClass)
            || Disk.class.isAssignableFrom(geometryClass)
            || Sphere.class.isAssignableFrom(geometryClass)
            || Capsule.class.isAssignableFrom(geometryClass)
            || Ellipsoid.class.isAssignableFrom(geometryClass)
            || Cylinder.class.isAssignableFrom(geometryClass)
            || Cone.class.isAssignableFrom(geometryClass);
    }
    
    private static float[] point(double va, double ha, float radius, float height){
        final double sinva = Math.sin(va);
        return new float[]{
                    (float) (Math.cos(ha) * sinva)          *radius,
                    (float) (Math.sin(Math.PI/2d + va))    *radius + height,
                    (float) (Math.sin(ha) * sinva)          *radius
                };
    }
    
    private static void point(double va, double ha, float radius, float height, float[] position, float[] normals){
        final double sinva = Math.sin(va);
        
        normals[0] = (float) (Math.cos(ha) * sinva);
        normals[1] = (float) Math.sin(Math.PI/2d + va);
        normals[2] = (float) (Math.sin(ha) * sinva);
                
        position[0] = normals[0] * radius;
        position[1] = normals[1] * radius + height;
        position[2] = normals[2] * radius;
    }

}
