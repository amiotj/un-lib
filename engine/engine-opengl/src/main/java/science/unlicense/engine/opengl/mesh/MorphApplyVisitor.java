
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.collection.Collections;
import science.unlicense.engine.opengl.operation.ShellVisitor;
import science.unlicense.impl.math.Vectors;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 * Apply morph set on given shell.
 * Caution : this will modify the mesh VBO.
 * 
 * @author Johann Sorel
 */
public class MorphApplyVisitor extends ShellVisitor{

    private final MorphTarget[] morphs;
    private final float[] morphVertice;
    
    public MorphApplyVisitor(Shell shell, MorphSet morphSet) {
        super(shell);
        morphs = new MorphTarget[morphSet.getMorphs().getSize()];
        Collections.copy(morphSet.getMorphs(), morphs, 0);
        morphVertice = new float[shell.getVertices().getSampleCount()];
    }

    protected void visit(Vertex v) {
        if(v.visited) return;
        
        float factor;
        for(int i=0;i<morphs.length;i++){
            factor = morphs[i].getFactor();
            if(factor<=0) continue;
            
            final VBO vertices = morphs[i].getVertices();
            vertices.getTupleFloat(v.index, morphVertice);
            Vectors.scale(morphVertice, factor, morphVertice);
            Vectors.add(v.vertice, morphVertice, v.vertice);
        }
    }

}
