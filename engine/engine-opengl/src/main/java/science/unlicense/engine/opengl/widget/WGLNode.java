
package science.unlicense.engine.opengl.widget;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.phase.FlattenVisitor;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.Renderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.impl.math.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class WGLNode extends WLeaf{

    private final FlattenVisitor visitor = new FlattenVisitor();

    protected final GLNode root = new GLNode();
    /**
     * node transform to map widget position.
     */
    protected final GLNode inSceneNode = new GLNode();
    private final CameraMono camera = new CameraMono();
    
    public WGLNode() {
        camera.setCameraType(CameraMono.TYPE_FLAT);
        root.getChildren().add(inSceneNode);
        root.getChildren().add(camera);
    }

    public GLNode getInSceneNode() {
        return inSceneNode;
    }
    
    protected void renderGL(GLProcessContext context, Matrix4x4 SceneToWidget){

        // fit camera
        Widget parent = this;
        while(parent.getParent()!=null){
            parent = (Widget) parent.getParent();
        }
        final Extent ext = parent.getEffectiveExtent();
        camera.getNodeTransform().getTranslation().setXY(ext.get(0)/2.0, ext.get(1)/2.0);
        camera.getNodeTransform().getScale().setXY(ext.get(0)/2.0, -ext.get(1)/2.0);
        camera.getNodeTransform().notifyChanged();

        // map widget transform to gl space
        final Affine ttt = getNodeToRootSpace();
        final Matrix4x4 nodeToWg = new Matrix4x4().setToIdentity();
        nodeToWg.set(0, 0, ttt.get(0, 0));
        nodeToWg.set(0, 1, ttt.get(0, 1));
        nodeToWg.set(1, 0, ttt.get(1, 0));
        nodeToWg.set(1, 1, ttt.get(1, 1));
        nodeToWg.set(0, 3, ttt.get(0, 2));
        nodeToWg.set(1, 3, ttt.get(1, 2));
        inSceneNode.getNodeTransform().set(nodeToWg);

        inSceneNode.accept(visitor, context);
        Sequence nodes = visitor.getCollection();
        //do the rendering
        for(int i=0,n=nodes.getSize();i<n;i++){
            processRenderers((RenderContext) context, (GLNode)nodes.get(i));
        }
    }

    protected void processRenderers(RenderContext renderContext, GLNode glNode){
        final Sequence updaters = glNode.getUpdaters();
        for(int i=0,n=updaters.getSize();i<n;i++){
            ((Updater)updaters.get(i)).update(renderContext, glNode);
        }

        final Sequence renderers = glNode.getRenderers();
        for(int i=0,n=renderers.getSize();i<n;i++){
            final Renderer renderer = (Renderer) renderers.get(i);
            if(renderer.getPhasePredicate().evaluate(this)){
                renderer.render(renderContext, camera, glNode);
            }
        }
    }

}
