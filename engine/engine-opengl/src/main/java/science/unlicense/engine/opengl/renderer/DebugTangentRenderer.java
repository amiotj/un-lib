

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.character.Chars;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;
import science.unlicense.engine.opengl.renderer.actor.Actors;

/**
 * Display the tangents stored in the mesh shell.
 * 
 * @author Johann Sorel
 */
public class DebugTangentRenderer extends AbstractRenderer {

    private static final ShaderTemplate TANGENT_GE;
    static {
        try{
            TANGENT_GE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/debugtangent-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private float tangentLength = 1.0f;
    private ActorProgram program;
    private Uniform uniLength;

    public DebugTangentRenderer() {
    }

    public DebugTangentRenderer(float tangentLength) {
        this.tangentLength = tangentLength;
    }

    public float getTangentLength() {
        return tangentLength;
    }

    public void setTangentLength(float tangentLength) {
        this.tangentLength = tangentLength;
    }
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        final Mesh mesh = (Mesh) node;

        if(program==null){
            program = new ActorProgram();
            
            //build material actor
            final Material material = new Material();
            material.setDiffuse(Color.RED);
            final MaterialActor materialActor = new MaterialActor(material,false);

            //build shell actor
            final Shape shape = mesh.getShape();
            final ActorExecutor shellActor = Actors.buildExecutor(mesh, shape);
            program.setExecutor(shellActor);

            final DefaultActor defaultActor = new DefaultActor(new Chars("DebugTangent"),false,
                    null, null, null, TANGENT_GE, null, true, false){
                    public void preDrawGL(RenderContext context, ActorProgram program) {
                        super.preDrawGL(context, program);
                        uniLength.setFloat(context.getGL().asGL2ES2(), tangentLength);
                    }
                };

            program.getActors().add(shellActor);
            program.getActors().add(defaultActor);
            program.getActors().add(materialActor);
            program.compile(context);
            uniLength = program.getUniform(new Chars("tangentLength"));
        }
        
        program.render(context, camera, mesh);
    }

    public void dispose(GLProcessContext context) {
        if(program!=null){
            program.releaseProgram(context);
        }
        uniLength = null;
    }
}
