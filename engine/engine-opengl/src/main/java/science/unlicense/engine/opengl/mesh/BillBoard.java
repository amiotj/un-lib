
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.geometry.BBox;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.VBO;


/**
 * A billboard is an image plan which keeps facing the
 * camera.
 *
 * @author Johann Sorel
 */
public class BillBoard extends AbstractShape {

    private VBO points = null;
    private float[] size = new float[2];
    private boolean ScreenCS = false;
    private Vector fixedAxis = null;
    private Vector anchor = new Vector(0, -1);

    public VBO getPoints() {
        return points;
    }

    public void setPoints(VBO points) {
        this.points = points;
    }

    public float[] getSize() {
        return size;
    }

    public void setSize(float[] size) {
        this.size = size;
    }

    public boolean isScreenCS() {
        return ScreenCS;
    }

    public void setScreenCS(boolean ScreenCS) {
        this.ScreenCS = ScreenCS;
    }

    public Vector getFixedAxis() {
        return fixedAxis;
    }

    public void setFixedAxis(Vector fixedAxis) {
        this.fixedAxis = fixedAxis;
    }

    /**
     * The anchor locates the rotation point of the billboard.
     * An anchor of {0,-1} will locate the point at the centered bottom position.
     * @return 
     */
    public Vector getAnchor() {
        return anchor;
    }

    public void setAnchor(Vector anchor) {
        this.anchor = anchor;
    }
    
    public BBox getBBox() {
        return new BBox(3);
    }

    public boolean isDirty() {
        return points!=null && points.isDirty();
    }

    public void dispose(GLProcessContext context) {
        if(points!=null){
            points.unloadFromGpuMemory(context.getGL());
        }
    }

    public void calculateBBox() {
    }

    public Shape copy() {
        final BillBoard cp = new BillBoard();
        cp.setName(getName());
        cp.setPoints(points);
        cp.setScreenCS(ScreenCS);
        cp.setFixedAxis(fixedAxis);
        cp.setSize(size);
        return cp;
    }

}
