
package science.unlicense.engine.opengl.mesh;

import science.unlicense.api.character.Chars;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractShape implements Shape {
    
    private Chars name = Chars.EMPTY;
    
    /**
     * {@inheritDoc }
     */
    public Chars getName() {
        return name;
    }

    /**
     * {@inheritDoc }
     */
    public void setName(Chars name) {
        this.name = name;
    }
    
}
