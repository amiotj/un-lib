
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * The shader executor execute the shader program.
 * A same program could be executed in a multitude of different ways, like for instancing
 * array rendering, deferred etc... that's why it's separate from the program.
 * 
 * @author Johann Sorel
 */
public interface ActorExecutor extends Actor {
    
    /**
     * 
     * @param context current rendering context
     * @param camera camera associate to this context
     * @param node Node to be rendered
     */
    void render(final ActorProgram program, final RenderContext context, final CameraMono camera, final GLNode node);

    /**
     * Dispose renderer and his associated resources.
     * @param context 
     */
    void dispose(GLProcessContext context);
}
