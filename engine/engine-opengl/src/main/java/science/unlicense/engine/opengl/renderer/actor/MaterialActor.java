

package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.CubeMapping;
import science.unlicense.engine.opengl.material.mapping.CylindricalMapping;
import science.unlicense.engine.opengl.material.mapping.DualParaboloidMapping;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.material.mapping.SphericalMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import science.unlicense.engine.opengl.material.mapping.ByVertexColorMapping;
import science.unlicense.engine.opengl.material.mapping.DistanceFieldMapping;
import science.unlicense.engine.opengl.material.mapping.VolumetricScreenSpaceMapping;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;

/**
 *
 * @author Johann Sorel
 */
public class MaterialActor extends AbstractActor{

    public static final Chars AMBIENT = new Chars("AMBIENT");
    public static final Chars DIFFUSE = new Chars("DIFFUSE");
    public static final Chars SPECULAR = new Chars("SPECULAR");
    public static final Chars SHININESS = new Chars("SHININESS");
    
    private static final Chars RATIO = new Chars("ratio");
    private static final ShaderTemplate NOLIGHT_FR;
    private static final ShaderTemplate GBO_FR;
    static {
        try{
            NOLIGHT_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/light/lights-none-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
            GBO_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/gbo-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    private final Material material;
    private final Layer[] lays;
    private final Actor[] actors;
    private final Uniform[] ratios;
    private Uniform uniformAmbient;
    private Uniform uniformDiffuse;
    private Uniform uniformSpecular;
    private Uniform uniformShininess;

    public MaterialActor(Material material, boolean useLight) {
        super(null);
        this.material = material;
        
        final Sequence layers = material.getLayers();
        lays = new Layer[layers.getSize()+2];
        Collections.copy(layers, lays, 0);
        actors = new Actor[lays.length];
        ratios = new Uniform[lays.length];
        for(int i=0,n=lays.length-2;i<n;i++){
            final Layer layer = lays[i];
            final int type = layer.getType();
            final int method = layer.getMethod();
            if(Layer.TYPE_DIFFUSE == type){
                actors[i] = buildTextureActor(type, method, i,layer.getMapping());
            }else if(Layer.TYPE_SPECULAR == type){
                actors[i] = buildTextureActor(type, method, i,layer.getMapping());
            }else if(Layer.TYPE_AMBIENT == type){
                actors[i] = buildTextureActor(type, method, i,layer.getMapping());
            }else if(Layer.TYPE_NORMAL == type){
                final Mapping mapping = layer.getMapping();
                actors[i] = new NormalActor((UVMapping) mapping);
            }else if(Layer.TYPE_ALPHA == type){
                throw new RuntimeException("unexpected");
            }else{
                throw new RuntimeException("unexpected");
            }
        }
        
        if(useLight){
            actors[actors.length-2] = new LightsActor(material);
        }else{
            actors[actors.length-2] = new DefaultActor(
                    new Chars("NoLight"), false, null, null, null, null, NOLIGHT_FR, true, true);
        }
        
        //global collector
        actors[actors.length-1] = new DefaultActor(
                    new Chars("gbo"), true, null, null, null, null, GBO_FR, true, true);
    }
    
    private Actor buildTextureActor(int type, int meth, int inc, Mapping mapping){
        final Chars produce;
        final Chars method;
        if(Layer.TYPE_DIFFUSE == type) produce = StructMaterial.MAT_DIFFUSE;
        else if(Layer.TYPE_SPECULAR == type) produce = StructMaterial.MAT_SPECULAR;
        else produce = StructMaterial.MAT_AMBIENT;
        
        if(Layer.METHOD_ADDITIVE == meth) method = StructMaterial.METHOD_ADDITIVE;
        else if(Layer.METHOD_MULTIPLY == meth) method = StructMaterial.METHOD_MULTIPLY;
        else if(Layer.METHOD_SRC_OVER == meth) method = StructMaterial.METHOD_SRC_OVER;
        else throw new InvalidArgumentException("Unknowned method "+meth);

        final Chars prefix = new Chars("matlayer"+inc+"_");
        
        if(mapping instanceof PlainColorMapping){
            return new PlainColorActor(produce, method, prefix,(PlainColorMapping) mapping);
        }else if(mapping instanceof ByVertexColorMapping){
            return new ByVertexColorActor(produce, method, prefix,(ByVertexColorMapping) mapping);
        }else if(mapping instanceof DistanceFieldMapping){
            return new DistanceFieldActor(produce, method, prefix,(DistanceFieldMapping) mapping);
        }else if(mapping instanceof UVMapping){
            return new TextureActor(produce, method, prefix,(UVMapping) mapping);
        }else if(mapping instanceof CubeMapping){
            return new TextureCubeActor(produce, method, prefix,(CubeMapping) mapping);
        }else if(mapping instanceof SphericalMapping){
            return new TextureSphericalActor(produce, method, prefix,(SphericalMapping) mapping);
        }else if(mapping instanceof DualParaboloidMapping){
            return new TextureDualParaboloidActor(produce, method, prefix,(DualParaboloidMapping) mapping);
        }else if(mapping instanceof CylindricalMapping){
            return new TextureCylindricalActor(produce, method, prefix,(CylindricalMapping) mapping);
        }else if(mapping instanceof ReflectionMapping){
            return new ReflectionActor(produce, method, prefix,(ReflectionMapping) mapping);
        }else if(mapping instanceof VolumetricScreenSpaceMapping){
            return new VolumetricScreenSpaceActor(produce, method, prefix,(VolumetricScreenSpaceMapping) mapping);
        }else{
            throw new RuntimeException("Unexpected mapping : "+mapping);
        }
    }

    public Chars getReuseUID() {
        final CharBuffer cb = new CharBuffer();
        for(int i=0;i<actors.length;i++){
            final Chars id = actors[i].getReuseUID();
            if(id==null) return null;
            if(!cb.isEmpty()) cb.append('*');
            cb.append(id);
        }
        return cb.toChars();
    }

    public int getMinGLSLVersion() {
        int min = GLC.GLSL.V110_GL20;
        for(int i=0;i<actors.length;i++){
            min = Maths.max(min,actors[i].getMinGLSLVersion());
        }
        return min;
    }

    public boolean isDirty() {
        for(int i=0;i<actors.length;i++){
            if(actors[i].isDirty()) return true;
        }
        return false;
    }

    public void initProgram(final RenderContext ctx,ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        template.getFragmentShaderTemplate().append(StructMaterial.TEMPLATE);
        for(int i=0;i<actors.length;i++){
            actors[i].initProgram(ctx, template, tess, geom);
        }
    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        
        if(uniformAmbient==null){
            uniformAmbient = program.getUniform(AMBIENT);
            uniformDiffuse = program.getUniform(DIFFUSE);
            uniformSpecular = program.getUniform(SPECULAR);
            uniformShininess = program.getUniform(SHININESS);
        }
        
        uniformAmbient.setVec4(gl, material.getAmbient().toRGBAPreMul());
        uniformDiffuse.setVec4(gl, material.getDiffuse().toRGBAPreMul());
        uniformSpecular.setVec4(gl, material.getSpecular().toRGBAPreMul());
        uniformShininess.setFloat(gl, material.getShininess());
        
        for(int i=0;i<actors.length;i++){
            if(ratios[i]==null && actors[i] instanceof AbstractMaterialValueActor){
                final AbstractMaterialValueActor ava = (AbstractMaterialValueActor) actors[i];
                ratios[i] = program.getUniform(ava.getUniquePrefix().concat(RATIO));
            }
            if(ratios[i]!=null){
                ratios[i].setFloat(context.getGL().asGL2ES2(), lays[i].getRatio());
            }
            actors[i].preDrawGL(context, program);
        }
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        for(int i=0;i<actors.length;i++){
            actors[i].postDrawGL(context, program);
        }
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniformAmbient = null;
        uniformDiffuse = null;
        uniformSpecular = null;
        uniformShininess = null;
        for(int i=0;i<ratios.length;i++) ratios[i] = null;
        for(int i=0;i<actors.length;i++){
            actors[i].dispose(context);
        }
    }
    
}