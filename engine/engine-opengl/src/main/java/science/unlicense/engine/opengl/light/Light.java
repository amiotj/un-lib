
package science.unlicense.engine.opengl.light;

import science.unlicense.api.color.Color;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 * Common attributes shared by all lights.
 *
 * Collada 1.5 :
 * A light embodies a source of illumination shining on the visual scene. A light source can be located within
 * the scene or infinitely far away. Light sources have many different properties and radiate light in many
 * different patterns and frequencies.
 *
 * @author Johann Sorel
 */
public abstract class Light extends GLNode {

    // light diffuse
    private Color diffuse;
    // light specular
    private Color specular;

    //light shadow map
    private ShadowMap shadowMap;
    
    private boolean castShadows = false;
    
    /**
     * Allow only subclass in this package to extend it.
     */
    protected Light(){}

    protected Light(final Color diffuse, final Color specular) {
        this.diffuse = diffuse;
        this.specular = specular;
    }

    public Color getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(Color diffuse) {
        this.diffuse = diffuse;
    }

    public Color getSpecular() {
        return specular;
    }

    public void setSpecular(Color specular) {
        this.specular = specular;
    }

    public void setCastShadows(boolean castShadows) {
        this.castShadows = castShadows;
    }

    public boolean isCastShadows() {
        return castShadows;
    }

    public ShadowMap getShadowMap() {
        return shadowMap;
    }

    public void setShadowMap(ShadowMap shadowMap) {
        this.shadowMap = shadowMap;
    }

}
