
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.Sorter;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.api.math.Maths;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.MorphSet;
import science.unlicense.engine.opengl.mesh.MorphTarget;
import science.unlicense.engine.opengl.shader.ShaderProgramTemplate;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;

/**
 * Morph target actor.
 * 
 * @author Johann Sorel
 */
public class MorphActor extends AbstractActor{

    public static final Chars LAYOUT_MORPH0 = new Chars("l_morph0");
    public static final Chars LAYOUT_MORPH1 = new Chars("l_morph1");
    public static final Chars LAYOUT_MORPH2 = new Chars("l_morph2");
    
    private static final ShaderTemplate MORPH_VE;
    static {
        try{
            MORPH_VE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/morph-0-ve.glsl"), ShaderTemplate.SHADER_VERTEX);
            }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static final Chars MORPH_FACTOR = new Chars("MORPH_FACTOR");
    private static final Sorter FACTOR_SORTER = new Sorter() {
        public int sort(Object first, Object second) {
            final float f1 = ((MorphTarget)first).getFactor();
            final float f2 = ((MorphTarget)second).getFactor();
            return Double.compare(f2, f1);
        }
    };
    
    //GL loaded informations
    private Uniform uniFactors;
    private final VertexAttribute[] attMorphs = new VertexAttribute[3];
    
    private final MorphSet morphset;

    public MorphActor(MorphSet morphset) {
        super(new Chars("Morph"));
        this.morphset = morphset;
    }

    public int getMinGLSLVersion() {
        return MORPH_VE.getMinGLSLVersion();
    }
    
    public void initProgram(RenderContext ctx, ShaderProgramTemplate template, boolean tess, boolean geom){
        super.initProgram(ctx, template, tess, geom);
        final ShaderTemplate vs = template.getVertexShaderTemplate();
        vs.append(MORPH_VE);
    }

    public void preDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        
        if(uniFactors==null){
            uniFactors = program.getUniform(MORPH_FACTOR);
            attMorphs[0] = program.getVertexAttribute(LAYOUT_MORPH0, gl);
            attMorphs[1] = program.getVertexAttribute(LAYOUT_MORPH1, gl);
            attMorphs[2] = program.getVertexAttribute(LAYOUT_MORPH2, gl);
        }
        
        //find the morph frames with highest factors, we will only use 3.
        final Object[] ms = morphset.getMorphs().toArray();
        Arrays.sort(ms, FACTOR_SORTER);
                
        final float[] factors = new float[3];
        for(int i=0,n=Maths.min(ms.length,3);i<n;i++){
            final MorphTarget frame = (MorphTarget) ms[i];
            factors[i] = frame.getFactor();
            attMorphs[i].enable(gl, frame.getVertices());
        }
                
        uniFactors.setVec3(gl, factors);
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        final GL2ES2 gl = context.getGL().asGL2ES2();
        attMorphs[0].disable(gl);
        attMorphs[1].disable(gl);
        attMorphs[2].disable(gl);
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        uniFactors = null;
        Arrays.fill(attMorphs, null);
    }

}
