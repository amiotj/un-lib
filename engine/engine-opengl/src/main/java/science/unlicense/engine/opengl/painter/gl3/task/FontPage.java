
package science.unlicense.engine.opengl.painter.gl3.task;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.buffer.DefaultByteBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.collection.primitive.FloatSequence;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.DefaultImage;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.ByteOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.io.IOUtilities;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.path.Path;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.painter2d.Font;
import science.unlicense.api.painter2d.FontContext;
import science.unlicense.api.painter2d.FontMetadata;
import science.unlicense.impl.geometry.Projections;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.TextureModel;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.gpu.opengl.shader.Shader;
import science.unlicense.impl.gpu.opengl.shader.ShaderProgram;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.impl.gpu.opengl.shader.VertexAttribute;
import science.unlicense.system.path.Paths;
import science.unlicense.api.painter2d.FontStore;

/**
 * Generate a font page image.
 *
 * @author Johann Sorel
 */
public class FontPage {

    public static TextureModel VEC1_USHORT(){
        final TextureModel tm = TextureModel.create2D(GLC.Texture.InternalFormat.R16UI, GLC.Texture.Format.RED_INTEGER, GLC.Texture.Type.UNSIGNED_SHORT);
        return tm;
    }
    private static final Chars P1 = new Chars("l_position1");
    private static final Chars P2 = new Chars("l_position2");
    private static final Chars P3 = new Chars("l_position3");
    private static final Chars P4 = new Chars("l_position4");
    private static final Chars P5 = new Chars("l_position5");
    private static final Chars MV = new Chars("MV");
    private static final Chars P = new Chars("P");
    private static final Chars SIZE = new Chars("SIZE");

    private final Font font;
    private int width;
    private int height;
    private FontStore resolver;
    private Chars family;
    private Texture2D texture;

    private int[] unicodes;
    private int nbGlpyhPerDim;
    private int glyphwidthi;
    private int glypheighti;
    private double glyphwidth;
    private double glypheight;
    private double scaleX;
    private double scaleY;
    private BBox glyphBox;

    public FontPage(Font font) {
        this.font = font;
    }

    public Font getFont() {
        return font;
    }

    public Texture2D getTexture() {
        return texture;
    }

    public Matrix3x3 getGlyphTransform(int unicode){
        final int index = Arrays.getFirstOccurence(unicodes, 0, unicodes.length, unicode);
        final int indeX = index % nbGlpyhPerDim;
        final int indeY = index / nbGlpyhPerDim;

        double scaleX = 1.0/width;
        double scaleY = 1.0/height;

        return new Matrix3x3(
                glyphwidthi*scaleX, 0, indeX*glyphwidthi*scaleX,
                0, glypheighti*scaleY, indeY*glypheighti*scaleY,
                0, 0, 1);
    }
    
    public Texture2D load() throws IOException {
        if(texture!=null){
            return texture;
        }

        //count characters in the font
        family = font.getFamilies()[0];
        resolver = FontContext.getResolver(family);
        unicodes = resolver.listCharacters(family).toArrayInt();
        Arrays.sort(unicodes);
        final FontMetadata metadata = resolver.getMetadata(family);

        glyphBox = metadata.getGlyphBox();
        final double boxwidth = glyphBox.getSpan(0);
        final double boxheight = glyphBox.getSpan(1);
        final double ascent = metadata.getAscent();

        //we compute the most appropriate ratio to obtain at 2x2 samples in size 32
        //since one pixel in the texture contains 4x4 samples
        final double scale = 16.0 / ascent;
        glyphwidth = boxwidth*scale;
        glypheight = boxheight*scale;
        glyphwidthi = (int) Math.ceil(glyphwidth);
        glypheighti = (int) Math.ceil(glypheight);

        nbGlpyhPerDim = (int) Math.ceil(Math.sqrt(unicodes.length));

        width = glyphwidthi * nbGlpyhPerDim;
        height = glypheighti * nbGlpyhPerDim;

        //load from cache if it exist
        final Path cachePath = GL3ImagePainter2D.getFontPageCacheFile(family);
        if(cachePath.exists()){
            final byte[] data = IOUtilities.readAll(cachePath.createInputStream());
            final Image image = new DefaultImage(new DefaultByteBuffer(data), new Extent.Long(width, height), null);
            texture = new Texture2D(image, VEC1_USHORT());
            return texture;
        }

        //create a texture for rendering
        final GLSource drawable = GLUtilities.createOffscreenSource(16, 16);
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        drawable.getCallbacks().add(context);

        texture = new Texture2D(width, height, VEC1_USHORT());
        final FBO fbo = new FBO(width, height);
        fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, texture);


        //prepare program
        final ShaderProgram program = new ShaderProgram();
        final Shader vertexShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-0-ve.glsl")).createInputStream())),Shader.SHADER_VERTEX);
        final Shader fragmentShader = new Shader(new Chars(IOUtilities.readAll(Paths.resolve(new Chars("mod:/un/engine/opengl/shader/painter/painter-glyph-4-fr.glsl")).createInputStream())),Shader.SHADER_FRAGMENT);
        program.setShader(vertexShader, Shader.SHADER_VERTEX);
        program.setShader(fragmentShader, Shader.SHADER_FRAGMENT);

        final GLNode scene = new GLNode();

        scaleX = 2.0/width;
        scaleY = 2.0/height;
        
        //build the scene
        context.getPhases().add(new RenderPhase(scene,new CameraMono(),fbo));
        scene.getRenderers().add(new BatchGlyphRenderer(unicodes, program));
        drawable.render();
//        for(int i=0;i<unicodes.length;i++) {
//            scene.getRenderers().add(new SingleGlyphRenderer(unicodes[i],i,program));
//            context.paint();
//            scene.getRenderers().removeAll();
//        }

        //render it
        context.getPhases().removeAll();
        context.getPhases().add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                scene.dispose(ctx);
                fbo.removeAttachment(GLC.FBO.Attachment.COLOR_0);
                texture.loadOnSystemMemory(ctx.getGL());
                texture.unloadFromGpuMemory(ctx.getGL());
                fbo.unloadFromGpuMemory(ctx.getGL());
                program.unloadFromGpuMemory(context.getGL());
            }
        });
        
        drawable.render();
        drawable.dispose();

        //save to file cache
        ByteOutputStream cacheOut = cachePath.createOutputStream();
        cacheOut.write(texture.getImage().getDataBuffer().toByteArray());
        cacheOut.close();
//        texture.getImage().getModels().add(ColorModel.MODEL_COLOR, GeometryMaskRasterizer.COLOR_MODEL);
//        Images.write(GeometryMaskRasterizer.to1BitParSample(texture.getImage()), new Chars("png"), Paths.resolve("file:/home/eclesia/page"+font.getFamilies()[0]+".png"));

        //load texture in provided gl context
        return texture;
    }

    private class BatchGlyphRenderer extends AbstractRenderer{

        private final ShaderProgram program;
        private final MatrixRW p = new Matrix3x3().setToIdentity();
        
        private final Sequence idx1 = new ArraySequence();
        private final Sequence vbo = new ArraySequence();
        private final Sequence mv = new ArraySequence();

        private BatchGlyphRenderer(int[] unicodes, ShaderProgram program) {
            this.program = program;
            state.setEnable(GLC.GETSET.State.COLOR_LOGIC_OP, true);
            
            final BBox target = new BBox(2);
            final Vector anchor = new Vector(0,0,0);
            final Vector current = new Vector(3);
            final Vector previous = new Vector(3);
            final Vector first = new Vector(3);
            final Vector control = new Vector(3);
            final float[] buffer = new float[3];
                
            for(int index=0;index<unicodes.length;index++){
                final Geometry2D geom = resolver.getGlyph(unicodes[index], family);
                if(geom==null) continue;
                
                //compute matrix
                final int indeX = index % nbGlpyhPerDim;
                final int indeY = index / nbGlpyhPerDim;
                final int offsetX = glyphwidthi * indeX;
                final int offsetY = glypheighti * indeY;
                target.setRange(0, offsetX*scaleX -1, (offsetX+glyphwidthi)*scaleX -1);
                target.setRange(1, offsetY*scaleY -1, (offsetY+glypheighti)*scaleY -1);
                final Matrix mv = Projections.stretched(glyphBox, target).toMatrix();
                this.mv.add(mv);
                
                //build vbo and render
                final FloatSequence seq = new FloatSequence();
                final PathIterator ite = geom.createPathIterator();

                seq.put(previous.toArrayFloat(buffer));
                seq.put(previous.toArrayFloat(buffer));

                while(ite.next()){
                    int type = ite.getType();
                    if(PathIterator.TYPE_MOVE_TO==type){
                        ite.getPosition(first);
                        ite.getPosition(current);
                    }else if(PathIterator.TYPE_LINE_TO==type){
                        ite.getPosition(current);
                        control.setZ(0);
                        current.setZ(0);
                        previous.setZ(0);
                        seq.put(anchor.toArrayFloat(buffer));
                        seq.put(current.toArrayFloat(buffer));
                        seq.put(previous.toArrayFloat(buffer));
                    }else if(PathIterator.TYPE_CLOSE==type){
                        control.setZ(0);
                        current.setZ(0);
                        previous.setZ(0);
                        seq.put(anchor.toArrayFloat(buffer));
                        seq.put(first.toArrayFloat(buffer));
                        seq.put(previous.toArrayFloat(buffer));
                    }else if(PathIterator.TYPE_QUADRATIC==type){
                        control.setZ(0);
                        current.setZ(0);
                        previous.setZ(0);
                        ite.getPosition(current);
                        seq.put(anchor.toArrayFloat(buffer));
                        seq.put(current.toArrayFloat(buffer));
                        seq.put(previous.toArrayFloat(buffer));

                        // special triangle
                        ite.getFirstControl(control);
                        control.setZ(1);
                        current.setZ(1);
                        previous.setZ(1);
                        seq.put(control.toArrayFloat());
                        seq.put(current.toArrayFloat());
                        seq.put(previous.toArrayFloat());

                    }else{
                        throw new RuntimeException("TODO " + type);
                    }

                    previous.set(current);
                }

                float[] coords = seq.toArrayFloat();
                VBO vbo = new VBO(coords,3);
                vbo.setForgetOnLoad(true);
                this.vbo.add(vbo);
                IndexRange idx1 = IndexRange.TRIANGLES(0, (coords.length-6)/3);
                this.idx1.add(idx1);
            }
            
        }

        @Override
        public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
            final GL2ES2 gl = context.getGL().asGL2ES2();

            gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);

            program.loadOnGpuMemory(gl);
            program.enable(gl);

            //gl.glEnable(GLC.GETSET.State.SCISSOR_TEST);
            //gl.glScissor(target.get, width, width, width);

            final VertexAttribute va1 = program.getVertexAttribute(P1, gl, 1);
            final VertexAttribute va2 = program.getVertexAttribute(P2, gl, 1);
            final VertexAttribute va3 = program.getVertexAttribute(P3, gl, 1);
            final VertexAttribute va4 = program.getVertexAttribute(P4, gl, 1);
            final VertexAttribute va5 = program.getVertexAttribute(P5, gl, 1);
            final Uniform uniMV = program.getUniform(MV);
            final Uniform uniP = program.getUniform(P);
            final Uniform uniSIZE = program.getUniform(SIZE);
            uniP.setMat3(gl, p.toArrayFloat());
            uniSIZE.setVec2(gl, new float[]{width,height});            
            
            for(int i=0,n=10;i<n;i++){
                final MatrixRW mv = (MatrixRW) this.mv.get(i);
                uniMV.setMat3(gl, mv.toArrayFloat());
                
                //build vbo and render
                VBO vbo = (VBO) this.vbo.get(i);
                va1.enable(gl, vbo, 0*3*4);
                va2.enable(gl, vbo, 1*3*4);
                va3.enable(gl, vbo, 2*3*4);
                va4.enable(gl, vbo, 3*3*4);
                va5.enable(gl, vbo, 4*3*4);
                ((IndexRange)idx1.get(i)).draw(gl, false);
                vbo.unbind(gl);
                vbo.unloadFromGpuMemory(context.getGL());
                GLUtilities.checkGLErrorsFail(gl);
            }
            
            program.disable(gl);
            dispose(context);
        }

        @Override
        public void dispose(GLProcessContext context) {
        }

    }
    
    private class SingleGlyphRenderer extends AbstractRenderer{

        private final ShaderProgram program;
        private final MatrixRW p = new Matrix3x3().setToIdentity();
        private Matrix mv;
        
        private IndexRange idx1;
        private VBO vbo;

        private SingleGlyphRenderer(int unicode, int index, ShaderProgram program) {
            this.program = program;
            state.setEnable(GLC.GETSET.State.COLOR_LOGIC_OP, true);
            
            Geometry2D geom = resolver.getGlyph(unicode, family);
            if(geom==null) return;

            //compute matrix
            final BBox target = new BBox(2);
            final int indeX = index % nbGlpyhPerDim;
            final int indeY = index / nbGlpyhPerDim;
            final int offsetX = glyphwidthi * indeX;
            final int offsetY = glypheighti * indeY;
            target.setRange(0, offsetX*scaleX -1, (offsetX+glyphwidthi)*scaleX -1);
            target.setRange(1, offsetY*scaleY -1, (offsetY+glypheighti)*scaleY -1);
            mv = Projections.stretched(glyphBox, target).toMatrix();
                
            final FloatSequence seq = new FloatSequence();
            final PathIterator ite = geom.createPathIterator();

            final Vector anchor = new Vector(0,0,0);
            final Vector current = new Vector(3);
            final Vector previous = new Vector(3);
            final Vector first = new Vector(3);
            final Vector control = new Vector(3);
            final float[] buffer = new float[3];

            seq.put(previous.toArrayFloat(buffer));
            seq.put(previous.toArrayFloat(buffer));

            while(ite.next()){
                int type = ite.getType();
                if(PathIterator.TYPE_MOVE_TO==type){
                    ite.getPosition(first);
                    ite.getPosition(current);
                }else if(PathIterator.TYPE_LINE_TO==type){
                    ite.getPosition(current);
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    seq.put(anchor.toArrayFloat(buffer));
                    seq.put(current.toArrayFloat(buffer));
                    seq.put(previous.toArrayFloat(buffer));
                }else if(PathIterator.TYPE_CLOSE==type){
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    seq.put(anchor.toArrayFloat(buffer));
                    seq.put(first.toArrayFloat(buffer));
                    seq.put(previous.toArrayFloat(buffer));
                }else if(PathIterator.TYPE_QUADRATIC==type){
                    control.setZ(0);
                    current.setZ(0);
                    previous.setZ(0);
                    ite.getPosition(current);
                    seq.put(anchor.toArrayFloat(buffer));
                    seq.put(current.toArrayFloat(buffer));
                    seq.put(previous.toArrayFloat(buffer));

                    // special triangle
                    ite.getFirstControl(control);
                    control.setZ(1);
                    current.setZ(1);
                    previous.setZ(1);
                    seq.put(control.toArrayFloat(buffer));
                    seq.put(current.toArrayFloat(buffer));
                    seq.put(previous.toArrayFloat(buffer));

                }else{
                    throw new RuntimeException("TODO " + type);
                }

                previous.set(current);
            }

            float[] coords = seq.toArrayFloat();
            vbo = new VBO(coords,3);
            vbo.setForgetOnLoad(true);
            idx1 = IndexRange.TRIANGLES(0, (coords.length-6)/3);
        }

        @Override
        public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
            if(vbo==null) return;
            final GL2ES2 gl = context.getGL().asGL2ES2();

            gl.asGL1().glLogicOp(GLC.LOGIC_OP.XOR);

            program.loadOnGpuMemory(gl);
            program.enable(gl);

            //gl.glEnable(GLC.GETSET.State.SCISSOR_TEST);
            //gl.glScissor(target.get, width, width, width);

            final VertexAttribute va1 = program.getVertexAttribute(P1, gl, 1);
            final VertexAttribute va2 = program.getVertexAttribute(P2, gl, 1);
            final VertexAttribute va3 = program.getVertexAttribute(P3, gl, 1);
            final VertexAttribute va4 = program.getVertexAttribute(P4, gl, 1);
            final VertexAttribute va5 = program.getVertexAttribute(P5, gl, 1);
            final Uniform uniMV = program.getUniform(MV);
            final Uniform uniP = program.getUniform(P);
            final Uniform uniSIZE = program.getUniform(SIZE);
            uniMV.setMat3(gl, mv.toArrayFloat());
            uniP.setMat3(gl, p.toArrayFloat());
            uniSIZE.setVec2(gl, new float[]{width,height});
            
            va1.enable(gl, vbo, 0*3*4);
            va2.enable(gl, vbo, 1*3*4);
            va3.enable(gl, vbo, 2*3*4);
            va4.enable(gl, vbo, 3*3*4);
            va5.enable(gl, vbo, 4*3*4);
            idx1.draw(gl, false);
            vbo.unbind(gl);
            vbo.unloadFromGpuMemory(context.getGL());
            GLUtilities.checkGLErrorsFail(gl);
            
            program.disable(gl);
            dispose(context);
        }

        @Override
        public void dispose(GLProcessContext context) {
        }

    }
    
}
