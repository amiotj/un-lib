
package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.api.geometry.BBox;
import science.unlicense.engine.opengl.mesh.AbstractShape;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * @author Johann Sorel
 */
public class ParticuleShape extends AbstractShape {

    private int nbParticule;
    private float lifeSpan;
    private double spawnRadius;
    private Vector gravity = new Vector(0, -1, 0);
    private boolean emitting = false;
    private ParticulePresenter presenter = new ParticuleColorPresenter();
    
    private VBO vertices;
    private VBO times;
    private IBO index;
    private IndexRange range;
    
    public ParticuleShape() {
        this(300,2,1);
    }

    public ParticuleShape(int nbParticule, float lifeSpan, double spawnRadius) {
        this.nbParticule = nbParticule;
        this.lifeSpan = lifeSpan;
        this.spawnRadius = spawnRadius;
    }

    public Vector getGravity() {
        return gravity;
    }

    public void setGravity(Vector gravity) {
        this.gravity = gravity;
    }

    public int getNbParticule() {
        return nbParticule;
    }

    public void setNbParticule(int nbParticule) {
        this.nbParticule = nbParticule;
    }

    public double getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(float lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public double getSpawnRadius() {
        return spawnRadius;
    }

    public void setSpawnRadius(double spawnRadius) {
        this.spawnRadius = spawnRadius;
    }

    public boolean isEmitting() {
        return emitting;
    }

    public void setEmitting(boolean emitting) {
        this.emitting = emitting;
    }
    
    public ParticulePresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(ParticulePresenter presenter) {
        this.presenter = presenter;
    }

    public VBO getVertices() {
        return vertices;
    }

    public void setVertices(VBO vertices) {
        this.vertices = vertices;
    }

    public VBO getTimes() {
        return times;
    }

    public void setTimes(VBO times) {
        this.times = times;
    }

    public IndexRange getRange() {
        return range;
    }

    public void setRange(IndexRange range) {
        this.range = range;
    }

    public IBO getIndex() {
        return index;
    }

    public void setIndex(IBO index) {
        this.index = index;
    }
    
    @Override
    public BBox getBBox() {
        return null;
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public void dispose(GLProcessContext context) {
    }

    @Override
    public void calculateBBox() {
    }

    @Override
    public Shape copy() {
        throw new UnimplementedException("Not supported yet.");
    }
    
}
