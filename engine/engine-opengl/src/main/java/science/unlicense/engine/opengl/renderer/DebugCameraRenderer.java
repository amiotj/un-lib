

package science.unlicense.engine.opengl.renderer;

import science.unlicense.api.gpu.opengl.GL1;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.math.Matrix;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.Vector;

/**
 * Draw a frustrum of the camera
 * TODO
 * 
 * @author Johann Sorel
 */
public class DebugCameraRenderer extends AbstractRenderer {
  
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        if(node instanceof CameraMono){
            final CameraMono cam = (CameraMono) node;
            final double near = cam.getNearPlane();
            final double far = cam.getFarPlane();
            final Matrix mvp = cam.calculateMVP(camera);
            final GL1 gl = context.getGL().asGL1();
              
            final Ray ray1 = cam.calculateRayCameraCS(0,  0);
            final Ray ray2 = cam.calculateRayCameraCS(0,  512);
            final Ray ray3 = cam.calculateRayCameraCS(512,0);
            final Ray ray4 = cam.calculateRayCameraCS(512,512);
            
            final Vector n1 = new Vector(ray1.getDirection().scale(near).localAdd(ray1.getPosition()),1);
            final Vector n2 = new Vector(ray2.getDirection().scale(near).localAdd(ray2.getPosition()),1);
            final Vector n3 = new Vector(ray3.getDirection().scale(near).localAdd(ray3.getPosition()),1);
            final Vector n4 = new Vector(ray4.getDirection().scale(near).localAdd(ray4.getPosition()),1);
            final Vector f1 = new Vector(ray1.getDirection().scale(far).localAdd(ray1.getPosition()),1);
            final Vector f2 = new Vector(ray2.getDirection().scale(far).localAdd(ray2.getPosition()),1);
            final Vector f3 = new Vector(ray3.getDirection().scale(far).localAdd(ray3.getPosition()),1);
            final Vector f4 = new Vector(ray4.getDirection().scale(far).localAdd(ray4.getPosition()),1);
            mvp.transform(n1, n1);
            mvp.transform(n2, n2);
            mvp.transform(n3, n3);
            mvp.transform(n4, n4);
            mvp.transform(f1, f1);
            mvp.transform(f2, f2);
            mvp.transform(f3, f3);
            mvp.transform(f4, f4);
            n1.localScale(1.0/n1.getW());
            n2.localScale(1.0/n2.getW());
            n3.localScale(1.0/n3.getW());
            n4.localScale(1.0/n4.getW());
            f1.localScale(1.0/f1.getW());
            f2.localScale(1.0/f2.getW());
            f3.localScale(1.0/f3.getW());
            f4.localScale(1.0/f4.getW());
            
            gl.glColor3f(1.0f,1.0f,1.0f);
            gl.glBegin(GL_LINES);
            
                //rays between near and far planes
                gl.glVertex3f((float)n1.getX(),(float)n1.getY(),(float)n1.getZ());
                gl.glVertex3f((float)f1.getX(),(float)f1.getY(),(float)f1.getZ());
                gl.glVertex3f((float)n2.getX(),(float)n2.getY(),(float)n2.getZ());
                gl.glVertex3f((float)f2.getX(),(float)f2.getY(),(float)f2.getZ());
                gl.glVertex3f((float)n3.getX(),(float)n3.getY(),(float)n3.getZ());
                gl.glVertex3f((float)f3.getX(),(float)f3.getY(),(float)f3.getZ());
                gl.glVertex3f((float)n4.getX(),(float)n4.getY(),(float)n4.getZ());
                gl.glVertex3f((float)f4.getX(),(float)f4.getY(),(float)f4.getZ());
                
                //near plane
                gl.glVertex3f((float)n1.getX(),(float)n1.getY(),(float)n1.getZ());
                gl.glVertex3f((float)n2.getX(),(float)n2.getY(),(float)n2.getZ());
                gl.glVertex3f((float)n2.getX(),(float)n2.getY(),(float)n2.getZ());
                gl.glVertex3f((float)n4.getX(),(float)n4.getY(),(float)n4.getZ());
                gl.glVertex3f((float)n4.getX(),(float)n4.getY(),(float)n4.getZ());
                gl.glVertex3f((float)n3.getX(),(float)n3.getY(),(float)n3.getZ());
                gl.glVertex3f((float)n3.getX(),(float)n3.getY(),(float)n3.getZ());
                gl.glVertex3f((float)n1.getX(),(float)n1.getY(),(float)n1.getZ());
                
                //far plane
                gl.glVertex3f((float)f1.getX(),(float)f1.getY(),(float)f1.getZ());
                gl.glVertex3f((float)f2.getX(),(float)f2.getY(),(float)f2.getZ());
                gl.glVertex3f((float)f2.getX(),(float)f2.getY(),(float)f2.getZ());
                gl.glVertex3f((float)f4.getX(),(float)f4.getY(),(float)f4.getZ());
                gl.glVertex3f((float)f4.getX(),(float)f4.getY(),(float)f4.getZ());
                gl.glVertex3f((float)f3.getX(),(float)f3.getY(),(float)f3.getZ());
                gl.glVertex3f((float)f3.getX(),(float)f3.getY(),(float)f3.getZ());
                gl.glVertex3f((float)f1.getX(),(float)f1.getY(),(float)f1.getZ());
            gl.glEnd();
            
            
        }
    }

    public void dispose(GLProcessContext context) {
    }
    
}
