
package science.unlicense.engine.opengl.renderer.actor;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.mapping.SpriteInfo;

/**
 *
 * @author Johann Sorel
 */
public class TextureActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate DIFFUSE_TEXTURE_TC;
    private static final ShaderTemplate DIFFUSE_TEXTURE_TE;
    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_TC = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/imagetex-1-tc.glsl"), ShaderTemplate.SHADER_TESS_CONTROL);
            DIFFUSE_TEXTURE_TE = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/imagetex-2-te.glsl"), ShaderTemplate.SHADER_TESS_EVAL);
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/imagetex-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final UVMapping mapping;


    //GL loaded informations
    private int[] reservedTexture;
    private Uniform unitTexture;
    private Uniform unitSprite;
    private Uniform unitScale;
    private Uniform unitOffset;
    
    public TextureActor(Chars produce, Chars method, Chars uniquePrefix, UVMapping mapping) {
        super(new Chars("TextureDiffuse"),false,produce, method, uniquePrefix,
                null,DIFFUSE_TEXTURE_TC,DIFFUSE_TEXTURE_TE,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }
    
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final Texture2D texture = mapping.getTexture();
        if(texture==null) return;
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        // Bind textures
        if(unitTexture == null){
            unitTexture = program.getUniform(uniquePrefix.concat(new Chars("tex")));
            unitSprite = program.getUniform(uniquePrefix.concat(new Chars("sprite")));
            unitScale = program.getUniform(uniquePrefix.concat(new Chars("scale")));
            unitOffset = program.getUniform(uniquePrefix.concat(new Chars("offset")));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        unitTexture.setInt(gl, reservedTexture[1]);
        unitScale.setVec2(gl, mapping.getScale().toArrayFloat());
        unitOffset.setVec2(gl, mapping.getOffset().toArrayFloat());

        final SpriteInfo sprite = mapping.getSprite();
        if(sprite!=null){
            final float[] params = sprite.toParameters();
            unitSprite.setVec4(gl, params);
        }else{
            unitSprite.setVec4(gl, new float[]{0,0,texture.getWidth(), texture.getHeight()});
        }
                
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        if(reservedTexture!=null){
            final GL gl = context.getGL();
            GLUtilities.checkGLErrorsFail(gl);

            // unbind textures
            gl.asGL1().glActiveTexture(reservedTexture[0]);
            gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
            context.getResourceManager().releaseTextureId(reservedTexture[0]);
        }
                
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        unitTexture = null;
        unitSprite = null;
        unitScale = null;
        unitOffset = null;
    }
    
}
