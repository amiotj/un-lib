
package science.unlicense.engine.opengl.widget;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.desktop.FrameManagers;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.image.Image;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Extents;
import science.unlicense.api.layout.FillConstraint;
import science.unlicense.api.layout.FormLayout;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.AbstractPhase;
import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.engine.opengl.phase.PhaseSequence;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WScrollContainer;
import science.unlicense.impl.image.process.geometric.FlipVerticalOperator;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.FBOAttachment;
import science.unlicense.api.desktop.UIFrame;

/**
 * Used to debug opengl frame context.
 * 
 * @author Johann Sorel
 */
public class WContextDebugFrame {
    
    private int row = 0;
    private final WContainer parent;
    private final Sequence viewPanes = new ArraySequence();
    
    int count = 100;
    
    public WContextDebugFrame(GLProcessContext context){
        
        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        frame.setSize(400, 960);
        frame.setVisible(true);
        
        final WContainer root = frame.getContainer();
        root.setLayout(new BorderLayout());
        
        
        parent = new WContainer(new FormLayout());
        root.addChild(new WScrollContainer(parent),BorderConstraint.CENTER);
        
        
        
        
        final Sequence phases = context.getPhases();        
        for(int i=0,n=phases.getSize();i<n;i++){
           test((Phase) phases.get(i));
        }
        
        //add a phase at the end to load in memory fbos
        phases.add(new AbstractPhase() {
            protected void processInt(GLProcessContext context) throws GLException {
                super.processInt(context);
                count--;
                
                if(count<0){
                    count = 100;
                    for(int i=0,n=viewPanes.getSize();i<n;i++){
                        ((ViewPane)viewPanes.get(i)).update(context);
                    }
                }
            }
        });
        
        frame.setVisible(true);
        
    }
    
    private void test(Phase phase){
        if(phase instanceof PhaseSequence){
            final Sequence phases = ((PhaseSequence)phase).getPhases();
            for(int i=0,n=phases.getSize();i<n;i++){
                test((Phase) phases.get(i));
             }
        }else{
            final ViewPane vp = new ViewPane(phase);
            parent.addChild(vp, FillConstraint.builder().coord(0, row++).build());
            viewPanes.add(vp);
        } 
    }
    
    private class ViewPane extends WContainer{
        
        private final Phase phase;
        private FBO fbo;
        private FBOAttachment[] attachements;
        private WGraphicImage[] wimages;
        private final WCheckBox enabled = new WCheckBox();
        
        public ViewPane(Phase phase) {
            super(new BorderLayout());
            this.phase = phase;
                      
            enabled.setText(phase.getId());
            enabled.setCheck(phase.isEnable());
            addChild(enabled,BorderConstraint.TOP);
            
            if(phase instanceof AbstractFboPhase){
                fbo = ((AbstractFboPhase)phase).getOutputFbo();
                if(fbo!=null){
                    attachements = fbo.getAttachements();
                    wimages = new WGraphicImage[attachements.length];

                    final WContainer center = new WContainer(new GridLayout(1, attachements.length));  
                    for(int i=0;i<attachements.length;i++){
                        final WGraphicImage gi = new WGraphicImage();
                        wimages[i] = gi;
                        wimages[i].setOverrideExtents(new Extents(120, 120));
                        wimages[i].setFitting(WGraphicImage.FITTING_SCALED);
                        center.getChildren().add(wimages[i]);
                        
                        wimages[i].addEventListener(MouseMessage.PREDICATE, new EventListener() {
                            public void receiveEvent(Event event) {
                                if( ((MouseMessage)event.getMessage()).getType() == MouseMessage.TYPE_TYPED ){
                                    final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
                                    frame.getContainer().setLayout(new BorderLayout());
                                    frame.getContainer().addChild(new WImageDebug(gi.getImage()),BorderConstraint.CENTER);
                                    frame.setSize(1024, 768);
                                    frame.setVisible(true);
                                }
                            }
                        });
                    }            
                    addChild(center,BorderConstraint.CENTER);
                }
            }
        }
        
        private void update(GLProcessContext context){
            enabled.setText(phase.getId());
            enabled.setCheck(phase.isEnable());
            if(fbo!=null && fbo.isOnGpuMemory()){
                fbo.loadOnSystemMemory(context.getGL());

                for(int i=0;i<attachements.length;i++){
                    Image image = attachements[i].getTexture().getImage();
                    wimages[i].setImage(image==null ? null : new FlipVerticalOperator().execute(image));
                }
            }
        }
    }
    
    
    private static class WImageDebug extends WContainer{

        public WImageDebug(final Image image) {
            super(new BorderLayout());
            
            final WLabel lbl = new WLabel(new Chars(""));
            lbl.setOverrideExtents(new Extents(300, 30));
            addChild(lbl, BorderConstraint.TOP);
            
            final WGraphicImage gra = new WGraphicImage(image);
            gra.setFitting(WGraphicImage.FITTING_CORNER);
            addChild(gra, BorderConstraint.CENTER);
            
            gra.addEventListener(MouseMessage.PREDICATE, new EventListener() {
                public void receiveEvent(Event event) {
                    final double[] pos = ((MouseMessage)event.getMessage()).getMousePosition().toArrayDouble();
                    pos[0] -= gra.getBoundingBox().getMin(0);
                    pos[1] -= gra.getBoundingBox().getMin(1);
                    System.out.println(Arrays.toChars(pos,10));
                    
                    final Extent.Long ext = image.getExtent();
                    if(pos[0]<0 || pos[1] <0 || pos[0]>=ext.get(0) || pos[1]>= ext.get(1)){
                        lbl.setText(Chars.EMPTY);
                    }else{              
                        final int[] coord = new int[]{(int)pos[0],(int)pos[1]};
                        final double[] samples = new double[image.getRawModel().getSampleCount()];
                        image.getRawModel().asTupleBuffer(image).getTupleDouble(coord, samples);
                        lbl.setText(Arrays.toChars(samples, 10));
                    }
                    
                }
            });
            
        }

        
        
        @Override
        protected void receiveEventParent(Event event) {
            super.receiveEventParent(event); //To change body of generated methods, choose Tools | Templates.
        }
        
        
        
    }
    
}
