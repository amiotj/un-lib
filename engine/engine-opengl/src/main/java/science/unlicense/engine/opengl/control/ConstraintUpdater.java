
package science.unlicense.engine.opengl.control;

import science.unlicense.api.physic.constraint.Constraint;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.scenegraph.GLNode;

/**
 *
 * @author Johann Sorel
 */
public class ConstraintUpdater implements Updater{

    private final Constraint constraint;

    public ConstraintUpdater(Constraint constraint) {
        this.constraint = constraint;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void update(RenderContext context, GLNode node) {
        constraint.apply();
    }

}
