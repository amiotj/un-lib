
package science.unlicense.engine.opengl.renderer.actor;

import science.unlicense.api.character.Chars;
import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GL2ES2;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;

/**
 *
 * @author Johann Sorel
 */
public class ReflectionActor extends AbstractMaterialValueActor{

    private static final ShaderTemplate DIFFUSE_TEXTURE_FR;
    static {
        try{
            DIFFUSE_TEXTURE_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/material/reflection-4-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final ReflectionMapping mapping;


    //GL loaded informations
    private int[] reservedTexture;
    private Uniform unitex;
    private Uniform uniSize;
    
    public ReflectionActor(Chars produce, Chars method, Chars uniquePrefix, ReflectionMapping mapping) {
        super(new Chars("Reflection"),false, produce, method, uniquePrefix,
                null,null,null,null,DIFFUSE_TEXTURE_FR);
        this.mapping = mapping;
    }
    
    public void preDrawGL(RenderContext context, ActorProgram program) {
        super.preDrawGL(context, program);
        final GL2ES2 gl = context.getGL().asGL2ES2();

        final FBO reflectionFbo = mapping.getGbo();
        if(reflectionFbo==null) return;
        
        final Texture2D texture = (Texture2D) reflectionFbo.getColorTexture();
        //load texture, image may have changed, no effect if already loaded
        texture.loadOnGpuMemory(gl);

        // Bind textures
        if(unitex == null){
            unitex = program.getUniform(uniquePrefix.concat(new Chars("tex")));
            uniSize = program.getUniform(uniquePrefix.concat(new Chars("screensize")));
        }
        reservedTexture = context.getResourceManager().reserveTextureId();
        gl.glActiveTexture(reservedTexture[0]);
        texture.bind(gl);
        unitex.setInt(gl, reservedTexture[1]);
        uniSize.setVec2(gl, new float[]{(float)context.getViewRectangle().getWidth(),(float)context.getViewRectangle().getHeight()});
        
    }

    public void postDrawGL(RenderContext context, ActorProgram program) {
        super.postDrawGL(context, program);
        final GL gl = context.getGL();
        GLUtilities.checkGLErrorsFail(gl);

        // unbind textures
        if(reservedTexture!=null){
            //texture may be null if the reflection fbo isn't here
            //this case can happen when multiply mirrors are in the scene
            //the first mirror will render the next ones even if they don't have a texture.
            gl.asGL1().glActiveTexture(reservedTexture[0]);
            gl.asGL1().glBindTexture(GL_TEXTURE_2D, 0);
            context.getResourceManager().releaseTextureId(reservedTexture[0]);
        }
        
    }

    public void dispose(GLProcessContext context) {
        super.dispose(context);
        unitex = null;
        uniSize = null;
    }
    
}
