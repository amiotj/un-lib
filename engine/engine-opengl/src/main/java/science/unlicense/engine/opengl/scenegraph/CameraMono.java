
package science.unlicense.engine.opengl.scenegraph;

/**
 * Camera for a single eye view point.
 * 
 * @author Johann Sorel
 */
public class CameraMono extends AbstractCamera {

    public static final int TYPE_FLAT = 0;
    public static final int TYPE_ORTHO = 1;
    public static final int TYPE_PERSPECTIVE = 2;

    /**
     * Create a right handed camera.
     */
    public CameraMono() {
        this(true);
    }
    
    /**
     * 
     * @param rightHanded is scene using a right handed coordinate system.
     */
    public CameraMono(boolean rightHanded) {
        this(rightHanded,true);
    }
    
    /**
     * 
     * @param rightHanded is scene using a right handed coordinate system.
     */
    public CameraMono(boolean rightHanded, boolean autoResize) {
        super(rightHanded,autoResize);
    }

}
