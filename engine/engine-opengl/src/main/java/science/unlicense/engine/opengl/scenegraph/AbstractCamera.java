

package science.unlicense.engine.opengl.scenegraph;

import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.math.Affine;
import science.unlicense.api.gpu.opengl.GL1;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import static science.unlicense.engine.opengl.scenegraph.CameraMono.TYPE_ORTHO;
import static science.unlicense.engine.opengl.scenegraph.CameraMono.TYPE_PERSPECTIVE;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.Affines;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class AbstractCamera extends Camera {
        
    //view size in screen space
    protected final Rectangle renderRectangle = new Rectangle();

    //camera and frustrum informations
    protected final Vector right = new Vector(1,0,0);
    protected final Vector up = new Vector(0,1,0);
    protected final boolean rightHanded;
    protected final boolean autoResize;

    //if set to true, an orthogonal projection is used in place of the perspective.
    protected int cameraType = TYPE_PERSPECTIVE;
    protected double fieldOfView = 45f;
    protected double nearPlane = 0.1f;
    protected double farPlane = 100f;

    //flag indicating matrix must be recalculated.
    private boolean dirty = true;
    
    /**
     * Camera3D to Homogeneous coordinates.
     */
    protected final Matrix4x4 projectionMatrix = new Matrix4x4().setToIdentity();
    
    protected GLProcessContext context;
    
    /**
     * Create a right handed camera.
     */
    public AbstractCamera() {
        this(true,true);
    }
        
    /**
     * 
     * @param rightHanded is scene using a right handed coordinate system.
     * @param autoResize resize automaticaly the paint area when the GL drawable size change.
     */
    public AbstractCamera(boolean rightHanded, boolean autoResize) {
        this.rightHanded = rightHanded;
        this.autoResize = autoResize;
    }

    public void copy(AbstractCamera camera){
        this.cameraType = camera.cameraType;
        this.farPlane = camera.farPlane;
        this.fieldOfView = camera.fieldOfView;
        this.nearPlane = camera.nearPlane;
        this.renderRectangle.set(camera.renderRectangle);
        this.projectionMatrix.set(camera.projectionMatrix);
        this.right.set(camera.right);
        this.up.set(camera.up);
    }
    
    public GLProcessContext getContext(){
        return context;
    }
    
    public Rectangle getRenderArea() {
        return renderRectangle;
    }

    public void setRenderArea(Rectangle paintArea) {
        if(this.renderRectangle.equals(paintArea)) return;
        this.renderRectangle.set(paintArea);
        setDirty(true);
    }

    public Vector getForward() {
        final Vector forward = getUpAxis().cross(getRightAxis(), null);
        forward.localNormalize();
        return forward;
    }
    
    protected boolean isDirty() {
        return dirty;
    }

    protected void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    /**
     * @return Vector3 copy of current up axis.
     */
    public Vector getUpAxis() {
        return up.copy();
    }

    public void setUpAxis(Vector value){
        this.up.set(value);
        this.up.localNormalize();
        setDirty(true);
    }

    public Vector getRightAxis(){
        return right.copy();
    }

    public void setRightAxis(Vector value){
        this.up.set(value);
        this.up.localNormalize();
        setDirty(true);
    }

    /**
     * @return field of view, in degree.
     */
    public double getFieldOfView() {
        return fieldOfView;
    }

    public void setFieldOfView(double fieldOfView) {
        if(this.fieldOfView==fieldOfView) return;
        this.fieldOfView = fieldOfView;
        setDirty(true);
    }

    /**
     * @return frustrum near plane.
     */
    public double getNearPlane() {
        return nearPlane;
    }

    public void setNearPlane(double nearPlane) {
        if(this.nearPlane==nearPlane) return;
        this.nearPlane = nearPlane;
        setDirty(true);
    }

    /**
     * @return frustrum far plane.
     */
    public double getFarPlane() {
        return farPlane;
    }

    public void setFarPlane(double farPlane) {
        if(this.farPlane==farPlane) return;
        this.farPlane = farPlane;
        setDirty(true);
    }

    public void setCameraType(int cameraType) {
        this.cameraType = cameraType;
        setDirty(true);
    }

    /**
     * DO NOT MODIFY MATRIX
     * @return DO NOT MODIFY MATRIX
     */
    public Matrix4x4 getProjectionMatrix() {
        if(isDirty()) calculateMatrices();
        return projectionMatrix;
    }

    /**
     * Calculate a ray from 2D X/Y projection.
     * A mouse click for example.
     *
     * Origin : anton gerdelan
     * http://www.antongerdelan.net/opengl4/raycasting.html
     *
     * @param mouse_x
     * @param mouse_y
     * @return Ray in World Space
     */
    public Ray calculateRayWorldCS(int mouse_x, int mouse_y){
        Ray ray = calculateRayCameraCS(mouse_x, mouse_y);
        //Step 4: 4d World Coordinates
        final Affine invertView = getNodeToRootSpace();
        final Vector ray_wor = (Vector) Affines.transformNormal(invertView,ray.getDirection(),new Vector(3));
        // don't forget to normalise the vector at some point
        ray_wor.localNormalize();

        ray.getPosition().set(invertView.getCol(invertView.getInputDimensions()));
        ray.setDirection(ray_wor);
        return ray;
    }
    
    /**
     * Calculate a ray from 2D X/Y projection.
     * A mouse click for example.
     *
     * Origin : anton gerdelan
     * http://www.antongerdelan.net/opengl4/raycasting.html
     *
     * @param mouse_x
     * @param mouse_y
     * @return Ray in Camera Space
     */
    public Ray calculateRayCameraCS(int mouse_x, int mouse_y){
        //Step 1: 3d Normalised Device Coordinates
        final double x = (2d * mouse_x) / renderRectangle.getWidth() - 1d;
        final double y = 1d - (2d * mouse_y) / renderRectangle.getHeight();
        //Step 2: 4d Homogeneous Clip Coordinates
        final Vector ray_clip = new Vector(x,y, 1.0,1.0);
        //Step 3: 4d Eye (Camera) Coordinates
        final Matrix4x4 invertProjection = getProjectionMatrix().invert();
        final Tuple ray_eye = invertProjection.transform(ray_clip,null);
        ray_clip.set(2, 1);

        return new Ray(new Vector(0,0,0), new Vector(ray_eye.getXYZ()).localNormalize());
    }
    
    public Vector[] calculateFrustrumCorners(){
        return calculateFrustrumCorners(Double.MAX_VALUE);
    }
    
    /**
     * Calculate frustrum corners.
     * 
     * @param maxDistance maximum far plane
     * @return 8 Vectors [n1,n2,n3,n4,f1,f2,f3,f4]
     */
    public Vector[] calculateFrustrumCorners(double maxDistance){
        final Ray ray1 = calculateRayCameraCS(0,  0);
        final Ray ray2 = calculateRayCameraCS(0,  (int)renderRectangle.getHeight());
        final Ray ray3 = calculateRayCameraCS((int)renderRectangle.getWidth(),0);
        final Ray ray4 = calculateRayCameraCS((int)renderRectangle.getWidth(),(int)renderRectangle.getHeight());

        final double far = Maths.min(farPlane,maxDistance);

        return new Vector[]{
            new Vector(ray1.getDirection().scale(nearPlane).localAdd(ray1.getPosition()),1),
            new Vector(ray2.getDirection().scale(nearPlane).localAdd(ray2.getPosition()),1),
            new Vector(ray3.getDirection().scale(nearPlane).localAdd(ray3.getPosition()),1),
            new Vector(ray4.getDirection().scale(nearPlane).localAdd(ray4.getPosition()),1),
            new Vector(ray1.getDirection().scale(far).localAdd(ray1.getPosition()),1),
            new Vector(ray2.getDirection().scale(far).localAdd(ray2.getPosition()),1),
            new Vector(ray3.getDirection().scale(far).localAdd(ray3.getPosition()),1),
            new Vector(ray4.getDirection().scale(far).localAdd(ray4.getPosition()),1)
        };
    }

    /**
     * 
     * @param context
     * @param nanotime
     * @param renderSize : in view port coordinate system, (0,0) is lower left
     */
    public void update(RenderContext context, long nanotime, Rectangle renderSize) {
        if(autoResize && !renderRectangle.equals(renderSize)){
            setRenderArea(renderSize);
        }
                
        this.context = context;
        
        //update the paint area
        final GL gl = context.getGL();
        if(gl!=null){
            gl.asGL1().glViewport((int)renderRectangle.getX(), (int)renderRectangle.getY(),
                    (int)renderRectangle.getWidth(), (int)renderRectangle.getHeight());
        }
    }
    
    protected void calculateMatrices() {
        setDirty(false);

        final double width = renderRectangle.getWidth();
        final double height = renderRectangle.getHeight();
            
        projectionMatrix.setToIdentity();
        final double ratio = width/height;
        //calculate edges
        final double top = nearPlane * Math.tan(fieldOfView * Math.PI / 360.0);
        final double bottom = -top;
        final double left = bottom * ratio;
        final double right = top * ratio;

        if(cameraType == TYPE_ORTHO){
            projectionMatrix.set(Matrices.ortho2(fieldOfView,ratio,nearPlane,farPlane,null));
            //Matrices.orthogonal(left, right, bottom, top, nearPlane, farPlane, projectionMatrix.getValues());
        }else if(cameraType == TYPE_PERSPECTIVE){
            projectionMatrix.set(Matrices.perspective(fieldOfView, width, height,
                nearPlane, farPlane, null, rightHanded));
        }
    }
    
}
