

package science.unlicense.engine.opengl.mesh.particle;

import science.unlicense.engine.opengl.renderer.actor.Actor;

/**
 *
 * @author Johann Sorel
 */
public interface ParticulePresenter {
    
    Actor createActor();
    
}
