

package science.unlicense.engine.opengl.light;

import science.unlicense.engine.opengl.scenegraph.Camera;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.impl.geometry.s2d.Rectangle;

/**
 * Describes the shadow map projected by a light.
 * 
 * @author Johann Sorel
 */
public class ShadowMap {
   
    private FBO fbo;
    private final CameraMono camera = new CameraMono(true, false);

    public ShadowMap() {
    }

    public FBO getFbo() {
        return fbo;
    }

    public void setFbo(FBO fbo) {
        this.fbo = fbo;
    }

    public CameraMono getCamera() {
        return camera;
    }
    
    public void update(Camera sceneCamera, Light light, int textureSize){
        //disable auto resize, this camera will render in an fbo which has a
        //size different from the gl canvas.
        if(light instanceof SpotLight){
            camera.setNearPlane(0.1);
            camera.setFarPlane(1000);
            camera.setFieldOfView(((SpotLight)light).getFallOffAngle()*2);
        }else if(light instanceof DirectionalLight){
            final DirectionalLight dl = (DirectionalLight) light;
            final CameraMono dll = dl.createFittingCamera((CameraMono) sceneCamera);
            camera.setNearPlane(dll.getNearPlane());
            camera.setFarPlane(dll.getFarPlane());
            camera.setFieldOfView(dll.getFieldOfView());
        }else if(light instanceof ProjectiveLight){
            final ProjectiveLight dl = (ProjectiveLight) light;
            final CameraMono dll = dl.createFittingCamera((CameraMono) sceneCamera);
            camera.setNearPlane(dll.getNearPlane());
            camera.setFarPlane(dll.getFarPlane());
            camera.setFieldOfView(dll.getFieldOfView());
        }else{
            //point light, TODO
        }
        camera.setRenderArea(new Rectangle(0, 0, textureSize, textureSize));
        if(!light.getChildren().contains(camera)){
            light.getChildren().add(camera);
        }
        
        if(fbo==null){
            fbo = new FBO(textureSize, textureSize);
            final Texture2D tex = new Texture2D(textureSize, textureSize, Texture2D.VEC2_FLOAT());
            fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, tex);
            fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(textureSize, textureSize, Texture2D.DEPTH24_STENCIL8()));
        }
        light.setShadowMap(this);
    }
    
    
}
