

package science.unlicense.engine.opengl.operation;

import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.image.Image;
import science.unlicense.api.painter2d.AbstractPainter2D;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.impl.geometry.path.FlattenPathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.triangulate.EarClipping;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.Matrix4x4;

/**
 * DRAFT.
 * This painter converts vector datas in a triangulated mesh.
 * TODO : what about curves ?
 * 
 * @author Johann Sorel
 */
public class MeshPainter2D extends AbstractPainter2D {

    private static class PaintedTriangle{
        private Triangle triangle;
        private int[] colors;
        
    }
    
    private final Sequence stack;
    private final Matrix4x4 transform;
    private final EarClipping triangulator = new EarClipping();
    private final double[] resolution = new double[]{1,1};

    public MeshPainter2D() {
        stack = new ArraySequence();
        transform = new Matrix4x4().setToIdentity();
    }
    
    private MeshPainter2D(Sequence triangles, Matrix4x4 trs){
        this.stack = triangles;
        this.transform = trs;
    }

    @Override
    public FontStore getFontStore() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void fill(Geometry2D geom) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void stroke(Geometry2D geom) {
        //not supported yet
    }

    @Override
    public void paint(Image image, Affine2 transform) {
        //not supported yet
    }
    
    private void process(Geometry2D geom){
        
        //triangulate this geometry
        final FlattenPathIterator ite = new FlattenPathIterator(
                geom.createPathIterator(), resolution);
        final Sequence triangles;
        try {
            triangles = triangulator.triangulate(ite);
        } catch (OperationException ex) {
            ex.printStackTrace();
            return;
        }
        
        //intersect them with previous triangles
        for(int i=0,n=stack.getSize();i<n;i++){
            for(int k=0,kn=triangles.getSize();k<kn;k++){
                
            }
        }
        
    }

    @Override
    public void dispose() {}
    
}
