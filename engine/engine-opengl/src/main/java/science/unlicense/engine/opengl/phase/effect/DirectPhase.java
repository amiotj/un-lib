
package science.unlicense.engine.opengl.phase.effect;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.character.Chars;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.gpu.opengl.GL1;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture;

/**
 * Render the given texture stretched on gl frame.
 *
 * @author Johann Sorel
 */
public class DirectPhase extends AbstractTexturePhase {

    private static final ShaderTemplate SHADER_FR;
    static {
        try{
            SHADER_FR = ShaderTemplate.create(new Chars("mod:/un/engine/opengl/shader/phase-direct-3-fr.glsl"), ShaderTemplate.SHADER_FRAGMENT);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private final DirectActor actor = new DirectActor();
    private AlphaBlending blending = AlphaBlending.create(AlphaBlending.SRC_OVER, 1f);
    
    public DirectPhase(Texture texture) {
        this(null,texture);
    }
    
    public DirectPhase(Texture texture,AlphaBlending blending) {
        this(null,texture,blending);
    }
    
    public DirectPhase(FBO output, Texture texture) {
        this(output,texture,null,null);
    }
    
    public DirectPhase(FBO output, Texture texture,AlphaBlending blending) {
        this(output,texture,null,blending);
    }
    
    public DirectPhase(FBO output, Texture texture, Matrix4x4 transform) {
        this(output,texture,transform,null);
    }
    
    public DirectPhase(FBO output, Texture texture, Matrix4x4 transform,AlphaBlending blending) {
        super(output,texture);
        if(transform!=null) setTransform(transform);
        if(blending!=null){
            this.blending = blending;
        }
    }

    public AlphaBlending getBlending() {
        return blending;
    }

    public void setBlending(AlphaBlending blending) {
        this.blending = blending;
    }

    protected DirectActor getActor() {
        return actor;
    }

    private final class DirectActor extends DefaultActor{

        public DirectActor() {
            super(new Chars("Direct"),false,null,null,null,null,SHADER_FR,true,true);
        }

        public void preDrawGL(RenderContext context, ActorProgram program) {
            super.preDrawGL(context, program);
            final GL1 gl = context.getGL().asGL1();
            
            gl.glStencilMask(0xFF);
            gl.glClearStencil(0);
            gl.glClear(GL_STENCIL_BUFFER_BIT);
            gl.glEnable(GL_STENCIL_TEST);
            gl.glStencilFunc(GL_ALWAYS, 1, 0xFF);
            gl.glStencilMask(0x00);
            
            GLUtilities.configureBlending(gl, blending);
        }

        public void postDrawGL(RenderContext context, ActorProgram program) {
            super.postDrawGL(context, program);
            final GL1 gl = context.getGL().asGL1();
            
            gl.glDisable(GL_BLEND);
        }

    }
}
