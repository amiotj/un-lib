
package science.unlicense.engine.opengl.control;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.math.Affine;
import science.unlicense.api.math.Matrix;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.animation.Updater;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.transform.NodeTransform;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.impl.math.Matrices;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Affines;
import science.unlicense.impl.math.Vector;

/**
 * Controller orbiting around a given target.
 *
 * @author Johann Sorel
 */
public class OrbitController implements Updater, EventListener{

    private static final double EPSILON = 1e-12;
    
    private final EventSource source;
    

    //variables
    private boolean enable = true;
    private final Vector up;
    private final Vector right;
    private final GLNode target;
    private final boolean rightHanded;
    private final double mouseSpeed = 0.005;
    private Tuple warpPosition = null;

    //mouse wheel variables
    private float pushpull = 0;

    //constraints
    private double mindistance = 0.1;
    private double maxdistance = Double.MAX_VALUE;
    private boolean linear = false;
    private double step = 0.3;

    //state
    private float distance = 1;
    private double horizontalAngle = 0.0f;
    private double verticalAngle = 0.0f;
    private double rollAngle = 0.0f;

    //moving target controls
    private double dragX = 0;
    private double dragY = 0;

    
    //current gesture state
    private final GestureState gestureState = new GestureState();    
    private final Sequence gestureTasks = new ArraySequence();
    
    /**
     * Create a right handed orbit camera with default up(0,1,0) and right(1,0,0) axis.
     *
     * @param source mouse and keyboard event source
     * @param target node to focus on
     */
    public OrbitController(EventSource source, GLNode target) {
        this(source,new Vector(0, 1, 0), new Vector(1, 0, 0),target,true);
    }
    
    /**
     * Create a right handed orbit camera.
     *
     * @param source mouse and keyboard event source
     * @param up upward direction
     * @param right right direction
     * @param target node to focus on
     */
    public OrbitController(EventSource source, Vector up, Vector right, GLNode target) {
        this(source,up,right,target,true);
    }
    
    /**
     * Create an orbit camera.
     *
     * @param source mouse and keyboard event source
     * @param up upward direction
     * @param right right direction
     * @param target node to focus on
     * @param rightHanded true for right handed coordinate system, false for leftHanded.
     */
    public OrbitController(EventSource source, Vector up, Vector right, GLNode target, boolean rightHanded) {
        this.up = up;
        this.right = right;
        this.target = target;
        this.rightHanded = rightHanded;
        this.source = source;
        
        if(source!=null){
            source.addEventListener(MouseMessage.PREDICATE, this);
        }
        
    }

    private Vector getForward() {
        return up.cross(right, null).localNormalize();
    }
    
    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
    
    /**
     * Configure a default orbit controller.
     * @return 
     */
    public OrbitController configureDefault(){
        gestureTasks.removeAll();
        gestureTasks.add(new Move(new Chars("Drag"),GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_2, MouseMessage.TYPE_MOVE)));
        gestureTasks.add(new RotateHorizontal(new Chars("Rotate H"),GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_1, MouseMessage.TYPE_MOVE)));
        gestureTasks.add(new RotateVertical(new Chars("Rotate V"),GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_1, MouseMessage.TYPE_MOVE)));
        gestureTasks.add(new RotateRoll(new Chars("Roll"),GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_3, MouseMessage.TYPE_MOVE)));
        gestureTasks.add(new Distance(new Chars("Distance"),GestureTrigger.createInstantMouseTrigger(GestureTrigger.ANY_BUTTON_OR_KEY, MouseMessage.TYPE_WHEEL)));
        gestureTasks.add(new PushPull(new Chars("Push/Pull"),GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_2, MouseMessage.TYPE_WHEEL)));
        return this;
    }

    public Sequence getGestureTasks() {
        return gestureTasks;
    }
    
    /**
     * Get pointer warp position.
     * Works ony if event source is a NewtFrame.
     * 
     * On each mouse event the mouse position is reset to this coordinate.
     * This allow to avoid mouse hitting the frame borders.
     * 
     * @return Tuple can be null
     */
    public Tuple getWarpPointer() {
        return warpPosition;
    }

    public void setWarpPointer(Tuple autoWarp) {
        this.warpPosition = autoWarp;
    }
    
    /**
     * Get distance from target.
     * @return float, distance.
     */
    public float getDistance() {
        return distance;
    }

    /**
     * Set distance from target.
     * @param distance
     */
    public void setDistance(float distance) {
        this.distance = distance;
    }

    /**
     * Get horizontal angle.
     * @return horizontal angle in Radians
     */
    public double getHorizontalAngle() {
        return horizontalAngle;
    }

    /**
     * Set horizontal angle.
     * @param horizontalAngle in Radians
     */
    public void setHorizontalAngle(double horizontalAngle) {
        this.horizontalAngle = horizontalAngle;
    }

    /**
     * Get vertical angle.
     * @return horizontal angle in Radians
     */
    public double getVerticalAngle() {
        return verticalAngle;
    }

    /**
     * Set vertical angle.
     * @param verticalAngle in Radians
     */
    public void setVerticalAngle(double verticalAngle) {
        this.verticalAngle = verticalAngle;
    }

    /**
     * Get roll angle.
     * @return roll angle
     */
    public double getRollAngle() {
        return rollAngle;
    }

    /**
     * Set roll angle.
     * @param rollAngle
     */
    public void setRollAngle(double rollAngle) {
        this.rollAngle = rollAngle;
    }

    /**
     * Get minimum camera distance to target.
     * @return minimum camera distance
     */
    public double getMinDistance() {
        return mindistance;
    }

    /**
     * Set minimum camera distance to target
     * @param mindistance minimum camera distance
     */
    public void setMinDistance(double mindistance) {
        this.mindistance = mindistance;
    }

    /**
     * Get maximum camera distance to target.
     * @return maximum camera distance
     */
    public double getMaxDistance() {
        return maxdistance;
    }

    /**
     * Set maximum camera distance to target
     * @param maxdistance maximum camera distance
     */
    public void setMaxDistance(double maxdistance) {
        this.maxdistance = maxdistance;
    }

    /**
     * When zooming In/Out ,indicate if the step value
     * is linear or relative to current distance.
     * If not linear, the distance value must be in range [0..1]
     * @return true if linear
     */
    public boolean isLinear() {
        return linear;
    }

    /**
     * Set camera distance calculation type.
     * @param linear true for linear, false for proportional
     */
    public void setLinear(boolean linear) {
        this.linear = linear;
    }

    /**
     * Get step used when zooming In/Out.
     * @return step
     */
    public double getStep() {
        return step;
    }

    /**
     * Set step used when zooming In/Out.
     * @param step
     */
    public void setStep(double step) {
        this.step = step;
    }

    public void update(RenderContext context, GLNode node) {
        if(!enable) return;
        if(rightHanded){
            updateRightHanded(context, node);
        }else{
            updateLeftHanded(context, node);
        }
    }
    
    private void updateRightHanded(RenderContext context, GLNode node) {
        checkConstraints();
//        
//        
//        //rotation with expected 'forward' direction
//        final Matrix3 rot3 = Matrix3.createRotation(new Vector(1, 0, 0), getForward());
//        Matrices.roundZeros(rot3.getValues(), EPSILON);
//        final Matrix4 rotateMatrix = new Matrix4().setToIdentity();
//        rotateMatrix.set(rot3);
//        
//        final Matrix4 orbitMatrix = Matrix4.focusedOrbit(verticalAngle,horizontalAngle, rollAngle, distance);
//        orbitMatrix.localMultiply(rotateMatrix);
//                
//        //convert to world space
//        orbitMatrix.localInvert();
//        orbitMatrix.localMultiply(target.getRootToNodeSpace());
//        node.setRootToNodeSpace(orbitMatrix);
//        
//        //calculate the target x/y move
//        if(dragX != 0 || dragY != 0){
//            final NodeTransform trs = node.getNodeTransform();
//            final Matrix rot = trs.getRotation();
//            final Vector dx = (Vector) rot.transform(right, new Vector(3));
//            final Vector dy = (Vector) rot.transform(up, new Vector(3));
//            final Vector targetTrs = target.getNodeTransform().getTranslation();
//            targetTrs.localAdd(dx.scale(dragX));
//            targetTrs.localAdd(dy.scale(dragY));
//            target.getNodeTransform().notifyChanged();
//            dragX = 0;
//            dragY = 0;
//        }
//        
//        
        
        
        //calculate rotation matrix
        final Affine3 mh = new Affine3().setToRotation(-horizontalAngle, up);
        final Affine3 mv = new Affine3().setToRotation(-verticalAngle, right);
        mh.localMultiply(mv);
        
        
        //calculate translation
        final Vector forward = up.cross(right, null).localNegate();
        Affines.transformNormal(mh, forward, forward);
        final Vector translation = forward.localNormalize().scale(distance);
        
        //calculate roll matrix
        final Affine3 mr = new Affine3().setToRotation(rollAngle, forward);
        mr.localMultiply(mh);
        
        //calculate node to chidl matrix
        final Affine3 nodeToChild = new Affine3();
        nodeToChild.localMultiply(mr);
        nodeToChild.setCol(3, translation.getValues());
        
        
        if(target==node.getParent()){
            //simplify maths, modify the node transform directly
            node.getNodeTransform().getRotation().set(mr.toMatrix());
            node.getNodeTransform().getTranslation().set(translation);
            node.getNodeTransform().notifyChanged();
        }else{
            //TODO : bugged
            final Affine targetrtn = target.getRootToNodeSpace();
            nodeToChild.localMultiply(targetrtn);
            node.setRootToNodeSpace(nodeToChild);
        }
        
        
        //calculate the target x/y move
        if(dragX != 0 || dragY != 0){
            final Affine3 trs = mr;
            final Vector dx = (Vector) Affines.transformNormal(trs, right, new Vector(3));
            final Vector dy = (Vector) Affines.transformNormal(trs, up,  new Vector(3));
            final Vector targetTrs = target.getNodeTransform().getTranslation();
            targetTrs.localAdd(dx.scale(dragX));
            targetTrs.localAdd(dy.scale(dragY));
            target.getNodeTransform().notifyChanged();
            dragX = 0;
            dragY = 0;
        }
        
        
//        //calculate the target position
//        final Matrix targetwtn = target.getNodeToRootSpace();
//        final Tuple np = targetwtn.transform(new Vector(0,0,0), 1);
//        Tuple v = new Vector(0, 0, distance);
//        v = mh.transform(v);
//        final Vector position = new Vector(v);
//        position.localAdd(np);
//
//        //calculate roll matrix
//        final Vector forward = up.cross(right, null)
//        mh.transform(forward, forward);
//        forward.localNormalize();
//        final Matrix3 mr = new Matrix3();
//        Matrices.createRotation3(rollAngle, forward, mr.getValues());
//        mr.localMultiply(mh);
//
//        //update the node transform
//        final NodeTransform trs = node.getNodeTransform();
//        trs.getTranslation().set(position);
//        trs.getRotation().set(mr);
//        trs.notifyChanged();
//
//
//        //calculate the target x/y move
//        if(dragX != 0 || dragY != 0){
//            final Matrix rot = trs.getRotation();
//            final Vector dx = (Vector) rot.transform(right, new Vector(3));
//            final Vector dy = (Vector) rot.transform(up, new Vector(3));
//            final Vector targetTrs = target.getNodeTransform().getTranslation();
//            targetTrs.localAdd(dx.scale(dragX));
//            targetTrs.localAdd(dy.scale(dragY));
//            target.getNodeTransform().notifyChanged();
//            dragX = 0;
//            dragY = 0;
//        }
//
//        //calculate the targer push/pull move
//        if(pushpull!=0){
//            final Vector targetTrs = target.getNodeTransform().getTranslation();
//            targetTrs.localAdd(forward.scale(pushpull));
//            target.getNodeTransform().notifyChanged();
//            pushpull = 0;
//        }
        
        
        
//        
//        //calculate rotation matrix
//        final Matrix3 mh = Matrix3.createRotation3(-horizontalAngle, up);
//        final Matrix3 mv = Matrix3.createRotation3(-verticalAngle, right);
//        mh.localMultiply(mv);
//        
//        //calculate the target position
//        final Matrix targetwtn = target.getNodeToRootSpace();
//        final Tuple np = targetwtn.transform(new Vector(0,0,0), 1);
//        Tuple v = new Vector(0, 0, distance);
//        v = mh.transform(v);
//        final Vector position = new Vector(v);
//        position.localAdd(np);
//
//        //calculate roll matrix
//        final Vector forward = new Vector(0,0,1);
//        mh.transform(forward, forward);
//        forward.localNormalize();
//        final Matrix3 mr = new Matrix3();
//        Matrices.createRotation3(rollAngle, forward, mr.getValues());
//        mr.localMultiply(mh);
//
//        //update the node transform
//        final NodeTransform trs = node.getNodeTransform();
//        trs.getTranslation().set(position);
//        trs.getRotation().set(mr);
//        trs.notifyChanged();
//
//
//        //calculate the target x/y move
//        if(dragX != 0 || dragY != 0){
//            final Matrix rot = trs.getRotation();
//            final Vector dx = (Vector) rot.transform(right, new Vector(3));
//            final Vector dy = (Vector) rot.transform(up, new Vector(3));
//            final Vector targetTrs = target.getNodeTransform().getTranslation();
//            targetTrs.localAdd(dx.scale(dragX));
//            targetTrs.localAdd(dy.scale(dragY));
//            target.getNodeTransform().notifyChanged();
//            dragX = 0;
//            dragY = 0;
//        }
//
//        //calculate the targer push/pull move
//        if(pushpull!=0){
//            final Vector targetTrs = target.getNodeTransform().getTranslation();
//            targetTrs.localAdd(forward.scale(pushpull));
//            target.getNodeTransform().notifyChanged();
//            pushpull = 0;
//        }
        
    }
    
    private void updateLeftHanded(RenderContext context, GLNode node) {
        checkConstraints();
        //calculate rotation matrix
        final Matrix3x3 mh = new Matrix3x3(Matrices.createRotation3(horizontalAngle, up, null));
        final Matrix3x3 mv = new Matrix3x3(Matrices.createRotation3(verticalAngle, right, null));
        mh.localMultiply(mv);

        //calculate the camera position
        final Affine targetwtn = target.getNodeToRootSpace();
        final Tuple np = targetwtn.transform(new Vector(0,0,0), null);
        Tuple v = new Vector(0, 0, -distance);
        v = mh.transform(v);
        final Vector position = new Vector(v);
        position.localAdd(np);

        //calculate roll matrix
        final Vector forward = new Vector(0,0,1);
        mh.transform(forward, forward);
        forward.localNormalize();
        final Matrix3x3 mr = new Matrix3x3(Matrices.createRotation3(rollAngle, forward, null));
        mr.localMultiply(mh);

        //update the node transform
        final NodeTransform trs = node.getNodeTransform();
        trs.getTranslation().set(position);
        trs.getRotation().set(mr);
        trs.notifyChanged();


        //calculate the target movement
        if(dragX != 0 || dragY != 0){
            final MatrixRW rot = trs.getRotation();
            final Vector dx = (Vector) rot.transform(right, new Vector(3));
            final Vector dy = (Vector) rot.transform(up, new Vector(3));
            final Vector targetTrs = target.getNodeTransform().getTranslation();
            targetTrs.localAdd(dx.scale(dragX));
            targetTrs.localAdd(dy.scale(dragY));
            target.getNodeTransform().notifyChanged();
            dragX = 0;
            dragY = 0;
        }

    }
    
    public void receiveEvent(Event event) {
        if(!enable) return;
        if(event.getMessage().isConsumed()) return;
        
        gestureState.update(event);
        
        for(int i=0,n=gestureTasks.getSize();i<n;i++){
            final GestureTask task = (GestureTask) gestureTasks.get(i);
            final GestureTrigger trigger = task.getTrigger();
            if(trigger.evaluate(gestureState)){
                task.execute(gestureState, this);
            }
        }
        
        //warp pointer if requested
        if(warpPosition!=null){
//            if(source instanceof NewtFrame){
//                ((NewtFrame)source).glWindow.warpPointer((int)warpPosition.getX(), (int)warpPosition.getY());
//                gestureState.mouseWarped(warpPosition.getX(), warpPosition.getY());
//            }
        }
        
    }

    private void checkConstraints(){
        if(distance<mindistance){
            distance = (float) mindistance;
        }else if(distance>maxdistance){
            distance = (float) maxdistance;
        }
    }
        
    public static abstract class GestureTask {
        
        protected GestureTrigger trigger;
        protected CharArray name = Chars.EMPTY;

        public GestureTask() {}

        public GestureTask(GestureTrigger trigger) {
            this.trigger = trigger;
        }
        
        public GestureTask(Chars name,GestureTrigger trigger) {
            this.name = name;
            this.trigger = trigger;
        }

        public CharArray getName() {
            return name;
        }

        public void setName(CharArray name) {
            this.name = name;
        }

        public GestureTrigger getTrigger() {
            return trigger;
        }

        public void setTrigger(GestureTrigger trigger) {
            this.trigger = trigger;
        }
        
        public abstract void execute(GestureState state, OrbitController control);
        
    }
    
    public static class RotateHorizontal extends GestureTask{
        public RotateHorizontal() {}
        public RotateHorizontal(GestureTrigger trigger) {super(trigger);}
        public RotateHorizontal(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, OrbitController control) {
            control.horizontalAngle += control.mouseSpeed * state.mouseDiffX;
        }
    }
    
    public static class RotateVertical extends GestureTask{
        public RotateVertical() {}
        public RotateVertical(GestureTrigger trigger) {super(trigger);}
        public RotateVertical(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, OrbitController control) {
            control.verticalAngle += control.mouseSpeed * state.mouseDiffY;
        }
    }
    
    public static class RotateRoll extends GestureTask{
        public RotateRoll() {}
        public RotateRoll(GestureTrigger trigger) {super(trigger);}
        public RotateRoll(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, OrbitController control) {
            control.rollAngle -= control.mouseSpeed * state.mouseDiffX;
        }
    }
    
    public static class Move extends GestureTask{
        public Move() {}
        public Move(GestureTrigger trigger) {super(trigger);}
        public Move(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, OrbitController control) {
            control.dragX += control.mouseSpeed * state.mouseDiffX * control.distance;
            control.dragY -= control.mouseSpeed * state.mouseDiffY * control.distance;
        }
    }
    
    public static class Distance extends GestureTask{
        public Distance() {}
        public Distance(GestureTrigger trigger) {super(trigger);}
        public Distance(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, OrbitController control) {
            double d = state.wheelScroll * control.step;
            if(!control.linear) d *= control.distance;
            //change distance to target
            control.distance -= d;
        }
    }
    
    public static class PushPull extends GestureTask{
        public PushPull() {}
        public PushPull(GestureTrigger trigger) {super(trigger);}
        public PushPull(Chars name, GestureTrigger trigger) {super(name,trigger);}
        public void execute(GestureState state, OrbitController control) {
            double d = state.wheelScroll * control.step;
            if(!control.linear) d *= control.distance;
            //push or pull target
            control.pushpull -= d;
        }
    }
    
}
