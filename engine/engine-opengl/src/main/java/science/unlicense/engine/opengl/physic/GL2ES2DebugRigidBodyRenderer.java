
package science.unlicense.engine.opengl.physic;

import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.math.Affine;
import science.unlicense.api.model.tree.NodeSequence;
import science.unlicense.api.physic.body.Body;
import science.unlicense.api.physic.body.BodyGroup;
import science.unlicense.api.physic.body.RigidBody;
import science.unlicense.api.predicate.ClassPredicate;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.GLC;

/**
 *
 * @author Johann Sorel
 */
public class GL2ES2DebugRigidBodyRenderer extends AbstractRenderer {

    private static final Color[] GROUP_PALETTE = new Color[]{
        new Color(0x00,0x00,0x00),
        new Color(0x80,0x00,0x00),
        new Color(0x00,0x80,0x00),
        new Color(0x80,0x80,0x00),
        new Color(0x00,0x00,0x80),
        new Color(0x80,0x00,0x80),
        new Color(0x00,0x80,0x80),
        new Color(0xc0,0xc0,0xc0),
        new Color(0x80,0x80,0x80),
        new Color(0xff,0x00,0x00),
        new Color(0x00,0xff,0x00),
        new Color(0xff,0xff,0x00),
        new Color(0x00,0x00,0xff),
        new Color(0xff,0x00,0xff),
        new Color(0x00,0xff,0xff),
        new Color(0xff,0xff,0xff)
    };

    private boolean viewBody = true;
    private boolean viewBodyVelocity = true;
    private boolean viewBodyForce = true;

    public GL2ES2DebugRigidBodyRenderer() {
    }

    public boolean isBodyVelocityVisible() {
        return viewBodyVelocity;
    }

    public void setBodyVisible(boolean visible) {
        this.viewBody = visible;
    }

    public boolean isBodyVisible() {
        return viewBody;
    }

    public void setBodyVelocityVisible(boolean visible) {
        this.viewBodyVelocity = visible;
    }

    public void setBodyForceVisible(boolean visible) {
        this.viewBodyForce = visible;
    }

    public boolean isBodyForceVisible() {
        return viewBodyForce;
    }

    @Override
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {

        final Sequence bodies = new NodeSequence(node, true, new ClassPredicate(RigidBody.class));

        final GeometryMesh m = new GeometryMesh(new BBox(3));

        final MeshRenderer rendererFront = new MeshRenderer(m);
        rendererFront.getState().setEnable(GLC.GETSET.State.DEPTH_TEST, false);
        rendererFront.getState().setEnable(GLC.GETSET.State.CULL_FACE, true);
        rendererFront.getState().setCulling(GLC.CULLING.BACK);
        rendererFront.getState().setDepthMask(false);
        rendererFront.getState().setPolygonMode(GLC.POLYGON_MODE.LINE);

        final MeshRenderer rendererBack = new MeshRenderer(m);
        rendererBack.getState().setEnable(GLC.GETSET.State.DEPTH_TEST, false);
        rendererBack.getState().setEnable(GLC.GETSET.State.CULL_FACE, true);
        //rendererBack.getState().setEnable(GLC.GETSET.State.LINE_STIPPLE, true);
        rendererBack.getState().setCulling(GLC.CULLING.FRONT);
        rendererBack.getState().setLineStipple((short)0xF0F0);
        rendererBack.getState().setLineWidth(1);
        rendererBack.getState().setDepthMask(false);
        rendererBack.getState().setPolygonMode(GLC.POLYGON_MODE.LINE);



        //render rigid body geometry
        if(viewBody){
            for(int i=0,n=bodies.getSize();i<n;i++){
                final Body body = (Body) bodies.get(i);
                if(body instanceof RigidBody){
                    final RigidBody rb = (RigidBody) body;
                    final Geometry geometry = rb.getGeometry();
                    m.updateGeometry(geometry);
                    Affine trs  = rb.getNodeToRootSpace();
                    m.getNodeTransform().set(trs);

                    m.getMaterial().setDiffuse(Color.GRAY_DARK);
                    rendererBack.render(context, camera, node);
                    
                    final BodyGroup group = body.getGroup();
                    if(group!=null){
                        int index = System.identityHashCode(group) % GROUP_PALETTE.length;
                        m.getMaterial().setDiffuse(GROUP_PALETTE[index]);
                    }else{
                        m.getMaterial().setDiffuse(Color.GRAY_LIGHT);
                    }
                    rendererFront.render(context, camera, node);

                    m.getShape().dispose(context);
                }
            }
        }
        m.dispose(context);

//        //render velocities and forces
//        if(viewBodyVelocity || viewBodyForce){
//            gl.glLineWidth(1f);
//            gl.glBegin(GL.GL_LINES);
//            for(int i=0,n=bodies.getSize();i<n;i++){
//                final Body body = (Body) bodies.get(i);
//
//                final Matrix M = body.getNodeToRootSpace();
//                final MatrixRW MVP = P.multiply(V.multiply(M));
//
//                p0.setXYZW(0, 0, 0, 1);
//                p1.set(body.getMotion().getVelocity());p1.setW(1);
//                p2.set(body.getMotion().getForce().scale(0.1));p2.setW(1);
//                MVP.transform(p0,p0);
//                MVP.transform(p1,p1);
//                MVP.transform(p2,p2);
//                if(p0.getZ()<0) return; //check not outside screen
//
//                if(viewBodyVelocity){
//                    gl.glColor3f(0f, 1f, 0f);
//                    gl.glVertex2f((float)(p0.getX()/p0.getZ()),(float)(p0.getY()/p0.getZ()));
//                    gl.glVertex2f((float)(p1.getX()/p1.getZ()),(float)(p1.getY()/p1.getZ()));
//                }
//                if(viewBodyForce){
//                    gl.glColor3f(0f, 0f, 1f);
//                    gl.glVertex2f((float)(p0.getX()/p0.getZ()),(float)(p0.getY()/p0.getZ()));
//                    gl.glVertex2f((float)(p2.getX()/p2.getZ()),(float)(p2.getY()/p2.getZ()));
//                }
//            }
//            gl.glEnd();
//        }
        
    }

    @Override
    public void dispose(GLProcessContext context) {

    }

}
