
package science.unlicense.api.game.navmap;

import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public interface NavigationMap {

    Vector navigate(Vector start, Vector direction, Vector buffer);
    
}
