
package science.unlicense.impl.game.renderer;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.io.IOException;
import science.unlicense.api.number.Float64;
import science.unlicense.api.number.Int32;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.renderer.actor.MaterialActor;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shape;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.operation.ShellVisitor;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.renderer.AbstractRenderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.Actors;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.engine.opengl.shader.ShaderTemplate;
import science.unlicense.impl.gpu.opengl.shader.Uniform;
import science.unlicense.engine.opengl.tessellation.Tessellator;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.renderer.actor.ActorExecutor;

/**
 * Spawn given mesh at the surface of the rendered object.
 * This can be used to add fine grain details to materials (like dust) or regular
 * mesh patterns (like grass).
 * 
 * @author Johann Sorel
 */
public class SurfaceSpawnRenderer extends AbstractRenderer {

    private static final Chars CST_SNB = new Chars("$SNB");
    private static final Chars CST_SNBC = new Chars("$SNBC");
    private static final Chars CST_SCOUNT = new Chars("$SMAX");
    private static final Chars CST_SVERTEX = new Chars("$S_VERTEX");
    private static final Chars CST_SNORNAL = new Chars("$S_NORMAL");
    private static final Chars CST_SUV = new Chars("$S_UV");
    
    private static final ShaderTemplate TEMPLATE_GE;
    static {
        try{
            TEMPLATE_GE = ShaderTemplate.create(new Chars("mod:/un/impl/game/renderer/surfacespawn-3-ge.glsl"), ShaderTemplate.SHADER_GEOMETRY);
        }catch(IOException ex){
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
    
    //max 5
    private int spawnCount = 3;
    private float size = 0.3f;
    private Mesh spawnedMesh;
    private ActorProgram program;
    private ActorExecutor exec;
    private Uniform uniScale;

    public SurfaceSpawnRenderer() {
    }

    public Mesh getSpawnedMesh() {
        return spawnedMesh;
    }

    /**
     * Only simple meshes made of 1 index range of triangles are supported.
     * 
     * @param spawnedMesh 
     */
    public void setSpawnedMesh(Mesh spawnedMesh) {
        this.spawnedMesh = spawnedMesh;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public void setSpawnCount(int spawnCount) {
        this.spawnCount = spawnCount;
    }

    public int getSpawnCount() {
        return spawnCount;
    }
    
    public void renderInternal(RenderContext context, CameraMono camera, GLNode node) {
        GLUtilities.checkGLErrorsFail(context.getGL());
        final Mesh mesh = (Mesh) node;

        if (program == null) {
            
            final ShaderTemplate template = fillTemplate();
            final MaterialActor materialActor = new MaterialActor(spawnedMesh.getMaterial(), true);

            //build shell actor
            final Shape shape = mesh.getShape();
            exec = Actors.buildExecutor(mesh, shape);

            final DefaultActor defaultActor = new DefaultActor(new Chars("SpawnSurface"),false,
                    null, null, null, template, null, true, false) {
                        public void preDrawGL(RenderContext context, ActorProgram program) {
                            super.preDrawGL(context, program);
                            uniScale.setFloat(context.getGL().asGL2ES2(), size);
                        }
                    };

            program = new ActorProgram();
            program.getActors().add(exec);

            final Tessellator tes = mesh.getTessellator();
            if (tes != null) {
                program.getActors().add(tes);
            }

            program.getActors().add(defaultActor);
            program.getActors().add(materialActor);
            program.compile(context);
            uniScale = program.getUniform(new Chars("spawnScale"));
        }

        GLUtilities.checkGLErrorsFail(context.getGL());
        exec.render(program, context, camera, mesh);
        GLUtilities.checkGLErrorsFail(context.getGL());
        //TODO, when do we release the program ?
        //program.releaseProgram(context);
    }

    private ShaderTemplate fillTemplate(){
        final ShaderTemplate template = new ShaderTemplate(TEMPLATE_GE);
        
        final Shell shell = (Shell) spawnedMesh.getShape();
        final int[] count = new int[1];
        final CharBuffer vbuffer = new CharBuffer();
        final CharBuffer nbuffer = new CharBuffer();
        final CharBuffer uvbuffer = new CharBuffer();
        
        ShellVisitor visitor = new ShellVisitor(shell) {
            protected void visit(ShellVisitor.Vertex vertex) {
                count[0]++;
                if(!vbuffer.isEmpty()){
                    vbuffer.append(',');
                    nbuffer.append(',');
                    uvbuffer.append(',');
                }
                vbuffer.append("vec3(");
                vbuffer.append(Float64.encode(vertex.vertice[0])).append(',');
                vbuffer.append(Float64.encode(vertex.vertice[1])).append(',');
                vbuffer.append(Float64.encode(vertex.vertice[2])).append(')');
                nbuffer.append("vec3(");
                nbuffer.append(Float64.encode(vertex.normal[0])).append(',');
                nbuffer.append(Float64.encode(vertex.normal[1])).append(',');
                nbuffer.append(Float64.encode(vertex.normal[2])).append(')');
                if(vertex.uv==null){
                    uvbuffer.append("vec2(0,0)");
                }else{
                    uvbuffer.append("vec2(");
                    uvbuffer.append(Float64.encode(vertex.uv[0])).append(',');
                    uvbuffer.append(Float64.encode(vertex.uv[1])).append(')');
                }
            }
        };
        visitor.visit();
        
        template.replaceTexts(CST_SCOUNT, Int32.encode(spawnCount));
        template.replaceTexts(CST_SNBC, Int32.encode(count[0]*spawnCount));
        template.replaceTexts(CST_SNB, Int32.encode(count[0]));
        template.replaceTexts(CST_SVERTEX, vbuffer.toChars());
        template.replaceTexts(CST_SNORNAL, nbuffer.toChars());
        template.replaceTexts(CST_SUV, uvbuffer.toChars());
        
        return template;
    }
    
    public void dispose(GLProcessContext context) {
        if (program != null) {
            program.releaseProgram(context);
        }
        uniScale = null;
    }
    
}