
package science.unlicense.impl.desktop.opengl;

import science.unlicense.api.desktop.FrameDecoration;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.desktop.Frame;

/**
 *
 * @author Johann Sorel
 */
public class GLFrameDecoration implements FrameDecoration{

    private GLUIFrame frame;

    public GLUIFrame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = (GLUIFrame) frame;
    }
    
    public Margin getMargin() {
        return frame.getGLFrame().getSource().getBinding().getFrameMargin();
    }

}
