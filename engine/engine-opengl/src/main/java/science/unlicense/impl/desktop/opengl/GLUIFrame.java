
package science.unlicense.impl.desktop.opengl;

import science.unlicense.api.CObjects;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.color.AlphaBlending;
import science.unlicense.api.desktop.FrameDecoration;
import science.unlicense.api.desktop.FrameManager;
import science.unlicense.api.desktop.FrameMessage;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.desktop.UIFrame;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.desktop.cursor.ImageCursor;
import science.unlicense.api.desktop.cursor.NamedCursor;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.Property;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLFrame;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.api.layout.BorderConstraint;
import science.unlicense.api.layout.BorderLayout;
import science.unlicense.api.layout.Margin;
import science.unlicense.api.logging.Logger;
import science.unlicense.api.logging.Loggers;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLExecutable;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import science.unlicense.engine.opengl.painter.gl3.GL3Painter2D;
import science.unlicense.engine.opengl.scenegraph.s2d.GLImageNode2D;
import science.unlicense.api.scenegraph.DefaultSceneNode;
import science.unlicense.api.scenegraph.SceneNode;
import science.unlicense.engine.opengl.widget.WGLView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.Widgets;
import science.unlicense.engine.ui.widget.frame.WFrameDecoration;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class GLUIFrame extends AbstractEventSource implements UIFrame {

    private final GLFrame glframe;
    private final DefaultGLProcessContext context = new DefaultGLProcessContext();

    //frame container
    private final WGLView containerView;
    private final GLImageNode2D containerNode = new GLImageNode2D();//texture updater
    private final WGLView.Value containerLink = new WGLView.Value(){
        private Texture2D tex1 = null;
        private Texture2D tex2 = null;
        @Override
        public void swapTexture(Image base) {
            tex2 = containerNode.getTexture();
            if (tex2==null) {
                tex2 = new Texture2D(base);
                containerNode.setTexture(tex2);
            } else {
                tex2.setImage(base);
            }
//            containerNode.setTexture(base);
//            //swap texture buffer
//            tex2 = tex1;
//            tex1 = base;
//            return tex2;
        }
    };
    private boolean containerVisible = true;

    //frame cursor
    private GL3Painter2D painter2D;
    private final SceneNode painterScene = new DefaultSceneNode(2);
    private Cursor defaultCursor = NamedCursor.DEFAULT;
    private Cursor currentCursor = NamedCursor.DEFAULT;
    private final Vector cursorPos = new Vector(2);
    private final GLImageNode2D cursorNode = new GLImageNode2D();
    private final Object LOCK = new Object();
    private GLUIFrame parent = null;
    private final Sequence children = new ArraySequence();
    private boolean modale = false;
    private boolean disposed = false;
    private final int nbSample = 4;


    //frame decoration
    private FrameDecoration decoration = new GLFrameDecoration();
    private WGLView decorationView;
    private final GLImageNode2D decorationNode = new GLImageNode2D();//texture updater
    private final WGLView.Value decorationLink = new WGLView.Value(){
        private Texture2D tex1 = null;
        private Texture2D tex2 = null;
        @Override
        public void swapTexture(Image base) {
            tex2 = decorationNode.getTexture();
            tex2.setImage(base);
//            decorationNode.setTexture(base);
//            //swap texture buffer
//            tex2 = tex1;
//            tex1 = base;
//            return tex2;
        }
    };
    

    public GLUIFrame(GLUIFrame parent, GLFrame glframe) {
        this.parent = parent;
        this.glframe = glframe;

        try {
            GL3ImagePainter2D.prepareUI();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        
        containerView = new WGLView(1,1);
        WContainer container = containerView.getContainer();
        container.getStyle().getSelfRule().setProperties(new Chars("background:\n{\nfill-paint:@color-background\n}"));

        decorationNode.setBlending(AlphaBlending.DEFAULT);
        painterScene.getChildren().add(cursorNode);
        painterScene.getChildren().add(containerNode);
        painterScene.getChildren().add(decorationNode);
        ((GLFrameDecoration)decoration).setFrame(this);


        glframe.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                updateCursor(me);
                if (isLocked()) return;

                //process the widget plan first
                if(containerView!=null){
                    if(decorationView!=null && decoration!=null){
                        //adjust position based on decoration margins
                        final Margin margin = decoration.getMargin();
                        final Tuple mousePosition = me.getMousePosition().copy();
                        mousePosition.getValues()[0] -= margin.left;
                        mousePosition.getValues()[1] -= margin.top;
                        final MouseMessage rme = new MouseMessage(
                                me.getType(),
                                me.getButton(),
                                me.getNbClick(),
                                mousePosition,
                                me.getMouseScreenPosition(),
                                me.getWheelOffset(),
                                me.isDragging());
                            containerView.receiveEvent(new Event(GLUIFrame.this,rme));
                        if(rme.isConsumed()) me.consume();
                    }else{
                        containerView.receiveEvent(new Event(GLUIFrame.this, me));
                    }

                }

                //process frame decoration
                if(decorationView!=null){
                    decorationView.receiveEvent(new Event(GLUIFrame.this,me));
                }
            }
        });


        glframe.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final KeyMessage ke = (KeyMessage) event.getMessage();
                if (isLocked()) return;

                //send the keyboard event only to the focused widget
                final Widget focused = getFocusedWidget();
                if(focused!=null){
                    focused.receiveEvent(new Event(GLUIFrame.this,ke));
                }
            }
        });

        glframe.addEventListener(FrameMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final FrameMessage fm = (FrameMessage) event.getMessage();

                if (fm.getType() == FrameMessage.TYPE_PREDISPOSE) {
                    try{
                        GLProcessContext context = GLUIFrame.this.getContext();
                        context.addTask(new GLExecutable() {
                            public Object execute() {
                                painter2D.dispose();
                                return null;
                            }
                        });
                        setVisible(false);
                    }catch(Exception ex){
                        //null pointer bug in Newt
                        ex.printStackTrace();
                        Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                    }
                } else if(fm.getType() == FrameMessage.TYPE_DISPOSED) {
                    disposed = true;

                    //remove from parent
                    if(GLUIFrame.this.parent!=null){
                        GLUIFrame.this.parent.children.remove(this);
                    }
                    //unlock those waiting
                    synchronized(LOCK){
                        LOCK.notifyAll();
                    }
                }


            }
        });

        glframe.getSource().getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                render(source);
            }

            @Override
            public void dispose(GLSource source) {

            }
        });

    }

    public GLProcessContext getContext() {
        return context;
    }

    public GLFrame getGLFrame() {
        return glframe;
    }

    public void setWidgetLayerVisible(boolean visible){
        this.containerVisible = visible;
    }

    public boolean isWidgetLayerVisible() {
        return containerVisible;
    }

    @Override
    public FrameManager getManager() {
        return GLUIFrameManager.INSTANCE;
    }

    @Override
    public WContainer getContainer() {
        return containerView!=null ? containerView.getContainer() : null;
    }
    
    @Override
    public Widget getFocusedWidget() {
        if(containerView!=null && containerView.getFocused()!=null) return containerView.getFocused();
        if(decorationView!=null && decorationView.getFocused()!=null) return decorationView.getFocused();
        return null;
    }

    @Override
    public void setFocusedWidget(Widget focused) {
        final Widget oldFocused = getFocusedWidget();
        if(CObjects.equals(oldFocused, focused)) return;
        if(containerView!=null) containerView.setFocused(focused);
        sendPropertyEvent(this, PROP_FOCUSED_WIDGET, oldFocused, focused);
    }

    @Override
    public Cursor getCursor() {
        return defaultCursor;
    }

    @Override
    public void setCursor(Cursor defaultCursor) {
        final boolean updateCurrent = currentCursor == this.defaultCursor;
        this.defaultCursor = defaultCursor;
        if(updateCurrent) setCurrentCursor(defaultCursor);
    }

    protected Cursor getCurrentCursor() {
        return currentCursor;
    }

    protected void setCurrentCursor(Cursor cursor) {
        this.currentCursor = cursor;
        if(cursor instanceof NamedCursor || cursor==null){
            glframe.setPointerVisible(true);
        }else{
            glframe.setPointerVisible(false);
        }
    }

    @Override
    public GLUIFrame getParentFrame() {
        return parent;
    }

    @Override
    public Sequence getChildrenFrames() {
        return Collections.readOnlySequence(children);
    }

    @Override
    public CharArray getTitle() {
        return glframe.getTitle();
    }

    @Override
    public Property varTitle() {
        return getProperty(PROP_TITLE);
    }
    
    @Override
    public void setTitle(CharArray title) {
        glframe.setTitle(title);
    }

    @Override
    public void setOnScreenLocation(Tuple location) {
        glframe.setOnScreenLocation(location);
    }

    @Override
    public Tuple getOnScreenLocation() {
        return glframe.getOnScreenLocation();
    }

    @Override
    public void setSize(int width, int height) {
        glframe.setSize(width, height);
    }

    @Override
    public Extent getSize() {
        return glframe.getSize();
    }

    @Override
    public void setVisible(boolean visible) {
        glframe.setVisible(visible);
    }

    @Override
    public boolean isVisible() {
        return glframe.isVisible();
    }

    @Override
    public Property varVisible() {
        return getProperty(PROP_VISIBLE);
    }
    
    void setModale(boolean modale) {
        this.modale = modale;
    }
    
    @Override
    public boolean isModale() {
        return modale;
    }

    public FrameDecoration getDecoration() {
        return decoration;
    }

    public void setDecoration(FrameDecoration decoration) {
        this.decoration = decoration;
        glframe.setDecorated(this.decoration instanceof GLFrameDecoration);
    }

    public FrameDecoration getSystemDecoration() {
        final GLFrameDecoration deco = new GLFrameDecoration();
        deco.setFrame(this);
        return deco;
    }

    @Override
    public int getState() {
        return glframe.getState();
    }

    @Override
    public void setState(int state) {
        glframe.setState(state);
    }

    @Override
    public void setMaximizable(boolean maximizable) {
        glframe.setMaximizable(maximizable);
    }

    @Override
    public boolean isMaximizable() {
        return glframe.isMaximizable();
    }

    @Override
    public Property varMaximizable() {
        return getProperty(PROP_MAXIMIZABLE);
    }
    
    @Override
    public void setMinimizable(boolean minimizable) {
        glframe.setMinimizable(minimizable);
    }

    @Override
    public boolean isMinimizable() {
        return glframe.isMinimizable();
    }

    @Override
    public Property varMinimizable() {
        return getProperty(PROP_MINIMIZABLE);
    }
    
    @Override
    public void setClosable(boolean closable) {
        glframe.setClosable(closable);
    }

    @Override
    public boolean isClosable() {
        return glframe.isClosable();
    }

    @Override
    public Property varClosable() {
        return getProperty(PROP_CLOSABLE);
    }
    
    @Override
    public void setAlwaysonTop(boolean ontop) {
        glframe.setAlwaysonTop(ontop);
    }

    @Override
    public boolean isAlwaysOnTop() {
        return glframe.isAlwaysOnTop();
    }

    @Override
    public boolean isTranslucent() {
        return !glframe.isOpaque();
    }

    @Override
    public void dispose() {
        glframe.dispose();
    }

    @Override
    public boolean isDisposed() {
        return disposed;
    }

    @Override
    public void waitForDisposal() {        
        //ensure thread is not a jogamp graphic or event thread
        final String name = Thread.currentThread().getName();
        if(name.contains("FPSAWTAnimator") || name.contains("EDT")){
            throw new IllegalStateException("You can not lock a frame rendering or event thread, use a new thread to wait for frame disposal.");
        }

        synchronized (LOCK) {
            while (isVisible()) {
                try {
                    LOCK.wait();
                } catch (InterruptedException ex) {
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
            }
        }
    }

    @Override
    public Class[] getEventClasses() {
        return glframe.getEventClasses();
    }

    @Override
    public EventListener[] getListeners(Predicate predicate) {
        return glframe.getListeners(predicate);
    }

    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
        glframe.addEventListener(predicate, listener);
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
        glframe.removeEventListener(predicate, listener);
    }

    void setParentFrame(GLUIFrame parent){
        this.parent = parent;
        parent.children.add(this);
    }

    private void updateCursor(MouseMessage me){
        cursorPos.set(me.getMousePosition());

        if(me.getType()==MouseMessage.TYPE_EXIT){
            //mouse mouse far away
            cursorPos.setXY(-1000, -1000);
        }

        if(containerView==null) return;
        final WContainer cnt = containerView.getContainer();
        if(cnt==null) return;
        final Tuple pos = me.getMousePosition();
        Widget child = Widgets.pickAt(cnt, pos.getX(), pos.getY());
        if(child!=null){
            final Cursor cursor = child.getStackCursor();
            if(cursor==null){
                setCursor(defaultCursor);
            }else{
                setCurrentCursor(cursor);
            }
        }
    }

    private void render(GLSource source) {
        context.execute(source);
        if(disposed) return;
        //update painter2D
        final Extent size = getSize();
        if(painter2D==null) painter2D = new GL3Painter2D(1,1,0);
        painter2D.setSize(new Extent.Long((int)size.get(0), (int)size.get(1)));
        painterScene.getNodeTransform().getScale().setXY(1,-1);
        painterScene.getNodeTransform().getTranslation().setXY(0, size.get(1));
        painterScene.getNodeTransform().notifyChanged();
        painter2D.flush(null,source);

        //render the frame decoration
        Margin margin = null;
        if(decoration instanceof WFrameDecoration){

            if(decorationView==null){
                final WFrameDecoration deco = (WFrameDecoration) decoration;
                decorationView = new WGLView(1,1,nbSample,source,decorationLink);
                final WContainer decoContainer = decorationView.getContainer();
                decoContainer.getStyle().getSelfRule().setProperties(new Chars("background:none;border:none;"));
                decoContainer.setLayout(new BorderLayout());
                decoContainer.addChild(deco.getWidget(), BorderConstraint.CENTER);
                decoContainer.setFrame(this);
            }

            //ensure the viewport in for the full frame
            source.getGL().asGL1().glViewport(0, 0, (int)size.get(0),(int)size.get(1));

            decorationView.setExtent(size);
            painter2D.render(decorationNode);
            painter2D.flush(null,source);

            //update the context, reducing view rectangle by decoration margins
            final GLProcessContext context = getContext();
            final Rectangle viewRect = context.getViewRectangle();
            final Rectangle glRectangle = context.getGLRectangle();
            margin = decoration.getMargin();
            //decoration margins are in top->down coordinate system
            //view rectangle is in bottom->up coordinate system
            viewRect.setX(glRectangle.getX()+margin.left);
            viewRect.setY(glRectangle.getY()+margin.bottom);
            viewRect.setWidth(glRectangle.getWidth()-margin.left-margin.right);
            viewRect.setHeight(glRectangle.getHeight()-margin.top-margin.bottom);
        }

        //render the main container
        if(containerView!=null && !containerView.isGLInitialized()){
            containerView.configureGL(nbSample, source, containerLink);
            containerView.getContainer().setFrame(this);
        }

        if(containerView!=null){
            //ensure the viewport in for the full frame
            source.getGL().asGL1().glViewport(0, 0, (int)size.get(0),(int)size.get(1));

            if(containerVisible) {
                if(margin!=null){
                    containerView.setExtent(new Extent.Double(
                            size.get(0)-margin.left-margin.right,
                            size.get(1)-margin.top-margin.bottom));
                    containerNode.getNodeTransform().setToTranslation(new double[]{margin.left,margin.top});
                }else{
                    containerView.setExtent(size);
                }
                painter2D.render(containerNode);
                painter2D.flush(null,source);

                //render possible opengl elements
                containerView.renderGL(painter2D.renderContext);
            }
        }

        //render the cursor
        if(currentCursor instanceof ImageCursor){
            //ensure the viewport in for the full frame
            source.getGL().asGL1().glViewport(0, 0, (int)size.get(0),(int)size.get(1));

            final ImageCursor cur = (ImageCursor) currentCursor;
            final Image img = cur.getImage();
            cursorNode.setImage(img);

            final Tuple pick = cur.getPick();
            final Vector imgCoord = cursorPos.copy();
            imgCoord.setX(cursorPos.get(0)-pick.get(0));
            imgCoord.setY(cursorPos.get(1)-pick.get(1));

            cursorNode.getNodeTransform().getTranslation().set(imgCoord);
            cursorNode.getNodeTransform().notifyChanged();

            painter2D.render(cursorNode);
            painter2D.flush(null,source);
        }

    }

    /**
     * Returns true if frame has a modal child frame.
     * @return
     */
    private boolean isLocked(){
        int n=children.getSize();
        if(n==0) return false;
        for(n--;n>=0;n--){
            if(((UIFrame)children.get(n)).isModale()) return true;
        }
        return false;
    }

}
