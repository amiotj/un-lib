package science.unlicense.impl.game.navmap;

import science.unlicense.api.game.navmap.NavigationMap;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.geometry.s3d.Grid;
import science.unlicense.api.math.Maths;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.math.Vector;

/**
 * Navigation map based on a grid.
 *
 * @author Johann Sorel
 */
public class GridNavMap implements NavigationMap {

    private final Grid navMap;
    private final TupleBuffer navMapSm;
    private final boolean[] sample = new boolean[1];
    private final Affine3 sceneToNavMap;
    private final Affine3 navMapToScene;
    private double groundDistance = -1;

    public GridNavMap(Grid navMap) {
        this.navMap = navMap;
        this.navMapSm = navMap.getImage().getRawModel().asTupleBuffer(navMap.getImage());
        this.sceneToNavMap = navMap.getGeomToGrid();
        this.navMapToScene = sceneToNavMap.invert();
    }

    public void setGroundDistance(double groundDistance) {
        this.groundDistance = groundDistance;
    }

    public double getGroundDistance() {
        return groundDistance;
    }

    private double getHeightAtGrid(final int[] gridCoord) {
        int inc = 0;
        if (navMapSm.getTupleBoolean(gridCoord, sample)[0]) {
            //search above
            while (navMapSm.getTupleBoolean(gridCoord, sample)[0] && inc < 10) {
                gridCoord[1]++;
                clampCoord(gridCoord);
                inc++;
            }
        } else {
            //search under
            while (!navMapSm.getTupleBoolean(gridCoord, sample)[0] && inc < 10) {
                gridCoord[1]--;
                clampCoord(gridCoord);
                inc++;
            }
        }

        final double[] geoCoord = navMapToScene.transform(new double[]{gridCoord[0], gridCoord[1], gridCoord[2]},null);
        return geoCoord[1];
    }

    private int[] toNavCoord(Tuple position) {
        position = sceneToNavMap.transform(position, null);
        int[] navMapCoord = new int[]{(int) position.getX(), (int) position.getY(), (int) position.getZ()};
        return clampCoord(navMapCoord);
    }

    private int[] clampCoord(int[] navMapCoord) {
        for (int i = 0; i < 3; i++) {
            navMapCoord[i] = Maths.clamp(navMapCoord[i], 0, (int)navMapSm.getExtent().get(i) - 1);
        }
        return navMapCoord;
    }

    private double getValue(double[][] matrix, double x, double z) {
        final double[] seq = new double[4];
        seq[0] = getValue(matrix[0], z);
        seq[1] = getValue(matrix[1], z);
        seq[2] = getValue(matrix[2], z);
        seq[3] = getValue(matrix[3], z);
        return getValue(seq, x);
    }

    private static double getValue(double[] points, double x) {
        return points[1] + 0.5 * x * (points[2] - points[0] + x * (2.0 * points[0] - 5.0 * points[1] + 4.0 * points[2] - points[3] + x * (3.0 * (points[1] - points[2]) + points[3] - points[0])));
    }

    public Vector navigate(Vector start, Vector direction, Vector buffer) {
        if(buffer==null) buffer = start.copy();
        
        final Vector position = new Vector(start).localAdd(direction);
        buffer.set(position);
        int[] navMapCoord = toNavCoord(position);

        navMapSm.getTupleBoolean(navMapCoord, sample);

        if (sample[0]) {
            //invalid position, collision
            final Vector lastPosition = new Vector(start);
            //try to calculate a movement anyway, until we reach the collision
            //or at least the movement on the other axis
            buffer.set(lastPosition);

            for (int i = 0; i < 3; i++) {
                buffer.set(i, position.get(i));
                navMapCoord = toNavCoord(buffer);
                navMapSm.getTupleBoolean(navMapCoord, sample);
                if (sample[0]) {
                    //can't fix this axis translation
                    buffer.set(i, lastPosition.get(i));
                }
            }
        }

        if (groundDistance >= 0) {
            //find the first collision under current location

            while (!sample[0] && navMapCoord[1] > 0) {
                navMapCoord[1]--;
                navMapSm.getTupleBoolean(navMapCoord, sample);
            }

            final double[][] nieghbor = new double[4][4];
            for (int x = 0; x < nieghbor[0].length; x++) {
                for (int z = 0; z < nieghbor[1].length; z++) {
                    nieghbor[x][z] = getHeightAtGrid(new int[]{
                        navMapCoord[0] - 2 + x,
                        navMapCoord[1],
                        navMapCoord[2] - 2 + z}
                    );
                }
            }

            final TupleRW navCoordD = sceneToNavMap.transform(buffer, null);
            navCoordD.setX((navCoordD.getX() - (navMapCoord[0] - 2)) / 4.0);
            navCoordD.setZ((navCoordD.getZ() - (navMapCoord[2] - 2)) / 4.0);

            final double z = getValue(nieghbor, navCoordD.getX(), navCoordD.getZ()) + groundDistance;
            buffer.setZ(z);
        }
        
        return buffer;
    }

}
