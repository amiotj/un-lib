
package science.unlicense.impl.game.navmap;

import static science.unlicense.api.gpu.opengl.GLC.*;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.game.navmap.NavigationMap;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Geometry;
import science.unlicense.api.geometry.coordsys.Axis;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.CoordinateSystems;
import science.unlicense.api.geometry.coordsys.Direction;
import science.unlicense.api.geometry.index.IndexRecord;
import science.unlicense.api.geometry.index.QuadTree;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.api.math.transform.Transform;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.geometry.Point;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.geometry.s3d.Ray;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * @author Johann Sorel
 */
public class MeshNavMap implements NavigationMap{
    
    private final Shell shell;
    private final CoordinateSystem quadCs;
    private final QuadTree quadtree;
    private final Transform transform;
    private final double[] tempd = new double[2];
    private final Vector tempv = new Vector(tempd);
    
    public MeshNavMap(Mesh mesh) {
        this(mesh.getCoordinateSystem(), (Shell)mesh.getShape());
    }
    
    public MeshNavMap(CoordinateSystem cs, Shell shell){
        this.shell = shell;
        
        //we build a coordinate system with only the right and forward axis
        final Axis[] axis = cs.getAxis();
        final Axis[] quadAxis = new Axis[2];
        for(int i=0;i<axis.length;i++){
            if(Direction.FORWARD.isCompatible(axis[i].getDirection())!=0){
                quadAxis[0] = axis[i];
            }else if(Direction.RIGHT.isCompatible(axis[i].getDirection())!=0){
                quadAxis[1] = axis[i];
            }
        }
        this.quadCs = new CoordinateSystem(quadAxis);
        this.transform = CoordinateSystems.createTransform(cs, quadCs);
        
        final BBox bbox = shell.getBBox();
        final BBox b = new BBox(2);
        transform.transform(bbox.getLower(), b.getLower());
        b.getUpper().set(b.getLower());
        b.expand(transform.transform(bbox.getUpper(),null));
        
        quadtree = new QuadTree(2, 8, b);
        
        final IBO index = shell.getIndexes();
        final VBO vertices = shell.getVertices();
        for(IndexRange r : shell.getModes()){
            //TODO handle other kinds of triangles structures
            if(r.getMode()!=GL_TRIANGLES) continue;
            
            final int offset = r.getIndexOffset();
            for(int i=0,n=r.getCount();i<n;i+=3){
                final int idx0 = index.getTuple(i+offset+0);
                final int idx1 = index.getTuple(i+offset+1);
                final int idx2 = index.getTuple(i+offset+2);
                final float[] t0 = vertices.getTupleFloat(idx0, null);
                final float[] t1 = vertices.getTupleFloat(idx1, null);
                final float[] t2 = vertices.getTupleFloat(idx2, null);
                final Triangle triangle = new Triangle(new Vector(t0), new Vector(t1), new Vector(t2));
                final IndexRecord rec = new IndexRecord(2, triangle);                
                transform.transform(triangle.getFirstCoord().getValues(), tempd);
                rec.getLower().set(tempd);
                rec.getUpper().set(tempd);
                rec.expand(transform.transform(triangle.getSecondCoord().getValues(), tempd));
                rec.expand(transform.transform(triangle.getThirdCoord().getValues(), tempd));
                
                quadtree.add(rec);
            }
        }
    }
    
    
    public Vector navigate(Vector start, Vector direction, Vector buffer) {
        buffer.set(start);
        buffer.localAdd(direction);
        
        final Ray ray = new Ray(buffer.add(new Vector(0, +1, 0)), new Vector(0, -1, 0));
        
        
        final BBox search = new BBox(2);
        transform.transform(buffer.getValues(), tempd);
        search.getLower().set(tempd);
        search.getUpper().set(tempd);
        
        boolean match = false;
        final Sequence result = quadtree.search(search);
        for(int i=0,n=result.getSize();i<n;i++){
            final IndexRecord rec = (IndexRecord) result.get(i);
            final Triangle triangle = (Triangle) rec.getValue();
            final Geometry intersection;
            try {
                intersection = triangle.intersection(ray);
                if(intersection!=null && intersection.getCentroid().distance(new Point(buffer))<0.5){
                    buffer.set(intersection.getCentroid().getCoordinate());
                    match = true;
                    break;
                }
            } catch (OperationException ex) {
                //TODO : raise exception in api ? or log ? or null return value ?
                ex.printStackTrace();
            }
        }
        
        if(!match){
            buffer.set(start);
        }
        
        return buffer;
    }

}
