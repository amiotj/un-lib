

package science.unlicense.impl.game.phase;

import science.unlicense.api.CObjects;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.CollectionMessage;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.event.Event;
import science.unlicense.api.event.EventListener;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.event.PropertyMessage;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GL;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.number.Int32;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.api.model.tree.DefaultNodeVisitor;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.NodeVisitor;
import science.unlicense.engine.opengl.GLExecutable;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.light.Light;
import science.unlicense.engine.opengl.light.ShadowMap;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.engine.opengl.phase.PhaseSequence;
import science.unlicense.engine.opengl.phase.TasksPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.BloomPhase;
import science.unlicense.engine.opengl.phase.effect.DOFPhase;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.engine.opengl.phase.effect.FXAAPhase;
import science.unlicense.engine.opengl.phase.effect.FastGaussianBlurPhase;
import science.unlicense.engine.opengl.phase.effect.GammaCorrectionPhase;
import science.unlicense.engine.opengl.phase.effect.LightPhase;
import science.unlicense.engine.opengl.phase.effect.SSAOBlendPhase;
import science.unlicense.engine.opengl.phase.effect.SSAOPhase;
import science.unlicense.engine.opengl.phase.picking.PickResetPhase;
import science.unlicense.engine.opengl.phase.reflection.ReflectionPhase;
import science.unlicense.engine.opengl.phase.shadow.LightspacePerspectiveShadowMapsPhase;
import science.unlicense.engine.opengl.physic.SceneWorld;
import science.unlicense.engine.opengl.physic.WorldUpdatePhase;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.image.process.ConvolutionMatrices;
import science.unlicense.impl.image.process.ConvolutionMatrix;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLState;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.FBOAttachment;
import science.unlicense.impl.gpu.opengl.resource.Texture;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.Texture2DMS;

/**
 * Simple game rendering phases.
 * Store all effects and phases parameters including :
 * - Resolution
 * - RGB/RGBA(Alpha) switch
 * - MSAA
 * - FXAA
 * - SSAO
 * - Picking
 * - Depth of Field
 * - Bloom
 * - Shadows
 * - Reflections
 * - Custom post effects
 * - User interface layer
 * 
 * TODO
 * - World physics
 * 
 * @author Johann Sorel
 */
public class GamePhases extends PhaseSequence implements EventSource {
    
    public static final Chars ANTIALIAZING_NONE = new Chars("NONE");
    public static final Chars ANTIALIAZING_FXAA = new Chars("FXAA");
    public static final Chars ANTIALIAZING_MSAA = new Chars("MSAA");
    public static Chars[] ANTIALIAZING;
    static {
        ANTIALIAZING = new Chars[]{ANTIALIAZING_NONE,ANTIALIAZING_FXAA};
        //find max multisampling
        final GLSource gd = GLUtilities.createOffscreenSource(1, 1);
        gd.getCallbacks().add(new GLCallback() {
            public void dispose(GLSource glad) {}
            public void execute(GLSource glad) {
                final int max = GLState.getInteger(glad.getGL(), GLC.GETSET.MAX_SAMPLES);
                for(int i=2;i<=max;i=i*2){
                    ANTIALIAZING = (Chars[]) Arrays.insert(ANTIALIAZING, 
                            ANTIALIAZING.length, ANTIALIAZING_MSAA.concat(' ').concat(Int32.encode(i)));
                }
            }
        });
        gd.render();
        gd.dispose();
    }
    
    public static final Chars PROPERTY_RESOLUTION_WIDTH     = new Chars("ResolutionWidth");
    public static final Chars PROPERTY_RESOLUTION_HEIGHT    = new Chars("ResolutionHeight");
    public static final Chars PROPERTY_ALPHA                = new Chars("AlphaEnable");
    public static final Chars PROPERTY_SSAO                 = new Chars("SsaoEnable");
    public static final Chars PROPERTY_SSAO_RADIUS          = new Chars("SsaoRadius");
    public static final Chars PROPERTY_SSAO_INTENSITY       = new Chars("SsaoIntensity");
    public static final Chars PROPERTY_SSAO_SCALE           = new Chars("SsaoScale");
    public static final Chars PROPERTY_SSAO_BIAS            = new Chars("SsaoBias");
    public static final Chars PROPERTY_DOF                  = new Chars("DofEnable");
    public static final Chars PROPERTY_DOF_MIN              = new Chars("DofFocalMinPlane");
    public static final Chars PROPERTY_DOF_MAX              = new Chars("DofFocalMaxPlane");
    public static final Chars PROPERTY_DOF_RADIUS           = new Chars("DofBlurRadius");
    public static final Chars PROPERTY_AASTATE              = new Chars("AAState");
    public static final Chars PROPERTY_BLOOM                = new Chars("BloomEnable");
    public static final Chars PROPERTY_BLOOM_VALUE          = new Chars("BloomValue");
    public static final Chars PROPERTY_SHADOW               = new Chars("ShadowEnable");
    public static final Chars PROPERTY_SHADOW_TEXTURE_SIZE  = new Chars("ShadowTextureSize");
    public static final Chars PROPERTY_DEFERRED_LIGHT       = new Chars("DeferredLight");
    public static final Chars PROPERTY_GAMMA_VALUE          = new Chars("GammaValue");
    
    private static final NodeVisitor REMOVE_SHADOWMAP = new DefaultNodeVisitor(){
        public Object visit(Node node, Object context) {
            if(node instanceof Light){
                final Light light = (Light) node;
                final ShadowMap sm = light.getShadowMap();
                if(sm!=null){
                    final GLProcessContext glpc = (GLProcessContext) context;
                    sm.getFbo().unloadFromGpuMemory(glpc.getGL());
                    sm.getFbo().unloadFromSystemMemory(glpc.getGL());
                    light.setShadowMap(null);
                }
            }
            return super.visit(node, context);
        }
    };
    
    private final EventListener postEffectListener = new EventListener() {
        public void receiveEvent(Event event) {
            if(event.getMessage() instanceof CollectionMessage){
                final CollectionMessage ce = (CollectionMessage) event.getMessage();
                for(Object o : ce.getNewElements()){
                    ((Phase)o).addEventListener(Predicate.TRUE, postEffectListener);
                }
                for(Object o : ce.getOldElements()){
                    ((Phase)o).removeEventListener(Predicate.TRUE, postEffectListener);
                }
            }
            update();
        }
    };
    
    private final EventManager manager = new EventManager();

    private GLNode scene      = new GLNode();
    private CameraMono camera = new CameraMono();
    private final SceneWorld world = new SceneWorld(scene);
    private final Extent size = new Extent.Double(1024, 768);
    
    //world update phase
    private final FBOResizePhase resizePhase;
    private final UpdatePhase updatePhase;
    private final TasksPhase tasksPhase;
    private final WorldUpdatePhase physicPhase;
        
    //shadow map update phase
    private final LightspacePerspectiveShadowMapsPhase shadowPhase;
    
    //reflection update phase
    private final ReflectionPhase reflectionPhase;
    
    //main rendering phase
    private final ClearPhase clearRenderFboMsPhase;
    private final ClearPhase clearRenderFboPhase;
    private final DeferredRenderPhase renderPhaseMS;
    private final PickResetPhase pickingPhase;
    private final GBO renderFboMs;
    private final GBO renderFbo;
    private final Texture diffuseTex;
    private final Texture specularTex;
    private final Texture positionWorldTex;
    private final Texture normalWorldTex;
    private final Texture2D pickMeshTex;
    private final Texture2D pickVertexTex;
    
    //lights phase
    private final ClearPhase clearLightFboPhase;
    private final LightPhase lightPhase;
    private final FBO lightFbo;
    private final Texture colorTex;
    
    //FXAA phase
    private final FXAAPhase fxaaPhase;
    private final FBO fxaaFbo;
    private final Texture fxaaTex;
    
    
    private final DirectPhase paintPhase;
    
    //ssao phase
    private final ClearPhase clearSsaoFboPhase;
    private final ClearPhase clearBlendFboPhase;
    private final SSAOPhase ssaoPhase;
    private final SSAOBlendPhase blendPhase;
    private final FBO ssaoFbo;
    private final FBO blendFbo;
    private final Texture ssaoTex;
    private final Texture blendTex;
        
    //dof phase
    private final ClearPhase clearVBlurFboPhase;
    private final ClearPhase clearHBlurFboPhase;
    private final ClearPhase clearDofFboPhase;
    private final FastGaussianBlurPhase vBlurPhase;
    private final FastGaussianBlurPhase hBlurPhase;
    private final DOFPhase dofPhase;
    private final FBO vblurFbo;
    private final FBO hblurFbo;
    private final FBO dofFbo;
    private final Texture vblurTex;
    private final Texture hblurTex;
    private final Texture dofTex;
    
    //bloom phase
    private final ClearPhase clearBloomFboPhase;
    private final BloomPhase bloomPhase;
    private final FBO bloomFbo;
    private final Texture bloomTex;

    //gamma phase
    private final ClearPhase clearGammaFboPhase;
    private final GammaCorrectionPhase gammaPhase;
    private final FBO gammaFbo;
    private final Texture gammaTex;

    //custom posteffect phases
    private final PhaseSequence postEffects = new PhaseSequence();

    //pipeline state
    private int resolutionWidth = -1;
    private int resolutionHeight = -1;
    private boolean alphaEnable = true;
    private final int subSampling = 2;
    private Chars aaState = ANTIALIAZING_NONE;
    private boolean ssaoEnable = true;
    private float ssaoRadius = 0.3f;
    private float ssaoIntensity = 2.0f;
    private float ssaoScale = 1.0f;
    private float ssaoBias = 0.5f;
    private boolean dofEnable = true;
    private float dofMinPlane = 5f;
    private float dofMaxPlane = 15f;
    private float dofBlurRadius = 4f;
    private boolean bloomEnable = true;
    private float bloomValue = 0.1f;
    private boolean shadowEnable = false;
    private int shadowTextureSize = 512;
    private boolean deferredLight = false;
    private float gammaValue = 2.2f;
    
    
    public GamePhases() {
        
        scene.getChildren().add(camera);
        
        final int width = (int) size.get(0);
        final int height = (int) size.get(1);
        
        //build phases
        resizePhase = new FBOResizePhase(new Chars("Resizing FBOs")); 
        updatePhase = new UpdatePhase(new Chars("Update Scene"),scene);
        tasksPhase = new TasksPhase(new Chars("Tasks"));
        physicPhase = new WorldUpdatePhase(new Chars("Update physics"),world);
        
        getPhases().add(resizePhase);
        getPhases().add(updatePhase);
        getPhases().add(tasksPhase);
        getPhases().add(physicPhase);
        physicPhase.setEnable(false);
        
        
        //shadow phase
        shadowPhase = new LightspacePerspectiveShadowMapsPhase(new Chars("Compute shadows"),scene, camera, shadowTextureSize);
        getPhases().add(shadowPhase);
                
        
        //main rendering phase
        renderFboMs         = new GBO(width, height, subSampling);
        renderFbo           = renderFboMs.createBlitFBO();
        diffuseTex          = renderFbo.getDiffuseTexture();
        specularTex         = renderFbo.getSpecularTexture();
        positionWorldTex    = renderFbo.getPositionTexture();
        normalWorldTex      = renderFbo.getNormalTexture();
        pickMeshTex         = (Texture2D) renderFbo.getMeshIdTexture();
        pickVertexTex       = (Texture2D) renderFbo.getVertexIdTexture();


        clearRenderFboMsPhase = new ClearPhase(renderFboMs);
        clearRenderFboMsPhase.setId(new Chars("Clear Render FBO MS"));
        clearRenderFboPhase = new ClearPhase(renderFbo);
        clearRenderFboPhase.setId(new Chars("Clear Render FBO"));
        renderPhaseMS = new DeferredRenderPhase(scene, camera, renderFboMs);
        renderPhaseMS.setBlitFbo(renderFbo);
        renderPhaseMS.setId(new Chars("Rendering"));
        pickingPhase = new PickResetPhase(pickMeshTex, pickVertexTex);
        pickingPhase.setId(new Chars("Picking"));

        
        //reflection phase, needs the main render phase informations
        reflectionPhase = new ReflectionPhase(renderPhaseMS);
        reflectionPhase.setId(new Chars("Compute reflection"));
        getPhases().add(reflectionPhase);

        getPhases().add(clearRenderFboMsPhase);
        getPhases().add(clearRenderFboPhase);
        getPhases().add(renderPhaseMS);
        getPhases().add(pickingPhase);
          
        
        // lightning phase
        colorTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        lightFbo = new FBO(width, height);
        lightFbo.addAttachment(new FBOAttachment(GLC.FBO.Attachment.COLOR_0, colorTex));
        lightPhase = new LightPhase(lightFbo, diffuseTex, specularTex, positionWorldTex, normalWorldTex, scene, camera);  
        lightPhase.setId(new Chars("Deferred lightning"));      
        clearLightFboPhase = new ClearPhase(lightFbo);
        clearLightFboPhase.setId(new Chars("Clear lights FBO"));
        getPhases().add(clearLightFboPhase);
        getPhases().add(lightPhase);
        
        
        // FXAA phase
        fxaaFbo = new FBO(width, height);
        fxaaTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        fxaaFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, fxaaTex);        
        fxaaPhase = new FXAAPhase(fxaaFbo,diffuseTex);
        fxaaPhase.setId(new Chars("Compute FXAA"));
        getPhases().add(fxaaPhase);
        
        
        //ssao phase
        ssaoTex = new Texture2D(width, height, Texture2D.VEC1_FLOAT());
        ssaoFbo = new FBO(width, height);
        ssaoFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, ssaoTex);
        clearSsaoFboPhase = new ClearPhase(ssaoFbo);
        clearSsaoFboPhase.setId(new Chars("Clear SSAO FBO"));
        ssaoPhase = new SSAOPhase(ssaoFbo, camera, positionWorldTex, normalWorldTex);
        ssaoPhase.setId(new Chars("Compute SSAO"));
        
        blendTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        blendFbo = new FBO(width, height);
        blendFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, blendTex);
        clearBlendFboPhase = new ClearPhase(blendFbo);
        clearBlendFboPhase.setId(new Chars("Clear blending SSAO FBO"));
        blendPhase = new SSAOBlendPhase(blendFbo, diffuseTex, ssaoTex);
        blendPhase.setId(new Chars("Blend SSAO"));
        
        getPhases().add(clearSsaoFboPhase);
        getPhases().add(ssaoPhase);
        getPhases().add(clearBlendFboPhase);
        getPhases().add(blendPhase);
        
        //depth of field phase
        vblurTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        hblurTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        dofTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());        
        vblurFbo = new FBO(width, height);
        hblurFbo = new FBO(width, height);
        dofFbo = new FBO(width, height);
        vblurFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, vblurTex);
        hblurFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, hblurTex);
        dofFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, dofTex);
        
        final ConvolutionMatrix gauss = ConvolutionMatrices.createGaussian((int)dofBlurRadius, 6);
        clearVBlurFboPhase = new ClearPhase(vblurFbo);
        clearVBlurFboPhase.setId(new Chars("Clear vertical blur FBO"));
        clearHBlurFboPhase = new ClearPhase(hblurFbo);
        clearHBlurFboPhase.setId(new Chars("Clear horizontal blur FBO"));
        clearDofFboPhase = new ClearPhase(dofFbo);       
        clearDofFboPhase.setId(new Chars("Clear DOF FBO"));         
        vBlurPhase = new FastGaussianBlurPhase(vblurFbo,blendTex,gauss,false);
        vBlurPhase.setId(new Chars("Compute vertical blur"));
        hBlurPhase = new FastGaussianBlurPhase(hblurFbo,vblurTex,gauss,true);
        hBlurPhase.setId(new Chars("Compute horizontal blur"));
        dofPhase = new DOFPhase(dofFbo,blendTex,positionWorldTex,hblurTex,1f,dofMinPlane,dofMaxPlane,50f,20f,camera);
        dofPhase.setId(new Chars("Compute DOF"));
        
        getPhases().add(clearVBlurFboPhase);
        getPhases().add(vBlurPhase);
        getPhases().add(clearHBlurFboPhase);
        getPhases().add(hBlurPhase);
        getPhases().add(clearDofFboPhase);
        getPhases().add(dofPhase);
        
        //bloom phase
        bloomTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());   
        bloomFbo = new FBO(width, height);
        bloomFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, bloomTex);
        clearBloomFboPhase = new ClearPhase(bloomFbo);
        clearBloomFboPhase.setId(new Chars("Clear Bloom FBO"));
        bloomPhase = new BloomPhase(bloomFbo, dofTex, (float)bloomValue);
        bloomPhase.setId(new Chars("Compute Bloom"));
        
        getPhases().add(clearBloomFboPhase);
        getPhases().add(bloomPhase);

        //gamma phase
        gammaTex = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        gammaFbo = new FBO(width, height);
        gammaFbo.addAttachment(GLC.FBO.Attachment.COLOR_0, gammaTex);
        clearGammaFboPhase = new ClearPhase(gammaFbo);
        clearGammaFboPhase.setId(new Chars("Clear Gamma FBO"));
        gammaPhase = new GammaCorrectionPhase(gammaFbo, bloomTex, new float[]{gammaValue,gammaValue,gammaValue});
        gammaPhase.setId(new Chars("Compute Gamma"));

        getPhases().add(clearGammaFboPhase);
        getPhases().add(gammaPhase);

        //last we render the texture on output
        paintPhase = new DirectPhase(gammaTex);
        paintPhase.setId(new Chars("Paint to output"));
        getPhases().add(paintPhase);
        
        
        //list to any change in post effect phases
        postEffects.getPhases().addEventListener(CollectionMessage.PREDICATE, postEffectListener);
        getPhases().add(postEffects);
        
        //update pipeline to configure default values
        update();
    }
    
    public PickResetPhase getPickingPhase() {
        return pickingPhase;
    }

    public DirectPhase getPaintPhase() {
        return paintPhase;
    }

    public Sequence getPostEffects() {
        return postEffects.getPhases();
    }

    public WorldUpdatePhase getPhysicPhase() {
        return physicPhase;
    }
    
    private void update(){
        
        Texture finalTex = diffuseTex;
        
        //RESOLUTION
        resizePhase.setResizeWidth(resolutionWidth);
        resizePhase.setResizeHeight(resolutionHeight);
        
        //ALPHA
        tasksPhase.addTask(new GLExecutable() {
            public Object execute() {
                final GL gl = context.getGL();
                
                //unload fbo
                renderFboMs.unloadFromGpuMemory(context.getGL());
                renderFbo.unloadFromGpuMemory(context.getGL());
                lightFbo.unloadFromGpuMemory(context.getGL());
                fxaaFbo.unloadFromGpuMemory(context.getGL());
                blendFbo.unloadFromGpuMemory(context.getGL());
                vblurFbo.unloadFromGpuMemory(context.getGL());
                hblurFbo.unloadFromGpuMemory(context.getGL());
                dofFbo.unloadFromGpuMemory(context.getGL());
                bloomFbo.unloadFromGpuMemory(context.getGL());
                
                //update textures formats
                if(alphaEnable){
                    renderFboMs.getTexture(GLC.FBO.Attachment.COLOR_0).reformat(gl, (subSampling>0) ? Texture2DMS.COLOR_RGBA() : Texture2D.COLOR_RGBA_CLAMPED());
                    diffuseTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                    colorTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                    fxaaTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                    blendTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                    vblurTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                    hblurTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                    dofTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());     
                    bloomTex.reformat(gl, Texture2D.COLOR_RGBA_CLAMPED());
                }else{
                    renderFboMs.getTexture(GLC.FBO.Attachment.COLOR_0).reformat(gl, (subSampling>0) ? Texture2DMS.COLOR_RGB() : Texture2D.COLOR_RGB_CLAMPED());
                    diffuseTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                    colorTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                    fxaaTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                    blendTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                    vblurTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                    hblurTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                    dofTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());     
                    bloomTex.reformat(gl, Texture2D.COLOR_RGB_CLAMPED());
                }
                
                
                return null;
            }
        });
        
        
        //SHADOW
        shadowPhase.setEnable(shadowEnable);
        shadowPhase.setTextureSize(shadowTextureSize);
        if(!shadowEnable){
            tasksPhase.addTask(new GLExecutable() {
                public Object execute() {
                    System.out.println("removing shadow maps");
                    scene.accept(REMOVE_SHADOWMAP, context);
                    return null;
                }
            });
        }
        
        //MSAA or FXAA
        if(aaState==ANTIALIAZING_FXAA){
            renderPhaseMS.setOutputFbo(renderFbo);
            renderPhaseMS.setBlitFbo(null);
            fxaaPhase.setEnable(true);
            finalTex = fxaaTex;
            
        }else if(aaState.startsWith(ANTIALIAZING_MSAA)){            
            final int samples = Int32.decode(aaState.truncate(5, aaState.getCharLength()));
            renderPhaseMS.setOutputFbo(renderFboMs);
            renderPhaseMS.setBlitFbo(renderFbo);
            tasksPhase.addTask(new GLExecutable() {
                public Object execute() {
                    renderFboMs.resample(context.getGL(), samples);
                    return null;
                }
            });
            fxaaPhase.setEnable(false);
            
        }else{
            renderPhaseMS.setOutputFbo(renderFbo);
            renderPhaseMS.setBlitFbo(null);
            fxaaPhase.setEnable(false);
        }
               
        
        //LIGHT
        //TODO loop on mesh renderers to add set light ?
        lightPhase.setEnable(deferredLight);
        clearLightFboPhase.setEnable(deferredLight);
        if(deferredLight){
            lightPhase.getTextures().replace(0,finalTex);
            finalTex = colorTex;
        }
        
        //TODO we should not need this, something is wrong
        //blit isn't correctly working, it keeps a lock on the fbo
        tasksPhase.addTask(new GLExecutable() {
            public Object execute() {
                renderFbo.unloadFromGpuMemory(context.getGL());
                return null;
            }
        });
                
        //SSAO
        clearSsaoFboPhase.setEnable(ssaoEnable);
        ssaoPhase.setEnable(ssaoEnable);
        clearBlendFboPhase.setEnable(ssaoEnable);
        blendPhase.setEnable(ssaoEnable);
        blendPhase.getTextures().replace(0,finalTex);
        ssaoPhase.setKernelRadius(ssaoRadius);
        ssaoPhase.setIntensity(ssaoIntensity);
        ssaoPhase.setScale(ssaoScale);
        ssaoPhase.setBias(ssaoBias);
        if(ssaoEnable){
            finalTex = blendTex;
        }
        
        //DOF
        final ConvolutionMatrix gauss = ConvolutionMatrices.createGaussian((int)dofBlurRadius, 6);
        vBlurPhase.setGaussianValues(gauss);
        hBlurPhase.setGaussianValues(gauss);
        dofPhase.setFocalPlaneDepth(dofMinPlane,dofMaxPlane);
        
        clearVBlurFboPhase.setEnable(dofEnable);
        vBlurPhase.setEnable(dofEnable);
        clearHBlurFboPhase.setEnable(dofEnable);
        hBlurPhase.setEnable(dofEnable);
        clearDofFboPhase.setEnable(dofEnable);
        dofPhase.setEnable(dofEnable);
        if(dofEnable){
            vBlurPhase.getTextures().replace(0,finalTex);
            dofPhase.getTextures().replace(0,finalTex);
            finalTex = dofTex;
        }
        
        //BLOOM
        clearBloomFboPhase.setEnable(bloomEnable);
        bloomPhase.setEnable(bloomEnable);
        bloomPhase.setBloomFactor((float)bloomValue);
        if(bloomEnable){
            bloomPhase.getTextures().replace(0,finalTex);
            finalTex = bloomTex;
        }

        //GAMMA
        final boolean gammaEnable = gammaValue != 1.0f;
        clearGammaFboPhase.setEnable(gammaEnable);
        gammaPhase.setEnable(gammaEnable);
        gammaPhase.setGammaFactor(new float[]{gammaValue,gammaValue,gammaValue});
        if(bloomEnable){
            gammaPhase.getTextures().replace(0,finalTex);
            finalTex = gammaTex;
        }
        
        //other post effects
        for(int i=0,n=postEffects.getPhases().getSize();i<n;i++){
            final GamePostEffectPhase postPhase = (GamePostEffectPhase) postEffects.getPhases().get(i);
            if(postPhase.isEnable()){
                finalTex = postPhase.update(this, finalTex);
            }
        }
        
        paintPhase.getTextures().replace(0,finalTex);
    }

    public Extent getSize() {
        return new Extent.Double(
                renderFbo.getWidth(),
                renderFbo.getHeight());
    }
    
    public GLNode getScene() {
        return scene;
    }
    
    public void setScene(GLNode scene) {
        CObjects.ensureNotNull(scene);
        this.scene = scene;
        updatePhase.setScene(scene);
        shadowPhase.setScene(scene);
        world.setScene(scene);
        renderPhaseMS.setRoot(scene);
        lightPhase.setScene(scene);
    }

    public CameraMono getCamera() {
        return camera;
    }
    
    public void setCamera(CameraMono camera){
        CObjects.ensureNotNull(camera);
        this.camera = camera;
        renderPhaseMS.setCamera(camera);
        lightPhase.setCamera(camera);
        shadowPhase.setCamera(camera);
        ssaoPhase.setCamera(camera);
        dofPhase.setCamera(camera);
    }
    
    /**
     * Set used FBO resolution.
     * Use 0 or negative for dynamic size
     * 
     * @param resolutionWidth 
     */
    public void setResolutionWidth(int resolutionWidth) {
        if(this.resolutionWidth==resolutionWidth) return;
        final int old = this.resolutionWidth;
        this.resolutionWidth = resolutionWidth;
        update();
        manager.sendPropertyEvent(this, PROPERTY_RESOLUTION_WIDTH, old, resolutionWidth);
    }

    public int getResolutionWidth() {
        return resolutionWidth;
    }
    
    /**
     * Set used FBO resolution.
     * Use 0 or negative for dynamic size
     * 
     * @param resolutionHeight
     */
    public void setResolutionHeight(int resolutionHeight) {
        if(this.resolutionHeight==resolutionHeight) return;
        final int old = this.resolutionHeight;
        this.resolutionHeight = resolutionHeight;
        update();
        manager.sendPropertyEvent(this, PROPERTY_RESOLUTION_HEIGHT, old, resolutionHeight);
    }

    public int getResolutionHeight() {
        return resolutionHeight;
    }
    
    public boolean isAlphaEnable() {
        return alphaEnable;
    }

    public void setAlphaEnable(boolean alphaEnable) {
        if(alphaEnable==this.alphaEnable) return;
        this.alphaEnable = alphaEnable;
        update();
        manager.sendPropertyEvent(this, PROPERTY_ALPHA, !alphaEnable, alphaEnable);
    }

    public Chars getAAState() {
        return aaState;
    }

    public void setAAState(Chars aaState) {
        if(aaState.equals(this.aaState)) return;
        final Chars old = this.aaState;
        this.aaState = aaState;
        update();
        manager.sendPropertyEvent(this, PROPERTY_AASTATE, old, aaState);
    }
        
    public boolean isSsaoEnable() {
        return ssaoEnable;
    }

    public void setSsaoEnable(boolean ssaoEnable) {
        if(ssaoEnable==this.ssaoEnable) return;
        this.ssaoEnable = ssaoEnable;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SSAO, !ssaoEnable, ssaoEnable);
    }

    public float getSsaoRadius() {
        return ssaoRadius;
    }

    public void setSsaoRadius(float ssaoRadius) {
        if(this.ssaoRadius==ssaoRadius) return;
        final float old = this.ssaoRadius;
        this.ssaoRadius = ssaoRadius;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SSAO_RADIUS, old, ssaoRadius);
    }

    public float getSsaoIntensity() {
        return ssaoIntensity;
    }

    public void setSsaoIntensity(float ssaoIntensity) {
        if(this.ssaoIntensity==ssaoIntensity) return;
        final float old = this.ssaoIntensity;
        this.ssaoIntensity = ssaoIntensity;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SSAO_INTENSITY, old, ssaoIntensity);
    }

    public float getSsaoScale() {
        return ssaoScale;
    }

    public void setSsaoScale(float ssaoScale) {
        if(this.ssaoScale==ssaoScale) return;
        final float old = this.ssaoScale;
        this.ssaoScale = ssaoScale;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SSAO_SCALE, old, ssaoScale);
    }

    public float getSsaoBias() {
        return ssaoBias;
    }

    public void setSsaoBias(float ssaoBias) {
        if(this.ssaoBias==ssaoBias) return;
        final float old = this.ssaoBias;
        this.ssaoBias = ssaoBias;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SSAO_BIAS, old, ssaoBias);
    }

    public boolean isDofEnable() {
        return dofEnable;
    }

    public void setDofEnable(boolean dofActive) {
        if(dofActive==this.dofEnable) return;
        this.dofEnable = dofActive;
        update();
        manager.sendPropertyEvent(this, PROPERTY_DOF, !dofActive, dofActive);
    }

    public float getDofFocalMinPlane() {
        return dofMinPlane;
    }

    public void setDofFocalMinPlane(float depth) {
        if(this.dofMinPlane==depth) return;
        final float old = this.dofMinPlane;
        this.dofMinPlane = depth;
        update();
        manager.sendPropertyEvent(this, PROPERTY_DOF_MIN, old, depth);
    }
    
    public float getDofFocalMaxPlane() {
        return dofMaxPlane;
    }

    public void setDofFocalMaxPlane(float depth) {
        if(this.dofMaxPlane==depth) return;
        final float old = this.dofMaxPlane;
        this.dofMaxPlane = depth;
        update();
        manager.sendPropertyEvent(this, PROPERTY_DOF_MAX, old, depth);
    }
    
    public float getDofBlurRadius() {
        return dofBlurRadius;
    }

    public void setDofBlurRadius(float radius) {
        if(this.dofBlurRadius==radius) return;
        final float old = this.dofBlurRadius;
        this.dofBlurRadius = radius;
        update();
        manager.sendPropertyEvent(this, PROPERTY_DOF_RADIUS, old, radius);
    }
    
    public boolean isBloomEnable() {
        return bloomEnable;
    }

    public void setBloomEnable(boolean bloomEnable) {
        if(bloomEnable==this.bloomEnable) return;
        this.bloomEnable = bloomEnable;
        update();
        manager.sendPropertyEvent(this, PROPERTY_BLOOM, !bloomEnable, bloomEnable);
    }

    public float getBloomValue() {
        return bloomValue;
    }

    public void setBloomValue(float bloomValue) {
        if(this.bloomValue==bloomValue) return;
        final float old = this.bloomValue;
        this.bloomValue = bloomValue;
        update();
        manager.sendPropertyEvent(this, PROPERTY_BLOOM_VALUE, old, bloomValue);
    }

    public float getGammaValue() {
        return gammaValue;
    }

    public void setGammaValue(float gammaValue) {
        if(this.gammaValue==gammaValue) return;
        final float old = this.gammaValue;
        this.gammaValue = gammaValue;
        update();
        manager.sendPropertyEvent(this, PROPERTY_GAMMA_VALUE, old, gammaValue);
    }

    public boolean isShadowEnable() {
        return shadowEnable;
    }

    public void setShadowEnable(boolean shadowEnable) {
        if(this.shadowEnable==shadowEnable) return;
        this.shadowEnable = shadowEnable;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SHADOW, !shadowEnable, shadowEnable);
    }

    public void setShadowTextureSize(int shadowTextureSize) {
        if(this.shadowTextureSize==shadowTextureSize) return;
        final int old = this.shadowTextureSize;
        this.shadowTextureSize = shadowTextureSize;
        update();
        manager.sendPropertyEvent(this, PROPERTY_SHADOW_TEXTURE_SIZE, old, shadowTextureSize);
    }

    public int getShadowTextureSize() {
        return shadowTextureSize;
    }

    public boolean isDeferredLight() {
        return deferredLight;
    }

    public void setDeferredLight(boolean deferredLight) {
        if(this.deferredLight==deferredLight) return;
        this.deferredLight = deferredLight;
        update();
        manager.sendPropertyEvent(this, PROPERTY_DEFERRED_LIGHT, !deferredLight, deferredLight);
    }
    
    @Override
    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }
    
    @Override
    public void addEventListener(Predicate predicate, EventListener listener) {
        manager.addEventListener(predicate, listener);
    }

    @Override
    public void removeEventListener(Predicate predicate, EventListener listener) {
        manager.removeEventListener(predicate, listener);
    }
    
}