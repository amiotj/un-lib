
package science.unlicense.impl.game.phase;

import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.impl.gpu.opengl.resource.Texture;

/**
 * Phase placed at the end of the rendering phases in the Game phases.
 * 
 * @author Johann Sorel
 */
public interface GamePostEffectPhase extends Phase {

    /**
     * Update this phase.
     * 
     * @param finalTex current result texture which will be rendered in the end.
     * @return replacement final texture
     */
    Texture update(GamePhases phases, Texture finalTex);
    
}
