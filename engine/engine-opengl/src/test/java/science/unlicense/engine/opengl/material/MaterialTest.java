

package science.unlicense.engine.opengl.material;

import science.unlicense.engine.opengl.material.Layer;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.phase.DeferredRenderTest;
import static science.unlicense.engine.opengl.phase.DeferredRenderTest.createQuadMesh;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLTest;

/**
 * Test materials.
 *
 * @author Johann Sorel
 */
public class MaterialTest extends GLTest {

    @Test
    public void diffuseColorTest(){

        final Mesh plan = createQuadMesh();
        final Layer layer = new Layer(new PlainColorMapping(Color.BLUE), Layer.TYPE_DIFFUSE);
        plan.getMaterial().putOrReplaceLayer(layer);

        final Image[] images = DeferredRenderTest.render(plan,100,100);
        final Image imgColor = images[0];
        final Image imgNormal = images[1];
        final Image imgPosition = images[2];
        
        
        final PixelBuffer cm = imgColor.getColorModel().asTupleBuffer(imgColor);
        
        for(int y=0;y<100;y++){
            for(int x=0;x<100;x++){
                final Color rgba = cm.getColor(new int[]{x,y});
                Assert.assertEquals(Color.BLUE, rgba);
            }
        }
    }

    @Test
    public void diffuseTextureTest(){

        //create an image we will use as a texture
        final Image texture = Images.create(new Extent.Long(100, 100),Images.IMAGE_TYPE_RGB);
        final RawModel textsm = texture.getRawModel();
        final PixelBuffer textcm = texture.getColorModel().asTupleBuffer(texture);
        textcm.setColor(new BBox(new double[]{0,0}, new double[]{100,100}), Color.GREEN);

        
        final Mesh plan = createQuadMesh();
        final UVMapping mapping = new UVMapping(new Texture2D(texture));
        final Layer layer = new Layer(mapping, Layer.TYPE_DIFFUSE);
        plan.getMaterial().putOrReplaceLayer(layer);

        final Image[] images = DeferredRenderTest.render(plan,100,100);
        final Image imgColor = images[0];
        final Image imgNormal = images[1];
        final Image imgPosition = images[2];
        
        
        final PixelBuffer cm = imgColor.getColorModel().asTupleBuffer(imgColor);
        
        for(int y=0;y<100;y++){
            for(int x=0;x<100;x++){
                final Color rgba = cm.getColor(new int[]{x,y});
                Assert.assertEquals(Color.GREEN, rgba);
            }
        }
    }

    @Test
    public void normalTextureTest(){

        final float[] normal = new Vector(0.3,0.1,0.6).localNormalize().toArrayFloat();
        //convert from -1:+1 to 0:+1 for image
        final float[] normalAsColor = new float[]{(normal[0]+1)/2,(normal[1]+1)/2,(normal[2]+1)/2};
        
        //create an image we will use as a texture
        final Image texture = Images.create(new Extent.Long(100, 100),Images.IMAGE_TYPE_RGB);
        final PixelBuffer textcm = texture.getColorModel().asTupleBuffer(texture);
        textcm.setColor(new BBox(new double[]{0,0}, new double[]{100,100}), new Color(normalAsColor));

        
        final Mesh plan = createQuadMesh();
        final UVMapping mapping = new UVMapping(new Texture2D(texture));
        final Layer layer = new Layer(mapping, Layer.TYPE_NORMAL);
        plan.getMaterial().putOrReplaceLayer(layer);

        final Image[] images = DeferredRenderTest.render(plan,100,100);
        final Image imgNormal = images[1];
        
        
        final TupleBuffer sm = imgNormal.getRawModel().asTupleBuffer(imgNormal);
        final float[] pixel = new float[3];
        
        for(int y=0;y<100;y++){
            for(int x=0;x<100;x++){
                sm.getTuple(new int[]{x,y}, pixel);
                Assert.assertArrayEquals(normal, pixel,0.1f);
            }
        }
    }
    
}
