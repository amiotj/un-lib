

package science.unlicense.engine.opengl.painter.gl4;

import org.junit.BeforeClass;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.api.painter2d.StrokeTest;
import static science.unlicense.impl.gpu.opengl.GLTest.isGLAvailable;

/**
 *
 * @author Johann Sorel
 */
public class GL4StrokeTest extends StrokeTest{

    @BeforeClass
    public static void beforeClass() {
        org.junit.Assume.assumeTrue(isGLAvailable());
    }
    
    protected ImagePainter2D createPainter(int width, int height) {
        return new GL3ImagePainter2D(width,height);
    }
    
}
