

package science.unlicense.engine.opengl.operation;

import science.unlicense.engine.opengl.operation.GridBuilder;
import org.junit.Test;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.operation.OperationException;
import science.unlicense.impl.geometry.s2d.Triangle;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class GridBuilderTest {
    
    //TODO make a real test case
    //this always test thare are no errors
    @Test
    public void testTriangle() throws OperationException{
        
        final BBox bbox = new BBox(
                new Vector(-355.0950012207031,-23.906999588012695,-355.7409973144531), 
                new Vector(355.7409973144531,303.0920104980469,355.0950012207031));
        final Extent.Long ext = new Extent.Long(100, 100, 100);
        
        final GridBuilder gb = new GridBuilder(bbox, ext);
        
        final Vector v0 = new Vector(-178.85400390625,24.038299560546875,-9.622349739074707);
        final Vector v1 = new Vector(-174.406005859375,24.840700149536133,-9.489870071411133);
        final Vector v2 = new Vector(-174.50900268554688,25.04829978942871,-14.165300369262695);        
        gb.appendTriangle(v0, v1, v2);
        
    }
    
}
