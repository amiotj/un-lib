
package science.unlicense.engine.opengl.physic;

import science.unlicense.engine.opengl.physic.SkinShell;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.buffer.DefaultBufferFactory;
import science.unlicense.api.buffer.FloatCursor;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.io.IOException;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.impl.math.Affine3;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 * Test the rendering is changed when the bones are moved.
 *
 * @author Johann Sorel
 */
public class SkinShellTest extends GLTest {

    /**
     * TODO
     * Make a real joint test
     * @throws IOException
     */
    @Ignore
    @Test
    public void testSkin() throws IOException{

        final Mesh mesh = new Mesh();
        mesh.getMaterial().setDiffuse(Color.RED);

        final Skeleton skeleton = new Skeleton();
        final Joint joint = new Joint(3);
        joint.setRootToNodeSpace(new Affine3(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0));
        skeleton.getChildren().add(joint);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        final SkinShell shell = new SkinShell();
        final FloatCursor vertices = DefaultBufferFactory.INSTANCE.createFloat(16).cursorFloat();
        vertices.write(new float[]{-1,-1,0});
        vertices.write(new float[]{ 0,-1,0});
        vertices.write(new float[]{ 0, 0,0});
        vertices.write(new float[]{-1, 0,0});

        final FloatCursor normals = DefaultBufferFactory.INSTANCE.createFloat(16).cursorFloat();
        normals.write(new float[]{0,0,1});
        normals.write(new float[]{0,0,1});
        normals.write(new float[]{0,0,1});
        normals.write(new float[]{0,0,1});

        final Buffer indices = DefaultBufferFactory.wrap(new int[]{
            0,1,2,
            2,3,0
        });

        final Buffer jointIndices = DefaultBufferFactory.wrap(new int[]{0,0,0,0});

        final Buffer weights = DefaultBufferFactory.wrap(new float[]{1,1,1,1});

        shell.setVertices(new VBO(vertices.getBuffer(),3));
        shell.setNormals(new VBO(normals.getBuffer(),3));
        shell.setIndexes(new IBO(indices,3),IndexRange.TRIANGLES(0, 6));
        shell.setJointIndexes(new VBO(jointIndices,1));
        shell.setWeights(new VBO(weights,1));
        shell.setMaxWeightPerVertex(1);
        shell.setSkeleton(skeleton);
//        shell.updateReducedJointList();
        mesh.setShape(shell);

        joint.setRootToNodeSpace(new Affine3(
                1, 0, 0, 1,
                0, 1, 0, 1,
                0, 0, 1, 0));
        skeleton.getChildren().add(joint);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        Image image = render(mesh, Color.TRS_WHITE);
        //TODO

    }

    private static Image render(Mesh mesh, Color background){

        final GLSource source = GLUtilities.createOffscreenSource(2, 2);
        final FBO fbo = new FBO(2, 2);
        fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2D(2, 2,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(2, 2,Texture2D.DEPTH24_STENCIL8()));

        //build the scene
        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();
        camera.setCameraType(camera.TYPE_FLAT);
        scene.getChildren().add(camera);

        scene.getChildren().add(mesh);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(background.toRGBAPreMul()));
        context.getPhases().add(new RenderPhase(scene,camera,fbo));

        final Image[] buffer = new Image[1];
        
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);

                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(source.getGL());
                final Image image = fbo.getColorTexture().getImage();
                buffer[0] = image;
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
            }
        });
        source.render();

        return buffer[0];
    }

}
