

package science.unlicense.engine.opengl.widget;

import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import science.unlicense.engine.opengl.painter.gl4.GL4PainterTest;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.layout.GridLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.api.image.Image;
import science.unlicense.engine.ui.visual.ViewRenderLoop;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.impl.gpu.opengl.GLTest;

/**
 *
 * @author Johann Sorel
 */
public class WLayoutClipTest extends GLTest {

    @Test
    @Ignore
    public void testclip(){
        final GL3ImagePainter2D painter = new GL3ImagePainter2D(60, 60);
        //ensure the resize does not cause any incorrect reseting
        painter.setSize(new Extent.Long(60, 60));

        final WContainer container = new WContainer(new GridLayout(3, 3));
        container.setEffectiveExtent(new Extent.Double(60, 60));
        container.getNodeTransform().setToTranslation(new double[]{30,30});
        
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WFill());
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WSpace(new Extent.Double(2)));
        container.getChildren().add(new WSpace(new Extent.Double(2)));

        ViewRenderLoop.render(container, painter, new BBox(new double[]{0,0}, new double[]{60,60}));

        final Image image = painter.getImage();

        GL4PainterTest.testImage(image, 20, 20, 20, 20, Color.TRS_BLACK, Color.RED);

    }

    private static class WFill extends WSpace{

        public WFill() {
            super(new Extent.Double(50, 50));
            setView(new WidgetView(this){
                protected void renderSelf(Painter2D painter, BBox dirtyArea, BBox innerBBox) {
                    super.renderSelf(painter, dirtyArea, innerBBox);
                    WFill.this.renderSelf(painter, dirtyArea, innerBBox);
                }
            });
        }

        private void renderSelf(Painter2D painter, BBox dirtyBBox, BBox innerBBox) {
            painter.setPaint(new ColorPaint(Color.RED));
            //we paint outside the area, the clip should have ensured we did not
            //paint out of the widget area
            painter.fill(new Rectangle(-10, -10, 100, 100));
        }

    }
}
