
package science.unlicense.engine.opengl.painter.gl4;

import science.unlicense.engine.opengl.painter.gl3.GL3ImagePainter2D;
import org.junit.Test;
import org.junit.Assert;
import org.junit.BeforeClass;
import science.unlicense.api.painter2d.ColorPaint;
import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.geometry.s2d.Rectangle;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.impl.math.Affine2;
import static science.unlicense.impl.gpu.opengl.GLTest.isGLAvailable;

/**
 *
 * @author Johann Sorel
 */
public class GL4PainterTest extends science.unlicense.api.painter2d.ImagePainter2DTest{

    private static final double DELTA = 0.00000001;
    private static final Color NONE = new Color(0, 0, 0, 0);

    @BeforeClass
    public static void beforeClass() {
        org.junit.Assume.assumeTrue(isGLAvailable());
    }
    
    protected ImagePainter2D createPainter(int width, int height) {
        return new GL3ImagePainter2D(width, height);
    }

    @Test
    public void testClip(){
        GL3ImagePainter2D painter;
        Image image;

        //sanity test
        painter = new GL3ImagePainter2D(100, 100);
        try{
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.fill(new Rectangle(0, 0, 100, 100));
            painter.flush();
            image = painter.getImage();
            testImage(image, 0, 0, 100, 100, NONE, Color.BLUE);
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }
        

        painter = new GL3ImagePainter2D(100, 100);
        try{
            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.fill(new Rectangle(10, 10, 80, 80));
            painter.flush();
            image = painter.getImage();
            testImage(image, 10, 10, 80, 80, NONE, Color.BLUE);
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

        //clip test
        painter = new GL3ImagePainter2D(100, 100);
        try{
            painter.setClip(new Rectangle(10, 10, 80, 80));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 10, 10, 80, 80, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            //ensure nothing has been painted
            image = painter.getImage();
            testImage(image, 0, 0, 100, 100, NONE, NONE);

            //paint a geometry, should be clipped
            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.fill(new Rectangle(0, 0, 100, 100));
            painter.flush();
            image = painter.getImage();
            testImage(image, 10, 10, 80, 80, NONE, Color.BLUE);
            testImage(painter.getDepthStencilImage(), 10, 10, 80, 80, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //remove the clip
            painter.setClip(null);
            painter.flush();
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            //ensure notice has been painted
            image = painter.getImage();
            testImage(image, 10, 10, 80, 80, NONE, Color.BLUE);
        }finally{
            painter.dispose();
        }

        //paint outside of the clip, should be ok since we removed the clip
        painter = new GL3ImagePainter2D(100, 100);
        try{
            painter.setPaint(new ColorPaint(Color.RED));
            painter.fill(new Rectangle(0, 0, 100, 100));
            painter.flush();
            image = painter.getImage();
            testImage(image, 0, 0, 100, 100, NONE, Color.RED);
            testImage(painter.getDepthStencilImage(), 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

    }

    @Test
    public void testClipStencilBuffer(){
        Painter2D painter;
        Image depthStencil;

        //sanity test
        final GL3ImagePainter2D glPainter = new GL3ImagePainter2D(100, 100);
        painter = glPainter;
        try{
            painter.setClip(new Rectangle(10, 20, 5, 30));
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 10, 20, 5, 30, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            // derivate painter
            final Affine2 m = new Affine2(
                    1, 0, 10,
                    0, 1, 20);
            painter = painter.derivate(m);
            //stencil buffer should still be the same
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 10, 20, 5, 30, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //change the stencil buffer using derivate painter
            painter.setClip(new Rectangle(10, 20, 5, 30));
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 20, 40, 5, 30, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //flush the clip
            painter.setClip(null);
            painter.flush();
            depthStencil = glPainter.getDepthStencilImage();
            Assert.assertNotNull(depthStencil);
            testImage(depthStencil, 0, 0, 0, 0, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }

    }

    
    @Test
    public void testClipStencilBuffer2(){
        GL3ImagePainter2D painter;

        //fill the stencil buffer
        painter = new GL3ImagePainter2D(100, 100);
        try{
            painter.setClip(new Rectangle(0, 0, 100, 100));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 0, 0, 100, 100, new double[]{0,0,0,0}, new double[]{1,0,0,0});

            //know set a smaller stencil buffer
            painter.setClip(new Rectangle(50, 50, 10, 10));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 50, 50, 10, 10, new double[]{0,0,0,0}, new double[]{1,0,0,0});
            painter.dispose();

            //do both in one call
            painter = new GL3ImagePainter2D(100, 100);
            painter.setClip(new Rectangle(0, 0, 100, 100));
            painter.setClip(new Rectangle(50, 50, 10, 10));
            painter.flush();
            testImage(painter.getDepthStencilImage(), 50, 50, 10, 10, new double[]{0,0,0,0}, new double[]{1,0,0,0});
        }finally{
            painter.dispose();
        }
        
    }
    

    public static void testImage(Image image, int x, int y, int width, int height, Color outside, Color inside){
        final RawModel sm = image.getRawModel();
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);
        final int imgWidth = (int) image.getExtent().getL(0);
        final int imgHeight = (int) image.getExtent().getL(1);

        final int[] coord = new int[2];
        for(coord[1]=0;coord[1]<imgHeight;coord[1]++){
            for(coord[0]=0;coord[0]<imgWidth;coord[0]++){
                final Color candidate = cm.getColor(coord);
                if(coord[0]>=x && coord[0]<(x+width) &&
                   coord[1]>=y && coord[1]<(y+height)){
                    Assert.assertEquals("INSIDE "+coord[0]+" "+coord[1], inside, candidate);
                }else{
                    Assert.assertEquals("OUTSIDE "+coord[0]+" "+coord[1], outside, candidate);
                }
            }
        }
    }

    public static void testImage(Image image, int x, int y, int width, int height, double[] outside, double[] inside){
        final TupleBuffer sm = image.getRawModel().asTupleBuffer(image);
        final double[] storage = new double[outside.length];
        final int imgWidth = (int) image.getExtent().getL(0);
        final int imgHeight = (int) image.getExtent().getL(1);

        final int[] coord = new int[2];
        for(coord[1]=0;coord[1]<imgHeight;coord[1]++){
            for(coord[0]=0;coord[0]<imgWidth;coord[0]++){
                sm.getTupleDouble(coord, storage);
                if(coord[0]>=x && coord[0]<(x+width) &&
                   coord[1]>=y && coord[1]<(y+height)){
                    Assert.assertArrayEquals("INSIDE "+coord[0]+" "+coord[1], inside, storage,DELTA);
                }else{
                    Assert.assertArrayEquals("OUTSIDE "+coord[0]+" "+coord[1], outside, storage,DELTA);
                }
            }
        }
    }

}
