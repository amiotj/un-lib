
package science.unlicense.engine.opengl.resource;

import science.unlicense.engine.opengl.resource.TransformFeedback;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.character.Chars;
import science.unlicense.api.gpu.opengl.GL4;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.DefaultRenderContext;
import science.unlicense.engine.opengl.phase.RenderContext;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.engine.opengl.renderer.actor.DefaultActor;
import science.unlicense.engine.opengl.renderer.actor.ActorProgram;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * @author Johann Sorel
 */
public class TransformFeedbackTest extends GLTest {

    private static final float DELTA = 0.00001f;
    
    @Test
    public void transformFeedbackTest() throws IOException{

        final GLSource source = GLUtilities.createOffscreenSource(1, 1);
        final GLProcessContext context = new DefaultGLProcessContext(source);
        
        final VBO input = new VBO(new float[]{1,2,3,4,5,6}, 3);
        final IBO idx = new IBO(new int[]{0,1}, 1);
        final IndexRange idxRange = IndexRange.POINTS(0, 2);        
        final VBO output = new VBO(new float[6], 3);
        
        final TransformFeedback feedback = new TransformFeedback();
        feedback.getResultVBO().add(output);
        
        final ActorProgram program = new ActorProgram();
        program.getActors().add(new DefaultActor(null, false,new Chars(
                "#version 330\n\n"
              + "<LAYOUT>\n"
              + "layout(location = 0) in vec3 val;\n"
              + "<VARIABLE_OUT>\n"
              + "vec3 res;\n"
              + "<OPERATION>\n"
              + "    outData.res = val*2;\n"
        ), null,null,null,null, true, true));
        program.setTransformFeedBackMode(GLC.TRANSFORM_FEEDBACK.MODE_INTERLEAVED);
        program.getTransformFeedBackVars().add(new Chars("VsDataModel.res"));
        
        context.getPhases().add(new AbstractFboPhase() {
            @Override
            protected void processInternal(GLProcessContext ctx) throws GLException {
                final RenderContext rctx = new DefaultRenderContext(ctx, new GLNode());
                final GL4 gl = ctx.getGL().asGL4();
                
                program.compile(rctx);
                program.enable(gl);
                
                gl.asGL2ES2().glEnableVertexAttribArray(0);
                input.loadOnGpuMemory(gl);
                feedback.loadOnGpuMemory(gl);
                idx.loadOnGpuMemory(gl);
                
                input.bind(gl, 0);                   
                idx.bind(gl);        
                feedback.bind(gl);     
                
                gl.asGL1().glEnable(science.unlicense.api.gpu.opengl.GLC.GL_RASTERIZER_DISCARD);
                feedback.begin(gl, GLC.TRANSFORM_FEEDBACK.PRIMITIVE.POINTS);
                idxRange.draw(gl, true);
                feedback.end(gl);
                
                feedback.unbind(gl);
                idx.unbind(gl);
                feedback.loadOnSystemMemory(gl);
                
                final float[] array = output.getPrimitiveBuffer().toFloatArray();
                Assert.assertArrayEquals(new float[]{2,4,6,8,10,12}, array, DELTA);
            }
        });
        
        source.render();
    }
    
}
