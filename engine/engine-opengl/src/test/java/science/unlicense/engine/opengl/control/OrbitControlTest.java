

package science.unlicense.engine.opengl.control;

import science.unlicense.engine.opengl.control.OrbitController;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Vector;

/**
 *
 * @author Johann Sorel
 */
public class OrbitControlTest {
    
    private static final double EPSILON = 1e-12;
    
    @Test
    public void atOriginTest(){
     
        final GLNode scene = new GLNode();
        final GLNode target = new GLNode();
        final GLNode satellite = new GLNode();
        
        scene.getChildren().add(target);
        scene.getChildren().add(satellite);
        
        final OrbitController controller = new OrbitController(null, new Vector(0, 1, 0), new Vector(0, 0, 1),target);
        controller.setDistance(10);        
        controller.update(null, satellite);        
        
        Assert.assertArrayEquals(new double[]{
            1, 0, 0, 10,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
            },satellite.getNodeToRootSpace().toMatrix().toArrayDouble(),EPSILON);
        
        
    }
    
    @Test
    public void targetOffsetTest(){
     
        final GLNode scene = new GLNode();
        final GLNode target = new GLNode();
        final GLNode satellite = new GLNode();
        
        target.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        target.getNodeTransform().notifyChanged();
        
        scene.getChildren().add(target);
        scene.getChildren().add(satellite);
        
        final OrbitController controller = new OrbitController(null, new Vector(0, 1, 0), new Vector(0, 0, 1),target);
        controller.setDistance(10);        
        controller.update(null, satellite);
        
        Assert.assertArrayEquals(new double[]{
            1, 0, 0, 11,
            0, 1, 0, 2,
            0, 0, 1, 3,
            0, 0, 0, 1,
            },satellite.getNodeToRootSpace().toMatrix().toArrayDouble(),EPSILON);
    }
    
    @Test
    public void parentOffsetTest(){
     
        final GLNode scene = new GLNode();
        final GLNode target = new GLNode();
        final GLNode satellite = new GLNode();
        
        scene.getNodeTransform().getTranslation().setXYZ(1, 2, 3);
        scene.getNodeTransform().notifyChanged();
        
        scene.getChildren().add(target);
        scene.getChildren().add(satellite);
        
        final OrbitController controller = new OrbitController(null, new Vector(0, 1, 0), new Vector(0, 0, 1),target);
        controller.setDistance(10);        
        controller.update(null, satellite);
        
        Assert.assertArrayEquals(new double[]{
            1, 0, 0, 11,
            0, 1, 0, 2,
            0, 0, 1, 3,
            0, 0, 0, 1,
            },satellite.getNodeToRootSpace().toMatrix().toArrayDouble(),EPSILON);
    }
    
    
}
