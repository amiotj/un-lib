
package science.unlicense.engine.opengl;

import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.phase.DefaultRenderContext;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.math.Matrix;
import science.unlicense.engine.opengl.control.NodeTransformController;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLTest;

/**
 *
 * @author Johann Sorel
 */
public class SceneTest extends GLTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void testWorldToNodeTransform(){

        final GLNode scene = new GLNode();
        final GLNode n1 = new GLNode();
        n1.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        n1.getNodeTransform().notifyChanged();
        final GLNode n2 = new GLNode();
        n2.getNodeTransform().getTranslation().setXYZ(3, -5, 7);
        n2.getNodeTransform().notifyChanged();
        scene.getChildren().add(n1);
        n1.getChildren().add(n2);

        //calculate our reference vector
        final Vector reference = new Vector(0, 0, 0, 1);
        n1.getNodeTransform().transform(reference,reference);
        n2.getNodeTransform().transform(reference,reference);

        Assert.assertEquals(5, reference.get(0), DELTA);
        Assert.assertEquals(-4, reference.get(1), DELTA);
        Assert.assertEquals(10, reference.get(2), DELTA);

        final Matrix worldToNode = n2.getRootToNodeSpace().toMatrix();
        final Vector candidate = new Vector(0, 0, 0, 1);
        worldToNode.transform(candidate,candidate);

        //we should be back to 0
        Assert.assertEquals(new Vector(-5, 4, -10, 1), candidate);

    }

    @Test
    public void testScale(){

        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();

        final GLNode n0 = new GLNode();
        n0.getNodeTransform().getTranslation().setXYZ(0, 7, 0);
        n0.getNodeTransform().notifyChanged();
        scene.getChildren().add(n0);

        final GLNode n1 = new GLNode();
        n1.getNodeTransform().getScale().setXYZ(3, 3, 3);
        n1.getNodeTransform().notifyChanged();
        n0.getChildren().add(n1);

        final GLNode n2 = new GLNode();
        n2.getNodeTransform().getTranslation().setXYZ(4, 0, 0);
        n2.getNodeTransform().notifyChanged();
        n1.getChildren().add(n2);


        camera.getNodeTransform().getTranslation().setXYZ(0, 0, -5);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);

        final Vector reference = new Vector(1, 1, 1, 1);

        Matrix matrix = n1.getNodeToRootSpace().toMatrix();
        Vector res = new Vector(4);
        matrix.transform(reference,res);
        Assert.assertEquals(3, res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3, res.get(2), DELTA);

        matrix = n1.calculateNodeToView(camera).toMatrix();
        matrix.transform(reference,res);
        Assert.assertEquals(3, res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3+5, res.get(2), DELTA);

        matrix = n2.getNodeToRootSpace().toMatrix();
        matrix.transform(reference,res);
        Assert.assertEquals(3*(4+1), res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3, res.get(2), DELTA);

        matrix = n2.calculateNodeToView(camera).toMatrix();
        matrix.transform(reference,res);
        Assert.assertEquals(3*(4+1), res.get(0), DELTA);
        Assert.assertEquals(3+7, res.get(1), DELTA);
        Assert.assertEquals(3+5, res.get(2), DELTA);


    }


    @Test
    public void testNodeToViewTransform(){

        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();


        final GLNode node = new GLNode();
        node.getNodeTransform().getTranslation().setXYZ(2, 1, 3);
        node.getNodeTransform().notifyChanged();
        scene.getChildren().add(node);

        camera.getNodeTransform().getTranslation().setXYZ(3, 5, 7);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);

        final Matrix4x4 expected = new Matrix4x4(
                1, 0, 0, -1,
                0, 1, 0, -4,
                0, 0, 1, -4,
                0, 0, 0, 1
        );

        final Matrix nodeToCamera = node.calculateNodeToView(camera).toMatrix();
        Assert.assertEquals(expected, nodeToCamera);
    }

    /**
     * Verify points alignement.
     */
    @Test
    public void testAlignment(){
        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        final DefaultRenderContext renderContext = new DefaultRenderContext(context, scene);
        //context.reshape(null, 0, 0, 800, 800);
        camera.update(renderContext, 0, renderContext.getViewRectangle());

        final Tuple t1 = new DefaultTuple(5, 0, 0, 1);
        final Tuple t2 = new DefaultTuple(-5, 0, 0, 1);

        final GLNode node1 = new GLNode();
        node1.getNodeTransform().getTranslation().setXYZ(-5, 0, 0);
        node1.getNodeTransform().notifyChanged();
        scene.getChildren().add(node1);

        final GLNode node2 = new GLNode();
        node2.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        node2.getNodeTransform().notifyChanged();
        scene.getChildren().add(node2);

        camera.getNodeTransform().getTranslation().setXYZ(0, 0, -10);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);

        Matrix mv1 = node1.calculateNodeToView(camera).toMatrix();
        Matrix mv2 = node2.calculateNodeToView(camera).toMatrix();
        Matrix mvp1 = node1.calculateMVP(camera);
        Matrix mvp2 = node2.calculateMVP(camera);
        //we should obtain the same point
        Tuple tmv1 = mv1.transform(t1);
        Tuple tmv2 = mv2.transform(t2);
        assertTEquals(tmv1, tmv2);
        Tuple tmvp1 = mvp1.transform(t1);
        Tuple tmvp2 = mvp2.transform(t2);
        assertTEquals(tmvp1, tmvp2);

        NodeTransformController control = new NodeTransformController(camera, new Vector(0, 1, 0, 0), new Vector(1, 0, 0, 0));
        control.rotate(control.getUp(), (float) Math.toRadians(45));
        control.update(null, camera);

        mv1 = node1.calculateNodeToView(camera).toMatrix();
        mv2 = node2.calculateNodeToView(camera).toMatrix();
        mvp1 = node1.calculateMVP(camera);
        mvp2 = node2.calculateMVP(camera);
        //we should obtain the same point
        tmv1 = mv1.transform(t1);
        tmv2 = mv2.transform(t2);
        assertTEquals(tmv1, tmv2);
        tmvp1 = mvp1.transform(t1);
        tmvp2 = mvp2.transform(t2);
        assertTEquals(tmvp1, tmvp2);
    }

    private static void assertTEquals(Tuple t1, Tuple t2){
        Assert.assertEquals(t1.getSize(), t2.getSize());
        for(int i=0;i<t1.getSize();i++){
            Assert.assertEquals(t1.get(i),t2.get(i),DELTA);
        }

    }

}
