

package science.unlicense.engine.opengl.phase;

import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.GBO;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.image.Image;
import science.unlicense.impl.gpu.opengl.GLException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.phase.picking.PickActor;
import science.unlicense.engine.opengl.phase.picking.PickResetPhase;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.TupleIterator;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.renderer.MeshRenderer;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.GLUtilities;

/**
 *
 * @author Johann Sorel
 */
public class PickingTest extends GLTest {
    
    @Test
    public void pickingTest() throws IOException{
        
        final int width = 10;
        final int height = 10;

        final GLSource source = GLUtilities.createOffscreenSource(width, height);
        final GLProcessContext context = new DefaultGLProcessContext(source);
        final Sequence phases = context.getPhases();
        
        final GBO fbo = new GBO(width, height, 0, true, false, false, false, true, true);
        final Texture2D texDiffuse  = (Texture2D) fbo.getDiffuseTexture();
        final Texture2D texPickMesh = (Texture2D) fbo.getMeshIdTexture();
        final Texture2D texPickVertex = (Texture2D) fbo.getVertexIdTexture();
        
        //build the scene        
        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();
        camera.getNodeTransform().getTranslation().setXYZ(0,0,30);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);
        
        
        final PickResetPhase pickResetPhases = new PickResetPhase(texPickMesh, texPickVertex);
        
        final GeometryMesh boxOne = new GeometryMesh(new BBox(new double[]{-5,-10,0},new double[]{5,10,1}));
        boxOne.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.RED)));
        boxOne.setPickable(true);
        ((MeshRenderer)boxOne.getRenderers().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxOne));
        scene.getChildren().add(boxOne);
        
        final GeometryMesh boxTwo = new GeometryMesh(new BBox(new double[]{-5,-5,1},new double[]{5,0,2}));
        boxTwo.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.GREEN)));
        boxTwo.setPickable(true);
        ((MeshRenderer)boxTwo.getRenderers().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxTwo));
        scene.getChildren().add(boxTwo);
        
        final GeometryMesh boxThree = new GeometryMesh(new BBox(new double[]{-5,0,1},new double[]{5,5,2}));
        boxThree.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.BLUE)));
        boxThree.setPickable(false);
        ((MeshRenderer)boxThree.getRenderers().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxThree));
        scene.getChildren().add(boxThree);
        
        final GeometryMesh boxFour = new GeometryMesh(new BBox(new double[]{-5,5,1},new double[]{5,10,2}));
        boxFour.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.WHITE)));
        boxFour.setPickable(false);
        ((MeshRenderer)boxFour.getRenderers().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, boxFour));
        scene.getChildren().add(boxFour);
        
        final DeferredRenderPhase renderPhase = new DeferredRenderPhase(scene, camera, fbo);
        

        phases.add(new UpdatePhase(scene));
        phases.add(new ClearPhase(fbo));
        phases.add(pickResetPhases);        
        phases.add(renderPhase);        
        phases.add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                texDiffuse.loadOnSystemMemory(ctx.getGL());
                texPickMesh.loadOnSystemMemory(ctx.getGL());
                texPickVertex.loadOnSystemMemory(ctx.getGL());
                final Image image0 = texDiffuse.getImage();
                final Image image1 = texPickMesh.getImage();
                final Image image2 = texPickVertex.getImage();
                
                //check diffuse colors
                checkImage(image0, new BBox(new Vector(0, 0), new Vector(10, 1)), new int[]{0,0,0});
                checkImage(image0, new BBox(new Vector(3, 1), new Vector(7,  3)), new int[]{255,0,0});      //RED
                checkImage(image0, new BBox(new Vector(3, 3), new Vector(7,  5)), new int[]{0,255,0});      //GREEN
                checkImage(image0, new BBox(new Vector(3, 5), new Vector(7,  7)), new int[]{0,0,255});      //BLUE
                checkImage(image0, new BBox(new Vector(3, 7), new Vector(7,  9)), new int[]{255,255,255});  //WHITE
                checkImage(image0, new BBox(new Vector(0, 9), new Vector(10,10)), new int[]{0,0,0});
                //check mesh id : picking start at index 3 and increment for each object
                checkImage(image1, new BBox(new Vector(0, 0), new Vector(10, 1)), new int[]{0});
                checkImage(image1, new BBox(new Vector(3, 1), new Vector(7,  3)), new int[]{3});
                checkImage(image1, new BBox(new Vector(3, 3), new Vector(7,  5)), new int[]{4});
                checkImage(image1, new BBox(new Vector(3, 5), new Vector(7,  7)), new int[]{1});    //occlusion = 1
                checkImage(image1, new BBox(new Vector(3, 7), new Vector(7,  9)), new int[]{1});    //occlusion = 1
                checkImage(image1, new BBox(new Vector(0, 9), new Vector(10,10)), new int[]{0});
            }
        });

        //render it
        source.render();
        
        scene.dispose(context);
        source.dispose();
    }
    
    private static void checkImage(Image image, BBox rectangle, int[] samples){
        final int[] buffer = new int[samples.length];
        
        final TupleIterator ite = image.getRawModel().asTupleBuffer(image).createIterator(rectangle);
        while(ite.nextAsInt(buffer)!=null){
            Assert.assertArrayEquals(samples, buffer);
        }
        
    }
    
}
