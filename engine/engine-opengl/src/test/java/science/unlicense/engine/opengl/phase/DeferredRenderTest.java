
package science.unlicense.engine.opengl.phase;

import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.GBO;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.renderer.Renderer;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 *
 * @author Johann Sorel
 */
public class DeferredRenderTest extends GLTest {

    private static final float DELTA = 0.01f;
    
    
    @Test
    public void diffuseNormalPositionTest() throws IOException{

        final Mesh plan = createQuadMesh();

        final Image[] images = render(plan,100,100);
        final Image imgColor = images[0];
        final Image imgNormal = images[1];
        final Image imgPosition = images[2];
        
        final int[] coord = new int[2];
        Assert.assertEquals(Color.BLUE, imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord));
        Assert.assertArrayEquals(new float[]{0,0,1}, (float[]) imgNormal.getRawModel().asTupleBuffer(imgNormal).getTuple(coord, null),DELTA);
        Assert.assertArrayEquals(new float[]{-0.5f,-0.5f,-1.0f}, (float[]) imgPosition.getRawModel().asTupleBuffer(imgPosition).getTuple(coord, null),DELTA);
        coord[0] = 99; coord[1] = 99;
        Assert.assertEquals(Color.BLUE, imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord));
        Assert.assertArrayEquals(new float[]{0,0,1}, (float[]) imgNormal.getRawModel().asTupleBuffer(imgNormal).getTuple(coord, null),DELTA);
        Assert.assertArrayEquals(new float[]{0.5f,0.5f,-1.0f}, (float[]) imgPosition.getRawModel().asTupleBuffer(imgPosition).getTuple(coord, null),DELTA);
        
    }
    
    public static Mesh createQuadMesh(){
        final Mesh plan = new Mesh();
        final Shell shell = new Shell();
        shell.setVertices(new VBO(new float[]{
            -0.5f,-0.5f,-1.0f,
            +0.5f,-0.5f,-1.0f,
            +0.5f,+0.5f,-1.0f,
            -0.5f,+0.5f,-1.0f
            },3));
        shell.setNormals(new VBO(new float[]{
            0,0,1,
            0,0,1,
            0,0,1,
            0,0,1
            },3));
        shell.setUVs(new VBO(new float[]{
            0,0,
            1,0,
            1,1,
            0,1
            },2));
        shell.setIndexes(new IBO(new int[]{0,1,2,2,3,0},3),IndexRange.TRIANGLES(0, 6));
        plan.setShape(shell);
        plan.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.BLUE)));
        ((Renderer)plan.getRenderers().get(0)).getState().setCulling(-1);
        shell.calculateTangents();
        
        return plan;
    }
    
    public static Image[] render(Mesh mesh, int width, int height) {

        final GLSource source = GLUtilities.createOffscreenSource(width,height);
        final GBO gbo = new GBO(width, height);
        final Texture2D texColor = (Texture2D) gbo.getDiffuseTexture();
        final Texture2D texNormal = (Texture2D) gbo.getNormalTexture();
        final Texture2D texPosition = (Texture2D) gbo.getPositionTexture();
        
        //build the scene
        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();
        camera.setCameraType(CameraMono.TYPE_ORTHO);
        scene.getChildren().add(camera);

        scene.getChildren().add(mesh);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(gbo,Color.TRS_BLACK.toRGBAPreMul()));
        context.getPhases().add(new DeferredRenderPhase(scene,camera,gbo));

        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                gbo.loadOnSystemMemory(source.getGL());
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
                scene.dispose(context);
            }
        });
        source.render();
        source.dispose();

        final Image imgColor = texColor.getImage();
        final Image imgNormal = texNormal.getImage();
        final Image imgPosition = texPosition.getImage();
        
        return new Image[]{imgColor,imgNormal,imgPosition};
    }
    
}
