

package science.unlicense.engine.opengl.phase;

import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 * Test ClearPhase.
 * @author Johann Sorel
 */
public class ClearPhaseTest extends GLTest {
 
    @Test
    public void testRedClear(){
        final Image image = render(Color.RED);
        Assert.assertNotNull(image);
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);

        for(int y=0;y<1;y++){
            for(int x=0;x<1;x++){
                final Color rgba = cm.getColor(new int[]{x,y});
                //test integer values to avoid rounding issues
                Assert.assertEquals(Color.RED.toARGB(), rgba.toARGB());
            }
        }
    }
    
    private static Image render(Color clearColor){

        final GLSource source = GLUtilities.createOffscreenSource(1, 1);
        final FBO fbo = new FBO(1, 1);
        fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2D(1, 1,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(1, 1,Texture2D.DEPTH24_STENCIL8()));

        //build the scene
        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();
        camera.setCameraType(camera.TYPE_FLAT);
        scene.getChildren().add(camera);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(fbo,clearColor.toRGBAPreMul()));

        final Image[] buffer = new Image[1];
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(source.getGL());
                final Image image = fbo.getColorTexture().getImage();
                buffer[0] = image;
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
                scene.dispose(context);
            }
        });
        source.render();
        source.dispose();

        return buffer[0];
    }
    
}
