

package science.unlicense.engine.opengl;

import science.unlicense.engine.opengl.CSUtilities;
import org.junit.Assert;
import org.junit.Test;
import science.unlicense.api.geometry.coordsys.Axis;
import science.unlicense.api.geometry.coordsys.CoordinateSystem;
import science.unlicense.api.geometry.coordsys.Direction;
import science.unlicense.api.math.MatrixRW;
import science.unlicense.api.unit.Units;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.mesh.MultipartMesh;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Matrix3x3;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;

/**
 *
 * @author Johann Sorel
 */
public class CSUtilitiesTest {

    private static final float DELTA = 0.00000001f;

    @Test
    public void invertHandVectorTest(){
        final Vector v = new Vector(5, 2, 3);
        CSUtilities.invertHandCS(v);
        Assert.assertEquals(new Vector(5, 2, -3), v);
    }

    @Test
    public void invertHandVBOTest(){
        final VBO vbo = new VBO(new float[]{1,2,3,4,5,6,7,8,9}, 3);
        CSUtilities.invertHandCS(vbo);
        Assert.assertArrayEquals(new float[]{1,2,-3,4,5,-6,7,8,-9}, vbo.getPrimitiveBuffer().toFloatArray(), DELTA);
    }

    @Test
    public void invertHandMatrix3Test(){
        Matrix3x3 m = new Matrix3x3(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        CSUtilities.invertHandCS(m);
        Assert.assertEquals(new Matrix3x3(
                1, 2,-3,
                4, 5,-6,
               -7,-8, 9), m);
    }

    @Test
    public void invertHandMatrix4Test(){
        final Matrix4x4 m = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9,10,11,12,
               13,14,15,16);
        CSUtilities.invertHandCS(m);
        Assert.assertEquals(new Matrix4x4(
                1,  2, -3,  4,
                5,  6, -7,  8,
               -9,-10, 11,-12,
               13, 14,-15, 16), m);
    }

    @Test
    public void invertHandMatrixTest(){
        MatrixRW ma = new Matrix3x3(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        CSUtilities.invertHandCS((MatrixRW)ma  );
        Assert.assertEquals(new Matrix3x3(
                1, 2,-3,
                4, 5,-6,
               -7,-8, 9), ma );

        ma = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9,10,11,12,
               13,14,15,16);
        CSUtilities.invertHandCS(ma);
        Assert.assertEquals(new Matrix4x4(
                1,  2, -3,  4,
                5,  6, -7,  8,
               -9,-10, 11,-12,
               13, 14,-15, 16), ma);
    }

    @Test
    public void transformNodeTest(){
        
        final CoordinateSystem sourceCs = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.BACKWARD, Units.METER),
                new Axis(Direction.LEFT, Units.METER),
                new Axis(Direction.UP, Units.METER),
            }
        );
        final CoordinateSystem targetCs = new CoordinateSystem(
            new Axis[]{
                new Axis(Direction.RIGHT, Units.CENTIMETER),
                new Axis(Direction.UP, Units.CENTIMETER),
                new Axis(Direction.FORWARD, Units.CENTIMETER)
            }
        );
        
        
        //create a small scene
        final GLNode root = new GLNode(sourceCs);        
        final GLNode branch = new GLNode(sourceCs);        
        final MultipartMesh mpm = new MultipartMesh(sourceCs);        
        final Mesh mesh = new Mesh(sourceCs);
        
        root.getChildren().add(branch);
        branch.getChildren().add(mpm);
        mpm.getChildren().add(mesh);
        
        
        final Shell shell = new Shell();
        shell.setVertices(new VBO(new float[]{1,2,3, 4,5,6, 7,8,9, 10,11,12}, 3));
        shell.setNormals( new VBO(new float[]{1,0,0, 0,1,0, 0,0,1,  0, 1, 0}, 3));
        shell.setUVs(new VBO(new float[]{0,0, 1,0, 1,1, 0,1}, 2));
        shell.setIndexes(new IBO(new int[]{0,1,2, 2,3,0}, 3), IndexRange.TRIANGLES(0, 6));
        mesh.setShape(shell);
        
        
        //transform
        CSUtilities.transform(root, targetCs);
        
        //check result
        Assert.assertTrue(root.getCoordinateSystem() == targetCs);
        Assert.assertTrue(branch.getCoordinateSystem() == targetCs);
        Assert.assertTrue(mpm.getCoordinateSystem() == targetCs);
        Assert.assertTrue(mesh.getCoordinateSystem() == targetCs);
        
        //check node transform changed
        
        //TODO
        
//        Assert.assertEquals(new Matrix4(
//                1, 0, 0, 0, 
//                0, 1, 0, 0, 
//                0, 0, 1, 0, 
//                0, 0, 0, 1), 
//                root.getNodeTransform().asMatrix());
        
        
        final float[] resVertices = shell.getVertices().getPrimitiveBuffer().toFloatArray();
        Assert.assertArrayEquals(new float[]{
             -200,  300,  -100,
             -500,  600,  -400,
             -800,  900,  -700,
            -1100, 1200, -1000
            }, resVertices, DELTA);
        
    }
    
}
