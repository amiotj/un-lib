
package science.unlicense.engine.opengl.renderer;

import science.unlicense.engine.opengl.renderer.DebugNormalRenderer;
import org.junit.Test;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.image.Image;
import science.unlicense.engine.opengl.RenderingTest;
import science.unlicense.engine.opengl.mesh.GeometryMesh;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLTest;

/**
 *
 * @author Johann Sorel
 */
public class DebugNormalRendererTest extends GLTest {

    @Test
    public void renderTest(){
        
        final GeometryMesh mesh = new GeometryMesh(new BBox(new Vector(-1,-1,-1), new Vector(1, 1, 1)));
        mesh.getRenderers().add(new DebugNormalRenderer());
        final CameraMono camera = new CameraMono();
        final GLNode scene = new GLNode();
        scene.getChildren().add(mesh);
        scene.getChildren().add(camera);
        
        Image img = RenderingTest.renderBasic(scene, camera, 100, 100);
        
    }
        
}
