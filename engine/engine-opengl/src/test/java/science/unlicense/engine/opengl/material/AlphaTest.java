
package science.unlicense.engine.opengl.material;

import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.Material;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.material.mapping.Mapping;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.Plan;
import science.unlicense.engine.opengl.mesh.Shell;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;
import science.unlicense.impl.gpu.opengl.resource.VBO;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.api.color.Color;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.color.PixelBuffer;
import science.unlicense.impl.math.Vector;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLTest;

/**
 * Various tests related to alpha blending.
 *
 * @author Johann Sorel
 */
public class AlphaTest extends GLTest {

    @Test
    public void colorTest(){

        final Material material = new Material();

        final Color halfRed = new Color(1f, 0f, 0f, 0.5f, false);

        final Mapping mapping = new PlainColorMapping(halfRed);
        final Layer layer = new Layer(mapping, Layer.TYPE_DIFFUSE);
        material.putOrReplaceLayer(layer);

        final Image image = render(material,Color.BLACK);
        Assert.assertNotNull(image);
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);

        //dest = black, src = red . this is the result of a normal SRC_OVER
        final Color resultColor = new Color(0.5f, 0f, 0f, 1f, false);
        for(int y=0;y<1;y++){
            for(int x=0;x<1;x++){
                final Color rgba = cm.getColor(new int[]{x,y});
                //test integer values to avoid rounding issues
                Assert.assertEquals(resultColor.toARGB(), rgba.toARGB());
            }
        }
    }

    //TODO there is a rounding issue here. find where
    @Ignore
    @Test
    public void textureTest(){

        final Color halfRed = new Color(1f, 0f, 0f, 0.5f, false);

        //create an image we will use as a texture
        final Image texture = Images.create(new Extent.Long(1, 1),Images.IMAGE_TYPE_RGBA);
        final PixelBuffer textcm = texture.getColorModel().asTupleBuffer(texture);
        textcm.setColor(new BBox(new double[]{0,0}, new double[]{100,100}), halfRed);

        final Material material = new Material();

        final UVMapping mapping = new UVMapping(new Texture2D(texture));
        final Layer layer = new Layer(mapping, Layer.TYPE_DIFFUSE);
        material.putOrReplaceLayer(layer);

        final Image image = render(material,Color.BLACK);
        Assert.assertNotNull(image);
        final PixelBuffer cm = image.getColorModel().asTupleBuffer(image);

        //dest = black, src = red . this is the result of a normal SRC_OVER
        final Color resultColor = new Color(0.5f, 0f, 0f, 1f, false);
        for(int y=0;y<1;y++){
            for(int x=0;x<1;x++){
                final Color rgba = cm.getColor(new int[]{x,y});
                //test integer values to avoid rounding issues
                Assert.assertEquals(resultColor.toARGB(), rgba.toARGB());
            }
        }
    }



    private static Image render(Material material, Color background){

        final GLSource source = GLUtilities.createOffscreenSource(1,1);
        final FBO fbo = new FBO(1, 1);
        fbo.addAttachment(GLC.FBO.Attachment.COLOR_0, new Texture2D(1, 1,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(1, 1,Texture2D.DEPTH24_STENCIL8()));

        //build the scene
        final GLNode scene = new GLNode();
        final CameraMono camera = new CameraMono();
        camera.setCameraType(camera.TYPE_FLAT);
        scene.getChildren().add(camera);

        final Plan plan = new Plan(
                new Vector(-1, -1, 0),
                new Vector(+1, -1, 0),
                new Vector(+1, +1, 0),
                new Vector(-1, +1, 0));
        plan.setMaterial(material);
        ((Shell)plan.getShape()).setUVs(new VBO(new float[]{0,0, 1,0, 1,1, 0,1},2));
        scene.getChildren().add(plan);

        //render it
        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(fbo,background.toRGBAPreMul()));
        final RenderPhase renderPhase = new RenderPhase(scene,camera,fbo);
        renderPhase.setUseBlending(true);
        context.getPhases().add(renderPhase);

        final Image[] buffer = new Image[1];
        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);

                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(source.getGL());
                final Image image = fbo.getColorTexture().getImage();
                buffer[0] = image;

                //release resources
                scene.dispose(context);
                context.dispose(source);
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
            }
        });
        source.render();

        return buffer[0];
    }

}
