

package science.unlicense.engine.opengl.light;

import science.unlicense.engine.opengl.light.AmbientLight;
import science.unlicense.engine.opengl.light.SpotLight;
import science.unlicense.engine.opengl.light.Attenuation;
import science.unlicense.engine.opengl.light.PointLight;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import science.unlicense.api.color.Color;
import science.unlicense.api.gpu.opengl.GLCallback;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.image.Image;
import science.unlicense.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.PlainColorMapping;
import science.unlicense.engine.opengl.mesh.Mesh;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderTest;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.LightPhase;
import science.unlicense.engine.opengl.scenegraph.CameraMono;
import science.unlicense.engine.opengl.scenegraph.GLNode;
import science.unlicense.impl.gpu.opengl.GLC;
import science.unlicense.impl.gpu.opengl.GLTest;
import science.unlicense.impl.gpu.opengl.GLUtilities;
import science.unlicense.impl.gpu.opengl.resource.FBO;
import science.unlicense.impl.gpu.opengl.resource.Texture2D;

/**
 *
 * @author Johann Sorel
 */
public class LightTest extends GLTest {
    
    private static final float DELTA = 0.0001f;
        
    /**
     * No light, the ambient factor should me 1.0, so we expect the diffuse color of the mesh.
     */
    @Test
    public void noLightTest() throws IOException{
        
        final GLNode scene = new GLNode();        
        final Mesh mesh = DeferredRenderTest.createQuadMesh();
        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.GREEN)));
        scene.getChildren().add(mesh);
        
        final Image imgColor = render(scene, 100, 100);
        
        final int[] coord = new int[2];
        Assert.assertEquals(Color.GREEN, imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord));
        coord[0] = 99; coord[1] = 99;
        Assert.assertEquals(Color.GREEN, imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord));
        
    }
    
    /**
     * Half intensity ambient light, so we expect half the diffuse color of the mesh.
     */
    @Test
    public void ambiantLightTest() throws IOException{
        
        final GLNode scene = new GLNode();        
        final Mesh mesh = DeferredRenderTest.createQuadMesh();
        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.GREEN)));
        scene.getChildren().add(mesh);
        
        final AmbientLight light = new AmbientLight(new Color(0.5f,0.5f,0.5f,1.0f),new Color(0.5f,0.5f,0.5f,1.0f));
        scene.getChildren().add(light);
        
        final Image imgColor = render(scene, 100, 100);
        
        final Color halfGreen = new Color(0.0f, 0.5f, 0.0f);
        
        final int[] coord = new int[2];
        Assert.assertEquals(halfGreen.toARGB(), imgColor.getColorModel().asTupleBuffer(imgColor).getARGB(coord));
        coord[0] = 99; coord[1] = 99;
        Assert.assertEquals(halfGreen.toARGB(), imgColor.getColorModel().asTupleBuffer(imgColor).getARGB(coord));
        
    }
    
    /**
     * 
     */
    @Test
    public void pointLightTest() throws IOException{
        
        final GLNode scene = new GLNode();        
        final Mesh mesh = DeferredRenderTest.createQuadMesh();
        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.GREEN)));
        scene.getChildren().add(mesh);
        
        final AmbientLight ambLight = new AmbientLight(new Color(0.0f,0.0f,0.0f,1.0f),new Color(0.5f,0.5f,0.5f,1.0f));
        scene.getChildren().add(ambLight);
        final PointLight light = new PointLight();
        light.setAttenuation(new Attenuation(1, 1, 0));
        light.getNodeTransform().getTranslation().setXYZ(0, 0, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);
        
        final Image imgColor = render(scene, 100, 100);
                
        final int[] coord = new int[2];
        Color colorCorner = imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord);
        coord[0] = 49; coord[1] = 49;
        Color colorCenter = imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord);
        
        //red and blue are at zero and green value at corner must be smaller
        Assert.assertEquals(0,colorCenter.getRed(), DELTA);
        Assert.assertEquals(0,colorCorner.getRed(), DELTA);
        Assert.assertEquals(0,colorCenter.getBlue(), DELTA);
        Assert.assertEquals(0,colorCorner.getBlue(), DELTA);
        Assert.assertTrue(colorCenter.getGreen() > colorCorner.getGreen());
    }
    
    /**
     * 
     */
    @Test
    public void spotLightTest() throws IOException{
        
        final GLNode scene = new GLNode();        
        final Mesh mesh = DeferredRenderTest.createQuadMesh();
        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(Color.GREEN)));
        scene.getChildren().add(mesh);
        
        final AmbientLight ambLight = new AmbientLight(new Color(0.0f,0.0f,0.0f,1.0f),new Color(0.5f,0.5f,0.5f,1.0f));
        scene.getChildren().add(ambLight);
        final SpotLight light = new SpotLight();
        light.setAttenuation(new Attenuation(1, 1, 0));
        light.setFallOffAngle(20f);
        light.setFallOffExponent(1f);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);
        
        final Image imgColor = render(scene, 100, 100);
        
        final int[] coord = new int[2];
        Color colorCorner = imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord);
        coord[0] = 49; coord[1] = 49;
        Color colorCenter = imgColor.getColorModel().asTupleBuffer(imgColor).getColor(coord);
        
        //red and blue are at zero and green value at corner must be 0
        Assert.assertEquals(0,colorCenter.getRed(), DELTA);
        Assert.assertEquals(0,colorCorner.getRed(), DELTA);
        Assert.assertEquals(0,colorCenter.getBlue(), DELTA);
        Assert.assertEquals(0,colorCorner.getBlue(), DELTA);
        Assert.assertEquals(0,colorCorner.getGreen(), DELTA);
        Assert.assertTrue(colorCenter.getGreen() > colorCorner.getGreen());
    }
    
    
    public static Image render(final GLNode scene, int width, int height) {

        final GLSource source = GLUtilities.createOffscreenSource(width,height);
        final GBO gbo = new GBO(width, height);
        final Texture2D texDiffuse = (Texture2D) gbo.getDiffuseTexture();
        final Texture2D texSpecular = (Texture2D) gbo.getSpecularTexture();
        final Texture2D texNormal = (Texture2D) gbo.getNormalTexture();
        final Texture2D texPosition = (Texture2D) gbo.getPositionTexture();
        
        final FBO lightfbo = new FBO(width, height);
        final Texture2D texColor = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        lightfbo.addAttachment(GLC.FBO.Attachment.COLOR_0,texColor);
        
        
        //build the scene
        final CameraMono camera = new CameraMono();
        camera.setCameraType(CameraMono.TYPE_ORTHO);
        scene.getChildren().add(camera);

        final DefaultGLProcessContext context = new DefaultGLProcessContext();
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(gbo,Color.TRS_BLACK.toRGBAPreMul()));
        context.getPhases().add(new DeferredRenderPhase(scene,camera,gbo));
        context.getPhases().add(new LightPhase(lightfbo, texDiffuse, texSpecular, texPosition, texNormal, scene, camera));

        source.getCallbacks().add(new GLCallback() {
            @Override
            public void execute(GLSource source) {
                context.execute(source);
                //get the result image
                lightfbo.loadOnSystemMemory(source.getGL());
                gbo.loadOnSystemMemory(source.getGL());
                //release resources
                scene.dispose(context);
                context.dispose(source);
            }

            @Override
            public void dispose(GLSource source) {
                context.dispose(source);
                gbo.unloadFromGpuMemory(source.getGL());
                lightfbo.unloadFromGpuMemory(source.getGL());
                scene.dispose(context);
            }
        });
        source.render();
        source.dispose();
        
        return texColor.getImage();
    }
    
}
