
package science.unlicense.engine.opengl.operation;

import science.unlicense.engine.opengl.operation.SkinBuffer;
import org.junit.Test;
import org.junit.Assert;
import science.unlicense.api.physic.skeleton.Joint;
import science.unlicense.api.physic.skeleton.Skeleton;
import science.unlicense.engine.opengl.physic.SkinShell;
import science.unlicense.impl.math.Matrix4x4;
import science.unlicense.impl.gpu.opengl.resource.IBO;
import science.unlicense.impl.gpu.opengl.resource.IndexRange;
import science.unlicense.impl.gpu.opengl.resource.VBO;

/**
 *
 * @author Johann Sorel
 */
public class SkinBufferTest {

    @Test
    public void oneJointTest(){
        
        final SkinShell shell = new SkinShell();
        
        shell.setVertices(new VBO(new float[]{0,1,2, 2,3,4, 4,5,6}, 3));
        shell.setIndexes(new IBO(new int[]{0,1,2}, 3), IndexRange.TRIANGLES(0, 1));
        shell.setJointIndexes(new VBO(new int[]{0,0,0}, 1));
        shell.setWeights(new VBO(new float[]{1,1,1}, 1));
        shell.setMaxWeightPerVertex(1);
        
        final Joint joint = new Joint(3);
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(joint);
        shell.setSkeleton(skeleton);
        
        
        new SkinBuffer(shell, 2, 0);
        
        float[] array = shell.getVertices().getPrimitiveBuffer().toFloatArray();
        
        Assert.assertArrayEquals(new float[]{0,2,4, 4,6,8, 8,10,12}, array, 0.00001f);
        
    }
    
    @Test
    public void oneJointConstantTest(){
        
        final SkinShell shell = new SkinShell();
        
        shell.setVertices(new VBO(new float[]{0,1,0, 2,0,0, 0,0,6}, 3));
        shell.setIndexes(new IBO(new int[]{0,1,2}, 3), IndexRange.TRIANGLES(0, 1));
        shell.setJointIndexes(new VBO(new int[]{0,0,0}, 1));
        shell.setWeights(new VBO(new float[]{1,1,1}, 1));
        shell.setMaxWeightPerVertex(1);
        
        final Joint joint = new Joint(3);
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(joint);
        shell.setSkeleton(skeleton);
        
        
        new SkinBuffer(shell, 1, 0.1);
        
        float[] array = shell.getVertices().getPrimitiveBuffer().toFloatArray();
        
        Assert.assertArrayEquals(new float[]{0,1.1f,0, 2.1f,0,0, 0,0,6.1f}, array, 0.00001f);
        
    }
    
    @Test
    public void twoJointTest(){
        
        final SkinShell shell = new SkinShell();
        
        shell.setVertices(new VBO(new float[]{
            4,15,0,  //will be pushed on -x
            8,10,0,  //will be pushed on +x
            5,22,0   //will be pushed on +y 
            }, 3)); 
        shell.setIndexes(new IBO(new int[]{0,1,2}, 3), IndexRange.TRIANGLES(0, 1));
        shell.setJointIndexes(new VBO(new int[]{0,1, 0,1, 0,1}, 2));
        shell.setWeights(new VBO(new float[]{0.5f,0.5f, 0.5f,0.5f, 0.5f,0.5f}, 2));
        shell.setMaxWeightPerVertex(2);
        
        final Joint joint0 = new Joint(3);
        joint0.getNodeTransform().set(new Matrix4x4(
                1, 0, 0, 5, 
                0, 1, 0, 10, 
                0, 0, 1, 0, 
                0, 0, 0, 1));
        final Joint joint1 = new Joint(3);
        joint1.getNodeTransform().set(new Matrix4x4(
                1, 0, 0, 5, 
                0, 1, 0, 20, 
                0, 0, 1, 0, 
                0, 0, 0, 1));
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(joint0);
        skeleton.getChildren().add(joint1);
        shell.setSkeleton(skeleton);
        
        
        new SkinBuffer(shell, 2, 0);
        
        float[] array = shell.getVertices().getPrimitiveBuffer().toFloatArray();
        
        Assert.assertArrayEquals(new float[]{
            3,15,0,
            11,10,0, 
            5,24,0
            }, array, 0.00001f);
        
    }
    
    
}
