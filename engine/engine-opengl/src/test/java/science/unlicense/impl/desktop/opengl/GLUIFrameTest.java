
package science.unlicense.impl.desktop.opengl;

import science.unlicense.api.desktop.AbstractFrameTest;
import science.unlicense.api.desktop.Frame;

/**
 *
 * @author Johann Sorel
 */
public class GLUIFrameTest extends AbstractFrameTest{

    @Override
    protected Frame createFrame() {
        return GLUIFrameManager.INSTANCE.createFrame(false);
    }
    
}
