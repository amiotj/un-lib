package science.unlicense.api.gpu.opengl;

import science.unlicense.api.character.CharArray;

public interface GL20 {

    /**
     * @param modeRGB ,value from enumeration group BlendEquationModeEXT
     * @param modeAlpha ,value from enumeration group BlendEquationModeEXT
     */
     void glBlendEquationSeparate (int modeRGB, int modeAlpha);

    /**
     * @param n
     * @param bufs ,value from enumeration group DrawBufferModeATI ,length n
     */
     void glDrawBuffers (java.nio.IntBuffer bufs);

     /**
      * Convenient method
      *
      * @param bufs
      */
     void glDrawBuffers(int[] bufs);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param sfail ,value from enumeration group StencilOp
     * @param dpfail ,value from enumeration group StencilOp
     * @param dppass ,value from enumeration group StencilOp
     */
     void glStencilOpSeparate (int face, int sfail, int dpfail, int dppass);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFuncSeparate (int face, int func, int ref, int mask);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMaskSeparate (int face, int mask);

    /**
     * @param program
     * @param shader
     */
     void glAttachShader (int program, int shader);

    /**
     * @param program
     * @param index
     * @param name
     */
     void glBindAttribLocation (int program, int index, science.unlicense.api.character.CharArray name);

    /**
     * @param shader
     */
     void glCompileShader (int shader);

     int glCreateProgram ();

    /**
     * @param type
     */
     int glCreateShader (int type);

    /**
     * @param program
     */
     void glDeleteProgram (int program);

    /**
     * @param shader
     */
     void glDeleteShader (int shader);

    /**
     * @param program
     * @param shader
     */
     void glDetachShader (int program, int shader);

    /**
     * @param index
     */
     void glDisableVertexAttribArray (int index);

    /**
     * @param index
     */
     void glEnableVertexAttribArray (int index);

    /**
     * @param program
     * @param index
     * @param bufSize
     * @param length
     * @param size
     * @param type
     * @param name ,length bufSize
     */
     void glGetActiveAttrib (int program, int index, java.nio.IntBuffer length, java.nio.IntBuffer size, java.nio.IntBuffer type, java.nio.ByteBuffer name);

    /**
     * @param program
     * @param index
     * @param bufSize
     * @param length
     * @param size
     * @param type
     * @param name ,length bufSize
     */
     void glGetActiveUniform (int program, int index, java.nio.IntBuffer length, java.nio.IntBuffer size, java.nio.IntBuffer type, java.nio.ByteBuffer name);

    /**
     * @param program
     * @param maxCount
     * @param count
     * @param shaders ,length maxCount
     */
     void glGetAttachedShaders (int program, java.nio.IntBuffer count, java.nio.IntBuffer shaders);

    /**
     * @param program
     * @param name
     */
     int glGetAttribLocation (int program, science.unlicense.api.character.CharArray name);

    /**
     * @param program
     * @param pname
     * @param params
     */
     void glGetProgramiv (int program, int pname, java.nio.IntBuffer params);

    /**
     * Convenient method.
     *
     * @param program
     * @param pname
     * @param params
     */
     void glGetProgramiv (int program, int pname, int[] params);

    /**
     * @param program
     * @param bufSize
     * @param length
     * @param infoLog ,length bufSize
     */
     void glGetProgramInfoLog (int program, int[] length, java.nio.ByteBuffer infoLog);

    /**
     * @param shader
     * @param pname
     * @param params
     */
     void glGetShaderiv (int shader, int pname, java.nio.IntBuffer params);

    /**
     * Convenient method.
     *
     * @param shader
     * @param pname
     * @param params
     */
     void glGetShaderiv (int shader, int pname, int[] params);

    /**
     * @param shader
     * @param bufSize
     * @param length
     * @param infoLog ,length bufSize
     */
     void glGetShaderInfoLog (int shader, java.nio.IntBuffer length, java.nio.ByteBuffer infoLog);

    /**
     * @param shader
     * @param bufSize
     * @param length
     * @param source ,length bufSize
     */
     void glGetShaderSource (int shader, java.nio.IntBuffer length, java.nio.ByteBuffer source);

    /**
     * @param program
     * @param name
     */
     int glGetUniformLocation (int program, science.unlicense.api.character.CharArray name);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformfv (int program, int location, java.nio.FloatBuffer params);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformiv (int program, int location, java.nio.IntBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribdv (int index, int pname, java.nio.DoubleBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribfv (int index, int pname, java.nio.FloatBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribiv (int index, int pname, java.nio.IntBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPointerPropertyARB
     * @param pointer
     */
     void glGetVertexAttribPointerv (int index, int pname, java.nio.ByteBuffer pointer);

    /**
     * @param program
     */
     boolean glIsProgram (int program);

    /**
     * @param shader
     */
     boolean glIsShader (int shader);

    /**
     * @param program
     */
     void glLinkProgram (int program);

    /**
     * @param shader
     * @param count
     * @param string ,length count
     * @param length ,length count
     */
     void glShaderSource (int shader, CharArray[] strings);

    /**
     * @param program
     */
     void glUseProgram (int program);

    /**
     * @param location
     * @param v0
     */
     void glUniform1f (int location, float v0);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2f (int location, float v0, float v1);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3f (int location, float v0, float v1, float v2);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4f (int location, float v0, float v1, float v2, float v3);

    /**
     * @param location
     * @param v0
     */
     void glUniform1i (int location, int v0);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2i (int location, int v0, int v1);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3i (int location, int v0, int v1, int v2);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4i (int location, int v0, int v1, int v2, int v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1fv (int location, float[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform2fv (int location, float[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform3fv (int location, float[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform4fv (int location, float[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1iv (int location, int[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform2iv (int location, int[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform3iv (int location, int[] value);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform4iv (int location, int[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2fv (int location, boolean transpose, float[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3fv (int location, boolean transpose, float[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4fv (int location, boolean transpose, float[] value);

    /**
     * @param program
     */
     void glValidateProgram (int program);

    /**
     * @param index
     * @param x
     */
     void glVertexAttrib1d (int index, double x);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib1dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param x
     */
     void glVertexAttrib1f (int index, float x);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib1fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     */
     void glVertexAttrib1s (int index, short x);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib1sv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttrib2d (int index, double x, double y);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib2dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttrib2f (int index, float x, float y);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib2fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttrib2s (int index, short x, short y);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib2sv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttrib3d (int index, double x, double y, double z);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib3dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttrib3f (int index, float x, float y, float z);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib3fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttrib3s (int index, short x, short y, short z);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib3sv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4Nbv (int index, java.nio.ByteBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4Niv (int index, java.nio.IntBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4Nsv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttrib4Nub (int index, byte x, byte y, byte z, byte w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4Nubv (int index, java.nio.ByteBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4Nuiv (int index, java.nio.IntBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4Nusv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4bv (int index, java.nio.ByteBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttrib4d (int index, double x, double y, double z, double w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttrib4f (int index, float x, float y, float z, float w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4iv (int index, java.nio.IntBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttrib4s (int index, short x, short y, short z, short w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4sv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4ubv (int index, java.nio.ByteBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4uiv (int index, java.nio.IntBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4usv (int index, java.nio.ShortBuffer v);

    /**
     * @param index
     * @param size
     * @param type ,value from enumeration group VertexAttribPointerType
     * @param normalized ,value from enumeration group Boolean
     * @param stride
     * @param pointer
     */
     void glVertexAttribPointer (int index, int size, int type, boolean normalized, int stride, long pointer);

}