package science.unlicense.api.gpu.opengl;

public interface GL10 {

    /**
     * @param mode ,value from enumeration group CullFaceMode
     */
     void glCullFace (int mode);

    /**
     * @param mode ,value from enumeration group FrontFaceDirection
     */
     void glFrontFace (int mode);

    /**
     * @param target ,value from enumeration group HintTarget
     * @param mode ,value from enumeration group HintMode
     */
     void glHint (int target, int mode);

    /**
     * @param width ,value from enumeration group CheckedFloat32
     */
     void glLineWidth (float width);

    /**
     * @param size ,value from enumeration group CheckedFloat32
     */
     void glPointSize (float size);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param mode ,value from enumeration group PolygonMode
     */
     void glPolygonMode (int face, int mode);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glScissor (int x, int y, int width, int height);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexParameterf (int target, int pname, float param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexParameterfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexParameteri (int target, int pname, int param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group TextureComponentCount
     * @param width
     * @param border ,value from enumeration group CheckedInt32
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexImage1D (int target, int level, int internalformat, int width, int border, int format, int type, java.nio.Buffer pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group TextureComponentCount
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexImage2D (int target, int level, int internalformat, int width, int height, int border, int format, int type, java.nio.Buffer pixels);

    /**
     * @param buf ,value from enumeration group DrawBufferMode
     */
     void glDrawBuffer (int buf);

    /**
     * @param mask ,value from enumeration group ClearBufferMask
     */
     void glClear (int mask);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glClearColor (float red, float green, float blue, float alpha);

    /**
     * @param s ,value from enumeration group StencilValue
     */
     void glClearStencil (int s);

    /**
     * @param depth
     */
     void glClearDepth (double depth);

    /**
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMask (int mask);

    /**
     * @param red ,value from enumeration group Boolean
     * @param green ,value from enumeration group Boolean
     * @param blue ,value from enumeration group Boolean
     * @param alpha ,value from enumeration group Boolean
     */
     void glColorMask (boolean red, boolean green, boolean blue, boolean alpha);

    /**
     * @param flag ,value from enumeration group Boolean
     */
     void glDepthMask (boolean flag);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glDisable (int cap);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glEnable (int cap);

     void glFinish ();

     void glFlush ();

    /**
     * @param sfactor ,value from enumeration group BlendingFactorSrc
     * @param dfactor ,value from enumeration group BlendingFactorDest
     */
     void glBlendFunc (int sfactor, int dfactor);

    /**
     * @param opcode ,value from enumeration group LogicOp
     */
     void glLogicOp (int opcode);

    /**
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFunc (int func, int ref, int mask);

    /**
     * @param fail ,value from enumeration group StencilOp
     * @param zfail ,value from enumeration group StencilOp
     * @param zpass ,value from enumeration group StencilOp
     */
     void glStencilOp (int fail, int zfail, int zpass);

    /**
     * @param func ,value from enumeration group DepthFunction
     */
     void glDepthFunc (int func);

    /**
     * @param pname ,value from enumeration group PixelStoreParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glPixelStoref (int pname, float param);

    /**
     * @param pname ,value from enumeration group PixelStoreParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glPixelStorei (int pname, int param);

    /**
     * @param src ,value from enumeration group ReadBufferMode
     */
     void glReadBuffer (int src);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glReadPixels (int x, int y, int width, int height, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data ,value from enumeration group Boolean
     */
     void glGetBooleanv (int pname, java.nio.ByteBuffer data);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetDoublev (int pname, java.nio.DoubleBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetDoublev (int pname, double[] data);

     int glGetError ();

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, java.nio.FloatBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, float[] data);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, java.nio.IntBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, int[] data);

    /**
     * @param name ,value from enumeration group StringName
     */
     byte glGetString (int name);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glGetTexImage (int target, int level, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexLevelParameterfv (int target, int level, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexLevelParameteriv (int target, int level, int pname, java.nio.IntBuffer params);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     boolean glIsEnabled (int cap);

    /**
     * @param near
     * @param far
     */
     void glDepthRange (double near, double far);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glViewport (int x, int y, int width, int height);

    /**
     * @param list ,value from enumeration group List
     * @param mode ,value from enumeration group ListMode
     */
     void glNewList (int list, int mode);

     void glEndList ();

    /**
     * @param list ,value from enumeration group List
     */
     void glCallList (int list);

    /**
     * @param n
     * @param type ,value from enumeration group ListNameType
     * @param lists
     */
     void glCallLists (int n, int type, java.nio.ByteBuffer lists);

    /**
     * @param list ,value from enumeration group List
     * @param range
     */
     void glDeleteLists (int list, int range);

    /**
     * @param range
     */
     int glGenLists (int range);

    /**
     * @param base ,value from enumeration group List
     */
     void glListBase (int base);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     */
     void glBegin (int mode);

    /**
     * @param width
     * @param height
     * @param xorig ,value from enumeration group CoordF
     * @param yorig ,value from enumeration group CoordF
     * @param xmove ,value from enumeration group CoordF
     * @param ymove ,value from enumeration group CoordF
     * @param bitmap
     */
     void glBitmap (int width, int height, float xorig, float yorig, float xmove, float ymove, java.nio.ByteBuffer bitmap);

    /**
     * @param red ,value from enumeration group ColorB
     * @param green ,value from enumeration group ColorB
     * @param blue ,value from enumeration group ColorB
     */
     void glColor3b (byte red, byte green, byte blue);

    /**
     * @param v ,value from enumeration group ColorB
     */
     void glColor3bv (java.nio.ByteBuffer v);

    /**
     * @param red ,value from enumeration group ColorD
     * @param green ,value from enumeration group ColorD
     * @param blue ,value from enumeration group ColorD
     */
     void glColor3d (double red, double green, double blue);

    /**
     * @param v ,value from enumeration group ColorD
     */
     void glColor3dv (java.nio.DoubleBuffer v);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     */
     void glColor3f (float red, float green, float blue);

    /**
     * @param v ,value from enumeration group ColorF
     */
     void glColor3fv (java.nio.FloatBuffer v);

    /**
     * @param red ,value from enumeration group ColorI
     * @param green ,value from enumeration group ColorI
     * @param blue ,value from enumeration group ColorI
     */
     void glColor3i (int red, int green, int blue);

    /**
     * @param v ,value from enumeration group ColorI
     */
     void glColor3iv (java.nio.IntBuffer v);

    /**
     * @param red ,value from enumeration group ColorS
     * @param green ,value from enumeration group ColorS
     * @param blue ,value from enumeration group ColorS
     */
     void glColor3s (short red, short green, short blue);

    /**
     * @param v ,value from enumeration group ColorS
     */
     void glColor3sv (java.nio.ShortBuffer v);

    /**
     * @param red ,value from enumeration group ColorUB
     * @param green ,value from enumeration group ColorUB
     * @param blue ,value from enumeration group ColorUB
     */
     void glColor3ub (byte red, byte green, byte blue);

    /**
     * @param v ,value from enumeration group ColorUB
     */
     void glColor3ubv (java.nio.ByteBuffer v);

    /**
     * @param red ,value from enumeration group ColorUI
     * @param green ,value from enumeration group ColorUI
     * @param blue ,value from enumeration group ColorUI
     */
     void glColor3ui (int red, int green, int blue);

    /**
     * @param v ,value from enumeration group ColorUI
     */
     void glColor3uiv (java.nio.IntBuffer v);

    /**
     * @param red ,value from enumeration group ColorUS
     * @param green ,value from enumeration group ColorUS
     * @param blue ,value from enumeration group ColorUS
     */
     void glColor3us (short red, short green, short blue);

    /**
     * @param v ,value from enumeration group ColorUS
     */
     void glColor3usv (java.nio.ShortBuffer v);

    /**
     * @param red ,value from enumeration group ColorB
     * @param green ,value from enumeration group ColorB
     * @param blue ,value from enumeration group ColorB
     * @param alpha ,value from enumeration group ColorB
     */
     void glColor4b (byte red, byte green, byte blue, byte alpha);

    /**
     * @param v ,value from enumeration group ColorB
     */
     void glColor4bv (java.nio.ByteBuffer v);

    /**
     * @param red ,value from enumeration group ColorD
     * @param green ,value from enumeration group ColorD
     * @param blue ,value from enumeration group ColorD
     * @param alpha ,value from enumeration group ColorD
     */
     void glColor4d (double red, double green, double blue, double alpha);

    /**
     * @param v ,value from enumeration group ColorD
     */
     void glColor4dv (java.nio.DoubleBuffer v);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glColor4f (float red, float green, float blue, float alpha);

    /**
     * @param v ,value from enumeration group ColorF
     */
     void glColor4fv (java.nio.FloatBuffer v);

    /**
     * @param red ,value from enumeration group ColorI
     * @param green ,value from enumeration group ColorI
     * @param blue ,value from enumeration group ColorI
     * @param alpha ,value from enumeration group ColorI
     */
     void glColor4i (int red, int green, int blue, int alpha);

    /**
     * @param v ,value from enumeration group ColorI
     */
     void glColor4iv (java.nio.IntBuffer v);

    /**
     * @param red ,value from enumeration group ColorS
     * @param green ,value from enumeration group ColorS
     * @param blue ,value from enumeration group ColorS
     * @param alpha ,value from enumeration group ColorS
     */
     void glColor4s (short red, short green, short blue, short alpha);

    /**
     * @param v ,value from enumeration group ColorS
     */
     void glColor4sv (java.nio.ShortBuffer v);

    /**
     * @param red ,value from enumeration group ColorUB
     * @param green ,value from enumeration group ColorUB
     * @param blue ,value from enumeration group ColorUB
     * @param alpha ,value from enumeration group ColorUB
     */
     void glColor4ub (byte red, byte green, byte blue, byte alpha);

    /**
     * @param v ,value from enumeration group ColorUB
     */
     void glColor4ubv (java.nio.ByteBuffer v);

    /**
     * @param red ,value from enumeration group ColorUI
     * @param green ,value from enumeration group ColorUI
     * @param blue ,value from enumeration group ColorUI
     * @param alpha ,value from enumeration group ColorUI
     */
     void glColor4ui (int red, int green, int blue, int alpha);

    /**
     * @param v ,value from enumeration group ColorUI
     */
     void glColor4uiv (java.nio.IntBuffer v);

    /**
     * @param red ,value from enumeration group ColorUS
     * @param green ,value from enumeration group ColorUS
     * @param blue ,value from enumeration group ColorUS
     * @param alpha ,value from enumeration group ColorUS
     */
     void glColor4us (short red, short green, short blue, short alpha);

    /**
     * @param v ,value from enumeration group ColorUS
     */
     void glColor4usv (java.nio.ShortBuffer v);

    /**
     * @param flag ,value from enumeration group Boolean
     */
     void glEdgeFlag (boolean flag);

    /**
     * @param flag ,value from enumeration group Boolean
     */
     void glEdgeFlagv (java.nio.ByteBuffer flag);

     void glEnd ();

    /**
     * @param c ,value from enumeration group ColorIndexValueD
     */
     void glIndexd (double c);

    /**
     * @param c ,value from enumeration group ColorIndexValueD
     */
     void glIndexdv (java.nio.DoubleBuffer c);

    /**
     * @param c ,value from enumeration group ColorIndexValueF
     */
     void glIndexf (float c);

    /**
     * @param c ,value from enumeration group ColorIndexValueF
     */
     void glIndexfv (java.nio.FloatBuffer c);

    /**
     * @param c ,value from enumeration group ColorIndexValueI
     */
     void glIndexi (int c);

    /**
     * @param c ,value from enumeration group ColorIndexValueI
     */
     void glIndexiv (java.nio.IntBuffer c);

    /**
     * @param c ,value from enumeration group ColorIndexValueS
     */
     void glIndexs (short c);

    /**
     * @param c ,value from enumeration group ColorIndexValueS
     */
     void glIndexsv (java.nio.ShortBuffer c);

    /**
     * @param nx
     * @param ny
     * @param nz
     */
     void glNormal3b (byte nx, byte ny, byte nz);

    /**
     * @param v
     */
     void glNormal3bv (java.nio.ByteBuffer v);

    /**
     * @param nx ,value from enumeration group CoordD
     * @param ny ,value from enumeration group CoordD
     * @param nz ,value from enumeration group CoordD
     */
     void glNormal3d (double nx, double ny, double nz);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glNormal3dv (java.nio.DoubleBuffer v);

    /**
     * @param nx ,value from enumeration group CoordF
     * @param ny ,value from enumeration group CoordF
     * @param nz ,value from enumeration group CoordF
     */
     void glNormal3f (float nx, float ny, float nz);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glNormal3fv (java.nio.FloatBuffer v);

    /**
     * @param nx
     * @param ny
     * @param nz
     */
     void glNormal3i (int nx, int ny, int nz);

    /**
     * @param v
     */
     void glNormal3iv (java.nio.IntBuffer v);

    /**
     * @param nx
     * @param ny
     * @param nz
     */
     void glNormal3s (short nx, short ny, short nz);

    /**
     * @param v
     */
     void glNormal3sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     */
     void glRasterPos2d (double x, double y);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glRasterPos2dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     */
     void glRasterPos2f (float x, float y);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glRasterPos2fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     */
     void glRasterPos2i (int x, int y);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glRasterPos2iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     */
     void glRasterPos2s (short x, short y);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glRasterPos2sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     * @param z ,value from enumeration group CoordD
     */
     void glRasterPos3d (double x, double y, double z);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glRasterPos3dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     * @param z ,value from enumeration group CoordF
     */
     void glRasterPos3f (float x, float y, float z);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glRasterPos3fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     * @param z ,value from enumeration group CoordI
     */
     void glRasterPos3i (int x, int y, int z);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glRasterPos3iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     * @param z ,value from enumeration group CoordS
     */
     void glRasterPos3s (short x, short y, short z);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glRasterPos3sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     * @param z ,value from enumeration group CoordD
     * @param w ,value from enumeration group CoordD
     */
     void glRasterPos4d (double x, double y, double z, double w);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glRasterPos4dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     * @param z ,value from enumeration group CoordF
     * @param w ,value from enumeration group CoordF
     */
     void glRasterPos4f (float x, float y, float z, float w);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glRasterPos4fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     * @param z ,value from enumeration group CoordI
     * @param w ,value from enumeration group CoordI
     */
     void glRasterPos4i (int x, int y, int z, int w);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glRasterPos4iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     * @param z ,value from enumeration group CoordS
     * @param w ,value from enumeration group CoordS
     */
     void glRasterPos4s (short x, short y, short z, short w);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glRasterPos4sv (java.nio.ShortBuffer v);

    /**
     * @param x1 ,value from enumeration group CoordD
     * @param y1 ,value from enumeration group CoordD
     * @param x2 ,value from enumeration group CoordD
     * @param y2 ,value from enumeration group CoordD
     */
     void glRectd (double x1, double y1, double x2, double y2);

    /**
     * @param v1 ,value from enumeration group CoordD
     * @param v2 ,value from enumeration group CoordD
     */
     void glRectdv (java.nio.DoubleBuffer v1, java.nio.DoubleBuffer v2);

    /**
     * @param x1 ,value from enumeration group CoordF
     * @param y1 ,value from enumeration group CoordF
     * @param x2 ,value from enumeration group CoordF
     * @param y2 ,value from enumeration group CoordF
     */
     void glRectf (float x1, float y1, float x2, float y2);

    /**
     * @param v1 ,value from enumeration group CoordF
     * @param v2 ,value from enumeration group CoordF
     */
     void glRectfv (java.nio.FloatBuffer v1, java.nio.FloatBuffer v2);

    /**
     * @param x1 ,value from enumeration group CoordI
     * @param y1 ,value from enumeration group CoordI
     * @param x2 ,value from enumeration group CoordI
     * @param y2 ,value from enumeration group CoordI
     */
     void glRecti (int x1, int y1, int x2, int y2);

    /**
     * @param v1 ,value from enumeration group CoordI
     * @param v2 ,value from enumeration group CoordI
     */
     void glRectiv (java.nio.IntBuffer v1, java.nio.IntBuffer v2);

    /**
     * @param x1 ,value from enumeration group CoordS
     * @param y1 ,value from enumeration group CoordS
     * @param x2 ,value from enumeration group CoordS
     * @param y2 ,value from enumeration group CoordS
     */
     void glRects (short x1, short y1, short x2, short y2);

    /**
     * @param v1 ,value from enumeration group CoordS
     * @param v2 ,value from enumeration group CoordS
     */
     void glRectsv (java.nio.ShortBuffer v1, java.nio.ShortBuffer v2);

    /**
     * @param s ,value from enumeration group CoordD
     */
     void glTexCoord1d (double s);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glTexCoord1dv (java.nio.DoubleBuffer v);

    /**
     * @param s ,value from enumeration group CoordF
     */
     void glTexCoord1f (float s);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glTexCoord1fv (java.nio.FloatBuffer v);

    /**
     * @param s ,value from enumeration group CoordI
     */
     void glTexCoord1i (int s);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glTexCoord1iv (java.nio.IntBuffer v);

    /**
     * @param s ,value from enumeration group CoordS
     */
     void glTexCoord1s (short s);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glTexCoord1sv (java.nio.ShortBuffer v);

    /**
     * @param s ,value from enumeration group CoordD
     * @param t ,value from enumeration group CoordD
     */
     void glTexCoord2d (double s, double t);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glTexCoord2dv (java.nio.DoubleBuffer v);

    /**
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     */
     void glTexCoord2f (float s, float t);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glTexCoord2fv (java.nio.FloatBuffer v);

    /**
     * @param s ,value from enumeration group CoordI
     * @param t ,value from enumeration group CoordI
     */
     void glTexCoord2i (int s, int t);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glTexCoord2iv (java.nio.IntBuffer v);

    /**
     * @param s ,value from enumeration group CoordS
     * @param t ,value from enumeration group CoordS
     */
     void glTexCoord2s (short s, short t);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glTexCoord2sv (java.nio.ShortBuffer v);

    /**
     * @param s ,value from enumeration group CoordD
     * @param t ,value from enumeration group CoordD
     * @param r ,value from enumeration group CoordD
     */
     void glTexCoord3d (double s, double t, double r);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glTexCoord3dv (java.nio.DoubleBuffer v);

    /**
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     * @param r ,value from enumeration group CoordF
     */
     void glTexCoord3f (float s, float t, float r);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glTexCoord3fv (java.nio.FloatBuffer v);

    /**
     * @param s ,value from enumeration group CoordI
     * @param t ,value from enumeration group CoordI
     * @param r ,value from enumeration group CoordI
     */
     void glTexCoord3i (int s, int t, int r);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glTexCoord3iv (java.nio.IntBuffer v);

    /**
     * @param s ,value from enumeration group CoordS
     * @param t ,value from enumeration group CoordS
     * @param r ,value from enumeration group CoordS
     */
     void glTexCoord3s (short s, short t, short r);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glTexCoord3sv (java.nio.ShortBuffer v);

    /**
     * @param s ,value from enumeration group CoordD
     * @param t ,value from enumeration group CoordD
     * @param r ,value from enumeration group CoordD
     * @param q ,value from enumeration group CoordD
     */
     void glTexCoord4d (double s, double t, double r, double q);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glTexCoord4dv (java.nio.DoubleBuffer v);

    /**
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     * @param r ,value from enumeration group CoordF
     * @param q ,value from enumeration group CoordF
     */
     void glTexCoord4f (float s, float t, float r, float q);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glTexCoord4fv (java.nio.FloatBuffer v);

    /**
     * @param s ,value from enumeration group CoordI
     * @param t ,value from enumeration group CoordI
     * @param r ,value from enumeration group CoordI
     * @param q ,value from enumeration group CoordI
     */
     void glTexCoord4i (int s, int t, int r, int q);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glTexCoord4iv (java.nio.IntBuffer v);

    /**
     * @param s ,value from enumeration group CoordS
     * @param t ,value from enumeration group CoordS
     * @param r ,value from enumeration group CoordS
     * @param q ,value from enumeration group CoordS
     */
     void glTexCoord4s (short s, short t, short r, short q);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glTexCoord4sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     */
     void glVertex2d (double x, double y);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glVertex2dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     */
     void glVertex2f (float x, float y);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glVertex2fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     */
     void glVertex2i (int x, int y);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glVertex2iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     */
     void glVertex2s (short x, short y);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glVertex2sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     * @param z ,value from enumeration group CoordD
     */
     void glVertex3d (double x, double y, double z);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glVertex3dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     * @param z ,value from enumeration group CoordF
     */
     void glVertex3f (float x, float y, float z);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glVertex3fv (java.nio.FloatBuffer v);

    /**
     * Convenient method.
     *
     * @param v ,value from enumeration group CoordF
     */
     void glVertex3fv (float[] v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     * @param z ,value from enumeration group CoordI
     */
     void glVertex3i (int x, int y, int z);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glVertex3iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     * @param z ,value from enumeration group CoordS
     */
     void glVertex3s (short x, short y, short z);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glVertex3sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     * @param z ,value from enumeration group CoordD
     * @param w ,value from enumeration group CoordD
     */
     void glVertex4d (double x, double y, double z, double w);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glVertex4dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     * @param z ,value from enumeration group CoordF
     * @param w ,value from enumeration group CoordF
     */
     void glVertex4f (float x, float y, float z, float w);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glVertex4fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     * @param z ,value from enumeration group CoordI
     * @param w ,value from enumeration group CoordI
     */
     void glVertex4i (int x, int y, int z, int w);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glVertex4iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     * @param z ,value from enumeration group CoordS
     * @param w ,value from enumeration group CoordS
     */
     void glVertex4s (short x, short y, short z, short w);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glVertex4sv (java.nio.ShortBuffer v);

    /**
     * @param plane ,value from enumeration group ClipPlaneName
     * @param equation
     */
     void glClipPlane (int plane, java.nio.DoubleBuffer equation);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param mode ,value from enumeration group ColorMaterialParameter
     */
     void glColorMaterial (int face, int mode);

    /**
     * @param pname ,value from enumeration group FogParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glFogf (int pname, float param);

    /**
     * @param pname ,value from enumeration group FogParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glFogfv (int pname, java.nio.FloatBuffer params);

    /**
     * @param pname ,value from enumeration group FogParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glFogi (int pname, int param);

    /**
     * @param pname ,value from enumeration group FogParameter
     * @param params ,value from enumeration group CheckedInt32
     */
     void glFogiv (int pname, java.nio.IntBuffer params);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glLightf (int light, int pname, float param);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glLightfv (int light, int pname, java.nio.FloatBuffer params);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glLighti (int light, int pname, int param);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param params ,value from enumeration group CheckedInt32
     */
     void glLightiv (int light, int pname, java.nio.IntBuffer params);

    /**
     * @param pname ,value from enumeration group LightModelParameter
     * @param param
     */
     void glLightModelf (int pname, float param);

    /**
     * @param pname ,value from enumeration group LightModelParameter
     * @param params
     */
     void glLightModelfv (int pname, java.nio.FloatBuffer params);

    /**
     * @param pname ,value from enumeration group LightModelParameter
     * @param param
     */
     void glLightModeli (int pname, int param);

    /**
     * @param pname ,value from enumeration group LightModelParameter
     * @param params
     */
     void glLightModeliv (int pname, java.nio.IntBuffer params);

    /**
     * @param factor ,value from enumeration group CheckedInt32
     * @param pattern ,value from enumeration group LineStipple
     */
     void glLineStipple (int factor, short pattern);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glMaterialf (int face, int pname, float param);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glMaterialfv (int face, int pname, java.nio.FloatBuffer params);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glMateriali (int face, int pname, int param);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param params ,value from enumeration group CheckedInt32
     */
     void glMaterialiv (int face, int pname, java.nio.IntBuffer params);

    /**
     * @param mask
     */
     void glPolygonStipple (java.nio.ByteBuffer mask);

    /**
     * @param mode ,value from enumeration group ShadingModel
     */
     void glShadeModel (int mode);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexEnvf (int target, int pname, float param);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexEnvfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexEnvi (int target, int pname, int param);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexEnviv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param param
     */
     void glTexGend (int coord, int pname, double param);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param params
     */
     void glTexGendv (int coord, int pname, java.nio.DoubleBuffer params);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexGenf (int coord, int pname, float param);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexGenfv (int coord, int pname, java.nio.FloatBuffer params);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexGeni (int coord, int pname, int param);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexGeniv (int coord, int pname, java.nio.IntBuffer params);

    /**
     * @param size
     * @param type ,value from enumeration group FeedbackType
     * @param buffer ,value from enumeration group FeedbackElement ,length size
     */
     void glFeedbackBuffer (int type, java.nio.FloatBuffer buffer);

    /**
     * @param size
     * @param buffer ,value from enumeration group SelectName ,length size
     */
     void glSelectBuffer (java.nio.IntBuffer buffer);

    /**
     * @param mode ,value from enumeration group RenderingMode
     */
     int glRenderMode (int mode);

     void glInitNames ();

    /**
     * @param name ,value from enumeration group SelectName
     */
     void glLoadName (int name);

    /**
     * @param token ,value from enumeration group FeedbackElement
     */
     void glPassThrough (float token);

     void glPopName ();

    /**
     * @param name ,value from enumeration group SelectName
     */
     void glPushName (int name);

    /**
     * @param red
     * @param green
     * @param blue
     * @param alpha
     */
     void glClearAccum (float red, float green, float blue, float alpha);

    /**
     * @param c ,value from enumeration group MaskedColorIndexValueF
     */
     void glClearIndex (float c);

    /**
     * @param mask ,value from enumeration group MaskedColorIndexValueI
     */
     void glIndexMask (int mask);

    /**
     * @param op ,value from enumeration group AccumOp
     * @param value ,value from enumeration group CoordF
     */
     void glAccum (int op, float value);

     void glPopAttrib ();

    /**
     * @param mask ,value from enumeration group AttribMask
     */
     void glPushAttrib (int mask);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param u1 ,value from enumeration group CoordD
     * @param u2 ,value from enumeration group CoordD
     * @param stride
     * @param order ,value from enumeration group CheckedInt32
     * @param points ,value from enumeration group CoordD
     */
     void glMap1d (int target, double u1, double u2, int stride, int order, java.nio.DoubleBuffer points);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param u1 ,value from enumeration group CoordF
     * @param u2 ,value from enumeration group CoordF
     * @param stride
     * @param order ,value from enumeration group CheckedInt32
     * @param points ,value from enumeration group CoordF
     */
     void glMap1f (int target, float u1, float u2, int stride, int order, java.nio.FloatBuffer points);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param u1 ,value from enumeration group CoordD
     * @param u2 ,value from enumeration group CoordD
     * @param ustride
     * @param uorder ,value from enumeration group CheckedInt32
     * @param v1 ,value from enumeration group CoordD
     * @param v2 ,value from enumeration group CoordD
     * @param vstride
     * @param vorder ,value from enumeration group CheckedInt32
     * @param points ,value from enumeration group CoordD
     */
     void glMap2d (int target, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, java.nio.DoubleBuffer points);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param u1 ,value from enumeration group CoordF
     * @param u2 ,value from enumeration group CoordF
     * @param ustride
     * @param uorder ,value from enumeration group CheckedInt32
     * @param v1 ,value from enumeration group CoordF
     * @param v2 ,value from enumeration group CoordF
     * @param vstride
     * @param vorder ,value from enumeration group CheckedInt32
     * @param points ,value from enumeration group CoordF
     */
     void glMap2f (int target, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, java.nio.FloatBuffer points);

    /**
     * @param un
     * @param u1 ,value from enumeration group CoordD
     * @param u2 ,value from enumeration group CoordD
     */
     void glMapGrid1d (int un, double u1, double u2);

    /**
     * @param un
     * @param u1 ,value from enumeration group CoordF
     * @param u2 ,value from enumeration group CoordF
     */
     void glMapGrid1f (int un, float u1, float u2);

    /**
     * @param un
     * @param u1 ,value from enumeration group CoordD
     * @param u2 ,value from enumeration group CoordD
     * @param vn
     * @param v1 ,value from enumeration group CoordD
     * @param v2 ,value from enumeration group CoordD
     */
     void glMapGrid2d (int un, double u1, double u2, int vn, double v1, double v2);

    /**
     * @param un
     * @param u1 ,value from enumeration group CoordF
     * @param u2 ,value from enumeration group CoordF
     * @param vn
     * @param v1 ,value from enumeration group CoordF
     * @param v2 ,value from enumeration group CoordF
     */
     void glMapGrid2f (int un, float u1, float u2, int vn, float v1, float v2);

    /**
     * @param u ,value from enumeration group CoordD
     */
     void glEvalCoord1d (double u);

    /**
     * @param u ,value from enumeration group CoordD
     */
     void glEvalCoord1dv (java.nio.DoubleBuffer u);

    /**
     * @param u ,value from enumeration group CoordF
     */
     void glEvalCoord1f (float u);

    /**
     * @param u ,value from enumeration group CoordF
     */
     void glEvalCoord1fv (java.nio.FloatBuffer u);

    /**
     * @param u ,value from enumeration group CoordD
     * @param v ,value from enumeration group CoordD
     */
     void glEvalCoord2d (double u, double v);

    /**
     * @param u ,value from enumeration group CoordD
     */
     void glEvalCoord2dv (java.nio.DoubleBuffer u);

    /**
     * @param u ,value from enumeration group CoordF
     * @param v ,value from enumeration group CoordF
     */
     void glEvalCoord2f (float u, float v);

    /**
     * @param u ,value from enumeration group CoordF
     */
     void glEvalCoord2fv (java.nio.FloatBuffer u);

    /**
     * @param mode ,value from enumeration group MeshMode1
     * @param i1 ,value from enumeration group CheckedInt32
     * @param i2 ,value from enumeration group CheckedInt32
     */
     void glEvalMesh1 (int mode, int i1, int i2);

    /**
     * @param i
     */
     void glEvalPoint1 (int i);

    /**
     * @param mode ,value from enumeration group MeshMode2
     * @param i1 ,value from enumeration group CheckedInt32
     * @param i2 ,value from enumeration group CheckedInt32
     * @param j1 ,value from enumeration group CheckedInt32
     * @param j2 ,value from enumeration group CheckedInt32
     */
     void glEvalMesh2 (int mode, int i1, int i2, int j1, int j2);

    /**
     * @param i ,value from enumeration group CheckedInt32
     * @param j ,value from enumeration group CheckedInt32
     */
     void glEvalPoint2 (int i, int j);

    /**
     * @param func ,value from enumeration group AlphaFunction
     * @param ref
     */
     void glAlphaFunc (int func, float ref);

    /**
     * @param xfactor
     * @param yfactor
     */
     void glPixelZoom (float xfactor, float yfactor);

    /**
     * @param pname ,value from enumeration group PixelTransferParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glPixelTransferf (int pname, float param);

    /**
     * @param pname ,value from enumeration group PixelTransferParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glPixelTransferi (int pname, int param);

    /**
     * @param map ,value from enumeration group PixelMap
     * @param mapsize ,value from enumeration group CheckedInt32
     * @param values ,length mapsize
     */
     void glPixelMapfv (int map, java.nio.FloatBuffer values);

    /**
     * @param map ,value from enumeration group PixelMap
     * @param mapsize ,value from enumeration group CheckedInt32
     * @param values ,length mapsize
     */
     void glPixelMapuiv (int map, java.nio.IntBuffer values);

    /**
     * @param map ,value from enumeration group PixelMap
     * @param mapsize ,value from enumeration group CheckedInt32
     * @param values ,length mapsize
     */
     void glPixelMapusv (int map, java.nio.ShortBuffer values);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param type ,value from enumeration group PixelCopyType
     */
     void glCopyPixels (int x, int y, int width, int height, int type);

    /**
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glDrawPixels (int width, int height, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param plane ,value from enumeration group ClipPlaneName
     * @param equation
     */
     void glGetClipPlane (int plane, java.nio.DoubleBuffer equation);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param params
     */
     void glGetLightfv (int light, int pname, java.nio.FloatBuffer params);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param params
     */
     void glGetLightiv (int light, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param query ,value from enumeration group GetMapQuery
     * @param v
     */
     void glGetMapdv (int target, int query, java.nio.DoubleBuffer v);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param query ,value from enumeration group GetMapQuery
     * @param v
     */
     void glGetMapfv (int target, int query, java.nio.FloatBuffer v);

    /**
     * @param target ,value from enumeration group MapTarget
     * @param query ,value from enumeration group GetMapQuery
     * @param v
     */
     void glGetMapiv (int target, int query, java.nio.IntBuffer v);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param params
     */
     void glGetMaterialfv (int face, int pname, java.nio.FloatBuffer params);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param params
     */
     void glGetMaterialiv (int face, int pname, java.nio.IntBuffer params);

    /**
     * @param map ,value from enumeration group PixelMap
     * @param values
     */
     void glGetPixelMapfv (int map, java.nio.FloatBuffer values);

    /**
     * @param map ,value from enumeration group PixelMap
     * @param values
     */
     void glGetPixelMapuiv (int map, java.nio.IntBuffer values);

    /**
     * @param map ,value from enumeration group PixelMap
     * @param values
     */
     void glGetPixelMapusv (int map, java.nio.ShortBuffer values);

    /**
     * @param mask
     */
     void glGetPolygonStipple (java.nio.ByteBuffer mask);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params
     */
     void glGetTexEnvfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params
     */
     void glGetTexEnviv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param params
     */
     void glGetTexGendv (int coord, int pname, java.nio.DoubleBuffer params);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param params
     */
     void glGetTexGenfv (int coord, int pname, java.nio.FloatBuffer params);

    /**
     * @param coord ,value from enumeration group TextureCoordName
     * @param pname ,value from enumeration group TextureGenParameter
     * @param params
     */
     void glGetTexGeniv (int coord, int pname, java.nio.IntBuffer params);

    /**
     * @param list ,value from enumeration group List
     */
     boolean glIsList (int list);

    /**
     * @param left
     * @param right
     * @param bottom
     * @param top
     * @param zNear
     * @param zFar
     */
     void glFrustum (double left, double right, double bottom, double top, double zNear, double zFar);

     void glLoadIdentity ();

    /**
     * @param m
     */
     void glLoadMatrixf (java.nio.FloatBuffer m);

    /**
     * Convenient method.
     *
     * @param m
     */
     void glLoadMatrixf (float[] m);

    /**
     * @param m
     */
     void glLoadMatrixd (java.nio.DoubleBuffer m);

    /**
     * Convenient method.
     *
     * @param m
     */
     void glLoadMatrixd (double[] m);

    /**
     * @param mode ,value from enumeration group MatrixMode
     */
     void glMatrixMode (int mode);

    /**
     * @param m
     */
     void glMultMatrixf (java.nio.FloatBuffer m);

    /**
     * @param m
     */
     void glMultMatrixd (java.nio.DoubleBuffer m);

    /**
     * @param left
     * @param right
     * @param bottom
     * @param top
     * @param zNear
     * @param zFar
     */
     void glOrtho (double left, double right, double bottom, double top, double zNear, double zFar);

     void glPopMatrix ();

     void glPushMatrix ();

    /**
     * @param angle
     * @param x
     * @param y
     * @param z
     */
     void glRotated (double angle, double x, double y, double z);

    /**
     * @param angle
     * @param x
     * @param y
     * @param z
     */
     void glRotatef (float angle, float x, float y, float z);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glScaled (double x, double y, double z);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glScalef (float x, float y, float z);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glTranslated (double x, double y, double z);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glTranslatef (float x, float y, float z);

}