package science.unlicense.api.gpu.opengl;

public interface GL13 extends science.unlicense.api.gpu.opengl.GL12 {

    /**
     * @param texture ,value from enumeration group TextureUnit
     */
     void glActiveTexture (int texture);

    /**
     * @param value
     * @param invert ,value from enumeration group Boolean
     */
     void glSampleCoverage (float value, boolean invert);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param width
     * @param height
     * @param depth
     * @param border ,value from enumeration group CheckedInt32
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexImage3D (int target, int level, int internalformat, int width, int height, int depth, int border, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexImage2D (int target, int level, int internalformat, int width, int height, int border, java.nio.Buffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param width
     * @param border ,value from enumeration group CheckedInt32
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexImage1D (int target, int level, int internalformat, int width, int border, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param zoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param depth
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage3D (int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage1D (int target, int level, int xoffset, int width, int format, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param img ,value from enumeration group CompressedTextureARB
     */
     void glGetCompressedTexImage (int target, int level, java.nio.ByteBuffer img);

    /**
     * @param texture ,value from enumeration group TextureUnit
     */
     void glClientActiveTexture (int texture);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordD
     */
     void glMultiTexCoord1d (int target, double s);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordD
     */
     void glMultiTexCoord1dv (int target, java.nio.DoubleBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordF
     */
     void glMultiTexCoord1f (int target, float s);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordF
     */
     void glMultiTexCoord1fv (int target, java.nio.FloatBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordI
     */
     void glMultiTexCoord1i (int target, int s);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordI
     */
     void glMultiTexCoord1iv (int target, java.nio.IntBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordS
     */
     void glMultiTexCoord1s (int target, short s);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordS
     */
     void glMultiTexCoord1sv (int target, java.nio.ShortBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordD
     * @param t ,value from enumeration group CoordD
     */
     void glMultiTexCoord2d (int target, double s, double t);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordD
     */
     void glMultiTexCoord2dv (int target, java.nio.DoubleBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     */
     void glMultiTexCoord2f (int target, float s, float t);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordF
     */
     void glMultiTexCoord2fv (int target, java.nio.FloatBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordI
     * @param t ,value from enumeration group CoordI
     */
     void glMultiTexCoord2i (int target, int s, int t);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordI
     */
     void glMultiTexCoord2iv (int target, java.nio.IntBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordS
     * @param t ,value from enumeration group CoordS
     */
     void glMultiTexCoord2s (int target, short s, short t);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordS
     */
     void glMultiTexCoord2sv (int target, java.nio.ShortBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordD
     * @param t ,value from enumeration group CoordD
     * @param r ,value from enumeration group CoordD
     */
     void glMultiTexCoord3d (int target, double s, double t, double r);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordD
     */
     void glMultiTexCoord3dv (int target, java.nio.DoubleBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     * @param r ,value from enumeration group CoordF
     */
     void glMultiTexCoord3f (int target, float s, float t, float r);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordF
     */
     void glMultiTexCoord3fv (int target, java.nio.FloatBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordI
     * @param t ,value from enumeration group CoordI
     * @param r ,value from enumeration group CoordI
     */
     void glMultiTexCoord3i (int target, int s, int t, int r);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordI
     */
     void glMultiTexCoord3iv (int target, java.nio.IntBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordS
     * @param t ,value from enumeration group CoordS
     * @param r ,value from enumeration group CoordS
     */
     void glMultiTexCoord3s (int target, short s, short t, short r);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordS
     */
     void glMultiTexCoord3sv (int target, java.nio.ShortBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordD
     * @param t ,value from enumeration group CoordD
     * @param r ,value from enumeration group CoordD
     * @param q ,value from enumeration group CoordD
     */
     void glMultiTexCoord4d (int target, double s, double t, double r, double q);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordD
     */
     void glMultiTexCoord4dv (int target, java.nio.DoubleBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     * @param r ,value from enumeration group CoordF
     * @param q ,value from enumeration group CoordF
     */
     void glMultiTexCoord4f (int target, float s, float t, float r, float q);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordF
     */
     void glMultiTexCoord4fv (int target, java.nio.FloatBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordI
     * @param t ,value from enumeration group CoordI
     * @param r ,value from enumeration group CoordI
     * @param q ,value from enumeration group CoordI
     */
     void glMultiTexCoord4i (int target, int s, int t, int r, int q);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordI
     */
     void glMultiTexCoord4iv (int target, java.nio.IntBuffer v);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordS
     * @param t ,value from enumeration group CoordS
     * @param r ,value from enumeration group CoordS
     * @param q ,value from enumeration group CoordS
     */
     void glMultiTexCoord4s (int target, short s, short t, short r, short q);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param v ,value from enumeration group CoordS
     */
     void glMultiTexCoord4sv (int target, java.nio.ShortBuffer v);

    /**
     * @param m
     */
     void glLoadTransposeMatrixf (java.nio.FloatBuffer m);

    /**
     * @param m
     */
     void glLoadTransposeMatrixd (java.nio.DoubleBuffer m);

    /**
     * @param m
     */
     void glMultTransposeMatrixf (java.nio.FloatBuffer m);

    /**
     * @param m
     */
     void glMultTransposeMatrixd (java.nio.DoubleBuffer m);

}