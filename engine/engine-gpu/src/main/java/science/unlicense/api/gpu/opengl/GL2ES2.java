
package science.unlicense.api.gpu.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GL2ES2 extends GL,GLES220 {
    
}
