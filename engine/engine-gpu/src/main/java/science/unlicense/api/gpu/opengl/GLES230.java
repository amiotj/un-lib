package science.unlicense.api.gpu.opengl;

import java.nio.LongBuffer;
import science.unlicense.api.character.CharArray;

public interface GLES230 {

    /**
     * @param src ,value from enumeration group ReadBufferMode
     */
     void glReadBuffer (int src);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param start
     * @param end
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     */
     void glDrawRangeElements (int mode, int start, int end, int count, int type, java.nio.ByteBuffer indices);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group TextureComponentCount
     * @param width
     * @param height
     * @param depth
     * @param border ,value from enumeration group CheckedInt32
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexImage3D (int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param zoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param depth
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexSubImage3D (int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param zoffset ,value from enumeration group CheckedInt32
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glCopyTexSubImage3D (int target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param width
     * @param height
     * @param depth
     * @param border ,value from enumeration group CheckedInt32
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexImage3D (int target, int level, int internalformat, int width, int height, int depth, int border, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param zoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param depth
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage3D (int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, java.nio.ByteBuffer data);

    /**
     * @param n
     * @param ids ,length n
     */
     void glGenQueries (java.nio.IntBuffer ids);

    /**
     * Convenient method.
     *
     * @param n
     * @param ids ,length n
     */
     void glGenQueries (int[] ids);

    /**
     * @param n
     * @param ids ,length n
     */
     void glDeleteQueries (java.nio.IntBuffer ids);

    /**
     * Convenient method.
     *
     * @param n
     * @param ids ,length n
     */
     void glDeleteQueries (int[] ids);

    /**
     * @param id
     */
     boolean glIsQuery (int id);

    /**
     * @param target
     * @param id
     */
     void glBeginQuery (int target, int id);

    /**
     * @param target
     */
     void glEndQuery (int target);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glGetQueryiv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param id
     * @param pname
     * @param params
     */
     void glGetQueryObjectuiv (int id, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     */
     boolean glUnmapBuffer (int target);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPointerNameARB
     * @param params
     */
     void glGetBufferPointerv (int target, int pname, java.nio.ByteBuffer params);

     /**
      * Convenient method
      *
      * @param bufs
      */
    void glDrawBuffers(int[] bufs);


    /**
     * @param n
     * @param bufs ,value from enumeration group DrawBufferModeATI ,length n
     */
     void glDrawBuffers (java.nio.IntBuffer bufs);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix2x3fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix3x2fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix2x4fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix4x2fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix3x4fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix4x3fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param srcX0
     * @param srcY0
     * @param srcX1
     * @param srcY1
     * @param dstX0
     * @param dstY0
     * @param dstX1
     * @param dstY1
     * @param mask ,value from enumeration group ClearBufferMask
     * @param filter
     */
     void glBlitFramebuffer (int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     */
     void glRenderbufferStorageMultisample (int target, int samples, int internalformat, int width, int height);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param texture ,value from enumeration group Texture
     * @param level ,value from enumeration group CheckedInt32
     * @param layer ,value from enumeration group CheckedInt32
     */
     void glFramebufferTextureLayer (int target, int attachment, int texture, int level, int layer);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param length ,value from enumeration group BufferSize
     * @param access ,value from enumeration group BufferAccessMask
     */
     void glMapBufferRange (int target, long offset, long length, int access);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param length ,value from enumeration group BufferSize
     */
     void glFlushMappedBufferRange (int target, long offset, long length);

    /**
     * @param array
     */
     void glBindVertexArray (int array);

    /**
     * @param n
     * @param arrays ,length n
     */
     void glDeleteVertexArrays (java.nio.IntBuffer arrays);

    /**
     * Convenient method.
     *
     * @param n
     * @param arrays ,length n
     */
    void glDeleteVertexArrays (int[] arrays);

    /**
     * @param n
     * @param arrays ,length n
     */
    void glGenVertexArrays (java.nio.IntBuffer arrays);

    /**
     * Convenient method.
     *
     * @param n
     * @param arrays ,length n
     */
    void glGenVertexArrays (int[] arrays);

    /**
     * @param array
     */
     boolean glIsVertexArray (int array);

    /**
     * @param target
     * @param index
     * @param data
     */
     void glGetIntegeri_v (int target, int index, java.nio.IntBuffer data);

    /**
     * @param primitiveMode
     */
     void glBeginTransformFeedback (int primitiveMode);

     void glEndTransformFeedback ();

    /**
     * @param target
     * @param index
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glBindBufferRange (int target, int index, int buffer, long offset, long size);

    /**
     * @param target
     * @param index
     * @param buffer
     */
     void glBindBufferBase (int target, int index, int buffer);

    /**
     * @param program
     * @param count
     * @param varyings ,length count
     * @param bufferMode
     */
     void glTransformFeedbackVaryings (int program, CharArray[] varyings, int bufferMode);

    /**
     * @param program
     * @param index
     * @param bufSize
     * @param length
     * @param size
     * @param type
     * @param name ,length bufSize
     */
     void glGetTransformFeedbackVarying (int program, int index, java.nio.IntBuffer length, java.nio.IntBuffer size, java.nio.IntBuffer type, java.nio.ByteBuffer name);

    /**
     * @param index
     * @param size
     * @param type ,value from enumeration group VertexAttribEnum
     * @param stride
     * @param pointer
     */
     void glVertexAttribIPointer (int index, int size, int type, int stride, long pointer);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribEnum
     * @param params
     */
     void glGetVertexAttribIiv (int index, int pname, java.nio.IntBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribEnum
     * @param params
     */
     void glGetVertexAttribIuiv (int index, int pname, java.nio.IntBuffer params);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttribI4i (int index, int x, int y, int z, int w);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttribI4ui (int index, int x, int y, int z, int w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4iv (int index, java.nio.IntBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribI4uiv (int index, java.nio.IntBuffer v);

    /**
     * @param program
     * @param location
     * @param params
     */
     void glGetUniformuiv (int program, int location, java.nio.IntBuffer params);

    /**
     * @param program
     * @param name
     */
     int glGetFragDataLocation (int program, science.unlicense.api.character.CharArray name);

    /**
     * @param location
     * @param v0
     */
     void glUniform1ui (int location, int v0);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2ui (int location, int v0, int v1);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3ui (int location, int v0, int v1, int v2);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4ui (int location, int v0, int v1, int v2, int v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1uiv (int location, java.nio.IntBuffer value);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2uiv (int location, java.nio.IntBuffer value);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3uiv (int location, java.nio.IntBuffer value);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4uiv (int location, java.nio.IntBuffer value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param value
     */
     void glClearBufferiv (int buffer, int drawbuffer, java.nio.IntBuffer value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param value
     */
     void glClearBufferuiv (int buffer, int drawbuffer, java.nio.IntBuffer value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param value
     */
     void glClearBufferfv (int buffer, int drawbuffer, java.nio.FloatBuffer value);

    /**
     * @param buffer
     * @param drawbuffer ,value from enumeration group DrawBufferName
     * @param depth
     * @param stencil
     */
     void glClearBufferfi (int buffer, int drawbuffer, float depth, int stencil);

    /**
     * @param name
     * @param index
     */
     byte glGetStringi (int name, int index);

    /**
     * @param readTarget
     * @param writeTarget
     * @param readOffset ,value from enumeration group BufferOffset
     * @param writeOffset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glCopyBufferSubData (int readTarget, int writeTarget, long readOffset, long writeOffset, long size);

    /**
     * @param program
     * @param uniformCount
     * @param uniformNames
     * @param uniformIndices
     */
     void glGetUniformIndices (int program, int uniformCount, science.unlicense.api.character.CharArray uniformNames, java.nio.IntBuffer uniformIndices);

    /**
     * @param program
     * @param uniformCount
     * @param uniformIndices ,length uniformCount
     * @param pname
     * @param params
     */
     void glGetActiveUniformsiv (int program, java.nio.IntBuffer uniformIndices, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param uniformBlockName
     */
     int glGetUniformBlockIndex (int program, science.unlicense.api.character.CharArray uniformBlockName);

    /**
     * @param program
     * @param uniformBlockIndex
     * @param pname
     * @param params
     */
     void glGetActiveUniformBlockiv (int program, int uniformBlockIndex, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param uniformBlockIndex
     * @param bufSize
     * @param length
     * @param uniformBlockName ,length bufSize
     */
     void glGetActiveUniformBlockName (int program, int uniformBlockIndex, java.nio.IntBuffer length, java.nio.ByteBuffer uniformBlockName);

    /**
     * @param program
     * @param uniformBlockIndex
     * @param uniformBlockBinding
     */
     void glUniformBlockBinding (int program, int uniformBlockIndex, int uniformBlockBinding);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     * @param instancecount
     */
     void glDrawArraysInstanced (int mode, int first, int count, int instancecount);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param instancecount
     */
     void glDrawElementsInstanced (int mode, int count, int type, long indices, int instancecount);

    /**
     * @param condition
     * @param flags
     */
     long glFenceSync (int condition, int flags);

    /**
     * @param sync ,value from enumeration group sync
     */
     boolean glIsSync (long sync);

    /**
     * @param sync ,value from enumeration group sync
     */
     void glDeleteSync (long sync);

    /**
     * @param sync ,value from enumeration group sync
     * @param flags
     * @param timeout
     */
     int glClientWaitSync (long sync, int flags, long timeout);

    /**
     * @param sync ,value from enumeration group sync
     * @param flags
     * @param timeout
     */
     void glWaitSync (long sync, int flags, long timeout);

    /**
     * @param pname
     * @param data
     */
     void glGetInteger64v (int pname, java.nio.LongBuffer data);

    /**
     * Convenient method.
     *
     * @param pname
     * @param data
     */
     void glGetInteger64v (int pname, long[] data);

    /**
     * @param sync ,value from enumeration group sync
     * @param pname
     * @param bufSize
     * @param length
     * @param values ,length bufSize
     */
     void glGetSynciv (long sync, int pname, java.nio.IntBuffer length, java.nio.IntBuffer values);

    /**
     * @param target
     * @param index
     * @param data
     */
     void glGetInteger64i_v (int target, int index, java.nio.LongBuffer data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPNameARB
     * @param params
     */
     void glGetBufferParameteri64v (int target, int pname, java.nio.ByteBuffer params);

    /**
     * @param count
     * @param samplers ,length count
     */
     void glGenSamplers (java.nio.IntBuffer samplers);

    /**
     * @param count
     * @param samplers ,length count
     */
     void glDeleteSamplers (java.nio.IntBuffer samplers);

    /**
     * @param sampler
     */
     boolean glIsSampler (int sampler);

    /**
     * @param unit
     * @param sampler
     */
     void glBindSampler (int unit, int sampler);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameteri (int sampler, int pname, int param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameteriv (int sampler, int pname, java.nio.IntBuffer param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterf (int sampler, int pname, float param);

    /**
     * @param sampler
     * @param pname
     * @param param
     */
     void glSamplerParameterfv (int sampler, int pname, java.nio.FloatBuffer param);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameteriv (int sampler, int pname, java.nio.IntBuffer params);

    /**
     * @param sampler
     * @param pname
     * @param params
     */
     void glGetSamplerParameterfv (int sampler, int pname, java.nio.FloatBuffer params);

    /**
     * @param index
     * @param divisor
     */
     void glVertexAttribDivisor (int index, int divisor);

    /**
     * @param target
     * @param id
     */
     void glBindTransformFeedback (int target, int id);

    /**
     * @param n
     * @param ids ,length n
     */
     void glDeleteTransformFeedbacks (java.nio.IntBuffer ids);

    /**
     * @param n
     * @param ids ,length n
     */
     void glGenTransformFeedbacks (java.nio.IntBuffer ids);

    /**
     * Convenient method.
     *
     * @param n
     * @param ids ,length n
     */
     void glGenTransformFeedbacks (int[] ids);

    /**
     * @param id
     */
     boolean glIsTransformFeedback (int id);

     void glPauseTransformFeedback ();

     void glResumeTransformFeedback ();

    /**
     * @param program
     * @param bufSize
     * @param length
     * @param binaryFormat
     * @param binary ,length bufSize
     */
     void glGetProgramBinary (int program, java.nio.IntBuffer length, java.nio.IntBuffer binaryFormat, java.nio.ByteBuffer binary);

    /**
     * @param program
     * @param binaryFormat
     * @param binary ,length length
     * @param length
     */
     void glProgramBinary (int program, int binaryFormat, java.nio.ByteBuffer binary);

    /**
     * @param program
     * @param pname ,value from enumeration group ProgramParameterPName
     * @param value
     */
     void glProgramParameteri (int program, int pname, int value);

    /**
     * @param target
     * @param numAttachments
     * @param attachments ,length numAttachments
     */
     void glInvalidateFramebuffer (int target, java.nio.IntBuffer attachments);

    /**
     * @param target
     * @param numAttachments
     * @param attachments ,length numAttachments
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glInvalidateSubFramebuffer (int target, java.nio.IntBuffer attachments, int x, int y, int width, int height);

    /**
     * @param target
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     */
     void glTexStorage2D (int target, int levels, int internalformat, int width, int height);

    /**
     * @param target
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     */
     void glTexStorage3D (int target, int levels, int internalformat, int width, int height, int depth);

    /**
     * @param target
     * @param internalformat
     * @param pname
     * @param bufSize
     * @param params ,length bufSize
     */
     void glGetInternalformativ (int target, int internalformat, int pname, java.nio.IntBuffer params);

}