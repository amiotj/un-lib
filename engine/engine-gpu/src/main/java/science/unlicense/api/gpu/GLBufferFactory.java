
package science.unlicense.api.gpu;

import java.nio.ByteBuffer;
import science.unlicense.api.buffer.BufferFactory;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.number.Primitive;

/**
 * Buffer factory optimized for OpenGL.
 * 
 * @author Johann Sorel
 */
public class GLBufferFactory implements BufferFactory {

    public static final GLBufferFactory INSTANCE = new GLBufferFactory();
    
    private GLBuffer createByte(long nbValue, int bestType) {
        return new GLBuffer(ByteBuffer.allocateDirect((int) nbValue),bestType,NumberEncoding.LITTLE_ENDIAN);
    }
    
    public GLBuffer createByte(long nbValue) {
        return createByte(nbValue, Primitive.TYPE_UBYTE);
    }

    public GLBuffer createInt(long nbValue) {
        return createByte(nbValue*4, Primitive.TYPE_INT);
    }

    public GLBuffer createLong(long nbValue) {
        return createByte(nbValue*8, Primitive.TYPE_LONG);
    }

    public GLBuffer createFloat(long nbValue) {
        return createByte(nbValue*4, Primitive.TYPE_FLOAT);
    }

    public GLBuffer createDouble(long nbValue) {
        return createByte(nbValue*8, Primitive.TYPE_DOUBLE);
    }
    
}
