package science.unlicense.api.gpu.opengl;

public interface GL31 extends science.unlicense.api.gpu.opengl.GL30 {

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     * @param instancecount
     */
     void glDrawArraysInstanced (int mode, int first, int count, int instancecount);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param instancecount
     */
     void glDrawElementsInstanced (int mode, int count, int type, long indices, int instancecount);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param internalformat
     * @param buffer
     */
     void glTexBuffer (int target, int internalformat, int buffer);

    /**
     * @param index
     */
     void glPrimitiveRestartIndex (int index);

    /**
     * @param readTarget
     * @param writeTarget
     * @param readOffset ,value from enumeration group BufferOffset
     * @param writeOffset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glCopyBufferSubData (int readTarget, int writeTarget, long readOffset, long writeOffset, long size);

    /**
     * @param program
     * @param uniformCount
     * @param uniformNames
     * @param uniformIndices
     */
     void glGetUniformIndices (int program, int uniformCount, science.unlicense.api.character.CharArray uniformNames, java.nio.IntBuffer uniformIndices);

    /**
     * @param program
     * @param uniformCount
     * @param uniformIndices ,length uniformCount
     * @param pname
     * @param params
     */
     void glGetActiveUniformsiv (int program, java.nio.IntBuffer uniformIndices, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param uniformIndex
     * @param bufSize
     * @param length
     * @param uniformName ,length bufSize
     */
     void glGetActiveUniformName (int program, int uniformIndex, java.nio.IntBuffer length, java.nio.ByteBuffer uniformName);

    /**
     * @param program
     * @param uniformBlockName
     */
     int glGetUniformBlockIndex (int program, science.unlicense.api.character.CharArray uniformBlockName);

    /**
     * @param program
     * @param uniformBlockIndex
     * @param pname
     * @param params
     */
     void glGetActiveUniformBlockiv (int program, int uniformBlockIndex, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param uniformBlockIndex
     * @param bufSize
     * @param length
     * @param uniformBlockName ,length bufSize
     */
     void glGetActiveUniformBlockName (int program, int uniformBlockIndex, java.nio.IntBuffer length, java.nio.ByteBuffer uniformBlockName);

    /**
     * @param program
     * @param uniformBlockIndex
     * @param uniformBlockBinding
     */
     void glUniformBlockBinding (int program, int uniformBlockIndex, int uniformBlockBinding);

    /**
     * @param target
     * @param index
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glBindBufferRange (int target, int index, int buffer, long offset, long size);

    /**
     * @param target
     * @param index
     * @param buffer
     */
     void glBindBufferBase (int target, int index, int buffer);

    /**
     * @param target
     * @param index
     * @param data
     */
     void glGetIntegeri_v (int target, int index, java.nio.IntBuffer data);

}