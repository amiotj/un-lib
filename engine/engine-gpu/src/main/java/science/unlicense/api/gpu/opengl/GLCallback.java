
package science.unlicense.api.gpu.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GLCallback {
    
    
    void execute(GLSource source);
    
    void dispose(GLSource source);
    
}
