package science.unlicense.api.gpu.opengl;

public interface GL41 extends science.unlicense.api.gpu.opengl.GL40 {

     void glReleaseShaderCompiler ();

    /**
     * @param count
     * @param shaders ,length count
     * @param binaryformat
     * @param binary ,length length
     * @param length
     */
     void glShaderBinary (java.nio.IntBuffer shaders, int binaryformat, java.nio.ByteBuffer binary);

    /**
     * @param shadertype
     * @param precisiontype
     * @param range
     * @param precision
     */
     void glGetShaderPrecisionFormat (int shadertype, int precisiontype, java.nio.IntBuffer range, java.nio.IntBuffer precision);

    /**
     * @param n
     * @param f
     */
     void glDepthRangef (float n, float f);

    /**
     * @param d
     */
     void glClearDepthf (float d);

    /**
     * @param program
     * @param bufSize
     * @param length
     * @param binaryFormat
     * @param binary ,length bufSize
     */
     void glGetProgramBinary (int program, java.nio.IntBuffer length, java.nio.IntBuffer binaryFormat, java.nio.ByteBuffer binary);

    /**
     * @param program
     * @param binaryFormat
     * @param binary ,length length
     * @param length
     */
     void glProgramBinary (int program, int binaryFormat, java.nio.ByteBuffer binary);

    /**
     * @param program
     * @param pname ,value from enumeration group ProgramParameterPName
     * @param value
     */
     void glProgramParameteri (int program, int pname, int value);

    /**
     * @param pipeline
     * @param stages
     * @param program
     */
     void glUseProgramStages (int pipeline, int stages, int program);

    /**
     * @param pipeline
     * @param program
     */
     void glActiveShaderProgram (int pipeline, int program);

    /**
     * @param type
     * @param count
     * @param strings ,length count
     */
     int glCreateShaderProgramv (int type, java.nio.ByteBuffer strings);

    /**
     * @param pipeline
     */
     void glBindProgramPipeline (int pipeline);

    /**
     * @param n
     * @param pipelines ,length n
     */
     void glDeleteProgramPipelines (java.nio.IntBuffer pipelines);

    /**
     * @param n
     * @param pipelines ,length n
     */
     void glGenProgramPipelines (java.nio.IntBuffer pipelines);

    /**
     * @param pipeline
     */
     boolean glIsProgramPipeline (int pipeline);

    /**
     * @param pipeline
     * @param pname
     * @param params
     */
     void glGetProgramPipelineiv (int pipeline, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1i (int program, int location, int v0);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1iv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1f (int program, int location, float v0);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1fv (int program, int location, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1d (int program, int location, double v0);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1dv (int program, int location, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     */
     void glProgramUniform1ui (int program, int location, int v0);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count
     */
     void glProgramUniform1uiv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2i (int program, int location, int v0, int v1);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2iv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2f (int program, int location, float v0, float v1);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2fv (int program, int location, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2d (int program, int location, double v0, double v1);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2dv (int program, int location, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     */
     void glProgramUniform2ui (int program, int location, int v0, int v1);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glProgramUniform2uiv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3i (int program, int location, int v0, int v1, int v2);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3iv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3f (int program, int location, float v0, float v1, float v2);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3fv (int program, int location, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3d (int program, int location, double v0, double v1, double v2);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3dv (int program, int location, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glProgramUniform3ui (int program, int location, int v0, int v1, int v2);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glProgramUniform3uiv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4i (int program, int location, int v0, int v1, int v2, int v3);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4iv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4f (int program, int location, float v0, float v1, float v2, float v3);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4fv (int program, int location, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4d (int program, int location, double v0, double v1, double v2, double v3);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4dv (int program, int location, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glProgramUniform4ui (int program, int location, int v0, int v1, int v2, int v3);

    /**
     * @param program
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glProgramUniform4uiv (int program, int location, java.nio.IntBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glProgramUniformMatrix2fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glProgramUniformMatrix3fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glProgramUniformMatrix4fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glProgramUniformMatrix2dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glProgramUniformMatrix3dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glProgramUniformMatrix4dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glProgramUniformMatrix2x3fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glProgramUniformMatrix3x2fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glProgramUniformMatrix2x4fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glProgramUniformMatrix4x2fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glProgramUniformMatrix3x4fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glProgramUniformMatrix4x3fv (int program, int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glProgramUniformMatrix2x3dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glProgramUniformMatrix3x2dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glProgramUniformMatrix2x4dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glProgramUniformMatrix4x2dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glProgramUniformMatrix3x4dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param program
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glProgramUniformMatrix4x3dv (int program, int location, boolean transpose, java.nio.DoubleBuffer value);

    /**
     * @param pipeline
     */
     void glValidateProgramPipeline (int pipeline);

    /**
     * @param pipeline
     * @param bufSize
     * @param length
     * @param infoLog ,length bufSize
     */
     void glGetProgramPipelineInfoLog (int pipeline, java.nio.IntBuffer length, java.nio.ByteBuffer infoLog);

    /**
     * @param index
     * @param x
     */
     void glVertexAttribL1d (int index, double x);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttribL2d (int index, double x, double y);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttribL3d (int index, double x, double y, double z);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttribL4d (int index, double x, double y, double z, double w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribL1dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribL2dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribL3dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param v
     */
     void glVertexAttribL4dv (int index, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param size
     * @param type
     * @param stride
     * @param pointer ,length size
     */
     void glVertexAttribLPointer (int index, int type, int stride, long pointer);

    /**
     * @param index
     * @param pname
     * @param params
     */
     void glGetVertexAttribLdv (int index, int pname, java.nio.DoubleBuffer params);

    /**
     * @param first
     * @param count
     * @param v
     */
     void glViewportArrayv (int first, int count, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param w
     * @param h
     */
     void glViewportIndexedf (int index, float x, float y, float w, float h);

    /**
     * @param index
     * @param v
     */
     void glViewportIndexedfv (int index, java.nio.FloatBuffer v);

    /**
     * @param first
     * @param count
     * @param v
     */
     void glScissorArrayv (int first, int count, java.nio.IntBuffer v);

    /**
     * @param index
     * @param left
     * @param bottom
     * @param width
     * @param height
     */
     void glScissorIndexed (int index, int left, int bottom, int width, int height);

    /**
     * @param index
     * @param v
     */
     void glScissorIndexedv (int index, java.nio.IntBuffer v);

    /**
     * @param first
     * @param count
     * @param v
     */
     void glDepthRangeArrayv (int first, int count, java.nio.DoubleBuffer v);

    /**
     * @param index
     * @param n
     * @param f
     */
     void glDepthRangeIndexed (int index, double n, double f);

    /**
     * @param target ,value from enumeration group TypeEnum
     * @param index
     * @param data
     */
     void glGetFloati_v (int target, int index, java.nio.FloatBuffer data);

    /**
     * @param target
     * @param index
     * @param data
     */
     void glGetDoublei_v (int target, int index, java.nio.DoubleBuffer data);

}