
package science.unlicense.api.gpu.opengl;

import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.predicate.Predicates;
import science.unlicense.system.util.ModuleSeeker;

/**
 *
 * @author Johann Sorel
 */
public class GLBindings {


    /**
     * Lists available GL bindings
     * @return array of GLBinding, never null but can be empty.
     */
    public static GLBinding[] getBindings(){
        final Sequence results = ModuleSeeker.searchValues(
                new Chars("services/science.unlicense.api.gpu.opengl.GLBinding"),
                Predicates.instanceOf(GLBinding.class));
        final GLBinding[] formats = new GLBinding[results.getSize()];
        Collections.copy(results, formats, 0);
        return formats;
    }

}
