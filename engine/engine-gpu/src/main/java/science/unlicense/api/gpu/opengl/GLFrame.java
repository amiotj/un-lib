
package science.unlicense.api.gpu.opengl;

import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.desktop.Frame;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.event.EventSource;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Tuple;

/**
 *
 * @author Johann Sorel
 */
public interface GLFrame extends EventSource{

    public static final Chars PROP_TITLE = Frame.PROP_TITLE;
    public static final Chars PROP_ON_SCREEN_LOCATION = Frame.PROP_ON_SCREEN_LOCATION;
    public static final Chars PROP_SIZE = Frame.PROP_SIZE;
    public static final Chars PROP_VISIBLE = Frame.PROP_VISIBLE;
    public static final Chars PROP_MAXIMIZABLE = Frame.PROP_MAXIMIZABLE;
    public static final Chars PROP_MINIMIZABLE = Frame.PROP_MINIMIZABLE;
    public static final Chars PROP_CLOSABLE = Frame.PROP_CLOSABLE;
    public static final Chars PROP_ALWAYSONTOP = Frame.PROP_ALWAYSONTOP;
    public static final Chars PROP_CURSOR = Frame.PROP_CURSOR;
    public static final Chars PROP_STATE = Frame.PROP_STATE;
    public static final Chars PROP_DISPOSED = Frame.PROP_DISPOSED;

    public static final int STATE_NORMAL = 0;
    public static final int STATE_MINIMIZED = 1;
    public static final int STATE_MAXIMIZED = 2;
    public static final int STATE_MAXIMIZED_VERTICAL = 3;
    public static final int STATE_MAXIMIZED_HORIZONTAL = 4;
    public static final int STATE_FULLSCREEN = 5;

    /**
     * Get frame title.
     *
     * @return frame title, can be null
     */
    CharArray getTitle();

    /**
     * Set frame title.
     *
     * @param title , can be null
     */
    void setTitle(CharArray title);
    
    /**
     *
     * @return true is pointer is visible
     */
    boolean isPointerVisible();

    /**
     *
     * @param visible pointer visible state
     */
    void setPointerVisible(boolean visible);

    /**
     * Get frame base cursor.
     *
     * @return Cursor, can be null
     */
    Cursor getCursor();

    /**
     *
     * @param cursor, null to hide cursor.
     */
    void setCursor(Cursor cursor);

    /**
     * Set frame location on screen.
     *
     * @param location
     */
    void setOnScreenLocation(Tuple location);

    /**
     * Get frame location on screen.
     *
     * @return frame location on screen.
     */
    Tuple getOnScreenLocation();

    /**
     * Set frame size.
     *
     * @param width
     * @param height
     */
    void setSize(int width, int height);

    /**
     * Get frame size.
     *
     * @return size
     */
    Extent getSize();

    /**
     * Set frame visible.
     *
     * @param visible
     */
    void setVisible(boolean visible);

    /**
     * Get frame visibility.
     *
     * @return true if frame is visible.
     */
    boolean isVisible();

    /**
     * Set decoration state.
     */
    void setDecorated(boolean decorated);

    /**
     *
     * @return true if frame is decorated.
     */
    boolean isDecorated();

    /**
     * A frame can be in only one state at the time.
     * States include :
     * - normal
     * - minimized
     * - maximized (horizontal,vertical,both)
     * - fullscreen
     *
     * @return current frame state
     */
    int getState();

    /**
     * Set frame state.
     *
     * @param state
     */
    void setState(int state);

    /**
     * Set frame maximizable state.
     *
     * @param maximizable
     */
    void setMaximizable(boolean maximizable);

    /**
     * Get frame maximizable state.
     *
     * @return true if frame is maximizable.
     */
    boolean isMaximizable();

    /**
     * Set frame minimizable state.
     *
     * @param minimizable
     */
    void setMinimizable(boolean minimizable);

    /**
     * Get frame minimizable state.
     *
     * @return true if frame is minimizable.
     */
    boolean isMinimizable();

    /**
     * Set frame closable state.
     *
     * @param closable
     */
    void setClosable(boolean closable);

    /**
     * Get frame closable state.
     *
     * @return true if frame is closable.
     */
    boolean isClosable();

    /**
     * Ask to always put the frame on top of any other.
     *
     * @param ontop
     */
    void setAlwaysonTop(boolean ontop);

    /**
     * Get frame on-top state.
     *
     * @return true if frame is always on top.
     */
    boolean isAlwaysOnTop();

    /**
     * Get frame opaque state.
     *
     * @return true if frame is opaque
     */
    boolean isOpaque();

    /**
     * Release all resources used by the frame.
     * It should not be used after this call.
     */
    void dispose();

    /**
     * Indicate if frame has been disposed.
     */
    boolean isDisposed();

    /**
     * Block calling thread until this frame has been disposed.
     *
     * TODO : seems strange, find a better way to implement this need
     */
    void waitForDisposal();

    GLSource getSource();

}
