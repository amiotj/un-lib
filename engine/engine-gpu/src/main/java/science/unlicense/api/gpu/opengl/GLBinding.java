
package science.unlicense.api.gpu.opengl;

import science.unlicense.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public interface GLBinding {
    
    GLSource createOffScreen(int width, int height);

    GLFrame createFrame(boolean opaque);

    Margin getFrameMargin();

}
