
package science.unlicense.api.gpu.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GL1ES extends GL, GLES110 {

}
