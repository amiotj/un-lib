package science.unlicense.api.gpu.opengl;

public interface GL21 extends science.unlicense.api.gpu.opengl.GL20 {

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix2x3fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*6
     */
     void glUniformMatrix3x2fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix2x4fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*8
     */
     void glUniformMatrix4x2fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix3x4fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*12
     */
     void glUniformMatrix4x3fv (int location, boolean transpose, java.nio.FloatBuffer value);

}