
package science.unlicense.api.gpu.opengl;

/**
 *
 * @author Johann Sorel
 */
public interface GL2ES3 extends GL,GLES232 {
    
}
