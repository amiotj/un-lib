package science.unlicense.api.gpu.opengl;

public interface GLES110 {

    /**
     * @param func ,value from enumeration group AlphaFunction
     * @param ref
     */
     void glAlphaFunc (int func, float ref);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glClearColor (float red, float green, float blue, float alpha);

    /**
     * @param d
     */
     void glClearDepthf (float d);

    /**
     * @param p
     * @param eqn
     */
     void glClipPlanef (int p, java.nio.FloatBuffer eqn);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glColor4f (float red, float green, float blue, float alpha);

    /**
     * @param n
     * @param f
     */
     void glDepthRangef (float n, float f);

    /**
     * @param pname ,value from enumeration group FogParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glFogf (int pname, float param);

    /**
     * @param pname ,value from enumeration group FogParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glFogfv (int pname, java.nio.FloatBuffer params);

    /**
     * @param l
     * @param r
     * @param b
     * @param t
     * @param n
     * @param f
     */
     void glFrustumf (float l, float r, float b, float t, float n, float f);

    /**
     * @param plane
     * @param equation
     */
     void glGetClipPlanef (int plane, java.nio.FloatBuffer equation);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, java.nio.FloatBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, float[] data);
     
    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param params
     */
     void glGetLightfv (int light, int pname, java.nio.FloatBuffer params);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param params
     */
     void glGetMaterialfv (int face, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params
     */
     void glGetTexEnvfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param pname ,value from enumeration group LightModelParameter
     * @param param
     */
     void glLightModelf (int pname, float param);

    /**
     * @param pname ,value from enumeration group LightModelParameter
     * @param params
     */
     void glLightModelfv (int pname, java.nio.FloatBuffer params);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glLightf (int light, int pname, float param);

    /**
     * @param light ,value from enumeration group LightName
     * @param pname ,value from enumeration group LightParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glLightfv (int light, int pname, java.nio.FloatBuffer params);

    /**
     * @param width ,value from enumeration group CheckedFloat32
     */
     void glLineWidth (float width);

    /**
     * @param m
     */
     void glLoadMatrixf (java.nio.FloatBuffer m);

    /**
     * Convenient method.
     *
     * @param m
     */
     void glLoadMatrixf (float[] m);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glMaterialf (int face, int pname, float param);

    /**
     * @param face ,value from enumeration group MaterialFace
     * @param pname ,value from enumeration group MaterialParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glMaterialfv (int face, int pname, java.nio.FloatBuffer params);

    /**
     * @param m
     */
     void glMultMatrixf (java.nio.FloatBuffer m);

    /**
     * @param target ,value from enumeration group TextureUnit
     * @param s ,value from enumeration group CoordF
     * @param t ,value from enumeration group CoordF
     * @param r ,value from enumeration group CoordF
     * @param q ,value from enumeration group CoordF
     */
     void glMultiTexCoord4f (int target, float s, float t, float r, float q);

    /**
     * @param nx ,value from enumeration group CoordF
     * @param ny ,value from enumeration group CoordF
     * @param nz ,value from enumeration group CoordF
     */
     void glNormal3f (float nx, float ny, float nz);

    /**
     * @param l
     * @param r
     * @param b
     * @param t
     * @param n
     * @param f
     */
     void glOrthof (float l, float r, float b, float t, float n, float f);

    /**
     * @param pname ,value from enumeration group PointParameterNameARB
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glPointParameterf (int pname, float param);

    /**
     * @param pname ,value from enumeration group PointParameterNameARB
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glPointParameterfv (int pname, java.nio.FloatBuffer params);

    /**
     * @param size ,value from enumeration group CheckedFloat32
     */
     void glPointSize (float size);

    /**
     * @param factor
     * @param units
     */
     void glPolygonOffset (float factor, float units);

    /**
     * @param angle
     * @param x
     * @param y
     * @param z
     */
     void glRotatef (float angle, float x, float y, float z);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glScalef (float x, float y, float z);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexEnvf (int target, int pname, float param);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexEnvfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexParameterf (int target, int pname, float param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexParameterfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glTranslatef (float x, float y, float z);

    /**
     * @param texture ,value from enumeration group TextureUnit
     */
     void glActiveTexture (int texture);

    /**
     * @param func
     * @param ref
     */
     void glAlphaFuncx (int func, int ref);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param buffer
     */
     void glBindBuffer (int target, int buffer);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param texture ,value from enumeration group Texture
     */
     void glBindTexture (int target, int texture);

    /**
     * @param sfactor ,value from enumeration group BlendingFactorSrc
     * @param dfactor ,value from enumeration group BlendingFactorDest
     */
     void glBlendFunc (int sfactor, int dfactor);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     * @param usage ,value from enumeration group BufferUsageARB
     */
     void glBufferData (int target, java.nio.Buffer data, int usage);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     */
     void glBufferSubData (int target, long offset, java.nio.Buffer data);

    /**
     * @param mask ,value from enumeration group ClearBufferMask
     */
     void glClear (int mask);

    /**
     * @param red
     * @param green
     * @param blue
     * @param alpha
     */
     void glClearColorx (int red, int green, int blue, int alpha);

    /**
     * @param depth
     */
     void glClearDepthx (int depth);

    /**
     * @param s ,value from enumeration group StencilValue
     */
     void glClearStencil (int s);

    /**
     * @param texture ,value from enumeration group TextureUnit
     */
     void glClientActiveTexture (int texture);

    /**
     * @param plane
     * @param equation
     */
     void glClipPlanex (int plane, java.nio.IntBuffer equation);

    /**
     * @param red ,value from enumeration group ColorUB
     * @param green ,value from enumeration group ColorUB
     * @param blue ,value from enumeration group ColorUB
     * @param alpha ,value from enumeration group ColorUB
     */
     void glColor4ub (byte red, byte green, byte blue, byte alpha);

    /**
     * @param red
     * @param green
     * @param blue
     * @param alpha
     */
     void glColor4x (int red, int green, int blue, int alpha);

    /**
     * @param red ,value from enumeration group Boolean
     * @param green ,value from enumeration group Boolean
     * @param blue ,value from enumeration group Boolean
     * @param alpha ,value from enumeration group Boolean
     */
     void glColorMask (boolean red, boolean green, boolean blue, boolean alpha);

    /**
     * @param size
     * @param type ,value from enumeration group ColorPointerType
     * @param stride
     * @param pointer
     */
     void glColorPointer (int size, int type, int stride, java.nio.ByteBuffer pointer);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexImage2D (int target, int level, int internalformat, int width, int height, int border, java.nio.Buffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, java.nio.Buffer data);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group PixelInternalFormat
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     */
     void glCopyTexImage2D (int target, int level, int internalformat, int x, int y, int width, int height, int border);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glCopyTexSubImage2D (int target, int level, int xoffset, int yoffset, int x, int y, int width, int height);

    /**
     * @param mode ,value from enumeration group CullFaceMode
     */
     void glCullFace (int mode);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glDeleteBuffers (java.nio.IntBuffer buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glDeleteBuffers (int[] buffers);
     
    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glDeleteTextures (java.nio.IntBuffer textures);

    /**
     * @param func ,value from enumeration group DepthFunction
     */
     void glDepthFunc (int func);

    /**
     * @param flag ,value from enumeration group Boolean
     */
     void glDepthMask (boolean flag);

    /**
     * @param n
     * @param f
     */
     void glDepthRangex (int n, int f);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glDisable (int cap);

    /**
     * @param array ,value from enumeration group EnableCap
     */
     void glDisableClientState (int array);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     */
     void glDrawArrays (int mode, int first, int count);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     */
     void glDrawElements (int mode, int count, int type, long indices);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glEnable (int cap);

    /**
     * @param array ,value from enumeration group EnableCap
     */
     void glEnableClientState (int array);

     void glFinish ();

     void glFlush ();

    /**
     * @param pname
     * @param param
     */
     void glFogx (int pname, int param);

    /**
     * @param pname
     * @param param
     */
     void glFogxv (int pname, java.nio.IntBuffer param);

    /**
     * @param mode ,value from enumeration group FrontFaceDirection
     */
     void glFrontFace (int mode);

    /**
     * @param l
     * @param r
     * @param b
     * @param t
     * @param n
     * @param f
     */
     void glFrustumx (int l, int r, int b, int t, int n, int f);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data ,value from enumeration group Boolean
     */
     void glGetBooleanv (int pname, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPNameARB
     * @param params
     */
     void glGetBufferParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param plane
     * @param equation
     */
     void glGetClipPlanex (int plane, java.nio.IntBuffer equation);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (java.nio.IntBuffer buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (int[] buffers);
     
    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glGenTextures (java.nio.IntBuffer textures);

     /**
      * Convenient method
      *
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
      */
     void glGenTextures(int[] textures);
     
     int glGetError ();

    /**
     * @param pname
     * @param params
     */
     void glGetFixedv (int pname, java.nio.IntBuffer params);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, java.nio.IntBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, int[] data);
     
    /**
     * @param light
     * @param pname
     * @param params
     */
     void glGetLightxv (int light, int pname, java.nio.IntBuffer params);

    /**
     * @param face
     * @param pname
     * @param params
     */
     void glGetMaterialxv (int face, int pname, java.nio.IntBuffer params);

    /**
     * @param pname ,value from enumeration group GetPointervPName
     * @param params
     */
     void glGetPointerv (int pname, java.nio.ByteBuffer params);

    /**
     * @param name ,value from enumeration group StringName
     */
     byte glGetString (int name);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params
     */
     void glGetTexEnviv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glGetTexEnvxv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glGetTexParameterxv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group HintTarget
     * @param mode ,value from enumeration group HintMode
     */
     void glHint (int target, int mode);

    /**
     * @param buffer
     */
     boolean glIsBuffer (int buffer);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     boolean glIsEnabled (int cap);

    /**
     * @param texture ,value from enumeration group Texture
     */
     boolean glIsTexture (int texture);

    /**
     * @param pname
     * @param param
     */
     void glLightModelx (int pname, int param);

    /**
     * @param pname
     * @param param
     */
     void glLightModelxv (int pname, java.nio.IntBuffer param);

    /**
     * @param light
     * @param pname
     * @param param
     */
     void glLightx (int light, int pname, int param);

    /**
     * @param light
     * @param pname
     * @param params
     */
     void glLightxv (int light, int pname, java.nio.IntBuffer params);

    /**
     * @param width
     */
     void glLineWidthx (int width);

     void glLoadIdentity ();

    /**
     * @param m
     */
     void glLoadMatrixx (java.nio.IntBuffer m);

    /**
     * @param opcode ,value from enumeration group LogicOp
     */
     void glLogicOp (int opcode);

    /**
     * @param face
     * @param pname
     * @param param
     */
     void glMaterialx (int face, int pname, int param);

    /**
     * @param face
     * @param pname
     * @param param
     */
     void glMaterialxv (int face, int pname, java.nio.IntBuffer param);

    /**
     * @param mode ,value from enumeration group MatrixMode
     */
     void glMatrixMode (int mode);

    /**
     * @param m
     */
     void glMultMatrixx (java.nio.IntBuffer m);

    /**
     * @param texture
     * @param s
     * @param t
     * @param r
     * @param q
     */
     void glMultiTexCoord4x (int texture, int s, int t, int r, int q);

    /**
     * @param nx
     * @param ny
     * @param nz
     */
     void glNormal3x (int nx, int ny, int nz);

    /**
     * @param type ,value from enumeration group NormalPointerType
     * @param stride
     * @param pointer
     */
     void glNormalPointer (int type, int stride, java.nio.ByteBuffer pointer);

    /**
     * @param l
     * @param r
     * @param b
     * @param t
     * @param n
     * @param f
     */
     void glOrthox (int l, int r, int b, int t, int n, int f);

    /**
     * @param pname ,value from enumeration group PixelStoreParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glPixelStorei (int pname, int param);

    /**
     * @param pname
     * @param param
     */
     void glPointParameterx (int pname, int param);

    /**
     * @param pname
     * @param params
     */
     void glPointParameterxv (int pname, java.nio.IntBuffer params);

    /**
     * @param size
     */
     void glPointSizex (int size);

    /**
     * @param factor
     * @param units
     */
     void glPolygonOffsetx (int factor, int units);

     void glPopMatrix ();

     void glPushMatrix ();

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glReadPixels (int x, int y, int width, int height, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param angle
     * @param x
     * @param y
     * @param z
     */
     void glRotatex (int angle, int x, int y, int z);

    /**
     * @param value
     * @param invert ,value from enumeration group Boolean
     */
     void glSampleCoverage (float value, boolean invert);

    /**
     * @param value
     * @param invert
     */
     void glSampleCoveragex (int value, boolean invert);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glScalex (int x, int y, int z);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glScissor (int x, int y, int width, int height);

    /**
     * @param mode ,value from enumeration group ShadingModel
     */
     void glShadeModel (int mode);

    /**
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFunc (int func, int ref, int mask);

    /**
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMask (int mask);

    /**
     * @param fail ,value from enumeration group StencilOp
     * @param zfail ,value from enumeration group StencilOp
     * @param zpass ,value from enumeration group StencilOp
     */
     void glStencilOp (int fail, int zfail, int zpass);

    /**
     * @param size
     * @param type ,value from enumeration group TexCoordPointerType
     * @param stride
     * @param pointer
     */
     void glTexCoordPointer (int size, int type, int stride, java.nio.ByteBuffer pointer);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexEnvi (int target, int pname, int param);

    /**
     * @param target
     * @param pname
     * @param param
     */
     void glTexEnvx (int target, int pname, int param);

    /**
     * @param target ,value from enumeration group TextureEnvTarget
     * @param pname ,value from enumeration group TextureEnvParameter
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexEnviv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glTexEnvxv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param internalformat ,value from enumeration group TextureComponentCount
     * @param width
     * @param height
     * @param border ,value from enumeration group CheckedInt32
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexImage2D (int target, int level, int internalformat, int width, int height, int border, int format, int type, java.nio.Buffer pixels);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexParameteri (int target, int pname, int param);

    /**
     * @param target
     * @param pname
     * @param param
     */
     void glTexParameterx (int target, int pname, int param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glTexParameterxv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param x
     * @param y
     * @param z
     */
     void glTranslatex (int x, int y, int z);

    /**
     * @param size
     * @param type ,value from enumeration group VertexPointerType
     * @param stride
     * @param pointer
     */
     void glVertexPointer (int size, int type, int stride, java.nio.ByteBuffer pointer);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glViewport (int x, int y, int width, int height);

}