
package science.unlicense.api.gpu.opengl;

import science.unlicense.api.collection.Sequence;

/**
 *
 * @author Johann Sorel
 */
public interface GLSource {

    GLBinding getBinding();

    GL getGL();
    
    int getX();
    
    int getY();
    
    int getWidth();
    
    int getHeight();

    Sequence getCallbacks();

    void render();

    void dispose();
    
}
