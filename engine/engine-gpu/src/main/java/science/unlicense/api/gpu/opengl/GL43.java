package science.unlicense.api.gpu.opengl;

public interface GL43 extends science.unlicense.api.gpu.opengl.GL42 {

    /**
     * @param target
     * @param internalformat
     * @param format
     * @param type
     * @param data
     */
     void glClearBufferData (int target, int internalformat, int format, int type, java.nio.ByteBuffer data);

    /**
     * @param target
     * @param internalformat
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     * @param format
     * @param type
     * @param data
     */
     void glClearBufferSubData (int target, int internalformat, long offset, long size, int format, int type, java.nio.ByteBuffer data);

    /**
     * @param num_groups_x
     * @param num_groups_y
     * @param num_groups_z
     */
     void glDispatchCompute (int num_groups_x, int num_groups_y, int num_groups_z);

    /**
     * @param indirect ,value from enumeration group BufferOffset
     */
     void glDispatchComputeIndirect (long indirect);

    /**
     * @param srcName
     * @param srcTarget
     * @param srcLevel
     * @param srcX
     * @param srcY
     * @param srcZ
     * @param dstName
     * @param dstTarget
     * @param dstLevel
     * @param dstX
     * @param dstY
     * @param dstZ
     * @param srcWidth
     * @param srcHeight
     * @param srcDepth
     */
     void glCopyImageSubData (int srcName, int srcTarget, int srcLevel, int srcX, int srcY, int srcZ, int dstName, int dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth);

    /**
     * @param target
     * @param pname
     * @param param
     */
     void glFramebufferParameteri (int target, int pname, int param);

    /**
     * @param target
     * @param pname
     * @param params
     */
     void glGetFramebufferParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target
     * @param internalformat
     * @param pname
     * @param bufSize
     * @param params ,length bufSize
     */
     void glGetInternalformati64v (int target, int internalformat, int pname, java.nio.ByteBuffer params);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     */
     void glInvalidateTexSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth);

    /**
     * @param texture
     * @param level
     */
     void glInvalidateTexImage (int texture, int level);

    /**
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param length ,value from enumeration group BufferSize
     */
     void glInvalidateBufferSubData (int buffer, long offset, long length);

    /**
     * @param buffer
     */
     void glInvalidateBufferData (int buffer);

    /**
     * @param target
     * @param numAttachments
     * @param attachments ,length numAttachments
     */
     void glInvalidateFramebuffer (int target, java.nio.IntBuffer attachments);

    /**
     * @param target
     * @param numAttachments
     * @param attachments ,length numAttachments
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glInvalidateSubFramebuffer (int target, java.nio.IntBuffer attachments, int x, int y, int width, int height);

    /**
     * @param mode
     * @param indirect
     * @param drawcount
     * @param stride
     */
     void glMultiDrawArraysIndirect (int mode, java.nio.ByteBuffer indirect, int drawcount, int stride);

    /**
     * @param mode
     * @param type
     * @param indirect
     * @param drawcount
     * @param stride
     */
     void glMultiDrawElementsIndirect (int mode, int type, java.nio.ByteBuffer indirect, int drawcount, int stride);

    /**
     * @param program
     * @param programInterface
     * @param pname
     * @param params
     */
     void glGetProgramInterfaceiv (int program, int programInterface, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param programInterface
     * @param name
     */
     int glGetProgramResourceIndex (int program, int programInterface, science.unlicense.api.character.CharArray name);

    /**
     * @param program
     * @param programInterface
     * @param index
     * @param bufSize
     * @param length
     * @param name ,length bufSize
     */
     void glGetProgramResourceName (int program, int programInterface, int index, java.nio.IntBuffer length, java.nio.ByteBuffer name);

    /**
     * @param program
     * @param programInterface
     * @param index
     * @param propCount
     * @param props ,length propCount
     * @param bufSize
     * @param length
     * @param params ,length bufSize
     */
     void glGetProgramResourceiv (int program, int programInterface, int index, java.nio.IntBuffer props, java.nio.IntBuffer length, java.nio.IntBuffer params);

    /**
     * @param program
     * @param programInterface
     * @param name
     */
     int glGetProgramResourceLocation (int program, int programInterface, science.unlicense.api.character.CharArray name);

    /**
     * @param program
     * @param programInterface
     * @param name
     */
     int glGetProgramResourceLocationIndex (int program, int programInterface, science.unlicense.api.character.CharArray name);

    /**
     * @param program
     * @param storageBlockIndex
     * @param storageBlockBinding
     */
     void glShaderStorageBlockBinding (int program, int storageBlockIndex, int storageBlockBinding);

    /**
     * @param target
     * @param internalformat
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glTexBufferRange (int target, int internalformat, int buffer, long offset, long size);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param fixedsamplelocations ,value from enumeration group Boolean
     */
     void glTexStorage2DMultisample (int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations);

    /**
     * @param target
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     * @param fixedsamplelocations ,value from enumeration group Boolean
     */
     void glTexStorage3DMultisample (int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations);

    /**
     * @param texture
     * @param target
     * @param origtexture
     * @param internalformat
     * @param minlevel
     * @param numlevels
     * @param minlayer
     * @param numlayers
     */
     void glTextureView (int texture, int target, int origtexture, int internalformat, int minlevel, int numlevels, int minlayer, int numlayers);

    /**
     * @param bindingindex
     * @param buffer
     * @param offset ,value from enumeration group BufferOffset
     * @param stride
     */
     void glBindVertexBuffer (int bindingindex, int buffer, long offset, int stride);

    /**
     * @param attribindex
     * @param size
     * @param type
     * @param normalized ,value from enumeration group Boolean
     * @param relativeoffset
     */
     void glVertexAttribFormat (int attribindex, int size, int type, boolean normalized, int relativeoffset);

    /**
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexAttribIFormat (int attribindex, int size, int type, int relativeoffset);

    /**
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexAttribLFormat (int attribindex, int size, int type, int relativeoffset);

    /**
     * @param attribindex
     * @param bindingindex
     */
     void glVertexAttribBinding (int attribindex, int bindingindex);

    /**
     * @param bindingindex
     * @param divisor
     */
     void glVertexBindingDivisor (int bindingindex, int divisor);

    /**
     * @param source
     * @param type
     * @param severity
     * @param count
     * @param ids ,length count
     * @param enabled ,value from enumeration group Boolean
     */
     void glDebugMessageControl (int source, int type, int severity, java.nio.IntBuffer ids, boolean enabled);

    /**
     * @param source
     * @param type
     * @param id
     * @param severity
     * @param length
     * @param buf
     */
     void glDebugMessageInsert (int source, int type, int id, int severity, int length, science.unlicense.api.character.CharArray buf);

    /**
     * @param callback
     * @param userParam
     */
     void glDebugMessageCallback (Object callback, long userParam);

    /**
     * @param count
     * @param bufSize
     * @param sources ,length count
     * @param types ,length count
     * @param ids ,length count
     * @param severities ,length count
     * @param lengths ,length count
     * @param messageLog ,length bufSize
     */
     int glGetDebugMessageLog (java.nio.IntBuffer sources, java.nio.IntBuffer types, java.nio.IntBuffer ids, java.nio.IntBuffer severities, java.nio.IntBuffer lengths, java.nio.ByteBuffer messageLog);

    /**
     * @param source
     * @param id
     * @param length
     * @param message
     */
     void glPushDebugGroup (int source, int id, int length, science.unlicense.api.character.CharArray message);

     void glPopDebugGroup ();

    /**
     * @param identifier
     * @param name
     * @param length
     * @param label
     */
     void glObjectLabel (int identifier, int name, int length, science.unlicense.api.character.CharArray label);

    /**
     * @param identifier
     * @param name
     * @param bufSize
     * @param length
     * @param label ,length bufSize
     */
     void glGetObjectLabel (int identifier, int name, java.nio.IntBuffer length, java.nio.ByteBuffer label);

    /**
     * @param ptr
     * @param length
     * @param label
     */
     void glObjectPtrLabel (long ptr, int length, science.unlicense.api.character.CharArray label);

    /**
     * @param ptr
     * @param bufSize
     * @param length
     * @param label ,length bufSize
     */
     void glGetObjectPtrLabel (long ptr, java.nio.IntBuffer length, java.nio.ByteBuffer label);

    /**
     * @param pname ,value from enumeration group GetPointervPName
     * @param params
     */
     void glGetPointerv (int pname, java.nio.ByteBuffer params);

}