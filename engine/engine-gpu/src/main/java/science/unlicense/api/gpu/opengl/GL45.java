package science.unlicense.api.gpu.opengl;

import java.nio.ByteBuffer;

public interface GL45 extends science.unlicense.api.gpu.opengl.GL44 {

    /**
     * @param origin
     * @param depth
     */
     void glClipControl (int origin, int depth);

    /**
     * @param n
     * @param ids
     */
     void glCreateTransformFeedbacks (int n, java.nio.IntBuffer ids);

    /**
     * @param xfb
     * @param index
     * @param buffer
     */
     void glTransformFeedbackBufferBase (int xfb, int index, int buffer);

    /**
     * @param xfb
     * @param index
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     */
     void glTransformFeedbackBufferRange (int xfb, int index, int buffer, long offset, long size);

    /**
     * @param xfb
     * @param pname
     * @param param
     */
     void glGetTransformFeedbackiv (int xfb, int pname, java.nio.IntBuffer param);

    /**
     * @param xfb
     * @param pname
     * @param index
     * @param param
     */
     void glGetTransformFeedbacki_v (int xfb, int pname, int index, java.nio.IntBuffer param);

    /**
     * @param xfb
     * @param pname
     * @param index
     * @param param
     */
     void glGetTransformFeedbacki64_v (int xfb, int pname, int index, java.nio.ByteBuffer param);

    /**
     * @param n
     * @param buffers
     */
     void glCreateBuffers (int n, java.nio.IntBuffer buffers);

    /**
     * @param buffer
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     * @param flags
     */
     void glNamedBufferStorage (int buffer, java.nio.ByteBuffer data, int flags);

    /**
     * @param buffer
     * @param size ,value from enumeration group BufferSize
     * @param data
     * @param usage
     */
     void glNamedBufferData (int buffer, long size, java.nio.ByteBuffer data, int usage);

    /**
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     * @param data
     */
     void glNamedBufferSubData (int buffer, long offset, long size, java.nio.ByteBuffer data);

    /**
     * @param readBuffer
     * @param writeBuffer
     * @param readOffset
     * @param writeOffset
     * @param size ,value from enumeration group BufferSize
     */
     void glCopyNamedBufferSubData (int readBuffer, int writeBuffer, long readOffset, long writeOffset, long size);

    /**
     * @param buffer
     * @param internalformat
     * @param format
     * @param type
     * @param data
     */
     void glClearNamedBufferData (int buffer, int internalformat, int format, int type, java.nio.ByteBuffer data);

    /**
     * @param buffer
     * @param internalformat
     * @param offset
     * @param size ,value from enumeration group BufferSize
     * @param format
     * @param type
     * @param data
     */
     void glClearNamedBufferSubData (int buffer, int internalformat, long offset, long size, int format, int type, long data);

    /**
     * @param buffer
     * @param access
     */
     void glMapNamedBuffer (int buffer, int access);

    /**
     * @param buffer
     * @param offset
     * @param length ,value from enumeration group BufferSize
     * @param access
     */
     void glMapNamedBufferRange (int buffer, long offset, long length, int access);

    /**
     * @param buffer
     */
     boolean glUnmapNamedBuffer (int buffer);

    /**
     * @param buffer
     * @param offset
     * @param length ,value from enumeration group BufferSize
     */
     void glFlushMappedNamedBufferRange (int buffer, long offset, long length);

    /**
     * @param buffer
     * @param pname
     * @param params
     */
     void glGetNamedBufferParameteriv (int buffer, int pname, java.nio.IntBuffer params);

    /**
     * @param buffer
     * @param pname
     * @param params
     */
     void glGetNamedBufferParameteri64v (int buffer, int pname, java.nio.ByteBuffer params);

    /**
     * @param buffer
     * @param pname
     * @param params
     */
     void glGetNamedBufferPointerv (int buffer, int pname, java.nio.ByteBuffer params);

    /**
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     * @param data
     */
     void glGetNamedBufferSubData (int buffer, long offset, long size, java.nio.ByteBuffer data);

    /**
     * @param n
     * @param framebuffers
     */
     void glCreateFramebuffers (int n, java.nio.IntBuffer framebuffers);

    /**
     * @param framebuffer
     * @param attachment
     * @param renderbuffertarget
     * @param renderbuffer
     */
     void glNamedFramebufferRenderbuffer (int framebuffer, int attachment, int renderbuffertarget, int renderbuffer);

    /**
     * @param framebuffer
     * @param pname
     * @param param
     */
     void glNamedFramebufferParameteri (int framebuffer, int pname, int param);

    /**
     * @param framebuffer
     * @param attachment
     * @param texture
     * @param level
     */
     void glNamedFramebufferTexture (int framebuffer, int attachment, int texture, int level);

    /**
     * @param framebuffer
     * @param attachment
     * @param texture
     * @param level
     * @param layer
     */
     void glNamedFramebufferTextureLayer (int framebuffer, int attachment, int texture, int level, int layer);

    /**
     * @param framebuffer
     * @param buf
     */
     void glNamedFramebufferDrawBuffer (int framebuffer, int buf);

    /**
     * @param framebuffer
     * @param n
     * @param bufs
     */
     void glNamedFramebufferDrawBuffers (int framebuffer, int n, java.nio.IntBuffer bufs);

    /**
     * @param framebuffer
     * @param src
     */
     void glNamedFramebufferReadBuffer (int framebuffer, int src);

    /**
     * @param framebuffer
     * @param numAttachments
     * @param attachments
     */
     void glInvalidateNamedFramebufferData (int framebuffer, int numAttachments, java.nio.IntBuffer attachments);

    /**
     * @param framebuffer
     * @param numAttachments
     * @param attachments
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glInvalidateNamedFramebufferSubData (int framebuffer, int numAttachments, java.nio.IntBuffer attachments, int x, int y, int width, int height);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param value
     */
     void glClearNamedFramebufferiv (int framebuffer, int buffer, int drawbuffer, java.nio.IntBuffer value);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param value
     */
     void glClearNamedFramebufferuiv (int framebuffer, int buffer, int drawbuffer, java.nio.IntBuffer value);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param value
     */
     void glClearNamedFramebufferfv (int framebuffer, int buffer, int drawbuffer, java.nio.FloatBuffer value);

    /**
     * @param framebuffer
     * @param buffer
     * @param drawbuffer
     * @param depth
     * @param stencil
     */
     void glClearNamedFramebufferfi (int framebuffer, int buffer, int drawbuffer, float depth, int stencil);

    /**
     * @param readFramebuffer
     * @param drawFramebuffer
     * @param srcX0
     * @param srcY0
     * @param srcX1
     * @param srcY1
     * @param dstX0
     * @param dstY0
     * @param dstX1
     * @param dstY1
     * @param mask
     * @param filter
     */
     void glBlitNamedFramebuffer (int readFramebuffer, int drawFramebuffer, int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter);

    /**
     * @param framebuffer
     * @param target
     */
     int glCheckNamedFramebufferStatus (int framebuffer, int target);

    /**
     * @param framebuffer
     * @param pname
     * @param param
     */
     void glGetNamedFramebufferParameteriv (int framebuffer, int pname, java.nio.IntBuffer param);

    /**
     * @param framebuffer
     * @param attachment
     * @param pname
     * @param params
     */
     void glGetNamedFramebufferAttachmentParameteriv (int framebuffer, int attachment, int pname, java.nio.IntBuffer params);

    /**
     * @param n
     * @param renderbuffers
     */
     void glCreateRenderbuffers (int n, java.nio.IntBuffer renderbuffers);

    /**
     * @param renderbuffer
     * @param internalformat
     * @param width
     * @param height
     */
     void glNamedRenderbufferStorage (int renderbuffer, int internalformat, int width, int height);

    /**
     * @param renderbuffer
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     */
     void glNamedRenderbufferStorageMultisample (int renderbuffer, int samples, int internalformat, int width, int height);

    /**
     * @param renderbuffer
     * @param pname
     * @param params
     */
     void glGetNamedRenderbufferParameteriv (int renderbuffer, int pname, java.nio.IntBuffer params);

    /**
     * @param target
     * @param n
     * @param textures
     */
     void glCreateTextures (int target, int n, java.nio.IntBuffer textures);

    /**
     * @param texture
     * @param internalformat
     * @param buffer
     */
     void glTextureBuffer (int texture, int internalformat, int buffer);

    /**
     * @param texture
     * @param internalformat
     * @param buffer
     * @param offset
     * @param size ,value from enumeration group BufferSize
     */
     void glTextureBufferRange (int texture, int internalformat, int buffer, long offset, long size);

    /**
     * @param texture
     * @param levels
     * @param internalformat
     * @param width
     */
     void glTextureStorage1D (int texture, int levels, int internalformat, int width);

    /**
     * @param texture
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     */
     void glTextureStorage2D (int texture, int levels, int internalformat, int width, int height);

    /**
     * @param texture
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     */
     void glTextureStorage3D (int texture, int levels, int internalformat, int width, int height, int depth);

    /**
     * @param texture
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param fixedsamplelocations
     */
     void glTextureStorage2DMultisample (int texture, int samples, int internalformat, int width, int height, boolean fixedsamplelocations);

    /**
     * @param texture
     * @param samples
     * @param internalformat
     * @param width
     * @param height
     * @param depth
     * @param fixedsamplelocations
     */
     void glTextureStorage3DMultisample (int texture, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param width
     * @param format
     * @param type
     * @param pixels
     */
     void glTextureSubImage1D (int texture, int level, int xoffset, int width, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param width
     * @param height
     * @param format
     * @param type
     * @param pixels
     */
     void glTextureSubImage2D (int texture, int level, int xoffset, int yoffset, int width, int height, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param type
     * @param pixels
     */
     void glTextureSubImage3D (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param width
     * @param format
     * @param imageSize
     * @param data
     */
     void glCompressedTextureSubImage1D (int texture, int level, int xoffset, int width, int format, int imageSize, java.nio.ByteBuffer data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param width
     * @param height
     * @param format
     * @param imageSize
     * @param data
     */
     void glCompressedTextureSubImage2D (int texture, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, java.nio.ByteBuffer data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param imageSize
     * @param data
     */
     void glCompressedTextureSubImage3D (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, java.nio.ByteBuffer data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param x
     * @param y
     * @param width
     */
     void glCopyTextureSubImage1D (int texture, int level, int xoffset, int x, int y, int width);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glCopyTextureSubImage2D (int texture, int level, int xoffset, int yoffset, int x, int y, int width, int height);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param x
     * @param y
     * @param width
     * @param height
     */
     void glCopyTextureSubImage3D (int texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameterf (int texture, int pname, float param);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameterfv (int texture, int pname, java.nio.FloatBuffer param);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameteri (int texture, int pname, int param);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glTextureParameterIiv (int texture, int pname, java.nio.IntBuffer params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glTextureParameterIuiv (int texture, int pname, java.nio.IntBuffer params);

    /**
     * @param texture
     * @param pname
     * @param param
     */
     void glTextureParameteriv (int texture, int pname, java.nio.IntBuffer param);

    /**
     * @param texture
     */
     void glGenerateTextureMipmap (int texture);

    /**
     * @param unit
     * @param texture
     */
     void glBindTextureUnit (int unit, int texture);

    /**
     * @param texture
     * @param level
     * @param format
     * @param type
     * @param bufSize
     * @param pixels
     */
     void glGetTextureImage (int texture, int level, int format, int type, int bufSize, java.nio.ByteBuffer pixels);

    /**
     * @param texture
     * @param level
     * @param bufSize
     * @param pixels
     */
     void glGetCompressedTextureImage (int texture, int level, int bufSize, java.nio.ByteBuffer pixels);

    /**
     * @param texture
     * @param level
     * @param pname
     * @param params
     */
     void glGetTextureLevelParameterfv (int texture, int level, int pname, java.nio.FloatBuffer params);

    /**
     * @param texture
     * @param level
     * @param pname
     * @param params
     */
     void glGetTextureLevelParameteriv (int texture, int level, int pname, java.nio.IntBuffer params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameterfv (int texture, int pname, java.nio.FloatBuffer params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameterIiv (int texture, int pname, java.nio.IntBuffer params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameterIuiv (int texture, int pname, java.nio.IntBuffer params);

    /**
     * @param texture
     * @param pname
     * @param params
     */
     void glGetTextureParameteriv (int texture, int pname, java.nio.IntBuffer params);

    /**
     * @param n
     * @param arrays
     */
     void glCreateVertexArrays (int n, java.nio.IntBuffer arrays);

    /**
     * @param vaobj
     * @param index
     */
     void glDisableVertexArrayAttrib (int vaobj, int index);

    /**
     * @param vaobj
     * @param index
     */
     void glEnableVertexArrayAttrib (int vaobj, int index);

    /**
     * @param vaobj
     * @param buffer
     */
     void glVertexArrayElementBuffer (int vaobj, int buffer);

    /**
     * @param vaobj
     * @param bindingindex
     * @param buffer
     * @param offset
     * @param stride
     */
     void glVertexArrayVertexBuffer (int vaobj, int bindingindex, int buffer, long offset, int stride);

    /**
     * @param vaobj
     * @param first
     * @param count
     * @param buffers
     * @param offsets
     * @param strides
     */
     void glVertexArrayVertexBuffers (int vaobj, int first, int count, java.nio.IntBuffer buffers, java.nio.ByteBuffer offsets, java.nio.IntBuffer strides);

    /**
     * @param vaobj
     * @param attribindex
     * @param bindingindex
     */
     void glVertexArrayAttribBinding (int vaobj, int attribindex, int bindingindex);

    /**
     * @param vaobj
     * @param attribindex
     * @param size
     * @param type
     * @param normalized
     * @param relativeoffset
     */
     void glVertexArrayAttribFormat (int vaobj, int attribindex, int size, int type, boolean normalized, int relativeoffset);

    /**
     * @param vaobj
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexArrayAttribIFormat (int vaobj, int attribindex, int size, int type, int relativeoffset);

    /**
     * @param vaobj
     * @param attribindex
     * @param size
     * @param type
     * @param relativeoffset
     */
     void glVertexArrayAttribLFormat (int vaobj, int attribindex, int size, int type, int relativeoffset);

    /**
     * @param vaobj
     * @param bindingindex
     * @param divisor
     */
     void glVertexArrayBindingDivisor (int vaobj, int bindingindex, int divisor);

    /**
     * @param vaobj
     * @param pname
     * @param param
     */
     void glGetVertexArrayiv (int vaobj, int pname, java.nio.IntBuffer param);

    /**
     * @param vaobj
     * @param index
     * @param pname
     * @param param
     */
     void glGetVertexArrayIndexediv (int vaobj, int index, int pname, java.nio.IntBuffer param);

    /**
     * @param vaobj
     * @param index
     * @param pname
     * @param param
     */
     void glGetVertexArrayIndexed64iv (int vaobj, int index, int pname, java.nio.ByteBuffer param);

    /**
     * @param n
     * @param samplers
     */
     void glCreateSamplers (int n, java.nio.IntBuffer samplers);

    /**
     * @param n
     * @param pipelines
     */
     void glCreateProgramPipelines (int n, java.nio.IntBuffer pipelines);

    /**
     * @param target
     * @param n
     * @param ids
     */
     void glCreateQueries (int target, int n, java.nio.IntBuffer ids);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjecti64v (int id, int buffer, int pname, long offset);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjectiv (int id, int buffer, int pname, long offset);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjectui64v (int id, int buffer, int pname, long offset);

    /**
     * @param id
     * @param buffer
     * @param pname
     * @param offset
     */
     void glGetQueryBufferObjectuiv (int id, int buffer, int pname, long offset);

    /**
     * @param barriers
     */
     void glMemoryBarrierByRegion (int barriers);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param type
     * @param bufSize
     * @param pixels
     */
     void glGetTextureSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, int bufSize, java.nio.ByteBuffer pixels);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param bufSize
     * @param pixels
     */
     void glGetCompressedTextureSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, java.nio.ByteBuffer pixels);

     int glGetGraphicsResetStatus ();

    /**
     * @param target
     * @param lod
     * @param bufSize
     * @param pixels
     */
     void glGetnCompressedTexImage (int target, int lod, int bufSize, java.nio.ByteBuffer pixels);

    /**
     * @param target
     * @param level
     * @param format
     * @param type
     * @param bufSize
     * @param pixels
     */
     void glGetnTexImage (int target, int level, int format, int type, int bufSize, java.nio.ByteBuffer pixels);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformdv (int program, int location, int bufSize, java.nio.DoubleBuffer params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformfv (int program, int location, int bufSize, java.nio.FloatBuffer params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformiv (int program, int location, int bufSize, java.nio.IntBuffer params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformuiv (int program, int location, int bufSize, java.nio.IntBuffer params);

    /**
     * @param x
     * @param y
     * @param width
     * @param height
     * @param format
     * @param type
     * @param bufSize
     * @param data
     */
     void glReadnPixels (int x, int y, int width, int height, int format, int type, int bufSize, java.nio.ByteBuffer data);

    /**
     * @param target
     * @param query
     * @param bufSize
     * @param v
     */
     void glGetnMapdv (int target, int query, int bufSize, java.nio.DoubleBuffer v);

    /**
     * @param target
     * @param query
     * @param bufSize
     * @param v
     */
     void glGetnMapfv (int target, int query, int bufSize, java.nio.FloatBuffer v);

    /**
     * @param target
     * @param query
     * @param bufSize
     * @param v
     */
     void glGetnMapiv (int target, int query, int bufSize, java.nio.IntBuffer v);

    /**
     * @param map
     * @param bufSize
     * @param values
     */
     void glGetnPixelMapfv (int map, int bufSize, java.nio.FloatBuffer values);

    /**
     * @param map
     * @param bufSize
     * @param values
     */
     void glGetnPixelMapuiv (int map, int bufSize, java.nio.IntBuffer values);

    /**
     * @param map
     * @param bufSize
     * @param values
     */
     void glGetnPixelMapusv (int map, int bufSize, java.nio.ShortBuffer values);

    /**
     * @param bufSize
     * @param pattern
     */
     void glGetnPolygonStipple (int bufSize, java.nio.ByteBuffer pattern);

    /**
     * @param target
     * @param format
     * @param type
     * @param bufSize
     * @param table
     */
     void glGetnColorTable (int target, int format, int type, int bufSize, java.nio.ByteBuffer table);

    /**
     * @param target
     * @param format
     * @param type
     * @param bufSize
     * @param image
     */
     void glGetnConvolutionFilter (int target, int format, int type, int bufSize, java.nio.ByteBuffer image);

    /**
     * @param target
     * @param format
     * @param type
     * @param rowBufSize
     * @param row
     * @param columnBufSize
     * @param column
     * @param span
     */
     void glGetnSeparableFilter (int target, int format, int type, int rowBufSize, java.nio.ByteBuffer row, int columnBufSize, java.nio.ByteBuffer column, java.nio.ByteBuffer span);

    /**
     * @param target
     * @param reset
     * @param format
     * @param type
     * @param bufSize
     * @param values
     */
     void glGetnHistogram (int target, boolean reset, int format, int type, int bufSize, java.nio.ByteBuffer values);

    /**
     * @param target
     * @param reset
     * @param format
     * @param type
     * @param bufSize
     * @param values
     */
     void glGetnMinmax (int target, boolean reset, int format, int type, int bufSize, java.nio.ByteBuffer values);

     void glTextureBarrier ();

}