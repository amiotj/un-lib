package science.unlicense.api.gpu.opengl;

public interface GLSC220 {

    /**
     * @param texture ,value from enumeration group TextureUnit
     */
     void glActiveTexture (int texture);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param buffer
     */
     void glBindBuffer (int target, int buffer);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param framebuffer
     */
     void glBindFramebuffer (int target, int framebuffer);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param renderbuffer
     */
     void glBindRenderbuffer (int target, int renderbuffer);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param texture ,value from enumeration group Texture
     */
     void glBindTexture (int target, int texture);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glBlendColor (float red, float green, float blue, float alpha);

    /**
     * @param mode ,value from enumeration group BlendEquationMode
     */
     void glBlendEquation (int mode);

    /**
     * @param modeRGB ,value from enumeration group BlendEquationModeEXT
     * @param modeAlpha ,value from enumeration group BlendEquationModeEXT
     */
     void glBlendEquationSeparate (int modeRGB, int modeAlpha);

    /**
     * @param sfactor ,value from enumeration group BlendingFactorSrc
     * @param dfactor ,value from enumeration group BlendingFactorDest
     */
     void glBlendFunc (int sfactor, int dfactor);

    /**
     * @param sfactorRGB ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param dfactorRGB ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param sfactorAlpha ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param dfactorAlpha ,value from enumeration group BlendFuncSeparateParameterEXT
     */
     void glBlendFuncSeparate (int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     * @param usage ,value from enumeration group BufferUsageARB
     */
     void glBufferData (int target, java.nio.Buffer data, int usage);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param offset ,value from enumeration group BufferOffset
     * @param size ,value from enumeration group BufferSize
     * @param data ,length size
     */
     void glBufferSubData (int target, long offset, java.nio.Buffer data);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     */
     int glCheckFramebufferStatus (int target);

    /**
     * @param mask ,value from enumeration group ClearBufferMask
     */
     void glClear (int mask);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glClearColor (float red, float green, float blue, float alpha);

    /**
     * @param d
     */
     void glClearDepthf (float d);

    /**
     * @param s ,value from enumeration group StencilValue
     */
     void glClearStencil (int s);

    /**
     * @param red ,value from enumeration group Boolean
     * @param green ,value from enumeration group Boolean
     * @param blue ,value from enumeration group Boolean
     * @param alpha ,value from enumeration group Boolean
     */
     void glColorMask (boolean red, boolean green, boolean blue, boolean alpha);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param imageSize
     * @param data ,value from enumeration group CompressedTextureARB ,length imageSize
     */
     void glCompressedTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, java.nio.ByteBuffer data);

     int glCreateProgram ();

    /**
     * @param mode ,value from enumeration group CullFaceMode
     */
     void glCullFace (int mode);

    /**
     * @param func ,value from enumeration group DepthFunction
     */
     void glDepthFunc (int func);

    /**
     * @param flag ,value from enumeration group Boolean
     */
     void glDepthMask (boolean flag);

    /**
     * @param n
     * @param f
     */
     void glDepthRangef (float n, float f);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glDisable (int cap);

    /**
     * @param index
     */
     void glDisableVertexAttribArray (int index);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     */
     void glDrawArrays (int mode, int first, int count);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param start
     * @param end
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     */
     void glDrawRangeElements (int mode, int start, int end, int count, int type, java.nio.ByteBuffer indices);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     void glEnable (int cap);

    /**
     * @param index
     */
     void glEnableVertexAttribArray (int index);

     void glFinish ();

     void glFlush ();

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param renderbuffertarget ,value from enumeration group RenderbufferTarget
     * @param renderbuffer
     */
     void glFramebufferRenderbuffer (int target, int attachment, int renderbuffertarget, int renderbuffer);

    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param textarget
     * @param texture
     * @param level
     */
     void glFramebufferTexture2D (int target, int attachment, int textarget, int texture, int level);

    /**
     * @param mode ,value from enumeration group FrontFaceDirection
     */
     void glFrontFace (int mode);

    /**
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (java.nio.IntBuffer buffers);

    /**
     * Convenient method.
     *
     * @param n
     * @param buffers ,length n
     */
     void glGenBuffers (int[] buffers);
     
    /**
     * @param target
     */
     void glGenerateMipmap (int target);

    /**
     * @param n
     * @param framebuffers ,length n
     */
     void glGenFramebuffers (java.nio.IntBuffer framebuffers);

    /**
     * @param n
     * @param renderbuffers ,length n
     */
     void glGenRenderbuffers (java.nio.IntBuffer renderbuffers);

    /**
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
     */
     void glGenTextures (java.nio.IntBuffer textures);

     /**
      * Convenient method
      *
     * @param n
     * @param textures ,value from enumeration group Texture ,length n
      */
     void glGenTextures(int[] textures);
     
    /**
     * @param program
     * @param name
     */
     int glGetAttribLocation (int program, science.unlicense.api.character.CharArray name);

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data ,value from enumeration group Boolean
     */
     void glGetBooleanv (int pname, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group BufferTargetARB
     * @param pname ,value from enumeration group BufferPNameARB
     * @param params
     */
     void glGetBufferParameteriv (int target, int pname, java.nio.IntBuffer params);

     int glGetError ();

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, java.nio.FloatBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetFloatv (int pname, float[] data);
     
    /**
     * @param target ,value from enumeration group FramebufferTarget
     * @param attachment ,value from enumeration group FramebufferAttachment
     * @param pname
     * @param params
     */
     void glGetFramebufferAttachmentParameteriv (int target, int attachment, int pname, java.nio.IntBuffer params);

     int glGetGraphicsResetStatus ();

    /**
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, java.nio.IntBuffer data);

    /**
     * Convenient method.
     *
     * @param pname ,value from enumeration group GetPName
     * @param data
     */
     void glGetIntegerv (int pname, int[] data);
     
    /**
     * @param program
     * @param pname
     * @param params
     */
     void glGetProgramiv (int program, int pname, java.nio.IntBuffer params);

    /**
     * Convenient method.
     *
     * @param program
     * @param pname
     * @param params
     */
     void glGetProgramiv (int program, int pname, int[] params);
     
    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param pname
     * @param params
     */
     void glGetRenderbufferParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param name ,value from enumeration group StringName
     */
     byte glGetString (int name);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameterfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group GetTextureParameter
     * @param params
     */
     void glGetTexParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformfv (int program, int location, int bufSize, java.nio.FloatBuffer params);

    /**
     * @param program
     * @param location
     * @param bufSize
     * @param params
     */
     void glGetnUniformiv (int program, int location, int bufSize, java.nio.IntBuffer params);

    /**
     * @param program
     * @param name
     */
     int glGetUniformLocation (int program, science.unlicense.api.character.CharArray name);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribfv (int index, int pname, java.nio.FloatBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPropertyARB
     * @param params
     */
     void glGetVertexAttribiv (int index, int pname, java.nio.IntBuffer params);

    /**
     * @param index
     * @param pname ,value from enumeration group VertexAttribPointerPropertyARB
     * @param pointer
     */
     void glGetVertexAttribPointerv (int index, int pname, java.nio.ByteBuffer pointer);

    /**
     * @param target ,value from enumeration group HintTarget
     * @param mode ,value from enumeration group HintMode
     */
     void glHint (int target, int mode);

    /**
     * @param cap ,value from enumeration group EnableCap
     */
     boolean glIsEnabled (int cap);

    /**
     * @param width ,value from enumeration group CheckedFloat32
     */
     void glLineWidth (float width);

    /**
     * @param pname ,value from enumeration group PixelStoreParameter
     * @param param ,value from enumeration group CheckedInt32
     */
     void glPixelStorei (int pname, int param);

    /**
     * @param factor
     * @param units
     */
     void glPolygonOffset (float factor, float units);

    /**
     * @param program
     * @param binaryFormat
     * @param binary ,length length
     * @param length
     */
     void glProgramBinary (int program, int binaryFormat, java.nio.ByteBuffer binary);

    /**
     * @param x
     * @param y
     * @param width
     * @param height
     * @param format
     * @param type
     * @param bufSize
     * @param data
     */
     void glReadnPixels (int x, int y, int width, int height, int format, int type, int bufSize, java.nio.ByteBuffer data);

    /**
     * @param target ,value from enumeration group RenderbufferTarget
     * @param internalformat
     * @param width
     * @param height
     */
     void glRenderbufferStorage (int target, int internalformat, int width, int height);

    /**
     * @param value
     * @param invert ,value from enumeration group Boolean
     */
     void glSampleCoverage (float value, boolean invert);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glScissor (int x, int y, int width, int height);

    /**
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFunc (int func, int ref, int mask);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param func ,value from enumeration group StencilFunction
     * @param ref ,value from enumeration group StencilValue
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilFuncSeparate (int face, int func, int ref, int mask);

    /**
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMask (int mask);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param mask ,value from enumeration group MaskedStencilValue
     */
     void glStencilMaskSeparate (int face, int mask);

    /**
     * @param fail ,value from enumeration group StencilOp
     * @param zfail ,value from enumeration group StencilOp
     * @param zpass ,value from enumeration group StencilOp
     */
     void glStencilOp (int fail, int zfail, int zpass);

    /**
     * @param face ,value from enumeration group StencilFaceDirection
     * @param sfail ,value from enumeration group StencilOp
     * @param dpfail ,value from enumeration group StencilOp
     * @param dppass ,value from enumeration group StencilOp
     */
     void glStencilOpSeparate (int face, int sfail, int dpfail, int dppass);

    /**
     * @param target
     * @param levels
     * @param internalformat
     * @param width
     * @param height
     */
     void glTexStorage2D (int target, int levels, int internalformat, int width, int height);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glTexParameterf (int target, int pname, float param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glTexParameterfv (int target, int pname, java.nio.FloatBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param param ,value from enumeration group CheckedInt32
     */
     void glTexParameteri (int target, int pname, int param);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param pname ,value from enumeration group TextureParameterName
     * @param params ,value from enumeration group CheckedInt32
     */
     void glTexParameteriv (int target, int pname, java.nio.IntBuffer params);

    /**
     * @param target ,value from enumeration group TextureTarget
     * @param level ,value from enumeration group CheckedInt32
     * @param xoffset ,value from enumeration group CheckedInt32
     * @param yoffset ,value from enumeration group CheckedInt32
     * @param width
     * @param height
     * @param format ,value from enumeration group PixelFormat
     * @param type ,value from enumeration group PixelType
     * @param pixels
     */
     void glTexSubImage2D (int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, java.nio.ByteBuffer pixels);

    /**
     * @param location
     * @param v0
     */
     void glUniform1f (int location, float v0);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1fv (int location, java.nio.FloatBuffer value);

    /**
     * @param location
     * @param v0
     */
     void glUniform1i (int location, int v0);

    /**
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform1iv (int location, int[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2f (int location, float v0, float v1);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUnifor2fv (int location, float[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     */
     void glUniform2i (int location, int v0, int v1);

    /**
     * @param location
     * @param count
     * @param value ,length count*2
     */
     void glUniform2iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform2iv (int location, int[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3f (int location, float v0, float v1, float v2);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform3fv (int location, float[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     */
     void glUniform3i (int location, int v0, int v1, int v2);

    /**
     * @param location
     * @param count
     * @param value ,length count*3
     */
     void glUniform3iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform3iv (int location, int[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4f (int location, float v0, float v1, float v2, float v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4fv (int location, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform4fv (int location, float[] value);

    /**
     * @param location
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
     void glUniform4i (int location, int v0, int v1, int v2, int v3);

    /**
     * @param location
     * @param count
     * @param value ,length count*4
     */
     void glUniform4iv (int location, java.nio.IntBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param value ,length count*1
     */
     void glUniform4iv (int location, int[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*4
     */
     void glUniformMatrix2fv (int location, boolean transpose, float[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*9
     */
     void glUniformMatrix3fv (int location, boolean transpose, float[] value);

    /**
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4fv (int location, boolean transpose, java.nio.FloatBuffer value);

    /**
     * Convenient method.
     *
     * @param location
     * @param count
     * @param transpose ,value from enumeration group Boolean
     * @param value ,length count*16
     */
     void glUniformMatrix4fv (int location, boolean transpose, float[] value);

    /**
     * @param program
     */
     void glUseProgram (int program);

    /**
     * @param index
     * @param x
     */
     void glVertexAttrib1f (int index, float x);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib1fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     */
     void glVertexAttrib2f (int index, float x, float y);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib2fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     */
     void glVertexAttrib3f (int index, float x, float y, float z);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib3fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param x
     * @param y
     * @param z
     * @param w
     */
     void glVertexAttrib4f (int index, float x, float y, float z, float w);

    /**
     * @param index
     * @param v
     */
     void glVertexAttrib4fv (int index, java.nio.FloatBuffer v);

    /**
     * @param index
     * @param size
     * @param type ,value from enumeration group VertexAttribPointerType
     * @param normalized ,value from enumeration group Boolean
     * @param stride
     * @param pointer
     */
     void glVertexAttribPointer (int index, int size, int type, boolean normalized, int stride, long pointer);

    /**
     * @param x ,value from enumeration group WinCoord
     * @param y ,value from enumeration group WinCoord
     * @param width
     * @param height
     */
     void glViewport (int x, int y, int width, int height);

}