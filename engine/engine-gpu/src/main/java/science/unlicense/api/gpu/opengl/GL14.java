package science.unlicense.api.gpu.opengl;

public interface GL14 extends science.unlicense.api.gpu.opengl.GL13 {

    /**
     * @param sfactorRGB ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param dfactorRGB ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param sfactorAlpha ,value from enumeration group BlendFuncSeparateParameterEXT
     * @param dfactorAlpha ,value from enumeration group BlendFuncSeparateParameterEXT
     */
     void glBlendFuncSeparate (int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param first
     * @param count
     * @param drawcount
     */
     void glMultiDrawArrays (int mode, java.nio.IntBuffer first, java.nio.IntBuffer count, int drawcount);

    /**
     * @param mode ,value from enumeration group PrimitiveType
     * @param count
     * @param type ,value from enumeration group DrawElementsType
     * @param indices
     * @param drawcount
     */
     void glMultiDrawElements (int mode, java.nio.IntBuffer count, int type, int indices, int drawcount);

    /**
     * @param pname ,value from enumeration group PointParameterNameARB
     * @param param ,value from enumeration group CheckedFloat32
     */
     void glPointParameterf (int pname, float param);

    /**
     * @param pname ,value from enumeration group PointParameterNameARB
     * @param params ,value from enumeration group CheckedFloat32
     */
     void glPointParameterfv (int pname, java.nio.FloatBuffer params);

    /**
     * @param pname ,value from enumeration group PointParameterNameARB
     * @param param
     */
     void glPointParameteri (int pname, int param);

    /**
     * @param pname ,value from enumeration group PointParameterNameARB
     * @param params
     */
     void glPointParameteriv (int pname, java.nio.IntBuffer params);

    /**
     * @param coord ,value from enumeration group CoordF
     */
     void glFogCoordf (float coord);

    /**
     * @param coord ,value from enumeration group CoordF
     */
     void glFogCoordfv (java.nio.FloatBuffer coord);

    /**
     * @param coord ,value from enumeration group CoordD
     */
     void glFogCoordd (double coord);

    /**
     * @param coord ,value from enumeration group CoordD
     */
     void glFogCoorddv (java.nio.DoubleBuffer coord);

    /**
     * @param type ,value from enumeration group FogPointerTypeEXT
     * @param stride
     * @param pointer
     */
     void glFogCoordPointer (int type, int stride, java.nio.ByteBuffer pointer);

    /**
     * @param red ,value from enumeration group ColorB
     * @param green ,value from enumeration group ColorB
     * @param blue ,value from enumeration group ColorB
     */
     void glSecondaryColor3b (byte red, byte green, byte blue);

    /**
     * @param v ,value from enumeration group ColorB
     */
     void glSecondaryColor3bv (java.nio.ByteBuffer v);

    /**
     * @param red ,value from enumeration group ColorD
     * @param green ,value from enumeration group ColorD
     * @param blue ,value from enumeration group ColorD
     */
     void glSecondaryColor3d (double red, double green, double blue);

    /**
     * @param v ,value from enumeration group ColorD
     */
     void glSecondaryColor3dv (java.nio.DoubleBuffer v);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     */
     void glSecondaryColor3f (float red, float green, float blue);

    /**
     * @param v ,value from enumeration group ColorF
     */
     void glSecondaryColor3fv (java.nio.FloatBuffer v);

    /**
     * @param red ,value from enumeration group ColorI
     * @param green ,value from enumeration group ColorI
     * @param blue ,value from enumeration group ColorI
     */
     void glSecondaryColor3i (int red, int green, int blue);

    /**
     * @param v ,value from enumeration group ColorI
     */
     void glSecondaryColor3iv (java.nio.IntBuffer v);

    /**
     * @param red ,value from enumeration group ColorS
     * @param green ,value from enumeration group ColorS
     * @param blue ,value from enumeration group ColorS
     */
     void glSecondaryColor3s (short red, short green, short blue);

    /**
     * @param v ,value from enumeration group ColorS
     */
     void glSecondaryColor3sv (java.nio.ShortBuffer v);

    /**
     * @param red ,value from enumeration group ColorUB
     * @param green ,value from enumeration group ColorUB
     * @param blue ,value from enumeration group ColorUB
     */
     void glSecondaryColor3ub (byte red, byte green, byte blue);

    /**
     * @param v ,value from enumeration group ColorUB
     */
     void glSecondaryColor3ubv (java.nio.ByteBuffer v);

    /**
     * @param red ,value from enumeration group ColorUI
     * @param green ,value from enumeration group ColorUI
     * @param blue ,value from enumeration group ColorUI
     */
     void glSecondaryColor3ui (int red, int green, int blue);

    /**
     * @param v ,value from enumeration group ColorUI
     */
     void glSecondaryColor3uiv (java.nio.IntBuffer v);

    /**
     * @param red ,value from enumeration group ColorUS
     * @param green ,value from enumeration group ColorUS
     * @param blue ,value from enumeration group ColorUS
     */
     void glSecondaryColor3us (short red, short green, short blue);

    /**
     * @param v ,value from enumeration group ColorUS
     */
     void glSecondaryColor3usv (java.nio.ShortBuffer v);

    /**
     * @param size
     * @param type ,value from enumeration group ColorPointerType
     * @param stride
     * @param pointer
     */
     void glSecondaryColorPointer (int size, int type, int stride, java.nio.ByteBuffer pointer);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     */
     void glWindowPos2d (double x, double y);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glWindowPos2dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     */
     void glWindowPos2f (float x, float y);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glWindowPos2fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     */
     void glWindowPos2i (int x, int y);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glWindowPos2iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     */
     void glWindowPos2s (short x, short y);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glWindowPos2sv (java.nio.ShortBuffer v);

    /**
     * @param x ,value from enumeration group CoordD
     * @param y ,value from enumeration group CoordD
     * @param z ,value from enumeration group CoordD
     */
     void glWindowPos3d (double x, double y, double z);

    /**
     * @param v ,value from enumeration group CoordD
     */
     void glWindowPos3dv (java.nio.DoubleBuffer v);

    /**
     * @param x ,value from enumeration group CoordF
     * @param y ,value from enumeration group CoordF
     * @param z ,value from enumeration group CoordF
     */
     void glWindowPos3f (float x, float y, float z);

    /**
     * @param v ,value from enumeration group CoordF
     */
     void glWindowPos3fv (java.nio.FloatBuffer v);

    /**
     * @param x ,value from enumeration group CoordI
     * @param y ,value from enumeration group CoordI
     * @param z ,value from enumeration group CoordI
     */
     void glWindowPos3i (int x, int y, int z);

    /**
     * @param v ,value from enumeration group CoordI
     */
     void glWindowPos3iv (java.nio.IntBuffer v);

    /**
     * @param x ,value from enumeration group CoordS
     * @param y ,value from enumeration group CoordS
     * @param z ,value from enumeration group CoordS
     */
     void glWindowPos3s (short x, short y, short z);

    /**
     * @param v ,value from enumeration group CoordS
     */
     void glWindowPos3sv (java.nio.ShortBuffer v);

    /**
     * @param red ,value from enumeration group ColorF
     * @param green ,value from enumeration group ColorF
     * @param blue ,value from enumeration group ColorF
     * @param alpha ,value from enumeration group ColorF
     */
     void glBlendColor (float red, float green, float blue, float alpha);

    /**
     * @param mode ,value from enumeration group BlendEquationMode
     */
     void glBlendEquation (int mode);

}