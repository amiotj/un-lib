
package science.unlicense.api.gpu;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import science.unlicense.api.buffer.AbstractBuffer;
import science.unlicense.api.buffer.Buffer;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.number.NumberEncoding;

/**
 *
 * @author Johann Sorel
 */
public class GLBuffer extends AbstractBuffer {

    private static final Method cleanerMethod;
    private static final Method cleanMethod;
    private static final Field attField;

    static {
        try {
            final Class clazzByteBuffer = Class.forName("java.nio.DirectByteBuffer");
            attField = clazzByteBuffer.getDeclaredField("att");
            attField.setAccessible(true);

            cleanerMethod = clazzByteBuffer.getMethod("cleaner");
            cleanerMethod.setAccessible(true);

            final Class clazzCleaner = Class.forName("sun.misc.Cleaner");
            cleanMethod = clazzCleaner.getMethod("clean");
            cleanMethod.setAccessible(true);


        } catch (ClassNotFoundException ex) {
            throw new IllegalStateException("DirectBuffer class not found");
        } catch (NoSuchFieldException ex) {
            throw new IllegalStateException("DirectBuffer class not found");
        } catch (SecurityException ex) {
            throw new IllegalStateException("DirectBuffer class not found");
        } catch (NoSuchMethodException ex) {
            throw new IllegalStateException("DirectBuffer class not found");
        }
    }

//    private final Exception ex = new Exception();
    private ByteBuffer buffer;

    public GLBuffer(ByteBuffer buffer, int primitiveType, NumberEncoding encoding) {
        super(primitiveType, encoding,GLBufferFactory.INSTANCE);
        this.buffer = buffer;
//        ex.fillInStackTrace();
    }
    
    public boolean isWritable() {
        return !buffer.isReadOnly();
    }

    public Buffer copy() {
        final GLBuffer copy = GLBufferFactory.INSTANCE.createByte(buffer.capacity());
        copy.buffer.put(buffer);
        return copy;
    }

    public long getBufferByteSize() {
        return buffer.capacity();
    }

    public ByteBuffer getBackEnd() {
        return buffer;
    }

    public byte readByte(long offset) {
        return buffer.get((int)offset);
    }

    public void readByte(byte[] array, int arrayOffset, int length, long offset) {
        final ByteBuffer buffer = this.buffer.duplicate();
        buffer.position((int) offset);
        buffer.get(array, arrayOffset, length);
    }

    public void writeByte(byte value, long offset) {
        buffer.put((int)offset, value);
    }

    public void writeByte(byte[] array, int arrayOffset, int length, long offset) {
        final ByteBuffer buffer = this.buffer.duplicate();
        buffer.position((int) offset);
        buffer.put(array, arrayOffset, length);
    }
    
    public void dispose(){
        if(buffer==null) return;
        dispose(buffer);
        this.buffer = null;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(buffer!=null){
//            Loggers.get().info(new Chars(
//                    "Unreleased direct buffer, the failsafe finalize method will dispose it. Memory "));
            //ex.printStackTrace();
//            JavaNioAccess.BufferPool pool = SharedSecrets.getJavaNioAccess().getDirectBufferPool();
//            Loggers.get().info(new Chars("B "+pool.getMemoryUsed()+"  "+pool.getCount()));
            dispose();
//            Loggers.get().info(new Chars("A "+pool.getMemoryUsed()+"  "+pool.getCount()));
        }
    }


    public static void dispose(java.nio.Buffer buffer){
        if(buffer!=null && buffer.isDirect()) {
            //the one way to clean java direct buffer
            try {
                if(!buffer.getClass().getName().equals("java.nio.DirectByteBuffer")) {
                    buffer = (java.nio.Buffer) attField.get(buffer);
                }
                final Object cleaner = cleanerMethod.invoke(buffer);
                cleanMethod.invoke(cleaner);
            } catch(Exception ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
    }


    /**
     * Return the size of given buffer in bytes.
     *
     * @param buffer
     * @return
     */
    public static int byteSize(java.nio.Buffer buffer) {
        final int size = buffer.capacity();
        if (buffer instanceof ByteBuffer) {
            return size;
        }else if (buffer instanceof ShortBuffer) {
            return size*2;
        }else if (buffer instanceof IntBuffer) {
            return size*4;
        }else if (buffer instanceof LongBuffer) {
            return size*8;
        }else if (buffer instanceof FloatBuffer) {
            return size*4;
        }else if (buffer instanceof DoubleBuffer) {
            return size*8;
        }else {
            throw new InvalidArgumentException("Unknowned buffer type : "+buffer);
        }
    }
    
}
