package science.unlicense.api.gpu.opengl;

public interface GL44 extends science.unlicense.api.gpu.opengl.GL43 {

    /**
     * @param target
     * @param size
     * @param data ,length size
     * @param flags
     */
     void glBufferStorage (int target, java.nio.ByteBuffer data, int flags);

    /**
     * @param texture
     * @param level
     * @param format
     * @param type
     * @param data
     */
     void glClearTexImage (int texture, int level, int format, int type, java.nio.ByteBuffer data);

    /**
     * @param texture
     * @param level
     * @param xoffset
     * @param yoffset
     * @param zoffset
     * @param width
     * @param height
     * @param depth
     * @param format
     * @param type
     * @param data
     */
     void glClearTexSubImage (int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, java.nio.ByteBuffer data);

    /**
     * @param target
     * @param first
     * @param count
     * @param buffers ,length count
     */
     void glBindBuffersBase (int target, int first, java.nio.IntBuffer buffers);

    /**
     * @param target
     * @param first
     * @param count
     * @param buffers ,length count
     * @param offsets ,length count
     * @param sizes ,length count
     */
     void glBindBuffersRange (int target, int first, java.nio.IntBuffer buffers, java.nio.ByteBuffer offsets, java.nio.ByteBuffer sizes);

    /**
     * @param first
     * @param count
     * @param textures ,length count
     */
     void glBindTextures (int first, java.nio.IntBuffer textures);

    /**
     * @param first
     * @param count
     * @param samplers ,length count
     */
     void glBindSamplers (int first, java.nio.IntBuffer samplers);

    /**
     * @param first
     * @param count
     * @param textures ,length count
     */
     void glBindImageTextures (int first, java.nio.IntBuffer textures);

    /**
     * @param first
     * @param count
     * @param buffers ,length count
     * @param offsets ,length count
     * @param strides ,length count
     */
     void glBindVertexBuffers (int first, java.nio.IntBuffer buffers, java.nio.ByteBuffer offsets, java.nio.IntBuffer strides);

}