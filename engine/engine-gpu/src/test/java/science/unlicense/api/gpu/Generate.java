
package science.unlicense.api.gpu;

import science.unlicense.api.character.CharBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.code.CodeContext;
import science.unlicense.api.code.Parameter;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.collection.HashSet;
import science.unlicense.api.collection.Iterator;
import science.unlicense.api.collection.Set;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.io.IOException;
import science.unlicense.api.model.tree.Node;
import science.unlicense.api.model.tree.Nodes;
import science.unlicense.api.number.Int32;
import science.unlicense.api.path.Path;
import science.unlicense.api.predicate.Predicate;
import science.unlicense.impl.code.java.JavaWriter;
import science.unlicense.impl.code.java.model.JavaClass;
import science.unlicense.impl.code.java.model.JavaDocumentation;
import science.unlicense.impl.code.java.model.JavaFunction;
import science.unlicense.impl.code.java.model.JavaMetas;
import science.unlicense.impl.code.java.model.JavaProperty;
import science.unlicense.impl.binding.xml.dom.DomElement;
import science.unlicense.impl.binding.xml.dom.DomNode;
import science.unlicense.impl.binding.xml.dom.DomReader;
import science.unlicense.impl.binding.xml.dom.DomUtilities;
import science.unlicense.system.path.Paths;

/**
 * Utility class to generate Khronos API java interfaces from API Registry xml files.
 *
 * @author Johann Sorel
 */
public class Generate {

    private static final Path REGISTRY_PATH = Paths.resolve(new Chars("file:/.../gl.xml"));
    private static final Path OUTPUT_PATH = Paths.resolve(new Chars("file:/.../glclasses"));
    private static final Chars DEFAULT_PACKAGE = new Chars("science.unlicense.api.gpu.opengl.");

    public static final JavaClass BUFFER_BYTE = new JavaClass(new Chars("java.nio.ByteBuffer"));
    public static final JavaClass BUFFER_SHORT = new JavaClass(new Chars("java.nio.ShortBuffer"));
    public static final JavaClass BUFFER_INT = new JavaClass(new Chars("java.nio.IntBuffer"));
    public static final JavaClass BUFFER_LONG = new JavaClass(new Chars("java.nio.LongBuffer"));
    public static final JavaClass BUFFER_FLOAT = new JavaClass(new Chars("java.nio.FloatBuffer"));
    public static final JavaClass BUFFER_DOUBLE = new JavaClass(new Chars("java.nio.DoubleBuffer"));


    private static final Chars REGISTRY = new Chars("registry");
    private static final Chars COMMENT = new Chars("comment");
    private static final Chars GROUPS = new Chars("groups");
    private static final Chars GROUP = new Chars("group");
    private static final Chars LEN = new Chars("len");
    private static final Chars NAME = new Chars("name");
    private static final Chars TYPES = new Chars("types");
    private static final Chars TYPE = new Chars("type");
    private static final Chars GLX = new Chars("glx");
    private static final Chars OPCODE = new Chars("opcode");
    private static final Chars FEATURE = new Chars("feature");
    private static final Chars REQUIRES = new Chars("requires");
    private static final Chars REQUIRE = new Chars("require");
    private static final Chars ENUMS = new Chars("enums");
    private static final Chars ENUM = new Chars("enum");
    private static final Chars VALUE = new Chars("value");
    private static final Chars UNUSED = new Chars("unused");
    private static final Chars START = new Chars("start");
    private static final Chars END = new Chars("end");
    private static final Chars VENDOR = new Chars("vendor");
    private static final Chars NAMESPACE = new Chars("namespace");
    private static final Chars API = new Chars("api");
    private static final Chars EXTENSIONS = new Chars("extensions");
    private static final Chars EXTENSION = new Chars("extension");
    private static final Chars SUPPORTED = new Chars("supported");
    private static final Chars NUMBER = new Chars("number");

    private static final Chars COMMANDS = new Chars("commands");
    private static final Chars COMMAND = new Chars("command");
    private static final Chars PROTO = new Chars("proto");
    private static final Chars PTYPE = new Chars("ptype");
    private static final Chars PARAM = new Chars("param");
    private static final Chars ALIAS = new Chars("alias");
    private static final Chars VECEQUIV = new Chars("vecequiv");

    private static final Chars BUFFER_REF = new Chars("const void *");
    private static final Chars BUFFER_REF2 = new Chars("void *");
    private static final Chars POINTER = new Chars(" *");
    private static final Chars BUFFER_POINTER = new Chars("void **");
    private static final Chars LEN_TEXT = new Chars("const void *const*");
    private static final Chars GLDEBUG = new Chars("GLDEBUGPROC");
//    private static final un.api.code.Class BUFFER_CLASS = new JavaClass(new Chars("science.unlicense.api.gpu.GLBuffer"));
//    private static final un.api.code.Class BUFFERPOINTER_CLASS = new JavaClass(new Chars("science.unlicense.api.gpu.GLBuffer"));
    private static final science.unlicense.api.code.Class BUFFER_CLASS = BUFFER_BYTE;
    private static final science.unlicense.api.code.Class BUFFERPOINTER_CLASS = BUFFER_BYTE;
    private static final science.unlicense.api.code.Class CHARARRAY_CLASS = new JavaClass(new Chars("science.unlicense.api.character.CharArray"));

    private static final Dictionary GLPTYPES = new HashDictionary();
    static {

        GLPTYPES.add(new Chars("GLboolean"), JavaClass.PRIMITIVE_BOOLEAN);

        GLPTYPES.add(new Chars("GLbyte"), JavaClass.PRIMITIVE_BYTE);
        GLPTYPES.add(new Chars("GLubyte"), JavaClass.PRIMITIVE_BYTE);

        GLPTYPES.add(new Chars("GLshort"), JavaClass.PRIMITIVE_SHORT);
        GLPTYPES.add(new Chars("GLushort"), JavaClass.PRIMITIVE_SHORT);

        GLPTYPES.add(new Chars("GLenum"), JavaClass.PRIMITIVE_INT);
        GLPTYPES.add(new Chars("GLint"), JavaClass.PRIMITIVE_INT);
        GLPTYPES.add(new Chars("GLuint"), JavaClass.PRIMITIVE_INT);
        GLPTYPES.add(new Chars("GLsizei"), JavaClass.PRIMITIVE_INT);
        GLPTYPES.add(new Chars("GLbitfield"), JavaClass.PRIMITIVE_INT);
        GLPTYPES.add(new Chars("GLfixed"), JavaClass.PRIMITIVE_INT);
        GLPTYPES.add(new Chars("GLclampx"), JavaClass.PRIMITIVE_INT);

        GLPTYPES.add(new Chars("GLsizeiptr"), JavaClass.PRIMITIVE_LONG);
        GLPTYPES.add(new Chars("GLintptr"), JavaClass.PRIMITIVE_LONG);
        GLPTYPES.add(new Chars("GLsync"), JavaClass.PRIMITIVE_LONG);
        GLPTYPES.add(new Chars("GLuint64"), JavaClass.PRIMITIVE_LONG);
        GLPTYPES.add(new Chars("GLint64"), JavaClass.PRIMITIVE_LONG);

        GLPTYPES.add(new Chars("GLfloat"), JavaClass.PRIMITIVE_FLOAT);
        GLPTYPES.add(new Chars("GLclampf"), JavaClass.PRIMITIVE_FLOAT);

        GLPTYPES.add(new Chars("GLdouble"), JavaClass.PRIMITIVE_DOUBLE);
        GLPTYPES.add(new Chars("GLclampd"), JavaClass.PRIMITIVE_DOUBLE);


        //special ones
        //TODO check for the ' *' text
        GLPTYPES.add(new Chars("GLchar"), CHARARRAY_CLASS);

    }



    public static void main(String[] args) throws IOException {
        new Generate(REGISTRY_PATH);

    }

    private final CodeContext context = new CodeContext();
    private DomNode root;

    private Generate(Path path) throws IOException{
        final DomReader reader = new DomReader();
        reader.setInput(path);
        root = reader.read();
        OUTPUT_PATH.createContainer();

        Iterator ite = root.getChildren().createIterator();
        while (ite.hasNext()) {
            final DomNode node = (DomNode) ite.next();
            if(!(node instanceof DomElement)) continue;
            final DomElement ele = (DomElement)node;
            if (FEATURE.equals(ele.getName().getLocalPart())) {
                final JavaClass clazz = toClass(ele);
                context.elements.add(clazz.id, clazz);
                JavaWriter writer =  new JavaWriter();
                writer.setOutput(OUTPUT_PATH.resolve(clazz.getShortName().concat(new Chars(".java"))));
                writer.write(clazz);
            }
        }

        //list all enums
        final JavaClass clazz = new JavaClass(DEFAULT_PACKAGE.concat(new Chars("GLC")));

        ite = root.getChildren().createIterator();
        final Set enum1 = new HashSet();
        final Set enum2 = new HashSet();
        while (ite.hasNext()) {
            final DomNode node = (DomNode) ite.next();
            if(!(node instanceof DomElement)) continue;
            final DomElement ele = (DomElement)node;
            if (GROUPS.equals(ele.getName().getLocalPart())) {

                final Iterator grpIte = ele.getChildren().createIterator();
                while (grpIte.hasNext()) {
                    final Node cdt = (Node) grpIte.next();
                    if (cdt instanceof DomElement && GROUP.equals(((DomElement)cdt).getName().getLocalPart())){
                        final DomElement group = (DomElement)cdt;
                        final Chars name = (Chars) group.getProperties().getValue(NAME);

                        final JavaProperty p = new JavaProperty();
                        p.id = name;
                        p.objclass = JavaClass.OBJECT_CLASS;

                        clazz.properties.add(p);
                    }

                }

            } else if (ENUMS.equals(ele.getName().getLocalPart())) {

//                final JavaClass clazz = toClass(ele);
//                context.elements.add(clazz.id, clazz);
//                JavaWriter writer =  new JavaWriter();
//                writer.setOutput(folder.resolve(clazz.getShortName()+".java"));
//                writer.write(clazz);
            }
        }

        final JavaWriter writer =  new JavaWriter();
        writer.setOutput(OUTPUT_PATH.resolve(clazz.getShortName().concat(new Chars(".java"))));
        writer.write(clazz);

    }

    private JavaClass toClass(final DomElement feature) {

        final Chars api = (Chars) feature.getProperties().getValue(API);
        final Chars number = (Chars) feature.getProperties().getValue(NUMBER);
        final Chars[] split = number.split('.');

        final JavaClass clazz = new JavaClass();
        clazz.id = DEFAULT_PACKAGE.concat(api.toUpperCase());
        clazz.id = clazz.id.concat(split[0]).concat(split[1]);
        clazz.setInterface(true);

        if (!split[1].equals(new Chars("0"))) {
            //extend previous version
            Chars parentId = DEFAULT_PACKAGE.concat(api.toUpperCase());
            parentId = parentId.concat(split[0]).concat( Int32.encode(Int32.decode(split[1])-1) );
            JavaClass parent = (JavaClass) context.elements.getValue(parentId);
            clazz.parents.add(parent);
        }

        final Iterator ite = feature.getChildren().createIterator();
        while (ite.hasNext()) {
            final DomNode node = (DomNode) ite.next();
            if(!(node instanceof DomElement)) continue;
            final DomElement require = (DomElement)node;
            if (REQUIRE.equals(require.getName().getLocalPart())) {
                final Iterator ite2 = require.getChildren().createIterator();
                commandLoop:
                while (ite2.hasNext()) {
                    final DomNode node2 = (DomNode) ite2.next();
                    if(!(node2 instanceof DomElement)) continue;
                    final DomElement command = (DomElement)node2;
                    if (COMMAND.equals(command.getName().getLocalPart())) {
                        final Chars commandName = (Chars) command.getProperties().getValue(NAME);
                        //some commands are duplicated
                        for(Iterator fite=clazz.functions.createIterator();fite.hasNext();){
                            if(((JavaFunction)fite.next()).id.equals(commandName)){
                                //command already exist
                                continue commandLoop;
                            }
                        }
                        clazz.functions.add(getCommand(commandName));
                    }
                }
            }
        }

        return clazz;
    }

    private JavaFunction getCommand(final Chars commandName) {

        final DomElement command = (DomElement) Nodes.SEARCHER.visit(root, new Predicate() {
            public Boolean evaluate(Object candidate) {
                if (!(candidate instanceof DomElement)) return false;
                final DomElement ele = (DomElement) candidate;
                if (!ele.getName().getLocalPart().equals(COMMAND)) return false;
                final DomElement proto = DomUtilities.getNodeForName(ele, PROTO);
                if (proto==null) return false;
                final DomElement tagname = DomUtilities.getNodeForName(proto, NAME);
                if (tagname==null) return false;
                return commandName.equals(tagname.getText());
            }
        });
        if(command==null){
            throw new InvalidArgumentException("Command not found for name :"+commandName);
        }

        final CharBuffer doc = new CharBuffer();

        final JavaFunction fct = new JavaFunction();
        fct.id = commandName;
        fct.metas.add(JavaMetas.ABSTRACT);

        final Set toRemove = new HashSet();

        final Iterator ite = command.getChildren().createIterator();
        while (ite.hasNext()) {
            Object obj = ite.next();
            if (!(obj instanceof DomElement)) continue;
            final DomElement ele = (DomElement) obj;
            if (PROTO.equals(ele.getName().getLocalPart())) {
                final DomElement returnType = DomUtilities.getNodeForName(ele, PTYPE);
                final Parameter out = new Parameter();
                if (returnType!=null) {
                    if (returnType.getText().startsWith(GLDEBUG)) {
                        //TODO how to handle this ?
                        final Parameter parameter = new Parameter();
                        parameter.type = JavaClass.OBJECT_CLASS;
                        fct.outParameters.add(parameter);
                    } else {
                        final science.unlicense.api.code.Class clazz = (science.unlicense.api.code.Class) GLPTYPES.getValue(returnType.getText());
                        out.type = clazz;
                        fct.outParameters.add(out);
                    }
                }
            } else if(PARAM.equals(ele.getName().getLocalPart())) {

                final DomElement returnType = DomUtilities.getNodeForName(ele, PTYPE);
                final DomElement name = DomUtilities.getNodeForName(ele, NAME);
                final Chars groupName = (Chars) ele.getProperties().getValue(GROUP);
                final Chars text = ele.getText();
                Chars lenName = (Chars) ele.getProperties().getValue(LEN);

                int compSize = 0;
                doc.append("@param ").append(name.getText());
                if (groupName!=null) doc.append(" ,value from enumeration group ").append(groupName);
                if (lenName!=null){
                    Chars[] parts = lenName.split('*');
                    Chars refName = null;
                    Integer size = null;
                    if(parts.length==1){
                        //a single property name
                        refName = parts[0];
                        try{
                            //single number case
                           size = Int32.decode(lenName);
                           refName = null;
                        }catch(Throwable ex){
                            if(refName.getFirstOccurence('(')>0) {
                                //case COMPSIZE(pname)
                                compSize = refName.split(',').length;
                                refName = null;
                                lenName = null;
                            }
                        }
                    }else if(parts.length==2){
                        //a property name * size
                        refName = parts[0];
                        size = Int32.decode(parts[1]);
                    }
                    if(refName!=null) {
                        toRemove.add(refName);
                        doc.append(" ,length ").append(lenName);
                    }
                }
                doc.append('\n');

                final Parameter in = new Parameter();
                if (returnType!=null) {
                    if (returnType.getText().startsWith(GLDEBUG)) {
                        //TODO how to handle this ?
                        in.type = JavaClass.OBJECT_CLASS;
                    } else {
                        final science.unlicense.api.code.Class clazz = (science.unlicense.api.code.Class) GLPTYPES.getValue(returnType.getText());
                        if(clazz==null) throw new RuntimeException("Unknowned type "+ returnType.getText());
                        in.type = clazz;
                    }
                } else {
                    if (compSize==0 && BUFFER_REF.equals(text)) {
                        in.type = JavaClass.PRIMITIVE_LONG;
                    } else if (BUFFER_REF.equals(text) || BUFFER_REF2.equals(text)) {
                        in.type = BUFFER_CLASS;
                    } else if(BUFFER_POINTER.equals(text)) {
                        in.type = BUFFERPOINTER_CLASS;
                    } else if(LEN_TEXT.equals(text)) {
                        in.type = JavaClass.PRIMITIVE_INT;
                    } else {
                        throw new RuntimeException("Unknown type "+text);
                    }
                }
                if(in.type==null) {
                    throw new RuntimeException("Undefined type "+ele);
                }
                if (lenName!=null || POINTER.equals(text)) {
                    //change type to a buffer
                    if(in.type == JavaClass.PRIMITIVE_BOOLEAN) {
                        in.type = BUFFER_BYTE;
                    }else if(in.type == JavaClass.PRIMITIVE_BYTE) {
                        in.type = BUFFER_BYTE;
                    }else if(in.type == JavaClass.PRIMITIVE_SHORT) {
                        in.type = BUFFER_SHORT;
                    }else if(in.type == JavaClass.PRIMITIVE_INT) {
                        in.type = BUFFER_INT;
                    }else if(in.type == JavaClass.PRIMITIVE_LONG) {
                        in.type = BUFFER_BYTE;
                    }else if(in.type == JavaClass.PRIMITIVE_FLOAT) {
                        in.type = BUFFER_FLOAT;
                    }else if(in.type == JavaClass.PRIMITIVE_DOUBLE) {
                        in.type = BUFFER_DOUBLE;
                    }else if(in.type == CHARARRAY_CLASS && lenName != null) {
                        in.type = BUFFER_BYTE;
                    }
                }

                in.id = name.getText();
                fct.inParameters.add(in);
            }
        }

        //remove various length parameters
        for(Iterator rite=toRemove.createIterator();rite.hasNext();){
            final Chars refName = (Chars) rite.next();
            boolean found = false;
            for (int i=0;i<fct.inParameters.getSize() && !found;i++) {
                final Parameter in = (Parameter) fct.inParameters.get(i);
                if (in.id.equals(refName)){
                    fct.inParameters.remove(i);
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new RuntimeException("Parameter "+refName+" not found in command "+commandName);
            }
        }


        fct.metas.add(new JavaDocumentation(doc.toChars().trimEnd()));
        return fct;
    }

}
