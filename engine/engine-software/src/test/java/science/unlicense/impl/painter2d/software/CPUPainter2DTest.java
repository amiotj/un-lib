
package science.unlicense.impl.painter2d.software;

import science.unlicense.impl.painter2d.software.CPUPainter2D;
import science.unlicense.api.painter2d.ImagePainter2D;

/**
 *
 * @author Johann Sorel
 */
public class CPUPainter2DTest extends science.unlicense.api.painter2d.ImagePainter2DTest{

    protected ImagePainter2D createPainter(int width, int height) {
        return new CPUPainter2D(width,height);
    }

}
