
package science.unlicense.impl.painter2d.software;

import science.unlicense.api.Orderable;
import science.unlicense.api.collection.ArraySequence;
import science.unlicense.api.collection.Collections;
import science.unlicense.api.collection.Sequence;
import science.unlicense.api.exception.InvalidArgumentException;
import science.unlicense.api.painter2d.Painter2D;
import science.unlicense.api.painter2d.AbstractPainter2D;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.path.FlattenPathIterator;
import science.unlicense.impl.geometry.path.RadialDistanceSimplifyPathIterator;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.color.ColorModel;
import science.unlicense.impl.image.process.paint.BlendOperator;
import science.unlicense.api.image.sample.RawModel;
import static science.unlicense.api.math.Maths.fract;
import static science.unlicense.api.math.Maths.ifract;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.api.painter2d.Rasterizer2D;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.font.ttf.TTFFontStore;

/**
 * Painter2D rendering on given image.
 * No gpu acceleration is done in this implementation.
 * This type of implementation is often called a Rasterizer.
 *
 * @author Johann Sorel
 */
public class CPUPainter2D extends AbstractPainter2D implements science.unlicense.api.painter2d.ImagePainter2D{

    private static final TTFFontStore FONT_STORE = new TTFFontStore();
    
    private static final Tuple EMPTY = new DefaultTuple(2);

    private Image image;
    private final ColorModel imageCM;
    private final RawModel imageSM;
    private final int width;
    private final int height;

    //variable for rasterizer
    private Rasterizer2D rasterizer;
    private Geometry2D currentGeometry;
    private final FlattenPathIterator flattenIterator = new FlattenPathIterator(null, null);
    private final RadialDistanceSimplifyPathIterator decimateIterator = new RadialDistanceSimplifyPathIterator(null, 0d);

    //TODO remove this, used for stroke, shoud rely on the rasterizer
    //serve to delimit the geometries area to reduce iterations over pixels
    private final BBox flagBbox = new BBox(2);
    private final Image flagImage;
    private final TupleBuffer fsm;
    private final byte[] flagSample = new byte[1];
    private final int[] flagCoordinate = new int[2];
    private final double[] decimationResolution = new double[]{1,1};
    private final TupleRW segmentStart = new DefaultTuple(2);
    private final TupleRW segmentEnd = new DefaultTuple(2);
    //path first coordinate, used when on a close segment
    private final TupleRW pathStart = new DefaultTuple(2);
    
    public CPUPainter2D(final int width, final int height) {
        this(Images.create(new Extent.Long(width, height), Images.IMAGE_TYPE_RGBA));
    }
    
    public CPUPainter2D(final Image image) {
        if(image == null){
            throw new NullPointerException("Image can not be null.");
        }
        if(image.getExtent().getDimension()!= 2){
            throw new InvalidArgumentException("Image must be 2D.");
        }
        this.image = image;
        this.imageCM = image.getColorModel();
        this.imageSM = image.getRawModel();
        this.width = (int) image.getExtent().getL(0);
        this.height = (int) image.getExtent().getL(1);
        //mask is one byte per pixel for alpha
        this.rasterizer = new PlainRasterizer(image.getExtent());
        
        //TODO remove this
        this.flagImage = Images.createCustomBand(new Extent.Long(width, height), 1, RawModel.TYPE_BYTE);
        this.fsm = flagImage.getRawModel().asTupleBuffer(flagImage);
    }

    private CPUPainter2D(CPUPainter2D tocopy, Affine2 trs){
        super(trs);
        this.image = tocopy.image;
        this.imageCM = tocopy.imageCM;
        this.imageSM = tocopy.imageSM;
        this.width = tocopy.width;
        this.height = tocopy.height;
        //mask is one byte per pixel for alpha
        this.rasterizer = new PlainRasterizer(image.getExtent());
        
        //TODO remove this
        this.flagImage = Images.createCustomBand(new Extent.Long(width, height), 1, RawModel.TYPE_BYTE);
        this.fsm = flagImage.getRawModel().asTupleBuffer(flagImage);
    }

    @Override
    public FontStore getFontStore() {
        return FONT_STORE;
    }
    
    public void fill(final Geometry2D geom) {
        if(paint == null || geom == null) return;
        this.currentGeometry = geom;

        //reset flag mask;
        flagBbox.set(currentGeometry.getBoundingBox());
        rasterizer.resetBBox(flagBbox);
        
        final Affine2 matrix = getFinalTransform();
        if(!matrix.isIdentity()){
            currentGeometry = new TransformedGeometry2D(currentGeometry, matrix);
        }
        rasterizer.rasterize(currentGeometry);
        fillPaint();
    }

    /**
     * Fill image with current paint using bitmask.
     */
    private void fillPaint(){
        final Affine2 matrix = getFinalTransform();
        Affine2 mt = matrix;
        if(!mt.isIdentity()){
            mt = (Affine2) matrix.invert();
        }
        paint.fill(image, rasterizer.getMask(), flagBbox, mt, alphaBlending);
    }

    public Painter2D derivate(Affine2 transform) {
        //we avoid stacking derivate painter, multiply matrices and
        //derivate original painter
        final Affine2 matrix = getFinalTransform();
        final Affine2 mul = (Affine2) transform.multiply(matrix);

        return new CPUPainter2D(this, mul);
    }

    public void paint(Image image, Affine2 transform) {
        final Affine2 matrix = getFinalTransform();
        if(!matrix.isIdentity()){
            transform = (Affine2) transform.multiply(matrix);
        }
        new BlendOperator().execute(this.image, image, alphaBlending, transform);
    }

    public void dispose() {
        
    }

    public void setSize(Extent.Long size) {
        this.image = Images.create(size,Images.IMAGE_TYPE_RGBA);
        this.rasterizer = new PlainRasterizer(this.image.getExtent());
    }

    public Extent.Long getSize() {
        return image.getExtent().copy();
    }

    public Image getImage() {
        return image;
    }

    ////////////////////////////////////////////////////////////////////////////
    // TODO : REPLACE BY RASTERIZER ////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    
    public void stroke(Geometry2D geom) {
        if(brush == null || geom == null || paint == null) return;

        final Affine2 matrix = getFinalTransform();
        if(!matrix.isIdentity()){
            geom = new TransformedGeometry2D(geom, matrix);
        }

        double width = brush.getMaxWidth();

        flagBbox.set(geom.getBoundingBox());
        flagBbox.setRange(0, flagBbox.getMin(0)-width, flagBbox.getMax(0)+width);
        flagBbox.setRange(1, flagBbox.getMin(1)-width, flagBbox.getMax(1)+width);
        resetBitMask();

        //simplify iterator, we only want line segments
        flattenIterator.reset(geom.createPathIterator(), decimationResolution);
        decimateIterator.reset(flattenIterator, decimationResolution[0]);

        if(antialiazing){
            strokeAA();
        }else{
            strokeNoAA(geom);
        }

        fillPaint();
    }

    public void strokeNoAA(Geometry2D geom){
        brush.bitMask(geom, decimationResolution, flagImage);
    }

    public void strokeAA(){
        while(decimateIterator.next()){
            final int stepType = decimateIterator.getType();
            draw:
            if(PathIterator.TYPE_MOVE_TO == stepType){
                decimateIterator.getPosition(segmentEnd);
                //Do nothing, just update coordinates
                decimateIterator.getPosition(pathStart);
            }else if(PathIterator.TYPE_LINE_TO == stepType || PathIterator.TYPE_CLOSE == stepType){
                if(PathIterator.TYPE_CLOSE == stepType){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                }else{
                    decimateIterator.getPosition(segmentEnd);
                }

                // Using Xiaolin Wu line algorithm
                float startX = (float)segmentStart.getX();
                float startY = (float)segmentStart.getY();
                float endX   = (float)segmentEnd.getX();
                float endY   = (float)segmentEnd.getY();

                final boolean steep = Math.abs(endY - startY) > Math.abs(endX - startX);

                if(steep){
                    float temp = startX;
                    startX = startY;
                    startY = temp;
                    temp = endX;
                    endX = endY;
                    endY = temp;
                }
                if(startX > endX){
                    float temp = startX;
                    startX = endX;
                    endX = temp;
                    temp = startY;
                    startY = endY;
                    endY = temp;
                }

                final float dx = endX - startX;
                final float dy = endY - startY;
                final float gradient = dy / dx;

                // handle first endpoint
                int xend = Math.round(startX);
                float yend = startY + gradient * (xend - startX);
                float xgap = ifract(startX + 0.5f);
                int xpxl1 = xend;   //this will be used in the main loop
                int ypxl1 = (int)yend;
                if(steep){
                    setFlagPixel(ypxl1,   xpxl1, ifract(yend) * xgap);
                    setFlagPixel(ypxl1+1, xpxl1,  fract(yend) * xgap);
                }else{
                    setFlagPixel(xpxl1, ypxl1  , ifract(yend) * xgap);
                    setFlagPixel(xpxl1, ypxl1+1,  fract(yend) * xgap);
                 }
                float intery = yend + gradient; // first y-intersection for the main loop

                // handle second endpoint
                xend = Math.round(endX);
                yend = endY + gradient * (xend - endX);
                xgap = fract(endX + 0.5f);
                int xpxl2 = xend; //this will be used in the main loop
                int ypxl2 = (int)yend;
                if(steep){
                    setFlagPixel(ypxl2  , xpxl2, ifract(yend) * xgap);
                    setFlagPixel(ypxl2+1, xpxl2,  fract(yend) * xgap);
                }else{
                    setFlagPixel(xpxl2, ypxl2,  ifract(yend) * xgap);
                    setFlagPixel(xpxl2, ypxl2+1, fract(yend) * xgap);
                }

                // main loop
                for(int x=xpxl1 + 1,n=xpxl2-1 ;x<n;x++){
                     if(steep){
                        setFlagPixel((int)intery  , x, ifract(intery));
                        setFlagPixel((int)intery+1, x,  fract(intery));
                     }else{
                        setFlagPixel(x, (int)intery,  ifract(intery));
                        setFlagPixel(x, (int)intery+1, fract(intery));
                     }
                    intery += gradient;
                }

            }else{
                throw new RuntimeException("Unexpected step type : "+stepType);
            }
            segmentStart.set(segmentEnd);
        }
    }

    private void resetBitMask(){
        flagSample[0] = 0;
        fsm.setTuple(flagBbox, flagSample);
        flagCoordinate[0] = 0;
        flagCoordinate[1] = 0;
        segmentStart.set(EMPTY);
        segmentEnd.set(EMPTY);
        pathStart.set(EMPTY);
    }
    
    private void setFlagPixel(int x, int y, float src){
        if(y<0 || y>=height) return;
        if(x<0 || x>=width) return;

        flagCoordinate[0] = x;
        flagCoordinate[1] = y;
        fsm.getTupleByte(flagCoordinate, flagSample);

        float dest = (flagSample[0] & 0xff) / 255f;
        src += dest * (1f - src);
        flagSample[0] = (byte)(src*255);
        fsm.setTuple(flagCoordinate, flagSample);
    }

    private void flipLine(int x, int y, float firstRatio, float lineRatio){
        if(y<0 || y>=height) return;
        if(x<0)x=0;
        int maxx = (int)Math.round(flagBbox.getMax(0));
        if(maxx>width) maxx = width;
        if(x>=maxx) return;
        flagCoordinate[1] = y;

        //fill the first pixel using given ratio
        flagCoordinate[0] = x;
        fsm.getTupleByte(flagCoordinate, flagSample);
        flagSample[0] = (byte)((255 - (flagSample[0] & 0xff))*lineRatio*(1f-firstRatio));
        fsm.setTuple(flagCoordinate, flagSample);

        x++;
        for(;x<maxx;x++){
            flagCoordinate[0] = x;
            fsm.getTupleByte(flagCoordinate, flagSample);
            flagSample[0] = (byte)((255 - (flagSample[0] & 0xff))*lineRatio);
            fsm.setTuple(flagCoordinate, flagSample);
        }
    }
    
    private Edge[] buildEdges(){
        final Affine2 matrix = getFinalTransform();
        if(!matrix.isIdentity()){
            currentGeometry = new TransformedGeometry2D(currentGeometry, matrix);
        }

        flattenIterator.reset(currentGeometry.createPathIterator(), decimationResolution);
        decimateIterator.reset(flattenIterator, decimationResolution[0]);

        final Sequence seq = new ArraySequence();

        while(decimateIterator.next()){
            final int stepType = decimateIterator.getType();
            draw:
            if(PathIterator.TYPE_MOVE_TO == stepType){
                decimateIterator.getPosition(segmentEnd);
                //Do nothing, just update coordinates
                decimateIterator.getPosition(pathStart);
            }else if(PathIterator.TYPE_LINE_TO == stepType || PathIterator.TYPE_CLOSE == stepType){
                if(PathIterator.TYPE_CLOSE == stepType){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                }else{
                    decimateIterator.getPosition(segmentEnd);
                }

                final Edge edge = new Edge();
                edge.sx = (float)segmentStart.getX();
                edge.sy = (float)segmentStart.getY();
                edge.ex = (float)segmentEnd.getX();
                edge.ey = (float)segmentEnd.getY();
                //horizontal line : skip it
                if(edge.sy == edge.ey) break draw;

                //flip points, we want y increasing
                if(edge.sy > edge.ey){
                    float temp = edge.sy;
                    edge.sy = edge.ey;
                    edge.ey = temp;
                    temp = edge.sx;
                    edge.sx = edge.ex;
                    edge.ex = temp;
                }

                final float dx = (float)(edge.ex - edge.sx);
                final float dy = (float)(edge.ey - edge.sy);
                edge.a = dy / dx;
                edge.b = edge.ey - (edge.a * edge.ex);

                seq.add(edge);

            }else{
                throw new RuntimeException("Unexpected step type : "+stepType);
            }
            segmentStart.set(segmentEnd);
        }

        final Edge[] edges = new Edge[seq.getSize()];
        Collections.copy(seq, edges, 0);
        return edges;
    }

    
    class Edge implements Orderable{

        //start and end points
        public float sx;
        public float sy;
        public float ex;
        public float ey;

        // equation values : ax + b
        public float a;
        public float b;

        //current work
        public float startx = Float.NaN;
        public float lineRatio = 0f;

        @Override
        public int order(Object other) {
            final Edge o = (Edge) other;

            if(startx == Float.NaN){
                //first sort call : sort by start y
                if(ey < o.ey){
                    return -1;
                }else if(ey > o.ey){
                    return +1;
                }
            }else{
                //scanline calls, sort by start x
                if(startx < o.startx){
                    return -1;
                }else if(startx > o.startx){
                    return +1;
                }
            }

            return 0;
        }

    }

}
