
package science.unlicense.impl.painter2d.software;

import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.painter2d.AbstractPainter2D;
import science.unlicense.api.painter2d.FontStore;
import science.unlicense.api.painter2d.ImagePainter2D;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.geometry.s2d.TransformedGeometry2D;
import science.unlicense.impl.image.process.paint.BlendOperator;
import science.unlicense.impl.math.Affine2;
import science.unlicense.impl.math.Vector2;
import science.unlicense.impl.font.ttf.TTFFontStore;

/**
 * Draft new software painter.
 * 
 * @author Johann Sorel
 */
public class SoftwarePainter2D extends AbstractPainter2D implements ImagePainter2D {

    private static final TTFFontStore FONT_STORE = new TTFFontStore();
    
    private Image image;
    private Image mask;
    private TupleBuffer masksm;
    private final BBox maskBbox = new BBox(2);
    private final byte[] maskSample = new byte[]{(byte)255};
    private final int[] maskCoordinate = new int[2];

    private final Vector2 segmentStart = new Vector2();
    private final Vector2 segmentEnd = new Vector2();
    //path first coordinate, used when on a close segment
    private final Vector2 pathStart = new Vector2();

    private final Vector2 corner = new Vector2();
    private final Vector2 split = new Vector2();
    private Vector2 temp;

    public SoftwarePainter2D(Image image) {
        this.image = image;
        this.mask = Images.createCustomBand(image.getExtent(), 1, RawModel.TYPE_BYTE);
        this.masksm = mask.getRawModel().asTupleBuffer(mask);
    }

    @Override
    public FontStore getFontStore() {
        return FONT_STORE;
    }
    
    @Override
    public void setSize(Extent.Long size) {
        image = Images.create(image, size);
        mask = Images.createCustomBand(image.getExtent(), 1, RawModel.TYPE_BYTE);
        masksm = mask.getRawModel().asTupleBuffer(mask);
    }

    @Override
    public Extent.Long getSize() {
        return image.getExtent();
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void fill(Geometry2D geom) {

        final Affine2 matrix = getFinalTransform();
        if(!matrix.isIdentity()){
            geom = new TransformedGeometry2D(geom, matrix);
        }

        final PathIterator ite = geom.createPathIterator();
        while (ite.next()) {
            final int type = ite.getType();

            if (PathIterator.TYPE_MOVE_TO == type) {
                ite.getPosition(pathStart);
                ite.getPosition(segmentStart);
                ite.getPosition(segmentEnd);
                ite.getPosition(corner);

            } else if (PathIterator.TYPE_LINE_TO == type) {
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_CUBIC == type) {
                //TODO
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_QUADRATIC == type) {
                //TODO
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_ARC == type) {
                //TODO
                segmentStart.set(segmentEnd);
                ite.getPosition(segmentEnd);
                fillTriangle(corner, segmentStart, segmentEnd);

            } else if (PathIterator.TYPE_CLOSE == type) {
                segmentEnd.set(segmentStart);
                segmentStart.set(pathStart);
                fillTriangle(corner, segmentStart, segmentEnd);

            }
        }

        maskBbox.set(geom.getBoundingBox());
        fillPaint();
        //reset mask
        maskSample[0] = 0;
        masksm.setTuple(maskBbox, maskSample);
        maskSample[0] = (byte)255;

    }

    /**
     * Fill image with current paint using bitmask.
     */
    private void fillPaint(){
        final Affine2 matrix = getFinalTransform();
        Affine2 mt = matrix;
        if(!mt.isIdentity()){
            mt = (Affine2) matrix.invert();
        }
        paint.fill(image, mask, maskBbox, mt, alphaBlending);
    }

    @Override
    public void stroke(Geometry2D geom) {
        
    }

    private void fillTriangle(Vector2 v1, Vector2 v2, Vector2 v3) {

        //sort by Y
        if(v1.y > v2.y) {
            temp = v2;
            v2 = v1;
            v1 = temp;
        }
        if(v2.y > v3.y) {
            temp = v3;
            v3 = v2;
            v2 = temp;
        }
        if(v1.y > v2.y) {
            temp = v2;
            v2 = v1;
            v1 = temp;
        }

        if (v2.y == v3.y) {
            fillUpwardTriangle(v1, v2, v3);
        } else if (v1.y == v2.y) {
            fillDownwardTriangle(v1, v2, v3);
        } else {
            //split in two triangles
            split.x = v1.x + (v2.y-v1.y) / ((v3.y-v1.y)) * (v3.x-v1.x);
            split.y = v2.y;
            fillUpwardTriangle(v1, v2, split);
            fillDownwardTriangle(v2, split, v3);
        }
        
    }

    private void fillUpwardTriangle(Vector2 v1, Vector2 v2, Vector2 v3) {
        double slope1 = (v2.x - v1.x) / (v2.y - v1.y);
        double slope2 = (v3.x - v1.x) / (v3.y - v1.y);
        if (slope1>slope2) {
            double d = slope1;
            slope1 = slope2;
            slope2 = d;
        }

        double start = v1.x;
        double end = v1.x;

        for (double y = v1.y; y <= v2.y; y++) {
            maskBbox.setRange(0, start, end);
            maskBbox.setRange(1, y, y+1);
            masksm.setTuple(maskBbox, maskSample);
            start += slope1;
            end += slope2;
        }
    }

    private void fillDownwardTriangle(Vector2 v1, Vector2 v2, Vector2 v3) {
        double slope1 = (v3.x - v1.x) / (v3.y - v1.y);
        double slope2 = (v3.x - v2.x) / (v3.y - v2.y);
        if (slope1 < slope2) {
            double d = slope1;
            slope1 = slope2;
            slope2 = d;
        }

        double start = v3.x;
        double end = v3.x;

        for (double y = v3.y; y > v1.y; y--) {
            maskBbox.setRange(0, start, end);
            maskBbox.setRange(1, y, y+1);
            masksm.setTuple(maskBbox, maskSample);
            start -= slope1;
            end -= slope2;
        }
    }

    @Override
    public void paint(Image image, Affine2 transform) {
        final Affine2 matrix = getFinalTransform();
        if (!matrix.isIdentity()) {
            transform = (Affine2) transform.multiply(matrix);
        }
        new BlendOperator().execute(this.image, image, alphaBlending, transform);
    }

    @Override
    public void dispose() {
    }

}
