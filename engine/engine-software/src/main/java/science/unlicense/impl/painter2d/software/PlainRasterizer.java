
package science.unlicense.impl.painter2d.software;

import science.unlicense.api.array.Arrays;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.math.Maths;

/**
 *
 * @author Johann Sorel
 */
public class PlainRasterizer extends AbstractRasterizer {

    public PlainRasterizer(Extent.Long extent) {
        super(extent);
    }

    @Override
    protected void process(ScanLine[] lines) {
        final int[] coord = new int[2];
        
        for(int i=0;i<lines.length;i++){
            if(lines[i]==null) continue;
            final int cellY = (int) Math.floor(lines[i].y0);
            if(cellY<0 || cellY>height) continue;

            //distance between y0 and y1 is maximum 1
            final double ratioY = lines[i].y1-lines[i].y0;

            for(int k=0;k<lines[i].ranges.length-1;k+=2){
                int startCellX = (int)lines[i].ranges[k];
                int endCellX = (int)(lines[i].ranges[k+1] +0.5);
                if(startCellX<0) startCellX = 0;
                if(endCellX>width) endCellX = width;

                for(int cellX=startCellX;cellX<endCellX;cellX++){
                    final double ratioX;
                    if(cellX==startCellX || cellX==endCellX-1){
                        ratioX = overlaps(lines[i].ranges[k], lines[i].ranges[k+1], cellX, cellX+1);
                    }else{
                        ratioX = 1.0;
                    }
                    
                    coord[0] = cellX;
                    coord[1] = cellY;
                    fsm.getTuple(coord, flagSample);
                    flagSample[0] =  (byte)((flagSample[0] & 0xFF) + (int)((ratioX*ratioY) * 255));
                    fsm.setTuple(coord, flagSample);
                }
            }
        }

    }

    private static double overlaps(double amin, double amax, double bmin, double bmax){
        if(amin>=bmax || amax<=bmin){
            //they don't overlaps
            return 0.0;
        }
        final double min = Maths.max(amin, bmin);
        final double max = Maths.min(amax, bmax);
        return max-min;

    }

}
