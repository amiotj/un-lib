
package science.unlicense.impl.painter2d.software;

import science.unlicense.api.Orderable;
import science.unlicense.api.array.Arrays;
import science.unlicense.api.collection.ArrayOrderedSet;
import science.unlicense.api.collection.OrderedSet;
import science.unlicense.api.collection.primitive.DoubleSet;
import science.unlicense.api.geometry.BBox;
import science.unlicense.api.geometry.Extent;
import science.unlicense.api.geometry.TupleBuffer;
import science.unlicense.api.image.Image;
import science.unlicense.api.image.Images;
import science.unlicense.api.image.sample.RawModel;
import science.unlicense.api.math.Tuple;
import science.unlicense.api.math.TupleRW;
import science.unlicense.api.painter2d.Rasterizer2D;
import science.unlicense.impl.geometry.operation.Distance;
import science.unlicense.impl.geometry.path.FlattenPathIterator;
import science.unlicense.impl.geometry.path.PathIterator;
import science.unlicense.impl.geometry.s2d.Geometry2D;
import science.unlicense.impl.math.DefaultTuple;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractRasterizer implements Rasterizer2D {
    
    private static final double EPSILON = 0.000000001;
    private static final double[] RESOLUTION = new double[]{0.5,0.5};
        
    
    private final FlattenPathIterator flattenIterator = new FlattenPathIterator(null, null);
    private final TupleRW segmentStart = new DefaultTuple(2);
    private final TupleRW segmentEnd = new DefaultTuple(2);
    //path first coordinate, used when on a close segment
    private final TupleRW pathStart = new DefaultTuple(2);

    private boolean horizonalFitting = true;
    private boolean enhanceContrast = true;


    protected final int width;
    protected final int height;
    protected final Image flagImage;
    protected final TupleBuffer fsm;
    protected final byte[] flagSample = new byte[1];
    
    public AbstractRasterizer(Extent.Long extent){
        this.flagImage = Images.createCustomBand(extent, 1, RawModel.TYPE_BYTE);
        this.fsm = flagImage.getRawModel().asTupleBuffer(flagImage);
        this.width = (int) extent.getL(0);
        this.height = (int) extent.getL(1);
    }
    
    public Extent.Long getExtent(){
        return flagImage.getExtent();
    }

    @Override
    public Image getMask() {
        return flagImage;
    }
    
    public void resetBBox(BBox flagBbox) {
        flagSample[0] = 0;
        fsm.setTuple(flagBbox, flagSample);
        segmentStart.setToZero();
        segmentEnd.setToZero();
        pathStart.setToZero();
    }
    
    public Image rasterize(Geometry2D currentGeometry) {

        final ScanLines edges = new ScanLines();

        //get all segments
        flattenIterator.reset(currentGeometry.createPathIterator(), RESOLUTION);
        
        //we collect all y values, they we be used to cut scanlines
        final DoubleSet cutSet = new DoubleSet();
        
        double minx = Double.MAX_VALUE;
        double maxx = -Double.MAX_VALUE;
        while(flattenIterator.next()){
            final int stepType = flattenIterator.getType();
            draw:
            if(PathIterator.TYPE_MOVE_TO == stepType){
                flattenIterator.getPosition(segmentEnd);
                //Do nothing, just update coordinates
                flattenIterator.getPosition(pathStart);
            }else if(PathIterator.TYPE_LINE_TO == stepType || PathIterator.TYPE_CLOSE == stepType){
                if(PathIterator.TYPE_CLOSE == stepType){
                    //end segment if the first position
                    segmentEnd.set(pathStart);
                }else{
                    flattenIterator.getPosition(segmentEnd);
                }

                final Edge edge = new Edge(
                        segmentStart.getX(),segmentStart.getY(),
                        segmentEnd.getX(),segmentEnd.getY());
                
                //horizontal line : skip it
                if(edge.y0 == edge.y1) break draw;
                //flip points, we want y increasing
                if(edge.y0 > edge.y1){
                    double temp = edge.y0;
                    edge.y0 = edge.y1;
                    edge.y1 = temp;
                    temp = edge.x0;
                    edge.x0 = edge.x1;
                    edge.x1 = temp;
                }

                cutSet.add(edge.y0);
                cutSet.add(edge.y1);
                if(edge.x0<minx) minx = edge.x0;
                if(edge.x1<minx) minx = edge.x1;
                if(edge.x0>maxx) maxx = edge.x0;
                if(edge.x1>maxx) maxx = edge.x1;
                
                edges.segments.add(edge);

            }else{
                throw new RuntimeException("Unexpected step type : "+stepType);
            }
            segmentStart.set(segmentEnd);
        }

        //now cut on each segment end and on all integer values
        double min = Math.ceil(cutSet.getFirst());
        double max = Math.floor(cutSet.getLast());
        for(;min<=max;min++) cutSet.add(min);
        final ScanLine[] scanlines = edges.cut(cutSet.toArrayDouble(), minx, maxx);

        if((max-min) < 20 && horizonalFitting){
            gridFittingH(scanlines);
        }
        if((max-min) < 14 && enhanceContrast){
            enhanceContrast(scanlines);
        }
        
        process(scanlines);
        return flagImage;
    }

    /**
     * Try to fit horizontaly scanlines.
     *
     * This algorithm found the center of small spans and calculate and average
     * displacement on X of at most 0.5 pixel.
     *
     * @param lines
     */
    private void gridFittingH(ScanLine[] lines){
        double nbH = 0;
        double sumHCenter = 0.0;
        for(ScanLine sl : lines){
            if(sl==null) continue;
            for(int i=0;i<sl.ranges.length-1;i+=2){
                final double span = sl.ranges[i+1] - sl.ranges[i];
                if(span<1.5){
                    double ratio = sl.y1-sl.y0;
                    nbH += ratio;
                    final double center = (sl.ranges[i] + sl.ranges[i+1])/2.0;
                    sumHCenter += (center - ((int)center))*ratio ;
                }
            }
        }

        if(nbH==0) return;

        //sumHcenter is in range [0 - 1]
        sumHCenter /= nbH;
        final double trsX;
        if(sumHCenter<0.5){
            trsX = 0.5-sumHCenter;
        }else{
            trsX = -(sumHCenter-0.5);
        }

        //we adjust all scanline by the translation
        for(ScanLine sl : lines){
            if(sl==null) continue;
            for(int i=0;i<sl.ranges.length;i++){
                sl.ranges[i] += trsX;
            }
        }

    }

    /**
     * Draft, try to improve contrast for small geometries.
     *
     * @param lines
     */
    private void enhanceContrast(ScanLine[] lines){
        for(ScanLine sl : lines){
            if(sl==null) continue;
            for(int i=0;i<sl.ranges.length-1;i+=2){
                final double span = sl.ranges[i+1] - sl.ranges[i];
                if(span>0.2 && span<1.0){
                    double halfSpan = span/2.0;
//                    halfSpan = Math.max(0.5, halfSpan*halfSpan);
                    double center = (sl.ranges[i] + sl.ranges[i+1])/2.0;
//                    int icenter = (int)center;
//                    int istart = (int)sl.ranges[i];
//                    final double decimalCenter = (center - icenter);
//                    final double decimalStart = (sl.ranges[i] - istart);
//                    if(istart!=icenter && decimalStart < 0.80){
//                        center = istart +0.5;
//                    }else if(decimalCenter>0.25 && decimalCenter <0.75){
//                        //near center, align it
//                        center = icenter +0.5;
//                    }
                    sl.ranges[i] = center-0.5;
                    sl.ranges[i+1] = center+0.5;
                }
            }
        }

    }


    protected abstract void process(ScanLine[] lines);
    
    
    public static final class Edge implements Orderable{
        public double x0;
        public double y0;
        public double x1;
        public double y1;

        public Edge(double x0, double y0, double x1, double y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }

        public int order(Object other) {
            double d = ((Edge)other).y0;
            if(d<y0) return +1;
            if(d>y0) return -1;
            return 0;
        }
    }
    
    public static final class ScanLine{
        public final double y0;
        public final double y1;
        public double[] ranges = Arrays.ARRAY_DOUBLE_EMPTY;

        public ScanLine(double y0, double y1) {
            this.y0 = y0;
            this.y1 = y1;
        }
        
    }
    
    private static final class ScanLines{
        public final OrderedSet segments = new ArrayOrderedSet();
        
        /**
         * Cut all scanlines.
         * @param cuts 
         * @param minx 
         * @param maxx 
         */
        private ScanLine[] cut(double[] cuts, double minx, double maxx){
            final double[] cutYminStart = new double[2];
            final double[] cutYMinEnd = new double[2];
            final double[] cutYmaxStart = new double[2];
            final double[] cutYMaxEnd = new double[2];
            final double[] buffer1 = new double[2];
            final double[] buffer2 = new double[2];
            final double[] ratio = new double[2];
            final double[] seg0 = new double[2];
            final double[] seg1 = new double[2];
            cutYminStart[0] = minx;
            cutYMinEnd[0] = maxx;
            cutYmaxStart[0] = minx;
            cutYMaxEnd[0] = maxx;
            
            final ScanLine[] lines = new ScanLine[cuts.length];
            final Object[] array = segments.toArray();
            int startIndex=0;
            
            for(int i=0;i<cuts.length-1;i++){
                lines[i] = new ScanLine(cuts[i], cuts[i+1]);
                cutYminStart[1] = lines[i].y0;
                cutYMinEnd[1] = lines[i].y0;
                cutYmaxStart[1] = lines[i].y1;
                cutYMaxEnd[1] = lines[i].y1;
                
                //cut all segments
                for(int k=startIndex;k<array.length;k++){
                    final Edge e = (Edge) array[k];
                    
                    if(e.y1<=lines[i].y0) continue; //this segment ends before intersecting
                    if(e.y0>=lines[i].y1) break; //all next segments are after this line
                    
                    //calculate intersection
                    seg0[0]=e.x0; seg0[1]=e.y0;
                    seg1[0]=e.x1; seg1[1]=e.y1;
                    Distance.distanceSquare(
                            cutYminStart, cutYMinEnd, buffer1, 
                            seg0, seg1, buffer2, ratio, EPSILON);
                    double cutx0 = buffer1[0];
                    Distance.distanceSquare(
                            cutYmaxStart, cutYMaxEnd, buffer1,
                            seg0, seg1, buffer2, ratio, EPSILON);
                    double cutx1 = buffer1[0];

                    lines[i].ranges = Arrays.insert(lines[i].ranges, lines[i].ranges.length, (cutx0+cutx1)/2.0);
                }

                //sort ranges
                Arrays.sort(lines[i].ranges);
            }

            return lines;
        }
    }
    
}
    
