
package science.unlicense.impl.gpu.jogamp.openal;

/**
 * A spot skeaper sends sounds in a specify direction and a given angle.
 * Direction is given by the node rotation.
 *
 * @author Johann Sorel
 */
public class SpotSpeaker extends Speaker{

    private float innerAngle = 15f;
    private float outterGain = 1f;
    private float outterAngle = 30f;

    /**
     * Set spot inner angle.
     * @param innerAngle
     */
    public void setInnerAngle(float innerAngle) {
        this.innerAngle = innerAngle;
    }

    /**
     * Get spot inner angle.
     * @return float
     */
    public float getInnerAngle() {
        return innerAngle;
    }

    /**
     * Set spot outter angle.
     * @param outterAngle
     */
    public void setOutterAngle(float outterAngle) {
        this.outterAngle = outterAngle;
    }

    /**
     * Get spot outter angle.
     * @return float
     */
    public float getOutterAngle() {
        return outterAngle;
    }

    /**
     * Set the audio outter gain.
     * @param outterGain
     */
    public void setOutterGain(float outterGain) {
        this.outterGain = outterGain;
    }

    /**
     * Get the audio gain.
     * @return float
     */
    public float getOutterGain() {
        return outterGain;
    }

}
