
package science.unlicense.impl.gpu.jogamp.opengl;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.event.WindowListener;
import com.jogamp.newt.event.WindowUpdateEvent;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.character.Chars;
import science.unlicense.api.event.AbstractEventSource;
import science.unlicense.api.event.Event;
import science.unlicense.api.geometry.Extent;
import science.unlicense.impl.math.DefaultTuple;
import science.unlicense.api.math.Tuple;
import science.unlicense.impl.math.Vector;
import science.unlicense.api.desktop.Frame;
import science.unlicense.api.desktop.FrameMessage;
import science.unlicense.api.desktop.KeyMessage;
import science.unlicense.api.desktop.MouseMessage;
import science.unlicense.api.desktop.cursor.Cursor;
import science.unlicense.api.event.EventManager;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.gpu.opengl.GLFrame;
import science.unlicense.api.gpu.opengl.GLSource;

/**
 * OpenGL Window.
 * Based on JOGL 2 Newt framework.
 *
 * @author Johann Sorel
 */
class JOGLFrame extends AbstractEventSource implements GLFrame, WindowListener, KeyListener, MouseListener{

    //OpenGL various context informations
    private final GLCapabilities glCapabilities;
    public final GLWindow glWindow;
    private final FPSAnimator glAnimator;
    private final JOGLSource source = new JOGLSource();
    private boolean disposed = false;
    
    JOGLFrame() {
       this(false);
    }

    JOGLFrame(boolean opaque) {
        this(opaque,GLProfile.getMaximum(true));
    }
    
    JOGLFrame(boolean opaque, GLProfile glProfile) {
        glCapabilities = new GLCapabilities(glProfile);
        glCapabilities.setBackgroundOpaque(opaque);
        glWindow = GLWindow.create(glCapabilities);
        source.setDrawable(glWindow);
        glAnimator = new FPSAnimator(glWindow, 30, false);
        glAnimator.start();
        glAnimator.pause();

        //attach listeners
        glWindow.addMouseListener(this);
        glWindow.addKeyListener(this);
        glWindow.addWindowListener(this);
    }

    public GLSource getSource() {
        return source;
    }

    public CharArray getTitle() {
        return new Chars(glWindow.getTitle());
    }

    public void setTitle(CharArray title) {
        glWindow.setTitle(title.toString());
    }

    public void setSize(int width, int height){
        //TODO some windows systems like xfce lose the frame position when resizing, bug ?
        //same position
        glWindow.setSize(width, height);
        //setOnScreenLocation(framePos);
    }

    public Extent getSize(){
        return new Extent.Double(
                glWindow.getWidth(),
                glWindow.getHeight());
    }

    public int getFps(){
        return glAnimator.getFPS();
    }
    
    public void setFps(int fps){
        glAnimator.setFPS(fps);
    }
    
    public void setVisible(boolean visible){
        glWindow.setVisible(visible);
        if(visible){
            glAnimator.resume();
        }else{
            glAnimator.pause();
        }
    }

    public boolean isVisible(){
        return glWindow.isVisible();
    }

    public int getState() {
        if(glWindow.isFullscreen()) return Frame.STATE_FULLSCREEN;
        if(glWindow.isMaximizedHorz()) return Frame.STATE_MAXIMIZED;
        if(glWindow.isVisible()) return Frame.STATE_MAXIMIZED;
        //TODO detect others
        return Frame.STATE_NORMAL;
    }

    public void setState(int state) {
        switch(state){
            case Frame.STATE_FULLSCREEN : glWindow.setFullscreen(true); break;
            case Frame.STATE_MINIMIZED : break;
            case Frame.STATE_MAXIMIZED : glWindow.setMaximized(true,true); break;
            case Frame.STATE_NORMAL : glWindow.setFullscreen(false); glWindow.setMaximized(false, false); break;
        }
    }

    public void setMaximizable(boolean maximizable) {
        //TODO
    }

    public boolean isMaximizable() {
        return true;
    }

    public void setMinimizable(boolean minimizable) {
        //TODO
    }

    public boolean isMinimizable() {
        return true;
    }

    public void setClosable(boolean closable) {
        //TODO
    }

    public boolean isClosable() {
        return true;
    }
    
    public void setOnScreenLocation(Tuple location) {
        glWindow.setTopLevelPosition((int)location.getX(), (int)location.getY());
    }

    public Vector getOnScreenLocation() {
        //this call is expensive
        //com.jogamp.nativewindow.util.Point pt = glWindow.getLocationOnScreen(new com.jogamp.nativewindow.util.Point());
        //return new DefaultTuple(pt.getX(), pt.getY());
        
        //use the window cached value instead
        return new Vector(glWindow.getX(), glWindow.getY());
    }
    
    /**
     * Needs a restart.
     * futur evolution in JOGL 2 ?
     * @param opaque
     */
    public void setOpaque(boolean opaque){
        glCapabilities.setBackgroundOpaque(opaque);
    }

    public boolean isOpaque(){
        return glCapabilities.isBackgroundOpaque();
    }

    // WINDOW EVENTS ///////////////////////////////////////////////////////////

    public void windowRepaint(WindowUpdateEvent e) {
    }
    public void windowResized(WindowEvent e) {
        sendPropertyEvent(this, Frame.PROP_SIZE, null, getSize());
    }
    public void windowMoved(WindowEvent e) {
        sendPropertyEvent(this, Frame.PROP_ON_SCREEN_LOCATION, null, getOnScreenLocation());
    }
    public void windowGainedFocus(WindowEvent e) {
    }
    public void windowLostFocus(WindowEvent e) {
    }
    public void windowDestroyNotify(WindowEvent e) {
        sendPropertyEvent(this, Frame.PROP_VISIBLE, null, isVisible());
        // stop running ..
        glAnimator.stop();
        final EventManager manager = getEventManager(false);
        if(manager!=null){
            manager.sendEvent(new Event(this, new FrameMessage(FrameMessage.TYPE_PREDISPOSE)));
        }
    }
    public void windowDestroyed(WindowEvent e) {
        final EventManager manager = getEventManager(false);
        if(manager!=null){
            manager.sendEvent(new Event(this, new FrameMessage(FrameMessage.TYPE_DISPOSED)));
            sendPropertyEvent(this, PROP_DISPOSED, false, true);
        }
    }

    // KEYBOARD EVENTS /////////////////////////////////////////////////////////

    public void keyPressed(KeyEvent e) {
        processEvent(null, toUNEvent(e));
    }
    public void keyReleased(KeyEvent e) {
        processEvent(null, toUNEvent(e));
    }

    // MOUSE EVENTS ////////////////////////////////////////////////////////////

    public void mouseClicked(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mouseEntered(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mouseExited(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mousePressed(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mouseReleased(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mouseMoved(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mouseDragged(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }
    public void mouseWheelMoved(MouseEvent e) {
        processEvent(toUNEvent(e), null);
    }

    private MouseMessage toUNEvent(MouseEvent me){
        final int type = me.getEventType();
        final short nbClick = me.getClickCount();
        final Tuple position = new DefaultTuple(me.getX(), me.getY());
        
        Vector screenPos = getOnScreenLocation();
        screenPos.localAdd(position);
        
        final boolean drag = me.isAnyButtonDown();
        final MouseMessage event;
        if(MouseEvent.EVENT_MOUSE_CLICKED == type){
            event = new MouseMessage(MouseMessage.TYPE_TYPED, me.getButton(), nbClick, position, screenPos, 0, false);
        }else if(MouseEvent.EVENT_MOUSE_DRAGGED == type){
            event = new MouseMessage(MouseMessage.TYPE_MOVE, me.getButton(), nbClick, position, screenPos, 0, true);
        }else if(MouseEvent.EVENT_MOUSE_ENTERED == type){
            event = new MouseMessage(MouseMessage.TYPE_ENTER, 0, nbClick, position, screenPos, 0, false);
        }else if(MouseEvent.EVENT_MOUSE_EXITED == type){
            event = new MouseMessage(MouseMessage.TYPE_EXIT, 0, nbClick, position, screenPos, 0, false);
        }else if(MouseEvent.EVENT_MOUSE_MOVED == type){
            event = new MouseMessage(MouseMessage.TYPE_MOVE, me.getButton(), nbClick, position, screenPos, 0, false);
        }else if(MouseEvent.EVENT_MOUSE_PRESSED == type){
            event = new MouseMessage(MouseMessage.TYPE_PRESS, me.getButton(), nbClick, position, screenPos, 0, false);
        }else if(MouseEvent.EVENT_MOUSE_RELEASED == type){
            event =  new MouseMessage(MouseMessage.TYPE_RELEASE, me.getButton(), nbClick, position, screenPos, 0, false);
        }else if(MouseEvent.EVENT_MOUSE_WHEEL_MOVED == type){
            event = new MouseMessage(MouseMessage.TYPE_WHEEL, 0, nbClick, position, screenPos, me.getRotation()[1], false);
        }else{
            throw new RuntimeException("Unknowned mouse event?");
        }

        return event;
    }

    private KeyMessage toUNEvent(KeyEvent ke){
        final int type = ke.getEventType();
        final KeyMessage event;
        if(KeyEvent.EVENT_KEY_PRESSED == type){
            event = new KeyMessage(KeyMessage.TYPE_PRESS, ke.getKeyChar(),ke.getKeyCode());
        }else if(KeyEvent.EVENT_KEY_RELEASED == type){
            event = new KeyMessage(KeyMessage.TYPE_RELEASE, ke.getKeyChar(),ke.getKeyCode());
        }else{
            throw new RuntimeException("Unknowned key event?");
        }

        return event;
    }

    /**
     * A new event from JOGAMP arrived, handle it.
     */
    protected void processEvent(MouseMessage me, KeyMessage ke){

        //send to listeners
        if(hasListeners()){
            if(me!=null && !me.isConsumed()) getEventManager().sendEvent(new Event(this, me));
            if(ke!=null && !ke.isConsumed()) getEventManager().sendEvent(new Event(this, ke));
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // EVENT SOURCE ////////////////////////////////////////////////////////////

    public Class[] getEventClasses() {
        return new Class[]{
            KeyMessage.class,
            MouseMessage.class
        };
    }

    /**
     *
     * @return true is pointer is visible
     */
    public boolean isPointerVisible() {
        return glWindow.isPointerVisible();
    }

    /**
     *
     * @param visible pointer visible state
     */
    public void setPointerVisible(boolean visible){
        glWindow.setPointerVisible(visible);
    }

    @Override
    public Cursor getCursor() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCursor(Cursor cursor) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isDecorated() {
        return !glWindow.isUndecorated();
    }

    @Override
    public void setDecorated(boolean decorated) {
        glWindow.setUndecorated(!decorated);
    }

    @Override
    public void setAlwaysonTop(boolean ontop) {
        glWindow.setAlwaysOnTop(ontop);
    }

    @Override
    public boolean isAlwaysOnTop() {
        return glWindow.isAlwaysOnTop();
    }

    @Override
    public void dispose() {
        disposed = true;
        glWindow.setVisible(false);
        glWindow.destroy();
        
    }

    @Override
    public boolean isDisposed() {
        return disposed;
    }

    @Override
    public void waitForDisposal() {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
