
package science.unlicense.impl.gpu.jogamp.openal;

/**
 * A Speaker which always produce the same amount of sound whatever the
 * node or listener positions. 
 * Use for background music or sounds.
 * 
 * @author Johann Sorel
 */
public class AmbiantSpeaker extends Speaker{
    
}
