package science.unlicense.impl.gpu.jogamp.openal;

import java.nio.ByteBuffer;
import science.unlicense.api.character.Chars;
import science.unlicense.api.collection.Dictionary;
import science.unlicense.api.collection.HashDictionary;
import science.unlicense.api.io.ArrayOutputStream;
import science.unlicense.api.io.DataOutputStream;
import science.unlicense.api.io.IOException;
import science.unlicense.api.media.AudioPacket;
import science.unlicense.api.media.AudioSamples;
import science.unlicense.api.media.AudioStreamMeta;
import science.unlicense.api.media.MediaPacket;
import science.unlicense.api.media.MediaReadStream;
import science.unlicense.api.media.MediaStore;
import science.unlicense.api.media.Medias;
import science.unlicense.api.number.NumberEncoding;
import science.unlicense.api.path.Path;
import static science.unlicense.impl.gpu.jogamp.openal.AudioContext.AL;
import science.unlicense.impl.gpu.jogamp.openal.resource.ALBuffer;
import science.unlicense.impl.gpu.jogamp.openal.resource.ALSource;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class RSFunctions {

    //TODO : make a real cache, weak/soft/phantom references.
    private static final Dictionary CACHE = new HashDictionary();
    
    private static synchronized ALBuffer read(Chars pathStr) throws IOException{
        ALBuffer data = (ALBuffer) CACHE.getValue(pathStr);
        if(data!=null) return data;
                    
        final Path path = Paths.resolve(pathStr);

        final MediaStore store = Medias.open(path);

        //tranform audio in a supported byte buffer
        final AudioStreamMeta meta = (AudioStreamMeta) store.getStreamsMeta()[0];
        final MediaReadStream reader = store.createReader(null);

        //recode stream in a stereo 16 bits per sample.
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out, NumberEncoding.LITTLE_ENDIAN);

        for (MediaPacket pack = reader.next(); pack != null; pack = reader.next()) {
            final AudioSamples samples = ((AudioPacket) pack).getSamples();
            final int[] audiosamples = samples.asPCM(null, 16);
            if (audiosamples.length == 1) {
                ds.writeUShort(audiosamples[0]);
                ds.writeUShort(audiosamples[0]);
            } else {
                ds.writeUShort(audiosamples[0]);
                ds.writeUShort(audiosamples[1]);
            }
        }
        
        final byte[] all = out.getBuffer().toArrayByte();

        //buffer which contains audio datas
        data = new ALBuffer();
        data.setFormat(AL.AL_FORMAT_STEREO16);
        data.setFrequency((int) meta.getSampleRate());
        data.setSize(all.length);
        data.setData(ByteBuffer.wrap(all));
        data.load();
        
        CACHE.add(pathStr, data);
        
        return data;
    }
    
    public static void playAudio(final Chars pathStr) throws IOException {
        new Thread() {

            @Override
            public void run() {
                try{
                    final ALBuffer data = read(pathStr);

                    //the audio source
                    final ALSource source = new ALSource();
                    source.setBuffer(data);
                    source.load();
                    source.play();

                    sleep(3000);
                    source.stop();
                    source.unload();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }

        }.start();

    }

}
