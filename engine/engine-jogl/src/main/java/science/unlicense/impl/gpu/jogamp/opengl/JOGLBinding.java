
package science.unlicense.impl.gpu.jogamp.opengl;

import com.jogamp.nativewindow.util.InsetsImmutable;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLOffscreenAutoDrawable;
import com.jogamp.opengl.GLProfile;
import science.unlicense.api.gpu.opengl.GLBinding;
import science.unlicense.api.gpu.opengl.GLFrame;
import science.unlicense.api.gpu.opengl.GLSource;
import science.unlicense.api.layout.Margin;

/**
 *
 * @author Johann Sorel
 */
public class JOGLBinding implements GLBinding {

    public static final GLBinding INSTANCE = new JOGLBinding();

    private static Margin margin = null;

    @Override
    public GLSource createOffScreen(int width, int height) {
        GLOffscreenAutoDrawable drawable = JOGLUtils.createOffscreenDrawable(width, height, GLProfile.getMaxProgrammable(true));
        JOGLSource source = new JOGLSource();
        source.setDrawable(drawable);
        return source;
    }

    @Override
    public GLFrame createFrame(boolean opaque) {
        return new JOGLFrame(opaque);
    }

    @Override
    public synchronized Margin getFrameMargin() {
        if(margin==null) {
            GLCapabilities glCapabilities = new GLCapabilities(GLProfile.getDefault());
            GLWindow glWindow = GLWindow.create(glCapabilities);

            final InsetsImmutable in = glWindow.getInsets();
            margin =  new Margin(in.getTopHeight(), in.getRightWidth(), in.getBottomHeight(), in.getLeftWidth());
        }
        return new Margin(margin.top, margin.right, margin.bottom, margin.left);
    }

}
