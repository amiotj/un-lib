
package science.unlicense.impl.gpu.jogamp.openal;

import science.unlicense.impl.gpu.jogamp.openal.resource.ALBuffer;
import science.unlicense.impl.gpu.jogamp.openal.resource.ALSource;
import science.unlicense.api.scenegraph.DefaultSceneNode;

/**
 * A speaker emit a serie of audio sequences.
 *
 * @author Johann Sorel
 */
public abstract class Speaker extends DefaultSceneNode {

    private final ALSource source = new ALSource();
    private ALBuffer data;

    public Speaker() {
        super(3);
    }

    public ALSource getSource() {
        return source;
    }

    public ALBuffer getData() {
        return data;
    }

    public void setData(ALBuffer data) {
        this.data = data;
    }

    public void load(){
        source.load();
        if(data!=null) data.load();
    }

    public void unload(){
        source.unload();
        if(data!=null)data.unload();
    }

}
