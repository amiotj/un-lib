
package science.unlicense.impl.gpu.jogamp.opengl;

import com.jogamp.opengl.GL2ES2;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import science.unlicense.api.character.CharArray;
import science.unlicense.api.exception.UnimplementedException;
import science.unlicense.api.gpu.GLBuffer;
import science.unlicense.api.gpu.opengl.GL1;
import science.unlicense.api.gpu.opengl.GL1ES;
import science.unlicense.api.gpu.opengl.GL2;
import science.unlicense.api.gpu.opengl.GL2ES3;
import science.unlicense.api.gpu.opengl.GL3;
import science.unlicense.api.gpu.opengl.GL4;
import science.unlicense.api.gpu.opengl.GLES220;

/**
 *
 * @author Johann Sorel
 */
public class JGL2ES2 implements GLES220,science.unlicense.api.gpu.opengl.GL2ES2{

    private JGL base;
    private GL2ES2 gl;

    JGL2ES2(JGL gl) {
        this.base = gl;
        this.gl = gl.gl.getGL2ES2();
    }

    @Override
    public boolean isGL1() {
        return base.isGL1();
    }

    @Override
    public boolean isGL2() {
        return base.isGL2();
    }

    @Override
    public boolean isGL3() {
        return base.isGL3();
    }

    @Override
    public boolean isGL4() {
        return base.isGL4();
    }

    @Override
    public boolean isGL1ES() {
        return base.isGL1ES();
    }

    @Override
    public boolean isGL2ES2() {
        return base.isGL2ES2();
    }

    @Override
    public boolean isGL2ES3() {
        return base.isGL2ES3();
    }

    public GL1 asGL1() {
        return base.asGL1();
    }

    public GL2 asGL2() {
        return base.asGL2();
    }

    public GL3 asGL3() {
        return base.asGL3();
    }

    public GL4 asGL4() {
        return base.asGL4();
    }

    public GL1ES asGL1ES() {
        return base.asGL1ES();
    }

    public science.unlicense.api.gpu.opengl.GL2ES2 asGL2ES2() {
        return base.asGL2ES2();
    }

    public GL2ES3 asGL2ES3() {
        return base.asGL2ES3();
    }

    @Override
    public void glActiveTexture(int texture) {
        gl.glActiveTexture(texture);
    }

    @Override
    public void glAttachShader(int program, int shader) {
        gl.glAttachShader(program, shader);
    }

    @Override
    public void glBindAttribLocation(int program, int index, CharArray name) {
        gl.glBindAttribLocation(program, index, name.toString());
    }

    @Override
    public void glBindBuffer(int target, int buffer) {
        gl.glBindBuffer(target, buffer);
    }

    @Override
    public void glBindFramebuffer(int target, int framebuffer) {
        gl.glBindFramebuffer(target, framebuffer);
    }

    @Override
    public void glBindRenderbuffer(int target, int renderbuffer) {
        gl.glBindRenderbuffer(target, renderbuffer);
    }

    @Override
    public void glBindTexture(int target, int texture) {
        gl.glBindTexture(target, texture);
    }

    @Override
    public void glBlendColor(float red, float green, float blue, float alpha) {
        gl.glBlendColor(red, green, blue, alpha);
    }

    @Override
    public void glBlendEquation(int mode) {
        gl.glBlendEquation(mode);
    }

    @Override
    public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
        gl.glBlendEquationSeparate(modeRGB, modeAlpha);
    }

    @Override
    public void glBlendFunc(int sfactor, int dfactor) {
        gl.glBlendFunc(sfactor, dfactor);
    }

    @Override
    public void glBlendFuncSeparate(int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha) {
        gl.glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
    }

    @Override
    public void glBufferData(int target, Buffer data, int usage) {
        gl.glBufferData(target, GLBuffer.byteSize(data), data, usage);
    }

    @Override
    public void glBufferSubData(int target, long offset, Buffer data) {
        gl.glBufferSubData(target, offset, GLBuffer.byteSize(data), data);
    }

    @Override
    public int glCheckFramebufferStatus(int target) {
        return gl.glCheckFramebufferStatus(target);
    }

    @Override
    public void glClear(int mask) {
        gl.glClear(mask);
    }

    @Override
    public void glClearColor(float red, float green, float blue, float alpha) {
        gl.glClearColor(red, green, blue, alpha);
    }

    @Override
    public void glClearDepthf(float d) {
        gl.glClearDepthf(d);
    }

    @Override
    public void glClearStencil(int s) {
        gl.glClearStencil(s);
    }

    @Override
    public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
        gl.glColorMask(red, green, blue, alpha);
    }

    @Override
    public void glCompileShader(int shader) {
        gl.glCompileShader(shader);
    }

    @Override
    public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, Buffer data) {
        gl.glCompressedTexImage2D(target, level, internalformat, width, height, border, GLBuffer.byteSize(data), data);
    }

    @Override
    public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, Buffer data) {
        gl.glCompressedTexSubImage2D( target,  level,  xoffset,  yoffset,  width,  height,  format, GLBuffer.byteSize(data), data);
    }

    @Override
    public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
        gl.glCopyTexImage2D(target, level, internalformat, x, y, width, height, border);
    }

    @Override
    public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
        gl.glCopyTexSubImage2D( target,  level,  xoffset,  yoffset,  x,  y,  width,  height);
    }

    @Override
    public int glCreateProgram() {
        return gl.glCreateProgram();
    }

    @Override
    public int glCreateShader(int type) {
        return gl.glCreateShader( type);
    }

    @Override
    public void glCullFace(int mode) {
        gl.glCullFace( mode);
    }

    @Override
    public void glDeleteBuffers(IntBuffer buffers) {
        gl.glDeleteBuffers(buffers.capacity(), buffers);
    }
    
    @Override
    public void glDeleteBuffers(int[] buffers) {
        gl.glDeleteBuffers(buffers.length, buffers, 0);
    }

    @Override
    public void glDeleteFramebuffers(IntBuffer framebuffers) {
        gl.glDeleteFramebuffers(framebuffers.capacity(), framebuffers);
    }

    @Override
    public void glDeleteFramebuffers(int[] framebuffers) {
        gl.glDeleteFramebuffers(framebuffers.length, framebuffers,0);
    }
    
    @Override
    public void glDeleteProgram(int program) {
        gl.glDeleteProgram( program);
    }

    @Override
    public void glDeleteRenderbuffers(IntBuffer renderbuffers) {
        gl.glDeleteRenderbuffers(renderbuffers.capacity(), renderbuffers);
    }

    @Override
    public void glDeleteShader(int shader) {
        gl.glDeleteShader( shader);
    }

    @Override
    public void glDeleteTextures(IntBuffer textures) {
        gl.glDeleteTextures(textures.capacity(), textures);
    }

    @Override
    public void glDepthFunc(int func) {
        gl.glDepthFunc( func);
    }

    @Override
    public void glDepthMask(boolean flag) {
        gl.glDepthMask( flag);
    }

    @Override
    public void glDepthRangef(float n, float f) {
        gl.glDepthRangef( n, f);
    }

    @Override
    public void glDetachShader(int program, int shader) {
        gl.glDetachShader( program, shader);
    }

    @Override
    public void glDisable(int cap) {
        gl.glDisable( cap);
    }

    @Override
    public void glDisableVertexAttribArray(int index) {
        gl.glDisableVertexAttribArray( index);
    }

    @Override
    public void glDrawArrays(int mode, int first, int count) {
        gl.glDrawArrays( mode, first, count);
    }

    @Override
    public void glDrawElements(int mode, int count, int type, long indices) {
        gl.glDrawElements( mode, count, type, indices);
    }

    @Override
    public void glEnable(int cap) {
        gl.glEnable( cap);
    }

    @Override
    public void glEnableVertexAttribArray(int index) {
        gl.glEnableVertexAttribArray( index);
    }

    @Override
    public void glFinish() {
        gl.glFinish();
    }

    @Override
    public void glFlush() {
        gl.glFlush();
    }

    @Override
    public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
        gl.glFramebufferRenderbuffer( target, attachment, renderbuffertarget, renderbuffer);
    }

    @Override
    public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
        gl.glFramebufferTexture2D( target, attachment, textarget, texture, level);
    }

    @Override
    public void glFrontFace(int mode) {
        gl.glFrontFace( mode);
    }

    @Override
    public void glGenBuffers(IntBuffer buffers) {
        gl.glGenBuffers(buffers.capacity(), buffers);
    }

    @Override
    public void glGenBuffers(int[] buffers) {
        gl.glGenBuffers(buffers.length,buffers,0);
    }
    
    @Override
    public void glGenerateMipmap(int target) {
        gl.glGenerateMipmap( target);
    }

    @Override
    public void glGenFramebuffers(IntBuffer framebuffers) {
        gl.glGenFramebuffers(framebuffers.capacity(), framebuffers);
    }

    @Override
    public void glGenFramebuffers(int[] framebuffers) {
        gl.glGenFramebuffers(framebuffers.length, framebuffers, 0);
    }
    
    @Override
    public void glGenRenderbuffers(IntBuffer renderbuffers) {
        gl.glGenRenderbuffers(renderbuffers.capacity(), renderbuffers);
    }

    @Override
    public void glGenTextures(IntBuffer textures) {
        gl.glGenTextures(textures.capacity(), textures);
    }

    @Override
    public void glGenTextures(int[] textures) {
        gl.glGenTextures(textures.length, textures, 0);
    }

    @Override
    public void glGetActiveAttrib(int program, int index, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
        throw new UnimplementedException("todo");
    }

    @Override
    public void glGetActiveUniform(int program, int index, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
        throw new UnimplementedException("todo");
    }

    @Override
    public void glGetAttachedShaders(int program, IntBuffer count, IntBuffer shaders) {
        gl.glGetAttachedShaders( program, shaders.capacity(), count, shaders);
    }

    @Override
    public int glGetAttribLocation(int program, CharArray name) {
        return gl.glGetAttribLocation( program, name.toString());
    }

    @Override
    public void glGetBooleanv(int pname, ByteBuffer data) {
        throw new UnimplementedException("todo");
    }

    @Override
    public void glGetBufferParameteriv(int target, int pname, IntBuffer params) {
        gl.glGetBufferParameteriv( target, pname, params);
    }

    @Override
    public int glGetError() {
        return gl.glGetError();
    }

    @Override
    public void glGetFloatv(int pname, FloatBuffer data) {
        gl.glGetFloatv( pname, data);
    }

    @Override
    public void glGetFloatv(int pname, float[] data) {
        gl.glGetFloatv( pname, data, 0);
    }

    @Override
    public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, IntBuffer params) {
        gl.glGetFramebufferAttachmentParameteriv( target, attachment, pname, params);
    }

    @Override
    public void glGetIntegerv(int pname, IntBuffer data) {
        gl.glGetIntegerv( pname, data);
    }

    @Override
    public void glGetIntegerv(int pname, int[] data) {
        gl.glGetIntegerv( pname, data, 0);
    }

    @Override
    public void glGetProgramiv(int program, int pname, IntBuffer params) {
        gl.glGetProgramiv(program, pname, params);
    }

    @Override
    public void glGetProgramiv(int program, int pname, int[] params) {
        gl.glGetProgramiv(program, pname, params,0);
    }

    @Override
    public void glGetProgramInfoLog(int program, int[] length, ByteBuffer infoLog) {
        gl.glGetProgramInfoLog(program, infoLog.capacity(), length==null ? null : IntBuffer.wrap(length), infoLog);
    }

    @Override
    public void glGetRenderbufferParameteriv(int target, int pname, IntBuffer params) {
        gl.glGetRenderbufferParameteriv( target, pname, params);
    }

    @Override
    public void glGetShaderiv(int shader, int pname, IntBuffer params) {
        gl.glGetShaderiv( shader, pname, params);
    }

    @Override
    public void glGetShaderiv(int shader, int pname, int[] params) {
        gl.glGetShaderiv( shader, pname, params, 0);
    }

    @Override
    public void glGetShaderInfoLog(int shader, IntBuffer length, ByteBuffer infoLog) {
        gl.glGetShaderInfoLog(shader, infoLog.capacity(), length, infoLog);
    }

    @Override
    public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, IntBuffer range, IntBuffer precision) {
        gl.glGetShaderPrecisionFormat( shadertype, precisiontype, range, precision);
    }

    @Override
    public void glGetShaderSource(int shader, IntBuffer length, ByteBuffer source) {
        throw new UnimplementedException("todo");
    }

    @Override
    public byte glGetString(int name) {
        throw new UnimplementedException("todo");
    }

    @Override
    public void glGetTexParameterfv(int target, int pname, FloatBuffer params) {
        gl.glGetTexParameterfv( target, pname, params);
    }

    @Override
    public void glGetTexParameteriv(int target, int pname, IntBuffer params) {
        gl.glGetTexParameteriv( target, pname, params);
    }

    @Override
    public void glGetUniformfv(int program, int location, FloatBuffer params) {
        gl.glGetUniformfv( program, location, params);
    }

    @Override
    public void glGetUniformiv(int program, int location, IntBuffer params) {
        gl.glGetUniformiv( program, location, params);
    }

    @Override
    public int glGetUniformLocation(int program, CharArray name) {
        return gl.glGetUniformLocation( program, name.toString());
    }

    @Override
    public void glGetVertexAttribfv(int index, int pname, FloatBuffer params) {
        gl.glGetVertexAttribfv( index, pname, params);
    }

    @Override
    public void glGetVertexAttribiv(int index, int pname, IntBuffer params) {
        gl.glGetVertexAttribiv( index, pname, params);
    }

    @Override
    public void glGetVertexAttribPointerv(int index, int pname, ByteBuffer pointer) {
        throw new UnimplementedException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void glHint(int target, int mode) {
        gl.glHint( target, mode);
    }

    @Override
    public boolean glIsBuffer(int buffer) {
        return gl.glIsBuffer( buffer);
    }

    @Override
    public boolean glIsEnabled(int cap) {
        return gl.glIsEnabled( cap);
    }

    @Override
    public boolean glIsFramebuffer(int framebuffer) {
        return gl.glIsFramebuffer( framebuffer);
    }

    @Override
    public boolean glIsProgram(int program) {
        return gl.glIsProgram( program);
    }

    @Override
    public boolean glIsRenderbuffer(int renderbuffer) {
        return gl.glIsRenderbuffer( renderbuffer);
    }

    @Override
    public boolean glIsShader(int shader) {
        return gl.glIsShader( shader);
    }

    @Override
    public boolean glIsTexture(int texture) {
        return gl.glIsTexture( texture);
    }

    @Override
    public void glLineWidth(float width) {
        gl.glLineWidth( width);
    }

    @Override
    public void glLinkProgram(int program) {
        gl.glLinkProgram( program);
    }

    @Override
    public void glPixelStorei(int pname, int param) {
        gl.glPixelStorei( pname, param);
    }

    @Override
    public void glPolygonOffset(float factor, float units) {
        gl.glPolygonOffset( factor, units);
    }

    @Override
    public void glReadPixels(int x, int y, int width, int height, int format, int type, ByteBuffer pixels) {
        gl.glReadPixels( x, y, width, height, format, type, pixels);
    }

    @Override
    public void glReleaseShaderCompiler() {
        gl.glReleaseShaderCompiler();
    }

    @Override
    public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
        gl.glRenderbufferStorage( target, internalformat, width, height);
    }

    @Override
    public void glSampleCoverage(float value, boolean invert) {
        gl.glSampleCoverage(value, invert);
    }

    @Override
    public void glScissor(int x, int y, int width, int height) {
        gl.glScissor(x, y, width, height);
    }

    @Override
    public void glShaderBinary(IntBuffer shaders, int binaryformat, ByteBuffer binary) {
        gl.glShaderBinary(shaders.capacity(), shaders, binaryformat, binary, binary.capacity());
    }

    @Override
    public void glShaderSource(int shader, CharArray[] strings) {
        final String[] jvmStrings = JGL.toString(strings);
        final int[] lengths = new int[jvmStrings.length];
        for(int i=0;i<lengths.length;i++) lengths[i] = jvmStrings[i].length();
        gl.glShaderSource(shader, jvmStrings.length, jvmStrings, lengths, 0);
    }

    @Override
    public void glStencilFunc(int func, int ref, int mask) {
        gl.glStencilFunc(func, ref, mask);
    }

    @Override
    public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
        gl.glStencilFuncSeparate(face, func, ref, mask);
    }

    @Override
    public void glStencilMask(int mask) {
        gl.glStencilMask(mask);
    }

    @Override
    public void glStencilMaskSeparate(int face, int mask) {
        gl.glStencilMaskSeparate(face, mask);
    }

    @Override
    public void glStencilOp(int fail, int zfail, int zpass) {
        gl.glStencilOp(fail, zfail, zpass);
    }

    @Override
    public void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass) {
        gl.glStencilOpSeparate(face, sfail, dpfail, dppass);
    }

    @Override
    public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
        gl.glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
    }

    @Override
    public void glTexParameterf(int target, int pname, float param) {
        gl.glTexParameterf(target, pname, param);
    }

    @Override
    public void glTexParameterfv(int target, int pname, FloatBuffer params) {
        gl.glTexParameterfv(target, pname, params);
    }

    @Override
    public void glTexParameteri(int target, int pname, int param) {
        gl.glTexParameteri(target, pname, param);
    }

    @Override
    public void glTexParameteriv(int target, int pname, IntBuffer params) {
        gl.glTexParameteriv(target, pname, params);
    }

    @Override
    public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, ByteBuffer pixels) {
        gl.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
    }

    @Override
    public void glUniform1fv(int location, FloatBuffer value) {
        gl.glUniform1fv(location, value.capacity()/1, value);
    }

    @Override
    public void glUniform1fv(int location, float[] value) {
        gl.glUniform1fv(location, value.length/1, value, 0);
    }

    @Override
    public void glUniform2fv(int location, FloatBuffer value) {
        gl.glUniform2fv(location, value.capacity()/2, value);
    }

    @Override
    public void glUniform2fv(int location, float[] value) {
        gl.glUniform2fv(location, value.length/2, value, 0);
    }

    @Override
    public void glUniform3fv(int location, FloatBuffer value) {
        gl.glUniform3fv(location, value.capacity()/3, value);
    }

    @Override
    public void glUniform3fv(int location, float[] value) {
        gl.glUniform3fv(location, value.length/3, value, 0);
    }

    @Override
    public void glUniform4fv(int location, FloatBuffer value) {
        gl.glUniform4fv(location, value.capacity()/4, value);
    }

    @Override
    public void glUniform4fv(int location, float[] value) {
        gl.glUniform4fv(location, value.length/4, value, 0);
    }

    @Override
    public void glUniform1iv(int location, IntBuffer value) {
        gl.glUniform1iv(location, value.capacity()/1, value);
    }

    @Override
    public void glUniform1iv(int location, int[] value) {
        gl.glUniform1iv(location, value.length/1, value, 0);
    }

    @Override
    public void glUniform2iv(int location, IntBuffer value) {
        gl.glUniform2iv(location, value.capacity()/2, value);
    }

    @Override
    public void glUniform2iv(int location, int[] value) {
        gl.glUniform2iv(location, value.length/2, value, 0);
    }

    @Override
    public void glUniform3iv(int location, IntBuffer value) {
        gl.glUniform3iv(location, value.capacity()/3, value);
    }

    @Override
    public void glUniform3iv(int location, int[] value) {
        gl.glUniform3iv(location, value.length/3, value, 0);
    }

    @Override
    public void glUniform4iv(int location, IntBuffer value) {
        gl.glUniform4iv(location, value.capacity()/4, value);
    }

    @Override
    public void glUniform4iv(int location, int[] value) {
        gl.glUniform4iv(location, value.length/4, value, 0);
    }

    @Override
    public void glUniform1f(int location, float v0) {
        gl.glUniform1f(location, v0);
    }

    @Override
    public void glUniform1i(int location, int v0) {
        gl.glUniform1i(location, v0);
    }

    @Override
    public void glUniform2f(int location, float v0, float v1) {
        gl.glUniform2f(location, v0, v1);
    }

    @Override
    public void glUniform2i(int location, int v0, int v1) {
        gl.glUniform2i(location, v0, v1);
    }

    @Override
    public void glUniform3f(int location, float v0, float v1, float v2) {
        gl.glUniform3f(location, v0, v1, v2);
    }

    @Override
    public void glUniform3i(int location, int v0, int v1, int v2) {
        gl.glUniform3i(location, v0, v1, v2);
    }

    @Override
    public void glUniform4f(int location, float v0, float v1, float v2, float v3) {
        gl.glUniform4f(location, v0, v1, v2, v3);
    }

    @Override
    public void glUniform4i(int location, int v0, int v1, int v2, int v3) {
        gl.glUniform4i(location, v0, v1, v2, v3);
    }

    @Override
    public void glUniformMatrix2fv(int location, boolean transpose, FloatBuffer value) {
        gl.glUniformMatrix2fv(location, value.capacity()/4, transpose, value);
    }

    @Override
    public void glUniformMatrix2fv (int location, boolean transpose, float[] value) {
        gl.glUniformMatrix2fv(location, value.length/4, transpose, value,0);
    }

    @Override
    public void glUniformMatrix3fv(int location, boolean transpose, FloatBuffer value) {
        gl.glUniformMatrix3fv(location, value.capacity()/9, transpose, value);
    }

    @Override
    public void glUniformMatrix3fv (int location, boolean transpose, float[] value) {
        gl.glUniformMatrix3fv(location, value.length/9, transpose, value,0);
    }

    @Override
    public void glUniformMatrix4fv(int location, boolean transpose, FloatBuffer value) {
        gl.glUniformMatrix4fv(location, value.capacity()/16, transpose, value);
    }

    @Override
    public void glUniformMatrix4fv (int location, boolean transpose, float[] value) {
        gl.glUniformMatrix4fv(location, value.length/16, transpose, value,0);
    }

    @Override
    public void glUseProgram(int program) {
        gl.glUseProgram(program);
    }

    @Override
    public void glValidateProgram(int program) {
        gl.glValidateProgram(program);
    }

    @Override
    public void glVertexAttrib1f(int index, float x) {
        gl.glVertexAttrib1f(index, x);
    }

    @Override
    public void glVertexAttrib1fv(int index, FloatBuffer v) {
        gl.glVertexAttrib1fv(index, v);
    }

    @Override
    public void glVertexAttrib2f(int index, float x, float y) {
        gl.glVertexAttrib2f(index, x, y);
    }

    @Override
    public void glVertexAttrib2fv(int index, FloatBuffer v) {
        gl.glVertexAttrib2fv(index, v);
    }

    @Override
    public void glVertexAttrib3f(int index, float x, float y, float z) {
        gl.glVertexAttrib3f(index, x, y, z);
    }

    @Override
    public void glVertexAttrib3fv(int index, FloatBuffer v) {
        gl.glVertexAttrib3fv(index, v);
    }

    @Override
    public void glVertexAttrib4f(int index, float x, float y, float z, float w) {
        gl.glVertexAttrib4f(index, x, y, z, w);
    }

    @Override
    public void glVertexAttrib4fv(int index, FloatBuffer v) {
        gl.glVertexAttrib4fv(index, v);
    }

    @Override
    public void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, long pointer) {
        gl.glVertexAttribPointer(index, size, type, normalized, stride, pointer);
    }

    @Override
    public void glViewport(int x, int y, int width, int height) {
        gl.glViewport(x, y, width, height);
    }

}